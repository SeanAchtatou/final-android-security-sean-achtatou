package com.uc.browser;

public final class R {

    public final class anim {

        /* renamed from: brD */
        public static final int alpha_juc_in = 2130968576;

        /* renamed from: brE */
        public static final int alpha_juc_out = 2130968577;

        /* renamed from: brF */
        public static final int alpha_out = 2130968578;

        /* renamed from: brG */
        public static final int alpha_out_activity = 2130968579;

        /* renamed from: brH */
        public static final int controlbar_fs_fade_in = 2130968580;

        /* renamed from: brI */
        public static final int controlbar_fs_fade_out = 2130968581;

        /* renamed from: brJ */
        public static final int controlbar_fs_urlbar_fade_in = 2130968582;

        /* renamed from: brK */
        public static final int controlbar_fs_urlbar_fade_out = 2130968583;

        /* renamed from: brL */
        public static final int fade_in_bookmarklist = 2130968585;

        /* renamed from: brM */
        public static final int fade_in_editor = 2130968586;

        /* renamed from: brN */
        public static final int fade_in_menubox = 2130968587;

        /* renamed from: brO */
        public static final int fade_out_bookmarklist = 2130968589;

        /* renamed from: brP */
        public static final int fade_out_editor = 2130968590;

        /* renamed from: brQ */
        public static final int fade_out_menubox = 2130968591;

        /* renamed from: brR */
        public static final int menu_popup = 2130968592;

        /* renamed from: brS */
        public static final int multi_window_in = 2130968593;

        /* renamed from: brT */
        public static final int multi_window_layout = 2130968594;

        /* renamed from: brU */
        public static final int multi_window_out = 2130968595;

        /* renamed from: brV */
        public static final int multiwindow_item_close = 2130968596;

        /* renamed from: brW */
        public static final int progress_runner = 2130968597;

        /* renamed from: brX */
        public static final int safe_shield = 2130968598;

        /* renamed from: brY */
        public static final int scale_up = 2130968599;

        /* renamed from: brZ */
        public static final int uccontextmenu_alpha_in = 2130968600;

        /* renamed from: bsa */
        public static final int uccontextmenu_scale_in = 2130968601;

        /* renamed from: bsb */
        public static final int winnum_anim = 2130968602;
        public static final int fade_in = 2130968584;
        public static final int fade_out = 2130968588;
        /* added by JADX */

        /* renamed from: alpha_juc_in  reason: collision with other field name */
        public static final int f1alpha_juc_in = 2130968576;
        /* added by JADX */

        /* renamed from: alpha_juc_out  reason: collision with other field name */
        public static final int f2alpha_juc_out = 2130968577;
        /* added by JADX */

        /* renamed from: alpha_out  reason: collision with other field name */
        public static final int f3alpha_out = 2130968578;
        /* added by JADX */

        /* renamed from: alpha_out_activity  reason: collision with other field name */
        public static final int f4alpha_out_activity = 2130968579;
        /* added by JADX */

        /* renamed from: controlbar_fs_fade_in  reason: collision with other field name */
        public static final int f5controlbar_fs_fade_in = 2130968580;
        /* added by JADX */

        /* renamed from: controlbar_fs_fade_out  reason: collision with other field name */
        public static final int f6controlbar_fs_fade_out = 2130968581;
        /* added by JADX */

        /* renamed from: controlbar_fs_urlbar_fade_in  reason: collision with other field name */
        public static final int f7controlbar_fs_urlbar_fade_in = 2130968582;
        /* added by JADX */

        /* renamed from: controlbar_fs_urlbar_fade_out  reason: collision with other field name */
        public static final int f8controlbar_fs_urlbar_fade_out = 2130968583;
        /* added by JADX */

        /* renamed from: fade_in_bookmarklist  reason: collision with other field name */
        public static final int f9fade_in_bookmarklist = 2130968585;
        /* added by JADX */

        /* renamed from: fade_in_editor  reason: collision with other field name */
        public static final int f10fade_in_editor = 2130968586;
        /* added by JADX */

        /* renamed from: fade_in_menubox  reason: collision with other field name */
        public static final int f11fade_in_menubox = 2130968587;
        /* added by JADX */

        /* renamed from: fade_out_bookmarklist  reason: collision with other field name */
        public static final int f12fade_out_bookmarklist = 2130968589;
        /* added by JADX */

        /* renamed from: fade_out_editor  reason: collision with other field name */
        public static final int f13fade_out_editor = 2130968590;
        /* added by JADX */

        /* renamed from: fade_out_menubox  reason: collision with other field name */
        public static final int f14fade_out_menubox = 2130968591;
        /* added by JADX */

        /* renamed from: menu_popup  reason: collision with other field name */
        public static final int f15menu_popup = 2130968592;
        /* added by JADX */

        /* renamed from: multi_window_in  reason: collision with other field name */
        public static final int f16multi_window_in = 2130968593;
        /* added by JADX */

        /* renamed from: multi_window_layout  reason: collision with other field name */
        public static final int f17multi_window_layout = 2130968594;
        /* added by JADX */

        /* renamed from: multi_window_out  reason: collision with other field name */
        public static final int f18multi_window_out = 2130968595;
        /* added by JADX */

        /* renamed from: multiwindow_item_close  reason: collision with other field name */
        public static final int f19multiwindow_item_close = 2130968596;
        /* added by JADX */

        /* renamed from: progress_runner  reason: collision with other field name */
        public static final int f20progress_runner = 2130968597;
        /* added by JADX */

        /* renamed from: safe_shield  reason: collision with other field name */
        public static final int f21safe_shield = 2130968598;
        /* added by JADX */

        /* renamed from: scale_up  reason: collision with other field name */
        public static final int f22scale_up = 2130968599;
        /* added by JADX */

        /* renamed from: uccontextmenu_alpha_in  reason: collision with other field name */
        public static final int f23uccontextmenu_alpha_in = 2130968600;
        /* added by JADX */

        /* renamed from: uccontextmenu_scale_in  reason: collision with other field name */
        public static final int f24uccontextmenu_scale_in = 2130968601;
        /* added by JADX */

        /* renamed from: winnum_anim  reason: collision with other field name */
        public static final int f25winnum_anim = 2130968602;
    }

    public final class array {

        /* renamed from: aRM */
        public static final int share_page = 2131034140;

        /* renamed from: bkN */
        public static final int browser_model = 2131034144;

        /* renamed from: bkO */
        public static final int camera_upload = 2131034141;

        /* renamed from: bkP */
        public static final int help_orders = 2131034145;

        /* renamed from: bkQ */
        public static final int mdisk_upload = 2131034142;

        /* renamed from: bkR */
        public static final int pref_download_app = 2131034112;

        /* renamed from: bkS */
        public static final int pref_download_finish_tips = 2131034113;

        /* renamed from: bkT */
        public static final int pref_encode = 2131034138;

        /* renamed from: bkU */
        public static final int pref_encode_values = 2131034131;

        /* renamed from: bkV */
        public static final int pref_imagequantity = 2131034137;

        /* renamed from: bkW */
        public static final int pref_land_mode = 2131034134;

        /* renamed from: bkX */
        public static final int pref_land_values = 2131034135;

        /* renamed from: bkY */
        public static final int pref_mynavi_lines = 2131034128;

        /* renamed from: bkZ */
        public static final int pref_page_up_down_location = 2131034126;

        /* renamed from: bla */
        public static final int pref_page_up_down_location_values = 2131034127;

        /* renamed from: blb */
        public static final int pref_picture_quality = 2131034116;

        /* renamed from: blc */
        public static final int pref_picture_quality_values = 2131034117;

        /* renamed from: bld */
        public static final int pref_pre_read_mode = 2131034120;

        /* renamed from: ble */
        public static final int pref_pre_read_mode_values = 2131034121;

        /* renamed from: blf */
        public static final int pref_proxy_server = 2131034124;

        /* renamed from: blg */
        public static final int pref_proxy_server_values = 2131034125;

        /* renamed from: blh */
        public static final int pref_save_password = 2131034146;

        /* renamed from: bli */
        public static final int pref_scan_mode_values = 2131034129;

        /* renamed from: blj */
        public static final int pref_textsize = 2131034114;

        /* renamed from: blk */
        public static final int pref_textsize_values = 2131034115;

        /* renamed from: bll */
        public static final int pref_thream = 2131034136;

        /* renamed from: blm */
        public static final int pref_thream_values = 2131034130;

        /* renamed from: bln */
        public static final int pref_user_agent = 2131034122;

        /* renamed from: blo */
        public static final int pref_user_agent_values = 2131034123;

        /* renamed from: blp */
        public static final int pref_website_safe = 2131034139;

        /* renamed from: blq */
        public static final int pref_word_subsection = 2131034118;

        /* renamed from: blr */
        public static final int pref_word_subsection_values = 2131034119;

        /* renamed from: bls */
        public static final int pref_zoom_mode = 2131034132;

        /* renamed from: blt */
        public static final int pref_zoom_values = 2131034133;

        /* renamed from: blu */
        public static final int view_image = 2131034143;
        /* added by JADX */

        /* renamed from: pref_download_app  reason: collision with other field name */
        public static final int f26pref_download_app = 2131034112;
        /* added by JADX */

        /* renamed from: pref_download_finish_tips  reason: collision with other field name */
        public static final int f27pref_download_finish_tips = 2131034113;
        /* added by JADX */

        /* renamed from: pref_textsize  reason: collision with other field name */
        public static final int f28pref_textsize = 2131034114;
        /* added by JADX */

        /* renamed from: pref_textsize_values  reason: collision with other field name */
        public static final int f29pref_textsize_values = 2131034115;
        /* added by JADX */

        /* renamed from: pref_picture_quality  reason: collision with other field name */
        public static final int f30pref_picture_quality = 2131034116;
        /* added by JADX */

        /* renamed from: pref_picture_quality_values  reason: collision with other field name */
        public static final int f31pref_picture_quality_values = 2131034117;
        /* added by JADX */

        /* renamed from: pref_word_subsection  reason: collision with other field name */
        public static final int f32pref_word_subsection = 2131034118;
        /* added by JADX */

        /* renamed from: pref_word_subsection_values  reason: collision with other field name */
        public static final int f33pref_word_subsection_values = 2131034119;
        /* added by JADX */

        /* renamed from: pref_pre_read_mode  reason: collision with other field name */
        public static final int f34pref_pre_read_mode = 2131034120;
        /* added by JADX */

        /* renamed from: pref_pre_read_mode_values  reason: collision with other field name */
        public static final int f35pref_pre_read_mode_values = 2131034121;
        /* added by JADX */

        /* renamed from: pref_user_agent  reason: collision with other field name */
        public static final int f36pref_user_agent = 2131034122;
        /* added by JADX */

        /* renamed from: pref_user_agent_values  reason: collision with other field name */
        public static final int f37pref_user_agent_values = 2131034123;
        /* added by JADX */

        /* renamed from: pref_proxy_server  reason: collision with other field name */
        public static final int f38pref_proxy_server = 2131034124;
        /* added by JADX */

        /* renamed from: pref_proxy_server_values  reason: collision with other field name */
        public static final int f39pref_proxy_server_values = 2131034125;
        /* added by JADX */

        /* renamed from: pref_page_up_down_location  reason: collision with other field name */
        public static final int f40pref_page_up_down_location = 2131034126;
        /* added by JADX */

        /* renamed from: pref_page_up_down_location_values  reason: collision with other field name */
        public static final int f41pref_page_up_down_location_values = 2131034127;
        /* added by JADX */

        /* renamed from: pref_mynavi_lines  reason: collision with other field name */
        public static final int f42pref_mynavi_lines = 2131034128;
        /* added by JADX */

        /* renamed from: pref_scan_mode_values  reason: collision with other field name */
        public static final int f43pref_scan_mode_values = 2131034129;
        /* added by JADX */

        /* renamed from: pref_thream_values  reason: collision with other field name */
        public static final int f44pref_thream_values = 2131034130;
        /* added by JADX */

        /* renamed from: pref_encode_values  reason: collision with other field name */
        public static final int f45pref_encode_values = 2131034131;
        /* added by JADX */

        /* renamed from: pref_zoom_mode  reason: collision with other field name */
        public static final int f46pref_zoom_mode = 2131034132;
        /* added by JADX */

        /* renamed from: pref_zoom_values  reason: collision with other field name */
        public static final int f47pref_zoom_values = 2131034133;
        /* added by JADX */

        /* renamed from: pref_land_mode  reason: collision with other field name */
        public static final int f48pref_land_mode = 2131034134;
        /* added by JADX */

        /* renamed from: pref_land_values  reason: collision with other field name */
        public static final int f49pref_land_values = 2131034135;
        /* added by JADX */

        /* renamed from: pref_thream  reason: collision with other field name */
        public static final int f50pref_thream = 2131034136;
        /* added by JADX */

        /* renamed from: pref_imagequantity  reason: collision with other field name */
        public static final int f51pref_imagequantity = 2131034137;
        /* added by JADX */

        /* renamed from: pref_encode  reason: collision with other field name */
        public static final int f52pref_encode = 2131034138;
        /* added by JADX */

        /* renamed from: pref_website_safe  reason: collision with other field name */
        public static final int f53pref_website_safe = 2131034139;
        /* added by JADX */

        /* renamed from: share_page  reason: collision with other field name */
        public static final int f54share_page = 2131034140;
        /* added by JADX */

        /* renamed from: camera_upload  reason: collision with other field name */
        public static final int f55camera_upload = 2131034141;
        /* added by JADX */

        /* renamed from: mdisk_upload  reason: collision with other field name */
        public static final int f56mdisk_upload = 2131034142;
        /* added by JADX */

        /* renamed from: view_image  reason: collision with other field name */
        public static final int f57view_image = 2131034143;
        /* added by JADX */

        /* renamed from: browser_model  reason: collision with other field name */
        public static final int f58browser_model = 2131034144;
        /* added by JADX */

        /* renamed from: help_orders  reason: collision with other field name */
        public static final int f59help_orders = 2131034145;
        /* added by JADX */

        /* renamed from: pref_save_password  reason: collision with other field name */
        public static final int f60pref_save_password = 2131034146;
    }

    public final class attr {

        /* renamed from: aXW */
        public static final int background_left = 2130771968;

        /* renamed from: aXX */
        public static final int background_right = 2130771969;

        /* renamed from: aXY */
        public static final int image_height = 2130771985;

        /* renamed from: aXZ */
        public static final int image_src = 2130771986;

        /* renamed from: aYa */
        public static final int image_width = 2130771984;

        /* renamed from: aYb */
        public static final int item_Height = 2130771989;

        /* renamed from: aYc */
        public static final int item_Width = 2130771988;

        /* renamed from: aYd */
        public static final int item_background_default = 2130771970;

        /* renamed from: aYe */
        public static final int item_background_focus = 2130771972;

        /* renamed from: aYf */
        public static final int item_background_pressed = 2130771973;

        /* renamed from: aYg */
        public static final int item_background_selected = 2130771971;

        /* renamed from: aYh */
        public static final int item_bgDefault = 2130771992;

        /* renamed from: aYi */
        public static final int item_bgFocus = 2130771993;

        /* renamed from: aYj */
        public static final int item_bgPress = 2130771994;

        /* renamed from: aYk */
        public static final int item_h_space = 2130771974;

        /* renamed from: aYl */
        public static final int item_height = 2130771975;

        /* renamed from: aYm */
        public static final int item_image_height = 2130771977;

        /* renamed from: aYn */
        public static final int item_image_width = 2130771978;

        /* renamed from: aYo */
        public static final int item_textColor = 2130771991;

        /* renamed from: aYp */
        public static final int item_textSize = 2130771990;

        /* renamed from: aYq */
        public static final int item_textVisibility = 2130771995;

        /* renamed from: aYr */
        public static final int item_width = 2130771976;

        /* renamed from: aYs */
        public static final int need_checkmark = 2130771980;

        /* renamed from: aYt */
        public static final int num_margin_left = 2130771982;

        /* renamed from: aYu */
        public static final int num_margin_top = 2130771983;

        /* renamed from: aYv */
        public static final int num_size = 2130771981;

        /* renamed from: aYw */
        public static final int shadow_width = 2130771979;
        public static final int background = 2130771987;
        /* added by JADX */

        /* renamed from: background_left  reason: collision with other field name */
        public static final int f61background_left = 2130771968;
        /* added by JADX */

        /* renamed from: background_right  reason: collision with other field name */
        public static final int f62background_right = 2130771969;
        /* added by JADX */

        /* renamed from: item_background_default  reason: collision with other field name */
        public static final int f63item_background_default = 2130771970;
        /* added by JADX */

        /* renamed from: item_background_selected  reason: collision with other field name */
        public static final int f64item_background_selected = 2130771971;
        /* added by JADX */

        /* renamed from: item_background_focus  reason: collision with other field name */
        public static final int f65item_background_focus = 2130771972;
        /* added by JADX */

        /* renamed from: item_background_pressed  reason: collision with other field name */
        public static final int f66item_background_pressed = 2130771973;
        /* added by JADX */

        /* renamed from: item_h_space  reason: collision with other field name */
        public static final int f67item_h_space = 2130771974;
        /* added by JADX */

        /* renamed from: item_height  reason: collision with other field name */
        public static final int f68item_height = 2130771975;
        /* added by JADX */

        /* renamed from: item_width  reason: collision with other field name */
        public static final int f69item_width = 2130771976;
        /* added by JADX */

        /* renamed from: item_image_height  reason: collision with other field name */
        public static final int f70item_image_height = 2130771977;
        /* added by JADX */

        /* renamed from: item_image_width  reason: collision with other field name */
        public static final int f71item_image_width = 2130771978;
        /* added by JADX */

        /* renamed from: shadow_width  reason: collision with other field name */
        public static final int f72shadow_width = 2130771979;
        /* added by JADX */

        /* renamed from: need_checkmark  reason: collision with other field name */
        public static final int f73need_checkmark = 2130771980;
        /* added by JADX */

        /* renamed from: num_size  reason: collision with other field name */
        public static final int f74num_size = 2130771981;
        /* added by JADX */

        /* renamed from: num_margin_left  reason: collision with other field name */
        public static final int f75num_margin_left = 2130771982;
        /* added by JADX */

        /* renamed from: num_margin_top  reason: collision with other field name */
        public static final int f76num_margin_top = 2130771983;
        /* added by JADX */

        /* renamed from: image_width  reason: collision with other field name */
        public static final int f77image_width = 2130771984;
        /* added by JADX */

        /* renamed from: image_height  reason: collision with other field name */
        public static final int f78image_height = 2130771985;
        /* added by JADX */

        /* renamed from: image_src  reason: collision with other field name */
        public static final int f79image_src = 2130771986;
        /* added by JADX */

        /* renamed from: item_Width  reason: collision with other field name */
        public static final int f80item_Width = 2130771988;
        /* added by JADX */

        /* renamed from: item_Height  reason: collision with other field name */
        public static final int f81item_Height = 2130771989;
        /* added by JADX */

        /* renamed from: item_textSize  reason: collision with other field name */
        public static final int f82item_textSize = 2130771990;
        /* added by JADX */

        /* renamed from: item_textColor  reason: collision with other field name */
        public static final int f83item_textColor = 2130771991;
        /* added by JADX */

        /* renamed from: item_bgDefault  reason: collision with other field name */
        public static final int f84item_bgDefault = 2130771992;
        /* added by JADX */

        /* renamed from: item_bgFocus  reason: collision with other field name */
        public static final int f85item_bgFocus = 2130771993;
        /* added by JADX */

        /* renamed from: item_bgPress  reason: collision with other field name */
        public static final int f86item_bgPress = 2130771994;
        /* added by JADX */

        /* renamed from: item_textVisibility  reason: collision with other field name */
        public static final int f87item_textVisibility = 2130771995;
    }

    public final class color {

        /* renamed from: aTK */
        public static final int bookmark_list_bg = 2131165207;

        /* renamed from: aXq */
        public static final int controlbar_fullscreen_bg = 2131165217;
        public static final int background = 2131165199;
        public static final int black = 2131165195;

        /* renamed from: bvA */
        public static final int channel_discribe_textcolor_highlight = 2131165256;

        /* renamed from: bvB */
        public static final int channel_list_divider = 2131165257;

        /* renamed from: bvC */
        public static final int channel_subtitle_textcolor = 2131165253;

        /* renamed from: bvD */
        public static final int channel_subtitle_textcolor_highlight = 2131165254;

        /* renamed from: bvE */
        public static final int channel_title_textcolor = 2131165251;

        /* renamed from: bvF */
        public static final int channel_title_textcolor_highlight = 2131165252;

        /* renamed from: bvG */
        public static final int controlbar_bg_gradian_bottom = 2131165214;

        /* renamed from: bvH */
        public static final int controlbar_bg_gradian_center = 2131165213;

        /* renamed from: bvI */
        public static final int controlbar_bg_gradian_top = 2131165212;

        /* renamed from: bvJ */
        public static final int controlbar_bg_line_second = 2131165211;

        /* renamed from: bvK */
        public static final int controlbar_bg_line_top = 2131165210;

        /* renamed from: bvL */
        public static final int controlbar_text = 2131165208;

        /* renamed from: bvM */
        public static final int controlbar_text_disable = 2131165209;

        /* renamed from: bvN */
        public static final int cover = 2131165226;

        /* renamed from: bvO */
        public static final int default_auto_completed_text = 2131165223;

        /* renamed from: bvP */
        public static final int default_bg = 2131165222;

        /* renamed from: bvQ */
        public static final int default_button_text = 2131165220;

        /* renamed from: bvR */
        public static final int default_list_bg = 2131165221;

        /* renamed from: bvS */
        public static final int default_text = 2131165218;

        /* renamed from: bvT */
        public static final int default_text_lowlight = 2131165219;

        /* renamed from: bvU */
        public static final int default_webview_bg = 2131165225;

        /* renamed from: bvV */
        public static final int dialog_button_text_color = 2131165238;

        /* renamed from: bvW */
        public static final int dialog_button_text_color_heightlight = 2131165239;

        /* renamed from: bvX */
        public static final int dialog_text_color = 2131165237;

        /* renamed from: bvY */
        public static final int gray = 2131165196;

        /* renamed from: bvZ */
        public static final int input_box_text_heightlight = 2131165204;

        /* renamed from: bvp */
        public static final int autocomplete_list_bg = 2131165224;

        /* renamed from: bvq */
        public static final int bookmark_divider_1 = 2131165249;

        /* renamed from: bvr */
        public static final int bookmark_divider_2 = 2131165250;

        /* renamed from: bvs */
        public static final int bookmark_item_divider = 2131165190;

        /* renamed from: bvt */
        public static final int bookmark_list_bg_toolbar_button_text_color = 2131165186;

        /* renamed from: bvu */
        public static final int bookmark_listbg_pressed_begin = 2131165191;

        /* renamed from: bvv */
        public static final int bookmark_listbg_pressed_end = 2131165192;

        /* renamed from: bvw */
        public static final int bookmark_tab_bg = 2131165206;

        /* renamed from: bvx */
        public static final int bookmark_tab_text_default_color = 2131165185;

        /* renamed from: bvy */
        public static final int bookmark_tab_text_selected_color = 2131165184;

        /* renamed from: bvz */
        public static final int channel_discribe_textcolor = 2131165255;

        /* renamed from: bwA */
        public static final int title_text = 2131165198;

        /* renamed from: bwB */
        public static final int translucent_white = 2131165227;

        /* renamed from: bwC */
        public static final int transparent2 = 2131165240;

        /* renamed from: bwD */
        public static final int uc_spinner_text_color = 2131165193;

        /* renamed from: bwE */
        public static final int uidialog_textcolor = 2131165248;

        /* renamed from: bwF */
        public static final int upload_font_msg = 2131165241;

        /* renamed from: bwG */
        public static final int upload_font_percentage_text = 2131165243;

        /* renamed from: bwH */
        public static final int upload_font_progress = 2131165242;

        /* renamed from: bwI */
        public static final int upload_font_text_color = 2131165229;

        /* renamed from: bwa */
        public static final int maincontent_gallery_bg = 2131165200;

        /* renamed from: bwb */
        public static final int maincontent_list_text_default = 2131165202;

        /* renamed from: bwc */
        public static final int maincontent_list_text_highlight = 2131165203;

        /* renamed from: bwd */
        public static final int maincontent_text_disable = 2131165201;

        /* renamed from: bwe */
        public static final int menu_text = 2131165215;

        /* renamed from: bwf */
        public static final int menu_text_disable = 2131165216;

        /* renamed from: bwg */
        public static final int multiwindow_new_window_button_textcolor = 2131165258;

        /* renamed from: bwh */
        public static final int my_bookmark_folder_title = 2131165187;

        /* renamed from: bwi */
        public static final int my_bookmark_website_name = 2131165188;

        /* renamed from: bwj */
        public static final int my_bookmark_website_url = 2131165189;

        /* renamed from: bwk */
        public static final int mynavi_edit_link_text_color = 2131165205;

        /* renamed from: bwl */
        public static final int navigation_bg_shadow_mask_bottom = 2131165260;

        /* renamed from: bwm */
        public static final int navigation_bg_shadow_mask_top = 2131165259;

        /* renamed from: bwn */
        public static final int navigation_folder_shadow_bottom = 2131165262;

        /* renamed from: bwo */
        public static final int navigation_folder_shadow_top = 2131165261;

        /* renamed from: bwp */
        public static final int plugin_text_day = 2131165235;

        /* renamed from: bwq */
        public static final int plugin_text_night = 2131165236;

        /* renamed from: bwr */
        public static final int pupup_menu_bg = 2131165233;

        /* renamed from: bws */
        public static final int pupup_menu_stroke = 2131165234;

        /* renamed from: bwt */
        public static final int search_engineview_list_bg = 2131165230;

        /* renamed from: bwu */
        public static final int search_engineview_title_bg = 2131165231;

        /* renamed from: bwv */
        public static final int search_engineview_title_edge = 2131165232;

        /* renamed from: bww */
        public static final int set_summary_color = 2131165245;

        /* renamed from: bwx */
        public static final int set_title_color = 2131165244;

        /* renamed from: bwy */
        public static final int set_zebra1_color = 2131165246;

        /* renamed from: bwz */
        public static final int set_zebra2_color = 2131165247;

        /* renamed from: gU */
        public static final int initial_loading_text = 2131165228;
        public static final int transparent = 2131165197;
        public static final int white = 2131165194;
        /* added by JADX */

        /* renamed from: bookmark_tab_text_selected_color  reason: collision with other field name */
        public static final int f88bookmark_tab_text_selected_color = 2131165184;
        /* added by JADX */

        /* renamed from: bookmark_tab_text_default_color  reason: collision with other field name */
        public static final int f89bookmark_tab_text_default_color = 2131165185;
        /* added by JADX */

        /* renamed from: bookmark_list_bg_toolbar_button_text_color  reason: collision with other field name */
        public static final int f90bookmark_list_bg_toolbar_button_text_color = 2131165186;
        /* added by JADX */

        /* renamed from: my_bookmark_folder_title  reason: collision with other field name */
        public static final int f91my_bookmark_folder_title = 2131165187;
        /* added by JADX */

        /* renamed from: my_bookmark_website_name  reason: collision with other field name */
        public static final int f92my_bookmark_website_name = 2131165188;
        /* added by JADX */

        /* renamed from: my_bookmark_website_url  reason: collision with other field name */
        public static final int f93my_bookmark_website_url = 2131165189;
        /* added by JADX */

        /* renamed from: bookmark_item_divider  reason: collision with other field name */
        public static final int f94bookmark_item_divider = 2131165190;
        /* added by JADX */

        /* renamed from: bookmark_listbg_pressed_begin  reason: collision with other field name */
        public static final int f95bookmark_listbg_pressed_begin = 2131165191;
        /* added by JADX */

        /* renamed from: bookmark_listbg_pressed_end  reason: collision with other field name */
        public static final int f96bookmark_listbg_pressed_end = 2131165192;
        /* added by JADX */

        /* renamed from: uc_spinner_text_color  reason: collision with other field name */
        public static final int f97uc_spinner_text_color = 2131165193;
        /* added by JADX */

        /* renamed from: gray  reason: collision with other field name */
        public static final int f98gray = 2131165196;
        /* added by JADX */

        /* renamed from: title_text  reason: collision with other field name */
        public static final int f99title_text = 2131165198;
        /* added by JADX */

        /* renamed from: maincontent_gallery_bg  reason: collision with other field name */
        public static final int f100maincontent_gallery_bg = 2131165200;
        /* added by JADX */

        /* renamed from: maincontent_text_disable  reason: collision with other field name */
        public static final int f101maincontent_text_disable = 2131165201;
        /* added by JADX */

        /* renamed from: maincontent_list_text_default  reason: collision with other field name */
        public static final int f102maincontent_list_text_default = 2131165202;
        /* added by JADX */

        /* renamed from: maincontent_list_text_highlight  reason: collision with other field name */
        public static final int f103maincontent_list_text_highlight = 2131165203;
        /* added by JADX */

        /* renamed from: input_box_text_heightlight  reason: collision with other field name */
        public static final int f104input_box_text_heightlight = 2131165204;
        /* added by JADX */

        /* renamed from: mynavi_edit_link_text_color  reason: collision with other field name */
        public static final int f105mynavi_edit_link_text_color = 2131165205;
        /* added by JADX */

        /* renamed from: bookmark_tab_bg  reason: collision with other field name */
        public static final int f106bookmark_tab_bg = 2131165206;
        /* added by JADX */

        /* renamed from: bookmark_list_bg  reason: collision with other field name */
        public static final int f107bookmark_list_bg = 2131165207;
        /* added by JADX */

        /* renamed from: controlbar_text  reason: collision with other field name */
        public static final int f108controlbar_text = 2131165208;
        /* added by JADX */

        /* renamed from: controlbar_text_disable  reason: collision with other field name */
        public static final int f109controlbar_text_disable = 2131165209;
        /* added by JADX */

        /* renamed from: controlbar_bg_line_top  reason: collision with other field name */
        public static final int f110controlbar_bg_line_top = 2131165210;
        /* added by JADX */

        /* renamed from: controlbar_bg_line_second  reason: collision with other field name */
        public static final int f111controlbar_bg_line_second = 2131165211;
        /* added by JADX */

        /* renamed from: controlbar_bg_gradian_top  reason: collision with other field name */
        public static final int f112controlbar_bg_gradian_top = 2131165212;
        /* added by JADX */

        /* renamed from: controlbar_bg_gradian_center  reason: collision with other field name */
        public static final int f113controlbar_bg_gradian_center = 2131165213;
        /* added by JADX */

        /* renamed from: controlbar_bg_gradian_bottom  reason: collision with other field name */
        public static final int f114controlbar_bg_gradian_bottom = 2131165214;
        /* added by JADX */

        /* renamed from: menu_text  reason: collision with other field name */
        public static final int f115menu_text = 2131165215;
        /* added by JADX */

        /* renamed from: menu_text_disable  reason: collision with other field name */
        public static final int f116menu_text_disable = 2131165216;
        /* added by JADX */

        /* renamed from: controlbar_fullscreen_bg  reason: collision with other field name */
        public static final int f117controlbar_fullscreen_bg = 2131165217;
        /* added by JADX */

        /* renamed from: default_text  reason: collision with other field name */
        public static final int f118default_text = 2131165218;
        /* added by JADX */

        /* renamed from: default_text_lowlight  reason: collision with other field name */
        public static final int f119default_text_lowlight = 2131165219;
        /* added by JADX */

        /* renamed from: default_button_text  reason: collision with other field name */
        public static final int f120default_button_text = 2131165220;
        /* added by JADX */

        /* renamed from: default_list_bg  reason: collision with other field name */
        public static final int f121default_list_bg = 2131165221;
        /* added by JADX */

        /* renamed from: default_bg  reason: collision with other field name */
        public static final int f122default_bg = 2131165222;
        /* added by JADX */

        /* renamed from: default_auto_completed_text  reason: collision with other field name */
        public static final int f123default_auto_completed_text = 2131165223;
        /* added by JADX */

        /* renamed from: autocomplete_list_bg  reason: collision with other field name */
        public static final int f124autocomplete_list_bg = 2131165224;
        /* added by JADX */

        /* renamed from: default_webview_bg  reason: collision with other field name */
        public static final int f125default_webview_bg = 2131165225;
        /* added by JADX */

        /* renamed from: cover  reason: collision with other field name */
        public static final int f126cover = 2131165226;
        /* added by JADX */

        /* renamed from: translucent_white  reason: collision with other field name */
        public static final int f127translucent_white = 2131165227;
        /* added by JADX */

        /* renamed from: initial_loading_text  reason: collision with other field name */
        public static final int f128initial_loading_text = 2131165228;
        /* added by JADX */

        /* renamed from: upload_font_text_color  reason: collision with other field name */
        public static final int f129upload_font_text_color = 2131165229;
        /* added by JADX */

        /* renamed from: search_engineview_list_bg  reason: collision with other field name */
        public static final int f130search_engineview_list_bg = 2131165230;
        /* added by JADX */

        /* renamed from: search_engineview_title_bg  reason: collision with other field name */
        public static final int f131search_engineview_title_bg = 2131165231;
        /* added by JADX */

        /* renamed from: search_engineview_title_edge  reason: collision with other field name */
        public static final int f132search_engineview_title_edge = 2131165232;
        /* added by JADX */

        /* renamed from: pupup_menu_bg  reason: collision with other field name */
        public static final int f133pupup_menu_bg = 2131165233;
        /* added by JADX */

        /* renamed from: pupup_menu_stroke  reason: collision with other field name */
        public static final int f134pupup_menu_stroke = 2131165234;
        /* added by JADX */

        /* renamed from: plugin_text_day  reason: collision with other field name */
        public static final int f135plugin_text_day = 2131165235;
        /* added by JADX */

        /* renamed from: plugin_text_night  reason: collision with other field name */
        public static final int f136plugin_text_night = 2131165236;
        /* added by JADX */

        /* renamed from: dialog_text_color  reason: collision with other field name */
        public static final int f137dialog_text_color = 2131165237;
        /* added by JADX */

        /* renamed from: dialog_button_text_color  reason: collision with other field name */
        public static final int f138dialog_button_text_color = 2131165238;
        /* added by JADX */

        /* renamed from: dialog_button_text_color_heightlight  reason: collision with other field name */
        public static final int f139dialog_button_text_color_heightlight = 2131165239;
        /* added by JADX */

        /* renamed from: transparent2  reason: collision with other field name */
        public static final int f140transparent2 = 2131165240;
        /* added by JADX */

        /* renamed from: upload_font_msg  reason: collision with other field name */
        public static final int f141upload_font_msg = 2131165241;
        /* added by JADX */

        /* renamed from: upload_font_progress  reason: collision with other field name */
        public static final int f142upload_font_progress = 2131165242;
        /* added by JADX */

        /* renamed from: upload_font_percentage_text  reason: collision with other field name */
        public static final int f143upload_font_percentage_text = 2131165243;
        /* added by JADX */

        /* renamed from: set_title_color  reason: collision with other field name */
        public static final int f144set_title_color = 2131165244;
        /* added by JADX */

        /* renamed from: set_summary_color  reason: collision with other field name */
        public static final int f145set_summary_color = 2131165245;
        /* added by JADX */

        /* renamed from: set_zebra1_color  reason: collision with other field name */
        public static final int f146set_zebra1_color = 2131165246;
        /* added by JADX */

        /* renamed from: set_zebra2_color  reason: collision with other field name */
        public static final int f147set_zebra2_color = 2131165247;
        /* added by JADX */

        /* renamed from: uidialog_textcolor  reason: collision with other field name */
        public static final int f148uidialog_textcolor = 2131165248;
        /* added by JADX */

        /* renamed from: bookmark_divider_1  reason: collision with other field name */
        public static final int f149bookmark_divider_1 = 2131165249;
        /* added by JADX */

        /* renamed from: bookmark_divider_2  reason: collision with other field name */
        public static final int f150bookmark_divider_2 = 2131165250;
        /* added by JADX */

        /* renamed from: channel_title_textcolor  reason: collision with other field name */
        public static final int f151channel_title_textcolor = 2131165251;
        /* added by JADX */

        /* renamed from: channel_title_textcolor_highlight  reason: collision with other field name */
        public static final int f152channel_title_textcolor_highlight = 2131165252;
        /* added by JADX */

        /* renamed from: channel_subtitle_textcolor  reason: collision with other field name */
        public static final int f153channel_subtitle_textcolor = 2131165253;
        /* added by JADX */

        /* renamed from: channel_subtitle_textcolor_highlight  reason: collision with other field name */
        public static final int f154channel_subtitle_textcolor_highlight = 2131165254;
        /* added by JADX */

        /* renamed from: channel_discribe_textcolor  reason: collision with other field name */
        public static final int f155channel_discribe_textcolor = 2131165255;
        /* added by JADX */

        /* renamed from: channel_discribe_textcolor_highlight  reason: collision with other field name */
        public static final int f156channel_discribe_textcolor_highlight = 2131165256;
        /* added by JADX */

        /* renamed from: channel_list_divider  reason: collision with other field name */
        public static final int f157channel_list_divider = 2131165257;
        /* added by JADX */

        /* renamed from: multiwindow_new_window_button_textcolor  reason: collision with other field name */
        public static final int f158multiwindow_new_window_button_textcolor = 2131165258;
        /* added by JADX */

        /* renamed from: navigation_bg_shadow_mask_top  reason: collision with other field name */
        public static final int f159navigation_bg_shadow_mask_top = 2131165259;
        /* added by JADX */

        /* renamed from: navigation_bg_shadow_mask_bottom  reason: collision with other field name */
        public static final int f160navigation_bg_shadow_mask_bottom = 2131165260;
        /* added by JADX */

        /* renamed from: navigation_folder_shadow_top  reason: collision with other field name */
        public static final int f161navigation_folder_shadow_top = 2131165261;
        /* added by JADX */

        /* renamed from: navigation_folder_shadow_bottom  reason: collision with other field name */
        public static final int f162navigation_folder_shadow_bottom = 2131165262;
    }

    public final class dimen {

        /* renamed from: aiA */
        public static final int add_sch_difference_right = 2131231038;

        /* renamed from: aiB */
        public static final int add_sch_edittext = 2131231057;

        /* renamed from: aiC */
        public static final int add_sch_engine_divider_height = 2131231078;

        /* renamed from: aiD */
        public static final int add_sch_engine_divider_marginleft = 2131231079;

        /* renamed from: aiE */
        public static final int add_sch_engine_divider_width = 2131231002;

        /* renamed from: aiF */
        public static final int add_sch_engine_dropdown_paddingleft = 2131231066;

        /* renamed from: aiG */
        public static final int add_sch_engine_text_height = 2131231076;

        /* renamed from: aiH */
        public static final int add_sch_engine_text_marginLeft = 2131231075;

        /* renamed from: aiI */
        public static final int add_sch_engine_text_marginTop = 2131231074;

        /* renamed from: aiJ */
        public static final int add_sch_engine_textsize = 2131231077;

        /* renamed from: aiK */
        public static final int add_sch_engineicon_layout_paddingleft = 2131231063;

        /* renamed from: aiL */
        public static final int add_sch_engineicon_paddingtop = 2131231065;

        /* renamed from: aiM */
        public static final int add_sch_engineicon_size = 2131231064;

        /* renamed from: aiN */
        public static final int add_sch_enginelist_height = 2131231004;

        /* renamed from: aiO */
        public static final int add_sch_enginelist_marginLeft = 2131231072;

        /* renamed from: aiP */
        public static final int add_sch_enginelist_marginTop = 2131231071;

        /* renamed from: aiQ */
        public static final int add_sch_enginelist_padding = 2131231073;

        /* renamed from: aiR */
        public static final int add_sch_enginelist_width = 2131231001;

        /* renamed from: aiS */
        public static final int add_sch_face_fix = 2131231032;

        /* renamed from: aiT */
        public static final int add_sch_face_size = 2131231030;

        /* renamed from: aiU */
        public static final int add_sch_height = 2131231039;

        /* renamed from: aiV */
        public static final int add_sch_height_input_state = 2131231040;

        /* renamed from: aiW */
        public static final int add_sch_inputlist_paddingbottom = 2131231070;

        /* renamed from: aiX */
        public static final int add_sch_inputlist_paddingleft = 2131231068;

        /* renamed from: aiY */
        public static final int add_sch_inputlist_paddingright = 2131231069;

        /* renamed from: aiZ */
        public static final int add_sch_inputlist_paddingtop = 2131231067;

        /* renamed from: aip */
        public static final int add_sch_add_edittext_paddingbottom = 2131231082;

        /* renamed from: aiq */
        public static final int add_sch_add_edittext_paddingleft = 2131231080;

        /* renamed from: air */
        public static final int add_sch_add_edittext_paddingright = 2131231081;

        /* renamed from: ais */
        public static final int add_sch_address_paddingleft = 2131231047;

        /* renamed from: ait */
        public static final int add_sch_address_paddingright = 2131231048;

        /* renamed from: aiu */
        public static final int add_sch_address_paddingtop = 2131231046;

        /* renamed from: aiv */
        public static final int add_sch_address_size = 2131231049;

        /* renamed from: aiw */
        public static final int add_sch_bookmark_width = 2131231089;

        /* renamed from: aix */
        public static final int add_sch_button_width = 2131231034;

        /* renamed from: aiy */
        public static final int add_sch_cancle_width = 2131231051;

        /* renamed from: aiz */
        public static final int add_sch_difference_left = 2131231037;

        /* renamed from: ajA */
        public static final int add_sch_text_margin_left2 = 2131231044;

        /* renamed from: ajB */
        public static final int add_sch_text_paddingtop = 2131231042;

        /* renamed from: ajC */
        public static final int add_sch_textsize = 2131231041;

        /* renamed from: ajD */
        public static final int add_sch_urlitem_paddingtop = 2131231091;

        /* renamed from: ajE */
        public static final int big_listitem_text = 2131230885;

        /* renamed from: ajF */
        public static final int bookmark_divider_margin = 2131230780;

        /* renamed from: ajG */
        public static final int bookmark_file_padding_left = 2131230770;

        /* renamed from: ajH */
        public static final int bookmark_file_padding_right = 2131230771;

        /* renamed from: ajI */
        public static final int bookmark_folder_font = 2131230777;

        /* renamed from: ajJ */
        public static final int bookmark_item_height = 2131230776;

        /* renamed from: ajK */
        public static final int bookmark_notip_font = 2131230781;

        /* renamed from: ajL */
        public static final int bookmark_selected_bg_padding = 2131230769;

        /* renamed from: ajM */
        public static final int bookmark_strip_height = 2131230783;

        /* renamed from: ajN */
        public static final int bookmark_subfile_divider_margin = 2131230786;

        /* renamed from: ajO */
        public static final int bookmark_subfile_padding_left = 2131230772;

        /* renamed from: ajP */
        public static final int bookmark_tab_edge = 2131230782;

        /* renamed from: ajQ */
        public static final int bookmark_tab_height = 2131230774;

        /* renamed from: ajR */
        public static final int bookmark_tab_top_padding = 2131230784;

        /* renamed from: ajS */
        public static final int bookmark_tab_width = 2131230775;

        /* renamed from: ajT */
        public static final int bookmark_tabfont = 2131230785;

        /* renamed from: ajU */
        public static final int bookmark_text_image_gap = 2131230773;

        /* renamed from: ajV */
        public static final int bookmark_text_padding_right = 2131230787;

        /* renamed from: ajW */
        public static final int bookmark_webname_font = 2131230778;

        /* renamed from: ajX */
        public static final int bookmark_weburl_font = 2131230779;

        /* renamed from: ajY */
        public static final int channel_bg_border = 2131230929;

        /* renamed from: ajZ */
        public static final int channel_bg_border_padding_lr = 2131230930;

        /* renamed from: aja */
        public static final int add_sch_inputstate_width = 2131231050;

        /* renamed from: ajb */
        public static final int add_sch_listitem_height = 2131231053;

        /* renamed from: ajc */
        public static final int add_sch_listitem_paddingbottom = 2131231056;

        /* renamed from: ajd */
        public static final int add_sch_listitem_paddingleft = 2131231054;

        /* renamed from: aje */
        public static final int add_sch_listitem_paddingtop = 2131231055;

        /* renamed from: ajf */
        public static final int add_sch_listitem_textsize = 2131231052;

        /* renamed from: ajg */
        public static final int add_sch_mainpage_text_paddingtop = 2131231045;

        /* renamed from: ajh */
        public static final int add_sch_offset_left = 2131231035;

        /* renamed from: aji */
        public static final int add_sch_offset_top = 2131231036;

        /* renamed from: ajj */
        public static final int add_sch_padding_left = 2131231031;

        /* renamed from: ajk */
        public static final int add_sch_padding_top = 2131231033;

        /* renamed from: ajl */
        public static final int add_sch_progress_paddingleft = 2131231090;

        /* renamed from: ajm */
        public static final int add_sch_sch_edittext_paddingbottom = 2131231085;

        /* renamed from: ajn */
        public static final int add_sch_sch_edittext_paddingleft = 2131231083;

        /* renamed from: ajo */
        public static final int add_sch_sch_edittext_paddingright = 2131231084;

        /* renamed from: ajp */
        public static final int add_sch_schengine_height = 2131231005;

        /* renamed from: ajq */
        public static final int add_sch_schengine_image_size = 2131231062;

        /* renamed from: ajr */
        public static final int add_sch_schengine_img_paddingbottom = 2131231060;

        /* renamed from: ajs */
        public static final int add_sch_schengine_img_paddingtop = 2131231059;

        /* renamed from: ajt */
        public static final int add_sch_schengine_padding = 2131231061;

        /* renamed from: aju */
        public static final int add_sch_schengine_text = 2131231058;

        /* renamed from: ajv */
        public static final int add_sch_schengine_width = 2131231003;

        /* renamed from: ajw */
        public static final int add_sch_state_text = 2131231086;

        /* renamed from: ajx */
        public static final int add_sch_state_text_paddingleft = 2131231087;

        /* renamed from: ajy */
        public static final int add_sch_state_text_paddingtop = 2131231088;

        /* renamed from: ajz */
        public static final int add_sch_text_margin_left = 2131231043;

        /* renamed from: akA */
        public static final int controlbar_button_image_height = 2131230747;

        /* renamed from: akB */
        public static final int controlbar_button_image_width = 2131230748;

        /* renamed from: akC */
        public static final int controlbar_button_text_padding_buttom = 2131230749;

        /* renamed from: akD */
        public static final int controlbar_fullscreen_height = 2131230735;

        /* renamed from: akE */
        public static final int controlbar_fullscreen_popup_width = 2131230736;

        /* renamed from: akF */
        public static final int controlbar_height = 2131230734;

        /* renamed from: akG */
        public static final int controlbar_item_paddingTop = 2131230744;

        /* renamed from: akH */
        public static final int controlbar_item_width = 2131230739;

        /* renamed from: akI */
        public static final int controlbar_item_width_2 = 2131230740;

        /* renamed from: akJ */
        public static final int controlbar_item_width_3 = 2131230741;

        /* renamed from: akK */
        public static final int controlbar_item_width_4 = 2131230742;

        /* renamed from: akL */
        public static final int controlbar_item_width_5 = 2131230743;

        /* renamed from: akM */
        public static final int controlbar_land_height = 2131230795;

        /* renamed from: akN */
        public static final int controlbar_min_container_height = 2131230799;

        /* renamed from: akO */
        public static final int controlbar_port_height = 2131230796;

        /* renamed from: akP */
        public static final int controlbar_text_size = 2131230745;

        /* renamed from: akQ */
        public static final int controlbar_winnum_margin_left = 2131230737;

        /* renamed from: akR */
        public static final int controlbar_winnum_margin_top = 2131230738;

        /* renamed from: akS */
        public static final int controlbar_winnum_text_size = 2131230746;

        /* renamed from: akT */
        public static final int dialog_button_height = 2131230857;

        /* renamed from: akU */
        public static final int dialog_button_margin = 2131230858;

        /* renamed from: akV */
        public static final int dialog_button_margin_land = 2131230872;

        /* renamed from: akW */
        public static final int dialog_button_textsize = 2131230870;

        /* renamed from: akX */
        public static final int dialog_button_width = 2131230856;

        /* renamed from: akY */
        public static final int dialog_content_height_fix = 2131230860;

        /* renamed from: akZ */
        public static final int dialog_content_max_height = 2131230859;

        /* renamed from: aka */
        public static final int channel_content_text_linespace = 2131230944;

        /* renamed from: akb */
        public static final int channel_content_text_size = 2131230943;

        /* renamed from: akc */
        public static final int channel_contenttext_padding_bottom = 2131230942;

        /* renamed from: akd */
        public static final int channel_contenttext_padding_left = 2131230939;

        /* renamed from: ake */
        public static final int channel_contenttext_padding_right = 2131230940;

        /* renamed from: akf */
        public static final int channel_contenttext_padding_top = 2131230941;

        /* renamed from: akg */
        public static final int channel_divider_height = 2131230925;

        /* renamed from: akh */
        public static final int channel_gallery_divider_height = 2131230802;

        /* renamed from: aki */
        public static final int channel_gallery_height = 2131230805;

        /* renamed from: akj */
        public static final int channel_gallery_item_height = 2131230806;

        /* renamed from: akk */
        public static final int channel_gallery_item_image_height = 2131230808;

        /* renamed from: akl */
        public static final int channel_gallery_item_image_width = 2131230809;

        /* renamed from: akm */
        public static final int channel_gallery_item_width = 2131230807;

        /* renamed from: akn */
        public static final int channel_icon_padding_left = 2131230932;

        /* renamed from: ako */
        public static final int channel_icon_padding_right = 2131230933;

        /* renamed from: akp */
        public static final int channel_item_describe_textsize = 2131230937;

        /* renamed from: akq */
        public static final int channel_item_height = 2131230934;

        /* renamed from: akr */
        public static final int channel_item_padding_top = 2131230931;

        /* renamed from: aks */
        public static final int channel_item_subtitle_textsize = 2131230936;

        /* renamed from: akt */
        public static final int channel_item_title_textsize = 2131230935;

        /* renamed from: aku */
        public static final int channel_padding_left = 2131230926;

        /* renamed from: akv */
        public static final int channel_padding_right = 2131230927;

        /* renamed from: akw */
        public static final int channel_padding_top = 2131230928;

        /* renamed from: akx */
        public static final int channel_title_bg_height = 2131230938;

        /* renamed from: aky */
        public static final int controlbar2_button_image_height = 2131230945;

        /* renamed from: akz */
        public static final int controlbar2_button_image_width = 2131230946;

        /* renamed from: alA */
        public static final int face_highlight_height = 2131231028;

        /* renamed from: alB */
        public static final int file_grid_icon_size = 2131230826;

        /* renamed from: alC */
        public static final int file_grid_item_height = 2131230827;

        /* renamed from: alD */
        public static final int file_grid_item_width = 2131230828;

        /* renamed from: alE */
        public static final int file_list_icon_size = 2131230825;

        /* renamed from: alF */
        public static final int flashview_height = 2131230879;

        /* renamed from: alG */
        public static final int flashview_width = 2131230878;

        /* renamed from: alH */
        public static final int free_copy_lollipop_margin_bottom = 2131230970;

        /* renamed from: alI */
        public static final int free_copy_lollipop_margin_top = 2131230969;

        /* renamed from: alJ */
        public static final int free_copy_shadow_width = 2131230968;

        /* renamed from: alK */
        public static final int free_menu_item_padding_h = 2131230965;

        /* renamed from: alL */
        public static final int free_menu_item_padding_v = 2131230966;

        /* renamed from: alM */
        public static final int free_menu_margin = 2131230967;

        /* renamed from: alN */
        public static final int free_menu_text_size = 2131230964;

        /* renamed from: alO */
        public static final int fresh_us_data_btn_margintop = 2131230843;

        /* renamed from: alP */
        public static final int fullscreen_upload_info_textsize = 2131230726;

        /* renamed from: alQ */
        public static final int history_bookmark_start_width = 2131230801;

        /* renamed from: alR */
        public static final int homepage_corner_size = 2131230720;

        /* renamed from: alS */
        public static final int init_addr_text = 2131230881;

        /* renamed from: alT */
        public static final int init_title_text = 2131230880;

        /* renamed from: alU */
        public static final int initial_loading_star_margin = 2131230830;

        /* renamed from: alV */
        public static final int initial_loading_text_margin = 2131230831;

        /* renamed from: alW */
        public static final int input_text_size = 2131230888;

        /* renamed from: alX */
        public static final int juc_download_buttontext_size = 2131230899;

        /* renamed from: alY */
        public static final int juc_download_filenametext_size = 2131230900;

        /* renamed from: alZ */
        public static final int juc_download_itemtext_size = 2131230903;

        /* renamed from: ala */
        public static final int dialog_content_max_height_land = 2131230873;

        /* renamed from: alb */
        public static final int dialog_edit_bookmark_prefix_width = 2131230853;

        /* renamed from: alc */
        public static final int dialog_edit_mynavi_line_height = 2131230850;

        /* renamed from: ald */
        public static final int dialog_edit_mynavi_line_space = 2131230849;

        /* renamed from: ale */
        public static final int dialog_edit_mynavi_prefix_width = 2131230851;

        /* renamed from: alf */
        public static final int dialog_frame_padding_bottom = 2131230869;

        /* renamed from: alg */
        public static final int dialog_frame_padding_bottom_land = 2131230877;

        /* renamed from: alh */
        public static final int dialog_frame_padding_left = 2131230867;

        /* renamed from: ali */
        public static final int dialog_frame_padding_left_land = 2131230875;

        /* renamed from: alj */
        public static final int dialog_frame_padding_right = 2131230868;

        /* renamed from: alk */
        public static final int dialog_frame_padding_right_land = 2131230876;

        /* renamed from: all */
        public static final int dialog_frame_padding_top = 2131230866;

        /* renamed from: alm */
        public static final int dialog_frame_padding_top_land = 2131230874;

        /* renamed from: aln */
        public static final int dialog_layout_margin = 2131230865;

        /* renamed from: alo */
        public static final int dialog_padding_bottom = 2131230864;

        /* renamed from: alp */
        public static final int dialog_padding_left = 2131230862;

        /* renamed from: alq */
        public static final int dialog_padding_right = 2131230863;

        /* renamed from: alr */
        public static final int dialog_padding_top = 2131230861;

        /* renamed from: als */
        public static final int dialog_spinner_line_height = 2131230852;

        /* renamed from: alt */
        public static final int dlg_app_list_max_list_height = 2131230727;

        /* renamed from: alu */
        public static final int dot_padding = 2131230836;

        /* renamed from: alv */
        public static final int dotset_margintop = 2131230835;

        /* renamed from: alw */
        public static final int edittext_padding_bottom = 2131230725;

        /* renamed from: alx */
        public static final int edittext_padding_top = 2131230724;

        /* renamed from: aly */
        public static final int face_border = 2131231027;

        /* renamed from: alz */
        public static final int face_frame_corner = 2131231029;

        /* renamed from: amA */
        public static final int menu_dialog_height = 2131230914;

        /* renamed from: amB */
        public static final int menu_dialog_landscape_max_height = 2131230920;

        /* renamed from: amC */
        public static final int menu_dialog_portrait_max_height = 2131230919;

        /* renamed from: amD */
        public static final int menu_h_space = 2131230812;

        /* renamed from: amE */
        public static final int menu_item_height = 2131230813;

        /* renamed from: amF */
        public static final int menu_item_icon_height = 2131230816;

        /* renamed from: amG */
        public static final int menu_item_icon_width = 2131230817;

        /* renamed from: amH */
        public static final int menu_item_padding_left_right = 2131230921;

        /* renamed from: amI */
        public static final int menu_item_padding_top_bottom = 2131230922;

        /* renamed from: amJ */
        public static final int menu_item_text_size = 2131230912;

        /* renamed from: amK */
        public static final int menu_item_width = 2131230814;

        /* renamed from: amL */
        public static final int menu_padding = 2131230815;

        /* renamed from: amM */
        public static final int menu_padding_text2icon = 2131230918;

        /* renamed from: amN */
        public static final int menu_tab_height = 2131230923;

        /* renamed from: amO */
        public static final int menu_tag_item_text_size = 2131230913;

        /* renamed from: amP */
        public static final int menu_text_size = 2131230883;

        /* renamed from: amQ */
        public static final int menu_v_space = 2131230811;

        /* renamed from: amR */
        public static final int multi_window_address_text_size = 2131230890;

        /* renamed from: amS */
        public static final int multi_window_numsquare_margin = 2131230991;

        /* renamed from: amT */
        public static final int multi_window_title_margin = 2131230990;

        /* renamed from: amU */
        public static final int multi_window_title_text_size = 2131230889;

        /* renamed from: amV */
        public static final int multiwindow_content_max_height = 2131231018;

        /* renamed from: amW */
        public static final int multiwindow_frame_height = 2131231013;

        /* renamed from: amX */
        public static final int multiwindow_frame_width = 2131231012;

        /* renamed from: amY */
        public static final int multiwindow_gallery_margin = 2131230992;

        /* renamed from: amZ */
        public static final int multiwindow_item_close_area_size = 2131230993;

        /* renamed from: ama */
        public static final int juc_download_speedtext_size = 2131230901;

        /* renamed from: amb */
        public static final int juc_download_statebar_size = 2131230902;

        /* renamed from: amc */
        public static final int juc_download_xoffset = 2131230898;

        /* renamed from: amd */
        public static final int juc_image_of_the_multiple = 2131230906;

        /* renamed from: ame */
        public static final int juc_multiple_font = 2131230904;

        /* renamed from: amf */
        public static final int juc_resolution = 2131230905;

        /* renamed from: amg */
        public static final int juc_text_large = 2131230897;

        /* renamed from: amh */
        public static final int juc_text_medium = 2131230896;

        /* renamed from: ami */
        public static final int juc_text_small = 2131230895;

        /* renamed from: amj */
        public static final int list_icon_height = 2131230800;

        /* renamed from: amk */
        public static final int list_item_height = 2131230722;

        /* renamed from: aml */
        public static final int list_scrollbar_size = 2131230723;

        /* renamed from: amm */
        public static final int listitem_title_text = 2131230884;

        /* renamed from: amn */
        public static final int loading_page_text_margin = 2131230973;

        /* renamed from: amo */
        public static final int loading_page_text_size = 2131230974;

        /* renamed from: amp */
        public static final int loading_text_marginbottom = 2131230838;

        /* renamed from: amq */
        public static final int loading_text_margintop = 2131230837;

        /* renamed from: amr */
        public static final int loading_text_size = 2131230839;

        /* renamed from: ams */
        public static final int mainpage_widget_padding = 2131230731;

        /* renamed from: amt */
        public static final int manifier_margin_end = 2131230972;

        /* renamed from: amu */
        public static final int manifier_margin_start = 2131230971;

        /* renamed from: amv */
        public static final int menu2_item_height = 2131230924;

        /* renamed from: amw */
        public static final int menu_bg_padding_left = 2131230915;

        /* renamed from: amx */
        public static final int menu_bg_padding_right = 2131230916;

        /* renamed from: amy */
        public static final int menu_bg_padding_top = 2131230917;

        /* renamed from: amz */
        public static final int menu_column_width = 2131230810;

        /* renamed from: anA */
        public static final int mutiwindowlist_maxtitletextlength = 2131230984;

        /* renamed from: anB */
        public static final int mutiwindowlist_point_padding_left = 2131230976;

        /* renamed from: anC */
        public static final int mutiwindowlist_point_padding_top = 2131230977;

        /* renamed from: anD */
        public static final int mutiwindowlist_selectarea_padding_x = 2131230982;

        /* renamed from: anE */
        public static final int mutiwindowlist_shortcuttext_padding_left = 2131230978;

        /* renamed from: anF */
        public static final int mutiwindowlist_shortcuttext_padding_top = 2131230979;

        /* renamed from: anG */
        public static final int mynavi_bar_height = 2131230750;

        /* renamed from: anH */
        public static final int mynavi_bar_height_double = 2131230751;

        /* renamed from: anI */
        public static final int mynavi_edit_list_tail_height = 2131230854;

        /* renamed from: anJ */
        public static final int mynavi_item_cols = 2131230763;

        /* renamed from: anK */
        public static final int mynavi_item_cols_land = 2131230764;

        /* renamed from: anL */
        public static final int mynavi_item_height = 2131230753;

        /* renamed from: anM */
        public static final int mynavi_item_padding_bottom = 2131230757;

        /* renamed from: anN */
        public static final int mynavi_item_padding_left = 2131230754;

        /* renamed from: anO */
        public static final int mynavi_item_padding_right = 2131230756;

        /* renamed from: anP */
        public static final int mynavi_item_padding_top = 2131230755;

        /* renamed from: anQ */
        public static final int mynavi_item_per_page = 2131230975;

        /* renamed from: anR */
        public static final int mynavi_item_textarea_height = 2131230762;

        /* renamed from: anS */
        public static final int mynavi_item_textsize = 2131230761;

        /* renamed from: anT */
        public static final int mynavi_item_width = 2131230752;

        /* renamed from: anU */
        public static final int mynavi_padding_left = 2131230759;

        /* renamed from: anV */
        public static final int mynavi_padding_right = 2131230760;

        /* renamed from: anW */
        public static final int mynavi_padding_top = 2131230758;

        /* renamed from: anX */
        public static final int navigation_channel_item_height = 2131230768;

        /* renamed from: anY */
        public static final int navigation_folder_shadow_height = 2131230766;

        /* renamed from: anZ */
        public static final int navigation_icon_padding = 2131230767;

        /* renamed from: ana */
        public static final int multiwindow_item_close_size = 2131230994;

        /* renamed from: anb */
        public static final int multiwindow_item_height = 2131230995;

        /* renamed from: anc */
        public static final int multiwindow_item_padding = 2131231000;

        /* renamed from: and */
        public static final int multiwindow_item_width = 2131230996;

        /* renamed from: ane */
        public static final int multiwindow_list_max_height = 2131230989;

        /* renamed from: anf */
        public static final int multiwindow_new_window_button_area_height = 2131231017;

        /* renamed from: ang */
        public static final int multiwindow_new_window_button_height = 2131231015;

        /* renamed from: anh */
        public static final int multiwindow_new_window_button_textsize = 2131231016;

        /* renamed from: ani */
        public static final int multiwindow_new_window_button_width = 2131231014;

        /* renamed from: anj */
        public static final int multiwindow_num_height = 2131230999;

        /* renamed from: ank */
        public static final int multiwindow_num_width = 2131230998;

        /* renamed from: anl */
        public static final int multiwindow_shadow_size = 2131230997;

        /* renamed from: anm */
        public static final int multiwindow_snapshot_height = 2131231011;

        /* renamed from: ann */
        public static final int multiwindow_snapshot_width = 2131231010;

        /* renamed from: ano */
        public static final int multiwindow_textsize = 2131230732;

        /* renamed from: anp */
        public static final int multiwindowlist_textsize = 2131230733;

        /* renamed from: anq */
        public static final int mutiwindow_height_winnum_2 = 2131231006;

        /* renamed from: anr */
        public static final int mutiwindow_height_winnum_4 = 2131231007;

        /* renamed from: ans */
        public static final int mutiwindow_item_height = 2131231009;

        /* renamed from: ant */
        public static final int mutiwindow_item_width = 2131231008;

        /* renamed from: anu */
        public static final int mutiwindow_shortcutpage_height = 2131230988;

        /* renamed from: anv */
        public static final int mutiwindow_shortcutpage_width = 2131230987;

        /* renamed from: anw */
        public static final int mutiwindowlist_closebutton_padding_right = 2131230981;

        /* renamed from: anx */
        public static final int mutiwindowlist_closebutton_padding_top = 2131230980;

        /* renamed from: any */
        public static final int mutiwindowlist_closebutton_size = 2131230983;

        /* renamed from: anz */
        public static final int mutiwindowlist_item_height = 2131230985;

        /* renamed from: aoA */
        public static final int search_inputurl_submit_mix = 2131230824;

        /* renamed from: aoB */
        public static final int set_arrow_margin = 2131230952;

        /* renamed from: aoC */
        public static final int set_item_height = 2131230953;

        /* renamed from: aoD */
        public static final int set_summary_font = 2131230950;

        /* renamed from: aoE */
        public static final int set_text_margin = 2131230951;

        /* renamed from: aoF */
        public static final int set_tilte_font = 2131230949;

        /* renamed from: aoG */
        public static final int spot_area_height = 2131230729;

        /* renamed from: aoH */
        public static final int spot_dot_padding = 2131230728;

        /* renamed from: aoI */
        public static final int spot_page_length = 2131230986;

        /* renamed from: aoJ */
        public static final int statusbar_height = 2131230794;

        /* renamed from: aoK */
        public static final int t_message_paddingBottom = 2131230910;

        /* renamed from: aoL */
        public static final int t_message_paddingLeft = 2131230908;

        /* renamed from: aoM */
        public static final int t_message_paddingRight = 2131230909;

        /* renamed from: aoN */
        public static final int t_message_textSize = 2131230911;

        /* renamed from: aoO */
        public static final int title_progress_height = 2131230797;

        /* renamed from: aoP */
        public static final int title_text_margin_bottom = 2131230798;

        /* renamed from: aoQ */
        public static final int title_text_size = 2131230882;

        /* renamed from: aoR */
        public static final int titlebar_height = 2131230793;

        /* renamed from: aoS */
        public static final int ucmascot_height = 2131230834;

        /* renamed from: aoT */
        public static final int ucmascot_margintop = 2131230832;

        /* renamed from: aoU */
        public static final int ucmascot_width = 2131230833;

        /* renamed from: aoV */
        public static final int ui_dialog_button_textsize = 2131230871;

        /* renamed from: aoW */
        public static final int uidialog1_font = 2131230954;

        /* renamed from: aoX */
        public static final int uidialog2_font = 2131230955;

        /* renamed from: aoY */
        public static final int uidialog_btn_height = 2131230957;

        /* renamed from: aoZ */
        public static final int uidialog_btn_width = 2131230956;

        /* renamed from: aoa */
        public static final int navigation_shadow_height = 2131230765;

        /* renamed from: aob */
        public static final int network_check_guide_font_size = 2131230948;

        /* renamed from: aoc */
        public static final int network_error_font_size = 2131230947;

        /* renamed from: aod */
        public static final int notice_arrow_gap = 2131230792;

        /* renamed from: aoe */
        public static final int notice_arrowedge_margin = 2131230789;

        /* renamed from: aof */
        public static final int notice_dialog_height = 2131230855;

        /* renamed from: aog */
        public static final int notice_font = 2131230788;

        /* renamed from: aoh */
        public static final int notice_shaderbottom_height = 2131230790;

        /* renamed from: aoi */
        public static final int notice_textcenter_offset = 2131230791;

        /* renamed from: aoj */
        public static final int page_seperator_height = 2131231019;

        /* renamed from: aok */
        public static final int prefix_title_size = 2131230891;

        /* renamed from: aol */
        public static final int progressbar_height = 2131230847;

        /* renamed from: aom */
        public static final int progressbar_text_padding = 2131230848;

        /* renamed from: aon */
        public static final int progressbar_text_size = 2131230892;

        /* renamed from: aoo */
        public static final int reading_mode_entrance_height = 2131231022;

        /* renamed from: aop */
        public static final int reading_mode_entrance_padding_right = 2131231023;

        /* renamed from: aoq */
        public static final int reading_mode_entrance_padding_top = 2131231024;

        /* renamed from: aor */
        public static final int reading_mode_entrance_width = 2131231021;

        /* renamed from: aos */
        public static final int reading_mode_tail_button_size = 2131231026;

        /* renamed from: aot */
        public static final int reading_mode_tail_height = 2131231020;

        /* renamed from: aou */
        public static final int reading_mode_tail_textsize = 2131231025;

        /* renamed from: aov */
        public static final int screen_width = 2131230829;

        /* renamed from: aow */
        public static final int search_inputurl_input_bar_height = 2131230820;

        /* renamed from: aox */
        public static final int search_inputurl_input_height = 2131230821;

        /* renamed from: aoy */
        public static final int search_inputurl_input_padding_v = 2131230822;

        /* renamed from: aoz */
        public static final int search_inputurl_submit_button_width = 2131230823;

        /* renamed from: apA */
        public static final int webwidget_thin_stroke = 2131231093;

        /* renamed from: apB */
        public static final int widget_bottom_padding = 2131230730;

        /* renamed from: apC */
        public static final int zoom_button_height = 2131230894;

        /* renamed from: apD */
        public static final int zoom_button_widht = 2131230893;

        /* renamed from: apE */
        public static final int zoomcontrol_padding_bottom = 2131230907;

        /* renamed from: apa */
        public static final int uidialog_margin = 2131230959;

        /* renamed from: apb */
        public static final int uidialog_title_height = 2131230958;

        /* renamed from: apc */
        public static final int unit_in = 2131230961;

        /* renamed from: apd */
        public static final int unit_mm = 2131230962;

        /* renamed from: ape */
        public static final int unit_pt = 2131230960;

        /* renamed from: apf */
        public static final int upload_font_progress_margin = 2131230844;

        /* renamed from: apg */
        public static final int upload_font_progress_text_size = 2131230845;

        /* renamed from: aph */
        public static final int upload_font_text_margintop = 2131230846;

        /* renamed from: api */
        public static final int urlitem_icon_height = 2131230819;

        /* renamed from: apj */
        public static final int urlitem_icon_width = 2131230818;

        /* renamed from: apk */
        public static final int urlitem_text_detail_size = 2131230887;

        /* renamed from: apl */
        public static final int urlitem_text_title_size = 2131230886;

        /* renamed from: apm */
        public static final int view_main_content_inputurl_height = 2131230803;

        /* renamed from: apn */
        public static final int view_main_content_inputurl_padding_v = 2131230804;

        /* renamed from: apo */
        public static final int wap10_line_space = 2131230963;

        /* renamed from: app */
        public static final int webpage_bottom_space = 2131230721;

        /* renamed from: apq */
        public static final int website_marginbottom = 2131230841;

        /* renamed from: apr */
        public static final int website_margintop = 2131230840;

        /* renamed from: aps */
        public static final int website_textsize = 2131230842;

        /* renamed from: apt */
        public static final int webwidget_bold_stroke = 2131231092;

        /* renamed from: apu */
        public static final int webwidget_button_corner = 2131231095;

        /* renamed from: apv */
        public static final int webwidget_checkbox_size = 2131231099;

        /* renamed from: apw */
        public static final int webwidget_frame_corner = 2131231094;

        /* renamed from: apx */
        public static final int webwidget_frame_shadow_bold = 2131231097;

        /* renamed from: apy */
        public static final int webwidget_frame_shadow_large = 2131231096;

        /* renamed from: apz */
        public static final int webwidget_frame_shadow_thin = 2131231098;
        /* added by JADX */

        /* renamed from: homepage_corner_size  reason: collision with other field name */
        public static final int f163homepage_corner_size = 2131230720;
        /* added by JADX */

        /* renamed from: webpage_bottom_space  reason: collision with other field name */
        public static final int f164webpage_bottom_space = 2131230721;
        /* added by JADX */

        /* renamed from: list_item_height  reason: collision with other field name */
        public static final int f165list_item_height = 2131230722;
        /* added by JADX */

        /* renamed from: list_scrollbar_size  reason: collision with other field name */
        public static final int f166list_scrollbar_size = 2131230723;
        /* added by JADX */

        /* renamed from: edittext_padding_top  reason: collision with other field name */
        public static final int f167edittext_padding_top = 2131230724;
        /* added by JADX */

        /* renamed from: edittext_padding_bottom  reason: collision with other field name */
        public static final int f168edittext_padding_bottom = 2131230725;
        /* added by JADX */

        /* renamed from: fullscreen_upload_info_textsize  reason: collision with other field name */
        public static final int f169fullscreen_upload_info_textsize = 2131230726;
        /* added by JADX */

        /* renamed from: dlg_app_list_max_list_height  reason: collision with other field name */
        public static final int f170dlg_app_list_max_list_height = 2131230727;
        /* added by JADX */

        /* renamed from: spot_dot_padding  reason: collision with other field name */
        public static final int f171spot_dot_padding = 2131230728;
        /* added by JADX */

        /* renamed from: spot_area_height  reason: collision with other field name */
        public static final int f172spot_area_height = 2131230729;
        /* added by JADX */

        /* renamed from: widget_bottom_padding  reason: collision with other field name */
        public static final int f173widget_bottom_padding = 2131230730;
        /* added by JADX */

        /* renamed from: mainpage_widget_padding  reason: collision with other field name */
        public static final int f174mainpage_widget_padding = 2131230731;
        /* added by JADX */

        /* renamed from: multiwindow_textsize  reason: collision with other field name */
        public static final int f175multiwindow_textsize = 2131230732;
        /* added by JADX */

        /* renamed from: multiwindowlist_textsize  reason: collision with other field name */
        public static final int f176multiwindowlist_textsize = 2131230733;
        /* added by JADX */

        /* renamed from: controlbar_height  reason: collision with other field name */
        public static final int f177controlbar_height = 2131230734;
        /* added by JADX */

        /* renamed from: controlbar_fullscreen_height  reason: collision with other field name */
        public static final int f178controlbar_fullscreen_height = 2131230735;
        /* added by JADX */

        /* renamed from: controlbar_fullscreen_popup_width  reason: collision with other field name */
        public static final int f179controlbar_fullscreen_popup_width = 2131230736;
        /* added by JADX */

        /* renamed from: controlbar_winnum_margin_left  reason: collision with other field name */
        public static final int f180controlbar_winnum_margin_left = 2131230737;
        /* added by JADX */

        /* renamed from: controlbar_winnum_margin_top  reason: collision with other field name */
        public static final int f181controlbar_winnum_margin_top = 2131230738;
        /* added by JADX */

        /* renamed from: controlbar_item_width  reason: collision with other field name */
        public static final int f182controlbar_item_width = 2131230739;
        /* added by JADX */

        /* renamed from: controlbar_item_width_2  reason: collision with other field name */
        public static final int f183controlbar_item_width_2 = 2131230740;
        /* added by JADX */

        /* renamed from: controlbar_item_width_3  reason: collision with other field name */
        public static final int f184controlbar_item_width_3 = 2131230741;
        /* added by JADX */

        /* renamed from: controlbar_item_width_4  reason: collision with other field name */
        public static final int f185controlbar_item_width_4 = 2131230742;
        /* added by JADX */

        /* renamed from: controlbar_item_width_5  reason: collision with other field name */
        public static final int f186controlbar_item_width_5 = 2131230743;
        /* added by JADX */

        /* renamed from: controlbar_item_paddingTop  reason: collision with other field name */
        public static final int f187controlbar_item_paddingTop = 2131230744;
        /* added by JADX */

        /* renamed from: controlbar_text_size  reason: collision with other field name */
        public static final int f188controlbar_text_size = 2131230745;
        /* added by JADX */

        /* renamed from: controlbar_winnum_text_size  reason: collision with other field name */
        public static final int f189controlbar_winnum_text_size = 2131230746;
        /* added by JADX */

        /* renamed from: controlbar_button_image_height  reason: collision with other field name */
        public static final int f190controlbar_button_image_height = 2131230747;
        /* added by JADX */

        /* renamed from: controlbar_button_image_width  reason: collision with other field name */
        public static final int f191controlbar_button_image_width = 2131230748;
        /* added by JADX */

        /* renamed from: controlbar_button_text_padding_buttom  reason: collision with other field name */
        public static final int f192controlbar_button_text_padding_buttom = 2131230749;
        /* added by JADX */

        /* renamed from: mynavi_bar_height  reason: collision with other field name */
        public static final int f193mynavi_bar_height = 2131230750;
        /* added by JADX */

        /* renamed from: mynavi_bar_height_double  reason: collision with other field name */
        public static final int f194mynavi_bar_height_double = 2131230751;
        /* added by JADX */

        /* renamed from: mynavi_item_width  reason: collision with other field name */
        public static final int f195mynavi_item_width = 2131230752;
        /* added by JADX */

        /* renamed from: mynavi_item_height  reason: collision with other field name */
        public static final int f196mynavi_item_height = 2131230753;
        /* added by JADX */

        /* renamed from: mynavi_item_padding_left  reason: collision with other field name */
        public static final int f197mynavi_item_padding_left = 2131230754;
        /* added by JADX */

        /* renamed from: mynavi_item_padding_top  reason: collision with other field name */
        public static final int f198mynavi_item_padding_top = 2131230755;
        /* added by JADX */

        /* renamed from: mynavi_item_padding_right  reason: collision with other field name */
        public static final int f199mynavi_item_padding_right = 2131230756;
        /* added by JADX */

        /* renamed from: mynavi_item_padding_bottom  reason: collision with other field name */
        public static final int f200mynavi_item_padding_bottom = 2131230757;
        /* added by JADX */

        /* renamed from: mynavi_padding_top  reason: collision with other field name */
        public static final int f201mynavi_padding_top = 2131230758;
        /* added by JADX */

        /* renamed from: mynavi_padding_left  reason: collision with other field name */
        public static final int f202mynavi_padding_left = 2131230759;
        /* added by JADX */

        /* renamed from: mynavi_padding_right  reason: collision with other field name */
        public static final int f203mynavi_padding_right = 2131230760;
        /* added by JADX */

        /* renamed from: mynavi_item_textsize  reason: collision with other field name */
        public static final int f204mynavi_item_textsize = 2131230761;
        /* added by JADX */

        /* renamed from: mynavi_item_textarea_height  reason: collision with other field name */
        public static final int f205mynavi_item_textarea_height = 2131230762;
        /* added by JADX */

        /* renamed from: mynavi_item_cols  reason: collision with other field name */
        public static final int f206mynavi_item_cols = 2131230763;
        /* added by JADX */

        /* renamed from: mynavi_item_cols_land  reason: collision with other field name */
        public static final int f207mynavi_item_cols_land = 2131230764;
        /* added by JADX */

        /* renamed from: navigation_shadow_height  reason: collision with other field name */
        public static final int f208navigation_shadow_height = 2131230765;
        /* added by JADX */

        /* renamed from: navigation_folder_shadow_height  reason: collision with other field name */
        public static final int f209navigation_folder_shadow_height = 2131230766;
        /* added by JADX */

        /* renamed from: navigation_icon_padding  reason: collision with other field name */
        public static final int f210navigation_icon_padding = 2131230767;
        /* added by JADX */

        /* renamed from: navigation_channel_item_height  reason: collision with other field name */
        public static final int f211navigation_channel_item_height = 2131230768;
        /* added by JADX */

        /* renamed from: bookmark_selected_bg_padding  reason: collision with other field name */
        public static final int f212bookmark_selected_bg_padding = 2131230769;
        /* added by JADX */

        /* renamed from: bookmark_file_padding_left  reason: collision with other field name */
        public static final int f213bookmark_file_padding_left = 2131230770;
        /* added by JADX */

        /* renamed from: bookmark_file_padding_right  reason: collision with other field name */
        public static final int f214bookmark_file_padding_right = 2131230771;
        /* added by JADX */

        /* renamed from: bookmark_subfile_padding_left  reason: collision with other field name */
        public static final int f215bookmark_subfile_padding_left = 2131230772;
        /* added by JADX */

        /* renamed from: bookmark_text_image_gap  reason: collision with other field name */
        public static final int f216bookmark_text_image_gap = 2131230773;
        /* added by JADX */

        /* renamed from: bookmark_tab_height  reason: collision with other field name */
        public static final int f217bookmark_tab_height = 2131230774;
        /* added by JADX */

        /* renamed from: bookmark_tab_width  reason: collision with other field name */
        public static final int f218bookmark_tab_width = 2131230775;
        /* added by JADX */

        /* renamed from: bookmark_item_height  reason: collision with other field name */
        public static final int f219bookmark_item_height = 2131230776;
        /* added by JADX */

        /* renamed from: bookmark_folder_font  reason: collision with other field name */
        public static final int f220bookmark_folder_font = 2131230777;
        /* added by JADX */

        /* renamed from: bookmark_webname_font  reason: collision with other field name */
        public static final int f221bookmark_webname_font = 2131230778;
        /* added by JADX */

        /* renamed from: bookmark_weburl_font  reason: collision with other field name */
        public static final int f222bookmark_weburl_font = 2131230779;
        /* added by JADX */

        /* renamed from: bookmark_divider_margin  reason: collision with other field name */
        public static final int f223bookmark_divider_margin = 2131230780;
        /* added by JADX */

        /* renamed from: bookmark_notip_font  reason: collision with other field name */
        public static final int f224bookmark_notip_font = 2131230781;
        /* added by JADX */

        /* renamed from: bookmark_tab_edge  reason: collision with other field name */
        public static final int f225bookmark_tab_edge = 2131230782;
        /* added by JADX */

        /* renamed from: bookmark_strip_height  reason: collision with other field name */
        public static final int f226bookmark_strip_height = 2131230783;
        /* added by JADX */

        /* renamed from: bookmark_tab_top_padding  reason: collision with other field name */
        public static final int f227bookmark_tab_top_padding = 2131230784;
        /* added by JADX */

        /* renamed from: bookmark_tabfont  reason: collision with other field name */
        public static final int f228bookmark_tabfont = 2131230785;
        /* added by JADX */

        /* renamed from: bookmark_subfile_divider_margin  reason: collision with other field name */
        public static final int f229bookmark_subfile_divider_margin = 2131230786;
        /* added by JADX */

        /* renamed from: bookmark_text_padding_right  reason: collision with other field name */
        public static final int f230bookmark_text_padding_right = 2131230787;
        /* added by JADX */

        /* renamed from: notice_font  reason: collision with other field name */
        public static final int f231notice_font = 2131230788;
        /* added by JADX */

        /* renamed from: notice_arrowedge_margin  reason: collision with other field name */
        public static final int f232notice_arrowedge_margin = 2131230789;
        /* added by JADX */

        /* renamed from: notice_shaderbottom_height  reason: collision with other field name */
        public static final int f233notice_shaderbottom_height = 2131230790;
        /* added by JADX */

        /* renamed from: notice_textcenter_offset  reason: collision with other field name */
        public static final int f234notice_textcenter_offset = 2131230791;
        /* added by JADX */

        /* renamed from: notice_arrow_gap  reason: collision with other field name */
        public static final int f235notice_arrow_gap = 2131230792;
        /* added by JADX */

        /* renamed from: titlebar_height  reason: collision with other field name */
        public static final int f236titlebar_height = 2131230793;
        /* added by JADX */

        /* renamed from: statusbar_height  reason: collision with other field name */
        public static final int f237statusbar_height = 2131230794;
        /* added by JADX */

        /* renamed from: controlbar_land_height  reason: collision with other field name */
        public static final int f238controlbar_land_height = 2131230795;
        /* added by JADX */

        /* renamed from: controlbar_port_height  reason: collision with other field name */
        public static final int f239controlbar_port_height = 2131230796;
        /* added by JADX */

        /* renamed from: title_progress_height  reason: collision with other field name */
        public static final int f240title_progress_height = 2131230797;
        /* added by JADX */

        /* renamed from: title_text_margin_bottom  reason: collision with other field name */
        public static final int f241title_text_margin_bottom = 2131230798;
        /* added by JADX */

        /* renamed from: controlbar_min_container_height  reason: collision with other field name */
        public static final int f242controlbar_min_container_height = 2131230799;
        /* added by JADX */

        /* renamed from: list_icon_height  reason: collision with other field name */
        public static final int f243list_icon_height = 2131230800;
        /* added by JADX */

        /* renamed from: history_bookmark_start_width  reason: collision with other field name */
        public static final int f244history_bookmark_start_width = 2131230801;
        /* added by JADX */

        /* renamed from: channel_gallery_divider_height  reason: collision with other field name */
        public static final int f245channel_gallery_divider_height = 2131230802;
        /* added by JADX */

        /* renamed from: view_main_content_inputurl_height  reason: collision with other field name */
        public static final int f246view_main_content_inputurl_height = 2131230803;
        /* added by JADX */

        /* renamed from: view_main_content_inputurl_padding_v  reason: collision with other field name */
        public static final int f247view_main_content_inputurl_padding_v = 2131230804;
        /* added by JADX */

        /* renamed from: channel_gallery_height  reason: collision with other field name */
        public static final int f248channel_gallery_height = 2131230805;
        /* added by JADX */

        /* renamed from: channel_gallery_item_height  reason: collision with other field name */
        public static final int f249channel_gallery_item_height = 2131230806;
        /* added by JADX */

        /* renamed from: channel_gallery_item_width  reason: collision with other field name */
        public static final int f250channel_gallery_item_width = 2131230807;
        /* added by JADX */

        /* renamed from: channel_gallery_item_image_height  reason: collision with other field name */
        public static final int f251channel_gallery_item_image_height = 2131230808;
        /* added by JADX */

        /* renamed from: channel_gallery_item_image_width  reason: collision with other field name */
        public static final int f252channel_gallery_item_image_width = 2131230809;
        /* added by JADX */

        /* renamed from: menu_column_width  reason: collision with other field name */
        public static final int f253menu_column_width = 2131230810;
        /* added by JADX */

        /* renamed from: menu_v_space  reason: collision with other field name */
        public static final int f254menu_v_space = 2131230811;
        /* added by JADX */

        /* renamed from: menu_h_space  reason: collision with other field name */
        public static final int f255menu_h_space = 2131230812;
        /* added by JADX */

        /* renamed from: menu_item_height  reason: collision with other field name */
        public static final int f256menu_item_height = 2131230813;
        /* added by JADX */

        /* renamed from: menu_item_width  reason: collision with other field name */
        public static final int f257menu_item_width = 2131230814;
        /* added by JADX */

        /* renamed from: menu_padding  reason: collision with other field name */
        public static final int f258menu_padding = 2131230815;
        /* added by JADX */

        /* renamed from: menu_item_icon_height  reason: collision with other field name */
        public static final int f259menu_item_icon_height = 2131230816;
        /* added by JADX */

        /* renamed from: menu_item_icon_width  reason: collision with other field name */
        public static final int f260menu_item_icon_width = 2131230817;
        /* added by JADX */

        /* renamed from: urlitem_icon_width  reason: collision with other field name */
        public static final int f261urlitem_icon_width = 2131230818;
        /* added by JADX */

        /* renamed from: urlitem_icon_height  reason: collision with other field name */
        public static final int f262urlitem_icon_height = 2131230819;
        /* added by JADX */

        /* renamed from: search_inputurl_input_bar_height  reason: collision with other field name */
        public static final int f263search_inputurl_input_bar_height = 2131230820;
        /* added by JADX */

        /* renamed from: search_inputurl_input_height  reason: collision with other field name */
        public static final int f264search_inputurl_input_height = 2131230821;
        /* added by JADX */

        /* renamed from: search_inputurl_input_padding_v  reason: collision with other field name */
        public static final int f265search_inputurl_input_padding_v = 2131230822;
        /* added by JADX */

        /* renamed from: search_inputurl_submit_button_width  reason: collision with other field name */
        public static final int f266search_inputurl_submit_button_width = 2131230823;
        /* added by JADX */

        /* renamed from: search_inputurl_submit_mix  reason: collision with other field name */
        public static final int f267search_inputurl_submit_mix = 2131230824;
        /* added by JADX */

        /* renamed from: file_list_icon_size  reason: collision with other field name */
        public static final int f268file_list_icon_size = 2131230825;
        /* added by JADX */

        /* renamed from: file_grid_icon_size  reason: collision with other field name */
        public static final int f269file_grid_icon_size = 2131230826;
        /* added by JADX */

        /* renamed from: file_grid_item_height  reason: collision with other field name */
        public static final int f270file_grid_item_height = 2131230827;
        /* added by JADX */

        /* renamed from: file_grid_item_width  reason: collision with other field name */
        public static final int f271file_grid_item_width = 2131230828;
        /* added by JADX */

        /* renamed from: screen_width  reason: collision with other field name */
        public static final int f272screen_width = 2131230829;
        /* added by JADX */

        /* renamed from: initial_loading_star_margin  reason: collision with other field name */
        public static final int f273initial_loading_star_margin = 2131230830;
        /* added by JADX */

        /* renamed from: initial_loading_text_margin  reason: collision with other field name */
        public static final int f274initial_loading_text_margin = 2131230831;
        /* added by JADX */

        /* renamed from: ucmascot_margintop  reason: collision with other field name */
        public static final int f275ucmascot_margintop = 2131230832;
        /* added by JADX */

        /* renamed from: ucmascot_width  reason: collision with other field name */
        public static final int f276ucmascot_width = 2131230833;
        /* added by JADX */

        /* renamed from: ucmascot_height  reason: collision with other field name */
        public static final int f277ucmascot_height = 2131230834;
        /* added by JADX */

        /* renamed from: dotset_margintop  reason: collision with other field name */
        public static final int f278dotset_margintop = 2131230835;
        /* added by JADX */

        /* renamed from: dot_padding  reason: collision with other field name */
        public static final int f279dot_padding = 2131230836;
        /* added by JADX */

        /* renamed from: loading_text_margintop  reason: collision with other field name */
        public static final int f280loading_text_margintop = 2131230837;
        /* added by JADX */

        /* renamed from: loading_text_marginbottom  reason: collision with other field name */
        public static final int f281loading_text_marginbottom = 2131230838;
        /* added by JADX */

        /* renamed from: loading_text_size  reason: collision with other field name */
        public static final int f282loading_text_size = 2131230839;
        /* added by JADX */

        /* renamed from: website_margintop  reason: collision with other field name */
        public static final int f283website_margintop = 2131230840;
        /* added by JADX */

        /* renamed from: website_marginbottom  reason: collision with other field name */
        public static final int f284website_marginbottom = 2131230841;
        /* added by JADX */

        /* renamed from: website_textsize  reason: collision with other field name */
        public static final int f285website_textsize = 2131230842;
        /* added by JADX */

        /* renamed from: fresh_us_data_btn_margintop  reason: collision with other field name */
        public static final int f286fresh_us_data_btn_margintop = 2131230843;
        /* added by JADX */

        /* renamed from: upload_font_progress_margin  reason: collision with other field name */
        public static final int f287upload_font_progress_margin = 2131230844;
        /* added by JADX */

        /* renamed from: upload_font_progress_text_size  reason: collision with other field name */
        public static final int f288upload_font_progress_text_size = 2131230845;
        /* added by JADX */

        /* renamed from: upload_font_text_margintop  reason: collision with other field name */
        public static final int f289upload_font_text_margintop = 2131230846;
        /* added by JADX */

        /* renamed from: progressbar_height  reason: collision with other field name */
        public static final int f290progressbar_height = 2131230847;
        /* added by JADX */

        /* renamed from: progressbar_text_padding  reason: collision with other field name */
        public static final int f291progressbar_text_padding = 2131230848;
        /* added by JADX */

        /* renamed from: dialog_edit_mynavi_line_space  reason: collision with other field name */
        public static final int f292dialog_edit_mynavi_line_space = 2131230849;
        /* added by JADX */

        /* renamed from: dialog_edit_mynavi_line_height  reason: collision with other field name */
        public static final int f293dialog_edit_mynavi_line_height = 2131230850;
        /* added by JADX */

        /* renamed from: dialog_edit_mynavi_prefix_width  reason: collision with other field name */
        public static final int f294dialog_edit_mynavi_prefix_width = 2131230851;
        /* added by JADX */

        /* renamed from: dialog_spinner_line_height  reason: collision with other field name */
        public static final int f295dialog_spinner_line_height = 2131230852;
        /* added by JADX */

        /* renamed from: dialog_edit_bookmark_prefix_width  reason: collision with other field name */
        public static final int f296dialog_edit_bookmark_prefix_width = 2131230853;
        /* added by JADX */

        /* renamed from: mynavi_edit_list_tail_height  reason: collision with other field name */
        public static final int f297mynavi_edit_list_tail_height = 2131230854;
        /* added by JADX */

        /* renamed from: notice_dialog_height  reason: collision with other field name */
        public static final int f298notice_dialog_height = 2131230855;
        /* added by JADX */

        /* renamed from: dialog_button_width  reason: collision with other field name */
        public static final int f299dialog_button_width = 2131230856;
        /* added by JADX */

        /* renamed from: dialog_button_height  reason: collision with other field name */
        public static final int f300dialog_button_height = 2131230857;
        /* added by JADX */

        /* renamed from: dialog_button_margin  reason: collision with other field name */
        public static final int f301dialog_button_margin = 2131230858;
        /* added by JADX */

        /* renamed from: dialog_content_max_height  reason: collision with other field name */
        public static final int f302dialog_content_max_height = 2131230859;
        /* added by JADX */

        /* renamed from: dialog_content_height_fix  reason: collision with other field name */
        public static final int f303dialog_content_height_fix = 2131230860;
        /* added by JADX */

        /* renamed from: dialog_padding_top  reason: collision with other field name */
        public static final int f304dialog_padding_top = 2131230861;
        /* added by JADX */

        /* renamed from: dialog_padding_left  reason: collision with other field name */
        public static final int f305dialog_padding_left = 2131230862;
        /* added by JADX */

        /* renamed from: dialog_padding_right  reason: collision with other field name */
        public static final int f306dialog_padding_right = 2131230863;
        /* added by JADX */

        /* renamed from: dialog_padding_bottom  reason: collision with other field name */
        public static final int f307dialog_padding_bottom = 2131230864;
        /* added by JADX */

        /* renamed from: dialog_layout_margin  reason: collision with other field name */
        public static final int f308dialog_layout_margin = 2131230865;
        /* added by JADX */

        /* renamed from: dialog_frame_padding_top  reason: collision with other field name */
        public static final int f309dialog_frame_padding_top = 2131230866;
        /* added by JADX */

        /* renamed from: dialog_frame_padding_left  reason: collision with other field name */
        public static final int f310dialog_frame_padding_left = 2131230867;
        /* added by JADX */

        /* renamed from: dialog_frame_padding_right  reason: collision with other field name */
        public static final int f311dialog_frame_padding_right = 2131230868;
        /* added by JADX */

        /* renamed from: dialog_frame_padding_bottom  reason: collision with other field name */
        public static final int f312dialog_frame_padding_bottom = 2131230869;
        /* added by JADX */

        /* renamed from: dialog_button_textsize  reason: collision with other field name */
        public static final int f313dialog_button_textsize = 2131230870;
        /* added by JADX */

        /* renamed from: ui_dialog_button_textsize  reason: collision with other field name */
        public static final int f314ui_dialog_button_textsize = 2131230871;
        /* added by JADX */

        /* renamed from: dialog_button_margin_land  reason: collision with other field name */
        public static final int f315dialog_button_margin_land = 2131230872;
        /* added by JADX */

        /* renamed from: dialog_content_max_height_land  reason: collision with other field name */
        public static final int f316dialog_content_max_height_land = 2131230873;
        /* added by JADX */

        /* renamed from: dialog_frame_padding_top_land  reason: collision with other field name */
        public static final int f317dialog_frame_padding_top_land = 2131230874;
        /* added by JADX */

        /* renamed from: dialog_frame_padding_left_land  reason: collision with other field name */
        public static final int f318dialog_frame_padding_left_land = 2131230875;
        /* added by JADX */

        /* renamed from: dialog_frame_padding_right_land  reason: collision with other field name */
        public static final int f319dialog_frame_padding_right_land = 2131230876;
        /* added by JADX */

        /* renamed from: dialog_frame_padding_bottom_land  reason: collision with other field name */
        public static final int f320dialog_frame_padding_bottom_land = 2131230877;
        /* added by JADX */

        /* renamed from: flashview_width  reason: collision with other field name */
        public static final int f321flashview_width = 2131230878;
        /* added by JADX */

        /* renamed from: flashview_height  reason: collision with other field name */
        public static final int f322flashview_height = 2131230879;
        /* added by JADX */

        /* renamed from: init_title_text  reason: collision with other field name */
        public static final int f323init_title_text = 2131230880;
        /* added by JADX */

        /* renamed from: init_addr_text  reason: collision with other field name */
        public static final int f324init_addr_text = 2131230881;
        /* added by JADX */

        /* renamed from: title_text_size  reason: collision with other field name */
        public static final int f325title_text_size = 2131230882;
        /* added by JADX */

        /* renamed from: menu_text_size  reason: collision with other field name */
        public static final int f326menu_text_size = 2131230883;
        /* added by JADX */

        /* renamed from: listitem_title_text  reason: collision with other field name */
        public static final int f327listitem_title_text = 2131230884;
        /* added by JADX */

        /* renamed from: big_listitem_text  reason: collision with other field name */
        public static final int f328big_listitem_text = 2131230885;
        /* added by JADX */

        /* renamed from: urlitem_text_title_size  reason: collision with other field name */
        public static final int f329urlitem_text_title_size = 2131230886;
        /* added by JADX */

        /* renamed from: urlitem_text_detail_size  reason: collision with other field name */
        public static final int f330urlitem_text_detail_size = 2131230887;
        /* added by JADX */

        /* renamed from: input_text_size  reason: collision with other field name */
        public static final int f331input_text_size = 2131230888;
        /* added by JADX */

        /* renamed from: multi_window_title_text_size  reason: collision with other field name */
        public static final int f332multi_window_title_text_size = 2131230889;
        /* added by JADX */

        /* renamed from: multi_window_address_text_size  reason: collision with other field name */
        public static final int f333multi_window_address_text_size = 2131230890;
        /* added by JADX */

        /* renamed from: prefix_title_size  reason: collision with other field name */
        public static final int f334prefix_title_size = 2131230891;
        /* added by JADX */

        /* renamed from: progressbar_text_size  reason: collision with other field name */
        public static final int f335progressbar_text_size = 2131230892;
        /* added by JADX */

        /* renamed from: zoom_button_widht  reason: collision with other field name */
        public static final int f336zoom_button_widht = 2131230893;
        /* added by JADX */

        /* renamed from: zoom_button_height  reason: collision with other field name */
        public static final int f337zoom_button_height = 2131230894;
        /* added by JADX */

        /* renamed from: juc_text_small  reason: collision with other field name */
        public static final int f338juc_text_small = 2131230895;
        /* added by JADX */

        /* renamed from: juc_text_medium  reason: collision with other field name */
        public static final int f339juc_text_medium = 2131230896;
        /* added by JADX */

        /* renamed from: juc_text_large  reason: collision with other field name */
        public static final int f340juc_text_large = 2131230897;
        /* added by JADX */

        /* renamed from: juc_download_xoffset  reason: collision with other field name */
        public static final int f341juc_download_xoffset = 2131230898;
        /* added by JADX */

        /* renamed from: juc_download_buttontext_size  reason: collision with other field name */
        public static final int f342juc_download_buttontext_size = 2131230899;
        /* added by JADX */

        /* renamed from: juc_download_filenametext_size  reason: collision with other field name */
        public static final int f343juc_download_filenametext_size = 2131230900;
        /* added by JADX */

        /* renamed from: juc_download_speedtext_size  reason: collision with other field name */
        public static final int f344juc_download_speedtext_size = 2131230901;
        /* added by JADX */

        /* renamed from: juc_download_statebar_size  reason: collision with other field name */
        public static final int f345juc_download_statebar_size = 2131230902;
        /* added by JADX */

        /* renamed from: juc_download_itemtext_size  reason: collision with other field name */
        public static final int f346juc_download_itemtext_size = 2131230903;
        /* added by JADX */

        /* renamed from: juc_multiple_font  reason: collision with other field name */
        public static final int f347juc_multiple_font = 2131230904;
        /* added by JADX */

        /* renamed from: juc_resolution  reason: collision with other field name */
        public static final int f348juc_resolution = 2131230905;
        /* added by JADX */

        /* renamed from: juc_image_of_the_multiple  reason: collision with other field name */
        public static final int f349juc_image_of_the_multiple = 2131230906;
        /* added by JADX */

        /* renamed from: zoomcontrol_padding_bottom  reason: collision with other field name */
        public static final int f350zoomcontrol_padding_bottom = 2131230907;
        /* added by JADX */

        /* renamed from: t_message_paddingLeft  reason: collision with other field name */
        public static final int f351t_message_paddingLeft = 2131230908;
        /* added by JADX */

        /* renamed from: t_message_paddingRight  reason: collision with other field name */
        public static final int f352t_message_paddingRight = 2131230909;
        /* added by JADX */

        /* renamed from: t_message_paddingBottom  reason: collision with other field name */
        public static final int f353t_message_paddingBottom = 2131230910;
        /* added by JADX */

        /* renamed from: t_message_textSize  reason: collision with other field name */
        public static final int f354t_message_textSize = 2131230911;
        /* added by JADX */

        /* renamed from: menu_item_text_size  reason: collision with other field name */
        public static final int f355menu_item_text_size = 2131230912;
        /* added by JADX */

        /* renamed from: menu_tag_item_text_size  reason: collision with other field name */
        public static final int f356menu_tag_item_text_size = 2131230913;
        /* added by JADX */

        /* renamed from: menu_dialog_height  reason: collision with other field name */
        public static final int f357menu_dialog_height = 2131230914;
        /* added by JADX */

        /* renamed from: menu_bg_padding_left  reason: collision with other field name */
        public static final int f358menu_bg_padding_left = 2131230915;
        /* added by JADX */

        /* renamed from: menu_bg_padding_right  reason: collision with other field name */
        public static final int f359menu_bg_padding_right = 2131230916;
        /* added by JADX */

        /* renamed from: menu_bg_padding_top  reason: collision with other field name */
        public static final int f360menu_bg_padding_top = 2131230917;
        /* added by JADX */

        /* renamed from: menu_padding_text2icon  reason: collision with other field name */
        public static final int f361menu_padding_text2icon = 2131230918;
        /* added by JADX */

        /* renamed from: menu_dialog_portrait_max_height  reason: collision with other field name */
        public static final int f362menu_dialog_portrait_max_height = 2131230919;
        /* added by JADX */

        /* renamed from: menu_dialog_landscape_max_height  reason: collision with other field name */
        public static final int f363menu_dialog_landscape_max_height = 2131230920;
        /* added by JADX */

        /* renamed from: menu_item_padding_left_right  reason: collision with other field name */
        public static final int f364menu_item_padding_left_right = 2131230921;
        /* added by JADX */

        /* renamed from: menu_item_padding_top_bottom  reason: collision with other field name */
        public static final int f365menu_item_padding_top_bottom = 2131230922;
        /* added by JADX */

        /* renamed from: menu_tab_height  reason: collision with other field name */
        public static final int f366menu_tab_height = 2131230923;
        /* added by JADX */

        /* renamed from: menu2_item_height  reason: collision with other field name */
        public static final int f367menu2_item_height = 2131230924;
        /* added by JADX */

        /* renamed from: channel_divider_height  reason: collision with other field name */
        public static final int f368channel_divider_height = 2131230925;
        /* added by JADX */

        /* renamed from: channel_padding_left  reason: collision with other field name */
        public static final int f369channel_padding_left = 2131230926;
        /* added by JADX */

        /* renamed from: channel_padding_right  reason: collision with other field name */
        public static final int f370channel_padding_right = 2131230927;
        /* added by JADX */

        /* renamed from: channel_padding_top  reason: collision with other field name */
        public static final int f371channel_padding_top = 2131230928;
        /* added by JADX */

        /* renamed from: channel_bg_border  reason: collision with other field name */
        public static final int f372channel_bg_border = 2131230929;
        /* added by JADX */

        /* renamed from: channel_bg_border_padding_lr  reason: collision with other field name */
        public static final int f373channel_bg_border_padding_lr = 2131230930;
        /* added by JADX */

        /* renamed from: channel_item_padding_top  reason: collision with other field name */
        public static final int f374channel_item_padding_top = 2131230931;
        /* added by JADX */

        /* renamed from: channel_icon_padding_left  reason: collision with other field name */
        public static final int f375channel_icon_padding_left = 2131230932;
        /* added by JADX */

        /* renamed from: channel_icon_padding_right  reason: collision with other field name */
        public static final int f376channel_icon_padding_right = 2131230933;
        /* added by JADX */

        /* renamed from: channel_item_height  reason: collision with other field name */
        public static final int f377channel_item_height = 2131230934;
        /* added by JADX */

        /* renamed from: channel_item_title_textsize  reason: collision with other field name */
        public static final int f378channel_item_title_textsize = 2131230935;
        /* added by JADX */

        /* renamed from: channel_item_subtitle_textsize  reason: collision with other field name */
        public static final int f379channel_item_subtitle_textsize = 2131230936;
        /* added by JADX */

        /* renamed from: channel_item_describe_textsize  reason: collision with other field name */
        public static final int f380channel_item_describe_textsize = 2131230937;
        /* added by JADX */

        /* renamed from: channel_title_bg_height  reason: collision with other field name */
        public static final int f381channel_title_bg_height = 2131230938;
        /* added by JADX */

        /* renamed from: channel_contenttext_padding_left  reason: collision with other field name */
        public static final int f382channel_contenttext_padding_left = 2131230939;
        /* added by JADX */

        /* renamed from: channel_contenttext_padding_right  reason: collision with other field name */
        public static final int f383channel_contenttext_padding_right = 2131230940;
        /* added by JADX */

        /* renamed from: channel_contenttext_padding_top  reason: collision with other field name */
        public static final int f384channel_contenttext_padding_top = 2131230941;
        /* added by JADX */

        /* renamed from: channel_contenttext_padding_bottom  reason: collision with other field name */
        public static final int f385channel_contenttext_padding_bottom = 2131230942;
        /* added by JADX */

        /* renamed from: channel_content_text_size  reason: collision with other field name */
        public static final int f386channel_content_text_size = 2131230943;
        /* added by JADX */

        /* renamed from: channel_content_text_linespace  reason: collision with other field name */
        public static final int f387channel_content_text_linespace = 2131230944;
        /* added by JADX */

        /* renamed from: controlbar2_button_image_height  reason: collision with other field name */
        public static final int f388controlbar2_button_image_height = 2131230945;
        /* added by JADX */

        /* renamed from: controlbar2_button_image_width  reason: collision with other field name */
        public static final int f389controlbar2_button_image_width = 2131230946;
        /* added by JADX */

        /* renamed from: network_error_font_size  reason: collision with other field name */
        public static final int f390network_error_font_size = 2131230947;
        /* added by JADX */

        /* renamed from: network_check_guide_font_size  reason: collision with other field name */
        public static final int f391network_check_guide_font_size = 2131230948;
        /* added by JADX */

        /* renamed from: set_tilte_font  reason: collision with other field name */
        public static final int f392set_tilte_font = 2131230949;
        /* added by JADX */

        /* renamed from: set_summary_font  reason: collision with other field name */
        public static final int f393set_summary_font = 2131230950;
        /* added by JADX */

        /* renamed from: set_text_margin  reason: collision with other field name */
        public static final int f394set_text_margin = 2131230951;
        /* added by JADX */

        /* renamed from: set_arrow_margin  reason: collision with other field name */
        public static final int f395set_arrow_margin = 2131230952;
        /* added by JADX */

        /* renamed from: set_item_height  reason: collision with other field name */
        public static final int f396set_item_height = 2131230953;
        /* added by JADX */

        /* renamed from: uidialog1_font  reason: collision with other field name */
        public static final int f397uidialog1_font = 2131230954;
        /* added by JADX */

        /* renamed from: uidialog2_font  reason: collision with other field name */
        public static final int f398uidialog2_font = 2131230955;
        /* added by JADX */

        /* renamed from: uidialog_btn_width  reason: collision with other field name */
        public static final int f399uidialog_btn_width = 2131230956;
        /* added by JADX */

        /* renamed from: uidialog_btn_height  reason: collision with other field name */
        public static final int f400uidialog_btn_height = 2131230957;
        /* added by JADX */

        /* renamed from: uidialog_title_height  reason: collision with other field name */
        public static final int f401uidialog_title_height = 2131230958;
        /* added by JADX */

        /* renamed from: uidialog_margin  reason: collision with other field name */
        public static final int f402uidialog_margin = 2131230959;
        /* added by JADX */

        /* renamed from: unit_pt  reason: collision with other field name */
        public static final int f403unit_pt = 2131230960;
        /* added by JADX */

        /* renamed from: unit_in  reason: collision with other field name */
        public static final int f404unit_in = 2131230961;
        /* added by JADX */

        /* renamed from: unit_mm  reason: collision with other field name */
        public static final int f405unit_mm = 2131230962;
        /* added by JADX */

        /* renamed from: wap10_line_space  reason: collision with other field name */
        public static final int f406wap10_line_space = 2131230963;
        /* added by JADX */

        /* renamed from: free_menu_text_size  reason: collision with other field name */
        public static final int f407free_menu_text_size = 2131230964;
        /* added by JADX */

        /* renamed from: free_menu_item_padding_h  reason: collision with other field name */
        public static final int f408free_menu_item_padding_h = 2131230965;
        /* added by JADX */

        /* renamed from: free_menu_item_padding_v  reason: collision with other field name */
        public static final int f409free_menu_item_padding_v = 2131230966;
        /* added by JADX */

        /* renamed from: free_menu_margin  reason: collision with other field name */
        public static final int f410free_menu_margin = 2131230967;
        /* added by JADX */

        /* renamed from: free_copy_shadow_width  reason: collision with other field name */
        public static final int f411free_copy_shadow_width = 2131230968;
        /* added by JADX */

        /* renamed from: free_copy_lollipop_margin_top  reason: collision with other field name */
        public static final int f412free_copy_lollipop_margin_top = 2131230969;
        /* added by JADX */

        /* renamed from: free_copy_lollipop_margin_bottom  reason: collision with other field name */
        public static final int f413free_copy_lollipop_margin_bottom = 2131230970;
        /* added by JADX */

        /* renamed from: manifier_margin_start  reason: collision with other field name */
        public static final int f414manifier_margin_start = 2131230971;
        /* added by JADX */

        /* renamed from: manifier_margin_end  reason: collision with other field name */
        public static final int f415manifier_margin_end = 2131230972;
        /* added by JADX */

        /* renamed from: loading_page_text_margin  reason: collision with other field name */
        public static final int f416loading_page_text_margin = 2131230973;
        /* added by JADX */

        /* renamed from: loading_page_text_size  reason: collision with other field name */
        public static final int f417loading_page_text_size = 2131230974;
        /* added by JADX */

        /* renamed from: mynavi_item_per_page  reason: collision with other field name */
        public static final int f418mynavi_item_per_page = 2131230975;
        /* added by JADX */

        /* renamed from: mutiwindowlist_point_padding_left  reason: collision with other field name */
        public static final int f419mutiwindowlist_point_padding_left = 2131230976;
        /* added by JADX */

        /* renamed from: mutiwindowlist_point_padding_top  reason: collision with other field name */
        public static final int f420mutiwindowlist_point_padding_top = 2131230977;
        /* added by JADX */

        /* renamed from: mutiwindowlist_shortcuttext_padding_left  reason: collision with other field name */
        public static final int f421mutiwindowlist_shortcuttext_padding_left = 2131230978;
        /* added by JADX */

        /* renamed from: mutiwindowlist_shortcuttext_padding_top  reason: collision with other field name */
        public static final int f422mutiwindowlist_shortcuttext_padding_top = 2131230979;
        /* added by JADX */

        /* renamed from: mutiwindowlist_closebutton_padding_top  reason: collision with other field name */
        public static final int f423mutiwindowlist_closebutton_padding_top = 2131230980;
        /* added by JADX */

        /* renamed from: mutiwindowlist_closebutton_padding_right  reason: collision with other field name */
        public static final int f424mutiwindowlist_closebutton_padding_right = 2131230981;
        /* added by JADX */

        /* renamed from: mutiwindowlist_selectarea_padding_x  reason: collision with other field name */
        public static final int f425mutiwindowlist_selectarea_padding_x = 2131230982;
        /* added by JADX */

        /* renamed from: mutiwindowlist_closebutton_size  reason: collision with other field name */
        public static final int f426mutiwindowlist_closebutton_size = 2131230983;
        /* added by JADX */

        /* renamed from: mutiwindowlist_maxtitletextlength  reason: collision with other field name */
        public static final int f427mutiwindowlist_maxtitletextlength = 2131230984;
        /* added by JADX */

        /* renamed from: mutiwindowlist_item_height  reason: collision with other field name */
        public static final int f428mutiwindowlist_item_height = 2131230985;
        /* added by JADX */

        /* renamed from: spot_page_length  reason: collision with other field name */
        public static final int f429spot_page_length = 2131230986;
        /* added by JADX */

        /* renamed from: mutiwindow_shortcutpage_width  reason: collision with other field name */
        public static final int f430mutiwindow_shortcutpage_width = 2131230987;
        /* added by JADX */

        /* renamed from: mutiwindow_shortcutpage_height  reason: collision with other field name */
        public static final int f431mutiwindow_shortcutpage_height = 2131230988;
        /* added by JADX */

        /* renamed from: multiwindow_list_max_height  reason: collision with other field name */
        public static final int f432multiwindow_list_max_height = 2131230989;
        /* added by JADX */

        /* renamed from: multi_window_title_margin  reason: collision with other field name */
        public static final int f433multi_window_title_margin = 2131230990;
        /* added by JADX */

        /* renamed from: multi_window_numsquare_margin  reason: collision with other field name */
        public static final int f434multi_window_numsquare_margin = 2131230991;
        /* added by JADX */

        /* renamed from: multiwindow_gallery_margin  reason: collision with other field name */
        public static final int f435multiwindow_gallery_margin = 2131230992;
        /* added by JADX */

        /* renamed from: multiwindow_item_close_area_size  reason: collision with other field name */
        public static final int f436multiwindow_item_close_area_size = 2131230993;
        /* added by JADX */

        /* renamed from: multiwindow_item_close_size  reason: collision with other field name */
        public static final int f437multiwindow_item_close_size = 2131230994;
        /* added by JADX */

        /* renamed from: multiwindow_item_height  reason: collision with other field name */
        public static final int f438multiwindow_item_height = 2131230995;
        /* added by JADX */

        /* renamed from: multiwindow_item_width  reason: collision with other field name */
        public static final int f439multiwindow_item_width = 2131230996;
        /* added by JADX */

        /* renamed from: multiwindow_shadow_size  reason: collision with other field name */
        public static final int f440multiwindow_shadow_size = 2131230997;
        /* added by JADX */

        /* renamed from: multiwindow_num_width  reason: collision with other field name */
        public static final int f441multiwindow_num_width = 2131230998;
        /* added by JADX */

        /* renamed from: multiwindow_num_height  reason: collision with other field name */
        public static final int f442multiwindow_num_height = 2131230999;
        /* added by JADX */

        /* renamed from: multiwindow_item_padding  reason: collision with other field name */
        public static final int f443multiwindow_item_padding = 2131231000;
        /* added by JADX */

        /* renamed from: add_sch_enginelist_width  reason: collision with other field name */
        public static final int f444add_sch_enginelist_width = 2131231001;
        /* added by JADX */

        /* renamed from: add_sch_engine_divider_width  reason: collision with other field name */
        public static final int f445add_sch_engine_divider_width = 2131231002;
        /* added by JADX */

        /* renamed from: add_sch_schengine_width  reason: collision with other field name */
        public static final int f446add_sch_schengine_width = 2131231003;
        /* added by JADX */

        /* renamed from: add_sch_enginelist_height  reason: collision with other field name */
        public static final int f447add_sch_enginelist_height = 2131231004;
        /* added by JADX */

        /* renamed from: add_sch_schengine_height  reason: collision with other field name */
        public static final int f448add_sch_schengine_height = 2131231005;
        /* added by JADX */

        /* renamed from: mutiwindow_height_winnum_2  reason: collision with other field name */
        public static final int f449mutiwindow_height_winnum_2 = 2131231006;
        /* added by JADX */

        /* renamed from: mutiwindow_height_winnum_4  reason: collision with other field name */
        public static final int f450mutiwindow_height_winnum_4 = 2131231007;
        /* added by JADX */

        /* renamed from: mutiwindow_item_width  reason: collision with other field name */
        public static final int f451mutiwindow_item_width = 2131231008;
        /* added by JADX */

        /* renamed from: mutiwindow_item_height  reason: collision with other field name */
        public static final int f452mutiwindow_item_height = 2131231009;
        /* added by JADX */

        /* renamed from: multiwindow_snapshot_width  reason: collision with other field name */
        public static final int f453multiwindow_snapshot_width = 2131231010;
        /* added by JADX */

        /* renamed from: multiwindow_snapshot_height  reason: collision with other field name */
        public static final int f454multiwindow_snapshot_height = 2131231011;
        /* added by JADX */

        /* renamed from: multiwindow_frame_width  reason: collision with other field name */
        public static final int f455multiwindow_frame_width = 2131231012;
        /* added by JADX */

        /* renamed from: multiwindow_frame_height  reason: collision with other field name */
        public static final int f456multiwindow_frame_height = 2131231013;
        /* added by JADX */

        /* renamed from: multiwindow_new_window_button_width  reason: collision with other field name */
        public static final int f457multiwindow_new_window_button_width = 2131231014;
        /* added by JADX */

        /* renamed from: multiwindow_new_window_button_height  reason: collision with other field name */
        public static final int f458multiwindow_new_window_button_height = 2131231015;
        /* added by JADX */

        /* renamed from: multiwindow_new_window_button_textsize  reason: collision with other field name */
        public static final int f459multiwindow_new_window_button_textsize = 2131231016;
        /* added by JADX */

        /* renamed from: multiwindow_new_window_button_area_height  reason: collision with other field name */
        public static final int f460multiwindow_new_window_button_area_height = 2131231017;
        /* added by JADX */

        /* renamed from: multiwindow_content_max_height  reason: collision with other field name */
        public static final int f461multiwindow_content_max_height = 2131231018;
        /* added by JADX */

        /* renamed from: page_seperator_height  reason: collision with other field name */
        public static final int f462page_seperator_height = 2131231019;
        /* added by JADX */

        /* renamed from: reading_mode_tail_height  reason: collision with other field name */
        public static final int f463reading_mode_tail_height = 2131231020;
        /* added by JADX */

        /* renamed from: reading_mode_entrance_width  reason: collision with other field name */
        public static final int f464reading_mode_entrance_width = 2131231021;
        /* added by JADX */

        /* renamed from: reading_mode_entrance_height  reason: collision with other field name */
        public static final int f465reading_mode_entrance_height = 2131231022;
        /* added by JADX */

        /* renamed from: reading_mode_entrance_padding_right  reason: collision with other field name */
        public static final int f466reading_mode_entrance_padding_right = 2131231023;
        /* added by JADX */

        /* renamed from: reading_mode_entrance_padding_top  reason: collision with other field name */
        public static final int f467reading_mode_entrance_padding_top = 2131231024;
        /* added by JADX */

        /* renamed from: reading_mode_tail_textsize  reason: collision with other field name */
        public static final int f468reading_mode_tail_textsize = 2131231025;
        /* added by JADX */

        /* renamed from: reading_mode_tail_button_size  reason: collision with other field name */
        public static final int f469reading_mode_tail_button_size = 2131231026;
        /* added by JADX */

        /* renamed from: face_border  reason: collision with other field name */
        public static final int f470face_border = 2131231027;
        /* added by JADX */

        /* renamed from: face_highlight_height  reason: collision with other field name */
        public static final int f471face_highlight_height = 2131231028;
        /* added by JADX */

        /* renamed from: face_frame_corner  reason: collision with other field name */
        public static final int f472face_frame_corner = 2131231029;
        /* added by JADX */

        /* renamed from: add_sch_face_size  reason: collision with other field name */
        public static final int f473add_sch_face_size = 2131231030;
        /* added by JADX */

        /* renamed from: add_sch_padding_left  reason: collision with other field name */
        public static final int f474add_sch_padding_left = 2131231031;
        /* added by JADX */

        /* renamed from: add_sch_face_fix  reason: collision with other field name */
        public static final int f475add_sch_face_fix = 2131231032;
        /* added by JADX */

        /* renamed from: add_sch_padding_top  reason: collision with other field name */
        public static final int f476add_sch_padding_top = 2131231033;
        /* added by JADX */

        /* renamed from: add_sch_button_width  reason: collision with other field name */
        public static final int f477add_sch_button_width = 2131231034;
        /* added by JADX */

        /* renamed from: add_sch_offset_left  reason: collision with other field name */
        public static final int f478add_sch_offset_left = 2131231035;
        /* added by JADX */

        /* renamed from: add_sch_offset_top  reason: collision with other field name */
        public static final int f479add_sch_offset_top = 2131231036;
        /* added by JADX */

        /* renamed from: add_sch_difference_left  reason: collision with other field name */
        public static final int f480add_sch_difference_left = 2131231037;
        /* added by JADX */

        /* renamed from: add_sch_difference_right  reason: collision with other field name */
        public static final int f481add_sch_difference_right = 2131231038;
        /* added by JADX */

        /* renamed from: add_sch_height  reason: collision with other field name */
        public static final int f482add_sch_height = 2131231039;
        /* added by JADX */

        /* renamed from: add_sch_height_input_state  reason: collision with other field name */
        public static final int f483add_sch_height_input_state = 2131231040;
        /* added by JADX */

        /* renamed from: add_sch_textsize  reason: collision with other field name */
        public static final int f484add_sch_textsize = 2131231041;
        /* added by JADX */

        /* renamed from: add_sch_text_paddingtop  reason: collision with other field name */
        public static final int f485add_sch_text_paddingtop = 2131231042;
        /* added by JADX */

        /* renamed from: add_sch_text_margin_left  reason: collision with other field name */
        public static final int f486add_sch_text_margin_left = 2131231043;
        /* added by JADX */

        /* renamed from: add_sch_text_margin_left2  reason: collision with other field name */
        public static final int f487add_sch_text_margin_left2 = 2131231044;
        /* added by JADX */

        /* renamed from: add_sch_mainpage_text_paddingtop  reason: collision with other field name */
        public static final int f488add_sch_mainpage_text_paddingtop = 2131231045;
        /* added by JADX */

        /* renamed from: add_sch_address_paddingtop  reason: collision with other field name */
        public static final int f489add_sch_address_paddingtop = 2131231046;
        /* added by JADX */

        /* renamed from: add_sch_address_paddingleft  reason: collision with other field name */
        public static final int f490add_sch_address_paddingleft = 2131231047;
        /* added by JADX */

        /* renamed from: add_sch_address_paddingright  reason: collision with other field name */
        public static final int f491add_sch_address_paddingright = 2131231048;
        /* added by JADX */

        /* renamed from: add_sch_address_size  reason: collision with other field name */
        public static final int f492add_sch_address_size = 2131231049;
        /* added by JADX */

        /* renamed from: add_sch_inputstate_width  reason: collision with other field name */
        public static final int f493add_sch_inputstate_width = 2131231050;
        /* added by JADX */

        /* renamed from: add_sch_cancle_width  reason: collision with other field name */
        public static final int f494add_sch_cancle_width = 2131231051;
        /* added by JADX */

        /* renamed from: add_sch_listitem_textsize  reason: collision with other field name */
        public static final int f495add_sch_listitem_textsize = 2131231052;
        /* added by JADX */

        /* renamed from: add_sch_listitem_height  reason: collision with other field name */
        public static final int f496add_sch_listitem_height = 2131231053;
        /* added by JADX */

        /* renamed from: add_sch_listitem_paddingleft  reason: collision with other field name */
        public static final int f497add_sch_listitem_paddingleft = 2131231054;
        /* added by JADX */

        /* renamed from: add_sch_listitem_paddingtop  reason: collision with other field name */
        public static final int f498add_sch_listitem_paddingtop = 2131231055;
        /* added by JADX */

        /* renamed from: add_sch_listitem_paddingbottom  reason: collision with other field name */
        public static final int f499add_sch_listitem_paddingbottom = 2131231056;
        /* added by JADX */

        /* renamed from: add_sch_edittext  reason: collision with other field name */
        public static final int f500add_sch_edittext = 2131231057;
        /* added by JADX */

        /* renamed from: add_sch_schengine_text  reason: collision with other field name */
        public static final int f501add_sch_schengine_text = 2131231058;
        /* added by JADX */

        /* renamed from: add_sch_schengine_img_paddingtop  reason: collision with other field name */
        public static final int f502add_sch_schengine_img_paddingtop = 2131231059;
        /* added by JADX */

        /* renamed from: add_sch_schengine_img_paddingbottom  reason: collision with other field name */
        public static final int f503add_sch_schengine_img_paddingbottom = 2131231060;
        /* added by JADX */

        /* renamed from: add_sch_schengine_padding  reason: collision with other field name */
        public static final int f504add_sch_schengine_padding = 2131231061;
        /* added by JADX */

        /* renamed from: add_sch_schengine_image_size  reason: collision with other field name */
        public static final int f505add_sch_schengine_image_size = 2131231062;
        /* added by JADX */

        /* renamed from: add_sch_engineicon_layout_paddingleft  reason: collision with other field name */
        public static final int f506add_sch_engineicon_layout_paddingleft = 2131231063;
        /* added by JADX */

        /* renamed from: add_sch_engineicon_size  reason: collision with other field name */
        public static final int f507add_sch_engineicon_size = 2131231064;
        /* added by JADX */

        /* renamed from: add_sch_engineicon_paddingtop  reason: collision with other field name */
        public static final int f508add_sch_engineicon_paddingtop = 2131231065;
        /* added by JADX */

        /* renamed from: add_sch_engine_dropdown_paddingleft  reason: collision with other field name */
        public static final int f509add_sch_engine_dropdown_paddingleft = 2131231066;
        /* added by JADX */

        /* renamed from: add_sch_inputlist_paddingtop  reason: collision with other field name */
        public static final int f510add_sch_inputlist_paddingtop = 2131231067;
        /* added by JADX */

        /* renamed from: add_sch_inputlist_paddingleft  reason: collision with other field name */
        public static final int f511add_sch_inputlist_paddingleft = 2131231068;
        /* added by JADX */

        /* renamed from: add_sch_inputlist_paddingright  reason: collision with other field name */
        public static final int f512add_sch_inputlist_paddingright = 2131231069;
        /* added by JADX */

        /* renamed from: add_sch_inputlist_paddingbottom  reason: collision with other field name */
        public static final int f513add_sch_inputlist_paddingbottom = 2131231070;
        /* added by JADX */

        /* renamed from: add_sch_enginelist_marginTop  reason: collision with other field name */
        public static final int f514add_sch_enginelist_marginTop = 2131231071;
        /* added by JADX */

        /* renamed from: add_sch_enginelist_marginLeft  reason: collision with other field name */
        public static final int f515add_sch_enginelist_marginLeft = 2131231072;
        /* added by JADX */

        /* renamed from: add_sch_enginelist_padding  reason: collision with other field name */
        public static final int f516add_sch_enginelist_padding = 2131231073;
        /* added by JADX */

        /* renamed from: add_sch_engine_text_marginTop  reason: collision with other field name */
        public static final int f517add_sch_engine_text_marginTop = 2131231074;
        /* added by JADX */

        /* renamed from: add_sch_engine_text_marginLeft  reason: collision with other field name */
        public static final int f518add_sch_engine_text_marginLeft = 2131231075;
        /* added by JADX */

        /* renamed from: add_sch_engine_text_height  reason: collision with other field name */
        public static final int f519add_sch_engine_text_height = 2131231076;
        /* added by JADX */

        /* renamed from: add_sch_engine_textsize  reason: collision with other field name */
        public static final int f520add_sch_engine_textsize = 2131231077;
        /* added by JADX */

        /* renamed from: add_sch_engine_divider_height  reason: collision with other field name */
        public static final int f521add_sch_engine_divider_height = 2131231078;
        /* added by JADX */

        /* renamed from: add_sch_engine_divider_marginleft  reason: collision with other field name */
        public static final int f522add_sch_engine_divider_marginleft = 2131231079;
        /* added by JADX */

        /* renamed from: add_sch_add_edittext_paddingleft  reason: collision with other field name */
        public static final int f523add_sch_add_edittext_paddingleft = 2131231080;
        /* added by JADX */

        /* renamed from: add_sch_add_edittext_paddingright  reason: collision with other field name */
        public static final int f524add_sch_add_edittext_paddingright = 2131231081;
        /* added by JADX */

        /* renamed from: add_sch_add_edittext_paddingbottom  reason: collision with other field name */
        public static final int f525add_sch_add_edittext_paddingbottom = 2131231082;
        /* added by JADX */

        /* renamed from: add_sch_sch_edittext_paddingleft  reason: collision with other field name */
        public static final int f526add_sch_sch_edittext_paddingleft = 2131231083;
        /* added by JADX */

        /* renamed from: add_sch_sch_edittext_paddingright  reason: collision with other field name */
        public static final int f527add_sch_sch_edittext_paddingright = 2131231084;
        /* added by JADX */

        /* renamed from: add_sch_sch_edittext_paddingbottom  reason: collision with other field name */
        public static final int f528add_sch_sch_edittext_paddingbottom = 2131231085;
        /* added by JADX */

        /* renamed from: add_sch_state_text  reason: collision with other field name */
        public static final int f529add_sch_state_text = 2131231086;
        /* added by JADX */

        /* renamed from: add_sch_state_text_paddingleft  reason: collision with other field name */
        public static final int f530add_sch_state_text_paddingleft = 2131231087;
        /* added by JADX */

        /* renamed from: add_sch_state_text_paddingtop  reason: collision with other field name */
        public static final int f531add_sch_state_text_paddingtop = 2131231088;
        /* added by JADX */

        /* renamed from: add_sch_bookmark_width  reason: collision with other field name */
        public static final int f532add_sch_bookmark_width = 2131231089;
        /* added by JADX */

        /* renamed from: add_sch_progress_paddingleft  reason: collision with other field name */
        public static final int f533add_sch_progress_paddingleft = 2131231090;
        /* added by JADX */

        /* renamed from: add_sch_urlitem_paddingtop  reason: collision with other field name */
        public static final int f534add_sch_urlitem_paddingtop = 2131231091;
        /* added by JADX */

        /* renamed from: webwidget_bold_stroke  reason: collision with other field name */
        public static final int f535webwidget_bold_stroke = 2131231092;
        /* added by JADX */

        /* renamed from: webwidget_thin_stroke  reason: collision with other field name */
        public static final int f536webwidget_thin_stroke = 2131231093;
        /* added by JADX */

        /* renamed from: webwidget_frame_corner  reason: collision with other field name */
        public static final int f537webwidget_frame_corner = 2131231094;
        /* added by JADX */

        /* renamed from: webwidget_button_corner  reason: collision with other field name */
        public static final int f538webwidget_button_corner = 2131231095;
        /* added by JADX */

        /* renamed from: webwidget_frame_shadow_large  reason: collision with other field name */
        public static final int f539webwidget_frame_shadow_large = 2131231096;
        /* added by JADX */

        /* renamed from: webwidget_frame_shadow_bold  reason: collision with other field name */
        public static final int f540webwidget_frame_shadow_bold = 2131231097;
        /* added by JADX */

        /* renamed from: webwidget_frame_shadow_thin  reason: collision with other field name */
        public static final int f541webwidget_frame_shadow_thin = 2131231098;
        /* added by JADX */

        /* renamed from: webwidget_checkbox_size  reason: collision with other field name */
        public static final int f542webwidget_checkbox_size = 2131231099;
    }

    public final class drawable {

        /* renamed from: iV */
        public static final int ucmascot = 2130837608;
        public static final int icon = 2130837539;

        /* renamed from: jA */
        public static final int fileicon_back = 2130837522;

        /* renamed from: jB */
        public static final int fileicon_default = 2130837523;

        /* renamed from: jC */
        public static final int fileicon_dir = 2130837524;

        /* renamed from: jD */
        public static final int fileicon_document = 2130837525;

        /* renamed from: jE */
        public static final int fileicon_exe = 2130837526;

        /* renamed from: jF */
        public static final int fileicon_img = 2130837527;

        /* renamed from: jG */
        public static final int fileicon_video = 2130837528;

        /* renamed from: jH */
        public static final int fileicon_webpage = 2130837529;

        /* renamed from: jI */
        public static final int fileicon_zip = 2130837530;

        /* renamed from: jJ */
        public static final int fixed_quick_access = 2130837531;

        /* renamed from: jK */
        public static final int folder_icon = 2130837532;

        /* renamed from: jL */
        public static final int freemenu_bottom_left = 2130837533;

        /* renamed from: jM */
        public static final int freemenu_bottom_middle = 2130837534;

        /* renamed from: jN */
        public static final int freemenu_bottom_right = 2130837535;

        /* renamed from: jO */
        public static final int freemenu_top_left = 2130837536;

        /* renamed from: jP */
        public static final int freemenu_top_middle = 2130837537;

        /* renamed from: jQ */
        public static final int freemenu_top_right = 2130837538;

        /* renamed from: jR */
        public static final int initdot1 = 2130837540;

        /* renamed from: jS */
        public static final int initdot2 = 2130837541;

        /* renamed from: jT */
        public static final int initdot3 = 2130837542;

        /* renamed from: jU */
        public static final int initdotshadow = 2130837543;

        /* renamed from: jV */
        public static final int juc_in_available = 2130837544;

        /* renamed from: jW */
        public static final int juc_in_available_night = 2130837545;

        /* renamed from: jX */
        public static final int juc_in_unavailable = 2130837546;

        /* renamed from: jY */
        public static final int juc_in_unavailable_night = 2130837547;

        /* renamed from: jZ */
        public static final int juc_out_available = 2130837548;

        /* renamed from: ji */
        public static final int aaicon = 2130837504;

        /* renamed from: jj */
        public static final int blank = 2130837505;

        /* renamed from: jk */
        public static final int bookmark_icon_link = 2130837506;

        /* renamed from: jl */
        public static final int camera = 2130837507;

        /* renamed from: jm */
        public static final int dark_shadow_left = 2130837508;

        /* renamed from: jn */
        public static final int dark_shadow_right = 2130837509;

        /* renamed from: jo */
        public static final int download_dialog_divider = 2130837510;

        /* renamed from: jp */
        public static final int download_dialog_icon = 2130837511;

        /* renamed from: jq */
        public static final int download_downloading = 2130837512;

        /* renamed from: jr */
        public static final int download_error = 2130837513;

        /* renamed from: js */
        public static final int download_fd_bg = 2130837514;

        /* renamed from: jt */
        public static final int download_paused = 2130837515;

        /* renamed from: ju */
        public static final int download_waiting = 2130837516;

        /* renamed from: jv */
        public static final int end = 2130837517;

        /* renamed from: jw */
        public static final int end_night = 2130837518;

        /* renamed from: jx */
        public static final int file_selected = 2130837519;

        /* renamed from: jy */
        public static final int file_selected_night = 2130837520;

        /* renamed from: jz */
        public static final int fileicon_audio = 2130837521;

        /* renamed from: kA */
        public static final int reading_refresh_night = 2130837575;

        /* renamed from: kB */
        public static final int reading_stop = 2130837576;

        /* renamed from: kC */
        public static final int reading_stop_night = 2130837577;

        /* renamed from: kD */
        public static final int readingmode_edu_arrow = 2130837578;

        /* renamed from: kE */
        public static final int readingmode_edu_pop = 2130837579;

        /* renamed from: kF */
        public static final int readingmode_edu_record = 2130837580;

        /* renamed from: kG */
        public static final int readingmode_edu_splash = 2130837581;

        /* renamed from: kH */
        public static final int readingmode_off = 2130837582;

        /* renamed from: kI */
        public static final int readingmode_off_night = 2130837583;

        /* renamed from: kJ */
        public static final int readingmode_on = 2130837584;

        /* renamed from: kK */
        public static final int readingmode_on_night = 2130837585;

        /* renamed from: kL */
        public static final int safe_malware = 2130837586;

        /* renamed from: kM */
        public static final int safe_no_info = 2130837587;

        /* renamed from: kN */
        public static final int safe_not_scan = 2130837588;

        /* renamed from: kO */
        public static final int safe_safe = 2130837589;

        /* renamed from: kP */
        public static final int safe_scanning = 2130837590;

        /* renamed from: kQ */
        public static final int safeicon = 2130837591;

        /* renamed from: kR */
        public static final int screen_lock = 2130837592;

        /* renamed from: kS */
        public static final int screen_unlock = 2130837593;

        /* renamed from: kT */
        public static final int searchengine_icon_default = 2130837594;

        /* renamed from: kU */
        public static final int seek_thumb = 2130837595;

        /* renamed from: kV */
        public static final int selected = 2130837596;

        /* renamed from: kW */
        public static final int separator = 2130837597;

        /* renamed from: kX */
        public static final int setting_dialog_arrow = 2130837598;

        /* renamed from: kY */
        public static final int setting_group_arrow = 2130837599;

        /* renamed from: kZ */
        public static final int skin_disable = 2130837600;

        /* renamed from: ka */
        public static final int juc_out_available_night = 2130837549;

        /* renamed from: kb */
        public static final int juc_out_unavailable = 2130837550;

        /* renamed from: kc */
        public static final int juc_out_unavailable_night = 2130837551;

        /* renamed from: kd */
        public static final int list_divider = 2130837552;

        /* renamed from: ke */
        public static final int loading_circle_day = 2130837553;

        /* renamed from: kf */
        public static final int loading_circle_night = 2130837554;

        /* renamed from: kg */
        public static final int navi_3guc = 2130837555;

        /* renamed from: kh */
        public static final int navi_bibei = 2130837556;

        /* renamed from: ki */
        public static final int navi_sina = 2130837557;

        /* renamed from: kj */
        public static final int navi_uzone = 2130837558;

        /* renamed from: kk */
        public static final int plugin_bg_day = 2130837559;

        /* renamed from: kl */
        public static final int plugin_bg_night = 2130837560;

        /* renamed from: km */
        public static final int plugin_button_day = 2130837561;

        /* renamed from: kn */
        public static final int plugin_button_night = 2130837562;

        /* renamed from: ko */
        public static final int plugin_fail_day = 2130837563;

        /* renamed from: kp */
        public static final int plugin_fail_night = 2130837564;

        /* renamed from: kq */
        public static final int plugin_notice = 2130837565;

        /* renamed from: kr */
        public static final int plugin_notice_day = 2130837566;

        /* renamed from: ks */
        public static final int plugin_notice_night = 2130837567;

        /* renamed from: kt */
        public static final int plugin_progress_day = 2130837568;

        /* renamed from: ku */
        public static final int plugin_progress_night = 2130837569;

        /* renamed from: kv */
        public static final int reading_off = 2130837570;

        /* renamed from: kw */
        public static final int reading_off_night = 2130837571;

        /* renamed from: kx */
        public static final int reading_on = 2130837572;

        /* renamed from: ky */
        public static final int reading_on_night = 2130837573;

        /* renamed from: kz */
        public static final int reading_refresh = 2130837574;

        /* renamed from: la */
        public static final int space = 2130837601;

        /* renamed from: lb */
        public static final int start = 2130837602;

        /* renamed from: lc */
        public static final int start_night = 2130837603;

        /* renamed from: ld */
        public static final int tips_fit = 2130837604;

        /* renamed from: le */
        public static final int tips_zoom = 2130837605;

        /* renamed from: lf */
        public static final int top_title_shadow = 2130837606;

        /* renamed from: lg */
        public static final int ucbackground = 2130837607;

        /* renamed from: lh */
        public static final int uploadfont_progress = 2130837609;

        /* renamed from: li */
        public static final int uploadfont_progress_background = 2130837610;

        /* renamed from: lj */
        public static final int warning = 2130837611;

        /* renamed from: lk */
        public static final int webview_safe_popup = 2130837612;
        /* added by JADX */

        /* renamed from: aaicon  reason: collision with other field name */
        public static final int f543aaicon = 2130837504;
        /* added by JADX */

        /* renamed from: blank  reason: collision with other field name */
        public static final int f544blank = 2130837505;
        /* added by JADX */

        /* renamed from: bookmark_icon_link  reason: collision with other field name */
        public static final int f545bookmark_icon_link = 2130837506;
        /* added by JADX */

        /* renamed from: camera  reason: collision with other field name */
        public static final int f546camera = 2130837507;
        /* added by JADX */

        /* renamed from: dark_shadow_left  reason: collision with other field name */
        public static final int f547dark_shadow_left = 2130837508;
        /* added by JADX */

        /* renamed from: dark_shadow_right  reason: collision with other field name */
        public static final int f548dark_shadow_right = 2130837509;
        /* added by JADX */

        /* renamed from: download_dialog_divider  reason: collision with other field name */
        public static final int f549download_dialog_divider = 2130837510;
        /* added by JADX */

        /* renamed from: download_dialog_icon  reason: collision with other field name */
        public static final int f550download_dialog_icon = 2130837511;
        /* added by JADX */

        /* renamed from: download_downloading  reason: collision with other field name */
        public static final int f551download_downloading = 2130837512;
        /* added by JADX */

        /* renamed from: download_error  reason: collision with other field name */
        public static final int f552download_error = 2130837513;
        /* added by JADX */

        /* renamed from: download_fd_bg  reason: collision with other field name */
        public static final int f553download_fd_bg = 2130837514;
        /* added by JADX */

        /* renamed from: download_paused  reason: collision with other field name */
        public static final int f554download_paused = 2130837515;
        /* added by JADX */

        /* renamed from: download_waiting  reason: collision with other field name */
        public static final int f555download_waiting = 2130837516;
        /* added by JADX */

        /* renamed from: end  reason: collision with other field name */
        public static final int f556end = 2130837517;
        /* added by JADX */

        /* renamed from: end_night  reason: collision with other field name */
        public static final int f557end_night = 2130837518;
        /* added by JADX */

        /* renamed from: file_selected  reason: collision with other field name */
        public static final int f558file_selected = 2130837519;
        /* added by JADX */

        /* renamed from: file_selected_night  reason: collision with other field name */
        public static final int f559file_selected_night = 2130837520;
        /* added by JADX */

        /* renamed from: fileicon_audio  reason: collision with other field name */
        public static final int f560fileicon_audio = 2130837521;
        /* added by JADX */

        /* renamed from: fileicon_back  reason: collision with other field name */
        public static final int f561fileicon_back = 2130837522;
        /* added by JADX */

        /* renamed from: fileicon_default  reason: collision with other field name */
        public static final int f562fileicon_default = 2130837523;
        /* added by JADX */

        /* renamed from: fileicon_dir  reason: collision with other field name */
        public static final int f563fileicon_dir = 2130837524;
        /* added by JADX */

        /* renamed from: fileicon_document  reason: collision with other field name */
        public static final int f564fileicon_document = 2130837525;
        /* added by JADX */

        /* renamed from: fileicon_exe  reason: collision with other field name */
        public static final int f565fileicon_exe = 2130837526;
        /* added by JADX */

        /* renamed from: fileicon_img  reason: collision with other field name */
        public static final int f566fileicon_img = 2130837527;
        /* added by JADX */

        /* renamed from: fileicon_video  reason: collision with other field name */
        public static final int f567fileicon_video = 2130837528;
        /* added by JADX */

        /* renamed from: fileicon_webpage  reason: collision with other field name */
        public static final int f568fileicon_webpage = 2130837529;
        /* added by JADX */

        /* renamed from: fileicon_zip  reason: collision with other field name */
        public static final int f569fileicon_zip = 2130837530;
        /* added by JADX */

        /* renamed from: fixed_quick_access  reason: collision with other field name */
        public static final int f570fixed_quick_access = 2130837531;
        /* added by JADX */

        /* renamed from: folder_icon  reason: collision with other field name */
        public static final int f571folder_icon = 2130837532;
        /* added by JADX */

        /* renamed from: freemenu_bottom_left  reason: collision with other field name */
        public static final int f572freemenu_bottom_left = 2130837533;
        /* added by JADX */

        /* renamed from: freemenu_bottom_middle  reason: collision with other field name */
        public static final int f573freemenu_bottom_middle = 2130837534;
        /* added by JADX */

        /* renamed from: freemenu_bottom_right  reason: collision with other field name */
        public static final int f574freemenu_bottom_right = 2130837535;
        /* added by JADX */

        /* renamed from: freemenu_top_left  reason: collision with other field name */
        public static final int f575freemenu_top_left = 2130837536;
        /* added by JADX */

        /* renamed from: freemenu_top_middle  reason: collision with other field name */
        public static final int f576freemenu_top_middle = 2130837537;
        /* added by JADX */

        /* renamed from: freemenu_top_right  reason: collision with other field name */
        public static final int f577freemenu_top_right = 2130837538;
        /* added by JADX */

        /* renamed from: initdot1  reason: collision with other field name */
        public static final int f578initdot1 = 2130837540;
        /* added by JADX */

        /* renamed from: initdot2  reason: collision with other field name */
        public static final int f579initdot2 = 2130837541;
        /* added by JADX */

        /* renamed from: initdot3  reason: collision with other field name */
        public static final int f580initdot3 = 2130837542;
        /* added by JADX */

        /* renamed from: initdotshadow  reason: collision with other field name */
        public static final int f581initdotshadow = 2130837543;
        /* added by JADX */

        /* renamed from: juc_in_available  reason: collision with other field name */
        public static final int f582juc_in_available = 2130837544;
        /* added by JADX */

        /* renamed from: juc_in_available_night  reason: collision with other field name */
        public static final int f583juc_in_available_night = 2130837545;
        /* added by JADX */

        /* renamed from: juc_in_unavailable  reason: collision with other field name */
        public static final int f584juc_in_unavailable = 2130837546;
        /* added by JADX */

        /* renamed from: juc_in_unavailable_night  reason: collision with other field name */
        public static final int f585juc_in_unavailable_night = 2130837547;
        /* added by JADX */

        /* renamed from: juc_out_available  reason: collision with other field name */
        public static final int f586juc_out_available = 2130837548;
        /* added by JADX */

        /* renamed from: juc_out_available_night  reason: collision with other field name */
        public static final int f587juc_out_available_night = 2130837549;
        /* added by JADX */

        /* renamed from: juc_out_unavailable  reason: collision with other field name */
        public static final int f588juc_out_unavailable = 2130837550;
        /* added by JADX */

        /* renamed from: juc_out_unavailable_night  reason: collision with other field name */
        public static final int f589juc_out_unavailable_night = 2130837551;
        /* added by JADX */

        /* renamed from: list_divider  reason: collision with other field name */
        public static final int f590list_divider = 2130837552;
        /* added by JADX */

        /* renamed from: loading_circle_day  reason: collision with other field name */
        public static final int f591loading_circle_day = 2130837553;
        /* added by JADX */

        /* renamed from: loading_circle_night  reason: collision with other field name */
        public static final int f592loading_circle_night = 2130837554;
        /* added by JADX */

        /* renamed from: navi_3guc  reason: collision with other field name */
        public static final int f593navi_3guc = 2130837555;
        /* added by JADX */

        /* renamed from: navi_bibei  reason: collision with other field name */
        public static final int f594navi_bibei = 2130837556;
        /* added by JADX */

        /* renamed from: navi_sina  reason: collision with other field name */
        public static final int f595navi_sina = 2130837557;
        /* added by JADX */

        /* renamed from: navi_uzone  reason: collision with other field name */
        public static final int f596navi_uzone = 2130837558;
        /* added by JADX */

        /* renamed from: plugin_bg_day  reason: collision with other field name */
        public static final int f597plugin_bg_day = 2130837559;
        /* added by JADX */

        /* renamed from: plugin_bg_night  reason: collision with other field name */
        public static final int f598plugin_bg_night = 2130837560;
        /* added by JADX */

        /* renamed from: plugin_button_day  reason: collision with other field name */
        public static final int f599plugin_button_day = 2130837561;
        /* added by JADX */

        /* renamed from: plugin_button_night  reason: collision with other field name */
        public static final int f600plugin_button_night = 2130837562;
        /* added by JADX */

        /* renamed from: plugin_fail_day  reason: collision with other field name */
        public static final int f601plugin_fail_day = 2130837563;
        /* added by JADX */

        /* renamed from: plugin_fail_night  reason: collision with other field name */
        public static final int f602plugin_fail_night = 2130837564;
        /* added by JADX */

        /* renamed from: plugin_notice  reason: collision with other field name */
        public static final int f603plugin_notice = 2130837565;
        /* added by JADX */

        /* renamed from: plugin_notice_day  reason: collision with other field name */
        public static final int f604plugin_notice_day = 2130837566;
        /* added by JADX */

        /* renamed from: plugin_notice_night  reason: collision with other field name */
        public static final int f605plugin_notice_night = 2130837567;
        /* added by JADX */

        /* renamed from: plugin_progress_day  reason: collision with other field name */
        public static final int f606plugin_progress_day = 2130837568;
        /* added by JADX */

        /* renamed from: plugin_progress_night  reason: collision with other field name */
        public static final int f607plugin_progress_night = 2130837569;
        /* added by JADX */

        /* renamed from: reading_off  reason: collision with other field name */
        public static final int f608reading_off = 2130837570;
        /* added by JADX */

        /* renamed from: reading_off_night  reason: collision with other field name */
        public static final int f609reading_off_night = 2130837571;
        /* added by JADX */

        /* renamed from: reading_on  reason: collision with other field name */
        public static final int f610reading_on = 2130837572;
        /* added by JADX */

        /* renamed from: reading_on_night  reason: collision with other field name */
        public static final int f611reading_on_night = 2130837573;
        /* added by JADX */

        /* renamed from: reading_refresh  reason: collision with other field name */
        public static final int f612reading_refresh = 2130837574;
        /* added by JADX */

        /* renamed from: reading_refresh_night  reason: collision with other field name */
        public static final int f613reading_refresh_night = 2130837575;
        /* added by JADX */

        /* renamed from: reading_stop  reason: collision with other field name */
        public static final int f614reading_stop = 2130837576;
        /* added by JADX */

        /* renamed from: reading_stop_night  reason: collision with other field name */
        public static final int f615reading_stop_night = 2130837577;
        /* added by JADX */

        /* renamed from: readingmode_edu_arrow  reason: collision with other field name */
        public static final int f616readingmode_edu_arrow = 2130837578;
        /* added by JADX */

        /* renamed from: readingmode_edu_pop  reason: collision with other field name */
        public static final int f617readingmode_edu_pop = 2130837579;
        /* added by JADX */

        /* renamed from: readingmode_edu_record  reason: collision with other field name */
        public static final int f618readingmode_edu_record = 2130837580;
        /* added by JADX */

        /* renamed from: readingmode_edu_splash  reason: collision with other field name */
        public static final int f619readingmode_edu_splash = 2130837581;
        /* added by JADX */

        /* renamed from: readingmode_off  reason: collision with other field name */
        public static final int f620readingmode_off = 2130837582;
        /* added by JADX */

        /* renamed from: readingmode_off_night  reason: collision with other field name */
        public static final int f621readingmode_off_night = 2130837583;
        /* added by JADX */

        /* renamed from: readingmode_on  reason: collision with other field name */
        public static final int f622readingmode_on = 2130837584;
        /* added by JADX */

        /* renamed from: readingmode_on_night  reason: collision with other field name */
        public static final int f623readingmode_on_night = 2130837585;
        /* added by JADX */

        /* renamed from: safe_malware  reason: collision with other field name */
        public static final int f624safe_malware = 2130837586;
        /* added by JADX */

        /* renamed from: safe_no_info  reason: collision with other field name */
        public static final int f625safe_no_info = 2130837587;
        /* added by JADX */

        /* renamed from: safe_not_scan  reason: collision with other field name */
        public static final int f626safe_not_scan = 2130837588;
        /* added by JADX */

        /* renamed from: safe_safe  reason: collision with other field name */
        public static final int f627safe_safe = 2130837589;
        /* added by JADX */

        /* renamed from: safe_scanning  reason: collision with other field name */
        public static final int f628safe_scanning = 2130837590;
        /* added by JADX */

        /* renamed from: safeicon  reason: collision with other field name */
        public static final int f629safeicon = 2130837591;
        /* added by JADX */

        /* renamed from: screen_lock  reason: collision with other field name */
        public static final int f630screen_lock = 2130837592;
        /* added by JADX */

        /* renamed from: screen_unlock  reason: collision with other field name */
        public static final int f631screen_unlock = 2130837593;
        /* added by JADX */

        /* renamed from: searchengine_icon_default  reason: collision with other field name */
        public static final int f632searchengine_icon_default = 2130837594;
        /* added by JADX */

        /* renamed from: seek_thumb  reason: collision with other field name */
        public static final int f633seek_thumb = 2130837595;
        /* added by JADX */

        /* renamed from: selected  reason: collision with other field name */
        public static final int f634selected = 2130837596;
        /* added by JADX */

        /* renamed from: separator  reason: collision with other field name */
        public static final int f635separator = 2130837597;
        /* added by JADX */

        /* renamed from: setting_dialog_arrow  reason: collision with other field name */
        public static final int f636setting_dialog_arrow = 2130837598;
        /* added by JADX */

        /* renamed from: setting_group_arrow  reason: collision with other field name */
        public static final int f637setting_group_arrow = 2130837599;
        /* added by JADX */

        /* renamed from: skin_disable  reason: collision with other field name */
        public static final int f638skin_disable = 2130837600;
        /* added by JADX */

        /* renamed from: space  reason: collision with other field name */
        public static final int f639space = 2130837601;
        /* added by JADX */

        /* renamed from: start  reason: collision with other field name */
        public static final int f640start = 2130837602;
        /* added by JADX */

        /* renamed from: start_night  reason: collision with other field name */
        public static final int f641start_night = 2130837603;
        /* added by JADX */

        /* renamed from: tips_fit  reason: collision with other field name */
        public static final int f642tips_fit = 2130837604;
        /* added by JADX */

        /* renamed from: tips_zoom  reason: collision with other field name */
        public static final int f643tips_zoom = 2130837605;
        /* added by JADX */

        /* renamed from: top_title_shadow  reason: collision with other field name */
        public static final int f644top_title_shadow = 2130837606;
        /* added by JADX */

        /* renamed from: ucbackground  reason: collision with other field name */
        public static final int f645ucbackground = 2130837607;
        /* added by JADX */

        /* renamed from: ucmascot  reason: collision with other field name */
        public static final int f646ucmascot = 2130837608;
        /* added by JADX */

        /* renamed from: uploadfont_progress  reason: collision with other field name */
        public static final int f647uploadfont_progress = 2130837609;
        /* added by JADX */

        /* renamed from: uploadfont_progress_background  reason: collision with other field name */
        public static final int f648uploadfont_progress_background = 2130837610;
        /* added by JADX */

        /* renamed from: warning  reason: collision with other field name */
        public static final int f649warning = 2130837611;
        /* added by JADX */

        /* renamed from: webview_safe_popup  reason: collision with other field name */
        public static final int f650webview_safe_popup = 2130837612;
    }

    public final class id {
        public static final int button = 2131099837;
        public static final int content = 2131099790;
        public static final int divider = 2131099687;

        /* renamed from: eS */
        public static final int Browser_Controlbar = 2131099676;

        /* renamed from: eT */
        public static final int Browser_MainPage = 2131099702;

        /* renamed from: eU */
        public static final int Browser_TimeBar = 2131099696;

        /* renamed from: eV */
        public static final int Browser_TitleBar = 2131099652;

        /* renamed from: eW */
        public static final int Select = 2131099869;

        /* renamed from: eX */
        public static final int add_bookmark_to = 2131099664;

        /* renamed from: eY */
        public static final int add_bookmark_to_bookmark = 2131099668;

        /* renamed from: eZ */
        public static final int add_bookmark_to_bookmark_img = 2131099669;

        /* renamed from: fA */
        public static final int browser_page_attrs_image_url = 2131099715;

        /* renamed from: fB */
        public static final int browser_page_attrs_title = 2131099708;

        /* renamed from: fC */
        public static final int browser_page_attrs_url = 2131099712;

        /* renamed from: fD */
        public static final int browser_readmode_entrance = 2131099699;

        /* renamed from: fE */
        public static final int browser_safemark = 2131099697;

        /* renamed from: fF */
        public static final int browser_upmsg = 2131099722;

        /* renamed from: fG */
        public static final int btnCancel = 2131099661;

        /* renamed from: fH */
        public static final int btnConfirm = 2131099660;

        /* renamed from: fI */
        public static final int btnReshut = 2131099659;

        /* renamed from: fJ */
        public static final int btnShot = 2131099782;

        /* renamed from: fK */
        public static final int btnShut = 2131099658;

        /* renamed from: fL */
        public static final int button_area = 2131099791;

        /* renamed from: fM */
        public static final int cancel_bar = 2131099804;

        /* renamed from: fN */
        public static final int cancel_bookmark_list = 2131099805;

        /* renamed from: fO */
        public static final int cancle = 2131099689;

        /* renamed from: fP */
        public static final int clear_all = 2131099834;

        /* renamed from: fQ */
        public static final int close_refreshtimer = 2131099723;

        /* renamed from: fR */
        public static final int completeTimeText = 2131099736;

        /* renamed from: fS */
        public static final int container = 2131099806;

        /* renamed from: fT */
        public static final int controlbar = 2131099653;

        /* renamed from: fU */
        public static final int controlbar_button_back = 2131099824;

        /* renamed from: fV */
        public static final int controlbar_fs_main = 2131099706;

        /* renamed from: fW */
        public static final int controlbar_fs_main_default = 2131099703;

        /* renamed from: fX */
        public static final int controlbar_fs_popup = 2131099704;

        /* renamed from: fY */
        public static final int controlbar_fs_stop = 2131099705;

        /* renamed from: fZ */
        public static final int controlbar_head = 2131099826;

        /* renamed from: fa */
        public static final int add_bookmark_to_bookmark_text = 2131099670;

        /* renamed from: fb */
        public static final int add_bookmark_to_divider = 2131099666;

        /* renamed from: fc */
        public static final int add_bookmark_to_list = 2131099667;

        /* renamed from: fd */
        public static final int add_bookmark_to_navi = 2131099671;

        /* renamed from: fe */
        public static final int add_bookmark_to_navi_img = 2131099672;

        /* renamed from: ff */
        public static final int add_bookmark_to_navi_text = 2131099673;

        /* renamed from: fg */
        public static final int add_bookmark_to_text = 2131099665;

        /* renamed from: fh */
        public static final int address_bar_editText = 2131099877;

        /* renamed from: fi */
        public static final int address_bar_input = 2131099875;

        /* renamed from: fj */
        public static final int address_bar_textView = 2131099873;

        /* renamed from: fk */
        public static final int address_input_list = 2131099874;

        /* renamed from: fl */
        public static final int address_input_state = 2131099876;

        /* renamed from: fm */
        public static final int arrow = 2131099838;

        /* renamed from: fn */
        public static final int bookmark_container = 2131099802;

        /* renamed from: fo */
        public static final int bookmark_dlg_exportname = 2131099684;

        /* renamed from: fp */
        public static final int bookmark_dlg_filename = 2131099678;

        /* renamed from: fq */
        public static final int bookmark_dlg_filepath = 2131099680;

        /* renamed from: fr */
        public static final int bookmark_dlg_folder = 2131099682;

        /* renamed from: fs */
        public static final int bookmark_dlg_foldername = 2131099691;

        /* renamed from: ft */
        public static final int bookmark_dlg_name = 2131099690;

        /* renamed from: fu */
        public static final int bookmark_list = 2131099803;

        /* renamed from: fv */
        public static final int bookmark_star = 2131099761;

        /* renamed from: fw */
        public static final int bookmarkaddress = 2131099686;

        /* renamed from: fx */
        public static final int bookmarkname = 2131099685;

        /* renamed from: fy */
        public static final int breakfast = 2131099740;

        /* renamed from: fz */
        public static final int browser_page_attrs_address = 2131099710;

        /* renamed from: gA */
        public static final int filesize = 2131099754;

        /* renamed from: gB */
        public static final int flash = 2131099760;

        /* renamed from: gC */
        public static final int frame = 2131099789;

        /* renamed from: gD */
        public static final int fresh_back = 2131099776;

        /* renamed from: gE */
        public static final int fresh_cancel = 2131099774;

        /* renamed from: gF */
        public static final int fullscreen_progress = 2131099700;

        /* renamed from: gG */
        public static final int gone = 2131099650;

        /* renamed from: gH */
        public static final int goto_mainpage = 2131099775;

        /* renamed from: gI */
        public static final int history_item = 2131099762;

        /* renamed from: gJ */
        public static final int image_progress = 2131099694;

        /* renamed from: gK */
        public static final int imgCaptured = 2131099657;

        /* renamed from: gL */
        public static final int img_fit = 2131099718;

        /* renamed from: gM */
        public static final int img_zoom = 2131099717;

        /* renamed from: gN */
        public static final int initial_dotset = 2131099765;

        /* renamed from: gO */
        public static final int initial_loading_1 = 2131099766;

        /* renamed from: gP */
        public static final int initial_loading_2 = 2131099767;

        /* renamed from: gQ */
        public static final int initial_loading_3 = 2131099768;

        /* renamed from: gR */
        public static final int initial_loading_4 = 2131099769;

        /* renamed from: gS */
        public static final int initial_loading_5 = 2131099770;

        /* renamed from: gT */
        public static final int initial_loading_6 = 2131099771;

        /* renamed from: gU */
        public static final int initial_loading_text = 2131099772;

        /* renamed from: gV */
        public static final int initial_main = 2131099763;

        /* renamed from: gW */
        public static final int initial_website = 2131099773;

        /* renamed from: gX */
        public static final int input_url_bar = 2131099786;

        /* renamed from: gY */
        public static final int inputurl_layout = 2131099707;

        /* renamed from: gZ */
        public static final int intertimer = 2131099844;

        /* renamed from: ga */
        public static final int controlbar_image_back = 2131099825;

        /* renamed from: gb */
        public static final int controlbar_layout = 2131099701;

        /* renamed from: gc */
        public static final int debug_submit = 2131099730;

        /* renamed from: gd */
        public static final int deldownload = 2131099739;

        /* renamed from: ge */
        public static final int dlg_cancel = 2131099744;

        /* renamed from: gf */
        public static final int dlg_ok = 2131099743;

        /* renamed from: gg */
        public static final int dont_tips = 2131099835;

        /* renamed from: gh */
        public static final int downloadIcon = 2131099732;

        /* renamed from: gi */
        public static final int downloadProgressBar = 2131099735;

        /* renamed from: gj */
        public static final int downloadProgressText = 2131099734;

        /* renamed from: gk */
        public static final int downloadTaskName = 2131099733;

        /* renamed from: gl */
        public static final int download_content = 2131099737;

        /* renamed from: gm */
        public static final int download_dlg_filename = 2131099742;

        /* renamed from: gn */
        public static final int dpc_add = 2131099726;

        /* renamed from: go */
        public static final int engine_list_cover = 2131099864;

        /* renamed from: gp */
        public static final int enginelist_icon = 2131099853;

        /* renamed from: gq */
        public static final int enginelist_title = 2131099854;

        /* renamed from: gr */
        public static final int entrance_container = 2131099698;

        /* renamed from: gs */
        public static final int file_list = 2131099749;

        /* renamed from: gt */
        public static final int file_name = 2131099683;

        /* renamed from: gu */
        public static final int fileattr_filepath = 2131099751;

        /* renamed from: gv */
        public static final int fileattr_filesize = 2131099755;

        /* renamed from: gw */
        public static final int fileattr_lastmodify = 2131099753;

        /* renamed from: gx */
        public static final int filemgr_dlg_foldername = 2131099759;

        /* renamed from: gy */
        public static final int filename = 2131099741;

        /* renamed from: gz */
        public static final int filepath = 2131099750;

        /* renamed from: hA */
        public static final int open_bookmark_history = 2131099799;

        /* renamed from: hB */
        public static final int other_tv = 2131099663;

        /* renamed from: hC */
        public static final int page_down = 2131099808;

        /* renamed from: hD */
        public static final int page_up = 2131099807;

        /* renamed from: hE */
        public static final int pageattr_button_copy_address = 2131099709;

        /* renamed from: hF */
        public static final int pageattr_button_copy_image_url = 2131099714;

        /* renamed from: hG */
        public static final int pageattr_button_copy_url = 2131099711;

        /* renamed from: hH */
        public static final int pageattr_image_url = 2131099713;

        /* renamed from: hI */
        public static final int plugin_btn = 2131099812;

        /* renamed from: hJ */
        public static final int plugin_downloadfail_day = 2131099809;

        /* renamed from: hK */
        public static final int plugin_downloadfail_night = 2131099813;

        /* renamed from: hL */
        public static final int plugin_downloading_day = 2131099814;

        /* renamed from: hM */
        public static final int plugin_downloading_night = 2131099816;

        /* renamed from: hN */
        public static final int plugin_iv = 2131099810;

        /* renamed from: hO */
        public static final int plugin_needdownload_day = 2131099817;

        /* renamed from: hP */
        public static final int plugin_needdownload_night = 2131099818;

        /* renamed from: hQ */
        public static final int plugin_progressbar = 2131099815;

        /* renamed from: hR */
        public static final int plugin_tv = 2131099811;

        /* renamed from: hS */
        public static final int prefix_title = 2131099794;

        /* renamed from: hT */
        public static final int prefix_url_add = 2131099796;

        /* renamed from: hU */
        public static final int prifix_add = 2131099679;

        /* renamed from: hV */
        public static final int prifix_name = 2131099677;

        /* renamed from: hW */
        public static final int puller = 2131099798;

        /* renamed from: hX */
        public static final int record_discribe = 2131099843;

        /* renamed from: hY */
        public static final int record_title = 2131099842;

        /* renamed from: hZ */
        public static final int save_image_cancel = 2131099848;

        /* renamed from: ha */
        public static final int invisible = 2131099649;

        /* renamed from: hb */
        public static final int item_discript = 2131099758;

        /* renamed from: hc */
        public static final int item_icon = 2131099756;

        /* renamed from: hd */
        public static final int item_image = 2131099871;

        /* renamed from: he */
        public static final int item_title = 2131099757;

        /* renamed from: hf */
        public static final int itemtext = 2131099781;

        /* renamed from: hg */
        public static final int lastmodify = 2131099752;

        /* renamed from: hh */
        public static final int layoutDisplay = 2131099655;

        /* renamed from: hi */
        public static final int lunch = 2131099738;

        /* renamed from: hj */
        public static final int mainpage_bookmark = 2131099778;

        /* renamed from: hk */
        public static final int mainpage_buz = 2131099785;

        /* renamed from: hl */
        public static final int mainpage_channel = 2131099780;

        /* renamed from: hm */
        public static final int mainpage_navi = 2131099779;

        /* renamed from: hn */
        public static final int menu_layout = 2131099788;

        /* renamed from: ho */
        public static final int msg = 2131099829;

        /* renamed from: hp */
        public static final int msg1 = 2131099831;

        /* renamed from: hq */
        public static final int msg2 = 2131099832;

        /* renamed from: hr */
        public static final int msg3 = 2131099833;

        /* renamed from: hs */
        public static final int multiwindowlist = 2131099793;

        /* renamed from: ht */
        public static final int mynavi_cancel = 2131099801;

        /* renamed from: hu */
        public static final int mynavi_confirm = 2131099800;

        /* renamed from: hv */
        public static final int mynavi_title = 2131099795;

        /* renamed from: hw */
        public static final int mynavi_url = 2131099797;

        /* renamed from: hx */
        public static final int navi = 2131099787;

        /* renamed from: hy */
        public static final int new_window_button = 2131099792;

        /* renamed from: hz */
        public static final int no_sd_card_warning = 2131099748;

        /* renamed from: iA */
        public static final int seekbar = 2131099823;

        /* renamed from: iB */
        public static final int selector_list = 2131099654;

        /* renamed from: iC */
        public static final int seprator = 2131099839;

        /* renamed from: iD */
        public static final int sfPreview = 2131099656;

        /* renamed from: iE */
        public static final int smooth_discribe = 2131099841;

        /* renamed from: iF */
        public static final int smooth_title = 2131099840;

        /* renamed from: iG */
        public static final int submit = 2131099688;

        /* renamed from: iH */
        public static final int subtitle = 2131099675;

        /* renamed from: iI */
        public static final int t_msg = 2131099681;

        /* renamed from: iJ */
        public static final int tab_container = 2131099784;

        /* renamed from: iK */
        public static final int test_ip = 2131099724;

        /* renamed from: iL */
        public static final int tips_close = 2131099720;

        /* renamed from: iM */
        public static final int tips_over = 2131099721;

        /* renamed from: iN */
        public static final int tips_zoom_img = 2131099716;

        /* renamed from: iO */
        public static final int tips_zoom_msg = 2131099719;

        /* renamed from: iP */
        public static final int title_progress = 2131099695;

        /* renamed from: iQ */
        public static final int titlebar_layout = 2131099693;

        /* renamed from: iR */
        public static final int uc_pref_clear_cache = 2131099819;

        /* renamed from: iS */
        public static final int uc_pref_clear_cookie = 2131099820;

        /* renamed from: iT */
        public static final int uc_pref_clear_form = 2131099821;

        /* renamed from: iU */
        public static final int uc_pref_clear_history = 2131099822;

        /* renamed from: iV */
        public static final int ucmascot = 2131099764;

        /* renamed from: iW */
        public static final int ucsave = 2131099830;

        /* renamed from: iX */
        public static final int upl_add0 = 2131099727;

        /* renamed from: iY */
        public static final int upl_add1 = 2131099728;

        /* renamed from: iZ */
        public static final int upload_msg = 2131099870;

        /* renamed from: ia */
        public static final int save_image_name = 2131099845;

        /* renamed from: ib */
        public static final int save_image_ok = 2131099847;

        /* renamed from: ic */
        public static final int save_source_cancel = 2131099852;
        public static final int icon = 2131099674;

        /* renamed from: ie */
        public static final int save_source_name = 2131099849;

        /* renamed from: if */
        public static final int save_source_ok = 2131099851;

        /* renamed from: ig */
        public static final int saveimage_dlg_filename = 2131099846;

        /* renamed from: ih */
        public static final int savesource_dlg_filename = 2131099850;

        /* renamed from: ii */
        public static final int scrollTimer = 2131099729;

        /* renamed from: ij */
        public static final int searchRelativeLayout = 2131099855;

        /* renamed from: ik */
        public static final int search_bar_editText = 2131099858;

        /* renamed from: il */
        public static final int search_bar_input = 2131099859;

        /* renamed from: im */
        public static final int search_bar_textView = 2131099856;

        /* renamed from: io */
        public static final int search_engine_divider = 2131099867;

        /* renamed from: ip */
        public static final int search_engine_downicon = 2131099863;

        /* renamed from: iq */
        public static final int search_engine_icon = 2131099862;

        /* renamed from: ir */
        public static final int search_engine_icon_layout = 2131099861;

        /* renamed from: is */
        public static final int search_engine_list = 2131099865;

        /* renamed from: it */
        public static final int search_engine_list_icon = 2131099868;

        /* renamed from: iu */
        public static final int search_engine_text = 2131099866;

        /* renamed from: iv */
        public static final int search_input_cancle = 2131099860;

        /* renamed from: iw */
        public static final int search_input_list = 2131099857;

        /* renamed from: ix */
        public static final int searchengineimage = 2131099745;

        /* renamed from: iy */
        public static final int searchengineimg = 2131099747;

        /* renamed from: iz */
        public static final int searchenginetext = 2131099746;

        /* renamed from: ja */
        public static final int webPageShareLayout = 2131099651;

        /* renamed from: jb */
        public static final int webcontrol = 2131099783;

        /* renamed from: jc */
        public static final int websiteRelativeLayout = 2131099872;

        /* renamed from: jd */
        public static final int workspace = 2131099777;

        /* renamed from: je */
        public static final int zjj_add = 2131099725;

        /* renamed from: jf */
        public static final int zoom_in = 2131099879;

        /* renamed from: jg */
        public static final int zoom_out = 2131099878;
        public static final int list = 2131099731;
        public static final int message = 2131099828;
        public static final int progress = 2131099827;
        public static final int text = 2131099836;
        public static final int text1 = 2131099692;
        public static final int title = 2131099662;
        public static final int visible = 2131099648;
        /* added by JADX */

        /* renamed from: invisible  reason: collision with other field name */
        public static final int f651invisible = 2131099649;
        /* added by JADX */

        /* renamed from: gone  reason: collision with other field name */
        public static final int f652gone = 2131099650;
        /* added by JADX */

        /* renamed from: webPageShareLayout  reason: collision with other field name */
        public static final int f653webPageShareLayout = 2131099651;
        /* added by JADX */

        /* renamed from: Browser_TitleBar  reason: collision with other field name */
        public static final int f654Browser_TitleBar = 2131099652;
        /* added by JADX */

        /* renamed from: controlbar  reason: collision with other field name */
        public static final int f655controlbar = 2131099653;
        /* added by JADX */

        /* renamed from: selector_list  reason: collision with other field name */
        public static final int f656selector_list = 2131099654;
        /* added by JADX */

        /* renamed from: layoutDisplay  reason: collision with other field name */
        public static final int f657layoutDisplay = 2131099655;
        /* added by JADX */

        /* renamed from: sfPreview  reason: collision with other field name */
        public static final int f658sfPreview = 2131099656;
        /* added by JADX */

        /* renamed from: imgCaptured  reason: collision with other field name */
        public static final int f659imgCaptured = 2131099657;
        /* added by JADX */

        /* renamed from: btnShut  reason: collision with other field name */
        public static final int f660btnShut = 2131099658;
        /* added by JADX */

        /* renamed from: btnReshut  reason: collision with other field name */
        public static final int f661btnReshut = 2131099659;
        /* added by JADX */

        /* renamed from: btnConfirm  reason: collision with other field name */
        public static final int f662btnConfirm = 2131099660;
        /* added by JADX */

        /* renamed from: btnCancel  reason: collision with other field name */
        public static final int f663btnCancel = 2131099661;
        /* added by JADX */

        /* renamed from: other_tv  reason: collision with other field name */
        public static final int f664other_tv = 2131099663;
        /* added by JADX */

        /* renamed from: add_bookmark_to  reason: collision with other field name */
        public static final int f665add_bookmark_to = 2131099664;
        /* added by JADX */

        /* renamed from: add_bookmark_to_text  reason: collision with other field name */
        public static final int f666add_bookmark_to_text = 2131099665;
        /* added by JADX */

        /* renamed from: add_bookmark_to_divider  reason: collision with other field name */
        public static final int f667add_bookmark_to_divider = 2131099666;
        /* added by JADX */

        /* renamed from: add_bookmark_to_list  reason: collision with other field name */
        public static final int f668add_bookmark_to_list = 2131099667;
        /* added by JADX */

        /* renamed from: add_bookmark_to_bookmark  reason: collision with other field name */
        public static final int f669add_bookmark_to_bookmark = 2131099668;
        /* added by JADX */

        /* renamed from: add_bookmark_to_bookmark_img  reason: collision with other field name */
        public static final int f670add_bookmark_to_bookmark_img = 2131099669;
        /* added by JADX */

        /* renamed from: add_bookmark_to_bookmark_text  reason: collision with other field name */
        public static final int f671add_bookmark_to_bookmark_text = 2131099670;
        /* added by JADX */

        /* renamed from: add_bookmark_to_navi  reason: collision with other field name */
        public static final int f672add_bookmark_to_navi = 2131099671;
        /* added by JADX */

        /* renamed from: add_bookmark_to_navi_img  reason: collision with other field name */
        public static final int f673add_bookmark_to_navi_img = 2131099672;
        /* added by JADX */

        /* renamed from: add_bookmark_to_navi_text  reason: collision with other field name */
        public static final int f674add_bookmark_to_navi_text = 2131099673;
        /* added by JADX */

        /* renamed from: subtitle  reason: collision with other field name */
        public static final int f675subtitle = 2131099675;
        /* added by JADX */

        /* renamed from: Browser_Controlbar  reason: collision with other field name */
        public static final int f676Browser_Controlbar = 2131099676;
        /* added by JADX */

        /* renamed from: prifix_name  reason: collision with other field name */
        public static final int f677prifix_name = 2131099677;
        /* added by JADX */

        /* renamed from: bookmark_dlg_filename  reason: collision with other field name */
        public static final int f678bookmark_dlg_filename = 2131099678;
        /* added by JADX */

        /* renamed from: prifix_add  reason: collision with other field name */
        public static final int f679prifix_add = 2131099679;
        /* added by JADX */

        /* renamed from: bookmark_dlg_filepath  reason: collision with other field name */
        public static final int f680bookmark_dlg_filepath = 2131099680;
        /* added by JADX */

        /* renamed from: t_msg  reason: collision with other field name */
        public static final int f681t_msg = 2131099681;
        /* added by JADX */

        /* renamed from: bookmark_dlg_folder  reason: collision with other field name */
        public static final int f682bookmark_dlg_folder = 2131099682;
        /* added by JADX */

        /* renamed from: file_name  reason: collision with other field name */
        public static final int f683file_name = 2131099683;
        /* added by JADX */

        /* renamed from: bookmark_dlg_exportname  reason: collision with other field name */
        public static final int f684bookmark_dlg_exportname = 2131099684;
        /* added by JADX */

        /* renamed from: bookmarkname  reason: collision with other field name */
        public static final int f685bookmarkname = 2131099685;
        /* added by JADX */

        /* renamed from: bookmarkaddress  reason: collision with other field name */
        public static final int f686bookmarkaddress = 2131099686;
        /* added by JADX */

        /* renamed from: submit  reason: collision with other field name */
        public static final int f687submit = 2131099688;
        /* added by JADX */

        /* renamed from: cancle  reason: collision with other field name */
        public static final int f688cancle = 2131099689;
        /* added by JADX */

        /* renamed from: bookmark_dlg_name  reason: collision with other field name */
        public static final int f689bookmark_dlg_name = 2131099690;
        /* added by JADX */

        /* renamed from: bookmark_dlg_foldername  reason: collision with other field name */
        public static final int f690bookmark_dlg_foldername = 2131099691;
        /* added by JADX */

        /* renamed from: titlebar_layout  reason: collision with other field name */
        public static final int f691titlebar_layout = 2131099693;
        /* added by JADX */

        /* renamed from: image_progress  reason: collision with other field name */
        public static final int f692image_progress = 2131099694;
        /* added by JADX */

        /* renamed from: title_progress  reason: collision with other field name */
        public static final int f693title_progress = 2131099695;
        /* added by JADX */

        /* renamed from: Browser_TimeBar  reason: collision with other field name */
        public static final int f694Browser_TimeBar = 2131099696;
        /* added by JADX */

        /* renamed from: browser_safemark  reason: collision with other field name */
        public static final int f695browser_safemark = 2131099697;
        /* added by JADX */

        /* renamed from: entrance_container  reason: collision with other field name */
        public static final int f696entrance_container = 2131099698;
        /* added by JADX */

        /* renamed from: browser_readmode_entrance  reason: collision with other field name */
        public static final int f697browser_readmode_entrance = 2131099699;
        /* added by JADX */

        /* renamed from: fullscreen_progress  reason: collision with other field name */
        public static final int f698fullscreen_progress = 2131099700;
        /* added by JADX */

        /* renamed from: controlbar_layout  reason: collision with other field name */
        public static final int f699controlbar_layout = 2131099701;
        /* added by JADX */

        /* renamed from: Browser_MainPage  reason: collision with other field name */
        public static final int f700Browser_MainPage = 2131099702;
        /* added by JADX */

        /* renamed from: controlbar_fs_main_default  reason: collision with other field name */
        public static final int f701controlbar_fs_main_default = 2131099703;
        /* added by JADX */

        /* renamed from: controlbar_fs_popup  reason: collision with other field name */
        public static final int f702controlbar_fs_popup = 2131099704;
        /* added by JADX */

        /* renamed from: controlbar_fs_stop  reason: collision with other field name */
        public static final int f703controlbar_fs_stop = 2131099705;
        /* added by JADX */

        /* renamed from: controlbar_fs_main  reason: collision with other field name */
        public static final int f704controlbar_fs_main = 2131099706;
        /* added by JADX */

        /* renamed from: inputurl_layout  reason: collision with other field name */
        public static final int f705inputurl_layout = 2131099707;
        /* added by JADX */

        /* renamed from: browser_page_attrs_title  reason: collision with other field name */
        public static final int f706browser_page_attrs_title = 2131099708;
        /* added by JADX */

        /* renamed from: pageattr_button_copy_address  reason: collision with other field name */
        public static final int f707pageattr_button_copy_address = 2131099709;
        /* added by JADX */

        /* renamed from: browser_page_attrs_address  reason: collision with other field name */
        public static final int f708browser_page_attrs_address = 2131099710;
        /* added by JADX */

        /* renamed from: pageattr_button_copy_url  reason: collision with other field name */
        public static final int f709pageattr_button_copy_url = 2131099711;
        /* added by JADX */

        /* renamed from: browser_page_attrs_url  reason: collision with other field name */
        public static final int f710browser_page_attrs_url = 2131099712;
        /* added by JADX */

        /* renamed from: pageattr_image_url  reason: collision with other field name */
        public static final int f711pageattr_image_url = 2131099713;
        /* added by JADX */

        /* renamed from: pageattr_button_copy_image_url  reason: collision with other field name */
        public static final int f712pageattr_button_copy_image_url = 2131099714;
        /* added by JADX */

        /* renamed from: browser_page_attrs_image_url  reason: collision with other field name */
        public static final int f713browser_page_attrs_image_url = 2131099715;
        /* added by JADX */

        /* renamed from: tips_zoom_img  reason: collision with other field name */
        public static final int f714tips_zoom_img = 2131099716;
        /* added by JADX */

        /* renamed from: img_zoom  reason: collision with other field name */
        public static final int f715img_zoom = 2131099717;
        /* added by JADX */

        /* renamed from: img_fit  reason: collision with other field name */
        public static final int f716img_fit = 2131099718;
        /* added by JADX */

        /* renamed from: tips_zoom_msg  reason: collision with other field name */
        public static final int f717tips_zoom_msg = 2131099719;
        /* added by JADX */

        /* renamed from: tips_close  reason: collision with other field name */
        public static final int f718tips_close = 2131099720;
        /* added by JADX */

        /* renamed from: tips_over  reason: collision with other field name */
        public static final int f719tips_over = 2131099721;
        /* added by JADX */

        /* renamed from: browser_upmsg  reason: collision with other field name */
        public static final int f720browser_upmsg = 2131099722;
        /* added by JADX */

        /* renamed from: close_refreshtimer  reason: collision with other field name */
        public static final int f721close_refreshtimer = 2131099723;
        /* added by JADX */

        /* renamed from: test_ip  reason: collision with other field name */
        public static final int f722test_ip = 2131099724;
        /* added by JADX */

        /* renamed from: zjj_add  reason: collision with other field name */
        public static final int f723zjj_add = 2131099725;
        /* added by JADX */

        /* renamed from: dpc_add  reason: collision with other field name */
        public static final int f724dpc_add = 2131099726;
        /* added by JADX */

        /* renamed from: upl_add0  reason: collision with other field name */
        public static final int f725upl_add0 = 2131099727;
        /* added by JADX */

        /* renamed from: upl_add1  reason: collision with other field name */
        public static final int f726upl_add1 = 2131099728;
        /* added by JADX */

        /* renamed from: scrollTimer  reason: collision with other field name */
        public static final int f727scrollTimer = 2131099729;
        /* added by JADX */

        /* renamed from: debug_submit  reason: collision with other field name */
        public static final int f728debug_submit = 2131099730;
        /* added by JADX */

        /* renamed from: downloadIcon  reason: collision with other field name */
        public static final int f729downloadIcon = 2131099732;
        /* added by JADX */

        /* renamed from: downloadTaskName  reason: collision with other field name */
        public static final int f730downloadTaskName = 2131099733;
        /* added by JADX */

        /* renamed from: downloadProgressText  reason: collision with other field name */
        public static final int f731downloadProgressText = 2131099734;
        /* added by JADX */

        /* renamed from: downloadProgressBar  reason: collision with other field name */
        public static final int f732downloadProgressBar = 2131099735;
        /* added by JADX */

        /* renamed from: completeTimeText  reason: collision with other field name */
        public static final int f733completeTimeText = 2131099736;
        /* added by JADX */

        /* renamed from: download_content  reason: collision with other field name */
        public static final int f734download_content = 2131099737;
        /* added by JADX */

        /* renamed from: lunch  reason: collision with other field name */
        public static final int f735lunch = 2131099738;
        /* added by JADX */

        /* renamed from: deldownload  reason: collision with other field name */
        public static final int f736deldownload = 2131099739;
        /* added by JADX */

        /* renamed from: breakfast  reason: collision with other field name */
        public static final int f737breakfast = 2131099740;
        /* added by JADX */

        /* renamed from: filename  reason: collision with other field name */
        public static final int f738filename = 2131099741;
        /* added by JADX */

        /* renamed from: download_dlg_filename  reason: collision with other field name */
        public static final int f739download_dlg_filename = 2131099742;
        /* added by JADX */

        /* renamed from: dlg_ok  reason: collision with other field name */
        public static final int f740dlg_ok = 2131099743;
        /* added by JADX */

        /* renamed from: dlg_cancel  reason: collision with other field name */
        public static final int f741dlg_cancel = 2131099744;
        /* added by JADX */

        /* renamed from: searchengineimage  reason: collision with other field name */
        public static final int f742searchengineimage = 2131099745;
        /* added by JADX */

        /* renamed from: searchenginetext  reason: collision with other field name */
        public static final int f743searchenginetext = 2131099746;
        /* added by JADX */

        /* renamed from: searchengineimg  reason: collision with other field name */
        public static final int f744searchengineimg = 2131099747;
        /* added by JADX */

        /* renamed from: no_sd_card_warning  reason: collision with other field name */
        public static final int f745no_sd_card_warning = 2131099748;
        /* added by JADX */

        /* renamed from: file_list  reason: collision with other field name */
        public static final int f746file_list = 2131099749;
        /* added by JADX */

        /* renamed from: filepath  reason: collision with other field name */
        public static final int f747filepath = 2131099750;
        /* added by JADX */

        /* renamed from: fileattr_filepath  reason: collision with other field name */
        public static final int f748fileattr_filepath = 2131099751;
        /* added by JADX */

        /* renamed from: lastmodify  reason: collision with other field name */
        public static final int f749lastmodify = 2131099752;
        /* added by JADX */

        /* renamed from: fileattr_lastmodify  reason: collision with other field name */
        public static final int f750fileattr_lastmodify = 2131099753;
        /* added by JADX */

        /* renamed from: filesize  reason: collision with other field name */
        public static final int f751filesize = 2131099754;
        /* added by JADX */

        /* renamed from: fileattr_filesize  reason: collision with other field name */
        public static final int f752fileattr_filesize = 2131099755;
        /* added by JADX */

        /* renamed from: item_icon  reason: collision with other field name */
        public static final int f753item_icon = 2131099756;
        /* added by JADX */

        /* renamed from: item_title  reason: collision with other field name */
        public static final int f754item_title = 2131099757;
        /* added by JADX */

        /* renamed from: item_discript  reason: collision with other field name */
        public static final int f755item_discript = 2131099758;
        /* added by JADX */

        /* renamed from: filemgr_dlg_foldername  reason: collision with other field name */
        public static final int f756filemgr_dlg_foldername = 2131099759;
        /* added by JADX */

        /* renamed from: flash  reason: collision with other field name */
        public static final int f757flash = 2131099760;
        /* added by JADX */

        /* renamed from: bookmark_star  reason: collision with other field name */
        public static final int f758bookmark_star = 2131099761;
        /* added by JADX */

        /* renamed from: history_item  reason: collision with other field name */
        public static final int f759history_item = 2131099762;
        /* added by JADX */

        /* renamed from: initial_main  reason: collision with other field name */
        public static final int f760initial_main = 2131099763;
        /* added by JADX */

        /* renamed from: ucmascot  reason: collision with other field name */
        public static final int f761ucmascot = 2131099764;
        /* added by JADX */

        /* renamed from: initial_dotset  reason: collision with other field name */
        public static final int f762initial_dotset = 2131099765;
        /* added by JADX */

        /* renamed from: initial_loading_1  reason: collision with other field name */
        public static final int f763initial_loading_1 = 2131099766;
        /* added by JADX */

        /* renamed from: initial_loading_2  reason: collision with other field name */
        public static final int f764initial_loading_2 = 2131099767;
        /* added by JADX */

        /* renamed from: initial_loading_3  reason: collision with other field name */
        public static final int f765initial_loading_3 = 2131099768;
        /* added by JADX */

        /* renamed from: initial_loading_4  reason: collision with other field name */
        public static final int f766initial_loading_4 = 2131099769;
        /* added by JADX */

        /* renamed from: initial_loading_5  reason: collision with other field name */
        public static final int f767initial_loading_5 = 2131099770;
        /* added by JADX */

        /* renamed from: initial_loading_6  reason: collision with other field name */
        public static final int f768initial_loading_6 = 2131099771;
        /* added by JADX */

        /* renamed from: initial_loading_text  reason: collision with other field name */
        public static final int f769initial_loading_text = 2131099772;
        /* added by JADX */

        /* renamed from: initial_website  reason: collision with other field name */
        public static final int f770initial_website = 2131099773;
        /* added by JADX */

        /* renamed from: fresh_cancel  reason: collision with other field name */
        public static final int f771fresh_cancel = 2131099774;
        /* added by JADX */

        /* renamed from: goto_mainpage  reason: collision with other field name */
        public static final int f772goto_mainpage = 2131099775;
        /* added by JADX */

        /* renamed from: fresh_back  reason: collision with other field name */
        public static final int f773fresh_back = 2131099776;
        /* added by JADX */

        /* renamed from: workspace  reason: collision with other field name */
        public static final int f774workspace = 2131099777;
        /* added by JADX */

        /* renamed from: mainpage_bookmark  reason: collision with other field name */
        public static final int f775mainpage_bookmark = 2131099778;
        /* added by JADX */

        /* renamed from: mainpage_navi  reason: collision with other field name */
        public static final int f776mainpage_navi = 2131099779;
        /* added by JADX */

        /* renamed from: mainpage_channel  reason: collision with other field name */
        public static final int f777mainpage_channel = 2131099780;
        /* added by JADX */

        /* renamed from: itemtext  reason: collision with other field name */
        public static final int f778itemtext = 2131099781;
        /* added by JADX */

        /* renamed from: btnShot  reason: collision with other field name */
        public static final int f779btnShot = 2131099782;
        /* added by JADX */

        /* renamed from: webcontrol  reason: collision with other field name */
        public static final int f780webcontrol = 2131099783;
        /* added by JADX */

        /* renamed from: tab_container  reason: collision with other field name */
        public static final int f781tab_container = 2131099784;
        /* added by JADX */

        /* renamed from: mainpage_buz  reason: collision with other field name */
        public static final int f782mainpage_buz = 2131099785;
        /* added by JADX */

        /* renamed from: input_url_bar  reason: collision with other field name */
        public static final int f783input_url_bar = 2131099786;
        /* added by JADX */

        /* renamed from: navi  reason: collision with other field name */
        public static final int f784navi = 2131099787;
        /* added by JADX */

        /* renamed from: menu_layout  reason: collision with other field name */
        public static final int f785menu_layout = 2131099788;
        /* added by JADX */

        /* renamed from: frame  reason: collision with other field name */
        public static final int f786frame = 2131099789;
        /* added by JADX */

        /* renamed from: button_area  reason: collision with other field name */
        public static final int f787button_area = 2131099791;
        /* added by JADX */

        /* renamed from: new_window_button  reason: collision with other field name */
        public static final int f788new_window_button = 2131099792;
        /* added by JADX */

        /* renamed from: multiwindowlist  reason: collision with other field name */
        public static final int f789multiwindowlist = 2131099793;
        /* added by JADX */

        /* renamed from: prefix_title  reason: collision with other field name */
        public static final int f790prefix_title = 2131099794;
        /* added by JADX */

        /* renamed from: mynavi_title  reason: collision with other field name */
        public static final int f791mynavi_title = 2131099795;
        /* added by JADX */

        /* renamed from: prefix_url_add  reason: collision with other field name */
        public static final int f792prefix_url_add = 2131099796;
        /* added by JADX */

        /* renamed from: mynavi_url  reason: collision with other field name */
        public static final int f793mynavi_url = 2131099797;
        /* added by JADX */

        /* renamed from: puller  reason: collision with other field name */
        public static final int f794puller = 2131099798;
        /* added by JADX */

        /* renamed from: open_bookmark_history  reason: collision with other field name */
        public static final int f795open_bookmark_history = 2131099799;
        /* added by JADX */

        /* renamed from: mynavi_confirm  reason: collision with other field name */
        public static final int f796mynavi_confirm = 2131099800;
        /* added by JADX */

        /* renamed from: mynavi_cancel  reason: collision with other field name */
        public static final int f797mynavi_cancel = 2131099801;
        /* added by JADX */

        /* renamed from: bookmark_container  reason: collision with other field name */
        public static final int f798bookmark_container = 2131099802;
        /* added by JADX */

        /* renamed from: bookmark_list  reason: collision with other field name */
        public static final int f799bookmark_list = 2131099803;
        /* added by JADX */

        /* renamed from: cancel_bar  reason: collision with other field name */
        public static final int f800cancel_bar = 2131099804;
        /* added by JADX */

        /* renamed from: cancel_bookmark_list  reason: collision with other field name */
        public static final int f801cancel_bookmark_list = 2131099805;
        /* added by JADX */

        /* renamed from: container  reason: collision with other field name */
        public static final int f802container = 2131099806;
        /* added by JADX */

        /* renamed from: page_up  reason: collision with other field name */
        public static final int f803page_up = 2131099807;
        /* added by JADX */

        /* renamed from: page_down  reason: collision with other field name */
        public static final int f804page_down = 2131099808;
        /* added by JADX */

        /* renamed from: plugin_downloadfail_day  reason: collision with other field name */
        public static final int f805plugin_downloadfail_day = 2131099809;
        /* added by JADX */

        /* renamed from: plugin_iv  reason: collision with other field name */
        public static final int f806plugin_iv = 2131099810;
        /* added by JADX */

        /* renamed from: plugin_tv  reason: collision with other field name */
        public static final int f807plugin_tv = 2131099811;
        /* added by JADX */

        /* renamed from: plugin_btn  reason: collision with other field name */
        public static final int f808plugin_btn = 2131099812;
        /* added by JADX */

        /* renamed from: plugin_downloadfail_night  reason: collision with other field name */
        public static final int f809plugin_downloadfail_night = 2131099813;
        /* added by JADX */

        /* renamed from: plugin_downloading_day  reason: collision with other field name */
        public static final int f810plugin_downloading_day = 2131099814;
        /* added by JADX */

        /* renamed from: plugin_progressbar  reason: collision with other field name */
        public static final int f811plugin_progressbar = 2131099815;
        /* added by JADX */

        /* renamed from: plugin_downloading_night  reason: collision with other field name */
        public static final int f812plugin_downloading_night = 2131099816;
        /* added by JADX */

        /* renamed from: plugin_needdownload_day  reason: collision with other field name */
        public static final int f813plugin_needdownload_day = 2131099817;
        /* added by JADX */

        /* renamed from: plugin_needdownload_night  reason: collision with other field name */
        public static final int f814plugin_needdownload_night = 2131099818;
        /* added by JADX */

        /* renamed from: uc_pref_clear_cache  reason: collision with other field name */
        public static final int f815uc_pref_clear_cache = 2131099819;
        /* added by JADX */

        /* renamed from: uc_pref_clear_cookie  reason: collision with other field name */
        public static final int f816uc_pref_clear_cookie = 2131099820;
        /* added by JADX */

        /* renamed from: uc_pref_clear_form  reason: collision with other field name */
        public static final int f817uc_pref_clear_form = 2131099821;
        /* added by JADX */

        /* renamed from: uc_pref_clear_history  reason: collision with other field name */
        public static final int f818uc_pref_clear_history = 2131099822;
        /* added by JADX */

        /* renamed from: seekbar  reason: collision with other field name */
        public static final int f819seekbar = 2131099823;
        /* added by JADX */

        /* renamed from: controlbar_button_back  reason: collision with other field name */
        public static final int f820controlbar_button_back = 2131099824;
        /* added by JADX */

        /* renamed from: controlbar_image_back  reason: collision with other field name */
        public static final int f821controlbar_image_back = 2131099825;
        /* added by JADX */

        /* renamed from: controlbar_head  reason: collision with other field name */
        public static final int f822controlbar_head = 2131099826;
        /* added by JADX */

        /* renamed from: msg  reason: collision with other field name */
        public static final int f823msg = 2131099829;
        /* added by JADX */

        /* renamed from: ucsave  reason: collision with other field name */
        public static final int f824ucsave = 2131099830;
        /* added by JADX */

        /* renamed from: msg1  reason: collision with other field name */
        public static final int f825msg1 = 2131099831;
        /* added by JADX */

        /* renamed from: msg2  reason: collision with other field name */
        public static final int f826msg2 = 2131099832;
        /* added by JADX */

        /* renamed from: msg3  reason: collision with other field name */
        public static final int f827msg3 = 2131099833;
        /* added by JADX */

        /* renamed from: clear_all  reason: collision with other field name */
        public static final int f828clear_all = 2131099834;
        /* added by JADX */

        /* renamed from: dont_tips  reason: collision with other field name */
        public static final int f829dont_tips = 2131099835;
        /* added by JADX */

        /* renamed from: arrow  reason: collision with other field name */
        public static final int f830arrow = 2131099838;
        /* added by JADX */

        /* renamed from: seprator  reason: collision with other field name */
        public static final int f831seprator = 2131099839;
        /* added by JADX */

        /* renamed from: smooth_title  reason: collision with other field name */
        public static final int f832smooth_title = 2131099840;
        /* added by JADX */

        /* renamed from: smooth_discribe  reason: collision with other field name */
        public static final int f833smooth_discribe = 2131099841;
        /* added by JADX */

        /* renamed from: record_title  reason: collision with other field name */
        public static final int f834record_title = 2131099842;
        /* added by JADX */

        /* renamed from: record_discribe  reason: collision with other field name */
        public static final int f835record_discribe = 2131099843;
        /* added by JADX */

        /* renamed from: intertimer  reason: collision with other field name */
        public static final int f836intertimer = 2131099844;
        /* added by JADX */

        /* renamed from: save_image_name  reason: collision with other field name */
        public static final int f837save_image_name = 2131099845;
        /* added by JADX */

        /* renamed from: saveimage_dlg_filename  reason: collision with other field name */
        public static final int f838saveimage_dlg_filename = 2131099846;
        /* added by JADX */

        /* renamed from: save_image_ok  reason: collision with other field name */
        public static final int f839save_image_ok = 2131099847;
        /* added by JADX */

        /* renamed from: save_image_cancel  reason: collision with other field name */
        public static final int f840save_image_cancel = 2131099848;
        /* added by JADX */

        /* renamed from: save_source_name  reason: collision with other field name */
        public static final int f841save_source_name = 2131099849;
        /* added by JADX */

        /* renamed from: savesource_dlg_filename  reason: collision with other field name */
        public static final int f842savesource_dlg_filename = 2131099850;
        /* added by JADX */

        /* renamed from: save_source_ok  reason: collision with other field name */
        public static final int f843save_source_ok = 2131099851;
        /* added by JADX */

        /* renamed from: save_source_cancel  reason: collision with other field name */
        public static final int f844save_source_cancel = 2131099852;
        /* added by JADX */

        /* renamed from: enginelist_icon  reason: collision with other field name */
        public static final int f845enginelist_icon = 2131099853;
        /* added by JADX */

        /* renamed from: enginelist_title  reason: collision with other field name */
        public static final int f846enginelist_title = 2131099854;
        /* added by JADX */

        /* renamed from: searchRelativeLayout  reason: collision with other field name */
        public static final int f847searchRelativeLayout = 2131099855;
        /* added by JADX */

        /* renamed from: search_bar_textView  reason: collision with other field name */
        public static final int f848search_bar_textView = 2131099856;
        /* added by JADX */

        /* renamed from: search_input_list  reason: collision with other field name */
        public static final int f849search_input_list = 2131099857;
        /* added by JADX */

        /* renamed from: search_bar_editText  reason: collision with other field name */
        public static final int f850search_bar_editText = 2131099858;
        /* added by JADX */

        /* renamed from: search_bar_input  reason: collision with other field name */
        public static final int f851search_bar_input = 2131099859;
        /* added by JADX */

        /* renamed from: search_input_cancle  reason: collision with other field name */
        public static final int f852search_input_cancle = 2131099860;
        /* added by JADX */

        /* renamed from: search_engine_icon_layout  reason: collision with other field name */
        public static final int f853search_engine_icon_layout = 2131099861;
        /* added by JADX */

        /* renamed from: search_engine_icon  reason: collision with other field name */
        public static final int f854search_engine_icon = 2131099862;
        /* added by JADX */

        /* renamed from: search_engine_downicon  reason: collision with other field name */
        public static final int f855search_engine_downicon = 2131099863;
        /* added by JADX */

        /* renamed from: engine_list_cover  reason: collision with other field name */
        public static final int f856engine_list_cover = 2131099864;
        /* added by JADX */

        /* renamed from: search_engine_list  reason: collision with other field name */
        public static final int f857search_engine_list = 2131099865;
        /* added by JADX */

        /* renamed from: search_engine_text  reason: collision with other field name */
        public static final int f858search_engine_text = 2131099866;
        /* added by JADX */

        /* renamed from: search_engine_divider  reason: collision with other field name */
        public static final int f859search_engine_divider = 2131099867;
        /* added by JADX */

        /* renamed from: search_engine_list_icon  reason: collision with other field name */
        public static final int f860search_engine_list_icon = 2131099868;
        /* added by JADX */

        /* renamed from: Select  reason: collision with other field name */
        public static final int f861Select = 2131099869;
        /* added by JADX */

        /* renamed from: upload_msg  reason: collision with other field name */
        public static final int f862upload_msg = 2131099870;
        /* added by JADX */

        /* renamed from: item_image  reason: collision with other field name */
        public static final int f863item_image = 2131099871;
        /* added by JADX */

        /* renamed from: websiteRelativeLayout  reason: collision with other field name */
        public static final int f864websiteRelativeLayout = 2131099872;
        /* added by JADX */

        /* renamed from: address_bar_textView  reason: collision with other field name */
        public static final int f865address_bar_textView = 2131099873;
        /* added by JADX */

        /* renamed from: address_input_list  reason: collision with other field name */
        public static final int f866address_input_list = 2131099874;
        /* added by JADX */

        /* renamed from: address_bar_input  reason: collision with other field name */
        public static final int f867address_bar_input = 2131099875;
        /* added by JADX */

        /* renamed from: address_input_state  reason: collision with other field name */
        public static final int f868address_input_state = 2131099876;
        /* added by JADX */

        /* renamed from: address_bar_editText  reason: collision with other field name */
        public static final int f869address_bar_editText = 2131099877;
        /* added by JADX */

        /* renamed from: zoom_out  reason: collision with other field name */
        public static final int f870zoom_out = 2131099878;
        /* added by JADX */

        /* renamed from: zoom_in  reason: collision with other field name */
        public static final int f871zoom_in = 2131099879;
    }

    public final class layout {

        /* renamed from: aqA */
        public static final int dlg_application_list = 2130903059;

        /* renamed from: aqB */
        public static final int downlaod_sys_notification = 2130903060;

        /* renamed from: aqC */
        public static final int downlaod_sys_notification_complete = 2130903061;

        /* renamed from: aqD */
        public static final int download = 2130903062;

        /* renamed from: aqE */
        public static final int download_del = 2130903063;

        /* renamed from: aqF */
        public static final int download_dialog_inputfilename = 2130903064;

        /* renamed from: aqG */
        public static final int engineitem = 2130903065;

        /* renamed from: aqH */
        public static final int file_chooser = 2130903066;

        /* renamed from: aqI */
        public static final int file_dialog_attr = 2130903067;

        /* renamed from: aqJ */
        public static final int file_item_grid = 2130903068;

        /* renamed from: aqK */
        public static final int file_item_list = 2130903069;

        /* renamed from: aqL */
        public static final int file_maintain = 2130903070;

        /* renamed from: aqM */
        public static final int filemgr_dialog_newfolder = 2130903071;

        /* renamed from: aqN */
        public static final int flash_window = 2130903072;

        /* renamed from: aqO */
        public static final int history_bookmark_item = 2130903073;

        /* renamed from: aqP */
        public static final int initial_loading = 2130903074;

        /* renamed from: aqQ */
        public static final int launcher = 2130903075;

        /* renamed from: aqR */
        public static final int listitem = 2130903076;

        /* renamed from: aqS */
        public static final int main = 2130903077;

        /* renamed from: aqT */
        public static final int menu_dialog_layout = 2130903081;

        /* renamed from: aqU */
        public static final int multiwindow_manager_ex = 2130903082;

        /* renamed from: aqV */
        public static final int multiwindowlist_manager = 2130903083;

        /* renamed from: aqW */
        public static final int mynavi_edit = 2130903084;

        /* renamed from: aqX */
        public static final int mynavi_edit_list_dlg = 2130903085;

        /* renamed from: aqY */
        public static final int notice = 2130903086;

        /* renamed from: aqZ */
        public static final int page_up_down_button = 2130903087;

        /* renamed from: aqi */
        public static final int activity_selector = 2130903040;

        /* renamed from: aqj */
        public static final int activitycamera = 2130903041;

        /* renamed from: aqk */
        public static final int application_item = 2130903043;

        /* renamed from: aql */
        public static final int autocomplete_item = 2130903044;

        /* renamed from: aqm */
        public static final int autocomplete_urlitem = 2130903045;

        /* renamed from: aqn */
        public static final int bar_layout = 2130903046;

        /* renamed from: aqo */
        public static final int bookmark_dialog_editbookmark = 2130903047;

        /* renamed from: aqp */
        public static final int bookmark_dialog_export = 2130903048;

        /* renamed from: aqq */
        public static final int bookmark_dialog_newbookmark = 2130903049;

        /* renamed from: aqr */
        public static final int bookmark_dialog_newfolder = 2130903050;

        /* renamed from: aqs */
        public static final int bookmark_dialog_spinneritem = 2130903051;

        /* renamed from: aqt */
        public static final int browser = 2130903052;

        /* renamed from: aqu */
        public static final int browser_controlbar_fullscreen = 2130903053;

        /* renamed from: aqv */
        public static final int browser_dialog_pageattrs = 2130903054;

        /* renamed from: aqw */
        public static final int browser_dialog_tipszoommode = 2130903055;

        /* renamed from: aqx */
        public static final int browser_dialog_upmsg = 2130903056;

        /* renamed from: aqy */
        public static final int close_timerrefresh = 2130903057;

        /* renamed from: aqz */
        public static final int debug_setting = 2130903058;

        /* renamed from: ara */
        public static final int pref_clear_all = 2130903094;

        /* renamed from: arb */
        public static final int pref_seekbar = 2130903095;

        /* renamed from: arc */
        public static final int preference_layout = 2130903096;

        /* renamed from: ard */
        public static final int progress_dlg_content = 2130903097;

        /* renamed from: are */
        public static final int quit_dialog = 2130903098;

        /* renamed from: arf */
        public static final int reading_page_tail = 2130903099;

        /* renamed from: arg */
        public static final int readingmode_edu_dlg = 2130903100;

        /* renamed from: arh */
        public static final int refresh_timer = 2130903101;

        /* renamed from: ari */
        public static final int save_image_dialog = 2130903102;

        /* renamed from: arj */
        public static final int save_source_dialog = 2130903103;

        /* renamed from: ark */
        public static final int search_enginelist_item = 2130903104;

        /* renamed from: arl */
        public static final int search_list_engine = 2130903105;

        /* renamed from: arm */
        public static final int searchdialog = 2130903106;

        /* renamed from: arn */
        public static final int select = 2130903107;

        /* renamed from: aro */
        public static final int single_choice_list_item = 2130903108;

        /* renamed from: arp */
        public static final int single_choice_list_item_no_checkmark = 2130903109;

        /* renamed from: arq */
        public static final int title_container = 2130903110;

        /* renamed from: arr */
        public static final int uc_traffic_save = 2130903111;

        /* renamed from: ars */
        public static final int uccontextmenu_item = 2130903112;

        /* renamed from: art */
        public static final int upload_font = 2130903113;

        /* renamed from: aru */
        public static final int urlitem = 2130903114;

        /* renamed from: arv */
        public static final int websitedialog = 2130903115;

        /* renamed from: arw */
        public static final int workspace_screen = 2130903116;

        /* renamed from: arx */
        public static final int zoom_juc_controls = 2130903117;

        /* renamed from: eX */
        public static final int add_bookmark_to = 2130903042;

        /* renamed from: hJ */
        public static final int plugin_downloadfail_day = 2130903088;

        /* renamed from: hK */
        public static final int plugin_downloadfail_night = 2130903089;

        /* renamed from: hL */
        public static final int plugin_downloading_day = 2130903090;

        /* renamed from: hM */
        public static final int plugin_downloading_night = 2130903091;

        /* renamed from: hO */
        public static final int plugin_needdownload_day = 2130903092;

        /* renamed from: hP */
        public static final int plugin_needdownload_night = 2130903093;

        /* renamed from: hj */
        public static final int mainpage_bookmark = 2130903078;

        /* renamed from: hl */
        public static final int mainpage_channel = 2130903079;

        /* renamed from: hm */
        public static final int mainpage_navi = 2130903080;
        /* added by JADX */

        /* renamed from: activity_selector  reason: collision with other field name */
        public static final int f872activity_selector = 2130903040;
        /* added by JADX */

        /* renamed from: activitycamera  reason: collision with other field name */
        public static final int f873activitycamera = 2130903041;
        /* added by JADX */

        /* renamed from: add_bookmark_to  reason: collision with other field name */
        public static final int f874add_bookmark_to = 2130903042;
        /* added by JADX */

        /* renamed from: application_item  reason: collision with other field name */
        public static final int f875application_item = 2130903043;
        /* added by JADX */

        /* renamed from: autocomplete_item  reason: collision with other field name */
        public static final int f876autocomplete_item = 2130903044;
        /* added by JADX */

        /* renamed from: autocomplete_urlitem  reason: collision with other field name */
        public static final int f877autocomplete_urlitem = 2130903045;
        /* added by JADX */

        /* renamed from: bar_layout  reason: collision with other field name */
        public static final int f878bar_layout = 2130903046;
        /* added by JADX */

        /* renamed from: bookmark_dialog_editbookmark  reason: collision with other field name */
        public static final int f879bookmark_dialog_editbookmark = 2130903047;
        /* added by JADX */

        /* renamed from: bookmark_dialog_export  reason: collision with other field name */
        public static final int f880bookmark_dialog_export = 2130903048;
        /* added by JADX */

        /* renamed from: bookmark_dialog_newbookmark  reason: collision with other field name */
        public static final int f881bookmark_dialog_newbookmark = 2130903049;
        /* added by JADX */

        /* renamed from: bookmark_dialog_newfolder  reason: collision with other field name */
        public static final int f882bookmark_dialog_newfolder = 2130903050;
        /* added by JADX */

        /* renamed from: bookmark_dialog_spinneritem  reason: collision with other field name */
        public static final int f883bookmark_dialog_spinneritem = 2130903051;
        /* added by JADX */

        /* renamed from: browser  reason: collision with other field name */
        public static final int f884browser = 2130903052;
        /* added by JADX */

        /* renamed from: browser_controlbar_fullscreen  reason: collision with other field name */
        public static final int f885browser_controlbar_fullscreen = 2130903053;
        /* added by JADX */

        /* renamed from: browser_dialog_pageattrs  reason: collision with other field name */
        public static final int f886browser_dialog_pageattrs = 2130903054;
        /* added by JADX */

        /* renamed from: browser_dialog_tipszoommode  reason: collision with other field name */
        public static final int f887browser_dialog_tipszoommode = 2130903055;
        /* added by JADX */

        /* renamed from: browser_dialog_upmsg  reason: collision with other field name */
        public static final int f888browser_dialog_upmsg = 2130903056;
        /* added by JADX */

        /* renamed from: close_timerrefresh  reason: collision with other field name */
        public static final int f889close_timerrefresh = 2130903057;
        /* added by JADX */

        /* renamed from: debug_setting  reason: collision with other field name */
        public static final int f890debug_setting = 2130903058;
        /* added by JADX */

        /* renamed from: dlg_application_list  reason: collision with other field name */
        public static final int f891dlg_application_list = 2130903059;
        /* added by JADX */

        /* renamed from: downlaod_sys_notification  reason: collision with other field name */
        public static final int f892downlaod_sys_notification = 2130903060;
        /* added by JADX */

        /* renamed from: downlaod_sys_notification_complete  reason: collision with other field name */
        public static final int f893downlaod_sys_notification_complete = 2130903061;
        /* added by JADX */

        /* renamed from: download  reason: collision with other field name */
        public static final int f894download = 2130903062;
        /* added by JADX */

        /* renamed from: download_del  reason: collision with other field name */
        public static final int f895download_del = 2130903063;
        /* added by JADX */

        /* renamed from: download_dialog_inputfilename  reason: collision with other field name */
        public static final int f896download_dialog_inputfilename = 2130903064;
        /* added by JADX */

        /* renamed from: engineitem  reason: collision with other field name */
        public static final int f897engineitem = 2130903065;
        /* added by JADX */

        /* renamed from: file_chooser  reason: collision with other field name */
        public static final int f898file_chooser = 2130903066;
        /* added by JADX */

        /* renamed from: file_dialog_attr  reason: collision with other field name */
        public static final int f899file_dialog_attr = 2130903067;
        /* added by JADX */

        /* renamed from: file_item_grid  reason: collision with other field name */
        public static final int f900file_item_grid = 2130903068;
        /* added by JADX */

        /* renamed from: file_item_list  reason: collision with other field name */
        public static final int f901file_item_list = 2130903069;
        /* added by JADX */

        /* renamed from: file_maintain  reason: collision with other field name */
        public static final int f902file_maintain = 2130903070;
        /* added by JADX */

        /* renamed from: filemgr_dialog_newfolder  reason: collision with other field name */
        public static final int f903filemgr_dialog_newfolder = 2130903071;
        /* added by JADX */

        /* renamed from: flash_window  reason: collision with other field name */
        public static final int f904flash_window = 2130903072;
        /* added by JADX */

        /* renamed from: history_bookmark_item  reason: collision with other field name */
        public static final int f905history_bookmark_item = 2130903073;
        /* added by JADX */

        /* renamed from: initial_loading  reason: collision with other field name */
        public static final int f906initial_loading = 2130903074;
        /* added by JADX */

        /* renamed from: launcher  reason: collision with other field name */
        public static final int f907launcher = 2130903075;
        /* added by JADX */

        /* renamed from: listitem  reason: collision with other field name */
        public static final int f908listitem = 2130903076;
        /* added by JADX */

        /* renamed from: main  reason: collision with other field name */
        public static final int f909main = 2130903077;
        /* added by JADX */

        /* renamed from: mainpage_bookmark  reason: collision with other field name */
        public static final int f910mainpage_bookmark = 2130903078;
        /* added by JADX */

        /* renamed from: mainpage_channel  reason: collision with other field name */
        public static final int f911mainpage_channel = 2130903079;
        /* added by JADX */

        /* renamed from: mainpage_navi  reason: collision with other field name */
        public static final int f912mainpage_navi = 2130903080;
        /* added by JADX */

        /* renamed from: menu_dialog_layout  reason: collision with other field name */
        public static final int f913menu_dialog_layout = 2130903081;
        /* added by JADX */

        /* renamed from: multiwindow_manager_ex  reason: collision with other field name */
        public static final int f914multiwindow_manager_ex = 2130903082;
        /* added by JADX */

        /* renamed from: multiwindowlist_manager  reason: collision with other field name */
        public static final int f915multiwindowlist_manager = 2130903083;
        /* added by JADX */

        /* renamed from: mynavi_edit  reason: collision with other field name */
        public static final int f916mynavi_edit = 2130903084;
        /* added by JADX */

        /* renamed from: mynavi_edit_list_dlg  reason: collision with other field name */
        public static final int f917mynavi_edit_list_dlg = 2130903085;
        /* added by JADX */

        /* renamed from: notice  reason: collision with other field name */
        public static final int f918notice = 2130903086;
        /* added by JADX */

        /* renamed from: page_up_down_button  reason: collision with other field name */
        public static final int f919page_up_down_button = 2130903087;
        /* added by JADX */

        /* renamed from: plugin_downloadfail_day  reason: collision with other field name */
        public static final int f920plugin_downloadfail_day = 2130903088;
        /* added by JADX */

        /* renamed from: plugin_downloadfail_night  reason: collision with other field name */
        public static final int f921plugin_downloadfail_night = 2130903089;
        /* added by JADX */

        /* renamed from: plugin_downloading_day  reason: collision with other field name */
        public static final int f922plugin_downloading_day = 2130903090;
        /* added by JADX */

        /* renamed from: plugin_downloading_night  reason: collision with other field name */
        public static final int f923plugin_downloading_night = 2130903091;
        /* added by JADX */

        /* renamed from: plugin_needdownload_day  reason: collision with other field name */
        public static final int f924plugin_needdownload_day = 2130903092;
        /* added by JADX */

        /* renamed from: plugin_needdownload_night  reason: collision with other field name */
        public static final int f925plugin_needdownload_night = 2130903093;
        /* added by JADX */

        /* renamed from: pref_clear_all  reason: collision with other field name */
        public static final int f926pref_clear_all = 2130903094;
        /* added by JADX */

        /* renamed from: pref_seekbar  reason: collision with other field name */
        public static final int f927pref_seekbar = 2130903095;
        /* added by JADX */

        /* renamed from: preference_layout  reason: collision with other field name */
        public static final int f928preference_layout = 2130903096;
        /* added by JADX */

        /* renamed from: progress_dlg_content  reason: collision with other field name */
        public static final int f929progress_dlg_content = 2130903097;
        /* added by JADX */

        /* renamed from: quit_dialog  reason: collision with other field name */
        public static final int f930quit_dialog = 2130903098;
        /* added by JADX */

        /* renamed from: reading_page_tail  reason: collision with other field name */
        public static final int f931reading_page_tail = 2130903099;
        /* added by JADX */

        /* renamed from: readingmode_edu_dlg  reason: collision with other field name */
        public static final int f932readingmode_edu_dlg = 2130903100;
        /* added by JADX */

        /* renamed from: refresh_timer  reason: collision with other field name */
        public static final int f933refresh_timer = 2130903101;
        /* added by JADX */

        /* renamed from: save_image_dialog  reason: collision with other field name */
        public static final int f934save_image_dialog = 2130903102;
        /* added by JADX */

        /* renamed from: save_source_dialog  reason: collision with other field name */
        public static final int f935save_source_dialog = 2130903103;
        /* added by JADX */

        /* renamed from: search_enginelist_item  reason: collision with other field name */
        public static final int f936search_enginelist_item = 2130903104;
        /* added by JADX */

        /* renamed from: search_list_engine  reason: collision with other field name */
        public static final int f937search_list_engine = 2130903105;
        /* added by JADX */

        /* renamed from: searchdialog  reason: collision with other field name */
        public static final int f938searchdialog = 2130903106;
        /* added by JADX */

        /* renamed from: select  reason: collision with other field name */
        public static final int f939select = 2130903107;
        /* added by JADX */

        /* renamed from: single_choice_list_item  reason: collision with other field name */
        public static final int f940single_choice_list_item = 2130903108;
        /* added by JADX */

        /* renamed from: single_choice_list_item_no_checkmark  reason: collision with other field name */
        public static final int f941single_choice_list_item_no_checkmark = 2130903109;
        /* added by JADX */

        /* renamed from: title_container  reason: collision with other field name */
        public static final int f942title_container = 2130903110;
        /* added by JADX */

        /* renamed from: uc_traffic_save  reason: collision with other field name */
        public static final int f943uc_traffic_save = 2130903111;
        /* added by JADX */

        /* renamed from: uccontextmenu_item  reason: collision with other field name */
        public static final int f944uccontextmenu_item = 2130903112;
        /* added by JADX */

        /* renamed from: upload_font  reason: collision with other field name */
        public static final int f945upload_font = 2130903113;
        /* added by JADX */

        /* renamed from: urlitem  reason: collision with other field name */
        public static final int f946urlitem = 2130903114;
        /* added by JADX */

        /* renamed from: websitedialog  reason: collision with other field name */
        public static final int f947websitedialog = 2130903115;
        /* added by JADX */

        /* renamed from: workspace_screen  reason: collision with other field name */
        public static final int f948workspace_screen = 2130903116;
        /* added by JADX */

        /* renamed from: zoom_juc_controls  reason: collision with other field name */
        public static final int f949zoom_juc_controls = 2130903117;
    }

    public final class string {

        /* renamed from: aCY */
        public static final int page_loading = 2131296612;

        /* renamed from: aHA */
        public static final int add_sch_inputaddress = 2131296779;

        /* renamed from: aHB */
        public static final int add_sch_search = 2131296778;

        /* renamed from: aHC */
        public static final int add_to_bookmark = 2131296790;

        /* renamed from: aHD */
        public static final int add_to_bookmark_dialog_title = 2131296791;

        /* renamed from: aHE */
        public static final int add_to_navigation = 2131296789;

        /* renamed from: aHF */
        public static final int addbookmark = 2131296634;

        /* renamed from: aHG */
        public static final int addbookmarkdir = 2131296636;

        /* renamed from: aHH */
        public static final int addhistory = 2131296621;

        /* renamed from: aHI */
        public static final int address = 2131296259;

        /* renamed from: aHJ */
        public static final int addtobookmark = 2131296683;

        /* renamed from: aHK */
        public static final int alert_dialog_cancel = 2131296432;

        /* renamed from: aHL */
        public static final int alert_dialog_change_access_points = 2131296437;

        /* renamed from: aHM */
        public static final int alert_dialog_change_brightness = 2131296435;

        /* renamed from: aHN */
        public static final int alert_dialog_close = 2131296433;

        /* renamed from: aHO */
        public static final int alert_dialog_ok = 2131296430;

        /* renamed from: aHP */
        public static final int alert_dialog_open = 2131296431;

        /* renamed from: aHQ */
        public static final int alert_dialog_over = 2131296434;

        /* renamed from: aHR */
        public static final int alert_dialog_play = 2131296436;

        /* renamed from: aHS */
        public static final int alipay_is_donwload_title = 2131296839;

        /* renamed from: aHT */
        public static final int alipay_is_download = 2131296840;

        /* renamed from: aHU */
        public static final int alipay_no = 2131296842;

        /* renamed from: aHV */
        public static final int alipay_sdcard_no_space = 2131296837;

        /* renamed from: aHW */
        public static final int alipay_sdcard_not_exists = 2131296838;

        /* renamed from: aHX */
        public static final int alipay_yes = 2131296841;

        /* renamed from: aHY */
        public static final int allowlessthenfour = 2131296699;

        /* renamed from: aHZ */
        public static final int app_name = 2131296258;

        /* renamed from: aHi */
        public static final int action_add_shortcut = 2131296732;

        /* renamed from: aHj */
        public static final int activity_add_bookmark = 2131296314;

        /* renamed from: aHk */
        public static final int activity_add_mynavi = 2131296316;

        /* renamed from: aHl */
        public static final int activity_edit_mynavi = 2131296315;

        /* renamed from: aHm */
        public static final int activity_not_found = 2131296296;

        /* renamed from: aHn */
        public static final int activity_title_bookmark = 2131296304;

        /* renamed from: aHo */
        public static final int activity_title_download = 2131296311;

        /* renamed from: aHp */
        public static final int activity_title_history = 2131296307;

        /* renamed from: aHq */
        public static final int activity_title_histroy_favorite = 2131296308;

        /* renamed from: aHr */
        public static final int activity_title_inputurl = 2131296305;

        /* renamed from: aHs */
        public static final int activity_title_preference = 2131296309;

        /* renamed from: aHt */
        public static final int activity_title_search = 2131296306;

        /* renamed from: aHu */
        public static final int activity_title_selector = 2131296313;

        /* renamed from: aHv */
        public static final int activity_title_submaincontent = 2131296310;

        /* renamed from: aHw */
        public static final int activity_title_window = 2131296312;

        /* renamed from: aHx */
        public static final int add_cancle = 2131296774;

        /* renamed from: aHy */
        public static final int add_enter = 2131296775;

        /* renamed from: aHz */
        public static final int add_sch_default_searchengine = 2131296773;

        /* renamed from: aIA */
        public static final int camera_fail = 2131296755;

        /* renamed from: aIB */
        public static final int cannot_back = 2131296270;

        /* renamed from: aIC */
        public static final int cannotrefreshtimeragain = 2131296698;

        /* renamed from: aID */
        public static final int changed_to_landscape = 2131296797;

        /* renamed from: aIE */
        public static final int changed_to_portstrait = 2131296798;

        /* renamed from: aIF */
        public static final int checkupdateu = 2131296735;

        /* renamed from: aIG */
        public static final int choose_engine = 2131296298;

        /* renamed from: aIH */
        public static final int choose_mynavi_position = 2131296515;

        /* renamed from: aII */
        public static final int choose_mynavi_position_title = 2131296517;

        /* renamed from: aIJ */
        public static final int clear_all_browser_hostory = 2131296781;

        /* renamed from: aIK */
        public static final int clear_all_success = 2131296593;

        /* renamed from: aIL */
        public static final int clearhistory = 2131296620;

        /* renamed from: aIM */
        public static final int closeDownload = 2131296623;

        /* renamed from: aIN */
        public static final int close_refreshtimer_note = 2131296746;

        /* renamed from: aIO */
        public static final int closecurrentwindow = 2131296688;

        /* renamed from: aIP */
        public static final int confirm = 2131296271;

        /* renamed from: aIQ */
        public static final int confirmcoverbookmark = 2131296705;

        /* renamed from: aIR */
        public static final int confirmcoverbookmarkfile = 2131296707;

        /* renamed from: aIS */
        public static final int contextmenu_bookmark_addtonavi = 2131296406;

        /* renamed from: aIT */
        public static final int contextmenu_bookmark_delete = 2131296401;

        /* renamed from: aIU */
        public static final int contextmenu_bookmark_deleteall = 2131296404;

        /* renamed from: aIV */
        public static final int contextmenu_bookmark_deletefolder = 2131296402;

        /* renamed from: aIW */
        public static final int contextmenu_bookmark_edit = 2131296400;

        /* renamed from: aIX */
        public static final int contextmenu_bookmark_openinnewwindow = 2131296399;

        /* renamed from: aIY */
        public static final int contextmenu_bookmark_renamefolder = 2131296403;

        /* renamed from: aIZ */
        public static final int contextmenu_bookmark_sharepage = 2131296405;

        /* renamed from: aIa */
        public static final int app_name_dot = 2131296257;

        /* renamed from: aIb */
        public static final int avaliablespacenotenough = 2131296708;

        /* renamed from: aIc */
        public static final int bookmark = 2131296619;

        /* renamed from: aId */
        public static final int bookmark_cancle = 2131296771;

        /* renamed from: aIe */
        public static final int bookmark_file_choose_title = 2131296288;

        /* renamed from: aIf */
        public static final int bookmark_submit = 2131296770;

        /* renamed from: aIg */
        public static final int bookmark_sync_export = 2131296398;

        /* renamed from: aIh */
        public static final int bookmark_sync_import = 2131296397;

        /* renamed from: aIi */
        public static final int bookmark_sync_sync = 2131296396;

        /* renamed from: aIj */
        public static final int bookmark_title_backtoroot = 2131296299;

        /* renamed from: aIk */
        public static final int bookmarkdir = 2131296719;

        /* renamed from: aIl */
        public static final int bookmarkfileexists = 2131296706;

        /* renamed from: aIm */
        public static final int bookmarkfilenotnull = 2131296738;

        /* renamed from: aIn */
        public static final int bookmarkisexists = 2131296704;

        /* renamed from: aIo */
        public static final int bookmarknamenotnull = 2131296701;

        /* renamed from: aIp */
        public static final int bookmarkvalueotnull = 2131296702;

        /* renamed from: aIq */
        public static final int browserFileSystem = 2131296625;

        /* renamed from: aIr */
        public static final int browserFrame307Post = 2131296645;

        /* renamed from: aIs */
        public static final int browserFrameFormResubmitLabel = 2131296647;

        /* renamed from: aIt */
        public static final int browserFrameFormResubmitMessage = 2131296648;

        /* renamed from: aIu */
        public static final int browserFrameRedirect = 2131296644;

        /* renamed from: aIv */
        public static final int browser_dialog_page_attrs = 2131296662;

        /* renamed from: aIw */
        public static final int browser_name = 2131296803;

        /* renamed from: aIx */
        public static final int btn_cancle_text = 2131296762;

        /* renamed from: aIy */
        public static final int btn_submit_text = 2131296761;

        /* renamed from: aIz */
        public static final int buz_channel_load_failed = 2131296794;

        /* renamed from: aJA */
        public static final int contextmenu_mynavi_unsolidify = 2131296424;

        /* renamed from: aJB */
        public static final int contextmenu_openlink_background = 2131296671;

        /* renamed from: aJC */
        public static final int contextmenu_openlink_newwindow = 2131296670;

        /* renamed from: aJD */
        public static final int contextmenu_pageattrs = 2131296678;

        /* renamed from: aJE */
        public static final int contextmenu_pick_inputmethod = 2131296407;

        /* renamed from: aJF */
        public static final int contextmenu_quicksearch = 2131296679;

        /* renamed from: aJG */
        public static final int contextmenu_view_image = 2131296677;

        /* renamed from: aJH */
        public static final int contextmenu_window_closeother = 2131296494;

        /* renamed from: aJI */
        public static final int continueDonwloadTask = 2131296627;

        /* renamed from: aJJ */
        public static final int controlbar_back = 2131296324;

        /* renamed from: aJK */
        public static final int controlbar_backward = 2131296318;

        /* renamed from: aJL */
        public static final int controlbar_clear = 2131296330;

        /* renamed from: aJM */
        public static final int controlbar_closeothers = 2131296334;

        /* renamed from: aJN */
        public static final int controlbar_download_clear = 2131296641;

        /* renamed from: aJO */
        public static final int controlbar_finish = 2131296335;

        /* renamed from: aJP */
        public static final int controlbar_forward = 2131296320;

        /* renamed from: aJQ */
        public static final int controlbar_homepage = 2131296317;

        /* renamed from: aJR */
        public static final int controlbar_makedir = 2131296326;

        /* renamed from: aJS */
        public static final int controlbar_menu = 2131296323;

        /* renamed from: aJT */
        public static final int controlbar_newbookmark = 2131296331;

        /* renamed from: aJU */
        public static final int controlbar_newfolder = 2131296332;

        /* renamed from: aJV */
        public static final int controlbar_newwindow = 2131296322;

        /* renamed from: aJW */
        public static final int controlbar_ok = 2131296325;

        /* renamed from: aJX */
        public static final int controlbar_showtype = 2131296327;

        /* renamed from: aJY */
        public static final int controlbar_showtype_grid = 2131296328;

        /* renamed from: aJZ */
        public static final int controlbar_showtype_list = 2131296329;

        /* renamed from: aJa */
        public static final int contextmenu_bookmark_thislink = 2131296674;

        /* renamed from: aJb */
        public static final int contextmenu_closecurrentwindow = 2131296673;

        /* renamed from: aJc */
        public static final int contextmenu_createwindow = 2131296672;

        /* renamed from: aJd */
        public static final int contextmenu_download = 2131296675;

        /* renamed from: aJe */
        public static final int contextmenu_download_image = 2131296676;

        /* renamed from: aJf */
        public static final int contextmenu_file_attr = 2131296418;

        /* renamed from: aJg */
        public static final int contextmenu_file_copy = 2131296414;

        /* renamed from: aJh */
        public static final int contextmenu_file_cut = 2131296415;

        /* renamed from: aJi */
        public static final int contextmenu_file_delete = 2131296416;

        /* renamed from: aJj */
        public static final int contextmenu_file_paste = 2131296419;

        /* renamed from: aJk */
        public static final int contextmenu_file_rename = 2131296417;

        /* renamed from: aJl */
        public static final int contextmenu_history_addtonavi = 2131296429;

        /* renamed from: aJm */
        public static final int contextmenu_history_delete = 2131296428;

        /* renamed from: aJn */
        public static final int contextmenu_history_openinnewwindow = 2131296427;

        /* renamed from: aJo */
        public static final int contextmenu_input_copy = 2131296408;

        /* renamed from: aJp */
        public static final int contextmenu_input_cut = 2131296409;

        /* renamed from: aJq */
        public static final int contextmenu_input_paste = 2131296410;

        /* renamed from: aJr */
        public static final int contextmenu_input_select = 2131296412;

        /* renamed from: aJs */
        public static final int contextmenu_input_select_all = 2131296411;

        /* renamed from: aJt */
        public static final int contextmenu_input_select_cancle = 2131296413;

        /* renamed from: aJu */
        public static final int contextmenu_mynavi_add = 2131296422;

        /* renamed from: aJv */
        public static final int contextmenu_mynavi_delete = 2131296421;

        /* renamed from: aJw */
        public static final int contextmenu_mynavi_doubleline = 2131296426;

        /* renamed from: aJx */
        public static final int contextmenu_mynavi_edit = 2131296420;

        /* renamed from: aJy */
        public static final int contextmenu_mynavi_singleline = 2131296425;

        /* renamed from: aJz */
        public static final int contextmenu_mynavi_solidify = 2131296423;

        /* renamed from: aKA */
        public static final int dialog_msg_filecut_error = 2131296454;

        /* renamed from: aKB */
        public static final int dialog_msg_filename_cannot_null = 2131296452;

        /* renamed from: aKC */
        public static final int dialog_msg_filename_dulplicate = 2131296455;

        /* renamed from: aKD */
        public static final int dialog_msg_filepath = 2131296468;

        /* renamed from: aKE */
        public static final int dialog_msg_filesize = 2131296466;

        /* renamed from: aKF */
        public static final int dialog_msg_fit = 2131296492;

        /* renamed from: aKG */
        public static final int dialog_msg_folder_create_failure = 2131296456;

        /* renamed from: aKH */
        public static final int dialog_msg_is_sms_share_bookmark = 2131296502;

        /* renamed from: aKI */
        public static final int dialog_msg_is_sms_share_page = 2131296501;

        /* renamed from: aKJ */
        public static final int dialog_msg_lastmodify = 2131296467;

        /* renamed from: aKK */
        public static final int dialog_msg_nightmode = 2131296489;

        /* renamed from: aKL */
        public static final int dialog_msg_novelmode = 2131296484;

        /* renamed from: aKM */
        public static final int dialog_msg_novelmode_zoom = 2131296485;

        /* renamed from: aKN */
        public static final int dialog_msg_pageupdown_zoom = 2131296487;

        /* renamed from: aKO */
        public static final int dialog_msg_popup_anim_off = 2131296505;

        /* renamed from: aKP */
        public static final int dialog_msg_quit = 2131296449;

        /* renamed from: aKQ */
        public static final int dialog_msg_rename_failure = 2131296461;

        /* renamed from: aKR */
        public static final int dialog_msg_replace_file = 2131296470;

        /* renamed from: aKS */
        public static final int dialog_msg_sdcard_no_space = 2131296459;

        /* renamed from: aKT */
        public static final int dialog_msg_should_download = 2131296496;

        /* renamed from: aKU */
        public static final int dialog_msg_tips_not_qvga = 2131296483;

        /* renamed from: aKV */
        public static final int dialog_msg_tips_qvga = 2131296482;

        /* renamed from: aKW */
        public static final int dialog_msg_tipszoom = 2131296493;

        /* renamed from: aKX */
        public static final int dialog_msg_waiting_copy = 2131296458;

        /* renamed from: aKY */
        public static final int dialog_msg_zoom = 2131296491;

        /* renamed from: aKZ */
        public static final int dialog_pad_tip = 2131296507;

        /* renamed from: aKa */
        public static final int controlbar_stop = 2131296319;

        /* renamed from: aKb */
        public static final int controlbar_sync = 2131296333;

        /* renamed from: aKc */
        public static final int controlbar_window = 2131296321;

        /* renamed from: aKd */
        public static final int coversamename = 2131296715;

        /* renamed from: aKe */
        public static final int declaretion = 2131296743;

        /* renamed from: aKf */
        public static final int default_language = 2131296802;

        /* renamed from: aKg */
        public static final int default_release = 2131296801;

        /* renamed from: aKh */
        public static final int delAllDownloadTask = 2131296630;

        /* renamed from: aKi */
        public static final int delDownloadtask = 2131296631;

        /* renamed from: aKj */
        public static final int delFocuseDownloadTask = 2131296629;

        /* renamed from: aKk */
        public static final int del_bookmark_success = 2131296450;

        /* renamed from: aKl */
        public static final int delallbookmark = 2131296640;

        /* renamed from: aKm */
        public static final int delbookmark = 2131296638;

        /* renamed from: aKn */
        public static final int delbookmarkdir = 2131296639;

        /* renamed from: aKo */
        public static final int dialog_button_popup_anim_off = 2131296506;

        /* renamed from: aKp */
        public static final int dialog_help_slide_msg = 2131296508;

        /* renamed from: aKq */
        public static final int dialog_msg_closeother = 2131296495;

        /* renamed from: aKr */
        public static final int dialog_msg_createshortcut = 2131296481;

        /* renamed from: aKs */
        public static final int dialog_msg_deleteall = 2131296475;

        /* renamed from: aKt */
        public static final int dialog_msg_deleteall_bookmark = 2131296473;

        /* renamed from: aKu */
        public static final int dialog_msg_deleteall_history = 2131296474;

        /* renamed from: aKv */
        public static final int dialog_msg_deletebookmark = 2131296471;

        /* renamed from: aKw */
        public static final int dialog_msg_deletefile = 2131296463;

        /* renamed from: aKx */
        public static final int dialog_msg_deletefolder = 2131296472;

        /* renamed from: aKy */
        public static final int dialog_msg_download_task_unbreakable = 2131296653;

        /* renamed from: aKz */
        public static final int dialog_msg_filecopy_error = 2131296453;

        /* renamed from: aLA */
        public static final int dialog_title_please_select = 2131296498;

        /* renamed from: aLB */
        public static final int dialog_title_popup_anim_off = 2131296504;

        /* renamed from: aLC */
        public static final int dialog_title_quit = 2131296448;

        /* renamed from: aLD */
        public static final int dialog_title_rename = 2131296464;

        /* renamed from: aLE */
        public static final int dialog_title_renamefolder = 2131296444;

        /* renamed from: aLF */
        public static final int dialog_title_replace_file = 2131296469;

        /* renamed from: aLG */
        public static final int dialog_title_sharepage = 2131296500;

        /* renamed from: aLH */
        public static final int dialog_title_upload = 2131296497;

        /* renamed from: aLI */
        public static final int dialog_title_waiting_copy = 2131296457;

        /* renamed from: aLJ */
        public static final int dir_choose_title = 2131296285;

        /* renamed from: aLK */
        public static final int dir_root = 2131296772;

        /* renamed from: aLL */
        public static final int dirisexists = 2131296703;

        /* renamed from: aLM */
        public static final int dirnamenotnull = 2131296700;

        /* renamed from: aLN */
        public static final int disclaimer = 2131296739;

        /* renamed from: aLO */
        public static final int disclaimeraccept = 2131296740;

        /* renamed from: aLP */
        public static final int disclaimerdeny = 2131296741;

        /* renamed from: aLQ */
        public static final int dlg_button_autosave = 2131296576;

        /* renamed from: aLR */
        public static final int dlg_button_cancle = 2131296574;

        /* renamed from: aLS */
        public static final int dlg_button_neversave = 2131296579;

        /* renamed from: aLT */
        public static final int dlg_button_notsave = 2131296577;

        /* renamed from: aLU */
        public static final int dlg_button_ok = 2131296573;

        /* renamed from: aLV */
        public static final int dlg_button_replace = 2131296578;

        /* renamed from: aLW */
        public static final int dlg_button_save = 2131296575;

        /* renamed from: aLX */
        public static final int dont_tips_again = 2131296782;

        /* renamed from: aLY */
        public static final int downLoad = 2131296649;

        /* renamed from: aLZ */
        public static final int downloadSafeComment = 2131296643;

        /* renamed from: aLa */
        public static final int dialog_pageattr_address = 2131296666;

        /* renamed from: aLb */
        public static final int dialog_pageattr_copy = 2131296665;

        /* renamed from: aLc */
        public static final int dialog_pageattr_href_url = 2131296667;

        /* renamed from: aLd */
        public static final int dialog_pageattr_image_url = 2131296668;

        /* renamed from: aLe */
        public static final int dialog_pageattr_msg_copy = 2131296669;

        /* renamed from: aLf */
        public static final int dialog_pageattr_title = 2131296664;

        /* renamed from: aLg */
        public static final int dialog_play_not_support_rtsp = 2131296510;

        /* renamed from: aLh */
        public static final int dialog_rtsp_not_support = 2131296509;

        /* renamed from: aLi */
        public static final int dialog_title_bookmark_sharepage = 2131296499;

        /* renamed from: aLj */
        public static final int dialog_title_browse_model = 2131296503;

        /* renamed from: aLk */
        public static final int dialog_title_clear = 2131296442;

        /* renamed from: aLl */
        public static final int dialog_title_createshortcut = 2131296446;

        /* renamed from: aLm */
        public static final int dialog_title_deleteall = 2131296445;

        /* renamed from: aLn */
        public static final int dialog_title_deletebookmark = 2131296441;

        /* renamed from: aLo */
        public static final int dialog_title_deletefile = 2131296462;

        /* renamed from: aLp */
        public static final int dialog_title_deletefolder = 2131296443;

        /* renamed from: aLq */
        public static final int dialog_title_download_task_unbreakable = 2131296652;

        /* renamed from: aLr */
        public static final int dialog_title_editbookmark = 2131296439;

        /* renamed from: aLs */
        public static final int dialog_title_fileattr = 2131296465;

        /* renamed from: aLt */
        public static final int dialog_title_makedir = 2131296451;

        /* renamed from: aLu */
        public static final int dialog_title_newbookmark = 2131296438;

        /* renamed from: aLv */
        public static final int dialog_title_newfolder = 2131296440;

        /* renamed from: aLw */
        public static final int dialog_title_nightmode = 2131296488;

        /* renamed from: aLx */
        public static final int dialog_title_nightmode_brightness = 2131296490;

        /* renamed from: aLy */
        public static final int dialog_title_novelmode = 2131296447;

        /* renamed from: aLz */
        public static final int dialog_title_pageupdown = 2131296486;

        /* renamed from: aMA */
        public static final int freecopy_menu_copy = 2131296843;

        /* renamed from: aMB */
        public static final int freecopy_menu_search = 2131296844;

        /* renamed from: aMC */
        public static final int freecopy_menu_share = 2131296845;

        /* renamed from: aMD */
        public static final int fresh_us_data_complete = 2131296834;

        /* renamed from: aME */
        public static final int fresh_us_data_dlg_msg = 2131296832;

        /* renamed from: aMF */
        public static final int fresh_us_data_dlg_title = 2131296831;

        /* renamed from: aMG */
        public static final int fresh_us_data_error = 2131296836;

        /* renamed from: aMH */
        public static final int fresh_us_data_ongoing = 2131296833;

        /* renamed from: aMI */
        public static final int fs_fordward_backward_tips = 2131296511;

        /* renamed from: aMJ */
        public static final int gonow = 2131296278;

        /* renamed from: aMK */
        public static final int gotourl = 2131296274;

        /* renamed from: aML */
        public static final int hello = 2131296256;

        /* renamed from: aMM */
        public static final int history = 2131296618;

        /* renamed from: aMN */
        public static final int ignoresamename = 2131296716;

        /* renamed from: aMO */
        public static final int import_bookmark = 2131296709;

        /* renamed from: aMP */
        public static final int importbookmarkfail = 2131296713;

        /* renamed from: aMQ */
        public static final int importbookmarksuccess = 2131296711;

        /* renamed from: aMR */
        public static final int input_newname = 2131296750;

        /* renamed from: aMS */
        public static final int install_dialog_title = 2131296654;

        /* renamed from: aMT */
        public static final int install_dlg_msg_finished = 2131296655;

        /* renamed from: aMU */
        public static final int install_dlg_msg_install = 2131296658;

        /* renamed from: aMV */
        public static final int install_dlg_msg_install_risk = 2131296660;

        /* renamed from: aMW */
        public static final int install_dlg_msg_install_safe = 2131296659;

        /* renamed from: aMX */
        public static final int install_dlg_msg_install_unknow = 2131296661;

        /* renamed from: aMY */
        public static final int install_dlg_msg_open = 2131296657;

        /* renamed from: aMZ */
        public static final int install_dlg_toast_finished = 2131296656;

        /* renamed from: aMa */
        public static final int downloadSafeScan = 2131296642;

        /* renamed from: aMb */
        public static final int download_app_changed_note = 2131296546;

        /* renamed from: aMc */
        public static final int download_filename = 2131296651;

        /* renamed from: aMd */
        public static final int editbookmark = 2131296635;

        /* renamed from: aMe */
        public static final int editbookmarkdir = 2131296637;

        /* renamed from: aMf */
        public static final int emptydata = 2131296275;

        /* renamed from: aMg */
        public static final int emptytitle = 2131296276;

        /* renamed from: aMh */
        public static final int exit = 2131296622;

        /* renamed from: aMi */
        public static final int export_bookmark = 2131296710;

        /* renamed from: aMj */
        public static final int exportbookmarkfail = 2131296714;

        /* renamed from: aMk */
        public static final int exportbookmarksuccess = 2131296712;

        /* renamed from: aMl */
        public static final int file_already_root = 2131296281;

        /* renamed from: aMm */
        public static final int file_choose_file_null = 2131296282;

        /* renamed from: aMn */
        public static final int file_choose_title = 2131296283;

        /* renamed from: aMo */
        public static final int file_no_sdcard_warning = 2131296280;

        /* renamed from: aMp */
        public static final int file_not_found = 2131296829;

        /* renamed from: aMq */
        public static final int file_notin_sdcard = 2131296760;

        /* renamed from: aMr */
        public static final int file_opera_failed = 2131296284;

        /* renamed from: aMs */
        public static final int file_saved_as = 2131296650;

        /* renamed from: aMt */
        public static final int file_upload_supportless_type = 2131296754;

        /* renamed from: aMu */
        public static final int file_upload_too_large = 2131296753;

        /* renamed from: aMv */
        public static final int filenamenotnull = 2131296752;

        /* renamed from: aMw */
        public static final int fileupload = 2131296736;

        /* renamed from: aMx */
        public static final int flash_file_choose_title = 2131296286;

        /* renamed from: aMy */
        public static final int folder = 2131296293;

        /* renamed from: aMz */
        public static final int foldername = 2131296294;

        /* renamed from: aNA */
        public static final int menu_nonopic = 2131296392;

        /* renamed from: aNB */
        public static final int menu_nopic = 2131296391;

        /* renamed from: aNC */
        public static final int menu_normalmode = 2131296365;

        /* renamed from: aND */
        public static final int menu_novel_mode = 2131296373;

        /* renamed from: aNE */
        public static final int menu_outfullscreen = 2131296357;

        /* renamed from: aNF */
        public static final int menu_page_updown = 2131296372;

        /* renamed from: aNG */
        public static final int menu_pageattrs = 2131296383;

        /* renamed from: aNH */
        public static final int menu_pagefind = 2131296381;

        /* renamed from: aNI */
        public static final int menu_passoprt = 2131296394;

        /* renamed from: aNJ */
        public static final int menu_porttrait = 2131296369;

        /* renamed from: aNK */
        public static final int menu_porttrait_auto = 2131296370;

        /* renamed from: aNL */
        public static final int menu_quit = 2131296361;

        /* renamed from: aNM */
        public static final int menu_quit2 = 2131296354;

        /* renamed from: aNN */
        public static final int menu_recommendfriend = 2131296378;

        /* renamed from: aNO */
        public static final int menu_refresh = 2131296355;

        /* renamed from: aNP */
        public static final int menu_refreshtimer = 2131296379;

        /* renamed from: aNQ */
        public static final int menu_report = 2131296389;

        /* renamed from: aNR */
        public static final int menu_reportwebsite = 2131296388;

        /* renamed from: aNS */
        public static final int menu_return = 2131296385;

        /* renamed from: aNT */
        public static final int menu_search = 2131296350;

        /* renamed from: aNU */
        public static final int menu_select_copy = 2131296353;

        /* renamed from: aNV */
        public static final int menu_selectword = 2131296351;

        /* renamed from: aNW */
        public static final int menu_sharepage = 2131296387;

        /* renamed from: aNX */
        public static final int menu_skinmanager = 2131296390;

        /* renamed from: aNY */
        public static final int menu_stoprefreshtimer = 2131296380;

        /* renamed from: aNZ */
        public static final int menu_syssettings = 2131296360;

        /* renamed from: aNa */
        public static final int invalid_name = 2131296748;

        /* renamed from: aNb */
        public static final int juc = 2131296804;

        /* renamed from: aNc */
        public static final int juc_ua_header = 2131296799;

        /* renamed from: aNd */
        public static final int loadurl = 2131296616;

        /* renamed from: aNe */
        public static final int maincontent_inputurl = 2131296290;

        /* renamed from: aNf */
        public static final int maincontent_search = 2131296289;

        /* renamed from: aNg */
        public static final int maxwindownumber = 2131296720;

        /* renamed from: aNh */
        public static final int menu_about = 2131296377;

        /* renamed from: aNi */
        public static final int menu_addbookmark = 2131296363;

        /* renamed from: aNj */
        public static final int menu_bookmark = 2131296362;

        /* renamed from: aNk */
        public static final int menu_browsemode = 2131296364;

        /* renamed from: aNl */
        public static final int menu_checknetwork = 2131296375;

        /* renamed from: aNm */
        public static final int menu_checkupdate = 2131296374;

        /* renamed from: aNn */
        public static final int menu_clearmainpagedata = 2131296384;

        /* renamed from: aNo */
        public static final int menu_day = 2131296359;

        /* renamed from: aNp */
        public static final int menu_debug = 2131296367;

        /* renamed from: aNq */
        public static final int menu_downloadmanager = 2131296352;

        /* renamed from: aNr */
        public static final int menu_filemanager = 2131296382;

        /* renamed from: aNs */
        public static final int menu_fullscreen = 2131296356;

        /* renamed from: aNt */
        public static final int menu_help = 2131296376;

        /* renamed from: aNu */
        public static final int menu_history_bookmark = 2131296348;

        /* renamed from: aNv */
        public static final int menu_inputurl = 2131296349;

        /* renamed from: aNw */
        public static final int menu_land_state = 2131296371;

        /* renamed from: aNx */
        public static final int menu_landscape = 2131296368;

        /* renamed from: aNy */
        public static final int menu_more = 2131296366;

        /* renamed from: aNz */
        public static final int menu_nightmode = 2131296358;

        /* renamed from: aOA */
        public static final int navi_not_allow_null = 2131296785;

        /* renamed from: aOB */
        public static final int need_rename = 2131296747;

        /* renamed from: aOC */
        public static final int network_check_guid = 2131296796;

        /* renamed from: aOD */
        public static final int network_error = 2131296793;

        /* renamed from: aOE */
        public static final int newDownloadTask = 2131296624;

        /* renamed from: aOF */
        public static final int nolongerprompt = 2131296742;

        /* renamed from: aOG */
        public static final int notchangetheme = 2131296696;

        /* renamed from: aOH */
        public static final int notsupportrefreshtimer = 2131296690;

        /* renamed from: aOI */
        public static final int notsupportrefreshtimerHomePage = 2131296692;

        /* renamed from: aOJ */
        public static final int notsupportrefreshtimerZip = 2131296693;

        /* renamed from: aOK */
        public static final int notsupportrefreshtimerzoom = 2131296691;

        /* renamed from: aOL */
        public static final int null_mynavi_title = 2131296519;

        /* renamed from: aOM */
        public static final int open_source_error = 2131296828;

        /* renamed from: aON */
        public static final int open_throw_exception = 2131296297;

        /* renamed from: aOO */
        public static final int openinnewwindow = 2131296687;

        /* renamed from: aOP */
        public static final int opennewwindow = 2131296686;

        /* renamed from: aOQ */
        public static final int openwave = 2131296805;

        /* renamed from: aOR */
        public static final int opera = 2131296806;

        /* renamed from: aOS */
        public static final int out_of_memory = 2131296721;

        /* renamed from: aOT */
        public static final int page_loading_page = 2131296614;

        /* renamed from: aOU */
        public static final int page_safe_loading_page = 2131296613;

        /* renamed from: aOV */
        public static final int page_waiting = 2131296722;

        /* renamed from: aOW */
        public static final int page_waiting_failure = 2131296723;

        /* renamed from: aOX */
        public static final int pageattrs = 2131296684;

        /* renamed from: aOY */
        public static final int penselectmode = 2131296744;

        /* renamed from: aOZ */
        public static final int pictureoriginview = 2131296682;

        /* renamed from: aOa */
        public static final int menu_tab_favorite = 2131296345;

        /* renamed from: aOb */
        public static final int menu_tab_setting = 2131296346;

        /* renamed from: aOc */
        public static final int menu_tab_tools = 2131296347;

        /* renamed from: aOd */
        public static final int menu_test = 2131296386;

        /* renamed from: aOe */
        public static final int menu_uctraffic = 2131296395;

        /* renamed from: aOf */
        public static final int midlet_name = 2131296615;

        /* renamed from: aOg */
        public static final int mime_apk = 2131296727;

        /* renamed from: aOh */
        public static final int mime_audio = 2131296728;

        /* renamed from: aOi */
        public static final int mime_image = 2131296729;

        /* renamed from: aOj */
        public static final int mime_text = 2131296731;

        /* renamed from: aOk */
        public static final int mime_video = 2131296730;

        /* renamed from: aOl */
        public static final int mozilla = 2131296807;

        /* renamed from: aOm */
        public static final int msg_add_dir_success = 2131296477;

        /* renamed from: aOn */
        public static final int msg_add_fail = 2131296478;

        /* renamed from: aOo */
        public static final int msg_add_success = 2131296476;

        /* renamed from: aOp */
        public static final int msg_edit_fail = 2131296479;

        /* renamed from: aOq */
        public static final int msg_edit_success = 2131296480;

        /* renamed from: aOr */
        public static final int msg_no_position = 2131296518;

        /* renamed from: aOs */
        public static final int mynavi_fixed = 2131296516;

        /* renamed from: aOt */
        public static final int mynavi_message_show = 2131296788;

        /* renamed from: aOu */
        public static final int name_exist = 2131296749;

        /* renamed from: aOv */
        public static final int navi_already_exist = 2131296784;

        /* renamed from: aOw */
        public static final int navi_dialog_delete_msg = 2131296787;

        /* renamed from: aOx */
        public static final int navi_dialog_delete_title = 2131296786;

        /* renamed from: aOy */
        public static final int navi_full = 2131296792;

        /* renamed from: aOz */
        public static final int navi_load_failed = 2131296795;

        /* renamed from: aPA */
        public static final int pref_item_title_clear_cookie = 2131296584;

        /* renamed from: aPB */
        public static final int pref_item_title_clear_form = 2131296587;

        /* renamed from: aPC */
        public static final int pref_item_title_clear_history = 2131296589;

        /* renamed from: aPD */
        public static final int pref_item_title_clear_psw = 2131296571;

        /* renamed from: aPE */
        public static final int pref_item_title_download_app = 2131296545;

        /* renamed from: aPF */
        public static final int pref_item_title_download_finish_tips = 2131296544;

        /* renamed from: aPG */
        public static final int pref_item_title_download_in_background = 2131296543;

        /* renamed from: aPH */
        public static final int pref_item_title_download_path = 2131296540;

        /* renamed from: aPI */
        public static final int pref_item_title_download_posthint = 2131296605;

        /* renamed from: aPJ */
        public static final int pref_item_title_download_prehint = 2131296604;

        /* renamed from: aPK */
        public static final int pref_item_title_download_subsection = 2131296539;

        /* renamed from: aPL */
        public static final int pref_item_title_enable_sound = 2131296560;

        /* renamed from: aPM */
        public static final int pref_item_title_enablejs = 2131296567;

        /* renamed from: aPN */
        public static final int pref_item_title_encode = 2131296523;

        /* renamed from: aPO */
        public static final int pref_item_title_fit_screen = 2131296569;

        /* renamed from: aPP */
        public static final int pref_item_title_folding_mode = 2131296528;

        /* renamed from: aPQ */
        public static final int pref_item_title_history_record = 2131296606;

        /* renamed from: aPR */
        public static final int pref_item_title_homepage = 2131296530;

        /* renamed from: aPS */
        public static final int pref_item_title_install_flash = 2131296601;

        /* renamed from: aPT */
        public static final int pref_item_title_java_zoom = 2131296554;

        /* renamed from: aPU */
        public static final int pref_item_title_load_picture = 2131296524;

        /* renamed from: aPV */
        public static final int pref_item_title_most_visit_record = 2131296607;

        /* renamed from: aPW */
        public static final int pref_item_title_mynavi_lines = 2131296561;

        /* renamed from: aPX */
        public static final int pref_item_title_need_popup_anim = 2131296562;

        /* renamed from: aPY */
        public static final int pref_item_title_night_mode_brightness = 2131296555;

        /* renamed from: aPZ */
        public static final int pref_item_title_novel_mode = 2131296558;

        /* renamed from: aPa */
        public static final int pictureview = 2131296681;

        /* renamed from: aPb */
        public static final int pleaseinputintervaltimer = 2131296694;

        /* renamed from: aPc */
        public static final int plugin_install_error_incorrect_format = 2131296766;

        /* renamed from: aPd */
        public static final int plugin_install_error_need_restart = 2131296768;

        /* renamed from: aPe */
        public static final int plugin_install_error_no_space = 2131296767;

        /* renamed from: aPf */
        public static final int plugin_install_query = 2131296769;

        /* renamed from: aPg */
        public static final int plugin_install_success = 2131296765;

        /* renamed from: aPh */
        public static final int plugin_install_title = 2131296763;

        /* renamed from: aPi */
        public static final int plugin_install_waiting = 2131296764;

        /* renamed from: aPj */
        public static final int pref_item_clear_input_history = 2131296594;

        /* renamed from: aPk */
        public static final int pref_item_clear_record = 2131296591;

        /* renamed from: aPl */
        public static final int pref_item_clear_record_tip = 2131296592;

        /* renamed from: aPm */
        public static final int pref_item_clear_visit_history = 2131296595;

        /* renamed from: aPn */
        public static final int pref_item_msg_clear_cache = 2131296582;

        /* renamed from: aPo */
        public static final int pref_item_msg_clear_cookie = 2131296585;

        /* renamed from: aPp */
        public static final int pref_item_msg_clear_form = 2131296588;

        /* renamed from: aPq */
        public static final int pref_item_msg_clear_history = 2131296590;

        /* renamed from: aPr */
        public static final int pref_item_msg_clear_psw = 2131296572;

        /* renamed from: aPs */
        public static final int pref_item_msg_reset = 2131296598;

        /* renamed from: aPt */
        public static final int pref_item_subtitle_wifi_mode = 2131296537;

        /* renamed from: aPu */
        public static final int pref_item_summary_night_mode_brightness = 2131296556;

        /* renamed from: aPv */
        public static final int pref_item_title_accept_cookie = 2131296583;

        /* renamed from: aPw */
        public static final int pref_item_title_auto_landscape = 2131296529;

        /* renamed from: aPx */
        public static final int pref_item_title_block_popup_window = 2131296568;

        /* renamed from: aPy */
        public static final int pref_item_title_browse_model = 2131296548;

        /* renamed from: aPz */
        public static final int pref_item_title_clear_cache = 2131296581;

        /* renamed from: aQA */
        public static final int pref_item_title_zoom_mode = 2131296566;

        /* renamed from: aQB */
        public static final int pref_item_title_zoom_settings = 2131296526;

        /* renamed from: aQC */
        public static final int pref_title_browser = 2131296547;

        /* renamed from: aQD */
        public static final int pref_title_clear = 2131296580;

        /* renamed from: aQE */
        public static final int pref_title_display = 2131296520;

        /* renamed from: aQF */
        public static final int pref_title_download = 2131296538;

        /* renamed from: aQG */
        public static final int pref_title_flash = 2131296600;

        /* renamed from: aQH */
        public static final int pref_title_network = 2131296533;

        /* renamed from: aQI */
        public static final int pref_title_preread = 2131296608;

        /* renamed from: aQJ */
        public static final int pref_title_reset = 2131296596;

        /* renamed from: aQK */
        public static final int pref_title_security_settings = 2131296602;

        /* renamed from: aQL */
        public static final int pref_title_system = 2131296611;

        /* renamed from: aQM */
        public static final int pref_title_zoom_settings = 2131296564;

        /* renamed from: aQN */
        public static final int prefix_url_address = 2131296512;

        /* renamed from: aQO */
        public static final int preread_note = 2131296830;

        /* renamed from: aQP */
        public static final int quit_dlg_title = 2131296724;

        /* renamed from: aQQ */
        public static final int quit_note_download = 2131296725;

        /* renamed from: aQR */
        public static final int quit_tip = 2131296780;

        /* renamed from: aQS */
        public static final int readingmode_dlg_record = 2131296863;

        /* renamed from: aQT */
        public static final int readingmode_dlg_record_dis = 2131296864;

        /* renamed from: aQU */
        public static final int readingmode_dlg_smooth = 2131296861;

        /* renamed from: aQV */
        public static final int readingmode_dlg_smooth_dis = 2131296862;

        /* renamed from: aQW */
        public static final int readingmode_dlg_title = 2131296860;

        /* renamed from: aQX */
        public static final int readingmode_tail_caching = 2131296336;

        /* renamed from: aQY */
        public static final int readingmode_tail_end = 2131296339;

        /* renamed from: aQZ */
        public static final int readingmode_tail_fail = 2131296338;

        /* renamed from: aQa */
        public static final int pref_item_title_page_up_down_location = 2131296557;

        /* renamed from: aQb */
        public static final int pref_item_title_picture_quality = 2131296525;

        /* renamed from: aQc */
        public static final int pref_item_title_popupinfo = 2131296550;

        /* renamed from: aQd */
        public static final int pref_item_title_reset = 2131296597;

        /* renamed from: aQe */
        public static final int pref_item_title_save_and_exit = 2131296599;

        /* renamed from: aQf */
        public static final int pref_item_title_save_form = 2131296586;

        /* renamed from: aQg */
        public static final int pref_item_title_save_psw = 2131296570;

        /* renamed from: aQh */
        public static final int pref_item_title_save_psw_juc = 2131296559;

        /* renamed from: aQi */
        public static final int pref_item_title_scroll = 2131296551;

        /* renamed from: aQj */
        public static final int pref_item_title_set_default = 2131296553;

        /* renamed from: aQk */
        public static final int pref_item_title_show_avatar = 2131296532;

        /* renamed from: aQl */
        public static final int pref_item_title_taskcount = 2131296542;

        /* renamed from: aQm */
        public static final int pref_item_title_taskrename = 2131296541;

        /* renamed from: aQn */
        public static final int pref_item_title_textsize = 2131296521;

        /* renamed from: aQo */
        public static final int pref_item_title_theme = 2131296527;

        /* renamed from: aQp */
        public static final int pref_item_title_tips_exit = 2131296563;

        /* renamed from: aQq */
        public static final int pref_item_title_titlebar_fullscreen = 2131296531;

        /* renamed from: aQr */
        public static final int pref_item_title_use_proxy = 2131296535;

        /* renamed from: aQs */
        public static final int pref_item_title_usepreread = 2131296549;

        /* renamed from: aQt */
        public static final int pref_item_title_useragent = 2131296534;

        /* renamed from: aQu */
        public static final int pref_item_title_wap = 2131296552;

        /* renamed from: aQv */
        public static final int pref_item_title_wap_preread = 2131296609;

        /* renamed from: aQw */
        public static final int pref_item_title_website_hint = 2131296603;

        /* renamed from: aQx */
        public static final int pref_item_title_wifi_mode = 2131296536;

        /* renamed from: aQy */
        public static final int pref_item_title_word_subsection = 2131296522;

        /* renamed from: aQz */
        public static final int pref_item_title_www_preread = 2131296610;

        /* renamed from: aRA */
        public static final int save_source_to = 2131296827;

        /* renamed from: aRB */
        public static final int save_source_webkit_info = 2131296825;

        /* renamed from: aRC */
        public static final int savecurrentlink = 2131296680;

        /* renamed from: aRD */
        public static final int scan_contact_tip = 2131296759;

        /* renamed from: aRE */
        public static final int sch_cancel = 2131296777;

        /* renamed from: aRF */
        public static final int sch_search = 2131296776;

        /* renamed from: aRG */
        public static final int search = 2131296273;

        /* renamed from: aRH */
        public static final int share_fail = 2131296267;

        /* renamed from: aRI */
        public static final int share_image = 2131296266;

        /* renamed from: aRJ */
        public static final int share_image_fail = 2131296268;

        /* renamed from: aRK */
        public static final int share_image_not_yet = 2131296269;

        /* renamed from: aRL */
        public static final int share_link = 2131296264;

        /* renamed from: aRM */
        public static final int share_page = 2131296265;

        /* renamed from: aRN */
        public static final int share_suffix = 2131296263;

        /* renamed from: aRO */
        public static final int share_title = 2131296262;

        /* renamed from: aRP */
        public static final int sharepage_tips_content = 2131296756;

        /* renamed from: aRQ */
        public static final int sharepage_update_failure = 2131296758;

        /* renamed from: aRR */
        public static final int sharepage_update_tips = 2131296757;

        /* renamed from: aRS */
        public static final int shortcutsearch = 2131296685;

        /* renamed from: aRT */
        public static final int showDownloadTaskMessage = 2131296632;

        /* renamed from: aRU */
        public static final int showRenameDlg = 2131296633;

        /* renamed from: aRV */
        public static final int skin_file_choose_title = 2131296287;

        /* renamed from: aRW */
        public static final int stopDownloadTask = 2131296626;

        /* renamed from: aRX */
        public static final int tap_again_to_exit = 2131296783;

        /* renamed from: aRY */
        public static final int tcardnotusable = 2131296697;

        /* renamed from: aRZ */
        public static final int text_copied = 2131296745;

        /* renamed from: aRa */
        public static final int readingmode_tail_pause = 2131296337;

        /* renamed from: aRb */
        public static final int readingmode_tips_enter = 2131296340;

        /* renamed from: aRc */
        public static final int readingmode_tips_enter_button = 2131296343;

        /* renamed from: aRd */
        public static final int readingmode_tips_quit = 2131296341;

        /* renamed from: aRe */
        public static final int readingmode_tips_recommand_open_pageud = 2131296344;

        /* renamed from: aRf */
        public static final int readingmode_tips_saved = 2131296342;

        /* renamed from: aRg */
        public static final int refreshprompt = 2131296695;

        /* renamed from: aRh */
        public static final int reinput_note = 2131296751;

        /* renamed from: aRi */
        public static final int restartDonwloadTask = 2131296628;

        /* renamed from: aRj */
        public static final int save_image_dialog_title = 2131296817;

        /* renamed from: aRk */
        public static final int save_image_error = 2131296816;

        /* renamed from: aRl */
        public static final int save_image_file_exist = 2131296811;

        /* renamed from: aRm */
        public static final int save_image_file_name_error = 2131296812;

        /* renamed from: aRn */
        public static final int save_image_file_name_toolong = 2131296813;

        /* renamed from: aRo */
        public static final int save_image_filename_empty = 2131296810;

        /* renamed from: aRp */
        public static final int save_image_input_file_name = 2131296814;

        /* renamed from: aRq */
        public static final int save_image_sdcard_error = 2131296809;

        /* renamed from: aRr */
        public static final int save_image_to = 2131296815;

        /* renamed from: aRs */
        public static final int save_source = 2131296824;

        /* renamed from: aRt */
        public static final int save_source_file_exist = 2131296820;

        /* renamed from: aRu */
        public static final int save_source_file_name_error = 2131296821;

        /* renamed from: aRv */
        public static final int save_source_file_name_toolong = 2131296822;

        /* renamed from: aRw */
        public static final int save_source_filename_empty = 2131296819;

        /* renamed from: aRx */
        public static final int save_source_input_file_name = 2131296823;

        /* renamed from: aRy */
        public static final int save_source_loading_info = 2131296826;

        /* renamed from: aRz */
        public static final int save_source_sdcard_error = 2131296818;

        /* renamed from: aSA */
        public static final int upload_info_format = 2131296737;

        /* renamed from: aSB */
        public static final int upmsg = 2131296734;

        /* renamed from: aSC */
        public static final int uptitle = 2131296733;

        /* renamed from: aSD */
        public static final int web_user_agent = 2131296726;

        /* renamed from: aSE */
        public static final int webkit_tail = 2131296808;

        /* renamed from: aSF */
        public static final int webkit_ua_header = 2131296800;

        /* renamed from: aSG */
        public static final int zoom_mode = 2131296565;

        /* renamed from: aSa */
        public static final int text_max_size = 2131296460;

        /* renamed from: aSb */
        public static final int tip = 2131296277;

        /* renamed from: aSc */
        public static final int toast_enter_passport = 2131296279;

        /* renamed from: aSd */
        public static final int toast_net = 2131296301;

        /* renamed from: aSe */
        public static final int toast_nopic = 2131296393;

        /* renamed from: aSf */
        public static final int toast_stopping = 2131296689;

        /* renamed from: aSg */
        public static final int toast_wifi = 2131296300;

        /* renamed from: aSh */
        public static final int toast_wifi_optimist = 2131296302;

        /* renamed from: aSi */
        public static final int toast_wifi_pause_dl = 2131296303;

        /* renamed from: aSj */
        public static final int uc_initial_tip1 = 2131296853;

        /* renamed from: aSk */
        public static final int uc_initial_tip2 = 2131296854;

        /* renamed from: aSl */
        public static final int uc_initial_tip3 = 2131296855;

        /* renamed from: aSm */
        public static final int uc_initial_tip4 = 2131296856;

        /* renamed from: aSn */
        public static final int uc_spinner_cancel = 2131296261;

        /* renamed from: aSo */
        public static final int uc_spinner_title = 2131296260;

        /* renamed from: aSp */
        public static final int uc_traffic = 2131296847;

        /* renamed from: aSq */
        public static final int uc_traffic_clear_data = 2131296850;

        /* renamed from: aSr */
        public static final int uc_traffic_dialog_cancel = 2131296849;

        /* renamed from: aSs */
        public static final int uc_traffic_dialog_confirm = 2131296848;

        /* renamed from: aSt */
        public static final int uc_traffic_msg1 = 2131296851;

        /* renamed from: aSu */
        public static final int uc_traffic_msg2 = 2131296852;

        /* renamed from: aSv */
        public static final int uc_traffic_save_text1 = 2131296857;

        /* renamed from: aSw */
        public static final int uc_traffic_save_text2 = 2131296858;

        /* renamed from: aSx */
        public static final int uc_traffic_save_title = 2131296859;

        /* renamed from: aSy */
        public static final int uc_traffic_uc_save = 2131296846;

        /* renamed from: aSz */
        public static final int unknow_file_type = 2131296295;

        /* renamed from: aqD */
        public static final int download = 2131296617;
        public static final int cancel = 2131296272;

        /* renamed from: fw */
        public static final int bookmarkaddress = 2131296718;

        /* renamed from: fx */
        public static final int bookmarkname = 2131296717;

        /* renamed from: fz */
        public static final int browser_page_attrs_address = 2131296663;

        /* renamed from: gH */
        public static final int goto_mainpage = 2131296835;

        /* renamed from: gy */
        public static final int filename = 2131296291;

        /* renamed from: gz */
        public static final int filepath = 2131296292;

        /* renamed from: hA */
        public static final int open_bookmark_history = 2131296514;

        /* renamed from: hS */
        public static final int prefix_title = 2131296513;
        public static final int ok = 2131296646;
        /* added by JADX */

        /* renamed from: hello  reason: collision with other field name */
        public static final int f950hello = 2131296256;
        /* added by JADX */

        /* renamed from: app_name_dot  reason: collision with other field name */
        public static final int f951app_name_dot = 2131296257;
        /* added by JADX */

        /* renamed from: app_name  reason: collision with other field name */
        public static final int f952app_name = 2131296258;
        /* added by JADX */

        /* renamed from: address  reason: collision with other field name */
        public static final int f953address = 2131296259;
        /* added by JADX */

        /* renamed from: uc_spinner_title  reason: collision with other field name */
        public static final int f954uc_spinner_title = 2131296260;
        /* added by JADX */

        /* renamed from: uc_spinner_cancel  reason: collision with other field name */
        public static final int f955uc_spinner_cancel = 2131296261;
        /* added by JADX */

        /* renamed from: share_title  reason: collision with other field name */
        public static final int f956share_title = 2131296262;
        /* added by JADX */

        /* renamed from: share_suffix  reason: collision with other field name */
        public static final int f957share_suffix = 2131296263;
        /* added by JADX */

        /* renamed from: share_link  reason: collision with other field name */
        public static final int f958share_link = 2131296264;
        /* added by JADX */

        /* renamed from: share_page  reason: collision with other field name */
        public static final int f959share_page = 2131296265;
        /* added by JADX */

        /* renamed from: share_image  reason: collision with other field name */
        public static final int f960share_image = 2131296266;
        /* added by JADX */

        /* renamed from: share_fail  reason: collision with other field name */
        public static final int f961share_fail = 2131296267;
        /* added by JADX */

        /* renamed from: share_image_fail  reason: collision with other field name */
        public static final int f962share_image_fail = 2131296268;
        /* added by JADX */

        /* renamed from: share_image_not_yet  reason: collision with other field name */
        public static final int f963share_image_not_yet = 2131296269;
        /* added by JADX */

        /* renamed from: cannot_back  reason: collision with other field name */
        public static final int f964cannot_back = 2131296270;
        /* added by JADX */

        /* renamed from: confirm  reason: collision with other field name */
        public static final int f965confirm = 2131296271;
        /* added by JADX */

        /* renamed from: search  reason: collision with other field name */
        public static final int f966search = 2131296273;
        /* added by JADX */

        /* renamed from: gotourl  reason: collision with other field name */
        public static final int f967gotourl = 2131296274;
        /* added by JADX */

        /* renamed from: emptydata  reason: collision with other field name */
        public static final int f968emptydata = 2131296275;
        /* added by JADX */

        /* renamed from: emptytitle  reason: collision with other field name */
        public static final int f969emptytitle = 2131296276;
        /* added by JADX */

        /* renamed from: tip  reason: collision with other field name */
        public static final int f970tip = 2131296277;
        /* added by JADX */

        /* renamed from: gonow  reason: collision with other field name */
        public static final int f971gonow = 2131296278;
        /* added by JADX */

        /* renamed from: toast_enter_passport  reason: collision with other field name */
        public static final int f972toast_enter_passport = 2131296279;
        /* added by JADX */

        /* renamed from: file_no_sdcard_warning  reason: collision with other field name */
        public static final int f973file_no_sdcard_warning = 2131296280;
        /* added by JADX */

        /* renamed from: file_already_root  reason: collision with other field name */
        public static final int f974file_already_root = 2131296281;
        /* added by JADX */

        /* renamed from: file_choose_file_null  reason: collision with other field name */
        public static final int f975file_choose_file_null = 2131296282;
        /* added by JADX */

        /* renamed from: file_choose_title  reason: collision with other field name */
        public static final int f976file_choose_title = 2131296283;
        /* added by JADX */

        /* renamed from: file_opera_failed  reason: collision with other field name */
        public static final int f977file_opera_failed = 2131296284;
        /* added by JADX */

        /* renamed from: dir_choose_title  reason: collision with other field name */
        public static final int f978dir_choose_title = 2131296285;
        /* added by JADX */

        /* renamed from: flash_file_choose_title  reason: collision with other field name */
        public static final int f979flash_file_choose_title = 2131296286;
        /* added by JADX */

        /* renamed from: skin_file_choose_title  reason: collision with other field name */
        public static final int f980skin_file_choose_title = 2131296287;
        /* added by JADX */

        /* renamed from: bookmark_file_choose_title  reason: collision with other field name */
        public static final int f981bookmark_file_choose_title = 2131296288;
        /* added by JADX */

        /* renamed from: maincontent_search  reason: collision with other field name */
        public static final int f982maincontent_search = 2131296289;
        /* added by JADX */

        /* renamed from: maincontent_inputurl  reason: collision with other field name */
        public static final int f983maincontent_inputurl = 2131296290;
        /* added by JADX */

        /* renamed from: filename  reason: collision with other field name */
        public static final int f984filename = 2131296291;
        /* added by JADX */

        /* renamed from: filepath  reason: collision with other field name */
        public static final int f985filepath = 2131296292;
        /* added by JADX */

        /* renamed from: folder  reason: collision with other field name */
        public static final int f986folder = 2131296293;
        /* added by JADX */

        /* renamed from: foldername  reason: collision with other field name */
        public static final int f987foldername = 2131296294;
        /* added by JADX */

        /* renamed from: unknow_file_type  reason: collision with other field name */
        public static final int f988unknow_file_type = 2131296295;
        /* added by JADX */

        /* renamed from: activity_not_found  reason: collision with other field name */
        public static final int f989activity_not_found = 2131296296;
        /* added by JADX */

        /* renamed from: open_throw_exception  reason: collision with other field name */
        public static final int f990open_throw_exception = 2131296297;
        /* added by JADX */

        /* renamed from: choose_engine  reason: collision with other field name */
        public static final int f991choose_engine = 2131296298;
        /* added by JADX */

        /* renamed from: bookmark_title_backtoroot  reason: collision with other field name */
        public static final int f992bookmark_title_backtoroot = 2131296299;
        /* added by JADX */

        /* renamed from: toast_wifi  reason: collision with other field name */
        public static final int f993toast_wifi = 2131296300;
        /* added by JADX */

        /* renamed from: toast_net  reason: collision with other field name */
        public static final int f994toast_net = 2131296301;
        /* added by JADX */

        /* renamed from: toast_wifi_optimist  reason: collision with other field name */
        public static final int f995toast_wifi_optimist = 2131296302;
        /* added by JADX */

        /* renamed from: toast_wifi_pause_dl  reason: collision with other field name */
        public static final int f996toast_wifi_pause_dl = 2131296303;
        /* added by JADX */

        /* renamed from: activity_title_bookmark  reason: collision with other field name */
        public static final int f997activity_title_bookmark = 2131296304;
        /* added by JADX */

        /* renamed from: activity_title_inputurl  reason: collision with other field name */
        public static final int f998activity_title_inputurl = 2131296305;
        /* added by JADX */

        /* renamed from: activity_title_search  reason: collision with other field name */
        public static final int f999activity_title_search = 2131296306;
        /* added by JADX */

        /* renamed from: activity_title_history  reason: collision with other field name */
        public static final int f1000activity_title_history = 2131296307;
        /* added by JADX */

        /* renamed from: activity_title_histroy_favorite  reason: collision with other field name */
        public static final int f1001activity_title_histroy_favorite = 2131296308;
        /* added by JADX */

        /* renamed from: activity_title_preference  reason: collision with other field name */
        public static final int f1002activity_title_preference = 2131296309;
        /* added by JADX */

        /* renamed from: activity_title_submaincontent  reason: collision with other field name */
        public static final int f1003activity_title_submaincontent = 2131296310;
        /* added by JADX */

        /* renamed from: activity_title_download  reason: collision with other field name */
        public static final int f1004activity_title_download = 2131296311;
        /* added by JADX */

        /* renamed from: activity_title_window  reason: collision with other field name */
        public static final int f1005activity_title_window = 2131296312;
        /* added by JADX */

        /* renamed from: activity_title_selector  reason: collision with other field name */
        public static final int f1006activity_title_selector = 2131296313;
        /* added by JADX */

        /* renamed from: activity_add_bookmark  reason: collision with other field name */
        public static final int f1007activity_add_bookmark = 2131296314;
        /* added by JADX */

        /* renamed from: activity_edit_mynavi  reason: collision with other field name */
        public static final int f1008activity_edit_mynavi = 2131296315;
        /* added by JADX */

        /* renamed from: activity_add_mynavi  reason: collision with other field name */
        public static final int f1009activity_add_mynavi = 2131296316;
        /* added by JADX */

        /* renamed from: controlbar_homepage  reason: collision with other field name */
        public static final int f1010controlbar_homepage = 2131296317;
        /* added by JADX */

        /* renamed from: controlbar_backward  reason: collision with other field name */
        public static final int f1011controlbar_backward = 2131296318;
        /* added by JADX */

        /* renamed from: controlbar_stop  reason: collision with other field name */
        public static final int f1012controlbar_stop = 2131296319;
        /* added by JADX */

        /* renamed from: controlbar_forward  reason: collision with other field name */
        public static final int f1013controlbar_forward = 2131296320;
        /* added by JADX */

        /* renamed from: controlbar_window  reason: collision with other field name */
        public static final int f1014controlbar_window = 2131296321;
        /* added by JADX */

        /* renamed from: controlbar_newwindow  reason: collision with other field name */
        public static final int f1015controlbar_newwindow = 2131296322;
        /* added by JADX */

        /* renamed from: controlbar_menu  reason: collision with other field name */
        public static final int f1016controlbar_menu = 2131296323;
        /* added by JADX */

        /* renamed from: controlbar_back  reason: collision with other field name */
        public static final int f1017controlbar_back = 2131296324;
        /* added by JADX */

        /* renamed from: controlbar_ok  reason: collision with other field name */
        public static final int f1018controlbar_ok = 2131296325;
        /* added by JADX */

        /* renamed from: controlbar_makedir  reason: collision with other field name */
        public static final int f1019controlbar_makedir = 2131296326;
        /* added by JADX */

        /* renamed from: controlbar_showtype  reason: collision with other field name */
        public static final int f1020controlbar_showtype = 2131296327;
        /* added by JADX */

        /* renamed from: controlbar_showtype_grid  reason: collision with other field name */
        public static final int f1021controlbar_showtype_grid = 2131296328;
        /* added by JADX */

        /* renamed from: controlbar_showtype_list  reason: collision with other field name */
        public static final int f1022controlbar_showtype_list = 2131296329;
        /* added by JADX */

        /* renamed from: controlbar_clear  reason: collision with other field name */
        public static final int f1023controlbar_clear = 2131296330;
        /* added by JADX */

        /* renamed from: controlbar_newbookmark  reason: collision with other field name */
        public static final int f1024controlbar_newbookmark = 2131296331;
        /* added by JADX */

        /* renamed from: controlbar_newfolder  reason: collision with other field name */
        public static final int f1025controlbar_newfolder = 2131296332;
        /* added by JADX */

        /* renamed from: controlbar_sync  reason: collision with other field name */
        public static final int f1026controlbar_sync = 2131296333;
        /* added by JADX */

        /* renamed from: controlbar_closeothers  reason: collision with other field name */
        public static final int f1027controlbar_closeothers = 2131296334;
        /* added by JADX */

        /* renamed from: controlbar_finish  reason: collision with other field name */
        public static final int f1028controlbar_finish = 2131296335;
        /* added by JADX */

        /* renamed from: readingmode_tail_caching  reason: collision with other field name */
        public static final int f1029readingmode_tail_caching = 2131296336;
        /* added by JADX */

        /* renamed from: readingmode_tail_pause  reason: collision with other field name */
        public static final int f1030readingmode_tail_pause = 2131296337;
        /* added by JADX */

        /* renamed from: readingmode_tail_fail  reason: collision with other field name */
        public static final int f1031readingmode_tail_fail = 2131296338;
        /* added by JADX */

        /* renamed from: readingmode_tail_end  reason: collision with other field name */
        public static final int f1032readingmode_tail_end = 2131296339;
        /* added by JADX */

        /* renamed from: readingmode_tips_enter  reason: collision with other field name */
        public static final int f1033readingmode_tips_enter = 2131296340;
        /* added by JADX */

        /* renamed from: readingmode_tips_quit  reason: collision with other field name */
        public static final int f1034readingmode_tips_quit = 2131296341;
        /* added by JADX */

        /* renamed from: readingmode_tips_saved  reason: collision with other field name */
        public static final int f1035readingmode_tips_saved = 2131296342;
        /* added by JADX */

        /* renamed from: readingmode_tips_enter_button  reason: collision with other field name */
        public static final int f1036readingmode_tips_enter_button = 2131296343;
        /* added by JADX */

        /* renamed from: readingmode_tips_recommand_open_pageud  reason: collision with other field name */
        public static final int f1037readingmode_tips_recommand_open_pageud = 2131296344;
        /* added by JADX */

        /* renamed from: menu_tab_favorite  reason: collision with other field name */
        public static final int f1038menu_tab_favorite = 2131296345;
        /* added by JADX */

        /* renamed from: menu_tab_setting  reason: collision with other field name */
        public static final int f1039menu_tab_setting = 2131296346;
        /* added by JADX */

        /* renamed from: menu_tab_tools  reason: collision with other field name */
        public static final int f1040menu_tab_tools = 2131296347;
        /* added by JADX */

        /* renamed from: menu_history_bookmark  reason: collision with other field name */
        public static final int f1041menu_history_bookmark = 2131296348;
        /* added by JADX */

        /* renamed from: menu_inputurl  reason: collision with other field name */
        public static final int f1042menu_inputurl = 2131296349;
        /* added by JADX */

        /* renamed from: menu_search  reason: collision with other field name */
        public static final int f1043menu_search = 2131296350;
        /* added by JADX */

        /* renamed from: menu_selectword  reason: collision with other field name */
        public static final int f1044menu_selectword = 2131296351;
        /* added by JADX */

        /* renamed from: menu_downloadmanager  reason: collision with other field name */
        public static final int f1045menu_downloadmanager = 2131296352;
        /* added by JADX */

        /* renamed from: menu_select_copy  reason: collision with other field name */
        public static final int f1046menu_select_copy = 2131296353;
        /* added by JADX */

        /* renamed from: menu_quit2  reason: collision with other field name */
        public static final int f1047menu_quit2 = 2131296354;
        /* added by JADX */

        /* renamed from: menu_refresh  reason: collision with other field name */
        public static final int f1048menu_refresh = 2131296355;
        /* added by JADX */

        /* renamed from: menu_fullscreen  reason: collision with other field name */
        public static final int f1049menu_fullscreen = 2131296356;
        /* added by JADX */

        /* renamed from: menu_outfullscreen  reason: collision with other field name */
        public static final int f1050menu_outfullscreen = 2131296357;
        /* added by JADX */

        /* renamed from: menu_nightmode  reason: collision with other field name */
        public static final int f1051menu_nightmode = 2131296358;
        /* added by JADX */

        /* renamed from: menu_day  reason: collision with other field name */
        public static final int f1052menu_day = 2131296359;
        /* added by JADX */

        /* renamed from: menu_syssettings  reason: collision with other field name */
        public static final int f1053menu_syssettings = 2131296360;
        /* added by JADX */

        /* renamed from: menu_quit  reason: collision with other field name */
        public static final int f1054menu_quit = 2131296361;
        /* added by JADX */

        /* renamed from: menu_bookmark  reason: collision with other field name */
        public static final int f1055menu_bookmark = 2131296362;
        /* added by JADX */

        /* renamed from: menu_addbookmark  reason: collision with other field name */
        public static final int f1056menu_addbookmark = 2131296363;
        /* added by JADX */

        /* renamed from: menu_browsemode  reason: collision with other field name */
        public static final int f1057menu_browsemode = 2131296364;
        /* added by JADX */

        /* renamed from: menu_normalmode  reason: collision with other field name */
        public static final int f1058menu_normalmode = 2131296365;
        /* added by JADX */

        /* renamed from: menu_more  reason: collision with other field name */
        public static final int f1059menu_more = 2131296366;
        /* added by JADX */

        /* renamed from: menu_debug  reason: collision with other field name */
        public static final int f1060menu_debug = 2131296367;
        /* added by JADX */

        /* renamed from: menu_landscape  reason: collision with other field name */
        public static final int f1061menu_landscape = 2131296368;
        /* added by JADX */

        /* renamed from: menu_porttrait  reason: collision with other field name */
        public static final int f1062menu_porttrait = 2131296369;
        /* added by JADX */

        /* renamed from: menu_porttrait_auto  reason: collision with other field name */
        public static final int f1063menu_porttrait_auto = 2131296370;
        /* added by JADX */

        /* renamed from: menu_land_state  reason: collision with other field name */
        public static final int f1064menu_land_state = 2131296371;
        /* added by JADX */

        /* renamed from: menu_page_updown  reason: collision with other field name */
        public static final int f1065menu_page_updown = 2131296372;
        /* added by JADX */

        /* renamed from: menu_novel_mode  reason: collision with other field name */
        public static final int f1066menu_novel_mode = 2131296373;
        /* added by JADX */

        /* renamed from: menu_checkupdate  reason: collision with other field name */
        public static final int f1067menu_checkupdate = 2131296374;
        /* added by JADX */

        /* renamed from: menu_checknetwork  reason: collision with other field name */
        public static final int f1068menu_checknetwork = 2131296375;
        /* added by JADX */

        /* renamed from: menu_help  reason: collision with other field name */
        public static final int f1069menu_help = 2131296376;
        /* added by JADX */

        /* renamed from: menu_about  reason: collision with other field name */
        public static final int f1070menu_about = 2131296377;
        /* added by JADX */

        /* renamed from: menu_recommendfriend  reason: collision with other field name */
        public static final int f1071menu_recommendfriend = 2131296378;
        /* added by JADX */

        /* renamed from: menu_refreshtimer  reason: collision with other field name */
        public static final int f1072menu_refreshtimer = 2131296379;
        /* added by JADX */

        /* renamed from: menu_stoprefreshtimer  reason: collision with other field name */
        public static final int f1073menu_stoprefreshtimer = 2131296380;
        /* added by JADX */

        /* renamed from: menu_pagefind  reason: collision with other field name */
        public static final int f1074menu_pagefind = 2131296381;
        /* added by JADX */

        /* renamed from: menu_filemanager  reason: collision with other field name */
        public static final int f1075menu_filemanager = 2131296382;
        /* added by JADX */

        /* renamed from: menu_pageattrs  reason: collision with other field name */
        public static final int f1076menu_pageattrs = 2131296383;
        /* added by JADX */

        /* renamed from: menu_clearmainpagedata  reason: collision with other field name */
        public static final int f1077menu_clearmainpagedata = 2131296384;
        /* added by JADX */

        /* renamed from: menu_return  reason: collision with other field name */
        public static final int f1078menu_return = 2131296385;
        /* added by JADX */

        /* renamed from: menu_test  reason: collision with other field name */
        public static final int f1079menu_test = 2131296386;
        /* added by JADX */

        /* renamed from: menu_sharepage  reason: collision with other field name */
        public static final int f1080menu_sharepage = 2131296387;
        /* added by JADX */

        /* renamed from: menu_reportwebsite  reason: collision with other field name */
        public static final int f1081menu_reportwebsite = 2131296388;
        /* added by JADX */

        /* renamed from: menu_report  reason: collision with other field name */
        public static final int f1082menu_report = 2131296389;
        /* added by JADX */

        /* renamed from: menu_skinmanager  reason: collision with other field name */
        public static final int f1083menu_skinmanager = 2131296390;
        /* added by JADX */

        /* renamed from: menu_nopic  reason: collision with other field name */
        public static final int f1084menu_nopic = 2131296391;
        /* added by JADX */

        /* renamed from: menu_nonopic  reason: collision with other field name */
        public static final int f1085menu_nonopic = 2131296392;
        /* added by JADX */

        /* renamed from: toast_nopic  reason: collision with other field name */
        public static final int f1086toast_nopic = 2131296393;
        /* added by JADX */

        /* renamed from: menu_passoprt  reason: collision with other field name */
        public static final int f1087menu_passoprt = 2131296394;
        /* added by JADX */

        /* renamed from: menu_uctraffic  reason: collision with other field name */
        public static final int f1088menu_uctraffic = 2131296395;
        /* added by JADX */

        /* renamed from: bookmark_sync_sync  reason: collision with other field name */
        public static final int f1089bookmark_sync_sync = 2131296396;
        /* added by JADX */

        /* renamed from: bookmark_sync_import  reason: collision with other field name */
        public static final int f1090bookmark_sync_import = 2131296397;
        /* added by JADX */

        /* renamed from: bookmark_sync_export  reason: collision with other field name */
        public static final int f1091bookmark_sync_export = 2131296398;
        /* added by JADX */

        /* renamed from: contextmenu_bookmark_openinnewwindow  reason: collision with other field name */
        public static final int f1092contextmenu_bookmark_openinnewwindow = 2131296399;
        /* added by JADX */

        /* renamed from: contextmenu_bookmark_edit  reason: collision with other field name */
        public static final int f1093contextmenu_bookmark_edit = 2131296400;
        /* added by JADX */

        /* renamed from: contextmenu_bookmark_delete  reason: collision with other field name */
        public static final int f1094contextmenu_bookmark_delete = 2131296401;
        /* added by JADX */

        /* renamed from: contextmenu_bookmark_deletefolder  reason: collision with other field name */
        public static final int f1095contextmenu_bookmark_deletefolder = 2131296402;
        /* added by JADX */

        /* renamed from: contextmenu_bookmark_renamefolder  reason: collision with other field name */
        public static final int f1096contextmenu_bookmark_renamefolder = 2131296403;
        /* added by JADX */

        /* renamed from: contextmenu_bookmark_deleteall  reason: collision with other field name */
        public static final int f1097contextmenu_bookmark_deleteall = 2131296404;
        /* added by JADX */

        /* renamed from: contextmenu_bookmark_sharepage  reason: collision with other field name */
        public static final int f1098contextmenu_bookmark_sharepage = 2131296405;
        /* added by JADX */

        /* renamed from: contextmenu_bookmark_addtonavi  reason: collision with other field name */
        public static final int f1099contextmenu_bookmark_addtonavi = 2131296406;
        /* added by JADX */

        /* renamed from: contextmenu_pick_inputmethod  reason: collision with other field name */
        public static final int f1100contextmenu_pick_inputmethod = 2131296407;
        /* added by JADX */

        /* renamed from: contextmenu_input_copy  reason: collision with other field name */
        public static final int f1101contextmenu_input_copy = 2131296408;
        /* added by JADX */

        /* renamed from: contextmenu_input_cut  reason: collision with other field name */
        public static final int f1102contextmenu_input_cut = 2131296409;
        /* added by JADX */

        /* renamed from: contextmenu_input_paste  reason: collision with other field name */
        public static final int f1103contextmenu_input_paste = 2131296410;
        /* added by JADX */

        /* renamed from: contextmenu_input_select_all  reason: collision with other field name */
        public static final int f1104contextmenu_input_select_all = 2131296411;
        /* added by JADX */

        /* renamed from: contextmenu_input_select  reason: collision with other field name */
        public static final int f1105contextmenu_input_select = 2131296412;
        /* added by JADX */

        /* renamed from: contextmenu_input_select_cancle  reason: collision with other field name */
        public static final int f1106contextmenu_input_select_cancle = 2131296413;
        /* added by JADX */

        /* renamed from: contextmenu_file_copy  reason: collision with other field name */
        public static final int f1107contextmenu_file_copy = 2131296414;
        /* added by JADX */

        /* renamed from: contextmenu_file_cut  reason: collision with other field name */
        public static final int f1108contextmenu_file_cut = 2131296415;
        /* added by JADX */

        /* renamed from: contextmenu_file_delete  reason: collision with other field name */
        public static final int f1109contextmenu_file_delete = 2131296416;
        /* added by JADX */

        /* renamed from: contextmenu_file_rename  reason: collision with other field name */
        public static final int f1110contextmenu_file_rename = 2131296417;
        /* added by JADX */

        /* renamed from: contextmenu_file_attr  reason: collision with other field name */
        public static final int f1111contextmenu_file_attr = 2131296418;
        /* added by JADX */

        /* renamed from: contextmenu_file_paste  reason: collision with other field name */
        public static final int f1112contextmenu_file_paste = 2131296419;
        /* added by JADX */

        /* renamed from: contextmenu_mynavi_edit  reason: collision with other field name */
        public static final int f1113contextmenu_mynavi_edit = 2131296420;
        /* added by JADX */

        /* renamed from: contextmenu_mynavi_delete  reason: collision with other field name */
        public static final int f1114contextmenu_mynavi_delete = 2131296421;
        /* added by JADX */

        /* renamed from: contextmenu_mynavi_add  reason: collision with other field name */
        public static final int f1115contextmenu_mynavi_add = 2131296422;
        /* added by JADX */

        /* renamed from: contextmenu_mynavi_solidify  reason: collision with other field name */
        public static final int f1116contextmenu_mynavi_solidify = 2131296423;
        /* added by JADX */

        /* renamed from: contextmenu_mynavi_unsolidify  reason: collision with other field name */
        public static final int f1117contextmenu_mynavi_unsolidify = 2131296424;
        /* added by JADX */

        /* renamed from: contextmenu_mynavi_singleline  reason: collision with other field name */
        public static final int f1118contextmenu_mynavi_singleline = 2131296425;
        /* added by JADX */

        /* renamed from: contextmenu_mynavi_doubleline  reason: collision with other field name */
        public static final int f1119contextmenu_mynavi_doubleline = 2131296426;
        /* added by JADX */

        /* renamed from: contextmenu_history_openinnewwindow  reason: collision with other field name */
        public static final int f1120contextmenu_history_openinnewwindow = 2131296427;
        /* added by JADX */

        /* renamed from: contextmenu_history_delete  reason: collision with other field name */
        public static final int f1121contextmenu_history_delete = 2131296428;
        /* added by JADX */

        /* renamed from: contextmenu_history_addtonavi  reason: collision with other field name */
        public static final int f1122contextmenu_history_addtonavi = 2131296429;
        /* added by JADX */

        /* renamed from: alert_dialog_ok  reason: collision with other field name */
        public static final int f1123alert_dialog_ok = 2131296430;
        /* added by JADX */

        /* renamed from: alert_dialog_open  reason: collision with other field name */
        public static final int f1124alert_dialog_open = 2131296431;
        /* added by JADX */

        /* renamed from: alert_dialog_cancel  reason: collision with other field name */
        public static final int f1125alert_dialog_cancel = 2131296432;
        /* added by JADX */

        /* renamed from: alert_dialog_close  reason: collision with other field name */
        public static final int f1126alert_dialog_close = 2131296433;
        /* added by JADX */

        /* renamed from: alert_dialog_over  reason: collision with other field name */
        public static final int f1127alert_dialog_over = 2131296434;
        /* added by JADX */

        /* renamed from: alert_dialog_change_brightness  reason: collision with other field name */
        public static final int f1128alert_dialog_change_brightness = 2131296435;
        /* added by JADX */

        /* renamed from: alert_dialog_play  reason: collision with other field name */
        public static final int f1129alert_dialog_play = 2131296436;
        /* added by JADX */

        /* renamed from: alert_dialog_change_access_points  reason: collision with other field name */
        public static final int f1130alert_dialog_change_access_points = 2131296437;
        /* added by JADX */

        /* renamed from: dialog_title_newbookmark  reason: collision with other field name */
        public static final int f1131dialog_title_newbookmark = 2131296438;
        /* added by JADX */

        /* renamed from: dialog_title_editbookmark  reason: collision with other field name */
        public static final int f1132dialog_title_editbookmark = 2131296439;
        /* added by JADX */

        /* renamed from: dialog_title_newfolder  reason: collision with other field name */
        public static final int f1133dialog_title_newfolder = 2131296440;
        /* added by JADX */

        /* renamed from: dialog_title_deletebookmark  reason: collision with other field name */
        public static final int f1134dialog_title_deletebookmark = 2131296441;
        /* added by JADX */

        /* renamed from: dialog_title_clear  reason: collision with other field name */
        public static final int f1135dialog_title_clear = 2131296442;
        /* added by JADX */

        /* renamed from: dialog_title_deletefolder  reason: collision with other field name */
        public static final int f1136dialog_title_deletefolder = 2131296443;
        /* added by JADX */

        /* renamed from: dialog_title_renamefolder  reason: collision with other field name */
        public static final int f1137dialog_title_renamefolder = 2131296444;
        /* added by JADX */

        /* renamed from: dialog_title_deleteall  reason: collision with other field name */
        public static final int f1138dialog_title_deleteall = 2131296445;
        /* added by JADX */

        /* renamed from: dialog_title_createshortcut  reason: collision with other field name */
        public static final int f1139dialog_title_createshortcut = 2131296446;
        /* added by JADX */

        /* renamed from: dialog_title_novelmode  reason: collision with other field name */
        public static final int f1140dialog_title_novelmode = 2131296447;
        /* added by JADX */

        /* renamed from: dialog_title_quit  reason: collision with other field name */
        public static final int f1141dialog_title_quit = 2131296448;
        /* added by JADX */

        /* renamed from: dialog_msg_quit  reason: collision with other field name */
        public static final int f1142dialog_msg_quit = 2131296449;
        /* added by JADX */

        /* renamed from: del_bookmark_success  reason: collision with other field name */
        public static final int f1143del_bookmark_success = 2131296450;
        /* added by JADX */

        /* renamed from: dialog_title_makedir  reason: collision with other field name */
        public static final int f1144dialog_title_makedir = 2131296451;
        /* added by JADX */

        /* renamed from: dialog_msg_filename_cannot_null  reason: collision with other field name */
        public static final int f1145dialog_msg_filename_cannot_null = 2131296452;
        /* added by JADX */

        /* renamed from: dialog_msg_filecopy_error  reason: collision with other field name */
        public static final int f1146dialog_msg_filecopy_error = 2131296453;
        /* added by JADX */

        /* renamed from: dialog_msg_filecut_error  reason: collision with other field name */
        public static final int f1147dialog_msg_filecut_error = 2131296454;
        /* added by JADX */

        /* renamed from: dialog_msg_filename_dulplicate  reason: collision with other field name */
        public static final int f1148dialog_msg_filename_dulplicate = 2131296455;
        /* added by JADX */

        /* renamed from: dialog_msg_folder_create_failure  reason: collision with other field name */
        public static final int f1149dialog_msg_folder_create_failure = 2131296456;
        /* added by JADX */

        /* renamed from: dialog_title_waiting_copy  reason: collision with other field name */
        public static final int f1150dialog_title_waiting_copy = 2131296457;
        /* added by JADX */

        /* renamed from: dialog_msg_waiting_copy  reason: collision with other field name */
        public static final int f1151dialog_msg_waiting_copy = 2131296458;
        /* added by JADX */

        /* renamed from: dialog_msg_sdcard_no_space  reason: collision with other field name */
        public static final int f1152dialog_msg_sdcard_no_space = 2131296459;
        /* added by JADX */

        /* renamed from: text_max_size  reason: collision with other field name */
        public static final int f1153text_max_size = 2131296460;
        /* added by JADX */

        /* renamed from: dialog_msg_rename_failure  reason: collision with other field name */
        public static final int f1154dialog_msg_rename_failure = 2131296461;
        /* added by JADX */

        /* renamed from: dialog_title_deletefile  reason: collision with other field name */
        public static final int f1155dialog_title_deletefile = 2131296462;
        /* added by JADX */

        /* renamed from: dialog_msg_deletefile  reason: collision with other field name */
        public static final int f1156dialog_msg_deletefile = 2131296463;
        /* added by JADX */

        /* renamed from: dialog_title_rename  reason: collision with other field name */
        public static final int f1157dialog_title_rename = 2131296464;
        /* added by JADX */

        /* renamed from: dialog_title_fileattr  reason: collision with other field name */
        public static final int f1158dialog_title_fileattr = 2131296465;
        /* added by JADX */

        /* renamed from: dialog_msg_filesize  reason: collision with other field name */
        public static final int f1159dialog_msg_filesize = 2131296466;
        /* added by JADX */

        /* renamed from: dialog_msg_lastmodify  reason: collision with other field name */
        public static final int f1160dialog_msg_lastmodify = 2131296467;
        /* added by JADX */

        /* renamed from: dialog_msg_filepath  reason: collision with other field name */
        public static final int f1161dialog_msg_filepath = 2131296468;
        /* added by JADX */

        /* renamed from: dialog_title_replace_file  reason: collision with other field name */
        public static final int f1162dialog_title_replace_file = 2131296469;
        /* added by JADX */

        /* renamed from: dialog_msg_replace_file  reason: collision with other field name */
        public static final int f1163dialog_msg_replace_file = 2131296470;
        /* added by JADX */

        /* renamed from: dialog_msg_deletebookmark  reason: collision with other field name */
        public static final int f1164dialog_msg_deletebookmark = 2131296471;
        /* added by JADX */

        /* renamed from: dialog_msg_deletefolder  reason: collision with other field name */
        public static final int f1165dialog_msg_deletefolder = 2131296472;
        /* added by JADX */

        /* renamed from: dialog_msg_deleteall_bookmark  reason: collision with other field name */
        public static final int f1166dialog_msg_deleteall_bookmark = 2131296473;
        /* added by JADX */

        /* renamed from: dialog_msg_deleteall_history  reason: collision with other field name */
        public static final int f1167dialog_msg_deleteall_history = 2131296474;
        /* added by JADX */

        /* renamed from: dialog_msg_deleteall  reason: collision with other field name */
        public static final int f1168dialog_msg_deleteall = 2131296475;
        /* added by JADX */

        /* renamed from: msg_add_success  reason: collision with other field name */
        public static final int f1169msg_add_success = 2131296476;
        /* added by JADX */

        /* renamed from: msg_add_dir_success  reason: collision with other field name */
        public static final int f1170msg_add_dir_success = 2131296477;
        /* added by JADX */

        /* renamed from: msg_add_fail  reason: collision with other field name */
        public static final int f1171msg_add_fail = 2131296478;
        /* added by JADX */

        /* renamed from: msg_edit_fail  reason: collision with other field name */
        public static final int f1172msg_edit_fail = 2131296479;
        /* added by JADX */

        /* renamed from: msg_edit_success  reason: collision with other field name */
        public static final int f1173msg_edit_success = 2131296480;
        /* added by JADX */

        /* renamed from: dialog_msg_createshortcut  reason: collision with other field name */
        public static final int f1174dialog_msg_createshortcut = 2131296481;
        /* added by JADX */

        /* renamed from: dialog_msg_tips_qvga  reason: collision with other field name */
        public static final int f1175dialog_msg_tips_qvga = 2131296482;
        /* added by JADX */

        /* renamed from: dialog_msg_tips_not_qvga  reason: collision with other field name */
        public static final int f1176dialog_msg_tips_not_qvga = 2131296483;
        /* added by JADX */

        /* renamed from: dialog_msg_novelmode  reason: collision with other field name */
        public static final int f1177dialog_msg_novelmode = 2131296484;
        /* added by JADX */

        /* renamed from: dialog_msg_novelmode_zoom  reason: collision with other field name */
        public static final int f1178dialog_msg_novelmode_zoom = 2131296485;
        /* added by JADX */

        /* renamed from: dialog_title_pageupdown  reason: collision with other field name */
        public static final int f1179dialog_title_pageupdown = 2131296486;
        /* added by JADX */

        /* renamed from: dialog_msg_pageupdown_zoom  reason: collision with other field name */
        public static final int f1180dialog_msg_pageupdown_zoom = 2131296487;
        /* added by JADX */

        /* renamed from: dialog_title_nightmode  reason: collision with other field name */
        public static final int f1181dialog_title_nightmode = 2131296488;
        /* added by JADX */

        /* renamed from: dialog_msg_nightmode  reason: collision with other field name */
        public static final int f1182dialog_msg_nightmode = 2131296489;
        /* added by JADX */

        /* renamed from: dialog_title_nightmode_brightness  reason: collision with other field name */
        public static final int f1183dialog_title_nightmode_brightness = 2131296490;
        /* added by JADX */

        /* renamed from: dialog_msg_zoom  reason: collision with other field name */
        public static final int f1184dialog_msg_zoom = 2131296491;
        /* added by JADX */

        /* renamed from: dialog_msg_fit  reason: collision with other field name */
        public static final int f1185dialog_msg_fit = 2131296492;
        /* added by JADX */

        /* renamed from: dialog_msg_tipszoom  reason: collision with other field name */
        public static final int f1186dialog_msg_tipszoom = 2131296493;
        /* added by JADX */

        /* renamed from: contextmenu_window_closeother  reason: collision with other field name */
        public static final int f1187contextmenu_window_closeother = 2131296494;
        /* added by JADX */

        /* renamed from: dialog_msg_closeother  reason: collision with other field name */
        public static final int f1188dialog_msg_closeother = 2131296495;
        /* added by JADX */

        /* renamed from: dialog_msg_should_download  reason: collision with other field name */
        public static final int f1189dialog_msg_should_download = 2131296496;
        /* added by JADX */

        /* renamed from: dialog_title_upload  reason: collision with other field name */
        public static final int f1190dialog_title_upload = 2131296497;
        /* added by JADX */

        /* renamed from: dialog_title_please_select  reason: collision with other field name */
        public static final int f1191dialog_title_please_select = 2131296498;
        /* added by JADX */

        /* renamed from: dialog_title_bookmark_sharepage  reason: collision with other field name */
        public static final int f1192dialog_title_bookmark_sharepage = 2131296499;
        /* added by JADX */

        /* renamed from: dialog_title_sharepage  reason: collision with other field name */
        public static final int f1193dialog_title_sharepage = 2131296500;
        /* added by JADX */

        /* renamed from: dialog_msg_is_sms_share_page  reason: collision with other field name */
        public static final int f1194dialog_msg_is_sms_share_page = 2131296501;
        /* added by JADX */

        /* renamed from: dialog_msg_is_sms_share_bookmark  reason: collision with other field name */
        public static final int f1195dialog_msg_is_sms_share_bookmark = 2131296502;
        /* added by JADX */

        /* renamed from: dialog_title_browse_model  reason: collision with other field name */
        public static final int f1196dialog_title_browse_model = 2131296503;
        /* added by JADX */

        /* renamed from: dialog_title_popup_anim_off  reason: collision with other field name */
        public static final int f1197dialog_title_popup_anim_off = 2131296504;
        /* added by JADX */

        /* renamed from: dialog_msg_popup_anim_off  reason: collision with other field name */
        public static final int f1198dialog_msg_popup_anim_off = 2131296505;
        /* added by JADX */

        /* renamed from: dialog_button_popup_anim_off  reason: collision with other field name */
        public static final int f1199dialog_button_popup_anim_off = 2131296506;
        /* added by JADX */

        /* renamed from: dialog_pad_tip  reason: collision with other field name */
        public static final int f1200dialog_pad_tip = 2131296507;
        /* added by JADX */

        /* renamed from: dialog_help_slide_msg  reason: collision with other field name */
        public static final int f1201dialog_help_slide_msg = 2131296508;
        /* added by JADX */

        /* renamed from: dialog_rtsp_not_support  reason: collision with other field name */
        public static final int f1202dialog_rtsp_not_support = 2131296509;
        /* added by JADX */

        /* renamed from: dialog_play_not_support_rtsp  reason: collision with other field name */
        public static final int f1203dialog_play_not_support_rtsp = 2131296510;
        /* added by JADX */

        /* renamed from: fs_fordward_backward_tips  reason: collision with other field name */
        public static final int f1204fs_fordward_backward_tips = 2131296511;
        /* added by JADX */

        /* renamed from: prefix_url_address  reason: collision with other field name */
        public static final int f1205prefix_url_address = 2131296512;
        /* added by JADX */

        /* renamed from: prefix_title  reason: collision with other field name */
        public static final int f1206prefix_title = 2131296513;
        /* added by JADX */

        /* renamed from: open_bookmark_history  reason: collision with other field name */
        public static final int f1207open_bookmark_history = 2131296514;
        /* added by JADX */

        /* renamed from: choose_mynavi_position  reason: collision with other field name */
        public static final int f1208choose_mynavi_position = 2131296515;
        /* added by JADX */

        /* renamed from: mynavi_fixed  reason: collision with other field name */
        public static final int f1209mynavi_fixed = 2131296516;
        /* added by JADX */

        /* renamed from: choose_mynavi_position_title  reason: collision with other field name */
        public static final int f1210choose_mynavi_position_title = 2131296517;
        /* added by JADX */

        /* renamed from: msg_no_position  reason: collision with other field name */
        public static final int f1211msg_no_position = 2131296518;
        /* added by JADX */

        /* renamed from: null_mynavi_title  reason: collision with other field name */
        public static final int f1212null_mynavi_title = 2131296519;
        /* added by JADX */

        /* renamed from: pref_title_display  reason: collision with other field name */
        public static final int f1213pref_title_display = 2131296520;
        /* added by JADX */

        /* renamed from: pref_item_title_textsize  reason: collision with other field name */
        public static final int f1214pref_item_title_textsize = 2131296521;
        /* added by JADX */

        /* renamed from: pref_item_title_word_subsection  reason: collision with other field name */
        public static final int f1215pref_item_title_word_subsection = 2131296522;
        /* added by JADX */

        /* renamed from: pref_item_title_encode  reason: collision with other field name */
        public static final int f1216pref_item_title_encode = 2131296523;
        /* added by JADX */

        /* renamed from: pref_item_title_load_picture  reason: collision with other field name */
        public static final int f1217pref_item_title_load_picture = 2131296524;
        /* added by JADX */

        /* renamed from: pref_item_title_picture_quality  reason: collision with other field name */
        public static final int f1218pref_item_title_picture_quality = 2131296525;
        /* added by JADX */

        /* renamed from: pref_item_title_zoom_settings  reason: collision with other field name */
        public static final int f1219pref_item_title_zoom_settings = 2131296526;
        /* added by JADX */

        /* renamed from: pref_item_title_theme  reason: collision with other field name */
        public static final int f1220pref_item_title_theme = 2131296527;
        /* added by JADX */

        /* renamed from: pref_item_title_folding_mode  reason: collision with other field name */
        public static final int f1221pref_item_title_folding_mode = 2131296528;
        /* added by JADX */

        /* renamed from: pref_item_title_auto_landscape  reason: collision with other field name */
        public static final int f1222pref_item_title_auto_landscape = 2131296529;
        /* added by JADX */

        /* renamed from: pref_item_title_homepage  reason: collision with other field name */
        public static final int f1223pref_item_title_homepage = 2131296530;
        /* added by JADX */

        /* renamed from: pref_item_title_titlebar_fullscreen  reason: collision with other field name */
        public static final int f1224pref_item_title_titlebar_fullscreen = 2131296531;
        /* added by JADX */

        /* renamed from: pref_item_title_show_avatar  reason: collision with other field name */
        public static final int f1225pref_item_title_show_avatar = 2131296532;
        /* added by JADX */

        /* renamed from: pref_title_network  reason: collision with other field name */
        public static final int f1226pref_title_network = 2131296533;
        /* added by JADX */

        /* renamed from: pref_item_title_useragent  reason: collision with other field name */
        public static final int f1227pref_item_title_useragent = 2131296534;
        /* added by JADX */

        /* renamed from: pref_item_title_use_proxy  reason: collision with other field name */
        public static final int f1228pref_item_title_use_proxy = 2131296535;
        /* added by JADX */

        /* renamed from: pref_item_title_wifi_mode  reason: collision with other field name */
        public static final int f1229pref_item_title_wifi_mode = 2131296536;
        /* added by JADX */

        /* renamed from: pref_item_subtitle_wifi_mode  reason: collision with other field name */
        public static final int f1230pref_item_subtitle_wifi_mode = 2131296537;
        /* added by JADX */

        /* renamed from: pref_title_download  reason: collision with other field name */
        public static final int f1231pref_title_download = 2131296538;
        /* added by JADX */

        /* renamed from: pref_item_title_download_subsection  reason: collision with other field name */
        public static final int f1232pref_item_title_download_subsection = 2131296539;
        /* added by JADX */

        /* renamed from: pref_item_title_download_path  reason: collision with other field name */
        public static final int f1233pref_item_title_download_path = 2131296540;
        /* added by JADX */

        /* renamed from: pref_item_title_taskrename  reason: collision with other field name */
        public static final int f1234pref_item_title_taskrename = 2131296541;
        /* added by JADX */

        /* renamed from: pref_item_title_taskcount  reason: collision with other field name */
        public static final int f1235pref_item_title_taskcount = 2131296542;
        /* added by JADX */

        /* renamed from: pref_item_title_download_in_background  reason: collision with other field name */
        public static final int f1236pref_item_title_download_in_background = 2131296543;
        /* added by JADX */

        /* renamed from: pref_item_title_download_finish_tips  reason: collision with other field name */
        public static final int f1237pref_item_title_download_finish_tips = 2131296544;
        /* added by JADX */

        /* renamed from: pref_item_title_download_app  reason: collision with other field name */
        public static final int f1238pref_item_title_download_app = 2131296545;
        /* added by JADX */

        /* renamed from: download_app_changed_note  reason: collision with other field name */
        public static final int f1239download_app_changed_note = 2131296546;
        /* added by JADX */

        /* renamed from: pref_title_browser  reason: collision with other field name */
        public static final int f1240pref_title_browser = 2131296547;
        /* added by JADX */

        /* renamed from: pref_item_title_browse_model  reason: collision with other field name */
        public static final int f1241pref_item_title_browse_model = 2131296548;
        /* added by JADX */

        /* renamed from: pref_item_title_usepreread  reason: collision with other field name */
        public static final int f1242pref_item_title_usepreread = 2131296549;
        /* added by JADX */

        /* renamed from: pref_item_title_popupinfo  reason: collision with other field name */
        public static final int f1243pref_item_title_popupinfo = 2131296550;
        /* added by JADX */

        /* renamed from: pref_item_title_scroll  reason: collision with other field name */
        public static final int f1244pref_item_title_scroll = 2131296551;
        /* added by JADX */

        /* renamed from: pref_item_title_wap  reason: collision with other field name */
        public static final int f1245pref_item_title_wap = 2131296552;
        /* added by JADX */

        /* renamed from: pref_item_title_set_default  reason: collision with other field name */
        public static final int f1246pref_item_title_set_default = 2131296553;
        /* added by JADX */

        /* renamed from: pref_item_title_java_zoom  reason: collision with other field name */
        public static final int f1247pref_item_title_java_zoom = 2131296554;
        /* added by JADX */

        /* renamed from: pref_item_title_night_mode_brightness  reason: collision with other field name */
        public static final int f1248pref_item_title_night_mode_brightness = 2131296555;
        /* added by JADX */

        /* renamed from: pref_item_summary_night_mode_brightness  reason: collision with other field name */
        public static final int f1249pref_item_summary_night_mode_brightness = 2131296556;
        /* added by JADX */

        /* renamed from: pref_item_title_page_up_down_location  reason: collision with other field name */
        public static final int f1250pref_item_title_page_up_down_location = 2131296557;
        /* added by JADX */

        /* renamed from: pref_item_title_novel_mode  reason: collision with other field name */
        public static final int f1251pref_item_title_novel_mode = 2131296558;
        /* added by JADX */

        /* renamed from: pref_item_title_save_psw_juc  reason: collision with other field name */
        public static final int f1252pref_item_title_save_psw_juc = 2131296559;
        /* added by JADX */

        /* renamed from: pref_item_title_enable_sound  reason: collision with other field name */
        public static final int f1253pref_item_title_enable_sound = 2131296560;
        /* added by JADX */

        /* renamed from: pref_item_title_mynavi_lines  reason: collision with other field name */
        public static final int f1254pref_item_title_mynavi_lines = 2131296561;
        /* added by JADX */

        /* renamed from: pref_item_title_need_popup_anim  reason: collision with other field name */
        public static final int f1255pref_item_title_need_popup_anim = 2131296562;
        /* added by JADX */

        /* renamed from: pref_item_title_tips_exit  reason: collision with other field name */
        public static final int f1256pref_item_title_tips_exit = 2131296563;
        /* added by JADX */

        /* renamed from: pref_title_zoom_settings  reason: collision with other field name */
        public static final int f1257pref_title_zoom_settings = 2131296564;
        /* added by JADX */

        /* renamed from: zoom_mode  reason: collision with other field name */
        public static final int f1258zoom_mode = 2131296565;
        /* added by JADX */

        /* renamed from: pref_item_title_zoom_mode  reason: collision with other field name */
        public static final int f1259pref_item_title_zoom_mode = 2131296566;
        /* added by JADX */

        /* renamed from: pref_item_title_enablejs  reason: collision with other field name */
        public static final int f1260pref_item_title_enablejs = 2131296567;
        /* added by JADX */

        /* renamed from: pref_item_title_block_popup_window  reason: collision with other field name */
        public static final int f1261pref_item_title_block_popup_window = 2131296568;
        /* added by JADX */

        /* renamed from: pref_item_title_fit_screen  reason: collision with other field name */
        public static final int f1262pref_item_title_fit_screen = 2131296569;
        /* added by JADX */

        /* renamed from: pref_item_title_save_psw  reason: collision with other field name */
        public static final int f1263pref_item_title_save_psw = 2131296570;
        /* added by JADX */

        /* renamed from: pref_item_title_clear_psw  reason: collision with other field name */
        public static final int f1264pref_item_title_clear_psw = 2131296571;
        /* added by JADX */

        /* renamed from: pref_item_msg_clear_psw  reason: collision with other field name */
        public static final int f1265pref_item_msg_clear_psw = 2131296572;
        /* added by JADX */

        /* renamed from: dlg_button_ok  reason: collision with other field name */
        public static final int f1266dlg_button_ok = 2131296573;
        /* added by JADX */

        /* renamed from: dlg_button_cancle  reason: collision with other field name */
        public static final int f1267dlg_button_cancle = 2131296574;
        /* added by JADX */

        /* renamed from: dlg_button_save  reason: collision with other field name */
        public static final int f1268dlg_button_save = 2131296575;
        /* added by JADX */

        /* renamed from: dlg_button_autosave  reason: collision with other field name */
        public static final int f1269dlg_button_autosave = 2131296576;
        /* added by JADX */

        /* renamed from: dlg_button_notsave  reason: collision with other field name */
        public static final int f1270dlg_button_notsave = 2131296577;
        /* added by JADX */

        /* renamed from: dlg_button_replace  reason: collision with other field name */
        public static final int f1271dlg_button_replace = 2131296578;
        /* added by JADX */

        /* renamed from: dlg_button_neversave  reason: collision with other field name */
        public static final int f1272dlg_button_neversave = 2131296579;
        /* added by JADX */

        /* renamed from: pref_title_clear  reason: collision with other field name */
        public static final int f1273pref_title_clear = 2131296580;
        /* added by JADX */

        /* renamed from: pref_item_title_clear_cache  reason: collision with other field name */
        public static final int f1274pref_item_title_clear_cache = 2131296581;
        /* added by JADX */

        /* renamed from: pref_item_msg_clear_cache  reason: collision with other field name */
        public static final int f1275pref_item_msg_clear_cache = 2131296582;
        /* added by JADX */

        /* renamed from: pref_item_title_accept_cookie  reason: collision with other field name */
        public static final int f1276pref_item_title_accept_cookie = 2131296583;
        /* added by JADX */

        /* renamed from: pref_item_title_clear_cookie  reason: collision with other field name */
        public static final int f1277pref_item_title_clear_cookie = 2131296584;
        /* added by JADX */

        /* renamed from: pref_item_msg_clear_cookie  reason: collision with other field name */
        public static final int f1278pref_item_msg_clear_cookie = 2131296585;
        /* added by JADX */

        /* renamed from: pref_item_title_save_form  reason: collision with other field name */
        public static final int f1279pref_item_title_save_form = 2131296586;
        /* added by JADX */

        /* renamed from: pref_item_title_clear_form  reason: collision with other field name */
        public static final int f1280pref_item_title_clear_form = 2131296587;
        /* added by JADX */

        /* renamed from: pref_item_msg_clear_form  reason: collision with other field name */
        public static final int f1281pref_item_msg_clear_form = 2131296588;
        /* added by JADX */

        /* renamed from: pref_item_title_clear_history  reason: collision with other field name */
        public static final int f1282pref_item_title_clear_history = 2131296589;
        /* added by JADX */

        /* renamed from: pref_item_msg_clear_history  reason: collision with other field name */
        public static final int f1283pref_item_msg_clear_history = 2131296590;
        /* added by JADX */

        /* renamed from: pref_item_clear_record  reason: collision with other field name */
        public static final int f1284pref_item_clear_record = 2131296591;
        /* added by JADX */

        /* renamed from: pref_item_clear_record_tip  reason: collision with other field name */
        public static final int f1285pref_item_clear_record_tip = 2131296592;
        /* added by JADX */

        /* renamed from: clear_all_success  reason: collision with other field name */
        public static final int f1286clear_all_success = 2131296593;
        /* added by JADX */

        /* renamed from: pref_item_clear_input_history  reason: collision with other field name */
        public static final int f1287pref_item_clear_input_history = 2131296594;
        /* added by JADX */

        /* renamed from: pref_item_clear_visit_history  reason: collision with other field name */
        public static final int f1288pref_item_clear_visit_history = 2131296595;
        /* added by JADX */

        /* renamed from: pref_title_reset  reason: collision with other field name */
        public static final int f1289pref_title_reset = 2131296596;
        /* added by JADX */

        /* renamed from: pref_item_title_reset  reason: collision with other field name */
        public static final int f1290pref_item_title_reset = 2131296597;
        /* added by JADX */

        /* renamed from: pref_item_msg_reset  reason: collision with other field name */
        public static final int f1291pref_item_msg_reset = 2131296598;
        /* added by JADX */

        /* renamed from: pref_item_title_save_and_exit  reason: collision with other field name */
        public static final int f1292pref_item_title_save_and_exit = 2131296599;
        /* added by JADX */

        /* renamed from: pref_title_flash  reason: collision with other field name */
        public static final int f1293pref_title_flash = 2131296600;
        /* added by JADX */

        /* renamed from: pref_item_title_install_flash  reason: collision with other field name */
        public static final int f1294pref_item_title_install_flash = 2131296601;
        /* added by JADX */

        /* renamed from: pref_title_security_settings  reason: collision with other field name */
        public static final int f1295pref_title_security_settings = 2131296602;
        /* added by JADX */

        /* renamed from: pref_item_title_website_hint  reason: collision with other field name */
        public static final int f1296pref_item_title_website_hint = 2131296603;
        /* added by JADX */

        /* renamed from: pref_item_title_download_prehint  reason: collision with other field name */
        public static final int f1297pref_item_title_download_prehint = 2131296604;
        /* added by JADX */

        /* renamed from: pref_item_title_download_posthint  reason: collision with other field name */
        public static final int f1298pref_item_title_download_posthint = 2131296605;
        /* added by JADX */

        /* renamed from: pref_item_title_history_record  reason: collision with other field name */
        public static final int f1299pref_item_title_history_record = 2131296606;
        /* added by JADX */

        /* renamed from: pref_item_title_most_visit_record  reason: collision with other field name */
        public static final int f1300pref_item_title_most_visit_record = 2131296607;
        /* added by JADX */

        /* renamed from: pref_title_preread  reason: collision with other field name */
        public static final int f1301pref_title_preread = 2131296608;
        /* added by JADX */

        /* renamed from: pref_item_title_wap_preread  reason: collision with other field name */
        public static final int f1302pref_item_title_wap_preread = 2131296609;
        /* added by JADX */

        /* renamed from: pref_item_title_www_preread  reason: collision with other field name */
        public static final int f1303pref_item_title_www_preread = 2131296610;
        /* added by JADX */

        /* renamed from: pref_title_system  reason: collision with other field name */
        public static final int f1304pref_title_system = 2131296611;
        /* added by JADX */

        /* renamed from: page_loading  reason: collision with other field name */
        public static final int f1305page_loading = 2131296612;
        /* added by JADX */

        /* renamed from: page_safe_loading_page  reason: collision with other field name */
        public static final int f1306page_safe_loading_page = 2131296613;
        /* added by JADX */

        /* renamed from: page_loading_page  reason: collision with other field name */
        public static final int f1307page_loading_page = 2131296614;
        /* added by JADX */

        /* renamed from: midlet_name  reason: collision with other field name */
        public static final int f1308midlet_name = 2131296615;
        /* added by JADX */

        /* renamed from: loadurl  reason: collision with other field name */
        public static final int f1309loadurl = 2131296616;
        /* added by JADX */

        /* renamed from: download  reason: collision with other field name */
        public static final int f1310download = 2131296617;
        /* added by JADX */

        /* renamed from: history  reason: collision with other field name */
        public static final int f1311history = 2131296618;
        /* added by JADX */

        /* renamed from: bookmark  reason: collision with other field name */
        public static final int f1312bookmark = 2131296619;
        /* added by JADX */

        /* renamed from: clearhistory  reason: collision with other field name */
        public static final int f1313clearhistory = 2131296620;
        /* added by JADX */

        /* renamed from: addhistory  reason: collision with other field name */
        public static final int f1314addhistory = 2131296621;
        /* added by JADX */

        /* renamed from: exit  reason: collision with other field name */
        public static final int f1315exit = 2131296622;
        /* added by JADX */

        /* renamed from: closeDownload  reason: collision with other field name */
        public static final int f1316closeDownload = 2131296623;
        /* added by JADX */

        /* renamed from: newDownloadTask  reason: collision with other field name */
        public static final int f1317newDownloadTask = 2131296624;
        /* added by JADX */

        /* renamed from: browserFileSystem  reason: collision with other field name */
        public static final int f1318browserFileSystem = 2131296625;
        /* added by JADX */

        /* renamed from: stopDownloadTask  reason: collision with other field name */
        public static final int f1319stopDownloadTask = 2131296626;
        /* added by JADX */

        /* renamed from: continueDonwloadTask  reason: collision with other field name */
        public static final int f1320continueDonwloadTask = 2131296627;
        /* added by JADX */

        /* renamed from: restartDonwloadTask  reason: collision with other field name */
        public static final int f1321restartDonwloadTask = 2131296628;
        /* added by JADX */

        /* renamed from: delFocuseDownloadTask  reason: collision with other field name */
        public static final int f1322delFocuseDownloadTask = 2131296629;
        /* added by JADX */

        /* renamed from: delAllDownloadTask  reason: collision with other field name */
        public static final int f1323delAllDownloadTask = 2131296630;
        /* added by JADX */

        /* renamed from: delDownloadtask  reason: collision with other field name */
        public static final int f1324delDownloadtask = 2131296631;
        /* added by JADX */

        /* renamed from: showDownloadTaskMessage  reason: collision with other field name */
        public static final int f1325showDownloadTaskMessage = 2131296632;
        /* added by JADX */

        /* renamed from: showRenameDlg  reason: collision with other field name */
        public static final int f1326showRenameDlg = 2131296633;
        /* added by JADX */

        /* renamed from: addbookmark  reason: collision with other field name */
        public static final int f1327addbookmark = 2131296634;
        /* added by JADX */

        /* renamed from: editbookmark  reason: collision with other field name */
        public static final int f1328editbookmark = 2131296635;
        /* added by JADX */

        /* renamed from: addbookmarkdir  reason: collision with other field name */
        public static final int f1329addbookmarkdir = 2131296636;
        /* added by JADX */

        /* renamed from: editbookmarkdir  reason: collision with other field name */
        public static final int f1330editbookmarkdir = 2131296637;
        /* added by JADX */

        /* renamed from: delbookmark  reason: collision with other field name */
        public static final int f1331delbookmark = 2131296638;
        /* added by JADX */

        /* renamed from: delbookmarkdir  reason: collision with other field name */
        public static final int f1332delbookmarkdir = 2131296639;
        /* added by JADX */

        /* renamed from: delallbookmark  reason: collision with other field name */
        public static final int f1333delallbookmark = 2131296640;
        /* added by JADX */

        /* renamed from: controlbar_download_clear  reason: collision with other field name */
        public static final int f1334controlbar_download_clear = 2131296641;
        /* added by JADX */

        /* renamed from: downloadSafeScan  reason: collision with other field name */
        public static final int f1335downloadSafeScan = 2131296642;
        /* added by JADX */

        /* renamed from: downloadSafeComment  reason: collision with other field name */
        public static final int f1336downloadSafeComment = 2131296643;
        /* added by JADX */

        /* renamed from: browserFrameRedirect  reason: collision with other field name */
        public static final int f1337browserFrameRedirect = 2131296644;
        /* added by JADX */

        /* renamed from: browserFrame307Post  reason: collision with other field name */
        public static final int f1338browserFrame307Post = 2131296645;
        /* added by JADX */

        /* renamed from: browserFrameFormResubmitLabel  reason: collision with other field name */
        public static final int f1339browserFrameFormResubmitLabel = 2131296647;
        /* added by JADX */

        /* renamed from: browserFrameFormResubmitMessage  reason: collision with other field name */
        public static final int f1340browserFrameFormResubmitMessage = 2131296648;
        /* added by JADX */

        /* renamed from: downLoad  reason: collision with other field name */
        public static final int f1341downLoad = 2131296649;
        /* added by JADX */

        /* renamed from: file_saved_as  reason: collision with other field name */
        public static final int f1342file_saved_as = 2131296650;
        /* added by JADX */

        /* renamed from: download_filename  reason: collision with other field name */
        public static final int f1343download_filename = 2131296651;
        /* added by JADX */

        /* renamed from: dialog_title_download_task_unbreakable  reason: collision with other field name */
        public static final int f1344dialog_title_download_task_unbreakable = 2131296652;
        /* added by JADX */

        /* renamed from: dialog_msg_download_task_unbreakable  reason: collision with other field name */
        public static final int f1345dialog_msg_download_task_unbreakable = 2131296653;
        /* added by JADX */

        /* renamed from: install_dialog_title  reason: collision with other field name */
        public static final int f1346install_dialog_title = 2131296654;
        /* added by JADX */

        /* renamed from: install_dlg_msg_finished  reason: collision with other field name */
        public static final int f1347install_dlg_msg_finished = 2131296655;
        /* added by JADX */

        /* renamed from: install_dlg_toast_finished  reason: collision with other field name */
        public static final int f1348install_dlg_toast_finished = 2131296656;
        /* added by JADX */

        /* renamed from: install_dlg_msg_open  reason: collision with other field name */
        public static final int f1349install_dlg_msg_open = 2131296657;
        /* added by JADX */

        /* renamed from: install_dlg_msg_install  reason: collision with other field name */
        public static final int f1350install_dlg_msg_install = 2131296658;
        /* added by JADX */

        /* renamed from: install_dlg_msg_install_safe  reason: collision with other field name */
        public static final int f1351install_dlg_msg_install_safe = 2131296659;
        /* added by JADX */

        /* renamed from: install_dlg_msg_install_risk  reason: collision with other field name */
        public static final int f1352install_dlg_msg_install_risk = 2131296660;
        /* added by JADX */

        /* renamed from: install_dlg_msg_install_unknow  reason: collision with other field name */
        public static final int f1353install_dlg_msg_install_unknow = 2131296661;
        /* added by JADX */

        /* renamed from: browser_dialog_page_attrs  reason: collision with other field name */
        public static final int f1354browser_dialog_page_attrs = 2131296662;
        /* added by JADX */

        /* renamed from: browser_page_attrs_address  reason: collision with other field name */
        public static final int f1355browser_page_attrs_address = 2131296663;
        /* added by JADX */

        /* renamed from: dialog_pageattr_title  reason: collision with other field name */
        public static final int f1356dialog_pageattr_title = 2131296664;
        /* added by JADX */

        /* renamed from: dialog_pageattr_copy  reason: collision with other field name */
        public static final int f1357dialog_pageattr_copy = 2131296665;
        /* added by JADX */

        /* renamed from: dialog_pageattr_address  reason: collision with other field name */
        public static final int f1358dialog_pageattr_address = 2131296666;
        /* added by JADX */

        /* renamed from: dialog_pageattr_href_url  reason: collision with other field name */
        public static final int f1359dialog_pageattr_href_url = 2131296667;
        /* added by JADX */

        /* renamed from: dialog_pageattr_image_url  reason: collision with other field name */
        public static final int f1360dialog_pageattr_image_url = 2131296668;
        /* added by JADX */

        /* renamed from: dialog_pageattr_msg_copy  reason: collision with other field name */
        public static final int f1361dialog_pageattr_msg_copy = 2131296669;
        /* added by JADX */

        /* renamed from: contextmenu_openlink_newwindow  reason: collision with other field name */
        public static final int f1362contextmenu_openlink_newwindow = 2131296670;
        /* added by JADX */

        /* renamed from: contextmenu_openlink_background  reason: collision with other field name */
        public static final int f1363contextmenu_openlink_background = 2131296671;
        /* added by JADX */

        /* renamed from: contextmenu_createwindow  reason: collision with other field name */
        public static final int f1364contextmenu_createwindow = 2131296672;
        /* added by JADX */

        /* renamed from: contextmenu_closecurrentwindow  reason: collision with other field name */
        public static final int f1365contextmenu_closecurrentwindow = 2131296673;
        /* added by JADX */

        /* renamed from: contextmenu_bookmark_thislink  reason: collision with other field name */
        public static final int f1366contextmenu_bookmark_thislink = 2131296674;
        /* added by JADX */

        /* renamed from: contextmenu_download  reason: collision with other field name */
        public static final int f1367contextmenu_download = 2131296675;
        /* added by JADX */

        /* renamed from: contextmenu_download_image  reason: collision with other field name */
        public static final int f1368contextmenu_download_image = 2131296676;
        /* added by JADX */

        /* renamed from: contextmenu_view_image  reason: collision with other field name */
        public static final int f1369contextmenu_view_image = 2131296677;
        /* added by JADX */

        /* renamed from: contextmenu_pageattrs  reason: collision with other field name */
        public static final int f1370contextmenu_pageattrs = 2131296678;
        /* added by JADX */

        /* renamed from: contextmenu_quicksearch  reason: collision with other field name */
        public static final int f1371contextmenu_quicksearch = 2131296679;
        /* added by JADX */

        /* renamed from: savecurrentlink  reason: collision with other field name */
        public static final int f1372savecurrentlink = 2131296680;
        /* added by JADX */

        /* renamed from: pictureview  reason: collision with other field name */
        public static final int f1373pictureview = 2131296681;
        /* added by JADX */

        /* renamed from: pictureoriginview  reason: collision with other field name */
        public static final int f1374pictureoriginview = 2131296682;
        /* added by JADX */

        /* renamed from: addtobookmark  reason: collision with other field name */
        public static final int f1375addtobookmark = 2131296683;
        /* added by JADX */

        /* renamed from: pageattrs  reason: collision with other field name */
        public static final int f1376pageattrs = 2131296684;
        /* added by JADX */

        /* renamed from: shortcutsearch  reason: collision with other field name */
        public static final int f1377shortcutsearch = 2131296685;
        /* added by JADX */

        /* renamed from: opennewwindow  reason: collision with other field name */
        public static final int f1378opennewwindow = 2131296686;
        /* added by JADX */

        /* renamed from: openinnewwindow  reason: collision with other field name */
        public static final int f1379openinnewwindow = 2131296687;
        /* added by JADX */

        /* renamed from: closecurrentwindow  reason: collision with other field name */
        public static final int f1380closecurrentwindow = 2131296688;
        /* added by JADX */

        /* renamed from: toast_stopping  reason: collision with other field name */
        public static final int f1381toast_stopping = 2131296689;
        /* added by JADX */

        /* renamed from: notsupportrefreshtimer  reason: collision with other field name */
        public static final int f1382notsupportrefreshtimer = 2131296690;
        /* added by JADX */

        /* renamed from: notsupportrefreshtimerzoom  reason: collision with other field name */
        public static final int f1383notsupportrefreshtimerzoom = 2131296691;
        /* added by JADX */

        /* renamed from: notsupportrefreshtimerHomePage  reason: collision with other field name */
        public static final int f1384notsupportrefreshtimerHomePage = 2131296692;
        /* added by JADX */

        /* renamed from: notsupportrefreshtimerZip  reason: collision with other field name */
        public static final int f1385notsupportrefreshtimerZip = 2131296693;
        /* added by JADX */

        /* renamed from: pleaseinputintervaltimer  reason: collision with other field name */
        public static final int f1386pleaseinputintervaltimer = 2131296694;
        /* added by JADX */

        /* renamed from: refreshprompt  reason: collision with other field name */
        public static final int f1387refreshprompt = 2131296695;
        /* added by JADX */

        /* renamed from: notchangetheme  reason: collision with other field name */
        public static final int f1388notchangetheme = 2131296696;
        /* added by JADX */

        /* renamed from: tcardnotusable  reason: collision with other field name */
        public static final int f1389tcardnotusable = 2131296697;
        /* added by JADX */

        /* renamed from: cannotrefreshtimeragain  reason: collision with other field name */
        public static final int f1390cannotrefreshtimeragain = 2131296698;
        /* added by JADX */

        /* renamed from: allowlessthenfour  reason: collision with other field name */
        public static final int f1391allowlessthenfour = 2131296699;
        /* added by JADX */

        /* renamed from: dirnamenotnull  reason: collision with other field name */
        public static final int f1392dirnamenotnull = 2131296700;
        /* added by JADX */

        /* renamed from: bookmarknamenotnull  reason: collision with other field name */
        public static final int f1393bookmarknamenotnull = 2131296701;
        /* added by JADX */

        /* renamed from: bookmarkvalueotnull  reason: collision with other field name */
        public static final int f1394bookmarkvalueotnull = 2131296702;
        /* added by JADX */

        /* renamed from: dirisexists  reason: collision with other field name */
        public static final int f1395dirisexists = 2131296703;
        /* added by JADX */

        /* renamed from: bookmarkisexists  reason: collision with other field name */
        public static final int f1396bookmarkisexists = 2131296704;
        /* added by JADX */

        /* renamed from: confirmcoverbookmark  reason: collision with other field name */
        public static final int f1397confirmcoverbookmark = 2131296705;
        /* added by JADX */

        /* renamed from: bookmarkfileexists  reason: collision with other field name */
        public static final int f1398bookmarkfileexists = 2131296706;
        /* added by JADX */

        /* renamed from: confirmcoverbookmarkfile  reason: collision with other field name */
        public static final int f1399confirmcoverbookmarkfile = 2131296707;
        /* added by JADX */

        /* renamed from: avaliablespacenotenough  reason: collision with other field name */
        public static final int f1400avaliablespacenotenough = 2131296708;
        /* added by JADX */

        /* renamed from: import_bookmark  reason: collision with other field name */
        public static final int f1401import_bookmark = 2131296709;
        /* added by JADX */

        /* renamed from: export_bookmark  reason: collision with other field name */
        public static final int f1402export_bookmark = 2131296710;
        /* added by JADX */

        /* renamed from: importbookmarksuccess  reason: collision with other field name */
        public static final int f1403importbookmarksuccess = 2131296711;
        /* added by JADX */

        /* renamed from: exportbookmarksuccess  reason: collision with other field name */
        public static final int f1404exportbookmarksuccess = 2131296712;
        /* added by JADX */

        /* renamed from: importbookmarkfail  reason: collision with other field name */
        public static final int f1405importbookmarkfail = 2131296713;
        /* added by JADX */

        /* renamed from: exportbookmarkfail  reason: collision with other field name */
        public static final int f1406exportbookmarkfail = 2131296714;
        /* added by JADX */

        /* renamed from: coversamename  reason: collision with other field name */
        public static final int f1407coversamename = 2131296715;
        /* added by JADX */

        /* renamed from: ignoresamename  reason: collision with other field name */
        public static final int f1408ignoresamename = 2131296716;
        /* added by JADX */

        /* renamed from: bookmarkname  reason: collision with other field name */
        public static final int f1409bookmarkname = 2131296717;
        /* added by JADX */

        /* renamed from: bookmarkaddress  reason: collision with other field name */
        public static final int f1410bookmarkaddress = 2131296718;
        /* added by JADX */

        /* renamed from: bookmarkdir  reason: collision with other field name */
        public static final int f1411bookmarkdir = 2131296719;
        /* added by JADX */

        /* renamed from: maxwindownumber  reason: collision with other field name */
        public static final int f1412maxwindownumber = 2131296720;
        /* added by JADX */

        /* renamed from: out_of_memory  reason: collision with other field name */
        public static final int f1413out_of_memory = 2131296721;
        /* added by JADX */

        /* renamed from: page_waiting  reason: collision with other field name */
        public static final int f1414page_waiting = 2131296722;
        /* added by JADX */

        /* renamed from: page_waiting_failure  reason: collision with other field name */
        public static final int f1415page_waiting_failure = 2131296723;
        /* added by JADX */

        /* renamed from: quit_dlg_title  reason: collision with other field name */
        public static final int f1416quit_dlg_title = 2131296724;
        /* added by JADX */

        /* renamed from: quit_note_download  reason: collision with other field name */
        public static final int f1417quit_note_download = 2131296725;
        /* added by JADX */

        /* renamed from: web_user_agent  reason: collision with other field name */
        public static final int f1418web_user_agent = 2131296726;
        /* added by JADX */

        /* renamed from: mime_apk  reason: collision with other field name */
        public static final int f1419mime_apk = 2131296727;
        /* added by JADX */

        /* renamed from: mime_audio  reason: collision with other field name */
        public static final int f1420mime_audio = 2131296728;
        /* added by JADX */

        /* renamed from: mime_image  reason: collision with other field name */
        public static final int f1421mime_image = 2131296729;
        /* added by JADX */

        /* renamed from: mime_video  reason: collision with other field name */
        public static final int f1422mime_video = 2131296730;
        /* added by JADX */

        /* renamed from: mime_text  reason: collision with other field name */
        public static final int f1423mime_text = 2131296731;
        /* added by JADX */

        /* renamed from: action_add_shortcut  reason: collision with other field name */
        public static final int f1424action_add_shortcut = 2131296732;
        /* added by JADX */

        /* renamed from: uptitle  reason: collision with other field name */
        public static final int f1425uptitle = 2131296733;
        /* added by JADX */

        /* renamed from: upmsg  reason: collision with other field name */
        public static final int f1426upmsg = 2131296734;
        /* added by JADX */

        /* renamed from: checkupdateu  reason: collision with other field name */
        public static final int f1427checkupdateu = 2131296735;
        /* added by JADX */

        /* renamed from: fileupload  reason: collision with other field name */
        public static final int f1428fileupload = 2131296736;
        /* added by JADX */

        /* renamed from: upload_info_format  reason: collision with other field name */
        public static final int f1429upload_info_format = 2131296737;
        /* added by JADX */

        /* renamed from: bookmarkfilenotnull  reason: collision with other field name */
        public static final int f1430bookmarkfilenotnull = 2131296738;
        /* added by JADX */

        /* renamed from: disclaimer  reason: collision with other field name */
        public static final int f1431disclaimer = 2131296739;
        /* added by JADX */

        /* renamed from: disclaimeraccept  reason: collision with other field name */
        public static final int f1432disclaimeraccept = 2131296740;
        /* added by JADX */

        /* renamed from: disclaimerdeny  reason: collision with other field name */
        public static final int f1433disclaimerdeny = 2131296741;
        /* added by JADX */

        /* renamed from: nolongerprompt  reason: collision with other field name */
        public static final int f1434nolongerprompt = 2131296742;
        /* added by JADX */

        /* renamed from: declaretion  reason: collision with other field name */
        public static final int f1435declaretion = 2131296743;
        /* added by JADX */

        /* renamed from: penselectmode  reason: collision with other field name */
        public static final int f1436penselectmode = 2131296744;
        /* added by JADX */

        /* renamed from: text_copied  reason: collision with other field name */
        public static final int f1437text_copied = 2131296745;
        /* added by JADX */

        /* renamed from: close_refreshtimer_note  reason: collision with other field name */
        public static final int f1438close_refreshtimer_note = 2131296746;
        /* added by JADX */

        /* renamed from: need_rename  reason: collision with other field name */
        public static final int f1439need_rename = 2131296747;
        /* added by JADX */

        /* renamed from: invalid_name  reason: collision with other field name */
        public static final int f1440invalid_name = 2131296748;
        /* added by JADX */

        /* renamed from: name_exist  reason: collision with other field name */
        public static final int f1441name_exist = 2131296749;
        /* added by JADX */

        /* renamed from: input_newname  reason: collision with other field name */
        public static final int f1442input_newname = 2131296750;
        /* added by JADX */

        /* renamed from: reinput_note  reason: collision with other field name */
        public static final int f1443reinput_note = 2131296751;
        /* added by JADX */

        /* renamed from: filenamenotnull  reason: collision with other field name */
        public static final int f1444filenamenotnull = 2131296752;
        /* added by JADX */

        /* renamed from: file_upload_too_large  reason: collision with other field name */
        public static final int f1445file_upload_too_large = 2131296753;
        /* added by JADX */

        /* renamed from: file_upload_supportless_type  reason: collision with other field name */
        public static final int f1446file_upload_supportless_type = 2131296754;
        /* added by JADX */

        /* renamed from: camera_fail  reason: collision with other field name */
        public static final int f1447camera_fail = 2131296755;
        /* added by JADX */

        /* renamed from: sharepage_tips_content  reason: collision with other field name */
        public static final int f1448sharepage_tips_content = 2131296756;
        /* added by JADX */

        /* renamed from: sharepage_update_tips  reason: collision with other field name */
        public static final int f1449sharepage_update_tips = 2131296757;
        /* added by JADX */

        /* renamed from: sharepage_update_failure  reason: collision with other field name */
        public static final int f1450sharepage_update_failure = 2131296758;
        /* added by JADX */

        /* renamed from: scan_contact_tip  reason: collision with other field name */
        public static final int f1451scan_contact_tip = 2131296759;
        /* added by JADX */

        /* renamed from: file_notin_sdcard  reason: collision with other field name */
        public static final int f1452file_notin_sdcard = 2131296760;
        /* added by JADX */

        /* renamed from: btn_submit_text  reason: collision with other field name */
        public static final int f1453btn_submit_text = 2131296761;
        /* added by JADX */

        /* renamed from: btn_cancle_text  reason: collision with other field name */
        public static final int f1454btn_cancle_text = 2131296762;
        /* added by JADX */

        /* renamed from: plugin_install_title  reason: collision with other field name */
        public static final int f1455plugin_install_title = 2131296763;
        /* added by JADX */

        /* renamed from: plugin_install_waiting  reason: collision with other field name */
        public static final int f1456plugin_install_waiting = 2131296764;
        /* added by JADX */

        /* renamed from: plugin_install_success  reason: collision with other field name */
        public static final int f1457plugin_install_success = 2131296765;
        /* added by JADX */

        /* renamed from: plugin_install_error_incorrect_format  reason: collision with other field name */
        public static final int f1458plugin_install_error_incorrect_format = 2131296766;
        /* added by JADX */

        /* renamed from: plugin_install_error_no_space  reason: collision with other field name */
        public static final int f1459plugin_install_error_no_space = 2131296767;
        /* added by JADX */

        /* renamed from: plugin_install_error_need_restart  reason: collision with other field name */
        public static final int f1460plugin_install_error_need_restart = 2131296768;
        /* added by JADX */

        /* renamed from: plugin_install_query  reason: collision with other field name */
        public static final int f1461plugin_install_query = 2131296769;
        /* added by JADX */

        /* renamed from: bookmark_submit  reason: collision with other field name */
        public static final int f1462bookmark_submit = 2131296770;
        /* added by JADX */

        /* renamed from: bookmark_cancle  reason: collision with other field name */
        public static final int f1463bookmark_cancle = 2131296771;
        /* added by JADX */

        /* renamed from: dir_root  reason: collision with other field name */
        public static final int f1464dir_root = 2131296772;
        /* added by JADX */

        /* renamed from: add_sch_default_searchengine  reason: collision with other field name */
        public static final int f1465add_sch_default_searchengine = 2131296773;
        /* added by JADX */

        /* renamed from: add_cancle  reason: collision with other field name */
        public static final int f1466add_cancle = 2131296774;
        /* added by JADX */

        /* renamed from: add_enter  reason: collision with other field name */
        public static final int f1467add_enter = 2131296775;
        /* added by JADX */

        /* renamed from: sch_search  reason: collision with other field name */
        public static final int f1468sch_search = 2131296776;
        /* added by JADX */

        /* renamed from: sch_cancel  reason: collision with other field name */
        public static final int f1469sch_cancel = 2131296777;
        /* added by JADX */

        /* renamed from: add_sch_search  reason: collision with other field name */
        public static final int f1470add_sch_search = 2131296778;
        /* added by JADX */

        /* renamed from: add_sch_inputaddress  reason: collision with other field name */
        public static final int f1471add_sch_inputaddress = 2131296779;
        /* added by JADX */

        /* renamed from: quit_tip  reason: collision with other field name */
        public static final int f1472quit_tip = 2131296780;
        /* added by JADX */

        /* renamed from: clear_all_browser_hostory  reason: collision with other field name */
        public static final int f1473clear_all_browser_hostory = 2131296781;
        /* added by JADX */

        /* renamed from: dont_tips_again  reason: collision with other field name */
        public static final int f1474dont_tips_again = 2131296782;
        /* added by JADX */

        /* renamed from: tap_again_to_exit  reason: collision with other field name */
        public static final int f1475tap_again_to_exit = 2131296783;
        /* added by JADX */

        /* renamed from: navi_already_exist  reason: collision with other field name */
        public static final int f1476navi_already_exist = 2131296784;
        /* added by JADX */

        /* renamed from: navi_not_allow_null  reason: collision with other field name */
        public static final int f1477navi_not_allow_null = 2131296785;
        /* added by JADX */

        /* renamed from: navi_dialog_delete_title  reason: collision with other field name */
        public static final int f1478navi_dialog_delete_title = 2131296786;
        /* added by JADX */

        /* renamed from: navi_dialog_delete_msg  reason: collision with other field name */
        public static final int f1479navi_dialog_delete_msg = 2131296787;
        /* added by JADX */

        /* renamed from: mynavi_message_show  reason: collision with other field name */
        public static final int f1480mynavi_message_show = 2131296788;
        /* added by JADX */

        /* renamed from: add_to_navigation  reason: collision with other field name */
        public static final int f1481add_to_navigation = 2131296789;
        /* added by JADX */

        /* renamed from: add_to_bookmark  reason: collision with other field name */
        public static final int f1482add_to_bookmark = 2131296790;
        /* added by JADX */

        /* renamed from: add_to_bookmark_dialog_title  reason: collision with other field name */
        public static final int f1483add_to_bookmark_dialog_title = 2131296791;
        /* added by JADX */

        /* renamed from: navi_full  reason: collision with other field name */
        public static final int f1484navi_full = 2131296792;
        /* added by JADX */

        /* renamed from: network_error  reason: collision with other field name */
        public static final int f1485network_error = 2131296793;
        /* added by JADX */

        /* renamed from: buz_channel_load_failed  reason: collision with other field name */
        public static final int f1486buz_channel_load_failed = 2131296794;
        /* added by JADX */

        /* renamed from: navi_load_failed  reason: collision with other field name */
        public static final int f1487navi_load_failed = 2131296795;
        /* added by JADX */

        /* renamed from: network_check_guid  reason: collision with other field name */
        public static final int f1488network_check_guid = 2131296796;
        /* added by JADX */

        /* renamed from: changed_to_landscape  reason: collision with other field name */
        public static final int f1489changed_to_landscape = 2131296797;
        /* added by JADX */

        /* renamed from: changed_to_portstrait  reason: collision with other field name */
        public static final int f1490changed_to_portstrait = 2131296798;
        /* added by JADX */

        /* renamed from: juc_ua_header  reason: collision with other field name */
        public static final int f1491juc_ua_header = 2131296799;
        /* added by JADX */

        /* renamed from: webkit_ua_header  reason: collision with other field name */
        public static final int f1492webkit_ua_header = 2131296800;
        /* added by JADX */

        /* renamed from: default_release  reason: collision with other field name */
        public static final int f1493default_release = 2131296801;
        /* added by JADX */

        /* renamed from: default_language  reason: collision with other field name */
        public static final int f1494default_language = 2131296802;
        /* added by JADX */

        /* renamed from: browser_name  reason: collision with other field name */
        public static final int f1495browser_name = 2131296803;
        /* added by JADX */

        /* renamed from: juc  reason: collision with other field name */
        public static final int f1496juc = 2131296804;
        /* added by JADX */

        /* renamed from: openwave  reason: collision with other field name */
        public static final int f1497openwave = 2131296805;
        /* added by JADX */

        /* renamed from: opera  reason: collision with other field name */
        public static final int f1498opera = 2131296806;
        /* added by JADX */

        /* renamed from: mozilla  reason: collision with other field name */
        public static final int f1499mozilla = 2131296807;
        /* added by JADX */

        /* renamed from: webkit_tail  reason: collision with other field name */
        public static final int f1500webkit_tail = 2131296808;
        /* added by JADX */

        /* renamed from: save_image_sdcard_error  reason: collision with other field name */
        public static final int f1501save_image_sdcard_error = 2131296809;
        /* added by JADX */

        /* renamed from: save_image_filename_empty  reason: collision with other field name */
        public static final int f1502save_image_filename_empty = 2131296810;
        /* added by JADX */

        /* renamed from: save_image_file_exist  reason: collision with other field name */
        public static final int f1503save_image_file_exist = 2131296811;
        /* added by JADX */

        /* renamed from: save_image_file_name_error  reason: collision with other field name */
        public static final int f1504save_image_file_name_error = 2131296812;
        /* added by JADX */

        /* renamed from: save_image_file_name_toolong  reason: collision with other field name */
        public static final int f1505save_image_file_name_toolong = 2131296813;
        /* added by JADX */

        /* renamed from: save_image_input_file_name  reason: collision with other field name */
        public static final int f1506save_image_input_file_name = 2131296814;
        /* added by JADX */

        /* renamed from: save_image_to  reason: collision with other field name */
        public static final int f1507save_image_to = 2131296815;
        /* added by JADX */

        /* renamed from: save_image_error  reason: collision with other field name */
        public static final int f1508save_image_error = 2131296816;
        /* added by JADX */

        /* renamed from: save_image_dialog_title  reason: collision with other field name */
        public static final int f1509save_image_dialog_title = 2131296817;
        /* added by JADX */

        /* renamed from: save_source_sdcard_error  reason: collision with other field name */
        public static final int f1510save_source_sdcard_error = 2131296818;
        /* added by JADX */

        /* renamed from: save_source_filename_empty  reason: collision with other field name */
        public static final int f1511save_source_filename_empty = 2131296819;
        /* added by JADX */

        /* renamed from: save_source_file_exist  reason: collision with other field name */
        public static final int f1512save_source_file_exist = 2131296820;
        /* added by JADX */

        /* renamed from: save_source_file_name_error  reason: collision with other field name */
        public static final int f1513save_source_file_name_error = 2131296821;
        /* added by JADX */

        /* renamed from: save_source_file_name_toolong  reason: collision with other field name */
        public static final int f1514save_source_file_name_toolong = 2131296822;
        /* added by JADX */

        /* renamed from: save_source_input_file_name  reason: collision with other field name */
        public static final int f1515save_source_input_file_name = 2131296823;
        /* added by JADX */

        /* renamed from: save_source  reason: collision with other field name */
        public static final int f1516save_source = 2131296824;
        /* added by JADX */

        /* renamed from: save_source_webkit_info  reason: collision with other field name */
        public static final int f1517save_source_webkit_info = 2131296825;
        /* added by JADX */

        /* renamed from: save_source_loading_info  reason: collision with other field name */
        public static final int f1518save_source_loading_info = 2131296826;
        /* added by JADX */

        /* renamed from: save_source_to  reason: collision with other field name */
        public static final int f1519save_source_to = 2131296827;
        /* added by JADX */

        /* renamed from: open_source_error  reason: collision with other field name */
        public static final int f1520open_source_error = 2131296828;
        /* added by JADX */

        /* renamed from: file_not_found  reason: collision with other field name */
        public static final int f1521file_not_found = 2131296829;
        /* added by JADX */

        /* renamed from: preread_note  reason: collision with other field name */
        public static final int f1522preread_note = 2131296830;
        /* added by JADX */

        /* renamed from: fresh_us_data_dlg_title  reason: collision with other field name */
        public static final int f1523fresh_us_data_dlg_title = 2131296831;
        /* added by JADX */

        /* renamed from: fresh_us_data_dlg_msg  reason: collision with other field name */
        public static final int f1524fresh_us_data_dlg_msg = 2131296832;
        /* added by JADX */

        /* renamed from: fresh_us_data_ongoing  reason: collision with other field name */
        public static final int f1525fresh_us_data_ongoing = 2131296833;
        /* added by JADX */

        /* renamed from: fresh_us_data_complete  reason: collision with other field name */
        public static final int f1526fresh_us_data_complete = 2131296834;
        /* added by JADX */

        /* renamed from: goto_mainpage  reason: collision with other field name */
        public static final int f1527goto_mainpage = 2131296835;
        /* added by JADX */

        /* renamed from: fresh_us_data_error  reason: collision with other field name */
        public static final int f1528fresh_us_data_error = 2131296836;
        /* added by JADX */

        /* renamed from: alipay_sdcard_no_space  reason: collision with other field name */
        public static final int f1529alipay_sdcard_no_space = 2131296837;
        /* added by JADX */

        /* renamed from: alipay_sdcard_not_exists  reason: collision with other field name */
        public static final int f1530alipay_sdcard_not_exists = 2131296838;
        /* added by JADX */

        /* renamed from: alipay_is_donwload_title  reason: collision with other field name */
        public static final int f1531alipay_is_donwload_title = 2131296839;
        /* added by JADX */

        /* renamed from: alipay_is_download  reason: collision with other field name */
        public static final int f1532alipay_is_download = 2131296840;
        /* added by JADX */

        /* renamed from: alipay_yes  reason: collision with other field name */
        public static final int f1533alipay_yes = 2131296841;
        /* added by JADX */

        /* renamed from: alipay_no  reason: collision with other field name */
        public static final int f1534alipay_no = 2131296842;
        /* added by JADX */

        /* renamed from: freecopy_menu_copy  reason: collision with other field name */
        public static final int f1535freecopy_menu_copy = 2131296843;
        /* added by JADX */

        /* renamed from: freecopy_menu_search  reason: collision with other field name */
        public static final int f1536freecopy_menu_search = 2131296844;
        /* added by JADX */

        /* renamed from: freecopy_menu_share  reason: collision with other field name */
        public static final int f1537freecopy_menu_share = 2131296845;
        /* added by JADX */

        /* renamed from: uc_traffic_uc_save  reason: collision with other field name */
        public static final int f1538uc_traffic_uc_save = 2131296846;
        /* added by JADX */

        /* renamed from: uc_traffic  reason: collision with other field name */
        public static final int f1539uc_traffic = 2131296847;
        /* added by JADX */

        /* renamed from: uc_traffic_dialog_confirm  reason: collision with other field name */
        public static final int f1540uc_traffic_dialog_confirm = 2131296848;
        /* added by JADX */

        /* renamed from: uc_traffic_dialog_cancel  reason: collision with other field name */
        public static final int f1541uc_traffic_dialog_cancel = 2131296849;
        /* added by JADX */

        /* renamed from: uc_traffic_clear_data  reason: collision with other field name */
        public static final int f1542uc_traffic_clear_data = 2131296850;
        /* added by JADX */

        /* renamed from: uc_traffic_msg1  reason: collision with other field name */
        public static final int f1543uc_traffic_msg1 = 2131296851;
        /* added by JADX */

        /* renamed from: uc_traffic_msg2  reason: collision with other field name */
        public static final int f1544uc_traffic_msg2 = 2131296852;
        /* added by JADX */

        /* renamed from: uc_initial_tip1  reason: collision with other field name */
        public static final int f1545uc_initial_tip1 = 2131296853;
        /* added by JADX */

        /* renamed from: uc_initial_tip2  reason: collision with other field name */
        public static final int f1546uc_initial_tip2 = 2131296854;
        /* added by JADX */

        /* renamed from: uc_initial_tip3  reason: collision with other field name */
        public static final int f1547uc_initial_tip3 = 2131296855;
        /* added by JADX */

        /* renamed from: uc_initial_tip4  reason: collision with other field name */
        public static final int f1548uc_initial_tip4 = 2131296856;
        /* added by JADX */

        /* renamed from: uc_traffic_save_text1  reason: collision with other field name */
        public static final int f1549uc_traffic_save_text1 = 2131296857;
        /* added by JADX */

        /* renamed from: uc_traffic_save_text2  reason: collision with other field name */
        public static final int f1550uc_traffic_save_text2 = 2131296858;
        /* added by JADX */

        /* renamed from: uc_traffic_save_title  reason: collision with other field name */
        public static final int f1551uc_traffic_save_title = 2131296859;
        /* added by JADX */

        /* renamed from: readingmode_dlg_title  reason: collision with other field name */
        public static final int f1552readingmode_dlg_title = 2131296860;
        /* added by JADX */

        /* renamed from: readingmode_dlg_smooth  reason: collision with other field name */
        public static final int f1553readingmode_dlg_smooth = 2131296861;
        /* added by JADX */

        /* renamed from: readingmode_dlg_smooth_dis  reason: collision with other field name */
        public static final int f1554readingmode_dlg_smooth_dis = 2131296862;
        /* added by JADX */

        /* renamed from: readingmode_dlg_record  reason: collision with other field name */
        public static final int f1555readingmode_dlg_record = 2131296863;
        /* added by JADX */

        /* renamed from: readingmode_dlg_record_dis  reason: collision with other field name */
        public static final int f1556readingmode_dlg_record_dis = 2131296864;
    }

    public final class style {

        /* renamed from: aCA */
        public static final int AutoCompleteTextView = 2131361804;

        /* renamed from: aCB */
        public static final int BrowserStart = 2131361807;

        /* renamed from: aCC */
        public static final int BrowserTheme = 2131361806;

        /* renamed from: aCD */
        public static final int Editor = 2131361809;

        /* renamed from: aCE */
        public static final int EditorActivityAnimation = 2131361805;

        /* renamed from: aCF */
        public static final int Foo = 2131361808;

        /* renamed from: aCG */
        public static final int Transparent = 2131361816;

        /* renamed from: aCH */
        public static final int Transparent2 = 2131361817;

        /* renamed from: aCI */
        public static final int Transparent3 = 2131361818;

        /* renamed from: aCJ */
        public static final int bookmark_dialog_transparent = 2131361814;

        /* renamed from: aCK */
        public static final int context_menu = 2131361810;

        /* renamed from: aCL */
        public static final int context_menu_anim = 2131361793;

        /* renamed from: aCM */
        public static final int controlbar2_button_image = 2131361798;

        /* renamed from: aCN */
        public static final int controlbar_barlayout = 2131361795;

        /* renamed from: aCO */
        public static final int controlbar_button_image = 2131361797;

        /* renamed from: aCP */
        public static final int controlbar_button_layout = 2131361794;

        /* renamed from: aCQ */
        public static final int controlbar_button_text = 2131361796;

        /* renamed from: aCR */
        public static final int dialog_transparent = 2131361815;

        /* renamed from: aCS */
        public static final int fullscreen_controlbar_image = 2131361803;

        /* renamed from: aCT */
        public static final int maincontent_button_text = 2131361802;

        /* renamed from: aCU */
        public static final int maincontent_list_text = 2131361799;

        /* renamed from: aCV */
        public static final int multi_window_dialog = 2131361811;

        /* renamed from: aCW */
        public static final int multiwindow_dialog_animation = 2131361792;

        /* renamed from: aCX */
        public static final int notice_dialog = 2131361813;

        /* renamed from: aCY */
        public static final int page_loading = 2131361801;

        /* renamed from: aCZ */
        public static final int spinner_list_text = 2131361800;

        /* renamed from: are */
        public static final int quit_dialog = 2131361812;
        /* added by JADX */

        /* renamed from: multiwindow_dialog_animation  reason: collision with other field name */
        public static final int f1557multiwindow_dialog_animation = 2131361792;
        /* added by JADX */

        /* renamed from: context_menu_anim  reason: collision with other field name */
        public static final int f1558context_menu_anim = 2131361793;
        /* added by JADX */

        /* renamed from: controlbar_button_layout  reason: collision with other field name */
        public static final int f1559controlbar_button_layout = 2131361794;
        /* added by JADX */

        /* renamed from: controlbar_barlayout  reason: collision with other field name */
        public static final int f1560controlbar_barlayout = 2131361795;
        /* added by JADX */

        /* renamed from: controlbar_button_text  reason: collision with other field name */
        public static final int f1561controlbar_button_text = 2131361796;
        /* added by JADX */

        /* renamed from: controlbar_button_image  reason: collision with other field name */
        public static final int f1562controlbar_button_image = 2131361797;
        /* added by JADX */

        /* renamed from: controlbar2_button_image  reason: collision with other field name */
        public static final int f1563controlbar2_button_image = 2131361798;
        /* added by JADX */

        /* renamed from: maincontent_list_text  reason: collision with other field name */
        public static final int f1564maincontent_list_text = 2131361799;
        /* added by JADX */

        /* renamed from: spinner_list_text  reason: collision with other field name */
        public static final int f1565spinner_list_text = 2131361800;
        /* added by JADX */

        /* renamed from: page_loading  reason: collision with other field name */
        public static final int f1566page_loading = 2131361801;
        /* added by JADX */

        /* renamed from: maincontent_button_text  reason: collision with other field name */
        public static final int f1567maincontent_button_text = 2131361802;
        /* added by JADX */

        /* renamed from: fullscreen_controlbar_image  reason: collision with other field name */
        public static final int f1568fullscreen_controlbar_image = 2131361803;
        /* added by JADX */

        /* renamed from: AutoCompleteTextView  reason: collision with other field name */
        public static final int f1569AutoCompleteTextView = 2131361804;
        /* added by JADX */

        /* renamed from: EditorActivityAnimation  reason: collision with other field name */
        public static final int f1570EditorActivityAnimation = 2131361805;
        /* added by JADX */

        /* renamed from: BrowserTheme  reason: collision with other field name */
        public static final int f1571BrowserTheme = 2131361806;
        /* added by JADX */

        /* renamed from: BrowserStart  reason: collision with other field name */
        public static final int f1572BrowserStart = 2131361807;
        /* added by JADX */

        /* renamed from: Foo  reason: collision with other field name */
        public static final int f1573Foo = 2131361808;
        /* added by JADX */

        /* renamed from: Editor  reason: collision with other field name */
        public static final int f1574Editor = 2131361809;
        /* added by JADX */

        /* renamed from: context_menu  reason: collision with other field name */
        public static final int f1575context_menu = 2131361810;
        /* added by JADX */

        /* renamed from: multi_window_dialog  reason: collision with other field name */
        public static final int f1576multi_window_dialog = 2131361811;
        /* added by JADX */

        /* renamed from: quit_dialog  reason: collision with other field name */
        public static final int f1577quit_dialog = 2131361812;
        /* added by JADX */

        /* renamed from: notice_dialog  reason: collision with other field name */
        public static final int f1578notice_dialog = 2131361813;
        /* added by JADX */

        /* renamed from: bookmark_dialog_transparent  reason: collision with other field name */
        public static final int f1579bookmark_dialog_transparent = 2131361814;
        /* added by JADX */

        /* renamed from: dialog_transparent  reason: collision with other field name */
        public static final int f1580dialog_transparent = 2131361815;
        /* added by JADX */

        /* renamed from: Transparent  reason: collision with other field name */
        public static final int f1581Transparent = 2131361816;
        /* added by JADX */

        /* renamed from: Transparent2  reason: collision with other field name */
        public static final int f1582Transparent2 = 2131361817;
        /* added by JADX */

        /* renamed from: Transparent3  reason: collision with other field name */
        public static final int f1583Transparent3 = 2131361818;
    }

    public final class styleable {
        public static final int aEA = 2;
        public static final int aEB = 1;
        public static final int aEC = 5;
        public static final int aED = 6;
        public static final int aEE = 7;
        public static final int aEF = 4;
        public static final int aEG = 3;
        public static final int aEH = 8;
        public static final int[] aEI = {R.attr.f77image_width, R.attr.f78image_height, R.attr.f79image_src};
        public static final int aEJ = 1;
        public static final int aEK = 2;
        public static final int aEL = 0;
        public static final int[] aEM = {R.attr.f61background_left, R.attr.f62background_right, R.attr.f63item_background_default, R.attr.f64item_background_selected, R.attr.f65item_background_focus, R.attr.f66item_background_pressed, R.attr.f67item_h_space, R.attr.f68item_height, R.attr.f69item_width, R.attr.f70item_image_height, R.attr.f71item_image_width, R.attr.f72shadow_width};
        public static final int aEN = 0;
        public static final int aEO = 1;
        public static final int aEP = 2;
        public static final int aEQ = 4;
        public static final int aER = 5;
        public static final int aES = 3;
        public static final int aET = 6;
        public static final int aEU = 9;
        public static final int aEV = 10;
        public static final int aEW = 8;
        public static final int aEX = 11;
        public static final int[] aEY = {R.attr.f73need_checkmark};
        public static final int aEZ = 0;
        public static final int[] aEy = {R.attr.background, R.attr.f80item_Width, R.attr.f81item_Height, R.attr.f82item_textSize, R.attr.f83item_textColor, R.attr.f84item_bgDefault, R.attr.f85item_bgFocus, R.attr.f86item_bgPress, R.attr.f87item_textVisibility};
        public static final int aEz = 0;
        public static final int[] aFa = {R.attr.f74num_size, R.attr.f75num_margin_left, R.attr.f76num_margin_top};
        public static final int aFb = 1;
        public static final int aFc = 2;
        public static final int aFd = 0;
        public static final int akq = 7;
    }
}
