package com.uc.browser;

import android.content.Context;
import android.graphics.Canvas;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import com.uc.e.a.e;
import com.uc.e.ad;
import java.util.Vector;

public class MultiWindowLayout extends View {
    e auC;

    public MultiWindowLayout(Context context, Vector vector) {
        super(context);
        this.auC = new e(vector);
        setFocusable(true);
        this.auC.a(new ad() {
            public void cJ() {
                MultiWindowLayout.this.invalidate();
            }
        });
    }

    public void c(Vector vector) {
        this.auC.c(vector);
        postInvalidate();
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return this.auC.c(keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.auC.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.auC.setSize(i3 - i, i4 - i2);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                return this.auC.b((byte) 0, (int) motionEvent.getX(), (int) motionEvent.getY());
            case 1:
                return this.auC.b((byte) 1, (int) motionEvent.getX(), (int) motionEvent.getY());
            case 2:
                return this.auC.b((byte) 2, (int) motionEvent.getX(), (int) motionEvent.getY());
            default:
                return false;
        }
    }

    public Vector xa() {
        return this.auC.BM();
    }
}
