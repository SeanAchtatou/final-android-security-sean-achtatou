package com.uc.browser;

import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.uc.browser.UCAlertDialog;
import com.uc.browser.UCR;
import com.uc.h.a;
import com.uc.h.e;

public class ViewCloseRefreshTimer extends RelativeLayout implements View.OnClickListener, a {
    private ImageView db;

    public ViewCloseRefreshTimer(Context context) {
        super(context);
        a();
    }

    public ViewCloseRefreshTimer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.uc.browser.ViewCloseRefreshTimer, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a() {
        LayoutInflater.from(getContext()).inflate((int) R.layout.f889close_timerrefresh, (ViewGroup) this, true);
        this.db = (ImageView) findViewById(R.id.f721close_refreshtimer);
        this.db.setOnClickListener(this);
        k();
        e.Pb().a(this);
    }

    public void k() {
        this.db.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aWX));
    }

    public void onClick(View view) {
        new UCAlertDialog.Builder(getContext()).aH(R.string.f1073menu_stoprefreshtimer).aG(R.string.f1438close_refreshtimer_note).a((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ModelBrowser.gD().aS(74);
            }
        }).c((int) R.string.cancel, (DialogInterface.OnClickListener) null).show();
    }
}
