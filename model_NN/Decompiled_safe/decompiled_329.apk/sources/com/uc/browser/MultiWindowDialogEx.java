package com.uc.browser;

import android.app.Dialog;
import android.content.Context;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.animation.Animation;

public class MultiWindowDialogEx extends Dialog implements Animation.AnimationListener {
    MultiWindowViewEx qy = new MultiWindowViewEx(getContext());
    Context qz;

    public MultiWindowDialogEx(Context context) {
        super(context, R.style.f1576multi_window_dialog);
        getWindow().setGravity(48);
        getWindow().addFlags(1056);
        getWindow().setLayout(-1, -1);
        setContentView(this.qy);
        this.qz = context;
    }

    public void dismiss() {
        findViewById(R.id.f655controlbar).setVisibility(4);
        this.qy.a(this);
    }

    public void onAnimationEnd(Animation animation) {
        super.dismiss();
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i != 84) {
            return super.onKeyUp(i, keyEvent);
        }
        if (!(ModelBrowser.gD() == null || ModelBrowser.gD() == null)) {
            ModelBrowser.gD().a((int) ModelBrowser.zJ, getContext());
        }
        dismiss();
        return true;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() != 1) {
            return super.onTouchEvent(motionEvent);
        }
        dismiss();
        return true;
    }

    public void show() {
        findViewById(R.id.f655controlbar).setVisibility(0);
        super.show();
        this.qy.q(this.qz);
    }
}
