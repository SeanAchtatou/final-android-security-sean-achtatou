package com.uc.browser;

import android.content.Context;
import android.content.res.ColorStateList;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.uc.browser.UCR;
import com.uc.h.e;
import java.util.Vector;

public class WebsiteSearchListAdapter extends BaseAdapter {
    private Context cr;
    protected Vector lO;
    private LayoutInflater tu;
    private TextView tv;

    public WebsiteSearchListAdapter(Context context, Vector vector) {
        this.tu = LayoutInflater.from(context);
        this.cr = context;
        if (vector != null) {
            this.lO = vector;
        } else {
            this.lO = new Vector();
        }
    }

    public int getCount() {
        return this.lO.size();
    }

    public Object getItem(int i) {
        return null;
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        int[][] iArr = {new int[]{16842919}, new int[0]};
        int[] iArr2 = {e.Pb().getColor(35), e.Pb().getColor(34)};
        View inflate = view == null ? this.tu.inflate((int) R.layout.f908listitem, (ViewGroup) null) : view;
        if (i % 2 == 0) {
            inflate.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aXg));
        } else {
            inflate.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aXh));
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, e.Pb().kp(R.dimen.f496add_sch_listitem_height));
        int kp = e.Pb().kp(R.dimen.f497add_sch_listitem_paddingleft);
        int kp2 = e.Pb().kp(R.dimen.f499add_sch_listitem_paddingbottom);
        int kp3 = e.Pb().kp(R.dimen.f498add_sch_listitem_paddingtop);
        this.tv = (TextView) inflate.findViewById(R.id.f778itemtext);
        this.tv.setText(((b.a.a.e) this.lO.get(i)).abL);
        this.tv.setLayoutParams(layoutParams);
        this.tv.setTextColor(new ColorStateList(iArr, iArr2));
        this.tv.setPadding(kp, kp3, kp, kp2);
        this.tv.setEllipsize(TextUtils.TruncateAt.MIDDLE);
        return inflate;
    }
}
