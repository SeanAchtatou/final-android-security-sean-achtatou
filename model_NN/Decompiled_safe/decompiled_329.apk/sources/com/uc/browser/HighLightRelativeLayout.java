package com.uc.browser;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

public class HighLightRelativeLayout extends RelativeLayout {
    public HighLightRelativeLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void childDrawableStateChanged(View view) {
        d(view.getDrawableState());
        super.childDrawableStateChanged(view);
    }

    /* access modifiers changed from: protected */
    public void d(int[] iArr) {
        for (int i = 0; i < getChildCount(); i++) {
            Drawable background = getChildAt(i).getBackground();
            if (background != null) {
                background.setState(iArr);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        d(getDrawableState());
        super.drawableStateChanged();
    }
}
