package com.uc.browser;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.uc.browser.UCR;
import com.uc.h.a;
import com.uc.h.e;

public class TitleBarLayout extends RelativeLayout implements a {
    public TitleBarLayout(Context context) {
        super(context);
        k();
        e.Pb().a(this);
    }

    public TitleBarLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        k();
        e.Pb().a(this);
    }

    public TitleBarLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        k();
        e.Pb().a(this);
    }

    public void k() {
        setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aXv));
        try {
            ((TextView) findViewById(R.id.f694Browser_TimeBar)).setTextColor(e.Pb().getColor(82));
            ((TextView) findViewById(R.id.f654Browser_TitleBar)).setTextColor(e.Pb().getColor(82));
        } catch (Exception e) {
        }
    }
}
