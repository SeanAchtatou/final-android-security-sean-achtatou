package com.uc.browser;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import com.uc.a.e;

public class UcwebContentProvider extends ContentProvider {
    private static final String AUTHORITY = "com.uc.browser.UcwebContentProvider";
    private static final String aZc = "getall";
    private static final int aZd = 1;
    private static final UriMatcher aZe = new UriMatcher(-1);
    private static final String btZ = "getversion";
    private static final String bua = "geticondata";
    private static final String[] bub = {"ver", "icondata"};
    private static final int buc = 2;
    private static final int bud = 3;

    static {
        aZe.addURI(AUTHORITY, aZc, 1);
        aZe.addURI(AUTHORITY, btZ, 2);
        aZe.addURI(AUTHORITY, bua, 3);
    }

    public int delete(Uri uri, String str, String[] strArr) {
        return 0;
    }

    public String getType(Uri uri) {
        return null;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    public boolean onCreate() {
        return false;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        String[] strArr3 = (strArr == null || strArr.length == 0) ? bub : strArr;
        switch (aZe.match(uri)) {
            case 1:
            case 2:
            case 3:
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        int length = strArr3.length;
        String[] strArr4 = new String[length];
        for (int i = 0; i < length; i++) {
            if (bub[0].equals(strArr3[i])) {
                strArr4[i] = e.nU();
            } else if (bub[1].equals(strArr3[i])) {
                strArr4[i] = e.nV();
            }
            if (strArr4[i] == null) {
                strArr4[i] = "";
            }
        }
        MatrixCursor matrixCursor = new MatrixCursor(strArr3);
        matrixCursor.moveToFirst();
        matrixCursor.addRow(strArr4);
        return matrixCursor;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        return 0;
    }
}
