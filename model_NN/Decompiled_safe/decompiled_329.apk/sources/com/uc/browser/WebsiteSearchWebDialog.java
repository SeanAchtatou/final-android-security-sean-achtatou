package com.uc.browser;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import b.a.a.e;
import b.a.a.f;
import com.uc.a.l;
import com.uc.browser.UCR;
import com.uc.h.a;

public class WebsiteSearchWebDialog extends Dialog implements a {
    /* access modifiers changed from: private */
    public InputMethodManager cA;
    private DialogInterface.OnDismissListener cB;
    /* access modifiers changed from: private */
    public e cC;
    /* access modifiers changed from: private */
    public boolean cD = false;
    private View cl;
    /* access modifiers changed from: private */
    public EditText cm;
    /* access modifiers changed from: private */
    public WebsiteSearchListView cn;
    /* access modifiers changed from: private */
    public TextView co;
    private RelativeLayout cp;
    private TextView cq;
    private Context cr;
    /* access modifiers changed from: private */
    public URLBarListener cs;
    /* access modifiers changed from: private */
    public String ct;
    /* access modifiers changed from: private */
    public String cu;
    /* access modifiers changed from: private */
    public AdapterAutoComplete cv;
    private int cw;
    private Drawable cx;
    private Drawable cy;
    private Drawable cz;

    public interface URLBarListener {
        void bS(String str);

        void dp();

        void onCancel();
    }

    public WebsiteSearchWebDialog(Context context) {
        super(context, R.style.f1582Transparent2);
        this.cr = context;
        getWindow().setSoftInputMode(4);
        this.cA = (InputMethodManager) context.getSystemService("input_method");
        this.cl = LayoutInflater.from(context).inflate((int) R.layout.f947websitedialog, (ViewGroup) null);
        com.uc.h.e.Pb().a(this);
        bP();
    }

    private void bQ() {
        this.cm.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (6 != i) {
                    return true;
                }
                if (WebsiteSearchWebDialog.this.cm.getText().length() != 0) {
                    String obj = WebsiteSearchWebDialog.this.cm.getText().toString();
                    if (com.uc.a.e.nR().nZ() != null) {
                        com.uc.a.e.nR().nZ().bp(obj);
                    }
                    WebsiteSearchWebDialog.this.cs.bS(obj);
                    return true;
                }
                WebsiteSearchWebDialog.this.cs.onCancel();
                return true;
            }
        });
        this.cm.addTextChangedListener(new TextWatcher() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.uc.browser.WebsiteSearchWebDialog.a(com.uc.browser.WebsiteSearchWebDialog, boolean):boolean
             arg types: [com.uc.browser.WebsiteSearchWebDialog, int]
             candidates:
              com.uc.browser.WebsiteSearchWebDialog.a(com.uc.browser.WebsiteSearchWebDialog, b.a.a.e):b.a.a.e
              com.uc.browser.WebsiteSearchWebDialog.a(com.uc.browser.WebsiteSearchWebDialog, boolean):boolean */
            public void afterTextChanged(Editable editable) {
                System.currentTimeMillis();
                if (!WebsiteSearchWebDialog.this.cD && editable.toString().length() > 0) {
                    WebsiteSearchWebDialog.this.cv.k(com.uc.a.e.nR().om());
                    WebsiteSearchWebDialog.this.cv.l(com.uc.a.e.nR().ot());
                    WebsiteSearchWebDialog.this.cv.l(com.uc.a.e.nR().qE());
                    boolean unused = WebsiteSearchWebDialog.this.cD = true;
                } else if (editable.toString().length() <= 0) {
                    WebsiteSearchWebDialog.this.cv.k(null);
                    boolean unused2 = WebsiteSearchWebDialog.this.cD = false;
                }
                WebsiteSearchWebDialog.this.cv.bV(editable.toString());
                if (WebsiteSearchWebDialog.this.cn.getCount() <= 0) {
                    WebsiteSearchWebDialog.this.cn.setVisibility(8);
                } else if (WebsiteSearchWebDialog.this.cn.getVisibility() == 8) {
                    WebsiteSearchWebDialog.this.cn.setVisibility(0);
                }
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (WebsiteSearchWebDialog.this.cm.getText().length() == 0) {
                    WebsiteSearchWebDialog.this.co.setText(WebsiteSearchWebDialog.this.ct);
                    WebsiteSearchWebDialog.this.cm.setTextColor(com.uc.h.e.Pb().getColor(33));
                    return;
                }
                WebsiteSearchWebDialog.this.co.setText(WebsiteSearchWebDialog.this.cu);
                WebsiteSearchWebDialog.this.cn.setNextFocusUpId(WebsiteSearchWebDialog.this.cm.getId());
            }
        });
        this.cm.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i != 23 && i != 66) {
                    return false;
                }
                if (WebsiteSearchWebDialog.this.co.getText().toString().equals(WebsiteSearchWebDialog.this.cu)) {
                    String obj = WebsiteSearchWebDialog.this.cm.getText().toString();
                    if (com.uc.a.e.nR().nZ() != null) {
                        com.uc.a.e.nR().nZ().bp(obj);
                    }
                    WebsiteSearchWebDialog.this.cs.bS(obj);
                    return false;
                }
                WebsiteSearchWebDialog.this.cs.onCancel();
                return false;
            }
        });
        this.cn.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScroll(AbsListView absListView, int i, int i2, int i3) {
            }

            public void onScrollStateChanged(AbsListView absListView, int i) {
                WebsiteSearchWebDialog.this.cA.hideSoftInputFromWindow(WebsiteSearchWebDialog.this.cn.getWindowToken(), 0);
            }
        });
        this.cq = (TextView) this.cl.findViewById(R.id.f865address_bar_textView);
        this.cq.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                WebsiteSearchWebDialog.this.hide();
            }
        });
        com.uc.a.e.nR().nZ();
        this.cv = new AdapterAutoComplete(0);
        this.cv.bV("");
        this.cn.setAdapter((ListAdapter) this.cv);
        this.cn.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView adapterView, View view, int i, long j) {
                e eVar = (e) adapterView.getItemAtPosition(i);
                if (eVar.abU == 101) {
                    l nZ = com.uc.a.e.nR().nZ();
                    WebsiteSearchWebDialog.this.cs.bS(nZ.a(eVar.abL, (byte) nZ.oL()));
                    return;
                }
                WebsiteSearchWebDialog.this.cm.setText(eVar.abL);
                e unused = WebsiteSearchWebDialog.this.cC = eVar;
                Selection.setSelection(WebsiteSearchWebDialog.this.cm.getText(), WebsiteSearchWebDialog.this.cm.getText().length());
            }
        });
    }

    public void a(DialogInterface.OnDismissListener onDismissListener) {
        this.cB = onDismissListener;
    }

    public void a(Configuration configuration) {
        if (configuration.orientation == 1) {
            if (this.cm != null && this.cm.hasFocus() && isShowing()) {
                this.cA.showSoftInput(this.cm, 1);
            }
        } else if (configuration.orientation == 2 && this.cm != null && this.cm.hasFocus() && isShowing()) {
            this.cA.hideSoftInputFromWindow(this.cm.getWindowToken(), 0);
        }
    }

    public void a(View.OnClickListener onClickListener) {
        this.co.setOnClickListener(onClickListener);
    }

    public void a(View.OnKeyListener onKeyListener) {
        this.cm.setOnKeyListener(onKeyListener);
    }

    public void a(URLBarListener uRLBarListener) {
        this.cs = uRLBarListener;
        a(new View.OnClickListener() {
            public void onClick(View view) {
                if (WebsiteSearchWebDialog.this.co.getText().toString().equals(WebsiteSearchWebDialog.this.cu)) {
                    String obj = WebsiteSearchWebDialog.this.cm.getText().toString();
                    if (com.uc.a.e.nR().nZ() != null) {
                        com.uc.a.e.nR().nZ().bp(obj);
                    }
                    WebsiteSearchWebDialog.this.cs.bS(obj);
                    if (WebsiteSearchWebDialog.this.cC != null && WebsiteSearchWebDialog.this.cC.abL.equals(obj)) {
                        switch (WebsiteSearchWebDialog.this.cC.abU) {
                            case 1:
                                f.l(1, f.auq);
                                break;
                            case 2:
                            case 3:
                                f.l(1, f.aus);
                                break;
                            case 4:
                            case 5:
                                f.l(1, f.aur);
                                break;
                        }
                    }
                    f.l(1, f.aup);
                    return;
                }
                WebsiteSearchWebDialog.this.cs.onCancel();
            }
        });
        a(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                WebsiteSearchWebDialog.this.cs.dp();
            }
        });
    }

    public void bP() {
        this.ct = this.cr.getResources().getString(R.string.f1466add_cancle);
        this.cu = this.cr.getResources().getString(R.string.f1467add_enter);
        k();
        this.cm.setImeOptions(6);
        this.cn.setNextFocusUpId(this.cm.getId());
        bQ();
    }

    public String bR() {
        return this.cm.getText().toString();
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 4) {
            hide();
            return true;
        } else if (keyEvent.getKeyCode() == 84) {
            return true;
        } else {
            return super.dispatchKeyEvent(keyEvent);
        }
    }

    public void hide() {
        dismiss();
        if (this.cB != null) {
            this.cB.onDismiss(null);
        }
    }

    public void k() {
        this.cx = com.uc.h.e.Pb().getDrawable(UCR.drawable.aXx);
        this.cy = com.uc.h.e.Pb().getDrawable(UCR.drawable.aXf);
        this.cz = com.uc.h.e.Pb().getDrawable(UCR.drawable.aXe);
        this.cw = com.uc.h.e.Pb().kp(R.dimen.f523add_sch_add_edittext_paddingleft);
        this.cn = (WebsiteSearchListView) this.cl.findViewById(R.id.f866address_input_list);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.setMargins(0, com.uc.h.e.Pb().kp(R.dimen.f483add_sch_height_input_state) - 5, 0, 0);
        this.cn.setLayoutParams(layoutParams);
        this.cn.setDivider(new ColorDrawable(com.uc.h.e.Pb().getColor(113)));
        this.cn.setDividerHeight(1);
        this.cp = (RelativeLayout) this.cl.findViewById(R.id.f867address_bar_input);
        this.cp.setBackgroundDrawable(this.cx);
        this.co = (TextView) this.cl.findViewById(R.id.f868address_input_state);
        this.co.setTextSize(0, (float) com.uc.h.e.Pb().kp(R.dimen.f529add_sch_state_text));
        this.co.setBackgroundDrawable(this.cz);
        this.co.setTextColor(com.uc.h.e.Pb().getColor(27));
        this.co.setText(this.ct);
        this.cm = (EditText) this.cl.findViewById(R.id.f869address_bar_editText);
        this.cm.setBackgroundDrawable(this.cy);
        this.cm.setPadding(this.cw, this.cm.getPaddingTop(), this.cw, this.cm.getPaddingBottom());
        this.cm.setTextSize(0, (float) com.uc.h.e.Pb().kp(R.dimen.f500add_sch_edittext));
        this.cm.setTextColor(com.uc.h.e.Pb().getColor(33));
        this.cm.setSelectAllOnFocus(true);
        this.cm.setHighlightColor(com.uc.h.e.Pb().getColor(0));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(this.cl);
    }

    public void r(String str) {
        this.cm.setText(str);
        this.cm.selectAll();
    }

    public void show() {
        if (ActivityBrowser.bqy) {
            getWindow().setFlags(1024, 3072);
        } else {
            getWindow().setFlags(2048, 3072);
        }
        this.cv.i(com.uc.a.e.nR().nZ().oz());
        this.cv.k(null);
        this.cD = false;
        this.cv.bV("");
        this.cm.requestFocus();
        super.show();
        this.cn.setVisibility(8);
        if (this.cn.getCount() > 0) {
            this.cn.setVisibility(0);
            this.cl.setBackgroundColor(com.uc.h.e.Pb().getColor(112));
        }
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().aS(132);
        }
        if (this.cr.getResources().getConfiguration().orientation == 1) {
            this.cA.toggleSoftInput(0, 1);
        }
    }
}
