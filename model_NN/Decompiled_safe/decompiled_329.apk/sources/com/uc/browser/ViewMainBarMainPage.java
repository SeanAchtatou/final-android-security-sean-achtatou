package com.uc.browser;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.uc.browser.UCR;
import com.uc.e.ac;
import com.uc.e.ad;
import com.uc.e.h;
import com.uc.e.r;
import com.uc.h.e;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class ViewMainBarMainPage extends ViewMainBar {
    public static final byte bYL = 0;
    public static final byte bYM = 1;
    private static final int bYP = 0;
    private static final int bYQ = 1;
    private static final int bYR = 2;
    private static final int bYS = 3;
    private byte bYN = 0;
    private r bYO;
    private List bYT;
    private List bYU;
    private ad blV;

    public ViewMainBarMainPage(Context context) {
        super(context);
    }

    public ViewMainBarMainPage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    private void OZ() {
        e Pb = e.Pb();
        int kp = Pb.kp(R.dimen.f184controlbar_item_width_3);
        int kp2 = Pb.kp(R.dimen.f177controlbar_height);
        int kp3 = Pb.kp(R.dimen.f188controlbar_text_size);
        int kp4 = Pb.kp(R.dimen.f187controlbar_item_paddingTop);
        this.bYO = new r();
        this.bYO.a(this);
        this.bYO.aw(kp, kp2);
        Resources resources = getResources();
        this.bYT = new ArrayList();
        ac acVar = new ac(0, 0, 0);
        acVar.setText(resources.getString(R.string.f1024controlbar_newbookmark));
        acVar.aV(kp3);
        acVar.setPadding(0, kp4, 0, 4);
        this.bYT.add(acVar);
        ac acVar2 = new ac(1, 0, 0);
        acVar2.setText(resources.getString(R.string.f1025controlbar_newfolder));
        acVar2.aV(kp3);
        acVar2.setPadding(0, kp4, 0, 4);
        this.bYT.add(acVar2);
        ac acVar3 = new ac(2, 0, 0);
        acVar3.setText(resources.getString(R.string.f1026controlbar_sync));
        acVar3.aV(kp3);
        acVar3.setPadding(0, kp4, 0, 4);
        this.bYT.add(acVar3);
        this.bYU = new ArrayList();
        ac acVar4 = new ac(3, 0, 0);
        acVar4.setText(resources.getString(R.string.f1023controlbar_clear));
        acVar4.aV(kp3);
        acVar4.setPadding(0, kp4, 0, 4);
        Vector ot = com.uc.a.e.nR().oa().ot();
        if (ot == null || ot.size() <= 0) {
            acVar4.H(false);
        }
        this.bYU.add(acVar4);
        j(this.bYT);
    }

    private void j(List list) {
        this.bYO.clear();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            this.bYO.b((ac) it.next());
        }
        this.bYO.yi();
        this.bYO.invalidate();
    }

    public void Pa() {
        Vector ot = com.uc.a.e.nR().oa().ot();
        if (ot == null || ot.size() <= 0) {
            ((ac) this.bYU.get(0)).H(false);
        } else {
            ((ac) this.bYU.get(0)).H(true);
        }
        this.bYO.invalidate();
    }

    /* access modifiers changed from: protected */
    public void a() {
        OZ();
        super.a();
    }

    public void a(ad adVar) {
        super.a(adVar);
        if (adVar != null) {
            this.bYO.a(adVar);
            this.HS.a(adVar);
            this.blV = adVar;
            return;
        }
        this.bYO.a(this);
    }

    public void e(h hVar) {
        this.bYO.c(hVar);
    }

    public void k() {
        super.k();
        e Pb = e.Pb();
        this.bYO.s(Pb.getDrawable(UCR.drawable.aXp));
        this.bYO.m(Pb.getDrawable(UCR.drawable.aWt));
        int color = Pb.getColor(10);
        int color2 = Pb.getColor(11);
        if (this.bYU != null) {
            for (ac acVar : this.bYU) {
                acVar.setTextColor(color);
                acVar.ks(color2);
            }
        }
        if (this.bYT != null) {
            for (ac acVar2 : this.bYT) {
                acVar2.setTextColor(color);
                acVar2.ks(color2);
            }
        }
    }

    public void kj(int i) {
        if (i == 0) {
            this.bYN = 0;
            kF().invalidate();
        } else if (i == 1) {
            this.bYN = 1;
            this.bYO.invalidate();
        }
    }

    public void kk(int i) {
        if (i == 0) {
            j(this.bYT);
        } else if (i == 1) {
            j(this.bYU);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.bYN == 0) {
            super.onDraw(canvas);
        } else if (this.bYN == 1) {
            this.bYO.draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.bYO.setSize(i3 - i, i4 - i2);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.bYN == 0) {
            return super.onTouchEvent(motionEvent);
        }
        if (this.bYN == 1) {
            return this.bYO.d(motionEvent);
        }
        return true;
    }

    public void postInvalidate() {
        super.postInvalidate();
        if (this.blV != null) {
            this.blV.cJ();
        }
    }
}
