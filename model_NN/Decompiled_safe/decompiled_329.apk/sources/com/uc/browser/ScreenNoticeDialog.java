package com.uc.browser;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import com.uc.browser.UCR;
import com.uc.h.e;

public class ScreenNoticeDialog extends Dialog {
    private ContentView brt;

    class ContentView extends View implements Runnable {
        private static final int aXA = 3;
        private static final String aXy = "试一试左右滑屏";
        private static final String aXz = "看看你发现了什么?";
        private int aXB = ((int) getResources().getDimension(R.dimen.f233notice_shaderbottom_height));
        private int aXC = ((int) getResources().getDimension(R.dimen.f231notice_font));
        private int aXD = ((int) getResources().getDimension(R.dimen.f234notice_textcenter_offset));
        private int aXE = ((int) getResources().getDimension(R.dimen.f232notice_arrowedge_margin));
        private int aXF = ((int) getResources().getDimension(R.dimen.f235notice_arrow_gap));
        private Bitmap aXG = e.Pb().kl(UCR.drawable.aTy);
        private Bitmap aXH = e.Pb().kl(UCR.drawable.aTz);
        private int aXI = 3;
        private boolean aXJ;
        private Drawable cx = e.Pb().getDrawable(UCR.drawable.aTA);
        private Paint pL;
        private int textColor = -16777216;

        public ContentView(Context context) {
            super(context);
            AD();
            setBackgroundDrawable(this.cx);
        }

        private void AD() {
            this.pL = new Paint(1);
            this.pL.setFilterBitmap(true);
            this.pL.setColor(this.textColor);
            this.pL.setTextSize((float) this.aXC);
        }

        private int fK(int i) {
            switch (i) {
                case 1:
                    return ModelBrowser.zQ;
                case 2:
                    return 76;
                default:
                    return 255;
            }
        }

        public void AE() {
            this.aXJ = false;
            new Thread(this).start();
        }

        public void AF() {
            this.aXJ = true;
        }

        public void onDraw(Canvas canvas) {
            this.pL.setAlpha(255);
            canvas.drawText(aXy, (((float) getWidth()) - this.pL.measureText(aXy)) / 2.0f, (float) (((getHeight() - this.aXB) / 2) - this.aXD), this.pL);
            canvas.drawText(aXz, (((float) getWidth()) - this.pL.measureText(aXz)) / 2.0f, ((float) (((getHeight() - this.aXB) / 2) + this.aXD)) - this.pL.ascent(), this.pL);
            int width = getWidth() - this.aXE;
            int i = 0;
            while (i < 3) {
                Bitmap bitmap = this.aXI == i ? this.aXH : this.aXG;
                this.pL.setAlpha(fK(i));
                canvas.drawBitmap(bitmap, (float) ((width - (this.aXF * i)) - this.aXH.getWidth()), 0.0f, this.pL);
                i++;
            }
            int i2 = this.aXE;
            canvas.scale(-1.0f, 1.0f);
            int i3 = 0;
            while (i3 < 3) {
                Bitmap bitmap2 = this.aXI == i3 ? this.aXH : this.aXG;
                this.pL.setAlpha(fK(i3));
                canvas.drawBitmap(bitmap2, (float) (-((this.aXF * i3) + i2 + bitmap2.getWidth())), 0.0f, this.pL);
                i3++;
            }
            this.aXI--;
            if (this.aXI < 0) {
                this.aXI = 3;
            }
        }

        public void onMeasure(int i, int i2) {
            setMeasuredDimension(View.MeasureSpec.getSize(i), this.cx.getIntrinsicHeight());
        }

        public void run() {
            while (!this.aXJ) {
                postInvalidate();
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public ScreenNoticeDialog(Context context) {
        super(context, R.style.f1578notice_dialog);
        requestWindowFeature(1);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        this.brt = new ContentView(context);
        setContentView(this.brt);
    }

    public void dismiss() {
        this.brt.AF();
        super.dismiss();
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 84) {
            return true;
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        dismiss();
        return super.onTouchEvent(motionEvent);
    }

    public void show() {
        super.show();
        this.brt.AE();
    }
}
