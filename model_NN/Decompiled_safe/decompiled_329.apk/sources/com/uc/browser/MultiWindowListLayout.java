package com.uc.browser;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.uc.browser.UCR;
import com.uc.c.w;
import com.uc.e.a.f;
import com.uc.e.a.k;
import com.uc.e.ad;
import com.uc.h.e;

public class MultiWindowListLayout extends View implements k {
    private Drawable aEd = e.Pb().getDrawable(UCR.drawable.aWN);
    f bTI;
    private Drawable bTJ = e.Pb().getDrawable(UCR.drawable.aXu);
    private Drawable bTK = e.Pb().getDrawable(UCR.drawable.aXu);
    private int bTL = w.Qt;
    private int bTM;
    private int vl = e.Pb().kp(R.dimen.f428mutiwindowlist_item_height);

    public MultiWindowListLayout(Context context) {
        super(context);
        d(context);
    }

    public MultiWindowListLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        d(context);
    }

    private void d(Context context) {
        e Pb = e.Pb();
        this.bTL = Pb.kp(R.dimen.f432multiwindow_list_max_height);
        this.bTI = new f();
        this.bTI.fu(1);
        this.bTI.fm(this.vl);
        this.bTI.a(new com.uc.e.b.e(Pb.getColor(70)), 1);
        this.bTI.f(new Drawable[]{null, this.bTJ, this.bTK});
        this.bTI.B(Pb.getDrawable(UCR.drawable.aWN));
        this.bTI.ft(Pb.kp(R.dimen.f166list_scrollbar_size));
        this.bTI.a(new ad() {
            public void cJ() {
                MultiWindowListLayout.this.invalidate();
            }
        });
        this.bTI.a(this);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return this.bTI.c(keyEvent);
    }

    public void fN(int i) {
        ViewGroup.LayoutParams layoutParams;
        if (this.bTI != null) {
            int i2 = i > this.bTL ? this.bTL : i;
            if (i2 != this.bTI.getHeight()) {
                this.bTM = i2;
                this.bTI.setSize(this.bTI.getWidth(), i2);
                ViewGroup.LayoutParams layoutParams2 = getLayoutParams();
                if (layoutParams2 == null) {
                    layoutParams = new ViewGroup.LayoutParams(getWidth(), i2 + getPaddingBottom() + getPaddingTop());
                } else {
                    layoutParams2.height = i2 + getPaddingBottom() + getPaddingTop();
                    layoutParams = layoutParams2;
                }
                setLayoutParams(layoutParams);
                requestLayout();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        System.currentTimeMillis();
        canvas.translate(0.0f, (float) (getHeight() - this.bTM));
        canvas.clipRect(0, 0, getWidth(), this.bTM);
        this.bTI.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        int yZ = this.bTI.yZ();
        if (yZ > this.bTL) {
            yZ = this.bTL;
        }
        this.bTM = yZ;
        this.bTI.setSize(i3 - i, yZ);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.bTI != null) {
            int yZ = this.bTI.yZ();
            if (yZ > this.bTL) {
                yZ = this.bTL;
            }
            setMeasuredDimension(getMeasuredWidth(), yZ + getPaddingBottom() + getPaddingTop());
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                return this.bTI.b((byte) 0, (int) motionEvent.getX(), (((int) motionEvent.getY()) - getHeight()) + this.bTM);
            case 1:
                return this.bTI.b((byte) 1, (int) motionEvent.getX(), (((int) motionEvent.getY()) - getHeight()) + this.bTM);
            case 2:
                return this.bTI.b((byte) 2, (int) motionEvent.getX(), (((int) motionEvent.getY()) - getHeight()) + this.bTM);
            default:
                return false;
        }
    }
}
