package com.uc.browser;

import android.content.Context;
import android.preference.ListPreference;
import android.util.AttributeSet;

public class ListPrefWithSummary extends ListPreference {
    public ListPrefWithSummary(Context context) {
        super(context);
    }

    public ListPrefWithSummary(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onDialogClosed(boolean z) {
        super.onDialogClosed(z);
        setSummary(getEntry());
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(boolean z, Object obj) {
        super.onSetInitialValue(z, obj);
        setSummary(getEntry());
    }

    public void setValue(String str) {
        super.setValue(str);
        setSummary(getEntry());
    }
}
