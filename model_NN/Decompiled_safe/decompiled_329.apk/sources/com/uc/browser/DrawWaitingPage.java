package com.uc.browser;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.Transformation;
import com.uc.a.h;
import com.uc.c.j;
import com.uc.h.e;

public class DrawWaitingPage {
    private Transformation aa;
    private Paint brj;
    private String brk;
    private String brl;
    private Drawable brm = null;
    private Drawable brn = null;
    private Drawable bro = null;
    private Animation brp;
    private Animation brq;
    private View view;

    public DrawWaitingPage(View view2, boolean z, String str) {
        this.view = view2;
        if (z) {
            AE();
        }
        this.brj = new Paint(1);
    }

    public void AE() {
        try {
            this.brp = AnimationUtils.loadAnimation(this.view.getContext(), R.anim.f21safe_shield);
        } catch (Exception e) {
            AnimationSet animationSet = new AnimationSet(true);
            LinearInterpolator linearInterpolator = new LinearInterpolator();
            animationSet.setInterpolator(linearInterpolator);
            RotateAnimation rotateAnimation = new RotateAnimation(0.0f, 360.0f, 0.5f, 0.5f);
            rotateAnimation.setInterpolator(linearInterpolator);
            rotateAnimation.setDuration(1200);
            rotateAnimation.setRepeatCount(-1);
            animationSet.addAnimation(rotateAnimation);
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setInterpolator(linearInterpolator);
            alphaAnimation.setDuration(600);
            alphaAnimation.setRepeatMode(2);
            alphaAnimation.setRepeatCount(-1);
            animationSet.addAnimation(alphaAnimation);
            this.brp = animationSet;
        }
        this.brp.setStartTime(-1);
    }

    public void FB() {
        this.brp = null;
    }

    public void eG(String str) {
        this.brl = str;
    }

    public void j(Canvas canvas) {
        int width = this.view.getWidth();
        int height = this.view.getHeight();
        if (this.aa == null) {
            this.aa = new Transformation();
        }
        boolean Fz = ActivityBrowser.Fz();
        if (Fz) {
            this.brm = e.Pb().km(R.drawable.f592loading_circle_night);
        } else {
            this.brm = e.Pb().km(R.drawable.f591loading_circle_day);
        }
        boolean z = this.brp != null && this.brp.getTransformation(System.currentTimeMillis(), this.aa);
        if (this.brm != null) {
            int intValue = Integer.valueOf(com.uc.a.e.nR().bw(h.afD)).intValue();
            this.brj.setTextSize(this.view.getResources().getDimension(R.dimen.f417loading_page_text_size));
            canvas.drawColor(j.eR[0]);
            if (Fz) {
                this.brj.setColor(-14208709);
                if (this.bro != null) {
                    this.bro.setAlpha(128);
                }
            } else {
                this.brj.setColor(-12500671);
                if (this.bro != null) {
                    this.bro.setAlpha(255);
                }
            }
            canvas.translate((float) (width / 2), ((((float) height) - this.view.getResources().getDimension(R.dimen.f416loading_page_text_margin)) + this.brj.ascent()) / 2.0f);
            if (intValue <= 1 && this.bro != null) {
                this.bro.setBounds((-this.bro.getIntrinsicWidth()) / 2, (-this.bro.getIntrinsicHeight()) / 2, this.bro.getIntrinsicWidth() / 2, this.bro.getIntrinsicHeight() / 2);
                this.bro.draw(canvas);
            }
            if (this.brp != null) {
                canvas.save();
                canvas.concat(this.aa.getMatrix());
                this.brm.setBounds((-this.brm.getIntrinsicWidth()) / 2, (-this.brm.getIntrinsicHeight()) / 2, this.brm.getIntrinsicWidth() / 2, this.brm.getIntrinsicHeight() / 2);
                if (Fz) {
                    this.brm.setAlpha(128);
                } else {
                    this.brm.setAlpha(255);
                }
                this.brm.draw(canvas);
                canvas.restore();
                if (intValue <= 1 && this.brn != null) {
                    canvas.save();
                    this.brn.setAlpha(((int) (this.aa.getAlpha() * 255.0f)) >> (Fz ? 1 : 0));
                    this.brn.setBounds((-this.brn.getIntrinsicWidth()) / 2, (-this.brn.getIntrinsicHeight()) / 2, this.brn.getIntrinsicWidth() / 2, this.brn.getIntrinsicHeight() / 2);
                    this.brn.draw(canvas);
                    canvas.restore();
                }
            }
            String string = intValue <= 1 ? this.view.getResources().getString(R.string.f1306page_safe_loading_page) : this.view.getResources().getString(R.string.f1307page_loading_page);
            canvas.translate((-this.brj.measureText(string)) / 2.0f, (this.view.getResources().getDimension(R.dimen.f416loading_page_text_margin) - this.brj.ascent()) + ((float) (this.brm.getIntrinsicHeight() / 2)));
            canvas.drawText(string, 0.0f, 0.0f, this.brj);
        }
        if (!z) {
            this.brp = null;
        } else {
            this.view.postInvalidate();
        }
    }

    public void k(Canvas canvas) {
        if (this.brj == null) {
            this.brj = new Paint();
            this.brj.setStyle(Paint.Style.FILL);
            this.brj.setAntiAlias(true);
            this.brj.setTextSize(18.0f);
            this.brj.setTypeface(Typeface.DEFAULT);
            this.brj.setTextAlign(Paint.Align.LEFT);
        }
        if (!ActivityBrowser.Fz()) {
            this.brj.setColor(-16777216);
            canvas.drawColor(-1);
        } else {
            this.brj.setColor(-14661520);
            canvas.drawColor(-16250864);
        }
        if (this.brl == null) {
            this.brl = this.view.getResources().getString(R.string.f1415page_waiting_failure);
        }
        if (this.brl != null) {
            canvas.drawText(this.brl, (((float) this.view.getWidth()) - this.brj.measureText(this.brl)) / 2.0f, (float) ((this.view.getHeight() - 18) / 2), this.brj);
        }
    }

    public boolean zH() {
        return this.brp != null;
    }
}
