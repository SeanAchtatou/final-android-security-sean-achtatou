package com.uc.browser;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import com.uc.a.e;
import com.uc.a.n;

public class ActivityUploadFont extends Activity {
    /* access modifiers changed from: private */
    public ViewUploadFont auN;
    /* access modifiers changed from: private */
    public boolean g = false;
    private n p = new n() {
        public boolean eW(int i) {
            ActivityUploadFont.this.auN.postInvalidate();
            if (i < 99 || ActivityUploadFont.this.g) {
                return false;
            }
            boolean unused = ActivityUploadFont.this.g = true;
            ActivityUploadFont.this.finish();
            return false;
        }
    };

    public void c() {
        e.nR().nZ().f(this.p);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 3072);
        this.auN = new ViewUploadFont(this);
        setContentView(this.auN);
        e.nR().nZ().i(this.p);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (4 != i) {
            return super.onKeyDown(i, keyEvent);
        }
        this.auN.invalidate();
        return true;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i == 84) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z && ActivityBrowser.Fz()) {
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
            }
            ActivityBrowser.e(this);
        }
    }
}
