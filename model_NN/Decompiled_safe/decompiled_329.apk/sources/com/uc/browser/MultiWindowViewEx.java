package com.uc.browser;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.uc.browser.AdapterMultiWindow;
import com.uc.browser.UCR;
import com.uc.e.a.i;
import com.uc.e.a.n;
import com.uc.e.a.u;
import com.uc.h.a;
import com.uc.h.e;
import java.util.Vector;

public class MultiWindowViewEx extends RelativeLayout implements View.OnClickListener, AdapterMultiWindow.WindowObserver, n, a {
    public static final int avn = 4;
    public static long avs = 0;
    private static final int avu = 1;
    private Animation Z = AnimationUtils.loadAnimation(getContext(), R.anim.f11fade_in_menubox);
    private RelativeLayout avo;
    private Button avp;
    private AdapterMultiWindow avq;
    private MultiWindowListLayout avr;
    private Animation avt = AnimationUtils.loadAnimation(getContext(), R.anim.f14fade_out_menubox);
    private View avv;

    public MultiWindowViewEx(Context context) {
        super(context);
        d(context);
    }

    public MultiWindowViewEx(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        d(context);
    }

    public MultiWindowViewEx(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        d(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.uc.browser.MultiWindowViewEx, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void d(Context context) {
        LayoutInflater.from(context).inflate((int) R.layout.f914multiwindow_manager_ex, (ViewGroup) this, true);
        this.avo = (RelativeLayout) findViewById(R.id.content);
        this.avp = (Button) findViewById(R.id.f788new_window_button);
        this.avp.setOnClickListener(this);
        k();
        e.Pb().a(this);
    }

    private void r(Context context) {
        this.avr = new MultiWindowListLayout(context);
        int count = this.avq.getCount();
        Vector vector = new Vector();
        for (int i = 0; i < count; i++) {
            String str = ((WindowItem) this.avq.getItem(i)).bs;
            u uVar = new u();
            if (str == null || str.equals("")) {
                str = getContext().getResources().getString(R.string.f952app_name);
            }
            uVar.dg(str);
            vector.add(uVar);
        }
        this.avo.removeAllViews();
        this.avr.bTI.c(vector);
        this.avr.bTI.eM(ModelBrowser.gD().hj().wj());
        this.avo.addView(this.avr, new RelativeLayout.LayoutParams(-1, -2));
        this.avr.bTI.a(this);
    }

    private void s(Context context) {
        Vector vector = new Vector();
        int count = this.avq.getCount();
        this.avq.a(this);
        for (int i = 0; i < count; i++) {
            WindowItem windowItem = (WindowItem) this.avq.getItem(i);
            i iVar = new i();
            if (windowItem.bs == null || windowItem.bs.equals("")) {
                windowItem.bs = getContext().getResources().getString(R.string.f952app_name);
            }
            iVar.dg(windowItem.bs);
            iVar.e(windowItem.bt);
            vector.add(iVar);
        }
        this.avo.removeAllViews();
        MultiWindowLayout multiWindowLayout = new MultiWindowLayout(context, vector);
        multiWindowLayout.setId(1);
        multiWindowLayout.auC.a(this);
        multiWindowLayout.auC.eE(ModelBrowser.gD().hj().wj());
        if (e.Pb().Pc()) {
            this.avo.addView(multiWindowLayout, new RelativeLayout.LayoutParams(-1, e.Pb().kp(R.dimen.f449mutiwindow_height_winnum_2)));
        } else if (count <= 2) {
            this.avo.addView(multiWindowLayout, new RelativeLayout.LayoutParams(-1, e.Pb().kp(R.dimen.f449mutiwindow_height_winnum_2)));
        } else {
            this.avo.addView(multiWindowLayout, new RelativeLayout.LayoutParams(-1, e.Pb().kp(R.dimen.f450mutiwindow_height_winnum_4)));
        }
        this.avv = multiWindowLayout;
    }

    public void a(Animation.AnimationListener animationListener) {
        avs = System.currentTimeMillis();
        ((SAnimLinearLayout) findViewById(R.id.f786frame)).TQ = null;
        this.avq.b(this);
        if (ModelBrowser.gD() == null || ModelBrowser.gD().gR() > 1) {
            findViewById(R.id.f786frame).startAnimation(null);
            if (animationListener != null) {
                animationListener.onAnimationEnd(null);
                return;
            }
            return;
        }
        this.avt.setAnimationListener(animationListener);
        findViewById(R.id.f786frame).startAnimation(this.avt);
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (this.avq.getCount() <= 4) {
            this.avq.i(this);
        }
    }

    public void eD(int i) {
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().hj().eO(i);
            ModelBrowser.gD().a(143, new Boolean(true));
        }
        this.avp.setEnabled(true);
        this.avp.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aWY));
        this.avp.setTextColor(e.Pb().getColor(96));
    }

    public void eP(int i) {
        ModelBrowser.gD().hj().eP(i);
        ModelBrowser.gD().hl();
    }

    public void k() {
        e Pb = e.Pb();
        findViewById(R.id.f786frame).setBackgroundDrawable(Pb.getDrawable(UCR.drawable.aUB));
        this.avo.setBackgroundDrawable(Pb.getDrawable(UCR.drawable.aUH));
        this.avp.setBackgroundDrawable(Pb.getDrawable(UCR.drawable.aWY));
        this.avp.setTextColor(Pb.getColor(96));
    }

    public void onClick(View view) {
        int wh = ModelBrowser.gD().hj().wh();
        this.avq.yW();
        ModelBrowser.gD().hl();
        if (wh >= 15) {
            this.avp.setEnabled(false);
            this.avp.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aUD));
            this.avp.setTextColor(e.Pb().getColor(97));
            return;
        }
        this.avp.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aWY));
        this.avp.setTextColor(e.Pb().getColor(96));
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int count = this.avq.getCount();
        if (count <= 4 && this.avv != null) {
            if (e.Pb().Pc()) {
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.avv.getLayoutParams();
                layoutParams.height = e.Pb().kp(R.dimen.f449mutiwindow_height_winnum_2);
                this.avv.setLayoutParams(layoutParams);
            } else if (count <= 2) {
                RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.avv.getLayoutParams();
                layoutParams2.height = e.Pb().kp(R.dimen.f449mutiwindow_height_winnum_2);
                this.avv.setLayoutParams(layoutParams2);
            } else {
                RelativeLayout.LayoutParams layoutParams3 = (RelativeLayout.LayoutParams) this.avv.getLayoutParams();
                layoutParams3.height = e.Pb().kp(R.dimen.f450mutiwindow_height_winnum_4);
                this.avv.setLayoutParams(layoutParams3);
            }
        }
        super.onMeasure(i, i2);
    }

    public void q(Context context) {
        avs = System.currentTimeMillis();
        ((SAnimLinearLayout) findViewById(R.id.f786frame)).TQ = null;
        this.avq = new AdapterMultiWindow(context);
        int count = this.avq.getCount();
        if (count >= 15) {
            this.avp.setEnabled(false);
            this.avp.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aUD));
            this.avp.setTextColor(e.Pb().getColor(97));
        } else {
            this.avp.setEnabled(true);
            this.avp.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aWY));
            this.avp.setTextColor(e.Pb().getColor(96));
        }
        this.avv = null;
        if (count <= 4) {
            s(context);
        } else {
            r(context);
        }
        findViewById(R.id.f786frame).startAnimation((ModelBrowser.gD() == null || ModelBrowser.gD().gR() > 1) ? null : this.Z);
    }

    public void update() {
        int count = this.avq.getCount();
        MultiWindowLayout multiWindowLayout = (MultiWindowLayout) findViewById(1);
        Vector xa = multiWindowLayout.xa();
        for (int i = 0; i < count; i++) {
            WindowItem windowItem = (WindowItem) this.avq.getItem(i);
            i iVar = (i) xa.elementAt(i);
            if (windowItem.bs == null || windowItem.bs.equals("")) {
                windowItem.bs = getContext().getResources().getString(R.string.f952app_name);
            }
            iVar.dg(windowItem.bs);
            iVar.e(windowItem.bt);
        }
        multiWindowLayout.c(xa);
    }
}
