package com.uc.browser;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import com.uc.browser.UCR;
import com.uc.e.ad;
import com.uc.e.z;
import com.uc.h.e;

public class TabContainer extends View implements ad {
    private Drawable baA;
    private Drawable baB;
    private Drawable baC;
    private Drawable baD;
    private Drawable baE;
    private int baF;
    private TabData[] baG;
    private int baH;
    private float baI;
    private float baJ;
    private onTabChangedListener baK;
    private int baq;
    private int bar;
    private int bas;
    private int bat;
    private int bau;
    private int bav;
    private int baw;
    private int bax;
    private int bay;
    private int baz;

    public class TabData {
        /* access modifiers changed from: private */
        public String aU;
        private Bitmap aaR;
        /* access modifiers changed from: private */
        public z tI;

        public TabData(String str, Bitmap bitmap, z zVar) {
            this.aU = str;
            this.tI = zVar;
            this.aaR = bitmap;
            zVar.a(TabContainer.this);
        }
    }

    public interface onTabChangedListener {
        void aX(int i, int i2);
    }

    public TabContainer(Context context) {
        super(context);
        this.bay = 3;
        this.baz = -2;
        this.baF = 0;
        this.baH = -1;
        a();
    }

    public TabContainer(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public TabContainer(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.bay = 3;
        this.baz = -2;
        this.baF = 0;
        this.baH = -1;
        a();
    }

    private void a() {
        if (this.baA == null) {
            this.baA = e.Pb().getDrawable(UCR.drawable.aXm);
            this.baB = e.Pb().getDrawable(UCR.drawable.aXn);
            this.baD = e.Pb().getDrawable(UCR.drawable.aTO);
            this.baE = e.Pb().getDrawable(UCR.drawable.aTP);
            this.baq = (int) getResources().getDimension(R.dimen.f217bookmark_tab_height);
            this.bar = (int) getResources().getDimension(R.dimen.f225bookmark_tab_edge);
            this.bas = (int) getResources().getDimension(R.dimen.f218bookmark_tab_width);
            this.bat = (int) getResources().getDimension(R.dimen.f227bookmark_tab_top_padding);
            this.bau = (int) getResources().getDimension(R.dimen.f226bookmark_strip_height);
            this.bav = (int) getResources().getDimension(R.dimen.f228bookmark_tabfont);
        }
    }

    public Drawable Bd() {
        return this.baD;
    }

    public Drawable Be() {
        return this.baE;
    }

    public Drawable Bf() {
        return this.baA;
    }

    public Drawable Bg() {
        return this.baB;
    }

    public Drawable Bh() {
        return this.baC;
    }

    public int Bi() {
        return this.baH;
    }

    public int Bj() {
        return this.baw;
    }

    public int Bk() {
        return this.bax;
    }

    public void F(Drawable drawable) {
        this.baD = drawable;
    }

    public void G(Drawable drawable) {
        this.baE = drawable;
    }

    public void H(Drawable drawable) {
        this.baA = drawable;
    }

    public void I(Drawable drawable) {
        this.baB = drawable;
    }

    public void J(Drawable drawable) {
        this.baC = drawable;
    }

    public void a(onTabChangedListener ontabchangedlistener) {
        this.baK = ontabchangedlistener;
    }

    public void a(TabData[] tabDataArr) {
        this.baG = tabDataArr;
        for (TabData a2 : tabDataArr) {
            a2.tI.setSize(getWidth(), getHeight() - this.baq);
        }
    }

    public void bo(int i) {
        if (this.baH != i) {
            int i2 = this.baH;
            this.baH = i;
            invalidate();
            if (this.baK != null) {
                this.baK.aX(i2, i);
            }
        }
    }

    public void cJ() {
        invalidate();
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            switch (keyEvent.getKeyCode()) {
                case 21:
                    bo(this.baH != 0 ? this.baH - 1 : 0);
                    break;
                case 22:
                    bo(this.baH != size() - 1 ? this.baH + 1 : size() - 1);
                    break;
            }
        }
        return this.baG[this.baH].tI.c(keyEvent);
    }

    public void draw(Canvas canvas) {
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int width = (getWidth() - paddingLeft) - paddingRight;
        int height = (getHeight() - paddingTop) - paddingBottom;
        this.bar = -paddingLeft;
        canvas.save();
        canvas.translate(0.0f, (float) this.baq);
        Drawable background = getBackground();
        if (background != null) {
            background.setBounds(new Rect(0, 0, getWidth(), getHeight() - this.baq));
            background.draw(canvas);
        }
        canvas.restore();
        Paint paint = new Paint(1);
        int size = (width - (this.bar * 2)) / size();
        canvas.translate((float) paddingLeft, (float) paddingTop);
        int i = 0;
        while (i < size()) {
            canvas.save();
            canvas.translate((float) (this.bar + (size * i)), 0.0f);
            Drawable drawable = i == this.baH ? this.baB : this.baA;
            drawable.setBounds(new Rect(0, 0, size, this.baq));
            drawable.draw(canvas);
            paint.setTextSize((float) this.bav);
            float measureText = (((((float) size) - paint.measureText(this.baG[i].aU)) - ((float) this.bay)) + ((float) this.baz)) / 2.0f;
            paint.setColor(i == this.baH ? this.bax : this.baw);
            canvas.drawText(this.baG[i].aU, measureText + ((float) this.bay), (float) ((this.baq + this.bav) / 2), paint);
            paint.setAlpha(255);
            canvas.restore();
            i++;
        }
        this.baD.setBounds(this.bar, this.baq - this.bau, this.bar + (this.baH * size), this.baq);
        this.baD.draw(canvas);
        this.baE.setBounds(this.bar + ((this.baH + 1) * size), this.baq - this.bau, width - this.bar, this.baq);
        this.baE.draw(canvas);
        canvas.translate(0.0f, (float) this.baq);
        canvas.clipRect(0, 0, width, height - this.baq);
        this.baG[this.baH].tI.onDraw(canvas);
    }

    public void fT(int i) {
        this.baF = i;
    }

    public void fU(int i) {
        this.baw = i;
    }

    public void fV(int i) {
        this.bax = i;
    }

    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        for (TabData a2 : this.baG) {
            a2.tI.setSize((getWidth() - getPaddingLeft()) - getPaddingRight(), ((getHeight() - this.baq) - getPaddingBottom()) - getPaddingTop());
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (motionEvent.getY() > ((float) this.baq)) {
            return this.baG[this.baH].tI.b((byte) action, (int) motionEvent.getX(), ((int) motionEvent.getY()) - this.baq);
        }
        if (action == 0) {
            this.baI = motionEvent.getX();
            this.baJ = motionEvent.getY();
            bo((int) ((motionEvent.getX() * ((float) size())) / ((float) getWidth())));
        }
        return true;
    }

    public int size() {
        return this.baG.length;
    }
}
