package com.uc.browser;

import a.a.a.c.h.l;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Picture;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.text.ClipboardManager;
import android.text.Editable;
import android.view.ContextMenu;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.webkit.WebViewDatabase;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import b.a.a.a.f;
import b.a.a.d;
import b.b;
import com.uc.a.c;
import com.uc.a.e;
import com.uc.a.h;
import com.uc.a.n;
import com.uc.a.q;
import com.uc.browser.UCAlertDialog;
import com.uc.browser.UCR;
import com.uc.browser.ViewZoomControls;
import com.uc.browser.WebsiteSearchSchDialog;
import com.uc.c.ab;
import com.uc.c.an;
import com.uc.c.au;
import com.uc.c.bx;
import com.uc.c.cb;
import com.uc.c.cd;
import com.uc.c.m;
import com.uc.c.w;
import com.uc.f.g;
import com.uc.j.a;
import com.uc.plugin.ac;
import com.uc.plugin.ae;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

public final class ModelBrowser {
    public static final int AB = 10;
    public static final int AC = 96;
    private static final int AE = 0;
    private static final int AF = 1;
    private static final int AG = 2;
    public static int AM = 7;
    private static final String AT = "pref_tips_forward_preread";
    public static final String AZ = "pref_key_rm_enter_count";
    public static final int Aa = 163;
    public static final int Ab = 164;
    public static final int Ac = 165;
    public static final int Ad = 166;
    public static final int Ae = 167;
    public static final int Af = 168;
    public static final int Ag = 169;
    public static final int Ah = 0;
    public static final int Ai = 1;
    public static final int Aj = 2;
    private static final String Al = "passport_tips_counter";
    public static final String Ba = "pref_key_rm_quit_count";
    public static final String Bb = "pref_key_rm_tips_count";
    public static final String Bc = "pref_reading_edu_dlg";
    private static final String LOGTAG = "MODE_BROWSER";
    public static final String vA = "state_full_screen";
    private static final RelativeLayout.LayoutParams vL = new RelativeLayout.LayoutParams(-1, -1);
    private static final int vS = 10066329;
    private static ModelBrowser vz = new ModelBrowser();
    private static final int wE = 0;
    private static final int wF = 1;
    private static final int wG = 2;
    private static final int wH = 0;
    private static final int wI = 1;
    public static final int wM = 0;
    public static final int wN = 1;
    public static final int wO = 2;
    public static final int wP = 3;
    public static final String wQ = "uc.ucplayer";
    public static final String wR = "uc.ucdl";
    public static final String wS = "com.blovestorm";
    private static final String wT = "ext:back_to_app";
    public static final String wU = "ext:dt_search/";
    public static final String wV = "ext:dt_xhtml_page/";
    public static final String wW = "ext:dt_page/";
    public static final String wX = "ext:cm_page/";
    public static final String wY = "com.uc.news.UC_BROWSER_NEWS_COMPLETED";
    public static final String wZ = "uc.ucdl.UC_BROWSER_UCDL_COMPLETED";
    static final long we = 30000;
    private static final int wf = 300000;
    private static final String wn = "30";
    static final int wo = 0;
    static final int wp = 1;
    static final int wq = 2;
    static final int wr = 3;
    static final int ws = 4;
    static final int wt = 5;
    static final int wu = 6;
    static final int wv = 7;
    static final int ww = 8;
    static final int wx = 1;
    static final int wy = 0;
    public static final String wz = "ext:startpage";
    public static final int xA = 23;
    public static final int xB = 24;
    public static final int xC = 25;
    public static final int xD = 26;
    public static final int xE = 27;
    public static final int xF = 28;
    public static final int xG = 29;
    public static final int xH = 30;
    public static final int xI = 31;
    public static final int xJ = 32;
    public static final int xK = 33;
    public static final int xL = 34;
    public static final int xM = 35;
    public static final int xN = 36;
    public static final int xO = 37;
    public static final int xP = 38;
    public static final int xQ = 39;
    public static final int xR = 40;
    public static final int xS = 41;
    public static final int xT = 42;
    public static final int xU = 43;
    public static final int xV = 44;
    public static final int xW = 47;
    public static final int xX = 48;
    public static final int xY = 49;
    public static final int xZ = 50;
    public static final String xa = "com.blovestorm.UC_BROWSER_BLOVESTORM_COMPLETED";
    public static final String xb = "ext:lp:lp_ucl";
    public static final String xc = "should_show_zoom_mode_tips";
    public static final String xd = "should_show_novel_mode_tips";
    public static final String xe = "should_show_page_up_down_mode_tips";
    public static final String xf = "should_show_nightmode_tips";
    public static final String xg = "uc_install_prefname";
    public static final String xh = "uc_install_ex";
    public static final String xi = "uc_help_tips";
    public static final String xl = "uc_tips_fullscreen";
    public static final int xn = 10;
    public static final int xo = 11;
    public static final int xp = 12;
    public static final int xq = 13;
    public static final int xr = 14;
    public static final int xs = 15;
    public static final int xt = 16;
    public static final int xu = 17;
    public static final int xv = 18;
    public static final int xw = 19;
    public static final int xx = 20;
    public static final int xy = 21;
    public static final int xz = 22;
    public static final int yA = 78;
    public static final int yB = 79;
    public static final int yC = 80;
    public static final int yD = 81;
    public static final int yE = 83;
    public static final int yF = 85;
    public static final int yG = 86;
    public static final int yH = 87;
    public static final int yI = 88;
    public static final int yJ = 89;
    public static final int yK = 90;
    public static final int yL = 91;
    public static final int yM = 92;
    public static final int yN = 93;
    public static final int yO = 94;
    public static final int yP = 95;
    public static final int yQ = 96;
    public static final int yR = 97;
    public static final int yS = 98;
    public static final int yT = 99;
    public static final int yU = 101;
    public static final int yV = 102;
    public static final int yW = 103;
    public static final int yX = 104;
    public static final int yY = 105;
    public static final int yZ = 106;
    public static final int ya = 51;
    public static final int yb = 52;
    public static final int yc = 53;
    public static final int yd = 54;
    public static final int ye = 55;
    public static final int yf = 56;
    public static final int yg = 57;
    public static final int yh = 58;
    public static final int yi = 59;
    public static final int yj = 60;
    public static final int yk = 61;
    public static final int yl = 62;
    public static final int ym = 63;
    public static final int yn = 64;
    public static final int yo = 65;
    public static final int yp = 66;
    public static final int yq = 67;
    public static final int yr = 68;
    public static final int ys = 69;
    public static final int yt = 70;
    private static final int yu = 71;
    public static final int yv = 72;
    public static final int yw = 74;
    public static final int yx = 75;
    public static final int yy = 76;
    public static final int yz = 77;
    public static final int zA = 136;
    public static final int zB = 137;
    public static final int zC = 138;
    public static final int zD = 139;
    public static final int zE = 140;
    public static final int zF = 141;
    public static final int zG = 142;
    public static final int zH = 143;
    public static final int zI = 145;
    public static final int zJ = 146;
    public static final int zK = 147;
    public static final int zL = 148;
    public static final int zM = 149;
    public static final int zN = 150;
    public static final int zO = 151;
    public static final int zP = 152;
    public static final int zQ = 153;
    public static final int zR = 154;
    public static final int zS = 155;
    public static final int zT = 156;
    public static final int zU = 157;
    public static final int zV = 158;
    public static final int zW = 159;
    public static final int zX = 160;
    public static final int zY = 161;
    public static final int zZ = 162;
    public static final int za = 108;
    public static final int zb = 109;
    public static final int zc = 110;
    public static final int zd = 111;
    public static final int ze = 112;
    public static final int zf = 113;
    public static final int zg = 114;
    public static final int zh = 115;
    public static final int zi = 116;
    public static final int zj = 117;
    public static final int zk = 118;
    public static final int zl = 119;
    public static final int zm = 120;
    public static final int zn = 121;
    public static final int zo = 122;
    public static final int zp = 123;
    public static final int zq = 126;
    public static final int zr = 127;
    public static final int zs = 128;
    public static final int zt = 129;
    public static final int zu = 130;
    public static final int zv = 131;
    public static final int zw = 132;
    public static final int zx = 133;
    public static final int zy = 134;
    public static final int zz = 135;
    private MultiWindowDialogEx AA;
    public int AD = -1;
    int AH = 0;
    private boolean AI = false;
    Dialog AJ = null;
    private boolean AK = false;
    private Dialog AL = null;
    private String AN;
    private long AO;
    private long AP;
    private QuitThread AQ = new QuitThread();
    /* access modifiers changed from: private */
    public boolean AR = false;
    /* access modifiers changed from: private */
    public byte AS = 0;
    private View AU;
    /* access modifiers changed from: private */
    public Button AV;
    private RelativeLayout.LayoutParams AW;
    /* access modifiers changed from: private */
    public ViewReadingPageTail AX;
    private int AY = -1;
    /* access modifiers changed from: private */
    public boolean Ak = false;
    private ProgressDialog Am;
    private String An = null;
    private TextView Ao = null;
    private BroadcastReceiver Ap;
    private boolean Aq = false;
    private boolean Ar = false;
    private boolean As = false;
    private boolean At = true;
    private boolean Au = true;
    private View Av = null;
    int Aw;
    int Ax;
    int Ay;
    int Az;
    /* access modifiers changed from: private */
    public View bi;
    /* access modifiers changed from: private */
    public ActivityBrowser bk;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
         arg types: [com.uc.browser.ModelBrowser, int]
         candidates:
          com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
          com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
          com.uc.browser.ModelBrowser.a(int, long):void
          com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
          com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
          com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
         arg types: [int, int]
         candidates:
          com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
          com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
          com.uc.browser.ModelBrowser.a(int, long):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
          com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
          com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
          com.uc.browser.ModelBrowser.a(int, java.lang.Object):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
         arg types: [com.uc.browser.ModelBrowser$YesOrNoDlgData, int]
         candidates:
          com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
          com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
          com.uc.browser.ModelBrowser.a(int, long):void
          com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
          com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
          com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, boolean):boolean
         arg types: [com.uc.browser.ModelBrowser, int]
         candidates:
          com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, int):int
          com.uc.browser.ModelBrowser.b(android.content.Context, int):android.app.Dialog
          com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, android.content.Context):android.app.Dialog
          com.uc.browser.ModelBrowser.b(android.view.View, boolean):void
          com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, java.lang.Object):void
          com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, java.lang.String):void
          com.uc.browser.ModelBrowser.b(boolean, boolean):void
          com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, boolean):boolean */
        public void handleMessage(Message message) {
            Object[] objArr;
            WebViewJUC webViewJUC;
            ActivityBrowser unused = ModelBrowser.this.bk;
            if (!ActivityBrowser.bqO) {
                switch (message.what) {
                    case 10:
                        int intValue = message.obj != null ? ((Integer) message.obj).intValue() : 1;
                        ModelBrowser.this.aO(intValue);
                        if (intValue == 1 && ModelBrowser.this.vC != null && ModelBrowser.this.vC.ep != null) {
                            ModelBrowser.this.vC.ep.rA();
                            return;
                        }
                        return;
                    case 11:
                        ModelBrowser.this.X((String) message.obj);
                        return;
                    case 12:
                        ModelBrowser.this.stopLoading();
                        return;
                    case 13:
                        ModelBrowser.this.a((Integer) message.obj);
                        return;
                    case 14:
                        ModelBrowser.this.hk();
                        return;
                    case 15:
                        Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1412maxwindownumber, 0).show();
                        return;
                    case 16:
                        ModelBrowser.this.s((WindowUCWeb) message.obj);
                        return;
                    case 17:
                        ModelBrowser.this.a((WindowUCWeb) message.obj, (WindowUCWeb) null, message.arg1);
                        return;
                    case 18:
                        ModelBrowser.this.aU(((Integer) message.obj).intValue());
                        return;
                    case 19:
                        ModelBrowser.this.refresh();
                        return;
                    case 20:
                    case UCR.Color.bSo /*45*/:
                    case UCR.Color.bSp /*46*/:
                    case 68:
                    case UCR.Color.bSM /*73*/:
                    case 75:
                    case 79:
                    case 82:
                    case UCR.Color.bSW /*84*/:
                    case 100:
                    case UCR.Color.bwj /*107*/:
                    case UCR.Color.bTm /*124*/:
                    case UCR.Color.bTn /*125*/:
                    case UCR.Color.bTG /*144*/:
                    default:
                        return;
                    case 21:
                        ModelBrowser.this.bk.t((String) message.obj);
                        return;
                    case 22:
                        ModelBrowser.this.aP(message.arg1);
                        return;
                    case 23:
                        ModelBrowser.this.hn();
                        return;
                    case 24:
                        ModelBrowser.this.mHandler.removeMessages(24);
                        ModelBrowser.this.mHandler.sendMessageDelayed(ModelBrowser.this.mHandler.obtainMessage(24), ModelBrowser.we);
                        ModelBrowser.this.gG();
                        return;
                    case 25:
                        if (ModelBrowser.this.wg != null && ModelBrowser.this.wg.isHeld()) {
                            ModelBrowser.this.wg.release();
                            return;
                        }
                        return;
                    case 26:
                        String[] strArr = (String[]) message.obj;
                        if (strArr != null && strArr[0] != null && strArr[3] != null) {
                            ModelBrowser.this.e(strArr);
                            return;
                        }
                        return;
                    case 27:
                        ModelBrowser.this.hu();
                        return;
                    case 28:
                        if (ModelBrowser.this.vK != null) {
                            ModelBrowser.this.vK.m((String) message.obj);
                            return;
                        }
                        return;
                    case 29:
                        ModelBrowser.this.clearCache(true);
                        return;
                    case 30:
                        if (ModelBrowser.this.vK != null) {
                            ModelBrowser.this.vK.bA();
                            return;
                        }
                        return;
                    case 31:
                        if (ModelBrowser.this.vK != null) {
                            ModelBrowser.this.vK.bB();
                            return;
                        }
                        return;
                    case 32:
                        ModelBrowser.this.hQ();
                        return;
                    case 33:
                        ModelBrowser.this.ac(message.obj.toString());
                        return;
                    case 34:
                        ModelBrowser.this.aR(message.arg1);
                        return;
                    case 35:
                        if (1 == ((Integer) message.obj).intValue()) {
                            ModelBrowser.this.hy();
                            return;
                        } else if (((Integer) message.obj).intValue() == 0) {
                            ModelBrowser.this.hz();
                            return;
                        } else {
                            return;
                        }
                    case 36:
                        ModelBrowser.this.l(message.obj);
                        return;
                    case 37:
                        ModelBrowser.this.a(message.obj.toString());
                        return;
                    case 38:
                        ModelBrowser.this.hS();
                        return;
                    case 39:
                        ModelBrowser.this.g(message.arg1, message.obj);
                        return;
                    case 40:
                        ModelBrowser.this.ho();
                        return;
                    case 41:
                        ModelBrowser.this.hq();
                        return;
                    case 42:
                        ModelBrowser.this.hU();
                        return;
                    case 43:
                        ModelBrowser.this.C(((Boolean) message.obj).booleanValue());
                        ModelBrowser.this.hC();
                        return;
                    case 44:
                        e.nR().oj().ok();
                        System.gc();
                        return;
                    case 47:
                        WebViewDatabase.getInstance(ModelBrowser.this.bk).clearFormData();
                        System.gc();
                        return;
                    case 48:
                        ModelBrowser.this.gC();
                        return;
                    case 49:
                        ModelBrowser.gD().aS(129);
                        ModelBrowser.gD().a(10, (Object) 0);
                        return;
                    case 50:
                        ModelBrowser.this.hH();
                        return;
                    case 51:
                        ModelBrowser.this.gT();
                        System.gc();
                        return;
                    case 52:
                        if (ModelBrowser.this.vJ != null) {
                            ModelBrowser.this.vJ.cI(null);
                            return;
                        }
                        return;
                    case 53:
                        Object[] objArr2 = (Object[]) message.obj;
                        if (objArr2 != null && objArr2.length > 1) {
                            ModelBrowser.this.a((WindowUCWeb) objArr2[0], (WindowUCWeb) objArr2[1], message.arg1);
                            return;
                        }
                        return;
                    case 54:
                        ModelBrowser.this.hO();
                        System.gc();
                        return;
                    case 55:
                        if (ModelBrowser.this.bi instanceof WebViewJUC) {
                            ((WebViewJUC) ModelBrowser.this.bi).e(21, new KeyEvent(0, 21));
                            return;
                        }
                        return;
                    case 56:
                        if (ModelBrowser.this.bi instanceof WebViewJUC) {
                            ((WebViewJUC) ModelBrowser.this.bi).e(22, new KeyEvent(0, 22));
                            return;
                        }
                        return;
                    case 57:
                        int intValue2 = ((Integer) message.obj).intValue();
                        ModelBrowser.this.gW();
                        if (ModelBrowser.this.wk != null) {
                            ModelBrowser.this.wk.setState(intValue2);
                            if ((ModelBrowser.this.bi instanceof WebViewJUC) && !((WebViewJUC) ModelBrowser.this.bi).ae()) {
                                ModelBrowser.this.wk.setVisibility(0);
                                return;
                            }
                            return;
                        }
                        return;
                    case 58:
                        try {
                            Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1413out_of_memory, 0).show();
                            return;
                        } catch (OutOfMemoryError e) {
                            return;
                        }
                    case 59:
                        ModelBrowser.this.ik();
                        return;
                    case 60:
                        ModelBrowser.this.gW();
                        if (ModelBrowser.this.wk != null) {
                            ModelBrowser.this.wk.CL();
                            int state = ModelBrowser.this.wk.getState();
                            if (state > 0) {
                                if ((ModelBrowser.this.bi instanceof WebViewJUC) && !((WebViewJUC) ModelBrowser.this.bi).ae()) {
                                    ModelBrowser.this.wk.setVisibility(0);
                                }
                                ModelBrowser.this.vT.bO(true);
                                ModelBrowser.this.hE();
                            } else {
                                ModelBrowser.this.wk.setVisibility(8);
                                ModelBrowser.this.vT.bO(false);
                            }
                            g.Jn().il(state);
                            return;
                        }
                        return;
                    case 61:
                        ModelBrowser.this.dt();
                        return;
                    case 62:
                        if (message.obj != null) {
                            ModelBrowser.this.ad(message.obj.toString());
                            return;
                        }
                        return;
                    case 63:
                        if (ModelBrowser.this.vK != null && message.obj != null) {
                            ModelBrowser.this.vK.a(message.obj);
                            return;
                        }
                        return;
                    case 64:
                        if (1 == message.arg1) {
                            ModelBrowser.this.B(true);
                            return;
                        } else {
                            ModelBrowser.this.B(false);
                            return;
                        }
                    case 65:
                        if (message.obj != null) {
                            Toast.makeText(ModelBrowser.this.bk, message.obj.toString(), 1).show();
                            return;
                        }
                        return;
                    case 66:
                        ModelBrowser.this.hC();
                        return;
                    case 67:
                        ModelBrowser.this.hV();
                        return;
                    case 69:
                        if (ModelBrowser.this.vU != null) {
                            ModelBrowser.this.vU.bringToFront();
                            System.gc();
                            return;
                        }
                        return;
                    case 70:
                        ModelBrowser.this.a(Boolean.valueOf(((Integer) message.obj).intValue() > 0));
                        if (((Integer) message.obj).intValue() == 1) {
                            ModelBrowser.this.hD();
                            return;
                        }
                        return;
                    case 71:
                        ModelBrowser.this.a((String) message.getData().get(ActivityEditMyNavi.bvi), (HashMap) message.obj, message.arg1);
                        return;
                    case 72:
                        try {
                            ModelBrowser.this.m(message.obj);
                            return;
                        } catch (Exception e2) {
                            return;
                        }
                    case 74:
                        ModelBrowser.this.W();
                        return;
                    case 76:
                        ModelBrowser.this.ib();
                        return;
                    case 77:
                        ModelBrowser.this.g(0);
                        return;
                    case 78:
                        ModelBrowser.this.g(1);
                        return;
                    case 80:
                        if (ModelBrowser.this.bi != null && ModelBrowser.this.bi.getClass() == WebViewJUC.class && (webViewJUC = (WebViewJUC) ModelBrowser.this.bi) != null) {
                            webViewJUC.v();
                            return;
                        }
                        return;
                    case 81:
                        ModelBrowser.this.k(message.obj);
                        return;
                    case 83:
                        ModelBrowser.this.a(message.obj, message.arg1);
                        return;
                    case 85:
                        ModelBrowser.this.j(message.obj);
                        return;
                    case 86:
                        ModelBrowser.this.gB();
                        return;
                    case 87:
                        ModelBrowser.this.P();
                        return;
                    case 88:
                        if (message.obj != null) {
                            ModelBrowser.this.ab(message.obj.toString());
                            return;
                        }
                        return;
                    case 89:
                        WebViewJUC ia = ModelBrowser.this.ia();
                        if (ia != null) {
                            ia.aT();
                            return;
                        }
                        return;
                    case 90:
                        ModelBrowser.this.aT(((Integer) message.obj).intValue());
                        return;
                    case 91:
                        ModelBrowser.this.a((DownloadDlgData) message.obj, message.arg1);
                        return;
                    case 92:
                        ModelBrowser.this.a((c) message.obj);
                        return;
                    case 93:
                        try {
                            if (message.obj != null) {
                                Bundle bundle = (Bundle) message.obj;
                                ModelBrowser.this.l(bundle.getString(e.RF), bundle.getString(e.RH));
                                return;
                            }
                            return;
                        } catch (Exception e3) {
                            return;
                        }
                    case 94:
                        ModelBrowser.this.f(message.arg1, message.obj);
                        return;
                    case 95:
                        Vector qc = e.nR().qc();
                        if (qc == null || true == qc.isEmpty()) {
                            ModelBrowser.this.wK = true;
                            ModelBrowser.this.X("ext:update_sharepage");
                            return;
                        }
                        ModelBrowser.this.bk.startActivityForResult(new Intent(ModelBrowser.this.bk.getBaseContext(), ActivitySelector.class), 4);
                        return;
                    case 96:
                        ModelBrowser.this.a((YesOrNoDlgData) message.obj);
                        return;
                    case 97:
                        if (e.nR().nY().bw(h.afx).equals(e.RD)) {
                            ModelBrowser.this.b((YesOrNoDlgData) message.obj);
                            return;
                        } else {
                            ((YesOrNoDlgData) message.obj).agI.aV();
                            return;
                        }
                    case 98:
                        try {
                            if (message.obj != null) {
                                if (e.nR().nY().bw(h.afz).equals(e.RD)) {
                                    ModelBrowser.this.bk.Fv();
                                }
                                Toast.makeText(ModelBrowser.this.bk, message.obj.toString(), 1).show();
                                return;
                            }
                            return;
                        } catch (Exception e4) {
                            return;
                        }
                    case 99:
                        ModelBrowser.this.a((YesOrNoDlgData) message.obj, true);
                        return;
                    case 101:
                        if (ModelBrowser.this.bk != null) {
                            ModelBrowser.this.bk.Fr();
                            return;
                        }
                        return;
                    case 102:
                        ModelBrowser.this.ij();
                        return;
                    case 103:
                        System.gc();
                        return;
                    case 104:
                        try {
                            WebViewJUC ia2 = ModelBrowser.this.ia();
                            if (ia2 != null) {
                                ia2.kE(Integer.parseInt(message.obj.toString()));
                                return;
                            }
                            return;
                        } catch (Exception e5) {
                            return;
                        }
                    case 105:
                        ModelBrowser.this.il();
                        return;
                    case 106:
                        if (message.obj != null && (message.obj instanceof View)) {
                            ModelBrowser.this.a((ViewZoomControls.ZoomController) message.obj, Integer.valueOf(message.arg1).intValue());
                            return;
                        }
                        return;
                    case 108:
                        ModelBrowser.this.A(((Boolean) message.obj).booleanValue());
                        return;
                    case 109:
                        ModelBrowser.this.ha();
                        return;
                    case 110:
                        ModelBrowser.this.gx();
                        return;
                    case 111:
                        ModelBrowser.this.i(message.obj);
                        return;
                    case 112:
                        ModelBrowser.this.gw();
                        return;
                    case 113:
                        ModelBrowser.this.e(message.arg1, message.obj);
                        return;
                    case 114:
                        ModelBrowser.this.a(message.arg1 == 1, (String) message.obj);
                        return;
                    case 115:
                        ModelBrowser.this.a((Bundle) message.obj);
                        return;
                    case 116:
                        if (!com.uc.b.g.i(ModelBrowser.this.bk, "uc.ucplayer")) {
                            ModelBrowser.this.a(90, (Object) 2);
                            return;
                        } else {
                            d.a(ModelBrowser.this.bk, (byte[]) message.obj);
                            return;
                        }
                    case 117:
                        ModelBrowser.this.aL(message.arg1);
                        return;
                    case 118:
                        ModelBrowser.this.v(((Boolean) message.obj).booleanValue());
                        return;
                    case 119:
                        ModelBrowser.this.d((String) message.obj, message.arg1);
                        return;
                    case 120:
                        ModelBrowser.this.W((String) message.obj);
                        return;
                    case 121:
                        ModelBrowser.this.gu();
                        return;
                    case 122:
                        ModelBrowser.this.gv();
                        return;
                    case 123:
                        ModelBrowser.this.aK(message.arg1);
                        return;
                    case 126:
                        ModelBrowser.this.gt();
                        return;
                    case 127:
                        ModelBrowser.this.k((String) message.obj, "UPDATE_PROGRESS_URL");
                        return;
                    case 128:
                        ModelBrowser.this.gs();
                        return;
                    case 129:
                        if (ModelBrowser.this.vC != null && ModelBrowser.this.vC.eq != null) {
                            ModelBrowser.this.vC.eq.Ph();
                            return;
                        }
                        return;
                    case 130:
                        ModelBrowser.this.hp();
                        return;
                    case 131:
                        if (ModelBrowser.this.vC != null && ModelBrowser.this.vC.ep != null) {
                            ModelBrowser.this.vC.ep.rA();
                            return;
                        }
                        return;
                    case 132:
                        if (ModelBrowser.this.vT != null && !ModelBrowser.this.vT.IC()) {
                            ModelBrowser.this.vT.IQ();
                            return;
                        }
                        return;
                    case 133:
                        if (ModelBrowser.this.bi != null) {
                            if (ModelBrowser.this.bi == ModelBrowser.this.vC) {
                                ModelBrowser.this.bi.requestLayout();
                                ModelBrowser.this.bi.invalidate();
                            } else if (ModelBrowser.this.vP != null) {
                                ModelBrowser.this.vP.requestLayout();
                                ModelBrowser.this.vP.invalidate();
                            }
                        }
                        System.gc();
                        return;
                    case 134:
                        if (ModelBrowser.this.vC != null && ModelBrowser.this.vC.ep != null) {
                            ModelBrowser.this.vC.ep.ap();
                            return;
                        }
                        return;
                    case 135:
                        ModelBrowser.this.e(ModelBrowser.this.bi);
                        return;
                    case 136:
                        try {
                            if (ModelBrowser.this.bk != null) {
                                Object[] objArr3 = (Object[]) message.obj;
                                String obj = objArr3[0].toString();
                                String obj2 = objArr3[1].toString();
                                String obj3 = objArr3[2].toString();
                                String obj4 = objArr3[3].toString();
                                String obj5 = objArr3[4].toString();
                                Intent intent = new Intent();
                                intent.putExtra(ActivityEditMyNavi.bvj, obj);
                                intent.putExtra("filePath", obj2);
                                intent.putExtra("fileName", obj3);
                                intent.putExtra("message", obj4);
                                intent.putExtra("safestatus", obj5);
                                intent.setClass(ModelBrowser.this.bk, ActivityDownloadDialog.class);
                                ModelBrowser.this.bk.startActivity(intent);
                                return;
                            }
                            return;
                        } catch (Exception e6) {
                            return;
                        }
                    case 137:
                        if (ModelBrowser.this.xm != null) {
                            ModelBrowser.this.xm.a(message.arg1, message.obj.toString(), 0);
                            return;
                        }
                        return;
                    case 138:
                        if (ModelBrowser.this.xm != null) {
                            Object[] objArr4 = (Object[]) message.obj;
                            ModelBrowser.this.xm.a(((Integer) objArr4[0]).intValue(), (String) objArr4[1], ((Integer) objArr4[2]).intValue());
                            return;
                        }
                        return;
                    case 139:
                        if (ModelBrowser.this.xm != null) {
                            ModelBrowser.this.xm.cancel(((Integer) message.obj).intValue());
                            return;
                        }
                        return;
                    case 140:
                        if (ModelBrowser.this.xm != null) {
                            ModelBrowser.this.xm.n(message.arg1, (String) message.obj);
                            return;
                        }
                        return;
                    case 141:
                        if (ModelBrowser.this.xm != null) {
                            ModelBrowser.this.xm.yU();
                            return;
                        }
                        return;
                    case 142:
                        Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1400avaliablespacenotenough, 0).show();
                        return;
                    case 143:
                        Boolean bool = (Boolean) message.obj;
                        return;
                    case ModelBrowser.zI /*145*/:
                        ModelBrowser.this.gX();
                        return;
                    case ModelBrowser.zJ /*146*/:
                        ModelBrowser.this.g((Context) message.obj);
                        return;
                    case ModelBrowser.zK /*147*/:
                        ModelBrowser.this.it();
                        return;
                    case ModelBrowser.zL /*148*/:
                        if (message.obj != null && (objArr = (Object[]) message.obj) != null && objArr.length >= 2) {
                            d.b((Context) objArr[0], objArr[1].toString());
                            return;
                        }
                        return;
                    case ModelBrowser.zM /*149*/:
                        ModelBrowser.this.gr();
                        return;
                    case 150:
                        if (message.obj != null) {
                            Object[] objArr5 = (Object[]) message.obj;
                            ModelBrowser.this.a((YesOrNoDlgData) objArr5[0], (String) objArr5[1], (String) objArr5[2]);
                            return;
                        }
                        return;
                    case ModelBrowser.zO /*151*/:
                        e.nR().qx();
                        return;
                    case ModelBrowser.zP /*152*/:
                        if (ModelBrowser.this.vU != null) {
                            ModelBrowser.this.vU.cY(true);
                            return;
                        }
                        return;
                    case ModelBrowser.zQ /*153*/:
                        ModelBrowser.this.aM(message.arg1);
                        return;
                    case ModelBrowser.zR /*154*/:
                        ModelBrowser.this.iu();
                        return;
                    case ModelBrowser.zS /*155*/:
                        if (message.obj != null) {
                            ModelBrowser.this.ai((String) message.obj);
                            return;
                        }
                        return;
                    case ModelBrowser.zT /*156*/:
                        ModelBrowser.this.F(((Boolean) message.obj).booleanValue());
                        return;
                    case ModelBrowser.zU /*157*/:
                        int[] iArr = (int[]) message.obj;
                        ModelBrowser.this.y(iArr[0], iArr[1]);
                        return;
                    case ModelBrowser.zV /*158*/:
                        try {
                            if (message.obj != null) {
                                Object[] objArr6 = (Object[]) message.obj;
                                String obj6 = objArr6[0].toString();
                                n nVar = (n) objArr6[1];
                                if (ModelBrowser.this.vK != null) {
                                    ModelBrowser.this.vK.a(obj6, nVar);
                                    return;
                                }
                                return;
                            }
                            return;
                        } catch (Exception e7) {
                            return;
                        }
                    case ModelBrowser.zW /*159*/:
                        ModelBrowser.this.iz();
                        return;
                    case ModelBrowser.zX /*160*/:
                        ab.sl().a((Object[]) message.obj, message.arg1 == 0);
                        return;
                    case ModelBrowser.zY /*161*/:
                        if (ModelBrowser.this.vE != null) {
                            ModelBrowser.this.vE.aD();
                            return;
                        }
                        return;
                    case ModelBrowser.zZ /*162*/:
                        String qG = e.nR().qG();
                        if (qG != null) {
                            if (!qG.startsWith("ext:es")) {
                                qG = "ext:es:passport:" + qG;
                            }
                            ModelBrowser.this.X(qG);
                            return;
                        }
                        return;
                    case ModelBrowser.Aa /*163*/:
                        String qI = e.nR().qI();
                        if (qI != null) {
                            String str = !qI.startsWith("ext:es") ? "ext:es:passport:" + qI : qI;
                            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ModelBrowser.this.bk);
                            int i = defaultSharedPreferences.getInt(ModelBrowser.Al, 0);
                            if (i < 5 && !ModelBrowser.this.Ak && ((Boolean) message.obj).booleanValue()) {
                                Toast.makeText(ModelBrowser.this.bk, (int) R.string.f972toast_enter_passport, 0).show();
                                boolean unused2 = ModelBrowser.this.Ak = true;
                                defaultSharedPreferences.edit().putInt(ModelBrowser.Al, i + 1).commit();
                            }
                            ModelBrowser.this.X(str);
                            return;
                        }
                        return;
                    case ModelBrowser.Ab /*164*/:
                        ModelBrowser.this.gq();
                        return;
                    case ModelBrowser.Ac /*165*/:
                        ModelBrowser.this.iJ();
                        return;
                    case ModelBrowser.Ad /*166*/:
                        ModelBrowser.this.a(message);
                        return;
                    case ModelBrowser.Ae /*167*/:
                        ModelBrowser.this.gp();
                        return;
                    case ModelBrowser.Af /*168*/:
                        e.nR().w(h.afD, "0");
                        return;
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public n p = new n() {
        Dialog Sj = null;

        public void G(final String str, final String str2) {
            if (this.Sj != null) {
                qU();
            }
            ModelBrowser.this.bk.runOnUiThread(new Runnable() {
                public void run() {
                    UCAlertDialog.Builder builder = new UCAlertDialog.Builder(ModelBrowser.this.bk);
                    builder.L(str);
                    View inflate = LayoutInflater.from(ModelBrowser.this.bk).inflate((int) R.layout.f929progress_dlg_content, (ViewGroup) null);
                    TextView textView = (TextView) inflate.findViewById(R.id.message);
                    textView.setTextColor(com.uc.h.e.Pb().getColor(7));
                    textView.setText(str2);
                    builder.c(inflate);
                    builder.setCancelable(false);
                    AnonymousClass42.this.Sj = builder.fh();
                    AnonymousClass42.this.Sj.show();
                }
            });
        }

        public void H(String str, String str2) {
            ModelBrowser.this.l(str, str2);
        }

        public void a(Object obj) {
            ModelBrowser.this.a(11, "ext:null");
            ModelBrowser.this.a(63, obj);
        }

        public void a(String str, String str2, String str3, String str4, int i) {
            ModelBrowser.this.a(136, new String[]{str, str2, str3, str4, String.valueOf(i)});
        }

        public boolean a(String str, String str2, int i, Object obj, boolean z) {
            switch (i) {
                case 0:
                    ModelBrowser.this.a(91, 0, new DownloadDlgData(str, str2, (String) obj));
                    break;
                case 1:
                    ModelBrowser.this.a(96, new YesOrNoDlgData(str, str2, (q) obj));
                    break;
                case 2:
                    ModelBrowser.this.a(97, new YesOrNoDlgData(str, str2, (q) obj));
                    break;
                case 3:
                    ModelBrowser.this.a(99, new YesOrNoDlgData(str, str2, (q) obj));
                    break;
                case 4:
                    ModelBrowser.this.a(96, new YesOrNoDlgData(str, str2, new q() {
                        String br;

                        public void aU() {
                            ac.eE("flash").cB(this.br);
                        }

                        public q av(String str) {
                            this.br = str;
                            return this;
                        }
                    }.av((String) obj)));
                    break;
                case 5:
                    ModelBrowser.this.a(91, 5, new DownloadDlgData(str, str2, (String) obj));
                    break;
                case 6:
                    ModelBrowser.this.a(96, new YesOrNoDlgData(str, str2, new q() {
                        public void aU() {
                            ModelBrowser.this.bk.startActivity(new Intent(ModelBrowser.this.getContext(), ActivityDownload.class));
                        }
                    }));
                    break;
                case 7:
                    ModelBrowser.this.a(91, 7, new DownloadDlgData(str, str2, (String) obj));
                    break;
            }
            return false;
        }

        public void aD() {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(ModelBrowser.zY);
            }
        }

        public void aG(boolean z) {
            try {
                new GeoLocationManager(ModelBrowser.this.bk).f(z);
            } catch (Exception e) {
            }
        }

        public void bK(String str) {
            ModelBrowser.this.a(62, str);
        }

        public boolean bL(String str) {
            if (str == null) {
                return false;
            }
            ModelBrowser.this.a(21, str);
            return false;
        }

        public boolean bM(String str) {
            if (ModelBrowser.this.vJ == null) {
                return true;
            }
            ModelBrowser.this.a(39, 2, str);
            return true;
        }

        public void bN(String str) {
            ModelBrowser.this.a(65, str);
        }

        public void bO(String str) {
            ModelBrowser.this.a(98, str);
        }

        public byte[] bP(String str) {
            try {
                return e.nR().a(ModelBrowser.this.bk.getAssets().open(str));
            } catch (IOException e) {
                return null;
            }
        }

        public InputStream bQ(String str) {
            if (str == null) {
                return null;
            }
            try {
                String trim = str.trim();
                if (trim.length() == 0) {
                    return null;
                }
                if (ModelBrowser.this.bk != null) {
                    return ModelBrowser.this.bk.getAssets().open(trim);
                }
                return null;
            } catch (Exception e) {
                return null;
            }
        }

        public void bR(String str) {
            Intent intent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
            intent.setData(Uri.fromFile(new File(str)));
            ModelBrowser.this.bk.sendBroadcast(intent);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.uc.browser.ModelBrowser.c(com.uc.browser.ModelBrowser, boolean):boolean
         arg types: [com.uc.browser.ModelBrowser, int]
         candidates:
          com.uc.browser.ModelBrowser.c(com.uc.browser.ModelBrowser, android.content.Context):android.app.Dialog
          com.uc.browser.ModelBrowser.c(com.uc.browser.ModelBrowser, java.lang.String):void
          com.uc.browser.ModelBrowser.c(boolean, boolean):void
          com.uc.browser.ModelBrowser.c(com.uc.browser.ModelBrowser, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
         arg types: [int, int]
         candidates:
          com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
          com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
          com.uc.browser.ModelBrowser.a(int, long):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
          com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
          com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
          com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
          com.uc.browser.ModelBrowser.a(int, java.lang.Object):void */
        public boolean dO(int i) {
            if (i > 0) {
                if ((i >= 100 ? 99 : i) >= 99) {
                    boolean unused = ModelBrowser.this.wA = false;
                    ModelBrowser.this.a(64, (Object) 0);
                }
            }
            return false;
        }

        public void dP(int i) {
            ModelBrowser.this.a(139, Integer.valueOf(i));
        }

        public void i(int i, String str) {
            ModelBrowser.this.a(137, i, str);
        }

        public void ij() {
            ModelBrowser.this.aS(102);
        }

        public void j(int i, String str) {
            ModelBrowser.this.a(140, i, str);
        }

        public boolean ni() {
            return UcCamera.wu();
        }

        public void pQ() {
            if (ModelBrowser.this.wK || true != e.nR().pP()) {
                e.nR().aC(false);
            } else {
                e.nR().pQ();
            }
        }

        public void q(Object obj) {
            ModelBrowser.this.a(138, obj);
        }

        public void qS() {
            ModelBrowser.this.aS(ModelBrowser.Ac);
        }

        public void qT() {
            ModelBrowser.this.aS(ModelBrowser.Ac);
        }

        public void qU() {
            ModelBrowser.this.bk.runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        if (AnonymousClass42.this.Sj != null) {
                            Dialog dialog = AnonymousClass42.this.Sj;
                            AnonymousClass42.this.Sj = null;
                            dialog.dismiss();
                        }
                    } catch (Exception e) {
                    }
                }
            });
        }

        public boolean qV() {
            if (true != ModelBrowser.this.hj().wg()) {
                return false;
            }
            ModelBrowser.this.aS(52);
            return false;
        }

        public boolean qW() {
            if (ModelBrowser.this.bi == null) {
                return false;
            }
            if (ModelBrowser.this.bi.getClass() == ViewMainpage.class) {
                WebViewJUC cz = ((ViewMainpage) ModelBrowser.this.bi).cz();
                if (cz == null) {
                    return false;
                }
                cz.postInvalidate();
            } else if (ModelBrowser.this.bi.getClass() == WebViewJUC.class) {
                ((WebViewJUC) ModelBrowser.this.bi).postInvalidate();
            }
            return false;
        }

        public int qX() {
            if (ModelBrowser.this.bk == null) {
                return 0;
            }
            Display defaultDisplay = ModelBrowser.this.bk.getWindowManager().getDefaultDisplay();
            return 2 == ModelBrowser.this.bk.getResources().getConfiguration().orientation ? defaultDisplay.getHeight() : defaultDisplay.getWidth();
        }

        public int qY() {
            if (ModelBrowser.this.bk == null) {
                return 0;
            }
            Display defaultDisplay = ModelBrowser.this.bk.getWindowManager().getDefaultDisplay();
            return 2 == ModelBrowser.this.bk.getResources().getConfiguration().orientation ? defaultDisplay.getWidth() : defaultDisplay.getHeight();
        }

        public boolean qZ() {
            if (ModelBrowser.this.wj != null) {
                return ModelBrowser.this.wj.getVisibility() == 0;
            }
            return false;
        }

        public void qg() {
            new saveCDDataThread().start();
        }

        public f[] ra() {
            return new f[]{f.a(ModelBrowser.this.bk.getResources(), R.drawable.f593navi_3guc), f.a(ModelBrowser.this.bk.getResources(), R.drawable.f595navi_sina), f.a(ModelBrowser.this.bk.getResources(), R.drawable.f596navi_uzone), f.a(ModelBrowser.this.bk.getResources(), R.drawable.f594navi_bibei)};
        }

        public void rb() {
            ModelBrowser.this.aS(141);
        }

        public void rc() {
        }

        public void rd() {
            ModelBrowser.this.a(142, (Object) null);
        }

        public void re() {
            ModelBrowser.this.aS(131);
        }

        public boolean shouldOverrideUrlLoading(String str) {
            ModelBrowser.this.a(11, str);
            return false;
        }
    };
    public final String tY = "buffer";
    private Activity vB;
    /* access modifiers changed from: private */
    public ViewMainpage vC;
    private com.uc.h.e vD;
    /* access modifiers changed from: private */
    public ViewWebSchMainPage vE;
    private URLSearchBarManager vF;
    private WebsiteSearchSchDialog vG;
    private WebsiteSearchWebDialog vH;
    protected int vI = 0;
    /* access modifiers changed from: private */
    public MultiWindowManager vJ;
    /* access modifiers changed from: private */
    public WindowUCWeb vK;
    private RelativeLayout vM;
    private RelativeLayout.LayoutParams vN;
    private RelativeLayout vO;
    /* access modifiers changed from: private */
    public RelativeLayout vP;
    private ViewMainBar vQ;
    private String vR;
    /* access modifiers changed from: private */
    public MenuDialog vT;
    /* access modifiers changed from: private */
    public ViewControlBarFullScreen vU;
    private RelativeLayout.LayoutParams vV;
    private int vW = 20;
    private TitleBarLayout vX;
    private TextView vY;
    private TextView vZ;
    /* access modifiers changed from: private */
    public boolean wA = false;
    private int wB;
    /* access modifiers changed from: private */
    public String wC;
    /* access modifiers changed from: private */
    public int wD = 0;
    ViewZoomControls wJ = null;
    public boolean wK = false;
    private boolean wL = false;
    private ImageView wa;
    private ProgressBar wb;
    private ProgressBar wc;
    private boolean wd = false;
    /* access modifiers changed from: private */
    public PowerManager.WakeLock wg;
    private boolean wh = false;
    private boolean wi = false;
    /* access modifiers changed from: private */
    public DriftEditTextForRelativeLayout wj = null;
    /* access modifiers changed from: private */
    public ViewPageUpDownButton wk;
    private WebViewSafePopup wl;
    /* access modifiers changed from: private */
    public ViewCloseRefreshTimer wm;
    /* access modifiers changed from: private */
    public int xj = -1;
    private int xk = -1;
    /* access modifiers changed from: private */
    public NotificationMgr xm = null;

    public class DownloadDlgData {
        String aU;
        String agH;
        String avw;

        public DownloadDlgData(String str, String str2, String str3) {
            this.aU = str;
            this.agH = str2;
            this.avw = str3;
        }
    }

    abstract class DownloadOnClickListener implements View.OnClickListener {
        protected String[] bRB = null;

        DownloadOnClickListener(String[] strArr) {
            this.bRB = strArr;
        }
    }

    class QuitThread extends Thread {
        QuitThread() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.uc.browser.ModelBrowser.d(com.uc.browser.ModelBrowser, boolean):void
         arg types: [com.uc.browser.ModelBrowser, int]
         candidates:
          com.uc.browser.ModelBrowser.d(com.uc.browser.ModelBrowser, android.content.Context):android.app.Dialog
          com.uc.browser.ModelBrowser.d(com.uc.browser.ModelBrowser, java.lang.String):java.lang.String
          com.uc.browser.ModelBrowser.d(int, java.lang.String):void
          com.uc.browser.ModelBrowser.d(java.lang.String, int):void
          com.uc.browser.ModelBrowser.d(com.uc.browser.ModelBrowser, boolean):void */
        public void run() {
            while (!ActivityBrowser.bqP) {
                try {
                    Thread.sleep(200);
                } catch (Exception e) {
                }
            }
            if (ModelBrowser.this.AR) {
                ModelBrowser.this.gt();
            } else {
                ModelBrowser.this.w(false);
            }
            WebViewJUC.ceU = null;
            ModelBrowser.this.stopLoading();
            ModelBrowser.this.mHandler.removeCallbacksAndMessages(null);
            e.nR().oG();
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().hx();
            }
            ModelBrowser.gF();
            if (true == b.a.a.f.h(b.a.a.f.auu, false)) {
                b.a.a.f.wP();
            }
            b.a.a.f.cP(b.a.a.f.auv);
            if (ModelBrowser.this.xm != null) {
                ModelBrowser.this.xm.yU();
            }
            if (ModelBrowser.this.bk != null) {
                try {
                    ModelBrowser.this.bk.getApplicationContext().unregisterReceiver(com.uc.b.c.fA());
                } catch (Exception e2) {
                }
                PreferenceManager.getDefaultSharedPreferences(ModelBrowser.this.bk).edit().putBoolean("uc_destroy", true).commit();
                ModelBrowser.this.bk.destroy();
            }
        }
    }

    public class YesOrNoDlgData {
        String aU;
        String agH;
        q agI;

        public YesOrNoDlgData(String str, String str2, q qVar) {
            this.aU = str;
            this.agH = str2;
            this.agI = qVar;
        }
    }

    class saveCDDataThread extends Thread {
        private saveCDDataThread() {
        }

        public void run() {
            e.nR().qg();
        }
    }

    private ModelBrowser() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
     arg types: [int, int]
     candidates:
      com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
      com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
      com.uc.browser.ModelBrowser.a(int, long):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
      com.uc.browser.ModelBrowser.a(int, java.lang.Object):void */
    private void a() {
        com.uc.h.e Pb = com.uc.h.e.Pb();
        Pb.d(this.bk);
        com.uc.h.d dVar = new com.uc.h.d(this.bk);
        if (ActivityBrowser.Fz()) {
            Pb.fY("theme/night.uct");
            Pb.update();
        } else {
            String string = PreferenceManager.getDefaultSharedPreferences(this.bk).getString(com.uc.h.d.bxp, null);
            if (string != null) {
                dVar.eT(string);
            } else {
                int parseInt = Integer.parseInt(e.nR().nY().bw(h.afa));
                if (parseInt == -1) {
                    e.nR().w(h.afa, String.valueOf(0));
                    dVar.hy(0);
                } else if (parseInt >= dVar.GS().size() || dVar.GS().get(parseInt) == null || !((com.uc.h.c) dVar.GS().get(parseInt)).Dj()) {
                    dVar.hy(0);
                } else {
                    dVar.hy(parseInt);
                }
            }
        }
        this.vQ = new ViewMainBar(this.bk);
        this.vT = new MenuDialog(this.bk);
        this.vF = new URLSearchBarManager(this.bk);
        if (this.vU != null) {
            this.vU.destroyDrawingCache();
            this.vU = null;
            this.vU = null;
            this.vV = null;
        }
        if (this.wm != null) {
            this.wm.removeAllViews();
            this.wm.destroyDrawingCache();
            this.wm = null;
        }
        this.vJ = new MultiWindowManager();
        if (true == ActivityUpdate.IP) {
            this.vK = this.vJ.cJ(null);
            if (e.RD.equals(e.nR().nY().bw(h.afK))) {
                a(18, (Object) 1);
            }
            if (e.RD.equals(e.nR().nY().bw(h.afN))) {
                a(33, (Object) 3);
            }
            if (e.RD.equals(e.nR().nY().bw(h.afy))) {
                a(70, (Object) 3);
            }
        }
        g Jn = g.Jn();
        Jn.Jo();
        Jn.update();
        ac.EY();
        ac.setActivity(this.bk);
        Intent intent = new Intent(this.bk, ActivityDownload.class);
        intent.setAction("com.uc.browser.clickDownloadNotification");
        this.xm = new NotificationMgr(this.bk, PendingIntent.getActivity(this.bk, 0, intent, 0));
        IntentFilter intentFilter = new IntentFilter("android.intent.action.MEDIA_MOUNTED");
        intentFilter.addAction("android.intent.action.MEDIA_EJECT");
        intentFilter.addDataScheme("file");
        this.bk.getApplicationContext().registerReceiver(com.uc.b.c.fA(), intentFilter);
        if (this.bk.gR() != -1) {
            return;
        }
        if (ActivityBrowser.Fq() < 550) {
            this.bk.aN(1);
        } else {
            this.bk.aN(0);
        }
    }

    /* access modifiers changed from: private */
    public void a(Bundle bundle) {
        Intent intent = new Intent(this.bk, ActivityFlash.class);
        intent.putExtras(bundle);
        this.bk.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void a(Message message) {
        if (message != null && message.obj != null) {
            String str = (String) message.obj;
            if (message.arg1 == 1) {
                Toast.makeText(this.bk, str, 0).show();
            } else {
                af(str);
            }
        }
    }

    private void a(View view, boolean z) {
        if (view == this.vC) {
            if (this.vC.getParent() != null) {
                ((ViewGroup) this.vC.getParent()).removeAllViews();
            }
            this.bk.setContentView(this.vC);
            return;
        }
        this.wd = z;
        this.vP = (RelativeLayout) LayoutInflater.from(this.bk).inflate((int) R.layout.f884browser, (ViewGroup) null);
        iD();
        iy();
        this.bk.setContentView(this.vP);
        this.vM = (RelativeLayout) this.vP.findViewById(R.id.f700Browser_MainPage);
        this.vO = (RelativeLayout) this.vP.findViewById(R.id.f699controlbar_layout);
        this.vX = (TitleBarLayout) this.vP.findViewById(R.id.f691titlebar_layout);
        this.vX.k();
        this.vY = (TextView) this.vP.findViewById(R.id.f654Browser_TitleBar);
        this.vZ = (TextView) this.vP.findViewById(R.id.f694Browser_TimeBar);
        this.wa = (ImageView) this.vP.findViewById(R.id.f695browser_safemark);
        this.wb = (ProgressBar) this.vP.findViewById(R.id.f693title_progress);
        this.wc = (ProgressBar) this.vP.findViewById(R.id.f698fullscreen_progress);
        if (this.AD > 0 && this.AD < 96) {
            this.wb.setProgress(this.AD);
            this.wc.setProgress(this.AD);
        }
        if (this.vR != null) {
            k(this.vR, "loadPage");
        }
        if (this.wd) {
            this.vY.setVisibility(0);
        } else {
            this.vY.setVisibility(8);
        }
        this.vM.addView(view, vL);
        this.vD.kp(R.dimen.f177controlbar_height);
        this.vO.removeView(this.vQ);
        this.vO.addView(this.vQ);
        if (this.vU != null) {
            hy();
            this.vM.addView(this.vU);
            ha();
            this.vP.findViewById(R.id.f699controlbar_layout).setVisibility(8);
        }
        iz();
        iJ();
    }

    /* access modifiers changed from: private */
    public void a(Object obj, int i) {
        if (an.vA().vC()) {
            WebViewJUC webViewJUC = null;
            if (this.bi != null && (this.bi instanceof WebViewJUC)) {
                webViewJUC = (WebViewJUC) this.bi;
            }
            if (webViewJUC != null) {
                webViewJUC.Qr();
            }
            if (obj != null ? ((Boolean) obj).booleanValue() : false) {
                String vP2 = an.vA().vP();
                switch (i) {
                    case WebViewJUC.cga /*4097*/:
                        if (vP2 != null) {
                            ((ClipboardManager) this.bk.getSystemService("clipboard")).setText(vP2);
                            Toast.makeText(this.bk, (int) R.string.f1437text_copied, 0).show();
                            break;
                        }
                        break;
                    case WebViewJUC.cgb /*4098*/:
                        a(94, 2, new String[]{vP2 + this.bk.getResources().getString(R.string.f957share_suffix), " ", vP2});
                        break;
                    case WebViewJUC.cgc /*4099*/:
                        if (!(e.nR() == null || e.nR().nZ() == null)) {
                            a(39, 2, e.nR().nZ().a(vP2, (byte) e.nR().nZ().oL()));
                            break;
                        }
                }
            } else {
                an.vA().vB();
                if (webViewJUC != null) {
                    webViewJUC.QC();
                }
            }
            gq();
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, HashMap hashMap, int i) {
        WebViewZoom webViewZoom;
        if (str != null && str.length() != 0 && this.bi == (webViewZoom = (WebViewZoom) hashMap.get("webview"))) {
            switch (i) {
                case com.uc.d.h.cdx /*655361*/:
                    if (this.vJ != null && str != null) {
                        String str2 = b.acH + ((this.vK == null || true != this.vK.bI() || str.contains(b.acI)) ? str : b.acI + str);
                        String Y = Y(str2);
                        if (Y != null) {
                            d(j(str2, Y));
                        } else {
                            this.vJ.cJ(str2);
                        }
                        b.a.a.f.l(0, b.a.a.f.att);
                        return;
                    }
                    return;
                case com.uc.d.h.cdy /*655362*/:
                    X(b.acH + str);
                    b.a.a.f.l(0, b.a.a.f.aty);
                    return;
                case com.uc.d.h.cdz /*655363*/:
                    if (str != null) {
                        String[] strArr = new String[5];
                        strArr[0] = str;
                        strArr[1] = webViewZoom.getUrl();
                        CookieSyncManager.createInstance(this.bk.getApplicationContext());
                        strArr[2] = CookieManager.getInstance().getCookie(str);
                        if (strArr[2] == null || strArr[2].length() == 0) {
                            strArr[2] = CookieManager.getInstance().getCookie(webViewZoom.getUrl());
                        }
                        strArr[3] = URLUtil.guessFileName(str, null, null);
                        strArr[4] = "down:webkit";
                        e(strArr);
                    }
                    b.a.a.f.l(0, b.a.a.f.atx);
                    return;
                case com.uc.d.h.cdA /*655364*/:
                case com.uc.d.h.cdD /*655367*/:
                case com.uc.d.h.cdE /*655368*/:
                default:
                    return;
                case com.uc.d.h.cdB /*655365*/:
                    this.AN = str;
                    Intent intent = new Intent();
                    intent.setAction(ActivityBookmarkEx.asd);
                    if (this.vK != null) {
                        intent.putExtra(ActivityBookmarkEx.asf, this.vK.getTitle());
                        intent.putExtra(ActivityBookmarkEx.asg, this.vK.getUrl());
                    }
                    intent.setClass(this.bk, ActivityBookmarkEx.class);
                    this.bk.startActivity(intent);
                    b.a.a.f.l(0, b.a.a.f.atr);
                    return;
                case com.uc.d.h.cdC /*655366*/:
                    this.AN = str;
                    hu();
                    b.a.a.f.l(0, b.a.a.f.atw);
                    return;
                case com.uc.d.h.cdF /*655369*/:
                    if (!(this.vJ == null || str == null)) {
                        String str3 = b.acH + ((this.vK == null || true != this.vK.bI() || str.contains(b.acI)) ? str : b.acI + str);
                        String Y2 = Y(str3);
                        if (Y2 != null) {
                            d(j(str3, Y2));
                        } else {
                            this.vJ.cI(str3);
                        }
                    }
                    b.a.a.f.l(0, b.a.a.f.atu);
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void aR(int i) {
        if (this.vU == null) {
            return;
        }
        if (i == 0) {
            this.vU.cV(false);
        } else if (1 == i) {
            this.vU.cV(true);
        }
    }

    /* access modifiers changed from: private */
    public void aa(String str) {
        if (str != null) {
            ((ClipboardManager) this.bk.getSystemService("clipboard")).setText(str);
            Toast.makeText(this.bk, (int) R.string.f1361dialog_pageattr_msg_copy, 0).show();
        }
    }

    /* access modifiers changed from: private */
    public void ad(String str) {
        View inflate = LayoutInflater.from(this.bk).inflate((int) R.layout.f888browser_dialog_upmsg, (ViewGroup) null);
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this.bk);
        builder.aH(R.string.f1425uptitle);
        builder.c(inflate);
        TextView textView = (TextView) inflate.findViewById(R.id.f720browser_upmsg);
        textView.setTextColor(com.uc.h.e.Pb().getColor(7));
        if (str == null || str.length() == 0) {
            textView.setText((int) R.string.f1426upmsg);
        } else {
            textView.setText(str);
        }
        builder.a((int) R.string.ok, (DialogInterface.OnClickListener) null);
        builder.fh().show();
    }

    /* access modifiers changed from: private */
    public void ai(String str) {
        try {
            if (!e.nR().a(new URI(str))) {
                Toast.makeText(this.bk, (int) R.string.f1520open_source_error, 0).show();
            } else {
                X(b.acU + str);
            }
        } catch (URISyntaxException e) {
        }
    }

    public static int ak(String str) {
        if (str != null) {
            return e.nR().bD(str);
        }
        return 0;
    }

    private Dialog b(final Context context, int i) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        if (i == 0) {
            builder.M(context.getString(R.string.f1175dialog_msg_tips_qvga));
        } else if (i == 1) {
            builder.M(context.getString(R.string.f1176dialog_msg_tips_not_qvga));
        } else if (i == 3) {
            builder.M(context.getString(R.string.f1200dialog_pad_tip));
        }
        if (i == 3) {
            builder.a((int) R.string.f965confirm, (DialogInterface.OnClickListener) null);
        } else {
            builder.a((int) R.string.f971gonow, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    ModelBrowser.gD().a(11, "http://wap.uc.cn");
                }
            });
        }
        if (i != 3) {
            builder.c((int) R.string.f1125alert_dialog_cancel, (DialogInterface.OnClickListener) null);
        }
        UCAlertDialog fh = builder.fh();
        fh.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                if (ModelBrowser.this.xj <= 0) {
                    ModelBrowser.this.i(context).show();
                }
            }
        });
        return fh;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
     arg types: [boolean, int]
     candidates:
      com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
      com.uc.browser.ModelBrowser.a(int, long):void
      com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
      com.uc.browser.ModelBrowser.a(java.lang.Object, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
      com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
      com.uc.browser.ModelBrowser.a(int, long):void
      com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
      com.uc.browser.ModelBrowser.a(android.view.View, boolean):void */
    private void b(View view, boolean z) {
        if (view != null) {
            a((Object) false, 0);
            iy();
            if (view == this.vC) {
                if (this.vM != null) {
                    this.vM.removeView(this.bi);
                }
                this.bi = view;
                this.bk.setContentView(this.vC);
                this.vC.P();
                aS(23);
                return;
            }
            if (this.bi != view) {
                if (this.vM == null) {
                    a(view, true);
                }
                this.bk.setContentView(this.vP);
                this.vM.removeView(this.bi);
                this.bi = view;
                this.vM.removeView(view);
                this.vM.addView(view, vL);
                e(view);
                if (this.wj != null) {
                    try {
                        this.vM.removeView(this.wj);
                        this.vM.addView(this.wj);
                    } catch (Exception e) {
                    }
                }
                try {
                    if (this.vO != null) {
                        if (!ActivityBrowser.bqy) {
                            this.vO.setVisibility(0);
                            this.vO.removeView(this.vQ);
                            this.vO.addView(this.vQ);
                        } else {
                            this.vO.setVisibility(8);
                        }
                    }
                    if (this.wk != null) {
                        this.wk.setVisibility(8);
                        try {
                            this.vM.removeView(this.wk);
                            this.vM.addView(this.wk);
                        } catch (Exception e2) {
                        }
                        this.wk.bringToFront();
                    }
                    if (this.wm != null) {
                        try {
                            this.vM.removeView(this.wm);
                            this.vM.addView(this.wm);
                        } catch (Exception e3) {
                        }
                    }
                    if (this.vU != null) {
                        try {
                            this.vM.removeView(this.vU);
                            this.vM.addView(this.vU);
                        } catch (Exception e4) {
                        }
                    }
                    view.requestFocus();
                } catch (Exception e5) {
                }
                if ((this.bi instanceof WebViewZoom) && this.wl != null) {
                    this.wl.setVisibility(8);
                }
            }
            if (this.wm != null) {
                this.wm.setVisibility(4);
                if (this.vK != null) {
                    this.vK.j(false);
                }
            }
            if (this.wJ != null) {
                this.wJ.setVisibility(8);
            }
            if (z) {
                f(view);
            }
            aS(23);
            P();
            boolean a2 = this.vK.a(this.bi);
            ViewMainBar.P(a2);
            if (this.vU != null) {
                this.vU.P(a2);
            }
            if (this.bi != null && (this.bi instanceof WebViewJUC)) {
                y(((WebViewJUC) this.bi).G(), 0);
            }
            iz();
            m.dq().bz();
        }
    }

    /* access modifiers changed from: private */
    public void clearCache(boolean z) {
        if (this.vJ != null) {
            this.vJ.bz();
        }
        e.nR().ae(z);
    }

    /* access modifiers changed from: private */
    public void e(View view) {
        if (view != null && this.vY != null && this.vZ != null) {
            boolean contains = e.nR().nY().bw(h.aeZ).contains(e.RD);
            if ((this.vK == null || !this.vK.a(view)) && (this.vU == null || contains)) {
                this.vX.setVisibility(0);
                this.vY.setVisibility(0);
                this.wc.setVisibility(8);
                this.wd = true;
                if (this.vK != null && this.vK.bk()) {
                    aP(this.vK.getProgress());
                }
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.vM.getLayoutParams();
                layoutParams.addRule(3, R.id.f691titlebar_layout);
                this.vM.setLayoutParams(layoutParams);
            } else {
                this.vX.setVisibility(8);
                this.wd = false;
                RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.vM.getLayoutParams();
                layoutParams2.addRule(3, R.id.f698fullscreen_progress);
                this.vM.setLayoutParams(layoutParams2);
                if (this.vK != null && this.vK.bk()) {
                    aP(this.vK.getProgress());
                }
            }
            hc();
            if (this.vK != null) {
                this.vK.r(0);
            }
        }
    }

    private void f(View view) {
        if (view instanceof WebViewJUC) {
            WebViewJUC webViewJUC = (WebViewJUC) view;
            if (this.wk == null) {
                gW();
            }
            if (this.wk.getParent() == null) {
                this.vM.addView(this.wk);
            }
            this.wk.bringToFront();
            if (!webViewJUC.ae()) {
                this.wk.setVisibility(0);
            } else {
                this.wk.setVisibility(8);
            }
        }
    }

    private boolean f(String[] strArr) {
        int cu;
        int cu2;
        g.Jn().getClass();
        if (!(1 != g.Jn().JZ() || (cu = d.cu(strArr[3])) == 22 || cu == 24 || cu == 25 || (cu2 = d.cu(strArr[0])) == 22 || cu2 == 24 || cu2 == 25)) {
            if (-4 != d(new String[]{strArr[0], strArr[2], strArr[1]})) {
                return true;
            }
            g Jn = g.Jn();
            Jn.getClass();
            Jn.iu(0);
            h nY = e.nR().nY();
            Jn.getClass();
            nY.w(h.afl, String.valueOf(0));
            nY.pp();
            Toast.makeText(this.bk, (int) R.string.f1239download_app_changed_note, 1).show();
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void g(final Context context) {
        final WebsiteSearchSchDialog websiteSearchSchDialog = new WebsiteSearchSchDialog(context);
        websiteSearchSchDialog.bP();
        websiteSearchSchDialog.a(new WebsiteSearchSchDialog.SearchBarListener() {
            public void E(String str) {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(122);
                    ModelBrowser.gD().a(39, 2, str);
                    ModelBrowser.gD().a(123, 1, (Object) null);
                }
                if (!(context instanceof Activity) || (context instanceof ActivityBrowser)) {
                    websiteSearchSchDialog.dismiss();
                    return;
                }
                ((Activity) context).setResult(-1);
                ((Activity) context).finish();
            }

            public void dp() {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(123, 0, (Object) null);
                }
            }

            public void onCancel() {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().aS(122);
                }
                websiteSearchSchDialog.dismiss();
            }
        });
        websiteSearchSchDialog.fv("");
    }

    private void gA() {
        if (this.Ao != null && this.vM != null) {
            this.vM.removeView(this.Ao);
            this.Ao = null;
        }
    }

    public static ModelBrowser gD() {
        return vz;
    }

    public static synchronized ModelBrowser gE() {
        ModelBrowser modelBrowser;
        synchronized (ModelBrowser.class) {
            if (vz == null) {
                vz = new ModelBrowser();
            }
            modelBrowser = vz;
        }
        return modelBrowser;
    }

    public static void gF() {
        if (vz != null) {
            vz = null;
        }
    }

    private boolean gI() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.bk.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.getType() == 1;
    }

    private void gO() {
        this.xj = -1;
        this.xk = -1;
        SharedPreferences sharedPreferences = this.bk.getSharedPreferences(xg, 0);
        if (!sharedPreferences.contains(xh)) {
            sharedPreferences.edit().putInt(xh, 1).commit();
        } else {
            this.xj = sharedPreferences.getInt(xh, 0);
            sharedPreferences.edit().putInt(xh, this.xj + 1).commit();
        }
        if (!sharedPreferences.contains(xi)) {
            sharedPreferences.edit().putInt(xi, 1).commit();
            return;
        }
        this.xk = sharedPreferences.getInt(xi, 0);
        sharedPreferences.edit().putInt(xi, this.xk + 1).commit();
    }

    private void gP() {
        this.vG = new WebsiteSearchSchDialog(this.bk);
        this.vG.a(this.vF.wW());
    }

    private void gQ() {
        this.vH = new WebsiteSearchWebDialog(this.bk);
        this.vH.a(this.vF.wX());
    }

    private void gS() {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this.bk);
        builder.aH(R.string.f1197dialog_title_popup_anim_off);
        builder.aG(R.string.f1198dialog_msg_popup_anim_off);
        builder.a((int) R.string.f1199dialog_button_popup_anim_off, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ModelBrowser.this.aN(2);
            }
        });
        builder.c((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ModelBrowser.this.aN(0);
            }
        });
        builder.fh().show();
    }

    /* access modifiers changed from: private */
    public void gX() {
        if (this.Av.getParent() != null) {
            this.Av.setVisibility(8);
        }
    }

    private void go() {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this.bk, true);
        builder.L("boost cache setting");
        LinearLayout linearLayout = new LinearLayout(this.bk);
        final EditText editText = new EditText(this.bk);
        editText.setText("" + m.oG);
        TextView textView = new TextView(this.bk);
        textView.setText("val mus be >0&&<=1");
        linearLayout.addView(textView);
        Button button = new Button(this.bk);
        button.setText("BOOM!");
        linearLayout.addView(editText);
        linearLayout.addView(button);
        builder.c(linearLayout);
        final UCAlertDialog fh = builder.fh();
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String obj = editText.getText().toString();
                if (obj != null) {
                    float floatValue = Float.valueOf(obj).floatValue();
                    if (floatValue > 0.0f && floatValue <= 1.0f) {
                        m.oG = floatValue;
                        Toast.makeText(ModelBrowser.this.bk, "Success!", 0).show();
                    }
                }
                fh.dismiss();
            }
        });
        if (fh != null) {
            fh.show();
        }
    }

    /* access modifiers changed from: private */
    public void gp() {
        cb.MA().kS();
        a(11, w.OW);
        if (this.bk != null) {
            Toast.makeText(this.bk, this.bk.getResources().getString(R.string.f1542uc_traffic_clear_data), 0).show();
        }
    }

    /* access modifiers changed from: private */
    public void gq() {
        if (this.bi != null && (this.bi instanceof WebViewJUC)) {
            ((WebViewJUC) this.bi).Qw();
        }
    }

    /* access modifiers changed from: private */
    public void gr() {
        (this.bi instanceof ViewMainpage ? ((ViewMainpage) this.bi).uR() : this.vQ).kK();
    }

    /* access modifiers changed from: private */
    public void gw() {
        if (this.Am != null) {
            this.Am.dismiss();
            this.Am = null;
        }
    }

    private void gy() {
        this.Ao = new TextView(this.bk);
        com.uc.h.e Pb = com.uc.h.e.Pb();
        this.Ao.setTextSize((float) Pb.kp(R.dimen.f169fullscreen_upload_info_textsize));
        this.Ao.setTextColor(-1);
        this.Ao.setBackgroundDrawable(Pb.getDrawable(UCR.drawable.aWA));
        this.Ao.setPadding(2, 0, 3, 0);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(3, R.id.f698fullscreen_progress);
        this.Ao.setLayoutParams(layoutParams);
        if (this.vM != null) {
            this.vM.addView(this.Ao);
        }
    }

    private void gz() {
        if (this.Ao.getParent() == null && this.vM != null) {
            this.vM.addView(this.Ao);
        }
        if (this.vM != null && this.vM != this.Ao.getParent()) {
            ((ViewGroup) this.Ao.getParent()).removeView(this.Ao);
            this.vM.addView(this.Ao);
        }
    }

    /* access modifiers changed from: private */
    public Dialog h(Context context) {
        ScreenNoticeDialog screenNoticeDialog = new ScreenNoticeDialog(context);
        screenNoticeDialog.show();
        this.AK = false;
        screenNoticeDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                ModelBrowser.this.hG();
            }
        });
        return screenNoticeDialog;
    }

    /* access modifiers changed from: private */
    public void hG() {
        if (this.AL != null) {
            this.AL.show();
        }
        this.AL = null;
    }

    /* access modifiers changed from: private */
    public void hH() {
        boolean z;
        int width = this.bk.getWindowManager().getDefaultDisplay().getWidth();
        int height = this.bk.getWindowManager().getDefaultDisplay().getHeight();
        try {
            z = a.Pk() >= ((double) AM);
        } catch (Throwable th) {
            z = false;
        }
        if (z) {
            if (this.xj <= 0) {
                b(this.bk, 3).show();
                this.AK = true;
            }
        } else if (e.nT().equals(b.ace) && width * height <= 96000) {
            b(this.bk, 0).show();
            this.AK = true;
        } else if (e.nT().equals("140") && width * height >= 126800) {
            b(this.bk, 1).show();
            this.AK = true;
        } else if (this.xj <= 0) {
            i((Context) this.bk).show();
            this.AK = true;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00aa A[Catch:{ Exception -> 0x010a }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b3 A[Catch:{ Exception -> 0x010a }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00dd A[Catch:{ Exception -> 0x010a }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x010e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.net.Uri hJ() {
        /*
            r10 = this;
            r6 = 0
            r5 = 1
            java.lang.String r9 = "shareImage.jpg"
            java.lang.String r8 = "shareImage."
            java.lang.String r7 = "/Images/"
            java.lang.String r0 = "shareImage.jpg"
            r10.AS = r5
            android.view.View r0 = r10.bi
            com.uc.browser.WebViewJUC r0 = (com.uc.browser.WebViewJUC) r0
            if (r0 == 0) goto L_0x001c
            android.view.View r1 = r10.bi
            java.lang.Class r1 = r1.getClass()
            java.lang.Class<com.uc.browser.WebViewJUC> r2 = com.uc.browser.WebViewJUC.class
            if (r1 == r2) goto L_0x001e
        L_0x001c:
            r0 = r6
        L_0x001d:
            return r0
        L_0x001e:
            java.lang.String r1 = r0.F()
            if (r1 == 0) goto L_0x0107
            r2 = 46
            int r2 = r1.lastIndexOf(r2)
            r3 = 47
            int r3 = r1.lastIndexOf(r3)
            if (r3 <= 0) goto L_0x0107
            if (r2 <= r3) goto L_0x0107
            int r3 = r1.length()
            if (r2 >= r3) goto L_0x0107
            java.lang.String r3 = new java.lang.String
            int r2 = r2 + 1
            int r4 = r1.length()
            java.lang.String r1 = r1.substring(r2, r4)
            r3.<init>(r1)
            java.lang.String r1 = "jpg"
            boolean r1 = r1.equalsIgnoreCase(r3)
            if (r1 != 0) goto L_0x0069
            java.lang.String r1 = "jpeg"
            boolean r1 = r1.equalsIgnoreCase(r3)
            if (r1 != 0) goto L_0x0069
            java.lang.String r1 = "jpe"
            boolean r1 = r1.equalsIgnoreCase(r3)
            if (r1 != 0) goto L_0x0069
            java.lang.String r1 = "jfif"
            boolean r1 = r1.equalsIgnoreCase(r3)
            if (r1 == 0) goto L_0x00e3
        L_0x0069:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "shareImage."
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            r10.AS = r5
        L_0x007e:
            com.uc.a.e r2 = com.uc.a.e.nR()
            com.uc.a.h r2 = r2.nY()
            java.lang.String r3 = "uc_pref_download_path"
            java.lang.String r2 = r2.bw(r3)
            java.io.File r3 = new java.io.File
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.StringBuilder r4 = r4.append(r2)
            java.lang.String r5 = "/Images/"
            java.lang.StringBuilder r4 = r4.append(r7)
            java.lang.String r4 = r4.toString()
            r3.<init>(r4)
            boolean r4 = r3.exists()     // Catch:{ Exception -> 0x010a }
            if (r4 != 0) goto L_0x00ad
            r3.mkdirs()     // Catch:{ Exception -> 0x010a }
        L_0x00ad:
            boolean r4 = r3.isDirectory()     // Catch:{ Exception -> 0x010a }
            if (r4 != 0) goto L_0x00b9
            r3.delete()     // Catch:{ Exception -> 0x010a }
            r3.mkdirs()     // Catch:{ Exception -> 0x010a }
        L_0x00b9:
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x010a }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x010a }
            r4.<init>()     // Catch:{ Exception -> 0x010a }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ Exception -> 0x010a }
            java.lang.String r4 = "/Images/"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x010a }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ Exception -> 0x010a }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x010a }
            r3.<init>(r1)     // Catch:{ Exception -> 0x010a }
            byte r1 = r10.AS     // Catch:{ Exception -> 0x010a }
            boolean r0 = r0.a(r3, r1)     // Catch:{ Exception -> 0x010a }
            if (r0 == 0) goto L_0x010e
            android.net.Uri r0 = android.net.Uri.fromFile(r3)     // Catch:{ Exception -> 0x010a }
            goto L_0x001d
        L_0x00e3:
            java.lang.String r1 = "png"
            boolean r1 = r1.equalsIgnoreCase(r3)
            if (r1 == 0) goto L_0x0103
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "shareImage."
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            r2 = 0
            r10.AS = r2
            goto L_0x007e
        L_0x0103:
            java.lang.String r1 = "shareImage.jpg"
            r10.AS = r5
        L_0x0107:
            r1 = r9
            goto L_0x007e
        L_0x010a:
            r0 = move-exception
            r0 = r6
            goto L_0x001d
        L_0x010e:
            r0 = r6
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.browser.ModelBrowser.hJ():android.net.Uri");
    }

    /* access modifiers changed from: private */
    public void hO() {
        if (this.vJ != null) {
            this.vJ.eO(this.vJ.wj());
            ViewMainBar.kI();
            if (this.vU != null) {
                this.vU.QR();
            }
            k(this.vJ.wi().getUrl(), "closeCurrentWindow()");
        }
    }

    private void hc() {
    }

    private String hh() {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ((ActivityManager) this.bk.getSystemService("activity")).getMemoryInfo(memoryInfo);
        return "Memory Info: available=" + (memoryInfo.availMem / cb.bPw) + "K threshold=" + (memoryInfo.threshold / cb.bPw) + cb.bPz;
    }

    /* access modifiers changed from: private */
    public void hi() {
        ViewMainBar.R(this.vK.bk());
        ViewMainBar.Q(canGoForward());
        if (this.vU != null) {
            this.vU.d(canGoBack(), bo());
            this.vU.Q(canGoForward());
        }
    }

    /* access modifiers changed from: private */
    public void hn() {
        if (this.vK != null) {
            boolean bk2 = this.vK.bk();
            hi();
            ViewMainBar.R(bk2);
            if (!bk2) {
                aP(100);
            }
            if (this.vU != null) {
                this.vU.cW(bk2);
            }
            boolean a2 = this.vK.a(this.vK.getCurrentView());
            ViewMainBar.P(a2);
            if (this.vU != null) {
                this.vU.P(a2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void ho() {
        if (this.vK != null) {
            if (this.vK.bk()) {
                if (true == V()) {
                    W();
                }
                this.vK.stopLoading();
            } else if (this.wA) {
                this.wA = false;
                if (true == V()) {
                    W();
                }
                e.nR().stopLoading();
            } else if (!bo()) {
                a((Integer) -1);
                b.a.a.f.l(0, b.a.a.f.asU);
            } else {
                he();
            }
        }
    }

    /* access modifiers changed from: private */
    public void hp() {
        if (this.vK != null) {
            if (this.vK.bk()) {
                if (true == V()) {
                    W();
                }
                this.vK.stopLoading();
            } else if (this.wA) {
                this.wA = false;
                if (true == V()) {
                    W();
                }
                e.nR().stopLoading();
            }
            if (!bo()) {
                a((Integer) -1);
                b.a.a.f.l(0, b.a.a.f.asU);
                return;
            }
            he();
        }
    }

    /* access modifiers changed from: private */
    public void hq() {
        if (this.vK != null) {
            if (!this.vK.bk()) {
                this.vK.refresh();
            } else {
                this.vK.stopLoading();
            }
        }
    }

    /* access modifiers changed from: private */
    public void hu() {
        if (this.vK != null) {
            View inflate = LayoutInflater.from(this.bk).inflate((int) R.layout.f886browser_dialog_pageattrs, (ViewGroup) null);
            Button button = (Button) inflate.findViewById(R.id.f707pageattr_button_copy_address);
            Button button2 = (Button) inflate.findViewById(R.id.f709pageattr_button_copy_url);
            Button button3 = (Button) inflate.findViewById(R.id.f712pageattr_button_copy_image_url);
            String title = this.vK.getTitle();
            final String url = this.vK.getUrl();
            final String bi2 = this.vK.bi();
            if (bi2 == null) {
                bi2 = this.AN;
                this.AN = null;
            }
            final String F = this.vK.F();
            String str = title == null ? "" : title;
            if (url == null || url.length() == 0 || url.equals("ext:waiting")) {
                button.setVisibility(4);
            }
            if (bi2 == null || bi2.length() == 0) {
                button2.setVisibility(4);
            }
            if (F == null || F.length() == 0) {
                inflate.findViewById(R.id.f711pageattr_image_url).setVisibility(8);
                ((TextView) inflate.findViewById(R.id.f713browser_page_attrs_image_url)).setVisibility(8);
                button3.setVisibility(8);
            }
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    ModelBrowser.this.aa(url);
                }
            });
            button2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    ModelBrowser.this.aa(bi2);
                }
            });
            button3.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    ModelBrowser.this.aa(F);
                }
            });
            ((TextView) inflate.findViewById(R.id.f706browser_page_attrs_title)).setText(str);
            ((TextView) inflate.findViewById(R.id.f708browser_page_attrs_address)).setText(url);
            ((TextView) inflate.findViewById(R.id.f710browser_page_attrs_url)).setText(bi2);
            ((TextView) inflate.findViewById(R.id.f713browser_page_attrs_image_url)).setText(F);
            new UCAlertDialog.Builder(this.bk).aH(R.string.f1354browser_dialog_page_attrs).c(inflate).a((int) R.string.cancel, (DialogInterface.OnClickListener) null).fh().show();
        }
    }

    /* access modifiers changed from: private */
    public Dialog i(final Context context) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.L(context.getString(R.string.f1139dialog_title_createshortcut));
        builder.M(context.getString(R.string.f1174dialog_msg_createshortcut));
        builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ModelBrowser.this.hI();
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        UCAlertDialog fh = builder.fh();
        fh.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                if (ModelBrowser.this.bi != null && (ModelBrowser.this.bi instanceof ViewMainpage)) {
                    Dialog unused = ModelBrowser.this.h(context);
                }
            }
        });
        return fh;
    }

    /* access modifiers changed from: private */
    public void i(Object obj) {
        this.Am = new ProgressDialog(this.bk);
        this.Am.setMessage(this.bk.getString(R.string.f1451scan_contact_tip));
        this.Am.setOnCancelListener((DialogInterface.OnCancelListener) obj);
        this.Am.show();
    }

    /* access modifiers changed from: private */
    public void it() {
        this.bk.startActivity(new Intent(this.bk, ActivityDownload.class));
    }

    /* access modifiers changed from: private */
    public void iu() {
        if (this.bi instanceof WebViewZoom) {
            Toast.makeText(this.bk, (int) R.string.f1517save_source_webkit_info, 0).show();
        } else if (!(this.bi instanceof WebViewJUC)) {
        } else {
            if (!((WebViewJUC) this.bi).at()) {
                Toast.makeText(this.bk, (int) R.string.f1518save_source_loading_info, 0).show();
            } else {
                iv().show();
            }
        }
    }

    private Dialog iv() {
        View inflate = LayoutInflater.from(this.bk).inflate((int) R.layout.f935save_source_dialog, (ViewGroup) null);
        final UCAlertDialog fh = new UCAlertDialog.Builder(this.bk).aH(R.string.f1516save_source).c(inflate).fh();
        String id = id();
        String str = (id == null || id.length() == 0) ? "未命名" : id;
        final EditText editText = (EditText) inflate.findViewById(R.id.f841save_source_name);
        editText.setText(str);
        ((TextView) inflate.findViewById(R.id.f683file_name)).setTextColor(com.uc.h.e.Pb().getColor(78));
        View findViewById = inflate.findViewById(R.id.f843save_source_ok);
        View findViewById2 = inflate.findViewById(R.id.f844save_source_cancel);
        AnonymousClass62 r4 = new View.OnClickListener() {
            public void onClick(View view) {
                WebViewJUC webViewJUC;
                switch (view.getId()) {
                    case R.id.f843save_source_ok /*2131099851*/:
                        String obj = editText.getText().toString();
                        int length = obj.trim().getBytes().length;
                        String bw = e.nR().nY().bw(h.afm);
                        if (obj == null || length == 0) {
                            Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1511save_source_filename_empty, 0).show();
                            return;
                        } else if (!e.nR().ae(obj)) {
                            Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1513save_source_file_name_error, 0).show();
                            return;
                        } else if (255 < length) {
                            Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1514save_source_file_name_toolong, 0).show();
                            return;
                        } else {
                            File file = new File(bw + "/Pages/");
                            try {
                                if (!file.exists()) {
                                    file.mkdirs();
                                }
                                if (!file.isDirectory()) {
                                    file.delete();
                                    file.mkdirs();
                                }
                                File file2 = new File(bw + "/Pages/" + obj + ".uhtml");
                                if (file2.exists()) {
                                    Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1512save_source_file_exist, 0).show();
                                    return;
                                } else if (!file2.createNewFile()) {
                                    throw new Exception("create file fail");
                                } else {
                                    if (!(ModelBrowser.this.bi == null || ModelBrowser.this.bi.getClass() != WebViewJUC.class || (webViewJUC = (WebViewJUC) ModelBrowser.this.bi) == null)) {
                                        webViewJUC.a(file2);
                                        Toast.makeText(ModelBrowser.this.bk, ModelBrowser.this.bk.getResources().getString(R.string.f1519save_source_to) + file2.getAbsolutePath(), 0).show();
                                    }
                                    fh.dismiss();
                                    return;
                                }
                            } catch (Exception e) {
                                Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1510save_source_sdcard_error, 0).show();
                                fh.dismiss();
                                return;
                            }
                        }
                    case R.id.f844save_source_cancel /*2131099852*/:
                        fh.dismiss();
                        return;
                    default:
                        return;
                }
            }
        };
        findViewById.setOnClickListener(r4);
        findViewById2.setOnClickListener(r4);
        return fh;
    }

    private Dialog iw() {
        String F;
        View inflate = LayoutInflater.from(this.bk).inflate((int) R.layout.f934save_image_dialog, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.f683file_name)).setTextColor(com.uc.h.e.Pb().getColor(78));
        final UCAlertDialog fh = new UCAlertDialog.Builder(this.bk).aH(R.string.f1509save_image_dialog_title).c(inflate).fh();
        String str = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg";
        this.AS = 1;
        WebViewJUC webViewJUC = (WebViewJUC) this.bi;
        if (!(webViewJUC == null || (F = webViewJUC.F()) == null)) {
            int lastIndexOf = F.lastIndexOf(46);
            int lastIndexOf2 = F.lastIndexOf(47);
            if (lastIndexOf2 > 0 && lastIndexOf > lastIndexOf2 && lastIndexOf < F.length()) {
                String str2 = new String(F.substring(lastIndexOf + 1, F.length()));
                String str3 = new String(F.substring(lastIndexOf2 + 1, lastIndexOf));
                if ("jpg".equalsIgnoreCase(str2) || "jpeg".equalsIgnoreCase(str2) || "jpe".equalsIgnoreCase(str2) || "jfif".equalsIgnoreCase(str2)) {
                    this.AS = 1;
                    str = str3 + '.' + str2;
                } else if ("png".equalsIgnoreCase(str2)) {
                    this.AS = 0;
                    str = str3 + '.' + str2;
                } else {
                    this.AS = 1;
                    str = str3 + ".jpg";
                }
            }
        }
        final EditText editText = (EditText) inflate.findViewById(R.id.f837save_image_name);
        editText.setText(str);
        int lastIndexOf3 = str.lastIndexOf(46);
        if (lastIndexOf3 > 0) {
            editText.setSelection(0, lastIndexOf3);
        } else {
            editText.setSelection(0, str.length());
        }
        View findViewById = inflate.findViewById(R.id.f839save_image_ok);
        View findViewById2 = inflate.findViewById(R.id.f840save_image_cancel);
        AnonymousClass63 r4 = new View.OnClickListener() {
            public void onClick(View view) {
                WebViewJUC webViewJUC;
                switch (view.getId()) {
                    case R.id.f839save_image_ok /*2131099847*/:
                        String obj = editText.getText().toString();
                        int length = obj.trim().getBytes().length;
                        String bw = e.nR().nY().bw(h.afm);
                        if (length == 0) {
                            Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1502save_image_filename_empty, 0).show();
                            return;
                        } else if (!e.nR().ae(obj)) {
                            Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1504save_image_file_name_error, 0).show();
                            return;
                        } else if (255 < length) {
                            Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1505save_image_file_name_toolong, 0).show();
                            return;
                        } else {
                            File file = new File(bw + w.NC);
                            try {
                                if (!file.exists()) {
                                    file.mkdirs();
                                }
                                if (!file.isDirectory()) {
                                    file.delete();
                                    file.mkdirs();
                                }
                                File file2 = new File(bw + w.NC + obj);
                                if (file2.exists()) {
                                    Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1503save_image_file_exist, 0).show();
                                    return;
                                } else if (!file2.createNewFile()) {
                                    throw new Exception("create file fail");
                                } else {
                                    if (!(ModelBrowser.this.bi == null || ModelBrowser.this.bi.getClass() != WebViewJUC.class || (webViewJUC = (WebViewJUC) ModelBrowser.this.bi) == null)) {
                                        if (webViewJUC.a(file2, ModelBrowser.this.AS)) {
                                            Toast.makeText(ModelBrowser.this.bk, ModelBrowser.this.bk.getString(R.string.f1507save_image_to) + file2.getAbsolutePath(), 0).show();
                                        } else {
                                            Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1508save_image_error, 0).show();
                                        }
                                    }
                                    fh.dismiss();
                                    return;
                                }
                            } catch (Exception e) {
                                Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1501save_image_sdcard_error, 0).show();
                                fh.dismiss();
                                return;
                            }
                        }
                    case R.id.f840save_image_cancel /*2131099848*/:
                        fh.dismiss();
                        return;
                    default:
                        return;
                }
            }
        };
        findViewById.setOnClickListener(r4);
        findViewById2.setOnClickListener(r4);
        return fh;
    }

    private Dialog j(Context context) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        View inflate = LayoutInflater.from(this.bk).inflate((int) R.layout.f887browser_dialog_tipszoommode, (ViewGroup) null);
        try {
            if (ActivityBrowser.Fz()) {
                ((TextView) inflate.findViewById(R.id.f716img_fit)).getCompoundDrawables()[3].setAlpha(UCR.Color.bTn);
                ((TextView) inflate.findViewById(R.id.f715img_zoom)).getCompoundDrawables()[3].setAlpha(UCR.Color.bTn);
            } else {
                ((TextView) inflate.findViewById(R.id.f716img_fit)).getCompoundDrawables()[3].setAlpha(255);
                ((TextView) inflate.findViewById(R.id.f715img_zoom)).getCompoundDrawables()[3].setAlpha(255);
            }
        } catch (Exception e) {
        }
        builder.c(inflate);
        final UCAlertDialog fh = builder.fh();
        inflate.findViewById(R.id.f718tips_close).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                fh.cancel();
            }
        });
        inflate.findViewById(R.id.f719tips_over).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                e.nR().nY().w("should_show_zoom_mode_tips", l.FALSE);
                fh.cancel();
            }
        });
        return fh;
    }

    private String[] j(String str, String str2) {
        String n = WindowUCWeb.n(str);
        String lowerCase = n.toLowerCase();
        String[] strArr = {n.substring(lowerCase.indexOf(str2), n.length()), "", ""};
        if (this.bi != null && n.contains(b.acH)) {
            if (this.bi instanceof WebViewJUC) {
                strArr[1] = e.nR().getCookie(n.substring(lowerCase.indexOf(str2), n.length()));
                CookieSyncManager.createInstance(this.bk.getApplicationContext());
                strArr[1] = CookieManager.getInstance().getCookie(n.substring(lowerCase.indexOf(str2), n.length()));
            }
            if (this.vK != null) {
                strArr[2] = this.vK.getUrl();
            }
        }
        return strArr;
    }

    private Dialog k(Context context) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.aH(R.string.f1140dialog_title_novelmode);
        boolean z = e.nR().nY().bw(h.afq).trim().equals(e.RE);
        ScrollView scrollView = new ScrollView(context);
        TextView textView = new TextView(context);
        textView.setTextColor(com.uc.h.e.Pb().getColor(78));
        textView.setTextSize(18.0f);
        if (z) {
            textView.setText((int) R.string.f1178dialog_msg_novelmode_zoom);
        } else {
            textView.setText((int) R.string.f1177dialog_msg_novelmode);
        }
        scrollView.addView(textView);
        builder.c(scrollView);
        builder.a((int) R.string.f1126alert_dialog_close, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.c((int) R.string.f1127alert_dialog_over, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                e.nR().nY().w("should_show_novel_mode_tips", "0");
                dialogInterface.cancel();
            }
        });
        return builder.fh();
    }

    /* access modifiers changed from: private */
    public void k(Object obj) {
        if (!an.vA().vC()) {
            int i = 0;
            if (obj != null) {
                i = ((Integer) obj).intValue();
            }
            if (this.bi == null) {
                return;
            }
            if (this.bi instanceof WebViewJUC) {
                ((WebViewJUC) this.bi).d(i);
            } else if (this.bi instanceof WebViewZoom) {
                an.vA().vV();
            }
        }
    }

    private Dialog l(Context context) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.aH(R.string.f1179dialog_title_pageupdown);
        builder.aG(R.string.f1180dialog_msg_pageupdown_zoom);
        builder.a((int) R.string.f1126alert_dialog_close, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.c((int) R.string.f1127alert_dialog_over, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                e.nR().nY().w("should_show_page_up_down_mode_tips", "0");
                dialogInterface.cancel();
            }
        });
        return builder.fh();
    }

    private Dialog m(final Context context) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.aH(R.string.f1181dialog_title_nightmode);
        builder.aG(R.string.f1182dialog_msg_nightmode);
        builder.a((int) R.string.f1126alert_dialog_close, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.b((int) R.string.f1128alert_dialog_change_brightness, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
                ModelBrowser.this.n(context).show();
            }
        });
        builder.c((int) R.string.f1127alert_dialog_over, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                e.nR().nY().w("should_show_nightmode_tips", "0");
                dialogInterface.cancel();
            }
        });
        return builder.fh();
    }

    /* access modifiers changed from: private */
    public void m(String str, String str2) {
        Intent intent = new Intent(ActivityUpdate.IA);
        intent.setType("text/plain");
        intent.putExtra(ActivityUpdate.II, str);
        intent.putExtra(ActivityUpdate.IJ, str2);
        this.bk.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public Dialog n(final Context context) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.aH(R.string.f1183dialog_title_nightmode_brightness);
        View inflate = LayoutInflater.from(context).inflate((int) R.layout.f927pref_seekbar, (ViewGroup) null);
        final SeekBar seekBar = (SeekBar) inflate.findViewById(R.id.f819seekbar);
        builder.c(inflate);
        seekBar.setProgress(Integer.parseInt(e.nR().nY().bw(h.afv)) - 25);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                ActivityBrowser.a((Activity) context, i + 25);
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                e.nR().nY().w(h.afv, "" + (seekBar.getProgress() + 25));
                dialogInterface.cancel();
            }
        });
        return builder.fh();
    }

    /* access modifiers changed from: private */
    public void refresh() {
        if (this.vK != null) {
            this.vK.refresh();
            if (this.wm != null) {
                this.wm.setVisibility(4);
                if (this.vK != null) {
                    this.vK.j(false);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public final void w(boolean z) {
        e.nR().w(z);
    }

    public void A(boolean z) {
        if (this.bi == null) {
            return;
        }
        if (z) {
            f(this.bi);
        } else if (this.wk != null) {
            this.wk.setVisibility(8);
        }
    }

    public void B(boolean z) {
        hi();
        ViewMainBar.R(z);
        if (!z) {
            aP(100);
        }
        if (this.vU != null) {
            this.vU.cW(z);
        }
    }

    public void C(boolean z) {
    }

    public void D(boolean z) {
        WebViewZoom bh;
        if (this.vK != null && (bh = this.vK.bh()) != null) {
            bh.setNetworkAvailable(z);
        }
    }

    public void E(boolean z) {
        Intent intent = new Intent();
        intent.setAction(ActivityBookmarkEx.asd);
        intent.setClass(this.bk, ActivityBookmarkEx.class);
        if (this.vK != null && !z) {
            intent.putExtra(ActivityBookmarkEx.asg, this.vK.getUrl());
            intent.putExtra(ActivityBookmarkEx.asf, this.vK.getTitle());
            intent.putExtra(ActivityBookmarkEx.asi, this.vK.p());
        }
        this.bk.startActivity(intent);
    }

    public void F(boolean z) {
        boolean iA = iA();
        boolean iB = iB();
        if (iA && iB) {
            G(true);
        } else if (iB) {
            boolean iE = iE();
            G(iE);
            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.bk);
            int i = defaultSharedPreferences.getInt(Bb, 0);
            if (!iE && i < 3 && z) {
                defaultSharedPreferences.edit().putInt(Bb, i + 1).commit();
            }
        } else {
            iC();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.c(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.uc.browser.ModelBrowser.c(com.uc.browser.ModelBrowser, android.content.Context):android.app.Dialog
      com.uc.browser.ModelBrowser.c(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.c(com.uc.browser.ModelBrowser, boolean):boolean
      com.uc.browser.ModelBrowser.c(boolean, boolean):void */
    public void G(boolean z) {
        c(z, true);
    }

    public void P() {
        WebViewJUC webViewJUC;
        if (this.bi != null && this.bi.getClass() == ViewMainpage.class) {
            ViewMainpage viewMainpage = (ViewMainpage) this.bi;
            if (viewMainpage != null) {
                viewMainpage.P();
            }
        } else if (this.bi != null && this.bi.getClass() == WebViewJUC.class && (webViewJUC = (WebViewJUC) this.bi) != null) {
            webViewJUC.P();
        }
    }

    public boolean V() {
        if (this.bi == null || this.bi.getClass() != WebViewJUC.class) {
            return false;
        }
        return ((WebViewJUC) this.bi).V();
    }

    public void W() {
        if (this.bi != null && this.bi.getClass() == WebViewJUC.class) {
            ((WebViewJUC) this.bi).W();
            if (this.wm != null) {
                this.wm.setVisibility(4);
                if (this.vK != null) {
                    this.vK.j(false);
                }
            }
        }
    }

    public void W(String str) {
        if (this.vG == null) {
            gP();
        }
        this.vG.fv(str);
    }

    /* access modifiers changed from: package-private */
    public View X(String str) {
        String str2;
        i((byte) 0);
        boolean contains = e.nR().bw(h.afg).contains(e.RD);
        if (this.Ar && contains && this.At) {
            Toast.makeText(this.bk, (int) R.string.f995toast_wifi_optimist, 0).show();
            this.At = false;
        }
        this.vI = 0;
        if (str == null || this.vK == null) {
            return this.bi;
        }
        if (str.contains("ext:wo:")) {
            a(39, 2, str.replace("ext:wo:", ""));
            return this.bi;
        }
        if (str == null || !str.endsWith(b.acu)) {
            str2 = str;
        } else {
            this.vK.bC = true;
            str2 = str.substring(0, str.length() - b.acu.length());
        }
        this.vR = WindowUCWeb.n(str2);
        k(this.vR, "loadURL");
        View a2 = a(this.vK, str2);
        if (!this.vK.bC) {
            return a2;
        }
        this.vK.bC = false;
        return a2;
    }

    /* access modifiers changed from: package-private */
    public String Y(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        String lowerCase = str.toLowerCase();
        if (lowerCase.contains(b.acp)) {
            return b.acp;
        }
        if (lowerCase.contains(b.acq)) {
            return b.acq;
        }
        if (lowerCase.contains(b.acr)) {
            return b.acr;
        }
        if (lowerCase.contains(b.acs)) {
            return b.acs;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void Z(String str) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.b(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, int):int
      com.uc.browser.ModelBrowser.b(android.content.Context, int):android.app.Dialog
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, android.content.Context):android.app.Dialog
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, boolean):boolean
      com.uc.browser.ModelBrowser.b(boolean, boolean):void
      com.uc.browser.ModelBrowser.b(android.view.View, boolean):void */
    /* access modifiers changed from: package-private */
    public View a(WindowUCWeb windowUCWeb, String str) {
        String str2;
        View view = null;
        if (windowUCWeb != null && this.vK == windowUCWeb) {
            view = this.bi;
        }
        if (str == null || windowUCWeb == null) {
            return view;
        }
        if (str.contains("ext:wo:")) {
            a(39, 2, str.replace("ext:wo:", ""));
            return view;
        }
        if (str.contains("ext:startpage")) {
            str2 = str.trim();
            if (str2.startsWith("ext:startpage")) {
                return view;
            }
        } else if (str.contains("ext:lp:lp_favor")) {
            aS(49);
            return view;
        } else if (str.contains(w.Pc)) {
            aS(54);
            return view;
        } else {
            str2 = str;
        }
        String Y = Y(str2);
        if (Y != null) {
            d(j(str2, Y));
            return view;
        } else if (str2.contains(wT)) {
            he();
            return view;
        } else if (this.vK != windowUCWeb) {
            View a2 = windowUCWeb.a(str2, this.bk);
            P();
            return a2;
        } else if (this.vK == null) {
            return view;
        } else {
            View a3 = this.vK.a(str2, this.bk);
            b(a3, false);
            return a3;
        }
    }

    public void a(int i, int i2, int i3, Object obj) {
        this.mHandler.sendMessage(Message.obtain(null, i, i2, i3, obj));
    }

    /* access modifiers changed from: package-private */
    public void a(int i, int i2, long j) {
        this.mHandler.sendMessageDelayed(Message.obtain(null, i, i2, 0), j);
    }

    public void a(int i, int i2, Object obj) {
        this.mHandler.sendMessage(Message.obtain(null, i, i2, 0, obj));
    }

    /* access modifiers changed from: package-private */
    public void a(int i, long j) {
        this.mHandler.sendMessageDelayed(Message.obtain((Handler) null, i), j);
    }

    public void a(int i, Object obj) {
        this.mHandler.sendMessage(Message.obtain(null, i, obj));
    }

    public void a(final int i, final String str, boolean z) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this.bk);
        this.wD = 1;
        builder.aH(R.string.f1190dialog_title_upload);
        builder.a(z ? R.array.f56mdisk_upload : R.array.f55camera_upload, this.wD, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                int unused = ModelBrowser.this.wD = i;
            }
        });
        builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            long Sg;

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(DialogInterface dialogInterface, int i) {
                if (System.currentTimeMillis() - this.Sg >= 500) {
                    this.Sg = System.currentTimeMillis();
                    if (1 == ModelBrowser.this.wD) {
                        Intent intent = new Intent(ModelBrowser.this.bk.getBaseContext(), ActivityCamera.class);
                        intent.putExtra("file_maxlength", i);
                        ModelBrowser.this.bk.startActivityForResult(intent, 3);
                    } else if (2 == ModelBrowser.this.wD) {
                        e.nR().qN();
                    } else {
                        Intent intent2 = new Intent(ModelBrowser.this.bk, ActivityChooseFile.class);
                        intent2.putExtra(ActivityChooseFile.uv, 1);
                        intent2.putExtra("file_maxlength", i);
                        intent2.putExtra(ActivityChooseFile.ux, str);
                        intent2.putExtra(ActivityChooseFile.uy, false);
                        ModelBrowser.this.bk.startActivityForResult(intent2, 2);
                    }
                }
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                WebViewJUC ia = ModelBrowser.this.ia();
                if (ia != null) {
                    ia.ap();
                }
            }
        });
        builder.fh().show();
    }

    public void a(Activity activity) {
        this.vB = activity;
    }

    /* access modifiers changed from: protected */
    public void a(c cVar) {
        this.AH = 2;
        String string = this.bk.getString(R.string.f1428fileupload);
        String str = cVar.btW;
        int lastIndexOf = str.lastIndexOf(47);
        this.An = (string + "   " + str.substring(lastIndexOf < 0 ? 0 : lastIndexOf + 1, str.length())) + (" (" + e.j(cVar.btV) + au.aGF + e.j(cVar.btU) + ")");
        int i = ((int) ((cVar.btV * 80) / cVar.btU)) + 20;
        if (i <= 0 || i >= 100) {
            if (i >= 100) {
                if (this.wb.getVisibility() == 0) {
                    this.wb.setVisibility(8);
                }
                if (this.wc != null && this.wc.getVisibility() == 0) {
                    this.wc.setVisibility(8);
                }
            }
        } else if (this.wb.getVisibility() != 0) {
            this.wb.setVisibility(0);
        }
        this.wb.setProgress(i);
        if (this.wc != null) {
            this.wc.setProgress(i);
        }
        if ((this.vU != null) && (!e.nR().nY().bw(h.aeZ).contains(e.RD))) {
            if (this.Ao == null) {
                gy();
            }
            gz();
            this.Ao.setText(this.An);
            this.Ao.bringToFront();
        } else {
            t(this.An);
        }
        if (cVar.btV == cVar.btU) {
            this.AH = 0;
            gA();
            t(this.vK.getTitle());
        }
    }

    public void a(final DownloadDlgData downloadDlgData, int i) {
        View inflate = LayoutInflater.from(this.bk).inflate((int) R.layout.f888browser_dialog_upmsg, (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(R.id.f720browser_upmsg);
        textView.setTextColor(com.uc.h.e.Pb().getColor(7));
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this.bk);
        builder.L(downloadDlgData.aU);
        builder.c(inflate);
        if (5 == i || 7 == i) {
            int width = this.bk.getWindowManager().getDefaultDisplay().getWidth();
            if (width >= 720 || width < 260) {
                textView.setText(downloadDlgData.agH + bx.bxN);
            } else {
                textView.setText(downloadDlgData.agH);
            }
        } else {
            textView.setText(downloadDlgData.agH + this.bk.getString(R.string.f1189dialog_msg_should_download));
        }
        builder.a((int) R.string.f1266dlg_button_ok, new DialogInterface.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.uc.a.e.a(boolean, com.uc.a.n):int
             arg types: [int, com.uc.a.n]
             candidates:
              com.uc.a.e.a(java.lang.String, java.util.Hashtable):java.util.Vector
              com.uc.a.e.a(b.a.a.e, b.a.a.e):int
              com.uc.a.e.a(float, int):android.graphics.Bitmap
              com.uc.a.e.a(java.lang.String, byte):java.lang.String
              com.uc.a.e.a(int, android.graphics.Bitmap):void
              com.uc.a.e.a(int, short):void
              com.uc.a.e.a(android.graphics.Bitmap, int):void
              com.uc.a.e.a(com.uc.a.n, int):void
              com.uc.a.e.a(java.io.File, int):void
              com.uc.a.e.a(java.util.List, int):void
              com.uc.a.e.a(boolean, int):void
              com.uc.a.e.a(int, int):boolean
              com.uc.a.e.a(int, b.a.a.e):boolean
              com.uc.a.e.a(java.lang.String, b.a.a.c.e):boolean
              com.uc.a.e.a(boolean, com.uc.a.n):int */
            public void onClick(DialogInterface dialogInterface, int i) {
                if (!Environment.getExternalStorageState().equals("mounted")) {
                    Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1389tcardnotusable, 1).show();
                    return;
                }
                String[] bj = e.bj(downloadDlgData.avw);
                if (true == e.nR().nX().T(bj[3])) {
                    String V = e.nR().nX().V(bj[3]);
                    if (!V.equals(bj[3])) {
                        bj[3] = V;
                    }
                }
                if (e.RD.equals(e.nR().bw(h.afo))) {
                    e.nR().a(false, ModelBrowser.this.p);
                    e.nR().nX().a(bj);
                    return;
                }
                Intent intent = new Intent(ModelBrowser.this.bk, ActivityDownload.class);
                intent.putExtra("downloadinfo", bj);
                ModelBrowser.this.bk.startActivity(intent);
            }
        });
        builder.c((int) R.string.f1267dlg_button_cancle, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        if (this.AK) {
            this.AL = builder.fh();
        } else {
            builder.fh().show();
        }
    }

    public void a(final YesOrNoDlgData yesOrNoDlgData) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this.bk);
        builder.L(yesOrNoDlgData.aU).M(yesOrNoDlgData.agH);
        AnonymousClass38 r1 = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case -2:
                        yesOrNoDlgData.agI.aV();
                        return;
                    case -1:
                        yesOrNoDlgData.agI.aU();
                        return;
                    default:
                        return;
                }
            }
        };
        builder.a((int) R.string.f1266dlg_button_ok, r1);
        builder.c((int) R.string.f1267dlg_button_cancle, r1);
        UCAlertDialog fh = builder.fh();
        fh.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                yesOrNoDlgData.agI.aW();
            }
        });
        fh.show();
    }

    /* access modifiers changed from: protected */
    public void a(final YesOrNoDlgData yesOrNoDlgData, String str, String str2) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this.bk);
        builder.L(this.bk.getResources().getString(R.string.f970tip)).M(yesOrNoDlgData.agH);
        AnonymousClass3 r1 = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case -2:
                        yesOrNoDlgData.agI.aV();
                        return;
                    case -1:
                        yesOrNoDlgData.agI.aU();
                        return;
                    default:
                        return;
                }
            }
        };
        String string = (str == null || str.length() == 0) ? this.bk.getResources().getString(R.string.f1266dlg_button_ok) : str;
        String string2 = (str2 == null || str2.length() == 0) ? this.bk.getResources().getString(R.string.f1267dlg_button_cancle) : str2;
        builder.a(string, r1);
        builder.c(string2, r1);
        UCAlertDialog fh = builder.fh();
        fh.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                yesOrNoDlgData.agI.aW();
            }
        });
        fh.show();
    }

    public void a(final YesOrNoDlgData yesOrNoDlgData, boolean z) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this.bk);
        builder.L(yesOrNoDlgData.aU).M(yesOrNoDlgData.agH);
        AnonymousClass40 r1 = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case -3:
                        e.nR().nY().w(h.afx, e.RE);
                        yesOrNoDlgData.agI.aU();
                        return;
                    case -2:
                        yesOrNoDlgData.agI.aW();
                        return;
                    case -1:
                        yesOrNoDlgData.agI.aU();
                        return;
                    default:
                        return;
                }
            }
        };
        if (!z) {
            builder.a((int) R.string.f1268dlg_button_save, r1);
            builder.b((int) R.string.f1269dlg_button_autosave, r1);
            builder.c((int) R.string.f1267dlg_button_cancle, r1);
        } else {
            builder.a((int) R.string.f1266dlg_button_ok, r1);
            builder.c((int) R.string.f1267dlg_button_cancle, r1);
        }
        UCAlertDialog fh = builder.fh();
        fh.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                yesOrNoDlgData.agI.aW();
            }
        });
        fh.show();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.uc.browser.ViewZoomControls.ZoomController r3, int r4) {
        /*
            r2 = this;
            int r0 = b.b.acj     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            if (r0 != r4) goto L_0x0044
            com.uc.browser.ViewZoomControls r0 = r2.wJ     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            if (r0 != 0) goto L_0x000b
            r2.im()     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
        L_0x000b:
            android.widget.RelativeLayout r0 = r2.vM     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            if (r0 == 0) goto L_0x0039
            com.uc.browser.ViewZoomControls r0 = r2.wJ     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            if (r0 == 0) goto L_0x0039
            boolean r0 = r3.uf()     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            if (r0 == 0) goto L_0x003a
            com.uc.browser.ViewZoomControls r0 = r2.wJ     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            int r1 = r3.ah()     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            r0.hT(r1)     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            com.uc.browser.ViewZoomControls r0 = r2.wJ     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            r0.a(r3)     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            com.uc.browser.ViewZoomControls r0 = r2.wJ     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            r1 = 0
            r0.setVisibility(r1)     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            com.uc.browser.ViewZoomControls r0 = r2.wJ     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            r0.bringToFront()     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            android.widget.RelativeLayout r0 = r2.vM     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            com.uc.browser.ViewZoomControls r1 = r2.wJ     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            r0.addView(r1)     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
        L_0x0039:
            return
        L_0x003a:
            com.uc.browser.ViewZoomControls r0 = r2.wJ     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            r1 = 8
            r0.setVisibility(r1)     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            goto L_0x0039
        L_0x0042:
            r0 = move-exception
            goto L_0x0039
        L_0x0044:
            com.uc.browser.ViewZoomControls r0 = r2.wJ     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            if (r0 == 0) goto L_0x0039
            com.uc.browser.ViewZoomControls r0 = r2.wJ     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            r1 = 8
            r0.setVisibility(r1)     // Catch:{ Exception -> 0x0042, all -> 0x0050 }
            goto L_0x0039
        L_0x0050:
            r0 = move-exception
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void");
    }

    public void a(WebViewJUC webViewJUC, int i, int i2) {
        if (this.bi != null && webViewJUC == this.bi) {
            if (this.AY < 0) {
                this.AY = com.uc.h.e.Pb().kp(R.dimen.f463reading_mode_tail_height);
            }
            if (i <= this.AY && i2 != 4) {
                if (this.AX == null) {
                    this.AX = new ViewReadingPageTail(this.bk);
                    this.AW = new RelativeLayout.LayoutParams(-2, -2);
                    this.AW.addRule(12);
                    this.AW.addRule(14);
                    this.AX.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            if (ModelBrowser.this.bi != null && (ModelBrowser.this.bi instanceof WebViewJUC)) {
                                WebViewJUC webViewJUC = (WebViewJUC) ModelBrowser.this.bi;
                                switch (ModelBrowser.this.AX.type) {
                                    case 0:
                                        webViewJUC.K();
                                        return;
                                    case 1:
                                        webViewJUC.L();
                                        return;
                                    case 2:
                                        webViewJUC.L();
                                        return;
                                    case 3:
                                        ModelBrowser.this.vJ.J();
                                        return;
                                    default:
                                        return;
                                }
                            }
                        }
                    });
                }
                if (this.AX.getParent() != this.vM) {
                    if (this.AX.getParent() != null) {
                        ((ViewGroup) this.AX.getParent()).removeView(this.AX);
                    }
                    this.vM.addView(this.AX, this.AW);
                }
                this.AX.setVisibility(0);
                this.AX.bringToFront();
                int N = webViewJUC.N();
                this.AX.type = N;
                boolean equals = e.RD.equals(e.nR().nY().bw(h.afN));
                this.AX.setTextColor(equals ? -12693929 : -10921639);
                com.uc.h.e Pb = com.uc.h.e.Pb();
                switch (N) {
                    case 0:
                        this.AX.setText(Pb.getString(R.string.f1029readingmode_tail_caching));
                        this.AX.E(Pb.km(equals ? R.drawable.f615reading_stop_night : R.drawable.f614reading_stop));
                        this.AX.fL(0);
                        break;
                    case 1:
                        this.AX.setText(Pb.getString(R.string.f1030readingmode_tail_pause));
                        this.AX.E(Pb.km(equals ? R.drawable.f613reading_refresh_night : R.drawable.f612reading_refresh));
                        this.AX.fL(0);
                        break;
                    case 2:
                        this.AX.setText(Pb.getString(R.string.f1031readingmode_tail_fail));
                        this.AX.E(Pb.km(equals ? R.drawable.f613reading_refresh_night : R.drawable.f612reading_refresh));
                        this.AX.fL(0);
                        break;
                    case 3:
                        this.AX.setText(Pb.getString(R.string.f1032readingmode_tail_end));
                        this.AX.fL(8);
                        break;
                }
                this.AX.setAlpha((int) ((255.0d * ((double) (this.AY - i))) / ((double) this.AY)));
            } else if (this.AX != null) {
                this.AX.setVisibility(8);
                webViewJUC.Qw();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
      com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
      com.uc.browser.ModelBrowser.a(int, long):void
      com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
      com.uc.browser.ModelBrowser.a(android.view.View, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(WindowUCWeb windowUCWeb, WindowUCWeb windowUCWeb2, int i) {
        View view;
        WebViewJUC bJ;
        WebViewJUC ia;
        Class<WebViewZoom> cls = WebViewZoom.class;
        if (this.vC != null) {
            if (i == 2) {
                this.vC.a(windowUCWeb2);
                this.vC.B(windowUCWeb2.bK());
            } else {
                this.vC.a(windowUCWeb);
                this.vC.B(windowUCWeb.bK());
            }
        }
        if (this.AA != null && this.AA.isShowing()) {
            this.AA.dismiss();
        }
        if (windowUCWeb != null) {
            if (i == 0 && (ia = ia()) != null && ia.ac()) {
                ia.a(ia.a(0.60723f));
            }
            if (!(2 == i || this.vK == null || windowUCWeb == this.vK || (bJ = this.vK.bJ()) == null)) {
                bJ.ak();
            }
            if (i == 0 || 2 == i) {
                if (this.vU != null) {
                    windowUCWeb.h(true);
                }
                if (2 == i) {
                    if (windowUCWeb.bX != null) {
                        a(windowUCWeb, windowUCWeb.bX);
                        windowUCWeb.bX = null;
                        view = null;
                    } else {
                        a(windowUCWeb, "ext:openwindowbackground");
                        view = null;
                    }
                } else if (windowUCWeb.bX == null) {
                    view = windowUCWeb.a(this.bk, 1);
                } else {
                    this.vR = null;
                    view = a(windowUCWeb, windowUCWeb.bX);
                    windowUCWeb.bX = null;
                }
            } else {
                view = windowUCWeb.getCurrentView();
                if (!(view == null || this.vU == null)) {
                    Class<WebViewZoom> cls2 = WebViewZoom.class;
                    if (view.getClass() != cls) {
                        aR(0);
                    } else {
                        aR(1);
                    }
                }
            }
            if (2 == i && windowUCWeb2 != null) {
                view = windowUCWeb2.getCurrentView();
                if (!(view == null || this.vU == null)) {
                    Class<WebViewZoom> cls3 = WebViewZoom.class;
                    if (view.getClass() != cls) {
                        aR(0);
                    } else {
                        aR(1);
                    }
                }
                this.vR = null;
            }
            if (view != null) {
                if (this.vP != null) {
                    this.vM.removeAllViews();
                    this.vO.removeAllViews();
                }
                if (2 == i) {
                    this.vK = windowUCWeb2;
                    this.bi = view;
                } else {
                    this.vK = windowUCWeb;
                    this.bi = view;
                }
                a(view, false);
                view.requestFocus();
                e(view);
                f(this.bi);
                g(this.bi);
                if (!this.wL) {
                    hn();
                }
                if (i != 0) {
                    this.vK.bw();
                }
                if (2 == i && windowUCWeb2 != null) {
                    this.bk.t(windowUCWeb2.getTitle());
                } else if (1 == i) {
                    this.bk.t(windowUCWeb.getTitle());
                    k(windowUCWeb.getUrl(), "open existed window");
                } else if (i == 0) {
                    this.bk.t(this.bk.getString(R.string.f952app_name));
                }
                ViewMainBar.kI();
                if (this.vU != null) {
                    this.vU.QR();
                }
                if (true == this.vK.bk() && 1 == i) {
                    aP(this.vK.getProgress());
                }
                if (true == V()) {
                    if (this.wm != null) {
                        this.wm.setVisibility(0);
                        this.wm.bringToFront();
                        try {
                            this.vM.addView(this.wm);
                        } catch (Exception e) {
                        }
                    }
                    this.vK.j(true);
                }
            }
            P();
            if (1 == i) {
                e.nR().c((f) null);
            }
            if (this.wL) {
                this.wL = false;
                aS(101);
                im();
            }
            if (this.vK != null) {
                this.vK.bt();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Boolean bool) {
        this.vT.bP(bool.booleanValue());
        e.nR().nY().w(h.afy, bool.booleanValue() ? e.RD : "0");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.b(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, int):int
      com.uc.browser.ModelBrowser.b(android.content.Context, int):android.app.Dialog
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, android.content.Context):android.app.Dialog
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, boolean):boolean
      com.uc.browser.ModelBrowser.b(boolean, boolean):void
      com.uc.browser.ModelBrowser.b(android.view.View, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(Integer num) {
        if (this.vK == null) {
            return;
        }
        if (num.intValue() == 1 && !this.vK.canGoForward()) {
            return;
        }
        if (num.intValue() != -1 || this.vK.canGoBack()) {
            b(this.vK.q(num.intValue()), true);
            t(this.vK.getTitle());
            k(null, "goBackOrForward");
            if (this.bi != null && (this.bi instanceof WebViewJUC)) {
                a(106, b.ack, (WebViewJUC) this.bi);
                iy();
            }
            iJ();
        }
    }

    public void a(String str) {
        WebViewJUC webViewJUC;
        try {
            if (this.bi.getClass() == WebViewJUC.class && (webViewJUC = (WebViewJUC) this.bi) != null) {
                webViewJUC.a(str);
            }
        } catch (Exception e) {
        }
    }

    public void a(String str, String str2) {
        WebViewJUC webViewJUC;
        if (str != null && str2 != null && str != null && str2 != null) {
            str.trim();
            String str3 = !str.endsWith(au.aGF) ? str + au.aGF : str;
            if (this.bi.getClass() == ViewMainpage.class) {
                WebViewJUC cz = ((ViewMainpage) this.bi).cz();
                if (cz != null) {
                    cz.a(str3, str2);
                }
            } else if (this.bi.getClass() == WebViewJUC.class && (webViewJUC = (WebViewJUC) this.bi) != null) {
                webViewJUC.a(str3, str2);
            }
        }
    }

    public void a(boolean z, String str) {
        if (gZ()) {
            this.wl.K(str);
            this.wl.setVisibility(z ? 0 : 8);
        }
    }

    public void aK(int i) {
        if (this.vE != null) {
            this.vE.k(i);
        }
        if (this.vU != null) {
            this.vU.QS();
        }
    }

    /* access modifiers changed from: protected */
    public void aL(int i) {
        if (this.vT == null) {
            return;
        }
        if (i == 1) {
            this.vT.IQ();
        } else {
            this.vT.hide();
        }
    }

    public void aM(int i) {
        boolean z = this.Ar;
        this.Ar = gI();
        if (i == 1) {
            this.Aq = this.Ar;
        }
        boolean contains = e.nR().bw(h.afg).contains(e.RD);
        if (!this.Ar || !contains) {
            x(false);
            if (!this.Ar && z) {
                this.As = gM();
                return;
            }
            return;
        }
        if (this.As) {
            gN();
        }
        x(true);
    }

    public void aN(int i) {
        this.bk.aN(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.b(android.view.View, boolean):void
     arg types: [com.uc.browser.ViewMainpage, int]
     candidates:
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, int):int
      com.uc.browser.ModelBrowser.b(android.content.Context, int):android.app.Dialog
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, android.content.Context):android.app.Dialog
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, boolean):boolean
      com.uc.browser.ModelBrowser.b(boolean, boolean):void
      com.uc.browser.ModelBrowser.b(android.view.View, boolean):void */
    /* access modifiers changed from: package-private */
    public void aO(int i) {
        e.nR().c((f) null);
        e.nR().a((Picture) null);
        gT();
        if (this.vK != null) {
            ViewMainpage a2 = this.vK.a(this.bk, i);
            this.vE.k(1);
            b((View) a2, true);
        }
    }

    public void aP(int i) {
        if (i > 200 && i <= 300) {
            this.AH = 1;
            int i2 = i - 200;
        } else if (this.vP != null) {
            this.wb = (ProgressBar) this.vP.findViewById(R.id.f693title_progress);
            if (this.wb != null) {
                boolean z = (this.vU != null) & (!e.nR().nY().bw(h.aeZ).contains(e.RD));
                if (this.AH == 2) {
                    if (i >= 100) {
                        this.wb.setVisibility(8);
                        this.wc.setVisibility(8);
                        this.An = null;
                        gA();
                        t(this.vK.getTitle());
                    }
                    this.AH = 0;
                } else if (this.AH != 0) {
                } else {
                    if (i > 0 && i < 96) {
                        if (this.wb.getVisibility() != 0) {
                            this.wb.setVisibility(0);
                        }
                        if (z && this.wc.getVisibility() != 0) {
                            this.wc.setVisibility(0);
                        }
                        this.wb.setProgress(i);
                        this.wc.setProgress(i);
                    } else if (i >= 96 && i < 99) {
                        this.wb.setProgress(this.wb.getMax());
                        this.wc.setProgress(this.wc.getMax());
                        if (this.wc.getVisibility() == 0) {
                            this.wc.setVisibility(8);
                        }
                        if (this.wb.getVisibility() == 0) {
                            this.wb.setVisibility(8);
                        }
                    } else if (i >= 99) {
                        t(this.vK.getTitle());
                        if (this.wb != null) {
                            this.wb.setProgress(this.wb.getMax());
                            if (this.wb.getVisibility() == 0) {
                                this.wb.setVisibility(8);
                            }
                        }
                        if (this.wc != null) {
                            this.wc.setProgress(this.wc.getMax());
                            if (this.wc.getVisibility() == 0) {
                                this.wc.setVisibility(8);
                            }
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void aQ(int i) {
    }

    public void aS(int i) {
        this.mHandler.sendMessage(Message.obtain((Handler) null, i));
    }

    public void aT(int i) {
        switch (i) {
            case 1:
                e.nR().od();
                break;
            case 2:
                e.nR().oe();
                break;
            case 3:
                e.nR().of();
                break;
        }
        try {
            e.nR().d(this.p);
        } catch (Exception e) {
        }
    }

    public void aU(int i) {
        if (1 == i) {
            try {
                hs();
            } catch (Exception e) {
            }
        } else if (i == 0) {
            ht();
        }
    }

    public void ab(String str) {
        View inflate = LayoutInflater.from(this.bk).inflate((int) R.layout.f933refresh_timer, (ViewGroup) null);
        final EditText editText = (EditText) inflate.findViewById(R.id.f836intertimer);
        editText.setText(str);
        editText.setInputType(2);
        new UCAlertDialog.Builder(this.bk).aH(R.string.f1386pleaseinputintervaltimer).c(inflate).a((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (editText.getText().toString() != null) {
                    try {
                        int parseInt = Integer.parseInt(editText.getText().toString());
                        if (parseInt < Integer.parseInt(ModelBrowser.wn) || parseInt > 999) {
                            Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1387refreshprompt, 1).show();
                            ModelBrowser.gD().a(88, editText.getText().toString());
                        } else if (ModelBrowser.this.bi != null && ModelBrowser.this.bi.getClass() == WebViewJUC.class) {
                            ((WebViewJUC) ModelBrowser.this.bi).c(editText.getText().toString());
                            if (ModelBrowser.this.vK != null) {
                                ModelBrowser.this.gU();
                                ModelBrowser.this.vK.j(true);
                                if (ModelBrowser.this.wm != null) {
                                    ModelBrowser.this.wm.setVisibility(0);
                                    ModelBrowser.this.wm.bringToFront();
                                }
                            }
                        }
                    } catch (Exception e) {
                        ModelBrowser.gD().a(88, editText.getText().toString());
                        Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1387refreshprompt, 0).show();
                    }
                }
            }
        }).c((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        }).fh().show();
    }

    public void ac(String str) {
        int i = 0;
        if (str != null) {
            try {
                int parseInt = Integer.parseInt(str);
                h nY = e.nR().nY();
                if (parseInt != 0) {
                    i = 1;
                }
                nY.setTheme(i);
                if (parseInt == 0) {
                    ActivityBrowser.a(this.bk, -1);
                    ae.IS();
                    if (this.vC != null) {
                        this.vC.ep.ry().an(false);
                        com.uc.e.a.d.ey(0);
                    }
                    e.nR().nY().w(h.afN, "0");
                    if (this.wa != null && this.wa.getVisibility() == 0) {
                        this.wa.getDrawable().setAlpha(255);
                    }
                } else {
                    ActivityBrowser.e((Activity) this.bk);
                    ae.IR();
                    if (this.vC != null) {
                        this.vC.ep.ry().an(true);
                        com.uc.e.a.d.ey(-1728053248);
                    }
                    e.nR().nY().w(h.afN, e.RD);
                    if (parseInt == 1) {
                        hF();
                    }
                    if (this.wa != null && this.wa.getVisibility() == 0) {
                        this.wa.getDrawable().setAlpha(128);
                    }
                }
                iy();
                WebViewJUC webViewJUC = null;
                if (this.bi != null) {
                    if (this.bi.getClass() == ViewMainpage.class) {
                        webViewJUC = ((ViewMainpage) this.bi).cz();
                    } else if (this.bi.getClass() == WebViewJUC.class) {
                        webViewJUC = (WebViewJUC) this.bi;
                    }
                }
                if (webViewJUC != null) {
                    webViewJUC.postInvalidate();
                }
                if (this.vT != null) {
                    this.vT.hS(parseInt);
                }
                m.dq().bz();
            } catch (Exception e) {
            }
        }
    }

    public boolean ae(String str) {
        if (str == null) {
            return false;
        }
        String trim = str.trim();
        if (trim.length() == 0) {
            return false;
        }
        return !trim.contains("\\") && !trim.contains(au.aGF) && !trim.contains(cd.bVI) && !trim.contains("*") && !trim.contains("?") && !trim.contains("\"") && !trim.contains("<") && !trim.contains(">");
    }

    public void af(String str) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this.bk);
        View inflate = this.bk.getLayoutInflater().inflate((int) R.layout.f943uc_traffic_save, (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(R.id.f823msg);
        textView.setText(str);
        textView.setTextColor(com.uc.h.e.Pb().getColor(78));
        builder.c(inflate);
        builder.a((int) R.string.f1540uc_traffic_dialog_confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().a(11, w.OW);
                    b.a.a.f.l(2, b.a.a.f.atY);
                }
            }
        });
        builder.b((int) R.string.f1541uc_traffic_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                b.a.a.f.l(2, b.a.a.f.atZ);
            }
        });
        builder.fh().show();
    }

    public InputStream ag(String str) {
        String str2 = au.aGF + str;
        if (!str2.endsWith(".png") && !str2.endsWith(".gif") && !str2.endsWith(".jpg")) {
            str2 = str2 + ".png";
        }
        try {
            return this.bk.getAssets().open("uc/" + str2.substring(1));
        } catch (Exception e) {
            return null;
        }
    }

    public boolean ah(String str) {
        if (str == null || str.trim().length() == 0) {
            return false;
        }
        return str.contains("ext:a:") || str.contains("ext:e:") || str.contains("ext:es:") || str.contains("ext:as:") || str.contains(b.acI);
    }

    public boolean aj(String str) {
        return com.uc.b.g.i(this.bk, str);
    }

    public boolean as() {
        WebViewJUC ia = ia();
        if (ia != null) {
            return ia.as();
        }
        return false;
    }

    public int az() {
        return (int) this.bk.Fy();
    }

    public void b(MotionEvent motionEvent) {
        if (this.bi != this.vC && this.vU != null) {
            this.vU.dispatchTouchEvent(motionEvent);
        }
    }

    public void b(ActivityBrowser activityBrowser) {
        this.vD = com.uc.h.e.Pb();
        this.bk = activityBrowser;
        if (!e.nR().pO()) {
            com.uc.b.a.a(activityBrowser.getApplicationContext());
            Resources resources = activityBrowser.getResources();
            e.nR().a((int) resources.getDimension(R.dimen.f338juc_text_small), (int) resources.getDimension(R.dimen.f339juc_text_medium), (int) resources.getDimension(R.dimen.f340juc_text_large), (int) resources.getDimension(R.dimen.f341juc_download_xoffset), (int) resources.getDimension(R.dimen.f342juc_download_buttontext_size), (int) resources.getDimension(R.dimen.f343juc_download_filenametext_size), (int) resources.getDimension(R.dimen.f344juc_download_speedtext_size), (int) resources.getDimension(R.dimen.f345juc_download_statebar_size), (int) resources.getDimension(R.dimen.f346juc_download_itemtext_size), resources.getDimension(R.dimen.f347juc_multiple_font), resources.getDisplayMetrics().xdpi, new float[]{resources.getDimension(R.dimen.f403unit_pt), resources.getDimension(R.dimen.f404unit_in), resources.getDimension(R.dimen.f405unit_mm)}, (int) resources.getDimension(R.dimen.f406wap10_line_space));
            if (com.uc.b.b.pE == null || com.uc.b.b.pE.length() == 0) {
                TelephonyManager telephonyManager = (TelephonyManager) this.bk.getSystemService("phone");
                com.uc.b.b.pE = telephonyManager.getDeviceId();
                e.nR().br(com.uc.b.b.pE);
                e.nR().bs(telephonyManager.getSubscriberId());
            }
            e.nR().e(this.p);
            e.nR().d(this.p);
            e.nR().oE();
        }
        gO();
        if (this.xj > 0) {
            e.nR().qd();
        }
        activityBrowser.requestWindowFeature(3);
        activityBrowser.requestWindowFeature(4);
        this.wL = true;
        a();
        if (!ActivityUpdate.IP) {
            this.bk.Fs();
            this.wL = false;
        }
        this.Ap = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                ModelBrowser.this.mHandler.removeMessages(ModelBrowser.zQ);
                ModelBrowser.this.a((int) ModelBrowser.zQ, 1, 2000);
            }
        };
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        this.bk.registerReceiver(this.Ap, intentFilter);
        e.nR().oi();
        aM(1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
     arg types: [com.uc.browser.ModelBrowser$YesOrNoDlgData, int]
     candidates:
      com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
      com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
      com.uc.browser.ModelBrowser.a(int, long):void
      com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void */
    public void b(YesOrNoDlgData yesOrNoDlgData) {
        a(yesOrNoDlgData, false);
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z, final boolean z2) {
        if (!z || (z && this.vC != null && this.vC == this.bi && this.vC.uX() == 1)) {
            this.mHandler.postDelayed(new Runnable() {
                public void run() {
                    try {
                        e.nR().qz();
                        if (z2) {
                            ModelBrowser.this.vC.invalidate();
                        }
                    } catch (Throwable th) {
                    }
                }
            }, 500);
        }
    }

    /* access modifiers changed from: protected */
    public boolean b(File file) {
        if (file != null && file.isDirectory()) {
            String[] list = file.list();
            for (String file2 : list) {
                if (!b(new File(file, file2))) {
                    return false;
                }
            }
        }
        return file.delete();
    }

    public void bf() {
        this.wK = false;
        Vector qc = e.nR().qc();
        if (qc == null || qc.size() == 0) {
            WebViewJUC ia = ia();
            if (ia != null) {
                aS(12);
                ia.zG();
                return;
            }
            return;
        }
        this.bk.startActivityForResult(new Intent(this.bk.getBaseContext(), ActivitySelector.class), 4);
    }

    public boolean bk() {
        return this.vK.bk();
    }

    /* access modifiers changed from: package-private */
    public boolean bo() {
        if (this.vK == null) {
            return false;
        }
        return this.vK.bo();
    }

    public void c(boolean z, boolean z2) {
        RelativeLayout.LayoutParams layoutParams;
        RelativeLayout.LayoutParams layoutParams2;
        if (this.AV == null) {
            iD();
        }
        if (this.AV != null) {
            this.AV.setBackgroundResource(e.RD.equals(e.nR().nY().bw(h.afN)) ? z ? R.drawable.f623readingmode_on_night : R.drawable.f621readingmode_off_night : z ? R.drawable.f622readingmode_on : R.drawable.f620readingmode_off);
            this.AU.setVisibility(0);
            if (!(this.wa == null || this.wa.getVisibility() != 0 || (layoutParams2 = (RelativeLayout.LayoutParams) this.wa.getLayoutParams()) == null)) {
                layoutParams2.addRule(11, 0);
                this.wa.setLayoutParams(layoutParams2);
            }
            if (!(this.vZ == null || this.vZ.getVisibility() != 0 || (layoutParams = (RelativeLayout.LayoutParams) this.vZ.getLayoutParams()) == null)) {
                layoutParams.addRule(11, 0);
                this.vZ.setLayoutParams(layoutParams);
            }
            if (z2) {
                iI();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean canGoBack() {
        if (this.vK == null) {
            return false;
        }
        return this.vK.canGoBack();
    }

    /* access modifiers changed from: package-private */
    public boolean canGoForward() {
        if (this.vK == null) {
            return false;
        }
        return this.vK.canGoForward();
    }

    public n ch() {
        return this.p;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
     arg types: [int, int]
     candidates:
      com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
      com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
      com.uc.browser.ModelBrowser.a(int, long):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
      com.uc.browser.ModelBrowser.a(int, java.lang.Object):void */
    /* access modifiers changed from: package-private */
    public int d(String[] strArr) {
        if (com.uc.b.g.i(this.bk, wR)) {
            return d.a(this.bk, strArr);
        }
        a(90, (Object) 3);
        return -8;
    }

    public void d(int i, String str) {
        String qH;
        if (str != null && i > 0 && (qH = e.nR().qH()) != null) {
            gD().a(11, qH + "&scan_result=" + i + "&web_url=" + URLEncoder.encode(str));
        }
    }

    public void d(View view) {
        this.Av = view;
        if (this.bk != null) {
            try {
                if (view.getParent() == null) {
                    this.bk.addContentView(this.Av, new ViewGroup.LayoutParams(-1, -1));
                }
                this.Av.setVisibility(0);
                this.Av.bringToFront();
            } catch (Exception e) {
            }
        }
    }

    public void d(String str, int i) {
        if (this.vH == null) {
            gQ();
        }
        if (this.bi == this.vC) {
            this.vH.r("");
        } else if (str != null) {
            this.vH.r(str);
        } else {
            this.vH.r("");
        }
        this.vH.show();
    }

    public void dt() {
        if (this.bi != null) {
            WebViewJUC webViewJUC = null;
            if (this.bi instanceof ViewMainpage) {
                webViewJUC = ((ViewMainpage) this.bi).cz();
                if (webViewJUC == null) {
                    return;
                }
            } else if ((this.bi instanceof WebViewJUC) && (webViewJUC = (WebViewJUC) this.bi) == null) {
                return;
            }
            if (webViewJUC != null) {
                this.bk.openContextMenu(webViewJUC);
            }
        }
    }

    public void e(int i, Object obj) {
        Class<ActivityChooseFile> cls = ActivityChooseFile.class;
        if (System.currentTimeMillis() - this.AP >= 500) {
            this.AP = System.currentTimeMillis();
            if (i == 0) {
                if (UcCamera.wu()) {
                    this.bk.startActivityForResult(new Intent(this.bk.getBaseContext(), ActivityCamera.class), 5);
                    return;
                }
                Class<ActivityChooseFile> cls2 = ActivityChooseFile.class;
                Intent intent = new Intent(this.bk, cls);
                intent.putExtra(ActivityChooseFile.uv, 1);
                this.bk.startActivityForResult(intent, 6);
            } else if (1 != i) {
                Class<ActivityChooseFile> cls3 = ActivityChooseFile.class;
                Intent intent2 = new Intent(this.bk, cls);
                intent2.putExtra(ActivityChooseFile.uv, 1);
                this.bk.startActivityForResult(intent2, 6);
            } else if (UcCamera.wu()) {
                ic();
            } else {
                Class<ActivityChooseFile> cls4 = ActivityChooseFile.class;
                Intent intent3 = new Intent(this.bk, cls);
                intent3.putExtra(ActivityChooseFile.uv, 1);
                this.bk.startActivityForResult(intent3, 6);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void e(Context context) {
        try {
            File cacheDir = context.getCacheDir();
            if (cacheDir != null && cacheDir.isDirectory()) {
                b(cacheDir);
            }
        } catch (Exception e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.a.e.a(boolean, com.uc.a.n):int
     arg types: [int, com.uc.a.n]
     candidates:
      com.uc.a.e.a(java.lang.String, java.util.Hashtable):java.util.Vector
      com.uc.a.e.a(b.a.a.e, b.a.a.e):int
      com.uc.a.e.a(float, int):android.graphics.Bitmap
      com.uc.a.e.a(java.lang.String, byte):java.lang.String
      com.uc.a.e.a(int, android.graphics.Bitmap):void
      com.uc.a.e.a(int, short):void
      com.uc.a.e.a(android.graphics.Bitmap, int):void
      com.uc.a.e.a(com.uc.a.n, int):void
      com.uc.a.e.a(java.io.File, int):void
      com.uc.a.e.a(java.util.List, int):void
      com.uc.a.e.a(boolean, int):void
      com.uc.a.e.a(int, int):boolean
      com.uc.a.e.a(int, b.a.a.e):boolean
      com.uc.a.e.a(java.lang.String, b.a.a.c.e):boolean
      com.uc.a.e.a(boolean, com.uc.a.n):int */
    public void e(String[] strArr) {
        int lastIndexOf;
        String V;
        if (!Environment.getExternalStorageState().equals("mounted")) {
            Toast.makeText(this.bk, (int) R.string.f1389tcardnotusable, 1).show();
            return;
        }
        if (strArr.length > 3) {
            strArr[3] = e.nR().bG(strArr[3].trim());
        }
        if (true == f(strArr)) {
            return;
        }
        if (g.Jn().JO() || !e.nR().nX().U(strArr[3])) {
            View inflate = LayoutInflater.from(this.bk).inflate((int) R.layout.f896download_dialog_inputfilename, (ViewGroup) null);
            ((TextView) inflate.findViewById(R.id.f738filename)).setTextColor(com.uc.h.e.Pb().getColor(78));
            final UCAlertDialog fh = new UCAlertDialog.Builder(this.bk).aH(R.string.f1342file_saved_as).c(inflate).fh();
            View findViewById = inflate.findViewById(R.id.f740dlg_ok);
            View findViewById2 = inflate.findViewById(R.id.f741dlg_cancel);
            final EditText editText = (EditText) inflate.findViewById(R.id.f739download_dlg_filename);
            int length = strArr.length;
            String[] strArr2 = new String[length];
            for (int i = 0; i < length; i++) {
                strArr2[i] = strArr[i];
            }
            if (this.wC == null) {
                if (true == e.nR().nX().T(strArr2[3])) {
                    this.wC = e.nR().nX().V(strArr2[3]);
                }
                if (this.wC != null && !this.wC.equals(strArr2[3])) {
                    strArr2[3] = this.wC;
                }
            }
            editText.setText(strArr2[3]);
            if (!(strArr2[3] == null || "".equals(strArr2[3]) || (lastIndexOf = strArr2[3].lastIndexOf(46)) == -1)) {
                editText.setSelection(0, lastIndexOf);
            }
            AnonymousClass15 r1 = new DownloadOnClickListener(strArr2) {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.uc.a.e.a(boolean, com.uc.a.n):int
                 arg types: [int, com.uc.a.n]
                 candidates:
                  com.uc.a.e.a(java.lang.String, java.util.Hashtable):java.util.Vector
                  com.uc.a.e.a(b.a.a.e, b.a.a.e):int
                  com.uc.a.e.a(float, int):android.graphics.Bitmap
                  com.uc.a.e.a(java.lang.String, byte):java.lang.String
                  com.uc.a.e.a(int, android.graphics.Bitmap):void
                  com.uc.a.e.a(int, short):void
                  com.uc.a.e.a(android.graphics.Bitmap, int):void
                  com.uc.a.e.a(com.uc.a.n, int):void
                  com.uc.a.e.a(java.io.File, int):void
                  com.uc.a.e.a(java.util.List, int):void
                  com.uc.a.e.a(boolean, int):void
                  com.uc.a.e.a(int, int):boolean
                  com.uc.a.e.a(int, b.a.a.e):boolean
                  com.uc.a.e.a(java.lang.String, b.a.a.c.e):boolean
                  com.uc.a.e.a(boolean, com.uc.a.n):int */
                public void onClick(View view) {
                    switch (view.getId()) {
                        case R.id.f740dlg_ok /*2131099743*/:
                            if (!Environment.getExternalStorageState().equals("mounted")) {
                                Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1389tcardnotusable, 1).show();
                                return;
                            }
                            Editable text = editText.getText();
                            String obj = text != null ? text.toString() : null;
                            if (obj == null) {
                                obj = this.bRB[3];
                            }
                            if (obj != null) {
                                obj = obj.trim();
                            }
                            if (obj == null || obj.length() == 0) {
                                Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1444filenamenotnull, 1).show();
                                return;
                            }
                            this.bRB[3] = obj;
                            if (!e.nR().nX().U(this.bRB[3])) {
                                Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1440invalid_name, 1).show();
                                return;
                            } else if (!e.nR().nX().T(this.bRB[3])) {
                                if (e.RD.equals(e.nR().bw(h.afo))) {
                                    e.nR().a(false, ModelBrowser.this.p);
                                    e.nR().nX().a(this.bRB);
                                } else {
                                    Intent intent = new Intent(ModelBrowser.this.bk, ActivityDownload.class);
                                    intent.putExtra("downloadinfo", this.bRB);
                                    ModelBrowser.this.bk.startActivity(intent);
                                }
                                String unused = ModelBrowser.this.wC = (String) null;
                                fh.dismiss();
                                return;
                            } else {
                                editText.setText(this.bRB[3]);
                                Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1439need_rename, 1).show();
                                return;
                            }
                        case R.id.f741dlg_cancel /*2131099744*/:
                            String unused2 = ModelBrowser.this.wC = (String) null;
                            fh.dismiss();
                            return;
                        default:
                            return;
                    }
                }
            };
            findViewById.setOnClickListener(r1);
            findViewById2.setOnClickListener(r1);
            fh.show();
            return;
        }
        if (true == e.nR().nX().T(strArr[3]) && (V = e.nR().V(strArr[3])) != null) {
            strArr[3] = V;
        }
        if (e.RD.equals(e.nR().bw(h.afo))) {
            e.nR().a(false, this.p);
            e.nR().nX().a(strArr);
            return;
        }
        Intent intent = new Intent(this.bk, ActivityDownload.class);
        intent.putExtra("downloadinfo", strArr);
        this.bk.startActivity(intent);
    }

    public void f(int i, final Object obj) {
        final Intent intent = new Intent("android.intent.action.SEND");
        if (i != 0 || (obj != null && (obj instanceof String[]) && ((String[]) obj).length >= 3)) {
            if (i == 0) {
                intent.setType("text/plain");
                intent.putExtra("android.intent.extra.TEXT", ((String[]) obj)[0]);
            } else if (i == 2) {
                intent.setType("text/plain");
                intent.putExtra("android.intent.extra.TEXT", ((String[]) obj)[0]);
            } else {
                intent.setType("image/*");
                intent.putExtra("android.intent.extra.STREAM", (Uri) obj);
            }
            if (com.uc.b.b.ec() != 0) {
                List<ResolveInfo> queryIntentActivities = this.bk.getPackageManager().queryIntentActivities(intent, 0);
                if (queryIntentActivities.size() != 0) {
                    if (i == 0) {
                        ResolveInfo resolveInfo = new ResolveInfo();
                        ActivityInfo activityInfo = new ActivityInfo();
                        activityInfo.packageName = this.bk.getPackageName();
                        resolveInfo.activityInfo = activityInfo;
                        queryIntentActivities.add(0, resolveInfo);
                    }
                    d.a(this.bk, "分享到", queryIntentActivities, new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView adapterView, View view, int i, long j) {
                            String str = ((ResolveInfo) adapterView.getAdapter().getItem(i)).activityInfo.packageName;
                            String str2 = ((ResolveInfo) adapterView.getAdapter().getItem(i)).activityInfo.name;
                            if (str.equals(ModelBrowser.this.bk.getPackageName())) {
                                String str3 = ((String[]) obj)[1];
                                if (str3 != null && str3.contains(cb.bPH)) {
                                    str3 = str3.replace(cb.bPH, cb.bPI);
                                }
                                ModelBrowser.this.m(str3, ((String[]) obj)[2]);
                                return;
                            }
                            Intent intent = new Intent(ModelBrowser.this.bk, ActivityShield.class);
                            intent.putExtra(ActivityShield.tt, "android.intent.action.SEND");
                            intent.putExtra(ActivityShield.tr, str);
                            if (str2 != null) {
                                intent.putExtra(ActivityShield.ts, str2);
                            }
                            intent.setType(intent.getType());
                            intent.putExtra("android.intent.extra.TEXT", intent.getStringExtra("android.intent.extra.TEXT"));
                            intent.putExtra("android.intent.extra.STREAM", intent.getParcelableExtra("android.intent.extra.STREAM"));
                            ModelBrowser.this.bk.startActivity(intent);
                        }
                    });
                } else if (i == 0) {
                    m(((String[]) obj)[1], ((String[]) obj)[2]);
                } else if (i != 2) {
                    Toast.makeText(this.bk, (int) R.string.f963share_image_not_yet, 0).show();
                }
            } else if (i == 0) {
                m(((String[]) obj)[1], ((String[]) obj)[2]);
            } else if (i != 2) {
                Toast.makeText(this.bk, (int) R.string.f963share_image_not_yet, 0).show();
            }
        }
    }

    public void g(int i) {
        WebViewJUC ia = ia();
        if (ia != null) {
            ia.g(i);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
     arg types: [int, int]
     candidates:
      com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
      com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
      com.uc.browser.ModelBrowser.a(int, long):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
      com.uc.browser.ModelBrowser.a(int, java.lang.Object):void */
    public void g(int i, Object obj) {
        String str;
        if (i == 0) {
            byte b2 = (this.vK == null || true != this.vK.bI()) ? (byte) 0 : 1;
            WindowUCWeb a2 = this.vJ.a((Message) obj, this.bk);
            if (a2 != null && 1 == b2) {
                a2.d(b2);
            }
        } else if (1 == i) {
            String str2 = (String) obj;
            if (str2 != null) {
                this.vJ.cJ(str2);
            }
        } else if (2 == i) {
            String str3 = null;
            if (obj instanceof String[]) {
                str = ((String[]) obj)[0];
                str3 = ((String[]) obj)[1];
            } else {
                str = (String) obj;
            }
            if (str != null) {
                if (str.startsWith(wU) || str.startsWith(wV) || str.startsWith(wW) || str.startsWith(wX) || str3 != null) {
                    int u = this.vJ.u((str.startsWith(wU) || str.startsWith(wV)) ? 0 : str.startsWith(wW) ? 1 : str.startsWith(wX) ? 2 : str3 != null ? (byte) 3 : -1);
                    if (-1 != u) {
                        this.vJ.eQ(u);
                        this.bi = null;
                        WindowUCWeb k = this.vJ.k(u, str);
                        if (k != null) {
                            k.q(str3);
                        }
                    } else {
                        if (!this.vJ.wg()) {
                            this.vJ.eO(this.vJ.wh() - 1);
                        }
                        WindowUCWeb cJ = this.vJ.cJ(str);
                        if (cJ != null) {
                            cJ.q(str3);
                        }
                    }
                } else if (!this.vJ.wg()) {
                    hl();
                    a(11, str);
                } else {
                    this.vJ.cJ(str);
                }
                if (e.RD.equals(e.nR().nY().bw(h.afK))) {
                    a(18, (Object) 1);
                }
                if (e.RD.equals(e.nR().nY().bw(h.afN))) {
                    a(33, (Object) 3);
                }
                if (e.RD.equals(e.nR().nY().bw(h.afy))) {
                    a(70, (Object) 3);
                }
            }
        }
    }

    public void g(View view) {
        if (view != null && (view instanceof WebViewJUC)) {
            WebViewJUC webViewJUC = (WebViewJUC) view;
            if (webViewJUC.M()) {
                y(webViewJUC.G(), 0);
            }
        }
    }

    public void gB() {
        try {
            WebViewJUC ia = ia();
            if (ia != null) {
                ia.gB();
            }
        } catch (Exception e) {
        }
    }

    public void gC() {
        Bundle bundle = new Bundle();
        bundle.putString("uploadfont", "uploadfont");
        Intent intent = new Intent(this.bk, ActivityUploadFont.class);
        intent.putExtras(bundle);
        this.bk.startActivity(intent);
    }

    /* access modifiers changed from: package-private */
    public void gG() {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ((ActivityManager) this.bk.getSystemService("activity")).getMemoryInfo(memoryInfo);
        if (memoryInfo.availMem < memoryInfo.threshold) {
        }
    }

    public BroadcastReceiver gH() {
        return this.Ap;
    }

    public void gJ() {
        this.Ar = gI();
        boolean contains = e.nR().bw(h.afg).contains(e.RD);
        if (this.Ar && contains) {
            x(true);
        }
    }

    public boolean gK() {
        return this.Aq;
    }

    public boolean gL() {
        return this.Ar;
    }

    public boolean gM() {
        this.As = com.uc.c.a.d.zm().zr() > 0;
        aS(141);
        if (this.As && this.Au) {
            this.Au = false;
            Toast.makeText(this.bk, (int) R.string.f996toast_wifi_pause_dl, 0).show();
        }
        return true;
    }

    public void gN() {
        if (this.As) {
            com.uc.c.a.d.zm().zs();
            this.As = false;
        }
    }

    public int gR() {
        return this.bk.gR();
    }

    public void gT() {
        if (e.nR().nu()) {
            if (this.vC != null) {
                if (e.nR().qe()) {
                    this.vC.cA();
                    e.nR().qf();
                }
                this.vC.cB();
                this.vC.cC();
            }
            if (this.vG == null) {
                gP();
            }
            this.vG.bP();
            e.nR().Z(false);
        }
    }

    public void gU() {
        if (this.wm == null) {
            this.wm = new ViewCloseRefreshTimer(this.bk);
            this.wm.setVisibility(4);
            if (this.vK != null) {
                this.vK.j(false);
            }
        }
        try {
            if (this.vM != null && this.wm != null) {
                this.vM.addView(this.wm);
                this.wm.bringToFront();
            }
        } catch (Exception e) {
        }
    }

    public void gV() {
        if (this.wj == null) {
            this.wj = new DriftEditTextForRelativeLayout(this.bk);
            this.wj.a(new DriftEditTextBinder() {
                public void a(CharSequence charSequence) {
                    ModelBrowser.this.wj.setText(charSequence);
                }
            });
        }
        try {
            this.vM.addView(this.wj);
            this.wj.bringToFront();
        } catch (Exception e) {
        }
    }

    public void gW() {
        int i;
        if (this.wk == null) {
            this.wk = new ViewPageUpDownButton(this.bk);
            String bw = e.nR().nY().bw(h.afw);
            if (bw != null) {
                try {
                    i = Integer.parseInt(bw);
                } catch (Exception e) {
                    i = 2;
                }
            } else {
                i = 2;
            }
            this.wk.setState(i);
            this.wk.setVisibility(8);
        }
        if (this.vM != null) {
            try {
                this.vM.addView(this.wk);
                this.wk.bringToFront();
            } catch (Exception e2) {
            }
        }
    }

    public boolean gY() {
        return this.wl != null && this.wl.getVisibility() == 0;
    }

    public boolean gZ() {
        if (this.wl == null) {
            this.wl = new WebViewSafePopup(this.bk);
            this.wl.setVisibility(8);
        }
        if (this.wl.getParent() == null) {
            try {
                this.vM.addView(this.wl);
                this.wl.bringToFront();
            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }

    public Context getContext() {
        if (this.bk != null) {
            return this.bk;
        }
        return null;
    }

    public Activity gm() {
        return this.vB;
    }

    public ViewMainpage gn() {
        if (this.vC == null) {
            this.vC = (ViewMainpage) LayoutInflater.from(this.bk).inflate((int) R.layout.f907launcher, (ViewGroup) null);
            this.vC.a();
            this.vE = this.vC.cy().rz();
            this.vE.a(this.vF.wV());
            this.vE.aD();
            this.vC.a(this.vK);
        }
        return this.vC;
    }

    /* access modifiers changed from: protected */
    public void gs() {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this.bk, false);
        builder.L(this.bk.getResources().getString(R.string.f1483add_to_bookmark_dialog_title));
        builder.a(new String[]{this.bk.getString(R.string.f1482add_to_bookmark), this.bk.getString(R.string.f1481add_to_navigation)}, -1, false, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == 0) {
                    ModelBrowser.this.in();
                } else if (e.nR() != null) {
                    Intent intent = new Intent(ModelBrowser.this.bk, ActivityEditMyNavi.class);
                    intent.putExtra(ActivityEditMyNavi.bvj, ModelBrowser.this.vK.getTitle());
                    intent.putExtra(ActivityEditMyNavi.bvi, ModelBrowser.this.vK.getUrl());
                    intent.putExtra(ActivityEditMyNavi.bvk, ModelBrowser.this.vK.p());
                    intent.putExtra(ActivityEditMyNavi.bvl, -1);
                    ModelBrowser.this.bk.startActivity(intent);
                } else {
                    Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1484navi_full, (int) b.a.a.c.e.HTTP_MULT_CHOICE).show();
                }
                dialogInterface.dismiss();
            }
        });
        builder.b((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.fh().show();
    }

    /* access modifiers changed from: protected */
    public void gt() {
        clearCache(true);
        e(this.bk);
        e.nR().ob().ox();
        e.nR().oa().clearHistory();
        e.nR().nZ().ov();
        e.nR().nI();
    }

    public void gu() {
        if (this.vH == null) {
            gQ();
        }
        this.vH.hide();
    }

    /* access modifiers changed from: protected */
    public void gv() {
        if (this.vG == null) {
            gP();
        }
        this.vG.hide();
    }

    /* access modifiers changed from: protected */
    public void gx() {
        if (this.vJ != null) {
            this.vJ.cI(e.nR().nZ().oN());
        }
    }

    public void hA() {
        this.vU.setVisibility(8);
    }

    public void hB() {
        this.vU.setVisibility(0);
    }

    public void hC() {
        if (!l.FALSE.equals(e.nR().nY().bw("should_show_zoom_mode_tips"))) {
            j((Context) this.bk).show();
        }
    }

    public void hD() {
        if (!"0".equals(e.nR().nY().bw("should_show_novel_mode_tips"))) {
            k((Context) this.bk).show();
        }
    }

    public void hE() {
        boolean z = !"0".equals(e.nR().nY().bw("should_show_page_up_down_mode_tips"));
        boolean z2 = e.nR().nY().bw(h.afq).trim().equals(e.RE);
        if (z && z2) {
            l((Context) this.bk).show();
        }
    }

    public void hF() {
        if (!"0".equals(e.nR().nY().bw("should_show_nightmode_tips"))) {
            m((Context) this.bk).show();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void hI() {
        Intent intent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        intent.putExtra("android.intent.extra.shortcut.INTENT", new Intent(this.bk, ActivityUpdate.class));
        intent.putExtra("android.intent.extra.shortcut.NAME", this.bk.getString(R.string.f952app_name));
        intent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(this.bk, R.drawable.f543aaicon));
        intent.putExtra("duplicate", false);
        this.bk.sendBroadcast(intent);
    }

    public boolean hK() {
        return (this.bi.getClass() == ViewMainpage.class || id() == null || id().length() <= 0 || ie() == null || ie().length() <= 0) ? false : true;
    }

    public boolean hL() {
        return this.bi.getClass() != ViewMainpage.class;
    }

    public boolean hM() {
        return (this.bi.getClass() == ViewMainpage.class || this.vK.getUrl() == null || this.vK.getUrl().equals("")) ? false : true;
    }

    public boolean hN() {
        return this.bi.getClass() == ViewMainpage.class;
    }

    public void hP() {
        if (this.vU != null) {
            this.vU.QR();
        }
    }

    public void hQ() {
        if (this.bk != null) {
            if (this.vJ == null || this.vJ.wl() < 3) {
                if (this.bi != null && this.bi.getClass() == WebViewJUC.class && 1 == ((WebViewJUC) this.bi).C()) {
                    ab(wn);
                } else if (this.bi.getClass() == WebViewZoom.class) {
                    Toast.makeText(this.bk, (int) R.string.f1383notsupportrefreshtimerzoom, 0).show();
                } else if (this.bi.getClass() == ViewMainpage.class) {
                    Toast.makeText(this.bk, (int) R.string.f1384notsupportrefreshtimerHomePage, 0).show();
                } else {
                    Toast.makeText(this.bk, (int) R.string.f1385notsupportrefreshtimerZip, 1).show();
                }
            } else {
                Toast.makeText(this.bk, (int) R.string.f1391allowlessthenfour, 0).show();
            }
        }
    }

    public void hR() {
        if (this.wk != null) {
            this.wk.bringToFront();
        }
        if (this.wl != null) {
            this.wl.bringToFront();
        }
        if (this.wm != null) {
            this.wm.bringToFront();
        }
        if (this.wJ != null) {
            this.wJ.bringToFront();
        }
        if (this.vU != null) {
            this.vU.bringToFront();
        }
    }

    public void hS() {
        if (((this.bi != null && this.bi.getClass() == ViewMainpage.class) || (this.bi != null && this.bi.getClass() == WebViewJUC.class)) && this.wj != null) {
            this.wj.cS(true);
        }
        if (this.wj != null) {
            this.wj.cS(false);
        }
    }

    public void hT() {
        if (this.wj != null && this.wj.getVisibility() == 0) {
            this.wj.cT(true);
        }
    }

    public void hU() {
        stopLoading();
        if (b.b.a.OR() == 0) {
            a(62, "当前网络不可用，请检查设置！");
            return;
        }
        e.nR().stopLoading();
        if (this.vU != null) {
            this.vU.cW(false);
        }
        aS(67);
    }

    public void hV() {
        this.wA = true;
        e.nR().hU();
        e.nR().d(this.p);
    }

    public void hW() {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this.bk);
        builder.aH(R.string.f1425uptitle);
        builder.M(com.uc.h.e.Pb().getString(R.string.f1203dialog_play_not_support_rtsp));
        builder.a((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ModelBrowser.this.aT(2);
            }
        });
        builder.c((int) R.string.cancel, (DialogInterface.OnClickListener) null);
        builder.show();
    }

    public String hX() {
        if (this.bi instanceof WebViewZoom) {
            return ((WebViewZoom) this.bi).getUrl();
        }
        return null;
    }

    public boolean hY() {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
     arg types: [int, int]
     candidates:
      com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
      com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
      com.uc.browser.ModelBrowser.a(int, long):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
      com.uc.browser.ModelBrowser.a(int, java.lang.Object):void */
    public void hZ() {
        try {
            e.nR().oh();
            e.nR().og();
            e.nR().d(this.p);
            if (ActivityUpdate.IP) {
                e.nR().oE();
            }
            this.vW = (int) this.bk.getResources().getDimension(R.dimen.f290progressbar_height);
            d.d(this.bk);
            hx();
            this.wg = ((PowerManager) this.bk.getSystemService("power")).newWakeLock(1, "UCBrowser");
            ModelBrowser gD = gD();
            if (this.xk <= 0) {
                gD.aS(110);
            }
            if (this.xj <= 2) {
                gD.aS(50);
            }
            gD.a(90, (Object) 1);
            e.nR().qv();
            e.nR().qw();
            if (this.xk == 0) {
                gD.aS(zO);
                gD.aS(Af);
            }
            gD.aS(zY);
        } catch (Exception e) {
        }
    }

    public void ha() {
        Calendar instance = Calendar.getInstance();
        int i = instance.get(13);
        int i2 = instance.get(12);
        String str = "" + instance.get(11) + (i2 < 10 ? ":0" : cd.bVI) + i2;
        if (this.vZ != null) {
            this.vZ.setVisibility(0);
            if ((this.AU == null || this.AU.getVisibility() != 0) && (this.wa == null || this.wa.getVisibility() != 0)) {
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.vZ.getLayoutParams();
                if (layoutParams != null) {
                    layoutParams.addRule(11);
                    this.vZ.setLayoutParams(layoutParams);
                }
            } else {
                RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.vZ.getLayoutParams();
                if (layoutParams2 != null) {
                    layoutParams2.addRule(11, 0);
                    this.vZ.setLayoutParams(layoutParams2);
                }
            }
            this.vZ.setText(str);
        }
        a(109, (long) ((61 - i) * 1000));
    }

    public void hb() {
        if (this.mHandler != null) {
            this.mHandler.removeMessages(109);
        }
        if (this.mHandler != null) {
            this.vZ.setVisibility(8);
            this.vZ.setText((CharSequence) null);
        }
    }

    /* access modifiers changed from: package-private */
    public ViewGroup.LayoutParams hd() {
        if (this.vN == null) {
            return null;
        }
        Display defaultDisplay = this.bk.getWindowManager().getDefaultDisplay();
        if (this.vN.width < 0) {
            this.vN.width = defaultDisplay.getWidth();
        }
        if (this.vN.height < 0) {
            this.vN.height = defaultDisplay.getHeight();
        }
        return this.vN;
    }

    /* access modifiers changed from: package-private */
    public void he() {
        String bN;
        if (this.vK != null) {
            switch (this.vK.bH()) {
                case 0:
                    bN = wY;
                    break;
                case 1:
                    bN = wZ;
                    break;
                case 2:
                    bN = xa;
                    break;
                case 3:
                    bN = this.vK.bN();
                    this.bk.moveTaskToBack(true);
                    break;
                default:
                    return;
            }
            if (bN != null && bN.length() >= 3) {
                Intent intent = new Intent(bN);
                intent.setFlags(536870912);
                try {
                    this.bk.startActivity(intent);
                } catch (ActivityNotFoundException e) {
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.b(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, int):int
      com.uc.browser.ModelBrowser.b(android.content.Context, int):android.app.Dialog
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, android.content.Context):android.app.Dialog
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, boolean):boolean
      com.uc.browser.ModelBrowser.b(boolean, boolean):void
      com.uc.browser.ModelBrowser.b(android.view.View, boolean):void */
    /* access modifiers changed from: package-private */
    public void hf() {
        if (this.vK != null) {
            b(this.vK.getCurrentView(), true);
        }
    }

    /* access modifiers changed from: package-private */
    public void hg() {
        aO(1);
    }

    public MultiWindowManager hj() {
        if (this.vJ != null) {
            return this.vJ;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void hk() {
        hT();
        if (this.AA == null) {
            this.AA = new MultiWindowDialogEx(this.bk);
        }
        this.AA.show();
        if (this.vK != null) {
            this.vK.bu();
        }
    }

    public void hl() {
        if (this.AA != null && this.AA.isShowing()) {
            this.AA.dismiss();
        }
    }

    public void hm() {
        if (this.AA == null || !this.AA.isShowing()) {
            hk();
        } else {
            this.AA.dismiss();
        }
    }

    /* access modifiers changed from: package-private */
    public int hr() {
        int i = 0;
        if (this.vK == null) {
            return 0;
        }
        if (this.vK.canGoBack()) {
            i = 0 | 1;
        }
        return this.vK.canGoForward() ? i | 16 : i;
    }

    /* access modifiers changed from: package-private */
    public void hs() {
        int progress;
        Class<ViewMainpage> cls = ViewMainpage.class;
        ActivityBrowser.bqy = true;
        this.bk.getWindow().setFlags(1024, 3072);
        this.vJ.h(true);
        Class<ViewMainpage> cls2 = ViewMainpage.class;
        if (this.bi.getClass() != cls) {
            hy();
        }
        if (!(this.vU == null || this.vU.getParent() == null)) {
            ((ViewGroup) this.vU.getParent()).removeView(this.vU);
        }
        if (this.bi.getClass() != WebViewZoom.class) {
            this.vU = new ViewControlBarFullScreen(this.bk, false);
        } else {
            this.vU = new ViewControlBarFullScreen(this.bk, true);
        }
        this.vU.gd(this.vT.Fm());
        e(this.bi);
        if (((this.vU != null) && (!e.nR().nY().bw(h.aeZ).contains(e.RD))) && this.wc != null && (progress = this.wc.getProgress()) > 0 && progress < 96) {
            this.wc.setVisibility(0);
        }
        if (this.vT != null) {
            this.vT.bQ(true);
        }
        if (this.vK != null && true == this.vK.bk()) {
            this.vV = new RelativeLayout.LayoutParams(-1, -1);
            this.vV.addRule(12);
            this.vU.setLayoutParams(this.vV);
            hn();
        }
        Class<ViewMainpage> cls3 = ViewMainpage.class;
        if (this.bi.getClass() != cls) {
            this.vM.addView(this.vU);
            this.vU.bringToFront();
        }
        if (this.wJ != null) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.wJ.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            }
            layoutParams.bottomMargin = ((int) this.bk.getResources().getDimension(R.dimen.f350zoomcontrol_padding_bottom)) + ((int) this.bk.getResources().getDimension(R.dimen.f178controlbar_fullscreen_height));
            layoutParams.addRule(12);
            this.wJ.setLayoutParams(layoutParams);
            this.wJ.bringToFront();
        }
        this.vJ.aT();
        Class<ViewMainpage> cls4 = ViewMainpage.class;
        if (this.bi.getClass() != cls) {
            this.vU.d(canGoBack(), bo());
            this.vU.Q(canGoForward());
        }
        e.nR().nY().w(h.afK, e.RD);
        PreferenceManager.getDefaultSharedPreferences(this.bk).edit().putBoolean(vA, true).commit();
        boolean a2 = this.vK.a(this.vK.getCurrentView());
        ViewMainBar.P(a2);
        if (this.vU != null) {
            this.vU.P(a2);
        }
        ha();
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.bk);
        int i = defaultSharedPreferences.getInt(xl, 0);
        if (i < 3 && this.bi != this.vC && (this.bi instanceof WebViewJUC) && !((WebViewJUC) this.bi).ac()) {
            Toast.makeText(this.bk, this.bk.getString(R.string.f1204fs_fordward_backward_tips), 0).show();
            defaultSharedPreferences.edit().putInt(xl, i + 1).commit();
        }
    }

    /* access modifiers changed from: protected */
    public void ht() {
        RelativeLayout.LayoutParams layoutParams;
        Class<ViewMainpage> cls = ViewMainpage.class;
        ActivityBrowser.bqy = false;
        this.bk.getWindow().setFlags(2048, 3072);
        this.vJ.h(false);
        this.vT.bQ(false);
        if (this.vU != null) {
            if (this.vM != null) {
                this.vM.removeView(this.vU);
            }
            this.vU.destroyDrawingCache();
            this.vU = null;
            this.vV = null;
        }
        if (this.wc != null) {
            this.wc.setVisibility(8);
        }
        e(this.bi);
        if (!(this.wJ == null || (layoutParams = (RelativeLayout.LayoutParams) this.wJ.getLayoutParams()) == null)) {
            layoutParams.bottomMargin = (int) this.bk.getResources().getDimension(R.dimen.f350zoomcontrol_padding_bottom);
            layoutParams.addRule(12);
            this.wJ.setLayoutParams(layoutParams);
            this.wJ.bringToFront();
        }
        this.vJ.aT();
        Class<ViewMainpage> cls2 = ViewMainpage.class;
        if (this.bi.getClass() != cls) {
            hz();
        }
        e.nR().nY().w(h.afK, "0");
        PreferenceManager.getDefaultSharedPreferences(this.bk).edit().putBoolean(vA, false).commit();
        Class<ViewMainpage> cls3 = ViewMainpage.class;
        if (this.bi.getClass() != cls) {
            this.vP.findViewById(R.id.f699controlbar_layout).setVisibility(0);
        }
        hb();
    }

    /* access modifiers changed from: protected */
    public void hv() {
    }

    public boolean hw() {
        return this.wh;
    }

    public void hx() {
        if (this.wg != null && this.wg.isHeld()) {
            this.mHandler.removeMessages(25);
            this.wg.release();
        }
    }

    public void hy() {
        this.vO.setVisibility(8);
    }

    public void hz() {
        if (this.vO == null) {
            return;
        }
        if (ActivityBrowser.bqy || this.vO.getChildCount() != 0) {
            this.vO.setVisibility(8);
            return;
        }
        this.vO.setVisibility(0);
        this.vO.removeView(this.vQ);
        this.vO.addView(this.vQ);
    }

    public void i(byte b2) {
        if (Integer.valueOf(e.nR().bw(h.afD)).intValue() == 0 && b2 == -126) {
            if (this.wa != null) {
                this.wa.setVisibility(0);
                if (ActivityBrowser.Fz()) {
                    this.wa.getDrawable().setAlpha(128);
                }
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.wa.getLayoutParams();
                if (layoutParams != null) {
                    if (this.AU == null || this.AU.getVisibility() != 0) {
                        layoutParams.addRule(11);
                        this.wa.setLayoutParams(layoutParams);
                    } else {
                        layoutParams.addRule(11, 0);
                        this.wa.setLayoutParams(layoutParams);
                    }
                }
                if (this.vZ != null && this.vZ.getVisibility() == 0) {
                    RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.vZ.getLayoutParams();
                    layoutParams2.addRule(11, 0);
                    this.vZ.setLayoutParams(layoutParams2);
                }
            }
        } else if (this.wa != null) {
            this.wa.setVisibility(8);
            if (this.vZ != null && this.vZ.getVisibility() == 0) {
                RelativeLayout.LayoutParams layoutParams3 = (RelativeLayout.LayoutParams) this.vZ.getLayoutParams();
                layoutParams3.addRule(11);
                this.vZ.setLayoutParams(layoutParams3);
            }
        }
    }

    public boolean iA() {
        if (e.nR() != null) {
            return e.nR().oP();
        }
        return false;
    }

    public boolean iB() {
        if (!(this.bi instanceof WebViewJUC)) {
            return false;
        }
        WebViewJUC webViewJUC = (WebViewJUC) this.bi;
        return webViewJUC.M() || (webViewJUC.p() == 1 && webViewJUC.ao());
    }

    public void iC() {
        RelativeLayout.LayoutParams layoutParams;
        if (!(this.AU == null || this.AU.getParent() == null)) {
            this.AU.setVisibility(8);
        }
        if (this.wa != null && this.wa.getVisibility() == 0) {
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.wa.getLayoutParams();
            if (layoutParams2 != null) {
                layoutParams2.addRule(11);
                this.wa.setLayoutParams(layoutParams2);
            }
        } else if (this.vZ != null && this.vZ.getVisibility() == 0 && (layoutParams = (RelativeLayout.LayoutParams) this.vZ.getLayoutParams()) != null) {
            layoutParams.addRule(11);
            this.vZ.setLayoutParams(layoutParams);
        }
    }

    public void iD() {
        final AnonymousClass64 r1 = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case 0:
                        if (ModelBrowser.this.AV != null) {
                            ModelBrowser.this.AV.setEnabled(true);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        };
        if (this.vP != null) {
            this.AU = this.vP.findViewById(R.id.f696entrance_container);
            this.AV = (Button) this.vP.findViewById(R.id.f697browser_readmode_entrance);
            this.AV.setOnClickListener(new View.OnClickListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object, int):void
                 arg types: [com.uc.browser.ModelBrowser, boolean, int]
                 candidates:
                  com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String, java.lang.String):void
                  com.uc.browser.ModelBrowser.a(java.lang.String, java.util.HashMap, int):void
                  com.uc.browser.ModelBrowser.a(int, int, long):void
                  com.uc.browser.ModelBrowser.a(int, int, java.lang.Object):void
                  com.uc.browser.ModelBrowser.a(int, java.lang.String, boolean):void
                  com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, java.lang.String, java.lang.String):void
                  com.uc.browser.ModelBrowser.a(com.uc.browser.WebViewJUC, int, int):void
                  com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, com.uc.browser.WindowUCWeb, int):void
                  com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object, int):void */
                public void onClick(View view) {
                    ModelBrowser.this.a((Object) false, 0);
                    ModelBrowser.this.AV.setEnabled(false);
                    r1.sendMessageDelayed(r1.obtainMessage(0), 2000);
                    SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ModelBrowser.this.bk);
                    if (ModelBrowser.this.iA()) {
                        int i = defaultSharedPreferences.getInt(ModelBrowser.Ba, 0);
                        ModelBrowser.this.vJ.J();
                        if (e.nR() != null) {
                            e.nR().ad(false);
                        }
                        Toast.makeText(ModelBrowser.this.bk, i < 3 ? R.string.f1035readingmode_tips_saved : R.string.f1034readingmode_tips_quit, 0).show();
                        defaultSharedPreferences.edit().putInt(ModelBrowser.Ba, i + 1).commit();
                        return;
                    }
                    ModelBrowser.this.vJ.I();
                    if (e.nR() != null) {
                        e.nR().ad(true);
                    }
                    ModelBrowser.gD().a(74, (Object) null);
                    ModelBrowser.this.hi();
                    int i2 = defaultSharedPreferences.getInt(ModelBrowser.AZ, 0);
                    Toast.makeText(ModelBrowser.this.bk, (int) R.string.f1033readingmode_tips_enter, 0).show();
                    defaultSharedPreferences.edit().putInt(ModelBrowser.AZ, i2 + 1).commit();
                }
            });
        }
    }

    public boolean iE() {
        if (this.bi instanceof WebViewJUC) {
            return ((WebViewJUC) this.bi).M();
        }
        return false;
    }

    public boolean iF() {
        if (!(this.bi instanceof WebViewJUC)) {
            return false;
        }
        WebViewJUC webViewJUC = (WebViewJUC) this.bi;
        return !webViewJUC.zF() && !webViewJUC.zH();
    }

    public boolean iG() {
        if (this.bi instanceof WebViewJUC) {
            return !((WebViewJUC) this.bi).at();
        }
        return false;
    }

    public void iH() {
        PreferenceManager.getDefaultSharedPreferences(this.bk).edit().putBoolean(Bc, false).commit();
    }

    public Dialog iI() {
        final Dialog dialog = new Dialog(this.bk, R.style.f1575context_menu);
        dialog.requestWindowFeature(1);
        View inflate = LayoutInflater.from(this.bk).inflate((int) R.layout.f932readingmode_edu_dlg, (ViewGroup) null);
        boolean Fz = ActivityBrowser.Fz();
        TextView textView = (TextView) inflate.findViewById(R.id.title);
        View findViewById = inflate.findViewById(R.id.f831seprator);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.f830arrow);
        TextView textView2 = (TextView) inflate.findViewById(R.id.f832smooth_title);
        TextView textView3 = (TextView) inflate.findViewById(R.id.f833smooth_discribe);
        TextView textView4 = (TextView) inflate.findViewById(R.id.f834record_title);
        TextView textView5 = (TextView) inflate.findViewById(R.id.f835record_discribe);
        if (Fz) {
            textView.setTextColor(-9667958);
            textView2.setTextColor(-9667958);
            textView2.getCompoundDrawables()[0].setAlpha(128);
            textView3.setTextColor(-9667958);
            textView4.setTextColor(-9667958);
            textView4.getCompoundDrawables()[0].setAlpha(128);
            textView5.setTextColor(-9667958);
            findViewById.setBackgroundColor(-12234402);
            imageView.getDrawable().setAlpha(128);
        } else {
            textView.setTextColor(-1);
            textView2.setTextColor(-1);
            textView3.setTextColor(-1);
            textView4.setTextColor(-1);
            textView5.setTextColor(-1);
            findViewById.setBackgroundColor(-9800318);
        }
        inflate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.bk);
        boolean z = defaultSharedPreferences.getBoolean(Bc, false);
        dialog.setContentView(inflate);
        if (!z) {
            dialog.show();
            defaultSharedPreferences.edit().putBoolean(Bc, true).commit();
        }
        return dialog;
    }

    public void iJ() {
        i((byte) this.vK.bO());
    }

    public void iK() {
        d(this.vK.bO(), this.vK.getUrl());
    }

    public void iL() {
        switch (b.b.a.OS()) {
            case 1:
                b.a.a.f.l(1, b.a.a.f.atI);
                return;
            case 2:
                b.a.a.f.l(1, b.a.a.f.atF);
                return;
            case 3:
                b.a.a.f.l(1, b.a.a.f.atE);
                return;
            case 4:
                b.a.a.f.l(1, b.a.a.f.atH);
                return;
            case 5:
                b.a.a.f.l(1, b.a.a.f.atG);
                return;
            case 6:
                b.a.a.f.l(1, b.a.a.f.atJ);
                return;
            default:
                return;
        }
    }

    public WebViewJUC ia() {
        if (this.bi == null) {
            return null;
        }
        if (this.bi.getClass() == ViewMainpage.class) {
            return ((ViewMainpage) this.bi).cz();
        }
        if (this.bi.getClass() == WebViewJUC.class) {
            return (WebViewJUC) this.bi;
        }
        return null;
    }

    public void ib() {
        stopLoading();
        Bundle bundle = new Bundle();
        bundle.putString("networkcheck", "networkcheck");
        Intent intent = new Intent(this.bk, ActivityInitial.class);
        intent.putExtras(bundle);
        this.bk.startActivity(intent);
        b.a.a.f.l(0, b.a.a.f.asZ);
    }

    public void ic() {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this.bk);
        this.wD = 1;
        builder.aH(R.string.f1190dialog_title_upload);
        builder.a((int) R.array.f55camera_upload, this.wD, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                int unused = ModelBrowser.this.wD = i;
            }
        });
        builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            long Sg;

            public void onClick(DialogInterface dialogInterface, int i) {
                if (System.currentTimeMillis() - this.Sg >= 500) {
                    this.Sg = System.currentTimeMillis();
                    if (1 == ModelBrowser.this.wD) {
                        ModelBrowser.this.bk.startActivityForResult(new Intent(ModelBrowser.this.bk.getBaseContext(), ActivityCamera.class), 5);
                        return;
                    }
                    Intent intent = new Intent(ModelBrowser.this.bk, ActivityChooseFile.class);
                    intent.putExtra(ActivityChooseFile.uv, 1);
                    ModelBrowser.this.bk.startActivityForResult(intent, 6);
                }
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.fh().show();
    }

    public String id() {
        if (this.vK == null) {
            return null;
        }
        return this.vK.getTitle();
    }

    public String ie() {
        if (this.vK == null) {
            return null;
        }
        return this.vK.getUrl();
    }

    /* renamed from: if  reason: not valid java name */
    public int m1if() {
        if (this.vK == null) {
            return -1;
        }
        return this.vK.p();
    }

    public int ig() {
        if (this.vK == null) {
            return -1;
        }
        return this.vK.q();
    }

    public boolean ih() {
        return this.wL;
    }

    /* access modifiers changed from: protected */
    public void ii() {
        e.Ro = false;
        ActivityBrowser.bqO = true;
        if (this.vT != null) {
            this.vT.dismiss();
        }
        if (this.bk != null) {
            this.bk.finish();
        }
        if (!this.AQ.isAlive()) {
            this.AQ.start();
        }
    }

    public void ij() {
    }

    public void ik() {
        int intValue = Integer.valueOf(e.nR().bw(h.afB)).intValue();
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this.bk);
        builder.L(this.bk.getResources().getString(R.string.f1064menu_land_state));
        builder.a(this.bk.getResources().getStringArray(R.array.f48pref_land_mode), intValue, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i >= 0 && i <= 2) {
                    e.nR().w(h.afB, String.valueOf(i));
                    ActivityBrowser.d((Activity) ModelBrowser.this.bk);
                }
                if (dialogInterface != null) {
                    dialogInterface.cancel();
                }
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        UCAlertDialog fh = builder.fh();
        if (fh != null) {
            fh.show();
        }
    }

    public void il() {
        int i = 0;
        try {
            i = Integer.parseInt(e.nR().nY().bw(h.afq));
        } catch (Exception e) {
        }
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this.bk);
        builder.L(this.bk.getResources().getString(R.string.f1196dialog_title_browse_model));
        builder.a(this.bk.getResources().getStringArray(R.array.f58browser_model), i, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == 0) {
                    e.nR().nY().w(h.afq, String.valueOf(0));
                    g.Jn().ik(0);
                    b.a.a.f.l(0, b.a.a.f.ate);
                } else if (1 == i) {
                    e.nR().nY().w(h.afq, String.valueOf(1));
                    g.Jn().ik(1);
                } else if (2 == i) {
                    e.nR().nY().w(h.afq, String.valueOf(2));
                    g.Jn().ik(2);
                    b.a.a.f.l(0, b.a.a.f.atd);
                }
                ModelBrowser.this.aS(66);
                if (dialogInterface != null) {
                    dialogInterface.cancel();
                }
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        UCAlertDialog fh = builder.fh();
        if (fh != null) {
            fh.show();
        }
    }

    public void im() {
        if (this.wJ == null) {
            this.wJ = new ViewZoomControls(this.bk);
            this.wJ.setVisibility(8);
        }
        try {
            if (this.vM != null && this.wJ != null) {
                this.vM.addView(this.wJ);
                this.wJ.bringToFront();
            }
        } catch (Exception e) {
        }
    }

    public void in() {
        E(false);
    }

    public boolean io() {
        if (this.vT != null) {
            return !this.vT.IC();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void ip() {
        if (e.nR().os()) {
            ir();
        } else if (e.nR().bw(h.afQ).equals(e.RD)) {
            iq();
        } else {
            ii();
        }
    }

    public void iq() {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this.bk, true);
        builder.L(this.bk.getResources().getString(R.string.f1472quit_tip));
        View inflate = this.bk.getLayoutInflater().inflate((int) R.layout.f930quit_dialog, (ViewGroup) null);
        if (cb.MA().MF()) {
            ((TextView) inflate.findViewById(R.id.f826msg2)).setText(cb.MA().ME());
        } else {
            inflate.findViewById(R.id.f824ucsave).setVisibility(8);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) inflate.findViewById(R.id.f828clear_all).getLayoutParams();
            if (layoutParams != null) {
                layoutParams.addRule(3, R.id.f823msg);
            }
        }
        ((TextView) inflate.findViewById(R.id.f823msg)).setTextColor(com.uc.h.e.Pb().getColor(78));
        final CheckBox checkBox = (CheckBox) inflate.findViewById(R.id.f828clear_all);
        checkBox.setTextColor(com.uc.h.e.Pb().getColor(78));
        checkBox.setChecked(false);
        checkBox.setButtonDrawable(com.uc.h.e.Pb().getDrawable(UCR.drawable.aWV));
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            }
        });
        final CheckBox checkBox2 = (CheckBox) inflate.findViewById(R.id.f829dont_tips);
        checkBox2.setTextColor(com.uc.h.e.Pb().getColor(78));
        checkBox2.setButtonDrawable(com.uc.h.e.Pb().getDrawable(UCR.drawable.aWV));
        builder.c(inflate);
        builder.a((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                boolean unused = ModelBrowser.this.AR = checkBox.isChecked();
                if (checkBox2.isChecked()) {
                    e.nR().w(h.afQ, "0");
                }
                ModelBrowser.this.ii();
            }
        });
        builder.b((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.fh().show();
    }

    public void ir() {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this.bk, true);
        builder.aH(R.string.f1472quit_tip);
        builder.aG(R.string.f1417quit_note_download);
        builder.a((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ModelBrowser.this.ii();
            }
        });
        builder.b((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.fh().show();
    }

    public f is() {
        try {
            return f.a(this.bk.getResources(), R.drawable.f544blank);
        } catch (Exception | OutOfMemoryError e) {
            return null;
        }
    }

    public synchronized void ix() {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.bk);
        int i = defaultSharedPreferences.getInt(AT, 0);
        if (i < 5) {
            Toast.makeText(this.bk, (int) R.string.f1522preread_note, 0).show();
            defaultSharedPreferences.edit().putInt(AT, i + 1).commit();
        }
    }

    public void iy() {
        F(false);
    }

    public void iz() {
        if (!iA()) {
            if (iE()) {
                this.vJ.J();
            }
            F(false);
        } else if (iB() && !iE()) {
            this.vJ.I();
        }
    }

    public void j(Object obj) {
        try {
            int intValue = ((Integer) obj).intValue();
            WebViewJUC ia = ia();
            if (ia != null) {
                ia.dZ(intValue);
            } else if (this.bi != null && this.bi.getClass() == WebViewZoom.class) {
                ((WebViewZoom) this.bi).zE();
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0016  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void k(java.lang.String r3, java.lang.String r4) {
        /*
            r2 = this;
            if (r3 != 0) goto L_0x0042
            android.view.View r0 = r2.bi
            if (r0 == 0) goto L_0x0033
            android.view.View r0 = r2.bi
            boolean r0 = r0 instanceof com.uc.browser.WebViewJUC
            if (r0 == 0) goto L_0x0033
            android.view.View r0 = r2.bi
            com.uc.browser.WebViewJUC r0 = (com.uc.browser.WebViewJUC) r0
            java.lang.String r0 = r0.getUrl()
        L_0x0014:
            if (r0 != 0) goto L_0x0018
            java.lang.String r0 = ""
        L_0x0018:
            com.uc.a.e r1 = com.uc.a.e.nR()
            java.lang.String r0 = r1.bF(r0)
            com.uc.browser.MenuDialog r1 = r2.vT
            if (r1 == 0) goto L_0x0029
            com.uc.browser.MenuDialog r1 = r2.vT
            r1.ff(r0)
        L_0x0029:
            com.uc.browser.ViewControlBarFullScreen r1 = r2.vU
            if (r1 == 0) goto L_0x0032
            com.uc.browser.ViewControlBarFullScreen r1 = r2.vU
            r1.gd(r0)
        L_0x0032:
            return
        L_0x0033:
            android.view.View r0 = r2.bi
            boolean r0 = r0 instanceof com.uc.browser.WebViewZoom
            if (r0 == 0) goto L_0x0042
            android.view.View r0 = r2.bi
            com.uc.browser.WebViewZoom r0 = (com.uc.browser.WebViewZoom) r0
            java.lang.String r0 = r0.getUrl()
            goto L_0x0014
        L_0x0042:
            r0 = r3
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.browser.ModelBrowser.k(java.lang.String, java.lang.String):void");
    }

    public void l(Object obj) {
        int i;
        if (obj != null) {
            try {
                gV();
                Bundle bundle = (Bundle) obj;
                String string = bundle.getString("x");
                String string2 = bundle.getString("y");
                String string3 = bundle.getString("width");
                String string4 = bundle.getString("height");
                String string5 = bundle.getString("single");
                String string6 = bundle.getString("text");
                String string7 = bundle.getString("textsize");
                String string8 = bundle.getString("backgroundColor");
                String string9 = bundle.getString("textColor");
                boolean z = bundle.getBoolean("password");
                int i2 = bundle.getInt("maxlength");
                if (string != null && string2 != null && string3 != null && string4 != null && string5 != null) {
                    int parseInt = Integer.parseInt(string);
                    int parseInt2 = Integer.parseInt(string2);
                    int parseInt3 = Integer.parseInt(string3);
                    int parseInt4 = Integer.parseInt(string4);
                    int parseInt5 = Integer.parseInt(string5);
                    int parseInt6 = Integer.parseInt(string8);
                    int parseInt7 = Integer.parseInt(string9);
                    boolean z2 = parseInt5 == 1;
                    int i3 = 16;
                    if (string7 != null) {
                        try {
                            i3 = Integer.parseInt(string7);
                        } catch (Exception e) {
                            i = e.nR().getTextSize();
                        }
                    }
                    i = i3;
                    if ((this.bi.getClass() == ViewMainpage.class || this.bi.getClass() == WebViewJUC.class) && this.wj != null) {
                        this.wj.a(parseInt, parseInt2, parseInt3, parseInt4, z2, (float) i, string6, 0, 2, z, i2, parseInt6, parseInt7);
                        hR();
                    }
                }
            } catch (Exception e2) {
            }
        }
    }

    public void l(String str, String str2) {
        Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + str));
        intent.putExtra("sms_body", str2);
        this.bk.startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void m(Object obj) {
        if (obj != null) {
            Object[] objArr = (Object[]) obj;
            if (objArr.length >= 4) {
                int intValue = ((Integer) objArr[0]).intValue();
                int intValue2 = ((Integer) objArr[1]).intValue();
                String str = (String) objArr[2];
                boolean booleanValue = ((Boolean) objArr[3]).booleanValue();
                if (System.currentTimeMillis() - this.AO >= 500) {
                    this.AO = System.currentTimeMillis();
                    if (1 == intValue) {
                        if (UcCamera.wu()) {
                            Intent intent = new Intent(this.bk.getBaseContext(), ActivityCamera.class);
                            intent.putExtra("file_maxlength", intValue2);
                            this.bk.startActivityForResult(intent, 3);
                            return;
                        }
                        Intent intent2 = new Intent(this.bk, ActivityChooseFile.class);
                        intent2.putExtra(ActivityChooseFile.uv, 1);
                        intent2.putExtra("file_maxlength", intValue2);
                        intent2.putExtra(ActivityChooseFile.uy, false);
                        intent2.putExtra(ActivityChooseFile.ux, str);
                        this.bk.startActivityForResult(intent2, 2);
                    } else if (ia() == null) {
                    } else {
                        if (UcCamera.wu() || booleanValue) {
                            a(intValue2, str, booleanValue);
                            return;
                        }
                        Intent intent3 = new Intent(this.bk, ActivityChooseFile.class);
                        intent3.putExtra(ActivityChooseFile.uv, 1);
                        intent3.putExtra("file_maxlength", intValue2);
                        intent3.putExtra(ActivityChooseFile.ux, str);
                        intent3.putExtra(ActivityChooseFile.uy, false);
                        this.bk.startActivityForResult(intent3, 2);
                    }
                }
            }
        }
    }

    public void m(boolean z) {
        if (this.vK != null) {
            this.vK.m(z);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        Bundle extras;
        byte[] byteArray;
        Bundle extras2;
        Bundle extras3;
        Bundle extras4;
        switch (i) {
            case 0:
                if (-1 == i2 && intent != null && (extras4 = intent.getExtras()) != null) {
                    String string = extras4.getString(b.URL);
                    boolean z = extras4.getBoolean("IS_NEW_WINDOW");
                    boolean z2 = extras4.getBoolean("IS_HOME_WINDOW");
                    boolean z3 = extras4.getBoolean("IS_NEW_WINDOW_SEARCH");
                    if (string == null) {
                        return;
                    }
                    if (string.startsWith("ext:lp:lp_favor")) {
                        aS(49);
                        return;
                    } else if (true == z2) {
                        if (!(this.bi == null || this.bi.getClass() == ViewMainpage.class)) {
                            hg();
                        }
                        if (this.vK != null) {
                            this.vK.m(string);
                            return;
                        }
                        return;
                    } else if (!z && !z3) {
                        X(string);
                        return;
                    } else if (true != z3 || this.vJ.wg()) {
                        this.vJ.cJ(string);
                        return;
                    } else {
                        X(string);
                        return;
                    }
                } else {
                    return;
                }
            case 1:
                if (this.vK != null) {
                    this.vK.bv();
                    if (-1 == i2 && intent != null && (extras3 = intent.getExtras()) != null) {
                        String string2 = extras3.getString(b.URL);
                        boolean z4 = extras3.getBoolean("IS_NEW_WINDOW");
                        boolean z5 = extras3.getBoolean("IS_HOME_WINDOW");
                        if (string2 == null) {
                            return;
                        }
                        if (true == z5) {
                            this.vK.m(string2);
                            return;
                        } else if (!z4) {
                            X(string2);
                            return;
                        } else {
                            this.vJ.cJ(string2);
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 2:
                if (!(intent == null || (extras2 = intent.getExtras()) == null)) {
                    a(extras2.getString(ActivityChooseFile.uE), extras2.getString(ActivityChooseFile.uF));
                }
                WebViewJUC ia = ia();
                if (ia != null) {
                    ia.ap();
                    return;
                }
                return;
            case 3:
                if (-1 == i2) {
                    if (intent != null) {
                        try {
                            Bundle extras5 = intent.getExtras();
                            if (!(extras5 == null || (byteArray = extras5.getByteArray("buffer")) == null)) {
                                WebViewJUC ia2 = ia();
                                if (ia2 != null) {
                                    ia2.a(byteArray);
                                } else {
                                    return;
                                }
                            }
                        } catch (Exception e) {
                            Toast.makeText(this.bk, (int) R.string.f1447camera_fail, 1).show();
                        }
                    }
                } else if (i2 != 0) {
                    Toast.makeText(this.bk, (int) R.string.f1447camera_fail, 1).show();
                }
                WebViewJUC ia3 = ia();
                if (ia3 != null) {
                    ia3.ap();
                    return;
                }
                return;
            case 4:
                if (-1 == i2 && intent != null && (extras = intent.getExtras()) != null) {
                    int i3 = extras.getInt(ActivitySelector.aSI);
                    try {
                        String qa = e.nR().qa();
                        if (qa != null && qa.length() > 0 && !qa.startsWith(w.Os)) {
                            e.nR().t((byte) i3);
                            X(b.acz);
                            String lh = e.nR().lh();
                            if (lh != null && lh.length() > 0) {
                                b.a.a.f.l(2, lh);
                                return;
                            }
                            return;
                        }
                        return;
                    } catch (Exception e2) {
                        return;
                    }
                } else {
                    return;
                }
            case 5:
                if (-1 == i2) {
                    if (this.vK.getCurrentView() instanceof WebViewZoom) {
                        WebViewZoom webViewZoom = (WebViewZoom) this.vK.getCurrentView();
                        if (webViewZoom.zJ() != null) {
                            webViewZoom.zJ().setType(2);
                            webViewZoom.zJ().processPicData(intent);
                            return;
                        }
                        return;
                    }
                    return;
                } else if (i2 != 0) {
                    Toast.makeText(this.bk, (int) R.string.f1447camera_fail, 1).show();
                    return;
                } else {
                    return;
                }
            case 6:
                if (this.vK.getCurrentView() instanceof WebViewZoom) {
                    WebViewZoom webViewZoom2 = (WebViewZoom) this.vK.getCurrentView();
                    if (webViewZoom2.zJ() != null) {
                        webViewZoom2.zJ().setType(1);
                        webViewZoom2.zJ().processFileData(intent);
                        return;
                    }
                    return;
                }
                return;
            case 7:
                this.vC.eq.onActivityResult(7, 0, intent);
                return;
            case 8:
                if (-1 == i2) {
                    hg();
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(int, int, java.lang.Object):void
     arg types: [int, int, boolean]
     candidates:
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.util.HashMap, int):void
      com.uc.browser.ModelBrowser.a(int, int, long):void
      com.uc.browser.ModelBrowser.a(int, java.lang.String, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WebViewJUC, int, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, com.uc.browser.WindowUCWeb, int):void
      com.uc.browser.ModelBrowser.a(int, int, java.lang.Object):void */
    public void onConfigurationChanged(Configuration configuration) {
        boolean z;
        if (gD() != null) {
            gD().a(83, 0, (Object) false);
        }
        hc();
        hS();
        if (this.vJ != null) {
            this.vJ.aT();
        }
        try {
            z = this.vK.a(this.vK.getCurrentView());
        } catch (NullPointerException e) {
            z = false;
        }
        if (this.vT != null) {
            this.vT.IQ();
            this.vT.dr();
        }
        if (this.vH == null) {
            gQ();
        }
        this.vH.a(configuration);
        if (this.vG == null) {
            gP();
        }
        this.vG.a(configuration);
        if (!(this.vC == null || this.vC.cD() == null)) {
            this.vC.cD().dr();
        }
        if (!(this.vO == null || this.vQ == null)) {
            this.vO.removeAllViews();
            hn();
            this.vD.kp(R.dimen.f177controlbar_height);
            new RelativeLayout.LayoutParams(-1, -1);
            this.vO.addView(this.vQ);
            if (this.vU != null) {
                this.vM.removeView(this.vU);
                String QT = this.vU.QT();
                this.vV = (RelativeLayout.LayoutParams) this.vU.getLayoutParams();
                this.vU.removeAllViews();
                this.vU.destroyDrawingCache();
                this.vU = new ViewControlBarFullScreen(this.bk, false);
                this.vU.setVisibility(0);
                this.vU.gd(QT);
                this.vT.bQ(true);
                if (this.vV == null) {
                    this.vV = new RelativeLayout.LayoutParams(-1, -2);
                }
                this.vM.addView(this.vU, this.vV);
                this.vU.d(canGoBack(), bo());
                this.vU.Q(canGoForward());
                this.vU.cW(this.vK.bk());
                hy();
                ViewMainBar.P(z);
            }
        }
        if (this.vU != null) {
            this.vU.P(z);
        }
        System.gc();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
     arg types: [int, int]
     candidates:
      com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
      com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
      com.uc.browser.ModelBrowser.a(int, long):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
      com.uc.browser.ModelBrowser.a(int, java.lang.Object):void */
    public boolean onContextItemSelected(MenuItem menuItem) {
        String y;
        String cE;
        String cE2;
        int itemId = menuItem.getItemId();
        if (this.bi instanceof WebViewZoom) {
            WebViewZoom webViewZoom = (WebViewZoom) this.bi;
            WebView.HitTestResult hitTestResult = webViewZoom.getHitTestResult();
            String extra = hitTestResult.getExtra();
            int type = hitTestResult.getType();
            HashMap hashMap = new HashMap();
            hashMap.put("webview", webViewZoom);
            Message obtainMessage = this.mHandler.obtainMessage(71, itemId, 0, hashMap);
            switch (itemId) {
                case com.uc.d.h.cdx /*655361*/:
                case com.uc.d.h.cdz /*655363*/:
                case com.uc.d.h.cdF /*655369*/:
                    webViewZoom.requestFocusNodeHref(obtainMessage);
                    break;
                case com.uc.d.h.cdy /*655362*/:
                    a(11, b.acH + extra);
                    break;
                case com.uc.d.h.cdA /*655364*/:
                    if (this.vJ != null) {
                        this.vJ.cJ(null);
                    }
                    b.a.a.f.l(0, b.a.a.f.ats);
                    break;
                case com.uc.d.h.cdB /*655365*/:
                    if (5 != type) {
                        webViewZoom.requestFocusNodeHref(obtainMessage);
                        break;
                    } else {
                        this.AN = extra;
                        Intent intent = new Intent();
                        intent.setAction(ActivityBookmarkEx.asd);
                        intent.putExtra(ActivityBookmarkEx.asf, this.vK.getTitle());
                        intent.putExtra(ActivityBookmarkEx.asg, this.vK.getUrl());
                        intent.putExtra(ActivityBookmarkEx.asi, this.vK.p());
                        intent.setClass(this.bk, ActivityBookmarkEx.class);
                        this.bk.startActivity(intent);
                        b.a.a.f.l(0, b.a.a.f.atr);
                        break;
                    }
                case com.uc.d.h.cdC /*655366*/:
                    if (5 != type) {
                        webViewZoom.requestFocusNodeHref(obtainMessage);
                        break;
                    } else {
                        this.AN = extra;
                        hu();
                        b.a.a.f.l(0, b.a.a.f.atw);
                        break;
                    }
                case com.uc.d.h.cdD /*655367*/:
                    webViewZoom.requestFocusNodeHref(obtainMessage);
                    String string = this.bk.getResources().getString(R.string.f957share_suffix);
                    this.AN = extra;
                    if (this.AN != null && this.AN.trim().length() != 0) {
                        String str = (String) obtainMessage.getData().get(ActivityEditMyNavi.bvj);
                        if (str == null || str.trim().length() == 0) {
                            str = "";
                        }
                        a(94, 0, new String[]{str + " " + this.AN + " " + string, str, this.AN});
                        break;
                    } else {
                        Toast.makeText(this.bk, (int) R.string.f961share_fail, 0).show();
                        return false;
                    }
                case com.uc.d.h.cdE /*655368*/:
                    hO();
                    b.a.a.f.l(0, b.a.a.f.atv);
                    break;
                case com.uc.d.h.cdH /*655371*/:
                    aS(81);
                    break;
                case com.uc.d.h.cdI /*655372*/:
                    if (extra != null) {
                        String[] strArr = new String[5];
                        strArr[0] = extra;
                        strArr[1] = webViewZoom.getUrl();
                        CookieSyncManager.createInstance(this.bk.getApplicationContext());
                        strArr[2] = CookieManager.getInstance().getCookie(extra);
                        if (strArr[2] == null || strArr[2].length() == 0) {
                            strArr[2] = CookieManager.getInstance().getCookie(webViewZoom.getUrl());
                        }
                        strArr[3] = URLUtil.guessFileName(extra, null, null);
                        strArr[4] = "down:webkit";
                        e(strArr);
                        break;
                    }
                    break;
            }
        } else if ((this.bi instanceof WebViewJUC) || this.bi == this.vC) {
            WebViewJUC cz = this.bi == this.vC ? ((ViewMainpage) this.bi).cz() : this.bi instanceof WebViewJUC ? (WebViewJUC) this.bi : null;
            if (cz != null || 589828 == itemId || 589832 == itemId) {
                switch (itemId) {
                    case com.uc.d.h.ccz /*262145*/:
                        if (!(this.vJ == null || (cE2 = this.vC.cE()) == null)) {
                            this.vJ.cJ(cE2);
                            break;
                        }
                    case com.uc.d.h.ccA /*262146*/:
                    case com.uc.d.h.cdk /*589828*/:
                    case com.uc.d.h.cdP /*720900*/:
                        if (this.vJ != null) {
                            this.vJ.cJ(null);
                        }
                        b.a.a.f.l(0, b.a.a.f.ats);
                        break;
                    case com.uc.d.h.ccB /*262148*/:
                        if (!(this.vJ == null || (cE = this.vC.cE()) == null)) {
                            this.vJ.cI(cE);
                            break;
                        }
                    case com.uc.d.h.ccC /*262152*/:
                    case com.uc.d.h.cdo /*589832*/:
                    case com.uc.d.h.cdM /*720897*/:
                        hO();
                        b.a.a.f.l(0, b.a.a.f.atv);
                        break;
                    case com.uc.d.h.cdh /*589825*/:
                        if (this.bi != null && this.bi.getClass() == WebViewJUC.class) {
                            iw().show();
                            break;
                        }
                    case com.uc.d.h.cdi /*589826*/:
                        cz.Qt();
                        b.a.a.f.l(0, b.a.a.f.aty);
                        break;
                    case com.uc.d.h.cdj /*589827*/:
                        if (this.vK == null || 1 == this.vK.p()) {
                            a(11, "ext:download");
                        } else {
                            a(11, "ext:download:ser:");
                        }
                        b.a.a.f.l(0, b.a.a.f.atx);
                        break;
                    case com.uc.d.h.cdl /*589829*/:
                        Intent intent2 = new Intent();
                        intent2.setAction(ActivityBookmarkEx.asd);
                        intent2.setClass(this.bk, ActivityBookmarkEx.class);
                        intent2.putExtra(ActivityBookmarkEx.asf, this.vK.getTitle());
                        intent2.putExtra(ActivityBookmarkEx.asg, this.vK.getUrl());
                        intent2.putExtra(ActivityBookmarkEx.asi, this.vK.p());
                        this.bk.startActivity(intent2);
                        b.a.a.f.l(0, b.a.a.f.atr);
                        break;
                    case com.uc.d.h.cdm /*589830*/:
                        hu();
                        b.a.a.f.l(0, b.a.a.f.atw);
                        break;
                    case com.uc.d.h.cdn /*589831*/:
                        switch (cz.t()) {
                            case 0:
                                Uri hJ = hJ();
                                if (hJ != null) {
                                    this.AN = cz.F();
                                    String D = cz.D();
                                    if (D == null || D.trim().length() == 0) {
                                    }
                                    a(94, 1, hJ);
                                    break;
                                } else {
                                    Toast.makeText(this.bk, (int) R.string.f962share_image_fail, 0).show();
                                    return false;
                                }
                            case 1:
                                String string2 = this.bk.getResources().getString(R.string.f957share_suffix);
                                this.AN = cz.y();
                                if (this.AN != null && this.AN.trim().length() != 0) {
                                    String D2 = cz.D();
                                    if (D2 == null || D2.trim().length() == 0) {
                                        D2 = "";
                                    }
                                    a(94, 0, new String[]{D2 + " " + this.AN + " " + string2, D2, this.AN});
                                    break;
                                } else {
                                    Toast.makeText(this.bk, (int) R.string.f961share_fail, 0).show();
                                    return false;
                                }
                            case 2:
                                String string3 = this.bk.getResources().getString(R.string.f957share_suffix);
                                this.AN = cz.getUrl();
                                String title = cz.getTitle();
                                if (this.AN != null && this.AN.trim().length() != 0) {
                                    if (title == null || title.trim().length() == 0) {
                                        title = "";
                                    }
                                    a(94, 0, new String[]{title + " " + this.AN + " " + string3, title, this.AN});
                                    break;
                                } else {
                                    Toast.makeText(this.bk, (int) R.string.f961share_fail, 0).show();
                                    return false;
                                }
                        }
                    case com.uc.d.h.cdp /*589833*/:
                    case com.uc.d.h.cdN /*720898*/:
                        if (!(this.vJ == null || (y = cz.y()) == null || y.length() == 0)) {
                            String str2 = b.acH + (true == cz.z() ? "wap:" + y : cz.O() ? "forceusejuc:" + y : y) + b.acv;
                            String Y = Y(str2);
                            if (Y != null) {
                                d(j(str2, Y));
                            } else {
                                this.vJ.cI(str2);
                            }
                        }
                        b.a.a.f.l(0, b.a.a.f.atu);
                        break;
                    case com.uc.d.h.cdq /*589834*/:
                    case com.uc.d.h.cdO /*720899*/:
                        if (this.bk != null) {
                            if (this.vG == null) {
                                gP();
                            }
                            this.vG.fw(cz.D());
                            b.a.a.f.l(0, b.a.a.f.atq);
                            break;
                        }
                        break;
                    case com.uc.d.h.cdr /*589835*/:
                        a(81, (Object) 0);
                        break;
                    case com.uc.d.h.cds /*589836*/:
                    case com.uc.d.h.cdQ /*720901*/:
                        String y2 = cz.y();
                        if (!(y2 == null || y2.length() == 0)) {
                            String str3 = b.acH + (true == cz.z() ? "wap:" + y2 : cz.O() ? "forceusejuc:" + y2 : y2) + b.acv;
                            String Y2 = Y(str3);
                            if (Y2 != null) {
                                d(j(str3, Y2));
                            } else {
                                this.vJ.cJ(str3);
                            }
                            b.a.a.f.l(0, b.a.a.f.att);
                            break;
                        }
                    case com.uc.d.h.cdt /*589837*/:
                        if (this.bi instanceof WebViewJUC) {
                            WebViewJUC webViewJUC = (WebViewJUC) this.bi;
                            if (webViewJUC != null) {
                                String F = webViewJUC.F();
                                if (F != null) {
                                    a(11, F);
                                    break;
                                }
                            } else {
                                return true;
                            }
                        }
                        break;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        int i;
        WebViewJUC webViewJUC;
        int type;
        try {
            if (view instanceof WebViewZoom) {
                WebView.HitTestResult hitTestResult = ((WebViewZoom) this.bi).getHitTestResult();
                if (hitTestResult != null && (type = hitTestResult.getType()) != 0 && 9 != type) {
                    String extra = hitTestResult.getExtra();
                    switch (type) {
                        case 5:
                        case 8:
                            com.uc.d.g.a(this.bk, com.uc.d.h.cdK, contextMenu);
                            contextMenu.findItem(com.uc.d.h.cdA).setVisible(false);
                            contextMenu.findItem(com.uc.d.h.cdD).setVisible(false);
                            break;
                        case 6:
                        default:
                            com.uc.d.g.a(this.bk, com.uc.d.h.cdL, contextMenu);
                            contextMenu.findItem(com.uc.d.h.cdD).setVisible(false);
                            break;
                        case 7:
                            com.uc.d.g.a(this.bk, com.uc.d.h.cdJ, contextMenu);
                            contextMenu.findItem(com.uc.d.h.cdA).setVisible(false);
                            if (id() != null && id().length() > 0 && ie() != null && ie().length() > 0 && !ie().startsWith(w.Os)) {
                                contextMenu.findItem(com.uc.d.h.cdD).setTitle(this.bk.getString(R.string.f958share_link));
                                contextMenu.findItem(com.uc.d.h.cdD).setVisible(true);
                                break;
                            }
                    }
                    if (5 == type) {
                        contextMenu.findItem(com.uc.d.h.cdz).setVisible(false);
                    }
                    if (!this.vJ.wg()) {
                        MenuItem findItem = contextMenu.findItem(com.uc.d.h.cdx);
                        if (findItem != null) {
                            findItem.setVisible(false);
                        }
                        MenuItem findItem2 = contextMenu.findItem(com.uc.d.h.cdF);
                        if (findItem2 != null) {
                            findItem2.setVisible(false);
                        }
                        MenuItem findItem3 = contextMenu.findItem(com.uc.d.h.cdA);
                        if (findItem3 != null) {
                            findItem3.setVisible(false);
                        }
                    }
                    if ((extra != null && extra.contains(w.Os)) || (this.vJ != null && !this.vJ.wg())) {
                        contextMenu.findItem(com.uc.d.h.cdx).setVisible(false);
                        contextMenu.findItem(com.uc.d.h.cdF).setVisible(false);
                    }
                    if (ah(extra) && this.vJ != null && this.vJ.wg()) {
                        contextMenu.findItem(com.uc.d.h.cdx).setVisible(true);
                        contextMenu.findItem(com.uc.d.h.cdF).setVisible(true);
                    }
                    if (1 >= this.vJ.wh()) {
                        contextMenu.findItem(com.uc.d.h.cdE).setVisible(false);
                    }
                }
            } else if (view instanceof WebViewJUCMainpage) {
                WebViewJUC webViewJUC2 = (WebViewJUC) view;
                int t = webViewJUC2.t();
                if (webViewJUC2.B()) {
                    switch (t) {
                        case 0:
                            if (!webViewJUC2.A()) {
                                return;
                            }
                            break;
                        case 1:
                            break;
                        default:
                            com.uc.d.g.a(this.bk, com.uc.d.h.cdR, contextMenu);
                            if (this.vJ != null && 1 >= this.vJ.wh()) {
                                contextMenu.findItem(com.uc.d.h.cdM).setVisible(false);
                            }
                            if (gD() != null && gD().hj() != null) {
                                if (gD().hj().wg()) {
                                    contextMenu.findItem(com.uc.d.h.cdP).setVisible(true);
                                    return;
                                } else {
                                    contextMenu.findItem(com.uc.d.h.cdP).setVisible(false);
                                    return;
                                }
                            } else {
                                return;
                            }
                    }
                    com.uc.d.g.a(this.bk, com.uc.d.h.cdS, contextMenu);
                    if (this.vJ != null && 1 >= this.vJ.wh()) {
                        contextMenu.findItem(com.uc.d.h.cdM).setVisible(false);
                    }
                    if (!webViewJUC2.A() || ((webViewJUC2.y() != null && webViewJUC2.y().startsWith(w.Os)) || !this.vJ.wg())) {
                        contextMenu.findItem(com.uc.d.h.cdQ).setVisible(false);
                        contextMenu.findItem(com.uc.d.h.cdN).setVisible(false);
                    }
                    if (webViewJUC2.A() && ah(webViewJUC2.y()) && this.vJ != null && this.vJ.wg()) {
                        contextMenu.findItem(com.uc.d.h.cdQ).setVisible(true);
                        contextMenu.findItem(com.uc.d.h.cdN).setVisible(true);
                    }
                    webViewJUC2.cR(true);
                }
            } else if (view instanceof WebViewJUC) {
                if (this.bi instanceof WebViewJUC) {
                    webViewJUC = (WebViewJUC) this.bi;
                    if (webViewJUC != null) {
                        i = webViewJUC.t();
                    } else {
                        return;
                    }
                } else {
                    i = 2;
                    webViewJUC = null;
                }
                if (webViewJUC != null && webViewJUC.B()) {
                    switch (i) {
                        case 0:
                            com.uc.d.g.a(this.bk, com.uc.d.h.cdu, contextMenu);
                            if (webViewJUC != null) {
                                if (webViewJUC.w()) {
                                    contextMenu.findItem(com.uc.d.h.cdh).setVisible(false);
                                }
                                if (1 == webViewJUC.p()) {
                                    contextMenu.findItem(com.uc.d.h.cdi).setVisible(false);
                                } else {
                                    contextMenu.findItem(com.uc.d.h.cdt).setVisible(false);
                                }
                            }
                            if (!webViewJUC.A()) {
                                contextMenu.findItem(com.uc.d.h.cdj).setVisible(false);
                            }
                            if (this.vJ != null && 1 >= this.vJ.wh()) {
                                contextMenu.findItem(com.uc.d.h.cdo).setVisible(false);
                            }
                            if (!(gD() == null || gD().hj() == null || gD().hj().wg())) {
                                contextMenu.findItem(com.uc.d.h.cdk).setVisible(false);
                            }
                            if (id() == null || id().length() <= 0 || ie() == null || ie().length() <= 0 || ie().startsWith(w.Os)) {
                                contextMenu.findItem(com.uc.d.h.cdn).setVisible(false);
                            } else {
                                contextMenu.findItem(com.uc.d.h.cdn).setVisible(true);
                                contextMenu.findItem(com.uc.d.h.cdn).setTitle(this.bk.getString(R.string.f960share_image));
                                contextMenu.findItem(com.uc.d.h.cdn).setVisible(true);
                                contextMenu.findItem(com.uc.d.h.cdk).setVisible(true);
                            }
                            webViewJUC.cR(true);
                            return;
                        case 1:
                            com.uc.d.g.a(this.bk, com.uc.d.h.cdv, contextMenu);
                            if (this.vJ != null && 1 >= this.vJ.wh()) {
                                contextMenu.findItem(com.uc.d.h.cdo).setVisible(false);
                            }
                            if (!webViewJUC.A() || ((webViewJUC.y() != null && webViewJUC.y().startsWith(w.Os)) || !this.vJ.wg())) {
                                contextMenu.findItem(com.uc.d.h.cds).setVisible(false);
                                contextMenu.findItem(com.uc.d.h.cdp).setVisible(false);
                            }
                            if (webViewJUC.A() && ah(webViewJUC.y()) && this.vJ != null && this.vJ.wg()) {
                                contextMenu.findItem(com.uc.d.h.cds).setVisible(true);
                                contextMenu.findItem(com.uc.d.h.cdp).setVisible(true);
                            }
                            if (true == hK()) {
                                contextMenu.findItem(com.uc.d.h.cdn).setTitle(this.bk.getString(R.string.f958share_link));
                                contextMenu.findItem(com.uc.d.h.cdn).setVisible(true);
                            } else {
                                contextMenu.findItem(com.uc.d.h.cdn).setVisible(false);
                            }
                            webViewJUC.cR(true);
                            return;
                        case 2:
                        case 3:
                            com.uc.d.g.a(this.bk, com.uc.d.h.cdw, contextMenu);
                            if (this.vJ != null && 1 >= this.vJ.wh()) {
                                contextMenu.findItem(com.uc.d.h.cdo).setVisible(false);
                            }
                            if (!(gD() == null || gD().hj() == null || gD().hj().wg())) {
                                contextMenu.findItem(com.uc.d.h.cdk).setVisible(false);
                            }
                            if ((webViewJUC != null && webViewJUC.ad()) || i == 3) {
                                contextMenu.findItem(com.uc.d.h.cdr).setVisible(false);
                            }
                            if (true == hK()) {
                                contextMenu.findItem(com.uc.d.h.cdn).setTitle(this.bk.getString(R.string.f959share_page));
                                contextMenu.findItem(com.uc.d.h.cdn).setVisible(true);
                            } else {
                                contextMenu.findItem(com.uc.d.h.cdn).setVisible(false);
                            }
                            webViewJUC.cR(true);
                            return;
                        default:
                            return;
                    }
                }
            }
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.xm != null) {
            this.xm.yU();
        }
        if (e.nR().pO()) {
            e.nR().oG();
        }
        if (this.vK != null) {
            this.vK = null;
        }
        if (this.bi != null) {
            this.bi = null;
        }
        if (this.vJ != null) {
            this.vJ.destroy();
            this.vJ = null;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
     arg types: [boolean, int]
     candidates:
      com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
      com.uc.browser.ModelBrowser.a(int, long):void
      com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
      com.uc.browser.ModelBrowser.a(java.lang.Object, int):void */
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        a((Object) false, 0);
        switch (i) {
            case 4:
                if (true == this.vQ.onKeyDown(i, keyEvent)) {
                    return true;
                }
                if (this.vK != null && true == this.vK.canGoBack()) {
                    a((Integer) -1);
                    return true;
                } else if (this.vK == null || true != this.vK.bG()) {
                    if (e.nR().bw(h.afQ).equals(e.RD)) {
                        ip();
                    } else if (this.vI == 0) {
                        this.vI++;
                        Toast.makeText(this.bk, (int) R.string.f1475tap_again_to_exit, 0).show();
                    } else if (this.vI == 1) {
                        if (e.nR().os()) {
                            ir();
                        } else {
                            ii();
                        }
                    }
                    return true;
                } else {
                    he();
                    return true;
                }
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
                if (!((this.wj != null && this.wj.getVisibility() == 0 && (21 == i || 22 == i)) || this.vK == null || true != this.vK.c(i, keyEvent))) {
                    return true;
                }
            case 82:
                this.AI = true;
                return true;
        }
        return false;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        switch (i) {
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
                if (this.vK != null && true == this.vK.c(i, keyEvent)) {
                    return true;
                }
            case 82:
                if (!(this.bi == this.vC && this.vC != null && this.vC.uX() == 0)) {
                    if (true != this.AI || this.vQ == null || true != this.vQ.onKeyUp(i, keyEvent)) {
                        this.AI = false;
                        break;
                    } else {
                        if (!io() && ActivityBrowser.bqy) {
                            this.vU.cX(false);
                        }
                        this.AI = false;
                        return true;
                    }
                }
                break;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        aL(1);
        this.wh = true;
        if (!(this.vJ == null || this.vJ.wj() < 0 || this.vK == null || this.vK.bs() || this.wg == null)) {
            this.wg.acquire();
            this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(25), 300000);
        }
        if (true == this.wi) {
            this.wi = false;
        }
        if (this.bi instanceof WebViewJUC) {
            ((WebViewJUC) this.bi).aF();
            ((WebViewJUC) this.bi).Qz();
        }
        if (this.vK != null) {
            this.vK.bu();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        e.Ro = false;
        e.nR().d(this.p);
        if (e.nR().pP()) {
            e.nR().pQ();
        }
        if (this.bk != null) {
            e.nR().am(this.bk.getWindowManager().getDefaultDisplay().getWidth(), this.bk.getWindowManager().getDefaultDisplay().getHeight());
        }
        if (this.wh) {
            try {
                this.wh = false;
                if (this.vK != null) {
                    this.vK.br();
                    this.vK.bt();
                }
            } catch (Exception e) {
            }
            if (this.vG == null) {
                gP();
            }
            this.vG.dismiss();
            aK(-1);
            if (this.vH == null) {
                gQ();
            }
            this.vH.dismiss();
            aK(-1);
            hx();
        }
    }

    /* access modifiers changed from: package-private */
    public void removeMessages(int i) {
        this.mHandler.removeMessages(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.b(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, int):int
      com.uc.browser.ModelBrowser.b(android.content.Context, int):android.app.Dialog
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, android.content.Context):android.app.Dialog
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.b(com.uc.browser.ModelBrowser, boolean):boolean
      com.uc.browser.ModelBrowser.b(boolean, boolean):void
      com.uc.browser.ModelBrowser.b(android.view.View, boolean):void */
    public void s(WindowUCWeb windowUCWeb) {
        this.vK = windowUCWeb;
        this.bk.t(this.vK.getTitle());
        b(this.vK.getCurrentView(), true);
    }

    /* access modifiers changed from: package-private */
    public void stopLoading() {
        if (this.vK != null) {
            if (true == V()) {
                W();
            }
            this.vK.stopLoading();
        }
    }

    public void t(String str) {
        if (this.vY != null) {
            this.vY.setText(str);
        }
    }

    /* access modifiers changed from: protected */
    public void v(boolean z) {
        if (this.vT != null) {
            if (ActivityBrowser.bqy) {
                this.vT.getWindow().setFlags(1024, 3072);
                this.vT.bBQ = true;
            } else {
                this.vT.getWindow().setFlags(2048, 3072);
                this.vT.bBQ = false;
            }
            this.vT.bBR = z;
            if (this.bi == this.vC || this.bi == null) {
                this.vT.bN(z);
            } else {
                this.vT.r(null, z);
            }
        }
    }

    public void x(boolean z) {
        e nR = e.nR();
        if (z) {
            nR.w(h.aeV, "4");
            nR.w(h.afn, "6");
            nR.w(h.aeW, "0");
            nR.w(h.afb, e.RD);
            nR.w(h.afP, "3");
            return;
        }
        nR.w(h.aeV, nR.bw(h.afh));
        nR.w(h.afn, nR.bw(h.afi));
        nR.w(h.aeW, nR.bw(h.afj));
        nR.w(h.afP, nR.bw(h.afk));
        nR.w(h.afb, "0");
    }

    public void y(int i, int i2) {
        if (this.bi instanceof WebViewJUC) {
            WebViewJUC webViewJUC = (WebViewJUC) this.bi;
            if (webViewJUC.M()) {
                a(webViewJUC, i, i2);
            } else if (this.AX != null) {
                this.AX.setVisibility(8);
            }
        }
    }

    public void y(boolean z) {
        this.Aq = z;
    }

    public void z(boolean z) {
        int bB = this.bk.bB(z);
        if (!z && bB == -4) {
            gS();
        }
    }
}
