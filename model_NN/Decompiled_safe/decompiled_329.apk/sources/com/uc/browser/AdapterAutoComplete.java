package com.uc.browser;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import b.a.a.e;
import com.uc.browser.UCR;
import com.uc.c.bx;
import java.util.Vector;

public class AdapterAutoComplete extends BaseAdapter {
    public static final int Wm = 0;
    public static final int Wn = 1;
    protected Vector Wk;
    protected int Wl;
    protected Vector Wo;
    protected Vector Wp;
    private boolean Wq;
    protected Vector lO;
    protected final Object lock;
    private int type;

    public AdapterAutoComplete() {
        this.lock = new Object();
        this.type = 0;
        this.Wq = true;
        this.type = 1;
    }

    public AdapterAutoComplete(int i) {
        this.lock = new Object();
        this.type = 0;
        this.Wq = true;
        this.type = i;
    }

    public void aH(boolean z) {
        this.Wq = z;
    }

    public void bV(String str) {
        synchronized (this.lock) {
            this.Wl = 0;
            if (this.lO == null) {
            }
            if (this.Wp == null) {
                this.Wp = new Vector();
            }
            String lowerCase = str.toLowerCase();
            this.Wp.clear();
            if (this.Wq && lowerCase != null && !lowerCase.equals("")) {
                if ("www.".startsWith(lowerCase)) {
                    e eVar = new e();
                    eVar.abL = "www.";
                    this.Wp.add(eVar);
                }
                if ("wap.".startsWith(lowerCase)) {
                    e eVar2 = new e();
                    eVar2.abL = "wap.";
                    this.Wp.add(eVar2);
                }
                if (bx.bAh.startsWith(lowerCase)) {
                    e eVar3 = new e();
                    eVar3.abL = bx.bAh;
                    this.Wp.add(eVar3);
                }
                if ("https://".startsWith(lowerCase)) {
                    e eVar4 = new e();
                    eVar4.abL = "https://";
                    this.Wp.add(eVar4);
                }
                if ("bbs.".startsWith(lowerCase)) {
                    e eVar5 = new e();
                    eVar5.abL = "bbs.";
                    this.Wp.add(eVar5);
                }
                int lastIndexOf = lowerCase.lastIndexOf(".");
                if (!"www.".startsWith(lowerCase) && !"wap.".startsWith(lowerCase)) {
                    String substring = lastIndexOf < 0 ? lowerCase + "." : lowerCase.substring(0, lastIndexOf + 1);
                    if (!lowerCase.endsWith(".com") && !lowerCase.endsWith(".com.")) {
                        e eVar6 = new e();
                        eVar6.abL = substring + "com";
                        this.Wp.add(eVar6);
                    }
                    if (lowerCase.endsWith(".")) {
                        e eVar7 = new e();
                        eVar7.abL = substring + "cn";
                        e eVar8 = new e();
                        eVar8.abL = substring + "com.cn";
                        e eVar9 = new e();
                        eVar9.abL = substring + "net";
                        e eVar10 = new e();
                        eVar10.abL = substring + "org";
                        this.Wp.add(eVar7);
                        if (!substring.endsWith(".com.")) {
                            this.Wp.add(eVar9);
                            this.Wp.add(eVar8);
                            this.Wp.add(eVar10);
                        }
                    }
                }
            }
            if (this.Wk != null) {
                this.Wk.clear();
            } else {
                this.Wk = new Vector();
            }
            int length = lowerCase.length();
            int size = this.Wp.size();
            for (int i = 0; i < size; i++) {
                e eVar11 = (e) this.Wp.elementAt(i);
                if (eVar11.abL.toLowerCase().contains(lowerCase) && eVar11.abL.length() != length) {
                    this.Wk.add(eVar11);
                    this.Wl++;
                    if (this.Wl > 100) {
                        break;
                    }
                }
            }
            if (this.lO != null) {
                int size2 = this.lO.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    e eVar12 = (e) this.lO.elementAt(i2);
                    if ((eVar12.abL.toLowerCase().contains(lowerCase) && eVar12.abL.length() != length) || (eVar12.abM != null && eVar12.abM.toLowerCase().contains(lowerCase))) {
                        this.Wk.add(eVar12);
                        this.Wl++;
                        if (this.Wl > 100) {
                            break;
                        }
                    }
                }
            }
            if (this.Wo != null) {
                int size3 = this.Wo.size();
                for (int i3 = 0; i3 < size3; i3++) {
                    e eVar13 = (e) this.Wo.elementAt(i3);
                    if ((eVar13.abL.toLowerCase().contains(lowerCase) && eVar13.abL.length() != length) || (eVar13.abM != null && eVar13.abM.toLowerCase().contains(lowerCase))) {
                        this.Wk.add(eVar13);
                        this.Wl++;
                        if (this.Wl > 100) {
                            break;
                        }
                    }
                }
            }
            if (this.type == 0 && lowerCase != null && lowerCase.length() > 0) {
                e eVar14 = new e();
                eVar14.abU = 101;
                eVar14.abM = "搜索\"" + lowerCase + "\"";
                eVar14.abL = lowerCase;
                this.Wk.add(eVar14);
                this.Wl++;
            }
            notifyDataSetChanged();
        }
    }

    public int getCount() {
        int i;
        synchronized (this.lock) {
            i = this.Wl;
        }
        return i;
    }

    public Object getItem(int i) {
        if (this.Wk != null) {
            return (e) this.Wk.elementAt(i % this.Wk.size());
        }
        return null;
    }

    public long getItemId(int i) {
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        Drawable drawable;
        TextView textView;
        TextView textView2;
        Drawable drawable2;
        int[][] iArr = {new int[]{16842919}, new int[0]};
        int[] iArr2 = {com.uc.h.e.Pb().getColor(35), com.uc.h.e.Pb().getColor(34)};
        if (this.Wl == 0) {
            return null;
        }
        if (this.type == 1) {
            if (view != null) {
                TextView textView3 = (TextView) view;
                textView = textView3;
                textView2 = textView3;
            } else {
                textView = (TextView) LayoutInflater.from(viewGroup.getContext()).inflate((int) R.layout.f876autocomplete_item, viewGroup, false);
                textView2 = textView;
            }
            synchronized (this.lock) {
                e eVar = (e) this.Wk.elementAt(i);
                textView.setText(eVar.abL);
                textView.setTextColor(new ColorStateList(iArr, iArr2));
                if (i % 2 == 0) {
                    textView.setBackgroundDrawable(com.uc.h.e.Pb().getDrawable(UCR.drawable.aXg));
                } else {
                    textView.setBackgroundDrawable(com.uc.h.e.Pb().getDrawable(UCR.drawable.aXh));
                }
                switch (eVar.abU) {
                    case 1:
                        drawable2 = com.uc.h.e.Pb().getDrawable(UCR.drawable.aTN);
                        break;
                    case 2:
                    case 3:
                        drawable2 = com.uc.h.e.Pb().getDrawable(UCR.drawable.aUI);
                        break;
                    default:
                        drawable2 = null;
                        break;
                }
                textView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, drawable2, (Drawable) null);
                int kp = com.uc.h.e.Pb().kp(R.dimen.f498add_sch_listitem_paddingtop);
                int kp2 = com.uc.h.e.Pb().kp(R.dimen.f499add_sch_listitem_paddingbottom);
                int kp3 = com.uc.h.e.Pb().kp(R.dimen.f497add_sch_listitem_paddingleft);
                textView.setPadding(kp3, kp, kp3, kp2);
            }
            return textView2;
        }
        View inflate = view != null ? view : LayoutInflater.from(viewGroup.getContext()).inflate((int) R.layout.f877autocomplete_urlitem, viewGroup, false);
        TextView textView4 = (TextView) inflate.findViewById(R.id.title);
        TextView textView5 = (TextView) inflate.findViewById(R.id.f675subtitle);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.icon);
        synchronized (this.lock) {
            e eVar2 = (e) this.Wk.elementAt(i);
            textView4.setText(eVar2.abM == null ? eVar2.abL : eVar2.abM);
            textView5.setText(eVar2.abL);
            textView4.setTextColor(new ColorStateList(iArr, iArr2));
            textView5.setTextColor(new ColorStateList(iArr, iArr2));
            if (i % 2 == 0) {
                inflate.setBackgroundDrawable(com.uc.h.e.Pb().getDrawable(UCR.drawable.aXg));
            } else {
                inflate.setBackgroundDrawable(com.uc.h.e.Pb().getDrawable(UCR.drawable.aXh));
            }
            inflate.setPadding(com.uc.h.e.Pb().kp(R.dimen.f497add_sch_listitem_paddingleft), com.uc.h.e.Pb().kp(R.dimen.f534add_sch_urlitem_paddingtop), com.uc.h.e.Pb().kp(R.dimen.f497add_sch_listitem_paddingleft), com.uc.h.e.Pb().kp(R.dimen.f534add_sch_urlitem_paddingtop));
            switch (eVar2.abU) {
                case 1:
                    drawable = com.uc.h.e.Pb().getDrawable(UCR.drawable.aTN);
                    break;
                case 2:
                case 3:
                    drawable = com.uc.h.e.Pb().getDrawable(UCR.drawable.aUI);
                    break;
                default:
                    drawable = null;
                    break;
            }
            imageView.setImageDrawable(drawable);
        }
        return inflate;
    }

    public void i(Vector vector) {
        this.lO = vector;
    }

    public void j(Vector vector) {
        if (vector != null) {
            this.lO.addAll(vector);
        }
    }

    public void k(Vector vector) {
        this.Wo = vector;
    }

    public void l(Vector vector) {
        if (this.Wo == null) {
            k(vector);
        } else if (vector != null) {
            this.Wo.addAll(vector);
        }
    }

    public boolean sx() {
        return this.Wq;
    }
}
