package com.uc.browser;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class ChannelPageView extends RelativeLayout {
    private BuzLayout bkA;

    public ChannelPageView(Context context) {
        super(context);
        a();
    }

    public ChannelPageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    private void a() {
        addView(LayoutInflater.from(getContext()).inflate((int) R.layout.f911mainpage_channel, (ViewGroup) null));
        this.bkA = (BuzLayout) findViewById(R.id.f782mainpage_buz);
    }

    public BuzLayout Dr() {
        return this.bkA;
    }

    public String bi() {
        if (this.bkA == null) {
            return null;
        }
        return this.bkA.bi();
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (this.bkA != null) {
            return this.bkA.dispatchKeyEvent(keyEvent);
        }
        return false;
    }
}
