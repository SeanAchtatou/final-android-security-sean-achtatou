package com.uc.browser;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

public class SAnimLinearLayout extends LinearLayout {
    StringBuffer TQ = null;
    private Animation Z;
    private Transformation aa;

    public SAnimLinearLayout(Context context) {
        super(context);
    }

    public SAnimLinearLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void draw(Canvas canvas) {
        boolean z = false;
        if (this.TQ == null) {
            this.TQ = new StringBuffer();
        }
        this.TQ.append(System.currentTimeMillis() - MultiWindowViewEx.avs).append(" ");
        if (this.Z != null) {
            if (!this.Z.isInitialized()) {
                this.Z.initialize(getWidth(), getHeight(), getWidth(), getHeight());
            }
            if (this.aa == null) {
                this.aa = new Transformation();
            }
            z = this.Z.getTransformation(System.currentTimeMillis(), this.aa);
            canvas.concat(this.aa.getMatrix());
        }
        super.draw(canvas);
        if (z) {
            invalidate();
        } else {
            this.Z = null;
        }
    }

    public void startAnimation(Animation animation) {
        this.Z = animation;
        if (this.Z != null) {
            invalidate();
            this.Z.reset();
            this.Z.start();
        }
    }
}
