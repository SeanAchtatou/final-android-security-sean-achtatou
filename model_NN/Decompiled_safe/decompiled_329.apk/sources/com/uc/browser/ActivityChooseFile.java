package com.uc.browser;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.uc.a.e;
import com.uc.browser.UCAlertDialog;
import com.uc.browser.UCR;
import com.uc.c.au;
import com.uc.c.cd;
import com.uc.e.ac;
import com.uc.e.h;
import com.uc.e.z;
import java.io.File;

public class ActivityChooseFile extends ActivityWithUCMenu implements AdapterView.OnItemClickListener, h {
    private static final String KEY = "s";
    public static final int RESULT_OK = 12289;
    public static final String tW = "file_maxlength";
    public static final String uA = "title_res_id";
    private static final String uB = "file_root_temp";
    private static final String uC = "file_choose_temp";
    public static final String uE = "choose_file_path";
    public static final String uF = "choose_file_name";
    public static final String[] uJ = {"B", "KB", "MB", "GB"};
    public static final String uv = "file_choose_state";
    public static final String uw = "file_default_folder";
    public static final String ux = "file_type";
    public static final String uy = "file_should_filt";
    public static final String uz = "-1";
    private int tX = -1;
    private File uD;
    private boolean uG = true;
    String uH = null;
    private int uI = -1;
    private RelativeLayout ul;
    private BarLayout um;
    private ac un;
    private ac uo;
    private ac up;
    private ListView uq;
    /* access modifiers changed from: private */
    public AdapterFile ur;
    private boolean us = false;
    private File ut;
    private int uu;

    private boolean d(File file) {
        boolean z = this.uH == null || file.isDirectory();
        if (z) {
            return z;
        }
        String[] split = this.uH.split(cd.bVH);
        String lowerCase = file.getName().toLowerCase();
        for (String lowerCase2 : split) {
            if (lowerCase.endsWith(lowerCase2.toLowerCase())) {
                return true;
            }
        }
        return z;
    }

    private Dialog f(final Context context) {
        LayoutInflater from = LayoutInflater.from(context);
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        View inflate = from.inflate((int) R.layout.f882bookmark_dialog_newfolder, (ViewGroup) null);
        final EditText editText = (EditText) inflate.findViewById(R.id.f690bookmark_dlg_foldername);
        builder.L(getResources().getString(R.string.f1144dialog_title_makedir));
        builder.c(inflate);
        builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                String trim = editText.getText().toString().trim();
                if (trim != null && trim.length() > 0 && trim.getBytes().length <= 240) {
                    int de = ActivityFileMaintain.de(ActivityChooseFile.this.ur.ER().getPath() + au.aGF + trim);
                    if (de == 0) {
                        ActivityChooseFile.this.ur.EN();
                        dialogInterface.dismiss();
                    } else if (2 == de) {
                        Toast.makeText(context, (int) R.string.f1148dialog_msg_filename_dulplicate, 0).show();
                    } else if (!e.nR().ae(trim.toString())) {
                        Toast.makeText(context, "目录不能包含下列任何字符\\/:*?\"<>|", 0).show();
                    } else {
                        Toast.makeText(context, (int) R.string.f1149dialog_msg_folder_create_failure, 0).show();
                        dialogInterface.dismiss();
                    }
                } else if (240 < trim.getBytes().length) {
                    Toast.makeText(ActivityChooseFile.this, (int) R.string.f1153text_max_size, 0).show();
                } else {
                    editText.setText(trim);
                    Toast.makeText(context, (int) R.string.f1145dialog_msg_filename_cannot_null, 0).show();
                }
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        return builder.fh();
    }

    private boolean fz() {
        if (this.ur == null) {
            return false;
        }
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return false;
        }
        File ER = this.ur.ER();
        if (ER == null) {
            return false;
        }
        File parentFile = ER.getParentFile();
        if (parentFile == null || !parentFile.exists() || !parentFile.getPath().startsWith("/sdcard")) {
            return false;
        }
        this.ur.n(parentFile);
        this.uD = parentFile;
        return true;
    }

    private void t(boolean z) {
        this.um = (BarLayout) findViewById(R.id.f655controlbar);
        com.uc.h.e Pb = com.uc.h.e.Pb();
        int kp = Pb.kp(R.dimen.f183controlbar_item_width_2);
        int kp2 = Pb.kp(R.dimen.f177controlbar_height);
        int kp3 = Pb.kp(R.dimen.f188controlbar_text_size);
        int kp4 = Pb.kp(R.dimen.f187controlbar_item_paddingTop);
        this.um.HN.aw(kp, kp2);
        Resources resources = getResources();
        this.un = new ac(R.string.f1018controlbar_ok, 0, 0);
        this.un.bB(0, 0);
        this.un.aV(kp3);
        this.un.setText(resources.getString(R.string.f1018controlbar_ok));
        this.un.setPadding(0, kp4, 0, 4);
        this.un.H(true);
        this.um.a(this.un);
        if (z) {
            this.up = new ac(R.string.f1019controlbar_makedir, 0, 0);
            this.up.bB(0, 0);
            this.up.aV(kp3);
            this.up.setText(resources.getString(R.string.f1019controlbar_makedir));
            this.up.setPadding(0, kp4, 0, 4);
            this.up.H(true);
            this.um.a(this.up);
        }
        this.uo = new ac(R.string.f1017controlbar_back, 0, 0);
        this.uo.bB(0, 0);
        this.uo.aV(kp3);
        this.uo.setText(resources.getString(R.string.f1017controlbar_back));
        this.uo.setPadding(0, kp4, 0, 4);
        this.uo.H(true);
        this.um.a(this.uo);
        this.um.kJ();
        this.um.k();
        this.um.b(this);
    }

    public void b(z zVar, int i) {
        switch (i) {
            case R.string.f1017controlbar_back /*2131296324*/:
                if (!fz()) {
                    finish();
                    return;
                }
                this.ut = null;
                this.uI = -1;
                return;
            case R.string.f1018controlbar_ok /*2131296325*/:
                if (this.ut != null) {
                    Intent intent = new Intent();
                    intent.putExtra(uE, this.ut.getParent());
                    intent.putExtra(uF, this.ut.getName());
                    intent.putExtra(uv, this.uu);
                    setResult(RESULT_OK, intent);
                    finish();
                    return;
                } else if (!this.us) {
                    Toast.makeText(this, (int) R.string.f975file_choose_file_null, 0).show();
                    return;
                } else if (this.uD != null) {
                    Intent intent2 = new Intent();
                    intent2.putExtra(uE, this.uD.getParent());
                    intent2.putExtra(uF, this.uD.getName());
                    intent2.putExtra(uv, this.uu);
                    setResult(RESULT_OK, intent2);
                    finish();
                    return;
                } else {
                    return;
                }
            case R.string.f1019controlbar_makedir /*2131296326*/:
                f(this).show();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1) {
            setResult(77);
            finish();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.RelativeLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void onCreate(Bundle bundle) {
        int i;
        String str;
        String str2;
        super.onCreate(bundle);
        ActivityBrowser.g(this);
        requestWindowFeature(1);
        this.ul = (RelativeLayout) LayoutInflater.from(this).inflate((int) R.layout.f898file_chooser, (ViewGroup) this.ul, true);
        setContentView(this.ul);
        Intent intent = getIntent();
        if (intent != null) {
            this.uu = intent.getIntExtra(uv, 0);
            String stringExtra = intent.getStringExtra(uw);
            this.uH = intent.getStringExtra(ux);
            this.uG = intent.getBooleanExtra(uy, true);
            this.tX = intent.getIntExtra("file_maxlength", -1);
            i = intent.getIntExtra(uA, -1);
            str = stringExtra;
        } else {
            i = -1;
            str = null;
        }
        if (i != -1) {
            ((TextView) findViewById(R.id.f654Browser_TitleBar)).setText(i);
        }
        if (uz.equals(this.uH)) {
            this.us = true;
            if (i == -1) {
                ((TextView) findViewById(R.id.f654Browser_TitleBar)).setText((int) R.string.f978dir_choose_title);
            }
            t(true);
        } else {
            this.us = false;
            if (i == -1) {
                ((TextView) findViewById(R.id.f654Browser_TitleBar)).setText((int) R.string.f976file_choose_title);
            }
            t(false);
        }
        if (bundle != null) {
            str2 = bundle.getString(uB);
            this.uu = bundle.getInt(uv);
        } else {
            str2 = str;
        }
        if (str2 == null || str2 == "") {
            str2 = "/sdcard/";
        }
        this.uD = new File(str2);
        if (!this.uD.exists() && !str2.equals("/sdcard/")) {
            this.uD = new File("/sdcard/");
        }
        if (!this.uD.exists() || !Environment.getExternalStorageState().equals("mounted")) {
            findViewById(R.id.f745no_sd_card_warning).setVisibility(0);
            findViewById(R.id.f745no_sd_card_warning).bringToFront();
            this.un.setVisibility(4);
            return;
        }
        this.uq = (ListView) findViewById(R.id.f746file_list);
        this.uq.setSelector(com.uc.h.e.Pb().getDrawable(UCR.drawable.aXb));
        this.uq.setDivider(new ColorDrawable(com.uc.h.e.Pb().getColor(108)));
        this.uq.setDividerHeight(1);
        this.uq.setBackgroundColor(com.uc.h.e.Pb().getColor(47));
        this.uq.setCacheColorHint(com.uc.h.e.Pb().getColor(47));
        this.ur = new AdapterFile(this.uG ? this.uH : null);
        this.ur.setType(1);
        this.ur.n(this.uD);
        this.uq.setAdapter((ListAdapter) this.ur);
        this.uq.setOnItemClickListener(this);
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.ur.EO();
        FileItem fileItem = (FileItem) this.ur.getItem(i);
        if (fileItem.blx == -2) {
            if (!fz()) {
                Toast.makeText(this, (int) R.string.f974file_already_root, 0).show();
            }
        } else if (fileItem.blw.isDirectory()) {
            if (!this.us) {
                this.uD = fileItem.blw;
                this.ur.n(this.uD);
                this.ut = null;
                this.uI = -1;
            } else if (fileItem.blw == this.ut) {
                this.uD = fileItem.blw;
                this.ur.n(this.uD);
                this.ut = null;
                this.uI = -1;
            } else {
                fileItem.bx(true);
                this.ut = fileItem.blw;
                this.uI = i;
                this.ur.notifyDataSetChanged();
            }
        } else if (this.tX != -1 && fileItem.blw.length() > ((long) this.tX)) {
            float f = (float) this.tX;
            int i2 = 0;
            while (f > 1024.0f) {
                f /= 1024.0f;
                i2++;
            }
            Toast.makeText(this, String.format(getString(R.string.f1445file_upload_too_large), Float.valueOf(f), uJ[i2]), 0).show();
        } else if (this.uG || d(fileItem.blw)) {
            fileItem.bx(true);
            this.ut = fileItem.blw;
            this.uI = i;
            this.ur.notifyDataSetChanged();
        } else {
            Toast.makeText(this, (int) R.string.f1446file_upload_supportless_type, 0).show();
        }
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i != 84) {
            return super.onKeyUp(i, keyEvent);
        }
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a((int) ModelBrowser.zJ, this);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        FileItem fileItem;
        super.onRestoreInstanceState(bundle);
        int i = bundle.getInt(KEY);
        if (i != -1 && this.ur != null && (fileItem = (FileItem) this.ur.getItem(i)) != null) {
            this.ur.EO();
            fileItem.bx(true);
            this.ur.notifyDataSetChanged();
            this.uI = i;
            this.ut = fileItem.blw;
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString(uB, this.uD.getPath());
        bundle.putInt(uv, this.uu);
        bundle.putInt(KEY, this.uI);
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z && ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(this);
        }
        if (z && ActivityBrowser.Fz()) {
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
            }
            ActivityBrowser.e(this);
        }
    }
}
