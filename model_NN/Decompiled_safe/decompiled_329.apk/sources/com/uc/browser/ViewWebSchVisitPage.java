package com.uc.browser;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import com.uc.e.a.v;
import com.uc.e.a.x;
import com.uc.e.ad;

public class ViewWebSchVisitPage extends View {
    public static final int bWu = 1;
    public static final int bWv = 2;
    private Animation Z;
    private Transformation aa;
    /* access modifiers changed from: private */
    public boolean arR;
    /* access modifiers changed from: private */
    public URLBarAniListener bBT;
    private v bWk;
    public boolean bWl;
    private boolean bWm = false;
    private boolean bWn = false;
    private boolean bWo = false;
    private boolean bWp = false;
    private Animation bWq;
    private Animation bWr;
    private int bWs = 80;
    private int bWt = 80;
    private int bWw = 1;
    /* access modifiers changed from: private */
    public boolean bWx = false;
    private Transformation bpH;

    public interface URLBarAniListener {
        void ef();

        void eg();
    }

    public ViewWebSchVisitPage(Context context) {
        super(context);
        NX();
    }

    public ViewWebSchVisitPage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        NX();
    }

    private void NX() {
        this.bWk = new v();
        this.bWk.a(new ad() {
            public void cJ() {
                ViewWebSchVisitPage.this.postInvalidate();
            }
        });
        this.bpH = new Transformation();
    }

    /* access modifiers changed from: private */
    public void Ob() {
        setVisibility(8);
    }

    public String Fm() {
        return this.bWk.Fm();
    }

    public boolean Fn() {
        return this.bWk.Fn();
    }

    public void Fo() {
        this.bWk.Fo();
    }

    public int NY() {
        return this.bWk.getMax();
    }

    public int NZ() {
        return this.bWk.Fl();
    }

    public void Oa() {
        a(new URLBarAniListener() {
            public void ef() {
            }

            public void eg() {
                ViewWebSchVisitPage.this.Ob();
                boolean unused = ViewWebSchVisitPage.this.bWx = false;
                ViewWebSchVisitPage.this.jW(1);
            }
        });
    }

    public void a(URLBarAniListener uRLBarAniListener) {
        this.bBT = uRLBarAniListener;
    }

    public void a(x xVar) {
        this.bWk.a(xVar);
    }

    public void b(Animation animation) {
        this.arR = true;
        setVisibility(0);
        if (animation != null) {
            this.bWq = animation;
            this.bWq.initialize(1, 1, 1, 1);
            this.bWq.setStartTime(-1);
            this.bWm = true;
            startAnimation(this.bWq);
            invalidate();
        }
    }

    public void bM(boolean z) {
        if (z) {
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (-getHeight()));
            translateAnimation.setDuration((long) this.bWt);
            c(translateAnimation);
            return;
        }
        c(null);
    }

    public void bN(boolean z) {
        if (z) {
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) (-getHeight()), 0.0f);
            translateAnimation.setDuration((long) this.bWs);
            b(translateAnimation);
            return;
        }
        b((Animation) null);
    }

    public void c(Animation animation) {
        if (animation != null) {
            this.bWr = animation;
            this.bWr.initialize(1, 1, 1, 1);
            this.bWr.setStartTime(-1);
            this.bWo = true;
            this.bWr.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation animation) {
                    boolean unused = ViewWebSchVisitPage.this.arR = false;
                    ViewWebSchVisitPage.this.setVisibility(8);
                    if (ViewWebSchVisitPage.this.bBT != null) {
                        ViewWebSchVisitPage.this.bBT.eg();
                    }
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                }
            });
            startAnimation(this.bWr);
        } else {
            this.arR = false;
            if (this.bBT != null) {
                this.bBT.eg();
            }
        }
        invalidate();
    }

    public void draw(Canvas canvas) {
        boolean z = false;
        if (this.Z != null) {
            if (!this.Z.isInitialized()) {
                this.Z.initialize(getWidth(), getHeight(), getWidth(), getHeight());
            }
            if (this.aa == null) {
                this.aa = new Transformation();
            }
            z = this.Z.getTransformation(System.currentTimeMillis(), this.aa);
            canvas.concat(this.aa.getMatrix());
        }
        super.draw(canvas);
        if (z) {
            invalidate();
        } else {
            this.Z = null;
        }
    }

    public void eF(String str) {
        this.bWk.eF(str == null ? "" : str);
        this.bWk.Ix();
    }

    public void jW(int i) {
        this.bWw = i;
    }

    public void jX(int i) {
        this.bWs = i;
    }

    public void jY(int i) {
        this.bWt = i;
    }

    public void jZ(int i) {
        this.bWk.setMax(i);
    }

    public void ka(int i) {
        this.bWk.hw(i);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.arR) {
            this.bWk.onDraw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.bWk.setSize(i3 - i, i4 - i2);
        this.bWk.init(getResources().getConfiguration().orientation == 1 ? 0 : 1);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.bWk.onTouchEvent(motionEvent);
    }

    public void setProgress(int i) {
        if (!this.bWx) {
            this.bWk.setProgress(i < 5 ? 5 : i);
        }
    }

    public void setVisibility(int i) {
        super.setVisibility(i);
        this.bWk.Fo();
    }

    public void startAnimation(Animation animation) {
        this.Z = animation;
        if (this.Z != null) {
            invalidate();
            this.Z.reset();
            this.Z.start();
        }
    }
}
