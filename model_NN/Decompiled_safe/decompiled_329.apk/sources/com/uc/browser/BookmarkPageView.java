package com.uc.browser;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import b.a.a.e;
import b.b;
import com.uc.a.m;
import com.uc.browser.BookmarkTabContainer;
import com.uc.browser.TabContainer;
import com.uc.browser.UCAlertDialog;
import com.uc.c.au;
import com.uc.c.cd;
import com.uc.c.w;
import com.uc.d.c;
import com.uc.d.d;
import com.uc.d.f;
import com.uc.d.g;
import com.uc.e.h;
import com.uc.e.z;
import com.uc.h.a;
import java.util.ArrayList;
import java.util.Vector;

public class BookmarkPageView extends RelativeLayout implements BookmarkTabContainer.BookmarkDelegate, TabContainer.onTabChangedListener, d, h, a {
    private static final int azX = 1;
    private static final int bYP = 0;
    private static final int bYQ = 1;
    private static final int bYR = 2;
    private static final int bYS = 3;
    private static final int caA = 4;
    private static final int caB = 5;
    private static final int caC = 6;
    public static final int caF = 100;
    public static final int caG = 200;
    private static final int caI = 4097;
    private static final int caw = 0;
    private static final int cax = 1;
    private static final int cay = 2;
    private static final int caz = 3;
    private static final int wH = 0;
    private static final int wI = 1;
    static final int ws = 1;
    private ViewMainBarMainPage aeI;
    /* access modifiers changed from: private */
    public m aso;
    /* access modifiers changed from: private */
    public e bQY;
    /* access modifiers changed from: private */
    public int caD = 0;
    private int caE = 0;
    /* access modifiers changed from: private */
    public BookmarkTabContainer caH;
    private int cav = 0;
    private c lE;
    private f lF;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    ((InputMethodManager) BookmarkPageView.this.getContext().getSystemService("input_method")).toggleSoftInput(0, 1);
                    return;
                default:
                    return;
            }
        }
    };

    public BookmarkPageView(Context context) {
        super(context);
        a();
    }

    public BookmarkPageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    private Dialog A(Context context) {
        LayoutInflater from = LayoutInflater.from(context);
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        View inflate = from.inflate((int) R.layout.f879bookmark_dialog_editbookmark, (ViewGroup) null);
        final EditText editText = (EditText) inflate.findViewById(R.id.f678bookmark_dlg_filename);
        final EditText editText2 = (EditText) inflate.findViewById(R.id.f680bookmark_dlg_filepath);
        final Spinner spinner = (Spinner) inflate.findViewById(R.id.f682bookmark_dlg_folder);
        ArrayList arrayList = new ArrayList();
        arrayList.add("root");
        Vector ol = this.aso.ol();
        int i = 0;
        while (true) {
            if (i < (ol != null ? ol.size() : 0)) {
                arrayList.add("root/" + ((e) ol.elementAt(i)).abM);
                i++;
            } else {
                spinner.setAdapter((SpinnerAdapter) new ArrayAdapter(context, (int) R.layout.f883bookmark_dialog_spinneritem, arrayList));
                builder.L(getResources().getString(R.string.f1131dialog_title_newbookmark));
                builder.c(inflate);
                builder.a(getResources().getString(R.string.f1123alert_dialog_ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        BookmarkPageView.this.d(editText.getText().toString(), editText2.getText().toString(), spinner.getSelectedItemPosition());
                        BookmarkPageView.this.caH.jS();
                    }
                });
                builder.c(getResources().getString(R.string.f1125alert_dialog_cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                return builder.fh();
            }
        }
    }

    private Dialog B(Context context) {
        View inflate = LayoutInflater.from(context).inflate((int) R.layout.f879bookmark_dialog_editbookmark, (ViewGroup) null);
        final EditText editText = (EditText) inflate.findViewById(R.id.f678bookmark_dlg_filename);
        final EditText editText2 = (EditText) inflate.findViewById(R.id.f680bookmark_dlg_filepath);
        final Spinner spinner = (Spinner) inflate.findViewById(R.id.f682bookmark_dlg_folder);
        editText.requestFocus();
        ArrayList arrayList = new ArrayList();
        String string = context.getString(R.string.f1464dir_root);
        arrayList.add(string);
        Vector ol = this.aso.ol();
        int size = ol != null ? ol.size() : 0;
        for (int i = 0; i < size; i++) {
            arrayList.add(string + au.aGF + ((e) ol.elementAt(i)).abM);
        }
        spinner.setAdapter((SpinnerAdapter) new ArrayAdapter(context, (int) R.layout.f883bookmark_dialog_spinneritem, arrayList));
        editText.setText(this.bQY.abM);
        editText2.setText(this.bQY.abL);
        int i2 = this.caD;
        if (size <= this.caD) {
            i2 = -1;
        }
        spinner.setSelection(i2 + 1);
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.L(getResources().getString(R.string.f1132dialog_title_editbookmark));
        builder.c(inflate);
        builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                BookmarkPageView.this.c(editText.getText().toString(), editText2.getText().toString(), spinner.getSelectedItemPosition(), BookmarkPageView.this.bQY.abQ);
                BookmarkPageView.this.caH.jS();
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return builder.fh();
    }

    private Dialog C(Context context) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.L(getResources().getString(R.string.f1134dialog_title_deletebookmark));
        builder.M(getResources().getString(R.string.f1164dialog_msg_deletebookmark) + " " + this.bQY.abM + " ?");
        builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                int c = BookmarkPageView.this.caD;
                if (BookmarkPageView.this.caD >= com.uc.a.e.nR().ol().size()) {
                    c = -1;
                }
                BookmarkPageView.this.aso.c(-1 == c ? null : (e) BookmarkPageView.this.aso.ol().elementAt(c), BookmarkPageView.this.bQY);
                BookmarkPageView.this.caH.jS();
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return builder.fh();
    }

    private Dialog D(Context context) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.L(getResources().getString(R.string.f1136dialog_title_deletefolder));
        builder.M(getResources().getString(R.string.f1165dialog_msg_deletefolder) + " " + this.bQY.abM + " ?");
        builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                BookmarkPageView.this.aso.h(BookmarkPageView.this.bQY);
                BookmarkPageView.this.caH.jS();
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return builder.fh();
    }

    private Dialog E(Context context) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.L(getResources().getString(R.string.f1138dialog_title_deleteall));
        builder.M(getResources().getString(R.string.f1166dialog_msg_deleteall_bookmark));
        builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                BookmarkPageView.this.aso.on();
                BookmarkPageView.this.caH.jS();
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return builder.fh();
    }

    private Dialog F(Context context) {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.L(getResources().getString(R.string.f1138dialog_title_deleteall));
        builder.M(getResources().getString(R.string.f1167dialog_msg_deleteall_history));
        builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                com.uc.a.e.nR().oa().clearHistory();
                BookmarkPageView.this.caH.jT();
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return builder.fh();
    }

    private void Pi() {
        String oV = com.uc.a.e.nR().oj().oV();
        if (oV != null) {
            ModelBrowser.gD().a(11, oV);
        }
    }

    private void a() {
        addView(LayoutInflater.from(getContext()).inflate((int) R.layout.f910mainpage_bookmark, (ViewGroup) null));
        this.aso = com.uc.a.e.nR().oj();
        this.caH = (BookmarkTabContainer) findViewById(R.id.f781tab_container);
        com.uc.h.e Pb = com.uc.h.e.Pb();
        this.caH.a((TabContainer.onTabChangedListener) this);
        k();
        Pb.a(this);
        this.caH.a((BookmarkTabContainer.BookmarkDelegate) this);
        this.caH.jS();
    }

    /* access modifiers changed from: private */
    public void ax(final String str, String str2) {
        if (str != null && str2 != null) {
            Context context = getContext();
            LayoutInflater from = LayoutInflater.from(context);
            UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
            View inflate = from.inflate((int) R.layout.f880bookmark_dialog_export, (ViewGroup) null);
            final EditText editText = (EditText) inflate.findViewById(R.id.f684bookmark_dlg_exportname);
            editText.setText(str2);
            ((TextView) inflate.findViewById(R.id.f683file_name)).setTextColor(com.uc.h.e.Pb().getColor(78));
            builder.L(getResources().getString(R.string.f1402export_bookmark));
            builder.c(inflate);
            builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    BookmarkPageView.this.aw(str, editText.getText().toString());
                }
            });
            builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
            builder.fh().show();
        }
    }

    private boolean ga(String str) {
        if (str == null || str.length() <= 0) {
            return false;
        }
        return !str.startsWith(w.Os);
    }

    /* access modifiers changed from: private */
    public void gb(String str) {
        boolean z;
        e eVar = new e();
        eVar.abM = str;
        eVar.abP = true;
        if (eVar.abM == null) {
            Toast.makeText(getContext(), (int) R.string.f1392dirnamenotnull, 0).show();
            z = true;
        } else {
            eVar.abM = eVar.abM.trim();
            if (eVar.abM.length() == 0) {
                Toast.makeText(getContext(), (int) R.string.f1392dirnamenotnull, 0).show();
                z = true;
            } else {
                z = false;
            }
        }
        if (z) {
            k(getContext(), eVar.abM).show();
            return;
        }
        int f = this.aso.f(eVar);
        if (f == 0) {
            Toast.makeText(getContext(), (int) R.string.f1170msg_add_dir_success, 0).show();
        } else if (1 == f) {
            Toast.makeText(getContext(), (int) R.string.f1392dirnamenotnull, 0).show();
        } else if (2 == f) {
            Toast.makeText(getContext(), (int) R.string.f1395dirisexists, 0).show();
        }
    }

    /* access modifiers changed from: private */
    public void gc(String str) {
        String str2;
        boolean z;
        Context context = getContext();
        if (str == null) {
            Toast.makeText(context, (int) R.string.f1392dirnamenotnull, 0).show();
            z = true;
            str2 = str;
        } else {
            String trim = str.trim();
            if (trim.length() == 0) {
                Toast.makeText(context, (int) R.string.f1392dirnamenotnull, 0).show();
                str2 = trim;
                z = true;
            } else {
                str2 = trim;
                z = false;
            }
        }
        if (z) {
            l(context, str2).show();
            return;
        }
        this.bQY.abM = str2;
        int g = this.aso.g(this.bQY);
        if (g == 0) {
            Toast.makeText(context, (int) R.string.f1173msg_edit_success, 0).show();
        } else if (1 == g) {
            Toast.makeText(context, (int) R.string.f1392dirnamenotnull, 0).show();
        } else if (2 == g) {
            Toast.makeText(context, (int) R.string.f1395dirisexists, 0).show();
        }
        this.caH.jS();
    }

    public static final String[] j(String str) {
        String str2;
        String str3;
        int indexOf = str.indexOf("://");
        String substring = (-1 == indexOf || 6 <= indexOf) ? str : str.substring(indexOf + 3);
        int indexOf2 = substring.indexOf(au.aGF);
        if (indexOf2 > 0) {
            String substring2 = substring.substring(0, indexOf2);
            str3 = substring.substring(indexOf2);
            str2 = (str3 == null || str3.length() != 0) ? substring2 : substring2;
        } else {
            int indexOf3 = substring.indexOf("?");
            if (indexOf3 > 0) {
                String substring3 = substring.substring(0, indexOf3);
                str3 = substring.substring(indexOf3);
                str2 = substring3;
            } else {
                str2 = substring;
                str3 = "";
            }
        }
        String[] strArr = new String[2];
        if (str2 != null && str2.endsWith(cd.bVI)) {
            str2 = str2.substring(0, str2.length() - 1);
        }
        strArr[0] = str2;
        strArr[1] = str3;
        return strArr;
    }

    private Dialog k(Context context, String str) {
        LayoutInflater from = LayoutInflater.from(context);
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        View inflate = from.inflate((int) R.layout.f882bookmark_dialog_newfolder, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.f689bookmark_dlg_name)).setTextColor(com.uc.h.e.Pb().getColor(7));
        final EditText editText = (EditText) inflate.findViewById(R.id.f690bookmark_dlg_foldername);
        if (str != null) {
            editText.setText(str);
        }
        builder.L(getResources().getString(R.string.f1133dialog_title_newfolder));
        builder.c(inflate);
        builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                BookmarkPageView.this.gb(editText.getText().toString());
                BookmarkPageView.this.caH.jS();
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return builder.fh();
    }

    private Dialog l(Context context, String str) {
        LayoutInflater from = LayoutInflater.from(context);
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        View inflate = from.inflate((int) R.layout.f882bookmark_dialog_newfolder, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.f689bookmark_dlg_name)).setTextColor(com.uc.h.e.Pb().getColor(7));
        final EditText editText = (EditText) inflate.findViewById(R.id.f690bookmark_dlg_foldername);
        if (str != null) {
            editText.setText(str);
        }
        builder.L(getResources().getString(R.string.f1137dialog_title_renamefolder));
        builder.c(inflate);
        builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                BookmarkPageView.this.gc(editText.getText().toString());
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return builder.fh();
    }

    public void Ph() {
        if (this.caH != null) {
            this.caH.jS();
            this.caH.jT();
        }
        if (this.aeI != null) {
            this.aeI.Pa();
        }
    }

    public void Pj() {
        if (ga(this.bQY.abL)) {
            String str = this.bQY.abM;
            if (str == null || str.trim().length() == 0) {
                str = "";
            }
            ModelBrowser.gD().a(94, 0, new String[]{str + " " + this.bQY.abL + " " + getContext().getResources().getString(R.string.f957share_suffix), str, this.bQY.abL});
            return;
        }
        Toast.makeText(getContext(), (int) R.string.f961share_fail, 0).show();
    }

    public void a(e eVar, int i) {
        if (this.lE == null) {
            this.lE = new c(getContext());
        }
        this.lE.clear();
        if (this.lF == null) {
            this.lF = new f(getContext());
            this.lF.a();
        }
        this.lF.a(this.lE);
        this.lE.a(this.lF);
        this.lF.a(this);
        this.lE.cV();
        this.lF.show();
        if (i == 0) {
            g.a(getContext(), com.uc.d.h.ccu, this.lE);
            c cVar = this.lE;
            if (eVar != null) {
                if (eVar.abP) {
                    cVar.findItem(com.uc.d.h.ccr).setVisible(true);
                    cVar.findItem(com.uc.d.h.ccs).setVisible(true);
                    cVar.findItem(com.uc.d.h.ccp).setVisible(false);
                    cVar.findItem(com.uc.d.h.ccm).setVisible(false);
                    cVar.findItem(com.uc.d.h.ccn).setVisible(false);
                    cVar.findItem(com.uc.d.h.cco).setVisible(false);
                    cVar.findItem(com.uc.d.h.ccq).setVisible(false);
                    cVar.findItem(com.uc.d.h.cct).setVisible(false);
                    return;
                }
                cVar.findItem(com.uc.d.h.ccp).setVisible(true);
                cVar.findItem(com.uc.d.h.ccr).setVisible(false);
                cVar.findItem(com.uc.d.h.ccs).setVisible(false);
                if (ModelBrowser.gD() == null || true != ModelBrowser.gD().hj().wg()) {
                    cVar.findItem(com.uc.d.h.ccm).setVisible(false);
                    cVar.findItem(com.uc.d.h.ccn).setVisible(false);
                } else {
                    cVar.findItem(com.uc.d.h.ccm).setVisible(true);
                    cVar.findItem(com.uc.d.h.ccn).setVisible(true);
                }
                cVar.findItem(com.uc.d.h.cco).setVisible(true);
                cVar.findItem(com.uc.d.h.ccq).setVisible(true);
            }
        } else if (i == 1) {
            c cVar2 = this.lE;
            g.a(getContext(), com.uc.d.h.ccy, this.lE);
            if (ModelBrowser.gD() == null || true != ModelBrowser.gD().hj().wg()) {
                cVar2.findItem(com.uc.d.h.ccv).setVisible(false);
            } else {
                cVar2.findItem(com.uc.d.h.ccv).setVisible(true);
            }
            cVar2.findItem(com.uc.d.h.ccw).setVisible(true);
        } else if (i == 4097) {
            g.a(getContext(), com.uc.d.h.ccl, this.lE);
            if (com.uc.a.e.nR().oj().ol() == null && com.uc.a.e.nR().oj().e(null) == null) {
                this.lE.findItem(com.uc.d.h.cck).setVisible(false);
            }
        }
    }

    public void a(e eVar, int i, int i2, int i3) {
        if (i == 0) {
            com.uc.a.e.nR().ob().a(eVar.abM, eVar.abL, eVar.abS);
        }
        if (ModelBrowser.gD() != null) {
            String str = eVar.abL;
            if (6 == eVar.abU) {
                if (!ModelBrowser.gD().iA()) {
                    Toast.makeText(getContext(), (int) R.string.f1033readingmode_tips_enter, 0).show();
                }
                str = b.acY + str;
                if (com.uc.a.e.nR() != null) {
                    com.uc.a.e.nR().ad(true);
                }
            }
            if (1 == eVar.abS) {
                str = "wap:" + str;
            }
            ModelBrowser.gD().a(11, str);
        }
    }

    public void a(e eVar, int i, boolean z) {
        if (i != 1) {
            return;
        }
        if (z) {
            c(eVar.abM, eVar.abL, eVar.abS);
            return;
        }
        com.uc.a.e.nR().oj().c(null, eVar);
        Toast.makeText(getContext(), (int) R.string.f1143del_bookmark_success, 0).show();
    }

    public void a(ViewMainBarMainPage viewMainBarMainPage) {
        this.aeI = viewMainBarMainPage;
        this.aeI.e(this);
    }

    public boolean a(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case com.uc.d.h.aIh /*65537*/:
                Intent intent = new Intent(getContext(), ActivityChooseFile.class);
                intent.putExtra(ActivityChooseFile.uv, 1);
                intent.putExtra(ActivityChooseFile.uw, "/sdcard/UCfavorite");
                intent.putExtra(ActivityChooseFile.uA, (int) R.string.f981bookmark_file_choose_title);
                ((Activity) getContext()).startActivityForResult(intent, 7);
                break;
            case com.uc.d.h.aIg /*65538*/:
                ax("/sdcard/UCfavorite/", "UC书签备份");
                break;
            case com.uc.d.h.aIi /*65539*/:
                Pi();
                break;
            case com.uc.d.h.cck /*65540*/:
                showDialog(3);
                break;
            case com.uc.d.h.ccm /*131073*/:
                if (1 == this.bQY.abS) {
                    ModelBrowser.gD().a(39, 1, "wap:" + this.bQY.abL);
                } else {
                    ModelBrowser.gD().a(39, 1, this.bQY.abL);
                }
                if (this.bQY != null) {
                    com.uc.a.e.nR().ob().a(this.bQY.abM, this.bQY.abL, this.bQY.abS);
                    break;
                }
                break;
            case com.uc.d.h.ccn /*131074*/:
                if (!(ModelBrowser.gD().hj() == null || this.bQY == null)) {
                    if (1 == this.bQY.abS) {
                        ModelBrowser.gD().hj().cI("wap:" + this.bQY.abL);
                    } else {
                        ModelBrowser.gD().hj().cI(this.bQY.abL);
                    }
                    com.uc.a.e.nR().ob().a(this.bQY.abM, this.bQY.abL, this.bQY.abS);
                    break;
                }
            case com.uc.d.h.cco /*131075*/:
                Intent intent2 = new Intent();
                intent2.setAction(ActivityBookmarkEx.ase);
                intent2.setClass(getContext(), ActivityBookmarkEx.class);
                intent2.putExtra(ActivityBookmarkEx.asg, this.bQY.abL);
                intent2.putExtra(ActivityBookmarkEx.asf, this.bQY.abM);
                intent2.putExtra(ActivityBookmarkEx.ash, this.bQY.abR);
                intent2.putExtra(ActivityBookmarkEx.asj, this.bQY.abQ);
                getContext().startActivity(intent2);
                break;
            case com.uc.d.h.ccp /*131076*/:
            case com.uc.d.h.ccw /*196610*/:
                Intent intent3 = new Intent(getContext(), ActivityEditMyNavi.class);
                if (this.bQY != null) {
                    intent3.putExtra(ActivityEditMyNavi.bvj, this.bQY.abM);
                    intent3.putExtra(ActivityEditMyNavi.bvi, this.bQY.abL);
                    intent3.putExtra(ActivityEditMyNavi.bvk, this.bQY.abS);
                }
                intent3.putExtra(ActivityEditMyNavi.bvl, -1);
                getContext().startActivity(intent3);
                break;
            case com.uc.d.h.ccq /*131077*/:
                showDialog(1);
                break;
            case com.uc.d.h.ccr /*131078*/:
                showDialog(0);
                break;
            case com.uc.d.h.ccs /*131079*/:
                showDialog(4);
                break;
            case com.uc.d.h.cct /*131080*/:
                Pj();
                break;
            case com.uc.d.h.ccv /*196609*/:
                if (this.bQY != null) {
                    if (1 == this.bQY.abS) {
                        ModelBrowser.gD().a(39, 1, "wap:" + this.bQY.abL);
                    } else {
                        ModelBrowser.gD().a(39, 1, this.bQY.abL);
                    }
                    com.uc.a.e.nR().ob().a(this.bQY.abM, this.bQY.abL, this.bQY.abS);
                    break;
                }
                break;
            case com.uc.d.h.ccx /*196611*/:
                if (this.bQY != null) {
                    if (this.bQY.abU == 6) {
                        com.uc.a.e.nR().dN(this.bQY.abQ);
                    } else {
                        com.uc.a.e.nR().bn(this.bQY.abL);
                    }
                }
                this.caH.jT();
                break;
        }
        return true;
    }

    public void aX(int i, int i2) {
        if (this.aeI == null) {
            return;
        }
        if (i2 == 0) {
            this.aeI.kk(0);
        } else {
            this.aeI.kk(1);
        }
    }

    public void aw(String str, String str2) {
        Context context = getContext();
        if (str2 == null || str2.length() == 0) {
            Toast.makeText(context, (int) R.string.f1430bookmarkfilenotnull, 0).show();
        } else if (str != null && str2 != null) {
            if (com.uc.a.e.nR().A(str, str2)) {
                ay(str, str2);
            } else if (com.uc.a.e.nR().oj().B(str, str2)) {
                Toast.makeText(context, (int) R.string.f1404exportbookmarksuccess, 1).show();
            } else {
                Toast.makeText(context, (int) R.string.f1406exportbookmarkfail, 0).show();
            }
        }
    }

    public void ay(final String str, final String str2) {
        final Context context = getContext();
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.L(context.getString(R.string.f1398bookmarkfileexists));
        builder.M(context.getString(R.string.f1399confirmcoverbookmarkfile));
        builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (com.uc.a.e.nR().oj().B(str, str2)) {
                    Toast.makeText(context, (int) R.string.f1404exportbookmarksuccess, 1).show();
                } else {
                    Toast.makeText(context, (int) R.string.f1406exportbookmarkfail, 0).show();
                }
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                BookmarkPageView.this.ax(str, str2);
            }
        });
        builder.fh().show();
    }

    public void az(final String str, final String str2) {
        final Context context = getContext();
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.aH(R.string.f1396bookmarkisexists);
        builder.aG(R.string.f1397confirmcoverbookmark);
        builder.a((int) R.string.f1407coversamename, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (com.uc.a.e.nR().oj().x(str, str2)) {
                    Toast.makeText(context, (int) R.string.f1403importbookmarksuccess, 0).show();
                } else {
                    Toast.makeText(context, (int) R.string.f1405importbookmarkfail, 0).show();
                }
                BookmarkPageView.this.caH.jS();
            }
        });
        builder.b((int) R.string.f1408ignoresamename, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (com.uc.a.e.nR().oj().y(str, str2)) {
                    Toast.makeText(context, (int) R.string.f1403importbookmarksuccess, 0).show();
                } else {
                    Toast.makeText(context, (int) R.string.f1405importbookmarkfail, 0).show();
                }
                BookmarkPageView.this.caH.jS();
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.fh().show();
    }

    public void b(e eVar, int i, int i2, int i3) {
        this.bQY = eVar;
        this.caD = i2;
        this.caE = i3;
        if (!eVar.abP || i != 1) {
            a(eVar, i);
        }
    }

    public void b(z zVar, int i) {
        switch (i) {
            case 0:
                if (ModelBrowser.gD() != null) {
                    ModelBrowser.gD().E(true);
                    return;
                }
                return;
            case 1:
                showDialog(2);
                return;
            case 2:
                a((e) null, 4097);
                return;
            case 3:
                F(getContext()).show();
                return;
            default:
                return;
        }
    }

    public void c(e eVar, e eVar2, int i) {
        final Context context = getContext();
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.L(context.getString(R.string.f1396bookmarkisexists));
        builder.M(context.getString(R.string.f1397confirmcoverbookmark));
        final e eVar3 = eVar;
        final e eVar4 = eVar2;
        final int i2 = i;
        builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                int a2 = com.uc.a.e.nR().oj().a(eVar3, eVar4, i2);
                BookmarkPageView.this.caH.jS();
                if (a2 == 0) {
                    if (1 == i2) {
                        Toast.makeText(context, (int) R.string.f1169msg_add_success, 0).show();
                    } else {
                        Toast.makeText(context, (int) R.string.f1173msg_edit_success, 0).show();
                    }
                } else if (1 == a2) {
                    Toast.makeText(context, (int) R.string.f1393bookmarknamenotnull, 0).show();
                } else if (2 == a2) {
                    Toast.makeText(context, (int) R.string.f1394bookmarkvalueotnull, 0).show();
                }
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.fh().show();
    }

    public void c(String str, String str2, byte b2) {
        Context context = getContext();
        if (str == null) {
            Toast.makeText(context, (int) R.string.f1393bookmarknamenotnull, 0).show();
            return;
        }
        String trim = str.trim();
        if (trim.length() == 0) {
            Toast.makeText(context, (int) R.string.f1393bookmarknamenotnull, 0).show();
        } else if (str2 == null) {
            Toast.makeText(context, (int) R.string.f1394bookmarkvalueotnull, 0).show();
        } else {
            String trim2 = str2.trim();
            if (trim2.length() == 0) {
                Toast.makeText(context, (int) R.string.f1394bookmarkvalueotnull, 0).show();
                return;
            }
            e eVar = new e();
            eVar.abM = trim;
            eVar.abL = trim2;
            eVar.abS = b2;
            int a2 = com.uc.a.e.nR().oj().a(null, eVar);
            if (a2 == 0) {
                Toast.makeText(context, (int) R.string.f1169msg_add_success, 0).show();
            } else if (1 == a2) {
                Toast.makeText(context, (int) R.string.f1393bookmarknamenotnull, 0).show();
            } else if (2 == a2) {
                Toast.makeText(context, (int) R.string.f1394bookmarkvalueotnull, 0).show();
            } else if (3 == a2) {
                e(null, eVar);
            }
        }
    }

    public void c(String str, String str2, int i, int i2) {
        Vector ol = this.aso.ol();
        e eVar = new e();
        eVar.abM = str;
        eVar.abL = str2;
        eVar.abQ = i2;
        eVar.abP = false;
        e eVar2 = i == 0 ? null : (e) ol.elementAt(i - 1);
        int b2 = this.aso.b(eVar2, eVar);
        if (b2 == 0) {
            Toast.makeText(getContext(), (int) R.string.f1173msg_edit_success, 0).show();
        } else if (3 == b2) {
            c(eVar2, eVar, 2);
        }
    }

    public void d(String str, String str2, int i) {
        Vector ol = this.aso.ol();
        e eVar = new e();
        eVar.abM = str;
        eVar.abL = str2;
        eVar.abP = false;
        e eVar2 = i == 0 ? null : (e) ol.elementAt(i - 1);
        int a2 = this.aso.a(eVar2, eVar);
        if (a2 == 0) {
            Toast.makeText(getContext(), (int) R.string.f1169msg_add_success, 0).show();
        }
        if (3 == a2) {
            c(eVar2, eVar, 1);
        }
    }

    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (this.caH != null) {
            return this.caH.dispatchKeyEvent(keyEvent);
        }
        return false;
    }

    public void e(final e eVar, final e eVar2) {
        final Context context = getContext();
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(context);
        builder.L(context.getString(R.string.f1396bookmarkisexists));
        builder.M(context.getString(R.string.f1397confirmcoverbookmark));
        builder.a((int) R.string.f1123alert_dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                int a2 = com.uc.a.e.nR().oj().a(eVar, eVar2, 1);
                if (a2 == 0) {
                    Toast.makeText(context, (int) R.string.f1169msg_add_success, 0).show();
                } else if (1 == a2) {
                    Toast.makeText(context, (int) R.string.f1393bookmarknamenotnull, 0).show();
                } else if (2 == a2) {
                    Toast.makeText(context, (int) R.string.f1394bookmarkvalueotnull, 0).show();
                }
            }
        });
        builder.c((int) R.string.f1125alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        UCAlertDialog fh = builder.fh();
        fh.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                BookmarkPageView.this.caH.jT();
            }
        });
        fh.show();
    }

    public void k() {
        com.uc.h.e.Pb();
        this.caH.fT(com.uc.h.e.Pb().getColor(14));
        this.caH.bo(this.caH.Bi());
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        Bundle extras;
        switch (i) {
            case 7:
                if (intent != null && (extras = intent.getExtras()) != null) {
                    String string = extras.getString(ActivityChooseFile.uE);
                    String string2 = extras.getString(ActivityChooseFile.uF);
                    if (string != null && string2 != null) {
                        int z = com.uc.a.e.nR().oj().z(string, string2);
                        if (z == 0) {
                            Toast.makeText(getContext(), (int) R.string.f1403importbookmarksuccess, 0).show();
                        } else if (-1 == z) {
                            Toast.makeText(getContext(), (int) R.string.f1405importbookmarkfail, 0).show();
                        } else if (1 == z) {
                            az(string, string2);
                        }
                        this.caH.jS();
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 0:
                return l(getContext(), this.bQY.abM);
            case 1:
                return C(getContext());
            case 2:
                return k(getContext(), null);
            case 3:
                return E(getContext());
            case 4:
                return D(getContext());
            case 5:
                return A(getContext());
            case 6:
                return B(getContext());
            default:
                return null;
        }
    }

    public void onWindowFocusChanged(boolean z) {
        if (this.aeI != null) {
            this.aeI.Pa();
        }
    }

    public void showDialog(int i) {
        onCreateDialog(i).show();
        switch (i) {
            case 0:
            case 2:
                this.mHandler.sendEmptyMessageDelayed(1, 300);
                return;
            case 1:
            default:
                return;
        }
    }
}
