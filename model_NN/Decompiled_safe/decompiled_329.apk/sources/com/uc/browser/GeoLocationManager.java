package com.uc.browser;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import com.uc.c.cd;
import java.util.ArrayList;
import java.util.List;

public class GeoLocationManager {
    private static CdmaCellLocationEx bc;
    private ActivityBrowser bb;
    List bd;

    class WifiData {
        public String ee;
        public String ef;
        public String eg;
        public String eh;
        public String ei;

        private WifiData() {
        }

        public String cr() {
            return this.ee;
        }

        public String cs() {
            return this.ef;
        }

        public String ct() {
            return this.eg;
        }

        public String cu() {
            return this.eh;
        }

        public String cv() {
            return this.ei;
        }

        public void u(String str) {
            this.ee = str;
        }

        public void v(String str) {
            this.ef = str;
        }

        public void w(String str) {
            this.eg = str;
        }

        public void x(String str) {
            int indexOf = str.indexOf("-");
            this.eh = (indexOf < 0 || str.length() <= indexOf + 1) ? str : str.substring(indexOf + 1);
        }

        public void y(String str) {
            this.ei = str;
        }
    }

    static {
        bd();
    }

    public GeoLocationManager(Context context) {
        try {
            this.bb = (ActivityBrowser) context;
        } catch (Exception e) {
        }
    }

    private void b(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        wifiManager.startScan();
        List<ScanResult> scanResults = wifiManager.getScanResults();
        if (scanResults != null && scanResults.size() > 0) {
            this.bd = new ArrayList();
            for (ScanResult scanResult : scanResults) {
                this.bd.add(d(scanResult.toString()));
            }
        }
    }

    private static void bd() {
        try {
            bc = new CdmaCellLocationEx();
        } catch (VerifyError e) {
        }
    }

    private WifiData d(String str) {
        String[] split = str.split(cd.bVH);
        WifiData wifiData = new WifiData();
        if (split.length == 5) {
            wifiData.u(split[0].substring(split[0].indexOf(cd.bVI) + 1, split[0].length()).trim());
            wifiData.v(split[1].substring(split[1].indexOf(cd.bVI) + 1, split[1].length()).replaceAll(cd.bVI, "").trim());
            wifiData.w(split[2].substring(split[2].indexOf(cd.bVI) + 1, split[2].length()).trim());
            wifiData.x(split[3].substring(split[3].indexOf(cd.bVI) + 1, split[3].length()).trim());
            wifiData.y(split[4].substring(split[4].indexOf(cd.bVI) + 1, split[4].length()).trim());
        }
        return wifiData;
    }

    public WifiData be() {
        if (this.bd == null || this.bd.size() <= 0) {
            return null;
        }
        WifiData wifiData = (WifiData) this.bd.get(0);
        this.bd.remove(0);
        return wifiData;
    }

    /* JADX WARNING: Removed duplicated region for block: B:117:0x01eb A[ADDED_TO_REGION, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x0242 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x009f A[Catch:{ Exception -> 0x01f5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0117  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void f(boolean r25) {
        /*
            r24 = this;
            r0 = r24
            com.uc.browser.ActivityBrowser r0 = r0.bb     // Catch:{ Exception -> 0x01a3 }
            r2 = r0
            if (r2 != 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            r0 = r24
            com.uc.browser.ActivityBrowser r0 = r0.bb     // Catch:{ Exception -> 0x01a3 }
            r2 = r0
            java.lang.String r3 = "phone"
            java.lang.Object r2 = r2.getSystemService(r3)     // Catch:{ Exception -> 0x01a3 }
            android.telephony.TelephonyManager r2 = (android.telephony.TelephonyManager) r2     // Catch:{ Exception -> 0x01a3 }
            if (r2 == 0) goto L_0x0007
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 0
            java.lang.String r15 = "-1"
            r16 = 0
            r21 = r16
            r16 = r3
            r3 = r4
            r4 = r21
            r22 = r6
            r6 = r5
            r5 = r22
        L_0x0033:
            r17 = 4
            r0 = r4
            r1 = r17
            if (r0 >= r1) goto L_0x0254
            java.lang.String r17 = r2.getNetworkOperator()     // Catch:{ Exception -> 0x0231 }
            if (r17 == 0) goto L_0x024e
            int r18 = r17.length()     // Catch:{ Exception -> 0x0231 }
            r19 = 5
            r0 = r18
            r1 = r19
            if (r0 < r1) goto L_0x024e
            r18 = 0
            r19 = 3
            java.lang.String r16 = r17.substring(r18, r19)     // Catch:{ Exception -> 0x0231 }
            r18 = 3
            r19 = 5
            java.lang.String r3 = r17.substring(r18, r19)     // Catch:{ Exception -> 0x0231 }
            r17 = r16
            r16 = r3
        L_0x0060:
            int r3 = r2.getPhoneType()     // Catch:{ Exception -> 0x01e2 }
            r18 = 2
            r0 = r3
            r1 = r18
            if (r0 != r1) goto L_0x01a6
            com.uc.browser.CdmaCellLocationEx r3 = com.uc.browser.GeoLocationManager.bc     // Catch:{ Exception -> 0x01e2 }
            if (r3 == 0) goto L_0x01a6
            com.uc.browser.CdmaCellLocationEx r3 = com.uc.browser.GeoLocationManager.bc     // Catch:{ Exception -> 0x01e2 }
            android.telephony.CellLocation r18 = r2.getCellLocation()     // Catch:{ Exception -> 0x01e2 }
            r0 = r3
            r1 = r18
            int r3 = r0.a(r1)     // Catch:{ Exception -> 0x01e2 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x01e2 }
            com.uc.browser.CdmaCellLocationEx r6 = com.uc.browser.GeoLocationManager.bc     // Catch:{ Exception -> 0x0238 }
            android.telephony.CellLocation r18 = r2.getCellLocation()     // Catch:{ Exception -> 0x0238 }
            r0 = r6
            r1 = r18
            int r6 = r0.b(r1)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r5 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x0238 }
            r21 = r5
            r5 = r3
            r3 = r21
        L_0x0096:
            r0 = r15
            r1 = r17
            boolean r6 = r0.equals(r1)     // Catch:{ Exception -> 0x01f5 }
            if (r6 != 0) goto L_0x01eb
            boolean r6 = r15.equals(r16)     // Catch:{ Exception -> 0x01f5 }
            if (r6 != 0) goto L_0x01eb
            boolean r6 = r15.equals(r5)     // Catch:{ Exception -> 0x01f5 }
            if (r6 != 0) goto L_0x01eb
            boolean r6 = r15.equals(r3)     // Catch:{ Exception -> 0x01f5 }
            if (r6 != 0) goto L_0x01eb
            r2 = r3
            r4 = r16
            r3 = r5
            r5 = r17
        L_0x00b7:
            r6 = r2
            r21 = r3
            r3 = r5
            r5 = r21
        L_0x00bd:
            r2 = 0
            r16 = r7
            r7 = r2
        L_0x00c1:
            r2 = 4
            if (r7 >= r2) goto L_0x0242
            r0 = r24
            com.uc.browser.ActivityBrowser r0 = r0.bb     // Catch:{ Exception -> 0x022c }
            r2 = r0
            java.lang.String r17 = "location"
            r0 = r2
            r1 = r17
            java.lang.Object r2 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x022c }
            android.location.LocationManager r2 = (android.location.LocationManager) r2     // Catch:{ Exception -> 0x022c }
            java.lang.String r17 = "gps"
            r0 = r2
            r1 = r17
            android.location.Location r2 = r0.getLastKnownLocation(r1)     // Catch:{ Exception -> 0x022c }
            if (r2 == 0) goto L_0x0204
            double r17 = r2.getLongitude()     // Catch:{ Exception -> 0x022c }
            r19 = 4689928365371555840(0x4115f90000000000, double:360000.0)
            double r17 = r17 * r19
            r0 = r17
            int r0 = (int) r0     // Catch:{ Exception -> 0x022c }
            r17 = r0
            java.lang.String r16 = java.lang.String.valueOf(r17)     // Catch:{ Exception -> 0x022c }
            double r17 = r2.getLatitude()     // Catch:{ Exception -> 0x022c }
            r19 = 4689928365371555840(0x4115f90000000000, double:360000.0)
            double r17 = r17 * r19
            r0 = r17
            int r0 = (int) r0     // Catch:{ Exception -> 0x022c }
            r2 = r0
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x022c }
            boolean r8 = r15.equals(r16)     // Catch:{ Exception -> 0x020e }
            if (r8 != 0) goto L_0x0247
            boolean r8 = r15.equals(r2)     // Catch:{ Exception -> 0x020e }
            if (r8 != 0) goto L_0x0247
            r7 = r16
        L_0x0114:
            r8 = r2
        L_0x0115:
            if (r25 == 0) goto L_0x0196
            r2 = 0
            r21 = r14
            r14 = r9
            r9 = r21
            r22 = r12
            r12 = r11
            r11 = r22
            r23 = r10
            r10 = r13
            r13 = r23
        L_0x0127:
            r16 = 4
            r0 = r2
            r1 = r16
            if (r0 >= r1) goto L_0x023a
            r0 = r24
            com.uc.browser.ActivityBrowser r0 = r0.bb     // Catch:{ Exception -> 0x0218 }
            r16 = r0
            r0 = r24
            r1 = r16
            r0.b(r1)     // Catch:{ Exception -> 0x0218 }
            com.uc.browser.GeoLocationManager$WifiData r16 = r24.be()     // Catch:{ Exception -> 0x0218 }
            if (r16 == 0) goto L_0x0214
            java.lang.String r14 = r16.cs()     // Catch:{ Exception -> 0x0218 }
            java.lang.String r13 = r16.cu()     // Catch:{ Exception -> 0x0218 }
            com.uc.browser.GeoLocationManager$WifiData r16 = r24.be()     // Catch:{ Exception -> 0x0218 }
            java.lang.String r12 = r16.cs()     // Catch:{ Exception -> 0x0218 }
            java.lang.String r11 = r16.cu()     // Catch:{ Exception -> 0x0218 }
            com.uc.browser.GeoLocationManager$WifiData r16 = r24.be()     // Catch:{ Exception -> 0x0218 }
            java.lang.String r10 = r16.cs()     // Catch:{ Exception -> 0x0218 }
            java.lang.String r9 = r16.cu()     // Catch:{ Exception -> 0x0218 }
            boolean r16 = r15.equals(r14)     // Catch:{ Exception -> 0x0218 }
            if (r16 != 0) goto L_0x0214
            boolean r16 = r15.equals(r13)     // Catch:{ Exception -> 0x0218 }
            if (r16 != 0) goto L_0x0214
            boolean r16 = r15.equals(r12)     // Catch:{ Exception -> 0x0218 }
            if (r16 != 0) goto L_0x0214
            boolean r16 = r15.equals(r11)     // Catch:{ Exception -> 0x0218 }
            if (r16 != 0) goto L_0x0214
            boolean r16 = r15.equals(r10)     // Catch:{ Exception -> 0x0218 }
            if (r16 != 0) goto L_0x0214
            boolean r16 = r15.equals(r9)     // Catch:{ Exception -> 0x0218 }
            if (r16 != 0) goto L_0x0214
            r2 = r9
            r9 = r10
            r10 = r11
            r11 = r12
            r12 = r13
            r13 = r14
        L_0x018b:
            r14 = r2
            r21 = r9
            r9 = r13
            r13 = r21
            r22 = r12
            r12 = r10
            r10 = r22
        L_0x0196:
            com.uc.a.e r2 = com.uc.a.e.nR()     // Catch:{ Exception -> 0x01a3 }
            r16 = 1
            r15 = r25
            r2.a(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16)     // Catch:{ Exception -> 0x01a3 }
            goto L_0x0007
        L_0x01a3:
            r2 = move-exception
            goto L_0x0007
        L_0x01a6:
            int r3 = r2.getPhoneType()     // Catch:{ Exception -> 0x01e2 }
            r18 = 1
            r0 = r3
            r1 = r18
            if (r0 != r1) goto L_0x024a
            android.telephony.CellLocation r3 = r2.getCellLocation()     // Catch:{ Exception -> 0x01e2 }
            if (r3 != 0) goto L_0x01c3
            r3 = 0
            r6 = r3
        L_0x01b9:
            android.telephony.CellLocation r3 = r2.getCellLocation()     // Catch:{ Exception -> 0x01e2 }
            if (r3 != 0) goto L_0x01d3
            r3 = 0
        L_0x01c0:
            r5 = r6
            goto L_0x0096
        L_0x01c3:
            android.telephony.CellLocation r3 = r2.getCellLocation()     // Catch:{ Exception -> 0x01e2 }
            android.telephony.gsm.GsmCellLocation r3 = (android.telephony.gsm.GsmCellLocation) r3     // Catch:{ Exception -> 0x01e2 }
            int r3 = r3.getLac()     // Catch:{ Exception -> 0x01e2 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x01e2 }
            r6 = r3
            goto L_0x01b9
        L_0x01d3:
            android.telephony.CellLocation r3 = r2.getCellLocation()     // Catch:{ Exception -> 0x01e2 }
            android.telephony.gsm.GsmCellLocation r3 = (android.telephony.gsm.GsmCellLocation) r3     // Catch:{ Exception -> 0x01e2 }
            int r3 = r3.getCid()     // Catch:{ Exception -> 0x01e2 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x01e2 }
            goto L_0x01c0
        L_0x01e2:
            r3 = move-exception
            r3 = r6
        L_0x01e4:
            r21 = r5
            r5 = r3
            r3 = r21
            goto L_0x0096
        L_0x01eb:
            int r4 = r4 + 1
            r6 = r5
            r5 = r3
            r3 = r16
            r16 = r17
            goto L_0x0033
        L_0x01f5:
            r2 = move-exception
            r2 = r3
            r4 = r16
            r3 = r5
            r5 = r17
        L_0x01fc:
            r6 = r2
            r21 = r3
            r3 = r5
            r5 = r21
            goto L_0x00bd
        L_0x0204:
            r2 = r8
            r8 = r16
        L_0x0207:
            int r7 = r7 + 1
            r16 = r8
            r8 = r2
            goto L_0x00c1
        L_0x020e:
            r7 = move-exception
            r7 = r16
        L_0x0211:
            r8 = r2
            goto L_0x0115
        L_0x0214:
            int r2 = r2 + 1
            goto L_0x0127
        L_0x0218:
            r2 = move-exception
            r2 = r9
            r9 = r10
            r10 = r11
            r11 = r12
            r12 = r13
            r13 = r14
            r14 = r2
            r21 = r9
            r9 = r13
            r13 = r21
            r22 = r12
            r12 = r10
            r10 = r22
            goto L_0x0196
        L_0x022c:
            r2 = move-exception
            r2 = r8
            r7 = r16
            goto L_0x0211
        L_0x0231:
            r2 = move-exception
            r2 = r5
            r4 = r3
            r3 = r6
            r5 = r16
            goto L_0x01fc
        L_0x0238:
            r6 = move-exception
            goto L_0x01e4
        L_0x023a:
            r2 = r9
            r9 = r10
            r10 = r11
            r11 = r12
            r12 = r13
            r13 = r14
            goto L_0x018b
        L_0x0242:
            r2 = r8
            r7 = r16
            goto L_0x0114
        L_0x0247:
            r8 = r16
            goto L_0x0207
        L_0x024a:
            r3 = r5
            r5 = r6
            goto L_0x0096
        L_0x024e:
            r17 = r16
            r16 = r3
            goto L_0x0060
        L_0x0254:
            r2 = r5
            r4 = r3
            r3 = r6
            r5 = r16
            goto L_0x00b7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.browser.GeoLocationManager.f(boolean):void");
    }
}
