package com.uc.browser;

import android.content.Context;
import android.graphics.Rect;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import com.uc.browser.UCR;
import com.uc.e.b.h;
import com.uc.h.e;

public class DriftEditTextForRelativeLayout extends UCEditText implements TextWatcher {
    private boolean aDS;
    private DriftEditTextBinder cgA;
    private InputMethodManager cgB = ((InputMethodManager) getContext().getSystemService("input_method"));
    private int textColor;

    public DriftEditTextForRelativeLayout(Context context) {
        super(context);
        setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void a() {
        e Pb = e.Pb();
        setHighlightColor(Pb.getColor(0));
        setBackgroundDrawable(Pb.getDrawable(UCR.drawable.aWZ));
        setPadding(Pb.kp(R.dimen.f167edittext_padding_top), getPaddingTop(), Pb.kp(R.dimen.f167edittext_padding_top), getPaddingBottom());
    }

    public void a(int i, int i2, int i3, int i4, boolean z, float f, String str) {
        a(i, i2, i3, i4, z, f, str, 0, 0);
    }

    public void a(int i, int i2, int i3, int i4, boolean z, float f, String str, int i5, int i6) {
        a(i, i2, i3, i4, z, f, str, i5, i6, false, -1);
    }

    public void a(int i, int i2, int i3, int i4, boolean z, float f, String str, int i5, int i6, boolean z2, int i7) {
        a(i, i2, i3, i4, z, f, str, i5, i6, z2, i7, -1, -16777216);
    }

    public void a(int i, int i2, int i3, int i4, boolean z, float f, String str, int i5, int i6, boolean z2, int i7, int i8, int i9) {
        setText(str);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) getLayoutParams();
        layoutParams.setMargins(i + 1, i2 + 1, 0, 0);
        layoutParams.height = i4 - 2;
        layoutParams.width = i3 - 2;
        setTextSize(0, f);
        if (i7 > 0) {
            setFilters(new InputFilter[]{new InputFilter.LengthFilter(i7)});
            this.aDR = i7;
        }
        setSingleLine(z);
        this.aDS = z;
        setVisibility(0);
        if (z) {
            setHorizontallyScrolling(true);
            bb(true);
        } else {
            bb(false);
        }
        setWidth(i3);
        setPadding(i6, i5, i6, 0);
        if (z2) {
            setTransformationMethod(new PasswordTransformationMethod());
        } else {
            setTransformationMethod(null);
        }
        if (z) {
            setGravity(19);
            setImeOptions(6);
        } else {
            setGravity(51);
            setImeOptions(1073741830);
        }
        requestFocus();
        bringToFront();
        this.cgB.showSoftInput(this, 0);
        e Pb = e.Pb();
        setBackgroundDrawable(new h(0.0f, 0, Pb.getColor(120), Pb.getColor(121), Pb.kq(R.dimen.f540webwidget_frame_shadow_bold), Pb.kq(R.dimen.f537webwidget_frame_corner)));
        setTextColor(i9);
        this.textColor = i9;
        setSelectAllOnFocus(true);
        setHighlightColor(com.uc.a.e.nR().bw(com.uc.a.h.afN).contains(com.uc.a.e.RD) ? -14798011 : -5583119);
        setSelection(0, getText().length());
    }

    public void a(DriftEditTextBinder driftEditTextBinder) {
        this.cgA = driftEditTextBinder;
    }

    public void afterTextChanged(Editable editable) {
    }

    public void bI(int i, int i2) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) getLayoutParams();
        layoutParams.setMargins(i + 1, i2 + 1, 0, 0);
        setLayoutParams(layoutParams);
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void cS(boolean z) {
        setVisibility(8);
        if (true == z) {
            this.cgB.hideSoftInputFromWindow(getWindowToken(), 0);
            if (this.cgA != null) {
                this.cgA.a(getText());
            }
        }
    }

    public void cT(boolean z) {
        setVisibility(8);
        if (true == z && this.cgA != null) {
            this.cgA.a(getText());
        }
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean z, int i, Rect rect) {
        super.onFocusChanged(z, i, rect);
        if (!z) {
            setTextColor(this.textColor);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        boolean onKeyDown = super.onKeyDown(i, keyEvent);
        int keyCode = keyEvent.getKeyCode();
        if (keyCode == 4 || keyCode == 5 || keyCode == 82 || keyCode == 23 || keyCode == 84) {
            cS(true);
        } else if (keyCode == 66) {
            if (this.aDS) {
                cS(true);
            }
        } else if ((keyCode == 19 || keyCode == 20) && (this.aDS || !onKeyDown)) {
            cS(true);
        }
        return onKeyDown;
    }

    /* access modifiers changed from: protected */
    public void onSelectionChanged(int i, int i2) {
        if ((i != 0 || i2 != getEditableText().length() || i2 == 0) && (i == 0 || i != getEditableText().length() || i2 != 0)) {
            setTextColor(this.textColor);
        } else if (isFocused()) {
            setTextColor(com.uc.a.e.nR().bw(com.uc.a.h.afN).contains(com.uc.a.e.RD) ? this.textColor : -14013910);
        }
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (ModelBrowser.gD() != null && getVisibility() == 0) {
            ModelBrowser.gD().a(37, getText());
        }
    }

    public void show() {
        setTextColor(0);
        a(0, 0, 20, 100, true, 13.0f, "", 0, 0);
    }
}
