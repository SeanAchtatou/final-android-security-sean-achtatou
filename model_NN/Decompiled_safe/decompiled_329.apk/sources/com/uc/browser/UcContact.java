package com.uc.browser;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Contacts;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class UcContact {
    private static final int UB = 1;
    private static final int UC = 2;
    private static final int UD = 4;
    private static final int UE = 8;
    private static final Uri UF = Uri.parse("content://icc/adn");
    /* access modifiers changed from: private */
    public static final Uri UG = Uri.parse("content://sim/adn");
    /* access modifiers changed from: private */
    public static final Uri UH = Contacts.Phones.CONTENT_URI;
    /* access modifiers changed from: private */
    public static final Uri UI = Uri.parse("content://com.android.contacts/data/phones");
    /* access modifiers changed from: private */
    public boolean UA = false;
    /* access modifiers changed from: private */
    public final String[] UJ = {"number"};
    /* access modifiers changed from: private */
    public final String[] UK = {"number"};
    /* access modifiers changed from: private */
    public final String[] UL = {"data1"};
    /* access modifiers changed from: private */
    public boolean UM = false;
    /* access modifiers changed from: private */
    public boolean UN = false;
    /* access modifiers changed from: private */
    public CALLBACK Ux;
    private ContentResolver Uy;
    private QueryHandler Uz;

    public interface CALLBACK {
        void e(List list);
    }

    class QueryHandler extends AsyncQueryHandler {
        public QueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.uc.browser.UcContact.a(com.uc.browser.UcContact, boolean):boolean
         arg types: [com.uc.browser.UcContact, int]
         candidates:
          com.uc.browser.UcContact.a(com.uc.browser.UcContact, com.uc.browser.UcContact$CALLBACK):com.uc.browser.UcContact$CALLBACK
          com.uc.browser.UcContact.a(com.uc.browser.UcContact, boolean):boolean */
        /* access modifiers changed from: protected */
        public void onQueryComplete(int i, Object obj, Cursor cursor) {
            if (!UcContact.this.UA) {
                ArrayList arrayList = obj != null ? (ArrayList) obj : new ArrayList();
                if (cursor != null) {
                    boolean moveToFirst = cursor.moveToFirst();
                    while (moveToFirst) {
                        try {
                            int columnIndex = cursor.getColumnIndex("number");
                            if (columnIndex < 0) {
                                columnIndex = cursor.getColumnIndex("data1");
                            }
                            if (columnIndex >= 0) {
                                arrayList.add(cursor.getString(columnIndex));
                            }
                        } catch (Exception e) {
                        }
                        moveToFirst = cursor.moveToNext();
                    }
                }
                switch (i) {
                    case 1:
                        if (cursor != null && cursor.getCount() != 0) {
                            boolean unused = UcContact.this.UM = false;
                            break;
                        } else {
                            startQuery(8, arrayList, UcContact.UG, UcContact.this.UJ, null, null, null);
                            return;
                        }
                    case 2:
                        if (cursor != null && cursor.getCount() != 0) {
                            boolean unused2 = UcContact.this.UN = false;
                            break;
                        } else {
                            startQuery(4, arrayList, UcContact.UI, UcContact.this.UL, null, null, null);
                            return;
                        }
                    case 4:
                        boolean unused3 = UcContact.this.UN = false;
                        break;
                    case 8:
                        boolean unused4 = UcContact.this.UM = false;
                        break;
                }
                if (UcContact.this.UN) {
                    startQuery(2, arrayList, UcContact.UH, UcContact.this.UK, null, null, null);
                } else if (UcContact.this.Ux != null) {
                    CALLBACK f = UcContact.this.Ux;
                    CALLBACK unused5 = UcContact.this.Ux = (CALLBACK) null;
                    f.e(arrayList);
                }
            }
        }
    }

    public UcContact(ContentResolver contentResolver, CALLBACK callback) {
        this.Ux = callback;
        this.Uy = contentResolver;
        this.Uz = new QueryHandler(this.Uy);
    }

    private void rZ() {
        new Timer().schedule(new TimerTask() {
            public void run() {
                boolean unused = UcContact.this.UA = true;
                if (UcContact.this.Ux != null) {
                    UcContact.this.Ux.e(new ArrayList());
                }
            }
        }, 10000);
    }

    public void rV() {
        this.UM = true;
        this.Uz.startQuery(1, null, UF, this.UJ, null, null, null);
        rZ();
    }

    public void rW() {
        this.UN = true;
        this.Uz.startQuery(2, null, UH, this.UK, null, null, null);
        rZ();
    }

    public void rX() {
        this.UM = true;
        this.UN = true;
        this.Uz.startQuery(1, null, UF, this.UJ, null, null, null);
        rZ();
    }

    public boolean rY() {
        return this.UA;
    }
}
