package com.uc.browser;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import com.uc.browser.UCR;
import com.uc.d.c;
import com.uc.d.f;
import com.uc.d.g;
import com.uc.e.a.d;
import com.uc.e.ad;
import com.uc.e.h;
import com.uc.e.s;
import com.uc.e.v;
import com.uc.e.w;
import com.uc.e.z;
import com.uc.h.a;
import com.uc.h.e;
import java.util.ArrayList;
import java.util.Vector;

public class BuzLayout extends View implements v, w, a {
    private c lE;
    private f lF;
    /* access modifiers changed from: private */
    public BuzData oO = null;
    /* access modifiers changed from: private */
    public s oP;
    private e oQ;
    Drawable[] oR = null;
    Drawable oS = null;
    Drawable oT = null;
    private ArrayList oU;
    private Drawable[] oV;
    private String oW = null;
    private h oX = new h() {
        public void b(z zVar, int i) {
            String[] strArr;
            if (BuzLayout.this.oP != null) {
                Vector nN = BuzLayout.this.oO.nN();
                if (i < nN.size()) {
                    Vector vector = (Vector) nN.get(i);
                    if (vector.size() >= 5) {
                        strArr = (String[]) vector.get(4);
                        if (strArr != null && ModelBrowser.gD() != null) {
                            ModelBrowser.gD().a(11, strArr[0]);
                        }
                        return;
                    }
                }
                strArr = null;
                if (strArr != null) {
                    ModelBrowser.gD().a(11, strArr[0]);
                }
            }
        }
    };

    public BuzLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    private void a() {
        this.oP = new s();
        this.oQ = e.Pb();
        e.Pb().a(this);
        this.oP.fs(this.oQ.kp(R.dimen.f377channel_item_height));
        this.oP.setDividerHeight(1);
        this.oP.ft(e.Pb().kp(R.dimen.f166list_scrollbar_size));
        this.oP.a((w) this);
        this.oP.a(new ad() {
            public void cJ() {
                BuzLayout.this.postInvalidate();
            }
        });
        this.oP.c(this.oX);
        ds();
        k();
    }

    private void c(Canvas canvas) {
        Resources resources = getContext().getResources();
        Bitmap kl = this.oQ.kl(UCR.drawable.aWa);
        int height = ((getHeight() - kl.getHeight()) / 2) - (kl.getHeight() / 2);
        if (height < 0) {
            height += kl.getHeight() / 4;
        }
        int dimensionPixelOffset = resources.getDimensionPixelOffset(R.dimen.f390network_error_font_size);
        int dimensionPixelOffset2 = resources.getDimensionPixelOffset(R.dimen.f391network_check_guide_font_size);
        int i = dimensionPixelOffset / 2;
        canvas.drawBitmap(kl, (float) ((getWidth() - kl.getWidth()) / 2), (float) height, (Paint) null);
        Paint paint = new Paint();
        paint.setColor(this.oQ.getColor(84));
        paint.setTextSize((float) dimensionPixelOffset);
        paint.setAntiAlias(true);
        String string = resources.getString(R.string.f1485network_error);
        String string2 = resources.getString(R.string.f1486buz_channel_load_failed);
        String string3 = resources.getString(R.string.f1488network_check_guid);
        float measureText = paint.measureText(string);
        float measureText2 = paint.measureText(string2);
        int height2 = kl.getHeight() + height + (i * 2);
        canvas.drawText(string, (((float) getWidth()) - measureText) / 2.0f, (float) height2, paint);
        int i2 = dimensionPixelOffset + i + height2;
        canvas.drawText(string2, (((float) getWidth()) - measureText2) / 2.0f, (float) i2, paint);
        paint.setColor(this.oQ.getColor(85));
        int i3 = i2 + i;
        paint.setTextSize((float) dimensionPixelOffset2);
        float measureText3 = paint.measureText(string3);
        canvas.drawLine((((float) getWidth()) - measureText3) / 2.0f, (float) i3, (((float) getWidth()) + measureText3) / 2.0f, (float) i3, paint);
        paint.setColor(this.oQ.getColor(86));
        canvas.drawText(string3, (((float) getWidth()) - measureText3) / 2.0f, (float) ((i * 2) + i3), paint);
    }

    public void a(z zVar, int i) {
        this.oW = null;
        if (this.oP != null) {
            Vector nN = this.oO.nN();
            if (i < nN.size()) {
                Vector vector = (Vector) nN.get(i);
                if (vector.size() >= 5) {
                    this.oW = ((String[]) vector.get(4))[0];
                }
            }
        }
        if (this.oW != null) {
            dt();
        }
    }

    public String bi() {
        return this.oW;
    }

    public void c(z zVar) {
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return this.oP.c(keyEvent);
    }

    public void dr() {
        requestLayout();
    }

    public void ds() {
        if (this.oO != null) {
            this.oO.recycle();
        }
        if (this.oP != null) {
            this.oP.clear();
        }
        this.oO = new BuzData();
        Vector nN = this.oO.nN();
        if (nN != null) {
            new Vector();
            if (this.oU == null) {
                this.oU = new ArrayList();
            }
            this.oU.clear();
            int kp = this.oQ.kp(R.dimen.f369channel_padding_left);
            int kp2 = this.oQ.kp(R.dimen.f370channel_padding_right);
            int kp3 = this.oQ.kp(R.dimen.f374channel_item_padding_top);
            int kp4 = this.oQ.kp(R.dimen.f374channel_item_padding_top);
            int kp5 = this.oQ.kp(R.dimen.f370channel_padding_right);
            int kp6 = this.oQ.kp(R.dimen.f369channel_padding_left);
            int size = nN.size();
            int color = this.oQ.getColor(89);
            int color2 = this.oQ.getColor(90);
            int kp7 = this.oQ.kp(R.dimen.f378channel_item_title_textsize);
            int color3 = this.oQ.getColor(91);
            int color4 = this.oQ.getColor(92);
            int kp8 = this.oQ.kp(R.dimen.f379channel_item_subtitle_textsize);
            int color5 = this.oQ.getColor(93);
            int color6 = this.oQ.getColor(94);
            int kp9 = this.oQ.kp(R.dimen.f380channel_item_describe_textsize);
            d.at(color, color2);
            d.au(color3, color4);
            d.av(color5, color6);
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < size) {
                    Vector vector = (Vector) nN.get(i2);
                    if (vector != null && vector.size() >= 5) {
                        byte[] bArr = (byte[]) vector.get(2);
                        try {
                            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
                            d dVar = new d();
                            this.oU.add(dVar);
                            dVar.y(kp7);
                            dVar.ew(kp8);
                            dVar.ex(kp9);
                            dVar.as(-1, -1);
                            dVar.ev(kp6);
                            dVar.eu(kp5);
                            dVar.setPadding(kp, kp3, kp2, kp4);
                            dVar.c(decodeByteArray);
                            dVar.t((String) vector.get(0));
                            String[] strArr = (String[]) vector.get(3);
                            dVar.co(strArr[0]);
                            dVar.cp(strArr[1]);
                            this.oP.a(dVar);
                        } catch (Throwable th) {
                        }
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public void dt() {
        if (this.lE == null) {
            this.lE = new c(getContext());
        }
        this.lE.clear();
        if (this.lF == null) {
            this.lF = new f(getContext());
            this.lF.a();
        }
        this.lF.a(this.lE);
        this.lE.a(this.lF);
        this.lF.a((ActivityBrowser) getContext());
        this.lE.cV();
        this.lF.show();
        g.a(getContext(), com.uc.d.h.ccD, this.lE);
        c cVar = this.lE;
        if (ModelBrowser.gD().hj().wh() <= 1) {
            cVar.findItem(com.uc.d.h.ccC).setVisible(false);
        } else {
            cVar.findItem(com.uc.d.h.ccC).setVisible(true);
        }
        if (ModelBrowser.gD() != null && ModelBrowser.gD().hj() != null) {
            if (ModelBrowser.gD().hj().wg()) {
                cVar.findItem(com.uc.d.h.ccA).setVisible(true);
                cVar.findItem(com.uc.d.h.ccz).setVisible(true);
                cVar.findItem(com.uc.d.h.ccB).setVisible(true);
                return;
            }
            cVar.findItem(com.uc.d.h.ccA).setVisible(false);
            cVar.findItem(com.uc.d.h.ccz).setVisible(false);
            cVar.findItem(com.uc.d.h.ccB).setVisible(false);
        }
    }

    public void k() {
        if (this.oP != null) {
            if (this.oV == null) {
                this.oV = new Drawable[4];
            }
            this.oV[1] = this.oQ.getDrawable(UCR.drawable.aXu);
            this.oV[2] = this.oQ.getDrawable(UCR.drawable.aXu);
            this.oP.f(this.oV);
            this.oP.setDivider(new ColorDrawable(this.oQ.getColor(95)));
            this.oP.c(this.oX);
            setBackgroundDrawable(this.oQ.getDrawable(UCR.drawable.aWz));
            int color = this.oQ.getColor(89);
            int color2 = this.oQ.getColor(90);
            int color3 = this.oQ.getColor(91);
            int color4 = this.oQ.getColor(92);
            int color5 = this.oQ.getColor(93);
            int color6 = this.oQ.getColor(94);
            d.at(color, color2);
            d.au(color3, color4);
            d.av(color5, color6);
            d.l(this.oQ.getDrawable(UCR.drawable.aTB));
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.translate((float) getPaddingLeft(), (float) getPaddingTop());
        canvas.clipRect(0, 0, (getWidth() - getPaddingRight()) - getPaddingLeft(), (getHeight() - getPaddingBottom()) - getPaddingTop());
        if (this.oO == null || this.oO.nN() == null || this.oO.nN().size() <= 0) {
            c(canvas);
        } else {
            this.oP.onDraw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.oP.setSize(((i3 - i) - getPaddingLeft()) - getPaddingRight(), ((i4 - i2) - getPaddingTop()) - getPaddingBottom());
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                return this.oP.b((byte) 0, (int) motionEvent.getX(), (int) motionEvent.getY());
            case 1:
                return this.oP.b((byte) 1, (int) motionEvent.getX(), (int) motionEvent.getY());
            case 2:
                return this.oP.b((byte) 2, (int) motionEvent.getX(), (int) motionEvent.getY());
            default:
                return false;
        }
    }
}
