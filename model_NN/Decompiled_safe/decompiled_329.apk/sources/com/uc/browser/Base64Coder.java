package com.uc.browser;

import com.uc.c.ca;

public class Base64Coder {
    private static char[] Gi = new char[64];
    private static byte[] Gj = new byte[128];

    static {
        char c = 'A';
        int i = 0;
        while (c <= 'Z') {
            Gi[i] = c;
            c = (char) (c + 1);
            i++;
        }
        char c2 = 'a';
        while (c2 <= 'z') {
            Gi[i] = c2;
            c2 = (char) (c2 + 1);
            i++;
        }
        char c3 = ca.bNQ;
        while (c3 <= '9') {
            Gi[i] = c3;
            c3 = (char) (c3 + 1);
            i++;
        }
        int i2 = i + 1;
        Gi[i] = '+';
        int i3 = i2 + 1;
        Gi[i2] = '/';
        for (int i4 = 0; i4 < Gj.length; i4++) {
            Gj[i4] = -1;
        }
        for (int i5 = 0; i5 < 64; i5++) {
            Gj[Gi[i5]] = (byte) i5;
        }
    }

    public static char[] J(byte[] bArr) {
        return h(bArr, 0, bArr.length);
    }

    public static char[] h(byte[] bArr, int i, int i2) {
        int i3;
        byte b2;
        int i4;
        byte b3;
        int i5 = ((i2 * 4) + 2) / 3;
        char[] cArr = new char[(((i2 + 2) / 3) * 4)];
        int i6 = i + i2;
        int i7 = 0;
        int i8 = i;
        while (i8 < i6) {
            int i9 = i8 + 1;
            byte b4 = bArr[i8] & 255;
            if (i9 < i6) {
                i3 = i9 + 1;
                b2 = bArr[i9] & 255;
            } else {
                i3 = i9;
                b2 = 0;
            }
            if (i3 < i6) {
                i4 = i3 + 1;
                b3 = bArr[i3] & 255;
            } else {
                i4 = i3;
                b3 = 0;
            }
            int i10 = b4 >>> 2;
            int i11 = ((b4 & 3) << 4) | (b2 >>> 4);
            int i12 = ((b2 & 15) << 2) | (b3 >>> 6);
            byte b5 = b3 & 63;
            int i13 = i7 + 1;
            cArr[i7] = Gi[i10];
            int i14 = i13 + 1;
            cArr[i13] = Gi[i11];
            cArr[i14] = i14 < i5 ? Gi[i12] : '=';
            int i15 = i14 + 1;
            cArr[i15] = i15 < i5 ? Gi[b5] : '=';
            i7 = i15 + 1;
            i8 = i4;
        }
        return cArr;
    }
}
