package com.uc.browser;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ProgressBar;
import com.uc.browser.UCR;
import com.uc.h.a;
import com.uc.h.e;

public class LoadingProgressBar extends ProgressBar implements a {
    private Drawable bip;
    private Drawable cx;

    public LoadingProgressBar(Context context) {
        super(context);
        k();
        e.Pb().a(this);
    }

    public LoadingProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        k();
        e.Pb().a(this);
    }

    public LoadingProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        k();
        e.Pb().a(this);
    }

    public void k() {
        this.cx = e.Pb().getDrawable(UCR.drawable.aWy);
        this.bip = e.Pb().getDrawable(UCR.drawable.aWx);
    }

    /* access modifiers changed from: protected */
    public synchronized void onDraw(Canvas canvas) {
        Rect clipBounds = canvas.getClipBounds();
        if (!(clipBounds.bottom - clipBounds.top == 0 || clipBounds.right - clipBounds.left == 0)) {
            if (this.cx != null) {
                this.cx.setBounds(0, 0, getWidth(), getHeight());
                this.cx.draw(canvas);
            }
            if (this.bip != null) {
                int progress = getProgress();
                this.bip.setBounds(0, 0, (progress * getWidth()) / getMax(), getHeight());
                this.bip.draw(canvas);
            }
        }
    }
}
