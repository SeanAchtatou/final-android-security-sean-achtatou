package com.uc.browser;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class JavaScriptAndroidBridge {
    private UzoneCamera ceb = new UzoneCamera((ActivityBrowser) this.mContext, this.ced);
    private UzoneFileUpload cec = new UzoneFileUpload((ActivityBrowser) this.mContext, this.ced);
    private WebViewZoom ced;
    private UcContactForWebKit cee;
    private Context mContext;
    private int type;

    public JavaScriptAndroidBridge(Context context, WebViewZoom webViewZoom) {
        this.mContext = context;
        this.ced = webViewZoom;
    }

    private boolean PJ() {
        String url = this.ced.getUrl();
        if (url == null) {
            return false;
        }
        url.toLowerCase();
        return url.contains("uc.cn") || url.contains("ucweb.com");
    }

    public void fileUpload(String str) {
        this.cec.I(str);
    }

    public void fileUpload(String str, int i) {
        this.type = i;
        switch (i) {
            case 1:
                this.cec.fileUpload(str);
                return;
            case 2:
                this.ceb.cn(str);
                return;
            default:
                return;
        }
    }

    public String getContactList() {
        if (!PJ()) {
            return null;
        }
        if (this.cee != null) {
            return this.cee.getContactList();
        }
        return null;
    }

    public String getFileContent() {
        return this.type == 2 ? this.ceb.tR() : this.cec.getFileContent();
    }

    public String getFileName() {
        return this.type == 2 ? this.ceb.tP() : this.cec.getFileName();
    }

    public int getFileSize() {
        return this.type == 2 ? this.ceb.tS() : this.cec.getFileSize();
    }

    public String getFileType() {
        return this.type == 2 ? this.ceb.tQ() : this.cec.getFileType();
    }

    public void playFlash(String str) {
        String zK = this.ced.zK();
        Bundle bundle = new Bundle();
        bundle.putString(ActivityFlash.cak, str);
        bundle.putString(ActivityFlash.cal, zK);
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(115, bundle);
        }
    }

    public void processFileData(Intent intent) {
        if (this.type != 2) {
            this.cec.h(intent);
        }
    }

    public void processPicData(Intent intent) {
        this.ceb.h(intent);
    }

    public void scanContactList(String str) {
        if (PJ()) {
            this.cee = new UcContactForWebKit(this.mContext, this.ced);
            this.cee.scanContactList(str);
        }
    }

    public void setType(int i) {
        this.type = i;
    }
}
