package com.uc.browser;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.uc.h.e;
import java.util.ArrayList;
import java.util.List;

public class AdapterApplicationList extends BaseAdapter {
    public static final String[] bdz = {"uc.ucplayer"};
    List bdy;

    private void BO() {
        if (this.bdy != null) {
            ArrayList arrayList = new ArrayList();
            for (ResolveInfo resolveInfo : this.bdy) {
                for (String equals : bdz) {
                    if (resolveInfo.activityInfo.packageName.equals(equals)) {
                        arrayList.add(0, resolveInfo);
                    } else {
                        arrayList.add(resolveInfo);
                    }
                }
            }
            this.bdy.clear();
            this.bdy = arrayList;
            notifyDataSetChanged();
        }
    }

    public void g(List list) {
        this.bdy = list;
        BO();
    }

    public int getCount() {
        if (this.bdy == null) {
            return 0;
        }
        return this.bdy.size();
    }

    public Object getItem(int i) {
        if (this.bdy == null || i >= this.bdy.size()) {
            return null;
        }
        return (ResolveInfo) this.bdy.get(i);
    }

    public long getItemId(int i) {
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        Drawable drawable;
        String str;
        TextView textView = view != null ? (TextView) view : (TextView) LayoutInflater.from(viewGroup.getContext()).inflate((int) R.layout.f941single_choice_list_item_no_checkmark, viewGroup, false);
        PackageManager packageManager = viewGroup.getContext().getPackageManager();
        ResolveInfo resolveInfo = (ResolveInfo) getItem(i);
        if (resolveInfo != null) {
            String str2 = resolveInfo.activityInfo.packageName;
            try {
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str2, 0);
                int kp = e.Pb().kp(R.dimen.f268file_list_icon_size);
                try {
                    Drawable drawable2 = str2.equals("com.uc.browser") ? textView.getResources().getDrawable(R.drawable.icon) : resolveInfo.loadIcon(packageManager);
                    String str3 = (String) resolveInfo.activityInfo.loadLabel(packageManager);
                    drawable = drawable2;
                    str = str3;
                } catch (Exception e) {
                    drawable = str2.equals("com.uc.browser") ? textView.getResources().getDrawable(R.drawable.icon) : packageManager.getApplicationIcon(applicationInfo);
                    str = (String) packageManager.getApplicationLabel(applicationInfo);
                }
                if (drawable != null) {
                    drawable.setBounds(0, 0, kp, kp);
                }
                String str4 = str;
                for (String equals : bdz) {
                    if (equals.equals(str2)) {
                        str4 = str4 + "(推荐)";
                    }
                }
                textView.setText(str4);
                textView.setCompoundDrawables(drawable, null, null, null);
            } catch (PackageManager.NameNotFoundException e2) {
                textView.setText((CharSequence) null);
                textView.setCompoundDrawables(null, null, null, null);
            }
        }
        return textView;
    }
}
