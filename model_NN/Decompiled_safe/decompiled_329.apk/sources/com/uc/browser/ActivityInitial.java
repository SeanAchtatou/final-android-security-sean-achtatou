package com.uc.browser;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import b.a.a.a.f;
import b.b.a;
import com.uc.a.e;
import com.uc.a.n;
import com.uc.b.b;
import com.uc.f.g;
import java.io.IOException;
import java.io.InputStream;

public class ActivityInitial extends ActivityWithUCMenu {
    public static final byte k = 0;
    public static final byte l = 1;
    public static final byte m = 2;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ViewInitialLoading f1584a;

    /* renamed from: b  reason: collision with root package name */
    private final int f1585b = 1;
    private final int c = 2;
    private final int d = 3;
    private final int e = 4;
    private final int f = 5;
    /* access modifiers changed from: private */
    public boolean g = false;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public int i = 0;
    /* access modifiers changed from: private */
    public byte j;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    ActivityInitial.this.b();
                    return;
                case 2:
                    if (message.obj != null && ActivityInitial.this.f1584a != null) {
                        ActivityInitial.this.f1584a.setText(message.obj.toString());
                        return;
                    }
                    return;
                case 3:
                    ViewInitialLoading unused = ActivityInitial.this.f1584a = new ViewInitialLoading(ActivityInitial.this);
                    ActivityInitial.this.setContentView(ActivityInitial.this.f1584a);
                    if (ActivityInitial.this.f1584a != null) {
                        ActivityInitial.this.f1584a.IW();
                        return;
                    }
                    return;
                case 4:
                    if (ActivityInitial.this.f1584a != null) {
                        ActivityInitial.this.f1584a.Ja();
                        return;
                    }
                    return;
                case 5:
                    if (ActivityInitial.this.f1584a != null) {
                        ActivityInitial.this.f1584a.Jb();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    private long n = 0;
    private final long o = 1000;
    /* access modifiers changed from: private */
    public n p = new n() {
        public byte[] bP(String str) {
            try {
                return e.nR().a(ActivityInitial.this.getAssets().open(str));
            } catch (IOException e) {
                return null;
            }
        }

        public InputStream bQ(String str) {
            if (str == null) {
                return null;
            }
            try {
                String trim = str.trim();
                if (trim.length() == 0) {
                    return null;
                }
                return ActivityInitial.this.getAssets().open(trim);
            } catch (Exception e) {
                return null;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.uc.browser.ActivityInitial.a(com.uc.browser.ActivityInitial, boolean):boolean
         arg types: [com.uc.browser.ActivityInitial, int]
         candidates:
          com.uc.browser.ActivityInitial.a(com.uc.browser.ActivityInitial, byte):byte
          com.uc.browser.ActivityInitial.a(com.uc.browser.ActivityInitial, int):int
          com.uc.browser.ActivityInitial.a(com.uc.browser.ActivityInitial, com.uc.browser.ViewInitialLoading):com.uc.browser.ViewInitialLoading
          com.uc.browser.ActivityInitial.a(int, java.lang.Object):void
          com.uc.browser.ActivityInitial.a(com.uc.browser.ActivityInitial, boolean):boolean */
        public boolean eW(int i) {
            if (i < 99) {
                if ((-1 == i || -2 == i) && 1 == ActivityInitial.this.i) {
                    int unused = ActivityInitial.this.i = 2;
                }
                String ac = e.nR().nZ().ac(i, 0);
                if (-1 == i || -2 == i || -4 == i) {
                    e.nR().aC(true);
                    if (ActivityInitial.this.i == 3) {
                        ac = ActivityInitial.this.getString(R.string.f1528fresh_us_data_error);
                        ActivityInitial.this.a(2, ac);
                        ActivityInitial.this.a(5, (Object) null);
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                        }
                        ActivityInitial.this.finish();
                    }
                } else if (ActivityInitial.this.i == 3) {
                    ac = ActivityInitial.this.getString(R.string.f1525fresh_us_data_ongoing);
                }
                ActivityInitial.this.a(2, ac);
                return false;
            } else if (ActivityInitial.this.g) {
                return false;
            } else {
                if (ActivityInitial.this.i == 0) {
                    ActivityInitial.this.b();
                    return false;
                } else if (1 == ActivityInitial.this.i) {
                    int unused2 = ActivityInitial.this.i = 2;
                    if (!ActivityInitial.this.d()) {
                        boolean unused3 = ActivityInitial.this.g = true;
                        ActivityInitial.this.finish();
                        return false;
                    }
                    ActivityInitial.this.a(2, "获取首页数据中");
                    return false;
                } else if (2 == ActivityInitial.this.i) {
                    boolean unused4 = ActivityInitial.this.g = true;
                    if (ModelBrowser.gD() != null) {
                        ModelBrowser.gD().aS(51);
                    }
                    ActivityInitial.this.finish();
                    return false;
                } else if (3 != ActivityInitial.this.i || ActivityInitial.this.j != 0) {
                    return false;
                } else {
                    ActivityInitial.this.a(2, ActivityInitial.this.getString(R.string.f1526fresh_us_data_complete));
                    ActivityInitial.this.a(4, (Object) null);
                    if (ModelBrowser.gD() != null) {
                        ModelBrowser.gD().aS(51);
                    }
                    byte unused5 = ActivityInitial.this.j = (byte) 2;
                    return false;
                }
            }
        }

        public void oD() {
            e.nR().oD();
        }

        public int qX() {
            Display defaultDisplay = ActivityInitial.this.getWindowManager().getDefaultDisplay();
            return 2 == ActivityInitial.this.getResources().getConfiguration().orientation ? defaultDisplay.getHeight() : defaultDisplay.getWidth();
        }

        public int qY() {
            Display defaultDisplay = ActivityInitial.this.getWindowManager().getDefaultDisplay();
            return 2 == ActivityInitial.this.getResources().getConfiguration().orientation ? defaultDisplay.getWidth() : defaultDisplay.getHeight();
        }

        public f[] ra() {
            return new f[]{f.a(ActivityInitial.this.getResources(), R.drawable.f593navi_3guc), f.a(ActivityInitial.this.getResources(), R.drawable.f595navi_sina), f.a(ActivityInitial.this.getResources(), R.drawable.f596navi_uzone), f.a(ActivityInitial.this.getResources(), R.drawable.f594navi_bibei)};
        }

        public boolean xg() {
            return ActivityInitial.this.i == 0 && !ActivityInitial.this.g;
        }

        public boolean xh() {
            return !ActivityInitial.this.h;
        }
    };

    class enterUcwebThread extends Thread {
        private enterUcwebThread() {
        }

        public void run() {
            Looper.prepare();
            do {
                try {
                    if (!e.nR().pO()) {
                        try {
                            ActivityInitial.this.e();
                            if (b.pE == null || b.pE.length() == 0) {
                                TelephonyManager telephonyManager = (TelephonyManager) ActivityInitial.this.getSystemService("phone");
                                b.pE = telephonyManager.getDeviceId();
                                e.nR().br(b.pE);
                                e.nR().bs(telephonyManager.getSubscriberId());
                            }
                            e.nR().e(ActivityInitial.this.p);
                            e.nR().dD((int) ActivityInitial.this.getResources().getDimension(R.dimen.f164webpage_bottom_space));
                            if (!ActivityInitial.this.h || ActivityInitial.this.g) {
                                g.Jn().fh(b.b.b.PS());
                                ActivityInitial.this.c();
                                e.nR().pI();
                            } else {
                                ActivityInitial.this.finish();
                                e.nR().ac(true);
                                return;
                            }
                        } catch (Exception e) {
                        }
                    }
                    Thread.yield();
                    if (ActivityInitial.this.h && !ActivityInitial.this.g) {
                        ActivityInitial.this.finish();
                        e.nR().ac(true);
                        return;
                    }
                } catch (Exception e2) {
                    try {
                        if (ActivityInitial.this.h && !ActivityInitial.this.g) {
                            ActivityInitial.this.finish();
                            e.nR().ac(true);
                            return;
                        }
                        return;
                    } catch (Exception e3) {
                        return;
                    }
                }
            } while (!ActivityInitial.this.g);
        }
    }

    public void a() {
        Bundle extras;
        if (!e.nR().pO()) {
            new enterUcwebThread().start();
            return;
        }
        Intent intent = getIntent();
        intent.getExtras();
        if (!(intent == null || (extras = intent.getExtras()) == null)) {
            if (extras.getString("networkcheck") != null) {
                this.i = 1;
            } else if (extras.getString(b.b.acW) != null) {
                this.i = 3;
            }
        }
        this.h = false;
        if (this.i == 0) {
            b();
        } else if (1 == this.i) {
            e.nR().nZ().g(this.p);
        } else if (3 == this.i) {
            d();
        }
    }

    public void a(byte b2) {
        this.j = b2;
    }

    public void a(int i2, Object obj) {
        this.mHandler.sendMessage(Message.obtain(null, i2, obj));
    }

    public void b() {
        if (!this.g && !this.h) {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - this.n < 1000) {
                try {
                    Thread.sleep(1000 - (currentTimeMillis - this.n));
                } catch (InterruptedException e2) {
                }
            }
            if (!this.h) {
                this.g = true;
                if (!ActivityUpdate.IP) {
                    e.nR().oE();
                }
                startActivity(new Intent(this, ActivityBrowser.class));
                finish();
                e.nR().nZ().pN();
            }
        }
    }

    public void c() {
        e.nR().nZ().f(this.p);
    }

    public boolean d() {
        return e.nR().nZ().h(this.p);
    }

    public void e() {
        e.nR().a((int) getResources().getDimension(R.dimen.f338juc_text_small), (int) getResources().getDimension(R.dimen.f339juc_text_medium), (int) getResources().getDimension(R.dimen.f340juc_text_large), (int) getResources().getDimension(R.dimen.f341juc_download_xoffset), (int) getResources().getDimension(R.dimen.f342juc_download_buttontext_size), (int) getResources().getDimension(R.dimen.f343juc_download_filenametext_size), (int) getResources().getDimension(R.dimen.f344juc_download_speedtext_size), (int) getResources().getDimension(R.dimen.f345juc_download_statebar_size), (int) getResources().getDimension(R.dimen.f346juc_download_itemtext_size), getResources().getDimension(R.dimen.f347juc_multiple_font), getResources().getDisplayMetrics().xdpi, new float[]{getResources().getDimension(R.dimen.f403unit_pt), getResources().getDimension(R.dimen.f404unit_in), getResources().getDimension(R.dimen.f405unit_mm)}, (int) getResources().getDimension(R.dimen.f406wap10_line_space));
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.n = System.currentTimeMillis();
        super.onCreate(bundle);
        a();
        a.d(this);
        b.a.d(this);
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(ModelBrowser.vA, false)) {
            getWindow().setFlags(1024, 3072);
        }
        requestWindowFeature(1);
        this.f1584a = new ViewInitialLoading(this);
        if (3 == this.i) {
            this.f1584a.IZ();
            this.j = 0;
        }
        setContentView(this.f1584a);
        this.f1584a.IW();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.p = null;
        this.f1584a.destroy();
        this.f1584a = null;
        setContentView(new View(this));
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (4 != i2) {
            return super.onKeyDown(i2, keyEvent);
        }
        if (3 == this.i) {
            if (this.j == 0) {
                this.j = 1;
                e.nR().nZ().cm(i2);
            }
            setResult(0);
            finish();
            return true;
        }
        e.nR().nZ().cm(i2);
        if (this.i == 0) {
            this.h = true;
        } else {
            if (2 == this.i && ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(51);
            }
            this.h = true;
            finish();
        }
        return true;
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 == 84) {
            return true;
        }
        return super.onKeyUp(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.f1584a != null) {
            this.f1584a.IX();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.n = System.currentTimeMillis();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z && ActivityBrowser.Fz()) {
            try {
                Thread.sleep(300);
            } catch (InterruptedException e2) {
            }
            ActivityBrowser.e(this);
        }
    }
}
