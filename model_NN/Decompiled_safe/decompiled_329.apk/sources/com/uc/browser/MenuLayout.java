package com.uc.browser;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import com.uc.a.h;
import com.uc.browser.UCR;
import com.uc.e.aa;
import com.uc.e.ad;
import com.uc.e.l;
import com.uc.e.t;
import com.uc.h.a;
import com.uc.h.e;
import java.util.ArrayList;

public class MenuLayout extends View implements a {
    public static t A = null;
    public static t B = null;
    public static t C = null;
    public static t D = null;
    public static t E = null;
    public static t F = null;
    public static t G = null;
    public static t H = null;
    public static t I = null;
    public static t J = null;
    public static t K = null;
    public static t L = null;
    public static t M = null;
    public static t N = null;
    public static t O = null;
    public static t P = null;
    public static t Q = null;
    public static t R = null;
    public static t S = null;
    public static t T = null;
    public static t U = null;
    public static t V = null;
    public static t W = null;
    public static t X = null;
    public static t Y = null;
    public static final int r = -1000;
    public static final int s = -1001;
    public static final int t = -1002;
    private static t v;
    private static t w;
    private static t z;
    private Animation Z;
    private Transformation aa;
    private ArrayList ab = null;
    private long ac = -1;
    private aa u;

    public MenuLayout(Context context) {
        super(context);
        a();
    }

    public MenuLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    private void a() {
        if (this.u == null) {
            this.u = new aa();
        }
        this.u.d(new int[]{2, 4}, new int[]{2, 6});
        e Pb = e.Pb();
        getContext().getResources();
        Drawable drawable = Pb.getDrawable(UCR.drawable.aVq);
        Drawable drawable2 = Pb.getDrawable(UCR.drawable.aVH);
        Drawable drawable3 = Pb.getDrawable(UCR.drawable.aVr);
        Drawable drawable4 = Pb.getDrawable(UCR.drawable.aVL);
        Drawable drawable5 = Pb.getDrawable(UCR.drawable.aVN);
        Drawable drawable6 = Pb.getDrawable(UCR.drawable.aVM);
        Drawable drawable7 = Pb.getDrawable(UCR.drawable.aVq);
        Drawable drawable8 = Pb.getDrawable(UCR.drawable.aVH);
        Drawable drawable9 = Pb.getDrawable(UCR.drawable.aVr);
        Drawable drawable10 = Pb.getDrawable(UCR.drawable.aVK);
        this.u.h(new Drawable[]{null, Pb.getDrawable(UCR.drawable.aVI), drawable10});
        aa aaVar = this.u;
        aa.ii(Pb.kp(R.dimen.f366menu_tab_height));
        aa aaVar2 = this.u;
        aa.ij(Pb.kp(R.dimen.f367menu2_item_height));
        this.u.hZ(Pb.kp(R.dimen.f355menu_item_text_size));
        this.u.ia(Pb.kp(R.dimen.f356menu_tag_item_text_size));
        aa aaVar3 = this.u;
        aa.id(Pb.kp(R.dimen.f361menu_padding_text2icon));
        aa aaVar4 = this.u;
        aa.ig(Pb.kp(R.dimen.f364menu_item_padding_left_right));
        aa aaVar5 = this.u;
        aa.ih(Pb.kp(R.dimen.f365menu_item_padding_top_bottom));
        this.u.L(Pb.getDrawable(UCR.drawable.aXr));
        this.u.a(new ad() {
            public void cJ() {
                MenuLayout.this.postInvalidate();
            }
        });
        this.u.hV(Pb.kp(R.dimen.f260menu_item_icon_width));
        this.u.hW(Pb.kp(R.dimen.f259menu_item_icon_height));
        if (v == null) {
            v = new t();
            v.bE(r);
            v.setText(getContext().getString(R.string.f1038menu_tab_favorite));
            v.g(new Drawable[]{drawable, drawable7, drawable4});
        }
        if (w == null) {
            w = new t();
            w.bE(s);
            w.setText(getContext().getString(R.string.f1039menu_tab_setting));
            w.g(new Drawable[]{drawable3, drawable9, drawable6});
        }
        if (z == null) {
            z = new t();
            z.bE(t);
            z.setText(getContext().getString(R.string.f1040menu_tab_tools));
            z.g(new Drawable[]{drawable2, drawable8, drawable5});
        }
        ArrayList arrayList = new ArrayList();
        Drawable drawable11 = Pb.getDrawable(UCR.drawable.aUT);
        if (Q == null) {
            Q = new t();
            Q.bE(UCR.drawable.aUT);
            Q.setText(getContext().getString(R.string.f1041menu_history_bookmark));
            Q.setIcon(drawable11);
            Q.fy(0);
        }
        arrayList.add(Q);
        Drawable drawable12 = Pb.getDrawable(UCR.drawable.aUP);
        if (R == null) {
            R = new t();
            R.bE(UCR.drawable.aUP);
            R.setText(getContext().getString(R.string.f1056menu_addbookmark));
            R.setIcon(drawable12);
            R.fy(1);
        }
        arrayList.add(R);
        Drawable drawable13 = Pb.getDrawable(UCR.drawable.aNO);
        if (B == null) {
            B = new t();
            B.bE(UCR.drawable.aNO);
            B.setText(getContext().getString(R.string.f1048menu_refresh));
            B.setIcon(drawable13);
            B.fy(2);
        }
        arrayList.add(B);
        Drawable drawable14 = Pb.getDrawable(UCR.drawable.aNz);
        if (P == null) {
            P = new t();
            P.bE(UCR.drawable.aNz);
            P.setText(getContext().getString(R.string.f1051menu_nightmode));
            P.setIcon(drawable14);
            P.fy(3);
        }
        arrayList.add(P);
        if (com.uc.a.e.RD.equals(com.uc.a.e.nR().nY().bw(h.afK))) {
            if (H == null) {
                H = new t();
                H.fy(5);
            }
            Drawable drawable15 = Pb.getDrawable(UCR.drawable.aNE);
            H.bE(UCR.drawable.aNE);
            H.setText(getContext().getString(R.string.f1050menu_outfullscreen));
            H.setIcon(drawable15);
            arrayList.add(H);
        } else {
            if (H == null) {
                H = new t();
                H.fy(5);
            }
            Drawable drawable16 = Pb.getDrawable(UCR.drawable.aNs);
            H.bE(UCR.drawable.aNs);
            H.setText(getContext().getString(R.string.f1049menu_fullscreen));
            H.setIcon(drawable16);
            arrayList.add(H);
        }
        Drawable drawable17 = Pb.getDrawable(UCR.drawable.aVi);
        if (T == null) {
            T = new t();
            T.bE(UCR.drawable.aVi);
            T.setText(getContext().getString(R.string.f1045menu_downloadmanager));
            T.setIcon(drawable17);
            T.fy(7);
        }
        arrayList.add(T);
        Drawable drawable18 = Pb.getDrawable(UCR.drawable.aUX);
        if (X == null) {
            X = new t();
            X.bE(UCR.drawable.aUX);
            X.setText(getContext().getString(R.string.f1087menu_passoprt));
            X.setIcon(drawable18);
            X.fy(7);
        }
        arrayList.add(X);
        Drawable drawable19 = Pb.getDrawable(UCR.drawable.aNL);
        if (V == null) {
            V = new t();
            V.bE(UCR.drawable.aNL);
            V.setText(getContext().getString(R.string.f1047menu_quit2));
            V.fy(9);
            V.setIcon(drawable19);
        }
        arrayList.add(V);
        this.u.a(v, arrayList, new int[][]{new int[]{0, 1, 2, 3, 4, 5, 6, 7}, new int[]{0, 1, 2, 3, 4, 5, 6, 11}});
        ArrayList arrayList2 = new ArrayList();
        Drawable drawable20 = Pb.getDrawable(UCR.drawable.aNZ);
        if (C == null) {
            C = new t();
            C.bE(UCR.drawable.aNZ);
            C.setText(getContext().getString(R.string.f1053menu_syssettings));
            C.setIcon(drawable20);
            C.fy(0);
        }
        arrayList2.add(C);
        Drawable drawable21 = Pb.getDrawable(UCR.drawable.aNk);
        if (D == null) {
            D = new t();
            D.bE(UCR.drawable.aNk);
            D.setText(getContext().getString(R.string.f1057menu_browsemode));
            D.setIcon(drawable21);
            D.fy(1);
        }
        arrayList2.add(D);
        Drawable drawable22 = Pb.getDrawable(UCR.drawable.aND);
        if (E == null) {
            E = new t();
            E.bE(UCR.drawable.aND);
            E.setText(getContext().getString(R.string.f1066menu_novel_mode));
            E.setIcon(drawable22);
            E.fy(2);
        }
        arrayList2.add(E);
        Drawable drawable23 = Pb.getDrawable(UCR.drawable.aNF);
        if (F == null) {
            F = new t();
            F.bE(UCR.drawable.aNF);
            F.setText(getContext().getString(R.string.f1065menu_page_updown));
            F.setIcon(drawable23);
            F.fy(3);
        }
        arrayList2.add(F);
        Drawable drawable24 = Pb.getDrawable(UCR.drawable.aUR);
        if (G == null) {
            G = new t();
            G.bE(UCR.drawable.aUR);
            G.setText(getContext().getString(R.string.f1064menu_land_state));
            G.setIcon(drawable24);
            G.fy(4);
        }
        arrayList2.add(G);
        Drawable drawable25 = Pb.getDrawable(UCR.drawable.aNp);
        if (W == null) {
            W = new t();
            W.bE(UCR.drawable.aNp);
            W.setText(getContext().getString(R.string.f1084menu_nopic));
            W.setIcon(drawable25);
            W.fy(8);
        }
        arrayList2.add(W);
        Drawable drawable26 = Pb.getDrawable(UCR.drawable.aNP);
        if (I == null) {
            I = new t();
            I.bE(UCR.drawable.aNP);
            I.setText(getContext().getString(R.string.f1072menu_refreshtimer));
            I.setIcon(drawable26);
            I.fy(6);
        }
        arrayList2.add(I);
        Drawable drawable27 = Pb.getDrawable(UCR.drawable.aVO);
        if (J == null) {
            J = new t();
            J.bE(UCR.drawable.aVO);
            J.setText(getContext().getString(R.string.f1083menu_skinmanager));
            J.setIcon(drawable27);
            J.fy(7);
        }
        arrayList2.add(J);
        this.u.a(w, arrayList2, new int[][]{new int[]{0, 1, 2, 3, 4, 5, 6, 7}, new int[]{0, 1, 2, 3, 4, 5, 6, 7}});
        ArrayList arrayList3 = new ArrayList();
        Drawable drawable28 = Pb.getDrawable(UCR.drawable.aNr);
        if (S == null) {
            S = new t();
            S.bE(UCR.drawable.aNr);
            S.setText(getContext().getString(R.string.f1075menu_filemanager));
            S.setIcon(drawable28);
            S.fy(0);
        }
        arrayList3.add(S);
        Drawable drawable29 = Pb.getDrawable(UCR.drawable.aNW);
        if (K == null) {
            K = new t();
            K.bE(UCR.drawable.aNW);
            K.setText(getContext().getString(R.string.f1080menu_sharepage));
            K.setIcon(drawable29);
            K.fy(0);
        }
        arrayList3.add(K);
        Drawable drawable30 = Pb.getDrawable(UCR.drawable.aNv);
        if (L == null) {
            L = new t();
            L.bE(UCR.drawable.aNv);
            L.setText(getContext().getString(R.string.f1516save_source));
            L.setIcon(drawable30);
            L.fy(0);
        }
        arrayList3.add(L);
        Drawable drawable31 = Pb.getDrawable(UCR.drawable.aVC);
        if (U == null) {
            U = new t();
            U.bE(UCR.drawable.aVC);
            U.setText(getContext().getString(R.string.f1046menu_select_copy));
            U.setIcon(drawable31);
            U.fy(0);
        }
        arrayList3.add(U);
        Drawable drawable32 = Pb.getDrawable(UCR.drawable.aVG);
        if (M == null) {
            M = new t();
            M.bE(UCR.drawable.aVG);
            M.setText(getContext().getString(R.string.f1081menu_reportwebsite));
            M.setIcon(drawable32);
            M.fy(0);
        }
        arrayList3.add(M);
        Drawable drawable33 = Pb.getDrawable(UCR.drawable.aVl);
        if (N == null) {
            N = new t();
            N.bE(UCR.drawable.aVl);
            N.setText(getContext().getString(R.string.f1082menu_report));
            N.setIcon(drawable33);
            N.fy(0);
        }
        arrayList3.add(N);
        Drawable drawable34 = Pb.getDrawable(UCR.drawable.aNt);
        if (O == null) {
            O = new t();
            O.bE(UCR.drawable.aNt);
            O.setText(getContext().getString(R.string.f1069menu_help));
            O.setIcon(drawable34);
            O.fy(0);
        }
        arrayList3.add(O);
        Drawable drawable35 = Pb.getDrawable(UCR.drawable.aNh);
        if (Y == null) {
            Y = new t();
            Y.bE(UCR.drawable.aNh);
            Y.setText(getContext().getString(R.string.f1088menu_uctraffic));
            Y.setIcon(drawable35);
            Y.fy(0);
        }
        arrayList3.add(Y);
        this.u.a(z, arrayList3, new int[][]{new int[]{0, 1, 2, 3, 4, 5, 6, 7}, new int[]{0, 1, 2, 3, 4, 5, 6, 7}});
    }

    public static void setOrientation(int i) {
        aa.setOrientation(i);
        e Pb = e.Pb();
        aa.id(Pb.kp(R.dimen.f361menu_padding_text2icon));
        aa.ig(Pb.kp(R.dimen.f364menu_item_padding_left_right));
        aa.ih(Pb.kp(R.dimen.f365menu_item_padding_top_bottom));
        aa.ii(Pb.kp(R.dimen.f366menu_tab_height));
        aa.ij(Pb.kp(R.dimen.f367menu2_item_height));
    }

    public void a(int i) {
        this.u.a(i);
    }

    public void a(Animation animation) {
        this.Z = animation;
        if (this.Z != null) {
            invalidate();
            this.Z.reset();
            this.Z.start();
            this.ac = System.currentTimeMillis();
            this.ab = null;
        }
    }

    public void a(com.uc.e.h hVar) {
        this.u.c(hVar);
    }

    public void a(l lVar) {
        this.u.a(lVar);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return this.u.a(keyEvent);
    }

    public void draw(Canvas canvas) {
        boolean z2;
        Rect clipBounds = canvas.getClipBounds();
        if (clipBounds.bottom - clipBounds.top != 0 && clipBounds.right - clipBounds.left != 0) {
            long currentTimeMillis = System.currentTimeMillis() - (this.ac > 0 ? this.ac : 0);
            if (this.ac < 0 || currentTimeMillis > 300) {
                this.ac = currentTimeMillis;
                currentTimeMillis = 0;
            }
            if (ModelBrowser.gD() != null && ModelBrowser.gD().gR() == 1) {
                if (this.ab == null) {
                    this.ab = new ArrayList();
                }
                if (currentTimeMillis > 0) {
                    this.ab.add(Long.valueOf(currentTimeMillis));
                }
            }
            if (this.Z != null) {
                if (!this.Z.isInitialized()) {
                    this.Z.initialize(this.u.getWidth(), this.u.l(), this.u.getWidth(), this.u.l());
                }
                if (this.aa == null) {
                    this.aa = new Transformation();
                }
                z2 = this.Z.getTransformation(System.currentTimeMillis(), this.aa);
                canvas.concat(this.aa.getMatrix());
            } else {
                z2 = false;
            }
            this.u.draw(canvas);
            if (z2) {
                invalidate();
                return;
            }
            if (ModelBrowser.gD() != null && ModelBrowser.gD().gR() == 1 && this.ab != null && this.ab.size() > 2) {
                long longValue = (((Long) this.ab.get(this.ab.size() - 1)).longValue() - ((Long) this.ab.get(0)).longValue()) / ((long) (this.ab.size() - 1));
                if (longValue > 40 && longValue < 300) {
                    ModelBrowser.gD().z(false);
                    this.ab.clear();
                    this.ac = -1;
                } else if (longValue < 30) {
                    ModelBrowser.gD().z(true);
                    this.ab.clear();
                    this.ac = -1;
                }
            }
            this.Z = null;
        }
    }

    public void g() {
        this.u.Jf();
        this.ac = System.currentTimeMillis();
    }

    public int getOrientation() {
        return this.u.getOrientation();
    }

    public void h() {
        this.u.Jg();
    }

    public int i() {
        return this.u.i();
    }

    public void j() {
        this.u.j();
    }

    public void k() {
        e Pb = e.Pb();
        Drawable drawable = Pb.getDrawable(UCR.drawable.aVq);
        Drawable drawable2 = Pb.getDrawable(UCR.drawable.aVH);
        Drawable drawable3 = Pb.getDrawable(UCR.drawable.aVr);
        Drawable drawable4 = Pb.getDrawable(UCR.drawable.aVq);
        Drawable drawable5 = Pb.getDrawable(UCR.drawable.aVH);
        Drawable drawable6 = Pb.getDrawable(UCR.drawable.aVr);
        Drawable drawable7 = Pb.getDrawable(UCR.drawable.aVL);
        Drawable drawable8 = Pb.getDrawable(UCR.drawable.aVN);
        Drawable drawable9 = Pb.getDrawable(UCR.drawable.aVM);
        Drawable drawable10 = Pb.getDrawable(UCR.drawable.aVK);
        this.u.h(new Drawable[]{null, Pb.getDrawable(UCR.drawable.aVI), drawable10});
        aa aaVar = this.u;
        aa.ii(Pb.kp(R.dimen.f366menu_tab_height));
        aa aaVar2 = this.u;
        aa.ij(Pb.kp(R.dimen.f367menu2_item_height));
        this.u.hZ(Pb.kp(R.dimen.f355menu_item_text_size));
        this.u.ia(Pb.kp(R.dimen.f356menu_tag_item_text_size));
        aa aaVar3 = this.u;
        aa.id(Pb.kp(R.dimen.f361menu_padding_text2icon));
        aa aaVar4 = this.u;
        aa.ig(Pb.kp(R.dimen.f364menu_item_padding_left_right));
        aa aaVar5 = this.u;
        aa.ih(Pb.kp(R.dimen.f365menu_item_padding_top_bottom));
        this.u.L(Pb.getDrawable(UCR.drawable.aXr));
        if (v != null) {
            v.g(new Drawable[]{drawable4, drawable, drawable7});
        }
        if (w != null) {
            w.g(new Drawable[]{drawable6, drawable3, drawable9});
        }
        if (z != null) {
            z.g(new Drawable[]{drawable5, drawable2, drawable8});
        }
        Drawable drawable11 = Pb.getDrawable(UCR.drawable.aUT);
        if (Q != null) {
            Q.setIcon(drawable11);
        }
        Drawable drawable12 = Pb.getDrawable(UCR.drawable.aUP);
        if (R != null) {
            R.setIcon(drawable12);
        }
        Drawable drawable13 = Pb.getDrawable(UCR.drawable.aNO);
        if (B != null) {
            B.setIcon(drawable13);
        }
        Drawable drawable14 = Pb.getDrawable(UCR.drawable.aNz);
        if (P != null) {
            P.setIcon(drawable14);
        }
        Drawable drawable15 = Pb.getDrawable(UCR.drawable.aNr);
        if (S != null) {
            S.setIcon(drawable15);
        }
        Drawable drawable16 = Pb.getDrawable(UCR.drawable.aVi);
        if (T != null) {
            T.setIcon(drawable16);
        }
        Drawable drawable17 = Pb.getDrawable(UCR.drawable.aVC);
        if (U != null) {
            U.setIcon(drawable17);
        }
        Drawable drawable18 = Pb.getDrawable(UCR.drawable.aNL);
        if (V != null) {
            V.setIcon(drawable18);
        }
        Drawable drawable19 = Pb.getDrawable(UCR.drawable.aNZ);
        if (C != null) {
            C.setIcon(drawable19);
        }
        Drawable drawable20 = Pb.getDrawable(UCR.drawable.aNk);
        if (D != null) {
            D.setIcon(drawable20);
        }
        Drawable drawable21 = Pb.getDrawable(UCR.drawable.aND);
        if (E != null) {
            E.setIcon(drawable21);
        }
        Drawable drawable22 = Pb.getDrawable(UCR.drawable.aNF);
        if (F != null) {
            F.setIcon(drawable22);
        }
        Drawable drawable23 = Pb.getDrawable(UCR.drawable.aUR);
        if (G != null) {
            G.setIcon(drawable23);
        }
        if (com.uc.a.e.RD.equals(com.uc.a.e.nR().nY().bw(h.afK))) {
            if (H != null) {
                H.setIcon(Pb.getDrawable(UCR.drawable.aNE));
            }
        } else if (H != null) {
            H.setIcon(Pb.getDrawable(UCR.drawable.aNs));
        }
        Drawable drawable24 = Pb.getDrawable(UCR.drawable.aNP);
        if (I != null) {
            I.setIcon(drawable24);
        }
        Drawable drawable25 = Pb.getDrawable(UCR.drawable.aVO);
        if (J != null) {
            J.setIcon(drawable25);
        }
        Drawable drawable26 = Pb.getDrawable(UCR.drawable.aNW);
        if (K != null) {
            K.setIcon(drawable26);
        }
        Drawable drawable27 = Pb.getDrawable(UCR.drawable.aNv);
        if (L != null) {
            L.setIcon(drawable27);
        }
        Drawable drawable28 = Pb.getDrawable(UCR.drawable.aUX);
        if (X != null) {
            X.setIcon(drawable28);
        }
        Drawable drawable29 = Pb.getDrawable(UCR.drawable.aVG);
        if (M != null) {
            M.setIcon(drawable29);
        }
        Drawable drawable30 = Pb.getDrawable(UCR.drawable.aVl);
        if (N != null) {
            N.setIcon(drawable30);
        }
        Drawable drawable31 = Pb.getDrawable(UCR.drawable.aNh);
        if (Y != null) {
            Y.setIcon(drawable31);
        }
        Drawable drawable32 = Pb.getDrawable(UCR.drawable.aNt);
        if (O != null) {
            O.setIcon(drawable32);
        }
        this.u.k();
    }

    public int l() {
        return this.u.l();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
        super.onLayout(z2, i, i2, i3, i4);
        this.u.setSize(i3 - i, i4 - i2);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                return this.u.a((byte) 0, (int) motionEvent.getX(), (int) motionEvent.getY());
            case 1:
                return this.u.a((byte) 1, (int) motionEvent.getX(), (int) motionEvent.getY());
            case 2:
                return this.u.a((byte) 2, (int) motionEvent.getX(), (int) motionEvent.getY());
            default:
                return false;
        }
    }

    public void refresh() {
        if (this.u != null) {
            this.u.Je();
        }
    }

    public void reset() {
        this.u.reset();
    }
}
