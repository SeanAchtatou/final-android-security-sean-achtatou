package com.uc.browser;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.KeyEvent;
import com.uc.e.ac;
import com.uc.e.h;
import com.uc.e.z;
import com.uc.h.e;
import com.uc.plugin.Plugin;
import com.uc.plugin.ag;
import com.uc.plugin.c;
import java.util.Vector;

public class ActivityFlash extends Activity implements h, c {
    public static final String cak = "src";
    public static final String cal = "cookie";
    public static final String cam = "width";
    public static final String can = "height";
    private FlashWrapperView cao;
    private ag cap;
    private BarLayout um;
    private ac uo;

    private void yb() {
        this.um = (BarLayout) findViewById(R.id.f655controlbar);
        e Pb = e.Pb();
        int kp = Pb.kp(R.dimen.f183controlbar_item_width_2);
        int kp2 = Pb.kp(R.dimen.f177controlbar_height);
        int kp3 = Pb.kp(R.dimen.f188controlbar_text_size);
        int kp4 = Pb.kp(R.dimen.f187controlbar_item_paddingTop);
        this.um.HN.aw(kp, kp2);
        this.um.HN.v((byte) 1);
        Resources resources = getResources();
        this.uo = new ac(R.string.f1017controlbar_back, 0, 0);
        this.uo.bB(0, 0);
        this.uo.aV(kp3);
        this.uo.setText(resources.getString(R.string.f1017controlbar_back));
        this.uo.setPadding(0, kp4, 0, 4);
        this.uo.H(true);
        this.um.a(this.uo);
        this.um.kJ();
        this.um.b(this);
    }

    public void A(String str) {
        finish();
    }

    public void b(z zVar, int i) {
        switch (i) {
            case R.string.f1017controlbar_back /*2131296324*/:
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 86 && i2 == -1) {
            finish();
        }
    }

    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        requestWindowFeature(1);
        ActivityBrowser.g(this);
        setContentView((int) R.layout.f904flash_window);
        yb();
        Bundle extras = getIntent().getExtras();
        this.cao = (FlashWrapperView) findViewById(R.id.f757flash);
        Vector vector = new Vector();
        Vector vector2 = new Vector();
        if (extras != null) {
            vector.add(cak);
            vector2.add(extras.getString(cak));
            vector.add("width");
            vector2.add("" + ((int) getResources().getDimension(R.dimen.f321flashview_width)));
            vector.add("height");
            vector2.add("" + ((int) getResources().getDimension(R.dimen.f322flashview_height)));
            str = extras.getString(cal);
        } else {
            str = null;
        }
        Plugin a2 = com.uc.plugin.ac.a("application/x-shockwave-flash", vector, vector2, this);
        a2.a(this);
        if (str != null) {
            a2.d("COOKIE", str, null);
        }
        this.cap = a2.Qh();
        this.cao.addView(this.cap);
        this.cao.j(this.cap);
        this.cao.requestFocus();
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i != 84) {
            return super.onKeyUp(i, keyEvent);
        }
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a((int) ModelBrowser.zJ, this);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        com.uc.plugin.ac.w(this);
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        com.uc.plugin.ac.w(null);
        super.onStop();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            this.cao.requestFocus();
        }
    }
}
