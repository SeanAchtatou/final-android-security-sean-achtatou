package com.uc.browser;

import android.content.Context;
import android.util.AttributeSet;
import com.uc.h.e;

public class UrlBarEditText extends UCEditText {
    public UrlBarEditText(Context context) {
        super(context);
    }

    public UrlBarEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public UrlBarEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onSelectionChanged(int i, int i2) {
        if ((i != 0 || i2 != getEditableText().length() || i2 == 0) && (i == 0 || i != getEditableText().length() || i2 != 0)) {
            setTextColor(e.Pb().getColor(33));
        } else if (isFocused()) {
            setTextColor(-1);
        }
    }
}
