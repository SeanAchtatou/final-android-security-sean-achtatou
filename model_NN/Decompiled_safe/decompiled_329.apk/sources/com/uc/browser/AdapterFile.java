package com.uc.browser;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.uc.a.h;
import com.uc.browser.UCR;
import com.uc.c.cd;
import com.uc.h.e;
import java.io.File;
import java.io.FileFilter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Vector;

public class AdapterFile extends BaseAdapter {
    private static final String bnB = "/sdcard";
    public static final String[] bnF = {"byte", "KB", "MB", "GB"};
    private FileItem[] bnA;
    String[] bnC = null;
    private DecimalFormat bnD = new DecimalFormat("0.#");
    private SimpleDateFormat bnE = new SimpleDateFormat("yyyy-MM-dd hh:mm");
    private File bnz;
    private int type = 0;

    public AdapterFile() {
    }

    public AdapterFile(String str) {
        if (str == null) {
            return;
        }
        if (str.indexOf(44) != -1) {
            this.bnC = str.split(cd.bVH);
            return;
        }
        this.bnC = new String[1];
        this.bnC[0] = str;
    }

    public void EN() {
        n(this.bnz);
    }

    public int EO() {
        if (this.bnA == null) {
            return 0;
        }
        int i = 0;
        for (FileItem fileItem : this.bnA) {
            if (fileItem.blv) {
                fileItem.blv = false;
                i++;
            }
        }
        notifyDataSetChanged();
        return i;
    }

    public int EP() {
        if (this.bnA == null) {
            return 0;
        }
        int i = 0;
        for (FileItem fileItem : this.bnA) {
            if (fileItem.blv) {
                i++;
            }
        }
        return i;
    }

    public Vector EQ() {
        Vector vector = new Vector();
        for (FileItem fileItem : this.bnA) {
            File file = fileItem.blw;
            if (fileItem.blv) {
                vector.add(file);
            }
        }
        return vector;
    }

    public File ER() {
        return this.bnz;
    }

    public int getCount() {
        return 1 + (this.bnA == null ? 0 : this.bnA.length);
    }

    public Object getItem(int i) {
        if (i == 0) {
            return new FileItem();
        }
        if (this.bnA == null) {
            return null;
        }
        return this.bnA[i - 1];
    }

    public long getItemId(int i) {
        return 0;
    }

    public int getType() {
        return this.type;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        int i2;
        FileItem fileItem = (FileItem) getItem(i);
        if (fileItem.blw == null || !fileItem.blw.isDirectory()) {
            switch (fileItem.blx) {
                case -2:
                    i2 = R.drawable.f561fileicon_back;
                    break;
                case -1:
                case 0:
                case 10:
                case 13:
                case 14:
                case 16:
                default:
                    i2 = R.drawable.f562fileicon_default;
                    break;
                case 1:
                    i2 = R.drawable.f566fileicon_img;
                    break;
                case 2:
                case 22:
                    i2 = R.drawable.f560fileicon_audio;
                    break;
                case 3:
                    i2 = R.drawable.f567fileicon_video;
                    break;
                case 4:
                    i2 = R.drawable.f562fileicon_default;
                    break;
                case 5:
                    i2 = R.drawable.f562fileicon_default;
                    break;
                case 6:
                    i2 = R.drawable.f565fileicon_exe;
                    break;
                case 7:
                    i2 = R.drawable.f564fileicon_document;
                    break;
                case 8:
                    i2 = R.drawable.f562fileicon_default;
                    break;
                case 9:
                    i2 = R.drawable.f562fileicon_default;
                    break;
                case 11:
                    i2 = R.drawable.f562fileicon_default;
                    break;
                case 12:
                    i2 = R.drawable.f562fileicon_default;
                    break;
                case 15:
                    i2 = R.drawable.f560fileicon_audio;
                    break;
                case 17:
                    i2 = R.drawable.f566fileicon_img;
                    break;
                case 18:
                    i2 = R.drawable.f566fileicon_img;
                    break;
                case 19:
                    i2 = R.drawable.f566fileicon_img;
                    break;
                case 20:
                    i2 = R.drawable.f569fileicon_zip;
                    break;
                case 21:
                    i2 = R.drawable.f568fileicon_webpage;
                    break;
            }
        } else {
            i2 = R.drawable.f563fileicon_dir;
        }
        String string = fileItem.blx == -2 ? viewGroup.getResources().getString(R.string.f992bookmark_title_backtoroot) : fileItem.blw.getName();
        switch (this.type) {
            case 0:
                if (view == null || !(view instanceof TextView)) {
                    view = LayoutInflater.from(viewGroup.getContext()).inflate((int) R.layout.f900file_item_grid, viewGroup, false);
                }
                if (fileItem.blv) {
                    view.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aVK));
                } else {
                    view.setBackgroundDrawable(null);
                }
                TextView textView = (TextView) view;
                textView.setText(string);
                Drawable drawable = viewGroup.getResources().getDrawable(i2);
                int dimension = (int) viewGroup.getResources().getDimension(R.dimen.f269file_grid_icon_size);
                drawable.setBounds(0, 0, dimension, dimension);
                textView.setCompoundDrawables(null, drawable, null, null);
                textView.setTextColor(e.Pb().getColor(16));
                return view;
            case 1:
                View inflate = (view == null || !(view instanceof RelativeLayout)) ? LayoutInflater.from(viewGroup.getContext()).inflate((int) R.layout.f901file_item_list, viewGroup, false) : view;
                if (fileItem.blv) {
                    int paddingLeft = inflate.getPaddingLeft();
                    int paddingRight = inflate.getPaddingRight();
                    if (com.uc.a.e.RD.equals(com.uc.a.e.nR().nY().bw(h.afN))) {
                        inflate.setBackgroundDrawable(e.Pb().km(R.drawable.f559file_selected_night));
                    } else {
                        inflate.setBackgroundDrawable(e.Pb().km(R.drawable.f558file_selected));
                    }
                    inflate.setPadding(paddingLeft, inflate.getPaddingTop(), paddingRight, inflate.getPaddingBottom());
                } else {
                    inflate.setBackgroundDrawable(null);
                }
                TextView textView2 = (TextView) inflate.findViewById(R.id.f754item_title);
                TextView textView3 = (TextView) inflate.findViewById(R.id.f755item_discript);
                textView2.setTextColor(e.Pb().getColor(16));
                textView3.setTextColor(e.Pb().getColor(17));
                textView2.setText(string);
                ((ImageView) inflate.findViewById(R.id.f753item_icon)).setImageResource(i2);
                if (fileItem.blw == null || fileItem.blw.isDirectory()) {
                    textView3.setText((CharSequence) null);
                    return inflate;
                }
                int i3 = 0;
                float length = (float) fileItem.blw.length();
                while (length > 1024.0f) {
                    length /= 1024.0f;
                    i3++;
                }
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(this.bnE.format(new Date(fileItem.blw.lastModified()))).append(9).append(this.bnD.format((double) length)).append(" ").append(bnF[i3]);
                textView3.setText(stringBuffer.toString());
                return inflate;
            default:
                if (view == null) {
                    view = LayoutInflater.from(viewGroup.getContext()).inflate((int) R.layout.f900file_item_grid, viewGroup, false);
                }
                if (fileItem.blv) {
                    view.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aVK));
                } else {
                    view.setBackgroundDrawable(null);
                }
                TextView textView4 = (TextView) view;
                textView4.setText(fileItem.blw.getName());
                textView4.setTextColor(e.Pb().getColor(16));
                textView4.setCompoundDrawablesWithIntrinsicBounds(0, i2, 0, 0);
                return view;
        }
    }

    public void n(File file) {
        int i = 0;
        this.bnz = file;
        if (this.bnz.exists() && this.bnz.isDirectory()) {
            File[] listFiles = this.bnz.listFiles(new FileFilter() {
                public boolean accept(File file) {
                    if (AdapterFile.this.bnC != null && ActivityChooseFile.uz.equals(AdapterFile.this.bnC[0])) {
                        return file.isDirectory();
                    }
                    boolean z = AdapterFile.this.bnC == null || file.isDirectory();
                    if (z) {
                        return z;
                    }
                    String name = file.getName();
                    for (String endsWith : AdapterFile.this.bnC) {
                        if (name.endsWith(endsWith)) {
                            return true;
                        }
                    }
                    return z;
                }
            });
            if (listFiles == null || listFiles.length <= 0) {
                this.bnA = new FileItem[0];
                Arrays.sort(this.bnA);
            } else {
                this.bnA = new FileItem[listFiles.length];
                int length = listFiles.length;
                int i2 = 0;
                while (i < length) {
                    this.bnA[i2] = new FileItem(listFiles[i]);
                    i++;
                    i2++;
                }
                Arrays.sort(this.bnA);
            }
            notifyDataSetChanged();
        }
    }

    public void setType(int i) {
        this.type = i;
        notifyDataSetChanged();
    }
}
