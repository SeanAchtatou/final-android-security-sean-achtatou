package com.uc.browser;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.Timer;
import java.util.TimerTask;

public class WebViewSafePopup extends LinearLayout implements View.OnClickListener {
    private static final int tw = 15000;
    private static final int tx = -5172461;
    public static final int ty = 1;
    public static final int tz = 0;
    private TextView tA;
    TimerTask tB;
    Timer tC = new Timer();

    public WebViewSafePopup(Context context) {
        super(context);
        setOrientation(0);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        setBackgroundColor(tx);
        this.tA = new TextView(context);
        this.tA.setText("");
        this.tA.setTextColor(-1);
        this.tA.setPadding(4, 4, 4, 4);
        this.tA.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.f650webview_safe_popup, 0, 0, 0);
        this.tA.setCompoundDrawablePadding(3);
        addView(this.tA);
        this.tA.setFocusable(false);
        setFocusable(false);
        setOnClickListener(this);
    }

    public void K(String str) {
        if (str != null) {
            this.tA.setText(str);
        }
    }

    public void onClick(View view) {
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().iK();
        }
    }

    public void setVisibility(int i) {
        if (8 == i && 8 != getVisibility()) {
            super.setVisibility(8);
        } else if (i == 0) {
            if (getVisibility() != 0) {
                super.setVisibility(0);
                bringToFront();
            }
            if (this.tB != null) {
                this.tB.cancel();
                this.tB = null;
            }
            this.tB = new TimerTask() {
                public void run() {
                    if (8 != WebViewSafePopup.this.getVisibility()) {
                        ModelBrowser.gD().a(114, 0, (Object) null);
                        WebViewSafePopup.this.tB = null;
                    }
                }
            };
            this.tC.schedule(this.tB, 15000);
        }
    }
}
