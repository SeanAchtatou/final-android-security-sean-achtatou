package com.uc.plugin;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import com.uc.browser.ModelBrowser;
import com.uc.browser.R;

final class s extends Thread {
    String aGn = null;
    Dialog aGo = null;
    Activity aGp = null;
    final /* synthetic */ Context aGq;

    s(Context context) {
        this.aGq = context;
    }

    public Thread a(String str, Dialog dialog, Activity activity) {
        this.aGn = str;
        this.aGo = dialog;
        this.aGp = activity;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.plugin.p.g(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.uc.plugin.p.g(java.lang.String, java.lang.String):void
      com.uc.c.by.g(java.lang.String, java.lang.String):void
      com.uc.plugin.p.g(java.lang.String, boolean):boolean */
    public void run() {
        if (ac.eE("application/x-shockwave-flash").g(this.aGn, false)) {
            ModelBrowser.gD().a(65, this.aGq.getString(R.string.f1457plugin_install_success));
        } else {
            ModelBrowser.gD().a(65, this.aGq.getString(R.string.f1458plugin_install_error_incorrect_format));
        }
        this.aGo.dismiss();
        if (this.aGp != null) {
            this.aGp.setRequestedOrientation(4);
        }
    }
}
