package com.uc.plugin;

import android.content.DialogInterface;
import android.view.View;

class ai implements DialogInterface.OnClickListener {
    final /* synthetic */ e cgz;
    private View ls;

    ai(e eVar) {
        this.cgz = eVar;
    }

    public DialogInterface.OnClickListener b(View view) {
        this.ls = view;
        return this;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.ls.setOnClickListener(null);
        if (this.cgz.jh.abd != null) {
            this.cgz.jh.abd.Ib();
            this.cgz.jh.abd = null;
        }
    }
}
