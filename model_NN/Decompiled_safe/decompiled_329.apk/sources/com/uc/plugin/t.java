package com.uc.plugin;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import com.uc.browser.ModelBrowser;
import com.uc.browser.R;

final class t extends Thread {
    Dialog aGo = null;
    Activity aGp = null;
    final /* synthetic */ Context aGq;
    ad aGr = null;

    t(Context context) {
        this.aGq = context;
    }

    public Thread a(ad adVar, Dialog dialog, Activity activity) {
        this.aGr = adVar;
        this.aGo = dialog;
        this.aGp = activity;
        return this;
    }

    public void run() {
        if (this.aGr.Fp()) {
            ModelBrowser.gD().a(65, this.aGq.getString(R.string.f1457plugin_install_success));
        } else {
            ModelBrowser.gD().a(65, this.aGq.getString(R.string.f1458plugin_install_error_incorrect_format));
        }
        this.aGo.dismiss();
        if (this.aGp != null) {
            this.aGp.setRequestedOrientation(4);
        }
    }
}
