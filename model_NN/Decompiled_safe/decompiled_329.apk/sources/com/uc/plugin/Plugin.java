package com.uc.plugin;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import b.a.a.f;
import com.uc.browser.ModelBrowser;
import com.uc.browser.R;
import com.uc.browser.UCAlertDialog;
import com.uc.c.bx;
import java.lang.reflect.Method;
import java.util.Vector;

public class Plugin implements o {
    static final int cer = 0;
    static final int ces = 1;
    static final int cet = 2;
    static final int ceu = 3;
    static final int cev = 4;
    String aba;
    bx abd;
    Context bon = null;
    int ceA = 0;
    boolean ceB = false;
    String ceC = "";
    Vector ceD = null;
    Vector ceE = null;
    Vector ceF = null;
    c ceG = null;
    q ceH = null;
    Vector cew;
    Vector cex;
    ag cey;
    boolean cez = false;
    Context mContext;
    Handler mHandler;

    public Plugin(String str, Vector vector, Vector vector2, Context context, Handler handler, Context context2) {
        this.mHandler = handler;
        this.aba = str;
        this.cew = vector;
        this.cex = vector2;
        this.mContext = context;
        this.bon = context2;
        this.cey = new ag(this.mContext);
        this.ceB = ae.b(this);
        Qc();
    }

    private void Qc() {
        this.ceA = 0;
        this.cey.removeAllViews();
        this.cey.setBackgroundResource(!this.ceB ? R.drawable.f597plugin_bg_day : R.drawable.f598plugin_bg_night);
        ag agVar = this.cey;
        ag.inflate(this.mContext, !this.ceB ? R.layout.f924plugin_needdownload_day : R.layout.f925plugin_needdownload_night, this.cey);
        ((Button) this.cey.findViewById(R.id.f808plugin_btn)).setOnClickListener(new j(this));
    }

    /* access modifiers changed from: private */
    public void Qd() {
        this.ceA = 2;
        this.cey.removeAllViews();
        this.cey.setBackgroundResource(!this.ceB ? R.drawable.f597plugin_bg_day : R.drawable.f598plugin_bg_night);
        ag agVar = this.cey;
        ag.inflate(this.mContext, !this.ceB ? R.layout.f920plugin_downloadfail_day : R.layout.f921plugin_downloadfail_night, this.cey);
        ((Button) this.cey.findViewById(R.id.f808plugin_btn)).setOnClickListener(new i(this));
    }

    /* access modifiers changed from: private */
    public void Qe() {
        this.ceA = 1;
        this.cey.removeAllViews();
        this.cey.setBackgroundResource(!this.ceB ? R.drawable.f597plugin_bg_day : R.drawable.f598plugin_bg_night);
        ag agVar = this.cey;
        ag.inflate(this.mContext, !this.ceB ? R.layout.f922plugin_downloading_day : R.layout.f923plugin_downloading_night, this.cey);
        ((Button) this.cey.findViewById(R.id.f808plugin_btn)).setOnClickListener(new f(this));
    }

    /* access modifiers changed from: private */
    public void Qf() {
        this.ceA = 4;
        this.cey.removeAllViews();
        this.cey.setBackgroundResource(!this.ceB ? R.drawable.f597plugin_bg_day : R.drawable.f598plugin_bg_night);
        ag agVar = this.cey;
        ag.inflate(this.mContext, !this.ceB ? R.layout.f922plugin_downloading_day : R.layout.f923plugin_downloading_night, this.cey);
        ((Button) this.cey.findViewById(R.id.f808plugin_btn)).setOnClickListener(new e(this));
    }

    /* access modifiers changed from: private */
    public void Qg() {
        this.ceA = 3;
        this.cey.removeAllViews();
        this.cey.setBackgroundResource(!this.ceB ? R.drawable.f597plugin_bg_day : R.drawable.f598plugin_bg_night);
        this.ceH.a(this.mContext, this.cew, this.cex);
        this.cey.l(this.ceH.xA());
    }

    public void IR() {
        this.ceB = true;
        Qb();
    }

    public void IS() {
        this.ceB = false;
        Qb();
    }

    /* access modifiers changed from: package-private */
    public void Qb() {
        this.cey.setBackgroundResource(!this.ceB ? R.drawable.f597plugin_bg_day : R.drawable.f598plugin_bg_night);
        switch (this.ceA) {
            case 0:
                Qc();
                return;
            case 1:
                Qe();
                return;
            case 2:
                Qd();
                return;
            case 3:
                d("NIGHTMODE", Boolean.valueOf(this.ceB), null);
                return;
            case 4:
                Qf();
                return;
            default:
                return;
        }
    }

    public ag Qh() {
        return this.cey;
    }

    public void Qi() {
        ae.c(this);
        ac.eE(getType()).b(this);
    }

    public void a(c cVar) {
        this.ceG = cVar;
    }

    public void a(Class cls, Method method, Method method2, Method method3) {
        this.ceH = new q(this, cls, method, method2, method3);
        this.cez = true;
        if (!(this.ceD == null || this.ceE == null || this.ceF == null)) {
            int size = this.ceD.size();
            for (int i = 0; i < size; i++) {
                this.ceH.b((String) this.ceD.get(i), this.ceE.get(i), this.ceF.get(i));
            }
            this.ceD.clear();
            this.ceE.clear();
            this.ceF.clear();
            this.ceD = null;
            this.ceE = null;
            this.ceF = null;
        }
        this.mHandler.post(new a(this));
    }

    public void d(String str, Object obj, Object obj2) {
        this.mHandler.post(new d(this).a(str, obj, obj2));
    }

    public void eA(int i) {
        this.mHandler.post(new b(this).D(i));
    }

    public String getType() {
        return this.aba;
    }

    /* access modifiers changed from: package-private */
    public void kD(int i) {
        this.mHandler.post(new n(this).D(i));
    }

    public void loadSrc(String str, String str2) {
        if (str == null) {
            this.mHandler.post(new l(this));
            this.abd.Ib();
            this.abd = null;
            return;
        }
        this.ceC = str2;
        k kVar = new k(this);
        this.mHandler.post(new m(this));
        this.abd = new bx(null, str, str2, kVar);
        this.abd.start();
    }

    public void loadUrl(String str) {
        if (str != null) {
            try {
                ModelBrowser.gD().a(11, str);
                if (this.ceG != null) {
                    this.ceG.A(str);
                }
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void m(View view) {
        UCAlertDialog.Builder builder = this.bon == null ? new UCAlertDialog.Builder(this.mContext) : new UCAlertDialog.Builder(this.bon);
        builder.L("提示");
        builder.setCancelable(false);
        builder.M("确认下载插件吗？");
        builder.a("确定", new g(this).b(view)).c("取消", new h(this));
        builder.fh();
        builder.show();
    }

    public void stat(int i, String str, String str2, int i2) {
        f.l(i, str);
    }
}
