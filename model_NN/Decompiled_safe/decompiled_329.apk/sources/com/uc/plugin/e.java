package com.uc.plugin;

import android.view.View;
import com.uc.browser.UCAlertDialog;

class e implements View.OnClickListener {
    final /* synthetic */ Plugin jh;

    e(Plugin plugin) {
        this.jh = plugin;
    }

    public void onClick(View view) {
        if (this.jh.abd != null) {
            this.jh.abd.HZ();
        }
        UCAlertDialog.Builder builder = this.jh.bon == null ? new UCAlertDialog.Builder(this.jh.mContext) : new UCAlertDialog.Builder(this.jh.bon);
        builder.L("提示");
        builder.setCancelable(false);
        builder.M("是否取消加载flash？");
        builder.a("确定", new ai(this).b(view)).c("取消", new ah(this));
        builder.fh();
        builder.show();
    }
}
