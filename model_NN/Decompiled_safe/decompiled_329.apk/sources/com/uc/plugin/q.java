package com.uc.plugin;

import android.content.Context;
import android.view.View;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Vector;

class q {
    Class aaW;
    Method aaX;
    Method aaY;
    Method aaZ;
    Object avl;
    boolean avm = false;
    final /* synthetic */ Plugin jh;

    q(Plugin plugin, Class cls, Method method, Method method2, Method method3) {
        this.jh = plugin;
        this.aaW = cls;
        this.aaX = method;
        this.aaY = method2;
        this.aaZ = method3;
        try {
            this.avl = this.aaW.newInstance();
        } catch (IllegalAccessException | InstantiationException e) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, Vector vector, Vector vector2) {
        if (!this.avm) {
            this.avm = true;
            try {
                this.aaX.invoke(this.avl, context, vector, vector2, this.jh, Plugin.class.getMethods());
                this.jh.d("NIGHTMODE", Boolean.valueOf(this.jh.ceB), null);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(String str, Object obj, Object obj2) {
        try {
            this.aaZ.invoke(this.avl, str, obj, obj2);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
        }
    }

    /* access modifiers changed from: package-private */
    public View xA() {
        try {
            return (View) this.aaY.invoke(this.avl, new Object[0]);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            return null;
        }
    }
}
