package com.uc.plugin;

import android.content.Context;
import android.content.pm.PackageManager;
import com.uc.c.au;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

class r extends af {
    static String aEv = "~TMP~";
    FileInputStream aEw = null;
    Context mContext = null;

    public r(FileInputStream fileInputStream, Context context) {
        this.aEw = fileInputStream;
        this.mContext = context;
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0068 A[SYNTHETIC, Splitter:B:31:0x0068] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0074 A[SYNTHETIC, Splitter:B:39:0x0074] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(java.io.File r7, java.io.InputStream r8, java.util.zip.ZipEntry r9) {
        /*
            r6 = this;
            r5 = 0
            java.lang.String r4 = "/"
            java.lang.String r0 = r9.getName()
            java.lang.String r1 = "/"
            int r1 = r0.lastIndexOf(r4)
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)
            java.io.File r1 = new java.io.File
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = r7.getPath()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "/"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            r1.createNewFile()     // Catch:{ IOException -> 0x0050 }
            r0 = 1
            r2 = 0
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0082, IOException -> 0x0064, all -> 0x0070 }
            r3.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0082, IOException -> 0x0064, all -> 0x0070 }
            r1 = 10240(0x2800, float:1.4349E-41)
            byte[] r1 = new byte[r1]     // Catch:{ FileNotFoundException -> 0x0058, IOException -> 0x007f, all -> 0x007c }
        L_0x0041:
            int r2 = r8.read(r1)     // Catch:{ FileNotFoundException -> 0x0058, IOException -> 0x007f, all -> 0x007c }
            if (r2 >= 0) goto L_0x0053
            r3.flush()     // Catch:{ FileNotFoundException -> 0x0058, IOException -> 0x007f, all -> 0x007c }
            if (r3 == 0) goto L_0x004f
            r3.close()     // Catch:{ IOException -> 0x0078 }
        L_0x004f:
            return r0
        L_0x0050:
            r0 = move-exception
            r0 = r5
            goto L_0x004f
        L_0x0053:
            r4 = 0
            r3.write(r1, r4, r2)     // Catch:{ FileNotFoundException -> 0x0058, IOException -> 0x007f, all -> 0x007c }
            goto L_0x0041
        L_0x0058:
            r0 = move-exception
            r0 = r3
        L_0x005a:
            if (r0 == 0) goto L_0x0085
            r0.close()     // Catch:{ IOException -> 0x0061 }
            r0 = r5
            goto L_0x004f
        L_0x0061:
            r0 = move-exception
            r0 = r5
            goto L_0x004f
        L_0x0064:
            r0 = move-exception
            r0 = r2
        L_0x0066:
            if (r0 == 0) goto L_0x0085
            r0.close()     // Catch:{ IOException -> 0x006d }
            r0 = r5
            goto L_0x004f
        L_0x006d:
            r0 = move-exception
            r0 = r5
            goto L_0x004f
        L_0x0070:
            r0 = move-exception
            r1 = r2
        L_0x0072:
            if (r1 == 0) goto L_0x0077
            r1.close()     // Catch:{ IOException -> 0x007a }
        L_0x0077:
            throw r0
        L_0x0078:
            r1 = move-exception
            goto L_0x004f
        L_0x007a:
            r1 = move-exception
            goto L_0x0077
        L_0x007c:
            r0 = move-exception
            r1 = r3
            goto L_0x0072
        L_0x007f:
            r0 = move-exception
            r0 = r3
            goto L_0x0066
        L_0x0082:
            r0 = move-exception
            r0 = r2
            goto L_0x005a
        L_0x0085:
            r0 = r5
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.plugin.r.a(java.io.File, java.io.InputStream, java.util.zip.ZipEntry):boolean");
    }

    private boolean b(File file, File file2) {
        if (file == null || !file.isDirectory() || file2 == null) {
            return false;
        }
        if (!k(file2)) {
            return false;
        }
        return file.renameTo(file2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0068 A[SYNTHETIC, Splitter:B:31:0x0068] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0074 A[SYNTHETIC, Splitter:B:39:0x0074] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b(java.io.File r7, java.io.InputStream r8, java.util.zip.ZipEntry r9) {
        /*
            r6 = this;
            r5 = 0
            java.lang.String r4 = "/"
            java.lang.String r0 = r9.getName()
            java.lang.String r1 = "/"
            int r1 = r0.lastIndexOf(r4)
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)
            java.io.File r1 = new java.io.File
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = r7.getPath()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "/"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            r1.createNewFile()     // Catch:{ IOException -> 0x0050 }
            r0 = 1
            r2 = 0
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0082, IOException -> 0x0064, all -> 0x0070 }
            r3.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0082, IOException -> 0x0064, all -> 0x0070 }
            r1 = 10240(0x2800, float:1.4349E-41)
            byte[] r1 = new byte[r1]     // Catch:{ FileNotFoundException -> 0x0058, IOException -> 0x007f, all -> 0x007c }
        L_0x0041:
            int r2 = r8.read(r1)     // Catch:{ FileNotFoundException -> 0x0058, IOException -> 0x007f, all -> 0x007c }
            if (r2 >= 0) goto L_0x0053
            r3.flush()     // Catch:{ FileNotFoundException -> 0x0058, IOException -> 0x007f, all -> 0x007c }
            if (r3 == 0) goto L_0x004f
            r3.close()     // Catch:{ IOException -> 0x0078 }
        L_0x004f:
            return r0
        L_0x0050:
            r0 = move-exception
            r0 = r5
            goto L_0x004f
        L_0x0053:
            r4 = 0
            r3.write(r1, r4, r2)     // Catch:{ FileNotFoundException -> 0x0058, IOException -> 0x007f, all -> 0x007c }
            goto L_0x0041
        L_0x0058:
            r0 = move-exception
            r0 = r3
        L_0x005a:
            if (r0 == 0) goto L_0x0085
            r0.close()     // Catch:{ IOException -> 0x0061 }
            r0 = r5
            goto L_0x004f
        L_0x0061:
            r0 = move-exception
            r0 = r5
            goto L_0x004f
        L_0x0064:
            r0 = move-exception
            r0 = r2
        L_0x0066:
            if (r0 == 0) goto L_0x0085
            r0.close()     // Catch:{ IOException -> 0x006d }
            r0 = r5
            goto L_0x004f
        L_0x006d:
            r0 = move-exception
            r0 = r5
            goto L_0x004f
        L_0x0070:
            r0 = move-exception
            r1 = r2
        L_0x0072:
            if (r1 == 0) goto L_0x0077
            r1.close()     // Catch:{ IOException -> 0x007a }
        L_0x0077:
            throw r0
        L_0x0078:
            r1 = move-exception
            goto L_0x004f
        L_0x007a:
            r1 = move-exception
            goto L_0x0077
        L_0x007c:
            r0 = move-exception
            r1 = r3
            goto L_0x0072
        L_0x007f:
            r0 = move-exception
            r0 = r3
            goto L_0x0066
        L_0x0082:
            r0 = move-exception
            r0 = r2
            goto L_0x005a
        L_0x0085:
            r0 = r5
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.plugin.r.b(java.io.File, java.io.InputStream, java.util.zip.ZipEntry):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x005e A[SYNTHETIC, Splitter:B:34:0x005e] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x006a A[SYNTHETIC, Splitter:B:42:0x006a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean c(java.io.File r7, java.io.InputStream r8, java.util.zip.ZipEntry r9) {
        /*
            r6 = this;
            r5 = 0
            java.io.File r0 = new java.io.File
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = r7.getPath()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "/"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r9.getName()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            r0.createNewFile()     // Catch:{ IOException -> 0x0043 }
            r1 = 1
            r2 = 0
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x005a, all -> 0x0066 }
            r3.<init>(r0)     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x005a, all -> 0x0066 }
            r0 = 10240(0x2800, float:1.4349E-41)
            byte[] r0 = new byte[r0]     // Catch:{ FileNotFoundException -> 0x004b, IOException -> 0x0073, all -> 0x0070 }
        L_0x0033:
            int r2 = r8.read(r0)     // Catch:{ FileNotFoundException -> 0x004b, IOException -> 0x0073, all -> 0x0070 }
            if (r2 >= 0) goto L_0x0046
            r3.flush()     // Catch:{ FileNotFoundException -> 0x004b, IOException -> 0x0073, all -> 0x0070 }
            if (r3 == 0) goto L_0x007b
            r3.close()     // Catch:{ IOException -> 0x0054 }
            r0 = r1
        L_0x0042:
            return r0
        L_0x0043:
            r0 = move-exception
            r0 = r5
            goto L_0x0042
        L_0x0046:
            r4 = 0
            r3.write(r0, r4, r2)     // Catch:{ FileNotFoundException -> 0x004b, IOException -> 0x0073, all -> 0x0070 }
            goto L_0x0033
        L_0x004b:
            r0 = move-exception
            r0 = r3
        L_0x004d:
            if (r0 == 0) goto L_0x0079
            r0.close()     // Catch:{ IOException -> 0x0057 }
            r0 = r5
            goto L_0x0042
        L_0x0054:
            r0 = move-exception
            r0 = r1
            goto L_0x0042
        L_0x0057:
            r0 = move-exception
            r0 = r5
            goto L_0x0042
        L_0x005a:
            r0 = move-exception
            r0 = r2
        L_0x005c:
            if (r0 == 0) goto L_0x0079
            r0.close()     // Catch:{ IOException -> 0x0063 }
            r0 = r5
            goto L_0x0042
        L_0x0063:
            r0 = move-exception
            r0 = r5
            goto L_0x0042
        L_0x0066:
            r0 = move-exception
            r1 = r2
        L_0x0068:
            if (r1 == 0) goto L_0x006d
            r1.close()     // Catch:{ IOException -> 0x006e }
        L_0x006d:
            throw r0
        L_0x006e:
            r1 = move-exception
            goto L_0x006d
        L_0x0070:
            r0 = move-exception
            r1 = r3
            goto L_0x0068
        L_0x0073:
            r0 = move-exception
            r0 = r3
            goto L_0x005c
        L_0x0076:
            r0 = move-exception
            r0 = r2
            goto L_0x004d
        L_0x0079:
            r0 = r5
            goto L_0x0042
        L_0x007b:
            r0 = r1
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.plugin.r.c(java.io.File, java.io.InputStream, java.util.zip.ZipEntry):boolean");
    }

    private boolean h(File file) {
        boolean c;
        if (!j(file)) {
            return false;
        }
        ZipInputStream zipInputStream = new ZipInputStream(this.aEw);
        do {
            try {
                ZipEntry nextEntry = zipInputStream.getNextEntry();
                if (nextEntry == null) {
                    return true;
                }
                String name = nextEntry.getName();
                c = name.contains(".apk") ? c(file, zipInputStream, nextEntry) : name.startsWith("lib") ? b(file, zipInputStream, nextEntry) : name.startsWith("res") ? a(file, zipInputStream, nextEntry) : true;
                zipInputStream.closeEntry();
            } catch (IOException e) {
                return false;
            }
        } while (c);
        return false;
    }

    private static boolean i(File file) {
        if (!file.exists()) {
            return true;
        }
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                if (file2.isDirectory()) {
                    i(file2);
                } else {
                    file2.delete();
                }
            }
        }
        return file.delete();
    }

    private static boolean j(File file) {
        if (file == null) {
            return false;
        }
        if (file.exists()) {
            return file.isDirectory();
        }
        file.mkdirs();
        return true;
    }

    private boolean k(File file) {
        if (!file.exists()) {
            return true;
        }
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                if (file2.getName().contains(".apk") && !file2.delete()) {
                    return false;
                }
            }
        }
        return i(file);
    }

    public boolean g(File file) {
        if (file == null) {
            return false;
        }
        if (file.exists() && !file.isDirectory()) {
            return false;
        }
        if (!isValid()) {
            return false;
        }
        File file2 = new File(file.getParent() + au.aGF + aEv);
        if (!i(file2)) {
            return false;
        }
        if (!h(file2)) {
            i(file2);
            return false;
        } else if (b(file2, file)) {
            return true;
        } else {
            i(file2);
            return false;
        }
    }

    public boolean isValid() {
        try {
            if (this.aEw.available() != zk()) {
                return false;
            }
            if (this.aEw.read() != 1) {
                return false;
            }
            this.aEw.skip(4);
            this.aEw.skip(1);
            this.aEw.skip(4);
            if (this.aEw.read() > this.mContext.getPackageManager().getPackageInfo(this.mContext.getPackageName(), 0).versionCode) {
                return false;
            }
            this.aEw.skip(16);
            return true;
        } catch (IOException e) {
            return false;
        } catch (PackageManager.NameNotFoundException e2) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public int zk() {
        byte[] bArr = new byte[4];
        this.aEw.read(bArr);
        return ((bArr[0] << 24) & -16777216) | (bArr[3] & 255) | ((bArr[2] << 8) & 65280) | ((bArr[1] << 16) & 16711680);
    }
}
