package com.uc.plugin;

import android.widget.ProgressBar;
import com.uc.browser.R;

class b implements Runnable {
    final /* synthetic */ Plugin jh;
    int ll;

    b(Plugin plugin) {
        this.jh = plugin;
    }

    public Runnable D(int i) {
        this.ll = i;
        return this;
    }

    public void run() {
        if (this.ll < 0) {
            this.jh.Qd();
            return;
        }
        if (this.jh.ceA != 1) {
            this.jh.d("BACKGROUND", null, null);
            this.jh.Qe();
        }
        ProgressBar progressBar = (ProgressBar) this.jh.cey.findViewById(R.id.f811plugin_progressbar);
        if (progressBar != null) {
            progressBar.setProgress(this.ll);
            progressBar.postInvalidate();
        }
    }
}
