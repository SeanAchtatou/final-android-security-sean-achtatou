package com.uc.plugin;

import android.widget.ProgressBar;
import com.uc.browser.R;

class n implements Runnable {
    final /* synthetic */ Plugin jh;
    int ll;

    n(Plugin plugin) {
        this.jh = plugin;
    }

    public Runnable D(int i) {
        this.ll = i;
        return this;
    }

    public void run() {
        ProgressBar progressBar;
        if (this.ll >= 0 && (progressBar = (ProgressBar) this.jh.cey.findViewById(R.id.f811plugin_progressbar)) != null) {
            progressBar.setProgress(this.ll);
            progressBar.postInvalidate();
        }
    }
}
