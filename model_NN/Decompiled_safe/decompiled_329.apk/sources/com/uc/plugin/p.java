package com.uc.plugin;

import android.content.Context;
import android.view.View;
import com.uc.browser.UCAlertDialog;
import com.uc.c.au;
import com.uc.c.bx;
import com.uc.c.by;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.lang.reflect.Method;
import java.util.Timer;
import java.util.Vector;

public class p implements by {
    static final String abg = "PLUGINLASTCHECK";
    static final long abh = 604800000;
    Vector aaV = new Vector();
    private Class aaW;
    private Method aaX;
    private Method aaY;
    private Method aaZ;
    private String aba;
    private boolean abb = false;
    private boolean abc = true;
    /* access modifiers changed from: private */
    public bx abd = null;
    boolean abe = false;
    String abf = "0";
    final /* synthetic */ ac abi;

    p(ac acVar, String str) {
        this.abi = acVar;
        this.aba = str;
        File file = new File(ac.boi);
        if (file.exists()) {
            cC(ac.boi);
            file.delete();
        }
        if (us()) {
            ut();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0043 A[SYNTHETIC, Splitter:B:16:0x0043] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0048 A[SYNTHETIC, Splitter:B:19:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x006a A[SYNTHETIC, Splitter:B:39:0x006a] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x006f A[SYNTHETIC, Splitter:B:42:0x006f] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:61:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void N(java.lang.String r9, java.lang.String r10) {
        /*
            r8 = this;
            r4 = 0
            r6 = 0
            java.io.File r0 = new java.io.File
            r0.<init>(r9)
            java.io.File r1 = new java.io.File
            r1.<init>(r10)
            java.io.File r2 = new java.io.File
            java.lang.String r3 = r1.getParent()
            r2.<init>(r3)
            boolean r3 = r2.exists()
            if (r3 != 0) goto L_0x0022
            boolean r2 = r2.mkdirs()
            if (r2 != 0) goto L_0x0022
        L_0x0021:
            return
        L_0x0022:
            r2 = 10240(0x2800, float:1.4349E-41)
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x0085, all -> 0x0065 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0085, all -> 0x0065 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x0085, all -> 0x0065 }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0089, all -> 0x007b }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0089, all -> 0x007b }
        L_0x0030:
            int r4 = r3.available()     // Catch:{ Exception -> 0x003f, all -> 0x007f }
            if (r4 <= 0) goto L_0x0052
            int r4 = r3.read(r2)     // Catch:{ Exception -> 0x003f, all -> 0x007f }
            r5 = 0
            r0.write(r2, r5, r4)     // Catch:{ Exception -> 0x003f, all -> 0x007f }
            goto L_0x0030
        L_0x003f:
            r2 = move-exception
            r2 = r3
        L_0x0041:
            if (r2 == 0) goto L_0x0046
            r2.close()     // Catch:{ IOException -> 0x0075 }
        L_0x0046:
            if (r0 == 0) goto L_0x008d
            r0.close()     // Catch:{ IOException -> 0x0062 }
            r0 = r6
        L_0x004c:
            if (r0 != 0) goto L_0x0021
            r1.delete()
            goto L_0x0021
        L_0x0052:
            r2 = 1
            if (r3 == 0) goto L_0x0058
            r3.close()     // Catch:{ IOException -> 0x0073 }
        L_0x0058:
            if (r0 == 0) goto L_0x008f
            r0.close()     // Catch:{ IOException -> 0x005f }
            r0 = r2
            goto L_0x004c
        L_0x005f:
            r0 = move-exception
            r0 = r2
            goto L_0x004c
        L_0x0062:
            r0 = move-exception
            r0 = r6
            goto L_0x004c
        L_0x0065:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x0068:
            if (r2 == 0) goto L_0x006d
            r2.close()     // Catch:{ IOException -> 0x0077 }
        L_0x006d:
            if (r1 == 0) goto L_0x0072
            r1.close()     // Catch:{ IOException -> 0x0079 }
        L_0x0072:
            throw r0
        L_0x0073:
            r3 = move-exception
            goto L_0x0058
        L_0x0075:
            r2 = move-exception
            goto L_0x0046
        L_0x0077:
            r2 = move-exception
            goto L_0x006d
        L_0x0079:
            r1 = move-exception
            goto L_0x0072
        L_0x007b:
            r0 = move-exception
            r1 = r4
            r2 = r3
            goto L_0x0068
        L_0x007f:
            r1 = move-exception
            r2 = r3
            r7 = r0
            r0 = r1
            r1 = r7
            goto L_0x0068
        L_0x0085:
            r0 = move-exception
            r0 = r4
            r2 = r4
            goto L_0x0041
        L_0x0089:
            r0 = move-exception
            r0 = r4
            r2 = r3
            goto L_0x0041
        L_0x008d:
            r0 = r6
            goto L_0x004c
        L_0x008f:
            r0 = r2
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.plugin.p.N(java.lang.String, java.lang.String):void");
    }

    private String a(ClassLoader classLoader) {
        return "FlashPlugin";
    }

    private void eC(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.aaV.size()) {
                ((o) this.aaV.get(i3)).eA(i);
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    private String getPackage() {
        return "com.uc.plugins.flash";
    }

    private boolean us() {
        Class<Object> cls = Object.class;
        if (this.abb) {
            return true;
        }
        DexClassLoader dexClassLoader = new DexClassLoader(uw() + uv() + ".apk", uw(), uw(), ClassLoader.getSystemClassLoader());
        try {
            this.aaW = dexClassLoader.loadClass(getPackage() + "." + a(dexClassLoader));
            try {
                this.aaX = this.aaW.getMethod("initPlugin", Context.class, Vector.class, Vector.class, Object.class, Method[].class);
                this.aaY = this.aaW.getMethod("getWindow", null);
                this.aaZ = this.aaW.getMethod("onEvent", String.class, Object.class, Object.class);
                try {
                    this.abf = (String) dexClassLoader.loadClass(getPackage() + ".Info").getMethod("getVersion", null).invoke(null, null);
                    this.abb = true;
                    return true;
                } catch (Exception e) {
                    return false;
                }
            } catch (SecurityException e2) {
                return false;
            } catch (NoSuchMethodException e3) {
                return false;
            }
        } catch (ClassNotFoundException e4) {
            return false;
        }
    }

    private void uu() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.aaV.size()) {
                ((o) this.aaV.get(i2)).a(this.aaW, this.aaX, this.aaY, this.aaZ);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private String uv() {
        return "flash";
    }

    private String uw() {
        return ac.bog + uv() + au.aGF;
    }

    public void a(o oVar) {
        if (this.abb && this.abc) {
            oVar.a(this.aaW, this.aaX, this.aaY, this.aaZ);
        }
        this.aaV.add(oVar);
    }

    public void a(String str, String str2, int i, int i2) {
        if (!this.abe) {
            eC((i * 95) / i2);
        }
    }

    public void b(o oVar) {
        this.aaV.remove(oVar);
    }

    public synchronized void cA(String str) {
        this.abc = false;
        this.abe = false;
        eC(0);
        this.abd = new bx(null, str, "/data/data/com.uc.browser/tmp", this);
        this.abd.start();
    }

    public synchronized void cB(String str) {
        cA(str);
    }

    public boolean cC(String str) {
        if (!new ad(new File(str), new File(uw()), ac.bka).isValid()) {
            return false;
        }
        N(str, "/sdcard/UCDownloads/flash.upp");
        if (!this.abb) {
            return new ad(new File(str), new File(uw()), ac.bka).Fp();
        }
        new File(str).renameTo(new File(ac.boi));
        ac.mHandler.post(new v(this));
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.plugin.p.g(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.uc.plugin.p.g(java.lang.String, java.lang.String):void
      com.uc.c.by.g(java.lang.String, java.lang.String):void
      com.uc.plugin.p.g(java.lang.String, boolean):boolean */
    public void e(String str, String str2) {
        if (str2 != null) {
            this.abd = null;
            if (!this.abc) {
                this.abc = true;
                this.abe = true;
                g(str2, true);
                if (!this.abb) {
                    f(str, str2);
                }
            }
        }
    }

    public void f(String str, String str2) {
        ac.mHandler.post(new u(this));
        this.abe = true;
        this.abc = true;
        this.abd = null;
        if (str2 != null) {
            new File(str2).delete();
        }
        eC(-1);
    }

    public void g(String str, String str2) {
        f(str, str2);
    }

    public boolean g(String str, boolean z) {
        if (!cC(str) || !us()) {
            return false;
        }
        uu();
        if (z) {
            new File(str).delete();
        }
        return true;
    }

    public void h(View view) {
        this.abe = true;
        if (this.abd != null) {
            this.abd.HZ();
            UCAlertDialog.Builder builder = ac.bon == null ? new UCAlertDialog.Builder(ac.bka) : new UCAlertDialog.Builder(ac.bon);
            builder.L("插件");
            builder.setCancelable(false);
            builder.M("是否取消插件下载？");
            builder.a("确定", new x(this).b(view)).c("取消", new y(this));
            builder.fh();
            builder.show();
        }
    }

    public boolean isLoaded() {
        return this.abb;
    }

    /* access modifiers changed from: package-private */
    public void ut() {
        new Timer().schedule(new w(this), 5000);
    }
}
