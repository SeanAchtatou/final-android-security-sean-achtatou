package com.uc.plugin;

import android.content.SharedPreferences;
import com.uc.a.e;
import java.util.TimerTask;

class w extends TimerTask {
    final /* synthetic */ p baP;

    w(p pVar) {
        this.baP = pVar;
    }

    public void run() {
        SharedPreferences sharedPreferences = ac.bka.getSharedPreferences("PLUGINLASTCHECK", 0);
        long j = sharedPreferences.getLong("PLUGINLASTCHECK", 0);
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - j > 604800000) {
            e.nR().u("flash", this.baP.abf);
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putLong("PLUGINLASTCHECK", currentTimeMillis);
            edit.commit();
        }
    }
}
