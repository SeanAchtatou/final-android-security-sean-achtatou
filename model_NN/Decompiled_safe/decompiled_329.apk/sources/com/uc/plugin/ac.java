package com.uc.plugin;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import java.util.Vector;

public class ac {
    static Activity bka = null;
    public static final String bog = "/data/data/com.uc.browser/plugins/";
    private static final String boh = "/data/data/com.uc.browser/tmp";
    public static final String boi = "/data/data/com.uc.browser/tmp_local";
    private static final String boj = "/sdcard/UCDownloads/flash.upp";
    static ac bol = null;
    static boolean bom = true;
    static Context bon = null;
    static Handler mHandler = null;
    p bok = null;

    private ac() {
    }

    public static void EY() {
        mHandler = new Handler();
    }

    public static synchronized Plugin a(String str, Vector vector, Vector vector2) {
        Plugin plugin;
        synchronized (ac.class) {
            if (!bom) {
                plugin = null;
            } else {
                if (str != null) {
                    if (str.equalsIgnoreCase("application/x-shockwave-flash")) {
                        if (bol == null) {
                            bol = new ac();
                        }
                        plugin = bol.b(str, vector, vector2, null);
                    }
                }
                plugin = null;
            }
        }
        return plugin;
    }

    public static synchronized Plugin a(String str, Vector vector, Vector vector2, Context context) {
        Plugin plugin;
        synchronized (ac.class) {
            if (!bom) {
                plugin = null;
            } else {
                if (str != null) {
                    if (str.equalsIgnoreCase("application/x-shockwave-flash")) {
                        if (bol == null) {
                            bol = new ac();
                        }
                        plugin = bol.b(str, vector, vector2, context);
                    }
                }
                plugin = null;
            }
        }
        return plugin;
    }

    private synchronized Plugin b(String str, Vector vector, Vector vector2, Context context) {
        Plugin plugin;
        w(context);
        if (this.bok == null) {
            this.bok = new p(this, str);
        }
        plugin = new Plugin(str, vector, vector2, bka, mHandler, context);
        this.bok.a(plugin);
        return plugin;
    }

    public static p eE(String str) {
        if (bol == null) {
            return null;
        }
        return bol.bok;
    }

    public static void setActivity(Activity activity) {
        bka = activity;
    }

    public static void w(Context context) {
        bon = context;
    }

    public void EW() {
        bom = true;
    }

    public void EX() {
        bom = false;
    }
}
