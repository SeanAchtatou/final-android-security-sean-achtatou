package com.uc.plugin;

import android.widget.LinearLayout;
import java.util.Vector;

class d implements Runnable {
    final /* synthetic */ Plugin jh;
    String lm;
    Object ln;
    Object lo;

    d(Plugin plugin) {
        this.jh = plugin;
    }

    public Runnable a(String str, Object obj, Object obj2) {
        this.lm = str;
        this.ln = obj;
        this.lo = obj2;
        return this;
    }

    public void run() {
        if (this.lm.equals("PRESS")) {
            switch (this.jh.ceA) {
                case 3:
                    break;
                default:
                    LinearLayout linearLayout = (LinearLayout) this.jh.cey.getChildAt(0);
                    for (int i = 0; i < linearLayout.getChildCount(); i++) {
                        linearLayout.getChildAt(i).performClick();
                    }
                    break;
            }
        }
        if (this.jh.ceH != null) {
            this.jh.ceH.b(this.lm, this.ln, this.lo);
            return;
        }
        if (this.jh.ceD == null) {
            this.jh.ceD = new Vector();
            this.jh.ceE = new Vector();
            this.jh.ceF = new Vector();
        }
        this.jh.ceD.add(this.lm);
        this.jh.ceE.add(this.ln);
        this.jh.ceF.add(this.lo);
    }
}
