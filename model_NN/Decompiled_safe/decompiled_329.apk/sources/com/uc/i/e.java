package com.uc.i;

import android.os.Handler;
import android.os.Message;

class e implements Runnable {
    final /* synthetic */ f agG;
    final /* synthetic */ String apS;
    final /* synthetic */ int apT;
    final /* synthetic */ Handler apU;

    e(f fVar, String str, int i, Handler handler) {
        this.agG = fVar;
        this.apS = str;
        this.apT = i;
        this.apU = handler;
    }

    public void run() {
        try {
            synchronized (this.agG.bjX) {
                if (this.agG.bjY == null) {
                    this.agG.bjX.wait();
                }
            }
            this.agG.bjY.registerCallback(this.agG.bkc);
            String Pay = this.agG.bjY.Pay(this.apS);
            this.agG.bjZ = false;
            this.agG.bjY.unregisterCallback(this.agG.bkc);
            this.agG.bka.unbindService(this.agG.bkb);
            Message message = new Message();
            message.what = this.apT;
            message.obj = Pay;
            this.apU.sendMessage(message);
        } catch (Exception e) {
        }
    }
}
