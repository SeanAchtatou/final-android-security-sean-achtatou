package com.uc.i;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.alipay.android.app.IAlixPay;

class c implements ServiceConnection {
    final /* synthetic */ f agG;

    c(f fVar) {
        this.agG = fVar;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        synchronized (this.agG.bjX) {
            this.agG.bjY = IAlixPay.Stub.asInterface(iBinder);
            this.agG.bjX.notify();
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        this.agG.bjY = null;
    }
}
