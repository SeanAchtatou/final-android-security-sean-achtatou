package com.uc.i;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import com.alipay.android.app.IAlixPay;
import com.alipay.android.app.IRemoteServiceCallback;

public class f {
    Integer bjX = 0;
    IAlixPay bjY = null;
    boolean bjZ = false;
    Activity bka = null;
    /* access modifiers changed from: private */
    public ServiceConnection bkb = new c(this);
    /* access modifiers changed from: private */
    public IRemoteServiceCallback bkc = new d(this);

    public boolean a(String str, Handler handler, int i, Context context) {
        if (this.bjZ) {
            return false;
        }
        this.bjZ = true;
        this.bka = (Activity) context;
        if (this.bjY == null) {
            this.bka.bindService(new Intent(IAlixPay.class.getName()), this.bkb, 1);
        }
        new Thread(new e(this, str, i, handler)).start();
        return true;
    }
}
