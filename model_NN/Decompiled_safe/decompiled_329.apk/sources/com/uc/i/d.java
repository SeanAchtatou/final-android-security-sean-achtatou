package com.uc.i;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.alipay.android.app.IRemoteServiceCallback;

class d extends IRemoteServiceCallback.Stub {
    final /* synthetic */ f agG;

    d(f fVar) {
        this.agG = fVar;
    }

    public void startActivity(String str, String str2, int i, Bundle bundle) {
        Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
        Bundle bundle2 = bundle == null ? new Bundle() : bundle;
        try {
            bundle2.putInt("CallingPid", i);
            intent.putExtras(bundle2);
            intent.setClassName(str, str2);
            this.agG.bka.startActivity(intent);
        } catch (Exception e) {
        }
    }
}
