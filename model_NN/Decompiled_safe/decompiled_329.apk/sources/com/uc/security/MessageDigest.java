package com.uc.security;

import com.uc.c.r;
import java.util.Random;

public class MessageDigest {
    public static final byte ENCRYPT_M9_WITHOUT_DATA = 10;
    public static final byte ENCRYPT_M9_WITH_DATA = 11;
    private static final int[] M9_ANDROID_KEY = {101, 49, 57, 50, 51, 55, 97, 51, 97, 57, 51, 51, 102, 55, 101, 98};
    public static final byte M9_DECODE_DEST_OFFSET = 8;
    public static final byte M9_DECODE_DEST_OFFSET_LENGTH = 10;
    public static final int M9_DECODE_SRC_CHECK_ERROR = -3;
    public static final int M9_DECODE_SRC_NOT_M9 = -2;
    public static final int M9_DECODE_SRC_NULL = -1;
    public static final int M9_DECODE_SRC_OK = 0;
    public static final int M9_ENCODE_SRC_NULL = -1;
    public static final int M9_PLATFORM_ANDROID = 4;
    public static final int M9_PLATFORM_SERVER = 0;
    private static final int[] M9_SERVER_KEY = {97, 97, 49, 55, 49, 48, 50, 49, 102, 57, 52, 51, 56, 99, 98, 50};

    public static final byte[] m9Encode(byte[] bArr) {
        return m9Encode(4, bArr);
    }

    public static final byte[] m9Encode(int i, byte[] bArr) {
        int[] iArr = M9_ANDROID_KEY;
        int[] iArr2 = new int[8];
        System.arraycopy(iArr, 0, iArr2, 0, 8);
        int[] iArr3 = new int[8];
        System.arraycopy(iArr, 8, iArr3, 0, 8);
        byte[] bArr2 = new byte[8];
        int nextInt = new Random(System.currentTimeMillis()).nextInt();
        bArr2[0] = (byte) ((nextInt >> 24) & 255);
        bArr2[1] = (byte) ((nextInt >> 16) & 255);
        bArr2[2] = (byte) ((nextInt >> 8) & 255);
        bArr2[3] = (byte) (nextInt & 255);
        bArr2[4] = (byte) ((bArr2[0] + 87) % 256);
        bArr2[5] = (byte) ((bArr2[1] + 29) % 256);
        bArr2[6] = (byte) ((bArr2[2] + 171) % 256);
        bArr2[7] = (byte) ((bArr2[3] + 148) % 256);
        int length = bArr.length;
        byte[] bArr3 = new byte[(length + 10)];
        bArr3[0] = 109;
        bArr3[1] = r.DY;
        bArr3[2] = r.Eh;
        bArr3[3] = (byte) i;
        bArr3[4] = bArr2[0];
        bArr3[5] = bArr2[1];
        bArr3[6] = bArr2[2];
        bArr3[7] = bArr2[3];
        byte b2 = 0;
        for (int i2 = 0; i2 < length; i2++) {
            if (i2 % 8 == 0) {
                iArr2[0] = ((iArr2[0] + iArr3[0]) + bArr2[0]) % 256;
                iArr2[1] = ((iArr2[1] + iArr3[1]) + bArr2[1]) % 256;
                iArr2[2] = ((iArr2[2] + iArr3[2]) + bArr2[2]) % 256;
                iArr2[3] = ((iArr2[3] + iArr3[3]) + bArr2[3]) % 256;
                iArr2[4] = ((iArr2[4] + iArr3[4]) + bArr2[4]) % 256;
                iArr2[5] = ((iArr2[5] + iArr3[5]) + bArr2[5]) % 256;
                iArr2[6] = ((iArr2[6] + iArr3[6]) + bArr2[6]) % 256;
                iArr2[7] = ((iArr2[7] + iArr3[7]) + bArr2[7]) % 256;
            }
            byte b3 = bArr[i2] & 255;
            bArr3[i2 + 8] = (byte) ((iArr2[i2 % 8] ^ b3) & 255);
            b2 ^= b3;
        }
        bArr3[length + 8] = (byte) ((iArr2[0] ^ b2) & 255);
        bArr3[length + 8 + 1] = (byte) ((iArr2[1] ^ b2) & 255);
        return bArr3;
    }

    public static final int m9Decode(byte[] bArr) {
        if (bArr == null) {
            return -1;
        }
        int length = bArr.length;
        if (length < 10 || bArr[0] != 109 || bArr[1] != 57 || bArr[2] != 48) {
            return -2;
        }
        int[] iArr = M9_SERVER_KEY;
        int[] iArr2 = new int[8];
        System.arraycopy(iArr, 0, iArr2, 0, 8);
        int[] iArr3 = new int[8];
        System.arraycopy(iArr, 8, iArr3, 0, 8);
        byte[] bArr2 = new byte[8];
        bArr2[0] = bArr[4];
        bArr2[1] = bArr[5];
        bArr2[2] = bArr[6];
        bArr2[3] = bArr[7];
        bArr2[4] = (byte) ((bArr2[0] + 87) % 256);
        bArr2[5] = (byte) ((bArr2[1] + 29) % 256);
        bArr2[6] = (byte) ((bArr2[2] + 171) % 256);
        bArr2[7] = (byte) ((bArr2[3] + 148) % 256);
        byte b2 = 0;
        for (int i = 8; i < length - 2; i++) {
            if (i % 8 == 0) {
                iArr2[0] = ((iArr2[0] + iArr3[0]) + bArr2[0]) % 256;
                iArr2[1] = ((iArr2[1] + iArr3[1]) + bArr2[1]) % 256;
                iArr2[2] = ((iArr2[2] + iArr3[2]) + bArr2[2]) % 256;
                iArr2[3] = ((iArr2[3] + iArr3[3]) + bArr2[3]) % 256;
                iArr2[4] = ((iArr2[4] + iArr3[4]) + bArr2[4]) % 256;
                iArr2[5] = ((iArr2[5] + iArr3[5]) + bArr2[5]) % 256;
                iArr2[6] = ((iArr2[6] + iArr3[6]) + bArr2[6]) % 256;
                iArr2[7] = ((iArr2[7] + iArr3[7]) + bArr2[7]) % 256;
            }
            byte b3 = bArr[i] ^ iArr2[i % 8];
            bArr[i] = (byte) (b3 & 255);
            b2 ^= b3;
        }
        return (bArr[length - 2] == ((byte) ((iArr2[0] ^ b2) & 255)) && bArr[length - 1] == ((byte) ((iArr2[1] ^ b2) & 255))) ? 0 : -3;
    }
}
