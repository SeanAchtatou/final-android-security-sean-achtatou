package com.uc.e;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import com.uc.e.b.f;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

public class s extends u {
    private static final int aDU = 46;
    private static final float aDV = 2.4f;
    private static final int aEa = 1200;
    public static final int aEj = 0;
    public static final int aEk = 1;
    private static final int aEp = 300;
    protected int aBk = 0;
    private long aBn = -1;
    private Animation aBq = null;
    protected Drawable[] aDW = null;
    private int aDX = -1;
    private int aDY = -1;
    private float aDZ = 0.0f;
    protected Drawable aEb;
    protected int[] aEc;
    private Drawable aEd;
    private boolean aEe = true;
    private int aEf = 5;
    private Drawable aEg;
    private Drawable aEh;
    private int aEi = 0;
    private int aEl;
    private boolean aEm = true;
    protected z aEn;
    private AnimationSet aEo = null;
    private int aEq = 255;
    protected int aeK = -1;
    protected int dividerHeight;
    protected int dz = 46;

    public s() {
        this.bBg = true;
    }

    private void be(boolean z) {
        if (this.aEe || this.aEi == 1 || this.aEo != null) {
            return;
        }
        if (z) {
            this.aEo = new AnimationSet(true);
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(300);
            this.aEo.addAnimation(alphaAnimation);
            this.aEo.start();
            return;
        }
        this.aEo = null;
        this.aEe = true;
        Ix();
    }

    private void za() {
        boolean z = this.aEe;
        this.aEe = false;
        this.aEq = 255;
        this.aEo = null;
        if (z) {
            Ix();
        }
    }

    private boolean zb() {
        if (this.aEo == null) {
            return false;
        }
        Transformation transformation = new Transformation();
        boolean transformation2 = this.aEo.getTransformation(System.currentTimeMillis(), transformation);
        this.aEq = (int) (transformation.getAlpha() * 255.0f);
        if (!transformation2) {
            this.aEo = null;
            this.aEe = true;
        }
        return transformation2;
    }

    private boolean zc() {
        boolean z;
        boolean z2;
        int bd = bd(true);
        if (this.aBq != null) {
            Transformation transformation = new Transformation();
            float[] fArr = new float[9];
            boolean transformation2 = this.aBq.getTransformation(System.currentTimeMillis(), transformation);
            transformation.getMatrix().getValues(fArr);
            int i = ((int) fArr[5]) - this.aBk;
            this.aBk = (int) fArr[5];
            if (this.aBk > 0) {
                this.aBk = 0;
                z2 = false;
            } else {
                z2 = transformation2;
            }
            if (this.aSQ >= bd || this.aBk >= (this.aSQ - bd) - this.paddingBottom || i >= 0) {
                z = z2;
            } else {
                this.aBk = ((this.aSQ - bd) - this.paddingTop) - this.paddingBottom;
                z = false;
            }
            if (!z) {
                be(true);
            }
        } else {
            z = false;
        }
        if (!z) {
            this.aBq = null;
        }
        return z;
    }

    public void B(Drawable drawable) {
        this.aEd = drawable;
    }

    public void C(Drawable drawable) {
        this.aEg = drawable;
        if (drawable != null && this.aSQ > 0 && this.aSP > 0) {
            int intrinsicHeight = drawable.getIntrinsicHeight();
            if (intrinsicHeight <= 0) {
                intrinsicHeight = 5;
            }
            drawable.setBounds(0, this.aSQ - intrinsicHeight, this.aSP, this.aSQ);
        }
    }

    public void D(Drawable drawable) {
        this.aEh = drawable;
        if (drawable != null && this.aSQ > 0 && this.aSP > 0) {
            int intrinsicHeight = drawable.getIntrinsicHeight();
            if (intrinsicHeight <= 0) {
                intrinsicHeight = 5;
            }
            drawable.setBounds(0, 0, this.aSP, intrinsicHeight);
        }
    }

    /* access modifiers changed from: protected */
    public void a(float f, int i, boolean z) {
        float f2;
        long sqrt;
        float f3 = (float) this.aBk;
        float abs = Math.abs(f);
        float f4 = f * f;
        if (z) {
            f2 = (f4 * ((float) ((f > 0.0f ? 1 : -1) * 600))) + ((float) this.aBk);
            sqrt = (long) (1200.0f * abs);
        } else {
            f2 = (float) (this.aBk + i);
            sqrt = (long) Math.sqrt((double) Math.abs(i * 2 * aEa));
        }
        this.aBq = new TranslateAnimation(0.0f, 0.0f, f3, f2);
        this.aBq.setInterpolator(new a(this, aDV / abs));
        this.aBq.initialize(0, 0, 0, 0);
        this.aBq.setDuration(sqrt);
        this.aBq.start();
        Ix();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.e.s.a(float, int, boolean):void
     arg types: [int, int, int]
     candidates:
      com.uc.e.s.a(int, boolean, boolean):void
      com.uc.e.s.a(android.graphics.Canvas, com.uc.e.z, int):void
      com.uc.e.s.a(byte, int, int):boolean
      com.uc.e.z.a(byte, int, int):boolean
      com.uc.e.s.a(float, int, boolean):void */
    /* access modifiers changed from: protected */
    public void a(int i, boolean z, boolean z2) {
        int i2;
        if (this.vj != null && i >= 0 && i < this.vj.size()) {
            int i3 = 0;
            int i4 = this.aBk + this.paddingTop;
            while (i3 < i) {
                i3++;
                i4 = ((z) this.vj.get(i3)).getHeight() + this.dividerHeight + i4;
            }
            int height = ((z) this.vj.get(i)).getHeight() + this.dividerHeight;
            if (this.aSQ <= 0) {
                i2 = 0;
            } else if (z2) {
                int yZ = yZ();
                int i5 = ((this.aSQ - height) / 2) - (i4 - this.aBk);
                if (i5 + yZ < this.aSQ) {
                    i5 = this.aSQ - yZ;
                }
                if (i5 > 0) {
                    i5 = 0;
                }
                i2 = i5 - this.aBk;
            } else {
                i2 = i4 < 0 ? -i4 : i4 + height > this.aSQ ? (this.aSQ - i4) - height : 0;
            }
            if (i2 != 0) {
                if (z) {
                    a(0.0f, i2, false);
                } else {
                    this.aBk = i2 + this.aBk;
                }
                Ix();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, int i) {
        char c;
        if (i == this.aeK) {
            c = 2;
        } else if (i == this.bdt - 1) {
            if (this.aeK != -1) {
                gd(this.aeK).z((byte) 0);
                this.aeK = -1;
            }
            c = 1;
        } else {
            c = 0;
        }
        if (this.aDW != null && this.aDW[c] != null) {
            this.aDW[c].setBounds(0, 0, (getWidth() - this.paddingLeft) - this.paddingRight, ((z) this.vj.get(i)).getHeight());
            this.aDW[c].draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, z zVar, int i) {
        canvas.save();
        canvas.clipRect(0, 0, getWidth(), zVar.getHeight());
        ((z) this.vj.get(i)).onDraw(canvas);
        canvas.restore();
    }

    public void a(Drawable drawable, int i) {
        this.aEb = drawable;
        this.dividerHeight = i;
    }

    public void a(z zVar) {
        super.a(zVar);
        zVar.setSize((getWidth() - this.paddingLeft) - this.paddingRight, this.dz);
    }

    public void a(Collection collection, int i) {
        super.a(collection, i);
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            ((z) it.next()).setSize((getWidth() - this.paddingLeft) - this.paddingRight, this.dz);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.e.s.a(float, int, boolean):void
     arg types: [float, int, int]
     candidates:
      com.uc.e.s.a(int, boolean, boolean):void
      com.uc.e.s.a(android.graphics.Canvas, com.uc.e.z, int):void
      com.uc.e.s.a(byte, int, int):boolean
      com.uc.e.z.a(byte, int, int):boolean
      com.uc.e.s.a(float, int, boolean):void */
    public boolean a(byte b2, int i, int i2) {
        if (this.bBc) {
            return true;
        }
        switch (b2) {
            case 0:
                this.bBa = i2;
                this.aDX = i2;
                this.bAZ = i;
                this.aDY = i;
                this.aBn = System.currentTimeMillis();
                fo(e(i, i2, null));
                break;
            case 1:
                if (yZ() > this.aSQ) {
                    if (((double) Math.abs(this.aDZ)) <= 0.1d) {
                        be(true);
                        break;
                    } else {
                        a(this.aDZ, 0, true);
                        this.aDZ = 0.0f;
                        break;
                    }
                }
                break;
            case 2:
                za();
                if (this.bdv) {
                    int i3 = i2 - this.aDX;
                    fn(i3);
                    this.aDZ = ((float) i3) / ((float) (System.currentTimeMillis() - this.aBn));
                    this.aDX = i2;
                    this.aBn = System.currentTimeMillis();
                    return true;
                } else if (this.aDX < 0) {
                    this.bBa = i2;
                    this.aDX = i2;
                    this.bAZ = i;
                    this.aDY = i;
                    this.aBn = System.currentTimeMillis();
                    return true;
                } else {
                    int i4 = i2 - this.aDX;
                    float abs = (float) Math.abs(i - this.bAZ);
                    float abs2 = (float) Math.abs(i2 - this.bBa);
                    boolean z = abs > ((float) bBe);
                    if (abs == 0.0f) {
                        abs = 1.0E-6f;
                    }
                    if ((abs2 > ((float) bBe)) && ((double) (abs2 / abs)) > 0.7d) {
                        if (fn(i4)) {
                            this.aDZ = ((float) i4) / ((float) (System.currentTimeMillis() - this.aBn));
                            this.bdv = true;
                        }
                        this.aDX = i2;
                        this.aDY = i;
                        this.aBn = System.currentTimeMillis();
                        return true;
                    } else if (z) {
                        be(false);
                        fo(-1);
                        return false;
                    } else {
                        be(false);
                        return false;
                    }
                }
        }
        return true;
    }

    public boolean a(KeyEvent keyEvent) {
        boolean a2 = super.a(keyEvent);
        if (this.aeK >= 0) {
            this.bdt = this.aeK + 1;
            fo(-1);
        }
        if (this.bdt > 0) {
            fp(this.bdt - 1);
            if (keyEvent.getAction() == 0 && keyEvent.getKeyCode() == 23) {
                fo(this.bdt - 1);
            } else if (keyEvent.getAction() == 1 && keyEvent.getKeyCode() == 23 && !a2) {
                aq(this.bdt - 1);
                if (this.aEm) {
                    this.bdt = 0;
                }
            }
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public void aP(boolean z) {
        int yZ = yZ();
        int height = getHeight();
        if (yZ > height && this.aBk + yZ < height) {
            int i = (height - yZ) - this.aBk;
            if (z) {
                fq(i);
                return;
            }
            this.aBk = i + this.aBk;
            Ix();
        } else if (yZ > height || this.aBk >= 0) {
            zd();
        } else {
            int i2 = -this.aBk;
            if (z) {
                fq(i2);
                return;
            }
            this.aBk = i2 + this.aBk;
            Ix();
        }
    }

    public boolean aq(int i) {
        boolean aq = super.aq(i);
        if (this.aEm) {
            fo(-1);
        }
        return aq;
    }

    /* access modifiers changed from: protected */
    public void b(Canvas canvas, int i) {
        z zVar = (z) this.vj.get(i);
        if (this.aEb != null) {
            canvas.save();
            canvas.clipRect(new Rect(-this.paddingLeft, zVar.getHeight(), getWidth(), zVar.getHeight() + this.dividerHeight));
            this.aEb.setBounds(-this.paddingLeft, zVar.getHeight(), getWidth(), zVar.getHeight() + this.dividerHeight);
            this.aEb.draw(canvas);
            canvas.restore();
        }
    }

    public boolean b(byte b2, int i, int i2) {
        boolean b3 = super.b(b2, i, i2);
        switch (b2) {
            case 0:
                this.bBa = i2;
                this.aDX = i2;
                this.bAZ = i;
                this.aDY = i;
                this.aBn = System.currentTimeMillis();
                fo(e(i, i2, null));
                zd();
                break;
            case 1:
                this.bBa = -1;
                this.aDX = -1;
                this.bAZ = -1;
                this.aDY = -1;
                this.aBn = -1;
                this.bdv = false;
                if (!this.bBc) {
                    fo(-1);
                    break;
                }
                break;
        }
        return b3;
    }

    public void bc(boolean z) {
        this.aEm = z;
    }

    public int bd(boolean z) {
        if (z) {
            return this.aEl;
        }
        Iterator it = this.vj.iterator();
        int i = 0;
        while (it.hasNext()) {
            i = ((z) it.next()).getHeight() + i;
        }
        int size = (this.dividerHeight * (this.vj.size() > 1 ? this.vj.size() : 0)) + i;
        this.aEl = size;
        return size;
    }

    public void c(z zVar, int i) {
        super.c(zVar, i);
        zVar.setSize((getWidth() - this.paddingLeft) - this.paddingRight, this.dz);
    }

    public void c(Vector vector) {
        super.c(vector);
        if (vector != null) {
            Iterator it = vector.iterator();
            while (it.hasNext()) {
                ((z) it.next()).setSize((getWidth() - this.paddingLeft) - this.paddingRight, this.dz);
            }
            if (this.bdt >= vector.size()) {
                this.bdt = 0;
            }
            if (this.aeK >= vector.size() - 1) {
                this.aeK = -1;
            }
        }
        zf();
    }

    public void clear() {
        super.clear();
        this.aBk = 0;
        Ix();
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        int height;
        int i;
        canvas.translate((float) this.paddingLeft, (float) this.paddingTop);
        if (this.vj.size() > 0) {
            int size = this.vj.size();
            int i2 = 0;
            int i3 = this.aBk;
            while (i2 < size) {
                z zVar = (z) this.vj.get(i2);
                if (i3 > this.aSQ) {
                    break;
                }
                if (zVar.getHeight() + i3 + this.dividerHeight <= 0) {
                    height = zVar.getHeight();
                    i = this.dividerHeight;
                } else {
                    canvas.save();
                    canvas.translate(0.0f, (float) i3);
                    a(canvas, i2);
                    a(canvas, zVar, i2);
                    b(canvas, i2);
                    canvas.restore();
                    height = zVar.getHeight();
                    i = this.dividerHeight;
                }
                i2++;
                i3 = height + i + i3;
            }
            i(canvas);
        } else if (this.aEn != null) {
            canvas.save();
            this.aEn.onDraw(canvas);
            canvas.restore();
        }
        if (this.aEg != null) {
            int intrinsicHeight = this.aEg.getIntrinsicHeight();
            if (intrinsicHeight <= 0) {
                intrinsicHeight = 5;
            }
            this.aEg.setBounds(0 - this.paddingLeft, (this.aSQ - intrinsicHeight) - this.paddingTop, this.aSP, this.aSQ - this.paddingTop);
            this.aEg.draw(canvas);
        }
        if (this.aEh != null) {
            int intrinsicHeight2 = this.aEh.getIntrinsicHeight();
            if (intrinsicHeight2 <= 0) {
                intrinsicHeight2 = 5;
            }
            this.aEh.setBounds(0 - this.paddingLeft, 0, this.aSP, intrinsicHeight2);
            this.aEh.draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public int e(int i, int i2, int[] iArr) {
        if (i > this.aSP - this.paddingRight || i < this.paddingLeft || this.vj == null || this.vj.size() == 0) {
            return -1;
        }
        int i3 = this.aBk + this.paddingTop;
        int size = this.vj.size();
        int i4 = 0;
        int i5 = i3;
        while (i4 < size) {
            z zVar = (z) this.vj.get(i4);
            int i6 = i2 - i5;
            if (i6 <= this.dividerHeight || i6 >= zVar.getHeight() + this.dividerHeight) {
                i4++;
                i5 = zVar.getHeight() + this.dividerHeight + i5;
            } else {
                if (iArr != null) {
                    iArr[0] = this.paddingLeft;
                    iArr[1] = i5;
                }
                return i4;
            }
        }
        return -1;
    }

    public void e(Canvas canvas) {
        if (this.cx instanceof f) {
            f fVar = (f) this.cx;
            int i = this.dz + this.dividerHeight;
            int i2 = (this.aBk + this.paddingTop) % (i * 2);
            if (i2 > 0) {
                i2 -= i * 2;
            }
            fVar.setHeight(i);
            canvas.save();
            canvas.translate(0.0f, (float) i2);
            fVar.setBounds(0, i2, getWidth(), getHeight());
            fVar.draw(canvas);
            canvas.restore();
            return;
        }
        super.e(canvas);
    }

    /* access modifiers changed from: protected */
    public boolean es() {
        return zb() || zc();
    }

    public void f(Drawable[] drawableArr) {
        this.aDW = drawableArr;
    }

    public void fm(int i) {
        this.dz = i;
        if (this.vj != null) {
            Iterator it = this.vj.iterator();
            while (it.hasNext()) {
                z zVar = (z) it.next();
                zVar.setSize(zVar.getWidth(), this.dz);
            }
        }
    }

    public boolean fn(int i) {
        boolean z;
        int yZ = ((this.aSQ - yZ()) - this.paddingTop) - this.paddingBottom;
        if (yZ < 0) {
            int i2 = this.aBk;
            this.aBk += i;
            if (this.aBk > 0) {
                this.aBk = 0;
            } else if (this.aBk < yZ) {
                this.aBk = yZ;
            }
            z = i2 != this.aBk;
        } else {
            z = false;
        }
        if (z) {
            za();
            Ix();
        }
        return z;
    }

    public void fo(int i) {
        if (!(this.aeK == -1 || gd(this.aeK) == null)) {
            gd(this.aeK).z((byte) 0);
        }
        if (!(i == -1 || gd(i) == null)) {
            gd(i).z((byte) 2);
        }
        this.aeK = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.e.s.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      com.uc.e.s.a(float, int, boolean):void
      com.uc.e.s.a(android.graphics.Canvas, com.uc.e.z, int):void
      com.uc.e.s.a(byte, int, int):boolean
      com.uc.e.z.a(byte, int, int):boolean
      com.uc.e.s.a(int, boolean, boolean):void */
    /* access modifiers changed from: protected */
    public void fp(int i) {
        a(i, true, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.e.s.a(float, int, boolean):void
     arg types: [int, int, int]
     candidates:
      com.uc.e.s.a(int, boolean, boolean):void
      com.uc.e.s.a(android.graphics.Canvas, com.uc.e.z, int):void
      com.uc.e.s.a(byte, int, int):boolean
      com.uc.e.z.a(byte, int, int):boolean
      com.uc.e.s.a(float, int, boolean):void */
    /* access modifiers changed from: protected */
    public void fq(int i) {
        a(0.0f, i, false);
    }

    /* access modifiers changed from: protected */
    public int fr(int i) {
        if (i == this.aeK) {
            return 2;
        }
        return i == this.bdt - 1 ? 1 : 0;
    }

    public void fs(int i) {
        this.dz = i;
    }

    public void ft(int i) {
        this.aEf = i;
    }

    public void fu(int i) {
        this.aEi = i;
        za();
    }

    public void g(z zVar) {
        this.aEn = zVar;
        this.aEn.setSize(this.aSP, this.aSQ);
    }

    public int getDividerHeight() {
        return this.dividerHeight;
    }

    public int getScrollBarSize() {
        return this.aEf;
    }

    /* access modifiers changed from: protected */
    public void i(Canvas canvas) {
        int bd = bd(true);
        if (this.aEd != null && !this.aEe && this.aSQ < bd) {
            int i = ((-this.aBk) * this.aSQ) / ((this.paddingTop + bd) + this.paddingBottom);
            this.aEd.setBounds((this.aSP - this.aEf) - this.paddingLeft, i - this.paddingTop, this.aSP - this.paddingLeft, ((((this.aSQ * this.aSQ) / bd) + i) - this.paddingTop) - this.paddingBottom);
            this.aEd.setAlpha(this.aEq);
            this.aEd.draw(canvas);
        }
    }

    public void setDivider(Drawable drawable) {
        this.aEb = drawable;
    }

    public void setDividerHeight(int i) {
        this.dividerHeight = i;
    }

    public void setPadding(int i, int i2, int i3, int i4) {
        int width;
        super.setPadding(i, i2, i3, i4);
        if (this.vj != null && (width = (getWidth() - i) - i3) > 0 && this.dz > 0) {
            Iterator it = this.vj.iterator();
            while (it.hasNext()) {
                ((z) it.next()).setSize(width, this.dz);
            }
        }
    }

    public void setSize(int i, int i2) {
        super.setSize(i, i2);
        int i3 = (i - this.paddingLeft) - this.paddingRight;
        if (this.vj != null) {
            Iterator it = this.vj.iterator();
            while (it.hasNext()) {
                ((z) it.next()).setSize(i3, this.dz);
            }
        }
        if (this.aEn != null) {
            this.aEn.setSize(this.aSP, this.aSQ);
        }
        if (this.aEg != null) {
            int intrinsicHeight = this.aEg.getIntrinsicHeight();
            if (intrinsicHeight <= 0) {
                intrinsicHeight = 5;
            }
            this.aEg.setBounds(0, i2 - intrinsicHeight, i, i2);
        }
        if (this.aEh != null) {
            int intrinsicHeight2 = this.aEh.getIntrinsicHeight();
            if (intrinsicHeight2 <= 0) {
                intrinsicHeight2 = 5;
            }
            this.aEh.setBounds(0, 0, i, intrinsicHeight2);
        }
        zf();
    }

    public int yZ() {
        return bd(false);
    }

    public void zd() {
        this.aBq = null;
    }

    public void ze() {
        this.aBk = 0;
        Ix();
    }

    /* access modifiers changed from: protected */
    public void zf() {
        aP(true);
    }

    public Drawable zg() {
        return this.aEg;
    }

    public Drawable zh() {
        return this.aEh;
    }
}
