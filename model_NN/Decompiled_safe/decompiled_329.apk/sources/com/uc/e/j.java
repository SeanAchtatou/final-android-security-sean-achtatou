package com.uc.e;

import android.graphics.drawable.Drawable;

public class j {
    public static final int INFO = 1;
    public static final int Jf = 2;
    public static final int Jg = 3;
    public static final int Jh = 4;
    public static final int Ji = 5;
    private String[] Jj;
    private Drawable Jk;
    private int aT;
    private String br;

    public void bE(int i) {
        this.aT = i;
    }

    public int getItemId() {
        return this.aT;
    }

    public String getUrl() {
        return this.br;
    }

    public void i(String[] strArr) {
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = strArr[i].trim();
            strArr[i] = strArr[i].replaceAll("\r\n", "\r");
        }
        this.Jj = strArr;
    }

    public void k(Drawable drawable) {
        this.Jk = drawable;
    }

    public Drawable la() {
        return this.Jk;
    }

    public String[] lb() {
        return this.Jj;
    }

    public void setUrl(String str) {
        this.br = str;
    }
}
