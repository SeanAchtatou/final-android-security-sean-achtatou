package com.uc.e;

import java.util.Iterator;
import java.util.Vector;

public class ab extends z {
    private Vector bRC;
    public boolean bRD;

    public ab() {
        this(new Vector());
    }

    public ab(Vector vector) {
        this.bRD = false;
        this.bRC = vector;
    }

    public Vector MX() {
        return this.bRC;
    }

    public void i(z zVar) {
        this.bRC.add(zVar);
    }

    public void j(z zVar) {
        this.bRC.remove(zVar);
    }

    public void setSize(int i, int i2) {
        super.setSize(i, i2);
        Iterator it = this.bRC.iterator();
        while (it.hasNext()) {
            ((z) it.next()).setSize(i, i2);
        }
    }

    public void w(Vector vector) {
        this.bRC = vector;
    }
}
