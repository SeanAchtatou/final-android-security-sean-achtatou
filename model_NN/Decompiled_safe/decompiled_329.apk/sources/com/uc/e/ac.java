package com.uc.e;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;

public class ac {
    protected static final int caL = -1;
    protected String Bo;
    protected Drawable aBW;
    protected boolean aP = true;
    protected int bFy = -6568735;
    protected Drawable caM;
    private int caN = 0;
    protected int caO = -1;
    protected int caP = -1;
    protected int caQ = 0;
    protected int caR = 0;
    protected int caS = 255;
    protected int caT = 90;
    protected int gravity = 17;
    protected int height = -1;
    protected int id;
    private Paint pL;
    protected int paddingBottom = 0;
    protected int paddingLeft = 0;
    protected int paddingRight = 0;
    protected int paddingTop = 0;
    protected int textColor = -1;
    protected int textSize = 12;
    protected int visibility = 0;
    protected int width = -1;

    public ac(int i, int i2, int i3) {
        this.id = i;
        this.height = i2;
        this.width = i3;
    }

    private void Pl() {
        if (this.pL == null) {
            this.pL = new Paint();
        }
        this.pL.setTextSize((float) this.textSize);
        this.pL.setColor(this.textColor);
        this.pL.setAntiAlias(true);
        this.pL.setTypeface(Typeface.DEFAULT);
    }

    public void H(boolean z) {
        this.aP = z;
    }

    public void O(Drawable drawable) {
        if (drawable != null) {
            if (this.caP == -1) {
                this.caP = drawable.getIntrinsicWidth();
            }
            if (this.caO == -1) {
                this.caO = drawable.getIntrinsicHeight();
            }
            this.aBW = drawable;
            this.aBW.setBounds(0, 0, this.caP, this.caO);
        }
    }

    public void P(Drawable drawable) {
        this.caM = drawable;
    }

    public int Pm() {
        return this.textColor;
    }

    public int Pn() {
        return this.bFy;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [int, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    public void a(Canvas canvas, int i, int i2) {
        int i3;
        int i4;
        if (this.visibility != 1) {
            if (this.pL == null) {
                Pl();
            }
            this.pL.setShadowLayer(3.0f, 0.0f, 0.0f, -872415232);
            Drawable drawable = this.aBW;
            if (this.aP) {
                this.pL.setAlpha(this.caS);
                if (this.caM != null) {
                    drawable = this.caM;
                } else if (this.aBW != null) {
                    this.aBW.setAlpha(this.caS);
                }
                this.pL.setColor(this.textColor);
            } else {
                this.pL.setAlpha(this.caT);
                if (this.caM != null) {
                    drawable = this.caM;
                } else if (this.aBW != null) {
                    this.aBW.setAlpha(this.caT);
                }
                this.pL.setColor(this.bFy);
            }
            canvas.save();
            float f = -this.pL.getFontMetrics().ascent;
            int i5 = this.gravity;
            if (drawable != null) {
                int i6 = ((((i - this.paddingLeft) - this.paddingRight) - this.caP) / 2) + this.paddingLeft;
                int i7 = this.paddingTop;
                if (this.Bo == null || this.Bo.length() == 0 || this.caQ == 2) {
                    i7 = (i2 - this.caO) / 2;
                }
                canvas.translate((float) i6, (float) i7);
                drawable.draw(canvas);
                int i8 = i7;
                i4 = i6;
                i3 = i8;
            } else {
                i3 = 0;
                i4 = 0;
            }
            if (this.Bo != null && this.Bo.length() > 0 && this.caQ == 0) {
                canvas.translate((float) ((((((i - this.paddingLeft) - this.paddingRight) - this.caN) / 2) + this.paddingLeft) - i4), (float) ((drawable == null ? (int) (((((float) i2) - this.pL.descent()) - this.pL.ascent()) / 2.0f) : i2 - this.paddingBottom) - i3));
                canvas.drawText(this.Bo, 0.0f, 0.0f, this.pL);
            }
            canvas.restore();
        }
    }

    public void aV(int i) {
        if (this.textSize != i) {
            this.textSize = i;
            if (this.pL == null) {
                Pl();
            } else {
                this.pL.setTextSize((float) i);
            }
            if (this.Bo != null) {
                this.caN = (int) this.pL.measureText(this.Bo);
            }
        }
    }

    public void bB(int i, int i2) {
        this.caP = i;
        this.caO = i2;
        if (this.aBW != null) {
            this.aBW.setBounds(0, 0, this.caP, this.caO);
        }
    }

    public void bC(int i, int i2) {
        this.caS = i;
        this.caT = i2;
    }

    public int getId() {
        return this.id;
    }

    public String getText() {
        return this.Bo;
    }

    public int getTextSize() {
        return this.textSize;
    }

    public int getVisibility() {
        return this.visibility;
    }

    public void ks(int i) {
        this.bFy = i;
    }

    public void kt(int i) {
        this.caQ = i;
    }

    public void ku(int i) {
        this.caR = i;
    }

    public void setId(int i) {
        this.id = i;
    }

    public void setPadding(int i, int i2, int i3, int i4) {
        this.paddingLeft = i;
        this.paddingRight = i3;
        this.paddingTop = i2;
        this.paddingBottom = i4;
    }

    public void setText(String str) {
        if (this.pL == null) {
            Pl();
        }
        this.caN = (int) this.pL.measureText(str);
        this.Bo = str;
    }

    public void setTextColor(int i) {
        this.textColor = i;
    }

    public void setVisibility(int i) {
        this.visibility = i;
    }

    public boolean zP() {
        return this.aP;
    }
}
