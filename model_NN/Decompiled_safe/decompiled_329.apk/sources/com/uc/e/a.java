package com.uc.e;

import android.view.animation.Interpolator;

public class a implements Interpolator {
    private float dq;
    final /* synthetic */ s dr;

    public a(s sVar, float f) {
        this.dr = sVar;
        this.dq = f < 0.0f ? 1.0f - f : 1.0f;
    }

    public float getInterpolation(float f) {
        if (this.dq == 1.0f) {
            return 1.0f - ((1.0f - f) * (1.0f - f));
        }
        if (f <= this.dq) {
            return f;
        }
        float f2 = 1.0f - this.dq;
        return (f2 * (1.0f - ((1.0f - ((f - this.dq) / f2)) * (1.0f - ((f - this.dq) / f2))))) + this.dq;
    }
}
