package com.uc.e;

import android.graphics.Canvas;
import android.graphics.Paint;

class y {
    public static String brA = "...";

    private y() {
    }

    public static void a(String[] strArr, int i, Paint paint) {
        int breakText;
        int measureText = (int) paint.measureText(brA);
        for (int i2 = 0; i2 < strArr.length; i2++) {
            String str = strArr[i2];
            if (str != null && !str.trim().equals("") && (breakText = paint.breakText(str, true, (float) i, null)) < str.length()) {
                int i3 = breakText - 0;
                int i4 = 0;
                int i5 = i3;
                String substring = str.substring(0, i3);
                int i6 = i5;
                while (i6 != paint.breakText(substring, true, (float) (i - measureText), null)) {
                    int i7 = i4 + 1;
                    i6 = breakText - i7;
                    i4 = i7;
                    substring = str.substring(0, i6);
                }
                strArr[i2] = substring + brA;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Canvas.drawText(java.lang.String, int, int, float, float, android.graphics.Paint):void}
     arg types: [java.lang.String, int, int, int, float, android.graphics.Paint]
     candidates:
      ClspMth{android.graphics.Canvas.drawText(java.lang.CharSequence, int, int, float, float, android.graphics.Paint):void}
      ClspMth{android.graphics.Canvas.drawText(char[], int, int, float, float, android.graphics.Paint):void}
      ClspMth{android.graphics.Canvas.drawText(java.lang.String, int, int, float, float, android.graphics.Paint):void} */
    public static void a(String[] strArr, Canvas canvas, Paint paint, int i, int i2, int i3) {
        int i4;
        canvas.save();
        int textSize = (int) (paint.getTextSize() + ((float) i3));
        int i5 = 0;
        int i6 = i2;
        while (i5 < strArr.length) {
            String str = strArr[i5];
            if (str == null) {
                i4 = i6;
            } else if (str.trim().equals("")) {
                i4 = i6;
            } else {
                canvas.save();
                canvas.translate((float) ((int) ((((float) i) - paint.measureText(str)) / 2.0f)), 0.0f);
                canvas.drawText(str, 0, str.length(), 0.0f, paint.getTextSize(), paint);
                canvas.restore();
                i4 = i6 - textSize;
                if (i4 < textSize) {
                    break;
                }
                canvas.translate(0.0f, paint.getTextSize() + ((float) i3));
            }
            i5++;
            i6 = i4;
        }
        canvas.restore();
    }
}
