package com.uc.e;

import android.graphics.Canvas;
import android.view.KeyEvent;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

public class u extends z {
    private static final String tag = "UI BASE VIEW GROUP";
    protected int bdt = 0;
    protected int[] bdu = new int[2];
    protected boolean bdv = false;
    protected int bdw = -1;
    private boolean bdx = false;
    protected Vector vj = new Vector();

    public Vector BM() {
        return this.vj;
    }

    public void BN() {
        if (this.bdt > 0) {
            ((z) this.vj.get(this.bdt - 1)).z((byte) 0);
            this.bdt = 0;
            Ix();
        }
    }

    public void a(z zVar) {
        zVar.h(this);
        this.vj.add(zVar);
    }

    public void a(Collection collection, int i) {
        this.vj.addAll(i, collection);
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            ((z) it.next()).h(this);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:? A[Catch:{ Throwable -> 0x0083 }, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0012 A[Catch:{ Throwable -> 0x0083 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.view.KeyEvent r5) {
        /*
            r4 = this;
            r3 = 0
            r2 = 1
            int r0 = r5.getAction()     // Catch:{ Throwable -> 0x0083 }
            if (r0 != 0) goto L_0x000f
            int r0 = r5.getKeyCode()     // Catch:{ Throwable -> 0x0083 }
            switch(r0) {
                case 19: goto L_0x0086;
                case 20: goto L_0x0014;
                default: goto L_0x000f;
            }     // Catch:{ Throwable -> 0x0083 }
        L_0x000f:
            r0 = r3
        L_0x0010:
            if (r0 == 0) goto L_0x0013
            r0 = r2
        L_0x0013:
            return r0
        L_0x0014:
            java.util.Vector r0 = r4.vj     // Catch:{ Throwable -> 0x0083 }
            int r0 = r0.size()     // Catch:{ Throwable -> 0x0083 }
            int r1 = r4.bdt     // Catch:{ Throwable -> 0x0083 }
            if (r1 >= 0) goto L_0x0045
            int r0 = r4.bdt     // Catch:{ Throwable -> 0x0083 }
            int r0 = -r0
            r4.bdt = r0     // Catch:{ Throwable -> 0x0083 }
            java.util.Vector r0 = r4.vj     // Catch:{ Throwable -> 0x0083 }
            int r1 = r4.bdt     // Catch:{ Throwable -> 0x0083 }
            int r1 = r1 - r2
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Throwable -> 0x0083 }
            com.uc.e.z r0 = (com.uc.e.z) r0     // Catch:{ Throwable -> 0x0083 }
            r1 = 1
            r0.z(r1)     // Catch:{ Throwable -> 0x0083 }
            java.util.Vector r0 = r4.vj     // Catch:{ Throwable -> 0x0083 }
            int r1 = r4.bdt     // Catch:{ Throwable -> 0x0083 }
            int r1 = r1 - r2
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Throwable -> 0x0083 }
            com.uc.e.z r0 = (com.uc.e.z) r0     // Catch:{ Throwable -> 0x0083 }
            r0.c(r5)     // Catch:{ Throwable -> 0x0083 }
            r4.Ix()     // Catch:{ Throwable -> 0x0083 }
        L_0x0043:
            r0 = r2
            goto L_0x0010
        L_0x0045:
            int r1 = r4.bdt     // Catch:{ Throwable -> 0x0083 }
            if (r1 >= r0) goto L_0x0043
            int r0 = r4.bdt     // Catch:{ Throwable -> 0x0083 }
            if (r0 <= 0) goto L_0x005c
            java.util.Vector r0 = r4.vj     // Catch:{ Throwable -> 0x0083 }
            int r1 = r4.bdt     // Catch:{ Throwable -> 0x0083 }
            int r1 = r1 - r2
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Throwable -> 0x0083 }
            com.uc.e.z r0 = (com.uc.e.z) r0     // Catch:{ Throwable -> 0x0083 }
            r1 = 0
            r0.z(r1)     // Catch:{ Throwable -> 0x0083 }
        L_0x005c:
            int r0 = r4.bdt     // Catch:{ Throwable -> 0x0083 }
            int r0 = r0 + 1
            r4.bdt = r0     // Catch:{ Throwable -> 0x0083 }
            java.util.Vector r0 = r4.vj     // Catch:{ Throwable -> 0x0083 }
            int r1 = r4.bdt     // Catch:{ Throwable -> 0x0083 }
            int r1 = r1 - r2
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Throwable -> 0x0083 }
            com.uc.e.z r0 = (com.uc.e.z) r0     // Catch:{ Throwable -> 0x0083 }
            r1 = 1
            r0.z(r1)     // Catch:{ Throwable -> 0x0083 }
            java.util.Vector r0 = r4.vj     // Catch:{ Throwable -> 0x0083 }
            int r1 = r4.bdt     // Catch:{ Throwable -> 0x0083 }
            int r1 = r1 - r2
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Throwable -> 0x0083 }
            com.uc.e.z r0 = (com.uc.e.z) r0     // Catch:{ Throwable -> 0x0083 }
            r0.c(r5)     // Catch:{ Throwable -> 0x0083 }
            r4.Ix()     // Catch:{ Throwable -> 0x0083 }
            goto L_0x0043
        L_0x0083:
            r0 = move-exception
            r0 = r3
            goto L_0x0010
        L_0x0086:
            int r0 = r4.bdt     // Catch:{ Throwable -> 0x0083 }
            if (r0 >= 0) goto L_0x00b2
            int r0 = r4.bdt     // Catch:{ Throwable -> 0x0083 }
            int r0 = -r0
            r4.bdt = r0     // Catch:{ Throwable -> 0x0083 }
            java.util.Vector r0 = r4.vj     // Catch:{ Throwable -> 0x0083 }
            int r1 = r4.bdt     // Catch:{ Throwable -> 0x0083 }
            int r1 = r1 - r2
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Throwable -> 0x0083 }
            com.uc.e.z r0 = (com.uc.e.z) r0     // Catch:{ Throwable -> 0x0083 }
            r1 = 1
            r0.z(r1)     // Catch:{ Throwable -> 0x0083 }
            java.util.Vector r0 = r4.vj     // Catch:{ Throwable -> 0x0083 }
            int r1 = r4.bdt     // Catch:{ Throwable -> 0x0083 }
            int r1 = r1 - r2
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Throwable -> 0x0083 }
            com.uc.e.z r0 = (com.uc.e.z) r0     // Catch:{ Throwable -> 0x0083 }
            r0.c(r5)     // Catch:{ Throwable -> 0x0083 }
            r4.Ix()     // Catch:{ Throwable -> 0x0083 }
        L_0x00af:
            r0 = r2
            goto L_0x0010
        L_0x00b2:
            int r0 = r4.bdt     // Catch:{ Throwable -> 0x0083 }
            if (r0 <= r2) goto L_0x00af
            java.util.Vector r0 = r4.vj     // Catch:{ Throwable -> 0x0083 }
            int r1 = r4.bdt     // Catch:{ Throwable -> 0x0083 }
            int r1 = r1 - r2
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Throwable -> 0x0083 }
            com.uc.e.z r0 = (com.uc.e.z) r0     // Catch:{ Throwable -> 0x0083 }
            r1 = 0
            r0.z(r1)     // Catch:{ Throwable -> 0x0083 }
            int r0 = r4.bdt     // Catch:{ Throwable -> 0x0083 }
            int r0 = r0 - r2
            r4.bdt = r0     // Catch:{ Throwable -> 0x0083 }
            java.util.Vector r0 = r4.vj     // Catch:{ Throwable -> 0x0083 }
            int r1 = r4.bdt     // Catch:{ Throwable -> 0x0083 }
            int r1 = r1 - r2
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Throwable -> 0x0083 }
            com.uc.e.z r0 = (com.uc.e.z) r0     // Catch:{ Throwable -> 0x0083 }
            r1 = 1
            r0.z(r1)     // Catch:{ Throwable -> 0x0083 }
            java.util.Vector r0 = r4.vj     // Catch:{ Throwable -> 0x0083 }
            int r1 = r4.bdt     // Catch:{ Throwable -> 0x0083 }
            int r1 = r1 - r2
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Throwable -> 0x0083 }
            com.uc.e.z r0 = (com.uc.e.z) r0     // Catch:{ Throwable -> 0x0083 }
            r0.c(r5)     // Catch:{ Throwable -> 0x0083 }
            r4.Ix()     // Catch:{ Throwable -> 0x0083 }
            goto L_0x00af
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.e.u.a(android.view.KeyEvent):boolean");
    }

    /* access modifiers changed from: protected */
    public boolean aD(int i, int i2) {
        int e = e(i, i2, null);
        if (e == -1 || !ar(e)) {
            return performLongClick();
        }
        return true;
    }

    public boolean aq(int i) {
        return eZ(i);
    }

    public boolean ar(int i) {
        return hM(i);
    }

    public boolean b(byte b2, int i, int i2) {
        if (b2 == 0) {
            this.bBb = false;
            this.bBc = false;
        }
        if (!this.bBf) {
            return false;
        }
        return this.bdv ? d(b2, i, i2) : c(b2, i, i2);
    }

    public void bn(boolean z) {
        this.bdx = z;
    }

    public void c(z zVar, int i) {
        zVar.h(this);
        this.vj.add(i, zVar);
    }

    public void c(Vector vector) {
        this.vj = vector;
        if (vector != null) {
            Iterator it = this.vj.iterator();
            while (it.hasNext()) {
                ((z) it.next()).h(this);
            }
        }
    }

    public boolean c(byte b2, int i, int i2) {
        if (this.bdt > 0 && this.vj != null && this.bdt <= this.vj.size()) {
            ((z) this.vj.get(this.bdt - 1)).z((byte) 0);
            this.bdt = 0;
        }
        if (b2 == 0) {
            this.bdw = e(i, i2, this.bdu);
            if (this.bdw < 0 || this.vj == null || this.bdw > this.vj.size() - 1) {
                return d(b2, i, i2);
            }
            if (((z) this.vj.get(this.bdw)).b(b2, i - this.bdu[0], i2 - this.bdu[1])) {
                return true;
            }
            if (!this.bdx) {
                this.bdw = -1;
            }
            return d(b2, i, i2);
        } else if (this.bdw < 0 || this.vj == null || this.bdw > this.vj.size() - 1) {
            return d(b2, i, i2);
        } else {
            z zVar = (z) this.vj.get(this.bdw);
            if (zVar != null ? zVar.b(b2, i - this.bdu[0], i2 - this.bdu[1]) : false) {
                return true;
            }
            if (!this.bdx) {
                this.bdw = -1;
            }
            return d(b2, i, i2);
        }
    }

    public boolean c(KeyEvent keyEvent) {
        if (!this.bBf) {
            return true;
        }
        if (this.bdv) {
            return a(keyEvent);
        }
        if (this.bdt <= 0 || this.bdt > this.vj.size() || this.vj.get(this.bdt - 1) == null || !((z) this.vj.get(this.bdt - 1)).c(keyEvent)) {
            return a(keyEvent);
        }
        return true;
    }

    public void clear() {
        this.vj.clear();
    }

    public void clearFocus() {
        this.bdt = 0;
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        if (this.vj != null) {
            Iterator it = this.vj.iterator();
            while (it.hasNext()) {
                canvas.save();
                ((z) it.next()).onDraw(canvas);
                canvas.restore();
            }
        }
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        dispatchDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public int e(int i, int i2, int[] iArr) {
        return -1;
    }

    public void gc(int i) {
        this.vj.remove(i);
    }

    public z gd(int i) {
        if (this.vj == null || i < 0 || i >= this.vj.size()) {
            return null;
        }
        return (z) this.vj.get(i);
    }

    public int getChildCount() {
        if (this.vj == null) {
            return 0;
        }
        return this.vj.size();
    }

    /* access modifiers changed from: protected */
    public boolean k(int i, int i2) {
        int e = e(i, i2, null);
        if (e == -1) {
            return performClick();
        }
        aq(e);
        return true;
    }
}
