package com.uc.e;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import java.util.Iterator;
import java.util.Vector;

public class r extends z {
    public static int DEFAULT = 0;
    public static final int GONE = 2;
    public static final int INVISIBLE = 1;
    public static final int VISIBLE = 0;
    public static final byte aAD = 0;
    public static final byte aAE = 1;
    public static final int aAK = 0;
    public static final int aAL = 1;
    public static int aAN = 1;
    public static int aAO = 2;
    private Vector aAB;
    private byte aAC = 0;
    private h aAF;
    private int aAG = -1;
    private int aAH = -1;
    private int aAI = 0;
    private boolean aAJ = true;
    private int aAM = 1;
    private Drawable[] aAP = new Drawable[3];
    private int aAQ;
    private int aAR = 0;
    private int aAS = -1;
    private Bitmap aAT;
    private int bgColor = -1;
    private Drawable cx;
    private int height;
    private Vector lO;
    private int vk;
    private int vl;
    private int width;

    private void yj() {
        if (this.aAB == null) {
            this.aAR = 0;
            return;
        }
        int size = this.aAB.size();
        if (size > 1) {
            this.aAR = (this.width - (this.vk * size)) / (size - 1);
        } else if (size == 1) {
            this.aAR = this.width - this.vk;
        }
    }

    public void a(ad adVar) {
        this.bBl = adVar;
    }

    public int aC(int i, int i2) {
        int i3;
        if (this.aAB == null || this.aAB.size() == 0) {
            return -1;
        }
        int i4 = 0;
        if (this.vk + this.aAR > 0) {
            int i5 = i / (this.vk + this.aAR);
            i3 = i % (this.vk + this.aAR);
            i4 = i5;
        } else {
            i3 = i;
        }
        if (this.aAC == 0 && i3 > 0 && i3 < this.vk) {
            return i4;
        }
        if (this.aAC != 1 || i3 <= this.aAR || i3 >= this.aAR + this.vk) {
            return -1;
        }
        return (this.aAB.size() - i4) - 1;
    }

    public void aw(int i, int i2) {
        this.vk = i;
        int i3 = (i2 - this.paddingTop) - this.paddingBottom;
        if (i2 <= i3) {
            i3 = i2;
        }
        this.vl = i3;
        if (this.aAP[DEFAULT] != null) {
            this.aAP[DEFAULT].setBounds(0, 0, this.vk, this.vl);
        }
        if (this.aAP[aAN] != null) {
            this.aAP[aAN].setBounds(0, 0, this.vk, this.vl);
        }
        if (this.aAP[aAO] != null) {
            this.aAP[aAO].setBounds(0, 0, this.vk, this.vl);
        }
    }

    public boolean b(ac acVar) {
        if (this.lO == null) {
            this.lO = new Vector();
        }
        acVar.kt(this.aAI);
        return this.lO.add(acVar);
    }

    public void c(ac acVar) {
        if (this.lO != null) {
            this.lO.remove(acVar);
        }
    }

    public void c(h hVar) {
        this.aAF = hVar;
    }

    public void clear() {
        if (this.lO != null) {
            this.lO.clear();
        }
        if (this.aAB != null) {
            this.aAB.clear();
        }
    }

    public boolean d(MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        switch (motionEvent.getAction()) {
            case 0:
                this.aAG = aC(x, y);
                if (this.aAG < 0 || ((ac) this.aAB.get(this.aAG)).zP()) {
                    fa(this.aAG);
                } else {
                    this.aAG = -1;
                }
                return true;
            case 1:
                if (this.aAG != -1) {
                    fa(this.aAG);
                    eZ(this.aAG);
                }
                this.aAG = -1;
                return true;
            case 2:
                if (!(this.aAG == -1 || this.aAG == aC(x, y))) {
                    fa(this.aAG);
                    this.aAG = -1;
                }
                return true;
            case 3:
            default:
                return false;
            case 4:
                if (this.aAG != -1) {
                    fa(this.aAG);
                    this.aAG = -1;
                }
                return true;
        }
    }

    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        if (this.bgColor > 0) {
            paint.setColor(this.bgColor);
            canvas.drawRect(0.0f, 0.0f, (float) this.width, (float) this.height, paint);
        }
        if (this.cx != null) {
            switch (this.aAM) {
                case 0:
                case 1:
                    this.cx.setBounds(0, 0, this.width, this.height);
                    this.cx.draw(canvas);
                    break;
            }
        }
        if (this.aAB != null) {
            int size = this.aAB.size();
            if (this.aAJ) {
            }
            int i = this.paddingLeft;
            if (this.aAC == 1) {
                i = this.width - this.paddingRight;
            }
            canvas.translate(0.0f, (float) this.paddingTop);
            int i2 = 0;
            int i3 = i;
            while (i2 < size) {
                ac acVar = (ac) this.aAB.get(i2);
                if (this.aAC == 1) {
                    i3 -= this.vk;
                }
                canvas.translate((float) i3, 0.0f);
                if (i2 == this.aAG) {
                    if (this.aAP[aAO] != null) {
                        this.aAP[aAO].draw(canvas);
                    }
                } else if (i2 == this.aAH) {
                    if (this.aAP[aAN] != null) {
                        this.aAP[aAN].draw(canvas);
                    }
                } else if (this.aAP[DEFAULT] != null) {
                    this.aAP[DEFAULT].draw(canvas);
                }
                canvas.save();
                acVar.a(canvas, this.vk, this.vl);
                canvas.restore();
                canvas.translate((float) (-i3), 0.0f);
                i2++;
                i3 = this.aAC == 1 ? i3 - this.aAR : this.aAR + this.vk + i3;
            }
        }
    }

    public void eB(int i) {
        this.aAI = i;
    }

    public boolean eZ(int i) {
        if (this.aAF == null) {
            return false;
        }
        this.aAF.b(this, ((ac) this.aAB.get(i)).getId());
        return true;
    }

    public void fa(int i) {
        Ix();
    }

    public void m(Drawable drawable) {
        if (drawable != null) {
            this.aAP[aAO] = drawable;
            this.aAP[aAO].setBounds(0, 0, this.vk, this.vl);
        }
    }

    public void removeItem(int i) {
        if (this.lO != null) {
            this.lO.removeElementAt(i);
        }
    }

    public void s(Drawable drawable) {
        this.cx = drawable;
    }

    public void setPadding(int i, int i2, int i3, int i4) {
        this.paddingLeft = i;
        this.paddingTop = i2;
        this.paddingRight = i3;
        this.paddingBottom = i4;
    }

    public void setSize(int i, int i2) {
        this.width = i;
        this.height = i2;
        yj();
    }

    public void t(Drawable drawable) {
        if (drawable != null) {
            this.aAP[DEFAULT] = drawable;
            this.aAP[DEFAULT].setBounds(0, 0, this.vk, this.vl);
        }
    }

    public void u(Drawable drawable) {
        if (drawable != null) {
            this.aAP[aAN] = drawable;
            this.aAP[aAN].setBounds(0, 0, this.vk, this.vl);
        }
    }

    public void v(byte b2) {
        this.aAC = b2;
    }

    public final Vector xa() {
        return this.lO;
    }

    public void yi() {
        if (this.lO != null) {
            if (this.aAB == null) {
                this.aAB = new Vector();
            } else {
                this.aAB.clear();
            }
            Iterator it = this.lO.iterator();
            while (it.hasNext()) {
                ac acVar = (ac) it.next();
                int i = acVar.visibility;
                if (i == 0 || i == 1) {
                    this.aAB.add(acVar);
                }
            }
            yj();
        }
    }

    public h yk() {
        return this.aAF;
    }

    public byte yl() {
        return this.aAC;
    }
}
