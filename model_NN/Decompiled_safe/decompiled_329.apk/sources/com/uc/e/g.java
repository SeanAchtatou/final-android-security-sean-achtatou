package com.uc.e;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;

public class g extends z {
    private int sA;
    private Drawable sB;
    private int sC = 0;
    private int sD;
    private int sE;
    private int sF;
    private int sG;
    private float sH;
    private j sI;
    private int sJ;
    private String[] sK;
    private Drawable so;
    private Paint sp;
    private Paint sq;
    private int sr;
    private int ss;
    private int st;
    private int su;
    private int sv;
    private int sw = 0;
    private int sx = 0;
    private int sy;
    private int sz;

    public g() {
        a();
    }

    private void a() {
        this.sp = new Paint();
        this.sq = new Paint();
        this.sp.setAntiAlias(true);
        this.sq.setAntiAlias(true);
    }

    public void a(j jVar) {
        this.sI = jVar;
        this.sK = new String[this.sI.lb().length];
        for (int i = 0; i < this.sK.length; i++) {
            this.sK[i] = new String(this.sI.lb()[i]);
        }
    }

    public boolean a(KeyEvent keyEvent) {
        return false;
    }

    public void aA(int i) {
        this.su = i;
    }

    public void aB(int i) {
        this.sv = i;
    }

    public void aC(int i) {
        this.sJ = i;
    }

    public void aD(int i) {
        this.sw = i;
    }

    public void aE(int i) {
        this.sD = i;
    }

    public void aF(int i) {
        this.sG = i;
    }

    public void av(int i) {
        this.sA = i;
        this.sq.setTextSize((float) this.sA);
    }

    public void aw(int i) {
        this.sq.setColor(i);
    }

    public void ax(int i) {
        this.sr = i;
    }

    public void ay(int i) {
        this.ss = i;
    }

    public void az(int i) {
        this.st = i;
    }

    public void d(float f) {
        this.sH = f;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.so != null) {
            canvas.save();
            canvas.translate((float) (this.sv + this.sJ), (float) this.sv);
            this.so.draw(canvas);
            canvas.restore();
        }
        if (this.sI.la() != null) {
            canvas.save();
            canvas.translate((float) ((this.aSP - this.sI.la().getBounds().width()) / 2), (float) this.sv);
            this.sI.la().draw(canvas);
            canvas.restore();
        }
        canvas.save();
        canvas.translate((float) ((this.aSP - this.sB.getBounds().width()) / 2), (float) (this.sv + this.sw));
        this.sB.draw(canvas);
        canvas.restore();
        canvas.translate((float) (this.sr + this.sv + this.sJ), (float) (this.sv + this.sw + this.sC + this.st));
        if (this.sK != null) {
            y.a(this.sK, canvas, this.sq, this.sy, this.sz, this.sD);
        }
    }

    public j eK() {
        return this.sI;
    }

    /* access modifiers changed from: protected */
    public void eL() {
        super.eL();
        this.sx = (this.aSP - (this.sv * 2)) - (this.sJ * 2);
        if (this.so != null) {
            this.so.setBounds(0, 0, this.sx, this.sw);
        }
        this.sy = (((this.aSP - this.sr) - this.ss) - (this.sv * 2)) - (this.sJ * 2);
        this.sz = ((((this.aSQ - this.sw) - this.sC) - this.st) - this.su) - (this.sv * 2);
    }

    public void eM() {
        if (this.sI.lb() != null) {
            this.sK = new String[this.sI.lb().length];
            for (int i = 0; i < this.sK.length; i++) {
                this.sK[i] = new String(this.sI.lb()[i]);
            }
            this.sy = (((this.aSP - this.sr) - this.ss) - (this.sv * 2)) - (this.sJ * 2);
            y.a(this.sK, this.sy, this.sq);
        }
    }

    public int eN() {
        return this.sr;
    }

    public int eO() {
        return this.ss;
    }

    public int eP() {
        return this.st;
    }

    public int eQ() {
        return this.su;
    }

    public int eR() {
        return this.sv;
    }

    public int eS() {
        return this.sw;
    }

    public int eT() {
        return this.sD;
    }

    public int eU() {
        return this.sG;
    }

    public float eV() {
        return this.sH;
    }

    public void h(Drawable drawable) {
        this.so = drawable;
        if (this.so != null && this.sx != 0 && this.sw != 0) {
            this.so.setBounds(0, 0, this.sx, this.sw);
        }
    }

    public void i(Drawable drawable) {
        this.sB = drawable;
        if (this.sB != null) {
            this.sB.setBounds(0, 0, this.sB.getIntrinsicWidth(), this.sB.getIntrinsicHeight());
            this.sC = this.sB.getIntrinsicHeight();
        }
    }
}
