package com.uc.e;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import java.util.ArrayList;
import java.util.Vector;

public class f extends s {
    private static final long qE = 20;
    private static final byte qF = 0;
    private static final byte qG = 1;
    private static final byte qH = 2;
    private static final byte qI = 3;
    private static final byte qL = 0;
    private static final byte qM = 1;
    private static final byte qN = 2;
    protected ArrayList qA;
    protected ArrayList qB;
    private ArrayList qC = null;
    private ArrayList qD = null;
    private Drawable[] qJ;
    private int[] qK;
    private int qO = 0;
    private m qP;

    private void ao(int i) {
        this.vj.remove(i);
    }

    private boolean ap(int i) {
        return this.vj.get(i) instanceof ab;
    }

    private int[] as(int i) {
        int[] iArr = new int[2];
        int i2 = i + 1;
        int i3 = 0;
        while (true) {
            int i4 = i2;
            if (i3 < this.qB.size()) {
                if (i4 - ((Integer) this.qB.get(i3)).intValue() <= 0) {
                    iArr[0] = i3;
                    iArr[1] = i4 - 1;
                    break;
                }
                i2 = i4 - ((Integer) this.qB.get(i3)).intValue();
                i3++;
            } else {
                break;
            }
        }
        return iArr;
    }

    private int at(int i) {
        for (int i2 = 0; i2 < this.vj.size(); i2++) {
            if (this.vj.get(i2) == this.qA.get(i)) {
                return i2;
            }
        }
        return -1;
    }

    private boolean er() {
        boolean z;
        int size = this.qC.size();
        int i = 0;
        boolean z2 = false;
        while (i < size) {
            if (this.qC.get(i) != null) {
                if (this.qD.get(i) == null) {
                    this.qD.set(i, new Transformation());
                }
                if (((AnimationSet) this.qC.get(i)).getTransformation(System.currentTimeMillis(), (Transformation) this.qD.get(i))) {
                    z = true;
                    i++;
                    z2 = z;
                } else {
                    if (!((ab) this.qA.get(i)).bRD) {
                        an(i);
                    }
                    this.qC.set(i, null);
                    this.qD.set(i, null);
                }
            }
            z = z2;
            i++;
            z2 = z;
        }
        return z2;
    }

    private int et() {
        for (int i = 0; i < this.qA.size(); i++) {
            if (!(this.qA.get(i) instanceof ab)) {
                return i;
            }
        }
        return this.qA.size();
    }

    private int eu() {
        int et = et();
        return et < this.qA.size() ? at(et) : this.vj.size();
    }

    public void a(int i, z zVar) {
        ab abVar = (ab) this.qA.get(i);
        int intValue = ((Integer) this.qB.get(i)).intValue();
        abVar.i(zVar);
        if (abVar.bRD) {
            c(zVar, this.vj.indexOf(abVar) + intValue);
            this.qB.set(i, Integer.valueOf(intValue + 1));
        }
    }

    public void a(Drawable drawable, int i) {
        a(new Drawable[]{drawable, drawable, drawable}, i);
    }

    public void a(ab abVar) {
        int i;
        int i2;
        int indexOf = this.qA.indexOf(abVar);
        if (indexOf >= 0) {
            if (indexOf == 0) {
                i = 0;
            } else {
                int i3 = 0;
                int i4 = 0;
                while (i3 < indexOf) {
                    i3++;
                    i4 = ((Integer) this.qB.get(i3)).intValue() + i4;
                }
                i = i4;
            }
            Vector MX = abVar.MX();
            int height = abVar.getHeight();
            int i5 = 0;
            int i6 = 0;
            while (i5 < i) {
                i5++;
                i6 = ((z) this.vj.elementAt(i5)).getHeight() + i6;
            }
            int size = MX != null ? MX.size() : 0;
            if (((Integer) this.qB.get(indexOf)).intValue() != size + 1) {
                int i7 = height;
                int i8 = size - 1;
                while (i8 >= 0) {
                    c((z) MX.elementAt(i8), i + 1);
                    i8--;
                    i7 = ((z) MX.elementAt(i8)).getHeight() + i7;
                }
            }
            this.qB.set(indexOf, Integer.valueOf(size + 1));
            AnimationSet animationSet = new AnimationSet(true);
            ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.0f, 0.0f, 1.0f);
            scaleAnimation.setDuration(qE);
            animationSet.addAnimation(scaleAnimation);
            this.qC.set(indexOf, animationSet);
            animationSet.start();
            if (this.aBk + i6 > 0) {
                int yZ = yZ();
                i2 = yZ - i6 >= getHeight() ? -(this.aBk + i6) : -((yZ - getHeight()) + this.aBk);
            } else {
                i2 = 0;
            }
            if (i2 != 0) {
                fq(i2);
                Ix();
            }
            abVar.bRD = true;
        }
    }

    public void a(ab abVar, boolean z) {
        int i;
        int i2 = 0;
        int indexOf = this.qA.indexOf(abVar);
        if (indexOf >= 0) {
            if (indexOf == 0) {
                i = 0;
            } else {
                int i3 = 0;
                int i4 = 0;
                while (i3 < indexOf) {
                    i3++;
                    i4 = ((Integer) this.qB.get(i3)).intValue() + i4;
                }
                i = i4;
            }
            if (z) {
                Vector MX = abVar.MX();
                if (MX != null) {
                    i2 = MX.size();
                }
                if (((Integer) this.qB.get(indexOf)).intValue() != i2 + 1) {
                    for (int i5 = i2 - 1; i5 >= 0; i5--) {
                        c((z) MX.elementAt(i5), i + 1);
                    }
                }
                this.qB.set(indexOf, Integer.valueOf(i2 + 1));
                abVar.bRD = true;
                return;
            }
            for (int intValue = ((Integer) this.qB.get(indexOf)).intValue() - 1; intValue > 0; intValue--) {
                ao(i + 1);
            }
            this.qB.set(indexOf, 1);
            eL();
            abVar.bRD = false;
        }
    }

    public void a(m mVar) {
        this.qP = mVar;
    }

    public void a(z zVar) {
        if (zVar instanceof ab) {
            int eu = eu();
            int et = et();
            super.c(zVar, eu);
            this.qA.add(et, zVar);
            this.qB.add(et, 1);
            this.qC.add(et, null);
            this.qD.add(et, null);
            return;
        }
        super.a(zVar);
        this.qA.add(zVar);
        this.qB.add(1);
        this.qC.add(null);
        this.qD.add(null);
    }

    public void a(Drawable[] drawableArr, int i) {
        a(drawableArr, new int[]{i, i, i});
    }

    public void a(Drawable[] drawableArr, int[] iArr) {
        this.qJ = drawableArr;
        this.qK = iArr;
    }

    public void am(int i) {
        this.qO = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.e.s.a(float, int, boolean):void
     arg types: [int, int, int]
     candidates:
      com.uc.e.s.a(int, boolean, boolean):void
      com.uc.e.s.a(android.graphics.Canvas, com.uc.e.z, int):void
      com.uc.e.s.a(byte, int, int):boolean
      com.uc.e.z.a(byte, int, int):boolean
      com.uc.e.s.a(float, int, boolean):void */
    public void an(int i) {
        int i2;
        if (i == 0) {
            i2 = 0;
        } else {
            int i3 = 0;
            int i4 = 0;
            while (i3 < i) {
                i3++;
                i4 = ((Integer) this.qB.get(i3)).intValue() + i4;
            }
            i2 = i4;
        }
        for (int intValue = ((Integer) this.qB.get(i)).intValue() - 1; intValue > 0; intValue--) {
            ao(i2 + 1);
        }
        this.qB.set(i, 1);
        eL();
        int yZ = yZ();
        if (this.aBk + yZ >= this.aSQ) {
            return;
        }
        if (yZ < this.aSQ) {
            a(0.0f, -this.aBk, false);
        } else {
            a(0.0f, (this.aSQ - yZ) - this.aBk, false);
        }
    }

    public boolean aq(int i) {
        if (this.vj.get(i) instanceof ab) {
            ab abVar = (ab) this.vj.get(i);
            if (abVar.bRD) {
                b(abVar);
            } else {
                a(abVar);
            }
            return true;
        }
        fo(-1);
        if (this.qP == null) {
            return false;
        }
        int[] as = as(i);
        this.qP.a((z) this.vj.get(i), as[0], as[1]);
        return true;
    }

    public boolean ar(int i) {
        if (this.qP == null) {
            return false;
        }
        int[] as = as(i);
        this.qP.b((z) this.vj.get(i), as[0], as[1]);
        return true;
    }

    public void b(ab abVar) {
        int indexOf = this.qA.indexOf(abVar);
        if (indexOf >= 0) {
            AnimationSet animationSet = new AnimationSet(true);
            ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.0f, 1.0f, 0.0f);
            scaleAnimation.setDuration(qE);
            animationSet.addAnimation(scaleAnimation);
            this.qC.set(indexOf, animationSet);
            animationSet.start();
            Ix();
            abVar.bRD = false;
        }
    }

    public void c(Vector vector) {
        super.c(vector);
        this.qA = new ArrayList(vector);
        int size = vector != null ? vector.size() : 0;
        this.qB = new ArrayList();
        this.qC = new ArrayList();
        this.qD = new ArrayList();
        for (int i = 0; i < size; i++) {
            this.qB.add(1);
            this.qC.add(null);
            this.qD.add(null);
        }
    }

    public void clear() {
        this.qA.clear();
        this.qB.clear();
        this.qC.clear();
        this.qD.clear();
        super.clear();
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        int i;
        float f;
        Drawable drawable;
        int i2;
        int i3;
        int i4;
        int i5;
        Drawable drawable2;
        int i6;
        int i7;
        canvas.translate((float) this.paddingLeft, 0.0f);
        if (this.vj.size() > 0) {
            int size = this.vj.size();
            int i8 = 0;
            int i9 = this.aBk;
            Drawable drawable3 = null;
            int i10 = 0;
            int i11 = 0;
            int i12 = 0;
            while (i12 < size && i9 <= this.aSQ) {
                z zVar = (z) this.vj.get(i12);
                int height = zVar.getHeight();
                if (this.qD.get(i8) == null || i11 <= 0) {
                    i = height;
                    f = 1.0f;
                } else {
                    float[] fArr = new float[9];
                    ((Transformation) this.qD.get(i8)).getMatrix().getValues(fArr);
                    float f2 = fArr[4];
                    int i13 = (int) (((float) height) * fArr[4]);
                    canvas.getMatrix().postConcat(((Transformation) this.qD.get(i8)).getMatrix());
                    i = i13;
                    f = f2;
                }
                if (this.qJ == null || i12 >= size) {
                    drawable = drawable3;
                    i2 = i10;
                } else if (i11 == 1) {
                    drawable = this.qJ[1];
                    i2 = this.qK[1];
                } else if (i11 == 0) {
                    if ((i12 <= 0 || !ap(i12 - 1)) && !ap(i12)) {
                        drawable = this.qJ[2];
                        i2 = this.qK[2];
                    } else {
                        drawable = this.qJ[0];
                        i2 = this.qK[0];
                    }
                } else if (i11 <= 1 || i11 >= ((Integer) this.qB.get(i8)).intValue()) {
                    drawable = this.qJ[0];
                    i2 = this.qK[0];
                } else {
                    drawable = this.qJ[2];
                    i2 = this.qK[2];
                }
                int i14 = drawable != null ? this.dividerHeight + i : i;
                if (i9 + i14 <= 0) {
                    int i15 = i14 + i9;
                    int i16 = i11 + 1;
                    if (i16 == ((Integer) this.qB.get(i8)).intValue()) {
                        Drawable drawable4 = drawable;
                        i5 = 0;
                        i6 = i15;
                        i7 = i8 + 1;
                        drawable2 = drawable4;
                    } else {
                        drawable2 = drawable;
                        i5 = i16;
                        i6 = i15;
                        i7 = i8;
                    }
                } else {
                    canvas.save();
                    canvas.translate(0.0f, (float) i9);
                    canvas.save();
                    canvas.clipRect(0, 0, this.aSP, i);
                    char c = 0;
                    if (i11 > 0 || !(zVar instanceof ab)) {
                        c = 1;
                    }
                    if (!(this.aDW == null || this.aDW[c] == null)) {
                        this.aDW[c].setBounds(0, 0, this.aSP, i);
                        this.aDW[c].draw(canvas);
                    }
                    if (i12 == this.aeK) {
                        c = 3;
                    } else if (i12 == this.bdt - 1) {
                        c = 2;
                    }
                    if (!(this.aDW == null || this.aDW[c] == null)) {
                        this.aDW[c].setBounds(this.qO, 0, this.aSP - this.qO, i);
                        this.aDW[c].draw(canvas);
                    }
                    if (((double) f) > 0.5d) {
                        zVar.draw(canvas);
                    }
                    canvas.restore();
                    if (drawable != null) {
                        drawable.setBounds(0, -this.dividerHeight, this.aSP, i2 - this.dividerHeight);
                        drawable.draw(canvas);
                        if (i12 == size - 1 && !ap(i12)) {
                            drawable.setBounds(0, i - this.dividerHeight, this.aSP, (i + i2) - this.dividerHeight);
                            drawable.draw(canvas);
                        }
                        drawable = null;
                    }
                    canvas.restore();
                    int i17 = i11 + 1;
                    if (i17 == ((Integer) this.qB.get(i8)).intValue()) {
                        i4 = i8 + 1;
                        i3 = 0;
                    } else {
                        i3 = i17;
                        i4 = i8;
                    }
                    Drawable drawable5 = drawable;
                    i5 = i3;
                    drawable2 = drawable5;
                    int i18 = i4;
                    i6 = i14 + i9;
                    i7 = i18;
                }
                i12++;
                i8 = i7;
                i9 = i6;
                i11 = i5;
                i10 = i2;
                drawable3 = drawable2;
            }
            i(canvas);
        } else if (this.aEn != null) {
            this.aEn.draw(canvas);
        }
        Drawable zg = zg();
        if (zg != null) {
            int intrinsicHeight = zg.getIntrinsicHeight();
            if (intrinsicHeight <= 0) {
                intrinsicHeight = 5;
            }
            zg.setBounds(0 - this.paddingLeft, this.aSQ - intrinsicHeight, this.aSP, this.aSQ);
            zg.draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public boolean es() {
        return super.es() || er();
    }

    public void v(int i, int i2) {
        int at = at(i) + i2;
        z zVar = (z) this.vj.get(at);
        if (i2 == 0) {
            if ((zVar instanceof ab) && ((ab) zVar).bRD) {
                int i3 = 0;
                while (true) {
                    int i4 = i3;
                    if (i4 >= ((Integer) this.qB.get(i)).intValue()) {
                        break;
                    }
                    this.vj.remove(at);
                    i3 = i4 + 1;
                }
            } else {
                this.vj.remove(at);
            }
            this.qA.remove(i);
            this.qC.remove(i);
            this.qD.remove(i);
            return;
        }
        this.qB.set(i, Integer.valueOf(((Integer) this.qB.get(i)).intValue() - 1));
        ((ab) this.qA.get(i)).j(zVar);
        this.vj.remove(at);
    }

    public z w(int i, int i2) {
        return gd(at(i) + i2);
    }
}
