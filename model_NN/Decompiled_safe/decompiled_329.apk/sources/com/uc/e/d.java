package com.uc.e;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.uc.browser.UCR;
import com.uc.e.b.k;
import com.uc.h.e;

public class d extends z {
    private int pM;
    public int pN;
    private Drawable pO;
    private Drawable pP;
    private Drawable pQ;
    private int pR;
    private int pS;
    private x pT;

    public d(float f, int i, int i2) {
        setSize(i, i2);
        this.pR = (int) (((float) this.pM) * f);
        bL(true);
        e Pb = e.Pb();
        Bitmap kl = Pb.kl(UCR.drawable.aWl);
        Bitmap kl2 = Pb.kl(UCR.drawable.aWm);
        new Matrix().setRotate(180.0f);
        Bitmap kl3 = Pb.kl(UCR.drawable.aWn);
        Bitmap kl4 = Pb.kl(UCR.drawable.aWo);
        this.pP = new k(new Bitmap[]{kl, kl2}, new byte[]{3, 0}, (byte) 0);
        this.pQ = new k(new Bitmap[]{kl4, kl3, kl3}, new byte[]{0, 3, 0}, (byte) 0);
        this.pN = kl.getHeight();
    }

    public void a(x xVar) {
        this.pT = xVar;
    }

    public boolean a(byte b2, int i, int i2) {
        switch (b2) {
            case 0:
                this.pS = i;
                return true;
            case 1:
            default:
                return true;
            case 2:
                if (Math.abs(i - this.pS) < 5) {
                    return true;
                }
                this.pR += i - this.pS;
                if (this.pR > this.pM) {
                    this.pR = this.pM;
                }
                if (this.pR < 0) {
                    this.pR = 0;
                }
                this.pS = i;
                Ix();
                ei();
                return true;
        }
    }

    public void aj(int i) {
        this.pN = i;
    }

    public void c(float f) {
        this.pR = (int) (((float) this.pM) * f);
    }

    public void draw(Canvas canvas) {
        this.pL.setColor(-7829368);
        int width = (getWidth() - this.pM) / 2;
        int height = (getHeight() - this.pN) / 2;
        int height2 = (getHeight() + this.pN) / 2;
        int i = this.pR + width;
        this.pP.setBounds(new Rect((this.pO.getIntrinsicWidth() / 4) + width, height, this.pM + width, height2));
        this.pP.draw(canvas);
        this.pQ.setBounds(new Rect(width, height, i, height2));
        this.pQ.draw(canvas);
        this.pO.setBounds(i - (this.pO.getIntrinsicWidth() / 2), (getHeight() - this.pO.getIntrinsicHeight()) / 2, i + (this.pO.getIntrinsicWidth() / 2), (getHeight() + this.pO.getIntrinsicHeight()) / 2);
        this.pO.draw(canvas);
    }

    public void e(Drawable drawable) {
        this.pO = drawable;
    }

    public float eh() {
        return (((float) this.pR) * 1.0f) / ((float) this.pM);
    }

    /* access modifiers changed from: protected */
    public void ei() {
        if (this.pT != null) {
            this.pT.b(eh());
        }
    }

    public void f(Drawable drawable) {
        this.pP = drawable;
    }

    public void g(Drawable drawable) {
        this.pQ = drawable;
    }

    public int getLength() {
        return this.pM;
    }

    public void setLength(int i) {
        this.pR = (int) (((float) this.pR) * (((float) i) / ((float) this.pM)));
        this.pM = i;
    }

    public void setSize(int i, int i2) {
        super.setSize(i, i2);
        int width = (getWidth() * 5) / 6;
        this.pR = (int) (((float) this.pR) * (((float) width) / ((float) this.pM)));
        this.pM = width;
    }
}
