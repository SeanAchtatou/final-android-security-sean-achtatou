package com.uc.e;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;

public class b extends z {
    private static final long dD = 200;
    private String aU = "123";
    protected Drawable[] dA;
    protected Drawable dB;
    private AnimationSet dC;
    protected Bitmap dE;
    protected boolean dF = false;
    private Paint dG = null;
    protected Paint dH = null;
    private int dv = 18;
    private int dw = -1;
    protected boolean dx = true;
    protected int dy = 0;
    protected int dz = 100;

    private Paint getPaint() {
        if (this.dG == null) {
            this.dG = new Paint();
            this.dG.setAntiAlias(true);
            this.dG.setColor(this.dw);
            this.dG.setFakeBoldText(true);
            this.dG.setTextSize((float) this.dv);
        }
        return this.dG;
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, byte b2) {
        if (!(this.dA == null || this.dA[b2] == null)) {
            this.dA[b2].draw(canvas);
        }
        if (this.aU != null && this.dv > 0) {
            canvas.drawText(this.aU, (float) (this.dv / 2), (float) (((this.dy - this.dv) / 2) + this.dv), getPaint());
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, Paint paint) {
    }

    public void a(Drawable[] drawableArr) {
        this.dA = drawableArr;
    }

    public boolean a(byte b2, int i, int i2) {
        switch (b2) {
            case 0:
            case 1:
                return true;
            case 2:
                return false;
            default:
                return super.a(b2, i, i2);
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(Canvas canvas) {
        Transformation transformation = new Transformation();
        boolean transformation2 = this.dC.getTransformation(System.currentTimeMillis(), transformation);
        Matrix matrix = transformation.getMatrix();
        float[] fArr = new float[9];
        canvas.getMatrix().getValues(fArr);
        matrix.postTranslate(fArr[2], fArr[5] + ((float) this.dy));
        canvas.setMatrix(matrix);
        ci().setAlpha((int) (transformation.getAlpha() * 255.0f));
        transformation.getMatrix().getValues(fArr);
        this.aSQ = (int) (((float) this.dy) + (((float) this.dz) * fArr[4]));
        this.bBm.eL();
        return transformation2;
    }

    public boolean a(KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1) {
            return super.a(keyEvent);
        }
        switch (keyEvent.getKeyCode()) {
            case 19:
            case 20:
            case 21:
            case 22:
                return false;
            case 23:
            case 66:
                if (this.dx) {
                    cl();
                    Ix();
                } else {
                    cm();
                    Ix();
                }
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public Paint ci() {
        if (this.dH == null) {
            this.dH = new Paint();
        }
        return this.dH;
    }

    public void cj() {
        if (this.dz <= 0) {
            return;
        }
        if (this.dE == null || this.dE.isRecycled()) {
            try {
                this.dE = Bitmap.createBitmap(this.aSP, this.dz, Bitmap.Config.ARGB_8888);
                a(new Canvas(this.dE), ci());
                this.dF = true;
            } catch (Throwable th) {
            }
        }
    }

    public void ck() {
        if (this.dE != null && !this.dE.isRecycled()) {
            this.dE.recycle();
        }
        this.dF = false;
        this.dE = null;
    }

    public void cl() {
        this.dx = false;
        this.dC = new AnimationSet(true);
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.0f, 0.001f, 1.0f);
        scaleAnimation.setDuration(dD);
        this.dC.addAnimation(scaleAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.3f, 1.0f);
        alphaAnimation.setDuration(dD);
        this.dC.addAnimation(alphaAnimation);
        this.dC.start();
        cj();
    }

    public void cm() {
        this.dx = true;
        this.dC = new AnimationSet(true);
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.0f, 1.0f, 0.001f);
        scaleAnimation.setDuration(dD);
        this.dC.addAnimation(scaleAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.3f);
        alphaAnimation.setDuration(dD);
        this.dC.addAnimation(alphaAnimation);
        this.dC.start();
        cj();
    }

    public int cn() {
        return this.dz;
    }

    public int co() {
        return this.dv;
    }

    public int cp() {
        return this.dw;
    }

    public void draw(Canvas canvas) {
        boolean z = false;
        a(canvas, Iz());
        if (this.dC != null) {
            z = a(canvas);
        } else {
            canvas.translate(0.0f, (float) this.dy);
        }
        if (z || !this.dx) {
            a(canvas, ci());
        }
        if (z) {
            Ix();
            Iv();
            return;
        }
        this.dC = null;
        Iw();
    }

    public String getTitle() {
        return this.aU;
    }

    /* access modifiers changed from: protected */
    public boolean k(int i, int i2) {
        if (i2 > this.dy) {
            return super.k(i, i2);
        }
        if (this.dx) {
            cl();
            Ix();
        } else {
            cm();
            Ix();
        }
        return true;
    }

    public void o(boolean z) {
        this.dx = !z;
        this.aSQ = this.dy + this.dz;
    }

    public void setSize(int i, int i2) {
        super.setSize(i, i2);
        this.dy = i2;
        if (!this.dx) {
            this.aSQ = this.dy + this.dz;
        } else {
            this.aSQ = this.dy;
        }
    }

    public void t(String str) {
        this.aU = str;
    }

    public void y(int i) {
        this.dv = i;
    }

    public void z(int i) {
        this.dw = i;
    }
}
