package com.uc.e;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import com.uc.browser.R;
import com.uc.browser.UCR;
import com.uc.e.a.c;
import com.uc.h.e;
import java.lang.reflect.Array;

public class p extends u implements Animation.AnimationListener, n {
    private static int arM = e.Pb().kp(R.dimen.f401uidialog_title_height);
    private static int arN = e.Pb().kp(R.dimen.f304dialog_padding_top);
    private static int arO = e.Pb().kp(R.dimen.f402uidialog_margin);
    private static int arP = e.Pb().kp(R.dimen.f299dialog_button_width);
    private static int arQ = e.Pb().kp(R.dimen.f300dialog_button_height);
    private Animation Z;
    private String aU;
    private boolean arR;
    private boolean arS;
    private k arT;
    private q arU;
    private Drawable arV;
    private c[] arW;
    private int[] arX;
    private int[][] arY;
    private Matrix arZ;
    private Bitmap asa;
    private int asb;
    private int dv;
    private Drawable icon;
    private z tI;
    private int x;
    private int y;

    public p() {
        this.arX = new int[2];
        this.pL.setColor(e.Pb().getColor(7));
        this.pL.setTextSize((float) e.Pb().kp(R.dimen.f397uidialog1_font));
        this.icon = e.Pb().getDrawable(UCR.drawable.aUJ);
        this.arV = e.Pb().getDrawable(UCR.drawable.aUo);
        s(e.Pb().getDrawable(UCR.drawable.aUm));
    }

    public p(String str, z zVar, String... strArr) {
        this();
        t(str);
        f(zVar);
        k(strArr);
        wx();
    }

    private static int eS(int i) {
        return (arP * i) + (arO * (i + 1));
    }

    private Rect eT(int i) {
        return new Rect(this.arY[i][0], this.arY[i][1], this.arY[i][0] + this.arW[i].getWidth(), this.arY[i][1] + this.arW[i].getHeight());
    }

    private void h(Canvas canvas) {
        e(canvas);
        canvas.save();
        canvas.translate(0.0f, (float) arN);
        if (this.icon != null) {
            this.icon.setBounds(arO, (arM - this.icon.getIntrinsicHeight()) / 2, arO + this.icon.getIntrinsicWidth(), (arM + this.icon.getIntrinsicHeight()) / 2);
            this.icon.draw(canvas);
        }
        if (this.aU != null) {
            canvas.drawText(this.aU, (float) (arO + this.icon.getIntrinsicWidth() + (arO / 2)), ((((float) arM) - this.pL.descent()) - this.pL.ascent()) / 2.0f, this.pL);
        }
        this.arV.setBounds(arO, arM, getWidth() - arO, arM + 2);
        this.arV.draw(canvas);
        canvas.restore();
        if (this.tI != null) {
            canvas.save();
            canvas.translate((float) this.arX[0], (float) this.arX[1]);
            canvas.clipRect(0, 0, this.tI.getWidth(), this.tI.getHeight());
            this.tI.onDraw(canvas);
            canvas.restore();
        }
        if (this.arW.length > 0) {
            for (int i = 0; i < this.arW.length; i++) {
                canvas.save();
                canvas.translate((float) this.arY[i][0], (float) this.arY[i][1]);
                canvas.clipRect(0, 0, this.arW[i].getWidth(), this.arW[i].getHeight());
                this.arW[i].onDraw(canvas);
                canvas.restore();
            }
        }
    }

    private static int wA() {
        return arQ + (arO * 2);
    }

    private Rect wB() {
        return new Rect(this.arX[0], this.arX[1], this.arX[0] + this.tI.getWidth(), this.arX[1] + this.tI.getHeight());
    }

    public void a(k kVar) {
        this.arT = kVar;
    }

    public void a(q qVar) {
        this.arU = qVar;
    }

    public void dismiss() {
        this.Z = new ScaleAnimation(1.0f, 0.9f, 1.0f, 0.9f, 0.5f * ((float) getWidth()), 0.5f * ((float) getHeight()));
        this.Z.initialize(0, 0, 0, 0);
        this.Z.setDuration(200);
        this.Z.setAnimationListener(this);
        this.arS = true;
        invalidate();
    }

    public void draw(Canvas canvas) {
        if (this.arZ != null) {
            if (this.asa == null) {
                try {
                    this.asa = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
                    h(new Canvas(this.asa));
                } catch (Throwable th) {
                    return;
                }
            }
            canvas.drawBitmap(this.asa, this.arZ, null);
            return;
        }
        if (this.asa != null) {
            this.asa.recycle();
            this.asa = null;
        }
        h(canvas);
    }

    /* access modifiers changed from: protected */
    public int e(int i, int i2, int[] iArr) {
        if (wB().contains(i, i2)) {
            if (iArr != null) {
                iArr[0] = this.arX[0];
                iArr[1] = this.arX[1];
            }
            return this.vj.indexOf(this.tI);
        }
        for (int i3 = 0; i3 < this.arW.length; i3++) {
            if (eT(i3).contains(i, i2)) {
                if (iArr != null) {
                    iArr[0] = this.arY[i3][0];
                    iArr[1] = this.arY[i3][1];
                }
                return this.vj.indexOf(this.arW[i3]);
            }
        }
        return super.e(i, i2, iArr);
    }

    public void e(z zVar) {
        for (int i = 0; i < this.arW.length; i++) {
            if (this.arW[i] == zVar) {
                if (this.arU != null) {
                    this.arU.au(i);
                }
                dismiss();
                return;
            }
        }
    }

    public c eR(int i) {
        if (this.arW == null || this.arW.length <= i) {
            return null;
        }
        return this.arW[i];
    }

    /* access modifiers changed from: protected */
    public boolean es() {
        if (this.Z == null) {
            return false;
        }
        Transformation transformation = new Transformation();
        boolean transformation2 = this.Z.getTransformation(System.currentTimeMillis(), transformation);
        this.arZ = transformation2 ? transformation.getMatrix() : null;
        if (!transformation2) {
            this.Z = null;
        }
        return transformation2;
    }

    public void f(z zVar) {
        this.tI = zVar;
        a(zVar);
    }

    public Rect getBounds() {
        return new Rect(this.x, this.y, this.x + getWidth(), this.y + getHeight());
    }

    public boolean isShown() {
        return this.arR;
    }

    public void k(String... strArr) {
        if (strArr.length > 2) {
            throw new IllegalArgumentException("the number of buttons is larger than 2 !");
        }
        this.arW = new c[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            this.arW[i] = new c(strArr[i]);
            this.arW[i].setSize(arP, arQ);
            a(this.arW[i]);
            this.arW[i].a(this);
        }
        this.arY = (int[][]) Array.newInstance(Integer.TYPE, strArr.length, 2);
    }

    public void onAnimationEnd(Animation animation) {
        if (this.arS) {
            this.Z = null;
            this.arR = false;
            this.arS = false;
            invalidate();
            onDismiss();
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    /* access modifiers changed from: protected */
    public void onDismiss() {
        if (this.arT != null) {
            this.arT.b(this);
        }
    }

    public void setIcon(Drawable drawable) {
        this.icon = drawable;
    }

    /* access modifiers changed from: package-private */
    public void setPosition(int i, int i2) {
        this.x = i;
        this.y = i2;
    }

    public void show() {
        this.Z = new ScaleAnimation(0.9f, 1.0f, 0.9f, 1.0f, 0.5f * ((float) getWidth()), 0.5f * ((float) getHeight()));
        this.Z.initialize(0, 0, 0, 0);
        this.Z.setDuration(200);
        this.arR = true;
        invalidate();
    }

    public void t(String str) {
        this.aU = str;
    }

    public void wx() {
        int eS = eS(this.arW.length);
        int max = Math.max(eS, this.tI.getWidth());
        setSize(max, arM + arN + this.tI.getHeight() + (this.arW.length > 0 ? wA() : 0));
        this.tI.setSize(max - (arO * 2), this.tI.getHeight());
        this.arX[0] = arO;
        this.arX[1] = arM + arN;
        int width = ((getWidth() - eS) / 2) + arO;
        for (int i = 0; i < this.arY.length; i++) {
            this.arY[i][0] = ((arP + arO) * i) + width;
            this.arY[i][1] = arM + arN + this.tI.getHeight() + arO;
        }
    }

    public int wy() {
        return this.x;
    }

    public int wz() {
        return this.y;
    }
}
