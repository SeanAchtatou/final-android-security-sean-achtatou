package com.uc.e;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import java.util.Iterator;
import java.util.Vector;

public class c extends s {
    private int eC = 0;
    private int eD = 0;
    protected Drawable[] eE = new Drawable[3];
    protected Drawable[] eF = new Drawable[3];
    protected Drawable[] eG = new Drawable[3];
    private boolean eH;
    private int eI = 60;

    public void C(int i) {
        this.eI = i;
    }

    public void a(z zVar) {
        super.a(zVar);
        zVar.setSize(this.eC, this.eD);
    }

    public void b(z zVar) {
        if (zVar != null) {
            if (this.vj == null) {
                this.vj = new Vector();
            }
            if (this.eH) {
                this.vj.remove(0);
            }
            this.vj.insertElementAt(zVar, 0);
            zVar.h(this);
            this.eH = true;
        }
    }

    public void b(Drawable[] drawableArr) {
        for (int i = 0; i < 3; i++) {
            if (drawableArr[i] != null) {
                drawableArr[i].setBounds(0, 0, this.eC, this.eD);
            }
        }
        this.eE = drawableArr;
    }

    public void c(Drawable[] drawableArr) {
        for (int i = 0; i < 3; i++) {
            if (drawableArr[i] != null) {
                drawableArr[i].setBounds(0, 0, this.eC, this.eD);
            }
        }
        this.eF = drawableArr;
    }

    public Drawable[] cF() {
        return this.eE;
    }

    public Drawable[] cG() {
        return this.eF;
    }

    public Drawable[] cH() {
        return this.eG;
    }

    public int cI() {
        return this.eI;
    }

    public void d(Drawable[] drawableArr) {
        for (int i = 0; i < 3; i++) {
            if (drawableArr[i] != null) {
                drawableArr[i].setBounds(0, 0, this.eC, this.eD);
            }
        }
        this.eG = drawableArr;
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        int i;
        int i2;
        boolean z;
        canvas.translate((float) this.paddingLeft, 0.0f);
        if (this.vj != null && this.vj.size() > 0) {
            int size = this.vj.size();
            int i3 = this.aBk;
            if (this.eH) {
                z zVar = (z) this.vj.elementAt(0);
                if (zVar.getHeight() + i3 > 0) {
                    canvas.save();
                    canvas.translate(0.0f, (float) i3);
                    zVar.onDraw(canvas);
                    canvas.restore();
                }
                i3 = zVar.getHeight() + i3;
                i = 1;
            } else {
                i = 0;
            }
            int i4 = i3;
            boolean z2 = true;
            int i5 = i;
            while (i5 < size) {
                b bVar = (b) this.vj.get(i5);
                if (bVar.getHeight() + i4 < 0) {
                    i2 = bVar.getHeight() + i4;
                    z = false;
                } else if (i4 <= this.aSQ) {
                    if (z2) {
                        bVar.a(this.eE);
                    } else if (i5 != size - 1 || !bVar.dx) {
                        bVar.a(this.eG);
                    } else {
                        bVar.a(this.eF);
                    }
                    canvas.save();
                    canvas.translate(0.0f, (float) i4);
                    bVar.onDraw(canvas);
                    canvas.restore();
                    int height = i4 + bVar.getHeight();
                    if (!bVar.dx) {
                        i2 = height;
                        z = true;
                    } else {
                        i2 = height;
                        z = false;
                    }
                } else {
                    return;
                }
                i5++;
                z2 = z;
                i4 = i2;
            }
        }
    }

    public void l(int i, int i2) {
        int i3 = i >= this.aSP ? (this.aSP - this.paddingLeft) - this.paddingRight : i;
        this.eC = i3;
        this.eD = i2;
        for (Drawable drawable : this.eE) {
            if (drawable != null) {
                drawable.setBounds(0, 0, this.eC, this.eD);
            }
        }
        for (Drawable drawable2 : this.eF) {
            if (drawable2 != null) {
                drawable2.setBounds(0, 0, this.eC, this.eD);
            }
        }
        for (Drawable drawable3 : this.eG) {
            if (drawable3 != null) {
                drawable3.setBounds(0, 0, this.eC, this.eD);
            }
        }
        if (this.vj != null) {
            Iterator it = this.vj.iterator();
            while (it.hasNext()) {
                ((z) it.next()).setSize(i3, i2);
            }
            if (this.eH) {
                ((z) this.vj.elementAt(0)).setSize(i3, this.eI);
            }
        }
    }

    public void setSize(int i, int i2) {
        super.setSize(i, i2);
        if (this.eH) {
            ((z) this.vj.elementAt(0)).setSize(this.eC, this.eI);
        }
    }
}
