package com.uc.e;

import android.graphics.drawable.Drawable;

public class t {
    private int aGf = 0;
    private Drawable aGg;
    private String aGh;
    private Drawable[] aGi;
    private boolean aGj = true;
    private int aGk;
    private int aGl;

    public void H(boolean z) {
        this.aGj = z;
    }

    public void as(int i, int i2) {
        this.aGg.setBounds(0, 0, i, i2);
    }

    public void bE(int i) {
        this.aGf = i;
    }

    public void fy(int i) {
        this.aGk = i;
    }

    public void fz(int i) {
        this.aGl = i;
    }

    public void g(Drawable[] drawableArr) {
        this.aGi = drawableArr;
    }

    public Drawable getIcon() {
        return this.aGg;
    }

    public int getItemId() {
        return this.aGf;
    }

    public int getPosition() {
        return this.aGk;
    }

    public String getText() {
        return this.aGh;
    }

    public void setIcon(Drawable drawable) {
        this.aGg = drawable;
        this.aGg.setBounds(0, 0, this.aGg.getIntrinsicWidth(), this.aGg.getIntrinsicHeight());
    }

    public void setText(String str) {
        this.aGh = str;
    }

    public Drawable[] zO() {
        return this.aGi;
    }

    public boolean zP() {
        return this.aGj;
    }

    public int zQ() {
        return this.aGl;
    }
}
