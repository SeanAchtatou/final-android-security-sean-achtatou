package com.uc.e.a;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.uc.browser.ActivityEditMyNavi;
import com.uc.e.z;

public class d extends z {
    private static Bitmap XL;
    private static Paint XQ;
    private static int XY;
    private static int XZ;
    private static int Ya;
    private static int Yb;
    private static int Yc;
    private static int Yd = 0;
    private static Drawable Ye;
    private static int dw;
    private Bitmap XK;
    private Drawable XM;
    private String XN = "subtitle";
    private String XO = "discribe";
    private String XP = null;
    private int XR = -1;
    private int XS = -1;
    private int XT;
    private int XU;
    private int XV;
    private int XW = 14;
    private int XX = 20;
    private String bs = ActivityEditMyNavi.bvj;
    private int dv = 24;

    public static void at(int i, int i2) {
        dw = i;
        XY = i2;
    }

    public static void au(int i, int i2) {
        XZ = i;
        Ya = i2;
    }

    public static void av(int i, int i2) {
        Yb = i;
        Yc = i2;
    }

    public static void ey(int i) {
        Yd = i;
    }

    public static void l(Drawable drawable) {
        Ye = drawable;
    }

    /* access modifiers changed from: protected */
    public boolean ar(int i, int i2) {
        return true;
    }

    public void as(int i, int i2) {
        this.XR = i2;
        this.XS = i;
        if (this.XK != null) {
            this.XS = this.XS < 0 ? -this.XK.getWidth() : this.XS;
            this.XR = this.XR < 0 ? -this.XK.getHeight() : this.XR;
        }
        if (this.XM == null && this.XK != null) {
            this.XM = new BitmapDrawable(this.XK);
        }
        if (this.XM != null) {
            this.XM.setBounds(0, 0, Math.abs(this.XS), Math.abs(this.XR));
        }
    }

    public void c(Bitmap bitmap) {
        this.XK = bitmap;
        if (this.XK != null) {
            this.XS = this.XS < 0 ? -this.XK.getWidth() : this.XS;
            this.XR = this.XR < 0 ? -this.XK.getHeight() : this.XR;
            this.XM = new BitmapDrawable(this.XK);
            this.XM.setBounds(0, 0, Math.abs(this.XS), Math.abs(this.XR));
        }
    }

    public void co(String str) {
        this.XN = str;
    }

    public void cp(String str) {
        this.XO = str;
        if (this.XO != null) {
            if (this.pL == null) {
                this.pL = new Paint();
            }
            this.pL.setTextSize((float) this.XX);
            int width = (getWidth() - this.paddingLeft) - this.XV;
            if (width <= 0 || this.pL.measureText(this.XO) <= ((float) width)) {
                this.XP = null;
                return;
            }
            if (this.XP == null) {
                this.XP = this.XO.substring(0, this.XO.length() - 1);
            }
            while (this.XP.length() > 2 && this.pL.measureText(this.XP) > ((float) width)) {
                this.XP = this.XP.substring(0, this.XP.length() - 1);
            }
            this.XP = this.XP.substring(0, this.XP.length() - 1) + "...";
        }
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        canvas.translate((float) this.paddingLeft, (float) this.paddingTop);
        if (this.pL == null) {
            this.pL = new Paint();
            this.pL.setAntiAlias(true);
        }
        if (XQ == null) {
            XQ = new Paint();
            XQ.setAntiAlias(true);
            XQ.setTypeface(Typeface.DEFAULT_BOLD);
            XQ.setFakeBoldText(true);
        }
        if (this.XM != null) {
            this.XM.draw(canvas);
            if (Yd != 0) {
                if (XL == null && this.XK != null) {
                    XL = this.XK.extractAlpha();
                }
                this.pL.setColor(Yd);
                canvas.drawBitmap(XL, this.XM.getBounds(), this.XM.getBounds(), this.pL);
            } else if (XL != null) {
                XL.recycle();
                XL = null;
            }
        }
        if (Iz() == 0) {
            XQ.setColor(dw);
            XQ.setTextSize((float) this.dv);
            canvas.drawText(this.bs, (float) (Math.abs(this.XS) + this.XT), (((float) Math.abs(this.XR)) - this.pL.ascent()) / 2.0f, XQ);
            this.pL.setColor(Yb);
            this.pL.setTextSize((float) this.XX);
            canvas.drawText(this.XP == null ? this.XO : this.XP, 0.0f, (float) ((getHeight() - this.paddingTop) - this.paddingBottom), this.pL);
        } else {
            XQ.setColor(XY);
            XQ.setTextSize((float) this.dv);
            canvas.drawText(this.bs, (float) (Math.abs(this.XS) + this.XT), (((float) Math.abs(this.XR)) - this.pL.ascent()) / 2.0f, XQ);
            this.pL.setColor(Yc);
            this.pL.setTextSize((float) this.XX);
            canvas.drawText(this.XP == null ? this.XO : this.XP, 0.0f, (float) ((getHeight() - this.paddingTop) - this.paddingBottom), this.pL);
        }
        if (Ye != null) {
            canvas.translate((float) ((getWidth() - this.paddingRight) - Ye.getIntrinsicWidth()), (float) (((getHeight() - Ye.getIntrinsicHeight()) / 2) - this.paddingTop));
            Ye.setBounds(0, 0, Ye.getIntrinsicWidth(), Ye.getIntrinsicHeight());
            Ye.draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void eL() {
        super.eL();
        if (this.XO != null) {
            if (this.pL == null) {
                this.pL = new Paint();
            }
            this.pL.setTextSize((float) this.XX);
            int width = ((getWidth() - this.paddingLeft) - this.XV) - (Ye == null ? 0 : Ye.getIntrinsicWidth());
            if (width <= 0 || this.pL.measureText(this.XO) <= ((float) width)) {
                this.XP = null;
                return;
            }
            if (this.XP == null) {
                this.XP = this.XO.substring(0, this.XO.length() - 2);
            }
            while (this.XP.length() > 2 && this.pL.measureText(this.XP) > ((float) width)) {
                this.XP = this.XP.substring(0, this.XP.length() - 1);
            }
            this.XP = this.XP.substring(0, this.XP.length() - 1) + "...";
        }
    }

    public void eu(int i) {
        this.XT = i;
    }

    public void ev(int i) {
        this.XU = i;
    }

    public void ew(int i) {
        this.XW = i;
    }

    public void ex(int i) {
        this.XX = i;
    }

    public void t(String str) {
        this.bs = str;
    }

    public void y(int i) {
        this.dv = i;
    }
}
