package com.uc.e.a;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.view.KeyEvent;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import com.uc.browser.ModelBrowser;
import com.uc.browser.R;
import com.uc.e.u;
import com.uc.h.a;
import java.util.Vector;

public class e extends u implements a {
    private static final int abj = 4;
    private static final long abk = 0;
    private static final long abl = 200;
    private static final String tag = "multi window";
    private boolean abA = true;
    long abB = abk;
    private n abC;
    private int abm;
    private AnimationSet[] abn;
    private AnimationSet[] abo;
    private int[] abp;
    private int[] abq;
    private Transformation[] abr;
    private int[] abs = null;
    private int[] abt = null;
    private int abu;
    private int abv;
    private int abw;
    private int abx;
    private boolean aby = false;
    private int[] abz;
    private int dz;
    private int index = -1;
    com.uc.h.e oQ = com.uc.h.e.Pb();
    private int vr;

    public e() {
        com.uc.h.e.Pb().a(this);
    }

    public e(Vector vector) {
        com.uc.h.e.Pb().a(this);
        c(vector);
    }

    private boolean uA() {
        long currentTimeMillis = System.currentTimeMillis();
        boolean z = false;
        for (int i = 0; i < this.abw; i++) {
            if (this.abo[i] != null) {
                if (this.abr[i] == null) {
                    this.abr[i] = new Transformation();
                }
                if (!this.abo[i].getTransformation(currentTimeMillis, this.abr[i])) {
                    this.abo[i] = null;
                } else {
                    z = true;
                }
                float[] fArr = new float[9];
                this.abr[i].getMatrix().getValues(fArr);
                this.abp[i] = (int) fArr[2];
                this.abq[i] = (int) fArr[5];
                if (i == this.vr) {
                    this.abu = this.abp[i];
                    this.abv = this.abq[i];
                }
            }
        }
        return z;
    }

    private boolean ux() {
        if (this.abw > 4) {
            return false;
        }
        if (uy()) {
            return true;
        }
        return uA();
    }

    private boolean uy() {
        long currentTimeMillis = System.currentTimeMillis();
        boolean z = false;
        boolean z2 = false;
        for (int i = 0; i < this.abw; i++) {
            if (this.abn[i] != null) {
                if (this.abr[i] == null) {
                    this.abr[i] = new Transformation();
                }
                if (!this.abn[i].getTransformation(currentTimeMillis, this.abr[i])) {
                    this.vj.remove(i);
                    this.vj.insertElementAt(null, i);
                    this.abn[i] = null;
                    if (i == this.vr) {
                        boolean z3 = true;
                        for (int i2 = this.vr; i2 < this.abw; i2++) {
                            if (this.vj.get(i2) != null) {
                                z3 = false;
                            }
                        }
                        if (z3) {
                            for (int i3 = 0; i3 < i; i3++) {
                                if (this.vj.get(i3) != null) {
                                    this.vr = i3;
                                }
                            }
                        } else {
                            int i4 = i;
                            while (true) {
                                if (i4 >= this.abw) {
                                    break;
                                } else if (this.vj.get(i4) != null) {
                                    this.vr = i4;
                                    break;
                                } else {
                                    i4++;
                                }
                            }
                        }
                    }
                    z = true;
                } else {
                    z2 = true;
                }
                float[] fArr = new float[9];
                this.abr[i].getMatrix().getValues(fArr);
                this.abp[i] = (int) fArr[2];
                this.abq[i] = (int) fArr[5];
            }
        }
        if (!z2 && z) {
            uz();
        }
        return z2;
    }

    private boolean uz() {
        int i;
        boolean z;
        boolean z2;
        int i2 = 0;
        int i3 = 0;
        boolean z3 = false;
        while (i2 < this.abw) {
            if (((i) this.vj.get(i2)) != null) {
                if (i2 > this.index) {
                    int[] iArr = this.abz;
                    iArr[i2] = iArr[i2] - 1;
                }
                int i4 = this.abs[i3];
                int i5 = this.abt[i3];
                if (this.abp[i2] == i4 && this.abq[i2] == i5) {
                    z2 = z3;
                } else {
                    AnimationSet animationSet = new AnimationSet(true);
                    TranslateAnimation translateAnimation = new TranslateAnimation((float) this.abp[i2], (float) i4, (float) this.abq[i2], (float) i5);
                    translateAnimation.initialize(0, 0, 0, 0);
                    translateAnimation.setDuration(abl);
                    animationSet.addAnimation(translateAnimation);
                    this.abo[i2] = animationSet;
                    this.abo[i2].start();
                    z2 = true;
                }
                int i6 = i3 + 1;
                z = z2;
                i = i6;
            } else {
                i = i3;
                z = z3;
            }
            i2++;
            z3 = z;
            i3 = i;
        }
        return z3;
    }

    public void a(i iVar) {
        if (Math.abs(System.currentTimeMillis() - this.abB) >= 500) {
            this.abB = System.currentTimeMillis();
            this.index = this.vj == null ? -1 : this.vj.indexOf(iVar);
            if (this.index >= 0) {
                eD(this.index);
                this.abx--;
            }
            int i = 0;
            for (int i2 = 0; i2 < this.index; i2++) {
                if (this.vj.get(i2) == null) {
                    i++;
                }
            }
            this.index -= i;
            int wh = ModelBrowser.gD().hj().wh();
            if (this.index >= wh) {
                if (this.abC != null) {
                    this.abC.eD(wh - 1);
                }
            } else if (this.index >= 0 && this.abC != null) {
                this.abC.eD(this.index);
            }
        }
    }

    public void a(n nVar) {
        this.abC = nVar;
    }

    public boolean a(KeyEvent keyEvent) {
        boolean z;
        int i;
        boolean z2;
        int i2;
        if (keyEvent.getAction() == 0) {
            keyEvent.getKeyCode();
            return false;
        }
        if (keyEvent.getAction() == 1) {
            switch (keyEvent.getKeyCode()) {
                case 19:
                case 21:
                    int i3 = this.vr - 1;
                    while (true) {
                        if (i3 < 0) {
                            z = true;
                            i = 0;
                        } else if (this.vj.get(i3) != null) {
                            i = i3;
                            z = false;
                        } else {
                            i3--;
                        }
                    }
                    if (!z) {
                        this.vr = i;
                        this.aby = true;
                        Ix();
                    }
                    return true;
                case 20:
                case 22:
                    int i4 = this.vr;
                    while (true) {
                        i4++;
                        if (i4 >= this.abw) {
                            z2 = true;
                            i2 = 0;
                        } else if (this.vj.get(i4) != null) {
                            i2 = i4;
                            z2 = false;
                        }
                    }
                    if (!z2) {
                        this.vr = i2;
                        this.aby = true;
                        Ix();
                    }
                    return true;
                case 23:
                case 66:
                    int wh = ModelBrowser.gD().hj().wh();
                    if (this.vr >= wh) {
                        if (this.abC != null) {
                            this.abC.eP(wh - 1);
                        }
                    } else if (this.abC != null) {
                        this.abC.eP(this.vr);
                    }
                    return true;
            }
        }
        return false;
    }

    public void b(i iVar) {
        int i = 0;
        this.index = this.vj == null ? -1 : this.vj.indexOf(iVar);
        for (int i2 = 0; i2 < this.index; i2++) {
            if (this.vj.get(i2) == null) {
                i++;
            }
        }
        this.index -= i;
        int wh = ModelBrowser.gD().hj().wh();
        if (this.index >= wh) {
            if (this.abC != null) {
                this.abC.eP(wh - 1);
            }
        } else if (this.index >= 0 && this.abC != null) {
            this.abC.eP(this.index);
        }
    }

    public void c(Vector vector) {
        super.c(vector);
        this.abw = this.vj == null ? 0 : this.vj.size();
        this.abx = this.abw;
        if (this.abw > 0) {
            this.abz = new int[this.abw];
            for (int i = 0; i < this.abw; i++) {
                this.abz[i] = i;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        int size;
        if (this.vj != null && this.vj.size() != 0 && (size = this.vj.size()) <= 4) {
            if (this.aby) {
                this.abu = this.abs[this.abz[this.vr]];
                this.abv = this.abt[this.abz[this.vr]];
                this.aby = false;
            }
            for (int i = 0; i < size; i++) {
                i iVar = (i) this.vj.get(i);
                if (iVar != null) {
                    if (this.abr[i] != null) {
                        canvas.save();
                        Matrix matrix = canvas.getMatrix();
                        this.abr[i].getMatrix().getValues(new float[9]);
                        matrix.postConcat(this.abr[i].getMatrix());
                        canvas.setMatrix(matrix);
                        iVar.setAlpha((int) (this.abr[i].getAlpha() * 255.0f));
                    } else {
                        canvas.save();
                        canvas.translate((float) this.abp[i], (float) this.abq[i]);
                    }
                    if (this.abx == 1) {
                        iVar.aZ(true);
                    }
                    if (i == this.vr) {
                        iVar.ba(true);
                    }
                    iVar.draw(canvas);
                    canvas.restore();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public int e(int i, int i2, int[] iArr) {
        if (this.abw <= 4) {
            for (int i3 = 0; i3 < this.abw; i3++) {
                if (this.vj.get(i3) != null) {
                    int i4 = i - this.abp[i3];
                    int i5 = i2 - this.abq[i3];
                    if (i4 > 0 && i4 < this.abm && i5 > 0 && i5 < this.dz) {
                        if (iArr == null) {
                            return i3;
                        }
                        iArr[0] = this.abp[i3];
                        iArr[1] = this.abq[i3];
                        return i3;
                    }
                }
            }
        }
        return -1;
    }

    public void e(Canvas canvas) {
    }

    public void eD(int i) {
        if ((this.vj == null ? 0 : this.vj.size()) > 4) {
            this.vj.remove(i);
            this.vj.insertElementAt(null, i);
            Ix();
        } else if (this.abn[i] == null) {
            AnimationSet animationSet = new AnimationSet(true);
            animationSet.setInterpolator(new DecelerateInterpolator());
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(abk);
            animationSet.addAnimation(alphaAnimation);
            TranslateAnimation translateAnimation = new TranslateAnimation((float) this.abp[i], (float) this.abp[i], (float) this.abq[i], (float) this.abq[i]);
            translateAnimation.initialize(this.abm, this.dz, this.aSP, this.aSQ);
            translateAnimation.setDuration(abk);
            animationSet.addAnimation(translateAnimation);
            this.abn[i] = animationSet;
            this.abn[i].start();
            Ix();
        }
    }

    public void eE(int i) {
        this.vr = i;
        this.bdt = i;
    }

    /* access modifiers changed from: protected */
    public void eL() {
        int width;
        int i;
        int width2;
        int i2;
        int height;
        int i3;
        int i4;
        int i5;
        super.eL();
        this.abm = com.uc.h.e.Pb().kp(R.dimen.f451mutiwindow_item_width);
        this.dz = com.uc.h.e.Pb().kp(R.dimen.f452mutiwindow_item_height);
        if (!this.oQ.Pc()) {
            switch (this.abw) {
                case 1:
                case 2:
                    int width3 = (getWidth() - (this.abm * this.abw)) / (this.abw + 1);
                    int width4 = (getWidth() - ((getWidth() - (this.abm * this.abw)) / 3)) - this.abm;
                    width = width3;
                    i = 0;
                    width2 = 0;
                    i2 = (getHeight() - this.dz) / 2;
                    height = (getHeight() - this.dz) / 2;
                    i4 = 0;
                    i5 = width4;
                    i3 = 0;
                    break;
                case 3:
                case 4:
                    int width5 = (getWidth() - ((getWidth() - (this.abm * 2)) / 3)) - this.abm;
                    int width6 = (getWidth() - ((getWidth() - (this.abm * 2)) / 3)) - this.abm;
                    int height2 = (getHeight() - ((getHeight() - (this.dz * 2)) / 3)) - this.dz;
                    int height3 = (getHeight() - ((getHeight() - (this.dz * 2)) / 3)) - this.dz;
                    width = (getWidth() - (this.abm * 2)) / 3;
                    i = height3;
                    int height4 = (getHeight() - (this.dz * 2)) / 3;
                    width2 = (getWidth() - (this.abm * 2)) / 3;
                    i2 = height4;
                    int i6 = width6;
                    height = (getHeight() - (this.dz * 2)) / 3;
                    i3 = i6;
                    int i7 = width5;
                    i4 = height2;
                    i5 = i7;
                    break;
                default:
                    width = 0;
                    i = 0;
                    width2 = 0;
                    i2 = 0;
                    height = 0;
                    i3 = 0;
                    i4 = 0;
                    i5 = 0;
                    break;
            }
        } else {
            int width7 = (getWidth() - (this.abm * this.abw)) / (this.abw + 1);
            int i8 = (width7 * 2) + this.abm;
            int i9 = (width7 * 3) + (this.abm * 2);
            int width8 = (getWidth() - width7) - this.abm;
            int height5 = (getHeight() - this.dz) / 2;
            width2 = i9;
            i5 = i8;
            width = width7;
            i = height5;
            i4 = height5;
            i2 = height5;
            int i10 = width8;
            height = height5;
            i3 = i10;
        }
        this.abs = new int[]{width, i5, width2, i3};
        this.abt = new int[]{height, i2, i4, i};
        if (this.abw > 0 && this.abw <= 4) {
            this.abn = new AnimationSet[this.abw];
            this.abo = new AnimationSet[this.abw];
            this.abp = new int[this.abw];
            this.abq = new int[this.abw];
            this.abr = new Transformation[this.abw];
            if (this.abw <= 4) {
                for (int i11 = this.abw - 1; i11 >= 0; i11--) {
                    this.abp[i11] = this.abs[i11];
                    this.abq[i11] = this.abt[i11];
                }
                this.abu = this.abs[this.vr];
                this.abv = this.abt[this.vr];
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean es() {
        return ux();
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        com.uc.h.e.Pb().b(this);
    }

    public void k() {
        Ix();
    }
}
