package com.uc.e.a;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.MotionEvent;
import com.uc.e.ac;
import com.uc.e.h;
import com.uc.e.r;
import com.uc.e.u;
import com.uc.e.z;
import java.util.Iterator;
import java.util.Vector;

public class y extends u implements h {
    public int bXU = 46;
    public int bXV = 50;
    private r bXW;
    private aa bXX;
    private z tI;

    public y(z zVar) {
        this.tI = zVar;
        yb();
        a(zVar);
        a(this.bXW);
    }

    private void yb() {
        this.bXW = new r();
        this.bXW.c(this);
    }

    public ac A(String str, int i) {
        ac acVar = new ac(i, 0, 0);
        acVar.setText(str);
        this.bXW.b(acVar);
        return acVar;
    }

    public void F(byte b2) {
        this.bXW.v(b2);
    }

    public void M(Drawable drawable) {
        this.bXW.m(drawable);
    }

    public void OO() {
        this.bXW.clear();
    }

    public void OP() {
        this.bXW.yi();
    }

    public void a(aa aaVar) {
        this.bXX = aaVar;
    }

    public void b(z zVar, int i) {
        if (zVar == this.bXW && this.bXX != null) {
            this.bXX.bA(i);
        }
    }

    public boolean b(byte b2, int i, int i2) {
        if (i2 < this.tI.getHeight()) {
            return this.tI.b(b2, i, i2);
        }
        return this.bXW.d(MotionEvent.obtain(0, 0, b2, (float) i, (float) (i2 - this.tI.getHeight()), 0));
    }

    public void bA(int i, int i2) {
        Vector xa = this.bXW.xa();
        if (xa != null) {
            Iterator it = xa.iterator();
            while (it.hasNext()) {
                ac acVar = (ac) it.next();
                acVar.setTextColor(i);
                acVar.ks(i2);
            }
        }
    }

    public void bz(int i, int i2) {
        this.bXW.aw(i, i2);
    }

    public boolean c(KeyEvent keyEvent) {
        return this.tI.c(keyEvent);
    }

    public void draw(Canvas canvas) {
        e(canvas);
        canvas.save();
        canvas.clipRect(0, 0, this.tI.getWidth(), this.tI.getHeight());
        this.tI.onDraw(canvas);
        canvas.restore();
        canvas.save();
        canvas.translate(0.0f, (float) this.tI.getHeight());
        canvas.clipRect(0, 0, getWidth(), this.bXU);
        this.bXW.onDraw(canvas);
        canvas.restore();
    }

    public void ki(int i) {
        this.bXU = i;
    }

    public void n(Drawable drawable) {
        this.bXW.s(drawable);
    }

    public void setSize(int i, int i2) {
        super.setSize(i, i2);
        this.tI.setSize(i, i2 - this.bXU);
        this.bXW.setSize(i, this.bXU);
    }
}
