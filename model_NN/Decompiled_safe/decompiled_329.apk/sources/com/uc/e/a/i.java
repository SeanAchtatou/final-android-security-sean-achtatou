package com.uc.e.a;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.uc.browser.ActivityBrowser;
import com.uc.browser.R;
import com.uc.browser.UCR;
import com.uc.e.b.m;
import com.uc.e.z;
import com.uc.h.e;

public class i extends z {
    public static final int aBD = 1;
    public static final int aBE = 2;
    private Paint aBF = new Paint(1);
    private int aBG = 255;
    private String aBH;
    private boolean aBI = false;
    private boolean aBJ = false;
    private int aBK;
    private int aBL;
    private int aBM;
    private int aBN;
    private int aBO;
    private int aBP;
    private Bitmap aBQ;
    private Bitmap aBR;
    private Bitmap aBS;
    private Drawable aBT;
    private Drawable aBU;
    private boolean aBV = false;
    private int abm;
    private int dz;
    private e oQ = e.Pb();
    private int textSize;

    public i() {
        this.bBg = true;
        this.textSize = this.oQ.kp(R.dimen.f175multiwindow_textsize);
        this.abm = this.oQ.kp(R.dimen.f451mutiwindow_item_width);
        this.dz = this.oQ.kp(R.dimen.f452mutiwindow_item_height);
        this.aBL = this.abm;
        this.aBR = this.oQ.kl(UCR.drawable.aUb);
        this.aBS = this.oQ.kl(UCR.drawable.aUc);
        this.aBU = this.oQ.getDrawable(UCR.drawable.aUG);
        this.aBT = this.oQ.getDrawable(UCR.drawable.aUF);
        this.aBM = this.oQ.kp(R.dimen.f453multiwindow_snapshot_width);
        this.aBN = this.oQ.kp(R.dimen.f454multiwindow_snapshot_height);
        this.aBO = this.oQ.kp(R.dimen.f455multiwindow_frame_width);
        this.aBP = this.oQ.kp(R.dimen.f456multiwindow_frame_height);
        this.aBU.setBounds(0, (this.textSize * 3) / 2, this.aBO, ((this.textSize * 3) / 2) + this.aBP);
        this.aBT.setBounds(0, (this.textSize * 3) / 2, this.aBO, ((this.textSize * 3) / 2) + this.aBP);
        Rect rect = new Rect();
        this.aBT.getPadding(rect);
        this.aBK = rect.top;
    }

    private Bitmap d(Bitmap bitmap) {
        if (bitmap == null || bitmap.isRecycled()) {
            Bitmap createBitmap = Bitmap.createBitmap(this.aBM, this.aBN, Bitmap.Config.ARGB_4444);
            Canvas canvas = new Canvas(createBitmap);
            Paint paint = new Paint();
            paint.setColor(-10132123);
            paint.setTextSize(18.0f);
            canvas.drawColor(-1);
            canvas.drawText("内存过低", (float) ((this.aBM - ((int) paint.measureText("内存过低"))) / 2), (float) (this.aBN / 2), paint);
            canvas.drawText("无法获取缩略图", (float) ((this.aBM - ((int) paint.measureText("无法获取缩略图"))) / 2), (float) ((this.aBN / 2) + 23), paint);
            return createBitmap;
        } else if (bitmap.getWidth() == this.aBM && bitmap.getHeight() == this.aBN) {
            return bitmap;
        } else {
            Canvas canvas2 = new Canvas();
            Bitmap createBitmap2 = Bitmap.createBitmap(this.aBM, this.aBN, Bitmap.Config.RGB_565);
            canvas2.setBitmap(createBitmap2);
            float width = ((float) this.aBM) / ((float) bitmap.getWidth());
            canvas2.scale(width, width);
            canvas2.drawBitmap(bitmap, 0.0f, 0.0f, new Paint());
            if (!bitmap.isRecycled()) {
                bitmap.recycle();
            }
            return createBitmap2;
        }
    }

    private String df(String str) {
        int breakText = this.aBF.breakText(str, true, (float) this.aBL, null);
        if (str.length() <= breakText) {
            return str;
        }
        char[] cArr = new char[breakText];
        str.getChars(0, breakText, cArr, 0);
        return String.valueOf(cArr) + "...";
    }

    public void aZ(boolean z) {
        this.aBJ = z;
    }

    public boolean b(byte b2, int i, int i2) {
        switch (b2) {
            case 0:
                if (n(i, i2) == 2) {
                    this.aBV = true;
                    Ix();
                    break;
                }
                break;
            case 1:
                this.aBV = false;
                break;
            case 2:
                if (this.aBV && n(i, i2) != 2) {
                    this.aBV = false;
                    Ix();
                    break;
                }
        }
        boolean b3 = super.b(b2, i, i2);
        if (!b3 && this.aBV) {
            this.aBV = false;
            Ix();
        }
        return b3;
    }

    public void ba(boolean z) {
        this.aBI = z;
    }

    public void dg(String str) {
        this.aBH = str;
    }

    public void draw(Canvas canvas) {
        this.aBF.setColor(e.Pb().getColor(61));
        this.aBF.setTextSize((float) this.textSize);
        this.aBF.setAlpha(this.aBG);
        float measureText = this.aBF.measureText(this.aBH);
        if (measureText >= ((float) this.aBL)) {
            String df = df(this.aBH);
            canvas.drawText(df, (((float) this.abm) - this.aBF.measureText(df)) / 2.0f, (float) this.textSize, this.aBF);
        } else {
            canvas.drawText(this.aBH, (((float) this.abm) - measureText) / 2.0f, (float) this.textSize, this.aBF);
        }
        int i = this.textSize;
        canvas.save();
        canvas.translate((float) ((this.abm - this.aBM) / 2), (float) (((i * 3) / 2) + this.aBK));
        canvas.clipRect(0, 0, this.aBM, this.aBN);
        if (this.aBQ == null || this.aBQ.isRecycled()) {
            int i2 = ActivityBrowser.Fz() ? -16777216 : -1;
            canvas.drawColor(i2);
            m mVar = new m(i2);
            int i3 = (this.aBM > this.aBN ? this.aBN : this.aBM) / 3;
            mVar.setBounds(this.aBM / 3, (this.aBN - i3) / 2, (this.aBM / 3) + i3, (i3 + this.aBN) / 2);
            mVar.draw(canvas);
        } else {
            if (this.aBQ.getWidth() != this.aBM) {
                float width = ((float) this.aBM) / ((float) this.aBQ.getWidth());
                float height = ((float) this.aBN) / ((float) this.aBQ.getHeight());
                if (width <= height) {
                    width = height;
                }
                canvas.scale(width, width);
            }
            canvas.drawBitmap(this.aBQ, 0.0f, 0.0f, this.aBF);
        }
        canvas.restore();
        if (this.aBI) {
            this.aBU.draw(canvas);
            this.aBI = false;
        } else {
            this.aBT.draw(canvas);
        }
        if (!this.aBJ) {
            Bitmap bitmap = this.aBV ? this.aBS : this.aBR;
            canvas.drawBitmap(bitmap, (float) ((this.abm - bitmap.getWidth()) / 2), (float) (this.dz - bitmap.getHeight()), this.aBF);
        }
    }

    public void e(Bitmap bitmap) {
        this.aBQ = bitmap;
    }

    /* access modifiers changed from: protected */
    public boolean k(int i, int i2) {
        if (this.aBJ) {
            ((e) this.bBm).b(this);
            return true;
        } else if (n(i, i2) == 2) {
            ((e) this.bBm).a(this);
            return true;
        } else if (n(i, i2) != 1) {
            return false;
        } else {
            ((e) this.bBm).b(this);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public int n(int i, int i2) {
        return (i <= (this.abm - this.aBR.getWidth()) / 2 || i >= this.abm - ((this.abm - this.aBR.getWidth()) / 2) || i2 <= this.dz - this.aBR.getHeight()) ? 1 : 2;
    }

    public void setAlpha(int i) {
        this.aBG = i;
    }

    public String yC() {
        return this.aBH;
    }
}
