package com.uc.e.a;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.uc.browser.UCR;
import com.uc.e.z;
import com.uc.h.e;

public class g extends z {
    public static final int apV = 0;
    public static final int apW = 1;
    private int Jp;
    private Drawable Yt;
    private boolean aO;
    private String aU;
    private int apX;
    private Drawable apY;

    public g(String str, boolean z) {
        this(str, z, 0);
    }

    public g(String str, boolean z, int i) {
        this.Jp = 10;
        this.apY = e.Pb().getDrawable(UCR.drawable.checkbox);
        this.Yt = e.Pb().getDrawable(UCR.drawable.aTZ);
        this.aU = str;
        this.aO = z;
        this.pL.setTextSize(16.0f);
        bL(true);
    }

    public void aV(int i) {
        this.pL.setTextSize((float) i);
    }

    public void draw(Canvas canvas) {
        if (this.apX == 0) {
            canvas.drawText(this.aU, (float) this.Jp, ((((float) getHeight()) - this.pL.descent()) - this.pL.ascent()) / 2.0f, this.pL);
            Rect rect = new Rect((getWidth() - this.apY.getIntrinsicWidth()) - this.Jp, (getHeight() - this.apY.getIntrinsicHeight()) / 2, getWidth() - this.Jp, (getHeight() + this.apY.getIntrinsicHeight()) / 2);
            this.apY.setBounds(rect);
            this.apY.draw(canvas);
            if (this.aO) {
                this.Yt.setBounds(rect);
                this.Yt.draw(canvas);
                return;
            }
            return;
        }
        Rect rect2 = new Rect(this.Jp, (getHeight() - this.apY.getIntrinsicHeight()) / 2, this.Jp + this.apY.getIntrinsicWidth(), (getHeight() + this.apY.getIntrinsicHeight()) / 2);
        this.apY.setBounds(rect2);
        this.apY.draw(canvas);
        if (this.aO) {
            this.Yt.setBounds(rect2);
            this.Yt.draw(canvas);
        }
        canvas.drawText(this.aU, (float) (rect2.right + this.Jp), ((((float) getHeight()) - this.pL.descent()) - this.pL.ascent()) / 2.0f, this.pL);
    }

    public boolean k(int i, int i2) {
        wm();
        return true;
    }

    public void setTextColor(int i) {
        this.pL.setColor(i);
    }

    public void wm() {
        this.aO = !this.aO;
        Ix();
    }

    public boolean wn() {
        return this.aO;
    }
}
