package com.uc.e.a;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import b.a.a.e;
import com.uc.e.z;
import java.util.Vector;

public class h extends z {
    private static final float aBB = 0.5f;
    private int Yd = 0;
    private Drawable[] aAP = new Drawable[3];
    private Rect aBA;
    private float aBc;
    private int aBd = 16;
    private int aBe = -1154388842;
    private Drawable aBf;
    private int aBg = 1;
    private int aBh = 0;
    private int aBi = 0;
    private int aBj = 20;
    private int aBk = 0;
    private int aBl = -1;
    private int aBm = -1;
    private long aBn;
    private boolean aBo = false;
    private float aBp = 0.0f;
    private Animation aBq = null;
    private int aBr = 9;
    private int aBs = 9;
    private int aBt = 5;
    private int aBu = 14;
    private Drawable aBv;
    private Drawable aBw;
    private Drawable aBx;
    private Drawable aBy;
    private Drawable aBz;
    private Vector lO;
    private int mA = -1;
    private int vk = 72;
    private int vl = 72;
    private int vm = 4;
    private int vn;
    private int vo = 15;
    private int vp = 15;
    private int vq = -1;
    private int vr = -1;

    public h() {
        this.paddingRight = 14;
        this.paddingLeft = 14;
        this.aBj = this.paddingLeft + this.paddingRight;
    }

    private void a(Canvas canvas, e eVar, int i, Paint paint) {
        float f;
        String str;
        if (this.aAP[i] != null) {
            this.aAP[i].draw(canvas);
        }
        if (eVar != null) {
            canvas.save();
            canvas.clipRect(this.aBr, this.aBt, this.vk - this.aBs, this.vl - this.aBu);
            if (this.aBA == null) {
                this.aBA = new Rect(this.aBr, this.aBt, this.vk - this.aBs, this.vl - this.aBu);
            }
            if (eVar.abN != null) {
                BitmapDrawable bitmapDrawable = new BitmapDrawable(eVar.abN);
                bitmapDrawable.setBounds(this.aBA);
                bitmapDrawable.draw(canvas);
            } else if (this.aBz != null) {
                this.aBz.setBounds(this.aBA);
                this.aBz.draw(canvas);
            }
            canvas.restore();
            if (this.Yd != 0) {
                paint.setColor(this.Yd);
                paint.setAntiAlias(false);
                canvas.drawRect(this.aBA, paint);
                paint.setAntiAlias(true);
            }
            if (this.aBf == null) {
                paint.setColor(this.aBe);
                canvas.drawRect((float) this.aBr, (float) ((this.vl - this.aBu) - this.aBd), (float) (this.vk - this.aBs), (float) (this.vl - this.aBu), paint);
            } else {
                this.aBf.draw(canvas);
            }
            paint.setColor(this.vq);
            float measureText = paint.measureText(eVar.abM);
            String str2 = eVar.abM;
            if (measureText > ((float) ((this.vk - this.aBr) - this.aBs))) {
                str = eVar.abM.substring(0, paint.breakText(str2, true, (float) ((this.vk - this.aBr) - this.aBs), null));
                f = paint.measureText(str);
            } else {
                String str3 = str2;
                f = measureText;
                str = str3;
            }
            canvas.drawText(str, (((float) ((this.vk + this.aBr) - this.aBs)) - f) / 2.0f, (((float) (this.vl - this.aBu)) - ((((float) this.aBd) - this.aBc) / 2.0f)) - 1.0f, paint);
            if (eVar.abT && this.aBy != null) {
                this.aBy.draw(canvas);
            }
        } else if (this.aBx != null) {
            this.aBx.draw(canvas);
        }
    }

    private int fb(int i) {
        return fc(i - this.aBh);
    }

    private int fc(int i) {
        int i2 = this.aBh + i;
        if (i2 < 0) {
            i2 = 0;
        }
        if (i2 >= this.aBi - 1) {
            i2 = this.aBi - 1;
        }
        this.aBh = i2;
        int i3 = -(i2 * this.aSP);
        this.aBq = new TranslateAnimation((float) this.aBk, (float) i3, 0.0f, 0.0f);
        this.aBq.initialize(0, 0, 0, 0);
        this.aBq.setDuration((long) Math.sqrt((double) Math.abs((i3 - this.aBk) * 800)));
        this.aBq.setStartTime(-1);
        Ix();
        return 0;
    }

    private int g(float f) {
        int i;
        int i2 = this.aBh;
        if (f >= 0.5f || f <= -0.5f) {
            i = this.aBh + (f > 0.0f ? -1 : 1);
        } else {
            int i3 = (-this.aBk) - (this.aBh * this.aSP);
            i = i3 > (this.aSP >> 1) ? this.aBh + 1 : i3 < (-(this.aSP >> 1)) ? this.aBh - 1 : this.aBh;
        }
        if (i < 0) {
            i = 0;
        }
        if (i >= this.aBi - 1) {
            i = this.aBi - 1;
        }
        this.aBh = i;
        this.aBq = new TranslateAnimation((float) this.aBk, (float) (-(i * this.aSP)), 0.0f, 0.0f);
        this.aBq.initialize(0, 0, 0, 0);
        this.aBq.setDuration(200);
        this.aBq.setStartTime(-1);
        Ix();
        return 0;
    }

    private boolean yr() {
        if (this.aBq == null) {
            return false;
        }
        Transformation transformation = new Transformation();
        boolean transformation2 = this.aBq.getTransformation(System.currentTimeMillis(), transformation);
        float[] fArr = new float[9];
        transformation.getMatrix().getValues(fArr);
        this.aBk = (int) fArr[2];
        if (!transformation2) {
            this.aBq = null;
        }
        return transformation2;
    }

    public void A(Drawable drawable) {
        this.aBf = drawable;
        this.aBf.setBounds(0, 0, this.vk, this.vl);
    }

    public void a(int i, int i2, int i3, int i4) {
        this.aBr = i;
        this.aBt = i2;
        this.aBs = i3;
        this.aBu = i4;
    }

    public boolean a(byte b2, int i, int i2) {
        switch (b2) {
            case 0:
                this.aBl = i;
                this.aBm = i2;
                this.aBn = System.currentTimeMillis();
                this.mA = n(i, i2);
                this.vr = -1;
                if (!(this.mA == -1 && this.vr == -1)) {
                    Ix();
                }
                this.aBp = 0.0f;
                return true;
            case 1:
                g(this.aBp);
                this.aBo = false;
                if (!(this.mA == -1 && this.vr == -1)) {
                    Ix();
                }
                this.mA = -1;
                this.vr = -1;
                return true;
            case 2:
                int i3 = i - this.aBl;
                int abs = Math.abs(i2 - this.aBm);
                this.aBp = ((float) (i - this.aBl)) / ((float) (System.currentTimeMillis() - this.aBn));
                this.aBl = i;
                this.aBm = i2;
                this.aBn = System.currentTimeMillis();
                if (this.aBo || ((double) Math.abs(i3)) * 1.3d >= ((double) abs) || abs <= 15) {
                    if (this.aBi > 1) {
                        this.aBk = i3 + this.aBk;
                        this.aBo = true;
                        Ix();
                    }
                    return true;
                }
                if (!(this.mA == -1 && this.vr == -1)) {
                    Ix();
                }
                this.mA = -1;
                this.vr = -1;
                return false;
            default:
                return true;
        }
    }

    public boolean a(KeyEvent keyEvent) {
        this.mA = -1;
        int action = keyEvent.getAction();
        int size = this.lO == null ? 0 : this.lO.size();
        if (size == 0) {
            return false;
        }
        if (action == 0) {
            int i = this.vr % this.vm;
            int i2 = (this.vr / this.vm) % this.aBg;
            switch (keyEvent.getKeyCode()) {
                case 19:
                    if (this.vr < 0) {
                        int i3 = (((this.aBh + 1) * this.vm) * this.aBg) - 1;
                        if (i3 >= size - 1) {
                            i3 = size - 1;
                        }
                        this.vr = i3;
                        Ix();
                        return true;
                    } else if (i2 > 0) {
                        this.vr -= this.vm;
                        Ix();
                        return true;
                    } else {
                        this.vr = -1;
                        Ix();
                        return false;
                    }
                case 20:
                    if (this.vr < 0) {
                        this.vr = this.aBh * this.vm * this.aBg;
                        Ix();
                        return true;
                    } else if (i2 < this.aBg - 1) {
                        this.vr += this.vr + this.vm <= size - 1 ? this.vm : 0;
                        Ix();
                        return true;
                    } else {
                        this.vr = -1;
                        Ix();
                        return false;
                    }
                case 21:
                    if (this.vr < 0) {
                        this.vr = ((this.aBh + 1) * (this.vm * this.aBg)) - 1;
                        Ix();
                    } else if (i == 0) {
                        if (this.aBh > 0) {
                            fc(-1);
                            this.vr = ((this.vr - (this.vm * this.aBg)) + this.vm) - 1;
                        }
                    } else if (i > 0) {
                        this.vr--;
                        Ix();
                    }
                    return true;
                case 22:
                    if (this.vr < 0) {
                        this.vr = this.aBh * this.vm * this.aBg;
                        Ix();
                    } else if (i == this.vm - 1) {
                        if (this.aBh < this.aBi - 1) {
                            fc(1);
                            int i4 = ((this.aBh + 1) * this.aBg * this.vm) + (i2 * this.vm);
                            while (i4 >= size) {
                                i4 -= this.vm;
                            }
                            this.vr = i4;
                            Ix();
                        }
                    } else if (i < this.vm - 1 && this.vr < size - 1) {
                        this.vr++;
                        Ix();
                    }
                    return true;
            }
        } else if (action == 1) {
            switch (keyEvent.getKeyCode()) {
                case 19:
                case 20:
                case 21:
                case 22:
                    return true;
                case 23:
                case 66:
                    if (this.vr >= 0) {
                        eZ(this.vr);
                    }
                    return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean aD(int i, int i2) {
        int n = n(i, i2);
        if (n < 0) {
            return super.aD(i, i2);
        }
        hM(n);
        g(this.aBp);
        return true;
    }

    public e aI(int i) {
        if (this.lO == null || i >= this.lO.size()) {
            return null;
        }
        return (e) this.lO.elementAt(i % this.lO.size());
    }

    public void aJ(int i) {
        this.vm = i;
    }

    public void aw(int i, int i2) {
        this.vk = i;
        this.vl = i2;
    }

    public void clearFocus() {
        if (this.vr != -1) {
            this.vr = -1;
            Ix();
        }
    }

    public void draw(Canvas canvas) {
        int i;
        int i2;
        int i3;
        super.draw(canvas);
        canvas.save();
        canvas.clipRect(this.paddingLeft, this.paddingTop, this.aSP - this.paddingRight, this.aSQ - this.paddingBottom);
        canvas.translate((float) this.aBk, (float) this.paddingTop);
        if (this.lO != null && this.lO.size() != 0) {
            int size = this.lO.size();
            int i4 = (this.vn / 2) + this.paddingLeft + this.aBk;
            int i5 = this.vo;
            canvas.translate((float) ((this.vn / 2) + this.paddingLeft), (float) this.vo);
            Paint paint = new Paint();
            paint.setTextSize((float) this.vp);
            paint.setColor(this.vq);
            this.aBc = -paint.getFontMetrics().ascent;
            paint.setAntiAlias(true);
            int i6 = i4;
            int i7 = i5;
            int i8 = 0;
            while (i8 < size) {
                int i9 = i8 % this.vm;
                int i10 = (i8 / this.vm) % this.aBg;
                if ((i8 / this.aBg) * this.vm > 0 && i10 == 0 && i9 == 0) {
                    canvas.translate((float) this.aBj, (float) ((-(this.vl + this.vo)) * (this.aBg - 1)));
                    int i11 = i7 - ((-(this.vl + this.vo)) * (this.aBg - 1));
                    i = i6 + this.aBj;
                    i2 = i11;
                } else if (i10 <= 0 || i9 != 0) {
                    i = i6;
                    i2 = i7;
                } else {
                    canvas.translate((float) ((-(this.vk + this.vn)) * this.vm), (float) (this.vl + this.vo));
                    i = i6 - ((this.vk + this.vn) * this.vm);
                    i2 = i7 + this.vl + this.vo;
                }
                if (this.vk + i < 0 || i > this.aSP) {
                    i3 = this.vk + this.vn + i;
                    canvas.translate((float) (this.vk + this.vn), 0.0f);
                } else {
                    a(canvas, (e) this.lO.get(i8), this.mA == i8 ? 2 : this.vr == i8 ? 1 : 0, paint);
                    canvas.translate((float) (this.vk + this.vn), 0.0f);
                    i3 = this.vk + this.vn + i;
                }
                i8++;
                int i12 = i2;
                i6 = i3;
                i7 = i12;
            }
            canvas.restore();
            if (this.aBh > 0 && this.aBv != null) {
                canvas.save();
                int intrinsicWidth = this.aBv.getIntrinsicWidth();
                int intrinsicHeight = this.aBv.getIntrinsicHeight();
                this.aBv.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
                canvas.translate((float) ((this.paddingLeft - intrinsicWidth) / 2), (float) ((this.aSQ - intrinsicHeight) / 2));
                this.aBv.draw(canvas);
                canvas.restore();
            }
            if (this.aBh + 1 < (size + ((this.vm * this.aBg) - 1)) / (this.vm * this.aBg) && this.aBw != null) {
                canvas.save();
                int intrinsicWidth2 = this.aBw.getIntrinsicWidth();
                int intrinsicHeight2 = this.aBw.getIntrinsicHeight();
                this.aBw.setBounds(0, 0, intrinsicWidth2, intrinsicHeight2);
                canvas.translate((float) (this.aSP - ((intrinsicWidth2 + this.paddingRight) / 2)), (float) ((this.aSQ - intrinsicHeight2) / 2));
                this.aBw.draw(canvas);
                canvas.restore();
            }
        }
    }

    public void e(Drawable[] drawableArr) {
        if (drawableArr != null) {
            for (Drawable bounds : drawableArr) {
                bounds.setBounds(0, 0, this.vk, this.vl);
            }
            this.aAP = drawableArr;
            if (drawableArr[0] != null) {
                Rect rect = new Rect();
                if (drawableArr[0].getPadding(rect)) {
                    a(rect.left, rect.top, rect.right, rect.bottom);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void eL() {
        super.eL();
        int size = this.lO == null ? 0 : this.lO.size();
        if (size < this.vm) {
            this.vm = size;
        }
        this.vn = (((this.aSP - this.paddingLeft) - this.paddingRight) / this.vm) - this.vk;
        this.aBi = this.vm == 0 ? 0 : ((size + (this.vm * this.aBg)) - 1) / (this.vm * this.aBg);
        this.vo = (this.aSQ - (this.vl * this.aBg)) / (this.aBg + 1);
        this.aBj = this.paddingLeft + this.paddingRight;
        fb(this.aBh);
    }

    /* access modifiers changed from: protected */
    public boolean es() {
        return yr();
    }

    public void ey(int i) {
        this.Yd = i;
    }

    public void fd(int i) {
        this.vp = i;
    }

    public void fe(int i) {
        this.vq = i;
    }

    public void ff(int i) {
    }

    public void fg(int i) {
        this.aBg = i;
    }

    public void fh(int i) {
        this.aBd = i;
    }

    public void fi(int i) {
        this.aBe = i;
    }

    /* access modifiers changed from: protected */
    public boolean k(int i, int i2) {
        int n = n(i, i2);
        if (n < 0) {
            return super.k(i, i2);
        }
        eZ(n);
        g(this.aBp);
        return true;
    }

    /* access modifiers changed from: protected */
    public int n(int i, int i2) {
        if (this.lO == null || this.lO.size() == 0) {
            return -1;
        }
        int i3 = (((i - this.aBk) % this.aSP) - this.paddingLeft) - (this.vn / 2);
        int i4 = i2 - this.vo;
        if (i3 % (this.vk + this.vn) >= this.vk || i4 % (this.vl + this.vo) >= this.vl) {
            return -1;
        }
        int i5 = (i3 / (this.vk + this.vn)) + ((i4 / (this.vl + this.vo)) * this.vm) + (this.aBh * this.aBg * this.vm);
        if (i5 >= this.lO.size()) {
            return -1;
        }
        return i5;
    }

    public void p(Vector vector) {
        this.lO = vector;
        eL();
    }

    public void v(Drawable drawable) {
        this.aBv = drawable;
    }

    public void w(Drawable drawable) {
        this.aBx = drawable;
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicWidth2 = drawable.getIntrinsicWidth();
        int i = (((this.vk + this.aBr) - this.aBs) - intrinsicWidth) / 2;
        int i2 = (((this.vl + this.aBt) - this.aBu) - intrinsicWidth2) / 2;
        drawable.setBounds(i, i2, intrinsicWidth + i, intrinsicWidth2 + i2);
    }

    public void x(Drawable drawable) {
        if (drawable == null) {
            this.aBy = null;
            return;
        }
        this.aBy = drawable;
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicWidth2 = drawable.getIntrinsicWidth();
        int i = (this.vk - intrinsicWidth) - this.aBs;
        int i2 = this.aBt;
        drawable.setBounds(i, i2, intrinsicWidth + i, intrinsicWidth2 + i2);
    }

    public void y(Drawable drawable) {
        this.aBz = drawable;
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicWidth2 = drawable.getIntrinsicWidth();
        int i = (((this.vk + this.aBr) - this.aBs) - intrinsicWidth) / 2;
        int i2 = (((this.vl + this.aBt) - this.aBu) - intrinsicWidth2) / 2;
        drawable.setBounds(i, i2, intrinsicWidth + i, intrinsicWidth2 + i2);
    }

    public int yA() {
        return this.vp;
    }

    public int yB() {
        return this.vq;
    }

    public Drawable[] ys() {
        return this.aAP;
    }

    public Drawable yt() {
        return this.aBv;
    }

    public Drawable yu() {
        return this.aBx;
    }

    public Drawable yv() {
        return this.aBy;
    }

    public Drawable yw() {
        return this.aBz;
    }

    public Drawable yx() {
        return this.aBw;
    }

    public int yy() {
        return this.aBd;
    }

    public int yz() {
        return this.aBe;
    }

    public void z(Drawable drawable) {
        this.aBw = drawable;
    }
}
