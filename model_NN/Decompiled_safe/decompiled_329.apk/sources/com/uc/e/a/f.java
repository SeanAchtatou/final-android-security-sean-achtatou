package com.uc.e.a;

import android.graphics.Canvas;
import android.view.KeyEvent;
import com.uc.e.s;
import com.uc.e.z;

public class f extends s {
    public static final String tag = "MultiWindowListView";
    private n abC;
    private int apJ = 0;
    private k apK;
    private int index;

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, z zVar, int i) {
        u uVar = (u) zVar;
        if (this.vj.size() == 1) {
            uVar.b(true);
        }
        if (i == this.apJ) {
            uVar.c((Boolean) true);
            this.apJ = -1;
        }
        super.a(canvas, zVar, i);
    }

    public void a(k kVar) {
        this.apK = kVar;
    }

    public void a(n nVar) {
        this.abC = nVar;
    }

    public void a(u uVar) {
        this.index = this.vj == null ? -1 : this.vj.indexOf(uVar);
        if (this.index >= 0) {
            this.vj.remove(this.index);
            aP(false);
        }
        if (uVar.Fa()) {
            for (int i = 0; i < this.vj.size(); i++) {
                ((u) this.vj.get(i)).c((Boolean) false);
            }
            if (this.index != this.vj.size()) {
                eM(this.index);
            } else if (this.index - 1 >= 0) {
                eM(this.index - 1);
            }
        }
        if (this.abC != null) {
            this.abC.eD(this.index);
        }
        this.aeK = -1;
        this.bdt = 0;
    }

    public boolean a(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 23) {
            this.bdt--;
            if (this.bdt >= 0 && this.bdt < this.vj.size()) {
                b((u) this.vj.get(this.bdt));
            }
        }
        return (keyEvent.getAction() == 1 && this.bdt == 0) ? super.a(new KeyEvent(0, keyEvent.getKeyCode())) : super.a(keyEvent);
    }

    /* access modifiers changed from: protected */
    public void aP(boolean z) {
        int yZ = yZ();
        if (this.apK != null) {
            this.apK.fN(yZ);
        }
        super.aP(z);
    }

    public void b(u uVar) {
        int indexOf = this.vj == null ? -1 : this.vj.indexOf(uVar);
        if (this.abC != null) {
            this.abC.eP(indexOf);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.e.s.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      com.uc.e.a.f.a(android.graphics.Canvas, com.uc.e.z, int):void
      com.uc.e.s.a(float, int, boolean):void
      com.uc.e.s.a(android.graphics.Canvas, com.uc.e.z, int):void
      com.uc.e.s.a(byte, int, int):boolean
      com.uc.e.z.a(byte, int, int):boolean
      com.uc.e.s.a(int, boolean, boolean):void */
    /* access modifiers changed from: protected */
    public void eL() {
        super.eL();
        a(this.apJ, false, true);
    }

    public void eL(int i) {
        this.bdt = i;
    }

    public void eM(int i) {
        this.apJ = i;
        if (this.vj.elementAt(i) != null) {
            ((u) this.vj.get(i)).c((Boolean) true);
        }
    }
}
