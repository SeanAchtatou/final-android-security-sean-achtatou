package com.uc.e.a;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.uc.browser.R;
import com.uc.browser.UCR;
import com.uc.e.z;
import com.uc.h.e;

public class u extends z {
    private static final int aBE = 2;
    private static final int boB = 1;
    private Paint aBF = new Paint(1);
    private String aBH;
    private boolean aBJ = false;
    private int aBL = this.oQ.kp(R.dimen.f427mutiwindowlist_maxtitletextlength);
    private Bitmap aBR = this.oQ.kl(UCR.drawable.aUb);
    private boolean aBV = false;
    private boolean boA = false;
    private Bitmap bop = this.oQ.kl(UCR.drawable.aUl);
    private Bitmap boq = this.oQ.kl(UCR.drawable.aUc);
    private int bor = this.oQ.kp(R.dimen.f426mutiwindowlist_closebutton_size);
    private int bos = this.oQ.kp(R.dimen.f419mutiwindowlist_point_padding_left);
    private int bot = this.oQ.kp(R.dimen.f420mutiwindowlist_point_padding_top);
    private int bou = this.oQ.kp(R.dimen.f421mutiwindowlist_shortcuttext_padding_left);
    private int bov = this.oQ.kp(R.dimen.f422mutiwindowlist_shortcuttext_padding_top);
    private int bow = this.oQ.kp(R.dimen.f424mutiwindowlist_closebutton_padding_right);
    private int box = this.oQ.kp(R.dimen.f425mutiwindowlist_selectarea_padding_x);
    private int boy = (this.bor / 3);
    private int boz;
    e oQ = e.Pb();
    private int textSize = this.oQ.kp(R.dimen.f176multiwindowlist_textsize);

    public u() {
        bL(true);
    }

    public boolean Fa() {
        return this.boA;
    }

    public void b(Boolean bool) {
        this.aBJ = bool.booleanValue();
    }

    public boolean b(byte b2, int i, int i2) {
        switch (b2) {
            case 0:
                if (n(i, i2) == 2) {
                    this.aBV = true;
                    Ix();
                    break;
                }
                break;
            case 1:
                this.aBV = false;
                break;
            case 2:
                if (this.aBV && n(i, i2) != 2) {
                    this.aBV = false;
                    Ix();
                    break;
                }
        }
        boolean b3 = super.b(b2, i, i2);
        if (!b3 && this.aBV) {
            this.aBV = false;
            Ix();
        }
        return b3;
    }

    public void c(Boolean bool) {
        this.boA = bool.booleanValue();
    }

    /* access modifiers changed from: protected */
    public String df(String str) {
        int breakText = this.aBF.breakText(str, true, (float) this.aBL, null);
        if (str.length() <= breakText) {
            return str;
        }
        char[] cArr = new char[breakText];
        str.getChars(0, breakText, cArr, 0);
        return String.valueOf(cArr) + "...";
    }

    public void dg(String str) {
        this.aBH = str;
    }

    public void draw(Canvas canvas) {
        this.aBF.setColor(e.Pb().getColor(61));
        this.aBF.setTextSize((float) this.textSize);
        if (this.boA) {
            canvas.drawBitmap(this.bop, (float) this.bos, (float) this.bot, this.aBF);
        }
        canvas.drawText(df(this.aBH), (float) this.bou, (float) this.bov, this.aBF);
        if (!this.aBJ) {
            Bitmap bitmap = this.aBV ? this.boq : this.aBR;
            canvas.drawBitmap(bitmap, (float) ((this.aSP - bitmap.getWidth()) - this.bow), (float) ((this.aSQ - bitmap.getHeight()) / 2), this.aBF);
        }
        this.boz = ((this.aSP - this.aBR.getWidth()) - this.bow) - this.boy;
    }

    /* access modifiers changed from: protected */
    public boolean k(int i, int i2) {
        if (this.aBJ) {
            ((f) this.bBm).b(this);
            return true;
        } else if (n(i, i2) == 2) {
            ((f) this.bBm).a(this);
            Ix();
            return true;
        } else if (n(i, i2) != 1) {
            return false;
        } else {
            ((f) this.bBm).b(this);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public int n(int i, int i2) {
        if (i > this.boz) {
            return 2;
        }
        return (i <= this.box || i >= this.boz - this.box) ? -1 : 1;
    }

    public String yC() {
        return this.aBH;
    }
}
