package com.uc.e.a;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import com.uc.e.ac;
import com.uc.e.ad;

public class p extends ac {
    private Transformation aa;
    Paint blO;
    private int blP = 1;
    private int blQ;
    private int blR;
    private int blS;
    private int blT = -1;
    private Animation blU;
    private ad blV;

    public p(int i, int i2, int i3) {
        super(i, i2, i3);
    }

    public int DO() {
        return this.blT;
    }

    public void a(Canvas canvas, int i, int i2) {
        super.a(canvas, i, i2);
        if (this.blO == null) {
            this.blO = new Paint();
            this.blO.setTypeface(Typeface.DEFAULT_BOLD);
            this.blO.setAntiAlias(true);
            this.blO.setTextSize((float) this.blQ);
            this.blO.setColor(this.blT);
        }
        if (this.aP) {
            this.blO.setAlpha(this.caS);
        } else {
            this.blO.setAlpha(this.caT);
        }
        int i3 = ((((i - this.paddingLeft) - this.paddingRight) - this.caP) / 2) + this.paddingLeft;
        int measureText = this.blP < 10 ? i3 + this.blR : i3 + ((int) (((float) this.blR) - (this.blO.measureText("10") / 4.0f)));
        int i4 = (this.Bo == null || this.Bo.length() == 0 || this.caQ == 2) ? ((i2 - this.caO) / 2) + this.blS + this.textSize : this.paddingTop + this.blS + this.textSize;
        canvas.drawText(this.blP + "", (float) measureText, (float) i4, this.blO);
        if (this.blU != null && this.blV != null) {
            if (this.aa == null) {
                this.aa = new Transformation();
            }
            if (this.blU.getTransformation(System.currentTimeMillis(), this.aa)) {
                canvas.save();
                this.blO.setAlpha((int) (this.aa.getAlpha() * 255.0f));
                canvas.concat(this.aa.getMatrix());
                canvas.drawText(this.blP + "", (float) measureText, (float) i4, this.blO);
                this.blO.setAlpha(255);
                canvas.restore();
                this.blV.cJ();
                return;
            }
            this.blU = null;
        }
    }

    public void a(ad adVar) {
        this.blV = adVar;
    }

    public void hc(int i) {
        this.blP = i;
    }

    public void hd(int i) {
        this.blQ = i;
    }

    public void he(int i) {
        this.blR = i;
    }

    public void hf(int i) {
        this.blS = i;
    }

    public void hg(int i) {
        this.blT = i;
        if (this.blO != null) {
            this.blO.setColor(i);
        }
    }

    public void startAnimation(Animation animation) {
        this.blU = animation;
        this.blU.initialize(this.width, this.height, this.width, this.height);
        this.blU.start();
        if (this.blV != null) {
            this.blV.cJ();
        }
    }
}
