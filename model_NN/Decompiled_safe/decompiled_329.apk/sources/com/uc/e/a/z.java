package com.uc.e.a;

import android.graphics.Canvas;
import com.uc.browser.R;
import com.uc.h.e;

class z extends com.uc.e.z {
    private String Bo;
    final /* synthetic */ o cdW;

    public z(o oVar, String str, int i, int i2) {
        this.cdW = oVar;
        this.Bo = str;
        this.pL.setColor(e.Pb().getColor(78));
        this.pL.setTextSize((float) e.Pb().kp(R.dimen.f392set_tilte_font));
        setSize(i, i2);
    }

    public void draw(Canvas canvas) {
        canvas.drawText(this.Bo, (float) e.Pb().kp(R.dimen.f393set_summary_font), ((((float) getHeight()) - this.pL.descent()) - this.pL.ascent()) / 2.0f, this.pL);
    }
}
