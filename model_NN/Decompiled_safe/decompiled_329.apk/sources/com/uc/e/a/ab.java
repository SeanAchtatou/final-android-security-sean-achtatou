package com.uc.e.a;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import com.uc.browser.R;
import com.uc.browser.UCR;
import com.uc.e.b.l;
import com.uc.e.o;
import com.uc.e.z;
import com.uc.h.a;
import com.uc.h.e;

public class ab extends z implements a {
    private static int bpm = 120;
    private static int bpn = 120;
    private static final int bqc = 1;
    private static final int bqd = 2;
    private static final float[] bqn = {0.666f, 0.7125f};
    public static final int bqo = 0;
    public static final int bqp = 1;
    private static final int cgV = 3;
    private static int cgW = 5;
    private static final int cgZ = 0;
    private static final int cha = 1;
    public static final String tag = "WebSchMainPageView";
    /* access modifiers changed from: private */
    public j auy;
    private Animation bpA;
    private Animation bpB;
    private o bpE;
    private o bpF;
    private Transformation bpH;
    private boolean bpJ;
    private boolean bpK;
    private int bpU;
    private int bpV;
    private int bpW;
    private int bpX;
    private Drawable bpo;
    private Drawable bpp;
    private Drawable bpr;
    private Drawable bps;
    private Drawable bpu;
    private Rect bpv;
    private Animation bpz;
    private int bqh = -1;
    private String bqi = "搜索";
    private Paint bqj;
    private int bqm;
    private boolean cgD;
    private boolean cgE;
    private boolean cgF;
    private boolean cgG;
    private Animation cgH;
    private Animation cgI;
    private Animation cgJ;
    private Animation cgK;
    private Animation cgL;
    private Animation cgM;
    private Animation cgN;
    private Transformation cgO;
    private Transformation cgP;
    private Transformation cgQ;
    private int cgR;
    private int cgS;
    private String cgT = "输入网址";
    private int cgU;
    private int cgX;
    private boolean cgY;
    private int chb = -1;
    private int chc;
    private Drawable chd = null;
    private Drawable che = null;
    private Drawable chf = null;
    private int chg = 34;
    private int chh = 12;
    private int chi = 6;
    private int chj = 4;
    private int chk = 40;
    boolean chl = false;
    private Drawable cz;
    private int paddingLeft;
    private int paddingTop;
    private int textColor;

    public ab() {
        k();
        e Pb = e.Pb();
        this.bqj = new Paint();
        this.bqj.setAntiAlias(true);
        this.bqj.setColor(this.textColor);
        this.bqj.setTextSize((float) Pb.kp(R.dimen.f484add_sch_textsize));
        this.bpX = Pb.kp(R.dimen.f486add_sch_text_margin_left);
        this.paddingLeft = Pb.kp(R.dimen.f478add_sch_offset_left);
        this.paddingTop = Pb.kp(R.dimen.f479add_sch_offset_top);
        this.bpU = Pb.kp(R.dimen.f477add_sch_button_width);
        this.bpV = Pb.kp(R.dimen.f480add_sch_difference_left);
        this.bpW = Pb.kp(R.dimen.f481add_sch_difference_right);
        this.cgU = Pb.kp(R.dimen.f487add_sch_text_margin_left2);
        this.chg = Pb.kp(R.dimen.f473add_sch_face_size);
        this.chh = Pb.kp(R.dimen.f474add_sch_padding_left);
        this.chi = Pb.kp(R.dimen.f476add_sch_padding_top);
        this.chj = Pb.kp(R.dimen.f475add_sch_face_fix);
        this.chk = (this.chh + this.chg) - this.chj;
        this.bpH = new Transformation();
        this.cgO = new Transformation();
        this.cgP = new Transformation();
        this.cgQ = new Transformation();
        e.Pb().a(this);
    }

    private boolean Fk() {
        return this.bpo == null || this.bpp == null || this.bpr == null || this.bps == null || this.bpu == null;
    }

    private void a(Canvas canvas, Rect rect) {
        Rect rect2 = new Rect();
        rect2.left = rect.left + this.bpv.left;
        rect2.right = rect.right - this.bpv.right;
        rect2.top = rect.top + this.bpv.top;
        rect2.bottom = rect.bottom - this.bpv.bottom;
        int color = this.bqj.getColor();
        float textSize = this.bqj.getTextSize();
        this.bqj.setTextSize((float) e.Pb().kp(R.dimen.f529add_sch_state_text));
        float measureText = this.bqj.measureText("取消");
        this.bqj.setColor(this.chc);
        canvas.drawText("取消", ((((float) (rect2.right - rect2.left)) - measureText) / 2.0f) + ((float) rect2.left), (((((float) (rect2.bottom - rect2.top)) - this.bqj.ascent()) - this.bqj.descent()) / 2.0f) + ((float) rect2.top), this.bqj);
        this.bqj.setColor(color);
        this.bqj.setTextSize(textSize);
    }

    private boolean a(Animation animation, Animation animation2, Animation animation3) {
        long currentTimeMillis = System.currentTimeMillis();
        return animation3.getTransformation(currentTimeMillis, this.cgP) | animation.getTransformation(currentTimeMillis, this.bpH) | animation2.getTransformation(currentTimeMillis, this.cgO);
    }

    private boolean a(Animation animation, Animation animation2, Animation animation3, Animation animation4) {
        long currentTimeMillis = System.currentTimeMillis();
        boolean transformation = animation.getTransformation(currentTimeMillis, this.bpH);
        boolean transformation2 = animation2.getTransformation(currentTimeMillis, this.cgO);
        return animation4.getTransformation(currentTimeMillis, this.cgQ) | transformation | transformation2 | animation3.getTransformation(currentTimeMillis, this.cgP);
    }

    private void kG(int i) {
        if (i == 0) {
            this.cgD = false;
            this.bpz.reset();
            this.bpz.setStartTime(-1);
            this.cgJ.reset();
            this.cgJ.setStartTime(-1);
            this.cgL.reset();
            this.cgL.setStartTime(-1);
            this.bpB.reset();
            this.bpB.setStartTime(-1);
            return;
        }
        this.cgF = false;
        this.cgH.reset();
        this.cgH.setStartTime(-1);
        this.cgK.reset();
        this.cgK.setStartTime(-1);
        this.cgM.reset();
        this.cgM.setStartTime(-1);
        this.cgN.reset();
        this.cgN.setStartTime(-1);
    }

    private void kH(int i) {
        if (i == 0) {
            this.cgE = false;
            this.bpA.reset();
            this.bpA.setStartTime(-1);
            this.cgJ.reset();
            this.cgJ.setStartTime(-1);
            this.cgL.reset();
            this.cgL.setStartTime(-1);
            this.bpB.reset();
            this.bpB.setStartTime(-1);
            return;
        }
        this.cgG = false;
        this.cgI.reset();
        this.cgI.setStartTime(-1);
        this.cgK.reset();
        this.cgK.setStartTime(-1);
        this.cgM.reset();
        this.cgM.setStartTime(-1);
        this.cgN.reset();
        this.cgN.setStartTime(-1);
    }

    private void n(Canvas canvas) {
        canvas.save();
        canvas.translate((float) this.paddingLeft, (float) this.paddingTop);
        this.bpu.setBounds(this.paddingLeft, 0, this.aSP - (this.paddingLeft * 4), this.aSQ);
        this.bpu.draw(canvas);
        this.bpo.setBounds(this.bpV, 0, ((this.aSP - this.bpU) + this.bpW) - this.paddingLeft, this.aSQ);
        this.bpo.draw(canvas);
        this.cz.setBounds((this.aSP - this.bpU) - this.paddingLeft, -this.paddingTop, this.aSP - this.paddingLeft, this.aSQ);
        this.cz.draw(canvas);
        a(canvas, new Rect((this.aSP - this.bpU) - this.paddingLeft, -this.paddingTop, this.aSP - this.paddingLeft, this.aSQ));
        canvas.drawText(this.bqi, (float) (this.bpo.getBounds().left + this.cgU), (float) this.cgX, this.bqj);
        canvas.restore();
    }

    private void o(Canvas canvas) {
        canvas.save();
        canvas.translate((float) this.paddingLeft, (float) this.paddingTop);
        this.bpu.setBounds(this.paddingLeft, 0, this.aSP + ((-this.paddingLeft) * 4), this.aSQ);
        this.bpu.draw(canvas);
        this.bpp.draw(canvas);
        canvas.drawText(this.cgT, (float) (this.bpp.getBounds().left + this.bpX), (float) this.cgX, this.bqj);
        this.cz.setBounds((this.aSP - this.bpU) - this.paddingLeft, -this.paddingTop, this.aSP - this.paddingLeft, this.aSQ);
        this.cz.draw(canvas);
        a(canvas, new Rect((this.aSP - this.bpU) - this.paddingLeft, -this.paddingTop, this.aSP - this.paddingLeft, this.aSQ));
        canvas.drawText(this.cgT, (float) (this.bpp.getBounds().left + this.bpX), (float) this.cgX, this.bqj);
        canvas.restore();
    }

    public void Fi() {
        this.bpz = new ScaleAnimation(1.0f, ((float) this.aSP) / ((float) this.cgR), 1.0f, 1.0f);
        this.bpz.setDuration((long) bpm);
        this.bpz.setStartTime(-1);
        if (this.chd != null) {
            this.bpA = new AnimationSet(true);
            ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, ((float) this.aSP) / ((float) this.cgS), 1.0f, 1.0f);
            scaleAnimation.setDuration((long) bpn);
            ((AnimationSet) this.bpA).addAnimation(scaleAnimation);
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, (float) (-this.chk), 0.0f, 0.0f);
            translateAnimation.initialize(0, 0, 0, 0);
            translateAnimation.setDuration((long) bpn);
            ((AnimationSet) this.bpA).addAnimation(translateAnimation);
            this.bpA.setStartTime(-1);
        } else {
            this.bpA = new ScaleAnimation(1.0f, ((float) this.aSP) / ((float) this.cgS), 1.0f, 1.0f);
            this.bpA.setDuration((long) bpn);
            this.bpA.setStartTime(-1);
        }
        if (this.chd != null) {
            this.cgI = new AnimationSet(true);
            ScaleAnimation scaleAnimation2 = new ScaleAnimation(((float) this.aSP) / ((float) this.cgS), 1.0f, 1.0f, 1.0f);
            scaleAnimation2.setDuration((long) bpn);
            ((AnimationSet) this.cgI).addAnimation(scaleAnimation2);
            TranslateAnimation translateAnimation2 = new TranslateAnimation((float) (-this.chk), 0.0f, 0.0f, 0.0f);
            translateAnimation2.initialize(0, 0, 0, 0);
            translateAnimation2.setDuration((long) bpn);
            ((AnimationSet) this.cgI).addAnimation(translateAnimation2);
            this.cgI.setStartTime(-1);
        } else {
            this.cgI = new ScaleAnimation(((float) this.aSP) / ((float) this.cgS), 1.0f, 1.0f, 1.0f);
            this.cgI.setDuration((long) bpn);
            this.cgI.setStartTime(-1);
        }
        this.cgH = new ScaleAnimation(((float) this.aSP) / ((float) this.cgR), 1.0f, 1.0f, 1.0f);
        this.cgH.setDuration((long) (bpm * 2));
        this.cgH.setStartTime(-1);
        this.cgJ = new ScaleAnimation(1.0f, ((float) (this.aSP + (this.paddingLeft * 2))) / ((float) (this.aSP - this.bpU)), 1.0f, 1.0f);
        this.cgJ.setDuration((long) (bpm / 2));
        this.cgJ.setStartTime(-1);
        this.cgK = new ScaleAnimation(((float) this.aSP) / ((float) (this.aSP - this.bpU)), 1.0f, 1.0f, 1.0f);
        this.cgK.setDuration((long) ((bpm * 3) / 2));
        this.cgK.setStartTime(-1);
        this.cgL = new TranslateAnimation(0.0f, (float) this.paddingLeft, 0.0f, (float) this.paddingTop);
        this.cgL.setDuration((long) bpm);
        this.cgL.setStartTime(-1);
        this.cgL.initialize(0, 0, 0, 0);
        this.cgM = new TranslateAnimation((float) this.paddingLeft, 0.0f, (float) this.paddingTop, 0.0f);
        this.cgM.setDuration((long) bpm);
        this.cgM.setStartTime(-1);
        this.cgM.initialize(0, 0, 0, 0);
        this.bpB = new TranslateAnimation((float) this.aSP, (float) ((this.aSP - this.bpU) - this.paddingLeft), 0.0f, 0.0f);
        this.bpB.setDuration((long) (bpm * 2));
        this.bpB.setStartTime(-1);
        this.bpB.initialize(0, 0, 0, 0);
        this.cgN = new TranslateAnimation((float) ((this.aSP - this.bpU) - this.paddingLeft), (float) this.aSP, 0.0f, 0.0f);
        this.cgN.setDuration((long) (bpm * 2));
        this.cgN.setStartTime(-1);
        this.cgN.initialize(0, 0, 0, 0);
    }

    public void QM() {
        this.bpJ = false;
        this.bpK = false;
        this.cgD = false;
        this.cgE = false;
        this.cgF = false;
        this.cgG = false;
    }

    public void QN() {
        if (!this.cgD) {
            this.cgD = true;
            Ix();
        }
    }

    public void QO() {
        if (!this.cgE) {
            this.cgE = true;
            Ix();
        }
    }

    public void QP() {
        this.cgG = true;
        invalidate();
    }

    public void QQ() {
        this.cgF = true;
        invalidate();
    }

    public void a(Canvas canvas, Transformation transformation, Transformation transformation2, Transformation transformation3) {
        float[] fArr = new float[9];
        transformation2.getMatrix().getValues(fArr);
        int i = (int) (fArr[0] * ((float) this.aSP));
        transformation3.getMatrix().getValues(fArr);
        canvas.save();
        canvas.translate((float) ((int) fArr[2]), (float) ((int) fArr[5]));
        this.bpu.setBounds(this.aSP - i, 0, i, this.aSQ);
        this.bpu.draw(canvas);
        float[] fArr2 = new float[9];
        transformation.getMatrix().getValues(fArr2);
        int i2 = (int) (fArr2[0] * ((float) this.cgS));
        Rect rect = new Rect();
        if (this.chd != null) {
            rect.left = this.chk;
        }
        rect.left = (int) (fArr2[2] + ((float) rect.left));
        if (i2 >= (this.aSP - this.bpU) - this.paddingLeft) {
            rect.right = (this.aSP - this.bpU) - this.paddingLeft;
        } else {
            rect.right = i2;
        }
        rect.top = 0;
        rect.bottom = this.aSQ;
        this.bpp.setBounds(rect);
        this.bpp.draw(canvas);
        canvas.drawText(this.cgT, (float) (this.bpp.getBounds().left + this.bpX), (float) this.cgX, this.bqj);
        if (this.chd != null) {
            this.chd.setBounds(rect.left - this.chk, this.chi, rect.left, this.chi + this.chg);
            this.chd.draw(canvas);
            Drawable drawable = this.cgY ? this.chf : this.che;
            if (drawable != null) {
                drawable.setBounds(rect.left - this.chk, this.chi, rect.left, this.chi + this.chg);
                drawable.draw(canvas);
            }
        }
        this.bpo.setBounds(i2, 0, this.cgR + i2, this.aSQ);
        this.bpo.draw(canvas);
        canvas.drawText(this.cgT, (float) (this.bpp.getBounds().left + this.bpX), (float) this.cgX, this.bqj);
        canvas.drawText(this.bqi, (float) (this.bpo.getBounds().left + this.cgU), (float) this.cgX, this.bqj);
        invalidate();
    }

    public void a(Canvas canvas, Transformation transformation, Transformation transformation2, Transformation transformation3, Transformation transformation4) {
        float[] fArr = new float[9];
        transformation2.getMatrix().getValues(fArr);
        int i = (int) (fArr[0] * ((float) this.aSP));
        transformation3.getMatrix().getValues(fArr);
        canvas.save();
        canvas.translate((float) ((int) fArr[2]), (float) ((int) fArr[5]));
        this.bpu.setBounds(this.aSP - i, 0, i, this.aSQ);
        this.bpu.draw(canvas);
        transformation.getMatrix().getValues(fArr);
        int i2 = (int) (fArr[0] * ((float) this.cgR));
        transformation4.getMatrix().getValues(fArr);
        int i3 = (int) fArr[2];
        this.cz.setBounds(i3, -this.paddingTop, this.bpU + i3, this.aSQ);
        this.cz.draw(canvas);
        a(canvas, new Rect(i3, -this.paddingTop, this.bpU + i3, this.aSQ));
        this.bpp.setBounds((this.cgR - i2) + (this.chd == null ? 0 : this.chk), 0, this.aSP - i2, this.aSQ);
        this.bpp.draw(canvas);
        Rect bounds = this.bpp.getBounds();
        if (this.chd != null) {
            this.chd.setBounds(bounds.left - this.chk, this.chi, bounds.left, this.chi + this.chg);
            this.chd.draw(canvas);
            Drawable drawable = this.cgY ? this.chf : this.che;
            if (drawable != null) {
                drawable.setBounds(bounds.left - this.chk, this.chi, bounds.left, this.chi + this.chg);
                drawable.draw(canvas);
            }
        }
        int i4 = this.aSP - i2 < this.bpV ? this.bpV : this.aSP - i2;
        if (i3 > this.aSP) {
            this.bpo.setBounds(i4, 0, this.aSP, this.aSQ);
        } else if (i3 < ((this.aSP - this.bpU) - this.paddingLeft) + this.bpW) {
            this.bpo.setBounds(i4, 0, ((this.aSP - this.bpU) - this.paddingLeft) + this.bpW, this.aSQ);
        } else {
            this.bpo.setBounds(i4, 0, this.aSP - (this.aSP - i3), this.aSQ);
        }
        this.bpo.draw(canvas);
        canvas.drawText(this.cgT, (float) (this.bpp.getBounds().left + this.bpX), (float) this.cgX, this.bqj);
        canvas.drawText(this.bqi, (float) (this.bpo.getBounds().left + this.cgU), (float) this.cgX, this.bqj);
        canvas.restore();
        invalidate();
    }

    public void a(j jVar) {
        this.auy = jVar;
        a(new m(this));
        b(new l(this));
    }

    public void a(o oVar) {
        this.bpE = oVar;
    }

    public void b(Canvas canvas, Transformation transformation, Transformation transformation2, Transformation transformation3, Transformation transformation4) {
        if (transformation4 != null) {
            float[] fArr = new float[9];
            transformation4.getMatrix().getValues(fArr);
            int i = (int) fArr[2];
            this.cz.setBounds(i, -this.paddingTop, this.bpU + i, this.aSQ);
            canvas.save();
            canvas.translate((float) this.paddingLeft, (float) this.paddingTop);
            this.bpu.setBounds(this.paddingLeft, 0, this.aSP + ((-this.paddingLeft) * 4), this.aSQ);
            this.bpu.draw(canvas);
            this.bpp.draw(canvas);
            canvas.drawText(this.cgT, (float) (this.bpp.getBounds().left + this.bpX), (float) this.cgX, this.bqj);
            this.cz.draw(canvas);
            a(canvas, new Rect(i, -this.paddingTop, this.bpU + i, this.aSQ));
            canvas.drawText(this.cgT, (float) (this.bpp.getBounds().left + this.bpX), (float) this.cgX, this.bqj);
            canvas.restore();
            invalidate();
            return;
        }
        a(canvas, transformation, transformation2, transformation3);
    }

    public void b(o oVar) {
        this.bpF = oVar;
    }

    public void h(Bitmap bitmap) {
        boolean z = false;
        if ((this.chd != null && bitmap == null) || (this.chd == null && bitmap != null)) {
            z = true;
        }
        if (bitmap == null) {
            this.chd = null;
        } else {
            this.chd = new BitmapDrawable(bitmap);
        }
        if (z) {
            Fi();
        }
        Ix();
    }

    public void init(int i) {
        this.cgS = (int) (((float) this.aSP) * bqn[i]);
        this.cgR = this.aSP - this.cgS;
        this.cgX = (int) (((float) e.Pb().kp(R.dimen.f488add_sch_mainpage_text_paddingtop)) - this.bqj.ascent());
        Fi();
        QM();
    }

    public void k() {
        this.bpu = e.Pb().getDrawable(UCR.drawable.aXw);
        this.bpp = e.Pb().getDrawable(UCR.drawable.fi);
        this.bps = e.Pb().getDrawable(UCR.drawable.aTt);
        this.bpo = e.Pb().getDrawable(UCR.drawable.il);
        this.bpr = e.Pb().getDrawable(UCR.drawable.aWj);
        this.cz = e.Pb().getDrawable(UCR.drawable.aTu);
        this.bpv = new Rect();
        this.cz.getPadding(this.bpv);
        this.chc = e.Pb().getColor(27);
        this.textColor = e.Pb().getColor(25);
        this.bqm = e.Pb().getColor(26);
        if (this.bqj != null) {
            this.bqj.setColor(this.textColor);
        }
        e Pb = e.Pb();
        this.che = new l(Pb.getColor(36), Pb.kq(R.dimen.f472face_frame_corner), Pb.getColor(37), Pb.kp(R.dimen.f471face_highlight_height), Pb.getColor(38), Pb.kp(R.dimen.f470face_border));
        ((l) this.che).cH(false);
        this.chf = new l(Pb.getColor(36), Pb.kq(R.dimen.f472face_frame_corner), Pb.getColor(37), Pb.kp(R.dimen.f471face_highlight_height), Pb.getColor(39), Pb.kp(R.dimen.f470face_border));
        ((l) this.chf).cH(false);
    }

    public void k(int i) {
        if (i == 0) {
            this.chl = false;
            if (this.chb != -1) {
                if (this.chb == 0) {
                    QQ();
                }
                if (this.chb == 1) {
                    QP();
                }
                this.chb = -1;
                invalidate();
                return;
            }
            return;
        }
        this.chb = -1;
        QM();
        invalidate();
    }

    public void onDraw(Canvas canvas) {
        if (!Fk()) {
            if (this.chl) {
                if (this.chb == 0) {
                    n(canvas);
                } else if (this.chb == 1) {
                    o(canvas);
                } else {
                    this.chl = false;
                }
            }
            if (!this.cgD && !this.cgE && !this.cgF && !this.cgG && !this.chl) {
                p(canvas);
            } else if (this.cgD) {
                if (a(this.bpz, this.cgJ, this.cgL, this.bpB)) {
                    a(canvas, this.bpH, this.cgO, this.cgP, this.cgQ);
                    return;
                }
                n(canvas);
                kG(0);
                if (this.bpE != null) {
                    this.bpE.ww();
                }
                this.chb = 0;
                this.chl = true;
            } else if (this.cgE) {
                if (a(this.bpA, this.cgJ, this.cgL)) {
                    b(canvas, this.bpH, this.cgO, this.cgP, null);
                } else if (this.bpB.getTransformation(System.currentTimeMillis(), this.cgQ)) {
                    b(canvas, null, null, null, this.cgQ);
                } else {
                    o(canvas);
                    kH(0);
                    if (this.bpF != null) {
                        this.bpF.ww();
                    }
                    this.chb = 1;
                    this.chl = true;
                }
            } else if (this.cgG) {
                if (this.cgN.getTransformation(System.currentTimeMillis(), this.cgQ)) {
                    b(canvas, null, null, null, this.cgQ);
                } else if (a(this.cgI, this.cgK, this.cgM)) {
                    b(canvas, this.bpH, this.cgO, this.cgP, null);
                } else {
                    kH(1);
                    p(canvas);
                    this.chl = false;
                }
            } else if (!this.cgF) {
            } else {
                if (a(this.cgH, this.cgK, this.cgM, this.cgN)) {
                    a(canvas, this.bpH, this.cgO, this.cgP, this.cgQ);
                    return;
                }
                kG(1);
                p(canvas);
                this.chl = false;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00b6, code lost:
        if (r1 <= (r8.aSQ + com.uc.e.a.ab.cgW)) goto L_0x00bd;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r9) {
        /*
            r8 = this;
            r7 = 3
            r6 = 2
            r5 = 0
            r4 = 1
            boolean r0 = r8.cgE
            if (r0 != 0) goto L_0x0014
            boolean r0 = r8.cgD
            if (r0 != 0) goto L_0x0014
            boolean r0 = r8.cgG
            if (r0 != 0) goto L_0x0014
            boolean r0 = r8.cgF
            if (r0 == 0) goto L_0x0016
        L_0x0014:
            r0 = r4
        L_0x0015:
            return r0
        L_0x0016:
            float r0 = r9.getX()
            int r0 = (int) r0
            float r1 = r9.getY()
            int r1 = (int) r1
            int r2 = r9.getAction()
            switch(r2) {
                case 0: goto L_0x0029;
                case 1: goto L_0x004c;
                case 2: goto L_0x006e;
                default: goto L_0x0027;
            }
        L_0x0027:
            r0 = r4
            goto L_0x0015
        L_0x0029:
            int r1 = r8.cgS
            if (r0 <= r1) goto L_0x0035
            r8.bpJ = r4
            r8.bqh = r4
        L_0x0031:
            r8.Ix()
            goto L_0x0027
        L_0x0035:
            android.graphics.drawable.Drawable r1 = r8.chd
            if (r1 == 0) goto L_0x0045
            int r1 = r8.chk
            int r2 = r8.chj
            int r1 = r1 + r2
        L_0x003e:
            if (r0 <= r1) goto L_0x0047
            r8.bpK = r4
            r8.bqh = r6
            goto L_0x0031
        L_0x0045:
            r1 = r5
            goto L_0x003e
        L_0x0047:
            r8.cgY = r4
            r8.bqh = r7
            goto L_0x0031
        L_0x004c:
            boolean r0 = r8.bpJ
            if (r0 == 0) goto L_0x0055
            r8.bpJ = r5
            r8.QN()
        L_0x0055:
            boolean r0 = r8.bpK
            if (r0 == 0) goto L_0x005e
            r8.bpK = r5
            r8.QO()
        L_0x005e:
            boolean r0 = r8.cgY
            if (r0 == 0) goto L_0x0027
            r8.cgY = r5
            com.uc.e.a.j r0 = r8.auy
            if (r0 == 0) goto L_0x0027
            com.uc.e.a.j r0 = r8.auy
            r0.rE()
            goto L_0x0027
        L_0x006e:
            int r2 = r8.bqh
            if (r2 != r4) goto L_0x0092
            boolean r2 = r8.bpJ
            if (r2 != r4) goto L_0x00d3
            int r2 = r8.cgS
            int r3 = com.uc.e.a.ab.cgW
            int r2 = r2 - r3
            if (r0 < r2) goto L_0x008d
            int r2 = r8.aSP
            if (r0 > r2) goto L_0x008d
            int r2 = com.uc.e.a.ab.cgW
            int r2 = -r2
            if (r1 < r2) goto L_0x008d
            int r2 = r8.aSQ
            int r3 = com.uc.e.a.ab.cgW
            int r2 = r2 + r3
            if (r1 <= r2) goto L_0x0092
        L_0x008d:
            r8.bpJ = r5
            r8.Ix()
        L_0x0092:
            int r2 = r8.bqh
            if (r2 != r6) goto L_0x00bd
            boolean r2 = r8.bpK
            if (r2 != r4) goto L_0x00e9
            int r2 = r8.cgS
            int r3 = com.uc.e.a.ab.cgW
            int r2 = r2 + r3
            if (r0 > r2) goto L_0x00b8
            android.graphics.drawable.Drawable r2 = r8.chd
            if (r2 == 0) goto L_0x00e7
            int r2 = r8.chk
            int r3 = r8.chj
            int r2 = r2 + r3
        L_0x00aa:
            if (r0 < r2) goto L_0x00b8
            int r2 = com.uc.e.a.ab.cgW
            int r2 = -r2
            if (r1 < r2) goto L_0x00b8
            int r2 = r8.aSQ
            int r3 = com.uc.e.a.ab.cgW
            int r2 = r2 + r3
            if (r1 <= r2) goto L_0x00bd
        L_0x00b8:
            r8.bpK = r5
            r8.Ix()
        L_0x00bd:
            int r1 = r8.bqh
            if (r1 != r7) goto L_0x0027
            boolean r1 = r8.cgY
            if (r1 == 0) goto L_0x0027
            int r1 = r8.chk
            int r2 = r8.chj
            int r1 = r1 + r2
            if (r0 <= r1) goto L_0x0027
            r8.cgY = r5
            r8.Ix()
            goto L_0x0027
        L_0x00d3:
            int r2 = r8.cgS
            if (r0 <= r2) goto L_0x0092
            int r2 = r8.aSP
            if (r0 >= r2) goto L_0x0092
            if (r1 <= 0) goto L_0x0092
            int r2 = r8.aSQ
            if (r1 >= r2) goto L_0x0092
            r8.bpJ = r4
            r8.Ix()
            goto L_0x0092
        L_0x00e7:
            r2 = r5
            goto L_0x00aa
        L_0x00e9:
            int r2 = r8.cgS
            if (r0 >= r2) goto L_0x00bd
            android.graphics.drawable.Drawable r2 = r8.chd
            if (r2 == 0) goto L_0x0104
            int r2 = r8.chk
            int r3 = r8.chj
            int r2 = r2 + r3
        L_0x00f6:
            if (r0 <= r2) goto L_0x00bd
            if (r1 <= 0) goto L_0x00bd
            int r2 = r8.aSQ
            if (r1 >= r2) goto L_0x00bd
            r8.bpK = r4
            r8.Ix()
            goto L_0x00bd
        L_0x0104:
            r2 = r5
            goto L_0x00f6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.e.a.ab.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void p(Canvas canvas) {
        this.bpu.setBounds(0, 0, this.aSP, this.aSQ);
        this.bpu.draw(canvas);
        int i = this.chd == null ? 0 : this.chk;
        if (this.bpK) {
            this.bps.setBounds(i, 0, this.cgS, this.aSQ);
            this.bps.draw(canvas);
            this.bqj.setColor(this.bqm);
        } else {
            this.bpp.setBounds(i, 0, this.cgS, this.aSQ);
            this.bpp.draw(canvas);
            this.bqj.setColor(this.textColor);
        }
        if (this.chd != null) {
            this.chd.setBounds(this.chh, this.chi, this.chg + this.chh, this.chi + this.chg);
            this.chd.draw(canvas);
            Drawable drawable = this.cgY ? this.chf : this.che;
            if (drawable != null) {
                drawable.setBounds(this.chh, this.chi, this.chg + this.chh, this.chi + this.chg);
                drawable.draw(canvas);
            }
        }
        canvas.drawText(this.cgT, (float) (this.bpp.getBounds().left + this.bpX), (float) this.cgX, this.bqj);
        if (this.bpJ) {
            this.bpr.setBounds(this.cgS, 0, this.aSP, this.aSQ);
            this.bpr.draw(canvas);
            this.bqj.setColor(this.bqm);
        } else {
            this.bpo.setBounds(this.cgS, 0, this.aSP, this.aSQ);
            this.bpo.draw(canvas);
            this.bqj.setColor(this.textColor);
        }
        canvas.drawText(this.bqi, (float) (this.bpo.getBounds().left + this.cgU), (float) this.cgX, this.bqj);
        this.bqj.setColor(this.textColor);
    }
}
