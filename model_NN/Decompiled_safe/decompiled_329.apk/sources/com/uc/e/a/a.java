package com.uc.e.a;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.uc.e.z;
import java.util.ArrayList;
import java.util.Iterator;

public class a extends z {
    public static final int mw = 0;
    public static final int mx = 1;
    private int direction = 0;
    private int mA = -1;
    private ArrayList mB;
    private ArrayList mC;
    private Rect mD = new Rect();
    private q mE;
    private Drawable ms;
    private int mt;
    private int mu;
    private Drawable mv;
    private Drawable my;
    private Drawable mz;
    private int textColor = -1;
    private int textColorHighlight = 268435455;

    public a() {
        bL(true);
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void M(int r8) {
        /*
            r7 = this;
            r5 = 0
            if (r8 != 0) goto L_0x0065
            android.graphics.drawable.Drawable r0 = r7.my
        L_0x0005:
            if (r0 == 0) goto L_0x00ab
            android.graphics.Rect r1 = new android.graphics.Rect
            r1.<init>()
            boolean r0 = r0.getPadding(r1)
            if (r0 == 0) goto L_0x00ab
            int r0 = r1.left
            int r2 = r1.top
            int r3 = r1.right
            int r4 = r1.bottom
            r7.setPadding(r0, r2, r3, r4)
            int r0 = r1.left
            int r2 = r1.right
            int r0 = r0 + r2
            int r0 = r0 + r5
            int r2 = r1.top
            int r1 = r1.bottom
            int r1 = r1 + r2
            int r1 = r1 + r5
        L_0x0029:
            java.util.ArrayList r2 = r7.mB
            if (r2 == 0) goto L_0x00a7
            java.util.ArrayList r2 = r7.mC
            if (r2 != 0) goto L_0x0038
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r7.mC = r2
        L_0x0038:
            java.util.ArrayList r2 = r7.mC
            r2.clear()
            java.util.ArrayList r2 = r7.mB
            java.util.Iterator r2 = r2.iterator()
            r3 = r0
        L_0x0044:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0068
            java.lang.Object r0 = r2.next()
            com.uc.e.a.w r0 = (com.uc.e.a.w) r0
            android.graphics.Paint r4 = r7.pL
            java.lang.String r0 = r0.Bo
            float r0 = r4.measureText(r0)
            float r3 = (float) r3
            float r3 = r3 + r0
            int r3 = (int) r3
            java.util.ArrayList r4 = r7.mC
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            r4.add(r0)
            goto L_0x0044
        L_0x0065:
            android.graphics.drawable.Drawable r0 = r7.mz
            goto L_0x0005
        L_0x0068:
            android.graphics.Rect r0 = r7.mD
            if (r0 == 0) goto L_0x00a4
            android.graphics.Rect r0 = r7.mD
            int r0 = r0.left
            android.graphics.Rect r2 = r7.mD
            int r2 = r2.right
            int r0 = r0 + r2
            int r2 = r7.mt
            int r0 = r0 + r2
            java.util.ArrayList r2 = r7.mB
            int r2 = r2.size()
            int r0 = r0 * r2
            int r2 = r7.mt
            int r0 = r0 - r2
            int r0 = r0 + r3
            android.graphics.Rect r2 = r7.mD
            int r2 = r2.top
            android.graphics.Rect r3 = r7.mD
            int r3 = r3.bottom
            int r2 = r2 + r3
            int r1 = r1 + r2
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0090:
            float r0 = (float) r0
            android.graphics.Paint r2 = r7.pL
            float r2 = r2.descent()
            android.graphics.Paint r3 = r7.pL
            float r3 = r3.ascent()
            float r2 = r2 - r3
            float r0 = r0 + r2
            int r0 = (int) r0
        L_0x00a0:
            r7.setSize(r1, r0)
            return
        L_0x00a4:
            r0 = r1
            r1 = r3
            goto L_0x0090
        L_0x00a7:
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00a0
        L_0x00ab:
            r1 = r5
            r0 = r5
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.e.a.a.M(int):void");
    }

    private void a(w wVar, Canvas canvas) {
        canvas.drawText(wVar.Bo, (float) this.mD.left, ((float) this.mD.top) - this.pL.ascent(), this.pL);
    }

    private void b(Canvas canvas) {
        if (this.ms != null) {
            this.ms.setBounds((this.mt - this.mu) / 2, 0, (this.mu + this.mt) / 2, (getHeight() - this.paddingTop) - this.paddingBottom);
            this.ms.draw(canvas);
        }
    }

    public boolean L(int i) {
        if (this.mB == null) {
            return false;
        }
        Iterator it = this.mB.iterator();
        while (it.hasNext()) {
            w wVar = (w) it.next();
            if (wVar.id == i) {
                this.mB.remove(wVar);
                M(this.direction);
                return true;
            }
        }
        return false;
    }

    public void N(int i) {
        this.mt = i;
        M(this.direction);
    }

    public void O(int i) {
        this.mu = i;
    }

    public void P(int i) {
        this.textColorHighlight = i;
    }

    public void a(int i, int i2, int i3, int i4) {
        this.mD.left = i;
        this.mD.top = i2;
        this.mD.right = i3;
        this.mD.bottom = i4;
        M(this.direction);
    }

    public void a(Rect rect) {
        this.mD = rect;
        M(this.direction);
    }

    public void a(Drawable drawable) {
        this.ms = drawable;
    }

    public void a(q qVar) {
        this.mE = qVar;
    }

    public boolean a(byte b2, int i, int i2) {
        switch (b2) {
            case 0:
                this.mA = n(i, i2);
                if (this.mA < 0) {
                    return true;
                }
                Ix();
                return true;
            case 1:
                if (this.mA == -1 || this.mA != n(i, i2)) {
                    return true;
                }
                k(i, i2);
                this.mA = -1;
                Ix();
                return true;
            case 2:
                if (this.mA == -1 || this.mA == n(i, i2)) {
                    return true;
                }
                this.mA = -1;
                Ix();
                return true;
            default:
                return true;
        }
    }

    public boolean a(w wVar) {
        if (this.mB == null) {
            this.mB = new ArrayList();
        }
        this.mB.add(wVar);
        M(this.direction);
        return true;
    }

    public boolean a(w[] wVarArr) {
        if (this.mB == null) {
            this.mB = new ArrayList();
        }
        if (wVarArr != null) {
            for (w add : wVarArr) {
                this.mB.add(add);
            }
        }
        M(this.direction);
        return true;
    }

    public void b(Drawable drawable) {
        this.my = drawable;
        if (this.direction == 0) {
            M(this.direction);
        }
    }

    public void c(Drawable drawable) {
        this.mz = drawable;
        if (this.direction == 1) {
            M(this.direction);
        }
    }

    public boolean cY() {
        if (this.mB == null) {
            return false;
        }
        this.mB.clear();
        return true;
    }

    public Drawable cZ() {
        return this.my;
    }

    public void d(Drawable drawable) {
        this.mv = drawable;
    }

    public Drawable da() {
        return this.mz;
    }

    public void draw(Canvas canvas) {
        Drawable drawable = this.direction == 0 ? this.my : this.mz;
        if (drawable != null) {
            drawable.setBounds(0, 0, getWidth(), getHeight());
            drawable.draw(canvas);
        }
        if (this.mB != null) {
            canvas.save();
            canvas.translate((float) this.paddingLeft, (float) this.paddingTop);
            for (int i = 0; i < this.mC.size(); i++) {
                if (i == this.mA) {
                    if (this.mv != null) {
                        this.mv.setBounds(0, 0, (int) (((Float) this.mC.get(i)).floatValue() + ((float) this.mD.right) + ((float) this.mD.left)), (int) ((((float) (this.mD.top + this.mD.bottom)) + this.pL.descent()) - this.pL.ascent()));
                        this.mv.draw(canvas);
                    }
                    this.pL.setColor(this.textColorHighlight);
                } else {
                    this.pL.setColor(this.textColor);
                }
                a((w) this.mB.get(i), canvas);
                canvas.translate(((Float) this.mC.get(i)).floatValue(), 0.0f);
                canvas.translate((float) (this.mD.right + this.mD.left), 0.0f);
                if (i < this.mC.size() - 1) {
                    b(canvas);
                    canvas.translate((float) this.mt, 0.0f);
                }
            }
            canvas.restore();
        }
    }

    public int getDirection() {
        return this.direction;
    }

    /* access modifiers changed from: protected */
    public boolean k(int i, int i2) {
        int n = n(i, i2);
        if (this.mA >= 0) {
            this.mA = -1;
            Ix();
        }
        if (n < 0 || this.mB == null || n >= this.mB.size()) {
            return false;
        }
        if (this.mE != null) {
            this.mE.fJ(((w) this.mB.get(n)).id);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public int n(int i, int i2) {
        if (i2 < this.paddingTop || i2 > getHeight() - this.paddingBottom || this.mC == null || this.mB == null) {
            return -1;
        }
        int i3 = this.mD != null ? this.mD.left + this.mD.right : 0;
        int i4 = i - this.paddingLeft;
        for (int i5 = 0; i5 < this.mC.size(); i5++) {
            int floatValue = (int) (((float) i4) - (((Float) this.mC.get(i5)).floatValue() + ((float) i3)));
            if (floatValue <= 0) {
                return i5;
            }
            i4 = floatValue - this.mt;
        }
        return -1;
    }

    public void setDirection(int i) {
        this.direction = i;
        M(i);
    }

    public void setTextColor(int i) {
        this.textColor = i;
        this.pL.setColor(i);
    }

    public void setTextSize(float f) {
        this.pL.setTextSize(f);
        M(this.direction);
    }
}
