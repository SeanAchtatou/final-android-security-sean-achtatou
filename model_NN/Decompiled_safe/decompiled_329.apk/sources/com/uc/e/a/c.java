package com.uc.e.a;

import android.graphics.Canvas;
import com.uc.browser.R;
import com.uc.browser.UCR;
import com.uc.e.z;
import com.uc.h.e;

public class c extends z {
    private String Bo;
    private boolean Bp = false;
    private int textColor;

    public c(String str) {
        this.Bo = str;
        s(e.Pb().getDrawable(UCR.drawable.aWR));
        this.textColor = e.Pb().getColor(8);
        this.pL.setColor(this.textColor);
        this.pL.setTextSize((float) e.Pb().kp(R.dimen.f314ui_dialog_button_textsize));
        bL(true);
    }

    public void H(boolean z) {
        bL(z);
        if (!z) {
            this.pL.setColor(this.pL.getColor() & -1996488705);
            getBackground().setState(new int[]{-16842910});
            Ix();
            return;
        }
        reset();
    }

    public boolean a(byte b2, int i, int i2) {
        if (!this.bBg) {
            return false;
        }
        switch (b2) {
            case 0:
                this.Bp = false;
                iN();
                break;
            case 1:
                if (!this.Bp) {
                    performClick();
                }
                reset();
                break;
            case 2:
                if (i > getWidth() || i < 0 || i2 < 0 || i2 > getHeight()) {
                    this.Bp = true;
                    reset();
                    break;
                }
            default:
                return true;
        }
        return true;
    }

    public void aV(int i) {
        this.pL.setTextSize((float) i);
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        canvas.drawText(this.Bo, (((float) getWidth()) - this.pL.measureText(this.Bo)) / 2.0f, ((((float) getHeight()) - this.pL.descent()) - this.pL.ascent()) / 2.0f, this.pL);
    }

    public void iN() {
        setTextColor(e.Pb().getColor(9));
        getBackground().setState(new int[]{16842919});
        Ix();
    }

    /* access modifiers changed from: protected */
    public boolean k(int i, int i2) {
        reset();
        return super.k(i, i2);
    }

    public void reset() {
        setTextColor(e.Pb().getColor(8));
        getBackground().setState(new int[]{16842914});
        Ix();
    }

    public void setTextColor(int i) {
        this.pL.setColor(i);
    }
}
