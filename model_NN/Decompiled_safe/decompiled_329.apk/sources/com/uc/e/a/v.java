package com.uc.e.a;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import com.uc.browser.R;
import com.uc.browser.UCR;
import com.uc.e.h;
import com.uc.e.o;
import com.uc.e.z;
import com.uc.h.a;
import com.uc.h.e;

public class v extends z implements a {
    public static final int bpm = 120;
    public static final int bpn = 120;
    private static final float[] bqn = {0.667f, 0.667f};
    public static final int bqo = 0;
    public static final int bqp = 1;
    public int aSP;
    public int aSQ;
    /* access modifiers changed from: private */
    public x aux;
    private Animation bpA;
    private Animation bpB;
    /* access modifiers changed from: private */
    public String bpC;
    private String bpD;
    private o bpE;
    private o bpF;
    private h bpG;
    private Transformation bpH = new Transformation();
    private Transformation bpI = new Transformation();
    private boolean bpJ;
    private boolean bpK;
    private boolean bpL;
    private boolean bpM;
    private boolean bpN;
    private int bpO = 100;
    private int bpP = 0;
    private int bpQ = -1;
    private int bpR;
    private int bpS;
    private int bpT = e.Pb().kp(R.dimen.f532add_sch_bookmark_width);
    private int bpU = e.Pb().kp(R.dimen.f477add_sch_button_width);
    private int bpV = e.Pb().kp(R.dimen.f480add_sch_difference_left);
    private int bpW = e.Pb().kp(R.dimen.f481add_sch_difference_right);
    private int bpX = e.Pb().kp(R.dimen.f487add_sch_text_margin_left2);
    private int bpY = e.Pb().kp(R.dimen.f485add_sch_text_paddingtop);
    private int bpZ = e.Pb().kp(R.dimen.f490add_sch_address_paddingleft);
    private Drawable bpo;
    private Drawable bpp;
    private Drawable bpq;
    private Drawable bpr;
    private Drawable bps;
    private Drawable bpt;
    private Drawable bpu;
    private Rect bpv;
    private boolean bpw;
    private boolean bpx;
    private boolean bpy;
    private Animation bpz;
    private int bqa = e.Pb().kp(R.dimen.f489add_sch_address_paddingtop);
    private int bqb = e.Pb().kp(R.dimen.f491add_sch_address_paddingright);
    private final int bqc = 1;
    private final int bqd = 2;
    private final int bqe = 3;
    private final int bqf = 4;
    private final int bqg = 5;
    private int bqh = -1;
    private String bqi = "搜索";
    private Paint bqj = new Paint();
    private Paint bqk;
    private int bql;
    private int bqm = e.Pb().getColor(32);
    private Drawable cz;
    private int paddingLeft = e.Pb().kp(R.dimen.f478add_sch_offset_left);
    private int paddingTop = e.Pb().kp(R.dimen.f479add_sch_offset_top);
    private int textColor = e.Pb().getColor(31);

    public v() {
        this.bqj.setAntiAlias(true);
        this.bqj.setColor(this.textColor);
        this.bqj.setTextSize((float) e.Pb().kp(R.dimen.f484add_sch_textsize));
        this.bqk = new Paint();
        this.bqk.setColor(this.textColor);
        this.bqk.setTextSize((float) e.Pb().kp(R.dimen.f492add_sch_address_size));
        this.bqk.setAntiAlias(true);
        Fj();
        k();
        e.Pb().a(this);
    }

    private boolean Fk() {
        return this.bpo == null || this.bpp == null || this.bpq == null || this.bpr == null || this.bps == null || this.bpt == null || this.bpu == null;
    }

    private void a(Canvas canvas, Rect rect) {
        Rect rect2 = new Rect();
        rect2.left = rect.left + this.bpv.left;
        rect2.right = rect.right - this.bpv.right;
        rect2.top = rect.top + this.bpv.top;
        rect2.bottom = rect.bottom - this.bpv.bottom;
        int color = this.bqj.getColor();
        float textSize = this.bqj.getTextSize();
        this.bqj.setTextSize((float) e.Pb().kp(R.dimen.f529add_sch_state_text));
        float measureText = this.bqj.measureText("取消");
        this.bqj.setColor(e.Pb().getColor(27));
        canvas.drawText("取消", ((((float) (rect2.right - rect2.left)) - measureText) / 2.0f) + ((float) rect2.left), (((((float) (rect2.bottom - rect2.top)) - this.bqj.ascent()) - this.bqj.descent()) / 2.0f) + ((float) rect2.top), this.bqj);
        this.bqj.setColor(color);
        this.bqj.setTextSize(textSize);
    }

    public void Fi() {
        this.bpz = new ScaleAnimation(1.0f, ((float) this.aSP) / ((float) this.bpR), 1.0f, 1.0f);
        this.bpz.setDuration(120);
        this.bpz.setStartTime(-1);
        this.bpA = new ScaleAnimation(1.0f, ((float) (this.aSP - this.bpT)) / ((float) this.bpS), 1.0f, 1.0f);
        this.bpA.setDuration(120);
        this.bpA.setStartTime(-1);
        this.bpB = new TranslateAnimation((float) this.aSP, (float) (this.aSP - this.bpU), 0.0f, 0.0f);
        this.bpB.setDuration(240);
        this.bpB.setStartTime(-1);
        this.bpB.initialize(0, 0, 0, 0);
    }

    public void Fj() {
        this.bpJ = false;
        this.bpx = false;
        this.bpL = false;
        this.bpw = false;
        this.bpx = false;
        this.bpM = false;
        this.bpN = false;
    }

    public int Fl() {
        return this.bpQ;
    }

    public String Fm() {
        return this.bpC;
    }

    public boolean Fn() {
        return this.bpP < 96;
    }

    public void Fo() {
        this.bpw = false;
        this.bpx = false;
        this.bpy = false;
    }

    public void a(x xVar) {
        this.aux = xVar;
        a(new r(this));
        b(new t(this));
        d(new s(this));
    }

    public void a(o oVar) {
        this.bpE = oVar;
    }

    public void b(o oVar) {
        this.bpF = oVar;
    }

    public void bz(boolean z) {
        this.bpy = z;
    }

    public void d(h hVar) {
        this.bpG = hVar;
    }

    public void eF(String str) {
        this.bpC = str;
        this.bpD = str;
        if (str == null) {
            this.bpC = "";
            this.bpD = "";
        } else if (!"".equals(str) && this.bqk != null && this.aSP != 0) {
            int measureText = (int) this.bqk.measureText(this.bpD);
            int measureText2 = (int) this.bqk.measureText("...");
            if (measureText > (this.bpS - this.bpZ) - this.bqb) {
                while (measureText > (this.bpS - this.bpZ) - this.bqb) {
                    this.bpD = this.bpD.substring(0, this.bpD.length() - 1);
                    measureText = ((int) this.bqk.measureText(this.bpD)) + measureText2;
                }
                this.bpD += "...";
            }
        }
    }

    public int getMax() {
        return this.bpO;
    }

    public void hw(int i) {
        this.bpQ = i;
    }

    public void init(int i) {
        this.bpS = (int) (((float) this.aSP) * bqn[i]);
        this.bpR = this.aSP - this.bpS;
        if (this.bpC != null) {
            eF(this.bpC);
        }
        Fi();
    }

    public void k() {
        e Pb = e.Pb();
        this.bpu = Pb.getDrawable(UCR.drawable.aXx);
        this.bpp = Pb.getDrawable(UCR.drawable.fi);
        this.bps = Pb.getDrawable(UCR.drawable.aTt);
        this.bpo = Pb.getDrawable(UCR.drawable.il);
        this.bpr = Pb.getDrawable(UCR.drawable.aWj);
        this.bpq = Pb.getDrawable(10000);
        this.bpt = Pb.getDrawable(UCR.drawable.aTs);
        this.cz = Pb.getDrawable(UCR.drawable.aTu);
        this.bpv = new Rect();
        this.cz.getPadding(this.bpv);
        this.textColor = Pb.getColor(31);
        this.bqm = Pb.getColor(32);
        if (this.bqj != null) {
            this.bqj.setColor(this.textColor);
        }
        if (this.bqk != null) {
            this.bqk.setColor(this.textColor);
        }
    }

    public void onDraw(Canvas canvas) {
        if (!Fk()) {
            this.bpu.setBounds(0, 0, this.aSP, this.aSQ);
            this.bpu.draw(canvas);
            if (this.bpw) {
                long currentTimeMillis = System.currentTimeMillis();
                boolean transformation = this.bpz.getTransformation(currentTimeMillis, this.bpH);
                boolean transformation2 = this.bpB.getTransformation(currentTimeMillis, this.bpI);
                if (transformation || transformation2) {
                    float[] fArr = new float[9];
                    this.bpI.getMatrix().getValues(fArr);
                    int i = (int) fArr[2];
                    this.cz.setBounds(i, 0, this.bpU + i, this.aSQ);
                    this.cz.draw(canvas);
                    a(canvas, new Rect(i, 0, this.bpU + i, this.aSQ));
                    float[] fArr2 = new float[9];
                    this.bpH.getMatrix().getValues(fArr2);
                    int i2 = (int) (fArr2[0] * ((float) this.bpR));
                    int i3 = this.bpR - i2;
                    int i4 = (this.aSP - this.bpU) + this.bpW;
                    this.bpo.setBounds(this.aSP - i2 < this.bpV + this.paddingLeft ? this.bpV + this.paddingLeft : this.aSP - i2, this.paddingTop, this.aSP + i3 < i4 ? i4 : i3 + this.aSP, this.aSQ);
                    this.bpo.draw(canvas);
                    canvas.drawText(this.bqi, (float) (this.bpo.getBounds().left + this.bpX), this.bqj.getTextSize() + ((float) this.bpY), this.bqj);
                    this.bpp.setBounds(this.bpR - i2, this.paddingTop, this.aSP - i2, this.aSQ);
                    this.bpp.draw(canvas);
                    if (!(this.bpD == null || this.bpD.length() == 0)) {
                        this.bpq.setBounds(this.bpR - i2, this.paddingTop, (this.bpR - i2) + this.bpT, this.aSQ);
                        this.bpq.draw(canvas);
                    }
                    Ix();
                    return;
                }
                this.cz.setBounds(this.aSP - this.bpU, 0, this.aSP, this.aSQ);
                this.cz.draw(canvas);
                a(canvas, new Rect(this.aSP - this.bpU, 0, this.aSP, this.aSQ));
                this.bpo.setBounds(this.bpV + this.paddingLeft, this.paddingTop, (this.aSP - this.bpU) + this.bpW, this.aSQ);
                this.bpo.draw(canvas);
                canvas.drawText(this.bqi, (float) (this.bpo.getBounds().left + this.bpX), this.bqj.getTextSize() + ((float) this.bpY), this.bqj);
                this.bpz.reset();
                this.bpz.setStartTime(-1);
                this.bpB.reset();
                this.bpB.setStartTime(-1);
                this.bpw = false;
                if (this.bpE != null) {
                    this.bpE.ww();
                }
            } else if (!this.bpx) {
                if (this.bpK) {
                    this.bps.setBounds(this.paddingLeft, this.paddingTop, this.bpS - this.paddingLeft, this.aSQ);
                    this.bps.draw(canvas);
                } else {
                    this.bpp.setBounds(this.paddingLeft, this.paddingTop, this.bpS - this.paddingLeft, this.aSQ);
                    this.bpp.draw(canvas);
                }
                if (this.bpJ) {
                    this.bpr.setBounds((this.aSP - this.bpR) - this.paddingLeft, this.paddingTop, this.aSP - this.paddingLeft, this.aSQ);
                    this.bpr.draw(canvas);
                } else {
                    this.bpo.setBounds((this.aSP - this.bpR) - this.paddingLeft, this.paddingTop, this.aSP - this.paddingLeft, this.aSQ);
                    this.bpo.draw(canvas);
                }
                if (this.bpD == null || this.bpD.length() == 0) {
                    if (this.bpK) {
                        this.bqj.setColor(this.bqm);
                    } else {
                        this.bqj.setColor(this.textColor);
                    }
                    canvas.drawText("输入网址", (float) (this.bpT + this.paddingLeft + this.bpZ), this.bqj.getTextSize() + ((float) this.bqa), this.bqj);
                } else if (this.bpL) {
                    this.bpt.setBounds(this.paddingLeft, this.paddingTop, this.bpT + this.paddingLeft, this.aSQ);
                    this.bpt.draw(canvas);
                } else {
                    this.bpq.setBounds(this.paddingLeft, this.paddingTop, this.bpT + this.paddingLeft, this.aSQ);
                    this.bpq.draw(canvas);
                }
                if (this.bpJ) {
                    this.bqj.setColor(this.bqm);
                } else {
                    this.bqj.setColor(this.textColor);
                }
                canvas.drawText(this.bqi, (float) (this.bpo.getBounds().left + this.bpX), this.bqj.getTextSize() + ((float) this.bpY), this.bqj);
                this.bqj.setColor(this.textColor);
                if (this.bpK) {
                    this.bqk.setColor(this.bqm);
                } else {
                    this.bqk.setColor(this.textColor);
                }
                canvas.clipRect(0, 0, this.bpS - this.bqb, this.aSQ);
                if (this.bpD != null) {
                    canvas.drawText(this.bpD, (float) (this.bpq.getBounds().right + this.bpZ), this.bqk.getTextSize() + ((float) this.bqa), this.bqk);
                }
                this.bqk.setColor(this.textColor);
            } else if (this.bpA.getTransformation(System.currentTimeMillis(), this.bpH)) {
                float[] fArr3 = new float[9];
                this.bpH.getMatrix().getValues(fArr3);
                int i5 = (int) (fArr3[0] * ((float) this.bpS));
                int i6 = i5 > this.aSP - this.bpU ? this.aSP - this.bpU : i5;
                this.bpp.setBounds(this.paddingLeft, this.paddingTop, i6, this.aSQ);
                this.bpp.draw(canvas);
                if (this.bpD == null || this.bpD.length() == 0) {
                    canvas.drawText("输入网址", (float) (this.bpT + this.paddingLeft + this.bpZ), this.bqj.getTextSize() + ((float) this.bqa), this.bqj);
                } else {
                    this.bpq.setBounds(this.paddingLeft, this.paddingTop, this.bpT + this.paddingLeft, this.aSQ);
                    this.bpq.draw(canvas);
                }
                this.bqk.setColor(this.textColor);
                canvas.clipRect(0, 0, i6, this.aSQ);
                if (this.bpD != null) {
                    canvas.drawText(this.bpD, (float) (this.bpq.getBounds().right + this.bpZ), this.bqk.getTextSize() + ((float) this.bqa), this.bqk);
                }
                this.bpo.setBounds(this.bpT + i5, this.paddingTop, i5 + this.bpT + this.bpR, this.aSQ);
                this.bpo.draw(canvas);
                canvas.drawText(this.bqi, (float) (this.bpo.getBounds().left + this.bpX), this.bqj.getTextSize() + ((float) this.bpY), this.bqj);
                Ix();
            } else if (this.bpB.getTransformation(System.currentTimeMillis(), this.bpI)) {
                float[] fArr4 = new float[9];
                this.bpI.getMatrix().getValues(fArr4);
                int i7 = (int) fArr4[2];
                this.cz.setBounds(i7, 0, this.bpU + i7, this.aSQ);
                this.cz.draw(canvas);
                a(canvas, new Rect(i7, 0, this.bpU + i7, this.aSQ));
                this.bpp.setBounds(this.paddingLeft, this.paddingTop, this.aSP - this.bpU, this.aSQ);
                this.bpp.draw(canvas);
                canvas.clipRect(0, 0, this.aSP - this.bpU, this.aSQ);
                if (this.bpD != null) {
                    canvas.drawText(this.bpD, (float) (this.bpq.getBounds().right + this.bpZ), this.bqk.getTextSize() + ((float) this.bqa), this.bqk);
                }
                if (this.bpD == null || this.bpD.length() == 0) {
                    canvas.drawText("输入网址", (float) (this.bpT + this.paddingLeft + this.bpZ), this.bqj.getTextSize() + ((float) this.bqa), this.bqj);
                } else {
                    this.bpq.setBounds(this.paddingLeft, this.paddingTop, this.bpT + this.paddingLeft, this.aSQ);
                    this.bpq.draw(canvas);
                }
                Ix();
            } else {
                this.cz.setBounds(this.aSP - this.bpU, 0, this.aSP, this.aSQ);
                this.cz.draw(canvas);
                a(canvas, new Rect(this.aSP - this.bpU, 0, this.aSP, this.aSQ));
                this.bpp.setBounds(this.paddingLeft, this.paddingTop, this.aSP - this.bpU, this.aSQ);
                this.bpp.draw(canvas);
                if (this.bpD == null || this.bpD.length() == 0) {
                    canvas.drawText("输入网址", (float) (this.bpT + this.paddingLeft + this.bpZ), this.bqj.getTextSize() + ((float) this.bqa), this.bqj);
                } else {
                    this.bpq.setBounds(this.paddingLeft, this.paddingTop, this.bpT + this.paddingLeft, this.aSQ);
                    this.bpq.draw(canvas);
                }
                canvas.clipRect(0, 0, this.aSP - this.bpU, this.aSQ);
                if (this.bpD != null) {
                    canvas.drawText(this.bpD, (float) (this.bpq.getBounds().right + this.bpZ), this.bqk.getTextSize() + ((float) this.bqa), this.bqk);
                }
                this.bpA.reset();
                this.bpA.setStartTime(-1);
                this.bpB.reset();
                this.bpB.setStartTime(-1);
                this.bpx = false;
                if (this.bpF != null) {
                    this.bpF.ww();
                }
            }
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        switch (motionEvent.getAction()) {
            case 0:
                if (!this.bpy) {
                    if (x <= this.bpS) {
                        if (x <= this.bpT) {
                            if (!(this.bpD == null || this.bpD.length() == 0)) {
                                this.bpL = true;
                            }
                            this.bqh = 5;
                            Ix();
                            break;
                        } else {
                            this.bpK = true;
                            this.bqh = 2;
                            Ix();
                            break;
                        }
                    } else {
                        this.bpJ = true;
                        this.bqh = 1;
                        Ix();
                        break;
                    }
                } else if (x <= this.aSP - this.bpU) {
                    if (x > this.bpT && x < this.aSP - this.bpU) {
                        this.bqh = 4;
                        this.bpN = true;
                        Ix();
                        break;
                    } else {
                        if (!(this.bpD == null || this.bpD.length() == 0)) {
                            this.bpL = true;
                        }
                        this.bqh = 5;
                        Ix();
                        break;
                    }
                } else {
                    this.bqh = 3;
                    this.bpM = true;
                    Ix();
                    break;
                }
                break;
            case 1:
                if (this.bpM) {
                    this.aux.onCancel();
                    this.bpM = false;
                    Ix();
                }
                if (this.bpN) {
                    this.bpN = false;
                    this.aux.kP();
                    Ix();
                }
                if (this.bpJ) {
                    this.bpJ = false;
                    this.bpw = true;
                    Ix();
                }
                if (this.bpK) {
                    this.bpK = false;
                    this.bpx = true;
                    Ix();
                }
                if (this.bpL) {
                    this.bpL = false;
                    Ix();
                    this.bpG.b(this, 0);
                }
                this.bqh = -1;
                break;
            case 2:
                if (!this.bpy) {
                    if (this.bqh == 1) {
                        if (this.bpJ) {
                            if (x > this.aSP || x < this.aSP - this.bpR || y < 0 || y > this.aSQ) {
                                this.bpJ = false;
                                Ix();
                            }
                        } else if (x > this.aSP - this.bpR && x < this.aSP && y > 0 && y < this.aSQ) {
                            this.bpJ = true;
                            Ix();
                        }
                    }
                    if (this.bqh == 5) {
                        if (this.bpL) {
                            if (x > this.bpT || x < 0 || y < 0 || y > this.aSQ) {
                                this.bpL = false;
                                Ix();
                            }
                        } else if (x > 0 && x < this.bpT && y > 0 && y < this.aSQ && this.bpD != null && this.bpD.length() != 0) {
                            this.bpL = true;
                            Ix();
                        }
                    }
                    if (this.bqh == 2) {
                        if (!this.bpK) {
                            if (x < this.aSP - this.bpR && x > 0 && y > 0 && y < this.aSQ) {
                                this.bpK = true;
                                Ix();
                                break;
                            }
                        } else if (x > this.aSP - this.bpR || x < 0 || y < 0 || y > this.aSQ) {
                            this.bpK = false;
                            Ix();
                            break;
                        }
                    }
                } else {
                    if (this.bqh == 3) {
                        if (this.bpM) {
                            if (x > this.aSP || x < this.aSP - this.bpU || y < 0 || y > this.aSQ) {
                                this.bpM = false;
                                Ix();
                            }
                        } else if (x < this.aSP && x > this.aSP - this.bpU && y > 0 && y < this.aSQ) {
                            this.bpM = true;
                            Ix();
                        }
                    }
                    if (this.bqh == 4) {
                        if (this.bpN) {
                            if (x > this.aSP - this.bpU || x < this.bpT || y < 0 || y > this.aSQ) {
                                this.bpN = false;
                                Ix();
                            }
                        } else if (x < this.aSP - this.bpU && x > this.bpT && y > 0 && y < this.aSQ) {
                            this.bpN = true;
                            Ix();
                        }
                    }
                    if (this.bqh == 5) {
                        if (!this.bpL) {
                            if (x > 0 && x < this.bpT && y > 0 && y < this.aSQ && this.bpD != null && this.bpD.length() != 0) {
                                this.bpL = true;
                                Ix();
                                break;
                            }
                        } else if (x > this.bpT || x < 0 || y < 0 || y > this.aSQ) {
                            this.bpL = false;
                            Ix();
                            break;
                        }
                    }
                }
                break;
        }
        return true;
    }

    public void setMax(int i) {
        this.bpO = i;
    }

    public void setProgress(int i) {
        if (this.bql != this.bpO || i != this.bpO) {
            this.bql = this.bpP;
            this.bpP = i;
            this.bpy = true;
            invalidate();
        }
    }

    public void setSize(int i, int i2) {
        this.aSP = i;
        this.aSQ = i2;
        this.bpu.setBounds(0, 0, this.aSP, this.aSQ);
    }
}
