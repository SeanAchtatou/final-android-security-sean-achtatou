package com.uc.e.a;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Log;
import android.view.KeyEvent;
import b.a.a.e;
import java.util.Vector;

public class b extends com.uc.e.b {
    private static final String tag = "NavigationView";
    private int mA = -2;
    private Vector vj;
    private int vk = 45;
    private int vl = 25;
    private int vm = 5;
    private int vn;
    private int vo;
    private int vp = 16;
    private int vq = -1;
    private int vr = -2;

    public b() {
        this.bBg = true;
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, byte b2) {
        super.a(canvas, ((this.vr == -1 || b2 != 1) && (this.mA == -1 || b2 != 2)) ? b2 : 0);
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, Paint paint) {
        if (!this.dF || this.dE == null || !this.dE.isMutable()) {
            canvas.save();
            int size = this.vj == null ? 0 : this.vj.size();
            canvas.translate(0.0f, (float) (this.vo >> 1));
            int i = 0;
            for (int i2 = 0; i2 < size; i2++) {
                if (i == 0) {
                    i += this.vn >> 1;
                    canvas.translate((float) i, 0.0f);
                }
                canvas.drawText(((e) this.vj.get(i2)).abM, 0.0f, 19.0f, paint);
                canvas.translate((float) (this.vk + this.vn), 0.0f);
                i = this.vk + this.vn + i;
                if (i > this.aSP - this.vk) {
                    canvas.translate((float) (-i), (float) (this.vl + this.vo));
                    i = 0;
                }
            }
            canvas.restore();
        } else {
            canvas.drawBitmap(this.dE, 0.0f, 0.0f, paint);
        }
        if (this.mA >= 0) {
            canvas.save();
            canvas.translate((float) (((this.mA % this.vm) * (this.vk + this.vn)) + (this.vn / 2)), (float) (((this.mA / this.vm) * (this.vl + this.vo)) + (this.vo / 2)));
            paint.setColor(-2012047274);
            canvas.drawRoundRect(new RectF(0.0f, 0.0f, (float) this.vk, (float) this.vl), 5.0f, 5.0f, paint);
            paint.setColor(this.vq);
            canvas.drawText(((e) this.vj.get(this.mA)).abM, 0.0f, 19.0f, paint);
            canvas.restore();
        }
        if (this.vr >= 0) {
            canvas.save();
            int i3 = this.vr;
            canvas.translate((float) (((i3 % this.vm) * (this.vk + this.vn)) + (this.vn / 2)), (float) (((i3 / this.vm) * (this.vl + this.vo)) + (this.vo / 2)));
            paint.setColor(-1713563450);
            canvas.drawRoundRect(new RectF(0.0f, 0.0f, (float) this.vk, (float) this.vl), 5.0f, 5.0f, paint);
            paint.setColor(this.vq);
            canvas.drawText(((e) this.vj.get(i3)).abM, 0.0f, 19.0f, paint);
            canvas.restore();
        }
    }

    public boolean a(byte b2, int i, int i2) {
        if (this.vr > 0) {
            this.vr = -this.vr;
        }
        boolean a2 = super.a(b2, i, i2);
        switch (b2) {
            case 0:
                int n = n(i, i2);
                this.mA = n;
                if (n < -1) {
                    return a2;
                }
                Ix();
                return true;
            case 1:
                this.mA = -1;
                Ix();
                return a2;
            case 2:
                if (this.mA < 0 || this.mA == n(i, i2)) {
                    return a2;
                }
                this.mA = -1;
                Ix();
                return a2;
            default:
                return a2;
        }
    }

    public boolean a(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            if (this.dx) {
                this.vr = -1;
                return false;
            }
            int size = this.vj == null ? 0 : this.vj.size();
            switch (keyEvent.getKeyCode()) {
                case 19:
                    if (this.vr < -1) {
                        this.vr = size - 1;
                        Ix();
                    } else if (this.vr == -1) {
                        this.vr = -2;
                        Ix();
                        return false;
                    } else if (this.vr - this.vm >= 0) {
                        this.vr -= this.vm;
                        Ix();
                    } else if (this.vr >= this.vm || this.vr < 0) {
                        this.vr = -2;
                        Ix();
                        return false;
                    } else {
                        this.vr = -1;
                        Ix();
                    }
                    return true;
                case 20:
                    if (this.vr < -1) {
                        this.vr = -1;
                        Ix();
                    } else if (this.vr == -1 && size > 0) {
                        this.vr = 0;
                        Ix();
                    } else if (this.vr + this.vm < size) {
                        this.vr += this.vm;
                        Ix();
                    } else {
                        this.vr = -2;
                        Ix();
                        return false;
                    }
                    return true;
                case 21:
                    if (this.vr <= 0 && size > 0) {
                        this.vr = 1;
                        Ix();
                    } else if (this.vr > 0) {
                        this.vr--;
                        Ix();
                    }
                    return true;
                case 22:
                    if (this.vr == -1) {
                        this.vr = 0;
                        Ix();
                    } else if (this.vr < size) {
                        this.vr++;
                        Ix();
                    }
                    return true;
                case 23:
                case 66:
                    return true;
                default:
                    return false;
            }
        } else if (keyEvent.getAction() != 1) {
            return false;
        } else {
            switch (keyEvent.getKeyCode()) {
                case 19:
                case 20:
                case 21:
                case 22:
                    return false;
                case 23:
                case 66:
                    Log.w(tag, "on up, try toggle expand");
                    if (this.vr == -1) {
                        if (this.dx) {
                            cl();
                            Ix();
                        } else {
                            cm();
                            Ix();
                        }
                    }
                    if (this.vr >= 0) {
                        eZ(this.vr);
                    }
                    return true;
                default:
                    return false;
            }
        }
    }

    public e aI(int i) {
        return (e) this.vj.elementAt(i);
    }

    public void aJ(int i) {
        this.vm = i;
        eL();
    }

    /* access modifiers changed from: protected */
    public Paint ci() {
        if (this.dH == null) {
            this.dH = new Paint();
            this.dH.setAntiAlias(true);
            this.dH.setColor(this.vq);
            this.dH.setTextSize((float) this.vp);
        }
        return this.dH;
    }

    public void cl() {
        this.vr = -2;
        super.cl();
    }

    public void cm() {
        this.vr = -2;
        super.cm();
    }

    public void e(Vector vector) {
        this.vj = vector;
        eL();
    }

    /* access modifiers changed from: protected */
    public void eL() {
        this.vn = (this.aSP / this.vm) - this.vk;
        this.vo = 10;
        int size = this.vj != null ? this.vj.size() : 0;
        if (this.vm <= 0 || size <= 0) {
            this.dz = 0;
        } else {
            this.dz = ((size % this.vm > 0 ? 1 : 0) + (size / this.vm)) * (this.vl + this.vo);
        }
        if (this.dx) {
            this.aSQ = this.dy;
        } else {
            this.aSQ = this.dz + this.dy;
        }
    }

    /* access modifiers changed from: protected */
    public boolean k(int i, int i2) {
        int n;
        boolean k = super.k(i, i2);
        if (k || (n = n(i, i2)) < 0) {
            return k;
        }
        eZ(n);
        this.mA = -2;
        Ix();
        return true;
    }

    /* access modifiers changed from: protected */
    public int n(int i, int i2) {
        if (i2 < this.dy) {
            return -1;
        }
        if (this.vj == null || this.vj.size() == 0) {
            return -2;
        }
        int i3 = (i - (this.vn / 2)) % (this.vk + this.vn);
        int i4 = ((i2 - this.dy) - (this.vo / 2)) % (this.vl + this.vo);
        if (i3 >= this.vk || i4 >= this.vl || i3 * i4 <= 0) {
            return -1;
        }
        int i5 = ((i2 - this.dy) - (this.vo / 2)) / (this.vl + this.vo);
        int i6 = (i5 * this.vm) + ((i - (this.vn / 2)) / (this.vk + this.vn));
        if (i6 >= this.vj.size()) {
            return -2;
        }
        return i6;
    }

    public void setSize(int i, int i2) {
        super.setSize(i, i2);
        eL();
    }
}
