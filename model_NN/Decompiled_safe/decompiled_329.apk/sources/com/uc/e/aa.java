package com.uc.e;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import com.uc.h.a;
import com.uc.h.e;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class aa extends z implements a {
    public static final int GT = 0;
    public static final int GU = 1;
    public static final int NONE = -1;
    private static int bCS = 0;
    private static int bCX = 0;
    public static final int bDA = 2;
    public static final int bDB = 0;
    public static final int bDC = 1;
    public static final int bDD = 2;
    private static final int bDE = 5;
    private static int bDK = 5;
    private static int bDR = 0;
    private static int bDl = 0;
    private static int bDm = 0;
    private static int bDw = -1;
    private static int bDx = -1;
    public static final int bDy = 0;
    public static final int bDz = 1;
    public static final int bqo = 0;
    public static final int bqp = 1;
    Transformation CT = new Transformation();
    private int aAG;
    private t[][] bCT;
    private List[] bCU;
    private int bCV;
    private int bCW;
    private int bCY;
    private int bCZ;
    private int bDF = 6;
    private int bDG = 6;
    private int bDH = 6;
    private int bDI = 0;
    private int[] bDJ = null;
    private int bDL;
    private int bDM;
    private boolean bDN;
    private long bDO = 0;
    private float bDP = 0.0f;
    private float[] bDQ = {1.0f, 0.92f};
    Animation bDS = null;
    private boolean bDT = false;
    private int[] bDa;
    private int[] bDb;
    private Drawable bDc;
    private int bDd;
    private int bDe;
    private int bDf;
    private int bDg;
    private int bDh;
    private int bDi;
    private int bDj;
    private int bDk;
    private Drawable[] bDn;
    private Paint bDo = new Paint();
    private int bDp = 0;
    private int bDq = 0;
    private int bDr;
    private int bDs;
    private Paint bDt = new Paint();
    private l bDu;
    private int bDv = -1;

    public aa() {
        this.bDt.setColor(e.Pb().getColor(80));
        this.bDo.setColor(e.Pb().getColor(81));
        this.bDo.setAntiAlias(true);
        this.bDt.setAntiAlias(true);
        this.bCV = 0;
    }

    private void a(t tVar, Canvas canvas) {
        int i;
        int i2;
        if (tVar != null) {
            canvas.save();
            Drawable icon = tVar.getIcon();
            if (!tVar.zP()) {
                icon.setAlpha(128);
                i = this.bDo.getAlpha();
                this.bDo.setAlpha(128);
            } else {
                i = 0;
            }
            int intrinsicHeight = (int) (((float) (icon != null ? this.bDq != 0 ? this.bDq : icon.getIntrinsicHeight() : 0)) + this.bDo.getTextSize() + ((float) bDK));
            canvas.translate(0.0f, (float) (bCX > intrinsicHeight ? (bCX - intrinsicHeight) / 2 : 0));
            if (icon != null) {
                i2 = (this.bCW - this.bDp) / 2;
                canvas.translate((float) i2, 0.0f);
                icon.draw(canvas);
            } else {
                i2 = 0;
            }
            String text = tVar.getText();
            if (text != null) {
                int zQ = (this.bCW - tVar.zQ()) / 2;
                if (zQ <= 0) {
                    zQ = 0;
                }
                canvas.drawText(text, (float) (zQ - i2), (float) (this.bDq + this.bDr + bDK), this.bDo);
            }
            if (!tVar.zP()) {
                icon.setAlpha(255);
                this.bDo.setAlpha(i);
            }
            canvas.restore();
        }
    }

    private void b(t tVar, Canvas canvas) {
        canvas.save();
        String text = tVar.getText();
        if (text != null) {
            int measureText = ((int) (((float) this.bCY) - this.bDt.measureText(text))) / 2;
            if (measureText <= 0) {
                measureText = 0;
            }
            canvas.drawText(text, (float) measureText, (float) (((this.bCZ - this.bDs) / 2) + this.bDs), this.bDt);
        }
        canvas.restore();
    }

    private void bj(int i, int i2) {
        if (i2 >= this.bDd) {
            return;
        }
        if (this.bDN) {
            this.bDN = false;
            this.bDu.ul();
            return;
        }
        this.bDN = true;
    }

    private boolean d(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            switch (keyEvent.getKeyCode()) {
                case 19:
                    return true;
                case 20:
                    bDx = -1;
                    bDw = -bDw;
                    Ix();
                    return true;
                case 21:
                    if (bDx > 0) {
                        bDx--;
                        Ix();
                    }
                    return true;
                case 22:
                    if (this.bDJ != null && bDx < this.bDJ[0] - 1) {
                        bDx++;
                        Ix();
                    }
                    return true;
                case 23:
                    this.aAG = -(bDx + 1);
                    Ix();
                    return true;
            }
        } else if (keyEvent.getAction() == 1 && keyEvent.getKeyCode() == 23) {
            this.aAG = 0;
            this.bCV = bDx;
            if (bDw > 0) {
                bDw = -bDw;
            }
            if (bDx >= 0) {
                bDx = -1;
            }
            Ix();
            return true;
        }
        return false;
    }

    private boolean e(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            switch (keyEvent.getKeyCode()) {
                case 19:
                    if (bDw < 0) {
                        bDw = -bDw;
                        if (((t[]) this.bCU[bCS].get(this.bCV))[bDw - 1] == null || !((t[]) this.bCU[bCS].get(this.bCV))[bDw - 1].zP()) {
                            int i = bDw;
                            while (i - this.bDb[bCS] > 0) {
                                int i2 = i - this.bDb[bCS];
                                if (((t[]) this.bCU[bCS].get(this.bCV))[i2 - 1] == null || !((t[]) this.bCU[bCS].get(this.bCV))[i2 - 1].zP()) {
                                    i = i2;
                                } else {
                                    bDw = i2;
                                    Ix();
                                    return true;
                                }
                            }
                            bDw = 1;
                        }
                        Ix();
                        break;
                    } else {
                        int i3 = bDw;
                        while (i3 - this.bDb[bCS] > 0) {
                            int i4 = i3 - this.bDb[bCS];
                            if (((t[]) this.bCU[bCS].get(this.bCV))[i4 - 1] == null || !((t[]) this.bCU[bCS].get(this.bCV))[i4 - 1].zP()) {
                                i3 = i4;
                            } else {
                                bDw = i4;
                                Ix();
                                return true;
                            }
                        }
                        bDx = this.bCV;
                        bDw = -bDw;
                        Ix();
                        return true;
                    }
                    break;
                case 20:
                    if (bDw >= 0) {
                        int i5 = bDw;
                        while (i5 + this.bDb[bCS] <= ((t[]) this.bCU[bCS].get(this.bCV)).length) {
                            i5 += this.bDb[bCS];
                            if (((t[]) this.bCU[bCS].get(this.bCV))[i5 - 1] != null && ((t[]) this.bCU[bCS].get(this.bCV))[i5 - 1].zP()) {
                                bDw = i5;
                                Ix();
                                return true;
                            }
                        }
                        break;
                    } else {
                        bDw = -bDw;
                        if (((t[]) this.bCU[bCS].get(this.bCV))[bDw - 1] == null || !((t[]) this.bCU[bCS].get(this.bCV))[bDw - 1].zP()) {
                            int i6 = bDw;
                            while (i6 + this.bDb[bCS] <= ((t[]) this.bCU[bCS].get(this.bCV)).length) {
                                i6 += this.bDb[bCS];
                                if (((t[]) this.bCU[bCS].get(this.bCV))[i6 - 1] != null && ((t[]) this.bCU[bCS].get(this.bCV))[i6 - 1].zP()) {
                                    bDw = i6;
                                    Ix();
                                    return true;
                                }
                            }
                            bDw = 1;
                        }
                        Ix();
                        break;
                    }
                    break;
                case 21:
                    if (bDw >= 0) {
                        int i7 = bDw;
                        while (i7 % this.bDb[bCS] != 1) {
                            int i8 = i7 - 1;
                            if (((t[]) this.bCU[bCS].get(this.bCV))[i8 - 1] == null || !((t[]) this.bCU[bCS].get(this.bCV))[i8 - 1].zP()) {
                                i7 = i8;
                            } else {
                                bDw = i8;
                                Ix();
                                return true;
                            }
                        }
                        break;
                    } else {
                        bDw = -bDw;
                        if (((t[]) this.bCU[bCS].get(this.bCV))[bDw - 1] == null || !((t[]) this.bCU[bCS].get(this.bCV))[bDw - 1].zP()) {
                            int i9 = bDw;
                            while (i9 % this.bDb[bCS] != 1) {
                                int i10 = i9 - 1;
                                if (((t[]) this.bCU[bCS].get(this.bCV))[i10 - 1] == null || !((t[]) this.bCU[bCS].get(this.bCV))[i10 - 1].zP()) {
                                    i9 = i10;
                                } else {
                                    bDw = i10;
                                    Ix();
                                    return true;
                                }
                            }
                            bDw = 1;
                        }
                        Ix();
                        break;
                    }
                case 22:
                    if (bDw >= 0) {
                        int i11 = bDw;
                        while (i11 % this.bDb[bCS] != 0) {
                            int i12 = i11 + 1;
                            if (((t[]) this.bCU[bCS].get(this.bCV))[i12 - 1] == null || !((t[]) this.bCU[bCS].get(this.bCV))[i12 - 1].zP()) {
                                i11 = i12;
                            } else {
                                bDw = i12;
                                Ix();
                                return true;
                            }
                        }
                        break;
                    } else {
                        bDw = -bDw;
                        if (((t[]) this.bCU[bCS].get(this.bCV))[bDw - 1] == null || !((t[]) this.bCU[bCS].get(this.bCV))[bDw - 1].zP()) {
                            int i13 = bDw;
                            while (i13 % this.bDb[bCS] != 0) {
                                int i14 = i13 + 1;
                                if (((t[]) this.bCU[bCS].get(this.bCV))[i14 - 1] == null || !((t[]) this.bCU[bCS].get(this.bCV))[i14 - 1].zP()) {
                                    i13 = i14;
                                } else {
                                    bDw = i14;
                                    Ix();
                                    return true;
                                }
                            }
                            bDw = 1;
                        }
                        Ix();
                        break;
                    }
                case 23:
                    if (bDw < 0) {
                        return true;
                    }
                    if (bDw > 0 && bDw <= ((t[]) this.bCU[bCS].get(this.bCV)).length && ((t[]) this.bCU[bCS].get(this.bCV))[bDw - 1].zP()) {
                        this.aAG = bDw;
                        Ix();
                        return true;
                    }
            }
        } else if (keyEvent.getAction() == 1 && keyEvent.getKeyCode() == 23) {
            if (bDw < 0) {
                return true;
            }
            if (bDw > 0 && bDw <= ((t[]) this.bCU[bCS].get(this.bCV)).length && ((t[]) this.bCU[bCS].get(this.bCV))[bDw - 1].zP()) {
                eZ(((t[]) this.bCU[bCS].get(this.bCV))[bDw - 1].getItemId());
                this.aAG = 0;
                Ix();
                return true;
            }
        }
        return false;
    }

    private Animation hX(int i) {
        TranslateAnimation translateAnimation = new TranslateAnimation(0, 0.0f, 0, 0.0f, 0, (float) i, 0, (float) this.aSQ);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        translateAnimation.setDuration(this.bDO);
        translateAnimation.setStartTime(-1);
        if (translateAnimation != null) {
            translateAnimation.reset();
        }
        return translateAnimation;
    }

    private Animation hY(int i) {
        int i2 = this.aSQ - this.bDf;
        TranslateAnimation translateAnimation = new TranslateAnimation(0, 0.0f, 0, 0.0f, 0, (float) i, 0, (float) i2);
        translateAnimation.setInterpolator(new DecelerateInterpolator());
        translateAnimation.setDuration((long) (this.bDP * (((float) (i - i2)) / ((float) this.bDf))));
        translateAnimation.setStartTime(-1);
        if (translateAnimation != null) {
            translateAnimation.reset();
        }
        return translateAnimation;
    }

    private t ib(int i) {
        if (i != 0) {
            return i < 0 ? this.bCT[bCS][(-i) - 1] : ((t[]) this.bCU[bCS].get(this.bCV))[i - 1];
        }
        return null;
    }

    private void ic(int i) {
        this.bCV = i;
        if (bDw > 0) {
            bDw = -bDw;
        }
        Ix();
    }

    public static void id(int i) {
        bDK = i;
    }

    public static void ig(int i) {
        bDl = i;
    }

    public static void ih(int i) {
        bDm = i;
    }

    public static void ii(int i) {
        bDR = i;
    }

    public static void ij(int i) {
        bCX = i;
    }

    public static void setOrientation(int i) {
        bCS = i;
        bDw = -1;
        bDx = -1;
    }

    public void Jd() {
        for (int i = 0; i <= 1; i++) {
            for (t[] tVarArr : this.bCU[i]) {
                for (t tVar : (t[]) r2.next()) {
                    if (tVar != null) {
                        tVar.getIcon().setBounds(0, 0, this.bDp, this.bDq);
                    }
                }
            }
        }
    }

    public void Je() {
        for (int i = 0; i <= 1; i++) {
            for (t[] tVarArr : this.bCU[i]) {
                for (t tVar : (t[]) r2.next()) {
                    if (tVar != null) {
                        tVar.fz((int) this.bDo.measureText(tVar.getText()));
                    }
                }
            }
        }
    }

    public void Jf() {
        this.bBf = false;
        if (bDw > 0) {
            bDw = -bDw;
        }
        bDx = -1;
        if (this.bDv == 1) {
            this.bDv = 0;
            this.bDT = true;
        }
        this.bDv = 0;
        Ix();
    }

    public void Jg() {
        this.bBf = false;
        if (bDw > 0) {
            bDw = -bDw;
        }
        if (this.bDv == 0) {
            this.bDT = true;
            this.bDv = 1;
        }
        this.bDv = 1;
        Ix();
    }

    public int Jh() {
        return this.bDF;
    }

    public int Ji() {
        return this.bDG;
    }

    public int Jj() {
        return this.bDH;
    }

    public int Jk() {
        return bDK;
    }

    public int Jl() {
        return this.bDL;
    }

    public int Jm() {
        return this.bDM;
    }

    public void L(Drawable drawable) {
        this.bDc = drawable;
        Rect rect = new Rect();
        if (drawable.getPadding(rect)) {
            s(rect.left, rect.top, rect.right, rect.bottom);
        }
    }

    public void a(int i) {
        for (int i2 = 0; i2 < this.bDJ[bCS]; i2++) {
            if (this.bCT[bCS][i2].getItemId() == i && this.bCV != i2) {
                this.bCV = i2;
                if (bDw > 0) {
                    bDw = -bDw;
                }
                Ix();
            }
        }
    }

    public void a(l lVar) {
        this.bDu = lVar;
    }

    public void a(t tVar) {
        for (int i = 0; i < this.bDJ[bCS]; i++) {
            if (this.bCT[bCS][i] == tVar && this.bCV != i) {
                this.bCV = i;
                if (bDw > 0) {
                    bDw = -bDw;
                }
                Ix();
            }
        }
    }

    public void a(t tVar, List list, int[][] iArr) {
        for (int i = 0; i <= 1; i++) {
            if (this.bDJ[i] < 5) {
                t[] tVarArr = new t[(this.bDa[i] * this.bDb[i])];
                for (int i2 = 0; i2 < list.size(); i2++) {
                    int i3 = iArr[i][i2];
                    if (i3 < tVarArr.length) {
                        tVarArr[i3] = (t) list.get(i2);
                    }
                }
                this.bCU[i].add(tVarArr);
                this.bCT[i][this.bDJ[i]] = tVar;
                int[] iArr2 = this.bDJ;
                iArr2[i] = iArr2[i] + 1;
            }
            Iterator it = list.iterator();
            while (it.hasNext()) {
                t tVar2 = (t) it.next();
                if (tVar2.getIcon() != null) {
                    tVar2.getIcon().setBounds(0, 0, this.bDp, this.bDq);
                }
                tVar2.fz((int) this.bDo.measureText(tVar2.getText()));
            }
        }
    }

    public boolean a(byte b2, int i, int i2) {
        if (this.bDS != null) {
            return true;
        }
        switch (b2) {
            case 0:
                this.aAG = n(i, i2);
                if (bDw > 0) {
                    bDw = -bDw;
                }
                if (bDx >= 0) {
                    bDx = -1;
                }
                Ix();
                bj(i, i2);
                return true;
            case 1:
                if (this.aAG != 0) {
                    Ix();
                    t ib = ib(this.aAG);
                    if (ib != null && ib.zP()) {
                        eZ(ib.getItemId());
                    }
                }
                this.aAG = 0;
                bj(i, i2);
                return true;
            case 2:
                if (!(this.aAG == 0 || this.aAG == n(i, i2))) {
                    Ix();
                    this.aAG = 0;
                }
                return true;
            default:
                return false;
        }
    }

    public boolean a(KeyEvent keyEvent) {
        if (!this.bBf) {
            return true;
        }
        return bDx >= 0 ? d(keyEvent) : e(keyEvent);
    }

    public void bi(int i, int i2) {
    }

    public void d(int[] iArr, int[] iArr2) {
        this.bDa = new int[2];
        this.bDb = new int[2];
        this.bDa[1] = iArr2[0];
        this.bDb[1] = iArr2[1];
        this.bDa[0] = iArr[0];
        this.bDb[0] = iArr[1];
        this.bCT = new t[2][];
        this.bCT[1] = new t[5];
        this.bCT[0] = new t[5];
        this.bCU = new ArrayList[2];
        this.bCU[1] = new ArrayList();
        this.bCU[0] = new ArrayList();
        this.bDJ = new int[2];
    }

    public void draw(Canvas canvas) {
        this.bDd = this.aSQ - this.bDf;
        this.bDh = this.bDd + this.bDH;
        canvas.translate(0.0f, (float) this.bDd);
        canvas.save();
        for (int i = 0; i < this.bDJ[bCS]; i++) {
            if (i > 0) {
                canvas.translate((float) this.bCY, 0.0f);
            }
            if (bDx >= 0 && bDx == i) {
                this.bCT[bCS][i].zO()[1].draw(canvas);
            } else if (this.bCV == i) {
                this.bCT[bCS][i].zO()[2].draw(canvas);
            } else if ((-this.aAG) - 1 == i) {
                this.bCT[bCS][i].zO()[2].draw(canvas);
            } else {
                this.bCT[bCS][i].zO()[0].draw(canvas);
            }
            b(this.bCT[bCS][i], canvas);
        }
        canvas.restore();
        canvas.translate(0.0f, (float) this.bCZ);
        if (this.bDc != null) {
            this.bDc.draw(canvas);
        }
        canvas.translate((float) this.bDF, 0.0f);
        for (int i2 = 0; i2 < ((t[]) this.bCU[bCS].get(this.bCV)).length; i2++) {
            canvas.save();
            canvas.translate((float) bDl, (float) bDm);
            if (!(bDw - 1 != i2 || this.bDn[2] == null || ((t[]) this.bCU[bCS].get(this.bCV))[i2] == null)) {
                this.bDn[2].draw(canvas);
            }
            if (this.aAG - 1 == i2) {
                if (!(this.bDn[1] == null || ((t[]) this.bCU[bCS].get(this.bCV))[i2] == null || !((t[]) this.bCU[bCS].get(this.bCV))[i2].zP())) {
                    this.bDn[1].draw(canvas);
                }
            } else if (!(this.bDn[0] == null || ((t[]) this.bCU[bCS].get(this.bCV))[i2] == null)) {
                this.bDn[2].setBounds(0, 0, this.bCW - (bDl * 2), bCX - (bDm * 2));
                this.bDn[0].draw(canvas);
            }
            canvas.restore();
            a(((t[]) this.bCU[bCS].get(this.bCV))[i2], canvas);
            if (i2 % this.bDb[bCS] != this.bDb[bCS] - 1) {
                canvas.translate((float) this.bCW, 0.0f);
            } else {
                canvas.translate((float) ((-this.bCW) * (this.bDb[bCS] - 1)), (float) bCX);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void eL() {
        super.eL();
        this.bCW = ((this.aSP - this.bDF) - this.bDG) / this.bDb[bCS];
        this.bCY = this.aSP / (this.bDJ[bCS] == 0 ? 1 : this.bDJ[bCS]);
        this.bCZ = bDR;
        this.bDf = (bCX * this.bDa[bCS]) + this.bCZ + this.bDH + this.bDI;
        this.bDg = this.aSP;
        this.bDk = this.bDf - this.bDH;
        this.bDj = (this.bDg - this.bDF) - this.bDG;
        this.bDd = this.aSQ;
        this.bDh = this.bDd + this.bDH;
        this.bDe = 0;
        this.bDi = this.bDe + this.bDF;
        if (this.bDc != null) {
            this.bDc.setBounds(0, 0, this.bDg, this.bDf - this.bCZ);
        }
        if (this.bDn[0] != null) {
            this.bDn[0].setBounds(0, 0, this.bCW - (bDl * 2), bCX - (bDm * 2));
        }
        if (this.bDn[1] != null) {
            this.bDn[1].setBounds(0, 0, this.bCW - (bDl * 2), bCX - (bDm * 2));
        }
        if (this.bDn[2] != null) {
            this.bDn[2].setBounds(0, 0, this.bCW - (bDl * 2), bCX - (bDm * 2));
        }
        for (int i = 0; i < this.bDJ[bCS]; i++) {
            Drawable[] zO = this.bCT[bCS][i].zO();
            if (zO != null) {
                for (Drawable drawable : zO) {
                    if (drawable != null) {
                        if (i == this.bDJ[bCS] - 1) {
                            drawable.setBounds(0, 0, (this.aSP % this.bDJ[bCS]) + this.bCY, this.bCZ);
                        } else {
                            drawable.setBounds(0, 0, this.bCY, this.bCZ);
                        }
                    }
                }
            }
        }
        Jd();
    }

    public int getOrientation() {
        return bCS;
    }

    public void h(Drawable[] drawableArr) {
        this.bDn = drawableArr;
    }

    public void hV(int i) {
        this.bDp = i;
    }

    public void hW(int i) {
        this.bDq = i;
    }

    public void hZ(int i) {
        this.bDr = i;
        this.bDo.setTextSize((float) i);
    }

    public int i() {
        return this.bCT[bCS][this.bCV].getItemId();
    }

    public void ia(int i) {
        this.bDs = i;
        this.bDt.setTextSize((float) i);
    }

    public void ie(int i) {
        this.bDL = i;
    }

    /* renamed from: if  reason: not valid java name */
    public void m4if(int i) {
        this.bDM = i;
    }

    public void j() {
        this.bCV = 0;
    }

    public void k() {
        this.bDt.setColor(e.Pb().getColor(80));
        this.bDo.setColor(e.Pb().getColor(81));
        if (this.bDc != null) {
            this.bDc.setBounds(0, 0, this.bDg, this.bDf - this.bCZ);
        }
        if (this.bDn[0] != null) {
            this.bDn[0].setBounds(0, 0, this.bCW - (bDl * 2), bCX - (bDm * 2));
        }
        if (this.bDn[1] != null) {
            this.bDn[1].setBounds(0, 0, this.bCW - (bDl * 2), bCX - (bDm * 2));
        }
        if (this.bDn[2] != null) {
            this.bDn[2].setBounds(0, 0, this.bCW - (bDl * 2), bCX - (bDm * 2));
        }
        for (int i = 0; i < this.bDJ[bCS]; i++) {
            Drawable[] zO = this.bCT[bCS][i].zO();
            if (zO != null) {
                for (Drawable drawable : zO) {
                    if (drawable != null) {
                        if (i == this.bDJ[bCS] - 1) {
                            drawable.setBounds(0, 0, (this.aSP % this.bDJ[bCS]) + this.bCY, this.bCZ);
                        } else {
                            drawable.setBounds(0, 0, this.bCY, this.bCZ);
                        }
                    }
                }
            }
        }
        Jd();
    }

    public int l() {
        return this.bDf;
    }

    /* access modifiers changed from: protected */
    public int n(int i, int i2) {
        if (i2 > this.bDh && i2 <= (this.bDh + this.bCZ) - this.bDH) {
            for (int i3 = 1; i3 <= this.bDJ[bCS]; i3++) {
                if (i > (i3 - 1) * this.bCY && i < this.bCY * i3) {
                    return -i3;
                }
            }
        }
        if (i2 > this.bDh + this.bCZ && i2 <= this.bDh + this.bDk) {
            int i4 = 0;
            while (true) {
                if (i4 < this.bDa[bCS]) {
                    if (i2 > (bCX * i4) + ((this.bDh + this.bCZ) - this.bDH) && i2 < ((i4 + 1) * bCX) + ((this.bDh + this.bCZ) - this.bDH)) {
                        break;
                    }
                    i4++;
                } else {
                    i4 = -1;
                    break;
                }
            }
            int i5 = (i - this.bDF) / this.bCW;
            if (i5 >= this.bDb[bCS]) {
                i5 = -1;
            }
            if (!(i5 == -1 || i4 == -1)) {
                return (i4 * this.bDb[bCS]) + i5 + 1;
            }
        }
        return 0;
    }

    public void reset() {
        this.bDv = -1;
        this.bDS = null;
        this.bBf = true;
        this.bDd = this.aSQ;
    }

    public void s(int i, int i2, int i3, int i4) {
        this.bDF = i;
        this.bDH = i2;
        this.bDG = i3;
        this.bDI = i4;
        eL();
    }
}
