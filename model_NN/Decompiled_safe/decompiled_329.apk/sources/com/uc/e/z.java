package com.uc.e;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.KeyEvent;

public class z {
    private static final int bAQ = 700;
    public static final byte bAR = 0;
    public static final byte bAS = 1;
    public static final byte bAT = 2;
    public static final byte bAU = 0;
    public static final byte bAV = 1;
    public static final byte bAW = 2;
    public static final byte bAX = 3;
    protected static int bBe = 15;
    private static final int bBo = 4097;
    protected h aAF = null;
    protected int aSP = 0;
    protected int aSQ = 0;
    private byte bAY = 0;
    protected int bAZ = -1;
    protected int bBa = -1;
    protected boolean bBb = false;
    protected boolean bBc = false;
    private boolean bBd = false;
    protected boolean bBf = true;
    protected boolean bBg = false;
    private int bBh = -1;
    protected n bBi = null;
    protected v bBj = null;
    protected w bBk = null;
    protected ad bBl = null;
    protected z bBm = null;
    private Handler bBn = new e(this);
    protected Drawable cx = null;
    protected Paint pL = new Paint(1);
    protected int paddingBottom = 0;
    protected int paddingLeft = 0;
    protected int paddingRight = 0;
    protected int paddingTop = 0;

    public static final void hL(int i) {
        bBe = i;
    }

    public void Iv() {
    }

    public void Iw() {
    }

    public void Ix() {
        if (this.bBm != null) {
            this.bBm.Ix();
        } else {
            invalidate();
        }
    }

    public z Iy() {
        return this.bBm;
    }

    public byte Iz() {
        return this.bAY;
    }

    public void a(ad adVar) {
        this.bBl = adVar;
    }

    public void a(n nVar) {
        this.bBi = nVar;
    }

    public void a(v vVar) {
        this.bBj = vVar;
    }

    public void a(w wVar) {
        this.bBk = wVar;
    }

    public boolean a(byte b2, int i, int i2) {
        return this.bBg && b2 == 0 && ar(i, i2);
    }

    public boolean a(KeyEvent keyEvent) {
        switch (keyEvent.getKeyCode()) {
            case 23:
            case 66:
                if (this.bAY == 1 && this.bBg && keyEvent.getAction() == 1) {
                    return k(0, 0);
                }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean aD(int i, int i2) {
        if (this.bBj == null) {
            return false;
        }
        this.bBj.c(this);
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean ar(int i, int i2) {
        return this.bBg;
    }

    public boolean b(byte b2, int i, int i2) {
        return d(b2, i, i2);
    }

    public void bL(boolean z) {
        this.bBg = z;
    }

    public void c(h hVar) {
        this.aAF = hVar;
    }

    public boolean c(KeyEvent keyEvent) {
        return a(keyEvent);
    }

    public boolean d(byte b2, int i, int i2) {
        switch (b2) {
            case 0:
                this.bBb = false;
                this.bBc = false;
                this.bBd = true;
                this.bAZ = i;
                this.bBa = i2;
                this.bBn.sendMessageDelayed(this.bBn.obtainMessage(4097), 700);
                break;
            case 1:
                this.bBn.removeMessages(4097);
                this.bAZ = -1;
                this.bBa = -1;
                boolean z = this.bBd;
                this.bBd = false;
                if (this.bBc) {
                    return true;
                }
                if (!this.bBb && z && k(i, i2)) {
                    return true;
                }
            case 2:
                if (this.bBc) {
                    return true;
                }
                boolean z2 = Math.abs(i - this.bAZ) > bBe;
                boolean z3 = Math.abs(i2 - this.bBa) > bBe;
                if (z2 || z3) {
                    this.bBn.removeMessages(4097);
                    this.bBb = true;
                    break;
                } else if (this.bBg) {
                    return true;
                }
                break;
        }
        boolean a2 = a(b2, i, i2);
        if (a2) {
            return a2;
        }
        this.bBn.removeMessages(4097);
        this.bBd = false;
        return a2;
    }

    public void draw(Canvas canvas) {
        e(canvas);
    }

    public void e(Canvas canvas) {
        if (this.cx != null) {
            this.cx.setBounds(0, 0, this.aSP, this.aSQ);
            this.cx.draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void eL() {
    }

    public boolean eZ(int i) {
        if (this.aAF == null) {
            return false;
        }
        this.aAF.b(this, i);
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean es() {
        return false;
    }

    public Drawable getBackground() {
        return this.cx;
    }

    public int getHeight() {
        return this.aSQ;
    }

    public int getId() {
        return this.bBh;
    }

    public int getPaddingBottom() {
        return this.paddingBottom;
    }

    public int getPaddingLeft() {
        return this.paddingLeft;
    }

    public int getPaddingRight() {
        return this.paddingRight;
    }

    public int getPaddingTop() {
        return this.paddingTop;
    }

    public int getWidth() {
        return this.aSP;
    }

    public void h(z zVar) {
        this.bBm = zVar;
    }

    public boolean hM(int i) {
        if (this.bBk == null) {
            return false;
        }
        this.bBk.a(this, i);
        return true;
    }

    public void hN(int i) {
        this.paddingLeft = i;
    }

    public void hO(int i) {
        this.paddingRight = i;
    }

    public void hP(int i) {
        this.paddingTop = i;
    }

    public void hQ(int i) {
        this.paddingBottom = i;
    }

    public void invalidate() {
        if (this.bBl != null) {
            this.bBl.cJ();
        }
    }

    /* access modifiers changed from: protected */
    public boolean k(int i, int i2) {
        if (this.bBi == null) {
            return false;
        }
        this.bBi.e(this);
        return true;
    }

    /* access modifiers changed from: protected */
    public int n(int i, int i2) {
        return -1;
    }

    public void onDraw(Canvas canvas) {
        boolean es = es();
        draw(canvas);
        if (es) {
            Ix();
        }
    }

    public boolean performClick() {
        if (this.bBi == null) {
            return false;
        }
        this.bBi.e(this);
        return true;
    }

    public boolean performLongClick() {
        if (this.bBj == null) {
            return false;
        }
        this.bBj.c(this);
        return true;
    }

    public void s(Drawable drawable) {
        this.cx = drawable;
    }

    public void setId(int i) {
        this.bBh = i;
    }

    public void setPadding(int i, int i2, int i3, int i4) {
        this.paddingLeft = i;
        this.paddingTop = i2;
        this.paddingRight = i3;
        this.paddingBottom = i4;
    }

    public void setSize(int i, int i2) {
        this.aSP = i;
        this.aSQ = i2;
        eL();
    }

    public void z(byte b2) {
        if (this.bAY != b2) {
            this.bAY = b2;
            Ix();
        }
    }
}
