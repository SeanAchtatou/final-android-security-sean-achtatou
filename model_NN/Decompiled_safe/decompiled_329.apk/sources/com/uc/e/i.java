package com.uc.e;

import android.graphics.Canvas;
import android.view.KeyEvent;
import android.view.MotionEvent;

public class i {
    private z tI;
    private p tJ;
    private ad tK;

    public i(ad adVar) {
        this.tK = adVar;
    }

    protected static boolean b(KeyEvent keyEvent) {
        return keyEvent.getAction() == 0 && keyEvent.getKeyCode() == 4;
    }

    private void d(Canvas canvas) {
        canvas.save();
        canvas.translate((float) this.tJ.wy(), (float) this.tJ.wz());
        canvas.clipRect(0, 0, this.tJ.getWidth(), this.tJ.getHeight());
        this.tJ.onDraw(canvas);
    }

    public void a(p pVar) {
        a(pVar, (this.tI.getWidth() - pVar.getWidth()) / 2, (this.tI.getHeight() - pVar.getHeight()) / 2);
    }

    public void a(p pVar, int i, int i2) {
        this.tJ = pVar;
        pVar.setPosition(i, i2);
        pVar.a(this.tK);
        pVar.show();
    }

    public void cJ() {
        this.tK.cJ();
    }

    public void d(z zVar) {
        this.tI = zVar;
        zVar.a(this.tK);
    }

    public void dispatchDraw(Canvas canvas) {
        if (this.tI != null) {
            this.tI.onDraw(canvas);
        }
        if (fi()) {
            d(canvas);
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (this.tI == null) {
            return false;
        }
        if (!fi()) {
            return this.tI.c(keyEvent);
        }
        if (!b(keyEvent)) {
            return this.tJ.c(keyEvent);
        }
        this.tJ.dismiss();
        return true;
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        byte action = (byte) motionEvent.getAction();
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (this.tI != null) {
            if (!fi()) {
                return this.tI.b(action, x, y);
            }
            if (this.tJ.getBounds().contains(x, y)) {
                return this.tJ.b(action, x - this.tJ.wy(), y - this.tJ.wz());
            }
        }
        return false;
    }

    public boolean fi() {
        return this.tJ != null && this.tJ.isShown();
    }

    /* access modifiers changed from: protected */
    public void fj() {
    }

    public void layout(int i, int i2, int i3, int i4) {
        if (this.tI != null) {
            this.tI.setSize(i3 - i, i4 - i2);
        }
        if (this.tJ != null) {
            this.tJ.wx();
        }
        fj();
    }
}
