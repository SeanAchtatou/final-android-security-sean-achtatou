package com.uc.e.b;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.SweepGradient;
import android.graphics.drawable.Drawable;

public class m extends Drawable {
    private int cag;

    public m(int i) {
        this.cag = i;
    }

    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        Paint paint = new Paint(1);
        int i = bounds.right - bounds.left < bounds.bottom - bounds.top ? (bounds.right - bounds.left) / 2 : (bounds.bottom - bounds.top) / 2;
        paint.setShader(new SweepGradient((float) ((bounds.right + bounds.left) / 2), (float) ((bounds.bottom + bounds.top) / 2), this.cag, -7829368));
        canvas.drawCircle((float) ((bounds.right + bounds.left) / 2), (float) ((bounds.bottom + bounds.top) / 2), (float) i, paint);
        paint.setShader(null);
        paint.setColor(this.cag);
        canvas.drawCircle((float) ((bounds.right + bounds.left) / 2), (float) ((bounds.top + bounds.bottom) / 2), (float) (i - 4), paint);
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }
}
