package com.uc.e.b;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class a extends Drawable {
    private static Path pK;
    private int color;
    Paint pL = new Paint(1);

    public a(int i) {
        this.color = i;
    }

    public void draw(Canvas canvas) {
        if (pK == null) {
            Path path = new Path();
            path.moveTo(0.0f, 49.0f);
            path.cubicTo(9.0f, 52.0f, 22.0f, 63.0f, 33.0f, 76.0f);
            path.cubicTo(41.0f, 62.0f, 65.0f, 32.0f, 77.0f, 29.0f);
            path.cubicTo(73.0f, 19.0f, 76.0f, 10.0f, 76.0f, 0.0f);
            path.cubicTo(50.0f, 14.0f, 32.0f, 56.0f, 32.0f, 56.0f);
            path.lineTo(21.0f, 36.0f);
            path.lineTo(0.0f, 49.0f);
            pK = path;
        }
        Rect bounds = getBounds();
        int i = bounds.right - bounds.left;
        int i2 = bounds.bottom - bounds.top;
        canvas.save();
        canvas.translate(((float) bounds.left) + (((float) i) / 10.0f), ((float) bounds.top) + (((float) i2) / 10.0f));
        canvas.scale(((float) i) / 100.0f, ((float) i2) / 100.0f);
        this.pL.setColor(this.color);
        canvas.drawPath(pK, this.pL);
        canvas.restore();
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }
}
