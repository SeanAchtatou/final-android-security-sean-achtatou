package com.uc.e.b;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;

public class h extends Drawable {
    protected int auQ;
    protected int auS;
    protected float bgM;
    protected int bgN;
    protected float bgO;
    protected float bgP;
    protected LinearGradient bgQ;
    protected LinearGradient bgR;
    protected Paint pL;
    protected int shadowColor;

    public h(float f, int i, int i2, int i3, float f2, float f3) {
        this(f, i, i2, i2, i3, f2, f3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    public h(float f, int i, int i2, int i3, int i4, float f2, float f3) {
        this.bgM = 4.0f;
        this.auQ = -16777216;
        this.auS = -1;
        this.bgO = 10.0f;
        this.bgP = 3.0f;
        this.pL = new Paint(1);
        this.bgM = f;
        this.auQ = i;
        this.auS = i2;
        this.bgN = i3;
        this.bgO = f2;
        this.bgP = f3;
        this.shadowColor = i4;
        if ((i4 & -16777216) != 0 && ((double) f2) > 1.1d) {
            this.bgQ = new LinearGradient(0.0f, 0.0f, 0.0f, f2, i4, i4 & 16777215, Shader.TileMode.REPEAT);
        }
    }

    public void draw(Canvas canvas) {
        if (getBounds() != null) {
            RectF rectF = new RectF(getBounds());
            RectF rectF2 = new RectF(rectF.left - this.bgM, rectF.top - this.bgM, rectF.right + this.bgM, rectF.bottom + this.bgM);
            this.pL.setShader(null);
            this.pL.setColor(this.auQ);
            this.pL.setStrokeWidth(1.0f);
            this.pL.setStyle(Paint.Style.FILL);
            canvas.drawRoundRect(rectF2, this.bgP + this.bgM, this.bgP + this.bgM, this.pL);
            if (this.auS == this.bgN || this.bgR == null) {
                this.pL.setShader(null);
                this.pL.setColor(this.auS);
            } else {
                this.pL.setShader(this.bgR);
            }
            this.pL.setStyle(Paint.Style.FILL);
            canvas.drawRoundRect(rectF, this.bgP, this.bgP, this.pL);
            if (this.bgQ != null) {
                this.pL.setShader(this.bgQ);
                canvas.save();
                Path path = new Path();
                path.addRoundRect(rectF, this.bgP, this.bgP, Path.Direction.CCW);
                canvas.clipPath(path);
                canvas.translate(rectF.left, rectF.top);
                canvas.drawRect(0.0f, 0.0f, rectF.right - rectF.left, this.bgO, this.pL);
                canvas.restore();
            } else if (this.shadowColor != 0) {
                this.pL.setShader(null);
                this.pL.setColor(this.shadowColor);
                this.pL.setStyle(Paint.Style.FILL);
                this.pL.setAntiAlias(false);
                canvas.drawLine(rectF.left + this.bgP, rectF.top, rectF.right - this.bgP, rectF.top, this.pL);
                this.pL.setAntiAlias(true);
            }
            this.pL.setShader(null);
        }
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int i) {
    }

    public void setBounds(int i, int i2, int i3, int i4) {
        super.setBounds(i, i2, i3, i4);
        if (this.auS != this.bgN) {
            this.bgR = new LinearGradient((float) i, (float) i2, (float) i, (float) i4, this.auS, this.bgN, Shader.TileMode.REPEAT);
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }
}
