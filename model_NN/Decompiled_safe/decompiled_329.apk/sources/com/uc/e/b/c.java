package com.uc.e.b;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class c extends Drawable {
    private int abI;
    private int color;

    public c(int i, int i2) {
        this.color = i;
        this.abI = i2;
    }

    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        int i = bounds.right - bounds.left > bounds.bottom - bounds.top ? (bounds.bottom - bounds.top) / 3 : (bounds.right - bounds.left) / 3;
        Paint paint = new Paint(1);
        paint.setColor(this.abI);
        canvas.drawCircle((float) ((bounds.right + bounds.left) >> 1), (float) ((bounds.bottom + bounds.top) >> 1), (float) i, paint);
        paint.setColor(this.color);
        canvas.drawCircle((float) ((bounds.right + bounds.left) >> 1), (float) ((bounds.top + bounds.bottom) >> 1), (float) (i - 1), paint);
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }
}
