package com.uc.e.b;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.drawable.Drawable;

public class l extends Drawable {
    private int abI;
    private float bXY;
    private int bXZ;
    private int bYa;
    private int bYb;
    private int bYc;
    private boolean bYd = true;

    public l(int i, float f, int i2, int i3, int i4, int i5) {
        this.abI = i;
        this.bXY = f;
        this.bXZ = i2;
        this.bYa = i3;
        this.bYb = i4;
        this.bYc = i5;
    }

    public void cH(boolean z) {
        this.bYd = z;
    }

    public void draw(Canvas canvas) {
        canvas.save();
        Paint paint = new Paint();
        if (this.bYd) {
            paint.setAntiAlias(true);
        }
        Path path = new Path();
        Rect bounds = getBounds();
        RectF rectF = new RectF((float) (bounds.left + this.bYc), (float) (bounds.top + this.bYc), (float) (bounds.right - this.bYc), (float) ((bounds.bottom - this.bYc) - this.bYa));
        path.addRoundRect(rectF, this.bXY - ((float) this.bYc), this.bXY - ((float) this.bYc), Path.Direction.CCW);
        paint.setColor(this.bYb);
        canvas.drawRect(rectF, paint);
        canvas.clipPath(path, Region.Op.DIFFERENCE);
        RectF rectF2 = new RectF(bounds);
        paint.setColor(this.bXZ);
        canvas.drawRoundRect(rectF2, this.bXY, this.bXY, paint);
        RectF rectF3 = new RectF((float) bounds.left, (float) bounds.top, (float) bounds.right, (float) (bounds.bottom - this.bYa));
        paint.setColor(this.abI);
        canvas.drawRoundRect(rectF3, this.bXY, this.bXY, paint);
        canvas.restore();
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int i) {
    }

    public void setBounds(int i, int i2, int i3, int i4) {
        super.setBounds(i - this.bYc, i2 - this.bYc, this.bYc + i3, this.bYc + i4 + this.bYa);
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }
}
