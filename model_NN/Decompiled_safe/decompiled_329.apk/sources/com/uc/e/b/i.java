package com.uc.e.b;

import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Shader;

public class i extends h {
    protected int bpU;
    protected int brb;
    protected int brc;
    protected int brd;
    protected LinearGradient bre;
    protected int brf;
    protected int brg;
    protected int brh;
    protected boolean bri;

    public i(float f, int i, int i2, int i3, int i4, float f2, float f3, int i5, int i6, int i7, int i8, int i9) {
        this(f, i, i2, i3, i4, f2, f3, i5, i6, i7, i8, i9, false, 0, 0);
    }

    public i(float f, int i, int i2, int i3, int i4, float f2, float f3, int i5, int i6, int i7, int i8, int i9, boolean z, int i10, int i11) {
        super(f, i, i2, i3, i4, f2, f3);
        this.bri = false;
        this.brb = i5;
        this.brc = i6;
        this.brd = i9;
        this.bpU = i8;
        this.bri = z;
        this.brf = i10;
        this.brg = i11;
        this.brh = i7;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.bpU > 0) {
            Path path = new Path();
            Rect bounds = getBounds();
            path.addRoundRect(new RectF((float) (bounds.right - this.bpU), (float) bounds.top, (float) bounds.right, (float) bounds.bottom), new float[]{0.0f, 0.0f, this.bgP, this.bgP, this.bgP, this.bgP, 0.0f, 0.0f}, Path.Direction.CW);
            if (this.bre != null) {
                this.pL.setColor(-1);
                this.pL.setShader(this.bre);
                canvas.drawPath(path, this.pL);
            } else if ((this.brb & -16777216) != 0) {
                this.pL.setShader(null);
                this.pL.setColor(this.brb);
                canvas.drawPath(path, this.pL);
            }
            this.pL.setAntiAlias(false);
            this.pL.setShader(null);
            this.pL.setColor(this.brh);
            canvas.drawLine((float) (bounds.right - this.bpU), (float) bounds.top, ((float) bounds.right) - this.bgP, (float) bounds.top, this.pL);
            this.pL.setColor(this.brd);
            canvas.drawLine((float) ((bounds.right - this.bpU) - 1), (float) bounds.top, (float) ((bounds.right - this.bpU) - 1), (float) bounds.bottom, this.pL);
            if (this.bri) {
                int i = (int) ((((float) bounds.right) - (((((float) this.bpU) - this.bgP) * 5.0f) / 6.0f)) - (this.bgP / 2.0f));
                int i2 = (int) (((float) i) + (((((float) this.bpU) - this.bgP) * 2.0f) / 3.0f));
                int i3 = (i + i2) >> 1;
                int i4 = ((bounds.bottom + bounds.top) / 2) - ((i2 - i) / 4);
                int i5 = ((i2 - i) / 2) + i4;
                Path path2 = new Path();
                path2.moveTo((float) i, (float) i4);
                path2.lineTo((float) i3, (float) i5);
                path2.lineTo((float) i2, (float) i4);
                path2.lineTo((float) i3, (float) ((i4 + i4) - i5));
                canvas.save();
                canvas.translate(0.0f, -3.0f);
                canvas.clipPath(path2, Region.Op.DIFFERENCE);
                canvas.translate(0.0f, 3.0f);
                this.pL.setShader(null);
                this.pL.setColor(this.brg);
                canvas.translate(0.0f, 1.0f);
                canvas.drawPath(path2, this.pL);
                canvas.translate(0.0f, -1.0f);
                this.pL.setColor(this.brf);
                canvas.drawPath(path2, this.pL);
                canvas.restore();
            }
            this.pL.setAntiAlias(true);
        }
    }

    public void setBounds(int i, int i2, int i3, int i4) {
        super.setBounds(i, i2, i3, i4);
        if (this.brb != this.brc && this.bpU > 0) {
            this.bre = new LinearGradient((float) i, (float) i2, (float) i, (float) i4, this.brb, this.brc, Shader.TileMode.REPEAT);
        }
    }
}
