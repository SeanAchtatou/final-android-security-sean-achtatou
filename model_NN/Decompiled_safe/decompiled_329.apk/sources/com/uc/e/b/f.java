package com.uc.e.b;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;

public class f extends Drawable {
    private int baL;
    private int baM;
    private int height;

    public f(int i, int i2, int i3) {
        this.baL = i;
        this.baM = i2;
        this.height = i3;
    }

    public void draw(Canvas canvas) {
        int height2 = (getBounds().height() / this.height) + 1;
        for (int i = 0; i < height2; i++) {
            canvas.save();
            canvas.translate(0.0f, (float) (this.height * i));
            canvas.clipRect(0, 0, getBounds().width(), this.height);
            canvas.drawColor(i % 2 == 0 ? this.baL : this.baM);
            canvas.restore();
        }
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }

    public void setHeight(int i) {
        this.height = i;
    }
}
