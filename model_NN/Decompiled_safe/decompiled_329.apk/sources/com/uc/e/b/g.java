package com.uc.e.b;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;

public class g extends ColorDrawable {
    private int Jp;

    public g(int i, int i2) {
        super(i);
        this.Jp = i2;
    }

    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        canvas.save();
        canvas.clipRect(bounds.left + this.Jp, bounds.top, bounds.right - this.Jp, bounds.bottom);
        super.draw(canvas);
        canvas.restore();
    }
}
