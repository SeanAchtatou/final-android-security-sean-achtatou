package com.uc.e.b;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import com.uc.browser.ModelBrowser;
import java.lang.reflect.Method;

public class k extends Drawable {
    public static final byte bWA = 0;
    public static final byte bWB = 1;
    public static final byte bWC = 2;
    public static final byte bWD = 3;
    private static boolean bWF = false;
    private static Method bWG = null;
    public static final byte bWy = 0;
    public static final byte bWz = 1;
    private int bWE;
    private Bitmap[] bWH;
    private byte[] bWI;
    private byte bWJ;
    private Picture bWK;
    private Rect[] bWL;
    private Rect bWM;
    private Drawable bWN;

    public k(Bitmap bitmap, byte b2) {
        this(bitmap, (byte) 1, b2);
    }

    public k(Bitmap bitmap, byte b2, byte b3) {
        this(new Bitmap[]{bitmap}, new byte[]{b2}, b3);
    }

    public k(Drawable drawable) {
        this.bWE = ModelBrowser.zX;
        this.bWM = null;
        this.bWN = null;
        this.bWN = drawable;
    }

    public k(Bitmap[] bitmapArr, byte[] bArr, byte b2) {
        this(bitmapArr, bArr, b2, null);
    }

    public k(Bitmap[] bitmapArr, byte[] bArr, byte b2, Rect rect) {
        this.bWE = ModelBrowser.zX;
        this.bWM = null;
        this.bWN = null;
        if (bitmapArr.length > 3 || bArr.length > 3 || bitmapArr.length != bArr.length) {
            throw new IllegalArgumentException("Oh, the args are not allowed !");
        }
        this.bWH = bitmapArr;
        this.bWI = bArr;
        this.bWJ = b2;
        if (rect != null) {
            this.bWM = (rect.bottom < 0 || rect.top < 0 || rect.left < 0 || rect.right < 0) ? null : rect;
        }
    }

    private void Oc() {
        Rect bounds = getBounds();
        Rect[] b2 = b(bounds);
        this.bWK = new Picture();
        Canvas beginRecording = this.bWK.beginRecording(bounds.right - bounds.left, bounds.bottom - bounds.top);
        Paint paint = null;
        BitmapShader bitmapShader = null;
        for (int i = 0; i < b2.length; i++) {
            if (this.bWI[i] == 1) {
                if (this.bWJ == 0) {
                    bitmapShader = new BitmapShader(this.bWH[i], Shader.TileMode.REPEAT, Shader.TileMode.CLAMP);
                } else if (this.bWJ == 1) {
                    bitmapShader = new BitmapShader(this.bWH[i], Shader.TileMode.CLAMP, Shader.TileMode.REPEAT);
                }
                if (paint == null) {
                    paint = new Paint(2);
                    paint.setDither(true);
                }
                paint.setShader(bitmapShader);
                beginRecording.drawRect(b2[i], paint);
                paint.setShader(null);
            } else if (this.bWI[i] == 2) {
                bitmapShader = new BitmapShader(this.bWH[i], Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
                if (paint == null) {
                    paint = new Paint(2);
                    paint.setDither(true);
                }
                paint.setShader(bitmapShader);
                beginRecording.drawRect(b2[i], paint);
                paint.setShader(null);
            } else {
                if (paint == null) {
                    paint = new Paint(2);
                    paint.setDither(true);
                }
                beginRecording.drawBitmap(this.bWH[i], (Rect) null, b2[i], paint);
            }
        }
    }

    private void a(Rect[] rectArr, Canvas canvas) {
        if (this.bWK != null) {
            this.bWK.draw(canvas);
            return;
        }
        Oc();
        this.bWK.draw(canvas);
    }

    private Rect[] a(Rect rect, int i, int... iArr) {
        Rect[] rectArr = new Rect[iArr.length];
        if (i == 0) {
            int i2 = rect.left;
            for (int i3 = 0; i3 < rectArr.length; i3++) {
                rectArr[i3] = new Rect(i2, rect.top, iArr[i3] + i2, rect.bottom);
                i2 += iArr[i3];
            }
        } else {
            int i4 = rect.top;
            for (int i5 = 0; i5 < rectArr.length; i5++) {
                rectArr[i5] = new Rect(rect.left, i4, rect.right, iArr[i5] + i4);
                i4 += iArr[i5];
            }
        }
        return rectArr;
    }

    private Rect[] b(Rect rect) {
        if (this.bWN != null) {
            return null;
        }
        Rect bounds = getBounds();
        int i = bounds.right - bounds.left;
        int i2 = bounds.bottom - bounds.top;
        if (this.bWJ == 0) {
            if (this.bWH.length == 1) {
                return new Rect[]{bounds};
            } else if (this.bWH.length == 2) {
                if ((this.bWI[0] == 1 || this.bWI[0] == 3) && this.bWI[1] == 0) {
                    return a(bounds, 0, i - this.bWH[1].getWidth(), this.bWH[1].getWidth());
                } else if (this.bWI[0] == 0 && (this.bWI[1] == 1 || this.bWI[1] == 3)) {
                    return a(bounds, 0, this.bWH[0].getWidth(), i - this.bWH[0].getWidth());
                }
            } else if ((this.bWI[0] == 1 || this.bWI[0] == 3) && this.bWI[1] == 0 && (this.bWI[2] == 1 || this.bWI[2] == 3)) {
                int width = (i - this.bWH[1].getWidth()) / 2;
                return a(bounds, 0, width, this.bWH[1].getWidth(), width);
            } else if (this.bWI[0] == 0 && ((this.bWI[1] == 1 || this.bWI[1] == 3) && this.bWI[2] == 0)) {
                return a(bounds, 0, this.bWH[0].getWidth(), (i - this.bWH[0].getWidth()) - this.bWH[2].getWidth(), this.bWH[2].getWidth());
            }
        } else if (this.bWH.length == 1) {
            return new Rect[]{bounds};
        } else if (this.bWH.length == 2) {
            if ((this.bWI[0] == 1 || this.bWI[0] == 3) && this.bWI[1] == 0) {
                return a(bounds, 1, i2 - this.bWH[1].getHeight(), this.bWH[1].getHeight());
            } else if (this.bWI[0] == 0 && (this.bWI[1] == 1 || this.bWI[1] == 3)) {
                return a(bounds, 1, this.bWH[0].getHeight(), i2 - this.bWH[0].getHeight());
            }
        } else if ((this.bWI[0] == 1 || this.bWI[0] == 3) && this.bWI[1] == 0 && (this.bWI[2] == 1 || this.bWI[2] == 3)) {
            int height = (i2 - this.bWH[1].getHeight()) / 2;
            return a(bounds, 1, height, this.bWH[1].getHeight(), height);
        } else if (this.bWI[0] == 0 && ((this.bWI[1] == 1 || this.bWI[1] == 3) && this.bWI[2] == 0)) {
            return a(bounds, 1, this.bWH[0].getHeight(), (i2 - this.bWH[0].getHeight()) - this.bWH[2].getHeight(), this.bWH[2].getHeight());
        }
        throw new IllegalStateException();
    }

    private int g(Bitmap bitmap) {
        try {
            if (bWG == null) {
                bWG = Bitmap.class.getMethod("getDensity", new Class[0]);
            }
            return ((Integer) bWG.invoke(bitmap, new Object[0])).intValue();
        } catch (Exception e) {
            bWF = true;
            return ModelBrowser.zX;
        }
    }

    public void cD(boolean z) {
    }

    public void draw(Canvas canvas) {
        if (this.bWN != null) {
            this.bWN.draw(canvas);
            return;
        }
        Rect bounds = getBounds();
        if (bounds.right - bounds.left > 0 && bounds.bottom - bounds.top > 0) {
            a(this.bWL, canvas);
        }
    }

    public int getOpacity() {
        return 255;
    }

    public boolean getPadding(Rect rect) {
        if (this.bWN != null) {
            return this.bWN.getPadding(rect);
        }
        if (this.bWM == null) {
            return super.getPadding(rect);
        }
        if (rect == null) {
            new Rect(this.bWM);
        } else {
            rect.left = this.bWM.left;
            rect.top = this.bWM.top;
            rect.right = this.bWM.right;
            rect.bottom = this.bWM.bottom;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        if (this.bWN != null) {
            this.bWN.setBounds(rect);
        } else {
            this.bWL = b(rect);
        }
    }

    public void setAlpha(int i) {
    }

    public void setBounds(int i, int i2, int i3, int i4) {
        if (this.bWN != null) {
            this.bWN.setBounds(i, i2, i3, i4);
            return;
        }
        Rect bounds = getBounds();
        if (!(bounds.right - bounds.left == i3 - i && bounds.bottom - bounds.top == i4 - i2)) {
        }
        if (!(bounds.left == i && bounds.top == i2 && bounds.right == i3 && bounds.bottom == i4)) {
            this.bWK = null;
        }
        super.setBounds(i, i2, i3, i4);
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }

    public void setTargetDensity(int i) {
        this.bWE = i;
        if (this.bWH != null && this.bWH.length > 0) {
            for (int i2 = 0; i2 < this.bWH.length; i2++) {
                Bitmap bitmap = this.bWH[i2];
                int g = g(bitmap);
                if (g != this.bWE) {
                    this.bWH[i2] = Bitmap.createScaledBitmap(bitmap, (bitmap.getWidth() * this.bWE) / g, (bitmap.getHeight() * this.bWE) / g, true);
                    bitmap.recycle();
                }
            }
        }
    }
}
