package com.uc.e.b;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import java.util.ArrayList;

public class d extends Drawable {
    private Rect[] auO;
    private int auP;
    private int auQ;
    private int auR;
    private int auS;
    private Paint pL = new Paint(1);

    public d(Rect[] rectArr, int i, int i2, int i3, int i4) {
        this.auO = rectArr;
        this.auP = i;
        this.auR = i2;
        this.auQ = i3;
        this.auS = i4;
    }

    private Object[] xi() {
        ArrayList arrayList = new ArrayList();
        int length = this.auO.length;
        for (int i = 0; i < length; i++) {
            Rect rect = this.auO[i];
            int i2 = i - 1;
            if (i2 >= 0) {
                Rect rect2 = this.auO[i2];
                if (rect2.bottom < rect.top && rect2.right >= rect.left && rect.right >= rect2.left) {
                    arrayList.add(new Rect(rect2.left > rect.left ? rect2.left : rect.left, rect2.bottom, rect2.right, rect.top));
                }
                if (rect2.right < rect.left && rect2.bottom >= rect.top && rect.bottom >= rect2.top) {
                    arrayList.add(new Rect(rect2.right, rect2.top < rect.top ? rect.top : rect2.top, rect.left, rect2.bottom < rect.bottom ? rect2.bottom : rect.bottom));
                }
            }
            arrayList.add(this.auO[i]);
        }
        return arrayList.toArray();
    }

    public void draw(Canvas canvas) {
        if (this.auO == null || this.auO.length != 1) {
            Region region = new Region();
            for (Object obj : xi()) {
                region.op((Rect) obj, Region.Op.UNION);
            }
            Path boundaryPath = region.getBoundaryPath();
            this.pL.setPathEffect(new CornerPathEffect((float) this.auR));
            this.pL.setColor(this.auS);
            this.pL.setStyle(Paint.Style.FILL);
            canvas.drawPath(boundaryPath, this.pL);
            this.pL.setColor(this.auQ);
            this.pL.setStyle(Paint.Style.STROKE);
            this.pL.setStrokeWidth((float) this.auP);
            this.pL.setStrokeCap(Paint.Cap.ROUND);
            this.pL.setStrokeJoin(Paint.Join.ROUND);
            canvas.drawPath(boundaryPath, this.pL);
            return;
        }
        this.pL.setColor(this.auS);
        this.pL.setStyle(Paint.Style.FILL);
        RectF rectF = new RectF(this.auO[0]);
        canvas.drawRoundRect(rectF, (float) this.auR, (float) this.auR, this.pL);
        this.pL.setColor(this.auQ);
        this.pL.setStyle(Paint.Style.STROKE);
        this.pL.setStrokeWidth((float) this.auP);
        this.pL.setStrokeCap(Paint.Cap.ROUND);
        this.pL.setStrokeJoin(Paint.Join.ROUND);
        canvas.drawRoundRect(rectF, (float) this.auR, (float) this.auR, this.pL);
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }
}
