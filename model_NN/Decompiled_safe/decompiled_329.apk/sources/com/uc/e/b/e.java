package com.uc.e.b;

import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;

public class e extends ColorDrawable {
    public e() {
    }

    public e(int i) {
        super(i);
    }

    public void draw(Canvas canvas) {
        canvas.save();
        canvas.clipRect(getBounds());
        super.draw(canvas);
        canvas.restore();
    }
}
