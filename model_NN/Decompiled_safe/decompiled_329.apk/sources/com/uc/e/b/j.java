package com.uc.e.b;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class j extends Drawable {
    private Paint aBF;
    private int[] bCM;
    private int bCN;
    private boolean bCO;
    private int direction;

    public j(int[] iArr) {
        this(iArr, false);
    }

    public j(int[] iArr, boolean z) {
        this.bCN = 255;
        this.bCO = false;
        this.direction = 0;
        this.bCM = iArr;
        this.bCO = z;
    }

    public void draw(Canvas canvas) {
        if (this.bCM != null) {
            if (this.aBF == null) {
                this.aBF = new Paint();
            }
            Rect bounds = getBounds();
            if (this.direction == 0) {
                int i = bounds.bottom - bounds.top;
                for (int i2 = 0; i2 < i; i2++) {
                    if (i2 < this.bCM.length) {
                        this.aBF.setColor(this.bCM[i2]);
                        if (!this.bCO) {
                            this.aBF.setAlpha(this.bCN);
                        }
                        canvas.drawLine((float) bounds.left, (float) (bounds.top + i2), (float) bounds.right, (float) (bounds.top + i2), this.aBF);
                    }
                }
                return;
            }
            int i3 = bounds.right - bounds.left;
            for (int i4 = 0; i4 < i3; i4++) {
                if (i4 < this.bCM.length) {
                    this.aBF.setColor(this.bCM[i4]);
                    if (!this.bCO) {
                        this.aBF.setAlpha(this.bCN);
                    }
                    canvas.drawLine((float) (bounds.left + i4), (float) bounds.top, (float) (bounds.left + i4), (float) bounds.bottom, this.aBF);
                }
            }
        }
    }

    public int getOpacity() {
        return 0;
    }

    public void hU(int i) {
        this.direction = i;
    }

    public void setAlpha(int i) {
        this.bCN = i;
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }
}
