package com.uc.e.b;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class n extends Drawable {
    private int cef = 0;
    private int ceg = 0;
    private int ceh = 0;
    private Drawable[] cei;

    public int PK() {
        return this.cef;
    }

    public int PL() {
        return this.ceg;
    }

    public int PM() {
        return this.ceh;
    }

    public int PN() {
        if (this.cei == null) {
            return 0;
        }
        return this.cei[1].getIntrinsicWidth();
    }

    public void draw(Canvas canvas) {
        if (this.cei != null) {
            Rect bounds = getBounds();
            int PN = ((bounds.right - bounds.left) - this.cef) - PN();
            int i = this.ceg > PN ? PN : this.ceg;
            if (this.ceh <= PN) {
                PN = this.ceh;
            }
            if (i <= 0) {
                i = (bounds.right - PN) - this.cei[1].getIntrinsicWidth();
            }
            int intrinsicWidth = PN > 0 ? bounds.right - PN : this.cei[1].getIntrinsicWidth() + i;
            this.cei[0].setBounds(bounds.left, bounds.top, i, bounds.bottom);
            this.cei[0].draw(canvas);
            this.cei[1].setBounds(i, bounds.top, intrinsicWidth, bounds.bottom);
            this.cei[1].draw(canvas);
            this.cei[2].setBounds(intrinsicWidth, bounds.top, bounds.right, bounds.bottom);
            this.cei[2].draw(canvas);
        }
    }

    public int getOpacity() {
        return 0;
    }

    public boolean getPadding(Rect rect) {
        if (rect == null || this.cei == null) {
            return false;
        }
        Rect rect2 = new Rect();
        this.cei[0].getPadding(rect2);
        rect.left = rect2.left;
        this.cei[1].getPadding(rect2);
        rect.top = rect2.top;
        rect.bottom = rect2.bottom;
        this.cei[2].getPadding(rect2);
        rect.right = rect2.right;
        return true;
    }

    public void i(Drawable[] drawableArr) {
        this.cei = drawableArr;
    }

    public void kA(int i) {
        this.cef = i;
        this.ceg = this.ceg > 0 ? this.ceg < i ? i : this.ceg : -1;
        this.ceh = this.ceh > 0 ? this.ceh < i ? i : this.ceh : -1;
    }

    public void kB(int i) {
        this.ceg = i < this.cef ? this.cef : i;
        this.ceh = -1;
    }

    public void kC(int i) {
        this.ceh = i < this.cef ? this.cef : i;
        this.ceg = -1;
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }
}
