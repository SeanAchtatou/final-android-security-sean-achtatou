package com.uc.e.b;

import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Rect;
import android.graphics.Shader;

public class b extends h {
    protected LinearGradient Jl;
    protected float Jm;

    public b(float f, int i, int i2, int i3, float f2, float f3, int i4, int i5) {
        this(f, i, i2, i2, i3, f2, f3, (float) i4, i5);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    public b(float f, int i, int i2, int i3, int i4, float f2, float f3, float f4, int i5) {
        super(f, i, i2, i3, i4, f2, f3);
        this.Jm = f4;
        if ((-16777216 & i5) != 0) {
            this.Jl = new LinearGradient(0.0f, 0.0f, 0.0f, f4, i5, i5 & 16777215, Shader.TileMode.REPEAT);
        }
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        Rect bounds = getBounds();
        if (this.Jl != null) {
            canvas.save();
            this.pL.setShader(this.Jl);
            canvas.translate(0.0f, ((float) bounds.bottom) + this.bgM);
            canvas.drawRect(((float) bounds.left) + this.bgP, 0.0f, ((float) bounds.right) - this.bgP, this.Jm, this.pL);
            canvas.restore();
        }
    }
}
