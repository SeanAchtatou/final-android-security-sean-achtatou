package com.uc.c;

import b.a.b.a.a;
import b.b;
import java.util.Hashtable;

public class ce {
    public String ZQ;
    public String authority;
    public String bWO;
    public String bWP;
    public String bWQ;
    public String bWR;
    public String bWS;
    public boolean bWT;
    public int port;
    public String protocol;

    public ce(ce ceVar, String str) {
        this(ceVar, str, false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00d7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ce(com.uc.c.ce r12, java.lang.String r13, boolean r14) {
        /*
            r11 = this;
            r9 = 0
            r8 = 35
            r3 = 32
            r1 = 1
            r4 = 0
            r11.<init>()
            r0 = -1
            r11.port = r0
            r11.bWT = r4
            r11.bWT = r14
            boolean r0 = com.uc.c.bc.by(r13)
            if (r0 == 0) goto L_0x0018
        L_0x0017:
            return
        L_0x0018:
            int r0 = r13.length()
            r6 = r0
        L_0x001d:
            if (r6 <= 0) goto L_0x00e4
            int r0 = r6 - r1
            char r0 = r13.charAt(r0)
            if (r0 > r3) goto L_0x00e4
            int r0 = r6 + -1
            r6 = r0
            goto L_0x001d
        L_0x002b:
            if (r2 >= r6) goto L_0x0037
            char r0 = r13.charAt(r2)
            if (r0 > r3) goto L_0x0037
            int r0 = r2 + 1
            r2 = r0
            goto L_0x002b
        L_0x0037:
            java.lang.String r3 = "url:"
            r5 = 4
            r0 = r13
            boolean r0 = r0.regionMatches(r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x00e1
            int r0 = r2 + 4
        L_0x0043:
            int r2 = r13.length()
            if (r0 >= r2) goto L_0x00de
            char r2 = r13.charAt(r0)
            if (r2 != r8) goto L_0x00de
            r2 = r1
        L_0x0050:
            r3 = r0
        L_0x0051:
            if (r2 != 0) goto L_0x00db
            if (r3 >= r6) goto L_0x00db
            char r5 = r13.charAt(r3)
            r7 = 47
            if (r5 == r7) goto L_0x00db
            r7 = 58
            if (r5 != r7) goto L_0x00d3
            java.lang.String r2 = r13.substring(r0, r3)
            java.lang.String r2 = r2.toLowerCase()
            boolean r5 = r11.fQ(r2)
            if (r5 == 0) goto L_0x00db
            int r0 = r3 + 1
            r10 = r2
            r2 = r0
            r0 = r10
        L_0x0074:
            r11.protocol = r0
            if (r12 == 0) goto L_0x00d9
            if (r0 == 0) goto L_0x0082
            java.lang.String r3 = r12.protocol
            boolean r3 = r0.equals(r3)
            if (r3 == 0) goto L_0x00d9
        L_0x0082:
            java.lang.String r3 = r12.bWQ
            if (r3 == 0) goto L_0x0091
            java.lang.String r3 = r12.bWQ
            java.lang.String r5 = "/"
            boolean r3 = r3.startsWith(r5)
            if (r3 == 0) goto L_0x0091
            r0 = r9
        L_0x0091:
            if (r0 != 0) goto L_0x00d9
            java.lang.String r0 = r12.protocol
            r11.protocol = r0
            java.lang.String r0 = r12.authority
            r11.authority = r0
            java.lang.String r0 = r12.bWR
            r11.bWR = r0
            java.lang.String r0 = r12.ZQ
            r11.ZQ = r0
            int r0 = r12.port
            r11.port = r0
            java.lang.String r0 = r12.bWO
            r11.bWO = r0
            java.lang.String r0 = r12.bWQ
            r11.bWQ = r0
            r0 = r1
        L_0x00b0:
            int r1 = r13.indexOf(r8, r2)
            if (r1 < 0) goto L_0x00d7
            int r3 = r1 + 1
            java.lang.String r3 = r13.substring(r3, r6)
            r11.bWS = r3
        L_0x00be:
            if (r0 == 0) goto L_0x00ce
            if (r2 != r1) goto L_0x00ce
            java.lang.String r0 = r12.bWP
            r11.bWP = r0
            java.lang.String r0 = r11.bWS
            if (r0 != 0) goto L_0x00ce
            java.lang.String r0 = r12.bWS
            r11.bWS = r0
        L_0x00ce:
            r11.a(r11, r13, r2, r1)
            goto L_0x0017
        L_0x00d3:
            int r3 = r3 + 1
            goto L_0x0051
        L_0x00d7:
            r1 = r6
            goto L_0x00be
        L_0x00d9:
            r0 = r4
            goto L_0x00b0
        L_0x00db:
            r2 = r0
            r0 = r9
            goto L_0x0074
        L_0x00de:
            r2 = r4
            goto L_0x0050
        L_0x00e1:
            r0 = r2
            goto L_0x0043
        L_0x00e4:
            r2 = r4
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.ce.<init>(com.uc.c.ce, java.lang.String, boolean):void");
    }

    public ce(String str) {
        this((ce) null, str);
    }

    public ce(String str, boolean z) {
        this(null, str, z);
    }

    public static final String a(String str, Hashtable hashtable, Hashtable hashtable2) {
        int i;
        boolean z;
        int i2;
        boolean z2;
        char[] charArray = str.toCharArray();
        int length = str.length();
        StringBuffer stringBuffer = new StringBuffer(length);
        boolean z3 = true;
        int i3 = 0;
        while (true) {
            switch (z3) {
                case true:
                    if (i < length) {
                        i2 = i + 1;
                        char c = charArray[i];
                        if (c != '$') {
                            if (c != 65535) {
                                stringBuffer.append((char) c);
                                z2 = z3;
                                z = true;
                                break;
                            } else {
                                z2 = z3;
                                z = false;
                                break;
                            }
                        } else {
                            z2 = true;
                            z = true;
                            break;
                        }
                    } else {
                        i2 = i;
                        z2 = z3;
                        z = false;
                        break;
                    }
                case true:
                    StringBuffer stringBuffer2 = new StringBuffer();
                    while (true) {
                        if (i < length) {
                            int i4 = i + 1;
                            char c2 = charArray[i];
                            if (c2 == ')') {
                                i = i4;
                            } else if (c2 == '(') {
                                i = i4;
                            } else if (c2 == 65535) {
                                i = i4;
                            } else {
                                if (c2 == '$') {
                                    stringBuffer.append((char) c2);
                                } else if (bc.isLetterOrDigit((char) c2) || c2 == ':' || c2 == '_') {
                                    stringBuffer2.append((char) c2);
                                } else {
                                    i = i4 - 1;
                                }
                                i = i4;
                            }
                        }
                    }
                    String stringBuffer3 = stringBuffer2.toString();
                    int indexOf = stringBuffer3.indexOf(58);
                    if (indexOf > 0) {
                        stringBuffer3 = stringBuffer3.substring(indexOf + 1, stringBuffer3.length());
                    }
                    String str2 = (hashtable == null || !hashtable.containsKey(stringBuffer3)) ? (hashtable2 == null || !hashtable2.containsKey(stringBuffer3)) ? "" : (String) hashtable2.get(stringBuffer3) : (String) hashtable.get(stringBuffer3);
                    if (str2 != null) {
                        stringBuffer.append(str2);
                    }
                    z = true;
                    i2 = i;
                    z2 = true;
                    break;
                default:
                    i2 = i;
                    z2 = z3;
                    z = false;
                    break;
            }
            if (!z) {
                return stringBuffer.toString();
            }
            z3 = z2;
            i3 = i2;
        }
    }

    private void a(ce ceVar, String str, int i, int i2) {
        boolean z;
        int i3;
        String str2;
        String str3;
        String str4;
        int i4;
        String str5;
        String str6;
        String str7;
        String str8;
        boolean z2;
        String str9;
        int lastIndexOf;
        String str10;
        String str11;
        int i5;
        String str12 = ceVar.protocol;
        String str13 = ceVar.authority;
        String str14 = ceVar.bWR;
        String str15 = ceVar.ZQ;
        int i6 = ceVar.port;
        String str16 = ceVar.bWQ;
        String str17 = ceVar.bWP;
        String str18 = ceVar.bWS;
        if (i < i2) {
            int indexOf = str.indexOf(63);
            z = indexOf == i;
            if (indexOf == -1 || indexOf >= i2) {
                str3 = str17;
                i3 = i2;
                str2 = str;
            } else {
                String fR = fR(str.substring(indexOf + 1, i2));
                i3 = i2 > indexOf ? indexOf : i2;
                str2 = str.substring(0, indexOf);
                str3 = fR;
            }
        } else {
            z = false;
            i3 = i2;
            str2 = str;
            str3 = str17;
        }
        if (i <= i3 - 2 && str2.charAt(i) == '/' && str2.charAt(i + 1) == '/') {
            int i7 = i + 2;
            int indexOf2 = str2.indexOf(47, i7);
            if (indexOf2 < 0 && (indexOf2 = str2.indexOf(63, i7)) < 0) {
                indexOf2 = i3;
            }
            String substring = str2.substring(i7, indexOf2);
            int indexOf3 = substring.indexOf(64);
            if (indexOf3 != -1) {
                str4 = substring.substring(0, indexOf3);
                str10 = substring.substring(indexOf3 + 1);
            } else {
                str4 = null;
                str10 = substring;
            }
            if (str10 != null) {
                int indexOf4 = str10.indexOf(58);
                int i8 = -1;
                if (indexOf4 >= 0) {
                    if (str10.length() > indexOf4 + 1) {
                        i8 = bc.parseInt(str10.substring(indexOf4 + 1));
                    }
                    if (i8 != 0) {
                        str11 = str10.substring(0, indexOf4);
                        i5 = i8;
                    }
                }
                str11 = str10;
                i5 = i8;
            } else {
                int i9 = i6;
                str11 = "";
                i5 = i9;
            }
            if (i5 < -1) {
                throw new IllegalArgumentException();
            } else if (substring == null || substring.length() <= 0) {
                i4 = indexOf2;
                str5 = str11;
                i6 = i5;
                String str19 = substring;
                str6 = str16;
                str7 = str19;
            } else {
                i4 = indexOf2;
                str5 = str11;
                i6 = i5;
                String str20 = substring;
                str6 = "";
                str7 = str20;
            }
        } else {
            str4 = str14;
            i4 = i;
            str5 = str15;
            String str21 = str13;
            str6 = str16;
            str7 = str21;
        }
        String str22 = str5 == null ? "" : str5;
        if (i4 < i3) {
            if (str2.charAt(i4) == '/') {
                str8 = str2.substring(i4, i3);
                z2 = false;
            } else if (str6 == null || str6.length() <= 0) {
                str8 = (str7 != null ? au.aGF : "") + str2.substring(i4, i3);
                z2 = false;
            } else {
                int lastIndexOf2 = str6.lastIndexOf(47);
                String str23 = "";
                if (lastIndexOf2 == -1 && str7 != null) {
                    str23 = au.aGF;
                }
                str8 = str6.substring(0, lastIndexOf2 + 1) + str23 + str2.substring(i4, i3);
                z2 = true;
            }
        } else if (!z || str6 == null) {
            str8 = str6;
            z2 = false;
        } else {
            int lastIndexOf3 = str6.lastIndexOf(47);
            if (lastIndexOf3 < 0) {
                lastIndexOf3 = 0;
            }
            str8 = str6.substring(0, lastIndexOf3) + au.aGF;
            z2 = false;
        }
        if (str8 == null) {
            str8 = "";
        }
        if (z2) {
            String str24 = str8;
            while (true) {
                int indexOf5 = str24.indexOf("/./");
                if (indexOf5 < 0) {
                    break;
                }
                str24 = new StringBuffer(str24.substring(0, indexOf5)).append(str24.substring(indexOf5 + 2)).toString();
            }
            String str25 = str24;
            int i10 = 0;
            while (true) {
                int indexOf6 = str25.indexOf("/../", i10);
                if (indexOf6 <= 0) {
                    break;
                }
                int lastIndexOf4 = str25.lastIndexOf(47, indexOf6 - 1);
                if (lastIndexOf4 >= 0) {
                    str25 = str25.substring(0, lastIndexOf4) + str25.substring(indexOf6 + 3);
                    i10 = 0;
                } else {
                    i10 = indexOf6 + 3;
                }
            }
            str9 = str25;
            while (str9.endsWith("/..") && (lastIndexOf = str9.lastIndexOf(47, str9.indexOf("/..") - 1)) >= 0) {
                str9 = str9.substring(0, lastIndexOf + 1);
            }
            if (str9.startsWith("./") && str9.length() > 2) {
                str9 = str9.substring(2);
            }
            if (str9.endsWith("/.")) {
                str9 = str9.substring(0, str9.length() - 1);
            }
        } else {
            str9 = str8;
        }
        if (str9.startsWith("/../")) {
            str9 = str9.substring(3, str9.length());
        }
        ceVar.set(ceVar.protocol, str22, i6, str7, str4, (!this.bWT || str9 == null) ? str9 : fS(str9), str3, str18);
    }

    public static final String aq(String str, String str2) {
        StringBuffer stringBuffer = new StringBuffer();
        int length = str.length();
        int i = 0;
        boolean z = false;
        while (i < length) {
            char charAt = str.charAt(i);
            switch (charAt) {
                case '%':
                    try {
                        byte[] bArr = new byte[((length - i) / 3)];
                        int i2 = i;
                        int i3 = 0;
                        while (i2 + 2 < length && charAt == '%') {
                            int i4 = i3 + 1;
                            bArr[i3] = (byte) Integer.parseInt(str.substring(i2 + 1, i2 + 3), 16);
                            int i5 = i2 + 3;
                            if (i5 < length) {
                                charAt = str.charAt(i5);
                                i2 = i5;
                                i3 = i4;
                            } else {
                                i2 = i5;
                                i3 = i4;
                            }
                        }
                        if (i2 >= length || charAt != '%') {
                            stringBuffer.append(str2 != null ? new String(bArr, 0, i3, str2) : new String(bArr, 0, i3));
                            i = i2;
                            z = true;
                            break;
                        } else {
                            throw new IllegalArgumentException("URLDecoder: Incomplete trailing escape (%) pattern");
                        }
                    } catch (NumberFormatException e) {
                        throw new IllegalArgumentException("URLDecoder: Illegal hex characters in escape (%) pattern - " + e.getMessage());
                    }
                case '+':
                    stringBuffer.append(' ');
                    i++;
                    z = true;
                    break;
                default:
                    stringBuffer.append(charAt);
                    i++;
                    break;
            }
        }
        return z ? stringBuffer.toString() : str;
    }

    public static final String ar(String str, String str2) {
        if (bc.by(str)) {
            return "";
        }
        try {
            StringBuffer stringBuffer = new StringBuffer();
            byte[] dT = bc.dT(str);
            for (byte b2 : dT) {
                if (b2 == 32) {
                    stringBuffer.append("+");
                } else if (b2 == 10) {
                    stringBuffer.append("%0A");
                } else if ((b2 >= 48 && b2 <= 57) || ((b2 >= 65 && b2 <= 90) || ((b2 >= 97 && b2 <= 122) || b2 == 46 || b2 == 45 || b2 == 95 || b2 == 42))) {
                    stringBuffer.append((char) b2);
                } else if (str2 == null || str2.indexOf(b2) == -1) {
                    stringBuffer.append('%');
                    String hexString = Integer.toHexString(b2);
                    if (hexString.length() < 2) {
                        stringBuffer.append((char) ca.bNQ);
                    } else if (hexString.length() > 2) {
                        hexString = hexString.substring(hexString.length() - 2);
                    }
                    stringBuffer.append(hexString);
                } else {
                    stringBuffer.append((char) b2);
                }
            }
            return stringBuffer.toString();
        } catch (Exception e) {
            return str;
        }
    }

    public static String[] as(String str, String str2) {
        return b(str, str2, "", "", "", "");
    }

    public static final String at(String str, String str2) {
        int indexOf;
        String str3 = str + "=";
        if (str2 == null || (indexOf = str2.indexOf(str3)) == -1) {
            return null;
        }
        String substring = str2.substring(str3.length() + indexOf, str2.length());
        int indexOf2 = substring.indexOf(b.XF);
        return indexOf2 == -1 ? substring : substring.substring(0, indexOf2);
    }

    public static final String au(String str, String str2) {
        if (bc.by(str2)) {
            return str;
        }
        String trim = str2.trim();
        String lowerCase = trim.toLowerCase();
        if (lowerCase.startsWith(bx.bAh) || lowerCase.startsWith("https://") || lowerCase.startsWith(w.Os) || lowerCase.startsWith(b.acN) || lowerCase.startsWith("rtsp:") || lowerCase.startsWith(b.acp) || lowerCase.startsWith(b.acq) || lowerCase.startsWith(b.acr) || lowerCase.startsWith(b.acs)) {
            return trim;
        }
        ce ceVar = new ce(new ce((str.indexOf("://") != -1 || str.startsWith(w.Os)) ? str : bx.bAh + str), trim);
        return ceVar.a(ceVar);
    }

    public static final String b(String[] strArr, String[] strArr2) {
        if (strArr == null || strArr.length == 0) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            stringBuffer.append(fT(strArr[i]));
            stringBuffer.append("=");
            stringBuffer.append(fT(strArr2[i]));
            stringBuffer.append(b.XF);
        }
        stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        return stringBuffer.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bc.a(java.lang.String, java.lang.String, boolean):java.lang.String[]
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.uc.c.bc.a(char[], int, b.a.a.a.u):int
      com.uc.c.bc.a(int, b.a.a.a.f, int):b.a.a.a.f
      com.uc.c.bc.a(b.a.a.a.k, int, java.lang.String):void
      com.uc.c.bc.a(byte[], int, long):void
      com.uc.c.bc.a(byte[], int, short):void
      com.uc.c.bc.a(byte[], int, byte[]):void
      com.uc.c.bc.a(com.uc.c.ca, java.io.File, java.lang.String):boolean
      com.uc.c.bc.a(int[], int[], int[]):boolean
      com.uc.c.bc.a(java.io.InputStream, int, int):byte[]
      com.uc.c.bc.a(byte[], int, byte):byte[]
      com.uc.c.bc.a(int[], int, int):int[]
      com.uc.c.bc.a(short[], int, int):short[]
      com.uc.c.bc.a(java.lang.String, int, b.a.a.a.k):char[][]
      com.uc.c.bc.a(java.lang.String, java.lang.String, boolean):java.lang.String[] */
    public static String[] b(String str, String str2, String str3, String str4, String str5, String str6) {
        String str7;
        if (bc.dM(str4)) {
            return new String[]{str, str2, str3, str4, "down:lnk", str6};
        }
        if (bc.dM(str5)) {
            String dH = bb.dH(str5);
            if (bc.dM(dH)) {
                return new String[]{str, str2, str3, dH, "down:lnk", str6};
            }
        }
        String[] a2 = bc.a(str, au.aGF, false);
        String fX = q.fX();
        String str8 = a2.length == 1 ? fX : a2[a2.length - 1];
        try {
            int indexOf = str.indexOf("filename=");
            if (indexOf != -1) {
                String substring = str.substring("filename=".length() + indexOf, str.length());
                int indexOf2 = substring.indexOf(47);
                if (indexOf2 != -1) {
                    substring = substring.substring(0, indexOf2);
                }
                str7 = aq(substring, ca.bNE);
            } else {
                int indexOf3 = str8.indexOf("?");
                if (indexOf3 > 0) {
                    str8 = str8.substring(0, indexOf3);
                }
                try {
                    str7 = aq(str8, null);
                } catch (Exception e) {
                    str7 = fX;
                }
            }
        } catch (Exception e2) {
            str7 = fX;
        }
        return new String[]{str, str2, str3, str7, "down:lnk", str6};
    }

    private boolean fQ(String str) {
        return "http".equals(str) || "wtai".equals(str) || "https".equals(str) || a.IQ.equals(str) || "smsto".equals(str);
    }

    private String fR(String str) {
        if (bc.by(str)) {
            return str;
        }
        StringBuffer stringBuffer = new StringBuffer();
        String[] split = bc.split(str, b.XF);
        for (int i = 0; i < split.length; i++) {
            int indexOf = split[i].indexOf(61);
            if (indexOf != -1) {
                stringBuffer.append(fS(split[i].substring(0, indexOf))).append('=').append(fS(split[i].substring(indexOf + 1)));
            } else {
                stringBuffer.append(split[i]);
            }
            if (i != split.length - 1) {
                stringBuffer.append('&');
            }
        }
        return stringBuffer.toString();
    }

    private String fS(String str) {
        try {
            return fU(str);
        } catch (Exception e) {
            return str;
        }
    }

    public static final String fT(String str) {
        return ar(str, null);
    }

    public static final String fU(String str) {
        return ar(str, "/\\@#?%~!$^&*()_+-={}|[]:;,.");
    }

    public static final boolean fV(String str) {
        return str.startsWith("wap.") || str.startsWith("m.") || str.startsWith("3g.");
    }

    public static boolean fW(String str) {
        return !"img".equals(str);
    }

    public static String[] h(String str, String str2, String str3) {
        return b(str, str2, str3, "", "", "");
    }

    public static final String[] j(String str) {
        String str2;
        String str3;
        String str4;
        String str5;
        int indexOf = str.indexOf("://");
        int indexOf2 = str.indexOf(au.aGF);
        if (indexOf == -1 || indexOf2 != indexOf + 1) {
            str2 = "";
            str3 = str;
        } else {
            String str6 = str.substring(0, indexOf).equalsIgnoreCase("https") ? "https://" : "";
            str3 = str.substring(indexOf + 3);
            str2 = str6;
        }
        int indexOf3 = str3.indexOf(au.aGF);
        if (indexOf3 > 0) {
            String substring = str3.substring(0, indexOf3);
            str5 = str3.substring(indexOf3);
            if (str5 == null || str5.length() == 0) {
                str5 = au.aGF;
                str4 = substring;
            } else {
                str4 = substring;
            }
        } else {
            int indexOf4 = str3.indexOf("?");
            if (indexOf4 > 0) {
                String substring2 = str3.substring(0, indexOf4);
                str5 = str3.substring(indexOf4);
                str4 = substring2;
            } else if (!str3.startsWith("javascript:")) {
                str4 = str3;
                str5 = au.aGF;
            } else {
                str4 = str3;
                str5 = "";
            }
        }
        String[] strArr = new String[3];
        if (str4 != null && str4.endsWith(cd.bVI)) {
            str4 = str4.substring(0, str4.length() - 1);
        }
        strArr[0] = str4;
        strArr[1] = str5;
        strArr[2] = str2;
        return strArr;
    }

    public static final boolean k(String str) {
        String[] j = j(str.toLowerCase());
        if (fV(j[0])) {
            return true;
        }
        return j[1] != null && (j[1].endsWith(".wml") || j[1].endsWith(".xhtml") || ((j[1].contains(".wml") || j[1].contains(".xhtml")) && -1 != j[1].indexOf("#")));
    }

    private void set(String str, String str2, int i, String str3, String str4, String str5, String str6, String str7) {
        this.protocol = str;
        this.ZQ = str2;
        this.port = i;
        this.bWO = str6 == null ? str5 : str5 + "?" + str6;
        this.bWR = str4;
        this.bWQ = str5;
        this.bWS = str7;
        this.bWP = str6;
        this.authority = str3;
    }

    public String a(ce ceVar) {
        int length = (ceVar.protocol == null ? 0 : ceVar.protocol.length()) + 1;
        if (ceVar.authority != null && ceVar.authority.length() > 0) {
            length += ceVar.authority.length() + 2;
        }
        if (ceVar.bWQ != null) {
            length += ceVar.bWQ.length();
        }
        if (ceVar.bWP != null) {
            length += ceVar.bWP.length() + 1;
        }
        if (ceVar.bWS != null) {
            length += ceVar.bWS.length() + 1;
        }
        StringBuffer stringBuffer = new StringBuffer(length);
        if (ceVar.protocol != null) {
            stringBuffer.append(ceVar.protocol);
            stringBuffer.append(cd.bVI);
        }
        if (ceVar.authority != null && ceVar.authority.length() > 0) {
            stringBuffer.append("//");
            stringBuffer.append(ceVar.authority);
        }
        if (ceVar.bWQ != null) {
            stringBuffer.append(ceVar.bWQ.length() == 0 ? au.aGF : ceVar.bWQ);
        }
        if (ceVar.bWP != null) {
            stringBuffer.append('?');
            stringBuffer.append(ceVar.bWP);
        }
        if (ceVar.bWS != null) {
            stringBuffer.append("#");
            stringBuffer.append(ceVar.bWS);
        }
        return stringBuffer.toString();
    }

    public String toExternalForm() {
        return a(this);
    }
}
