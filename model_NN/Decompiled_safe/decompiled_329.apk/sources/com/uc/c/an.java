package com.uc.c;

import b.a.a.a.k;
import com.uc.b.e;
import java.util.Vector;

public class an {
    public static final byte agU = 0;
    public static final byte agV = 1;
    public static final byte agW = 2;
    public static final byte agX = 3;
    public static final byte agY = -1;
    public static final byte agZ = 0;
    public static final long ahG = -4503599358795015L;
    private static final an ahH = new an();
    private static final byte ahR = 1;
    public static int ahW = 0;
    public static int ahX = 0;
    public static final byte aha = 1;
    public static final byte ahb = -1;
    public static final byte ahc = 1;
    public static final byte ahd = 2;
    public static final byte ahe = 3;
    public static final byte ahf = 4;
    public static final byte aho = 0;
    public static final byte ahp = 1;
    public static final byte ahq = 0;
    public static final byte ahr = 1;
    public static final byte ahs = -1;
    public static final byte aht = 0;
    public static final byte ahu = 0;
    public static final byte ahv = 1;
    public static final byte ahw = 2;
    public static final byte ahx = 3;
    public static final byte ahy = 53;
    public static final byte ahz = 53;
    public static final int aic = -1;
    public static final int aid = 0;
    public static final int aie = 1;
    public static int aif = 200;
    public static final float aig = 0.6f;
    public static final float aih = 0.2f;
    public static final int aii = 5;
    public static final int aij = 0;
    public static final int aik = 1;
    public static final int ail = 15;
    private w Rp;
    public byte ahA;
    public byte ahB;
    public byte ahC;
    public byte ahD;
    private int ahE;
    private int ahF;
    boolean ahI;
    boolean ahJ;
    private bn ahK;
    private boolean ahL;
    private boolean ahM;
    private bq ahN;
    private int ahO;
    private int ahP;
    private Object[] ahQ;
    private int ahS;
    private int ahT;
    private int ahU;
    private ca ahV;
    int ahY;
    int ahZ;
    private Object[] ahg;
    private Object[] ahh;
    private Object[] ahi;
    private Object[] ahj;
    private Vector ahk;
    private int ahl;
    private byte ahm;
    private boolean ahn;
    int aia;
    int aib;

    private an() {
        this.ahg = null;
        this.ahh = null;
        this.ahi = null;
        this.ahj = null;
        this.ahk = null;
        this.ahl = 0;
        this.ahm = 0;
        this.ahn = false;
        this.ahA = 30;
        this.ahB = 30;
        this.ahC = 16;
        this.ahD = 16;
        this.ahE = k.adt * 2;
        this.ahF = (int) (((double) k.ads) * 2.5d);
        this.Rp = null;
        this.ahI = false;
        this.ahJ = false;
        this.ahK = null;
        this.ahL = false;
        this.ahM = false;
        this.ahN = null;
        this.ahO = 0;
        this.ahP = 0;
        this.ahQ = null;
        this.ahS = 0;
        this.ahT = w.ML / 5;
        this.ahU = 0;
        this.ahY = 0;
        this.ahZ = 0;
        this.aia = 0;
        this.aib = 0;
        this.Rp = w.ns();
        vN();
        ca jm = this.Rp.lA().jm();
        this.ahS = jm.bHq + e.bm(jm.bFQ);
        this.ahE = k.adt * 2;
        this.ahF = (int) (((double) k.ads) * 2.5d);
        this.ahT = k.ads * 3;
    }

    private void G(ca caVar) {
        boolean z;
        Vector b2 = caVar.b(caVar.bHl, caVar.bHm, 1);
        int[] iArr = {caVar.bGw, caVar.bGx, caVar.bGr, caVar.bGs};
        if (b2 == null || b2.size() <= 0) {
            if (caVar.LM() && bc.b(caVar.bGI, caVar.bGJ, iArr[0], iArr[1], iArr[2], iArr[3])) {
                z = b(caVar, caVar.bGI, caVar.bGJ);
            }
            z = false;
        } else {
            int[] iArr2 = new int[2];
            caVar.a(caVar.bHl, (byte[]) b2.elementAt(0), iArr2);
            if (bc.b(iArr2[0], iArr2[1], iArr[0], iArr[1], iArr[2], iArr[3])) {
                byte[] c = ca.c(caVar.bHl, caVar.bHm);
                int i = caVar.bHm;
                if (c[0] == 14) {
                    int i2 = i;
                    while (true) {
                        i2++;
                        byte[] c2 = ca.c(caVar.bHl, i2);
                        if (c2[0] != 15) {
                            long j = 1 << c2[0];
                            if ((ahG & j) == j) {
                                a(this.ahg, caVar.bHl, i2, -1);
                                z = true;
                                break;
                            }
                        } else {
                            z = false;
                            break;
                        }
                    }
                } else {
                    a(this.ahg, caVar.bHl, i, -1);
                    z = true;
                }
            }
            z = false;
        }
        if (!z) {
            iArr[1] = iArr[1] + (caVar.bGs >> 3);
            iArr[3] = iArr[3] - (caVar.bGs >> 3);
            Object[] a2 = a(caVar, iArr, 1);
            if (!k(a2)) {
                d(this.ahg, a2);
                z = true;
            }
        }
        if (z) {
            a(caVar, this.ahg);
        }
    }

    private void H(ca caVar) {
        I(caVar);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0162  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0177  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x02b2  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0002 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void I(com.uc.c.ca r21) {
        /*
            r20 = this;
            if (r21 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            r0 = r20
            java.lang.Object[] r0 = r0.ahg
            r4 = r0
            r0 = r20
            r1 = r4
            boolean r4 = r0.k(r1)
            if (r4 != 0) goto L_0x0002
            r0 = r20
            java.lang.Object[] r0 = r0.ahh
            r4 = r0
            r0 = r20
            r1 = r4
            boolean r4 = r0.k(r1)
            if (r4 != 0) goto L_0x0002
            r0 = r20
            java.lang.Object[] r0 = r0.ahg
            r4 = r0
            r0 = r20
            java.lang.Object[] r0 = r0.ahi
            r5 = r0
            r0 = r20
            r1 = r4
            r2 = r5
            boolean r4 = r0.f(r1, r2)
            if (r4 == 0) goto L_0x0047
            r0 = r20
            java.lang.Object[] r0 = r0.ahh
            r4 = r0
            r0 = r20
            java.lang.Object[] r0 = r0.ahj
            r5 = r0
            r0 = r20
            r1 = r4
            r2 = r5
            boolean r4 = r0.f(r1, r2)
            if (r4 != 0) goto L_0x0002
        L_0x0047:
            r0 = r20
            java.util.Vector r0 = r0.ahk
            r4 = r0
            r4.removeAllElements()
            r4 = 0
            r0 = r4
            r1 = r20
            r1.ahl = r0
            r0 = r20
            java.lang.Object[] r0 = r0.ahg
            r4 = r0
            r0 = r20
            java.lang.Object[] r0 = r0.ahh
            r5 = r0
            r0 = r20
            r1 = r4
            r2 = r5
            boolean r4 = r0.e(r1, r2)
            if (r4 == 0) goto L_0x00cf
            r0 = r20
            java.lang.Object[] r0 = r0.ahg
            r4 = r0
            r0 = r20
            r1 = r4
            com.uc.c.f r4 = r0.p(r1)
            r0 = r20
            java.lang.Object[] r0 = r0.ahg
            r5 = r0
            r0 = r20
            r1 = r5
            int r5 = r0.o(r1)
            byte[] r5 = com.uc.c.ca.c(r4, r5)
            r6 = 0
            byte r6 = r5[r6]
            if (r6 != 0) goto L_0x00b2
            r0 = r20
            int r0 = r0.ahl
            r6 = r0
            r0 = r20
            java.lang.Object[] r0 = r0.ahg
            r7 = r0
            r0 = r20
            r1 = r7
            int r7 = r0.n(r1)
            r0 = r20
            java.lang.Object[] r0 = r0.ahh
            r8 = r0
            r0 = r20
            r1 = r8
            int r8 = r0.n(r1)
            int r7 = r7 - r8
            int r7 = java.lang.Math.abs(r7)
            int r6 = r6 + r7
            r0 = r6
            r1 = r20
            r1.ahl = r0
        L_0x00b2:
            r6 = 2
            java.lang.Object[] r6 = new java.lang.Object[r6]
            r7 = 0
            r6[r7] = r5
            r5 = 1
            if (r4 == 0) goto L_0x00cd
            int r4 = r4.cJ
        L_0x00bd:
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r6[r5] = r4
            r0 = r20
            java.util.Vector r0 = r0.ahk
            r4 = r0
            r4.addElement(r6)
            goto L_0x0002
        L_0x00cd:
            r4 = 0
            goto L_0x00bd
        L_0x00cf:
            r4 = 1
            com.uc.c.f[] r10 = new com.uc.c.f[r4]
            r4 = 1
            int[] r11 = new int[r4]
            r4 = 0
            r5 = 0
            r6 = 0
            r0 = r21
            com.uc.c.f r0 = r0.bFR
            r7 = r0
            r10[r6] = r7
            r6 = 0
            r7 = -1
            r11[r6] = r7
            r6 = 0
            r8 = 1
            r0 = r20
            java.lang.Object[] r0 = r0.ahg
            r12 = r0
            r0 = r20
            r1 = r12
            int r12 = r0.m(r1)
            long r8 = r8 << r12
            long r6 = r6 | r8
            r8 = 1
            r0 = r20
            java.lang.Object[] r0 = r0.ahh
            r12 = r0
            r0 = r20
            r1 = r12
            int r12 = r0.m(r1)
            long r8 = r8 << r12
            long r6 = r6 | r8
            r8 = 1
            long r6 = r6 | r8
            r8 = 0
            r9 = 0
            r13 = r9
            r14 = r8
            r15 = r5
            r16 = r4
            r17 = r6
            r7 = r17
        L_0x0113:
            r0 = r21
            com.uc.c.i r0 = r0.bGd
            r4 = r0
            r0 = r21
            com.uc.c.f r0 = r0.bFR
            r5 = r0
            r6 = 0
            r9 = 0
            r12 = 1
            byte[] r5 = r4.b(r5, r6, r7, r9, r10, r11, r12)
            if (r5 == 0) goto L_0x02d2
            if (r16 == 0) goto L_0x018c
            if (r15 == 0) goto L_0x0186
            r0 = r20
            java.lang.Object[] r0 = r0.ahh
            r4 = r0
        L_0x012f:
            r6 = 0
            r6 = r10[r6]
            r9 = 0
            r9 = r11[r9]
            r0 = r20
            r1 = r4
            r2 = r6
            r3 = r9
            boolean r4 = r0.b(r1, r2, r3)
            if (r4 == 0) goto L_0x02c7
            r6 = 1
            r12 = r15
            r17 = r6
            r6 = r13
            r13 = r16
            r18 = r7
            r8 = r18
            r7 = r17
        L_0x014d:
            if (r13 == 0) goto L_0x02d9
            r14 = 0
            byte r14 = r5[r14]
            switch(r14) {
                case 0: goto L_0x021f;
                case 1: goto L_0x01ef;
                case 2: goto L_0x01da;
                case 12: goto L_0x01da;
                case 19: goto L_0x01ef;
                case 20: goto L_0x01ef;
                case 21: goto L_0x01ef;
                default: goto L_0x0155;
            }
        L_0x0155:
            r4 = r6
            r6 = 2
            java.lang.Object[] r6 = new java.lang.Object[r6]
            r14 = 0
            r6[r14] = r5
            r14 = 1
            r15 = 0
            r15 = r10[r15]
            if (r15 == 0) goto L_0x02b2
            r15 = 0
            r15 = r10[r15]
            int r15 = r15.cJ
        L_0x0167:
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)
            r6[r14] = r15
            r0 = r20
            java.util.Vector r0 = r0.ahk
            r14 = r0
            r14.addElement(r6)
        L_0x0175:
            if (r7 != 0) goto L_0x0002
            r6 = r7
            r17 = r8
            r7 = r17
            r9 = r12
            r12 = r13
        L_0x017e:
            if (r5 == 0) goto L_0x0002
            r13 = r4
            r14 = r6
            r15 = r9
            r16 = r12
            goto L_0x0113
        L_0x0186:
            r0 = r20
            java.lang.Object[] r0 = r0.ahg
            r4 = r0
            goto L_0x012f
        L_0x018c:
            r0 = r20
            java.lang.Object[] r0 = r0.ahg
            r4 = r0
            r6 = 0
            r6 = r10[r6]
            r9 = 0
            r9 = r11[r9]
            r0 = r20
            r1 = r4
            r2 = r6
            r3 = r9
            boolean r4 = r0.b(r1, r2, r3)
            if (r4 == 0) goto L_0x01c0
            r6 = 1
            r9 = 1
            r17 = r9
            r9 = r6
            r6 = r17
        L_0x01a9:
            if (r9 == 0) goto L_0x02b8
            if (r13 != 0) goto L_0x02b5
            r12 = 0
            r12 = r10[r12]
        L_0x01b0:
            r15 = 3674118(0x381006, double:1.8152555E-317)
            long r7 = r7 | r15
            r13 = r9
            r17 = r7
            r8 = r17
            r7 = r14
            r19 = r6
            r6 = r12
            r12 = r19
            goto L_0x014d
        L_0x01c0:
            r0 = r20
            java.lang.Object[] r0 = r0.ahh
            r4 = r0
            r6 = 0
            r6 = r10[r6]
            r9 = 0
            r9 = r11[r9]
            r0 = r20
            r1 = r4
            r2 = r6
            r3 = r9
            boolean r4 = r0.b(r1, r2, r3)
            if (r4 == 0) goto L_0x02c2
            r6 = 1
            r9 = r6
            r6 = r15
            goto L_0x01a9
        L_0x01da:
            r4 = 0
            r4 = r10[r4]
            if (r4 == 0) goto L_0x01ed
            r4 = 0
            r4 = r10[r4]
            int r4 = r4.cJ
        L_0x01e4:
            r0 = r20
            r1 = r5
            r2 = r4
            r0.u(r1, r2)
            r4 = r6
            goto L_0x0175
        L_0x01ed:
            r4 = 0
            goto L_0x01e4
        L_0x01ef:
            r14 = 0
            r4 = 0
            r4 = r10[r4]
            java.lang.Object[] r4 = r4.cE
            r15 = 0
            r15 = r11[r15]
            r4 = r4[r15]
            com.uc.c.f r4 = (com.uc.c.f) r4
            r10[r14] = r4
            r4 = 0
            r14 = -1
            r11[r4] = r14
            r4 = 0
            r4 = r10[r4]
            if (r4 == 0) goto L_0x021d
            r4 = 0
            r4 = r10[r4]
            int r4 = r4.cJ
        L_0x020c:
            r14 = 0
            byte r14 = r5[r14]
            r15 = 21
            if (r14 == r15) goto L_0x02d9
            r0 = r20
            r1 = r5
            r2 = r4
            r0.u(r1, r2)
            r4 = r6
            goto L_0x0175
        L_0x021d:
            r4 = 0
            goto L_0x020c
        L_0x021f:
            r14 = 0
            r14 = r10[r14]
            if (r6 == r14) goto L_0x025d
            byte[] r14 = r6.cH
            r15 = 0
            byte r14 = r14[r15]
            r15 = 21
            if (r14 != r15) goto L_0x024a
            byte[] r14 = r6.cH
            r15 = 0
            byte r14 = r14[r15]
            r15 = 21
            if (r14 != r15) goto L_0x025d
            com.uc.c.f r14 = r6.cG
            if (r14 == 0) goto L_0x025d
            com.uc.c.f r14 = r6.cG
            int r14 = r14.indexOf(r6)
            com.uc.c.f r15 = r6.cG
            short r15 = r15.cF
            r16 = 1
            int r15 = r15 - r16
            if (r14 != r15) goto L_0x025d
        L_0x024a:
            r14 = 0
            r14 = r10[r14]
            if (r14 == 0) goto L_0x028f
            r14 = 0
            r14 = r10[r14]
            int r14 = r14.cJ
        L_0x0254:
            byte[] r6 = r6.cH
            r0 = r20
            r1 = r6
            r2 = r14
            r0.u(r1, r2)
        L_0x025d:
            r6 = 0
            r6 = r10[r6]
            r14 = 0
            r14 = r10[r14]
            int r14 = r14.cJ
            r0 = r5
            r1 = r21
            r2 = r14
            char[] r14 = com.uc.c.f.a(r0, r1, r2)
            int r14 = r14.length
            if (r4 == 0) goto L_0x02b0
            if (r7 == 0) goto L_0x0297
            if (r12 == 0) goto L_0x0291
            r0 = r20
            java.lang.Object[] r0 = r0.ahh
            r4 = r0
        L_0x0279:
            r0 = r20
            r1 = r4
            int r4 = r0.n(r1)
            int r4 = r4 + 1
        L_0x0282:
            r0 = r20
            int r0 = r0.ahl
            r14 = r0
            int r4 = r4 + r14
            r0 = r4
            r1 = r20
            r1.ahl = r0
            goto L_0x0155
        L_0x028f:
            r14 = 0
            goto L_0x0254
        L_0x0291:
            r0 = r20
            java.lang.Object[] r0 = r0.ahg
            r4 = r0
            goto L_0x0279
        L_0x0297:
            r4 = 1
            int r4 = r14 - r4
            if (r12 == 0) goto L_0x02aa
            r0 = r20
            java.lang.Object[] r0 = r0.ahg
            r14 = r0
        L_0x02a1:
            r0 = r20
            r1 = r14
            int r14 = r0.n(r1)
            int r4 = r4 - r14
            goto L_0x0282
        L_0x02aa:
            r0 = r20
            java.lang.Object[] r0 = r0.ahh
            r14 = r0
            goto L_0x02a1
        L_0x02b0:
            r4 = r14
            goto L_0x0282
        L_0x02b2:
            r15 = 0
            goto L_0x0167
        L_0x02b5:
            r12 = r13
            goto L_0x01b0
        L_0x02b8:
            r12 = r6
            r6 = r13
            r13 = r9
            r17 = r7
            r8 = r17
            r7 = r14
            goto L_0x014d
        L_0x02c2:
            r6 = r15
            r9 = r16
            goto L_0x01a9
        L_0x02c7:
            r6 = r13
            r12 = r15
            r13 = r16
            r17 = r7
            r8 = r17
            r7 = r14
            goto L_0x014d
        L_0x02d2:
            r4 = r13
            r6 = r14
            r9 = r15
            r12 = r16
            goto L_0x017e
        L_0x02d9:
            r4 = r6
            goto L_0x0175
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.an.I(com.uc.c.ca):void");
    }

    private void a(int i, int i2, int i3, int i4, int i5, int i6) {
        b(i, i2, i3, i4, i5, i4, i5, i6);
    }

    private void a(ca caVar, Object[] objArr) {
        int[] a2 = a(objArr, caVar);
        int i = this.ahT;
        int i2 = a2[1] - (caVar.bGx + i);
        if (i2 < 0) {
            caVar.g(0, i2);
            return;
        }
        int i3 = (a2[2] + a2[1]) - ((caVar.bGx + caVar.bGs) - i);
        if (i3 > 0) {
            caVar.g(0, i3);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:75:0x019a, code lost:
        if (r5 == null) goto L_0x0100;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.uc.c.ca r25, java.lang.Object[] r26, int r27) {
        /*
            r24 = this;
            r0 = r24
            r1 = r26
            boolean r5 = r0.k(r1)
            if (r5 == 0) goto L_0x000b
        L_0x000a:
            return
        L_0x000b:
            r5 = 1
            r0 = r27
            r1 = r5
            if (r0 == r1) goto L_0x0025
            r5 = 2
            r0 = r27
            r1 = r5
            if (r0 == r1) goto L_0x0025
            r5 = 50
            r0 = r27
            r1 = r5
            if (r0 == r1) goto L_0x0025
            r5 = 52
            r0 = r27
            r1 = r5
            if (r0 != r1) goto L_0x0114
        L_0x0025:
            r5 = -1
            r13 = r5
        L_0x0027:
            r5 = -1
            if (r13 != r5) goto L_0x0118
            r5 = 1
            r14 = r5
        L_0x002c:
            r5 = 1
            r0 = r27
            r1 = r5
            if (r0 == r1) goto L_0x0046
            r5 = 50
            r0 = r27
            r1 = r5
            if (r0 == r1) goto L_0x0046
            r5 = 6
            r0 = r27
            r1 = r5
            if (r0 == r1) goto L_0x0046
            r5 = 56
            r0 = r27
            r1 = r5
            if (r0 != r1) goto L_0x011c
        L_0x0046:
            r5 = 1
            r15 = r5
        L_0x0048:
            r0 = r24
            r1 = r26
            java.lang.Object[] r16 = r0.j(r1)
            r0 = r24
            r1 = r16
            com.uc.c.f r17 = r0.p(r1)
            r0 = r24
            r1 = r16
            int r5 = r0.o(r1)
            r0 = r24
            r1 = r16
            int r6 = r0.n(r1)
            r7 = 0
            r8 = 0
            r9 = 0
            if (r15 == 0) goto L_0x01b9
            r0 = r24
            r1 = r16
            r2 = r25
            int[] r7 = r0.a(r1, r2)
            r18 = r7
        L_0x0079:
            r8 = 1
            com.uc.c.f[] r11 = new com.uc.c.f[r8]
            r8 = 1
            int[] r12 = new int[r8]
            r0 = r17
            r1 = r5
            byte[] r8 = com.uc.c.ca.c(r0, r1)
            r10 = 0
            r11[r10] = r17
            r10 = 0
            r12[r10] = r5
            r5 = 0
            r23 = r8
            r8 = r9
            r9 = r7
            r7 = r23
        L_0x0093:
            r10 = 1
            r19 = 0
            byte r19 = r7[r19]
            if (r19 != 0) goto L_0x0120
            r19 = 1
        L_0x009c:
            r20 = 0
            if (r19 == 0) goto L_0x01a7
            r0 = r17
            int r0 = r0.cJ
            r10 = r0
            r0 = r7
            r1 = r25
            r2 = r10
            char[] r7 = com.uc.c.f.a(r0, r1, r2)
            int r10 = r7.length
            r20 = r6
            r21 = r8
            r22 = r9
            r6 = r10
            r23 = r7
            r7 = r5
            r5 = r23
        L_0x00ba:
            if (r7 == 0) goto L_0x0128
            if (r14 == 0) goto L_0x0126
            if (r19 == 0) goto L_0x0124
            int r7 = r5.length
            r8 = 1
            int r7 = r7 - r8
        L_0x00c3:
            r8 = 0
            r23 = r8
            r8 = r7
            r7 = r23
        L_0x00c9:
            r9 = 0
            r9 = r11[r9]
            r10 = 0
            r10 = r12[r10]
            r0 = r24
            r1 = r16
            r2 = r9
            r3 = r10
            r4 = r8
            r0.a(r1, r2, r3, r4)
            if (r15 == 0) goto L_0x0100
            r0 = r24
            r1 = r16
            r2 = r25
            int[] r9 = r0.a(r1, r2)
            if (r14 == 0) goto L_0x0153
            if (r21 == 0) goto L_0x0140
            r10 = 0
            r10 = r9[r10]
            r20 = 0
            r20 = r18[r20]
            r0 = r10
            r1 = r20
            if (r0 <= r1) goto L_0x0100
            r0 = r24
            r1 = r9
            r2 = r22
            boolean r9 = r0.c(r1, r2)
            if (r9 == 0) goto L_0x016c
        L_0x0100:
            r0 = r24
            r1 = r25
            r2 = r16
            r0.a(r1, r2)
            r0 = r24
            r1 = r26
            r2 = r16
            r0.d(r1, r2)
            goto L_0x000a
        L_0x0114:
            r5 = 1
            r13 = r5
            goto L_0x0027
        L_0x0118:
            r5 = 0
            r14 = r5
            goto L_0x002c
        L_0x011c:
            r5 = 0
            r15 = r5
            goto L_0x0048
        L_0x0120:
            r19 = 0
            goto L_0x009c
        L_0x0124:
            r7 = 0
            goto L_0x00c3
        L_0x0126:
            r7 = -1
            goto L_0x00c3
        L_0x0128:
            if (r14 == 0) goto L_0x0133
            r8 = -1
            r0 = r20
            r1 = r8
            if (r0 <= r1) goto L_0x0133
            int r8 = r20 + -1
            goto L_0x00c9
        L_0x0133:
            if (r14 != 0) goto L_0x0183
            r8 = 1
            int r8 = r6 - r8
            r0 = r20
            r1 = r8
            if (r0 >= r1) goto L_0x0183
            int r8 = r20 + 1
            goto L_0x00c9
        L_0x0140:
            r0 = r24
            r1 = r9
            r2 = r18
            boolean r10 = r0.c(r1, r2)
            if (r10 == 0) goto L_0x01b3
            r20 = r8
            r21 = r10
            r22 = r9
            goto L_0x00ba
        L_0x0153:
            if (r21 == 0) goto L_0x0170
            r10 = 0
            r10 = r9[r10]
            r20 = 0
            r20 = r18[r20]
            r0 = r10
            r1 = r20
            if (r0 >= r1) goto L_0x0100
            r0 = r24
            r1 = r22
            r2 = r9
            boolean r9 = r0.c(r1, r2)
            if (r9 != 0) goto L_0x0100
        L_0x016c:
            r20 = r8
            goto L_0x00ba
        L_0x0170:
            r0 = r24
            r1 = r18
            r2 = r9
            boolean r10 = r0.c(r1, r2)
            if (r10 == 0) goto L_0x01b3
            r20 = r8
            r21 = r10
            r22 = r9
            goto L_0x00ba
        L_0x0183:
            r19 = 1
            r0 = r25
            com.uc.c.i r0 = r0.bGd
            r5 = r0
            r0 = r25
            com.uc.c.f r0 = r0.bFR
            r6 = r0
            r7 = 0
            r8 = -4503599358795015(0xfff00000100222f9, double:NaN)
            r10 = 0
            byte[] r5 = r5.b(r6, r7, r8, r10, r11, r12, r13)
            if (r5 == 0) goto L_0x0100
            r6 = r20
            r7 = r5
            r8 = r21
            r9 = r22
            r5 = r19
            goto L_0x0093
        L_0x01a7:
            r7 = r5
            r21 = r8
            r22 = r9
            r5 = r20
            r20 = r6
            r6 = r10
            goto L_0x00ba
        L_0x01b3:
            r20 = r8
            r21 = r10
            goto L_0x00ba
        L_0x01b9:
            r18 = r7
            r7 = r8
            goto L_0x0079
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.an.a(com.uc.c.ca, java.lang.Object[], int):void");
    }

    private void a(Object[] objArr, int i) {
        ((int[]) objArr[1])[0] = i;
    }

    private void a(Object[] objArr, ca caVar, int i, int i2, int i3, int i4) {
        Object[] objArr2;
        boolean z;
        this.ahU = caVar.ahU;
        if (this.ahP > caVar.ahU - this.ahT || this.ahP < this.ahT) {
            this.ahL = false;
            a(caVar, objArr, i, i2, i3, i4);
        } else if (vR() || vS()) {
            this.ahL = true;
            this.ahQ = objArr;
            if (this.ahN == null || !this.ahN.isAlive()) {
                this.ahN = new bq(19, null);
                this.ahM = false;
                this.ahN.start();
            } else if (this.ahM) {
                synchronized (this) {
                    notify();
                }
            }
        } else {
            this.ahL = false;
            Object[] c = c(caVar, i, i2);
            if (!k(c)) {
                int[] a2 = a(c, caVar);
                if (this.ahg == objArr) {
                    objArr2 = this.ahh;
                    z = true;
                } else {
                    objArr2 = this.ahg;
                    z = false;
                }
                int[] a3 = a(objArr2, caVar);
                if (!z || ((a2[0] >= a3[0] || a2[1] != a3[1]) && a2[1] >= a3[1])) {
                    if (z) {
                        return;
                    }
                    if ((a2[0] <= a3[0] || a2[1] != a3[1]) && a2[1] <= a3[1]) {
                        return;
                    }
                }
                d(objArr, c);
            }
        }
    }

    private void a(Object[] objArr, f fVar) {
        objArr[0] = fVar;
    }

    private void a(Object[] objArr, f fVar, int i) {
        a(objArr, fVar);
        a(objArr, i);
    }

    private void a(Object[] objArr, f fVar, int i, int i2) {
        a(objArr, fVar);
        a(objArr, i);
        b(objArr, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.an.a(com.uc.c.ca, int, int, int[], boolean):java.lang.Object[]
     arg types: [com.uc.c.ca, int, int, int[], int]
     candidates:
      com.uc.c.an.a(com.uc.c.ca, java.lang.Object[], int[], java.lang.Object[], java.lang.Object[]):boolean
      com.uc.c.an.a(com.uc.c.ca, int, int, int[], boolean):java.lang.Object[] */
    private boolean a(ca caVar, int[] iArr, Object[] objArr, Object[] objArr2) {
        int i = iArr[0];
        int i2 = iArr[1];
        Object[] a2 = a(caVar, i, i2, new int[]{0, i2, caVar.bGm, i2 + 15 <= caVar.ahU ? 15 : caVar.ahU - i2}, true);
        if (a2 == null) {
            a2 = a(caVar, i, i2, new int[]{0, caVar.bGx, caVar.bGm, caVar.bGx + caVar.bGs <= caVar.ahU ? caVar.bGs : caVar.ahU - caVar.bGx}, false);
        }
        if (a2 == null && caVar.bGx + caVar.bGs < caVar.ahU) {
            a2 = a(caVar, i, i2, new int[]{0, caVar.bGx + caVar.bGs, caVar.bGm, caVar.ahU - (caVar.bGx + caVar.bGs)}, false);
        }
        if (a2 == null && caVar.bGx > 0) {
            a2 = a(caVar, i, i2, new int[]{0, 0, caVar.bGm, caVar.bGx}, false);
        }
        if (a2 != null) {
            return a(caVar, a2, objArr, objArr2);
        }
        return false;
    }

    private boolean a(ca caVar, Object[] objArr, int i, int i2, int i3, int i4) {
        Object[] c = c(caVar, i, i2);
        if (k(c)) {
            this.ahL = false;
            return false;
        }
        if (!f(objArr, c)) {
            int[] a2 = a(c, caVar);
            boolean z = this.ahg == objArr;
            int[] a3 = a(z ? this.ahh : this.ahg, caVar);
            if ((z && a2[0] < a3[0] && a2[1] <= a3[1]) || (!z && a2[0] > a3[0] && a2[1] >= a3[1])) {
                a(caVar, c);
                d(objArr, c);
            }
        } else if (f.e(ca.c(p(objArr), o(objArr))) >= caVar.bGs) {
            caVar.g(0, i2 - i4 < 0 ? -(caVar.bGs >>> 3) : caVar.bGs >>> 3);
        }
        return true;
    }

    private boolean a(ca caVar, Object[] objArr, Object[] objArr2, Object[] objArr3) {
        int i;
        int i2;
        int i3;
        f p = p(objArr);
        int o = o(objArr);
        int n = n(objArr);
        byte[] c = ca.c(p, o);
        if (c == null) {
            return false;
        }
        if (c != null && c[0] == 0) {
            byte[][] b2 = f.b(caVar, c, p.cJ);
            char[] a2 = f.a(c, caVar, p.cJ);
            if (b2 != null) {
                int i4 = 0;
                i2 = -1;
                while (true) {
                    if (i4 >= b2.length) {
                        i = -1;
                        break;
                    }
                    int F = ca.F(b2[i4], 10);
                    if (n == -1 || (n >= i2 && n < F)) {
                        byte[] bArr = b2[i4];
                        i = F - 1;
                    } else {
                        i4++;
                        i2 = F;
                    }
                }
            } else if (a2 != null) {
                i = a2.length - 1;
                i2 = 0;
            } else {
                i = -1;
                i2 = -1;
            }
            if (i - i2 > 0 || (i2 == i && i2 > 0)) {
                if (i - i2 < 5) {
                    i3 = i2 - 1;
                } else {
                    int i5 = i - i2;
                    i3 = (int) (((float) i2) + (((float) i5) * 0.2f));
                    int i6 = (int) ((((float) i5) * 0.6f) + ((float) i3));
                    if (i6 <= i) {
                        i = i6;
                    }
                }
                d(objArr2, objArr);
                b(objArr2, i3);
                d(objArr3, objArr);
                b(objArr3, i);
                return true;
            } else if (i2 == 0 && i == 0) {
                d(objArr2, objArr);
                b(objArr2, -1);
                d(objArr3, objArr);
                b(objArr3, i);
                return true;
            }
        }
        return false;
    }

    private boolean a(Object[] objArr, ca caVar, int i, int i2) {
        int[] a2 = a(objArr, caVar);
        this.aia = a2[0];
        this.aib = objArr == this.ahg ? a2[1] : a2[1] + a2[2];
        if (objArr == this.ahg) {
            return bc.b(i, i2, (int) (((double) a2[0]) - (((double) this.ahE) * 0.5d)), a2[1] - this.ahF, this.ahE, this.ahF + a2[2]);
        }
        return bc.b(i, i2, (int) (((double) a2[0]) - (((double) this.ahE) * 0.5d)), a2[1], this.ahE, this.ahF + a2[2]);
    }

    private boolean a(Object[] objArr, byte[] bArr) {
        return ca.c(p(objArr), o(objArr)) == bArr;
    }

    private int[] a(ca caVar, Object[] objArr, boolean z) {
        int[] iArr = new int[4];
        int[] a2 = a(objArr, caVar);
        iArr[0] = a2[0] - ((z ? this.ahA : this.ahC) >> 1);
        iArr[1] = a2[1] - (z ? this.ahB : 0);
        iArr[2] = z ? this.ahA : this.ahC;
        iArr[2] = iArr[2] + 2;
        iArr[3] = a2[2] + (z ? this.ahB : this.ahD);
        return iArr;
    }

    private int[] a(Object[] objArr, ca caVar) {
        int i;
        int i2;
        byte[] bArr;
        int i3;
        byte[] bArr2;
        boolean z;
        int F;
        int[] iArr = null;
        f p = p(objArr);
        int o = o(objArr);
        int n = n(objArr);
        byte[] c = ca.c(p, o);
        int[] iArr2 = new int[2];
        if (c != null) {
            iArr = new int[3];
            if (c[0] == 0) {
                byte[][] b2 = f.b(caVar, c, p.cJ);
                char[] a2 = f.a(c, caVar, p.cJ);
                int f = f.f(c, 1);
                if (f == -1) {
                    f = caVar.bFQ;
                }
                if (b2 != null) {
                    int i4 = 0;
                    int i5 = 0;
                    while (true) {
                        if (i5 >= b2.length) {
                            i3 = i4;
                            bArr2 = c;
                            z = true;
                            break;
                        }
                        F = ca.F(b2[i5], 10);
                        if (n != -1 && (n < i4 || n >= F)) {
                            i5++;
                            i4 = F;
                        }
                    }
                    if (!this.ahI || n != F - 1 || i5 >= b2.length - 1) {
                        byte[] bArr3 = b2[i5];
                        i3 = i4;
                        bArr2 = bArr3;
                        z = true;
                    } else {
                        bArr2 = b2[i5 + 1];
                        z = false;
                        i3 = i4;
                    }
                } else {
                    i3 = 0;
                    bArr2 = c;
                    z = true;
                }
                int b3 = z ? e.bj(f).b(a2, i3, (n - i3) + 1) : 0;
                caVar.a(p, bArr2, iArr2);
                int i6 = iArr2[0] + b3;
                i = iArr2[1];
                i2 = i6;
                bArr = bArr2;
            } else {
                caVar.a(p, c, iArr2);
                int d = n == -1 ? iArr2[0] : iArr2[0] + f.d(c);
                byte[] bArr4 = c;
                i = iArr2[1];
                i2 = d;
                bArr = bArr4;
            }
            iArr[2] = f.e(bArr);
            iArr[0] = i2;
            iArr[1] = i;
            if (iArr[2] >= caVar.bGs) {
                iArr[1] = iArr[1] + (iArr[2] - 20);
                iArr[2] = 20;
            }
        }
        return iArr;
    }

    private Object[] a(ca caVar, int i, int i2, int[] iArr, boolean z) {
        Object[] objArr;
        Object[] objArr2;
        int i3;
        Object[] objArr3;
        Object[] objArr4;
        f[] fVarArr = {caVar.bFR};
        int[] iArr2 = {-1};
        int i4 = Integer.MAX_VALUE;
        Object[] objArr5 = null;
        Object[] objArr6 = null;
        while (true) {
            byte[] b2 = caVar.bGd.b(caVar.bFR, null, 1, iArr, fVarArr, iArr2, 1);
            if (b2 != null) {
                if (objArr6 == null) {
                    objArr3 = vv();
                    a(objArr3, fVarArr[0], iArr2[0], -1);
                    objArr4 = vv();
                } else {
                    objArr3 = objArr5;
                    objArr4 = objArr6;
                }
                a(objArr4, fVarArr[0], iArr2[0]);
                int i5 = 0;
                if (b2[0] == 0) {
                    i5 = f.a(b2, caVar, fVarArr[0].cJ).length - 1;
                }
                for (int i6 = -1; i6 <= i5; i6++) {
                    b(objArr4, i6);
                    int[] a2 = a(objArr4, caVar);
                    int abs = Math.abs(((a2[1] - i2) * (a2[1] - i2)) + ((a2[0] - i) * (a2[0] - i)));
                    if (abs < i4) {
                        d(objArr3, objArr4);
                        i4 = abs;
                    }
                }
                objArr = objArr4;
                objArr2 = objArr3;
                i3 = i4;
            } else {
                i3 = i4;
                objArr2 = objArr5;
                objArr = objArr6;
            }
            if (b2 == null) {
                return objArr2;
            }
            i4 = i3;
            objArr5 = objArr2;
            objArr6 = objArr;
        }
    }

    private Object[] a(ca caVar, int[] iArr, int i) {
        int[] iArr2 = new int[2];
        f[] fVarArr = {caVar.bFR};
        int[] iArr3 = {-1};
        while (caVar.bGd.b(caVar.bFR, null, ahG, iArr, fVarArr, iArr3, i) != null) {
            Vector b2 = caVar.b(fVarArr[0], iArr3[0], 1);
            if (b2 != null) {
                int size = b2.size();
                boolean z = i == -1;
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= size) {
                        continue;
                        break;
                    }
                    byte[] bArr = (byte[]) b2.elementAt(z ? (size - 1) - i3 : i3);
                    caVar.a(fVarArr[0], bArr, iArr2);
                    int e = f.e(bArr);
                    if (z ? bc.b(iArr2[0], iArr2[1] + e, iArr[0], iArr[1], iArr[2], iArr[3]) : bc.b(iArr2[0], iArr2[1], iArr[0], iArr[1], iArr[2], iArr[3]) || iArr2[1] + e >= iArr[1] + iArr[3]) {
                        int i4 = z ? 0 : -1;
                        if (z) {
                            if (bArr[0] == 0) {
                                i4 = f.a(bArr, caVar, fVarArr[0].cJ).length - 1;
                            } else if (bArr[0] == 120) {
                                ca.c(fVarArr[0], iArr3[0]);
                                i4 = ca.F(bArr, 9) - 1;
                            }
                        } else if (bArr[0] == 120 && i3 != 0) {
                            byte[] bArr2 = (byte[]) b2.elementAt(i3 - 1);
                            if (bArr2[0] == 120) {
                                i4 = ca.F(bArr2, 9);
                            }
                        }
                        Object[] vv = vv();
                        a(vv, fVarArr[0], iArr3[0], i4);
                        return vv;
                    }
                    i2 = i3 + 1;
                }
            }
        }
        return null;
    }

    private void ax(int i, int i2) {
        a(1, i2, -1, 0, 0, -1);
    }

    private void b(bf bfVar, bf bfVar2) {
        this.Rp.lA().b(bfVar, bfVar2);
    }

    private void b(Object[] objArr, int i) {
        ((int[]) objArr[1])[1] = i;
    }

    private boolean b(int i, int i2, int i3, int i4, int i5) {
        return b(-1, 0, i, i2, i3, i4, i5, -1);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private boolean b(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        ca jm;
        boolean z;
        boolean z2 = i == 1 && (i2 == 1 || i2 == 2 || i2 == 6 || i2 == 5 || i2 == 50 || i2 == 52 || i2 == 56 || i2 == 54);
        boolean z3 = i == 1 && (i2 == 53 || i2 == 8);
        boolean z4 = i == 1;
        boolean z5 = i3 == 2 || i3 == 3 || i3 == 4;
        r lA = this.Rp.lA();
        if (lA == null || (jm = lA.jm()) == null) {
            return false;
        }
        byte b2 = this.ahm;
        if (z5) {
            this.ahO = i4;
            this.ahP = i5;
        }
        if (i8 != 1) {
            switch (this.ahm) {
                case 0:
                    if (i8 == 0) {
                        vt();
                        this.ahm = 1;
                        if (!this.ahn) {
                            G(jm);
                            z = true;
                            break;
                        } else {
                            z = true;
                            break;
                        }
                    }
                    z = true;
                    break;
                case 1:
                    if (z4) {
                        if (k(this.ahg)) {
                            G(jm);
                        }
                        if (!z3) {
                            if (z2) {
                                a(jm, this.ahg, i2);
                                z = true;
                                break;
                            }
                            z = true;
                            break;
                        } else {
                            this.ahm = 2;
                            d(this.ahh, this.ahg);
                            z = true;
                            break;
                        }
                    } else {
                        if (z5) {
                            switch (i3) {
                                case 2:
                                    if (k(this.ahg)) {
                                        b(jm, i4, i5);
                                        this.ahI = true;
                                    } else if (!a(this.ahg, jm, i4, i5)) {
                                        l(this.ahg);
                                        this.ahI = false;
                                    } else {
                                        this.ahI = true;
                                    }
                                    z = true;
                                    break;
                                case 3:
                                    if (k(this.ahg)) {
                                        b(jm, i4, i5);
                                    }
                                    if (!k(this.ahg)) {
                                        d(this.ahh, this.ahg);
                                        this.ahJ = true;
                                        this.ahm = 2;
                                    }
                                    z = true;
                                    break;
                                default:
                                    z = true;
                                    break;
                            }
                        }
                        z = true;
                    }
                case 2:
                    if (z4) {
                        if (!z3) {
                            if (z2) {
                                a(jm, this.ahh, i2);
                                z = true;
                                break;
                            }
                            z = true;
                            break;
                        } else {
                            vz();
                            z = true;
                            break;
                        }
                    } else {
                        if (z5) {
                            switch (i3) {
                                case 2:
                                    boolean a2 = a(this.ahg, jm, i4, i5);
                                    this.ahI = a2;
                                    if (!a2) {
                                        boolean a3 = a(this.ahh, jm, i4, i5);
                                        this.ahJ = a3;
                                        if (!a3) {
                                            z = false;
                                            break;
                                        } else {
                                            this.ahI = false;
                                            z = true;
                                            break;
                                        }
                                    } else {
                                        this.ahm = 3;
                                        this.ahJ = false;
                                        z = true;
                                        break;
                                    }
                                case 3:
                                    if (!this.ahJ) {
                                        z = false;
                                        break;
                                    } else {
                                        int[] ay = ay(i4, i5);
                                        a(this.ahh, jm, ay[0], ay[1], i6, i7);
                                        z = true;
                                        break;
                                    }
                            }
                        }
                        z = true;
                    }
                    break;
                case 3:
                    if (z4) {
                        if (!z3) {
                            if (z2) {
                                a(jm, this.ahg, i2);
                                z = true;
                                break;
                            }
                            z = true;
                            break;
                        } else {
                            vz();
                            z = true;
                            break;
                        }
                    } else {
                        if (z5) {
                            switch (i3) {
                                case 2:
                                    boolean a4 = a(this.ahh, jm, i4, i5);
                                    this.ahJ = a4;
                                    if (!a4) {
                                        boolean a5 = a(this.ahg, jm, i4, i5);
                                        this.ahI = a5;
                                        if (!a5) {
                                            z = false;
                                            break;
                                        } else {
                                            this.ahJ = false;
                                            z = true;
                                            break;
                                        }
                                    } else {
                                        this.ahm = 2;
                                        this.ahI = false;
                                        z = true;
                                        break;
                                    }
                                case 3:
                                    if (!this.ahI) {
                                        z = false;
                                        break;
                                    } else {
                                        int[] ay2 = ay(i4, i5);
                                        a(this.ahg, jm, ay2[0], ay2[1], i6, i7);
                                        z = true;
                                        break;
                                    }
                            }
                        }
                        z = true;
                    }
                    break;
                default:
                    z = true;
                    break;
            }
        } else {
            try {
                vw();
                z = true;
            } catch (Exception e) {
                vw();
            }
        }
        this.ahn = false;
        if ((b2 == 0 && this.ahm == 1) || (b2 != 0 && this.ahm == 0)) {
            jm.iH(1);
        }
        this.ahY = i4;
        this.ahZ = i5;
        return z;
    }

    private boolean b(ca caVar, int i, int i2) {
        Object[] c = c(caVar, i, i2);
        if (k(c)) {
            return false;
        }
        d(this.ahg, c);
        return true;
    }

    private boolean b(ca caVar, Object[] objArr, Object[] objArr2, Object[] objArr3) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        if (caVar == null || objArr == null || objArr[0] == null || objArr[1] == null) {
            return false;
        }
        f fVar = (f) objArr[0];
        int intValue = ((Integer) objArr[1]).intValue();
        byte[] c = ca.c(fVar, intValue);
        if (c != null && 14 == c[0]) {
            f[] fVarArr = {fVar};
            int[] iArr = {intValue};
            boolean z6 = false;
            boolean z7 = false;
            boolean z8 = false;
            while (true) {
                byte[] a2 = caVar.bGd.a(fVar, (f) null, 32769, (int[]) null, fVarArr, iArr, 1);
                if (a2 == null) {
                    z = z7;
                    z2 = z8;
                    break;
                } else if (a2[0] == 0) {
                    if (!z6) {
                        a(objArr2, fVarArr[0], iArr[0], -1);
                        z3 = true;
                        z4 = true;
                    } else {
                        z3 = z6;
                        z4 = z8;
                    }
                    char[] a3 = f.a(a2, caVar, fVarArr[0].cJ);
                    if (a3 != null) {
                        a(objArr3, fVarArr[0], iArr[0], a3.length - 1);
                        z5 = true;
                    } else {
                        z5 = z7;
                    }
                    z6 = z3;
                    z7 = z5;
                    z8 = z4;
                } else if (a2[0] == 15) {
                    z = z7;
                    z2 = z8;
                    break;
                }
            }
        } else {
            z = false;
            z2 = false;
        }
        return z2 && z;
    }

    private boolean b(Object[] objArr, f fVar, int i) {
        return p(objArr) == fVar && o(objArr) == i;
    }

    private boolean c(int[] iArr, int[] iArr2) {
        return iArr[1] + iArr[2] <= iArr2[1];
    }

    private Object[] c(ca caVar, int i, int i2) {
        Object[] objArr;
        Object[] objArr2;
        int i3;
        Object[] objArr3;
        Object[] objArr4;
        f[] fVarArr = {caVar.bFR};
        int[] iArr = {-1};
        int[] iArr2 = {0, i2, caVar.bGm, 10};
        int i4 = Integer.MAX_VALUE;
        Object[] objArr5 = null;
        Object[] objArr6 = null;
        while (true) {
            byte[] b2 = caVar.bGd.b(caVar.bFR, null, ahG, iArr2, fVarArr, iArr, 1);
            if (b2 != null) {
                if (objArr6 == null) {
                    objArr3 = vv();
                    a(objArr3, fVarArr[0], iArr[0], -1);
                    objArr4 = vv();
                } else {
                    objArr3 = objArr5;
                    objArr4 = objArr6;
                }
                a(objArr4, fVarArr[0], iArr[0]);
                int i5 = 0;
                if (b2[0] == 0) {
                    i5 = f.a(b2, caVar, fVarArr[0].cJ).length - 1;
                }
                int i6 = i4;
                for (int i7 = -1; i7 <= i5; i7++) {
                    b(objArr4, i7);
                    int[] a2 = a(objArr4, caVar);
                    int abs = Math.abs(((a2[1] - i2) * (a2[1] - i2)) + ((a2[0] - i) * (a2[0] - i)));
                    if (abs < i6) {
                        d(objArr3, objArr4);
                        i6 = abs;
                    }
                }
                objArr = objArr4;
                objArr2 = objArr3;
                i3 = i6;
            } else {
                i3 = i4;
                objArr2 = objArr5;
                objArr = objArr6;
            }
            if (b2 == null) {
                return objArr2;
            }
            i4 = i3;
            objArr5 = objArr2;
            objArr6 = objArr;
        }
    }

    private void d(Object[] objArr, Object[] objArr2) {
        a(objArr, p(objArr2), o(objArr2), n(objArr2));
    }

    private boolean e(Object[] objArr, Object[] objArr2) {
        return p(objArr) == p(objArr2) && o(objArr) == o(objArr2);
    }

    private void i(Object[] objArr) {
        if (objArr != null) {
            objArr[0] = null;
            objArr[1] = null;
        }
    }

    private Object[] j(Object[] objArr) {
        Object[] vv = vv();
        d(vv, objArr);
        return vv;
    }

    private boolean k(Object[] objArr) {
        return p(objArr) == null;
    }

    private void l(Object[] objArr) {
        objArr[0] = null;
    }

    private int m(Object[] objArr) {
        return ca.c(p(objArr), o(objArr))[0];
    }

    private void u(byte[] bArr, int i) {
        boolean z;
        if (bArr[0] == 2 || bArr[0] == 1 || bArr[0] == 12 || bArr[0] == 20 || bArr[0] == 19 || bArr[0] == 21) {
            if (this.ahk.size() == 0) {
                z = true;
            } else {
                Object[] objArr = (Object[]) this.ahk.lastElement();
                byte[] bArr2 = objArr != null ? (byte[]) objArr[0] : null;
                z = (bArr == bArr2 || bArr2[0] == 2 || bArr2[0] == 1 || bArr2[0] == 12 || bArr2[0] == 20 || bArr2[0] == 19 || bArr2[0] == 21) ? false : true;
            }
            if (z) {
                this.ahl++;
                this.ahk.addElement(new Object[]{bArr, Integer.valueOf(i)});
            }
        }
    }

    public static an vA() {
        return ahH;
    }

    private String vE() {
        int i;
        byte[] bArr;
        int length;
        int i2;
        ca jm = this.Rp.lA().jm();
        if (jm == null) {
            return "";
        }
        H(jm);
        StringBuffer stringBuffer = new StringBuffer();
        if (f(this.ahg, this.ahh)) {
            return null;
        }
        if (e(this.ahg, this.ahh)) {
            byte[] c = ca.c(p(this.ahg), o(this.ahg));
            int min = Math.min(n(this.ahg), n(this.ahh)) + 1;
            int max = Math.max(n(this.ahg), n(this.ahh));
            if (c[0] == 0) {
                stringBuffer.append(f.a(c, jm, p(this.ahg).cJ), min, (max - min) + 1);
                return stringBuffer.toString();
            }
        } else if (this.ahk != null && this.ahk.size() > 0) {
            for (int i3 = 0; i3 < this.ahk.size(); i3++) {
                Object[] objArr = (Object[]) this.ahk.elementAt(i3);
                if (objArr != null) {
                    bArr = (byte[]) objArr[0];
                    i = objArr[1] != null ? ((Integer) objArr[1]).intValue() : 0;
                } else {
                    i = 0;
                    bArr = null;
                }
                if (bArr[0] == 2 || bArr[0] == 1 || bArr[0] == 12 || bArr[0] == 20 || bArr[0] == 19 || bArr[0] == 21) {
                    stringBuffer.append(10);
                } else if (bArr[0] == 0) {
                    char[] a2 = f.a(bArr, jm, i);
                    if (i3 == 0) {
                        Object[] objArr2 = a(this.ahg, bArr) ? this.ahg : this.ahh;
                        int length2 = a2.length - 1;
                        i2 = n(objArr2) + 1;
                        length = length2;
                    } else if (i3 == this.ahk.size() - 1) {
                        length = n(a(this.ahg, bArr) ? this.ahg : this.ahh);
                        i2 = 0;
                    } else {
                        length = a2.length - 1;
                        i2 = 0;
                    }
                    stringBuffer.append(a2, i2, (length - i2) + 1);
                }
            }
            return stringBuffer.toString().trim();
        }
        return null;
    }

    private void vN() {
        this.ahK = new bn(4, 1, this.Rp);
        this.ahK.a(ar.avF[35], (byte) 94, (at) null);
        this.ahK.a(ar.avF[10], (byte) 5, (at) null);
        this.ahK.a(ar.avF[72], (byte) 32, (at) null);
        this.ahK.a(ar.avF[73], (byte) 63, (at) null);
    }

    private boolean vR() {
        return bc.b(ahW, ahX, 0, w.ML - this.ahT, w.MK, this.ahT);
    }

    private boolean vS() {
        return bc.b(ahW, ahX, 0, 0, w.MK, this.ahT);
    }

    private final void vt() {
        if (this.ahg == null) {
            this.ahg = vv();
            this.ahi = j(this.ahg);
        } else {
            l(this.ahg);
            l(this.ahi);
        }
        if (this.ahh == null) {
            this.ahh = vv();
            this.ahj = j(this.ahh);
        } else {
            l(this.ahh);
            l(this.ahj);
        }
        if (this.ahk == null) {
            this.ahk = new Vector();
        }
        this.ahk.removeAllElements();
        this.ahl = 0;
        r lA = this.Rp.lA();
        this.ahV = null;
        if (lA != null) {
            this.ahV = lA.jm();
        }
    }

    private void vu() {
        this.ahm = 0;
        i(this.ahg);
        i(this.ahh);
        i(this.ahi);
        i(this.ahj);
        this.ahg = null;
        this.ahh = null;
        this.ahi = null;
        this.ahj = null;
        if (this.ahk != null) {
            this.ahk.removeAllElements();
            this.ahk = null;
        }
        this.ahl = 0;
        this.ahV = null;
    }

    private Object[] vv() {
        return new Object[]{null, new int[]{-1, -1}};
    }

    private void vw() {
        vu();
        this.Rp.lA().a(this.Rp.Kj);
        this.Rp.lA().b(this.Rp.Lt, (bf) null);
        this.ahI = false;
        this.ahJ = false;
        this.ahn = false;
        this.Rp.nn();
    }

    private void vz() {
    }

    public boolean J(ca caVar) {
        return caVar.bGd.b(caVar.bFR, null, ahG, null, new f[]{caVar.bFR}, new int[]{-1}, 1) == null;
    }

    public int[] K(ca caVar) {
        if (caVar == null) {
            return null;
        }
        return a(this.ahg, caVar);
    }

    public int[] L(ca caVar) {
        if (caVar == null) {
            return null;
        }
        return a(this.ahh, caVar);
    }

    public boolean O(byte[] bArr) {
        int size;
        if (!(!vC() || this.ahk == null || (size = this.ahk.size()) == 0)) {
            int i = 0;
            while (i < size) {
                try {
                    Object[] objArr = (Object[]) this.ahk.elementAt(i);
                    if (bArr == (objArr != null ? (byte[]) objArr[0] : null)) {
                        return true;
                    }
                    i++;
                } catch (Exception e) {
                    return false;
                }
            }
        }
        return false;
    }

    public boolean a(ca caVar, Object[] objArr, int[] iArr) {
        vH();
        Object[] vv = vv();
        Object[] vv2 = vv();
        boolean a2 = a(caVar, objArr, iArr, vv, vv2);
        if (a2) {
            vt();
            this.ahg = vv;
            this.ahh = vv2;
            this.ahm = 2;
            this.ahI = true;
            H(caVar);
        }
        return a2;
    }

    public boolean a(ca caVar, Object[] objArr, int[] iArr, Object[] objArr2, Object[] objArr3) {
        return objArr != null ? b(caVar, objArr, objArr2, objArr3) : a(caVar, iArr, objArr2, objArr3);
    }

    public boolean a(r rVar, int i, int i2) {
        if (rVar == null || !vC()) {
            return false;
        }
        boolean z = rVar.BR != null && rVar.BR.biv == 1;
        if (!z && (i2 == 1 || i2 == 2 || i2 == 6 || i2 == 5 || i2 == 50 || i2 == 52 || i2 == 56 || i2 == 54 || i2 == 53 || i2 == 8)) {
            ax(i, i2);
        } else if (z || i2 == -6 || i2 == -7) {
            rVar.d(i2, i, new int[]{0});
        }
        return true;
    }

    public void aA(int i, int i2) {
        this.ahL = false;
    }

    public void aN(boolean z) {
        this.ahL = z;
    }

    public int[] ay(int i, int i2) {
        this.aia = (i - this.ahY) + this.aia;
        this.aib += i2 - this.ahZ;
        return new int[]{this.aia, this.aib};
    }

    public boolean az(int i, int i2) {
        return b(2, i, i2, i, i2);
    }

    public String b(ca caVar, int[] iArr) {
        String str = null;
        vt();
        Object[] a2 = a(caVar, iArr, 1);
        if (!k(a2)) {
            d(this.ahg, a2);
            Object[] a3 = a(caVar, iArr, -1);
            if (!k(a3)) {
                d(this.ahh, a3);
                I(caVar);
                str = vE();
            }
        }
        vu();
        return str;
    }

    public boolean b(float f, float f2) {
        if (!(this.ahV == null || this.ahg == null || this.ahh == null)) {
            int i = this.ahV.bGw;
            int i2 = this.ahV.bGx;
            int[] a2 = a(this.ahg, this.ahV);
            int[] a3 = a(this.ahh, this.ahV);
            int i3 = a2[0] - i;
            int i4 = a2[1] - i2;
            int i5 = a3[0] - i;
            int i6 = a3[1] - i2;
            if ((f >= ((float) i3) || i3 < 0 || f2 > ((float) (a2[2] + i4))) && ((f2 >= ((float) i4) || i4 < 0) && ((f <= ((float) i5) || f2 < ((float) i6)) && f2 <= ((float) (a3[2] + i6))))) {
                return true;
            }
        }
        return false;
    }

    public boolean c(bf bfVar) {
        return false;
    }

    public boolean c(bk bkVar, ca caVar) {
        return false;
    }

    public boolean f(Object[] objArr, Object[] objArr2) {
        return objArr != null && objArr2 != null && p(objArr) == p(objArr2) && o(objArr) == o(objArr2) && n(objArr) == n(objArr2);
    }

    public boolean h(int i, int i2, int i3, int i4) {
        return b(3, i, i2, i3, i4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.an.a(com.uc.c.ca, java.lang.Object[], boolean):int[]
     arg types: [com.uc.c.ca, java.lang.Object[], int]
     candidates:
      com.uc.c.an.a(com.uc.c.ca, java.lang.Object[], int):void
      com.uc.c.an.a(java.lang.Object[], com.uc.c.f, int):void
      com.uc.c.an.a(com.uc.c.ca, int[], int):java.lang.Object[]
      com.uc.c.an.a(com.uc.c.ca, java.lang.Object[], int[]):boolean
      com.uc.c.an.a(com.uc.c.r, int, int):boolean
      com.uc.c.an.a(com.uc.c.ca, java.lang.Object[], boolean):int[] */
    public void m(Vector vector) {
        if (vector != null && vC()) {
            ca jm = this.Rp.lA().jm();
            switch (this.ahm) {
                case 1:
                    if (!k(this.ahi)) {
                        int[] a2 = a(jm, this.ahi, true);
                        a2[0] = a2[0] - 1;
                        a2[1] = a2[1] - 1;
                        a2[2] = a2[2] + 2;
                        a2[3] = a2[3] + 2;
                        vector.addElement(a2);
                        return;
                    }
                    return;
                case 2:
                case 3:
                    if (!k(this.ahj)) {
                        boolean z = 3 == this.ahm;
                        int[] a3 = a(jm, z ? this.ahi : this.ahj, z);
                        int[] a4 = a(jm, z ? this.ahg : this.ahh, z);
                        int[] iArr = new int[4];
                        iArr[0] = 0;
                        iArr[1] = Math.min(a3[1], a4[1]);
                        iArr[2] = jm.bGr;
                        iArr[3] = Math.max(a3[3] + a3[1], a4[3] + a4[1]) - iArr[1];
                        vector.addElement(iArr);
                        vector.addElement(a(jm, !z ? this.ahi : this.ahj, !z));
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public int n(Object[] objArr) {
        if (objArr == null || objArr[1] == null) {
            return -1;
        }
        return ((int[]) objArr[1])[1];
    }

    public int o(Object[] objArr) {
        if (objArr == null || objArr[1] == null) {
            return -1;
        }
        return ((int[]) objArr[1])[0];
    }

    public f p(Object[] objArr) {
        if (objArr == null) {
            return null;
        }
        return (f) objArr[0];
    }

    public int[] v(byte[] bArr, int i) {
        ca jm = this.Rp.lA().jm();
        int n = n(this.ahg);
        int n2 = n(this.ahh);
        Object[] objArr = (Object[]) this.ahk.elementAt(0);
        byte[] bArr2 = objArr != null ? (byte[]) objArr[0] : null;
        Object[] objArr2 = (Object[]) this.ahk.elementAt(this.ahk.size() - 1);
        byte[] bArr3 = objArr2 != null ? (byte[]) objArr2[0] : null;
        if (e(this.ahg, this.ahh)) {
            if (n < n2) {
                return new int[]{n + 1, n2 + 1};
            }
            return new int[]{n2 + 1, n + 1};
        } else if (bArr == bArr2) {
            char[] a2 = f.a(bArr, jm, i);
            int n3 = n(a(this.ahg, bArr) ? this.ahg : this.ahh);
            int length = a2.length;
            if (n3 + 1 > a2.length) {
                return new int[]{length, length};
            }
            return new int[]{n3 + 1, length};
        } else if (bArr == bArr3) {
            f.a(bArr, jm, i);
            return new int[]{0, n(a(this.ahg, bArr) ? this.ahg : this.ahh) + 1};
        } else {
            return new int[]{0, f.a(bArr, jm, i).length};
        }
    }

    public void vB() {
        if (vC()) {
            a(-1, 0, -1, 0, 0, 1);
        }
        this.ahL = false;
        if (this.ahM) {
            synchronized (this) {
                notify();
            }
        }
    }

    public boolean vC() {
        return this.ahm != 0;
    }

    public String vD() {
        if (!vC()) {
            return null;
        }
        return vE();
    }

    public int vF() {
        if (vC()) {
            return this.ahm;
        }
        return 0;
    }

    public int vG() {
        if (vC()) {
            return this.ahl;
        }
        return 0;
    }

    public void vH() {
        this.ahn = true;
    }

    public void vI() {
        this.ahn = false;
    }

    public boolean vJ() {
        return this.ahn;
    }

    public boolean vK() {
        if (vC()) {
            if (this.ahm == 1) {
                if (!k(this.ahg)) {
                    return f(this.ahg, this.ahi);
                }
            } else if (this.ahm == 2 || this.ahm == 3) {
                return f(this.ahg, this.ahi) && f(this.ahh, this.ahj);
            }
        }
        return false;
    }

    public String vL() {
        ca jm = this.Rp.lA().jm();
        return b(jm, new int[]{jm.bGw, jm.bGx, jm.bGr, jm.bGs});
    }

    public String vM() {
        ca jm = this.Rp.lA().jm();
        return b(jm, new int[]{0, 0, jm.bGm, jm.ahU});
    }

    public bn vO() {
        return this.ahK;
    }

    public String vP() {
        String vD = vD();
        vB();
        return vD;
    }

    public boolean vQ() {
        r lA = this.Rp.lA();
        ca jm = lA.jm();
        return !lA.jr() && !jm.ac() && jm.Lj() && !J(jm);
    }

    public void vT() {
        int i;
        int i2;
        ca jm = this.Rp.lA().jm();
        while (this.ahm != 0) {
            if (!this.ahL) {
                try {
                    this.ahM = true;
                    this.ahL = false;
                    synchronized (this) {
                        wait();
                    }
                } catch (Exception e) {
                }
            } else if (vS()) {
                boolean a2 = a(jm, this.ahQ, this.ahO, this.ahP - this.ahS, this.ahO, this.ahP);
                int i3 = this.ahS;
                boolean z = a2;
                int i4 = 1;
                while (true) {
                    if (z) {
                        i2 = i3;
                        break;
                    }
                    int i5 = i4 + 1;
                    int i6 = this.ahS * i5;
                    if (this.ahP - i6 <= 0) {
                        i2 = i6;
                        break;
                    }
                    i3 = i6;
                    z = a(jm, this.ahQ, this.ahO, this.ahP - i6, this.ahO, this.ahP);
                    i4 = i5;
                }
                this.ahP -= i2;
                this.Rp.Hc();
                bc.m((long) aif);
            } else if (vR()) {
                boolean a3 = a(jm, this.ahQ, this.ahO, this.ahS + this.ahP, this.ahO, this.ahP);
                int i7 = this.ahS;
                boolean z2 = a3;
                int i8 = 1;
                while (true) {
                    if (z2) {
                        i = i7;
                        break;
                    }
                    jm.g(0, i7);
                    int i9 = i8 + 1;
                    int i10 = this.ahS * i9;
                    if (this.ahP + i10 >= this.ahU) {
                        i = i10;
                        break;
                    }
                    i7 = i10;
                    z2 = a(jm, this.ahQ, this.ahO, this.ahP + i10, this.ahO, this.ahP);
                    i8 = i9;
                }
                this.ahP = i + this.ahP;
                this.Rp.Hc();
                bc.m((long) aif);
            }
        }
    }

    public boolean vU() {
        return (this.Rp == null || this.Rp.lA() == null || this.ahV != this.Rp.lA().jj()) ? false : true;
    }

    public void vV() {
        this.ahm = 1;
    }

    public boolean vW() {
        return this.ahm == 1;
    }

    public void vX() {
        this.ahm = 0;
    }

    public boolean vx() {
        return this.ahm != 0 && (this.ahI || this.ahJ);
    }

    public int vy() {
        if (this.ahI) {
            return 0;
        }
        return this.ahJ ? 1 : -1;
    }
}
