package com.uc.c;

import a.a.a.c.a.y;
import b.a.a.a.t;
import java.io.InputStream;

public class be extends InputStream {
    private int bgV = -1;
    private int bgW = -1;
    private int bgX = 0;
    private short[][] bgY = null;
    private int bgZ = 0;
    private short[][] bhA = null;
    private byte[] bhB = null;
    private short[] bhC = new short[256];
    private byte bhD = 8;
    private byte bhE = 0;
    private short[] bhF = new short[2];
    private short[][] bhG = null;
    private byte[] bhH = null;
    private short[][] bhI = null;
    private byte[] bhJ = null;
    private short[] bhK = new short[256];
    private byte bhL = 8;
    private byte bhM = 0;
    private long bhN = 0;
    private int bhO = 0;
    private int bhP = 0;
    private int bhQ = 0;
    private int bhR = 0;
    private int bhS = 0;
    private long bhT = 0;
    private byte bhU = 0;
    private int bha = 0;
    private int bhb = 0;
    private byte[] bhc = null;
    private int bhd = 0;
    private int bhe = 0;
    private int bhf = 0;
    private byte[] bhg = null;
    private cf bhh = null;
    private boolean bhi = false;
    private int bhj = 0;
    private int bhk = 0;
    private int bhl = -1;
    private int bhm = 0;
    private InputStream bhn = null;
    private short[] bho = new short[y.YW];
    private short[] bhp = new short[12];
    private short[] bhq = new short[12];
    private short[] bhr = new short[12];
    private short[] bhs = new short[12];
    private short[] bht = new short[y.YW];
    private short[][] bhu = new short[5][];
    private byte[] bhv = new byte[5];
    private short[] bhw = new short[114];
    private short[] bhx = new short[2];
    private short[][] bhy = null;
    private byte[] bhz = null;

    public be(InputStream inputStream) {
        for (int i = 0; i < 4; i++) {
            this.bhv[i] = 6;
            this.bhu[i] = new short[64];
        }
        this.bhv[4] = 4;
        this.bhu[4] = new short[16];
        this.bhn = inputStream;
        this.bhh = new cf();
        byte[] bArr = new byte[5];
        if (this.bhn.read(bArr, 0, 5) != 5) {
            throw new Exception();
        } else if (!au(bArr)) {
            throw new Exception();
        } else {
            for (int i2 = 0; i2 < 8; i2++) {
                int read = this.bhn.read();
                if (read < 0) {
                    throw new Exception("Can't read stream size");
                }
                this.bhN |= ((long) read) << (i2 * 8);
            }
            b(this.bho);
            b(this.bht);
            b(this.bhp);
            b(this.bhq);
            b(this.bhr);
            b(this.bhs);
            b(this.bhw);
            int i3 = 1 << (this.bgZ + this.bha);
            for (int i4 = 0; i4 < i3; i4++) {
                b(this.bgY[i4]);
            }
            for (int i5 = 0; i5 < 4; i5++) {
                b(this.bhu[i5]);
            }
            a(this.bhx, this.bhy, this.bhA, this.bhC, this.bhE);
            a(this.bhF, this.bhG, this.bhI, this.bhK, this.bhM);
            b(this.bhu[4]);
            for (int i6 = 0; i6 < 5; i6++) {
                this.bhm = (this.bhm << 8) | this.bhn.read();
            }
        }
    }

    private final int a(short[] sArr, int i) {
        short s = sArr[i];
        int i2 = (this.bhl >>> 11) * s;
        if ((this.bhm ^ Integer.MIN_VALUE) < (Integer.MIN_VALUE ^ i2)) {
            this.bhl = i2;
            sArr[i] = (short) (s + ((2048 - s) >>> 5));
            if ((this.bhl & -16777216) == 0) {
                this.bhm = (this.bhm << 8) | this.bhn.read();
                this.bhl <<= 8;
            }
            return 0;
        }
        this.bhl -= i2;
        this.bhm -= i2;
        sArr[i] = (short) (s - (s >>> 5));
        if ((this.bhl & -16777216) == 0) {
            this.bhm = (this.bhm << 8) | this.bhn.read();
            this.bhl <<= 8;
        }
        return 1;
    }

    private final int a(short[] sArr, short[] sArr2, short[] sArr3, short[] sArr4, int i, int i2) {
        return a(sArr, 0) == 0 ? b(sArr2, i) : a(sArr, 1) == 0 ? 8 + b(sArr3, i) : 8 + b(sArr4, i2) + 8;
    }

    private final void a(short[] sArr, short[][] sArr2, short[][] sArr3, short[] sArr4, int i) {
        b(sArr);
        for (int i2 = 0; i2 < i; i2++) {
            b(sArr2[i2]);
            b(sArr3[i2]);
        }
        b(sArr4);
    }

    private static final void a(short[][] sArr, byte[] bArr, short[][] sArr2, byte[] bArr2, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            sArr[i2] = new short[8];
            bArr[i2] = 3;
            sArr2[i2] = new short[8];
            bArr2[i2] = 3;
        }
    }

    private int b(short[] sArr, int i) {
        int i2 = 1;
        for (int i3 = i; i3 != 0; i3--) {
            i2 = a(sArr, i2) + (i2 << 1);
        }
        return i2 - (1 << i);
    }

    private static final void b(short[] sArr) {
        for (int i = 0; i < sArr.length; i++) {
            sArr[i] = 1024;
        }
    }

    private final void flush() {
        int i = this.bhd - this.bhf;
        if (i != 0) {
            this.bhh.write(this.bhc, this.bhf, i);
            this.bhi = true;
            if (this.bhd >= this.bhe) {
                this.bhd = 0;
            }
            this.bhf = this.bhd;
        }
    }

    private static final int gz(int i) {
        int i2 = i - 2;
        if (i2 < 4) {
            return i2;
        }
        return 3;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v9, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v10, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v16, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v76, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v83, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v84, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean CK() {
        /*
            r12 = this;
            r5 = 256(0x100, float:3.59E-43)
            r7 = 7
            r8 = 4
            r10 = 0
            r9 = 1
            long r0 = r12.bhT
            long r2 = r12.bhN
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 < 0) goto L_0x0010
            r0 = r10
        L_0x000f:
            return r0
        L_0x0010:
            long r0 = r12.bhT
            int r0 = (int) r0
            int r1 = r12.bgX
            r0 = r0 & r1
            short[] r1 = r12.bho
            int r2 = r12.bhO
            int r2 = r2 << 4
            int r2 = r2 + r0
            int r1 = r12.a(r1, r2)
            if (r1 != 0) goto L_0x00c3
            long r0 = r12.bhT
            int r0 = (int) r0
            short[][] r1 = r12.bgY
            int r2 = r12.bhb
            r0 = r0 & r2
            int r2 = r12.bgZ
            int r0 = r0 << r2
            byte r2 = r12.bhU
            r2 = r2 & 255(0xff, float:3.57E-43)
            r3 = 8
            int r4 = r12.bgZ
            int r3 = r3 - r4
            int r2 = r2 >>> r3
            int r0 = r0 + r2
            r0 = r1[r0]
            int r1 = r12.bhO
            if (r1 < r7) goto L_0x00a7
            int r1 = r12.bhd
            int r2 = r12.bhP
            int r1 = r1 - r2
            int r1 = r1 - r9
            if (r1 >= 0) goto L_0x004a
            int r2 = r12.bhe
            int r1 = r1 + r2
        L_0x004a:
            byte[] r2 = r12.bhc
            byte r1 = r2[r1]
            r2 = r9
        L_0x004f:
            int r3 = r1 >> 7
            r3 = r3 & 1
            int r1 = r1 << 1
            int r4 = r3 + 1
            int r4 = r4 << 8
            int r4 = r4 + r2
            int r4 = r12.a(r0, r4)
            int r2 = r2 << 1
            r2 = r2 | r4
            if (r3 == r4) goto L_0x006e
            r1 = r2
        L_0x0064:
            if (r1 >= r5) goto L_0x00b1
            int r2 = r1 << 1
            int r1 = r12.a(r0, r1)
            r1 = r1 | r2
            goto L_0x0064
        L_0x006e:
            if (r2 < r5) goto L_0x004f
            r0 = r2
        L_0x0071:
            byte r0 = (byte) r0
            r12.bhU = r0
            byte[] r0 = r12.bhc
            int r1 = r12.bhd
            int r2 = r1 + 1
            r12.bhd = r2
            byte r2 = r12.bhU
            r0[r1] = r2
            int r0 = r12.bhd
            int r1 = r12.bhe
            if (r0 < r1) goto L_0x0089
            r12.flush()
        L_0x0089:
            int r0 = r12.bhO
            if (r0 >= r8) goto L_0x00b3
            r0 = r10
        L_0x008e:
            r12.bhO = r0
            long r0 = r12.bhT
            r2 = 1
            long r0 = r0 + r2
            r12.bhT = r0
            int r0 = r10 + 1
        L_0x0099:
            long r0 = r12.bhT
            long r2 = r12.bhN
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 < 0) goto L_0x00a4
            r12.flush()
        L_0x00a4:
            r0 = r9
            goto L_0x000f
        L_0x00a7:
            r1 = r9
        L_0x00a8:
            int r2 = r1 << 1
            int r1 = r12.a(r0, r1)
            r1 = r1 | r2
            if (r1 < r5) goto L_0x00a8
        L_0x00b1:
            r0 = r1
            goto L_0x0071
        L_0x00b3:
            int r0 = r12.bhO
            r1 = 10
            if (r0 >= r1) goto L_0x00be
            int r0 = r12.bhO
            r1 = 3
            int r0 = r0 - r1
            goto L_0x008e
        L_0x00be:
            int r0 = r12.bhO
            r1 = 6
            int r0 = r0 - r1
            goto L_0x008e
        L_0x00c3:
            short[] r1 = r12.bhp
            int r2 = r12.bhO
            int r1 = r12.a(r1, r2)
            if (r1 != r9) goto L_0x0154
            short[] r1 = r12.bhq
            int r2 = r12.bhO
            int r1 = r12.a(r1, r2)
            if (r1 != 0) goto L_0x0125
            short[] r1 = r12.bht
            int r2 = r12.bhO
            int r2 = r2 << 4
            int r2 = r2 + r0
            int r1 = r12.a(r1, r2)
            if (r1 != 0) goto L_0x0137
            int r1 = r12.bhO
            if (r1 >= r7) goto L_0x0122
            r1 = 9
        L_0x00ea:
            r12.bhO = r1
            r1 = r9
        L_0x00ed:
            if (r1 != 0) goto L_0x0271
            short[] r1 = r12.bhF
            short[][] r2 = r12.bhG
            r2 = r2[r0]
            short[][] r3 = r12.bhI
            r3 = r3[r0]
            short[] r4 = r12.bhK
            byte[] r5 = r12.bhJ
            byte r5 = r5[r0]
            byte r6 = r12.bhL
            r0 = r12
            int r0 = r0.a(r1, r2, r3, r4, r5, r6)
            int r0 = r0 + 2
            int r1 = r12.bhO
            if (r1 >= r7) goto L_0x0151
            r1 = 8
        L_0x010e:
            r12.bhO = r1
        L_0x0110:
            int r1 = r12.bhP
            long r1 = (long) r1
            long r3 = r12.bhT
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 >= 0) goto L_0x011f
            int r1 = r12.bhP
            int r2 = r12.bgW
            if (r1 < r2) goto L_0x0221
        L_0x011f:
            r0 = r10
            goto L_0x000f
        L_0x0122:
            r1 = 11
            goto L_0x00ea
        L_0x0125:
            short[] r1 = r12.bhr
            int r2 = r12.bhO
            int r1 = r12.a(r1, r2)
            if (r1 != 0) goto L_0x0139
            int r1 = r12.bhQ
        L_0x0131:
            int r2 = r12.bhP
            r12.bhQ = r2
            r12.bhP = r1
        L_0x0137:
            r1 = r10
            goto L_0x00ed
        L_0x0139:
            short[] r1 = r12.bhs
            int r2 = r12.bhO
            int r1 = r12.a(r1, r2)
            if (r1 != 0) goto L_0x014a
            int r1 = r12.bhR
        L_0x0145:
            int r2 = r12.bhQ
            r12.bhR = r2
            goto L_0x0131
        L_0x014a:
            int r1 = r12.bhS
            int r2 = r12.bhR
            r12.bhS = r2
            goto L_0x0145
        L_0x0151:
            r1 = 11
            goto L_0x010e
        L_0x0154:
            int r1 = r12.bhR
            r12.bhS = r1
            int r1 = r12.bhQ
            r12.bhR = r1
            int r1 = r12.bhP
            r12.bhQ = r1
            short[] r1 = r12.bhx
            short[][] r2 = r12.bhy
            r2 = r2[r0]
            short[][] r3 = r12.bhA
            r3 = r3[r0]
            short[] r4 = r12.bhC
            byte[] r5 = r12.bhB
            byte r5 = r5[r0]
            byte r6 = r12.bhD
            r0 = r12
            int r0 = r0.a(r1, r2, r3, r4, r5, r6)
            int r0 = r0 + 2
            int r1 = r12.bhO
            if (r1 >= r7) goto L_0x01e4
            r1 = r7
        L_0x017e:
            r12.bhO = r1
            int r1 = gz(r0)
            byte r1 = (byte) r1
            short[][] r2 = r12.bhu
            r2 = r2[r1]
            byte[] r3 = r12.bhv
            byte r1 = r3[r1]
            int r1 = r12.b(r2, r1)
            if (r1 < r8) goto L_0x021d
            int r2 = r1 >> 1
            int r2 = r2 - r9
            r3 = r1 & 1
            r3 = r3 | 2
            int r3 = r3 << r2
            r12.bhP = r3
            int r3 = r12.bhP
            int r3 = r3 - r1
            int r3 = r3 - r9
            short[] r4 = r12.bhw
            r5 = 14
            if (r1 < r5) goto L_0x026c
            int r2 = r2 - r8
            r3 = r10
        L_0x01a9:
            if (r2 == 0) goto L_0x01e7
            int r4 = r12.bhl
            int r4 = r4 >>> 1
            r12.bhl = r4
            int r4 = r12.bhm
            int r5 = r12.bhl
            int r4 = r4 - r5
            int r4 = r4 >>> 31
            int r5 = r12.bhm
            int r6 = r12.bhl
            int r7 = r4 - r9
            r6 = r6 & r7
            int r5 = r5 - r6
            r12.bhm = r5
            int r3 = r3 << 1
            int r4 = r9 - r4
            r3 = r3 | r4
            int r4 = r12.bhl
            r5 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r4 = r4 & r5
            if (r4 != 0) goto L_0x01e1
            int r4 = r12.bhm
            int r4 = r4 << 8
            java.io.InputStream r5 = r12.bhn
            int r5 = r5.read()
            r4 = r4 | r5
            r12.bhm = r4
            int r4 = r12.bhl
            int r4 = r4 << 8
            r12.bhl = r4
        L_0x01e1:
            int r2 = r2 + -1
            goto L_0x01a9
        L_0x01e4:
            r1 = 10
            goto L_0x017e
        L_0x01e7:
            int r2 = r12.bhP
            int r3 = r3 << 4
            int r2 = r2 + r3
            r12.bhP = r2
            byte[] r2 = r12.bhv
            byte r2 = r2[r8]
            short[][] r3 = r12.bhu
            r3 = r3[r8]
            r4 = r10
            r11 = r2
            r2 = r3
            r3 = r11
        L_0x01fa:
            r5 = r10
            r6 = r10
            r7 = r9
        L_0x01fd:
            if (r5 >= r3) goto L_0x020d
            int r8 = r4 + r7
            int r8 = r12.a(r2, r8)
            int r7 = r7 << 1
            int r7 = r7 + r8
            int r8 = r8 << r5
            r6 = r6 | r8
            int r5 = r5 + 1
            goto L_0x01fd
        L_0x020d:
            int r2 = r12.bhP
            int r2 = r2 + r6
            r12.bhP = r2
            int r2 = r12.bhP
            if (r2 >= 0) goto L_0x0110
            r2 = 14
            if (r1 < r2) goto L_0x0110
            r0 = r10
            goto L_0x000f
        L_0x021d:
            r12.bhP = r1
            goto L_0x0110
        L_0x0221:
            int r1 = r12.bhd
            int r2 = r12.bhP
            int r1 = r1 - r2
            int r1 = r1 - r9
            if (r1 >= 0) goto L_0x0269
            int r2 = r12.bhe
            int r1 = r1 + r2
            r2 = r1
            r1 = r0
        L_0x022e:
            if (r1 == 0) goto L_0x0252
            int r3 = r12.bhe
            if (r2 < r3) goto L_0x0235
            r2 = r10
        L_0x0235:
            byte[] r3 = r12.bhc
            int r4 = r12.bhd
            int r5 = r4 + 1
            r12.bhd = r5
            byte[] r5 = r12.bhc
            int r6 = r2 + 1
            byte r2 = r5[r2]
            r3[r4] = r2
            int r2 = r12.bhd
            int r3 = r12.bhe
            if (r2 < r3) goto L_0x024e
            r12.flush()
        L_0x024e:
            int r1 = r1 + -1
            r2 = r6
            goto L_0x022e
        L_0x0252:
            long r1 = r12.bhT
            long r3 = (long) r0
            long r1 = r1 + r3
            r12.bhT = r1
            int r0 = r0 + r10
            int r0 = r12.bhd
            int r0 = r0 - r9
            if (r0 >= 0) goto L_0x0261
            int r1 = r12.bhe
            int r0 = r0 + r1
        L_0x0261:
            byte[] r1 = r12.bhc
            byte r0 = r1[r0]
            r12.bhU = r0
            goto L_0x0099
        L_0x0269:
            r2 = r1
            r1 = r0
            goto L_0x022e
        L_0x026c:
            r11 = r4
            r4 = r3
            r3 = r2
            r2 = r11
            goto L_0x01fa
        L_0x0271:
            r0 = r1
            goto L_0x0110
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.be.CK():boolean");
    }

    public boolean au(byte[] bArr) {
        if (bArr.length < 5) {
            return false;
        }
        byte b2 = bArr[0] & 255;
        int i = b2 % 9;
        int i2 = b2 / 9;
        int i3 = i2 % 5;
        int i4 = i2 / 5;
        int i5 = 0;
        for (int i6 = 0; i6 < 4; i6++) {
            i5 += (bArr[i6 + 1] & 255) << (i6 * 8);
        }
        if (i > 4 || i3 > 4 || i4 > 4) {
            return false;
        }
        if (this.bgY != null && this.bgZ == i && this.bha == i3) {
            return false;
        }
        this.bha = i3;
        this.bhb = (1 << i3) - 1;
        this.bgZ = i;
        int i7 = 1 << (this.bgZ + this.bha);
        this.bgY = new short[i7][];
        for (int i8 = 0; i8 < i7; i8++) {
            this.bgY[i8] = new short[768];
        }
        int i9 = 1 << i4;
        this.bhE = (byte) i9;
        this.bhy = new short[this.bhE][];
        this.bhz = new byte[this.bhE];
        this.bhA = new short[this.bhE][];
        this.bhB = new byte[this.bhE];
        a(this.bhy, this.bhz, this.bhA, this.bhB, this.bhE);
        this.bhM = (byte) i9;
        this.bhG = new short[this.bhM][];
        this.bhH = new byte[this.bhM];
        this.bhI = new short[this.bhM][];
        this.bhJ = new byte[this.bhM];
        a(this.bhG, this.bhH, this.bhI, this.bhJ, this.bhM);
        this.bgX = i9 - 1;
        if (i5 < 0) {
            return false;
        }
        if (this.bgV != i5) {
            this.bgV = i5;
            this.bgW = Math.max(this.bgV, 1);
            int max = Math.max(this.bgW, (int) t.bib);
            if (this.bhc == null || this.bhe != max) {
                this.bhc = new byte[max];
            }
            this.bhe = max;
            this.bhd = 0;
            this.bhf = 0;
        }
        return true;
    }

    public void close() {
        if (this.bhn != null) {
            this.bhn.close();
        }
        kc();
    }

    public void kc() {
        try {
            if (this.bhh != null) {
                this.bhh.close();
                this.bhh = null;
            }
            for (int i = 0; i < this.bgY.length; i++) {
                this.bgY[i] = null;
            }
            this.bhc = null;
            this.bho = null;
            this.bhp = null;
            this.bhq = null;
            this.bhr = null;
            this.bhs = null;
            this.bht = null;
            for (int i2 = 0; i2 < this.bhu.length; i2++) {
                this.bhu[i2] = null;
            }
            this.bhv = null;
            this.bhw = null;
            this.bhx = null;
            this.bhy = null;
            this.bhz = null;
            this.bhA = null;
            this.bhB = null;
            this.bhC = null;
            this.bhF = null;
            this.bhG = null;
            this.bhH = null;
            this.bhI = null;
            this.bhJ = null;
            this.bhK = null;
        } catch (Exception e) {
        }
    }

    public int read() {
        if (this.bhT >= this.bhN) {
            kc();
            return -1;
        }
        if (this.bhd == this.bhf) {
            CK();
        }
        if (this.bhi) {
            if (this.bhg == null) {
                this.bhg = this.bhh.Oe();
                this.bhk = this.bhh.size();
                this.bhh.reset();
            }
            if (this.bhj < this.bhk) {
                byte[] bArr = this.bhg;
                int i = this.bhj;
                this.bhj = i + 1;
                return bArr[i] & 255;
            }
            this.bhg = null;
            this.bhi = false;
            this.bhj = 0;
            this.bhk = 0;
        }
        byte[] bArr2 = this.bhc;
        int i2 = this.bhf;
        this.bhf = i2 + 1;
        return bArr2[i2] & 255;
    }
}
