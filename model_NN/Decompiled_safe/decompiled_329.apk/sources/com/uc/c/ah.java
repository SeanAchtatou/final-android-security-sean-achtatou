package com.uc.c;

import java.util.Iterator;
import java.util.Vector;

public class ah {
    public String aaa = null;
    public Vector aab = new Vector();
    public String ds = null;

    public void a(String[] strArr, String[] strArr2) {
        if (strArr != null && strArr2 != null && strArr.length == strArr2.length) {
            if (this.aab == null) {
                this.aab = new Vector();
            } else {
                this.aab.clear();
            }
            int length = strArr.length;
            for (int i = 0; i < length; i++) {
                if (!(strArr[i] == null || strArr2[i] == null)) {
                    this.aab.add(new h(strArr[i], strArr2[i]));
                }
            }
        }
    }

    public String getValue(String str) {
        if (this.aab == null) {
            return null;
        }
        Iterator it = this.aab.iterator();
        while (it.hasNext()) {
            h hVar = (h) it.next();
            if (hVar.ds.equals(str)) {
                return hVar.value;
            }
        }
        return null;
    }

    public void un() {
        int i;
        if (this.aab != null && this.aaa != null && this.aaa.length() != 0) {
            int size = this.aab.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    i = -1;
                    break;
                } else if (((h) this.aab.elementAt(i2)).ds.equals(this.aaa)) {
                    i = i2;
                    break;
                } else {
                    i2++;
                }
            }
            if (i >= 0) {
                this.aab.remove(i);
            }
        }
    }

    public boolean uo() {
        return (this.aab == null || this.aaa == null || this.aaa.length() == 0) ? false : true;
    }

    public boolean up() {
        if (this.aab == null || this.aaa == null || this.aaa.length() == 0) {
            return false;
        }
        String value = getValue(this.aaa);
        return (value == null || value.length() == 0) ? false : true;
    }
}
