package com.uc.c;

import b.a.a.c.a.b;
import b.a.a.c.a.c;
import b.a.a.c.g;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Hashtable;

public class ae extends q {
    private c Xy;

    public ae() {
    }

    public ae(String str, int i) {
        if (fS()) {
            b(str, i);
        }
    }

    public static Hashtable tU() {
        return null;
    }

    public void O(String str) {
        this.Xy.O(str);
    }

    public void b(String str, int i) {
        this.Xy = (c) g.z(str, i);
    }

    public String[] b(String str, boolean z) {
        return a(this.Xy.f(str, z));
    }

    public boolean canRead() {
        return this.Xy.canRead();
    }

    public boolean canWrite() {
        return this.Xy.canWrite();
    }

    public void cf() {
        this.Xy.cf();
    }

    public void close() {
        this.Xy.close();
    }

    public void create() {
        this.Xy.create();
    }

    public OutputStream cw() {
        return this.Xy.cw();
    }

    public boolean exists() {
        return this.Xy.exists();
    }

    public InputStream ez() {
        return this.Xy.ez();
    }

    public boolean fS() {
        return this.Xy == null;
    }

    public long fT() {
        return this.Xy.fT();
    }

    public void fU() {
        this.Xy.fU();
    }

    public String[] fV() {
        return a(b.fR());
    }

    public long fW() {
        return this.Xy.fW();
    }

    public OutputStream i(long j) {
        return this.Xy.i(j);
    }

    public boolean isDirectory() {
        return this.Xy.isDirectory();
    }

    public boolean isHidden() {
        return this.Xy.isHidden();
    }

    public long lastModified() {
        return this.Xy.lastModified();
    }

    public String[] list() {
        return a(this.Xy.ui());
    }
}
