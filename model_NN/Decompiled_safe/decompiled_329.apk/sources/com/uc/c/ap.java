package com.uc.c;

import android.graphics.Picture;
import b.a.a.a.k;
import b.a.a.a.u;
import java.util.Vector;

class ap {
    static final long avi = 100;
    final /* synthetic */ r Je;
    Vector avf = null;
    ca avg = null;
    bj avh = null;

    ap(r rVar, Vector vector, ca caVar, bj bjVar) {
        this.Je = rVar;
        this.avf = vector;
        this.avg = caVar;
        this.avh = bjVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int, int, int, int[], boolean, boolean, boolean):void
     arg types: [com.uc.c.bk, com.uc.c.ca, int, int, int, int, ?[OBJECT, ARRAY], int, int, int]
     candidates:
      com.uc.c.r.a(com.uc.c.bk, int, int, int, int, int, int, int, com.uc.c.w, int):int
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int, int, int, int[], boolean, boolean, boolean):void */
    public void eX(int i) {
        if (this.avf != null && this.avg != null && this.avh != null) {
            int size = this.avf.size();
            long currentTimeMillis = System.currentTimeMillis();
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < size) {
                    int[] iArr = (int[]) this.avf.get(i3);
                    Picture picture = new Picture();
                    bk bkVar = new bk(new u(k.uD(), picture.beginRecording(this.avg.bGm, this.avg.ahU)));
                    new Vector().add(iArr);
                    Vector KP = this.avg.KP();
                    this.avg.t(KP);
                    this.Je.a(bkVar, this.avg, iArr[0], iArr[1], iArr[2], iArr[3], (int[]) null, true, true, false);
                    this.avg.u(KP);
                    picture.endRecording();
                    this.avh.a(iArr[0], iArr[1], picture);
                    if (System.currentTimeMillis() - currentTimeMillis > ((long) i)) {
                        this.Je.p.qW();
                        return;
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
