package com.uc.c;

import b.a.a.a.a.a;
import b.a.a.a.f;
import b.a.a.a.g;
import b.a.a.a.u;
import com.uc.browser.ActivitySelector;
import java.util.Timer;

public class cg extends g {
    private static bk cbI = null;
    static f cbJ = null;
    public static boolean cbK = false;
    public static int cbR = 0;
    public static Timer cbS = null;
    protected boolean cbL = true;
    int cbM = PC();
    protected int cbN = 0;
    protected int cbO = 0;
    public int cbP = 0;
    private byte cbQ = 1;
    public byte cbT = 0;

    private int D(String str, int i) {
        if (bc.by(str)) {
            return -99999;
        }
        String lowerCase = str.toLowerCase();
        if (lowerCase.equals("sk2(left)")) {
            return -6;
        }
        if (lowerCase.equals("sk1(right)")) {
            return -7;
        }
        if (lowerCase.indexOf("soft") != -1) {
            if (lowerCase.charAt(lowerCase.length() - 1) == '1' || lowerCase.startsWith(as.azv)) {
                return i != -7 ? -6 : -7;
            }
            if (lowerCase.charAt(lowerCase.length() - 1) == '2' || lowerCase.startsWith(as.azw) || lowerCase.charAt(lowerCase.length() - 1) == '4') {
                return i != -6 ? -7 : -6;
            }
        }
        if (lowerCase.equals("clear")) {
            return -8;
        }
        if (lowerCase.equals(ActivitySelector.aSI) || lowerCase.equals("ok") || lowerCase.equals("send") || lowerCase.equals("fire") || lowerCase.equals("navi-center") || lowerCase.equals("start") || lowerCase.equals("enter")) {
            return 8;
        }
        if (lowerCase.equals("up") || lowerCase.equals("navi-up") || lowerCase.equals("up arrow") || lowerCase.equals("↑")) {
            return 1;
        }
        if (lowerCase.equals("down") || lowerCase.equals("navi-down") || lowerCase.equals("down arrow") || lowerCase.equals("↓")) {
            return 6;
        }
        if (lowerCase.equals(as.azv) || lowerCase.equals("navi-left") || lowerCase.equals("left arrow") || lowerCase.equals("sideup") || lowerCase.equals("←")) {
            return 2;
        }
        return (lowerCase.equals(as.azw) || lowerCase.equals("navi-right") || lowerCase.equals("right arrow") || lowerCase.equals("sidedown") || lowerCase.equals("→")) ? 5 : -99999;
    }

    private int PC() {
        int i = az.bcm == 0 ? 5 : 6;
        this.cbM = i;
        return i;
    }

    private void PG() {
        if (cbI != null) {
            cbI.b(null);
        }
    }

    private int[] bD(int i, int i2) {
        int[] iArr = new int[2];
        if (!cbK) {
            iArr[0] = i;
            iArr[1] = i2;
        } else if (this.cbM == 5) {
            iArr[0] = i2;
            iArr[1] = super.getWidth() - i;
        } else {
            iArr[0] = super.getHeight() - i2;
            iArr[1] = i;
        }
        return iArr;
    }

    private int kw(int i) {
        int D;
        if (!kx(i)) {
            String str = null;
            try {
                str = br(i);
            } catch (Exception e) {
            }
            if (str != null && (D = D(str, i)) != -99999) {
                return D;
            }
            int kz = kz(i);
            if (kz != -99999) {
                return kz;
            }
            int ky = ky(i);
            if (ky != 0) {
                return ky;
            }
        }
        return i;
    }

    public static boolean kx(int i) {
        return (i >= 48 && i <= 57) || i == 42 || i == 35;
    }

    private int ky(int i) {
        if ((i >= 97 && i <= 122) || ((i >= 65 && i <= 90) || i == -8 || i == -11 || i == 33 || i == 63 || (i >= 43 && i <= 45))) {
            return i;
        }
        try {
            return bs(i);
        } catch (Exception e) {
            return 0;
        }
    }

    private int kz(int i) {
        if ((i >= 97 && i <= 122) || (i >= 65 && i <= 90)) {
            return i;
        }
        if (i == -11 && bc.beZ == 0) {
            return 0;
        }
        if (i == -11 && bc.beZ == 3) {
            return -11;
        }
        if (i == -22 && bc.beZ == 2) {
            return 0;
        }
        if (i == -6 || i == -21 || i == 21 || i == -202 || i == 57345 || i == 113 || i == 65 || i == 66) {
            return -6;
        }
        if (i == -7 || ((i == -22 && bc.beZ != 3) || i == 22 || i == -203 || i == 57346 || i == 68 || i == 67 || i == 112 || i == 106)) {
            return -7;
        }
        if (i == -5 || i == -10 || ((i == -20 && bc.beZ != 3) || i == 20 || i == 23 || i == -14 || i == -26 || i == -200 || i == 13)) {
            return 8;
        }
        if (i == -8 || i == 8) {
            return -8;
        }
        return (i == -11 || i == -16 || i == -19 || i == -204) ? -10000 : -99999;
    }

    /* access modifiers changed from: protected */
    public void B(int i, int i2) {
    }

    /* access modifiers changed from: protected */
    public void D(int i, int i2) {
    }

    /* access modifiers changed from: protected */
    public void E(int i, int i2) {
    }

    public void Hc() {
        this.cbP |= 30;
        cJ();
    }

    /* access modifiers changed from: protected */
    public void L(int i, int i2) {
        int[] bD = bD(i, i2);
        D(bD[0], bD[1]);
    }

    /* access modifiers changed from: protected */
    public void M(int i, int i2) {
        int[] bD = bD(i, i2);
        E(bD[0], bD[1]);
    }

    /* access modifiers changed from: protected */
    public void N(int i, int i2) {
        int[] bD = bD(i, i2);
        B(bD[0], bD[1]);
    }

    public void PD() {
        if (cbS != null) {
            cbS.cancel();
            cbS = null;
        }
    }

    public void PE() {
        ar.mR.bL(kw(cbR));
    }

    public final void PF() {
        cbJ = null;
    }

    public void PH() {
        if (this.cbP != 0) {
            PI();
        }
    }

    public void PI() {
        cJ();
    }

    /* access modifiers changed from: protected */
    public void a(u uVar) {
        if (cbK) {
            PC();
            if (!(cbJ == null || (this.cbN == super.getHeight() && this.cbO == super.getWidth()))) {
                cbJ = null;
            }
            if (cbJ == null) {
                try {
                    int height = super.getHeight();
                    this.cbN = height;
                    int width = super.getWidth();
                    this.cbO = width;
                    if (width < 0) {
                        width = uVar.DE();
                    }
                    if (height < 0) {
                        height = uVar.DD();
                    }
                    cbJ = f.J(height, width);
                } catch (Exception e) {
                }
            }
            try {
                u jU = cbJ.jU();
                c(jU);
                int width2 = cbJ.getWidth();
                int height2 = cbJ.getHeight();
                jU.l(0, 0, width2, height2);
                try {
                    f(cbI);
                } catch (Exception e2) {
                }
                uVar.l(0, 0, height2, width2);
                a aVar = new a(cbJ);
                aVar.dU(this.cbM);
                aVar.ao(0, 0);
                aVar.setPosition(0, 0);
                aVar.a(uVar);
            } catch (Throwable th) {
                cbK = false;
                this.cbL = true;
                cbJ = null;
            }
        } else {
            c(uVar);
            try {
                f(cbI);
            } catch (Throwable th2) {
            }
        }
        PG();
    }

    /* access modifiers changed from: protected */
    public void bK(int i) {
    }

    /* access modifiers changed from: protected */
    public void bL(int i) {
    }

    /* access modifiers changed from: protected */
    public void bd(int i) {
    }

    /* access modifiers changed from: protected */
    public void bt(int i) {
        if ((bc.beZ == -1 || bc.beZ == 4 || !kt()) && this.cbQ == 1 && this.cbT != 0) {
            this.cbQ = 2;
            PD();
            this.cbT = 0;
        }
        try {
            this.cbT = 1;
            bd(kw(i));
        } catch (Exception e) {
        }
        if ((bc.beZ == -1 || bc.beZ == 4 || !kt()) && this.cbQ == 1) {
            cbR = i;
            PD();
            cbS = new Timer();
            cbS.schedule(new ci(1, this), 800);
        }
    }

    /* access modifiers changed from: protected */
    public void bu(int i) {
        try {
            this.cbQ = 3;
            this.cbT = 2;
            PD();
            bL(kw(i));
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void bv(int i) {
        try {
            if ((bc.beZ == -1 || bc.beZ == 4 || !kt()) && this.cbQ == 2) {
                this.cbQ = 1;
            }
            this.cbT = 0;
            PD();
            bK(kw(i));
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void c(u uVar) {
        if (cbI == null) {
            cbI = new bk(uVar);
        } else {
            cbI.b(uVar);
        }
    }

    /* access modifiers changed from: protected */
    public void f(bk bkVar) {
    }

    public int getHeight() {
        return cbK ? cbJ == null ? super.getWidth() : cbJ.getHeight() : this.cbN > 0 ? this.cbN : super.getHeight();
    }

    public int getWidth() {
        return cbK ? cbJ == null ? super.getHeight() : cbJ.getWidth() : this.cbO > 0 ? this.cbO : super.getWidth();
    }

    public void y(int i, int i2, int i3, int i4) {
        if (!cbK) {
            c(i, i2, i3, i4);
        } else if (cbJ != null) {
            c((cbJ.getHeight() - i2) - i4, i, i4, i3);
        } else {
            c((getHeight() - i2) - i4, i, i4, i3);
        }
    }
}
