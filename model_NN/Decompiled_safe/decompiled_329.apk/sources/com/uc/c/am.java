package com.uc.c;

import java.util.Hashtable;

public class am {
    public static final int afS = 50;
    public static final int afT = 100;
    public static final String afU = "";
    public static final String afV = "https://";
    private static am afW = null;
    private Hashtable afX = new Hashtable();
    private Hashtable afY = new Hashtable();

    private am() {
    }

    public static am uZ() {
        if (afW == null) {
            afW = new am();
        }
        return afW;
    }

    public ag O(String str, String str2) {
        if (str == null || str2 == null) {
            return null;
        }
        String[] j = ce.j(str);
        return new ag(this, j[2], j[0], str2);
    }

    public void a(ag agVar, String str) {
        int i;
        Hashtable hashtable;
        if (agVar != null) {
            String a2 = agVar.protocol;
            String b2 = agVar.ZQ;
            String c = agVar.ds;
            if (a2 != null && b2 != null && c != null) {
                if (a2.equalsIgnoreCase("https://")) {
                    i = 100;
                    hashtable = this.afY;
                } else {
                    i = 50;
                    hashtable = this.afX;
                }
                ba baVar = (ba) hashtable.get(b2);
                if (baVar == null) {
                    baVar = new ba(this, i);
                    hashtable.put(b2, baVar);
                }
                baVar.set(c, str);
            }
        }
    }

    public ag d(String str, String str2, String str3) {
        return new ag(this, str, str2, str3);
    }

    public String d(ag agVar) {
        String a2 = agVar.protocol;
        String b2 = agVar.ZQ;
        String c = agVar.ds;
        if (a2 == null || b2 == null || c == null) {
            return "";
        }
        ba baVar = (ba) (a2.equalsIgnoreCase("https://") ? this.afY : this.afX).get(b2);
        return baVar == null ? "" : baVar.get(c);
    }

    public void uY() {
        this.afX = new Hashtable();
        this.afY = new Hashtable();
    }
}
