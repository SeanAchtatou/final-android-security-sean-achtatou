package com.uc.c;

import b.a.a.a.f;
import b.b;
import com.uc.a.n;
import com.uc.b.e;
import com.uc.plugin.Plugin;
import com.uc.plugin.ag;
import java.util.Hashtable;
import java.util.Vector;

public class i {
    ca dt = null;
    private n p = null;

    public i(ca caVar) {
        this.dt = caVar;
    }

    public static int a(int i, String str) {
        int i2 = (az.bcr || i != 1) ? i : 0;
        if (i2 == 0 && bx.y(str, 1) != null) {
            i2 = 1;
        }
        if (i == 3) {
            return 1;
        }
        return i2;
    }

    private final String a(String str, int i, int i2, Hashtable hashtable, int[] iArr) {
        int i3;
        String g;
        int length = str.length();
        int i4 = i2 + 1;
        int i5 = i4;
        int i6 = i4;
        while (true) {
            if (i4 >= length) {
                i3 = length;
                break;
            }
            char charAt = str.charAt(i4);
            if (charAt == ')') {
                i3 = i5 + 1;
                break;
            }
            if (charAt == '(') {
                i6++;
            } else if (charAt == '$') {
                if (i6 == i5) {
                    iArr[0] = iArr[0] + 1;
                    i3 = i5;
                } else {
                    i3 = i5;
                }
            } else if (!bc.isLetterOrDigit(charAt)) {
                i3 = i5;
                break;
            }
            i5++;
            i4++;
        }
        String substring = str.substring(i6, i5);
        if (i == 1) {
            int indexOf = substring.indexOf(58);
            if (indexOf != -1) {
                substring = substring.substring(0, indexOf);
            }
            g = hashtable.get(substring) != null ? (String) hashtable.get(substring) : "";
        } else {
            g = bc.g("$(", substring, ")");
        }
        iArr[0] = iArr[0] + g.length() + i2;
        return (i2 == 0 && i3 == length) ? g : bc.g(str.substring(0, i2), g, str.substring(i3)).toString();
    }

    public static String a(String[] strArr, String[] strArr2, String str) {
        if (strArr == null || strArr2 == null || strArr.length != strArr2.length || str == null) {
            return null;
        }
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            if (str.equals(strArr[i])) {
                return strArr2[i];
            }
        }
        return null;
    }

    /* JADX WARN: Type inference failed for: r0v20, types: [java.lang.Object[]] */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00a7, code lost:
        if (r0.dt.a(r28, r7, r32[0], r32[1], r32[2], r32[3], 1, r13) != false) goto L_0x00a9;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0168  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] a(com.uc.c.f r28, int r29, long r30, int[] r32, com.uc.c.f r33, com.uc.c.f r34, com.uc.c.f[] r35, int[] r36, int r37) {
        /*
            r27 = this;
            r5 = 262144(0x40000, double:1.295163E-318)
            long r5 = r5 & r30
            r7 = 0
            int r5 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r5 == 0) goto L_0x00e7
            r5 = 1
            r13 = r5
        L_0x000d:
            r5 = 1
            r0 = r37
            r1 = r5
            if (r0 != r1) goto L_0x00eb
            r5 = 1
            r25 = r5
        L_0x0016:
            r5 = 0
            r6 = 2
            r0 = r6
            int[] r0 = new int[r0]
            r26 = r0
            if (r32 == 0) goto L_0x01ba
            r0 = r27
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            com.uc.c.f r6 = r6.bFR
            r0 = r28
            r1 = r6
            if (r0 == r1) goto L_0x01ba
            r0 = r28
            com.uc.c.f r0 = r0.cG
            r5 = r0
            r0 = r28
            com.uc.c.f r0 = r0.cG
            r6 = r0
            r0 = r6
            r1 = r28
            int r6 = r0.indexOf(r1)
            byte[] r5 = com.uc.c.ca.c(r5, r6)
            r0 = r27
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            com.uc.c.f r6 = r6.bFR
            r0 = r33
            r1 = r6
            if (r0 != r1) goto L_0x00f0
            r0 = r27
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            r0 = r28
            com.uc.c.f r0 = r0.cG
            r7 = r0
            r0 = r6
            r1 = r33
            r2 = r7
            r3 = r5
            r4 = r26
            r0.a(r1, r2, r3, r4)
        L_0x0060:
            r5 = 0
            r6 = r32[r5]
            r7 = 0
            r7 = r26[r7]
            int r6 = r6 - r7
            r32[r5] = r6
            r5 = 1
            r6 = r32[r5]
            r7 = 1
            r7 = r26[r7]
            int r6 = r6 - r7
            r32[r5] = r6
            r5 = 0
            r14 = r5
            r7 = r29
        L_0x0076:
            r0 = r28
            short r0 = r0.cF
            r5 = r0
            r6 = 1
            int r5 = r5 - r6
            if (r7 > r5) goto L_0x01b7
            if (r7 < 0) goto L_0x01b7
            r0 = r28
            java.lang.Object[] r0 = r0.cE
            r5 = r0
            r5 = r5[r7]
            r0 = r5
            r1 = r34
            if (r0 == r1) goto L_0x01b5
            if (r32 == 0) goto L_0x00a9
            r0 = r27
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            r6 = 0
            r8 = r32[r6]
            r6 = 1
            r9 = r32[r6]
            r6 = 2
            r10 = r32[r6]
            r6 = 3
            r11 = r32[r6]
            r12 = 1
            r6 = r28
            boolean r5 = r5.a(r6, r7, r8, r9, r10, r11, r12, r13)
            if (r5 == 0) goto L_0x01b5
        L_0x00a9:
            r0 = r28
            r1 = r7
            byte[] r5 = com.uc.c.ca.c(r0, r1)
            r8 = 1
            r6 = 0
            byte r6 = r5[r6]
            long r8 = r8 << r6
            long r8 = r8 & r30
            r10 = 0
            int r6 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r6 == 0) goto L_0x0100
            r6 = 0
            r35[r6] = r28
            r6 = 0
            r36[r6] = r7
        L_0x00c4:
            if (r32 == 0) goto L_0x00e4
            r0 = r27
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            com.uc.c.f r6 = r6.bFR
            r0 = r28
            r1 = r6
            if (r0 == r1) goto L_0x00e4
            r6 = 0
            r7 = r32[r6]
            r8 = 0
            r8 = r26[r8]
            int r7 = r7 + r8
            r32[r6] = r7
            r6 = 1
            r7 = r32[r6]
            r8 = 1
            r8 = r26[r8]
            int r7 = r7 + r8
            r32[r6] = r7
        L_0x00e4:
            if (r5 == 0) goto L_0x016e
        L_0x00e6:
            return r5
        L_0x00e7:
            r5 = 0
            r13 = r5
            goto L_0x000d
        L_0x00eb:
            r5 = 0
            r25 = r5
            goto L_0x0016
        L_0x00f0:
            r6 = 0
            int r7 = com.uc.c.f.b(r5)
            r26[r6] = r7
            r6 = 1
            int r5 = com.uc.c.f.c(r5)
            r26[r6] = r5
            goto L_0x0060
        L_0x0100:
            r0 = r28
            java.lang.Object[] r0 = r0.cE
            r6 = r0
            r6 = r6[r7]
            boolean r6 = r6 instanceof com.uc.c.f
            if (r6 == 0) goto L_0x014c
            r6 = 0
            byte r6 = r5[r6]
            r8 = 25
            if (r6 != r8) goto L_0x011d
            byte[] r6 = com.uc.c.ca.bLC
            r8 = 1
            int r5 = com.uc.c.f.b(r5, r6, r8)
            r5 = r5 & 1
            if (r5 != 0) goto L_0x014c
        L_0x011d:
            r0 = r28
            java.lang.Object[] r0 = r0.cE
            r5 = r0
            r15 = r5[r7]
            com.uc.c.f r15 = (com.uc.c.f) r15
            r5 = 1
            r0 = r37
            r1 = r5
            if (r0 != r1) goto L_0x0155
            r5 = 0
            r16 = r5
        L_0x012f:
            r0 = r28
            java.lang.Object[] r0 = r0.cE
            r5 = r0
            r20 = r5[r7]
            com.uc.c.f r20 = (com.uc.c.f) r20
            r14 = r27
            r17 = r30
            r19 = r32
            r21 = r34
            r22 = r35
            r23 = r36
            r24 = r37
            byte[] r5 = r14.a(r15, r16, r17, r19, r20, r21, r22, r23, r24)
            if (r5 != 0) goto L_0x00c4
        L_0x014c:
            r5 = 0
        L_0x014d:
            if (r25 == 0) goto L_0x0168
            int r6 = r7 + 1
            r14 = r5
            r7 = r6
            goto L_0x0076
        L_0x0155:
            r0 = r28
            java.lang.Object[] r0 = r0.cE
            r5 = r0
            r29 = r5[r7]
            com.uc.c.f r29 = (com.uc.c.f) r29
            r0 = r29
            short r0 = r0.cF
            r5 = r0
            r6 = 1
            int r5 = r5 - r6
            r16 = r5
            goto L_0x012f
        L_0x0168:
            int r6 = r7 + -1
            r14 = r5
            r7 = r6
            goto L_0x0076
        L_0x016e:
            r0 = r28
            r1 = r33
            if (r0 == r1) goto L_0x017b
            r0 = r28
            com.uc.c.f r0 = r0.cG
            r5 = r0
            if (r5 != 0) goto L_0x017e
        L_0x017b:
            r5 = 0
            goto L_0x00e6
        L_0x017e:
            r0 = r28
            com.uc.c.f r0 = r0.cG
            r5 = r0
            r0 = r5
            r1 = r28
            int r5 = r0.indexOf(r1)
            r6 = 1
            r0 = r37
            r1 = r6
            if (r0 != r1) goto L_0x01b3
            r6 = 1
        L_0x0191:
            int r7 = r5 + r6
            r0 = r28
            com.uc.c.f r0 = r0.cG
            r6 = r0
            r0 = r27
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            com.uc.c.f r11 = r5.bFR
            r5 = r27
            r8 = r30
            r10 = r32
            r12 = r34
            r13 = r35
            r14 = r36
            r15 = r37
            byte[] r5 = r5.a(r6, r7, r8, r10, r11, r12, r13, r14, r15)
            goto L_0x00e6
        L_0x01b3:
            r6 = -1
            goto L_0x0191
        L_0x01b5:
            r5 = r14
            goto L_0x014d
        L_0x01b7:
            r5 = r14
            goto L_0x00c4
        L_0x01ba:
            r14 = r5
            r7 = r29
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.i.a(com.uc.c.f, int, long, int[], com.uc.c.f, com.uc.c.f, com.uc.c.f[], int[], int):byte[]");
    }

    private byte[] a(f fVar, f fVar2, int i, f[] fVarArr, int[] iArr) {
        return a(fVar, fVar2, (long) i, (int[]) null, fVarArr, iArr, -1);
    }

    public static Object[] a(r rVar, int i, ca caVar) {
        return a(rVar, rVar.jm(), i, caVar);
    }

    public static Object[] a(r rVar, ca caVar, int i, ca caVar2) {
        return new Object[]{rVar, caVar, new int[]{i}, caVar2};
    }

    public static Object[] a(r rVar, ca caVar, ca caVar2) {
        return a(rVar, caVar, 0, caVar2);
    }

    public static Object[] a(String str, int i, int i2, int i3, String[] strArr, String[] strArr2, String str2) {
        return a(str, i, null, -1, null, null, null, i2, i3, strArr, strArr2, null, null, str2);
    }

    public static Object[] a(String str, int i, int i2, String str2) {
        return a(str, 0, null, -1, null, null, null, i, i2, null, null, null, null, str2);
    }

    public static Object[] a(String str, int i, String str2) {
        return a(str, i, 0, str2);
    }

    public static Object[] a(String str, int i, String str2, int i2, String str3, String str4, String str5, int i3, int i4, String[] strArr, String[] strArr2, String[] strArr3, String[] strArr4, String str6) {
        return a(str, i, str2, i2, str3, str4, str5, i3, i4, strArr, strArr2, strArr3, strArr4, str6, null);
    }

    public static Object[] a(String str, int i, String str2, int i2, String str3, String str4, String str5, int i3, int i4, String[] strArr, String[] strArr2, String[] strArr3, String[] strArr4, String str6, bb bbVar) {
        return new Object[]{str, str2, str3, str4, str5, strArr, strArr2, strArr3, strArr4, new int[]{i3, i4, i, i2}, str6, bbVar};
    }

    public static String b(String[] strArr) {
        if (strArr == null) {
            return cd.bUi;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (String append : strArr) {
            stringBuffer.append(append);
        }
        return stringBuffer.toString();
    }

    /* JADX WARN: Type inference failed for: r0v20, types: [java.lang.Object[]] */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00a7, code lost:
        if (r0.dt.a(r28, r7, r32[0], r32[1], r32[2], r32[3], 1, r13) != false) goto L_0x00a9;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0167  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0180  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] b(com.uc.c.f r28, int r29, long r30, int[] r32, com.uc.c.f r33, com.uc.c.f r34, com.uc.c.f[] r35, int[] r36, int r37) {
        /*
            r27 = this;
            r5 = 262144(0x40000, double:1.295163E-318)
            long r5 = r5 & r30
            r7 = 0
            int r5 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r5 == 0) goto L_0x00f3
            r5 = 1
            r13 = r5
        L_0x000d:
            r5 = 1
            r0 = r37
            r1 = r5
            if (r0 != r1) goto L_0x00f7
            r5 = 1
            r25 = r5
        L_0x0016:
            r5 = 0
            r6 = 2
            r0 = r6
            int[] r0 = new int[r0]
            r26 = r0
            if (r32 == 0) goto L_0x01d2
            r0 = r27
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            com.uc.c.f r6 = r6.bFR
            r0 = r28
            r1 = r6
            if (r0 == r1) goto L_0x01d2
            r0 = r28
            com.uc.c.f r0 = r0.cG
            r5 = r0
            r0 = r28
            com.uc.c.f r0 = r0.cG
            r6 = r0
            r0 = r6
            r1 = r28
            int r6 = r0.indexOf(r1)
            byte[] r5 = com.uc.c.ca.c(r5, r6)
            r0 = r27
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            com.uc.c.f r6 = r6.bFR
            r0 = r33
            r1 = r6
            if (r0 != r1) goto L_0x00fc
            r0 = r27
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            r0 = r28
            com.uc.c.f r0 = r0.cG
            r7 = r0
            r0 = r6
            r1 = r33
            r2 = r7
            r3 = r5
            r4 = r26
            r0.a(r1, r2, r3, r4)
        L_0x0060:
            r5 = 0
            r6 = r32[r5]
            r7 = 0
            r7 = r26[r7]
            int r6 = r6 - r7
            r32[r5] = r6
            r5 = 1
            r6 = r32[r5]
            r7 = 1
            r7 = r26[r7]
            int r6 = r6 - r7
            r32[r5] = r6
            r5 = 0
            r14 = r5
            r7 = r29
        L_0x0076:
            r0 = r28
            short r0 = r0.cF
            r5 = r0
            r6 = 1
            int r5 = r5 - r6
            if (r7 > r5) goto L_0x01cf
            if (r7 < 0) goto L_0x01cf
            r0 = r28
            java.lang.Object[] r0 = r0.cE
            r5 = r0
            r5 = r5[r7]
            r0 = r5
            r1 = r34
            if (r0 == r1) goto L_0x01cd
            if (r32 == 0) goto L_0x00a9
            r0 = r27
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            r6 = 0
            r8 = r32[r6]
            r6 = 1
            r9 = r32[r6]
            r6 = 2
            r10 = r32[r6]
            r6 = 3
            r11 = r32[r6]
            r12 = 1
            r6 = r28
            boolean r5 = r5.a(r6, r7, r8, r9, r10, r11, r12, r13)
            if (r5 == 0) goto L_0x01cd
        L_0x00a9:
            r0 = r28
            r1 = r7
            byte[] r5 = com.uc.c.ca.c(r0, r1)
            r8 = 1
            r6 = 0
            byte r6 = r5[r6]
            long r8 = r8 << r6
            long r8 = r8 & r30
            r10 = 0
            int r6 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r6 == 0) goto L_0x010c
            boolean r6 = com.uc.c.ca.aC(r5)
            if (r6 == 0) goto L_0x010c
            r6 = 0
            r35[r6] = r28
            r6 = 0
            r36[r6] = r7
        L_0x00ca:
            if (r32 == 0) goto L_0x00ea
            r0 = r27
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            com.uc.c.f r6 = r6.bFR
            r0 = r28
            r1 = r6
            if (r0 == r1) goto L_0x00ea
            r6 = 0
            r7 = r32[r6]
            r8 = 0
            r8 = r26[r8]
            int r7 = r7 + r8
            r32[r6] = r7
            r6 = 1
            r7 = r32[r6]
            r8 = 1
            r8 = r26[r8]
            int r7 = r7 + r8
            r32[r6] = r7
        L_0x00ea:
            if (r5 == 0) goto L_0x0186
            boolean r6 = com.uc.c.ca.aC(r5)
            if (r6 == 0) goto L_0x0186
        L_0x00f2:
            return r5
        L_0x00f3:
            r5 = 0
            r13 = r5
            goto L_0x000d
        L_0x00f7:
            r5 = 0
            r25 = r5
            goto L_0x0016
        L_0x00fc:
            r6 = 0
            int r7 = com.uc.c.f.b(r5)
            r26[r6] = r7
            r6 = 1
            int r5 = com.uc.c.f.c(r5)
            r26[r6] = r5
            goto L_0x0060
        L_0x010c:
            r0 = r28
            java.lang.Object[] r0 = r0.cE
            r6 = r0
            r6 = r6[r7]
            boolean r6 = r6 instanceof com.uc.c.f
            if (r6 == 0) goto L_0x0164
            r6 = 0
            byte r6 = r5[r6]
            r8 = 25
            if (r6 != r8) goto L_0x0129
            byte[] r6 = com.uc.c.ca.bLC
            r8 = 1
            int r6 = com.uc.c.f.b(r5, r6, r8)
            r6 = r6 & 1
            if (r6 != 0) goto L_0x0164
        L_0x0129:
            boolean r5 = com.uc.c.ca.aC(r5)
            if (r5 == 0) goto L_0x0164
            r0 = r28
            java.lang.Object[] r0 = r0.cE
            r5 = r0
            r15 = r5[r7]
            com.uc.c.f r15 = (com.uc.c.f) r15
            r5 = 1
            r0 = r37
            r1 = r5
            if (r0 != r1) goto L_0x016d
            r5 = 0
            r16 = r5
        L_0x0141:
            r0 = r28
            java.lang.Object[] r0 = r0.cE
            r5 = r0
            r20 = r5[r7]
            com.uc.c.f r20 = (com.uc.c.f) r20
            r14 = r27
            r17 = r30
            r19 = r32
            r21 = r34
            r22 = r35
            r23 = r36
            r24 = r37
            byte[] r5 = r14.b(r15, r16, r17, r19, r20, r21, r22, r23, r24)
            if (r5 == 0) goto L_0x0164
            boolean r6 = com.uc.c.ca.aC(r5)
            if (r6 != 0) goto L_0x00ca
        L_0x0164:
            r5 = 0
        L_0x0165:
            if (r25 == 0) goto L_0x0180
            int r6 = r7 + 1
            r14 = r5
            r7 = r6
            goto L_0x0076
        L_0x016d:
            r0 = r28
            java.lang.Object[] r0 = r0.cE
            r5 = r0
            r29 = r5[r7]
            com.uc.c.f r29 = (com.uc.c.f) r29
            r0 = r29
            short r0 = r0.cF
            r5 = r0
            r6 = 1
            int r5 = r5 - r6
            r16 = r5
            goto L_0x0141
        L_0x0180:
            int r6 = r7 + -1
            r14 = r5
            r7 = r6
            goto L_0x0076
        L_0x0186:
            r0 = r28
            r1 = r33
            if (r0 == r1) goto L_0x0193
            r0 = r28
            com.uc.c.f r0 = r0.cG
            r5 = r0
            if (r5 != 0) goto L_0x0196
        L_0x0193:
            r5 = 0
            goto L_0x00f2
        L_0x0196:
            r0 = r28
            com.uc.c.f r0 = r0.cG
            r5 = r0
            r0 = r5
            r1 = r28
            int r5 = r0.indexOf(r1)
            r6 = 1
            r0 = r37
            r1 = r6
            if (r0 != r1) goto L_0x01cb
            r6 = 1
        L_0x01a9:
            int r7 = r5 + r6
            r0 = r28
            com.uc.c.f r0 = r0.cG
            r6 = r0
            r0 = r27
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            com.uc.c.f r11 = r5.bFR
            r5 = r27
            r8 = r30
            r10 = r32
            r12 = r34
            r13 = r35
            r14 = r36
            r15 = r37
            byte[] r5 = r5.b(r6, r7, r8, r10, r11, r12, r13, r14, r15)
            goto L_0x00f2
        L_0x01cb:
            r6 = -1
            goto L_0x01a9
        L_0x01cd:
            r5 = r14
            goto L_0x0165
        L_0x01cf:
            r5 = r14
            goto L_0x00ca
        L_0x01d2:
            r14 = r5
            r7 = r29
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.i.b(com.uc.c.f, int, long, int[], com.uc.c.f, com.uc.c.f, com.uc.c.f[], int[], int):byte[]");
    }

    private void h(byte[] bArr) {
        if (bArr != null) {
            int b2 = f.b(bArr, ca.bLv, 0);
            if (b2 == 4) {
                if (this.p != null) {
                    this.p.eA(cb.MA().ML());
                }
            } else if (b2 == 5 && this.p != null) {
                this.p.gp();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x01e6 A[LOOP:2: B:18:0x00c8->B:41:0x01e6, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x01ec A[LOOP:1: B:6:0x0047->B:42:0x01ec, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0077 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00f8 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int j(byte[] r20) {
        /*
            r19 = this;
            byte[] r4 = com.uc.c.ca.bLD
            r5 = 3
            r0 = r20
            r1 = r4
            r2 = r5
            int r4 = com.uc.c.f.b(r0, r1, r2)
            r0 = r20
            r1 = r4
            int r5 = com.uc.c.bc.x(r0, r1)
            int r4 = r4 + 2
            int r13 = r4 + r5
        L_0x0016:
            if (r4 >= r13) goto L_0x0180
            int r5 = r4 + 1
            byte r4 = r20[r4]
            int r14 = r5 + 1
            byte r15 = r20[r5]
            switch(r4) {
                case 101: goto L_0x0026;
                case 102: goto L_0x012a;
                case 103: goto L_0x0023;
                case 104: goto L_0x00a7;
                default: goto L_0x0023;
            }
        L_0x0023:
            int r4 = r14 + r15
            goto L_0x0016
        L_0x0026:
            r0 = r20
            r1 = r14
            int r16 = com.uc.c.bc.x(r0, r1)
            r4 = 1
            com.uc.c.f[] r10 = new com.uc.c.f[r4]
            r4 = 0
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            com.uc.c.f r5 = r5.bFR
            r10[r4] = r5
            r4 = 1
            int[] r11 = new int[r4]
            r4 = 0
            r5 = -1
            r11[r4] = r5
            r4 = 0
            r5 = -1
            r17 = r5
            r18 = r4
        L_0x0047:
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            com.uc.c.f r5 = r4.bFR
            r6 = 0
            r7 = 33554432(0x2000000, double:1.6578092E-316)
            r9 = 0
            r12 = 1
            r4 = r19
            byte[] r4 = r4.a(r5, r6, r7, r9, r10, r11, r12)
            if (r4 == 0) goto L_0x00a2
            byte[] r5 = com.uc.c.ca.bLC
            r6 = 0
            int r5 = com.uc.c.f.b(r4, r5, r6)
            r0 = r5
            r1 = r16
            if (r0 != r1) goto L_0x0093
            byte[] r5 = com.uc.c.ca.bLC
            r6 = 1
            r7 = 0
            r8 = 0
            com.uc.c.f.a(r4, r5, r6, r7, r8)
            int r5 = com.uc.c.f.e(r4)
            r6 = 1
        L_0x0075:
            if (r4 != 0) goto L_0x01ec
            if (r6 == 0) goto L_0x0023
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            int r6 = r4.ahU
            int r5 = r5 + r6
            r4.ahU = r5
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            int r5 = r5.ahU
            r4.iU(r5)
            goto L_0x0023
        L_0x0093:
            if (r18 == 0) goto L_0x00a2
            int r5 = com.uc.c.f.b(r4)
            int r6 = com.uc.c.f.c(r4)
            int r6 = r6 + r17
            com.uc.c.f.a(r4, r5, r6)
        L_0x00a2:
            r5 = r17
            r6 = r18
            goto L_0x0075
        L_0x00a7:
            r0 = r20
            r1 = r14
            int r16 = com.uc.c.bc.x(r0, r1)
            r4 = 1
            com.uc.c.f[] r10 = new com.uc.c.f[r4]
            r4 = 0
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            com.uc.c.f r5 = r5.bFR
            r10[r4] = r5
            r4 = 1
            int[] r11 = new int[r4]
            r4 = 0
            r5 = -1
            r11[r4] = r5
            r4 = 0
            r5 = -1
            r17 = r5
            r18 = r4
        L_0x00c8:
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            com.uc.c.f r5 = r4.bFR
            r6 = 0
            r7 = 33554432(0x2000000, double:1.6578092E-316)
            r9 = 0
            r12 = 1
            r4 = r19
            byte[] r4 = r4.a(r5, r6, r7, r9, r10, r11, r12)
            if (r4 == 0) goto L_0x0125
            byte[] r5 = com.uc.c.ca.bLC
            r6 = 0
            int r5 = com.uc.c.f.b(r4, r5, r6)
            r0 = r5
            r1 = r16
            if (r0 != r1) goto L_0x0116
            byte[] r5 = com.uc.c.ca.bLC
            r6 = 1
            r7 = 1
            r8 = 0
            com.uc.c.f.a(r4, r5, r6, r7, r8)
            int r5 = com.uc.c.f.e(r4)
            r6 = 1
        L_0x00f6:
            if (r4 != 0) goto L_0x01e6
            if (r6 == 0) goto L_0x0023
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            int r6 = r4.ahU
            int r5 = r6 - r5
            r4.ahU = r5
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            int r5 = r5.ahU
            r4.iU(r5)
            goto L_0x0023
        L_0x0116:
            if (r18 == 0) goto L_0x0125
            int r5 = com.uc.c.f.b(r4)
            int r6 = com.uc.c.f.c(r4)
            int r6 = r6 - r17
            com.uc.c.f.a(r4, r5, r6)
        L_0x0125:
            r5 = r17
            r6 = r18
            goto L_0x00f6
        L_0x012a:
            r0 = r20
            r1 = r14
            int r16 = com.uc.c.bc.x(r0, r1)
            r4 = 1
            com.uc.c.f[] r10 = new com.uc.c.f[r4]
            r4 = 0
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            com.uc.c.f r5 = r5.bFR
            r10[r4] = r5
            r4 = 1
            int[] r11 = new int[r4]
            r4 = 0
            r5 = -1
            r11[r4] = r5
        L_0x0145:
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            com.uc.c.f r5 = r4.bFR
            r6 = 0
            r7 = 16777216(0x1000000, double:8.289046E-317)
            r9 = 0
            r12 = 1
            r4 = r19
            byte[] r4 = r4.a(r5, r6, r7, r9, r10, r11, r12)
            if (r4 == 0) goto L_0x017c
            byte[] r5 = com.uc.c.ca.bLD
            r6 = 0
            int r5 = com.uc.c.f.b(r4, r5, r6)
            r0 = r5
            r1 = r16
            if (r0 != r1) goto L_0x017c
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            r5 = 0
            r5 = r10[r5]
            r4.bHl = r5
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            r5 = 0
            r5 = r11[r5]
            r4.bHm = r5
            goto L_0x0023
        L_0x017c:
            if (r4 != 0) goto L_0x0145
            goto L_0x0023
        L_0x0180:
            r4 = 2
            int[] r4 = new int[r4]
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            com.uc.c.f r6 = r6.bHl
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r7 = r0
            com.uc.c.f r7 = r7.bHl
            java.lang.Object[] r7 = r7.cE
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r8 = r0
            int r8 = r8.bHm
            r20 = r7[r8]
            byte[] r20 = (byte[]) r20
            byte[] r20 = (byte[]) r20
            r0 = r5
            r1 = r6
            r2 = r20
            r3 = r4
            r0.a(r1, r2, r3)
            r5 = 0
            r5 = r4[r5]
            r6 = 1
            r4 = r4[r6]
            r6 = 4
            int r4 = r4 - r6
            r0 = r19
            r1 = r5
            r2 = r4
            r0.f(r1, r2)
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            com.uc.a.n r4 = r4.ch()
            if (r4 == 0) goto L_0x01db
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            r4.ap()
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            com.uc.a.n r4 = r4.ch()
            r4.qW()
        L_0x01db:
            r0 = r19
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            r5 = 1
            r4.iH(r5)
            r4 = 2
            return r4
        L_0x01e6:
            r17 = r5
            r18 = r6
            goto L_0x00c8
        L_0x01ec:
            r17 = r5
            r18 = r6
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.i.j(byte[]):int");
    }

    public static String r(byte[] bArr) {
        return r(bArr, 0);
    }

    public static String r(byte[] bArr, int i) {
        short f = (short) f.f(bArr, i == 0 ? 20 : 21);
        if (f == -1) {
            return null;
        }
        return f.e(bArr, f);
    }

    public static String s(byte[] bArr) {
        return r(bArr, 1);
    }

    public static int t(byte[] bArr) {
        return f.f(bArr, 22);
    }

    public static ca w(int i) {
        return ca.iA(i == 0 ? 1 : (i == 0 || i == 3) ? 2 : 0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:146:0x05ee, code lost:
        r4 = r9;
     */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0476  */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x01f0 A[EDGE_INSN: B:149:0x01f0->B:34:0x01f0 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x0254 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0137  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0187  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0213  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x026c  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x02b4  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x03ae  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int a(int r25, int r26, int[] r27) {
        /*
            r24 = this;
            r4 = 1
            com.uc.c.f[] r10 = new com.uc.c.f[r4]
            r4 = 1
            int[] r11 = new int[r4]
            r20 = 0
            r21 = 0
            r4 = 4
            r0 = r4
            int[] r0 = new int[r0]
            r22 = r0
            r5 = 0
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            com.uc.c.f r6 = r6.bHl
            r4.bHn = r6
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            int r6 = r6.bHm
            r4.bHo = r6
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            com.uc.c.f r4 = r4.bHl
            if (r4 == 0) goto L_0x0040
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            int r4 = r4.bHm
            r6 = -1
            if (r4 != r6) goto L_0x0282
        L_0x0040:
            r4 = 0
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            com.uc.c.f r5 = r5.bFR
            r10[r4] = r5
            r4 = 0
            r5 = -1
            r11[r4] = r5
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            com.uc.c.f r5 = r4.bFR
            r6 = 0
            r7 = -4503599342001424(0xfff00000110262f0, double:NaN)
            r9 = 0
            r4 = r24
            r12 = r25
            byte[] r4 = r4.b(r5, r6, r7, r9, r10, r11, r12)
            r5 = 1
            r6 = r4
        L_0x0066:
            r4 = 0
            if (r6 == 0) goto L_0x0097
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            r7 = 0
            r7 = r10[r7]
            r8 = 0
            r8 = r11[r8]
            r9 = 1
            java.util.Vector r4 = r4.b(r7, r8, r9)
            r7 = 0
            java.lang.Object r4 = r4.elementAt(r7)
            byte[] r4 = (byte[]) r4
            byte[] r4 = (byte[]) r4
            int r4 = com.uc.c.f.e(r4)
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r7 = r0
            int r7 = r7.bFQ
            int r7 = com.uc.b.e.bm(r7)
            int r4 = java.lang.Math.min(r4, r7)
            r7 = 1
            int r4 = r4 - r7
        L_0x0097:
            if (r6 == 0) goto L_0x00d6
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r12 = r0
            r7 = 0
            r13 = r10[r7]
            r7 = 0
            r14 = r11[r7]
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r7 = r0
            int r15 = r7.bGw
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r7 = r0
            int r7 = r7.bGx
            r8 = 1
            r0 = r25
            r1 = r8
            if (r0 != r1) goto L_0x02a7
            r8 = r4
        L_0x00b9:
            int r16 = r7 + r8
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r7 = r0
            r0 = r7
            short r0 = r0.bGr
            r17 = r0
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r7 = r0
            short r7 = r7.bGs
            int r18 = r7 - r4
            r19 = 0
            boolean r4 = r12.a(r13, r14, r15, r16, r17, r18, r19)
            if (r4 != 0) goto L_0x02ad
        L_0x00d6:
            r23 = 2
            r5 = 0
            if (r6 == 0) goto L_0x05f5
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            r6 = 0
            r6 = r10[r6]
            r7 = 0
            r7 = r11[r7]
            r8 = 1
            java.util.Vector r4 = r4.b(r6, r7, r8)
            r6 = 0
            java.lang.Object r4 = r4.elementAt(r6)
            byte[] r4 = (byte[]) r4
            byte[] r4 = (byte[]) r4
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            r7 = 0
            r7 = r10[r7]
            r0 = r6
            r1 = r7
            r2 = r4
            r3 = r22
            r0.a(r1, r2, r3)
            r4 = 1
            r4 = r22[r4]
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            int r6 = r6.bGx
            if (r4 > r6) goto L_0x0116
            r4 = 1
            r0 = r25
            r1 = r4
            if (r0 == r1) goto L_0x0134
        L_0x0116:
            r4 = -1
            r0 = r25
            r1 = r4
            if (r0 != r1) goto L_0x05f5
            r4 = 1
            r4 = r22[r4]
            r6 = 3
            r6 = r22[r6]
            int r4 = r4 + r6
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            int r6 = r6.bGx
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r7 = r0
            short r7 = r7.bGs
            int r6 = r6 + r7
            if (r4 < r6) goto L_0x05f5
        L_0x0134:
            r4 = 1
        L_0x0135:
            if (r4 != 0) goto L_0x0145
            r4 = 0
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            com.uc.c.f r5 = r5.bFR
            r10[r4] = r5
            r4 = 0
            r5 = -1
            r11[r4] = r5
        L_0x0145:
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            com.uc.c.f r5 = r4.bFR
            r6 = 0
            r7 = -4503599342001424(0xfff00000110262f0, double:NaN)
            r4 = 4
            int[] r9 = new int[r4]
            r4 = 0
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r12 = r0
            int r12 = r12.bGw
            r9[r4] = r12
            r4 = 1
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r12 = r0
            int r12 = r12.bGx
            r9[r4] = r12
            r4 = 2
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r12 = r0
            short r12 = r12.bGr
            r9[r4] = r12
            r4 = 3
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r12 = r0
            short r12 = r12.bGs
            r9[r4] = r12
            r4 = r24
            r12 = r25
            byte[] r5 = r4.b(r5, r6, r7, r9, r10, r11, r12)
            if (r5 == 0) goto L_0x01f0
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            r6 = 0
            r6 = r10[r6]
            r7 = 0
            r7 = r11[r7]
            r8 = 1
            java.util.Vector r4 = r4.b(r6, r7, r8)
            r6 = 0
            java.lang.Object r4 = r4.elementAt(r6)
            byte[] r4 = (byte[]) r4
            byte[] r4 = (byte[]) r4
            int r4 = com.uc.c.f.e(r4)
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            int r6 = r6.bFQ
            int r6 = com.uc.b.e.bm(r6)
            int r4 = java.lang.Math.min(r4, r6)
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r12 = r0
            r6 = 0
            r13 = r10[r6]
            r6 = 0
            r14 = r11[r6]
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            int r15 = r6.bGw
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            int r6 = r6.bGx
            r7 = 1
            r0 = r25
            r1 = r7
            if (r0 != r1) goto L_0x02aa
            r7 = r4
        L_0x01d3:
            int r16 = r6 + r7
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            r0 = r6
            short r0 = r0.bGr
            r17 = r0
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            short r6 = r6.bGs
            int r18 = r6 - r4
            r19 = 0
            boolean r4 = r12.a(r13, r14, r15, r16, r17, r18, r19)
            if (r4 == 0) goto L_0x0145
        L_0x01f0:
            r6 = r5
            r5 = r23
        L_0x01f3:
            r4 = 0
            r7 = 0
            r8 = 1
            r0 = r26
            r1 = r8
            if (r0 != r1) goto L_0x05f1
            if (r6 == 0) goto L_0x05f1
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r8 = r0
            r9 = 0
            r9 = r10[r9]
            r12 = 0
            r12 = r11[r12]
            r13 = 1
            java.util.Vector r8 = r8.b(r9, r12, r13)
            int r9 = r8.size()
            if (r9 <= 0) goto L_0x05f1
            r4 = 0
            java.lang.Object r4 = r8.elementAt(r4)
            byte[] r4 = (byte[]) r4
            byte[] r4 = (byte[]) r4
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r7 = r0
            r8 = 0
            r8 = r10[r8]
            r0 = r7
            r1 = r8
            r2 = r4
            r3 = r22
            r0.a(r1, r2, r3)
            r7 = 3
            int r4 = com.uc.c.f.e(r4)
            r22[r7] = r4
            r4 = 1
            r4 = r22[r4]
            r7 = 1
            r0 = r25
            r1 = r7
            if (r0 != r1) goto L_0x02b2
            r7 = 3
            r7 = r22[r7]
        L_0x023f:
            int r4 = r4 + r7
            r7 = 0
            r7 = r22[r7]
            r13 = r7
            r14 = r4
        L_0x0245:
            r4 = -1
            r7 = 0
            r8 = -1
            r9 = -1
            r15 = r5
            r16 = r21
            r5 = r9
            r9 = r6
            r6 = r8
            r8 = r4
            r4 = r20
        L_0x0252:
            if (r9 != 0) goto L_0x02b4
            r4 = 1
            r0 = r26
            r1 = r4
            if (r0 != r1) goto L_0x05ee
            r4 = 0
            r11[r4] = r8
            r4 = 0
            r10[r4] = r7
            r4 = 0
            r4 = r10[r4]
            r5 = 0
            r5 = r11[r5]
            byte[] r4 = com.uc.c.ca.c(r4, r5)
        L_0x026a:
            if (r16 == 0) goto L_0x0476
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            r5 = 0
            r5 = r10[r5]
            r4.bHl = r5
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            r5 = 0
            r5 = r11[r5]
            r4.bHm = r5
            r4 = 2
        L_0x0281:
            return r4
        L_0x0282:
            r4 = 0
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            com.uc.c.f r6 = r6.bHl
            r10[r4] = r6
            r4 = 0
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            int r6 = r6.bHm
            r11[r4] = r6
            r4 = 0
            r4 = r10[r4]
            java.lang.Object[] r4 = r4.cE
            r6 = 0
            r6 = r11[r6]
            r4 = r4[r6]
            byte[] r4 = (byte[]) r4
            byte[] r4 = (byte[]) r4
            r6 = r4
            goto L_0x0066
        L_0x02a7:
            r8 = 0
            goto L_0x00b9
        L_0x02aa:
            r7 = 0
            goto L_0x01d3
        L_0x02ad:
            int r4 = r5 + 1
            r5 = r4
            goto L_0x01f3
        L_0x02b2:
            r7 = 0
            goto L_0x023f
        L_0x02b4:
            r12 = 1
            if (r15 <= r12) goto L_0x05e4
            r12 = 0
            byte r12 = r9[r12]
            boolean r12 = com.uc.c.ca.iZ(r12)
            if (r12 == 0) goto L_0x05e4
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r12 = r0
            r17 = 0
            r17 = r10[r17]
            r18 = 0
            r18 = r11[r18]
            r19 = 1
            r0 = r12
            r1 = r17
            r2 = r18
            r3 = r19
            java.util.Vector r12 = r0.b(r1, r2, r3)
            int r12 = r12.size()
            if (r12 <= 0) goto L_0x05e4
            if (r26 == 0) goto L_0x05ee
            if (r4 == 0) goto L_0x02e6
            r4 = r9
            goto L_0x026a
        L_0x02e6:
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            r12 = 0
            r12 = r10[r12]
            r17 = 0
            r17 = r11[r17]
            r18 = 1
            r0 = r4
            r1 = r12
            r2 = r17
            r3 = r18
            java.util.Vector r4 = r0.b(r1, r2, r3)
            int r12 = r4.size()
            if (r12 > 0) goto L_0x0307
            r4 = r9
            goto L_0x026a
        L_0x0307:
            r12 = 0
            java.lang.Object r4 = r4.elementAt(r12)
            byte[] r4 = (byte[]) r4
            byte[] r4 = (byte[]) r4
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r12 = r0
            r17 = 0
            r17 = r10[r17]
            r0 = r12
            r1 = r17
            r2 = r4
            r3 = r22
            r0.a(r1, r2, r3)
            r12 = 3
            int r4 = com.uc.c.f.e(r4)
            r22[r12] = r4
            r4 = 0
            r4 = r22[r4]
            r12 = 1
            r0 = r25
            r1 = r12
            if (r0 != r1) goto L_0x0357
            r12 = 1
            r12 = r22[r12]
        L_0x0335:
            if (r12 < r14) goto L_0x033f
            r17 = 1
            r0 = r25
            r1 = r17
            if (r0 == r1) goto L_0x0349
        L_0x033f:
            if (r12 >= r14) goto L_0x05e4
            r17 = -1
            r0 = r25
            r1 = r17
            if (r0 != r1) goto L_0x05e4
        L_0x0349:
            int r4 = r4 - r13
            int r17 = java.lang.Math.abs(r4)
            r4 = 5
            r0 = r17
            r1 = r4
            if (r0 >= r1) goto L_0x0361
            r4 = r9
            goto L_0x026a
        L_0x0357:
            r12 = 1
            r12 = r22[r12]
            r17 = 3
            r17 = r22[r17]
            int r12 = r12 + r17
            goto L_0x0335
        L_0x0361:
            if (r12 == r6) goto L_0x0366
            r4 = -1
            if (r6 != r4) goto L_0x036e
        L_0x0366:
            r0 = r17
            r1 = r5
            if (r0 < r1) goto L_0x0386
            r4 = -1
            if (r5 == r4) goto L_0x0386
        L_0x036e:
            r4 = 0
            r11[r4] = r8
            r4 = 0
            r10[r4] = r7
            r4 = 0
            r4 = r10[r4]
            java.lang.Object[] r4 = r4.cE
            r5 = 0
            r5 = r11[r5]
            r26 = r4[r5]
            byte[] r26 = (byte[]) r26
            byte[] r26 = (byte[]) r26
            r4 = r26
            goto L_0x026a
        L_0x0386:
            r4 = 0
            r5 = r11[r4]
            r4 = 0
            r6 = r10[r4]
            r4 = 0
            r4 = r10[r4]
            java.lang.Object[] r4 = r4.cE
            r7 = 0
            r7 = r11[r7]
            r4 = r4[r7]
            byte[] r4 = (byte[]) r4
            byte[] r4 = (byte[]) r4
            r18 = r12
            r19 = r6
            r20 = r5
        L_0x03a0:
            r4 = 0
            r4 = r10[r4]
            java.lang.Object[] r4 = r4.cE
            r5 = 0
            r5 = r11[r5]
            r4 = r4[r5]
            boolean r4 = r4 instanceof com.uc.c.f
            if (r4 == 0) goto L_0x03c1
            r5 = 0
            r4 = 0
            r4 = r10[r4]
            java.lang.Object[] r4 = r4.cE
            r6 = 0
            r6 = r11[r6]
            r4 = r4[r6]
            com.uc.c.f r4 = (com.uc.c.f) r4
            r10[r5] = r4
            r4 = 0
            r5 = -1
            r11[r4] = r5
        L_0x03c1:
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            com.uc.c.f r5 = r4.bFR
            r6 = 0
            r7 = -4503599342001424(0xfff00000110262f0, double:NaN)
            r9 = 0
            r4 = r24
            r12 = r25
            byte[] r4 = r4.b(r5, r6, r7, r9, r10, r11, r12)
            int r15 = r15 + 1
            r5 = 0
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            int r6 = r6.bGx
            if (r6 != 0) goto L_0x03e9
            r6 = -1
            r0 = r25
            r1 = r6
            if (r0 == r1) goto L_0x0407
        L_0x03e9:
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            int r6 = r6.bGx
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r7 = r0
            short r7 = r7.bGs
            int r6 = r6 + r7
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r7 = r0
            int r7 = r7.ahU
            if (r6 < r7) goto L_0x05d8
            r6 = 1
            r0 = r25
            r1 = r6
            if (r0 != r1) goto L_0x05d8
        L_0x0407:
            if (r4 != 0) goto L_0x05d8
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            boolean r6 = r6.LE()
            if (r6 == 0) goto L_0x05d8
            r4 = 0
            r5 = -1
            r0 = r25
            r1 = r5
            if (r0 != r1) goto L_0x046d
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            int r5 = r5.ahU
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            short r6 = r6.bGs
            int r5 = r5 - r6
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            int r6 = r6.bGv
            int r5 = r5 - r6
        L_0x0432:
            r0 = r24
            r1 = r4
            r2 = r5
            r0.g(r1, r2)
            r4 = 0
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            com.uc.c.f r5 = r5.bFR
            r10[r4] = r5
            r4 = 0
            r5 = -1
            r11[r4] = r5
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            com.uc.c.f r5 = r4.bFR
            r6 = 0
            r7 = -4503599342001424(0xfff00000110262f0, double:NaN)
            r9 = 0
            r4 = r24
            r12 = r25
            byte[] r4 = r4.b(r5, r6, r7, r9, r10, r11, r12)
            r5 = 1
            r6 = 1
            r7 = r19
            r8 = r20
            r16 = r6
            r9 = r4
            r4 = r5
            r6 = r18
            r5 = r17
            goto L_0x0252
        L_0x046d:
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            int r5 = r5.bGv
            int r5 = -r5
            goto L_0x0432
        L_0x0476:
            if (r4 == 0) goto L_0x0575
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            r5 = 0
            r5 = r10[r5]
            r6 = 0
            r6 = r11[r6]
            r7 = 1
            java.util.Vector r4 = r4.b(r5, r6, r7)
            r5 = 0
            java.lang.Object r26 = r4.elementAt(r5)
            byte[] r26 = (byte[]) r26
            byte[] r26 = (byte[]) r26
            int r4 = com.uc.c.f.e(r26)
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            int r5 = r5.bFQ
            int r5 = com.uc.b.e.bm(r5)
            int r4 = java.lang.Math.min(r4, r5)
            r5 = 1
            int r4 = r4 - r5
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r12 = r0
            r5 = 0
            r13 = r10[r5]
            r5 = 0
            r14 = r11[r5]
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            int r15 = r5.bGw
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            int r5 = r5.bGx
            r6 = -1
            r0 = r25
            r1 = r6
            if (r0 != r1) goto L_0x04fe
            r6 = r4
        L_0x04c6:
            int r16 = r5 + r6
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            r0 = r5
            short r0 = r0.bGr
            r17 = r0
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            short r5 = r5.bGs
            int r18 = r5 - r4
            r19 = 0
            boolean r4 = r12.a(r13, r14, r15, r16, r17, r18, r19)
            if (r4 == 0) goto L_0x0500
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            r5 = 0
            r5 = r10[r5]
            r4.bHl = r5
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            r5 = 0
            r5 = r11[r5]
            r4.bHm = r5
            r4 = 1
        L_0x04f8:
            r5 = 0
            r6 = 0
            r27[r5] = r6
            goto L_0x0281
        L_0x04fe:
            r6 = 0
            goto L_0x04c6
        L_0x0500:
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            r5 = 0
            r5 = r10[r5]
            r0 = r4
            r1 = r5
            r2 = r26
            r3 = r22
            r0.a(r1, r2, r3)
            r4 = 3
            int r5 = com.uc.c.f.e(r26)
            r22[r4] = r5
            r4 = -1
            r0 = r25
            r1 = r4
            if (r0 != r1) goto L_0x0536
            r4 = 1
            r4 = r22[r4]
            r5 = 3
            r5 = r22[r5]
            int r4 = r4 + r5
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            int r5 = r5.bGx
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            short r6 = r6.bGs
            int r5 = r5 + r6
            if (r4 >= r5) goto L_0x0548
        L_0x0536:
            r4 = 1
            r0 = r25
            r1 = r4
            if (r0 != r1) goto L_0x0561
            r4 = 1
            r4 = r22[r4]
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            int r5 = r5.bGx
            if (r4 >= r5) goto L_0x0561
        L_0x0548:
            r4 = 0
            r4 = r10[r4]
            r5 = 0
            r5 = r11[r5]
            r6 = -1
            r0 = r25
            r1 = r6
            if (r0 != r1) goto L_0x055f
            r6 = 2
        L_0x0555:
            r0 = r24
            r1 = r4
            r2 = r5
            r3 = r6
            r0.a(r1, r2, r3)
        L_0x055d:
            r4 = 1
            goto L_0x04f8
        L_0x055f:
            r6 = 1
            goto L_0x0555
        L_0x0561:
            r4 = 0
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            short r5 = r5.bGs
            int r5 = r5 >> 2
            int r5 = r5 * r25
            r0 = r24
            r1 = r4
            r2 = r5
            r0.g(r1, r2)
            goto L_0x055d
        L_0x0575:
            r4 = 0
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            short r5 = r5.bGs
            int r5 = r5 >> 2
            int r5 = r5 * r25
            r0 = r24
            r1 = r4
            r2 = r5
            boolean r4 = r0.g(r1, r2)
            if (r4 != 0) goto L_0x05d3
            r4 = 0
            r4 = r27[r4]
            r5 = 1
            boolean r4 = com.uc.c.bc.aL(r4, r5)
            if (r4 == 0) goto L_0x05d3
            r4 = -1
            r0 = r25
            r1 = r4
            if (r0 != r1) goto L_0x05d3
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            r5 = 0
            r4.bHl = r5
            r0 = r24
            com.uc.c.ca r0 = r0.dt
            r4 = r0
            r5 = -1
            r4.bHm = r5
            r4 = 0
            r5 = 0
            r5 = r27[r5]
            r6 = 6
            int r5 = com.uc.c.bc.aK(r5, r6)
            r27[r4] = r5
            r4 = 0
            r5 = 0
            r5 = r27[r5]
            r6 = 1
            int r5 = com.uc.c.bc.aJ(r5, r6)
            r27[r4] = r5
            r0 = r24
            com.uc.a.n r0 = r0.p
            r4 = r0
            if (r4 == 0) goto L_0x05d0
            r0 = r24
            com.uc.a.n r0 = r0.p
            r4 = r0
            r4.sj()
        L_0x05d0:
            r4 = 1
            goto L_0x0281
        L_0x05d3:
            r4 = 0
            r5 = 0
            r27[r4] = r5
            goto L_0x05d0
        L_0x05d8:
            r6 = r18
            r7 = r19
            r8 = r20
            r9 = r4
            r4 = r5
            r5 = r17
            goto L_0x0252
        L_0x05e4:
            r17 = r5
            r18 = r6
            r19 = r7
            r20 = r8
            goto L_0x03a0
        L_0x05ee:
            r4 = r9
            goto L_0x026a
        L_0x05f1:
            r13 = r7
            r14 = r4
            goto L_0x0245
        L_0x05f5:
            r4 = r5
            goto L_0x0135
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.i.a(int, int, int[]):int");
    }

    public final String a(String str, int i, int i2) {
        if (bc.by(str)) {
            return str;
        }
        int indexOf = str.indexOf(36, i2);
        if (indexOf == -1) {
            return str;
        }
        int[] iArr = new int[1];
        return a(a(str, i, indexOf, ca.bHh, iArr), i, iArr[0]);
    }

    public String a(String str, byte[] bArr, int i) {
        if (bc.by(str)) {
            return str;
        }
        int indexOf = str.indexOf(36);
        if (indexOf == -1) {
            return str;
        }
        d((Vector) null, bArr, i);
        String str2 = str;
        while (indexOf != -1) {
            String a2 = a(str2, 1, 0);
            str2 = a2;
            indexOf = a2.indexOf(36);
        }
        return str2;
    }

    public String a(Vector vector, Vector vector2, byte[] bArr) {
        Vector a2 = a(vector, vector2, bArr, 0, 2, 0);
        int size = a2.size();
        if (size <= 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < size; i++) {
            stringBuffer.append(a2.elementAt(i));
            if (size > 1 && i < size - 1) {
                stringBuffer.append(cd.bVG);
            }
        }
        return stringBuffer.toString();
    }

    public Vector a(Vector vector, Vector vector2, byte[] bArr, int i) {
        return a(vector, vector2, bArr, 1, i, 0);
    }

    public Vector a(Vector vector, Vector vector2, byte[] bArr, int i, int i2, int i3) {
        char[] e = e(vector, bArr);
        Vector vector3 = new Vector();
        int b2 = f.b(bArr, ca.bLp, 0);
        int b3 = f.b(bArr, ca.bLp, 1);
        boolean aL = bc.aL((byte) t(bArr), 8);
        if (i2 != 0) {
            StringBuffer stringBuffer = new StringBuffer();
            for (char c : e) {
                int i4 = c + b2;
                if (i4 != -1) {
                    byte[] bArr2 = (byte[]) vector2.elementAt(i4);
                    if (bArr2[0] == 10) {
                        f.a(bArr2, ca.bLq, i, stringBuffer);
                        vector3.addElement(stringBuffer.toString());
                        stringBuffer.setLength(0);
                    }
                }
            }
        } else if (aL) {
            int i5 = 0;
            while (i5 < r1) {
                try {
                    vector3.addElement(Integer.toString(e[i5]));
                    i5++;
                } catch (Exception e2) {
                }
            }
            for (int i6 = 0; i6 < b3 - r1; i6++) {
                vector3.addElement("");
            }
        } else {
            vector3.addElement(Integer.toString(e[0]));
        }
        return vector3;
    }

    public void a(int i, byte[] bArr) {
        if (this.dt.bGh != null) {
            Vector vector = (Vector) this.dt.bGh.get(Integer.valueOf(this.dt.bS()));
            if (vector != null && i >= 0 && i < vector.size()) {
                vector.setElementAt(bArr, i);
            }
        }
    }

    public void a(int i, char[] cArr, int i2) {
        a(i, cArr, 1, i2);
    }

    public void a(int i, char[] cArr, int i2, int i3) {
        if (i >= 0) {
            char[][] cArr2 = (char[][]) ((Vector) this.dt.bHd.get(Integer.valueOf(i3 == -1 ? this.dt.bS() : i3))).elementAt(i);
            char[] cArr3 = cArr2[0];
            if (i2 != 1) {
                cArr2[0] = cArr;
            } else if (bc.a(cArr, cArr3)) {
                cArr2[1] = null;
            } else {
                cArr2[1] = cArr;
            }
        }
    }

    public void a(bb bbVar, int i) {
        Vector vector = (Vector) this.dt.bHf.get(Integer.valueOf(i));
        for (int i2 = 0; i2 < vector.size(); i2++) {
            Vector vector2 = (Vector) vector.elementAt(i2);
            for (int i3 = 0; i3 < vector2.size(); i3++) {
                byte[] bArr = (byte[]) vector2.elementAt(i3);
                if (bArr != null && bArr[0] == 6) {
                    String q = q(bArr, i);
                    short v = (short) v(bArr);
                    if (!bc.dM(q)) {
                        a(v, new char[0], 0, i);
                    } else if (q.startsWith(cd.bVd)) {
                        a(v, q.toCharArray(), 0, i);
                    } else {
                        a(v, bbVar.dD(q).toCharArray(), 0, i);
                    }
                }
            }
        }
    }

    public void a(Vector vector, byte[] bArr, int i, Vector vector2, Vector vector3, int i2) {
        a(vector, bArr, i, vector2, vector3, null, null, i2);
    }

    public void a(Vector vector, byte[] bArr, int i, Vector vector2, Vector vector3, Vector vector4, int i2) {
        if (vector != null && !vector.isEmpty()) {
            int bS = i2 == -1 ? this.dt.bS() : i2;
            int size = vector.size();
            boolean z = i == 0;
            boolean z2 = i == 1;
            boolean z3 = i == 2;
            boolean z4 = i == 3;
            boolean z5 = i == 4;
            boolean z6 = i == 5;
            for (int i3 = 0; i3 < size; i3++) {
                byte[] bArr2 = (byte[]) vector.elementAt(i3);
                if (bArr2 != null) {
                    int t = t(bArr2);
                    if (((!z2 && !z3) || !x(t)) && ((!z2 && !z3) || !q(bArr2) || bArr == bArr2)) {
                        String r = r(bArr2, z ? 1 : 0);
                        if (!bc.by(r) && ((!z || !p(bArr2, bS) || bArr2[0] == 6) && ((!z4 || bArr2[0] == 18) && ((!z5 || bArr2[0] == 9 || bArr2[0] == 7) && (!z6 || bArr2[0] == 26))))) {
                            if (bArr2[0] == 9) {
                                Vector a2 = a((Vector) this.dt.bHd.get(Integer.valueOf(bS)), vector, bArr2, i);
                                int size2 = a2.size();
                                if (z5 && size2 > 1) {
                                    StringBuffer stringBuffer = new StringBuffer();
                                    for (int i4 = 0; i4 < size2; i4++) {
                                        stringBuffer.append(a2.elementAt(i4));
                                        if (i4 < size2 - 1) {
                                            stringBuffer.append(cd.bVG);
                                        }
                                    }
                                    a2.removeAllElements();
                                    a2.addElement(stringBuffer.toString());
                                    size2 = a2.size();
                                }
                                for (int i5 = 0; i5 < size2; i5++) {
                                    vector2.addElement(r);
                                    vector3.addElement(a2.elementAt(i5));
                                    if (vector4 != null) {
                                        vector4.addElement(bArr2);
                                    }
                                }
                            } else {
                                String b2 = b((Vector) this.dt.bHd.get(Integer.valueOf(bS)), bArr2, i);
                                if (b2 != null) {
                                    vector2.addElement(r);
                                    vector3.addElement(b2);
                                    if (vector4 != null) {
                                        vector4.addElement(bArr2);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void a(Vector vector, byte[] bArr, int i, Vector vector2, Vector vector3, String[][] strArr, String[] strArr2, int i2) {
        boolean z;
        Vector vector4;
        Vector vector5;
        if (vector != null && !vector.isEmpty()) {
            int bS = i2 == -1 ? this.dt.bS() : i2;
            int size = vector.size();
            boolean z2 = i == 0;
            boolean z3 = i == 1;
            boolean z4 = i == 2;
            boolean z5 = i == 3;
            boolean z6 = i == 4;
            boolean z7 = i == 5;
            if (i == 6) {
            }
            if (strArr == null || strArr.length < 2 || strArr2 == null) {
                z = false;
                vector4 = null;
                vector5 = null;
            } else {
                z = true;
                Vector vector6 = new Vector();
                vector4 = new Vector();
                vector5 = vector6;
            }
            int i3 = 0;
            while (true) {
                int i4 = i3;
                if (i4 >= size) {
                    break;
                }
                byte[] bArr2 = (byte[]) vector.elementAt(i4);
                if (bArr2 != null) {
                    int t = t(bArr2);
                    if (z && bArr2[0] == 7) {
                        String r = r(bArr2, 0);
                        char[] e = e((Vector) this.dt.bHd.get(Integer.valueOf(bS)), bArr2);
                        if (!(r == null || e == null)) {
                            vector5.add(r);
                            vector4.add(String.valueOf(e));
                        }
                        if (f.b(bArr2, ca.bLr, 2) == 3) {
                            strArr2[0] = r;
                        }
                    }
                    if (((!z3 && !z4) || !x(t)) && ((!z3 && !z4) || !q(bArr2) || bArr == bArr2)) {
                        String r2 = r(bArr2, z2 ? 1 : 0);
                        if (!bc.by(r2) && ((!z2 || !p(bArr2, bS) || bArr2[0] == 6) && ((!z5 || bArr2[0] == 18) && ((!z6 || bArr2[0] == 9 || bArr2[0] == 7) && (!z7 || bArr2[0] == 26))))) {
                            if (bArr2[0] == 9) {
                                Vector a2 = a((Vector) this.dt.bHd.get(Integer.valueOf(bS)), vector, bArr2, i);
                                int size2 = a2.size();
                                if (z6 && size2 > 1) {
                                    StringBuffer stringBuffer = new StringBuffer();
                                    for (int i5 = 0; i5 < size2; i5++) {
                                        stringBuffer.append(a2.elementAt(i5));
                                        if (i5 < size2 - 1) {
                                            stringBuffer.append(cd.bVG);
                                        }
                                    }
                                    a2.removeAllElements();
                                    a2.addElement(stringBuffer.toString());
                                    size2 = a2.size();
                                }
                                for (int i6 = 0; i6 < size2; i6++) {
                                    vector2.addElement(r2);
                                    vector3.addElement(a2.elementAt(i6));
                                }
                            } else {
                                String b2 = b((Vector) this.dt.bHd.get(Integer.valueOf(bS)), bArr2, i);
                                if (b2 != null) {
                                    vector2.addElement(r2);
                                    vector3.addElement(b2);
                                }
                            }
                        }
                    }
                }
                i3 = i4 + 1;
            }
            if (z) {
                strArr[0] = new String[vector5.size()];
                for (int i7 = 0; i7 < vector5.size(); i7++) {
                    strArr[0][i7] = (String) vector5.get(i7);
                }
                strArr[1] = new String[vector4.size()];
                for (int i8 = 0; i8 < vector4.size(); i8++) {
                    strArr[1][i8] = (String) vector4.get(i8);
                }
            }
        }
    }

    public void a(byte[] bArr, byte b2) {
        f.a(bArr, ca.bLn, 3, b2, (byte[]) null);
    }

    public void a(String[] strArr, Vector vector, byte[] bArr, int i) {
        d(vector, bArr, i);
        int length = strArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            strArr[i2] = a(strArr[i2], 1, 0);
        }
    }

    public boolean a(f fVar, int i, int i2) {
        if (fVar == null || i < 0 || i >= fVar.cF) {
            return false;
        }
        if (!ca.iZ(ca.c(fVar, i)[0])) {
            return false;
        }
        if (!this.dt.d(fVar, i) || i2 != 0) {
            Vector b2 = this.dt.b(fVar, i, 1);
            if (b2 == null) {
                return false;
            }
            byte[] bArr = (byte[]) b2.elementAt(0);
            int[] iArr = new int[4];
            this.dt.a(fVar, bArr, iArr);
            iArr[3] = f.e(bArr);
            if (i2 == 0 || i2 == 1) {
                g(0, iArr[1] - this.dt.bGv);
            } else if (i2 == 2) {
                g(0, (iArr[1] + ((iArr[3] - this.dt.bGs) >> 1)) - this.dt.bGv);
            } else if (i2 == 3) {
                g(0, ((iArr[1] + iArr[3]) - this.dt.bGs) - this.dt.bGv);
            }
        }
        ca caVar = this.dt;
        this.dt.bHn = fVar;
        caVar.bHl = fVar;
        ca caVar2 = this.dt;
        this.dt.bHo = i;
        caVar2.bHm = i;
        return true;
    }

    public boolean a(r rVar, byte[] bArr, boolean z, int i) {
        if (bArr == null) {
            return false;
        }
        switch (bArr[0]) {
            case 13:
            case 17:
                c(rVar, bArr, z, i);
                return true;
            case 14:
                b(rVar, bArr, z, i);
                return true;
            case 15:
            case 16:
            default:
                return false;
        }
    }

    public boolean a(byte[] bArr, w wVar, r rVar, int i) {
        String[][] strArr = new String[2][];
        String[] strArr2 = new String[2];
        Object[] a2 = a(bArr, strArr, strArr2, i);
        if (a2 == null) {
            return false;
        }
        Object[] a3 = a(rVar, this.dt, 0, w(((int[]) a2[9])[0]));
        if (a2[0] != null && ((String) a2[0]).contains(b.acI)) {
            a2[0] = ((String) a2[0]).replaceAll(b.acI, "");
        }
        if (a2[0] != null) {
            a2[0] = bc.m((String) a2[0], true);
        }
        if (!ce.fW((String) a2[0])) {
            wVar.f((byte) ca.bGO);
        } else {
            wVar.b(a2, a3, strArr, strArr2);
        }
        return true;
    }

    public byte[] a(f fVar, f fVar2, long j, int[] iArr, f[] fVarArr, int[] iArr2, int i) {
        if (fVarArr == null || fVarArr[0] == null || iArr2 == null) {
            return null;
        }
        return a(fVarArr[0], (iArr2[0] == -1 && i == -1) ? fVarArr[0].cF - 1 : iArr2[0] + i, j, iArr, fVar, fVar2, fVarArr, iArr2, i);
    }

    public byte[] a(f fVar, f fVar2, long j, f[] fVarArr, int[] iArr) {
        return a(fVar, fVar2, j, (int[]) null, fVarArr, iArr, 1);
    }

    public char[] a(Vector vector, int i) {
        char[][] b2 = b(vector, i);
        if (b2[1] != null) {
            return b2[1];
        }
        char[] cArr = new char[b2[0].length];
        System.arraycopy(b2[0], 0, cArr, 0, b2[0].length);
        return cArr;
    }

    public Object[] a(Vector vector, byte[] bArr) {
        return a(vector, bArr, 0);
    }

    public Object[] a(Vector vector, byte[] bArr, int i) {
        return a(vector, bArr, i, (String[][]) null, (String[]) null, this.dt.bS());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:53:0x01bf, code lost:
        if (com.uc.c.bc.dM(r7) != false) goto L_0x01c1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object[] a(java.util.Vector r26, byte[] r27, int r28, java.lang.String[][] r29, java.lang.String[] r30, int r31) {
        /*
            r25 = this;
            if (r26 != 0) goto L_0x0004
            r5 = 0
        L_0x0003:
            return r5
        L_0x0004:
            r14 = 0
            r5 = 0
            r0 = r26
            r1 = r5
            java.lang.Object r5 = r0.elementAt(r1)
            byte[] r5 = (byte[]) r5
            byte[] r5 = (byte[]) r5
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            if (r5 == 0) goto L_0x0220
            byte[] r6 = com.uc.c.ca.bLA
            r7 = 0
            java.lang.String r6 = com.uc.c.f.a(r5, r6, r7)
            byte[] r7 = com.uc.c.ca.bLA
            r8 = 1
            java.lang.String r7 = com.uc.c.f.a(r5, r7, r8)
            byte[] r8 = com.uc.c.ca.bLA
            r9 = 2
            java.lang.String r8 = com.uc.c.f.a(r5, r8, r9)
            byte[] r9 = com.uc.c.ca.bLA
            r10 = 3
            int r9 = com.uc.c.f.b(r5, r9, r10)
            byte[] r10 = com.uc.c.ca.bLA
            r11 = 4
            int r10 = com.uc.c.f.b(r5, r10, r11)
            byte[] r11 = com.uc.c.ca.bLA
            r12 = 5
            java.lang.String r11 = com.uc.c.f.a(r5, r11, r12)
            r20 = r10
            r21 = r9
            r22 = r8
            r23 = r7
            r24 = r6
            r6 = r11
        L_0x004e:
            if (r30 == 0) goto L_0x005a
            r0 = r30
            int r0 = r0.length
            r7 = r0
            r8 = 2
            if (r7 < r8) goto L_0x005a
            r7 = 1
            r30[r7] = r6
        L_0x005a:
            r6 = 0
            r7 = 0
            r15 = 0
            r16 = 0
            java.util.Vector r9 = new java.util.Vector
            r9.<init>()
            java.util.Vector r10 = new java.util.Vector
            r10.<init>()
            r0 = r25
            com.uc.c.ca r0 = r0.dt
            r8 = r0
            boolean r8 = r8.KE()
            if (r8 == 0) goto L_0x0138
            r0 = r25
            com.uc.c.ca r0 = r0.dt
            r8 = r0
            r0 = r8
            r1 = r31
            boolean r8 = r0.jM(r1)
            r18 = r8
        L_0x0082:
            r0 = r25
            com.uc.c.ca r0 = r0.dt
            r8 = r0
            boolean r8 = r8.KE()
            if (r8 == 0) goto L_0x0145
            r0 = r25
            com.uc.c.ca r0 = r0.dt
            r8 = r0
            r0 = r8
            r1 = r31
            boolean r8 = r0.jN(r1)
        L_0x0099:
            r0 = r25
            com.uc.c.ca r0 = r0.dt
            r11 = r0
            boolean r11 = r11.KE()
            if (r11 == 0) goto L_0x0150
            r0 = r25
            com.uc.c.ca r0 = r0.dt
            r11 = r0
            r0 = r11
            r1 = r31
            boolean r11 = r0.jO(r1)
        L_0x00b0:
            if (r18 != 0) goto L_0x00b6
            if (r8 != 0) goto L_0x00b6
            if (r11 == 0) goto L_0x015f
        L_0x00b6:
            if (r18 == 0) goto L_0x015b
            r5 = 3
            r8 = r5
        L_0x00ba:
            r5 = r25
            r6 = r26
            r7 = r27
            r11 = r29
            r12 = r30
            r13 = r31
            r5.a(r6, r7, r8, r9, r10, r11, r12, r13)
            int r5 = r9.size()
            java.lang.String[] r5 = new java.lang.String[r5]
            r9.copyInto(r5)
            int r6 = r10.size()
            java.lang.String[] r6 = new java.lang.String[r6]
            r10.copyInto(r6)
            r17 = r16
            r19 = r14
            r14 = r5
            r16 = r15
            r15 = r6
        L_0x00e3:
            r9.removeAllElements()
            r10.removeAllElements()
            if (r18 != 0) goto L_0x00f6
            r0 = r25
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            boolean r5 = r5.Lz()
            if (r5 == 0) goto L_0x0102
        L_0x00f6:
            r0 = r25
            r1 = r15
            r2 = r26
            r3 = r27
            r4 = r31
            r0.a(r1, r2, r3, r4)
        L_0x0102:
            r0 = r25
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            boolean r5 = r5.Lu()
            if (r5 != 0) goto L_0x0118
            r0 = r25
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            boolean r5 = r5.Ly()
            if (r5 == 0) goto L_0x0212
        L_0x0118:
            r5 = 0
            r12 = r5
        L_0x011a:
            r10 = 0
            r11 = 0
            r0 = r25
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            r0 = r5
            java.lang.String r0 = r0.bFA
            r18 = r0
            r5 = r24
            r6 = r20
            r7 = r22
            r8 = r21
            r9 = r23
            r13 = r28
            java.lang.Object[] r5 = a(r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19)
            goto L_0x0003
        L_0x0138:
            r0 = r25
            com.uc.c.ca r0 = r0.dt
            r8 = r0
            boolean r8 = r8.LB()
            r18 = r8
            goto L_0x0082
        L_0x0145:
            r0 = r25
            com.uc.c.ca r0 = r0.dt
            r8 = r0
            boolean r8 = r8.LA()
            goto L_0x0099
        L_0x0150:
            r0 = r25
            com.uc.c.ca r0 = r0.dt
            r11 = r0
            boolean r11 = r11.Lu()
            goto L_0x00b0
        L_0x015b:
            r5 = 2
            r8 = r5
            goto L_0x00ba
        L_0x015f:
            r0 = r25
            com.uc.c.ca r0 = r0.dt
            r8 = r0
            boolean r8 = r8.Lv()
            if (r8 == 0) goto L_0x0216
            boolean r6 = com.uc.c.bc.dM(r24)
            if (r6 == 0) goto L_0x018f
            java.lang.String r6 = "http://command/command=lookfileList"
            r0 = r24
            r1 = r6
            boolean r6 = r0.startsWith(r1)
            if (r6 != 0) goto L_0x018f
            com.uc.c.bb r6 = new com.uc.c.bb
            com.uc.c.w r7 = com.uc.c.w.ns()
            r0 = r25
            com.uc.c.ca r0 = r0.dt
            r8 = r0
            r0 = r6
            r1 = r7
            r2 = r8
            r3 = r31
            r0.<init>(r1, r2, r3)
            r14 = r6
        L_0x018f:
            r0 = r25
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            java.util.HashMap r6 = r6.bHf
            java.lang.Integer r7 = java.lang.Integer.valueOf(r31)
            java.lang.Object r6 = r6.get(r7)
            java.util.Vector r6 = (java.util.Vector) r6
            if (r27 == 0) goto L_0x01cf
            r0 = r25
            com.uc.c.ca r0 = r0.dt
            r7 = r0
            boolean r7 = r7.Lz()
            if (r7 != 0) goto L_0x01cf
            java.lang.String r7 = s(r27)
            boolean r8 = com.uc.c.bc.dM(r7)
            if (r8 != 0) goto L_0x01c1
            java.lang.String r7 = r(r27)
            boolean r8 = com.uc.c.bc.dM(r7)
            if (r8 == 0) goto L_0x01cf
        L_0x01c1:
            java.lang.String r8 = "clientAction"
            r9.addElement(r8)
            java.lang.String r8 = ".click"
            java.lang.String r7 = com.uc.c.bc.Z(r7, r8)
            r10.addElement(r7)
        L_0x01cf:
            r0 = r25
            r1 = r6
            r2 = r27
            java.lang.String[][] r6 = r0.b(r1, r2)
            r7 = 0
            r15 = r6[r7]
            r7 = 1
            r16 = r6[r7]
            if (r5 == 0) goto L_0x01f0
            r8 = 1
            r5 = r25
            r6 = r26
            r7 = r27
            r11 = r29
            r12 = r30
            r13 = r31
            r5.a(r6, r7, r8, r9, r10, r11, r12, r13)
        L_0x01f0:
            int r5 = r9.size()
            java.lang.String[] r5 = new java.lang.String[r5]
            r9.copyInto(r5)
            int r6 = r10.size()
            java.lang.String[] r6 = new java.lang.String[r6]
            r10.copyInto(r6)
            r9.removeAllElements()
            r10.removeAllElements()
            r17 = r16
            r19 = r14
            r14 = r5
            r16 = r15
            r15 = r6
            goto L_0x00e3
        L_0x0212:
            r5 = 1
            r12 = r5
            goto L_0x011a
        L_0x0216:
            r17 = r16
            r19 = r14
            r14 = r6
            r16 = r15
            r15 = r7
            goto L_0x00e3
        L_0x0220:
            r20 = r10
            r21 = r9
            r22 = r8
            r23 = r7
            r24 = r6
            r6 = r11
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.i.a(java.util.Vector, byte[], int, java.lang.String[][], java.lang.String[], int):java.lang.Object[]");
    }

    public Object[] a(byte[] bArr, int i, String[][] strArr, String[] strArr2, int i2) {
        int i3;
        String[][] strArr3;
        int bS = i2 == -1 ? this.dt.bS() : i2;
        Vector n = n(bArr, bS);
        if (n != null) {
            return a(n, bArr, i, strArr, strArr2, bS);
        }
        if (bArr[0] != 14) {
            return null;
        }
        String a2 = f.a(bArr, ca.bLn, 0);
        String a3 = f.a(bArr, ca.bLn, 1);
        String[][] strArr4 = new String[2][];
        if (this.dt.Lu()) {
            i3 = 0;
            strArr3 = strArr4;
        } else if (this.dt.Ly()) {
            i3 = 0;
            strArr3 = b((Vector) this.dt.bHf.get(Integer.valueOf(this.dt.bS())), bArr);
        } else {
            i3 = 1;
            strArr3 = strArr4;
        }
        if (this.dt.Lw() || this.dt.Lz()) {
            a2 = a(a2, bArr, bS);
        }
        return a(a2, 0, null, -1, null, a3, null, i3, i, null, null, strArr3[0], strArr3[1], this.dt.bFA);
    }

    public Object[] a(byte[] bArr, String[][] strArr, String[] strArr2, int i) {
        return a(bArr, 0, strArr, strArr2, i);
    }

    public int b(r rVar, byte[] bArr, boolean z, int i) {
        String KQ = this.dt.KQ();
        if (KQ != null && KQ.contains("ext:startpage")) {
            KQ = KQ.trim();
            if (KQ.startsWith("ext:startpage")) {
                return 2;
            }
        }
        a(bArr, (byte) 1);
        if (!ce.fW(KQ)) {
            return 2;
        }
        if (KQ != null && KQ.contains(w.Ou)) {
            this.dt.ap();
        }
        if (z || rVar == null || rVar.ch() == null) {
            a(bArr, w.ns(), rVar, i == -1 ? this.dt.bS() : i);
        } else if (w.ns().JS == w.ns().lA()) {
            if (this.dt.Lw() && KQ != null && !bc.eh(KQ)) {
                KQ = "wap:" + KQ;
            }
            this.p.shouldOverrideUrlLoading(KQ);
            return 1;
        } else if (this.dt.iJ(14) || w.OX.equals(this.dt.bFA)) {
            if (KQ != null) {
                this.p.shouldOverrideUrlLoading(KQ);
            }
            return 1;
        } else {
            String lowerCase = KQ != null ? KQ.trim().toLowerCase() : KQ;
            if (lowerCase != null && lowerCase.startsWith(w.Qj)) {
                String substring = lowerCase.substring(19, lowerCase.length());
                String[] j = ce.j(substring);
                if (bc.by(substring) || (j[0].indexOf("uc.cn") == -1 && j[0].indexOf("ucweb.com") == -1 && j[0].indexOf("uc123.com") == -1)) {
                    return 2;
                }
            }
            if (lowerCase != null && lowerCase.contains("ext:wo:")) {
                if (true == this.p.bM(lowerCase.replace("ext:wo:", ""))) {
                    return 0;
                }
            }
            if (this.dt.Lw() && !bc.eh(KQ) && this.dt.bFA != null) {
                KQ = "wap:" + KQ;
            }
            rVar.ch().shouldOverrideUrlLoading(KQ + b.act);
        }
        return 1;
    }

    public int b(byte[] bArr, byte[] bArr2) {
        int bS = this.dt.bS();
        Vector vector = (Vector) this.dt.bGf.get(Integer.valueOf(bS));
        int size = vector.size();
        Vector vector2 = (Vector) this.dt.bGh.get(Integer.valueOf(bS));
        for (int i = 0; i < size; i++) {
            if (bc.g(bArr, (byte[]) vector.elementAt(i))) {
                if (vector2.elementAt(i) == null) {
                    vector2.setElementAt(bArr2, i);
                }
                return i;
            }
        }
        return -1;
    }

    public String b(Vector vector, byte[] bArr, int i) {
        e(vector, bArr);
        String str = null;
        switch (bArr[0]) {
            case 4:
            case 5:
                char[] e = e(vector, bArr);
                if (i == 0) {
                    return e[0] == '1' ? "check" : "uncheck";
                }
                if (e[0] == '1') {
                    str = f.a(bArr, ca.bLu, 0);
                }
                return (i == 6 && bArr[0] == 4) ? bc.dN(str) : str;
            case 6:
            case 13:
            case 17:
            case 18:
                if (i != 0 && i != 1) {
                    return new String(e(vector, bArr));
                }
                char[] c = c(vector, bArr, 0);
                if (c != null) {
                    return new String(c);
                }
                return null;
            case 7:
            case 26:
                return new String(e(vector, bArr));
            default:
                return null;
        }
    }

    public void b(int i, int i2, int i3) {
    }

    public void b(int i, byte[] bArr) {
        Vector vector;
        if (this.dt.bGf != null && (vector = (Vector) this.dt.bGf.get(Integer.valueOf(this.dt.bS()))) != null && i >= 0 && i < vector.size()) {
            vector.setElementAt(bArr, i);
        }
    }

    public void b(n nVar) {
        this.p = nVar;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(int, int):int}
     arg types: [int, short]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(int, int):int} */
    public boolean b(int i, int i2, int[] iArr) {
        boolean z;
        byte[] a2;
        int i3;
        boolean z2;
        Object[] objArr;
        boolean z3;
        int i4;
        int i5;
        boolean b2 = bc.b(this.dt.bGK, this.dt.bGL, this.dt.bGw, this.dt.bGx, this.dt.jk(this.dt.bGr), this.dt.jk(this.dt.bGs));
        if (!b2) {
            this.dt.bGK = this.dt.bGw + (this.dt.jk(this.dt.bGr) >>> 1);
            this.dt.bGL = this.dt.bGx + (this.dt.jk(this.dt.bGs) >>> 1);
        }
        int i6 = this.dt.bGK;
        int i7 = this.dt.bGL;
        f[] fVarArr = new f[1];
        int[] iArr2 = new int[1];
        int[] iArr3 = new int[2];
        try {
            int i8 = this.dt.bGr >>> 4;
            int i9 = this.dt.bGs >>> 4;
            int i10 = this.dt.bGr >>> 3;
            int i11 = this.dt.bGs >>> 3;
            int i12 = i8 << 1;
            int i13 = i9 << 1;
            this.dt.bGR = -1;
            if (i2 != 1) {
                int[] iArr4 = new int[4];
                Vector vector = new Vector(10);
                if (i2 == 0) {
                    switch (i) {
                        case 1:
                        case 6:
                            iArr4[0] = this.dt.bGK;
                            iArr4[1] = this.dt.bGL + (i == 1 ? -i13 : 0);
                            iArr4[2] = 2;
                            iArr4[3] = i13;
                            break;
                        case 2:
                        case 5:
                            iArr4[0] = this.dt.bGK - (i == 2 ? i12 : 0);
                            iArr4[1] = this.dt.bGL;
                            iArr4[2] = i12;
                            iArr4[3] = 2;
                            break;
                    }
                } else if (i2 == 2) {
                    iArr4[0] = this.dt.bGK;
                    iArr4[1] = this.dt.bGL;
                    iArr4[2] = 1;
                    iArr4[3] = 1;
                }
                fVarArr[0] = this.dt.bFR;
                iArr2[0] = -1;
                do {
                    a2 = a(this.dt.bFR, (f) null, (long) ca.bKR, iArr4, fVarArr, iArr2, 1);
                    if (a2 != null) {
                        Vector b3 = this.dt.b(fVarArr[0], iArr2[0], 1);
                        for (int i14 = 0; i14 < b3.size(); i14++) {
                            vector.addElement(new Object[]{b3.elementAt(i14), fVarArr[0], new Integer(iArr2[0])});
                        }
                        continue;
                    }
                } while (a2 != null);
                int size = vector.size();
                if (i2 == 2) {
                    if (size > 0) {
                        Object[] objArr2 = (Object[]) vector.elementAt(size - 1);
                        this.dt.bHl = (f) objArr2[1];
                        this.dt.bHm = ((Integer) objArr2[2]).intValue();
                        this.dt.bGS = 101;
                    } else {
                        this.dt.bHl = null;
                        this.dt.bHm = -1;
                        this.dt.bGS = 100;
                    }
                    i3 = -1;
                    z2 = true;
                } else if (i2 == 0) {
                    int i15 = Integer.MAX_VALUE;
                    boolean z4 = false;
                    int i16 = size - 1;
                    int i17 = -1;
                    while (true) {
                        if (i16 >= 0) {
                            objArr = (Object[]) vector.elementAt(i16);
                            this.dt.a((f) objArr[1], (byte[]) objArr[0], iArr3);
                            int i18 = iArr3[0];
                            int i19 = iArr3[1];
                            int d = f.d((byte[]) objArr[0]);
                            int e = f.e((byte[]) objArr[0]);
                            if (!bc.b(i6, i7, i18, i19, d, e)) {
                                switch (i) {
                                    case 1:
                                        int i20 = this.dt.bGJ - i19;
                                        if (i20 < i15) {
                                            i4 = i20;
                                            z3 = z4;
                                            i5 = i16;
                                            break;
                                        }
                                        i5 = i17;
                                        i4 = i15;
                                        z3 = z4;
                                        break;
                                    case 2:
                                        int i21 = this.dt.bGI - (i18 + d);
                                        if (i21 < i15) {
                                            i4 = i21;
                                            z3 = z4;
                                            i5 = i16;
                                            break;
                                        }
                                        i5 = i17;
                                        i4 = i15;
                                        z3 = z4;
                                        break;
                                    case 3:
                                    case 4:
                                    default:
                                        i5 = i17;
                                        i4 = i15;
                                        z3 = z4;
                                        break;
                                    case 5:
                                        int i22 = this.dt.bGI - i18;
                                        if (i22 < i15) {
                                            i4 = i22;
                                            z3 = z4;
                                            i5 = i16;
                                            break;
                                        }
                                        i5 = i17;
                                        i4 = i15;
                                        z3 = z4;
                                        break;
                                    case 6:
                                        int i23 = i19 - this.dt.bGJ;
                                        if (i23 < i15) {
                                            i4 = i23;
                                            z3 = z4;
                                            i5 = i16;
                                            break;
                                        }
                                        i5 = i17;
                                        i4 = i15;
                                        z3 = z4;
                                        break;
                                }
                            } else if (this.dt.bHl == null || !this.dt.bHl.equals(objArr[1]) || this.dt.bHm != ((Integer) objArr[2]).intValue()) {
                                this.dt.bHl = (f) objArr[1];
                                this.dt.bHm = ((Integer) objArr[2]).intValue();
                                this.dt.bGS = 101;
                                i3 = i16;
                                z2 = z4;
                            } else {
                                boolean z5 = false;
                                switch (i) {
                                    case 1:
                                        int i24 = this.dt.bGL - i9;
                                        if (i24 >= i19) {
                                            this.dt.bGL = i24;
                                            z5 = true;
                                            break;
                                        }
                                        break;
                                    case 2:
                                        int i25 = this.dt.bGK - i8;
                                        if (i25 >= i18) {
                                            this.dt.bGK = i25;
                                            z5 = true;
                                            break;
                                        }
                                        break;
                                    case 5:
                                        int i26 = this.dt.bGK + i8;
                                        if (i26 <= i18 + d) {
                                            this.dt.bGK = i26;
                                            z5 = true;
                                            break;
                                        }
                                        break;
                                    case 6:
                                        int i27 = this.dt.bGL + i9;
                                        if (i27 <= i19 + e) {
                                            this.dt.bGL = i27;
                                            z5 = true;
                                            break;
                                        }
                                        break;
                                }
                                if (z5) {
                                    this.dt.bGS = 101;
                                    i3 = i17;
                                    z2 = z5;
                                } else {
                                    i4 = i15;
                                    z3 = z5;
                                    i5 = i17;
                                }
                            }
                            i15 = i4;
                            z4 = z3;
                            i16--;
                            i17 = i5;
                        } else {
                            i3 = i17;
                            z2 = z4;
                        }
                    }
                    this.dt.bHl = (f) objArr[1];
                    this.dt.bHm = ((Integer) objArr[2]).intValue();
                    this.dt.bGS = 101;
                    i3 = i16;
                    z2 = z4;
                } else {
                    i3 = -1;
                    z2 = false;
                }
                if (!z2 && i3 != -1 && i3 < vector.size()) {
                    Object[] objArr3 = (Object[]) vector.elementAt(i3);
                    this.dt.a((f) objArr3[1], (byte[]) objArr3[0], iArr3);
                    int i28 = iArr3[0];
                    int i29 = iArr3[1];
                    int d2 = f.d((byte[]) objArr3[0]);
                    int e2 = f.e((byte[]) objArr3[0]);
                    this.dt.bHl = (f) objArr3[1];
                    this.dt.bHm = ((Integer) objArr3[2]).intValue();
                    this.dt.bGS = 101;
                    switch (i) {
                        case 1:
                            int i30 = this.dt.bGK;
                            int i31 = iArr4[1];
                            if (!bc.b(i30, i31, i28, i29, d2, e2)) {
                                this.dt.bGL = i29 + 2;
                                if (i28 > this.dt.bGK) {
                                    this.dt.bGK = i28 + 2;
                                }
                                if (i28 + d2 < this.dt.bGK) {
                                    this.dt.bGK = (i28 + d2) - 2;
                                    break;
                                }
                            } else {
                                this.dt.bGL = i31;
                                break;
                            }
                            break;
                        case 2:
                            int i32 = iArr4[0];
                            if (!bc.b(i32, this.dt.bGL, i28, i29, d2, e2)) {
                                this.dt.bGK = i28 + 2;
                                if (i29 > this.dt.bGL) {
                                    this.dt.bGL = i29;
                                }
                                if (i29 + e2 < this.dt.bGL) {
                                    this.dt.bGL = i29 + e2;
                                    break;
                                }
                            } else {
                                this.dt.bGK = i32;
                                break;
                            }
                            break;
                        case 5:
                            int i33 = iArr4[0] + iArr4[2];
                            if (!bc.b(i33, this.dt.bGL, i28, i29, d2, e2)) {
                                this.dt.bGK = (i28 + d2) - 2;
                                if (i29 > this.dt.bGL) {
                                    this.dt.bGL = i29;
                                }
                                if (i29 + e2 < this.dt.bGL) {
                                    this.dt.bGL = i29 + e2;
                                    break;
                                }
                            } else {
                                this.dt.bGK = i33;
                                break;
                            }
                            break;
                        case 6:
                            int i34 = this.dt.bGK;
                            int i35 = iArr4[1] + iArr4[3];
                            if (!bc.b(i34, i35, i28, i29, d2, e2)) {
                                this.dt.bGL = (i29 + e2) - 3;
                                if (i28 > this.dt.bGK) {
                                    this.dt.bGK = i28 + 2;
                                }
                                if (i28 + d2 < this.dt.bGK) {
                                    this.dt.bGK = (i28 + d2) - 2;
                                    break;
                                }
                            } else {
                                this.dt.bGL = i35;
                                break;
                            }
                            break;
                    }
                }
                z = z2 || i3 != -1;
                vector.removeAllElements();
            } else {
                z = false;
            }
            if (i2 != 2 && (!z || ((this.dt.bGK == i6 && this.dt.bGL == i7) || ((i == 1 && this.dt.bGL >= i7) || ((i == 6 && this.dt.bGL <= i7) || ((i == 2 && this.dt.bGK >= i6) || (i == 5 && this.dt.bGK <= i6))))))) {
                this.dt.bHl = null;
                this.dt.bHm = -1;
                this.dt.bGS = 100;
                if (i == 2) {
                    this.dt.bGK = Math.min(this.dt.bGK, i6);
                    this.dt.bGK -= i8;
                } else if (i == 5) {
                    this.dt.bGK = Math.max(this.dt.bGK, i6);
                    this.dt.bGK += i8;
                } else if (i == 1) {
                    this.dt.bGL = Math.min(this.dt.bGL, i7);
                    this.dt.bGL -= i9;
                } else if (i == 6) {
                    this.dt.bGL = Math.max(this.dt.bGL, i7);
                    this.dt.bGL += i9;
                }
            }
            int i36 = this.dt.bGK;
            int i37 = this.dt.bGm - 4;
            if (i36 > i37) {
                this.dt.bGK = i37;
            } else if (this.dt.bGK < 0) {
                this.dt.bGK = 0;
            }
            int i38 = this.dt.bGL;
            int max = Math.max(this.dt.ahU, (int) this.dt.bGs) - 4;
            if (i38 > max) {
                this.dt.bGL = max;
            } else if (this.dt.bGL < 0) {
                this.dt.bGL = 0;
            }
            switch (i) {
                case 1:
                    if (this.dt.bGL < this.dt.bGx || (this.dt.bGL > i11 && this.dt.bGL < this.dt.bGx + i11)) {
                        g(0, this.dt.bGL - i7);
                        break;
                    }
                case 2:
                    if (this.dt.bGK < this.dt.bGw || (this.dt.bGK > i10 && this.dt.bGK < this.dt.bGw + i10)) {
                        g(this.dt.bGK - i6, 0);
                        break;
                    }
                case 5:
                    if (this.dt.bGK >= this.dt.bGw + this.dt.bGr || (this.dt.bGK < Math.max(this.dt.bGm, (int) this.dt.bGr) - i10 && this.dt.bGK > (this.dt.bGw + this.dt.bGr) - i10)) {
                        g(this.dt.bGK - i6, 0);
                        break;
                    }
                case 6:
                    if (this.dt.bGL >= this.dt.bGx + this.dt.bGs || (this.dt.bGL < Math.max(this.dt.ahU, (int) this.dt.bGs) - i11 && this.dt.bGL > (this.dt.bGx + this.dt.bGs) - i11)) {
                        g(0, this.dt.bGL - i7);
                        break;
                    }
            }
            if (i2 != 1 && this.dt.bFM == 1) {
                int i39 = this.dt.bGu;
                int i40 = this.dt.bGv;
                int i41 = this.dt.bGr;
                int i42 = this.dt.bGs;
                fVarArr[0] = this.dt.bFR;
                iArr2[0] = -1;
                int[] iArr5 = {i39, i40, i41, i42};
                while (true) {
                    byte[] a3 = a(this.dt.bFR, (f) null, 8388608, iArr5, fVarArr, iArr2, -1);
                    if (a3 != null) {
                        this.dt.a(fVarArr[0], a3, iArr3);
                        int i43 = iArr3[0];
                        int i44 = iArr3[1];
                        int d3 = f.d(a3);
                        if (bc.b(this.dt.bGK, this.dt.bGL, i43, i44, d3, f.e(a3))) {
                            if ((i == 1 || i == 6 || i == 2) && this.dt.bGK <= (i43 + d3) - (d3 >> 2) && !bc.b(i43, i44, i39, i40, i41, i42)) {
                                f(i43, this.dt.bGv);
                            }
                            if ((i == 1 || i == 6 || i == 5) && this.dt.bGK >= (d3 >> 2) + i43 && !bc.b(i43 + d3, i44, i39, i40, i41, i42)) {
                                f(((i43 + d3) - i41) + 1, this.dt.bGv);
                            }
                        }
                    }
                    if (a3 == null) {
                    }
                }
            }
        } catch (Exception e3) {
        }
        boolean z6 = (b2 && i6 == this.dt.bGK && i7 == this.dt.bGL) ? false : true;
        if (z6) {
            return z6;
        }
        if (this.dt.bGL == 0 && i == 1 && i2 == 1 && bc.aL(iArr[0], 1)) {
            return false;
        }
        if (this.dt.bGL == 0 && bc.aL(iArr[0], 1) && i == 1) {
            this.dt.bHl = null;
            this.dt.bHm = -1;
            this.dt.bGS = ca.bGP;
            iArr[0] = bc.aK(iArr[0], 6);
            iArr[0] = bc.aJ(iArr[0], 1);
            return true;
        }
        iArr[0] = 0;
        return z6;
    }

    public byte[] b(f fVar, f fVar2, long j, int[] iArr, f[] fVarArr, int[] iArr2, int i) {
        if (fVarArr == null || fVarArr[0] == null || iArr2 == null) {
            return null;
        }
        return b(fVarArr[0], (iArr2[0] == -1 && i == -1) ? fVarArr[0].cF - 1 : iArr2[0] + i, j, iArr, fVar, fVar2, fVarArr, iArr2, i);
    }

    public char[][] b(Vector vector, int i) {
        return (char[][]) vector.elementAt(i);
    }

    public String[][] b(Vector vector, byte[] bArr) {
        int size = vector.size();
        Vector vector2 = new Vector();
        Vector vector3 = new Vector();
        for (int i = 0; i < size; i++) {
            a((Vector) vector.elementAt(i), bArr, 0, vector2, vector3, this.dt.bS());
        }
        String[] strArr = new String[vector2.size()];
        String[] strArr2 = new String[vector3.size()];
        vector2.copyInto(strArr);
        vector3.copyInto(strArr2);
        return new String[][]{strArr, strArr2};
    }

    public int c(r rVar, byte[] bArr, boolean z, int i) {
        if (n(bArr)) {
            if (!(!a(bArr, w.ns(), rVar, i) || rVar == null || rVar.ch() == null)) {
                rVar.ch().EF();
            }
            if (!(rVar == null || rVar.ch() == null)) {
                rVar.ch().ED();
            }
            return 0;
        } else if (p(bArr)) {
            d(bArr, 0, i);
            return 2;
        } else {
            if (o(bArr)) {
                h(bArr);
            }
            return 0;
        }
    }

    public int c(Vector vector, byte[] bArr) {
        if (vector == null || vector.isEmpty()) {
            return -1;
        }
        for (int size = vector.size() - 1; size >= 0; size--) {
            if (((byte[]) vector.elementAt(size)) == bArr) {
                return size;
            }
        }
        return -1;
    }

    public int c(byte[] bArr, byte[] bArr2) {
        int bS = this.dt.bS();
        Vector vector = (Vector) this.dt.bGf.get(Integer.valueOf(bS));
        int b2 = b(bArr, bArr2);
        if (b2 != -1) {
            return b2;
        }
        vector.addElement(bArr);
        ((Vector) this.dt.bGh.get(Integer.valueOf(bS))).addElement(bArr2);
        return vector.size() - 1;
    }

    public char[] c(Vector vector, byte[] bArr, int i) {
        char[][] b2 = b(vector, (short) v(bArr));
        return i == 1 ? b2[1] == null ? b2[0] : b2[1] : b2[0];
    }

    public final void cc() {
        if (this.dt.ahU > this.dt.bGs) {
            if (this.dt.bGx >= ((this.dt.ahU - this.dt.bGs) >> 1)) {
                this.dt.g(0, 0 - this.dt.bGv);
            } else {
                this.dt.g(0, (this.dt.ahU - this.dt.bGs) - this.dt.bGv);
            }
        }
    }

    public void cd() {
        Vector vector = (Vector) this.dt.bHf.get(Integer.valueOf(this.dt.bS()));
        int size = vector.size();
        for (int i = 0; i < size; i++) {
            Vector vector2 = (Vector) vector.elementAt(i);
            if (vector2.size() > 1) {
                d((byte[]) vector2.elementAt(1), 1, -1);
            }
        }
    }

    public String[][] ce() {
        Vector vector = (Vector) this.dt.bHf.get(Integer.valueOf(this.dt.bS()));
        Vector vector2 = new Vector();
        Vector vector3 = new Vector();
        int size = vector.size();
        for (int i = 0; i < size; i++) {
            a((Vector) vector.elementAt(i), (byte[]) null, 6, vector2, vector3, this.dt.bS());
        }
        String[][] strArr = {new String[vector2.size()], new String[vector3.size()]};
        vector2.copyInto(strArr[0]);
        vector3.copyInto(strArr[1]);
        return strArr;
    }

    public void cf() {
    }

    public void cg() {
        if (this.dt.bGY <= 0 && this.dt.bHR != -1) {
            f[] fVarArr = {this.dt.bHS};
            int[] iArr = {this.dt.bHT};
            byte[] a2 = a(this.dt.bFR, (f) null, (long) ca.bKR, (int[]) null, fVarArr, iArr, 1);
            this.dt.bHU = fVarArr[0];
            this.dt.bHV = iArr[0];
            if (a2 == null) {
                return;
            }
            if (this.dt.bHR != 0) {
                a(fVarArr[0], iArr[0], this.dt.bHR);
            } else if (this.dt.d(fVarArr[0], iArr[0])) {
                ca caVar = this.dt;
                ca caVar2 = this.dt;
                f fVar = fVarArr[0];
                caVar2.bHn = fVar;
                caVar.bHl = fVar;
                ca caVar3 = this.dt;
                ca caVar4 = this.dt;
                int i = iArr[0];
                caVar4.bHo = i;
                caVar3.bHm = i;
            }
        }
    }

    public n ch() {
        return this.p;
    }

    public String d(Vector vector, byte[] bArr) {
        return b(vector, bArr, 2);
    }

    public void d(int i, int i2) {
    }

    public void d(Vector vector, byte[] bArr, int i) {
        Vector vector2 = (Vector) this.dt.bHf.get(Integer.valueOf(i));
        Vector vector3 = new Vector();
        Vector vector4 = new Vector();
        Vector vector5 = new Vector();
        int size = vector2.size();
        a(vector, bArr, 5, vector3, vector4, i);
        for (int i2 = 0; i2 < size; i2++) {
            Vector vector6 = (Vector) vector2.elementAt(i2);
            if (vector != vector6) {
                a(vector6, bArr, 4, vector3, vector4, vector5, i);
            }
        }
        int size2 = vector3.size();
        for (int i3 = 0; i3 < size2; i3++) {
            byte[] bArr2 = null;
            if (i3 < vector5.size()) {
                bArr2 = (byte[]) vector5.elementAt(i3);
            }
            String str = (String) vector3.elementAt(i3);
            String str2 = (String) vector4.elementAt(i3);
            ca.bHh.put(str, (bArr2 == null || bArr2[0] != 7) ? a(str2, 1, 0) : str2);
        }
        vector3.removeAllElements();
        vector4.removeAllElements();
    }

    public boolean d(byte[] bArr, int i, int i2) {
        int bS = i2 == -1 ? this.dt.bS() : i2;
        Vector n = n(bArr, bS);
        int size = n.size();
        int size2 = ((Vector) this.dt.bHd.get(Integer.valueOf(bS))).size();
        boolean z = i == 0;
        boolean z2 = i == 1;
        for (int i3 = 1; i3 < size; i3++) {
            byte[] bArr2 = (byte[]) n.elementAt(i3);
            short v = (short) v(bArr2);
            if (v >= 0 && v < size2) {
                char[][] b2 = b((Vector) this.dt.bHd.get(Integer.valueOf(bS)), v);
                if (z) {
                    b2[1] = null;
                } else if (z2 && bArr2[0] == 7 && f.b(bArr2, ca.bLr, 2) == 3) {
                    b2[1] = null;
                }
            }
        }
        return true;
    }

    public boolean e(int i, int i2) {
        int bm = e.bm(this.dt.bFQ);
        return g((this.dt.bGr - bm) * i, (this.dt.bGs - bm) * i2);
    }

    public char[] e(Vector vector, byte[] bArr) {
        return c(vector, bArr, 1);
    }

    public boolean f(int i, int i2) {
        return g(i - this.dt.bGu, i2 - this.dt.bGv);
    }

    public char[][] f(Vector vector, byte[] bArr) {
        return b(vector, (short) v(bArr));
    }

    public int g(byte[] bArr) {
        if (u(bArr) || i(bArr)) {
            return 0;
        }
        int b2 = f.b(bArr, ca.bLB, 1);
        int b3 = f.b(bArr, ca.bLB, 3);
        String a2 = f.a(bArr, ca.bLB, 4);
        boolean aL = bc.aL(f.b(bArr, ca.bLB, 0), 8);
        if (ch() != null) {
            ch().w(new Object[]{Integer.valueOf(b2), Integer.valueOf(b3), a2, Boolean.valueOf(aL)});
        }
        return 3;
    }

    /* access modifiers changed from: package-private */
    public boolean g(int i, int i2) {
        int i3 = this.dt.bGu;
        int i4 = this.dt.bGv;
        if (!this.dt.ac()) {
            if (i != 0) {
                this.dt.bGu += i;
                int i5 = this.dt.bGm - this.dt.bGr;
                if (this.dt.bGu > i5) {
                    this.dt.bGu = i5;
                }
                if (this.dt.bGu < 0) {
                    this.dt.bGu = 0;
                }
            }
            if (i2 != 0) {
                this.dt.bGv += i2;
                int i6 = this.dt.ahU - this.dt.bGs;
                if (this.dt.bGv > i6) {
                    this.dt.bGv = i6;
                }
                if (this.dt.bGv < 0) {
                    this.dt.bGv = 0;
                }
            }
        } else {
            if (i != 0 && this.dt.ac() && !this.dt.LN()) {
                int abs = Math.abs(i);
                int i7 = i > 0 ? 1 : -1;
                ca caVar = this.dt;
                caVar.bGu = (this.dt.jk(abs) * i7) + caVar.bGu;
                int jk = this.dt.bGm - this.dt.jk(this.dt.bGr);
                if (this.dt.bGu > jk) {
                    this.dt.bGu = jk;
                }
                if (this.dt.bGu < 0) {
                    this.dt.bGu = 0;
                }
            }
            if (i2 != 0) {
                int abs2 = Math.abs(i2);
                int i8 = i2 > 0 ? 1 : -1;
                ca caVar2 = this.dt;
                caVar2.bGv = (this.dt.jk(abs2) * i8) + caVar2.bGv;
                int jk2 = this.dt.ac() ? this.dt.ahU - this.dt.jk(this.dt.bGs) : this.dt.ahU - this.dt.bGs;
                if (this.dt.bGv > jk2) {
                    this.dt.bGv = jk2;
                }
                if (this.dt.bGv < 0) {
                    this.dt.bGv = 0;
                }
            }
        }
        return (i3 == this.dt.bGu && i4 == this.dt.bGv) ? false : true;
    }

    public boolean h(int i, int i2) {
        return true;
    }

    public int i(byte[] bArr, int i) {
        if (u(bArr)) {
            return 1;
        }
        short v = (short) v(bArr);
        char[] a2 = a((Vector) this.dt.bHd.get(Integer.valueOf(i)), v);
        a2[0] = a2[0] == '1' ? ca.bNQ : '1';
        a(v, a2, i);
        i(bArr);
        return 1;
    }

    public boolean i(int i, int i2) {
        return true;
    }

    public boolean i(byte[] bArr) {
        if (m(bArr)) {
            return a(bArr, w.ns(), w.ns().lA(), -1);
        }
        return false;
    }

    public int j(byte[] bArr, int i) {
        if (u(bArr)) {
            return 0;
        }
        short v = (short) v(bArr);
        char[] a2 = a((Vector) this.dt.bHd.get(Integer.valueOf(i)), v);
        if (a2[0] == '1') {
            return 0;
        }
        a2[0] = a2[0] == '1' ? '0' : '1';
        a(v, a2, i);
        String r = r(bArr);
        Vector n = n(bArr, i);
        int size = n.size();
        for (int i2 = 1; i2 < size; i2++) {
            byte[] bArr2 = (byte[]) n.elementAt(i2);
            if (bArr2[0] == 5 && bArr2 != bArr && r.equals(r(bArr2))) {
                a((short) v(bArr2), new char[]{ca.bNQ}, i);
            }
        }
        i(bArr);
        return 2;
    }

    public Vector j(int i, int i2) {
        if (this.dt.bHf == null) {
            return null;
        }
        Vector vector = (Vector) this.dt.bHf.get(Integer.valueOf(i2 == -1 ? this.dt.bS() : i2));
        if (i < 0 || i >= vector.size()) {
            return null;
        }
        return (Vector) vector.elementAt(i);
    }

    public int k(byte[] bArr) {
        try {
            int b2 = f.b(bArr, ca.bLF, 1);
            Plugin jn = this.dt.jn(b2);
            if (jn == null) {
                return 0;
            }
            ag Qh = jn.Qh();
            if (Qh == null) {
                return 0;
            }
            Qh.requestFocus();
            this.dt.jn(b2).d("PRESS", null, null);
            return 2;
        } catch (Exception e) {
            return 0;
        }
    }

    public int k(byte[] bArr, int i) {
        boolean z;
        int i2;
        int i3;
        int i4;
        int i5;
        if (u(bArr) || i(bArr)) {
            return 0;
        }
        w ns = w.ns();
        int b2 = f.b(bArr, ca.bLr, 2);
        int b3 = f.b(bArr, ca.bLr, 0);
        String q = q(bArr, i);
        if (b3 <= 0) {
            b3 = 100;
        }
        if (q.length() > b3) {
            q = q.substring(0, b3);
        }
        switch (b2) {
            case 0:
            case 1:
                z = false;
                break;
            case 2:
                z = false | true;
                break;
            case 3:
                z = true | false;
                break;
            case 4:
                z = false | true;
                break;
            default:
                z = false;
                break;
        }
        if (bc.aL(t(bArr), 16)) {
            z |= true;
        }
        int min = Math.min(b3, ns.Kf);
        int i6 = az.bdf[147] | -16777216;
        int i7 = az.bdf[146] | -16777216;
        if (this.p != null) {
            int b4 = f.b(bArr, ca.bLr, 1);
            int[] iArr = new int[4];
            this.dt.a(this.dt.bHl, bArr, iArr);
            int i8 = iArr[0];
            int i9 = iArr[1];
            int d = f.d(bArr);
            int e = f.e(bArr);
            if (i9 + e + 4 > this.dt.bGx + w.ML) {
                this.dt.bGv = ((i9 + e) + 4) - w.ML;
                this.dt.bGx = this.dt.bGv;
            }
            if (i9 - 4 < this.dt.bGx) {
                this.dt.bGv = i9 - 4;
                this.dt.bGx = this.dt.bGv;
            }
            if (this.dt.ch() != null) {
                w.ns().b((f) null);
                this.dt.ch().qW();
            }
            int i10 = i8 - this.dt.bGw;
            int i11 = i9 - this.dt.bGx;
            if (this.dt.LK()) {
                int jl = this.dt.jl(i10);
                int jl2 = this.dt.jl(i11);
                int jl3 = this.dt.jl(d);
                int jl4 = this.dt.jl(e);
                i2 = jl;
                i3 = jl3;
                i4 = jl2;
                i5 = jl4;
            } else {
                int i12 = e;
                i2 = i10;
                i3 = d;
                i4 = i11;
                i5 = i12;
            }
            if (b4 == 1) {
                q = q.replaceAll(bx.bxN, " ");
            }
            Object[] objArr = new Object[11];
            objArr[0] = Integer.valueOf(i2);
            objArr[1] = Integer.valueOf(i4);
            objArr[2] = Integer.valueOf(i3);
            objArr[3] = Integer.valueOf(i5);
            objArr[4] = Integer.valueOf(b4 == 1 ? 1 : 0);
            objArr[5] = q;
            objArr[6] = String.valueOf(this.dt.jl(this.dt.bFQ));
            objArr[7] = Boolean.valueOf(z && (z & true));
            objArr[8] = Integer.valueOf(min);
            objArr[9] = Integer.valueOf(i6);
            objArr[10] = Integer.valueOf(i7);
            this.p.s(objArr);
        }
        return 0;
    }

    public int l(byte[] bArr, int i) {
        if (u(bArr)) {
            return 0;
        }
        Vector n = n(bArr, i);
        char[] e = e((Vector) this.dt.bHd.get(Integer.valueOf(i)), bArr);
        int b2 = f.b(bArr, ca.bLp, 0);
        int b3 = f.b(bArr, ca.bLp, 1);
        boolean aL = bc.aL((byte) t(bArr), 8);
        int i2 = aL ? 5 : 4;
        StringBuffer stringBuffer = new StringBuffer();
        Vector vector = new Vector(b3);
        int i3 = 0;
        while (i3 < b3) {
            int i4 = 0;
            byte[] bArr2 = (byte[]) n.elementAt(b2 + i3);
            if (bArr2[0] == 10) {
                if (aL) {
                    int i5 = 0;
                    while (true) {
                        if (i5 >= e.length) {
                            break;
                        } else if (e[i5] == i3) {
                            i4 = 1;
                            break;
                        } else {
                            i5++;
                        }
                    }
                } else {
                    i4 = i3 == e[0] ? 1 : 0;
                }
                f.a(bArr2, ca.bLq, 0, stringBuffer);
                String stringBuffer2 = stringBuffer.toString();
                stringBuffer.setLength(0);
                f.a(bArr2, ca.bLq, 2, stringBuffer);
                String stringBuffer3 = stringBuffer.toString();
                stringBuffer.setLength(0);
                vector.addElement(new Object[]{stringBuffer2, stringBuffer3, new int[]{i4}});
            }
            i3++;
        }
        if (ch() != null) {
            ch().d(vector, aL);
        } else {
            r.g(vector, i2);
        }
        return 3;
    }

    public Object[] l(byte[] bArr) {
        return m(bArr, 0);
    }

    public boolean m(byte[] bArr) {
        return bc.aL(t(bArr), 2);
    }

    public Object[] m(byte[] bArr, int i) {
        return a(bArr, i, (String[][]) null, (String[]) null, -1);
    }

    public int n(boolean z) {
        f fVar = this.dt.bHl;
        int i = this.dt.bHm;
        if (fVar == null || i < 0 || i >= fVar.cF || !this.dt.d(fVar, i)) {
            return 0;
        }
        if (fVar == null || i == -1) {
            return 0;
        }
        byte[] bArr = fVar.cE[i] instanceof byte[] ? (byte[]) fVar.cE[i] : ((f) fVar.cE[i]).cH;
        switch (bArr[0]) {
            case 4:
                int i2 = i(bArr, fVar.cJ);
                this.dt.f(fVar, i);
                return i2;
            case 5:
                int j = j(bArr, fVar.cJ);
                if (j == 0) {
                    return j;
                }
                String r = r(bArr);
                if (bc.dM(r)) {
                    for (short s = 0; s < fVar.cF; s++) {
                        byte[] bArr2 = fVar.cE[s] instanceof byte[] ? (byte[]) fVar.cE[s] : ((f) fVar.cE[s]).cH;
                        if (bArr2[0] == 5 && r.equals(r(bArr2))) {
                            this.dt.f(fVar, s);
                        }
                    }
                }
                return j;
            case 6:
                return g(bArr);
            case 7:
                return k(bArr, fVar.cJ);
            case 9:
                return l(bArr, fVar.cJ);
            case 13:
            case 14:
            case 17:
                a(w.ns().lA(), bArr, z, fVar.cJ);
                return 1;
            case 24:
                return j(bArr);
            case 28:
                return k(bArr);
            case 58:
                this.dt.ch().q(cd.fH(this.dt.bFA), this.dt.bFA);
                return 0;
            default:
                return r.e(bArr, this.dt);
        }
    }

    public Vector n(byte[] bArr, int i) {
        int bS = i == -1 ? this.dt.bS() : i;
        return j(o(bArr, bS), bS);
    }

    public boolean n(byte[] bArr) {
        switch (bArr[0]) {
            case 13:
                int b2 = f.b(bArr, ca.bLv, 0);
                return b2 == 1 || b2 == 3;
            case 17:
                return true;
            default:
                return false;
        }
    }

    public int o(byte[] bArr, int i) {
        Vector vector = (Vector) this.dt.bHf.get(Integer.valueOf(i));
        for (int size = vector.size() - 1; size >= 0; size--) {
            Vector vector2 = (Vector) vector.elementAt(size);
            for (int size2 = vector2.size() - 1; size2 >= 0; size2--) {
                if (((byte[]) vector2.elementAt(size2)) == bArr) {
                    return size;
                }
            }
        }
        return -1;
    }

    public boolean o(byte[] bArr) {
        if (bArr == null) {
            return false;
        }
        int b2 = f.b(bArr, ca.bLv, 0);
        return b2 == 4 || b2 == 5;
    }

    public boolean p(byte[] bArr) {
        if (bArr[0] == 13) {
            return f.b(bArr, ca.bLv, 0) == 2;
        }
        return false;
    }

    public boolean p(byte[] bArr, int i) {
        if (w(bArr)) {
            return false;
        }
        char[][] b2 = b((Vector) this.dt.bHd.get(Integer.valueOf(i)), (short) v(bArr));
        return b2[1] == null || bc.a(b2[0], b2[1]);
    }

    public String q(byte[] bArr, int i) {
        return b((Vector) this.dt.bHd.get(Integer.valueOf(i == -1 ? this.dt.bS() : i)), bArr, 2);
    }

    public boolean q(byte[] bArr) {
        return bArr != null && (bArr[0] == 13 || bArr[0] == 17);
    }

    public boolean u(byte[] bArr) {
        return x(t(bArr));
    }

    public int v(byte[] bArr) {
        return f.f(bArr, 24);
    }

    public boolean w(byte[] bArr) {
        if (bArr == null || bArr.length == 0) {
            return false;
        }
        if (7 == bArr[0]) {
            return 1 == f.b(bArr, ca.bLr, 3);
        }
        return false;
    }

    public boolean x(int i) {
        return bc.aL(i, 1);
    }
}
