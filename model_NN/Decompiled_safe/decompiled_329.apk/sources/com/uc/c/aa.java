package com.uc.c;

import b.a;
import java.io.File;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class aa {
    private static final String LOGTAG = "CacheManager";
    private static final String Ug = "data/data/com.uc.browser/files/cache";
    private static final String Uh = "data/data/com.uc.browser/cache/webResCache";
    private static final String Ui = "uccacheindex.dat";
    private static final String Uj = "uccachesorted.dat";
    private static final int Uk = 2097152;
    private static final int Ul = 300;
    private static final int Um = 100;
    private static final int Un = 30720;
    private static final int Uo = 1800000;
    private static final int Up = 20;
    private static int Ut = 0;
    private long Uq;
    private int Ur;
    private int Us;
    private Hashtable Uu;
    private List Uv;
    private ax Uw;

    private aa() {
        this.Uq = -1;
        this.Ur = 0;
        this.Us = 0;
        this.Uu = new Hashtable();
        this.Uv = Collections.synchronizedList(new LinkedList());
        this.Uw = null;
        File file = new File(Uh);
        if (!file.exists()) {
            file.mkdirs();
        }
        rS();
        rU();
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x004d A[Catch:{ all -> 0x0077 }] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0057 A[SYNTHETIC, Splitter:B:19:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0060 A[SYNTHETIC, Splitter:B:25:0x0060] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0068 A[SYNTHETIC, Splitter:B:30:0x0068] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b(java.lang.String r7, byte[] r8) {
        /*
            r6 = this;
            r4 = 0
            java.lang.String r0 = "data/data/com.uc.browser/cache/webResCache"
            java.io.File r0 = new java.io.File
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "data/data/com.uc.browser/cache/webResCache/"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            r1 = 0
            java.io.File r2 = new java.io.File     // Catch:{ FileNotFoundException -> 0x003e, IOException -> 0x005c, all -> 0x0065 }
            java.lang.String r3 = "data/data/com.uc.browser/cache/webResCache"
            r2.<init>(r3)     // Catch:{ FileNotFoundException -> 0x003e, IOException -> 0x005c, all -> 0x0065 }
            boolean r3 = r2.exists()     // Catch:{ FileNotFoundException -> 0x003e, IOException -> 0x005c, all -> 0x0065 }
            if (r3 != 0) goto L_0x002c
            r2.mkdirs()     // Catch:{ FileNotFoundException -> 0x003e, IOException -> 0x005c, all -> 0x0065 }
        L_0x002c:
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x003e, IOException -> 0x005c, all -> 0x0065 }
            r2.<init>(r0)     // Catch:{ FileNotFoundException -> 0x003e, IOException -> 0x005c, all -> 0x0065 }
            r2.write(r8)     // Catch:{ FileNotFoundException -> 0x007f, IOException -> 0x007c, all -> 0x0074 }
            r2.flush()     // Catch:{ FileNotFoundException -> 0x007f, IOException -> 0x007c, all -> 0x0074 }
            r0 = 1
            if (r2 == 0) goto L_0x003d
            r2.close()     // Catch:{ IOException -> 0x006c }
        L_0x003d:
            return r0
        L_0x003e:
            r0 = move-exception
            r0 = r1
        L_0x0040:
            java.io.File r1 = new java.io.File     // Catch:{ all -> 0x0077 }
            java.lang.String r2 = "data/data/com.uc.browser/cache/webResCache"
            r1.<init>(r2)     // Catch:{ all -> 0x0077 }
            boolean r2 = r1.exists()     // Catch:{ all -> 0x0077 }
            if (r2 != 0) goto L_0x0055
            r1.mkdirs()     // Catch:{ all -> 0x0077 }
            java.util.Hashtable r1 = r6.Uu     // Catch:{ all -> 0x0077 }
            r1.clear()     // Catch:{ all -> 0x0077 }
        L_0x0055:
            if (r0 == 0) goto L_0x005a
            r0.close()     // Catch:{ IOException -> 0x006e }
        L_0x005a:
            r0 = r4
            goto L_0x003d
        L_0x005c:
            r0 = move-exception
            r0 = r1
        L_0x005e:
            if (r0 == 0) goto L_0x0063
            r0.close()     // Catch:{ IOException -> 0x0070 }
        L_0x0063:
            r0 = r4
            goto L_0x003d
        L_0x0065:
            r0 = move-exception
        L_0x0066:
            if (r1 == 0) goto L_0x006b
            r1.close()     // Catch:{ IOException -> 0x0072 }
        L_0x006b:
            throw r0
        L_0x006c:
            r1 = move-exception
            goto L_0x003d
        L_0x006e:
            r0 = move-exception
            goto L_0x005a
        L_0x0070:
            r0 = move-exception
            goto L_0x0063
        L_0x0072:
            r1 = move-exception
            goto L_0x006b
        L_0x0074:
            r0 = move-exception
            r1 = r2
            goto L_0x0066
        L_0x0077:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0066
        L_0x007c:
            r0 = move-exception
            r0 = r2
            goto L_0x005e
        L_0x007f:
            r0 = move-exception
            r0 = r2
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.aa.b(java.lang.String, byte[]):boolean");
    }

    public static aa rH() {
        return bw.bxz;
    }

    private void rL() {
        if (this.Uu != null && this.Uu.size() != 0) {
            Iterator it = this.Uu.keySet().iterator();
            while (it.hasNext()) {
                Long l = (Long) it.next();
                ax axVar = (ax) this.Uu.get(l);
                if (!axVar.aSZ && axVar.aSW <= System.currentTimeMillis()) {
                    it.remove();
                    s(l);
                    this.Us++;
                }
            }
        }
    }

    private void rM() {
        if (this.Uu != null && this.Uu.size() != 0) {
            while (this.Uu.size() > 300) {
                s(this.Uv.get(0));
                this.Uv.remove(0);
                int i = this.Us + 1;
                this.Us = i;
                if (i >= 100) {
                    return;
                }
            }
            File file = new File(Uh);
            if (file.isDirectory()) {
                long j = 0;
                for (File length : file.listFiles()) {
                    j += length.length();
                }
                long j2 = j;
                while (j2 > 2097152) {
                    j2 -= (long) s(this.Uv.get(0));
                    this.Uv.remove(0);
                    int i2 = this.Us + 1;
                    this.Us = i2;
                    if (i2 >= 100) {
                        return;
                    }
                }
            }
        }
    }

    public static void rP() {
        File file = new File(Ug);
        File[] fileArr = null;
        if (file.exists()) {
            fileArr = file.listFiles();
        }
        if (fileArr != null) {
            for (File delete : fileArr) {
                try {
                    delete.delete();
                } catch (Exception e) {
                }
            }
            try {
                file.delete();
            } catch (Exception e2) {
            }
            a.deleteFile(Ui);
        }
    }

    private void rQ() {
        Enumeration elements = this.Uu.elements();
        long currentTimeMillis = System.currentTimeMillis();
        while (elements.hasMoreElements()) {
            ax axVar = (ax) elements.nextElement();
            if (currentTimeMillis >= axVar.aSW && !axVar.aSZ) {
                s(axVar.aSY);
                this.Us++;
            } else if (!this.Uv.contains(axVar.aSY)) {
                this.Uv.add((Long) axVar.aSY);
            }
        }
        String[] list = new File(Uh).list();
        if (list != null && this.Uu.size() != list.length) {
            for (String str : list) {
                try {
                    if (!this.Uu.containsKey(Long.valueOf(str))) {
                        File file = new File("data/data/com.uc.browser/cache/webResCache/" + str);
                        if (file.exists()) {
                            synchronized (file) {
                                file.delete();
                                this.Us++;
                            }
                        }
                    }
                } catch (Exception e) {
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x007d A[SYNTHETIC, Splitter:B:25:0x007d] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0082 A[Catch:{ IOException -> 0x0086 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x009a A[SYNTHETIC, Splitter:B:39:0x009a] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x009f A[Catch:{ IOException -> 0x00a3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:58:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void rR() {
        /*
            r8 = this;
            r4 = 0
            java.io.File r1 = new java.io.File
            java.lang.String r2 = "data/data/com.uc.browser/cache/webResCache/uccacheindex.dat"
            r1.<init>(r2)
            java.util.Hashtable r2 = r8.Uu
            if (r2 == 0) goto L_0x0014
            java.util.Hashtable r2 = r8.Uu
            int r2 = r2.size()
            if (r2 != 0) goto L_0x001e
        L_0x0014:
            boolean r2 = r1.exists()
            if (r2 == 0) goto L_0x001d
            r1.delete()
        L_0x001d:
            return
        L_0x001e:
            java.io.File r2 = new java.io.File
            java.lang.String r3 = "data/data/com.uc.browser/cache/webResCache"
            r2.<init>(r3)
            boolean r3 = r2.exists()
            if (r3 != 0) goto L_0x002e
            r2.mkdirs()
        L_0x002e:
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x00ae, all -> 0x0095 }
            r2.<init>(r1)     // Catch:{ IOException -> 0x00ae, all -> 0x0095 }
            java.io.DataOutputStream r3 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x00b2, all -> 0x00a5 }
            java.io.BufferedOutputStream r1 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x00b2, all -> 0x00a5 }
            r1.<init>(r2)     // Catch:{ IOException -> 0x00b2, all -> 0x00a5 }
            r3.<init>(r1)     // Catch:{ IOException -> 0x00b2, all -> 0x00a5 }
            java.util.Hashtable r1 = r8.Uu     // Catch:{ IOException -> 0x0079, all -> 0x00a9 }
            java.util.Enumeration r4 = r1.elements()     // Catch:{ IOException -> 0x0079, all -> 0x00a9 }
        L_0x0043:
            boolean r1 = r4.hasMoreElements()     // Catch:{ IOException -> 0x0079, all -> 0x00a9 }
            if (r1 == 0) goto L_0x0088
            java.lang.Object r8 = r4.nextElement()     // Catch:{ IOException -> 0x0079, all -> 0x00a9 }
            r0 = r8
            com.uc.c.ax r0 = (com.uc.c.ax) r0     // Catch:{ IOException -> 0x0079, all -> 0x00a9 }
            r1 = r0
            java.lang.Object r8 = r1.aSY     // Catch:{ IOException -> 0x0079, all -> 0x00a9 }
            java.lang.Long r8 = (java.lang.Long) r8     // Catch:{ IOException -> 0x0079, all -> 0x00a9 }
            long r5 = r8.longValue()     // Catch:{ IOException -> 0x0079, all -> 0x00a9 }
            r3.writeLong(r5)     // Catch:{ IOException -> 0x0079, all -> 0x00a9 }
            long r5 = r1.aSW     // Catch:{ IOException -> 0x0079, all -> 0x00a9 }
            r3.writeLong(r5)     // Catch:{ IOException -> 0x0079, all -> 0x00a9 }
            long r5 = r1.aSX     // Catch:{ IOException -> 0x0079, all -> 0x00a9 }
            r3.writeLong(r5)     // Catch:{ IOException -> 0x0079, all -> 0x00a9 }
            java.lang.String r5 = r1.ZQ     // Catch:{ IOException -> 0x0079, all -> 0x00a9 }
            if (r5 != 0) goto L_0x006e
            java.lang.String r5 = ""
            r1.ZQ = r5     // Catch:{ IOException -> 0x0079, all -> 0x00a9 }
        L_0x006e:
            java.lang.String r5 = r1.ZQ     // Catch:{ IOException -> 0x0079, all -> 0x00a9 }
            r3.writeUTF(r5)     // Catch:{ IOException -> 0x0079, all -> 0x00a9 }
            boolean r1 = r1.aSZ     // Catch:{ IOException -> 0x0079, all -> 0x00a9 }
            r3.writeBoolean(r1)     // Catch:{ IOException -> 0x0079, all -> 0x00a9 }
            goto L_0x0043
        L_0x0079:
            r1 = move-exception
            r1 = r3
        L_0x007b:
            if (r1 == 0) goto L_0x0080
            r1.close()     // Catch:{ IOException -> 0x0086 }
        L_0x0080:
            if (r2 == 0) goto L_0x001d
            r2.close()     // Catch:{ IOException -> 0x0086 }
            goto L_0x001d
        L_0x0086:
            r1 = move-exception
            goto L_0x001d
        L_0x0088:
            if (r3 == 0) goto L_0x008d
            r3.close()     // Catch:{ IOException -> 0x0093 }
        L_0x008d:
            if (r2 == 0) goto L_0x001d
            r2.close()     // Catch:{ IOException -> 0x0093 }
            goto L_0x001d
        L_0x0093:
            r1 = move-exception
            goto L_0x001d
        L_0x0095:
            r1 = move-exception
            r2 = r4
            r3 = r4
        L_0x0098:
            if (r2 == 0) goto L_0x009d
            r2.close()     // Catch:{ IOException -> 0x00a3 }
        L_0x009d:
            if (r3 == 0) goto L_0x00a2
            r3.close()     // Catch:{ IOException -> 0x00a3 }
        L_0x00a2:
            throw r1
        L_0x00a3:
            r2 = move-exception
            goto L_0x00a2
        L_0x00a5:
            r1 = move-exception
            r3 = r2
            r2 = r4
            goto L_0x0098
        L_0x00a9:
            r1 = move-exception
            r7 = r3
            r3 = r2
            r2 = r7
            goto L_0x0098
        L_0x00ae:
            r1 = move-exception
            r1 = r4
            r2 = r4
            goto L_0x007b
        L_0x00b2:
            r1 = move-exception
            r1 = r4
            goto L_0x007b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.aa.rR():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0067 A[SYNTHETIC, Splitter:B:16:0x0067] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006c A[Catch:{ IOException -> 0x0070 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0084 A[SYNTHETIC, Splitter:B:30:0x0084] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0089 A[Catch:{ IOException -> 0x008d }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void rS() {
        /*
            r6 = this;
            r3 = 0
            java.io.File r0 = new java.io.File
            java.lang.String r1 = "data/data/com.uc.browser/cache/webResCache/uccacheindex.dat"
            r0.<init>(r1)
            boolean r1 = r0.exists()
            if (r1 != 0) goto L_0x000f
        L_0x000e:
            return
        L_0x000f:
            java.util.Hashtable r1 = r6.Uu
            if (r1 != 0) goto L_0x001a
            java.util.Hashtable r1 = new java.util.Hashtable
            r1.<init>()
            r6.Uu = r1
        L_0x001a:
            java.util.Hashtable r1 = r6.Uu
            r1.clear()
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0099, all -> 0x007f }
            r1.<init>(r0)     // Catch:{ IOException -> 0x0099, all -> 0x007f }
            java.io.DataInputStream r0 = new java.io.DataInputStream     // Catch:{ IOException -> 0x009d, all -> 0x008f }
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x009d, all -> 0x008f }
            r2.<init>(r1)     // Catch:{ IOException -> 0x009d, all -> 0x008f }
            r0.<init>(r2)     // Catch:{ IOException -> 0x009d, all -> 0x008f }
        L_0x002e:
            int r2 = r0.available()     // Catch:{ IOException -> 0x0064, all -> 0x0093 }
            if (r2 <= 0) goto L_0x0072
            com.uc.c.ax r2 = new com.uc.c.ax     // Catch:{ IOException -> 0x0064, all -> 0x0093 }
            r3 = 0
            r2.<init>()     // Catch:{ IOException -> 0x0064, all -> 0x0093 }
            long r3 = r0.readLong()     // Catch:{ IOException -> 0x0064, all -> 0x0093 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ IOException -> 0x0064, all -> 0x0093 }
            r2.aSY = r3     // Catch:{ IOException -> 0x0064, all -> 0x0093 }
            long r3 = r0.readLong()     // Catch:{ IOException -> 0x0064, all -> 0x0093 }
            r2.aSW = r3     // Catch:{ IOException -> 0x0064, all -> 0x0093 }
            long r3 = r0.readLong()     // Catch:{ IOException -> 0x0064, all -> 0x0093 }
            r2.aSX = r3     // Catch:{ IOException -> 0x0064, all -> 0x0093 }
            java.lang.String r3 = r0.readUTF()     // Catch:{ IOException -> 0x0064, all -> 0x0093 }
            r2.ZQ = r3     // Catch:{ IOException -> 0x0064, all -> 0x0093 }
            java.util.Hashtable r3 = r6.Uu     // Catch:{ IOException -> 0x0064, all -> 0x0093 }
            java.lang.Object r4 = r2.aSY     // Catch:{ IOException -> 0x0064, all -> 0x0093 }
            r3.put(r4, r2)     // Catch:{ IOException -> 0x0064, all -> 0x0093 }
            boolean r3 = r0.readBoolean()     // Catch:{ IOException -> 0x0064, all -> 0x0093 }
            r2.aSZ = r3     // Catch:{ IOException -> 0x0064, all -> 0x0093 }
            goto L_0x002e
        L_0x0064:
            r2 = move-exception
        L_0x0065:
            if (r0 == 0) goto L_0x006a
            r0.close()     // Catch:{ IOException -> 0x0070 }
        L_0x006a:
            if (r1 == 0) goto L_0x000e
            r1.close()     // Catch:{ IOException -> 0x0070 }
            goto L_0x000e
        L_0x0070:
            r0 = move-exception
            goto L_0x000e
        L_0x0072:
            if (r0 == 0) goto L_0x0077
            r0.close()     // Catch:{ IOException -> 0x007d }
        L_0x0077:
            if (r1 == 0) goto L_0x000e
            r1.close()     // Catch:{ IOException -> 0x007d }
            goto L_0x000e
        L_0x007d:
            r0 = move-exception
            goto L_0x000e
        L_0x007f:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0082:
            if (r1 == 0) goto L_0x0087
            r1.close()     // Catch:{ IOException -> 0x008d }
        L_0x0087:
            if (r2 == 0) goto L_0x008c
            r2.close()     // Catch:{ IOException -> 0x008d }
        L_0x008c:
            throw r0
        L_0x008d:
            r1 = move-exception
            goto L_0x008c
        L_0x008f:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x0082
        L_0x0093:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r0
            r0 = r5
            goto L_0x0082
        L_0x0099:
            r0 = move-exception
            r0 = r3
            r1 = r3
            goto L_0x0065
        L_0x009d:
            r0 = move-exception
            r0 = r3
            goto L_0x0065
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.aa.rS():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x005c A[SYNTHETIC, Splitter:B:21:0x005c] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0061 A[Catch:{ IOException -> 0x0065 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x007c A[SYNTHETIC, Splitter:B:37:0x007c] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0081 A[Catch:{ IOException -> 0x0085 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void rT() {
        /*
            r6 = this;
            r3 = 0
            java.io.File r0 = new java.io.File
            java.lang.String r1 = "data/data/com.uc.browser/cache/webResCache/uccachesorted.dat"
            r0.<init>(r1)
            java.util.List r1 = r6.Uv
            if (r1 == 0) goto L_0x0014
            java.util.List r1 = r6.Uv
            int r1 = r1.size()
            if (r1 != 0) goto L_0x001e
        L_0x0014:
            boolean r1 = r0.exists()
            if (r1 == 0) goto L_0x001d
            r0.delete()
        L_0x001d:
            return
        L_0x001e:
            boolean r1 = r0.exists()     // Catch:{ IOException -> 0x0091, all -> 0x0077 }
            if (r1 != 0) goto L_0x0027
            r0.createNewFile()     // Catch:{ IOException -> 0x0091, all -> 0x0077 }
        L_0x0027:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0091, all -> 0x0077 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x0091, all -> 0x0077 }
            java.io.DataOutputStream r0 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x0095, all -> 0x0087 }
            java.io.BufferedOutputStream r2 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x0095, all -> 0x0087 }
            r2.<init>(r1)     // Catch:{ IOException -> 0x0095, all -> 0x0087 }
            r0.<init>(r2)     // Catch:{ IOException -> 0x0095, all -> 0x0087 }
            java.util.List r2 = r6.Uv     // Catch:{ IOException -> 0x0059, all -> 0x008b }
            int r2 = r2.size()     // Catch:{ IOException -> 0x0059, all -> 0x008b }
            r0.writeInt(r2)     // Catch:{ IOException -> 0x0059, all -> 0x008b }
            java.util.List r2 = r6.Uv     // Catch:{ IOException -> 0x0059, all -> 0x008b }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ IOException -> 0x0059, all -> 0x008b }
        L_0x0045:
            boolean r3 = r2.hasNext()     // Catch:{ IOException -> 0x0059, all -> 0x008b }
            if (r3 == 0) goto L_0x0067
            java.lang.Object r6 = r2.next()     // Catch:{ IOException -> 0x0059, all -> 0x008b }
            java.lang.Long r6 = (java.lang.Long) r6     // Catch:{ IOException -> 0x0059, all -> 0x008b }
            long r3 = r6.longValue()     // Catch:{ IOException -> 0x0059, all -> 0x008b }
            r0.writeLong(r3)     // Catch:{ IOException -> 0x0059, all -> 0x008b }
            goto L_0x0045
        L_0x0059:
            r2 = move-exception
        L_0x005a:
            if (r0 == 0) goto L_0x005f
            r0.close()     // Catch:{ IOException -> 0x0065 }
        L_0x005f:
            if (r1 == 0) goto L_0x001d
            r1.close()     // Catch:{ IOException -> 0x0065 }
            goto L_0x001d
        L_0x0065:
            r0 = move-exception
            goto L_0x001d
        L_0x0067:
            r0.flush()     // Catch:{ IOException -> 0x0059, all -> 0x008b }
            if (r0 == 0) goto L_0x006f
            r0.close()     // Catch:{ IOException -> 0x0075 }
        L_0x006f:
            if (r1 == 0) goto L_0x001d
            r1.close()     // Catch:{ IOException -> 0x0075 }
            goto L_0x001d
        L_0x0075:
            r0 = move-exception
            goto L_0x001d
        L_0x0077:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x007a:
            if (r1 == 0) goto L_0x007f
            r1.close()     // Catch:{ IOException -> 0x0085 }
        L_0x007f:
            if (r2 == 0) goto L_0x0084
            r2.close()     // Catch:{ IOException -> 0x0085 }
        L_0x0084:
            throw r0
        L_0x0085:
            r1 = move-exception
            goto L_0x0084
        L_0x0087:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x007a
        L_0x008b:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r0
            r0 = r5
            goto L_0x007a
        L_0x0091:
            r0 = move-exception
            r0 = r3
            r1 = r3
            goto L_0x005a
        L_0x0095:
            r0 = move-exception
            r0 = r3
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.aa.rT():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0057 A[SYNTHETIC, Splitter:B:24:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005c A[Catch:{ IOException -> 0x0060 }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0067 A[SYNTHETIC, Splitter:B:32:0x0067] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x006c A[Catch:{ IOException -> 0x0070 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void rU() {
        /*
            r8 = this;
            r3 = 0
            java.io.File r0 = new java.io.File
            java.lang.String r1 = "data/data/com.uc.browser/cache/webResCache/uccachesorted.dat"
            r0.<init>(r1)
            boolean r1 = r0.exists()
            if (r1 != 0) goto L_0x000f
        L_0x000e:
            return
        L_0x000f:
            java.util.List r1 = r8.Uv
            if (r1 != 0) goto L_0x001a
            java.util.Vector r1 = new java.util.Vector
            r1.<init>()
            r8.Uv = r1
        L_0x001a:
            java.util.List r1 = r8.Uv
            r1.clear()
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0052, all -> 0x0062 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x0052, all -> 0x0062 }
            java.io.DataInputStream r0 = new java.io.DataInputStream     // Catch:{ IOException -> 0x007c, all -> 0x0072 }
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x007c, all -> 0x0072 }
            r2.<init>(r1)     // Catch:{ IOException -> 0x007c, all -> 0x0072 }
            r0.<init>(r2)     // Catch:{ IOException -> 0x007c, all -> 0x0072 }
            int r2 = r0.readInt()     // Catch:{ IOException -> 0x007f, all -> 0x0076 }
            r3 = 0
        L_0x0033:
            if (r3 >= r2) goto L_0x0045
            java.util.List r4 = r8.Uv     // Catch:{ IOException -> 0x007f, all -> 0x0076 }
            long r5 = r0.readLong()     // Catch:{ IOException -> 0x007f, all -> 0x0076 }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ IOException -> 0x007f, all -> 0x0076 }
            r4.add(r5)     // Catch:{ IOException -> 0x007f, all -> 0x0076 }
            int r3 = r3 + 1
            goto L_0x0033
        L_0x0045:
            if (r0 == 0) goto L_0x004a
            r0.close()     // Catch:{ IOException -> 0x0050 }
        L_0x004a:
            if (r1 == 0) goto L_0x000e
            r1.close()     // Catch:{ IOException -> 0x0050 }
            goto L_0x000e
        L_0x0050:
            r0 = move-exception
            goto L_0x000e
        L_0x0052:
            r0 = move-exception
            r0 = r3
            r1 = r3
        L_0x0055:
            if (r0 == 0) goto L_0x005a
            r0.close()     // Catch:{ IOException -> 0x0060 }
        L_0x005a:
            if (r1 == 0) goto L_0x000e
            r1.close()     // Catch:{ IOException -> 0x0060 }
            goto L_0x000e
        L_0x0060:
            r0 = move-exception
            goto L_0x000e
        L_0x0062:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0065:
            if (r1 == 0) goto L_0x006a
            r1.close()     // Catch:{ IOException -> 0x0070 }
        L_0x006a:
            if (r2 == 0) goto L_0x006f
            r2.close()     // Catch:{ IOException -> 0x0070 }
        L_0x006f:
            throw r0
        L_0x0070:
            r1 = move-exception
            goto L_0x006f
        L_0x0072:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x0065
        L_0x0076:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r0
            r0 = r7
            goto L_0x0065
        L_0x007c:
            r0 = move-exception
            r0 = r3
            goto L_0x0055
        L_0x007f:
            r2 = move-exception
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.aa.rU():void");
    }

    private boolean u(Object obj) {
        return new File("data/data/com.uc.browser/cache/webResCache/" + obj.toString()).exists();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0036, code lost:
        if (r1 == null) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0043, code lost:
        r0 = r2;
        r1 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0069, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x006a, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x006d, code lost:
        r0 = r2;
        r1 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:?, code lost:
        return r1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0057 A[SYNTHETIC, Splitter:B:35:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0063 A[SYNTHETIC, Splitter:B:43:0x0063] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] v(java.lang.Object r7) {
        /*
            r6 = this;
            r4 = 0
            java.io.File r0 = new java.io.File
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "data/data/com.uc.browser/cache/webResCache/"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r7.toString()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            boolean r1 = r0.exists()
            if (r1 != 0) goto L_0x0025
            r0 = r4
        L_0x0024:
            return r0
        L_0x0025:
            monitor-enter(r0)     // Catch:{ FileNotFoundException -> 0x0070, IOException -> 0x0052, all -> 0x005f }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ all -> 0x003d }
            r1.<init>(r0)     // Catch:{ all -> 0x003d }
            long r2 = r0.length()     // Catch:{ all -> 0x0074 }
            int r2 = (int) r2     // Catch:{ all -> 0x0074 }
            byte[] r2 = new byte[r2]     // Catch:{ all -> 0x0074 }
            r1.read(r2)     // Catch:{ all -> 0x007a }
            monitor-exit(r0)     // Catch:{ all -> 0x007a }
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ IOException -> 0x004c }
        L_0x003b:
            r0 = r2
            goto L_0x0024
        L_0x003d:
            r1 = move-exception
            r2 = r4
            r3 = r4
        L_0x0040:
            monitor-exit(r0)     // Catch:{ all -> 0x0080 }
            throw r1     // Catch:{ FileNotFoundException -> 0x0042, IOException -> 0x006c, all -> 0x0069 }
        L_0x0042:
            r0 = move-exception
            r0 = r2
            r1 = r3
        L_0x0045:
            if (r0 == 0) goto L_0x004a
            r0.close()     // Catch:{ IOException -> 0x004f }
        L_0x004a:
            r0 = r1
            goto L_0x0024
        L_0x004c:
            r0 = move-exception
            r0 = r2
            goto L_0x0024
        L_0x004f:
            r0 = move-exception
            r0 = r1
            goto L_0x0024
        L_0x0052:
            r0 = move-exception
            r0 = r4
            r1 = r4
        L_0x0055:
            if (r0 == 0) goto L_0x005a
            r0.close()     // Catch:{ IOException -> 0x005c }
        L_0x005a:
            r0 = r1
            goto L_0x0024
        L_0x005c:
            r0 = move-exception
            r0 = r1
            goto L_0x0024
        L_0x005f:
            r0 = move-exception
            r1 = r4
        L_0x0061:
            if (r1 == 0) goto L_0x0066
            r1.close()     // Catch:{ IOException -> 0x0067 }
        L_0x0066:
            throw r0
        L_0x0067:
            r1 = move-exception
            goto L_0x0066
        L_0x0069:
            r0 = move-exception
            r1 = r2
            goto L_0x0061
        L_0x006c:
            r0 = move-exception
            r0 = r2
            r1 = r3
            goto L_0x0055
        L_0x0070:
            r0 = move-exception
            r0 = r4
            r1 = r4
            goto L_0x0045
        L_0x0074:
            r2 = move-exception
            r3 = r4
            r5 = r1
            r1 = r2
            r2 = r5
            goto L_0x0040
        L_0x007a:
            r3 = move-exception
            r5 = r3
            r3 = r2
            r2 = r1
            r1 = r5
            goto L_0x0040
        L_0x0080:
            r1 = move-exception
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.aa.v(java.lang.Object):byte[]");
    }

    public synchronized Object[] a(int i, Object obj, byte[] bArr, long j, String str, boolean z) {
        Object[] objArr;
        if (j > 0) {
            if (bArr.length <= Un) {
                if (z) {
                    if (!b(obj.toString(), bArr)) {
                        objArr = null;
                    }
                }
                long j2 = ((long) (i << 8)) | 0 | (((long) Ut) << 16);
                this.Uv.add((Long) obj);
                ax axVar = new ax();
                axVar.aSW = System.currentTimeMillis() + j;
                axVar.aSX = j2;
                axVar.ZQ = str;
                axVar.aSY = obj;
                this.Uu.put(obj, axVar);
                objArr = new Object[]{bArr, new long[]{System.currentTimeMillis() + j, j2}, str, Boolean.FALSE};
                this.Ur++;
            }
        }
        objArr = null;
        return objArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00a2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.Object[] a(b.a.a.c.e r12, java.lang.String r13, byte[] r14, int r15, java.lang.Long r16) {
        /*
            r11 = this;
            monitor-enter(r11)
            r0 = -1
            java.lang.String r2 = "Content-Type"
            java.lang.String r2 = r12.getHeaderField(r2)     // Catch:{ IOException -> 0x0157 }
            boolean r3 = com.uc.c.bc.dM(r2)     // Catch:{ IOException -> 0x0157 }
            if (r3 == 0) goto L_0x0172
            r3 = 3
            if (r15 == r3) goto L_0x0015
            r3 = 4
            if (r15 != r3) goto L_0x0172
        L_0x0015:
            r3 = -1
            java.lang.String r4 = "wml"
            int r2 = r2.indexOf(r4)     // Catch:{ IOException -> 0x0157 }
            if (r3 != r2) goto L_0x00bd
            r2 = 4
        L_0x001f:
            r3 = 3
            if (r2 != r3) goto L_0x00c0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0165 }
            r3.<init>()     // Catch:{ IOException -> 0x0165 }
            java.lang.StringBuilder r3 = r3.append(r13)     // Catch:{ IOException -> 0x0165 }
            java.lang.String r4 = "wml"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x0165 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x0165 }
            byte[] r3 = r3.getBytes()     // Catch:{ IOException -> 0x0165 }
            java.lang.Long r3 = com.uc.c.bc.ar(r3)     // Catch:{ IOException -> 0x0165 }
        L_0x003d:
            boolean r4 = r11.rJ()     // Catch:{ IOException -> 0x016a }
            if (r4 == 0) goto L_0x0095
            java.lang.String r4 = "cache-control"
            java.lang.String r4 = r12.getHeaderField(r4)     // Catch:{ IOException -> 0x016a }
            boolean r5 = com.uc.c.bc.dM(r4)     // Catch:{ IOException -> 0x016a }
            if (r5 == 0) goto L_0x0124
            java.lang.String r4 = r4.toLowerCase()     // Catch:{ IOException -> 0x016a }
            java.lang.String r5 = "private"
            int r5 = r4.indexOf(r5)     // Catch:{ IOException -> 0x016a }
            r6 = -1
            if (r5 != r6) goto L_0x006e
            java.lang.String r5 = "no-store"
            int r5 = r4.indexOf(r5)     // Catch:{ IOException -> 0x016a }
            r6 = -1
            if (r5 != r6) goto L_0x006e
            java.lang.String r5 = "no-cache"
            int r5 = r4.indexOf(r5)     // Catch:{ IOException -> 0x016a }
            r6 = -1
            if (r5 == r6) goto L_0x00e0
        L_0x006e:
            r0 = -3
        L_0x0070:
            r4 = -1
            int r4 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r4 == 0) goto L_0x007d
            r4 = 259200000(0xf731400, double:1.280618154E-315)
            int r4 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x0080
        L_0x007d:
            r0 = 259200000(0xf731400, double:1.280618154E-315)
        L_0x0080:
            if (r14 == 0) goto L_0x0095
            r4 = -1
            int r4 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x0095
            r4 = 3
            if (r2 == r4) goto L_0x008e
            r4 = 4
            if (r2 != r4) goto L_0x0095
        L_0x008e:
            r4 = 300000(0x493e0, double:1.482197E-318)
            long r0 = java.lang.Math.max(r0, r4)     // Catch:{ IOException -> 0x016a }
        L_0x0095:
            r4 = r0
            r1 = r2
            r2 = r3
        L_0x0098:
            r6 = 0
            r7 = 1
            r0 = r11
            r3 = r14
            java.lang.Object[] r0 = r0.a(r1, r2, r3, r4, r6, r7)     // Catch:{ all -> 0x0162 }
            if (r0 != 0) goto L_0x00bb
            r0 = 2
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x0162 }
            r2 = 0
            r0[r2] = r14     // Catch:{ all -> 0x0162 }
            r2 = 1
            r3 = 2
            long[] r3 = new long[r3]     // Catch:{ all -> 0x0162 }
            r4 = 0
            r5 = 0
            r3[r4] = r5     // Catch:{ all -> 0x0162 }
            r4 = 1
            int r1 = r1 << 8
            r1 = r1 | 0
            long r5 = (long) r1     // Catch:{ all -> 0x0162 }
            r3[r4] = r5     // Catch:{ all -> 0x0162 }
            r0[r2] = r3     // Catch:{ all -> 0x0162 }
        L_0x00bb:
            monitor-exit(r11)
            return r0
        L_0x00bd:
            r2 = 3
            goto L_0x001f
        L_0x00c0:
            r3 = 4
            if (r2 != r3) goto L_0x016e
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0165 }
            r3.<init>()     // Catch:{ IOException -> 0x0165 }
            java.lang.StringBuilder r3 = r3.append(r13)     // Catch:{ IOException -> 0x0165 }
            java.lang.String r4 = "xhtml"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x0165 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x0165 }
            byte[] r3 = r3.getBytes()     // Catch:{ IOException -> 0x0165 }
            java.lang.Long r3 = com.uc.c.bc.ar(r3)     // Catch:{ IOException -> 0x0165 }
            goto L_0x003d
        L_0x00e0:
            java.lang.String r5 = ","
            java.lang.String[] r4 = com.uc.c.bc.split(r4, r5)     // Catch:{ IOException -> 0x016a }
            r5 = 0
            r10 = r5
            r5 = r0
            r0 = r10
        L_0x00ea:
            int r1 = r4.length     // Catch:{ IOException -> 0x016a }
            if (r0 >= r1) goto L_0x0121
            r1 = r4[r0]     // Catch:{ IOException -> 0x016a }
            r7 = 61
            int r7 = r1.indexOf(r7)     // Catch:{ IOException -> 0x016a }
            r8 = -1
            if (r7 == r8) goto L_0x011e
            r8 = 0
            java.lang.String r8 = r1.substring(r8, r7)     // Catch:{ IOException -> 0x016a }
            java.lang.String r8 = r8.trim()     // Catch:{ IOException -> 0x016a }
            int r7 = r7 + 1
            int r9 = r1.length()     // Catch:{ IOException -> 0x016a }
            java.lang.String r1 = r1.substring(r7, r9)     // Catch:{ IOException -> 0x016a }
            java.lang.String r1 = r1.trim()     // Catch:{ IOException -> 0x016a }
            java.lang.String r7 = "max-age"
            boolean r7 = r7.equals(r8)     // Catch:{ IOException -> 0x016a }
            if (r7 == 0) goto L_0x011e
            long r5 = java.lang.Long.parseLong(r1)     // Catch:{ IOException -> 0x016a }
            r7 = 1000(0x3e8, double:4.94E-321)
            long r5 = r5 * r7
        L_0x011e:
            int r0 = r0 + 1
            goto L_0x00ea
        L_0x0121:
            r0 = r5
            goto L_0x0070
        L_0x0124:
            java.lang.String r4 = "pragma"
            java.lang.String r4 = r12.getHeaderField(r4)     // Catch:{ IOException -> 0x016a }
            boolean r5 = com.uc.c.bc.dM(r4)     // Catch:{ IOException -> 0x016a }
            if (r5 == 0) goto L_0x013c
            java.lang.String r5 = "no-cache"
            boolean r4 = r4.equalsIgnoreCase(r5)     // Catch:{ IOException -> 0x016a }
            if (r4 == 0) goto L_0x0070
            r0 = -4
            goto L_0x0070
        L_0x013c:
            java.lang.String r4 = "expires"
            java.lang.String r4 = r12.getHeaderField(r4)     // Catch:{ IOException -> 0x016a }
            boolean r5 = com.uc.c.bc.dM(r4)     // Catch:{ IOException -> 0x016a }
            if (r5 == 0) goto L_0x0070
            java.lang.String r0 = r4.trim()     // Catch:{ IOException -> 0x016a }
            long r0 = com.uc.c.bc.dY(r0)     // Catch:{ IOException -> 0x016a }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x016a }
            long r0 = r0 - r4
            goto L_0x0070
        L_0x0157:
            r0 = move-exception
            r0 = r15
            r1 = r16
        L_0x015b:
            r2 = -2
            r4 = r2
            r2 = r1
            r1 = r0
            goto L_0x0098
        L_0x0162:
            r0 = move-exception
            monitor-exit(r11)
            throw r0
        L_0x0165:
            r0 = move-exception
            r0 = r2
            r1 = r16
            goto L_0x015b
        L_0x016a:
            r0 = move-exception
            r0 = r2
            r1 = r3
            goto L_0x015b
        L_0x016e:
            r3 = r16
            goto L_0x003d
        L_0x0172:
            r2 = r15
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.aa.a(b.a.a.c.e, java.lang.String, byte[], int, java.lang.Long):java.lang.Object[]");
    }

    public synchronized long[] bU(String str) {
        long[] jArr;
        if (str != null) {
            if (str.trim().length() != 0) {
                long[] jArr2 = new long[this.Uu.size()];
                Enumeration elements = this.Uu.elements();
                int i = 0;
                while (elements.hasMoreElements()) {
                    ax axVar = (ax) elements.nextElement();
                    if (axVar != null && axVar.aSW > System.currentTimeMillis() && axVar.ZQ != null && axVar.ZQ.length() > 0 && str.contains(axVar.ZQ) && u(axVar.aSY)) {
                        jArr2[i] = ((Long) axVar.aSY).longValue();
                        axVar.aSZ = true;
                        i++;
                    }
                }
                jArr = new long[i];
                System.arraycopy(jArr2, 0, jArr, 0, i);
            }
        }
        jArr = new long[0];
        return jArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(int, int):int}
     arg types: [byte, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int} */
    public synchronized Object[] r(Object obj) {
        ax axVar;
        Object[] objArr;
        Object[] objArr2;
        if (obj == null) {
            objArr2 = null;
        } else {
            if (this.Uw == null || !this.Uw.equals(obj)) {
                ax axVar2 = (ax) this.Uu.get(obj);
                this.Uw = axVar2;
                axVar = axVar2;
            } else {
                axVar = this.Uw;
            }
            if (axVar == null) {
                objArr2 = null;
            } else {
                byte[] v = v(obj);
                if (v == null) {
                    this.Uu.remove(obj);
                    objArr = null;
                } else {
                    objArr = new Object[]{v, new long[]{axVar.aSW, axVar.aSX}, axVar.ZQ, Boolean.FALSE};
                }
                if (objArr != null) {
                    byte b2 = (byte) ((int) (axVar.aSX & 255));
                    if ((axVar.ZQ == null || axVar.ZQ.length() <= 0) && axVar.aSW < System.currentTimeMillis()) {
                        s(obj);
                        objArr2 = null;
                    } else {
                        axVar.aSW = ((long[]) objArr[1])[0];
                        axVar.aSX = (axVar.aSX & -256) | ((long) Math.min((int) ((byte) (b2 + 1)), 126));
                        axVar.aSX = (axVar.aSX & 65535) | (((long) Ut) << 16);
                        ((long[]) objArr[1])[1] = axVar.aSX;
                        axVar.aSZ = Boolean.TRUE.booleanValue();
                        this.Uv.remove(obj);
                        this.Uv.add((Long) obj);
                        objArr2 = objArr;
                    }
                } else {
                    objArr2 = objArr;
                }
            }
        }
        return objArr2;
    }

    public synchronized void rI() {
        rR();
        rT();
        this.Uu.clear();
        this.Uv.clear();
    }

    public boolean rJ() {
        return w.KW >= 1000000 && w.mQ() >= 102400;
    }

    public synchronized boolean rK() {
        boolean z;
        if (this.Ur > 20) {
            rR();
            rT();
            this.Ur = 0;
        }
        if (this.Uq == -1) {
            this.Uq = System.currentTimeMillis();
            this.Us = 0;
            rQ();
            rM();
            this.Us = 0;
            z = true;
        } else if (System.currentTimeMillis() - this.Uq >= 1800000) {
            this.Us = 0;
            this.Uq = System.currentTimeMillis();
            rL();
            rM();
            this.Us = 0;
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    public synchronized void rN() {
        Ut++;
    }

    public synchronized void rO() {
        this.Uu.clear();
        this.Uv.clear();
        this.Ur += 20;
        File file = new File(Uh);
        File[] listFiles = file.exists() ? file.listFiles() : null;
        if (listFiles != null) {
            for (File file2 : listFiles) {
                try {
                    synchronized (file2) {
                        file2.delete();
                    }
                } catch (Exception e) {
                }
            }
        }
    }

    public synchronized int s(Object obj) {
        int i;
        i = 0;
        ax axVar = (ax) this.Uu.get(obj);
        if (axVar != null && !axVar.aSZ) {
            File file = new File("data/data/com.uc.browser/cache/webResCache/" + axVar.aSY.toString());
            if (file.exists()) {
                i = (int) file.length();
                file.delete();
            }
            this.Uu.remove(axVar.aSY);
            this.Uv.remove(axVar.aSY);
            this.Ur++;
        }
        return i;
    }

    public synchronized void t(Object obj) {
        if (obj != null) {
            ax axVar = (ax) this.Uu.get(obj);
            if (axVar != null) {
                axVar.aSZ = Boolean.FALSE.booleanValue();
            }
        }
    }
}
