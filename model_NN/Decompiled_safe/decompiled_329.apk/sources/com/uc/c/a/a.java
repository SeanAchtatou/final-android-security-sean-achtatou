package com.uc.c.a;

import android.content.Context;
import b.a.a.c.b.b;
import b.a.a.c.e;
import com.uc.c.au;
import com.uc.c.az;
import com.uc.c.bx;
import com.uc.c.cd;
import com.uc.c.w;

public class a {
    public static Context cr = null;
    public static final byte eA = 1;
    public static final byte eB = 2;
    public static final int ew = 15000;
    public static final int ex = 307200;
    public static final byte ey = -1;
    public static final byte ez = 0;

    public static long a(e eVar) {
        String headerField = eVar.getHeaderField("Content-Range");
        if (headerField == null) {
            return -1;
        }
        try {
            return Long.parseLong(headerField.substring(headerField.lastIndexOf(47) + 1).trim());
        } catch (Exception e) {
            return -1;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:49:0x00e0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static b.a.a.c.e a(b.a.a.c.e r8, int r9, int r10, int r11, java.lang.String r12, java.lang.String r13, java.lang.String r14, byte[] r15) {
        /*
            a(r8, r13)     // Catch:{ Exception -> 0x00d5 }
            if (r12 == 0) goto L_0x000a
            java.lang.String r0 = "Range"
            r8.setRequestProperty(r0, r12)     // Catch:{ Exception -> 0x00d5 }
        L_0x000a:
            r8.setRequestMethod(r14)     // Catch:{ Exception -> 0x00d5 }
            java.lang.String r0 = "post"
            boolean r0 = r0.equalsIgnoreCase(r14)     // Catch:{ Exception -> 0x00d5 }
            if (r0 == 0) goto L_0x0021
            if (r15 == 0) goto L_0x0021
            java.io.OutputStream r0 = r8.cw()     // Catch:{ Exception -> 0x00d5 }
            r0.write(r15)     // Catch:{ Exception -> 0x00d5 }
            r0.close()     // Catch:{ Exception -> 0x00d5 }
        L_0x0021:
            int r0 = r8.getResponseCode()     // Catch:{ Exception -> 0x00d5 }
            r1 = 300(0x12c, float:4.2E-43)
            if (r0 < r1) goto L_0x006a
            r1 = 307(0x133, float:4.3E-43)
            if (r0 > r1) goto L_0x006a
            java.lang.String r0 = "Location"
            java.lang.String r0 = r8.getHeaderField(r0)     // Catch:{ Exception -> 0x00d5 }
            java.lang.String r1 = r8.getURL()     // Catch:{ Exception -> 0x00d5 }
            java.lang.String r1 = r1.trim()     // Catch:{ Exception -> 0x00d5 }
            java.lang.String r0 = com.uc.c.ce.au(r1, r0)     // Catch:{ Exception -> 0x00d5 }
            boolean r1 = r1.equals(r0)     // Catch:{ Exception -> 0x00d5 }
            if (r1 == 0) goto L_0x004a
            r8.close()     // Catch:{ Exception -> 0x00d5 }
            r8 = 0
        L_0x0049:
            return r8
        L_0x004a:
            int r2 = r10 + 1
            r1 = 5
            if (r10 >= r1) goto L_0x0065
            b.a.a.c.e r0 = z(r0)     // Catch:{ Exception -> 0x0104 }
            a(r8, r0)     // Catch:{ Exception -> 0x0104 }
            r8.close()     // Catch:{ Exception -> 0x0104 }
            r8 = 0
            r1 = r9
            r3 = r11
            r4 = r12
            r5 = r13
            r6 = r14
            r7 = r15
            b.a.a.c.e r8 = a(r0, r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0104 }
            goto L_0x0049
        L_0x0065:
            r8.close()     // Catch:{ Exception -> 0x0104 }
        L_0x0068:
            r8 = 0
            goto L_0x0049
        L_0x006a:
            r1 = 200(0xc8, float:2.8E-43)
            if (r0 != r1) goto L_0x00a5
            java.lang.String r0 = b(r8)     // Catch:{ Exception -> 0x00d5 }
            int r1 = b.b.a.OR()     // Catch:{ Exception -> 0x00d5 }
            r2 = 3
            if (r1 != r2) goto L_0x0049
            if (r0 == 0) goto L_0x0049
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ Exception -> 0x00d5 }
            java.lang.String r1 = "text/vnd.wap.wml"
            int r0 = r0.indexOf(r1)     // Catch:{ Exception -> 0x00d5 }
            if (r0 < 0) goto L_0x0049
            r0 = 1
            if (r11 >= r0) goto L_0x0049
            java.lang.String r0 = r8.getURL()     // Catch:{ Exception -> 0x00d5 }
            b.a.a.c.e r0 = z(r0)     // Catch:{ Exception -> 0x00d5 }
            a(r8, r0)     // Catch:{ Exception -> 0x00d5 }
            r8.close()     // Catch:{ Exception -> 0x00d5 }
            r8 = 0
            r3 = 1
            r1 = r9
            r2 = r10
            r4 = r12
            r5 = r13
            r6 = r14
            r7 = r15
            b.a.a.c.e r8 = a(r0, r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x00d5 }
            goto L_0x0049
        L_0x00a5:
            r1 = 206(0xce, float:2.89E-43)
            if (r0 != r1) goto L_0x00b5
            long r9 = a(r8)     // Catch:{ Exception -> 0x00d5 }
            r11 = 0
            int r9 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r9 > 0) goto L_0x0049
            r8 = 0
            goto L_0x0049
        L_0x00b5:
            r8.close()     // Catch:{ Exception -> 0x00d5 }
            int r3 = r11 + 1
            r0 = 2
            if (r11 >= r0) goto L_0x0068
            java.lang.String r11 = r8.getURL()     // Catch:{ Exception -> 0x0107 }
            b.a.a.c.e r0 = z(r11)     // Catch:{ Exception -> 0x0107 }
            a(r8, r0)     // Catch:{ Exception -> 0x0107 }
            r8 = 0
            r1 = r9
            r2 = r10
            r4 = r12
            r5 = r13
            r6 = r14
            r7 = r15
            b.a.a.c.e r8 = a(r0, r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0107 }
            goto L_0x0049
        L_0x00d5:
            r0 = move-exception
            r3 = r11
            r2 = r10
        L_0x00d8:
            r8.close()     // Catch:{ IOException -> 0x00fe }
        L_0x00db:
            int r1 = r9 + 1
            r10 = 2
            if (r9 >= r10) goto L_0x0068
            java.lang.String r9 = r8.getURL()
            b.a.a.c.e r0 = z(r9)     // Catch:{ Exception -> 0x00fa }
            a(r8, r0)     // Catch:{ IOException -> 0x0100 }
        L_0x00eb:
            r8 = 1000(0x3e8, double:4.94E-321)
            java.lang.Thread.sleep(r8)     // Catch:{ Exception -> 0x0102 }
        L_0x00f0:
            r4 = r12
            r5 = r13
            r6 = r14
            r7 = r15
            b.a.a.c.e r8 = a(r0, r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0049
        L_0x00fa:
            r8 = move-exception
            r8 = 0
            goto L_0x0049
        L_0x00fe:
            r10 = move-exception
            goto L_0x00db
        L_0x0100:
            r8 = move-exception
            goto L_0x00eb
        L_0x0102:
            r8 = move-exception
            goto L_0x00f0
        L_0x0104:
            r10 = move-exception
            r3 = r11
            goto L_0x00d8
        L_0x0107:
            r11 = move-exception
            r2 = r10
            goto L_0x00d8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.a.a.a(b.a.a.c.e, int, int, int, java.lang.String, java.lang.String, java.lang.String, byte[]):b.a.a.c.e");
    }

    public static void a(e eVar, e eVar2) {
        String headerField;
        StringBuffer stringBuffer = new StringBuffer();
        int i = 1;
        while (true) {
            String headerFieldKey = eVar.getHeaderFieldKey(i);
            if (headerFieldKey == null) {
                break;
            }
            if ("set-cookie".equalsIgnoreCase(headerFieldKey) && (headerField = eVar.getHeaderField(headerFieldKey)) != null && headerField.indexOf(61) > 0) {
                int lastIndexOf = headerField.lastIndexOf(59);
                if (lastIndexOf != -1) {
                    headerField = new String(headerField.substring(0, lastIndexOf));
                }
                stringBuffer.append(headerField).append(';').append(' ');
            }
            i++;
        }
        if (stringBuffer.length() > 0) {
            eVar2.setRequestProperty("Cookie", stringBuffer.toString());
        }
    }

    public static void a(e eVar, String str) {
        eVar.setRequestProperty("Accept", "application/vnd.wap.xhtml+xml,application/xml,text/vnd.wap.wml,text/html,application/xhtml+xml,image/jpeg;q=0.5,image/png;q=0.5,image/gif;q=0.5,image/*;q=0.6,video/*,audio/*,*/*;q=0.6," + cd.bVg + au.aGF + w.NQ);
        eVar.setRequestProperty("User-Agent", bx.hH(az.bcp));
        if (str != null && str.trim().length() > 0) {
            eVar.setRequestProperty("Referer", str.trim());
        }
    }

    public static String b(e eVar) {
        return eVar.getHeaderField("Content-Type");
    }

    public static e z(String str) {
        b a2 = b.a(str, ew);
        a2.setRequestProperty("User-Agent", bx.hH(az.bcp));
        return a2;
    }
}
