package com.uc.c.a;

import b.a.a.c.e;
import com.uc.c.w;
import java.io.File;
import java.util.concurrent.atomic.AtomicLong;

public final class f {
    public static int MM = w.Qt;
    public static final String bWZ = "GET";
    public static final short bXE = 500;
    private static final byte bXF = 10;
    public static final String bXa = "POST";
    public static final byte bXf = 3;
    public static final byte bXg = -1;
    public static final byte bXh = 1;
    public static final byte bXi = 0;
    public static final byte bXj = 2;
    public static final byte bXk = 100;
    public static final byte bXm = -2;
    public static final byte bXn = -1;
    public static final byte bXo = 0;
    static final byte bXv = 3;
    private long Xt = 0;
    private int bCR = -2;
    private boolean bXA = false;
    /* access modifiers changed from: private */
    public byte[] bXB = null;
    /* access modifiers changed from: private */
    public String bXC = null;
    private long bXD = 0;
    private int[] bXG = new int[10];
    private int bXH = 0;
    /* access modifiers changed from: private */
    public boolean bXI = false;
    private int bXJ = 0;
    private int bXK = 0;
    private short bXb;
    /* access modifiers changed from: private */
    public String bXc;
    /* access modifiers changed from: private */
    public String bXd;
    /* access modifiers changed from: private */
    public String bXe;
    /* access modifiers changed from: private */
    public byte bXl = -1;
    /* access modifiers changed from: private */
    public byte bXp = 0;
    /* access modifiers changed from: private */
    public long bXq = 1;
    /* access modifiers changed from: private */
    public AtomicLong bXr = null;
    /* access modifiers changed from: private */
    public String bXs = null;
    /* access modifiers changed from: private */
    public String bXt = null;
    private int bXu;
    /* access modifiers changed from: private */
    public byte bXw;
    /* access modifiers changed from: private */
    public g[] bXx = null;
    /* access modifiers changed from: private */
    public boolean bXy = false;
    /* access modifiers changed from: private */
    public d bXz;
    /* access modifiers changed from: private */
    public int bsj = a.ex;
    /* access modifiers changed from: private */
    public String method = "GET";

    f(d dVar, short s, String str, String str2, String str3, String str4, String str5, String str6, int i, byte[] bArr, String str7) {
        this.bXz = dVar;
        this.bXb = s;
        this.bXc = str;
        this.bXd = str2;
        this.bXe = str3;
        this.bXs = str4;
        this.bXt = str5;
        if (str6 != null) {
            this.method = str6;
        }
        this.bXw = (byte) i;
        this.bXr = new AtomicLong(0);
        this.bXB = bArr;
        this.bXC = str7;
    }

    public f(short s, String str, String str2, String str3, String str4, String str5, String str6, byte[] bArr, String str7) {
        this.bXb = s;
        this.bXc = str;
        this.bXd = str2;
        this.bXe = str3;
        this.bXs = str4;
        this.bXt = str5;
        if (str6 != null) {
            this.method = str6;
        }
        this.bXw = this.bXw;
        this.bXr = new AtomicLong(0);
        this.bXB = bArr;
        this.bXC = str7;
    }

    /* access modifiers changed from: private */
    public long d(e eVar) {
        String headerField = eVar.getHeaderField("content-length");
        if (headerField == null) {
            return -1;
        }
        try {
            return Long.parseLong(headerField.trim());
        } catch (Exception e) {
            return -1;
        }
    }

    private final String kh(int i) {
        int i2 = MM > 240 ? 999999 : 99999;
        String str = MM > 240 ? "1000k+" : "100k+";
        if (i > i2) {
            return str;
        }
        return String.format("%d", Integer.valueOf(i));
    }

    /* access modifiers changed from: package-private */
    public void D(byte b2) {
        this.bXl = b2;
    }

    /* access modifiers changed from: package-private */
    public void E(byte b2) {
        this.bXp = b2;
    }

    public final int OA() {
        return this.bXu;
    }

    /* access modifiers changed from: package-private */
    public g[] OB() {
        return this.bXx;
    }

    /* access modifiers changed from: package-private */
    public AtomicLong OC() {
        return this.bXr;
    }

    public boolean OD() {
        return this.bXy;
    }

    /* access modifiers changed from: package-private */
    public byte[] OE() {
        return this.bXB;
    }

    public final int OF() {
        return this.bCR;
    }

    public final boolean OG() {
        return this.bCR > -2 && this.bCR != 1;
    }

    public final String OH() {
        if (this.bCR == -1 || this.bCR == -2) {
            return "";
        }
        if (this.bXJ < 0 || this.bXK < 0) {
            return null;
        }
        return "顶(" + kh(this.bXJ) + ")";
    }

    /* access modifiers changed from: package-private */
    public final int OI() {
        return this.bXJ;
    }

    /* access modifiers changed from: package-private */
    public final int OJ() {
        return this.bXK;
    }

    public byte OK() {
        return this.bXp;
    }

    public void OL() {
        if (this.bXG != null) {
            long currentTimeMillis = System.currentTimeMillis() - this.Xt;
            if (currentTimeMillis > 0) {
                int i = (int) (((this.bXr.get() - this.bXD) * 1000) / currentTimeMillis);
                if (i < 0) {
                    i = 0;
                }
                if (this.bXH < 10) {
                    int[] iArr = this.bXG;
                    int i2 = this.bXH;
                    this.bXH = i2 + 1;
                    iArr[i2] = i;
                } else {
                    for (int i3 = 0; i3 < 9; i3++) {
                        this.bXG[i3] = this.bXG[i3 + 1];
                    }
                    this.bXG[9] = i;
                }
                int i4 = 0;
                for (int i5 = 0; i5 < this.bXH; i5++) {
                    i4 += this.bXG[i5];
                }
                this.bXu = i4 / this.bXH;
                this.Xt = System.currentTimeMillis();
                this.bXD = this.bXr.get();
            }
        }
    }

    public void OM() {
        this.bXH = 0;
        this.bXD = this.bXr.get();
        if (this.bXG != null) {
            for (int i = 0; i < this.bXG.length; i++) {
                this.bXG[i] = 0;
            }
        }
    }

    public void Oq() {
        this.bXl = 0;
        if (!this.bXy) {
            this.bXr.set(0);
        }
        if (this.bXz != null) {
            this.bXz.g(this);
        }
        boolean z = true;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.bXw) {
                break;
            }
            if (this.bXx[i2].Pp() + this.bXx[i2].Pr() < this.bXx[i2].Pq() || this.bXx[i2].Pq() <= 0) {
                g gVar = new g(this, this.bXc, this.bXe, this.bXd, this.method, this.bXr, i2, this.bXx[i2].Pp() + this.bXx[i2].Pr(), this.bXx[i2].Pq(), 0, this.bXx[i2].Pu(), this.bXB, this.bXC);
                this.bXx[i2] = null;
                this.bXx[i2] = gVar;
                gVar.start();
                z = false;
            } else {
                this.bXx[i2].kv(2);
            }
            i = i2 + 1;
        }
        if (z) {
            this.bXl = 2;
            this.bXp = 0;
            this.bXz.b(this, this.bXw);
            this.bXz.g(this);
        }
    }

    public final boolean Or() {
        return this.bXl == 100;
    }

    public short Os() {
        return this.bXb;
    }

    public String Ot() {
        return this.bXc;
    }

    public String Ou() {
        return this.bXe;
    }

    public byte Ov() {
        return this.bXl;
    }

    public long Ow() {
        return this.bXr.get();
    }

    public long Ox() {
        return this.bXq;
    }

    public String Oy() {
        return this.bXs;
    }

    public byte Oz() {
        return this.bXw;
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(g gVar, boolean z) {
        if (this.bXl == 1) {
            if (this.bXA) {
                File file = new File(this.bXs + this.bXt);
                if (file.exists()) {
                    file.delete();
                }
            }
        } else if (z) {
            try {
                if (this.bXq <= this.bXr.get()) {
                    if (this.bXq < 0) {
                        this.bXq = this.bXr.get();
                    }
                    this.bXI = true;
                    boolean z2 = true;
                    int i = 0;
                    while (true) {
                        if (i >= this.bXw) {
                            break;
                        } else if (this.bXx[i].Ps() != 2) {
                            z2 = false;
                            break;
                        } else {
                            i++;
                        }
                    }
                    if (z2 && this.bXl != 2) {
                        this.bXl = 2;
                        this.bXp = 0;
                        if (this.bXz != null) {
                            this.bXz.b(this, this.bXw);
                            this.bXz.g(this);
                        }
                    }
                } else if (this.bXl != 3) {
                    boolean z3 = false;
                    byte b2 = 0;
                    while (true) {
                        if (b2 < this.bXw) {
                            if (this.bXx[b2].Ps() < 0) {
                                g gVar2 = new g(this, this.bXx[b2].getUrl(), this.bXe, this.bXx[b2].zK(), this.method, this.bXr, b2, this.bXx[b2].Pp() + this.bXx[b2].Pr(), this.bXx[b2].Pq(), this.bXx[b2].Pt() + 1, this.bXx[b2].Pu(), this.bXB, this.bXC);
                                this.bXx[b2] = null;
                                this.bXx[b2] = gVar2;
                                gVar2.start();
                                break;
                            }
                            if (!z3) {
                                if (this.bXx[b2].Ps() == 1) {
                                    z3 = true;
                                }
                            }
                            b2++;
                        } else if (!z3) {
                            this.bXl = 3;
                            if (this.bXy) {
                                this.bXp = -2;
                            } else {
                                this.bXp = -1;
                            }
                            if (this.bXz != null) {
                                this.bXz.b(this, this.bXw);
                                this.bXz.g(this);
                            }
                        }
                    }
                }
            } catch (Exception e) {
            }
        } else if (!this.bXy) {
            this.bXl = 3;
            this.bXp = -1;
            this.bXI = true;
            this.bXz.b(this, this.bXw);
            this.bXz.g(this);
        } else {
            byte Po = gVar.Po();
            g gVar3 = this.bXx[Po];
            if (gVar3.Pt() >= 2 || gVar3.Ps() >= 1) {
                int i2 = 0;
                while (true) {
                    if (i2 < this.bXw) {
                        if (this.bXx[i2].Ps() == 1) {
                            break;
                        }
                        i2++;
                    } else {
                        this.bXl = 3;
                        if (this.bXy) {
                            this.bXp = -2;
                        } else {
                            this.bXp = -1;
                        }
                        if (!this.bXI) {
                            this.bXI = true;
                            if (this.bXz != null) {
                                this.bXz.b(this, this.bXw);
                                this.bXz.g(this);
                            }
                        }
                    }
                }
            } else {
                g gVar4 = new g(this, gVar3.getUrl(), this.bXe, gVar3.zK(), this.method, this.bXr, Po, gVar3.Pp() + gVar3.Pr(), gVar3.Pq(), gVar3.Pt() + 1, gVar3.Pu(), this.bXB, this.bXC);
                this.bXx[Po] = null;
                this.bXx[Po] = gVar4;
                gVar4.start();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(g[] gVarArr) {
        this.bXx = gVarArr;
    }

    public void aM(byte[] bArr) {
        this.bXB = bArr;
    }

    public boolean av(String str, String str2) {
        if (this.bXl != 2 || (str == null && str2 == null)) {
            return false;
        }
        if (str != null) {
            fX(str);
        }
        if (str2 != null) {
            setFileName(str2);
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void c(d dVar) {
        this.bXz = dVar;
    }

    /* access modifiers changed from: package-private */
    public void cE(boolean z) {
        this.bXI = z;
    }

    public void cF(boolean z) {
        this.bXA = z;
    }

    /* access modifiers changed from: package-private */
    public void cG(boolean z) {
        this.bXy = z;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        f fVar = (f) obj;
        if (this.bXc != null ? !this.bXc.equals(fVar.bXc) : fVar.bXc != null) {
            return false;
        }
        if (this.bXs != null ? !this.bXs.equals(fVar.bXs) : fVar.bXs != null) {
            return false;
        }
        return this.bXt != null ? this.bXt.equals(fVar.bXt) : fVar.bXt == null;
    }

    /* access modifiers changed from: package-private */
    public void fX(String str) {
        this.bXs = str;
    }

    public int getBlockSize() {
        return this.bsj;
    }

    public String getContentType() {
        return this.bXC;
    }

    public String getFileName() {
        return this.bXt;
    }

    /* access modifiers changed from: package-private */
    public String getMethod() {
        return this.method;
    }

    public int hashCode() {
        int i = 7 * 61;
        return (((((this.bXc != null ? this.bXc.hashCode() : 0) + 427) * 61) + (this.bXs != null ? this.bXs.hashCode() : 0)) * 61) + (this.bXt != null ? this.bXt.hashCode() : 0);
    }

    /* access modifiers changed from: package-private */
    public boolean isFinished() {
        if (this.bXx != null) {
            for (int i = 0; i < this.bXw; i++) {
                if (this.bXx[i] != null && !this.bXx[i].isChecked()) {
                    return false;
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void kd(int i) {
        this.bXw = (byte) i;
    }

    public final void ke(int i) {
        this.bCR = i;
    }

    public final void kf(int i) {
        this.bXJ = i;
    }

    public final void kg(int i) {
        this.bXK = i;
    }

    public void pause() {
        if (this.bXl == 0) {
            for (int i = 0; i < this.bXw; i++) {
                this.bXx[i].Pv();
            }
        }
        this.bXl = 1;
    }

    /* access modifiers changed from: package-private */
    public void setContentType(String str) {
        this.bXC = str;
    }

    /* access modifiers changed from: package-private */
    public void setFileName(String str) {
        this.bXt = str;
    }

    /* access modifiers changed from: package-private */
    public void setMethod(String str) {
        this.method = str;
    }

    /* access modifiers changed from: package-private */
    public void start() {
        new c(this).start();
    }

    /* access modifiers changed from: package-private */
    public void t(long j) {
        this.bXr.set(j);
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append((int) this.bXb).append(' ').append(this.bXc).append(' ').append(this.bXe).append(' ').append(this.bXy).append(' ').append(this.bXs).append(this.bXt).append(' ').append(this.bXq).append(' ').append(this.bXr).append(' ').append(this.method).append(' ').append(this.bXd).append(' ');
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public void u(long j) {
        this.bXq = j;
    }

    public String zK() {
        return this.bXd;
    }
}
