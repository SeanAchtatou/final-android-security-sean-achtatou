package com.uc.c.a;

import b.a.a.c.e;
import b.b.a;
import com.uc.b.h;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.concurrent.atomic.AtomicLong;

public class g extends Thread {
    public static final short[] caU = {8, 16, 32};
    private String GP;
    private boolean aO = true;
    private byte[] bXB = null;
    private String bXC = null;
    private String bXd;
    private String bXe;
    private AtomicLong bXr;
    private f caV;
    private byte caW;
    private long caX;
    private long caY;
    private long caZ;
    private byte cba = 1;
    private byte cbb = 0;
    private boolean cbc = false;
    private int cbd = a.ex;
    private boolean cbe = true;
    private e cbf = null;
    private RandomAccessFile cbg = null;
    private String method = null;
    private InputStream ru = null;

    public g(f fVar, String str, String str2, String str3, String str4, AtomicLong atomicLong, int i, long j, long j2, int i2, boolean z, e eVar, byte[] bArr, String str5) {
        this.caV = fVar;
        this.GP = str;
        this.bXe = str2;
        this.bXd = str3;
        this.method = str4;
        this.bXr = atomicLong;
        this.caW = (byte) i;
        this.caX = j;
        this.caY = j2;
        this.cbb = (byte) i2;
        this.cbc = z;
        this.cbf = eVar;
        this.bXB = bArr;
        this.bXC = str5;
    }

    public g(f fVar, String str, String str2, String str3, String str4, AtomicLong atomicLong, int i, long j, long j2, int i2, boolean z, byte[] bArr, String str5) {
        this.caV = fVar;
        this.GP = str;
        this.bXe = str2;
        this.bXd = str3;
        this.method = str4;
        this.bXr = atomicLong;
        this.caW = (byte) i;
        this.caX = j;
        this.caY = j2;
        this.cbb = (byte) i2;
        this.cbc = z;
        this.bXB = bArr;
        this.bXC = str5;
    }

    private void Pw() {
        if (this.cbg != null) {
            try {
                this.cbg.close();
            } catch (Exception e) {
            }
            this.cbg = null;
        }
    }

    private void Px() {
        boolean z;
        long j;
        byte[] bArr;
        boolean z2;
        long j2;
        if (this.caX != 0) {
            long Ox = this.caV.Ox();
            long Oz = (long) ((Ox >= 921600 ? (int) (Ox / ((long) this.caV.Oz())) : 307200) * this.caW);
            if (this.caX == Oz) {
                this.cbg.seek(this.caX);
                return;
            }
            long j3 = this.caX;
            long length = this.cbg.length();
            if (length == 0) {
                this.cbg.seek(Oz);
                this.bXr.addAndGet(-(this.caX - Oz));
                this.caX = Oz;
                return;
            }
            if (j3 > length) {
                z = true;
                j = length;
            } else {
                z = true;
                j = j3;
            }
            while (z) {
                byte[] bArr2 = new byte[(caU[0] << 10)];
                long length2 = j - ((long) bArr2.length);
                if (length2 < Oz) {
                    bArr = new byte[((int) (j - Oz))];
                    z2 = false;
                    j2 = Oz;
                } else {
                    long j4 = length2;
                    bArr = bArr2;
                    z2 = z;
                    j2 = j4;
                }
                this.cbg.seek(j2);
                this.cbg.readFully(bArr);
                for (int length3 = bArr.length - 1; length3 >= 0; length3--) {
                    if (bArr[length3] != 0) {
                        this.cbg.seek(((long) length3) + j2 + 1);
                        this.bXr.addAndGet(-(((this.caX - j2) - ((long) length3)) - 1));
                        this.caX = ((long) length3) + j2 + 1;
                        return;
                    }
                }
                if (!z2) {
                    this.cbg.seek(Oz);
                    this.bXr.addAndGet(-(this.caX - Oz));
                    this.caX = Oz;
                    return;
                }
                j -= (long) bArr.length;
                if (j <= Oz) {
                    this.cbg.seek(Oz);
                    this.bXr.addAndGet(-(this.caX - Oz));
                    this.caX = Oz;
                    return;
                }
                z = z2;
            }
        }
    }

    public byte Po() {
        return this.caW;
    }

    public long Pp() {
        return this.caX;
    }

    public long Pq() {
        return this.caY;
    }

    public long Pr() {
        return this.caZ;
    }

    public byte Ps() {
        return this.cba;
    }

    public byte Pt() {
        return this.cbb;
    }

    public boolean Pu() {
        return this.cbc;
    }

    /* access modifiers changed from: package-private */
    public void Pv() {
        cK(false);
        interrupt();
        if (this.cbf != null) {
            try {
                this.cbf.close();
            } catch (Exception e) {
            }
            this.cbf = null;
        }
        if (this.ru != null) {
            try {
                this.ru.close();
            } catch (Exception e2) {
            }
        }
        Pw();
    }

    /* access modifiers changed from: package-private */
    public void aM(byte[] bArr) {
        this.bXB = bArr;
    }

    public void cK(boolean z) {
        this.cbe = z;
    }

    public String getMethod() {
        return this.method;
    }

    public String getUrl() {
        return this.GP;
    }

    /* access modifiers changed from: package-private */
    public boolean isChecked() {
        return this.aO;
    }

    /* access modifiers changed from: package-private */
    public void kv(int i) {
        this.cba = (byte) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.a.f.a(com.uc.c.a.g, boolean):void
     arg types: [com.uc.c.a.g, int]
     candidates:
      com.uc.c.a.f.a(com.uc.c.a.f, byte):byte
      com.uc.c.a.f.a(com.uc.c.a.f, long):long
      com.uc.c.a.f.a(com.uc.c.a.f, b.a.a.c.e):long
      com.uc.c.a.f.a(com.uc.c.a.f, com.uc.c.a.g[]):com.uc.c.a.g[]
      com.uc.c.a.f.a(com.uc.c.a.g, boolean):void */
    public void run() {
        int i;
        byte[] bArr;
        int read;
        int read2;
        this.cba = 1;
        try {
            if (this.caX < this.caY || this.caY <= 0) {
                this.cbg = new RandomAccessFile(this.caV.Oy() + this.caV.getFileName(), "rw");
                byte[] bArr2 = new byte[(caU[1] << 10)];
                if (!this.cbc) {
                    if (this.cbf == null) {
                        this.cbf = a.z(this.GP);
                        if (this.bXd != null && this.bXd.trim().length() > 0) {
                            this.cbf.setRequestProperty("Cookie", this.bXd.trim());
                        }
                        if (this.bXe != null && this.bXe.trim().length() > 0) {
                            this.cbf.setRequestProperty("Referer", this.bXe.trim());
                        }
                        if ("post".equalsIgnoreCase(this.method) && this.bXC != null && this.bXC.trim().length() > 0) {
                            this.cbf.setRequestProperty("Content-Type", this.bXC.trim());
                        }
                        this.cbf = a.a(this.cbf, 0, 0, 0, null, this.bXe, this.method, this.bXB);
                    }
                    this.ru = this.cbf.ez();
                    byte[] bArr3 = bArr2;
                    int i2 = 0;
                    boolean z = true;
                    long currentTimeMillis = System.currentTimeMillis();
                    while (this.cbe && this.caV.Ox() > this.bXr.get() && z) {
                        int i3 = 0;
                        do {
                            read2 = this.ru.read(bArr3, i3, bArr3.length - i3);
                            if (-1 == read2) {
                                break;
                            }
                            i3 += read2;
                        } while (i3 != bArr3.length);
                        if (read2 == -1) {
                            z = false;
                        }
                        if (i3 != 0) {
                            this.cbb = 0;
                            this.cbg.write(bArr3, 0, i3);
                            this.bXr.addAndGet((long) i3);
                            this.caZ += (long) i3;
                            i2 += i3;
                            if (i2 > this.cbd) {
                                long currentTimeMillis2 = System.currentTimeMillis();
                                long j = currentTimeMillis2 - currentTimeMillis;
                                if (j <= 0) {
                                    j = 1;
                                }
                                int i4 = (int) (((long) i2) / j);
                                if (i4 < 33) {
                                    if (bArr3.length != (caU[0] << 10)) {
                                        if (a.OT()) {
                                            bArr3 = new byte[(caU[1] << 10)];
                                            i2 = 0;
                                            currentTimeMillis = currentTimeMillis2;
                                        } else {
                                            bArr3 = new byte[(caU[0] << 10)];
                                            i2 = 0;
                                            currentTimeMillis = currentTimeMillis2;
                                        }
                                    }
                                } else if (i4 < 65) {
                                    if (bArr3.length != (caU[1] << 10)) {
                                        bArr3 = new byte[(caU[1] << 10)];
                                        i2 = 0;
                                        currentTimeMillis = currentTimeMillis2;
                                    }
                                } else if (bArr3.length != (caU[2] << 10)) {
                                    bArr3 = new byte[(caU[2] << 10)];
                                    i2 = 0;
                                    currentTimeMillis = currentTimeMillis2;
                                }
                                i2 = 0;
                                currentTimeMillis = currentTimeMillis2;
                            }
                            if (this.caV.Ox() == this.bXr.get() || this.caZ == this.caY - this.caX) {
                                break;
                            }
                        }
                    }
                    this.ru.close();
                    this.ru = null;
                    this.cbg.close();
                    this.cbg = null;
                    this.cbf.close();
                    this.cba = 2;
                    if (this.cbe) {
                        this.caV.a(this, true);
                    }
                } else {
                    this.aO = false;
                    Px();
                    this.aO = true;
                    byte[] bArr4 = bArr2;
                    boolean z2 = true;
                    int i5 = 1;
                    while (this.cbe && this.caV.Ox() > this.bXr.get() && this.caZ < this.caY - this.caX) {
                        StringBuffer stringBuffer = new StringBuffer();
                        long j2 = this.caX + this.caZ;
                        long j3 = ((307200 * ((long) i5)) + j2) - 1;
                        if (j3 > this.caY) {
                            j3 = this.caY;
                        }
                        stringBuffer.append("bytes=").append(j2).append('-').append(j3);
                        if (this.cbf == null) {
                            this.cbf = a.z(this.GP);
                            if (this.bXd != null && this.bXd.trim().length() > 0) {
                                this.cbf.setRequestProperty("Cookie", this.bXd);
                            }
                            if ("post".equalsIgnoreCase(this.method) && this.bXC != null && this.bXC.trim().length() > 0) {
                                this.cbf.setRequestProperty("Content-Type", this.bXC);
                            }
                            this.cbf = a.a(this.cbf, 0, 0, 0, stringBuffer.toString(), this.bXe, this.method, this.bXB);
                            this.GP = this.cbf.getURL();
                            this.bXd = this.cbf.getRequestProperty("Cookie");
                        } else if (!z2) {
                            e z3 = a.z(this.GP);
                            String requestProperty = this.cbf.getRequestProperty("Cookie");
                            if (requestProperty != null && requestProperty.trim().length() > 0) {
                                z3.setRequestProperty("Cookie", requestProperty);
                            }
                            String str = this.bXC;
                            if ("post".equalsIgnoreCase(this.method) && this.bXC == null && this.bXC.trim().length() > 0) {
                                z3.setRequestProperty("Content-Type", str);
                            }
                            this.cbf.close();
                            this.cbf = a.a(z3, 0, 0, 0, stringBuffer.toString(), this.bXe, this.method, this.bXB);
                            this.GP = this.cbf.getURL().toString();
                            this.bXd = this.cbf.getRequestProperty("Cookie");
                        }
                        boolean z4 = z2 ? false : z2;
                        if (this.cbf.getResponseCode() != 206) {
                            throw new IOException("response code is not 206 error.");
                        }
                        this.ru = this.cbf.ez();
                        long currentTimeMillis3 = System.currentTimeMillis();
                        long j4 = this.caZ;
                        boolean z5 = true;
                        while (true) {
                            if (!this.cbe || this.caV.Ox() <= this.bXr.get() || !z5) {
                                break;
                            }
                            int i6 = 0;
                            do {
                                read = this.ru.read(bArr4, i6, bArr4.length - i6);
                                if (-1 == read) {
                                    break;
                                }
                                i6 += read;
                            } while (i6 != bArr4.length);
                            if (read == -1) {
                                z5 = false;
                            }
                            if (i6 != 0) {
                                this.cbg.write(bArr4, 0, i6);
                                this.cbb = 0;
                                if (!this.cbe) {
                                    break;
                                }
                                this.bXr.addAndGet((long) i6);
                                this.caZ += (long) i6;
                                if (this.caV.Ox() == this.bXr.get()) {
                                    break;
                                }
                            }
                        }
                        long currentTimeMillis4 = System.currentTimeMillis() - currentTimeMillis3;
                        if (currentTimeMillis4 <= 0) {
                            currentTimeMillis4 = 1;
                        }
                        int i7 = (int) ((this.caZ - j4) / currentTimeMillis4);
                        if (i7 < 33) {
                            i = 1;
                            if (bArr4.length != (caU[0] << 10)) {
                                if (a.OT()) {
                                    i = 2;
                                    bArr = new byte[(caU[1] << 10)];
                                } else {
                                    bArr = new byte[(caU[0] << 10)];
                                }
                            }
                            bArr = bArr4;
                        } else if (i7 < 65) {
                            i = 2;
                            if (bArr4.length != (caU[1] << 10)) {
                                bArr = new byte[(caU[1] << 10)];
                            }
                            bArr = bArr4;
                        } else {
                            i = 3;
                            if (bArr4.length != (caU[2] << 10)) {
                                bArr = new byte[(caU[2] << 10)];
                            }
                            bArr = bArr4;
                        }
                        this.ru.close();
                        z2 = z4;
                        bArr4 = bArr;
                        i5 = i;
                    }
                    if (this.cbf != null) {
                        this.cbf.close();
                    }
                    this.cbg.close();
                    this.cbg = null;
                    this.cba = 2;
                    if (this.cbe) {
                        this.caV.a(this, true);
                    }
                }
                Pw();
                if (this.ru != null) {
                    try {
                        this.ru.close();
                    } catch (Exception e) {
                    }
                    this.ru = null;
                }
                if (this.cbf != null) {
                    try {
                        this.cbf.close();
                    } catch (IOException e2) {
                    }
                    this.cbf = null;
                }
                this.bXB = null;
                this.aO = true;
                return;
            }
            this.cba = 2;
            this.caV.a(this, true);
            Pw();
            if (this.ru != null) {
                try {
                    this.ru.close();
                } catch (Exception e3) {
                }
                this.ru = null;
            }
            if (this.cbf != null) {
                try {
                    this.cbf.close();
                } catch (IOException e4) {
                }
                this.cbf = null;
            }
            this.bXB = null;
            this.aO = true;
        } catch (Exception e5) {
            this.cba = -1;
            if (this.cbe) {
                if (h.QK() == 0) {
                    this.cbb = 2;
                }
                this.caV.a(this, false);
            }
            Pw();
            if (this.ru != null) {
                try {
                    this.ru.close();
                } catch (Exception e6) {
                }
                this.ru = null;
            }
            if (this.cbf != null) {
                try {
                    this.cbf.close();
                } catch (IOException e7) {
                }
                this.cbf = null;
            }
            this.bXB = null;
            this.aO = true;
        } catch (Throwable th) {
            Pw();
            if (this.ru != null) {
                try {
                    this.ru.close();
                } catch (Exception e8) {
                }
                this.ru = null;
            }
            if (this.cbf != null) {
                try {
                    this.cbf.close();
                } catch (IOException e9) {
                }
                this.cbf = null;
            }
            this.bXB = null;
            this.aO = true;
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public void setContentType(String str) {
        this.bXC = str;
    }

    /* access modifiers changed from: package-private */
    public void setMethod(String str) {
        this.method = str;
    }

    public String toString() {
        return "DownloadThread [url=" + this.GP + ", threadId=" + ((int) this.caW) + ", from=" + this.caX + ", end=" + this.caY + ", taskSeq=" + ((int) this.caV.Os()) + "]";
    }

    public String zK() {
        return this.bXd;
    }
}
