package com.uc.c.a;

import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicInteger;

public class d {
    private static final byte aFB = 0;
    private static final byte aFC = 1;
    private static final byte aFD = 2;
    private static final byte aFE = 3;
    private static final byte aFF = 4;
    private static final byte aFG = 5;
    private static final byte aFH = 6;
    private static final byte aFI = 7;
    private static final byte aFJ = 8;
    static d aFq = new d();
    private Timer aFA = null;
    /* access modifiers changed from: private */
    public Vector aFr = new Vector();
    private Vector aFs = new Vector();
    private Vector aFt = new Vector();
    private Vector aFu = new Vector();
    private int aFv = 9;
    private int aFw = 1;
    private AtomicInteger aFx = new AtomicInteger();
    /* access modifiers changed from: private */
    public b aFy = null;
    private short aFz = 0;

    private d() {
        zp();
    }

    private synchronized int a(f fVar, boolean z, int i) {
        int i2;
        i2 = 0;
        switch (i) {
            case 0:
                zn();
                break;
            case 1:
                if (i(fVar) > 1) {
                    zn();
                    break;
                }
                break;
            case 2:
                if (d(fVar, z)) {
                    zn();
                    break;
                }
                break;
            case 3:
                if (b(fVar, z)) {
                    zn();
                    break;
                }
                break;
            case 4:
                if (k(fVar)) {
                    zn();
                    break;
                }
                break;
            case 5:
                if (m(fVar)) {
                    zn();
                    break;
                }
                break;
            case 6:
                bg(z);
                zq();
                break;
            case 7:
                i2 = zt();
                zq();
                break;
            case 8:
                if (zu()) {
                    zn();
                    break;
                }
                break;
        }
        return i2;
    }

    private boolean b(f fVar, boolean z) {
        int indexOf = this.aFr.indexOf(fVar);
        if (-1 == indexOf) {
            int indexOf2 = this.aFs.indexOf(fVar);
            if (-1 == indexOf2) {
                return false;
            }
            fVar.pause();
            this.aFs.remove(indexOf2);
            this.aFu.add(fVar);
            return false;
        } else if (!z && !fVar.OD()) {
            return false;
        } else {
            fVar.pause();
            this.aFx.addAndGet(-fVar.Oz());
            this.aFr.remove(indexOf);
            this.aFu.add(fVar);
            return true;
        }
    }

    private void bg(boolean z) {
        while (this.aFr.size() > 0) {
            int size = this.aFr.size() - 1;
            f fVar = (f) this.aFr.get(size);
            fVar.pause();
            if (z) {
                fVar.cF(true);
                File file = new File(fVar.Oy() + fVar.getFileName());
                if (file.exists() && file.delete()) {
                }
            }
            this.aFx.addAndGet(-fVar.Oz());
            this.aFr.remove(size);
        }
        while (this.aFs.size() > 0) {
            int size2 = this.aFs.size() - 1;
            f fVar2 = (f) this.aFs.get(size2);
            if (z) {
                fVar2.cF(true);
                File file2 = new File(fVar2.Oy() + fVar2.getFileName());
                if (file2.exists() && file2.delete()) {
                }
            }
            this.aFs.remove(size2);
        }
        while (this.aFu.size() > 0) {
            int size3 = this.aFu.size() - 1;
            f fVar3 = (f) this.aFu.get(size3);
            if (z) {
                fVar3.cF(true);
                File file3 = new File(fVar3.Oy() + fVar3.getFileName());
                if (file3.exists() && file3.delete()) {
                }
            }
            this.aFu.remove(size3);
        }
    }

    private boolean d(f fVar, boolean z) {
        int indexOf = this.aFr.indexOf(fVar);
        if (-1 != indexOf) {
            fVar.cF(true);
            fVar.pause();
            this.aFx.addAndGet(-fVar.Oz());
            this.aFr.remove(indexOf);
            if (z) {
                File file = new File(fVar.Oy() + fVar.getFileName());
                if (!file.exists() || !file.delete()) {
                }
            }
            return true;
        }
        int indexOf2 = this.aFs.indexOf(fVar);
        if (-1 != indexOf2) {
            this.aFs.remove(indexOf2);
            if (z) {
                File file2 = new File(fVar.Oy() + fVar.getFileName());
                if (file2.exists()) {
                    file2.delete();
                }
            }
            return true;
        }
        int indexOf3 = this.aFu.indexOf(fVar);
        if (-1 != indexOf3) {
            this.aFu.remove(indexOf3);
            if (z) {
                File file3 = new File(fVar.Oy() + fVar.getFileName());
                if (file3.exists()) {
                    file3.delete();
                }
            }
            return true;
        }
        int indexOf4 = this.aFt.indexOf(fVar);
        if (-1 == indexOf4) {
            return false;
        }
        this.aFt.remove(indexOf4);
        if (z) {
            File file4 = new File(fVar.Oy() + fVar.getFileName());
            if (file4.exists()) {
                file4.delete();
            }
        }
        return true;
    }

    private int i(f fVar) {
        File file = new File(fVar.Oy());
        if (!file.exists()) {
            file.mkdirs();
        }
        File file2 = new File(fVar.Oy() + fVar.getFileName());
        if (!file2.exists()) {
            try {
                file2.createNewFile();
            } catch (IOException e) {
            }
            fVar.c(this);
            fVar.D((byte) -1);
            fVar.u(1);
            this.aFs.add(fVar);
            return 2;
        }
        File file3 = file2;
        int i = 1;
        while (file3.exists()) {
            StringBuffer stringBuffer = new StringBuffer(fVar.Oy() + fVar.getFileName());
            int lastIndexOf = stringBuffer.lastIndexOf(".");
            if (lastIndexOf != -1) {
                stringBuffer.insert(lastIndexOf, "[" + i + "]");
                i++;
            } else {
                stringBuffer.append("[").append(i).append("]");
                i++;
            }
            file3 = new File(stringBuffer.toString());
        }
        try {
            file3.createNewFile();
        } catch (Exception e2) {
        }
        fVar.setFileName(file3.getName());
        fVar.c(this);
        fVar.D((byte) -1);
        fVar.u(1);
        this.aFs.add(fVar);
        return 3;
    }

    private boolean k(f fVar) {
        int indexOf = this.aFu.indexOf(fVar);
        if (-1 == indexOf || !fVar.isFinished()) {
            return false;
        }
        this.aFu.remove(indexOf);
        this.aFs.add(fVar);
        fVar.D((byte) -1);
        fVar.cE(false);
        return true;
    }

    private boolean m(f fVar) {
        int indexOf = this.aFt.indexOf(fVar);
        if (-1 != indexOf) {
            this.aFt.remove(indexOf);
            fVar.D((byte) -1);
            fVar.t(0);
            fVar.cE(false);
            this.aFs.add(fVar);
            return true;
        }
        int indexOf2 = this.aFu.indexOf(fVar);
        if (-1 != indexOf2) {
            this.aFu.remove(indexOf2);
            fVar.D((byte) -1);
            fVar.E((byte) -1);
            fVar.t(0);
            fVar.cE(false);
            this.aFs.add(fVar);
            return true;
        } else if (-1 == this.aFs.indexOf(fVar)) {
            return false;
        } else {
            fVar.D((byte) -1);
            fVar.E((byte) -1);
            fVar.t(0);
            fVar.cE(false);
            return true;
        }
    }

    public static d zm() {
        return aFq;
    }

    private void zn() {
        int i;
        int i2 = 0;
        while (i2 < this.aFr.size()) {
            f fVar = (f) this.aFr.get(i2);
            byte Ov = fVar.Ov();
            if (Ov == 2) {
                this.aFt.add(fVar);
                this.aFr.remove(i2);
                i = i2;
            } else if (Ov == 3) {
                this.aFu.add(fVar);
                this.aFr.remove(i2);
                i = i2;
            } else {
                i = i2 + 1;
            }
            i2 = i;
        }
        while (this.aFr.size() < this.aFw && 0 < this.aFs.size()) {
            f fVar2 = (f) this.aFs.get(0);
            this.aFs.remove(0);
            this.aFr.add(fVar2);
            byte OK = fVar2.OK();
            if (OK == 0 || OK == -1) {
                int i3 = this.aFv - this.aFx.get();
                if (i3 > 3) {
                    fVar2.kd(3);
                    this.aFx.addAndGet(3);
                } else if (i3 > 0) {
                    fVar2.kd(i3);
                    this.aFx.addAndGet(i3);
                } else {
                    fVar2.kd(1);
                    this.aFx.addAndGet(1);
                }
                fVar2.t(0);
                fVar2.start();
            } else {
                byte Oz = fVar2.Oz();
                fVar2.cE(false);
                this.aFx.addAndGet(Oz);
                fVar2.Oq();
            }
        }
        zq();
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0112 A[SYNTHETIC, Splitter:B:34:0x0112] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0117 A[SYNTHETIC, Splitter:B:37:0x0117] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01a3 A[SYNTHETIC, Splitter:B:53:0x01a3] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01a8 A[SYNTHETIC, Splitter:B:56:0x01a8] */
    /* JADX WARNING: Removed duplicated region for block: B:88:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void zp() {
        /*
            r39 = this;
            byte[] r4 = com.uc.c.az.By()
            if (r4 != 0) goto L_0x0007
        L_0x0006:
            return
        L_0x0007:
            r5 = 0
            r6 = 0
            java.io.ByteArrayInputStream r32 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x01d8, all -> 0x01c4 }
            r0 = r32
            r1 = r4
            r0.<init>(r1)     // Catch:{ Exception -> 0x01d8, all -> 0x01c4 }
            java.io.DataInputStream r33 = new java.io.DataInputStream     // Catch:{ Exception -> 0x01dc, all -> 0x01cb }
            r0 = r33
            r1 = r32
            r0.<init>(r1)     // Catch:{ Exception -> 0x01dc, all -> 0x01cb }
            short r4 = r33.readShort()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r4
            r1 = r39
            r1.aFz = r0     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            int r4 = r33.readInt()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r4
            r1 = r39
            r1.aFw = r0     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            int r4 = r33.readInt()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r4
            r1 = r39
            r1.aFv = r0     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            int r34 = r33.readInt()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r4 = 0
            r23 = r4
        L_0x003c:
            r0 = r23
            r1 = r34
            if (r0 >= r1) goto L_0x01ac
            short r6 = r33.readShort()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            java.lang.String r7 = r33.readUTF()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            java.lang.String r8 = r33.readUTF()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            java.lang.String r12 = r33.readUTF()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            java.lang.String r9 = r33.readUTF()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            java.lang.String r10 = r33.readUTF()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            java.lang.String r11 = r33.readUTF()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            byte r16 = r33.readByte()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            byte r17 = r33.readByte()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            long r18 = r33.readLong()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            java.lang.String r4 = r33.readUTF()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            int r5 = r4.length()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            if (r5 != 0) goto L_0x01e5
            r4 = 0
            r15 = r4
        L_0x0076:
            int r4 = r33.readInt()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r5 = 0
            if (r4 == 0) goto L_0x01e2
            byte[] r5 = new byte[r4]     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r13 = 0
            r0 = r33
            r1 = r5
            r2 = r13
            r3 = r4
            r0.read(r1, r2, r3)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r14 = r5
        L_0x0089:
            int r35 = r33.readInt()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            int r36 = r33.readInt()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            int r37 = r33.readInt()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            long r20 = r33.readLong()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            boolean r22 = r33.readBoolean()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            if (r17 == 0) goto L_0x00a5
            r4 = -1
            r0 = r17
            r1 = r4
            if (r0 != r1) goto L_0x0121
        L_0x00a5:
            com.uc.c.a.f r4 = new com.uc.c.a.f     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r13 = 3
            r5 = r39
            r4.<init>(r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r4
            r1 = r18
            r0.u(r1)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r4
            r1 = r35
            r0.ke(r1)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r4
            r1 = r36
            r0.kf(r1)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r4
            r1 = r37
            r0.kg(r1)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r4
            r1 = r17
            r0.E(r1)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r4
            r1 = r22
            r0.cG(r1)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r5 = 2
            r0 = r16
            r1 = r5
            if (r0 != r1) goto L_0x00ef
            r5 = 2
            r4.D(r5)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r4
            r1 = r18
            r0.t(r1)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r39
            java.util.Vector r0 = r0.aFt     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r5 = r0
            r5.add(r4)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
        L_0x00e9:
            int r4 = r23 + 1
            r23 = r4
            goto L_0x003c
        L_0x00ef:
            r5 = 3
            r0 = r16
            r1 = r5
            if (r0 != r1) goto L_0x011f
            r5 = 3
        L_0x00f6:
            r4.D(r5)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r4
            r1 = r20
            r0.t(r1)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r39
            java.util.Vector r0 = r0.aFu     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r5 = r0
            r5.add(r4)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            goto L_0x00e9
        L_0x0108:
            r4 = move-exception
            r4 = r33
            r5 = r32
        L_0x010d:
            r39.zo()     // Catch:{ all -> 0x01d0 }
            if (r4 == 0) goto L_0x0115
            r4.close()     // Catch:{ Exception -> 0x01bd }
        L_0x0115:
            if (r5 == 0) goto L_0x0006
            r5.close()     // Catch:{ IOException -> 0x011c }
            goto L_0x0006
        L_0x011c:
            r4 = move-exception
            goto L_0x0006
        L_0x011f:
            r5 = 1
            goto L_0x00f6
        L_0x0121:
            byte r13 = r33.readByte()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            com.uc.c.a.f r4 = new com.uc.c.a.f     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r5 = r39
            r4.<init>(r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r5 = 3
            r0 = r16
            r1 = r5
            if (r0 != r1) goto L_0x017b
            r5 = 3
        L_0x0133:
            r4.D(r5)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r4
            r1 = r17
            r0.E(r1)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r4
            r1 = r20
            r0.t(r1)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r4
            r1 = r22
            r0.cG(r1)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r4
            r1 = r18
            r0.u(r1)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            com.uc.c.a.g[] r5 = new com.uc.c.a.g[r13]     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r6 = 0
        L_0x0151:
            if (r6 >= r13) goto L_0x017d
            long r24 = r33.readLong()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            long r26 = r33.readLong()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            com.uc.c.a.g r16 = new com.uc.c.a.g     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            java.util.concurrent.atomic.AtomicLong r22 = r4.OC()     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r28 = 0
            r29 = 1
            r17 = r4
            r18 = r7
            r19 = r9
            r20 = r8
            r21 = r12
            r30 = r14
            r31 = r15
            r16.<init>(r17, r18, r19, r20, r21, r22, r23, r24, r26, r28, r29, r30, r31)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r5[r6] = r16     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            int r6 = r6 + 1
            goto L_0x0151
        L_0x017b:
            r5 = 1
            goto L_0x0133
        L_0x017d:
            r4.a(r5)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r4
            r1 = r35
            r0.ke(r1)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r4
            r1 = r36
            r0.kf(r1)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r4
            r1 = r37
            r0.kg(r1)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r0 = r39
            java.util.Vector r0 = r0.aFu     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            r5 = r0
            r5.add(r4)     // Catch:{ Exception -> 0x0108, all -> 0x019c }
            goto L_0x00e9
        L_0x019c:
            r4 = move-exception
            r5 = r33
            r6 = r32
        L_0x01a1:
            if (r5 == 0) goto L_0x01a6
            r5.close()     // Catch:{ Exception -> 0x01c0 }
        L_0x01a6:
            if (r6 == 0) goto L_0x01ab
            r6.close()     // Catch:{ IOException -> 0x01c2 }
        L_0x01ab:
            throw r4
        L_0x01ac:
            if (r33 == 0) goto L_0x01b1
            r33.close()     // Catch:{ Exception -> 0x01bb }
        L_0x01b1:
            if (r32 == 0) goto L_0x0006
            r32.close()     // Catch:{ IOException -> 0x01b8 }
            goto L_0x0006
        L_0x01b8:
            r4 = move-exception
            goto L_0x0006
        L_0x01bb:
            r4 = move-exception
            goto L_0x01b1
        L_0x01bd:
            r4 = move-exception
            goto L_0x0115
        L_0x01c0:
            r5 = move-exception
            goto L_0x01a6
        L_0x01c2:
            r5 = move-exception
            goto L_0x01ab
        L_0x01c4:
            r4 = move-exception
            r38 = r6
            r6 = r5
            r5 = r38
            goto L_0x01a1
        L_0x01cb:
            r4 = move-exception
            r5 = r6
            r6 = r32
            goto L_0x01a1
        L_0x01d0:
            r6 = move-exception
            r38 = r6
            r6 = r5
            r5 = r4
            r4 = r38
            goto L_0x01a1
        L_0x01d8:
            r4 = move-exception
            r4 = r6
            goto L_0x010d
        L_0x01dc:
            r4 = move-exception
            r4 = r6
            r5 = r32
            goto L_0x010d
        L_0x01e2:
            r14 = r5
            goto L_0x0089
        L_0x01e5:
            r15 = r4
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.a.d.zp():void");
    }

    private int zt() {
        int i;
        int i2 = 0;
        while (true) {
            i = i2;
            if (this.aFs.size() <= 0) {
                break;
            }
            int size = this.aFs.size() - 1;
            f fVar = (f) this.aFs.get(size);
            fVar.pause();
            this.aFs.remove(size);
            this.aFu.add(fVar);
            i2 = i + 1;
        }
        while (this.aFr.size() > 0) {
            int size2 = this.aFr.size() - 1;
            f fVar2 = (f) this.aFr.get(size2);
            fVar2.pause();
            this.aFx.addAndGet(-fVar2.Oz());
            this.aFr.remove(size2);
            this.aFu.add(fVar2);
            i++;
        }
        return i;
    }

    public void a(b bVar) {
        this.aFy = bVar;
    }

    public void a(f fVar, boolean z) {
        a(fVar, z, 3);
    }

    /* access modifiers changed from: package-private */
    public void b(f fVar, int i) {
        this.aFx.addAndGet(-i);
        a(null, false, 0);
    }

    public void bf(boolean z) {
        a(null, z, 6);
    }

    public void c(f fVar, boolean z) {
        a(fVar, z, 2);
    }

    public void fx(int i) {
        boolean z = i > this.aFw;
        this.aFw = i;
        if (z) {
            a(null, false, 0);
        }
    }

    /* access modifiers changed from: package-private */
    public void g(f fVar) {
        if (this.aFy != null) {
            switch (fVar.Ov()) {
                case 0:
                    this.aFy.e(fVar);
                    n(fVar);
                    return;
                case 1:
                    zD();
                    return;
                case 2:
                    a(null, false, 0);
                    this.aFy.b(fVar);
                    zD();
                    return;
                case 3:
                    a(null, false, 0);
                    this.aFy.a(fVar);
                    zD();
                    return;
                default:
                    return;
            }
        }
    }

    public void h(f fVar) {
        a(fVar, false, 1);
    }

    public void j(f fVar) {
        a(fVar, false, 4);
    }

    public void l(f fVar) {
        a(fVar, false, 5);
    }

    public void n(f fVar) {
        fVar.OM();
        if (this.aFA == null) {
            this.aFA = new Timer();
            e eVar = new e(this);
            eVar.bph = 0;
            this.aFA.schedule(eVar, 500, 500);
        }
    }

    public int zA() {
        return this.aFv;
    }

    public int zB() {
        return this.aFw;
    }

    public int zC() {
        return this.aFx.get();
    }

    public void zD() {
        boolean z;
        int i = 0;
        if (this.aFr != null) {
            boolean z2 = false;
            while (i < this.aFr.size()) {
                f fVar = (f) this.aFr.get(i);
                if (fVar != null) {
                    fVar.OM();
                }
                i++;
                z2 = (fVar == null || fVar.Ov() != 0) ? z2 : true;
            }
            z = z2;
        } else {
            z = false;
        }
        if (this.aFA != null && !z) {
            this.aFA.cancel();
            this.aFA = null;
        }
    }

    public final short zl() {
        short s = (short) (this.aFz + 1);
        this.aFz = s;
        return s;
    }

    /* JADX WARNING: Removed duplicated region for block: B:101:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00e1 A[SYNTHETIC, Splitter:B:25:0x00e1] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00e6 A[Catch:{ Exception -> 0x0205 }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0150 A[SYNTHETIC, Splitter:B:40:0x0150] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0155 A[Catch:{ Exception -> 0x01ec }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zo() {
        /*
            r30 = this;
            r2 = 0
            r3 = 0
            byte[] r4 = com.uc.c.az.By()     // Catch:{ Exception -> 0x0208, all -> 0x01ef }
            if (r4 == 0) goto L_0x0216
            java.io.ByteArrayInputStream r24 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x0208, all -> 0x01ef }
            r0 = r24
            r1 = r4
            r0.<init>(r1)     // Catch:{ Exception -> 0x0208, all -> 0x01ef }
            java.io.DataInputStream r25 = new java.io.DataInputStream     // Catch:{ Exception -> 0x0210, all -> 0x01f7 }
            r0 = r25
            r1 = r24
            r0.<init>(r1)     // Catch:{ Exception -> 0x0210, all -> 0x01f7 }
            byte r26 = r25.readByte()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r0 = r26
            com.uc.c.a.f[] r0 = new com.uc.c.a.f[r0]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r27 = r0
            short r2 = r25.readShort()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r0 = r2
            r1 = r30
            r1.aFz = r0     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            int r2 = com.uc.c.az.bbE     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r0 = r2
            r1 = r30
            r1.aFw = r0     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r2 = 9
            r0 = r2
            r1 = r30
            r1.aFv = r0     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r2 = 0
            r15 = r2
        L_0x003c:
            r0 = r15
            r1 = r26
            if (r0 >= r1) goto L_0x0162
            java.lang.String r5 = r25.readUTF()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            java.lang.String r7 = r25.readUTF()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            java.lang.String r6 = r25.readUTF()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r25.readUTF()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            short r4 = r25.readShort()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            byte r14 = r25.readByte()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            byte r16 = r25.readByte()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            java.lang.String r9 = r25.readUTF()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            java.lang.String r8 = r25.readUTF()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            int r17 = r25.readInt()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            int r28 = r25.readInt()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            com.uc.c.a.f r2 = new com.uc.c.a.f     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            java.lang.String r10 = "GET"
            r11 = 1
            r12 = 0
            r13 = 0
            r3 = r30
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r27[r15] = r2     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r2 = 1
            boolean r2 = com.uc.c.bc.aL(r14, r2)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            if (r2 == 0) goto L_0x00b3
            r2 = r27[r15]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r3 = 1
            r2.cG(r3)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
        L_0x0087:
            r2 = 2
            r0 = r16
            r1 = r2
            if (r0 != r1) goto L_0x00ea
            r2 = r27[r15]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r3 = 2
            r2.D(r3)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r2 = r27[r15]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r0 = r28
            long r0 = (long) r0     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r3 = r0
            r2.t(r3)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r0 = r30
            java.util.Vector r0 = r0.aFt     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r2 = r0
            r3 = r27[r15]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r2.add(r3)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
        L_0x00a6:
            r2 = r27[r15]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r0 = r28
            long r0 = (long) r0     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r3 = r0
            r2.u(r3)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            int r2 = r15 + 1
            r15 = r2
            goto L_0x003c
        L_0x00b3:
            r2 = r27[r15]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r3 = 0
            r2.cG(r3)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            goto L_0x0087
        L_0x00ba:
            r2 = move-exception
            r2 = r25
            r3 = r24
        L_0x00bf:
            r0 = r30
            java.util.Vector r0 = r0.aFr     // Catch:{ all -> 0x01fc }
            r4 = r0
            r4.clear()     // Catch:{ all -> 0x01fc }
            r0 = r30
            java.util.Vector r0 = r0.aFs     // Catch:{ all -> 0x01fc }
            r4 = r0
            r4.clear()     // Catch:{ all -> 0x01fc }
            r0 = r30
            java.util.Vector r0 = r0.aFt     // Catch:{ all -> 0x01fc }
            r4 = r0
            r4.clear()     // Catch:{ all -> 0x01fc }
            r0 = r30
            java.util.Vector r0 = r0.aFu     // Catch:{ all -> 0x01fc }
            r4 = r0
            r4.clear()     // Catch:{ all -> 0x01fc }
            if (r2 == 0) goto L_0x00e4
            r2.close()     // Catch:{ Exception -> 0x0205 }
        L_0x00e4:
            if (r3 == 0) goto L_0x00e9
            r3.close()     // Catch:{ Exception -> 0x0205 }
        L_0x00e9:
            return
        L_0x00ea:
            r2 = r27[r15]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r3 = 3
            r0 = r16
            r1 = r3
            if (r0 != r1) goto L_0x0159
            r3 = 3
        L_0x00f3:
            r2.D(r3)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r2 = r27[r15]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r0 = r17
            long r0 = (long) r0     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r3 = r0
            r2.t(r3)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r2 = 1
            r3 = r27[r15]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            boolean r3 = r3.OD()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            if (r2 != r3) goto L_0x015b
            r2 = r27[r15]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r3 = -2
            r2.E(r3)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            com.uc.c.a.g r8 = new com.uc.c.a.g     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r9 = r27[r15]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            java.lang.String r13 = "GET"
            r2 = r27[r15]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            java.util.concurrent.atomic.AtomicLong r14 = r2.OC()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r0 = r17
            long r0 = (long) r0     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r16 = r0
            r0 = r28
            long r0 = (long) r0     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r18 = r0
            r20 = 0
            r21 = 1
            r22 = 0
            r23 = 0
            r10 = r5
            r11 = r7
            r12 = r6
            r8.<init>(r9, r10, r11, r12, r13, r14, r15, r16, r18, r20, r21, r22, r23)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r2 = r27[r15]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r3 = 1
            com.uc.c.a.g[] r3 = new com.uc.c.a.g[r3]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r4 = 0
            r3[r4] = r8     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r2.a(r3)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
        L_0x013d:
            r0 = r30
            java.util.Vector r0 = r0.aFu     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r2 = r0
            r3 = r27[r15]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r2.add(r3)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            goto L_0x00a6
        L_0x0149:
            r2 = move-exception
            r3 = r25
            r4 = r24
        L_0x014e:
            if (r3 == 0) goto L_0x0153
            r3.close()     // Catch:{ Exception -> 0x01ec }
        L_0x0153:
            if (r4 == 0) goto L_0x0158
            r4.close()     // Catch:{ Exception -> 0x01ec }
        L_0x0158:
            throw r2
        L_0x0159:
            r3 = 1
            goto L_0x00f3
        L_0x015b:
            r2 = r27[r15]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r3 = -1
            r2.E(r3)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            goto L_0x013d
        L_0x0162:
            r2 = 0
        L_0x0163:
            r0 = r2
            r1 = r26
            if (r0 >= r1) goto L_0x01b1
            int r3 = r25.readInt()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            if (r3 == 0) goto L_0x01ae
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r0 = r25
            r1 = r3
            r0.read(r1)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            java.lang.String r4 = r25.readUTF()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r5 = r27[r2]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            if (r5 == 0) goto L_0x01ae
            r5 = r27[r2]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            java.lang.String r6 = "POST"
            r5.setMethod(r6)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r5 = r27[r2]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r5.aM(r3)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r5 = r27[r2]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r5.setContentType(r4)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r5 = r27[r2]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            com.uc.c.a.g[] r5 = r5.OB()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            if (r5 == 0) goto L_0x01ae
            int r6 = r5.length     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            if (r6 == 0) goto L_0x01ae
            r6 = 0
            r6 = r5[r6]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            java.lang.String r7 = "POST"
            r6.setMethod(r7)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r6 = 0
            r6 = r5[r6]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r6.aM(r3)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r3 = 0
            r3 = r5[r3]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r3.setContentType(r4)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
        L_0x01ae:
            int r2 = r2 + 1
            goto L_0x0163
        L_0x01b1:
            r2 = 0
        L_0x01b2:
            r0 = r2
            r1 = r26
            if (r0 >= r1) goto L_0x01d9
            int r3 = r25.readInt()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            int r4 = r25.readInt()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            int r5 = r25.readInt()     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r6 = r27[r2]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            if (r6 == 0) goto L_0x01d6
            r6 = r27[r2]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r6.ke(r3)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r3 = r27[r2]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r3.kf(r4)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r3 = r27[r2]     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
            r3.kg(r5)     // Catch:{ Exception -> 0x00ba, all -> 0x0149 }
        L_0x01d6:
            int r2 = r2 + 1
            goto L_0x01b2
        L_0x01d9:
            r2 = r25
            r3 = r24
        L_0x01dd:
            if (r2 == 0) goto L_0x01e2
            r2.close()     // Catch:{ Exception -> 0x01e9 }
        L_0x01e2:
            if (r3 == 0) goto L_0x00e9
            r3.close()     // Catch:{ Exception -> 0x01e9 }
            goto L_0x00e9
        L_0x01e9:
            r2 = move-exception
            goto L_0x00e9
        L_0x01ec:
            r3 = move-exception
            goto L_0x0158
        L_0x01ef:
            r4 = move-exception
            r29 = r4
            r4 = r2
            r2 = r29
            goto L_0x014e
        L_0x01f7:
            r2 = move-exception
            r4 = r24
            goto L_0x014e
        L_0x01fc:
            r4 = move-exception
            r29 = r4
            r4 = r3
            r3 = r2
            r2 = r29
            goto L_0x014e
        L_0x0205:
            r2 = move-exception
            goto L_0x00e9
        L_0x0208:
            r4 = move-exception
            r29 = r3
            r3 = r2
            r2 = r29
            goto L_0x00bf
        L_0x0210:
            r2 = move-exception
            r2 = r3
            r3 = r24
            goto L_0x00bf
        L_0x0216:
            r29 = r3
            r3 = r2
            r2 = r29
            goto L_0x01dd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.a.d.zo():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.az.b(byte[], boolean):boolean
     arg types: [byte[], int]
     candidates:
      com.uc.c.az.b(b.a.a.b.d, int):void
      com.uc.c.az.b(byte[], boolean):boolean */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00f0 A[SYNTHETIC, Splitter:B:27:0x00f0] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00f5 A[SYNTHETIC, Splitter:B:30:0x00f5] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x011c A[SYNTHETIC, Splitter:B:50:0x011c] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0121 A[SYNTHETIC, Splitter:B:53:0x0121] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void zq() {
        /*
            r13 = this;
            r11 = 0
            r10 = 0
            monitor-enter(r13)
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x013b, all -> 0x0117 }
            r1.<init>()     // Catch:{ Exception -> 0x013b, all -> 0x0117 }
            java.io.DataOutputStream r2 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x013f, all -> 0x0132 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x013f, all -> 0x0132 }
            java.util.Vector r0 = r13.zy()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            short r3 = r13.aFz     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeShort(r3)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            int r3 = r13.aFw     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeInt(r3)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            int r3 = r13.aFv     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeInt(r3)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            int r3 = r0.size()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeInt(r3)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
        L_0x002b:
            boolean r0 = r3.hasNext()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            if (r0 == 0) goto L_0x00fa
            java.lang.Object r0 = r3.next()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            com.uc.c.a.f r0 = (com.uc.c.a.f) r0     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            short r4 = r0.Os()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeShort(r4)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            java.lang.String r4 = r0.Ot()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeUTF(r4)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            java.lang.String r4 = r0.zK()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeUTF(r4)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            java.lang.String r4 = r0.getMethod()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeUTF(r4)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            java.lang.String r4 = r0.Ou()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeUTF(r4)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            java.lang.String r4 = r0.Oy()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeUTF(r4)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            java.lang.String r4 = r0.getFileName()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeUTF(r4)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            byte r4 = r0.Ov()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeByte(r4)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            byte r4 = r0.OK()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeByte(r4)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            long r5 = r0.Ox()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeLong(r5)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            java.lang.String r5 = r0.getContentType()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            if (r5 != 0) goto L_0x0085
            java.lang.String r5 = ""
        L_0x0085:
            r2.writeUTF(r5)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            byte[] r5 = r0.OE()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            if (r5 != 0) goto L_0x00e4
            r5 = 0
            r2.writeInt(r5)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
        L_0x0092:
            int r5 = r0.OF()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeInt(r5)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            int r5 = r0.OI()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeInt(r5)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            int r5 = r0.OJ()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeInt(r5)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            long r5 = r0.Ow()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeLong(r5)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            boolean r5 = r0.OD()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeBoolean(r5)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            if (r4 == 0) goto L_0x002b
            r5 = -1
            if (r4 == r5) goto L_0x002b
            byte r4 = r0.Oz()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeByte(r4)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            com.uc.c.a.g[] r0 = r0.OB()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r5 = r11
        L_0x00c6:
            if (r5 >= r4) goto L_0x002b
            r6 = r0[r5]     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            long r6 = r6.Pp()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r8 = r0[r5]     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            long r8 = r8.Pr()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            long r6 = r6 + r8
            r2.writeLong(r6)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r6 = r0[r5]     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            long r6 = r6.Pq()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeLong(r6)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            int r5 = r5 + 1
            goto L_0x00c6
        L_0x00e4:
            int r6 = r5.length     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.writeInt(r6)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r2.write(r5)     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            goto L_0x0092
        L_0x00ec:
            r0 = move-exception
            r0 = r2
        L_0x00ee:
            if (r0 == 0) goto L_0x00f3
            r0.close()     // Catch:{ IOException -> 0x012a }
        L_0x00f3:
            if (r1 == 0) goto L_0x00f8
            r1.close()     // Catch:{ IOException -> 0x012c }
        L_0x00f8:
            monitor-exit(r13)
            return
        L_0x00fa:
            r2.close()     // Catch:{ Exception -> 0x00ec, all -> 0x0136 }
            r0 = 0
            byte[] r2 = r1.toByteArray()     // Catch:{ Exception -> 0x013f, all -> 0x0132 }
            r3 = 1
            com.uc.c.az.b(r2, r3)     // Catch:{ Exception -> 0x013f, all -> 0x0132 }
            r1.close()     // Catch:{ Exception -> 0x013f, all -> 0x0132 }
            r1 = 0
            if (r10 == 0) goto L_0x010f
            r0.close()     // Catch:{ IOException -> 0x0128 }
        L_0x010f:
            if (r10 == 0) goto L_0x00f8
            r1.close()     // Catch:{ IOException -> 0x0115 }
            goto L_0x00f8
        L_0x0115:
            r0 = move-exception
            goto L_0x00f8
        L_0x0117:
            r0 = move-exception
            r1 = r10
            r2 = r10
        L_0x011a:
            if (r1 == 0) goto L_0x011f
            r1.close()     // Catch:{ IOException -> 0x012e }
        L_0x011f:
            if (r2 == 0) goto L_0x0124
            r2.close()     // Catch:{ IOException -> 0x0130 }
        L_0x0124:
            throw r0     // Catch:{ all -> 0x0125 }
        L_0x0125:
            r0 = move-exception
            monitor-exit(r13)
            throw r0
        L_0x0128:
            r0 = move-exception
            goto L_0x010f
        L_0x012a:
            r0 = move-exception
            goto L_0x00f3
        L_0x012c:
            r0 = move-exception
            goto L_0x00f8
        L_0x012e:
            r1 = move-exception
            goto L_0x011f
        L_0x0130:
            r1 = move-exception
            goto L_0x0124
        L_0x0132:
            r0 = move-exception
            r2 = r1
            r1 = r10
            goto L_0x011a
        L_0x0136:
            r0 = move-exception
            r12 = r2
            r2 = r1
            r1 = r12
            goto L_0x011a
        L_0x013b:
            r0 = move-exception
            r0 = r10
            r1 = r10
            goto L_0x00ee
        L_0x013f:
            r0 = move-exception
            r0 = r10
            goto L_0x00ee
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.a.d.zq():void");
    }

    public int zr() {
        return a(null, false, 7);
    }

    public int zs() {
        return a(null, false, 8);
    }

    public boolean zu() {
        boolean z = false;
        while (true) {
            boolean z2 = z;
            if (this.aFu.size() <= 0) {
                return z2;
            }
            z = k((f) this.aFu.get(this.aFu.size() - 1)) | z2;
        }
    }

    public Vector zv() {
        return this.aFr;
    }

    public Vector zw() {
        return this.aFs;
    }

    public Vector zx() {
        return this.aFt;
    }

    public Vector zy() {
        Vector vector = new Vector();
        vector.addAll(this.aFr);
        vector.addAll(this.aFs);
        vector.addAll(this.aFu);
        vector.addAll(this.aFt);
        return vector;
    }

    public Vector zz() {
        return this.aFu;
    }
}
