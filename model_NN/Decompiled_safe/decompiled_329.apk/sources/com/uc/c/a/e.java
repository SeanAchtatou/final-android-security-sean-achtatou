package com.uc.c.a;

import java.util.TimerTask;

class e extends TimerTask {
    public long bph = 0;
    final /* synthetic */ d bpi;

    e(d dVar) {
        this.bpi = dVar;
    }

    public void run() {
        boolean z;
        if (this.bpi.aFr != null) {
            if (0 == this.bph || System.currentTimeMillis() - this.bph > 5000) {
                this.bph = System.currentTimeMillis();
                if (this.bpi.aFr.size() > 0) {
                    this.bpi.zq();
                }
                z = true;
            } else {
                z = false;
            }
            for (int i = 0; i < this.bpi.aFr.size(); i++) {
                f fVar = (f) this.bpi.aFr.get(i);
                if (fVar != null && fVar.Ov() == 0) {
                    fVar.OL();
                    if (z) {
                        this.bpi.aFy.a(fVar, (int) ((fVar.Ow() * 100) / fVar.Ox()));
                    }
                }
            }
            if (this.bpi.aFy != null) {
                this.bpi.aFy.ba();
            }
        }
    }
}
