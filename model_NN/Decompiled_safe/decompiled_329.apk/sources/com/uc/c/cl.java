package com.uc.c;

import java.io.EOFException;
import java.io.InputStream;

public class cl extends InputStream {
    private boolean cgw = false;
    private InputStream cgx;
    private int cgy = -1;

    public cl(InputStream inputStream) {
        this.cgx = inputStream;
    }

    public boolean QJ() {
        return this.cgw;
    }

    public boolean kF(int i) {
        this.cgw = false;
        if (i <= 0) {
            return false;
        }
        this.cgy = i;
        return true;
    }

    public int read() {
        if (this.cgy == 0) {
            this.cgw = true;
            throw new EOFException();
        }
        int read = this.cgx.read();
        this.cgy--;
        return read;
    }

    public int read(byte[] bArr) {
        if (this.cgy == 0) {
            this.cgw = true;
            throw new EOFException();
        }
        int read = this.cgx.read(bArr, 0, this.cgy == -1 ? bArr.length : Math.min(this.cgy, bArr.length));
        this.cgy -= read;
        return read;
    }

    public int read(byte[] bArr, int i, int i2) {
        if (this.cgy == 0) {
            throw new EOFException();
        }
        int read = this.cgx.read(bArr, i, this.cgy == -1 ? i2 : Math.min(this.cgy, i2));
        this.cgy -= read;
        return read;
    }
}
