package com.uc.c;

import b.a.a.c.a.c;
import b.a.a.c.g;
import java.io.InputStream;
import java.io.OutputStream;

public class bp extends q {
    private c Xy;

    public bp() {
    }

    public bp(String str, int i) {
        if (fS()) {
            b(str, i);
        }
    }

    public static final String eH(String str) {
        for (int length = str.length() - 1; length >= 0; length--) {
            if (str.charAt(length) == '/') {
                return str.substring(length + 1, str.length());
            }
        }
        return str;
    }

    private static final String[] m(String[] strArr) {
        for (int i = 0; i < strArr.length; i++) {
            String str = strArr[i];
            boolean z = !str.endsWith(au.aGF);
            if (!z) {
                str = str.substring(0, str.length() - 1);
            }
            String eH = eH(str);
            if (!z) {
                eH = eH + '/';
            }
            strArr[i] = eH;
        }
        return strArr;
    }

    public void O(String str) {
        this.Xy.O(str);
    }

    public void b(String str, int i) {
        this.Xy = (c) g.z(str, i);
    }

    public String[] b(String str, boolean z) {
        return null;
    }

    public boolean canRead() {
        return this.Xy.canRead();
    }

    public boolean canWrite() {
        return this.Xy.canWrite();
    }

    public void cf() {
        this.Xy.cf();
    }

    public void close() {
        this.Xy.close();
    }

    public void create() {
        this.Xy.create();
    }

    public OutputStream cw() {
        return this.Xy.cw();
    }

    public boolean exists() {
        return this.Xy.exists();
    }

    public InputStream ez() {
        return this.Xy.ez();
    }

    public boolean fS() {
        return this.Xy == null;
    }

    public long fT() {
        return this.Xy.fT();
    }

    public void fU() {
        this.Xy.fU();
    }

    public String[] fV() {
        return null;
    }

    public long fW() {
        return this.Xy.fW();
    }

    public OutputStream i(long j) {
        return this.Xy.cw();
    }

    public boolean isDirectory() {
        return this.Xy.isDirectory();
    }

    public boolean isHidden() {
        return this.Xy.isHidden();
    }

    public long lastModified() {
        return this.Xy.lastModified();
    }

    public String[] list() {
        return null;
    }
}
