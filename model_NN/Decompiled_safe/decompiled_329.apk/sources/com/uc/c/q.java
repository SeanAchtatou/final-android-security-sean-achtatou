package com.uc.c;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Vector;

public abstract class q {
    public static final String vt = "unknow_file_";

    public static String P(String str) {
        String fX = fX();
        String a2 = a(str, '.');
        if (a2 == null || a2.length() < 1) {
            return fX;
        }
        if (!a2.toLowerCase().equals(".jad") && !a2.toLowerCase().equals(".jar")) {
            return fX;
        }
        return str.substring(0, str.indexOf(a2)) + ("_" + a2.substring(1, a2.length()));
    }

    public static final String Q(String str) {
        return "name_" + fX() + a(str, '.');
    }

    public static q R(String str) {
        return c(str, 3);
    }

    public static String a(String str, char c) {
        String str2;
        if (str == null) {
            return "unknow";
        }
        int lastIndexOf = str.lastIndexOf(c);
        if (lastIndexOf <= 0 || lastIndexOf >= str.length()) {
            str2 = str;
        } else {
            try {
                str2 = str.substring(lastIndexOf, str.length());
            } catch (Exception e) {
                str2 = str;
            }
        }
        int i = 0;
        for (int i2 = 0; i2 < str2.length(); i2++) {
            if ((str2.charAt(i2) < '0' || str2.charAt(i2) > '9') && ((str2.charAt(i2) < 'A' || str2.charAt(i2) > 'Z') && (str2.charAt(i2) < 'a' || str2.charAt(i2) > 'z'))) {
                i = i2;
            }
        }
        return i > 0 ? str2.substring(0, i) : str2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x008c A[SYNTHETIC, Splitter:B:26:0x008c] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00ca A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r6, java.lang.String r7, boolean r8, boolean r9, java.lang.Object r10) {
        /*
            r10 = 0
            r0 = 0
            r1 = 0
            r2 = r10
            r10 = r0
            r0 = r7
            r5 = r7
            r7 = r1
            r1 = r5
        L_0x0009:
            com.uc.c.q r2 = R(r6)     // Catch:{ Exception -> 0x00a0 }
            if (r2 == 0) goto L_0x001f
            boolean r3 = r2.exists()     // Catch:{ Exception -> 0x00a0 }
            if (r3 == 0) goto L_0x001b
            boolean r3 = r2.isDirectory()     // Catch:{ Exception -> 0x00a0 }
            if (r3 != 0) goto L_0x001f
        L_0x001b:
            r2.fU()     // Catch:{ Exception -> 0x00a0 }
            r2 = 0
        L_0x001f:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a0 }
            r3.<init>()     // Catch:{ Exception -> 0x00a0 }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Exception -> 0x00a0 }
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x00a0 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00a0 }
            com.uc.c.q r2 = R(r3)     // Catch:{ Exception -> 0x00a0 }
            if (r2 == 0) goto L_0x0084
            boolean r3 = r2.exists()     // Catch:{ Exception -> 0x00a0 }
            if (r3 == 0) goto L_0x0084
            if (r9 == 0) goto L_0x009e
            r1 = 1
        L_0x003f:
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00cb }
            r3.<init>()     // Catch:{ Exception -> 0x00cb }
            java.lang.String r4 = "["
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00cb }
            int r4 = r1 + 1
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x00cb }
            java.lang.String r3 = "]"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x00cb }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00cb }
            java.lang.String r1 = i(r0, r1)     // Catch:{ Exception -> 0x00cb }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00cb }
            r3.<init>()     // Catch:{ Exception -> 0x00cb }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Exception -> 0x00cb }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ Exception -> 0x00cb }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00cb }
            com.uc.c.q r2 = R(r3)     // Catch:{ Exception -> 0x00cb }
            boolean r3 = r2.exists()     // Catch:{ Exception -> 0x00a0 }
            if (r3 == 0) goto L_0x007f
            r2.close()     // Catch:{ Exception -> 0x00a0 }
            r1 = r4
            goto L_0x003f
        L_0x007f:
            if (r8 == 0) goto L_0x0084
            r2.create()     // Catch:{ Exception -> 0x00a0 }
        L_0x0084:
            r3 = 1
            r5 = r3
            r3 = r2
            r2 = r1
            r1 = r0
            r0 = r5
        L_0x008a:
            if (r3 == 0) goto L_0x0092
            boolean r4 = r3.fS()     // Catch:{ Exception -> 0x00c0 }
            if (r4 != 0) goto L_0x0092
        L_0x0092:
            if (r7 == 0) goto L_0x00c3
        L_0x0094:
            if (r7 == 0) goto L_0x00ca
            java.lang.Exception r6 = new java.lang.Exception
            java.lang.String r7 = "exist"
            r6.<init>(r7)
            throw r6
        L_0x009e:
            r7 = 1
            goto L_0x0084
        L_0x00a0:
            r1 = move-exception
            r1 = r2
        L_0x00a2:
            int r10 = r10 + 1
            r2 = 0
            r3 = 3
            if (r10 <= r3) goto L_0x00a9
            r2 = 1
        L_0x00a9:
            r3 = 3
            if (r10 >= r3) goto L_0x00b6
            java.lang.String r0 = Q(r0)
            r3 = r1
            r1 = r0
            r5 = r0
            r0 = r2
            r2 = r5
            goto L_0x008a
        L_0x00b6:
            java.lang.String r0 = P(r0)
            r3 = r1
            r1 = r0
            r5 = r0
            r0 = r2
            r2 = r5
            goto L_0x008a
        L_0x00c0:
            r3 = move-exception
            r3 = 0
            goto L_0x0092
        L_0x00c3:
            if (r0 != 0) goto L_0x0094
            r0 = r1
            r1 = r2
            r2 = r3
            goto L_0x0009
        L_0x00ca:
            return r2
        L_0x00cb:
            r1 = move-exception
            r1 = r2
            goto L_0x00a2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.q.a(java.lang.String, java.lang.String, boolean, boolean, java.lang.Object):java.lang.String");
    }

    public static String[] a(Enumeration enumeration) {
        Vector vector = new Vector();
        while (enumeration.hasMoreElements()) {
            vector.addElement(enumeration.nextElement());
        }
        String[] strArr = new String[vector.size()];
        vector.copyInto(strArr);
        return strArr;
    }

    public static q c(String str, int i) {
        String str2;
        StringBuffer stringBuffer = new StringBuffer();
        if (str != null) {
            if (!str.startsWith(au.aGF)) {
                stringBuffer.append('/');
            }
            stringBuffer.append("file://").append(str);
            str2 = stringBuffer.toString();
        } else {
            str2 = str;
        }
        switch (w.Nj) {
            case 0:
                return str2 != null ? new ae(str2, i) : new ae();
            case 1:
                return null;
            case 2:
                return null;
            default:
                return null;
        }
    }

    public static final String fX() {
        return "unknow_file_[" + ((((byte) ((int) System.currentTimeMillis())) + 255) % 255) + ']';
    }

    public static String[] fY() {
        q c = c(null, -1);
        if (c != null) {
            return c.fV();
        }
        return null;
    }

    public static String h(String str, String str2) {
        return a(str, str2, true, false, null);
    }

    public static final String i(String str, String str2) {
        if (str.indexOf(46) == -1) {
            return str + str2;
        }
        for (int length = str.length() - 1; length >= 0; length--) {
            if (str.charAt(length) == '.') {
                return str.substring(0, length) + str2 + str.substring(length, str.length());
            }
        }
        return str + str2;
    }

    /* access modifiers changed from: package-private */
    public abstract void O(String str);

    /* access modifiers changed from: package-private */
    public abstract void b(String str, int i);

    /* access modifiers changed from: package-private */
    public abstract String[] b(String str, boolean z);

    /* access modifiers changed from: package-private */
    public abstract boolean canRead();

    /* access modifiers changed from: package-private */
    public abstract boolean canWrite();

    public abstract void cf();

    public abstract void close();

    public abstract void create();

    public abstract OutputStream cw();

    public abstract boolean exists();

    public abstract InputStream ez();

    /* access modifiers changed from: package-private */
    public abstract boolean fS();

    /* access modifiers changed from: package-private */
    public abstract long fT();

    public abstract void fU();

    /* access modifiers changed from: package-private */
    public abstract String[] fV();

    /* access modifiers changed from: package-private */
    public abstract long fW();

    /* access modifiers changed from: package-private */
    public abstract OutputStream i(long j);

    /* access modifiers changed from: package-private */
    public abstract boolean isDirectory();

    /* access modifiers changed from: package-private */
    public abstract boolean isHidden();

    /* access modifiers changed from: package-private */
    public abstract long lastModified();

    /* access modifiers changed from: package-private */
    public abstract String[] list();
}
