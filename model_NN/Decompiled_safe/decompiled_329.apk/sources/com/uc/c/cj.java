package com.uc.c;

import java.io.EOFException;
import java.io.InputStream;

public class cj extends InputStream {
    public static int cgu = 512;
    private byte[] bhc;
    private int bhd;
    public int[] bze;
    public int bzl;
    private byte cgs;
    private InputStream cgt;
    public byte cgv;

    public cj(InputStream inputStream) {
        this.cgs = 0;
        this.bzl = 0;
        this.bhd = 0;
        this.bhc = null;
        this.cgt = null;
        this.cgv = 1;
        this.bze = null;
        this.bzl = 0;
        this.bhc = new byte[cgu];
        this.cgt = inputStream;
    }

    public cj(InputStream inputStream, int i) {
        this.cgs = 0;
        this.bzl = 0;
        this.bhd = 0;
        this.bhc = null;
        this.cgt = null;
        this.cgv = 1;
        this.bze = null;
        this.bzl = i;
        this.cgt = inputStream;
        this.cgs = 1;
    }

    private void fill() {
        this.bhd = 0;
        this.bzl = this.cgt.read(this.bhc, 0, cgu);
        if (this.bzl > 0 && this.bze != null) {
            int[] iArr = this.bze;
            iArr[0] = iArr[0] + this.bzl;
        }
    }

    public InputStream QI() {
        return this.cgt;
    }

    public final void close() {
        super.close();
        if (bc.aL(this.cgv, 2) && this.bze != null && this.bze[0] > 0) {
            w.Nq += this.bze[0];
            this.cgv = (byte) bc.aJ(this.cgv, 2);
        }
        if (this.cgt != null) {
            this.cgt.close();
        }
        this.bhc = null;
        this.cgt = null;
    }

    public final boolean markSupported() {
        return false;
    }

    public final int read() {
        if (this.cgs == 1) {
            if (this.bzl <= 0) {
                throw new EOFException();
            }
            this.bzl--;
            return this.cgt.read();
        } else if (this.cgv != (this.cgv | 1)) {
            if (this.bze != null) {
                int[] iArr = this.bze;
                iArr[0] = iArr[0] + 1;
            }
            return this.cgt.read();
        } else {
            if (this.bzl < 1) {
                fill();
                if (this.bzl < 0) {
                    return -1;
                }
            }
            this.bzl--;
            byte[] bArr = this.bhc;
            int i = this.bhd;
            this.bhd = i + 1;
            return bArr[i] & 255;
        }
    }

    public final int read(byte[] bArr, int i, int i2) {
        int i3;
        int i4;
        int i5;
        if (this.cgs == 1) {
            int read = this.cgt.read(bArr, i, i2);
            this.bzl -= read;
            return read;
        } else if (!bc.aL(this.cgv, 1)) {
            int read2 = this.cgt.read(bArr, i, i2);
            if (read2 <= 0 || this.bze == null) {
                return read2;
            }
            int[] iArr = this.bze;
            iArr[0] = iArr[0] + read2;
            return read2;
        } else {
            if (this.bzl > 0) {
                i5 = Math.min(this.bzl, i2);
                System.arraycopy(this.bhc, this.bhd, bArr, i, i5);
                this.bzl -= i5;
                this.bhd += i5;
                i3 = i + i5;
                i4 = i2 - i5;
            } else {
                i5 = 0;
                i4 = i2;
                i3 = i;
            }
            while (i4 > 0) {
                int read3 = this.cgt.read(bArr, i3, i4);
                if (read3 >= 0) {
                    if (this.bze != null) {
                        int[] iArr2 = this.bze;
                        iArr2[0] = iArr2[0] + read3;
                    }
                    i5 += read3;
                    i3 += read3;
                    i4 -= read3;
                } else if (i5 == 0) {
                    return -1;
                } else {
                    return i5;
                }
            }
            return i5;
        }
    }
}
