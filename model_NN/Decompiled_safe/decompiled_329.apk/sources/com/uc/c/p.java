package com.uc.c;

import a.a.a.c.a.y;
import b.a;
import b.a.a.c.e;
import com.uc.browser.ActivityChooseFile;
import com.uc.browser.ModelBrowser;
import com.uc.browser.UCR;
import com.uc.browser.WebViewJUC;
import java.io.IOException;
import java.io.InputStream;

public class p extends InputStream {
    public static final int[] rs = {0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10, 1, 11, 1, 13, 1, 15, 1, 17, 2, 19, 2, 23, 2, 27, 2, 31, 3, 35, 3, 43, 3, 51, 3, 59, 4, 67, 4, 83, 4, 99, 4, 115, 5, 131, 5, ModelBrowser.Aa, 5, 195, 5, 227, 0, 258};
    public static final int[] rt = {0, 1, 0, 2, 0, 3, 0, 4, 1, 5, 1, 7, 2, 9, 2, 13, 3, 17, 3, 25, 4, 33, 4, 49, 5, 65, 5, 97, 6, 129, 6, 193, 7, 257, 7, 385, 8, 513, 8, 769, 9, 1025, 9, 1537, 10, 2049, 10, 3073, 11, WebViewJUC.cga, 11, 6145, 12, 8193, 12, ActivityChooseFile.RESULT_OK, 13, 16385, 13, 24577};
    byte[] rA = new byte[a.MODE_APPEND];
    int rB = 0;
    byte[] rC = new byte[1324];
    int rD = 1024;
    int rE = 0;
    int rF = 0;
    int rG = 0;
    int rH;
    long[] rI = new long[2];
    short[] rJ = new short[1152];
    short[] rK = new short[128];
    byte[] rL = new byte[8];
    int[] rM = new int[30];
    int[] rN = new int[30];
    byte[] rO = new byte[30];
    int[] rP = new int[286];
    int[] rQ = new int[286];
    byte[] rR = new byte[286];
    byte[] rS;
    short[] rT;
    byte[] rU;
    int[] rV;
    int rW;
    int rX;
    InputStream ru;
    boolean rv = false;
    boolean rw = false;
    byte rx = 0;
    boolean ry;
    int rz;

    public p(InputStream inputStream) {
        this.ru = inputStream;
    }

    private final void B(byte[] bArr) {
        for (int i = 0; i < bArr.length; i++) {
            bArr[i] = 0;
        }
    }

    public static final int a(long[] jArr, short[] sArr) {
        short s = 0;
        while (sArr[s * 2] != -1) {
            s = sArr[(s * 2) + ((int) (jArr[0] & 1))];
            jArr[0] = jArr[0] >>> 1;
            jArr[1] = jArr[1] - 1;
            if (s == 0) {
                throw new IOException("5");
            }
        }
        return sArr[(s * 2) + 1];
    }

    private final void a(int i, int i2, byte[] bArr, int i3) {
        if (i + i2 < this.rA.length) {
            System.arraycopy(this.rA, i, bArr, i3 + 0, i2);
            return;
        }
        System.arraycopy(this.rA, i, bArr, i3 + 0, this.rA.length - i);
        System.arraycopy(this.rA, 0, bArr, (this.rA.length - i) + i3, i2 - (this.rA.length - i));
    }

    public static final void a(int[] iArr, byte[] bArr) {
        byte b2 = 0;
        for (int i = 0; i < bArr.length; i++) {
            if (b2 <= bArr[i]) {
                b2 = bArr[i];
            }
        }
        int i2 = b2 + 1;
        short[] sArr = new short[i2];
        for (byte b3 : bArr) {
            sArr[b3] = (short) (sArr[b3] + 1);
        }
        int[] iArr2 = new int[i2];
        sArr[0] = 0;
        int i3 = 0;
        for (int i4 = 1; i4 < i2; i4++) {
            i3 = (i3 + sArr[i4 - 1]) << 1;
            iArr2[i4] = i3;
        }
        for (int i5 = 0; i5 < iArr.length; i5++) {
            byte b4 = bArr[i5];
            if (b4 != 0) {
                iArr[i5] = iArr2[b4];
                iArr2[b4] = iArr2[b4] + 1;
            }
        }
    }

    public static final void a(int[] iArr, byte[] bArr, int[] iArr2, byte[] bArr2) {
        for (int i = 0; i <= 143; i++) {
            iArr[i] = i + 48;
            bArr[i] = 8;
        }
        for (int i2 = 144; i2 <= 255; i2++) {
            iArr[i2] = (i2 + e.HTTP_BAD_REQUEST) - UCR.Color.bTG;
            bArr[i2] = 9;
        }
        for (int i3 = 256; i3 <= 279; i3++) {
            iArr[i3] = i3 - 256;
            bArr[i3] = 7;
        }
        for (int i4 = 280; i4 < 286; i4++) {
            iArr[i4] = (i4 + y.YW) - 280;
            bArr[i4] = 8;
        }
        b(iArr, bArr);
        for (int i5 = 0; i5 < iArr2.length; i5++) {
            iArr2[i5] = i5;
            bArr2[i5] = 5;
        }
        b(iArr2, bArr2);
    }

    public static final void a(int[] iArr, byte[] bArr, int[] iArr2, short[] sArr) {
        for (int i = 0; i < sArr.length; i++) {
            sArr[i] = 0;
        }
        short s = 1;
        for (short s2 = 0; s2 < iArr.length; s2 = (short) (s2 + 1)) {
            if (bArr[s2] != 0) {
                short s3 = s;
                short s4 = 0;
                for (short s5 = 0; s5 < bArr[s2]; s5 = (short) (s5 + 1)) {
                    if (sArr[s4 * 2] == 0) {
                        short s6 = (short) (s3 + 1);
                        sArr[s4 * 2] = s3;
                        sArr[(s4 * 2) + 1] = s6;
                        s3 = (short) (s6 + 1);
                    }
                    s4 = sArr[(s4 * 2) + ((iArr[s2] >>> s5) & 1)];
                }
                sArr[s4 * 2] = -1;
                sArr[(s4 * 2) + 1] = (short) iArr2[s2];
                s = s3;
            }
        }
    }

    private final void b(int i, int i2, byte[] bArr, int i3) {
        if (i2 + i < this.rA.length) {
            System.arraycopy(bArr, i3, this.rA, i, i2);
            return;
        }
        System.arraycopy(bArr, i3, this.rA, i, this.rA.length - i);
        System.arraycopy(bArr, (this.rA.length - i) + i3, this.rA, 0, i2 - (this.rA.length - i));
    }

    public static final void b(int[] iArr, byte[] bArr) {
        for (int i = 0; i < iArr.length; i++) {
            int i2 = iArr[i];
            int i3 = 0;
            for (int i4 = 0; i4 < bArr[i]; i4++) {
                i3 = (i3 | ((i2 >>> i4) & 1)) << 1;
            }
            iArr[i] = i3 >>> 1;
        }
    }

    private final void c(int[] iArr) {
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = i;
        }
    }

    private final void eB() {
        if (!this.rw) {
            if (eF() == 31 && eF() == 139 && eF() == 8) {
                int eF = eF();
                h(6);
                if ((eF & 4) == 4) {
                    h((long) (eF() | (eF() << 8)));
                }
                if ((eF & 8) == 8) {
                    do {
                    } while (eF() != 0);
                }
                if ((eF & 16) == 16) {
                    do {
                    } while (eF() != 0);
                }
                if ((eF & 2) == 2) {
                    h(2);
                }
                this.rw = true;
            } else {
                throw new IOException("0");
            }
        }
        System.arraycopy(this.rC, this.rG, this.rC, 0, this.rE - this.rG);
        this.rE -= this.rG;
        this.rG = 0;
        this.rF = this.rE;
        if (this.rH == 0) {
            eC();
        }
        while (this.rC.length - this.rE > 300) {
            if ((this.rI[1] > 0 || this.rH > 0) && this.rx != 3) {
                if (this.rx == 0) {
                    eD();
                }
                if (this.rx == 1) {
                    if (this.rz != 0) {
                        eC();
                        int a2 = a(this.rI, this.rJ);
                        if (a2 < 256) {
                            this.rA[this.rB] = (byte) a2;
                            this.rB = (this.rB + 1) & 32767;
                            this.rC[this.rE] = (byte) a2;
                            this.rE++;
                        } else if (a2 == 256) {
                            this.rx = this.ry ? (byte) 2 : 0;
                        } else if (a2 > 285) {
                            throw new IOException(com.uc.a.e.RD);
                        } else {
                            int g = rs[((a2 - 257) << 1) + 1] + g((long) rs[(a2 - 257) << 1]);
                            eC();
                            int a3 = a(this.rI, this.rK);
                            int g2 = rt[(a3 << 1) + 1] + g((long) rt[a3 << 1]);
                            int i = this.rB - g2;
                            int length = i + (i < 0 ? this.rA.length : 0);
                            int i2 = g / g2;
                            int i3 = g - (g2 * i2);
                            for (int i4 = 0; i4 < i2; i4++) {
                                a(length, g2, this.rC, this.rE);
                                b(this.rB, g2, this.rC, this.rE);
                                this.rE += g2;
                                this.rB = (this.rB + g2) & 32767;
                            }
                            a(length, i3, this.rC, this.rE);
                            b(this.rB, i3, this.rC, this.rE);
                            this.rE += i3;
                            this.rB = (i3 + this.rB) & 32767;
                        }
                        eC();
                    } else if (this.rH > 0) {
                        int f = f(this.rC, this.rE, this.rC.length - this.rE > this.rH ? this.rH : this.rC.length - this.rE);
                        b(this.rB, f, this.rC, this.rE);
                        this.rE += f;
                        this.rB = (this.rB + f) & 32767;
                        this.rH -= f;
                    } else {
                        this.rx = this.ry ? (byte) 2 : 0;
                        eC();
                    }
                }
                if (this.rx == 2) {
                    this.rx = 3;
                    g(this.rI[1] & 7);
                    int g3 = g(8) | (g(8) << 8) | (g(8) << 16) | (g(8) << 24);
                    int g4 = g(8) | (g(8) << 8) | (g(8) << 16) | (g(8) << 24);
                }
            } else {
                return;
            }
        }
    }

    private final void eC() {
        if (this.rI[1] < 15) {
            eE();
        }
    }

    private final void eD() {
        this.ry = g(1) == 1;
        this.rz = g(2);
        if (this.rz == 3) {
            throw new IllegalArgumentException();
        }
        if (this.rz == 1) {
            a(this.rP, this.rR, this.rM, this.rO);
            c(this.rQ);
            c(this.rN);
            a(this.rP, this.rR, this.rQ, this.rJ);
            a(this.rM, this.rO, this.rN, this.rK);
        } else if (this.rz == 2) {
            int g = g(5);
            int g2 = g(5);
            int g3 = g(4);
            int[] iArr = {16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15};
            int[] iArr2 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
            this.rU = new byte[19];
            this.rV = new int[19];
            for (int i = 0; i < g3 + 4; i++) {
                this.rU[iArr[i]] = (byte) g(3);
            }
            a(this.rV, this.rU);
            b(this.rV, this.rU);
            this.rT = new short[76];
            a(this.rV, this.rU, iArr2, this.rT);
            B(this.rR);
            B(this.rO);
            int i2 = 0;
            byte b2 = 0;
            while (i2 < g + 258 + g2) {
                eC();
                int a2 = a(this.rI, this.rT);
                if (a2 < 16) {
                    b2 = (byte) a2;
                    a2 = 1;
                } else if (a2 == 16) {
                    a2 = g(2) + 3;
                } else if (a2 == 17) {
                    a2 = g(3) + 3;
                    b2 = 0;
                } else if (a2 == 18) {
                    a2 = g(7) + 11;
                    b2 = 0;
                }
                int i3 = i2;
                int i4 = 0;
                while (i4 < a2) {
                    if (i3 < g + 257) {
                        this.rR[i3] = b2;
                    } else {
                        this.rO[i3 - (g + 257)] = b2;
                    }
                    i4++;
                    i3++;
                }
                i2 = i3;
            }
            a(this.rP, this.rR);
            c(this.rQ);
            b(this.rP, this.rR);
            a(this.rP, this.rR, this.rQ, this.rJ);
            c(this.rN);
            a(this.rM, this.rO);
            b(this.rM, this.rO);
            a(this.rM, this.rO, this.rN, this.rK);
        } else {
            g(this.rI[1] & 7);
            this.rH = g(8) | (g(8) << 8);
            eC();
            if (this.rH + (g(8) | (g(8) << 8)) != 65535) {
                throw new IOException("3");
            }
            while (this.rI[1] != 0 && this.rH > 0) {
                int g4 = g(8);
                this.rA[this.rB] = (byte) g4;
                this.rB = (this.rB + 1) & 32767;
                this.rC[this.rE] = (byte) g4;
                this.rE++;
                this.rH--;
            }
        }
        this.rx = 1;
    }

    private final void eE() {
        if (!this.rv) {
            int i = (int) (7 - (this.rI[1] / 8));
            int f = f(this.rL, 0, i);
            if (f != i) {
                this.rv = true;
            }
            for (int i2 = 0; i2 < f; i2++) {
                long[] jArr = this.rI;
                jArr[0] = jArr[0] & ((255 << ((int) this.rI[1])) ^ -1);
                if (this.rL[i2] < 0) {
                    long[] jArr2 = this.rI;
                    jArr2[0] = jArr2[0] | (((long) (this.rL[i2] + 256)) << ((int) this.rI[1]));
                } else {
                    long[] jArr3 = this.rI;
                    jArr3[0] = jArr3[0] | (((long) this.rL[i2]) << ((int) this.rI[1]));
                }
                long[] jArr4 = this.rI;
                jArr4[1] = jArr4[1] + 8;
            }
        }
    }

    private final int g(long j) {
        if (j == 0) {
            return 0;
        }
        if (this.rI[1] < j) {
            eE();
        }
        int i = (int) (this.rI[0] & ((long) ((1 << ((int) j)) - 1)));
        long[] jArr = this.rI;
        jArr[0] = jArr[0] >>> ((int) j);
        long[] jArr2 = this.rI;
        jArr2[1] = jArr2[1] - j;
        return i;
    }

    public int available() {
        if (this.rE - this.rG < this.rC.length - e.HTTP_MULT_CHOICE) {
            eB();
        }
        return this.rE - this.rG;
    }

    public void close() {
        this.ru.close();
        this.rI = null;
        this.rJ = null;
        this.rK = null;
        this.rL = null;
        this.rA = null;
        this.rC = null;
        this.rM = null;
        this.rN = null;
        this.rO = null;
        this.rR = null;
        this.rP = null;
        this.rQ = null;
        this.rS = null;
        this.rT = null;
        this.rV = null;
    }

    public int eF() {
        if (this.rW <= this.rX) {
            return -1;
        }
        int read = this.ru.read();
        if (read == -1) {
            this.rX = this.rW;
            return read;
        }
        this.rX++;
        return read;
    }

    public int f(byte[] bArr, int i, int i2) {
        if (this.rW <= this.rX) {
            return -1;
        }
        int i3 = i;
        int i4 = 0;
        int i5 = this.rW <= (this.rX + i2) - i ? (this.rW - this.rX) + i : i2;
        while (i5 > 0) {
            int read = this.ru.read(bArr, i3, i5);
            if (read >= 0) {
                i4 += read;
                i3 += read;
                i5 -= read;
            } else if (i4 == 0) {
                return -1;
            } else {
                this.rX += i4;
                return i4;
            }
        }
        if (i4 <= 0) {
            return i4;
        }
        this.rX += i4;
        return i4;
    }

    public long h(long j) {
        long skip = this.ru.skip(j);
        if (skip > 0) {
            this.rX += (int) skip;
        }
        return skip;
    }

    public int read() {
        if (this.rE - this.rG == 0) {
            eB();
        }
        if (this.rE - this.rG == 0 && this.rv) {
            return -1;
        }
        byte[] bArr = this.rC;
        int i = this.rG;
        this.rG = i + 1;
        return (bArr[i] + 256) & 255;
    }

    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public int read(byte[] bArr, int i, int i2) {
        if (this.rE - this.rG < this.rC.length - e.HTTP_MULT_CHOICE) {
            eB();
        }
        int available = available();
        if (i2 <= available) {
            available = i2;
        }
        System.arraycopy(this.rC, this.rG, bArr, i, available);
        this.rG += available;
        if (available == 0) {
            return -1;
        }
        return available;
    }

    public long skip(long j) {
        long j2 = 0;
        this.rS = new byte[this.rD];
        while (j2 < j && this.rx != 3) {
            j2 += (long) read(this.rS);
        }
        return j2;
    }
}
