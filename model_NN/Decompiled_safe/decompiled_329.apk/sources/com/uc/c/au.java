package com.uc.c;

import b.a.a.a.f;
import b.a.a.d;
import com.uc.b.e;
import java.util.Vector;

public class au extends bi {
    public static final String aGF = "/";
    public static String aGG = aGF;
    public static final byte aGJ = 0;
    public static final byte aGK = 1;
    public static final byte aGL = 2;
    public static final byte aGM = 1;
    public static final byte aGN = 0;
    public static final byte aGO = 2;
    public static final String aGP = "-rename";
    public static final String aGQ = "-attribute";
    private Object aGC;
    private String aGD;
    private String aGE;
    public byte aGH;
    public byte aGI;
    private Vector aGR;
    private bf oY;
    private bf oZ;

    public au(w wVar, r rVar) {
        this(wVar, rVar, 0, 0);
    }

    public au(w wVar, r rVar, int i, int i2) {
        super(wVar, rVar);
        this.oY = null;
        this.oZ = null;
        this.aGC = null;
        this.aGD = null;
        this.aGE = null;
        this.aGH = 0;
        this.aGI = 0;
        this.aGH = (byte) i;
        this.aGI = (byte) i2;
        this.bjM = aGF;
        if (this.aGH == 0) {
            if (this.aGI == 0) {
                this.bjM = aGG;
            } else if (this.aGI == 1) {
                this.bjM = bb.bdW;
            }
            this.bjJ = new bf(ar.avG[0], -1);
            this.bjK = new bf(ar.avG[7], -1);
            this.oY = new bf(ar.avG[5], -1);
            this.oZ = new bf(ar.avG[1], -1);
            x(this.aGI);
            b(this.bjJ, this.bjK);
        } else if (this.aGH == 1) {
            if (this.aGI == 0) {
                this.bjM = bb.bdV;
            } else if (this.aGI == 1) {
                this.bjM = bb.bdW;
            }
            this.bjJ = new bf(ar.avG[0], -1);
            this.bjK = new bf(ar.avG[7], -1);
            fC(1);
            b(this.bjJ, this.bjK);
        } else if (this.aGH == 2) {
            this.bjM = bb.bdV;
            this.bjJ = new bf(ar.avG[0], -1);
            this.bjK = new bf(ar.avG[7], -1);
            fC(2);
            b(this.bjJ, this.bjK);
        }
        this.aGR = As();
    }

    private final void Aa() {
        if (this.mR != null) {
            this.mR.lB().at("");
        }
        close();
    }

    private final void Ab() {
        if (this.aGH == 1) {
            this.mS.BL.at(this.bjM);
            close();
            if (this.aGI == 0) {
                bb.bdV = this.bjM;
            } else if (this.aGI == 1) {
                bb.bdW = this.bjM;
            }
        }
    }

    private final void Ac() {
        new bq(0, this).start();
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0060 A[SYNTHETIC, Splitter:B:27:0x0060] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0065 A[SYNTHETIC, Splitter:B:30:0x0065] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x006a A[SYNTHETIC, Splitter:B:33:0x006a] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0075 A[SYNTHETIC, Splitter:B:38:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00b8 A[SYNTHETIC, Splitter:B:70:0x00b8] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00bd A[SYNTHETIC, Splitter:B:73:0x00bd] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x00c2 A[SYNTHETIC, Splitter:B:76:0x00c2] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x00cd A[SYNTHETIC, Splitter:B:81:0x00cd] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean Af() {
        /*
            r10 = this;
            r4 = 0
            r7 = 0
            java.lang.String r0 = r10.aGE
            if (r0 == 0) goto L_0x0014
            java.lang.String r0 = r10.aGE
            int r0 = r0.length()
            if (r0 == 0) goto L_0x0014
            boolean r0 = r10.zV()
            if (r0 == 0) goto L_0x0016
        L_0x0014:
            r0 = r7
        L_0x0015:
            return r0
        L_0x0016:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0109, all -> 0x00b2 }
            r0.<init>()     // Catch:{ Exception -> 0x0109, all -> 0x00b2 }
            java.lang.String r1 = r10.bjM     // Catch:{ Exception -> 0x0109, all -> 0x00b2 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0109, all -> 0x00b2 }
            java.lang.String r1 = r10.aGE     // Catch:{ Exception -> 0x0109, all -> 0x00b2 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0109, all -> 0x00b2 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0109, all -> 0x00b2 }
            com.uc.c.q r0 = com.uc.c.q.R(r0)     // Catch:{ Exception -> 0x0109, all -> 0x00b2 }
            java.lang.String r1 = r10.aGD     // Catch:{ Exception -> 0x0110, all -> 0x00e7 }
            com.uc.c.q r1 = com.uc.c.q.R(r1)     // Catch:{ Exception -> 0x0110, all -> 0x00e7 }
            boolean r2 = r0.exists()     // Catch:{ Exception -> 0x0117, all -> 0x00ed }
            if (r2 != 0) goto L_0x003e
            r0.create()     // Catch:{ Exception -> 0x0117, all -> 0x00ed }
        L_0x003e:
            java.io.InputStream r2 = r1.ez()     // Catch:{ Exception -> 0x0117, all -> 0x00ed }
            java.io.OutputStream r3 = r0.cw()     // Catch:{ Exception -> 0x011e, all -> 0x00f5 }
            r4 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r4]     // Catch:{ Exception -> 0x0057, all -> 0x00fe }
        L_0x004a:
            r5 = 0
            int r6 = r4.length     // Catch:{ Exception -> 0x0057, all -> 0x00fe }
            int r5 = r2.read(r4, r5, r6)     // Catch:{ Exception -> 0x0057, all -> 0x00fe }
            if (r5 <= 0) goto L_0x0080
            r6 = 0
            r3.write(r4, r6, r5)     // Catch:{ Exception -> 0x0057, all -> 0x00fe }
            goto L_0x004a
        L_0x0057:
            r4 = move-exception
            r8 = r2
            r2 = r0
            r0 = r8
            r9 = r3
            r3 = r1
            r1 = r9
        L_0x005e:
            if (r0 == 0) goto L_0x0063
            r0.close()     // Catch:{ Exception -> 0x00db }
        L_0x0063:
            if (r1 == 0) goto L_0x0068
            r1.close()     // Catch:{ Exception -> 0x00dd }
        L_0x0068:
            if (r3 == 0) goto L_0x0073
            boolean r0 = r3.fS()     // Catch:{ Exception -> 0x0106 }
            if (r0 != 0) goto L_0x0073
            r3.close()     // Catch:{ Exception -> 0x0106 }
        L_0x0073:
            if (r2 == 0) goto L_0x007e
            boolean r0 = r2.fS()     // Catch:{ Exception -> 0x00ae }
            if (r0 != 0) goto L_0x007e
            r2.close()     // Catch:{ Exception -> 0x00ae }
        L_0x007e:
            r0 = r7
            goto L_0x0015
        L_0x0080:
            r2.close()     // Catch:{ Exception -> 0x0057, all -> 0x00fe }
            r3.close()     // Catch:{ Exception -> 0x0057, all -> 0x00fe }
            r4 = 1
            if (r2 == 0) goto L_0x008c
            r2.close()     // Catch:{ Exception -> 0x00d7 }
        L_0x008c:
            if (r3 == 0) goto L_0x0091
            r3.close()     // Catch:{ Exception -> 0x00d9 }
        L_0x0091:
            if (r1 == 0) goto L_0x009c
            boolean r2 = r1.fS()     // Catch:{ Exception -> 0x0126 }
            if (r2 != 0) goto L_0x009c
            r1.close()     // Catch:{ Exception -> 0x0126 }
        L_0x009c:
            if (r0 == 0) goto L_0x00a7
            boolean r1 = r0.fS()     // Catch:{ Exception -> 0x00aa }
            if (r1 != 0) goto L_0x00a7
            r0.close()     // Catch:{ Exception -> 0x00aa }
        L_0x00a7:
            r0 = r4
            goto L_0x0015
        L_0x00aa:
            r0 = move-exception
            r0 = r4
            goto L_0x0015
        L_0x00ae:
            r0 = move-exception
            r0 = r7
            goto L_0x0015
        L_0x00b2:
            r0 = move-exception
            r1 = r4
            r2 = r4
            r3 = r4
        L_0x00b6:
            if (r1 == 0) goto L_0x00bb
            r1.close()     // Catch:{ Exception -> 0x00df }
        L_0x00bb:
            if (r2 == 0) goto L_0x00c0
            r2.close()     // Catch:{ Exception -> 0x00e1 }
        L_0x00c0:
            if (r4 == 0) goto L_0x00cb
            boolean r1 = r4.fS()     // Catch:{ Exception -> 0x00e5 }
            if (r1 != 0) goto L_0x00cb
            r4.close()     // Catch:{ Exception -> 0x00e5 }
        L_0x00cb:
            if (r3 == 0) goto L_0x00d6
            boolean r1 = r3.fS()     // Catch:{ Exception -> 0x00e3 }
            if (r1 != 0) goto L_0x00d6
            r3.close()     // Catch:{ Exception -> 0x00e3 }
        L_0x00d6:
            throw r0
        L_0x00d7:
            r2 = move-exception
            goto L_0x008c
        L_0x00d9:
            r2 = move-exception
            goto L_0x0091
        L_0x00db:
            r0 = move-exception
            goto L_0x0063
        L_0x00dd:
            r0 = move-exception
            goto L_0x0068
        L_0x00df:
            r1 = move-exception
            goto L_0x00bb
        L_0x00e1:
            r1 = move-exception
            goto L_0x00c0
        L_0x00e3:
            r1 = move-exception
            goto L_0x00d6
        L_0x00e5:
            r1 = move-exception
            goto L_0x00cb
        L_0x00e7:
            r1 = move-exception
            r2 = r4
            r3 = r0
            r0 = r1
            r1 = r4
            goto L_0x00b6
        L_0x00ed:
            r2 = move-exception
            r3 = r0
            r0 = r2
            r2 = r4
            r8 = r4
            r4 = r1
            r1 = r8
            goto L_0x00b6
        L_0x00f5:
            r3 = move-exception
            r8 = r3
            r3 = r0
            r0 = r8
            r9 = r4
            r4 = r1
            r1 = r2
            r2 = r9
            goto L_0x00b6
        L_0x00fe:
            r4 = move-exception
            r8 = r4
            r4 = r1
            r1 = r2
            r2 = r3
            r3 = r0
            r0 = r8
            goto L_0x00b6
        L_0x0106:
            r0 = move-exception
            goto L_0x0073
        L_0x0109:
            r0 = move-exception
            r0 = r4
            r1 = r4
            r2 = r4
            r3 = r4
            goto L_0x005e
        L_0x0110:
            r1 = move-exception
            r1 = r4
            r2 = r0
            r3 = r4
            r0 = r4
            goto L_0x005e
        L_0x0117:
            r2 = move-exception
            r2 = r0
            r3 = r1
            r0 = r4
            r1 = r4
            goto L_0x005e
        L_0x011e:
            r3 = move-exception
            r3 = r1
            r1 = r4
            r8 = r0
            r0 = r2
            r2 = r8
            goto L_0x005e
        L_0x0126:
            r1 = move-exception
            goto L_0x009c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.au.Af():boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x01a9, code lost:
        if (r0.fS() == false) goto L_0x01ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x01ab, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x01b4, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x01b5, code lost:
        if (r1 != null) goto L_0x01b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x01bb, code lost:
        if (r1.fS() == false) goto L_0x01bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x01bd, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x01cb, code lost:
        r0 = r1;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x01a5 A[SYNTHETIC, Splitter:B:33:0x01a5] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x01b4 A[ExcHandler: all (r0v2 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:1:0x0013] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void Ag() {
        /*
            r8 = this;
            java.lang.String r7 = "YES"
            java.lang.String r6 = "NO"
            java.lang.String r0 = ":"
            com.uc.c.ca r0 = com.uc.c.ca.Ku()
            r1 = 1
            r0.jh(r1)
            r1 = 0
            java.lang.String r2 = "-attribute"
            java.lang.String r3 = "ext:fls"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r4.<init>()     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r0.fp(r2)     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r0.KG()     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r0.KV()     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            java.lang.String[] r2 = com.uc.c.ar.avR     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r3 = 9
            r2 = r2[r3]     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r0.t(r2)     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r2 = 1602765(0x1874cd, float:2.245952E-39)
            r3 = 3
            r0.bs(r3, r2)     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r0.KX()     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            java.lang.String[] r3 = com.uc.c.ar.avR     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r4 = 16
            r3 = r3[r4]     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            java.lang.String r4 = ":"
            java.lang.String r3 = com.uc.c.bc.Z(r3, r4)     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            char[] r3 = r3.toCharArray()     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r0.f(r3)     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r0.KW()     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            java.lang.String r3 = r8.zY()     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            char[] r3 = r3.toCharArray()     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r0.f(r3)     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r0.KY()     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r3 = 3
            r0.bs(r3, r2)     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r0.KX()     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            java.lang.String[] r2 = com.uc.c.ar.avR     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r3 = 10
            r2 = r2[r3]     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            java.lang.String r3 = ":"
            java.lang.String r2 = com.uc.c.bc.Z(r2, r3)     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            char[] r2 = r2.toCharArray()     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r0.f(r2)     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r0.KW()     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            java.lang.String r2 = r8.bjM     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            char[] r2 = r2.toCharArray()     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r0.f(r2)     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r0.KY()     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            java.lang.String r2 = r8.zX()     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            com.uc.c.q r1 = com.uc.c.q.R(r2)     // Catch:{ Exception -> 0x01a1, all -> 0x01b4 }
            r2 = 6535423(0x63b8ff, float:9.158078E-39)
            r3 = 3
            r0.bs(r3, r2)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r0.KX()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            java.lang.String[] r2 = com.uc.c.ar.avR     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r3 = 11
            r2 = r2[r3]     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            java.lang.String r3 = ":"
            java.lang.String r2 = com.uc.c.bc.Z(r2, r3)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            char[] r2 = r2.toCharArray()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r0.f(r2)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r0.KW()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            boolean r2 = r1.isDirectory()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            if (r2 == 0) goto L_0x018e
            long r2 = r1.fW()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
        L_0x00bf:
            int r2 = (int) r2     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            long r2 = (long) r2     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            java.lang.String r2 = com.uc.c.bc.j(r2)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            char[] r2 = r2.toCharArray()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r0.f(r2)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r2 = 0
            r0.iS(r2)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            java.lang.String[] r2 = com.uc.c.ar.avR     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r3 = 12
            r2 = r2[r3]     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            java.lang.String r3 = ":"
            java.lang.String r2 = com.uc.c.bc.Z(r2, r3)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            char[] r2 = r2.toCharArray()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r0.f(r2)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r2 = -1
            r3 = 0
            long r4 = r1.lastModified()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            java.lang.String r2 = com.uc.c.bc.b(r2, r3, r4)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            char[] r2 = r2.toCharArray()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r0.f(r2)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r2 = 0
            r0.iS(r2)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            java.lang.String[] r2 = com.uc.c.ar.avR     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r3 = 13
            r2 = r2[r3]     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            java.lang.String r3 = ":"
            java.lang.String r2 = com.uc.c.bc.Z(r2, r3)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            char[] r2 = r2.toCharArray()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r0.f(r2)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            boolean r2 = r1.canRead()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            if (r2 == 0) goto L_0x0194
            java.lang.String r2 = "YES"
            r2 = r7
        L_0x0114:
            char[] r2 = r2.toCharArray()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r0.f(r2)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r0.KY()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            java.lang.String[] r2 = com.uc.c.ar.avR     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r3 = 14
            r2 = r2[r3]     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            java.lang.String r3 = ":"
            java.lang.String r2 = com.uc.c.bc.Z(r2, r3)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            char[] r2 = r2.toCharArray()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r0.f(r2)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            boolean r2 = r1.canWrite()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            if (r2 == 0) goto L_0x0199
            java.lang.String r2 = "YES"
            r2 = r7
        L_0x013a:
            char[] r2 = r2.toCharArray()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r0.f(r2)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r0.KY()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            java.lang.String[] r2 = com.uc.c.ar.avR     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r3 = 15
            r2 = r2[r3]     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            java.lang.String r3 = ":"
            java.lang.String r2 = com.uc.c.bc.Z(r2, r3)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            char[] r2 = r2.toCharArray()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r0.f(r2)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            boolean r2 = r1.isHidden()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            if (r2 == 0) goto L_0x019d
            java.lang.String r2 = "YES"
            r2 = r7
        L_0x0160:
            char[] r2 = r2.toCharArray()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r0.f(r2)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r0.KY()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r0.KW()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r0.KH()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            com.uc.c.r r2 = r8.mS     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r2.o(r0)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            com.uc.c.r r0 = r8.mS     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r2 = 0
            com.uc.c.bf r3 = r8.oZ     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            r0.b(r2, r3)     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            if (r1 == 0) goto L_0x0188
            boolean r0 = r1.fS()     // Catch:{ Exception -> 0x01cd }
            if (r0 != 0) goto L_0x0188
            r1.close()     // Catch:{ Exception -> 0x01cd }
        L_0x0188:
            com.uc.c.r r0 = r8.mS
            r0.jA()
        L_0x018d:
            return
        L_0x018e:
            long r2 = r1.fT()     // Catch:{ Exception -> 0x01ca, all -> 0x01b4 }
            goto L_0x00bf
        L_0x0194:
            java.lang.String r2 = "NO"
            r2 = r6
            goto L_0x0114
        L_0x0199:
            java.lang.String r2 = "NO"
            r2 = r6
            goto L_0x013a
        L_0x019d:
            java.lang.String r2 = "NO"
            r2 = r6
            goto L_0x0160
        L_0x01a1:
            r0 = move-exception
            r0 = r1
        L_0x01a3:
            if (r0 == 0) goto L_0x01ae
            boolean r1 = r0.fS()     // Catch:{ Exception -> 0x01c8 }
            if (r1 != 0) goto L_0x01ae
            r0.close()     // Catch:{ Exception -> 0x01c8 }
        L_0x01ae:
            com.uc.c.r r0 = r8.mS
            r0.jA()
            goto L_0x018d
        L_0x01b4:
            r0 = move-exception
            if (r1 == 0) goto L_0x01c0
            boolean r2 = r1.fS()     // Catch:{ Exception -> 0x01c6 }
            if (r2 != 0) goto L_0x01c0
            r1.close()     // Catch:{ Exception -> 0x01c6 }
        L_0x01c0:
            com.uc.c.r r1 = r8.mS
            r1.jA()
            throw r0
        L_0x01c6:
            r1 = move-exception
            goto L_0x01c0
        L_0x01c8:
            r0 = move-exception
            goto L_0x01ae
        L_0x01ca:
            r0 = move-exception
            r0 = r1
            goto L_0x01a3
        L_0x01cd:
            r0 = move-exception
            goto L_0x0188
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.au.Ag():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.ca.a(int, java.lang.String, java.lang.String, java.lang.String, int, int, int, int, int, int, int, boolean, boolean, boolean, boolean):void
     arg types: [int, java.lang.String, ?[OBJECT, ARRAY], java.lang.String, int, int, int, int, int, int, int, int, int, int, int]
     candidates:
      com.uc.c.ca.a(int, java.lang.String, java.lang.String, java.lang.String, int, int, int, int, int, boolean, boolean, boolean, boolean, int, int):void
      com.uc.c.ca.a(int, java.lang.String, java.lang.String, java.lang.String, int, int, int, int, int, int, int, boolean, boolean, boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.ca.b(boolean, int):boolean
     arg types: [int, int]
     candidates:
      com.uc.c.ca.b(int, com.uc.c.f):void
      com.uc.c.ca.b(java.io.DataOutputStream, java.util.Vector):void
      com.uc.c.ca.b(java.io.DataOutputStream, byte[]):void
      com.uc.c.ca.b(com.uc.c.f, int):java.lang.String
      com.uc.c.ca.b(com.uc.c.f, byte[]):void
      com.uc.c.ca.b(java.io.DataInputStream, com.uc.c.r):void
      com.uc.c.ca.b(boolean, int):boolean */
    private final void Ah() {
        ca Ku = ca.Ku();
        try {
            Ku.fp(w.OA + aGP);
            Ku.jh(1);
            Ku.KG();
            Ku.KV();
            Ku.t(ar.avR[7]);
            Ku.f(zX().toCharArray());
            Ku.KY();
            Ku.a(0, "n", (String) null, zY(), e.jP(), 0, 4, 512, -1, 2, 1, false, false, false, false);
            Ku.a(bc.dT("h"), (byte[]) null, bc.dT(zX()), 0, 0, 0);
            Ku.b(true, -1);
            Ku.KW();
            Ku.KH();
            this.mS.o(Ku);
            this.mS.b(this.oY, this.oZ);
        } catch (Exception e) {
        } finally {
            this.mS.jA();
        }
    }

    private boolean Aj() {
        String[] strArr = (String[]) this.aGC;
        return S(strArr[1], strArr[0]);
    }

    private void Ak() {
        new bq(2, this).start();
    }

    private boolean Am() {
        return dm(zX());
    }

    private final int Ap() {
        return ((byte[]) ((Object[]) Ar())[2])[1];
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.Vector As() {
        /*
            r12 = this;
            r2 = 5
            r11 = 3
            r10 = 2
            r9 = 1
            r8 = 0
            java.util.Vector r0 = new java.util.Vector
            r0.<init>(r2)
            java.lang.String[] r1 = new java.lang.String[r2]
            java.lang.String[] r2 = new java.lang.String[r2]
            int r3 = com.uc.c.bc.beZ
            if (r3 == 0) goto L_0x0016
            int r3 = com.uc.c.bc.beZ
            if (r3 != r11) goto L_0x00b7
        L_0x0016:
            java.lang.String r3 = "fileconn.dir.graphics.name"
            java.lang.String r3 = com.uc.c.bc.dO(r3)
            r1[r8] = r3
            java.lang.String r3 = "fileconn.dir.graphics"
            java.lang.String r3 = com.uc.c.bc.dO(r3)
            r2[r8] = r3
            java.lang.String r3 = "fileconn.dir.music.name"
            java.lang.String r3 = com.uc.c.bc.dO(r3)
            r1[r9] = r3
            java.lang.String r3 = "fileconn.dir.music"
            java.lang.String r3 = com.uc.c.bc.dO(r3)
            r2[r9] = r3
            java.lang.String r3 = "fileconn.dir.tones.name"
            java.lang.String r3 = com.uc.c.bc.dO(r3)
            r1[r10] = r3
            java.lang.String r3 = "fileconn.dir.tones"
            java.lang.String r3 = com.uc.c.bc.dO(r3)
            r2[r10] = r3
            java.lang.String r3 = "fileconn.dir.photos.name"
            java.lang.String r3 = com.uc.c.bc.dO(r3)
            r1[r11] = r3
            java.lang.String r3 = "fileconn.dir.photos"
            java.lang.String r3 = com.uc.c.bc.dO(r3)
            r2[r11] = r3
            r3 = 4
            java.lang.String r4 = "fileconn.dir.videos.name"
            java.lang.String r4 = com.uc.c.bc.dO(r4)
            r1[r3] = r4
            r3 = 4
            java.lang.String r4 = "fileconn.dir.videos"
            java.lang.String r4 = com.uc.c.bc.dO(r4)
            r2[r3] = r4
            java.lang.String r3 = "file://"
            r4 = r8
        L_0x006b:
            int r5 = r1.length     // Catch:{ Exception -> 0x0103, all -> 0x00b5 }
            if (r4 >= r5) goto L_0x0094
            r5 = r1[r4]     // Catch:{ Exception -> 0x0103, all -> 0x00b5 }
            java.lang.String r5 = r12.dp(r5)     // Catch:{ Exception -> 0x0103, all -> 0x00b5 }
            r1[r4] = r5     // Catch:{ Exception -> 0x0103, all -> 0x00b5 }
            r5 = r1[r4]     // Catch:{ Exception -> 0x0103, all -> 0x00b5 }
            if (r5 == 0) goto L_0x0089
            r5 = r1[r4]     // Catch:{ Exception -> 0x0103, all -> 0x00b5 }
            if (r5 == 0) goto L_0x0091
            r5 = r1[r4]     // Catch:{ Exception -> 0x0103, all -> 0x00b5 }
            r6 = 0
            char r5 = r5.charAt(r6)     // Catch:{ Exception -> 0x0103, all -> 0x00b5 }
            r6 = 256(0x100, float:3.59E-43)
            if (r5 > r6) goto L_0x0091
        L_0x0089:
            java.lang.String[] r5 = com.uc.c.ar.avR     // Catch:{ Exception -> 0x0103, all -> 0x00b5 }
            int r6 = r4 + 41
            r5 = r5[r6]     // Catch:{ Exception -> 0x0103, all -> 0x00b5 }
            r1[r4] = r5     // Catch:{ Exception -> 0x0103, all -> 0x00b5 }
        L_0x0091:
            int r4 = r4 + 1
            goto L_0x006b
        L_0x0094:
            int r4 = r3.length()     // Catch:{ Exception -> 0x0103, all -> 0x00b5 }
            r5 = r8
        L_0x0099:
            int r6 = r2.length     // Catch:{ Exception -> 0x0103, all -> 0x00b5 }
            if (r5 >= r6) goto L_0x00e3
            r6 = r2[r5]     // Catch:{ Exception -> 0x0103, all -> 0x00b5 }
            if (r6 == 0) goto L_0x00b2
            r6 = r2[r5]     // Catch:{ Exception -> 0x0103, all -> 0x00b5 }
            boolean r6 = r6.startsWith(r3)     // Catch:{ Exception -> 0x0103, all -> 0x00b5 }
            if (r6 == 0) goto L_0x00b2
            r6 = r2[r5]     // Catch:{ Exception -> 0x0103, all -> 0x00b5 }
            int r7 = r4 + 1
            java.lang.String r6 = r6.substring(r7)     // Catch:{ Exception -> 0x0103, all -> 0x00b5 }
            r2[r5] = r6     // Catch:{ Exception -> 0x0103, all -> 0x00b5 }
        L_0x00b2:
            int r5 = r5 + 1
            goto L_0x0099
        L_0x00b5:
            r0 = move-exception
            throw r0
        L_0x00b7:
            int r3 = com.uc.c.bc.beZ
            if (r3 != r11) goto L_0x00e3
            int r3 = com.uc.c.bc.bfa
            if (r3 != r9) goto L_0x00e3
            java.lang.String[] r3 = com.uc.c.ar.avR
            r4 = 41
            r3 = r3[r4]
            r1[r8] = r3
            java.lang.String r3 = "Nand/picture/"
            r2[r8] = r3
            java.lang.String[] r3 = com.uc.c.ar.avR
            r4 = 42
            r3 = r3[r4]
            r1[r9] = r3
            java.lang.String r3 = "Nand/music/"
            r2[r9] = r3
            java.lang.String[] r3 = com.uc.c.ar.avR
            r4 = 45
            r3 = r3[r4]
            r1[r10] = r3
            java.lang.String r3 = "Nand/video/"
            r2[r10] = r3
        L_0x00e3:
            r3 = r8
        L_0x00e4:
            int r4 = r1.length
            if (r3 >= r4) goto L_0x0102
            r4 = r2[r3]
            if (r4 == 0) goto L_0x00ff
            java.lang.Object[] r4 = new java.lang.Object[r11]
            r5 = r1[r3]
            r4[r8] = r5
            r5 = r2[r3]
            r4[r9] = r5
            byte[] r5 = new byte[r10]
            r5 = {1, 1} // fill-array
            r4[r10] = r5
            r0.addElement(r4)
        L_0x00ff:
            int r3 = r3 + 1
            goto L_0x00e4
        L_0x0102:
            return r0
        L_0x0103:
            r3 = move-exception
            goto L_0x00e3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.au.As():java.util.Vector");
    }

    private void R(String str, String str2) {
        this.aGC = new String[]{str, str2};
        new bq(1, this).start();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0027, code lost:
        if (r0 != null) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x002d, code lost:
        if (r0.fS() == false) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x002f, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0037, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0038, code lost:
        r3 = r1;
        r1 = r0;
        r0 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0041, code lost:
        if (r1.fS() == false) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0043, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0026 A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:1:0x0002] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x003d A[SYNTHETIC, Splitter:B:30:0x003d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean S(java.lang.String r4, java.lang.String r5) {
        /*
            r2 = 0
            r0 = 0
            boolean r1 = com.uc.c.bc.dM(r4)     // Catch:{ Exception -> 0x0026, all -> 0x0037 }
            if (r1 == 0) goto L_0x004e
            boolean r1 = com.uc.c.bc.dM(r5)     // Catch:{ Exception -> 0x0026, all -> 0x0037 }
            if (r1 == 0) goto L_0x004e
            com.uc.c.q r0 = com.uc.c.q.R(r4)     // Catch:{ Exception -> 0x0026, all -> 0x0037 }
            r0.O(r5)     // Catch:{ Exception -> 0x0026, all -> 0x0049 }
            r1 = 1
        L_0x0016:
            if (r0 == 0) goto L_0x0021
            boolean r2 = r0.fS()     // Catch:{ Exception -> 0x0023 }
            if (r2 != 0) goto L_0x0021
            r0.close()     // Catch:{ Exception -> 0x0023 }
        L_0x0021:
            r0 = r1
        L_0x0022:
            return r0
        L_0x0023:
            r0 = move-exception
            r0 = r1
            goto L_0x0022
        L_0x0026:
            r1 = move-exception
            if (r0 == 0) goto L_0x0032
            boolean r1 = r0.fS()     // Catch:{ Exception -> 0x0034 }
            if (r1 != 0) goto L_0x0032
            r0.close()     // Catch:{ Exception -> 0x0034 }
        L_0x0032:
            r0 = r2
            goto L_0x0022
        L_0x0034:
            r0 = move-exception
            r0 = r2
            goto L_0x0022
        L_0x0037:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
        L_0x003b:
            if (r1 == 0) goto L_0x0046
            boolean r2 = r1.fS()     // Catch:{ Exception -> 0x0047 }
            if (r2 != 0) goto L_0x0046
            r1.close()     // Catch:{ Exception -> 0x0047 }
        L_0x0046:
            throw r0
        L_0x0047:
            r1 = move-exception
            goto L_0x0046
        L_0x0049:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x003b
        L_0x004e:
            r1 = r2
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.au.S(java.lang.String, java.lang.String):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:105:0x01e2  */
    /* JADX WARNING: Removed duplicated region for block: B:115:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0164 A[SYNTHETIC, Splitter:B:59:0x0164] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0169 A[SYNTHETIC, Splitter:B:62:0x0169] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x016e  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0183 A[SYNTHETIC, Splitter:B:73:0x0183] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0188 A[SYNTHETIC, Splitter:B:76:0x0188] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0041 A[Catch:{ Throwable -> 0x015b, all -> 0x017a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void a(java.lang.String r19, java.io.OutputStream r20, com.uc.c.bx r21, com.uc.c.ca r22, int r23, int r24, boolean r25) {
        /*
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r10 = 0
            if (r25 == 0) goto L_0x0121
            if (r22 == 0) goto L_0x01eb
            r0 = r22
            java.util.Vector r0 = r0.bGH     // Catch:{ Throwable -> 0x015b, all -> 0x017a }
            r11 = r0
            if (r11 == 0) goto L_0x01eb
            java.lang.String r10 = "photo://UC_Photo_"
            int r10 = r10.length()     // Catch:{ Throwable -> 0x015b, all -> 0x017a }
            java.lang.String r11 = ".png"
            r0 = r19
            r1 = r11
            int r11 = r0.indexOf(r1)     // Catch:{ Throwable -> 0x015b, all -> 0x017a }
            r0 = r19
            r1 = r10
            r2 = r11
            java.lang.String r19 = r0.substring(r1, r2)     // Catch:{ Throwable -> 0x015b, all -> 0x017a }
            int r19 = java.lang.Integer.parseInt(r19)     // Catch:{ Throwable -> 0x015b, all -> 0x017a }
            r0 = r22
            java.util.Vector r0 = r0.bGH     // Catch:{ Throwable -> 0x015b, all -> 0x017a }
            r22 = r0
            r0 = r22
            r1 = r19
            java.lang.Object r19 = r0.elementAt(r1)     // Catch:{ Throwable -> 0x015b, all -> 0x017a }
            byte[] r19 = (byte[]) r19     // Catch:{ Throwable -> 0x015b, all -> 0x017a }
            byte[] r19 = (byte[]) r19     // Catch:{ Throwable -> 0x015b, all -> 0x017a }
        L_0x003f:
            if (r19 == 0) goto L_0x01e2
            java.io.ByteArrayInputStream r22 = new java.io.ByteArrayInputStream     // Catch:{ Throwable -> 0x015b, all -> 0x017a }
            r0 = r22
            r1 = r19
            r0.<init>(r1)     // Catch:{ Throwable -> 0x015b, all -> 0x017a }
            java.io.DataInputStream r5 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x01c1, all -> 0x01a7 }
            r0 = r5
            r1 = r22
            r0.<init>(r1)     // Catch:{ Throwable -> 0x01c1, all -> 0x01a7 }
            r17 = r5
            r5 = r4
            r4 = r22
            r22 = r17
        L_0x0059:
            if (r22 == 0) goto L_0x0145
            r6 = 10240(0x2800, float:1.4349E-41)
            byte[] r6 = new byte[r6]     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            java.lang.StringBuffer r10 = new java.lang.StringBuffer     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            java.lang.String[] r11 = com.uc.c.ar.avS     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            r12 = 24
            r11 = r11[r12]     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            r10.<init>(r11)     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            if (r25 == 0) goto L_0x013c
            r0 = r19
            int r0 = r0.length     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            r19 = r0
            r0 = r19
            long r0 = (long) r0     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            r11 = r0
        L_0x0075:
            r19 = r7
            r17 = r8
            r7 = r17
        L_0x007b:
            r0 = r22
            r1 = r6
            int r25 = r0.read(r1)     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            r9 = -1
            r0 = r25
            r1 = r9
            if (r0 == r1) goto L_0x0142
            boolean r9 = r21.GZ()     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            if (r9 == 0) goto L_0x0142
            r9 = 0
            r0 = r20
            r1 = r6
            r2 = r9
            r3 = r25
            r0.write(r1, r2, r3)     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            int r19 = r19 + r25
            long r13 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            long r13 = r13 - r7
            r15 = 1000(0x3e8, double:4.94E-321)
            int r25 = (r13 > r15 ? 1 : (r13 == r15 ? 0 : -1))
            if (r25 <= 0) goto L_0x007b
            java.lang.String[] r25 = com.uc.c.ar.avS     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            r7 = 24
            r25 = r25[r7]     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            int r25 = r25.length()     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            r0 = r10
            r1 = r25
            r0.setLength(r1)     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            r25 = 1
            r0 = r24
            r1 = r25
            if (r0 <= r1) goto L_0x00e8
            java.lang.String r25 = " ["
            r0 = r10
            r1 = r25
            java.lang.StringBuffer r25 = r0.append(r1)     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            int r7 = r23 + 1
            r0 = r25
            r1 = r7
            java.lang.StringBuffer r25 = r0.append(r1)     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            r7 = 47
            r0 = r25
            r1 = r7
            java.lang.StringBuffer r25 = r0.append(r1)     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            r0 = r25
            r1 = r24
            java.lang.StringBuffer r25 = r0.append(r1)     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            java.lang.String r7 = "] "
            r0 = r25
            r1 = r7
            r0.append(r1)     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
        L_0x00e8:
            r0 = r19
            long r0 = (long) r0     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            r7 = r0
            java.lang.String r25 = com.uc.c.bc.j(r7)     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            r0 = r10
            r1 = r25
            java.lang.StringBuffer r25 = r0.append(r1)     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            r7 = 47
            r0 = r25
            r1 = r7
            java.lang.StringBuffer r25 = r0.append(r1)     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            java.lang.String r7 = com.uc.c.bc.j(r11)     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            r0 = r25
            r1 = r7
            r0.append(r1)     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            java.lang.String r25 = r10.toString()     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            boolean r7 = r21.GZ()     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            if (r7 == 0) goto L_0x011b
            r0 = r21
            r1 = r25
            r0.eU(r1)     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
        L_0x011b:
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            goto L_0x007b
        L_0x0121:
            com.uc.c.q r19 = com.uc.c.q.R(r19)     // Catch:{ Throwable -> 0x015b, all -> 0x017a }
            boolean r22 = r19.isDirectory()     // Catch:{ Throwable -> 0x01c9, all -> 0x01af }
            if (r22 != 0) goto L_0x01d9
            boolean r22 = r19.exists()     // Catch:{ Throwable -> 0x01c9, all -> 0x01af }
            if (r22 == 0) goto L_0x01d9
            java.io.InputStream r22 = r19.ez()     // Catch:{ Throwable -> 0x01c9, all -> 0x01af }
            r4 = r5
            r5 = r19
            r19 = r10
            goto L_0x0059
        L_0x013c:
            long r11 = r5.fT()     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
            goto L_0x0075
        L_0x0142:
            com.uc.c.bc.gc()     // Catch:{ Throwable -> 0x01d1, all -> 0x01b9 }
        L_0x0145:
            if (r22 == 0) goto L_0x014a
            r22.close()     // Catch:{ Exception -> 0x0197 }
        L_0x014a:
            if (r4 == 0) goto L_0x014f
            r4.close()     // Catch:{ IOException -> 0x0199 }
        L_0x014f:
            if (r5 == 0) goto L_0x015a
            boolean r19 = r5.fS()
            if (r19 != 0) goto L_0x015a
            r5.close()     // Catch:{ IOException -> 0x019b }
        L_0x015a:
            return
        L_0x015b:
            r19 = move-exception
            r19 = r6
            r20 = r5
            r21 = r4
        L_0x0162:
            if (r19 == 0) goto L_0x0167
            r19.close()     // Catch:{ Exception -> 0x019d }
        L_0x0167:
            if (r20 == 0) goto L_0x016c
            r20.close()     // Catch:{ IOException -> 0x019f }
        L_0x016c:
            if (r21 == 0) goto L_0x015a
            boolean r19 = r21.fS()
            if (r19 != 0) goto L_0x015a
            r21.close()     // Catch:{ IOException -> 0x0178 }
            goto L_0x015a
        L_0x0178:
            r19 = move-exception
            goto L_0x015a
        L_0x017a:
            r19 = move-exception
            r20 = r6
            r21 = r5
            r22 = r4
        L_0x0181:
            if (r20 == 0) goto L_0x0186
            r20.close()     // Catch:{ Exception -> 0x01a1 }
        L_0x0186:
            if (r21 == 0) goto L_0x018b
            r21.close()     // Catch:{ IOException -> 0x01a3 }
        L_0x018b:
            if (r22 == 0) goto L_0x0196
            boolean r20 = r22.fS()
            if (r20 != 0) goto L_0x0196
            r22.close()     // Catch:{ IOException -> 0x01a5 }
        L_0x0196:
            throw r19
        L_0x0197:
            r19 = move-exception
            goto L_0x014a
        L_0x0199:
            r19 = move-exception
            goto L_0x014f
        L_0x019b:
            r19 = move-exception
            goto L_0x015a
        L_0x019d:
            r19 = move-exception
            goto L_0x0167
        L_0x019f:
            r19 = move-exception
            goto L_0x016c
        L_0x01a1:
            r20 = move-exception
            goto L_0x0186
        L_0x01a3:
            r20 = move-exception
            goto L_0x018b
        L_0x01a5:
            r20 = move-exception
            goto L_0x0196
        L_0x01a7:
            r19 = move-exception
            r20 = r6
            r21 = r22
            r22 = r4
            goto L_0x0181
        L_0x01af:
            r20 = move-exception
            r21 = r5
            r22 = r19
            r19 = r20
            r20 = r6
            goto L_0x0181
        L_0x01b9:
            r19 = move-exception
            r20 = r22
            r21 = r4
            r22 = r5
            goto L_0x0181
        L_0x01c1:
            r19 = move-exception
            r19 = r6
            r20 = r22
            r21 = r4
            goto L_0x0162
        L_0x01c9:
            r20 = move-exception
            r20 = r5
            r21 = r19
            r19 = r6
            goto L_0x0162
        L_0x01d1:
            r19 = move-exception
            r19 = r22
            r20 = r4
            r21 = r5
            goto L_0x0162
        L_0x01d9:
            r22 = r6
            r4 = r5
            r5 = r19
            r19 = r10
            goto L_0x0059
        L_0x01e2:
            r22 = r6
            r17 = r5
            r5 = r4
            r4 = r17
            goto L_0x0059
        L_0x01eb:
            r19 = r10
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.au.a(java.lang.String, java.io.OutputStream, com.uc.c.bx, com.uc.c.ca, int, int, boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean):void
     arg types: [java.lang.String, int, int, int, int, int, int, int]
     candidates:
      com.uc.c.bk.a(int, int, int, int, int, int, b.a.a.a.f, b.a.a.a.f):void
      com.uc.c.bk.a(int, int, int, int, int[], boolean, int, int):void
      com.uc.c.bk.a(int[], int, int, int, int, int, int, boolean):void
      com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int):void
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean):void */
    public static boolean a(bk bkVar, ca caVar, byte[] bArr, Object obj, int i) {
        int b2 = f.b(bArr);
        int c = f.c(bArr) + 2;
        int d = f.d(bArr);
        int e = f.e(bArr);
        int i2 = c + ((e - 15) >> 1);
        Object[] objArr = (Object[]) obj;
        String str = (String) objArr[0];
        byte[] bArr2 = (byte[]) objArr[2];
        f gb = bArr2[0] == 1 ? bc.aL(bArr2[1], 2) ? az.gb(27) : az.gb(26) : bArr2[0] == 0 ? az.gb(28) : null;
        if (gb != null) {
            bkVar.a(gb, b2, i2, 20);
        }
        bkVar.setColor(az.bdf[250]);
        int n = e.bj(caVar.bFQ).n(str, (d - 13) - 2);
        String Z = n != str.length() ? bc.Z(str.substring(0, n), "..") : str;
        bkVar.d(e.bj(caVar.bFQ));
        bkVar.a(Z, b2 + 13 + 2, c, (d - 13) - 2, e, 1, 2, true);
        return true;
    }

    private final boolean dl(String str) {
        return aGF.equals(str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0026, code lost:
        if (r1 != null) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x002c, code lost:
        if (r1.fS() == false) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x002e, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0037, code lost:
        r0 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return true;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0025 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0002] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean dm(java.lang.String r3) {
        /*
            r0 = 1
            r1 = 0
            com.uc.c.q r1 = com.uc.c.q.R(r3)     // Catch:{ Exception -> 0x0015, all -> 0x0025 }
            r1.cf()     // Catch:{ Exception -> 0x0036, all -> 0x0025 }
            if (r1 == 0) goto L_0x0014
            boolean r2 = r1.fS()     // Catch:{ Exception -> 0x0039 }
            if (r2 != 0) goto L_0x0014
            r1.close()     // Catch:{ Exception -> 0x0039 }
        L_0x0014:
            return r0
        L_0x0015:
            r0 = move-exception
            r0 = r1
        L_0x0017:
            r1 = 0
            if (r0 == 0) goto L_0x0023
            boolean r2 = r0.fS()     // Catch:{ Exception -> 0x0034 }
            if (r2 != 0) goto L_0x0023
            r0.close()     // Catch:{ Exception -> 0x0034 }
        L_0x0023:
            r0 = r1
            goto L_0x0014
        L_0x0025:
            r2 = move-exception
            if (r1 == 0) goto L_0x0014
            boolean r2 = r1.fS()     // Catch:{ Exception -> 0x0032 }
            if (r2 != 0) goto L_0x0014
            r1.close()     // Catch:{ Exception -> 0x0032 }
            goto L_0x0014
        L_0x0032:
            r1 = move-exception
            goto L_0x0014
        L_0x0034:
            r0 = move-exception
            goto L_0x0023
        L_0x0036:
            r0 = move-exception
            r0 = r1
            goto L_0x0017
        L_0x0039:
            r1 = move-exception
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.au.dm(java.lang.String):boolean");
    }

    private void dn(String str) {
        StringBuffer stringBuffer = new StringBuffer(30);
        if (str != null) {
            stringBuffer.append(this.bjM).append(str);
            if (!"".equals(str) && !str.endsWith(aGF)) {
                stringBuffer.append('/');
            }
        } else if (!zV()) {
            stringBuffer.append(m2do(this.bjM));
        } else {
            return;
        }
        c(null, stringBuffer.toString());
        this.mS.jA();
    }

    /* renamed from: do  reason: not valid java name */
    private static String m2do(String str) {
        int length = str.length() - 1;
        int i = 0;
        while (true) {
            if (length < 0) {
                length = i;
                break;
            }
            if (str.charAt(length) == '/') {
                if (i == 1) {
                    break;
                }
                i++;
            }
            length--;
        }
        return length >= 0 ? str.substring(0, length + 1) : str;
    }

    private String dp(String str) {
        if (str == null || str.length() <= 1) {
            return str;
        }
        try {
            String substring = str.endsWith(aGF) ? str.substring(0, str.length() - 1) : str;
            try {
                int lastIndexOf = substring.lastIndexOf(47);
                return (lastIndexOf < 0 || lastIndexOf >= substring.length()) ? substring : substring.substring(lastIndexOf + 1);
            } catch (Exception e) {
                return substring;
            }
        } catch (Exception e2) {
            return str;
        }
    }

    private final void fC(int i) {
        String[] strArr = ar.avR;
        this.bjL = new bn(10, -121, this.mR);
        if (i == 1) {
            this.bjL.a(strArr[19], (byte) 100, (at) null);
            this.bjL.a(strArr[20], (byte) 101, (at) null);
        } else if (i == 2) {
            this.bjL.a(strArr[21], (byte) ca.bGO, (at) null);
            this.bjL.a(strArr[46], (byte) 103, (at) null);
            this.bjL.a(strArr[20], (byte) 101, (at) null);
        }
    }

    private void list() {
        dn("");
    }

    private final void x(byte b2) {
        String[] strArr = ar.avR;
        this.bjL = new bn(10, -121, this.mR);
        if (w.nj() || b2 == 1) {
            this.bjL.a(strArr[3], (byte) ca.bGP, (at) null);
        }
        this.bjL.a(strArr[4], (byte) 21, (at) null);
        this.bjL.a(strArr[5], (byte) 23, (at) null);
        this.bjL.a(strArr[7], (byte) 24, (at) null);
        this.bjL.a(strArr[9], (byte) 25, (at) null);
        this.bjL.a(strArr[6], (byte) 3, (at) null);
        this.bjL.a(strArr[20], (byte) 101, (at) null);
    }

    private final boolean zV() {
        return aGF.equals(this.bjM);
    }

    private boolean zW() {
        ca jm = this.mS.jm();
        return (jm.bHl == null || jm.bHm == -1) ? false : true;
    }

    private String zX() {
        return this.bjM + zY();
    }

    private String zY() {
        return (String) ((Object[]) S(Aq()))[0];
    }

    private final void zZ() {
        if (this.aGH == 2) {
            if (this.mR != null) {
                this.mR.lB().at(zX());
                bb.bdV = this.bjM;
            }
            close();
        }
    }

    public void Ad() {
        if (Af()) {
            list();
            this.mR.mM();
            return;
        }
        list();
        this.mR.mN();
    }

    public boolean Ae() {
        return this.aGE != null && this.aGE.length() > 0;
    }

    public void Ai() {
        if (Aj()) {
            list();
            this.mR.mM();
            return;
        }
        list();
        this.mR.mN();
    }

    public void Al() {
        if (Am()) {
            list();
            this.mR.mM();
            return;
        }
        list();
        this.mR.mN();
    }

    public final boolean An() {
        int Ap = Ap();
        if (Ap == -1) {
            return false;
        }
        return !bc.aL(Ap, 2);
    }

    public final boolean Ao() {
        int Ap = Ap();
        if (Ap == -1) {
            return false;
        }
        return !bc.aL(Ap, 1);
    }

    public byte[] Aq() {
        ca jm = this.mS.jm();
        f fVar = jm.bHl;
        int i = jm.bHm;
        if (fVar == null || i < 0 || i >= fVar.cF || !jm.d(fVar, i)) {
            return null;
        }
        if (fVar == null || i == -1) {
            return null;
        }
        return fVar.cE[i] instanceof byte[] ? (byte[]) fVar.cE[i] : ((f) fVar.cE[i]).cH;
    }

    public Object Ar() {
        return S(Aq());
    }

    public Object S(byte[] bArr) {
        ca jm = this.mS.jm();
        return (Object[]) ((Vector) jm.bGl.get(Integer.valueOf(jm.bS()))).elementAt(f.b(bArr, r.Eu, 0));
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void a(int i, int[] iArr) {
        if (this.mS.BK != null && this.mS.BK.biv == 1) {
            this.mS.BK.bd(i);
        } else if (this.mS.BR == null || this.mS.BR.biv != 1) {
            switch (i) {
                case -8:
                    f((byte) 21);
                    this.mR.Hc();
                    return;
                case -7:
                    a(this.mS.dx());
                    return;
                case d.aaJ:
                    a(this.mS.dw());
                    return;
                case 8:
                case 53:
                    if (dQ()) {
                        return;
                    }
                    break;
            }
            this.mS.b(i, iArr);
        } else {
            this.mS.BR.bd(i);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.a(byte, boolean):boolean
     arg types: [byte, int]
     candidates:
      com.uc.c.r.a(com.uc.c.r, com.uc.c.bu):com.uc.c.bu
      com.uc.c.r.a(int, com.uc.c.ca):void
      com.uc.c.r.a(com.uc.c.ca, java.lang.String):void
      com.uc.c.r.a(com.uc.c.ca, boolean):void
      com.uc.c.r.a(byte, boolean):boolean */
    public final void a(bf bfVar) {
        if (bfVar != null) {
            if (bfVar == this.bjJ) {
                this.mS.BK.g(this.mS);
                this.mS.jA();
            } else if (bfVar == this.bjK) {
                if (this.mS.BK != null && this.mS.BK.biv == 1) {
                    this.mS.BK.ET();
                    this.mS.jA();
                } else if (!zV()) {
                    dn(null);
                } else {
                    close();
                    this.mR.Hc();
                }
            } else if (bfVar == this.oZ) {
                b(this.bjJ, this.bjK);
                this.mS.a((byte) -1, true);
            } else if (bfVar == this.oY) {
                ca jm = this.mS.jm();
                String[][] ce = jm.bGd.ce();
                if (jm.bFA.equals("ext:fls-rename") && ce != null && ce[0][0] != null && ce[0][0].equals("n") && ce[0][1] != null && ce[0][1].equals("h") && ce[1][0] != null && ce[1][1] != null) {
                    R(ce[1][0], ce[1][1]);
                    list();
                }
            } else if (bfVar != null && this.mS != null) {
                this.mS.b(bfVar);
            }
        }
    }

    public final void b(bf bfVar, bf bfVar2) {
        if (zV()) {
            this.mS.b((bf) null, bfVar2);
        } else {
            this.mS.b(bfVar, bfVar2);
        }
    }

    public void c(ca caVar) {
        c(caVar, this.bjM);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.ca.b(boolean, int):boolean
     arg types: [int, int]
     candidates:
      com.uc.c.ca.b(int, com.uc.c.f):void
      com.uc.c.ca.b(java.io.DataOutputStream, java.util.Vector):void
      com.uc.c.ca.b(java.io.DataOutputStream, byte[]):void
      com.uc.c.ca.b(com.uc.c.f, int):java.lang.String
      com.uc.c.ca.b(com.uc.c.f, byte[]):void
      com.uc.c.ca.b(java.io.DataInputStream, com.uc.c.r):void
      com.uc.c.ca.b(boolean, int):boolean */
    public final void c(ca caVar, String str) {
        String str2;
        q qVar;
        String[] strArr;
        int indexOf;
        int indexOf2;
        q R;
        String[] strArr2 = null;
        q qVar2 = null;
        try {
            if (!dl(str)) {
                R = q.R(str);
                if (R != null) {
                    strArr2 = R.list();
                }
                if (strArr2 == null) {
                    str2 = aGF;
                    q qVar3 = R;
                    strArr = strArr2;
                    qVar = qVar3;
                } else {
                    str2 = str;
                    String[] strArr3 = strArr2;
                    qVar = R;
                    strArr = strArr3;
                }
            } else {
                str2 = str;
                qVar = null;
                strArr = null;
            }
        } catch (Exception e) {
            if (0 == 0) {
                str2 = aGF;
                q qVar4 = R;
                strArr = null;
                qVar = qVar4;
            } else {
                str2 = str;
                qVar = R;
                strArr = null;
            }
        } catch (Exception e2) {
            e = e2;
        } catch (Throwable th) {
            if (0 == 0) {
            }
            throw th;
        }
        try {
            if (dl(str2)) {
                strArr = q.fY();
            }
            if (strArr == null) {
                if (!w.ng()) {
                    this.mR.aY(ar.avM[50] + ar.avR[33]);
                } else {
                    this.mR.mN();
                }
                close();
                if (qVar != null) {
                    try {
                        if (!qVar.fS()) {
                            qVar.close();
                        }
                    } catch (Exception e3) {
                    }
                }
            } else {
                this.bjM = str2;
                ca Ku = caVar == null ? ca.Ku() : caVar;
                Ku.jh(1);
                Ku.fp(w.OA);
                Ku.iH(10);
                Ku.iH(9);
                Ku.iH(15);
                Ku.bHs = (byte) (Ku.bHs | 8);
                Ku.iG(2);
                Ku.KG();
                Ku.KV();
                int i = w.MM - (Ku.bGw << 1);
                int bm = e.bm(e.jP()) + 4;
                Ku.bFM = 0;
                if (!zV()) {
                    r.a(Ku, 57, i, bm, new Object[]{"..", "..", new byte[]{1, 3}});
                } else {
                    for (int i2 = 0; i2 < this.aGR.size(); i2++) {
                        r.a(Ku, 57, i, bm, (Object[]) this.aGR.elementAt(i2));
                    }
                }
                if (this.aGI == 1) {
                    int i3 = 0;
                    while (strArr != null && i3 < strArr.length) {
                        String str3 = strArr[i3];
                        boolean z = !str3.endsWith(aGF);
                        if ((!z || str3.endsWith(".uhtml")) && ((indexOf2 = str3.indexOf(47, 0)) < 0 || indexOf2 >= str3.length() - 1)) {
                            if (!z) {
                                str3 = str3.substring(0, str3.length() - 1);
                            }
                            Object[] objArr = new Object[3];
                            objArr[0] = str3;
                            objArr[1] = str3;
                            byte[] bArr = new byte[2];
                            bArr[0] = z ? (byte) 0 : 1;
                            bArr[1] = z ? (byte) 0 : 1;
                            objArr[2] = bArr;
                            r.a(Ku, 57, i, bm, objArr);
                        }
                        i3++;
                    }
                } else {
                    for (int i4 = 0; i4 < 2; i4++) {
                        int i5 = 0;
                        while (strArr != null) {
                            if (i5 >= strArr.length) {
                                break;
                            }
                            String str4 = strArr[i5];
                            boolean z2 = !str4.endsWith(aGF);
                            if ((!z2 || i4 != 0) && ((z2 || i4 != 1) && ((indexOf = str4.indexOf(47, 0)) < 0 || indexOf >= str4.length() - 1))) {
                                if (!z2) {
                                    str4 = str4.substring(0, str4.length() - 1);
                                }
                                Object[] objArr2 = new Object[3];
                                objArr2[0] = str4;
                                objArr2[1] = str4;
                                byte[] bArr2 = new byte[2];
                                bArr2[0] = z2 ? (byte) 0 : 1;
                                bArr2[1] = z2 ? (byte) 0 : 1;
                                objArr2[2] = bArr2;
                                r.a(Ku, 57, i, bm, objArr2);
                            }
                            i5++;
                        }
                    }
                }
                Ku.Lq();
                Ku.KH();
                Ku.b(true, -1);
                this.mS.o(Ku);
                b(this.bjJ, this.bjK);
                if (qVar != null) {
                    try {
                        if (!qVar.fS()) {
                            qVar.close();
                        }
                    } catch (Exception e4) {
                    }
                }
            }
        } catch (Exception e5) {
            Exception exc = e5;
            qVar2 = qVar;
            e = exc;
            try {
                close();
                if (e.toString().toLowerCase().indexOf("securityexception") != -1) {
                    this.mR.aY(ar.avM[50] + ar.avR[33]);
                } else {
                    this.mR.mN();
                }
                if (qVar2 != null) {
                    try {
                        if (!qVar2.fS()) {
                            qVar2.close();
                        }
                    } catch (Exception e6) {
                    }
                }
            } catch (Throwable th2) {
                th = th2;
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            qVar2 = qVar;
            th = th4;
            if (qVar2 != null) {
                try {
                    if (!qVar2.fS()) {
                        qVar2.close();
                    }
                } catch (Exception e7) {
                }
            }
            throw th;
        }
    }

    public final void close() {
        aGG = this.bjM;
        this.mS.close();
    }

    public final boolean dQ() {
        this.mS.jm();
        byte[] Aq = Aq();
        if (Aq == null) {
            return true;
        }
        if (Aq[0] != 57) {
            return false;
        }
        Object[] objArr = (Object[]) S(Aq);
        byte[] bArr = (byte[]) objArr[2];
        if (bArr[0] == 1) {
            if (bc.aL(bArr[1], 2)) {
                dn(null);
            } else {
                dn((String) objArr[1]);
            }
        } else if (bArr[0] == 0) {
            if (this.aGH == 1) {
                f((byte) 100);
            } else if (this.aGH == 2) {
                f(ca.bGO);
            } else if (this.aGH == 0) {
            }
        }
        return true;
    }

    public final void f(byte b2) {
        switch (b2) {
            case 1:
                a(8, new int[1]);
                return;
            case 3:
                if (!Ae()) {
                    this.mR.aY(ar.avR[18]);
                    return;
                } else {
                    Ac();
                    return;
                }
            case 21:
                if (zW() && An() && !this.mS.a((byte) -121, b2, (Object) null)) {
                    Ak();
                    return;
                }
                return;
            case 23:
                int Ap = Ap();
                if (!bc.aL(Ap, 1) ? true : Ap == -1 ? false : false) {
                    this.aGD = zX();
                    this.aGE = zY();
                    this.mR.aY(ar.avR[17]);
                    return;
                }
                this.mR.mN();
                return;
            case 24:
                if (zW() && An()) {
                    Ah();
                    return;
                }
                return;
            case 25:
                if (zW() && An()) {
                    Ag();
                    return;
                }
                return;
            case 100:
                Ab();
                this.mR.Hc();
                return;
            case 101:
                close();
                this.mR.Hc();
                return;
            case 102:
                if (zW() && An() && Ao()) {
                    zZ();
                    this.mR.Hc();
                    return;
                }
                return;
            case 103:
                Aa();
                return;
            case 104:
                dQ();
                return;
            default:
                return;
        }
    }
}
