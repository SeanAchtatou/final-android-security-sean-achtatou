package com.uc.c;

import b.a.a.a.f;

class x {
    private static final int SF = 4096;
    private f SA;
    private byte[] SB = new byte[256];
    private int SC = 0;
    private boolean SD = false;
    private int SE;
    private short[] SG;
    private byte[] SH;
    private byte[] SI;
    private byte[] SJ;
    private byte[] Sl;
    int Sm;
    int Sn;
    private boolean So;
    private int Sp;
    private int[] Sq;
    private int[] Sr;
    private int[] Ss;
    private boolean St;
    private boolean Su;
    private int Sv;
    private int Sw;
    private int Sx;
    private int Sy;
    private int Sz;

    x() {
    }

    private void close() {
        this.Sr = null;
        this.Ss = null;
        this.Sq = null;
        this.SG = null;
        this.SH = null;
        this.SI = null;
        this.SB = null;
        this.Sl = null;
    }

    private int dQ(int i) {
        this.SC = read();
        if (i == 0) {
            j(this.SB, 0, this.SC);
        } else if (i == 1) {
            dS(this.SC);
        }
        return this.SC;
    }

    private int[] dR(int i) {
        int[] iArr = new int[i];
        for (int i2 = 0; i2 < i; i2++) {
            iArr[i2] = -16777216 | (read() << 16) | (read() << 8) | read();
        }
        return iArr;
    }

    private void dS(int i) {
        if (this.Sm + i < this.Sn) {
            this.Sm += i;
        }
    }

    private void j(byte[] bArr, int i, int i2) {
        System.arraycopy(this.Sl, this.Sm, bArr, 0, i2);
        this.Sm += i2;
    }

    private int read() {
        byte[] bArr = this.Sl;
        int i = this.Sm;
        this.Sm = i + 1;
        return bArr[i] & 255;
    }

    private void rg() {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        int[] iArr = new int[(this.Sy * this.Sz)];
        int i5 = 8;
        int i6 = 1;
        int i7 = 0;
        while (i7 < this.Sz) {
            if (this.Su) {
                if (i4 >= this.Sz) {
                    i6++;
                    switch (i6) {
                        case 2:
                            i4 = 4;
                            break;
                        case 3:
                            i4 = 2;
                            i5 = 4;
                            break;
                        case 4:
                            i4 = 1;
                            i5 = 2;
                            break;
                    }
                }
                i = i6;
                i2 = i5;
                i3 = i4 + i5;
            } else {
                i = i6;
                i2 = i5;
                i3 = i4;
                i4 = i7;
            }
            int i8 = i4 + this.Sx;
            if (i8 < this.Sz) {
                int i9 = i8 * this.Sy;
                int i10 = this.Sw + i9;
                int i11 = this.Sy + i10;
                int i12 = this.Sy + i9 < i11 ? i9 + this.Sy : i11;
                int i13 = i10;
                int i14 = this.Sy * i7;
                while (i13 < i12) {
                    int i15 = i14 + 1;
                    int i16 = this.Ss[this.SJ[i14] & 255];
                    if (i16 != 0) {
                        iArr[i13] = i16;
                    }
                    i13++;
                    i14 = i15;
                }
            }
            i7++;
            i4 = i3;
            i5 = i2;
            i6 = i;
        }
        this.SA = f.a(iArr, this.Sy, this.Sz, true);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r9v12, types: [int] */
    /* JADX WARN: Type inference failed for: r16v7, types: [int] */
    /* JADX WARN: Type inference failed for: r17v6, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void rh() {
        /*
            r24 = this;
            r2 = -1
            r0 = r24
            int r0 = r0.Sy
            r3 = r0
            r0 = r24
            int r0 = r0.Sz
            r4 = r0
            int r3 = r3 * r4
            r0 = r24
            byte[] r0 = r0.SJ
            r4 = r0
            if (r4 == 0) goto L_0x001b
            r0 = r24
            byte[] r0 = r0.SJ
            r4 = r0
            int r4 = r4.length
            if (r4 >= r3) goto L_0x0022
        L_0x001b:
            byte[] r4 = new byte[r3]
            r0 = r4
            r1 = r24
            r1.SJ = r0
        L_0x0022:
            r0 = r24
            short[] r0 = r0.SG
            r4 = r0
            if (r4 != 0) goto L_0x0032
            r4 = 4096(0x1000, float:5.74E-42)
            short[] r4 = new short[r4]
            r0 = r4
            r1 = r24
            r1.SG = r0
        L_0x0032:
            r0 = r24
            byte[] r0 = r0.SH
            r4 = r0
            if (r4 != 0) goto L_0x0042
            r4 = 4096(0x1000, float:5.74E-42)
            byte[] r4 = new byte[r4]
            r0 = r4
            r1 = r24
            r1.SH = r0
        L_0x0042:
            r0 = r24
            byte[] r0 = r0.SI
            r4 = r0
            if (r4 != 0) goto L_0x0052
            r4 = 4097(0x1001, float:5.741E-42)
            byte[] r4 = new byte[r4]
            r0 = r4
            r1 = r24
            r1.SI = r0
        L_0x0052:
            int r4 = r24.read()
            r5 = 1
            int r5 = r5 << r4
            int r6 = r5 + 1
            int r7 = r5 + 2
            int r8 = r4 + 1
            r9 = 1
            int r9 = r9 << r8
            r10 = 1
            int r9 = r9 - r10
            r10 = 0
        L_0x0063:
            if (r10 >= r5) goto L_0x0078
            r0 = r24
            short[] r0 = r0.SG
            r11 = r0
            r12 = 0
            r11[r10] = r12
            r0 = r24
            byte[] r0 = r0.SH
            r11 = r0
            byte r12 = (byte) r10
            r11[r10] = r12
            int r10 = r10 + 1
            goto L_0x0063
        L_0x0078:
            r10 = 0
            r11 = 0
            r12 = r11
            r13 = r10
            r14 = r10
            r15 = r2
            r16 = r8
            r17 = r9
            r18 = r7
            r11 = r10
            r7 = r10
            r8 = r10
            r9 = r10
        L_0x0088:
            if (r12 >= r3) goto L_0x009d
            if (r9 != 0) goto L_0x0189
            r0 = r14
            r1 = r16
            if (r0 >= r1) goto L_0x00ca
            if (r13 != 0) goto L_0x00b1
            r8 = 0
            r0 = r24
            r1 = r8
            int r8 = r0.dQ(r1)
            if (r8 > 0) goto L_0x00ab
        L_0x009d:
            r2 = r7
        L_0x009e:
            if (r2 >= r3) goto L_0x01a4
            r0 = r24
            byte[] r0 = r0.SJ
            r4 = r0
            r5 = 0
            r4[r2] = r5
            int r2 = r2 + 1
            goto L_0x009e
        L_0x00ab:
            r13 = 0
            r23 = r13
            r13 = r8
            r8 = r23
        L_0x00b1:
            r0 = r24
            byte[] r0 = r0.SB
            r19 = r0
            byte r19 = r19[r8]
            r0 = r19
            r0 = r0 & 255(0xff, float:3.57E-43)
            r19 = r0
            int r19 = r19 << r14
            int r11 = r11 + r19
            int r14 = r14 + 8
            int r8 = r8 + 1
            int r13 = r13 + -1
            goto L_0x0088
        L_0x00ca:
            r19 = r11 & r17
            int r11 = r11 >> r16
            int r14 = r14 - r16
            r0 = r19
            r1 = r18
            if (r0 > r1) goto L_0x009d
            r0 = r19
            r1 = r6
            if (r0 == r1) goto L_0x009d
            r0 = r19
            r1 = r5
            if (r0 != r1) goto L_0x00f4
            int r15 = r4 + 1
            r16 = 1
            int r16 = r16 << r15
            r17 = 1
            int r16 = r16 - r17
            int r17 = r5 + 2
            r18 = r17
            r17 = r16
            r16 = r15
            r15 = r2
            goto L_0x0088
        L_0x00f4:
            if (r15 != r2) goto L_0x010e
            r0 = r24
            byte[] r0 = r0.SI
            r10 = r0
            int r15 = r9 + 1
            r0 = r24
            byte[] r0 = r0.SH
            r20 = r0
            byte r20 = r20[r19]
            r10[r9] = r20
            r9 = r15
            r10 = r19
            r15 = r19
            goto L_0x0088
        L_0x010e:
            r0 = r19
            r1 = r18
            if (r0 != r1) goto L_0x01aa
            r0 = r24
            byte[] r0 = r0.SI
            r20 = r0
            int r21 = r9 + 1
            byte r10 = (byte) r10
            r20[r9] = r10
            r9 = r21
            r10 = r15
        L_0x0122:
            if (r10 <= r5) goto L_0x0141
            r0 = r24
            byte[] r0 = r0.SI
            r20 = r0
            int r21 = r9 + 1
            r0 = r24
            byte[] r0 = r0.SH
            r22 = r0
            byte r22 = r22[r10]
            r20[r9] = r22
            r0 = r24
            short[] r0 = r0.SG
            r9 = r0
            short r9 = r9[r10]
            r10 = r9
            r9 = r21
            goto L_0x0122
        L_0x0141:
            r0 = r24
            byte[] r0 = r0.SH
            r20 = r0
            byte r10 = r20[r10]
            r10 = r10 & 255(0xff, float:3.57E-43)
            r20 = 4096(0x1000, float:5.74E-42)
            r0 = r18
            r1 = r20
            if (r0 >= r1) goto L_0x009d
            r0 = r24
            byte[] r0 = r0.SI
            r20 = r0
            int r21 = r9 + 1
            r0 = r10
            byte r0 = (byte) r0
            r22 = r0
            r20[r9] = r22
            r0 = r24
            short[] r0 = r0.SG
            r9 = r0
            short r15 = (short) r15
            r9[r18] = r15
            r0 = r24
            byte[] r0 = r0.SH
            r9 = r0
            byte r15 = (byte) r10
            r9[r18] = r15
            int r9 = r18 + 1
            r15 = r9 & r17
            if (r15 != 0) goto L_0x01a5
            r15 = 4096(0x1000, float:5.74E-42)
            if (r9 >= r15) goto L_0x01a5
            int r15 = r16 + 1
            int r16 = r17 + r9
        L_0x017f:
            r17 = r16
            r18 = r9
            r16 = r15
            r9 = r21
            r15 = r19
        L_0x0189:
            int r9 = r9 + -1
            r0 = r24
            byte[] r0 = r0.SJ
            r19 = r0
            int r20 = r7 + 1
            r0 = r24
            byte[] r0 = r0.SI
            r21 = r0
            byte r21 = r21[r9]
            r19[r7] = r21
            int r7 = r12 + 1
            r12 = r7
            r7 = r20
            goto L_0x0088
        L_0x01a4:
            return
        L_0x01a5:
            r15 = r16
            r16 = r17
            goto L_0x017f
        L_0x01aa:
            r10 = r19
            goto L_0x0122
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.x.rh():void");
    }

    private void ri() {
        boolean z = false;
        boolean z2 = false;
        while (!z2 && !z) {
            switch (read()) {
                case 0:
                    break;
                case 33:
                    switch (read()) {
                        case 249:
                            rj();
                            continue;
                        default:
                            ro();
                            continue;
                    }
                case 44:
                    rl();
                    z = true;
                    break;
                case 59:
                    z2 = true;
                    break;
                default:
                    return;
            }
        }
    }

    private void rj() {
        dS(1);
        this.SD = (read() & 1) != 0;
        dS(2);
        this.SE = read();
        dS(1);
    }

    private void rk() {
        dS(6);
        rm();
        if (this.So) {
            this.Sq = dR(this.Sp);
        }
    }

    private void rl() {
        int i;
        this.Sw = rn();
        this.Sx = rn();
        this.Sy = rn();
        this.Sz = rn();
        int read = read();
        this.St = (read & 128) != 0;
        this.Su = (read & 64) != 0;
        this.Sv = 2 << (read & 7);
        if (this.St) {
            this.Sr = dR(this.Sv);
            this.Ss = this.Sr;
        } else {
            this.Ss = this.Sq;
        }
        if (this.SD) {
            i = this.Ss[this.SE];
            this.Ss[this.SE] = 0;
        } else {
            i = 0;
        }
        if (this.Ss != null) {
            rh();
            ro();
            rg();
            if (this.SD) {
                this.Ss[this.SE] = i;
            }
        }
    }

    private void rm() {
        dS(4);
        int read = read();
        this.So = (read & 128) != 0;
        this.Sp = 2 << (read & 7);
        dS(2);
    }

    private int rn() {
        return read() | (read() << 8);
    }

    private void ro() {
        do {
            dQ(1);
        } while (this.SC > 0);
    }

    public f i(byte[] bArr, int i, int i2) {
        if (bArr == null || i < 0 || i2 <= 0 || i2 > bArr.length) {
            return null;
        }
        this.Sl = bArr;
        this.Sm = i;
        this.Sn = i2;
        rk();
        ri();
        close();
        return this.SA;
    }
}
