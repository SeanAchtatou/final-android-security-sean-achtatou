package com.uc.c;

import android.graphics.PathEffect;
import android.graphics.Rect;
import b.a.a.a.f;
import b.a.a.a.k;
import b.a.a.a.u;
import com.uc.b.e;

public class bk {
    private static final byte bkH = 30;
    private static final int[] bkI = {15658734, 16776960, 16744640, 16711808, 33023, 8454016, 16744448, 5592405};
    private static f[] bkJ = new f[bkI.length];
    public u bkB;
    public final int bkC = 5;
    private Object[] bkD = new Object[5];
    private int[] bkE = new int[5];
    private Object[] bkF = new Object[5];
    private int bkG = -1;
    private int[] bkK = new int[4];
    PathEffect bkL = null;
    int bkM = 0;
    public int left;
    public int top;

    public bk(u uVar) {
        a(uVar, 0, 0);
    }

    public bk(u uVar, int i, int i2) {
        a(uVar, i, i2);
    }

    public static final int bd(int i, int i2) {
        int i3 = (i & 16711680) >> 16;
        int i4 = (i & 65280) >> 8;
        int i5 = i & 255;
        return (Math.abs(i3 - ((16711680 & i2) >> 16)) >= 50 || Math.abs(i4 - ((65280 & i2) >> 8)) >= 50 || Math.abs(i5 - (i2 & 255)) >= 50) ? i : (((i3 + 64) % 255) << 16) | (((i4 + 64) % 255) << 8) | ((i5 + 64) % 255);
    }

    public k DA() {
        return this.bkB.DA();
    }

    public int DB() {
        return this.bkB.DB() - this.left;
    }

    public int DC() {
        return this.bkB.DC() - this.top;
    }

    public int DD() {
        return this.bkB.DD();
    }

    public int DE() {
        return this.bkB.DE();
    }

    public void DF() {
        this.bkB.DF();
    }

    public void DG() {
        this.bkB.DG();
    }

    public final void Ds() {
        this.bkB.Ds();
    }

    public final void Dt() {
        this.bkB.Dt();
    }

    public final void Du() {
        this.bkB.save();
    }

    public final void Dv() {
        this.bkB.restore();
    }

    public void Dw() {
        this.bkL = this.bkB.DT();
    }

    public void Dx() {
        this.bkB.a(this.bkL);
    }

    public void Dy() {
        this.bkM = this.bkB.DU();
    }

    public void Dz() {
        this.bkB.gY(this.bkM);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.a.a.k.b(java.lang.String, int, boolean):int
     arg types: [java.lang.String, int, int]
     candidates:
      b.a.a.a.k.b(char[], int, int):int
      b.a.a.a.k.b(java.lang.String, int, java.lang.String):java.lang.String
      b.a.a.a.k.b(java.lang.String, int, boolean):int */
    public int a(String str, int i, int i2, int i3, int i4, int i5, int i6, k kVar, int i7) {
        String str2;
        String str3;
        if (str == null) {
            return i5 + i7;
        }
        int i8 = i5;
        int n = kVar.n(str, i3 - (i6 << 1));
        String str4 = str;
        while (true) {
            if (n != str4.length()) {
                str2 = str4.substring(0, n);
                str3 = str4.substring(n, str4.length());
            } else {
                str2 = str4;
                str3 = str4;
            }
            b(str2, i + i6, i8, i3 - (i6 << 1), i4, 1, 1);
            int i9 = i8 + i7;
            if (i9 >= i2 + i4 || n == str3.length()) {
                return i9;
            }
            i8 = i9;
            n = kVar.b(str3, i3 - (i6 << 1), true);
            str4 = str3;
        }
    }

    public void a(char c, int i, int i2, int i3) {
        this.bkB.a(c, this.left + i, this.top + i2, i3);
    }

    public void a(int i, int i2, int i3, int i4, int i5, int i6, f fVar, f fVar2) {
        if (fVar != null) {
            int i7 = this.left + i;
            int i8 = this.top + i2;
            int i9 = this.left + i5;
            int i10 = this.top + i6;
            if (fVar2 != null) {
                fVar2.jU().a(fVar, 0, 0, 20);
                Du();
                this.bkB.l(i9, i10, i3, i4);
                this.bkB.a(fVar2, i9 - i7, i10 - i8, 20);
                Dv();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(int, int, boolean):void
     arg types: [int, int, int]
     candidates:
      com.uc.c.bk.a(int, int, int[][]):void
      com.uc.c.bk.a(b.a.a.a.u, int, int):void
      com.uc.c.bk.a(int, int, boolean):void */
    public void a(int i, int i2, int i3, int i4, int i5, boolean z) {
        int i6 = i5 == 1 ? 2 : 1;
        if (z) {
            m(i + i6, i2, ((i + i3) - i6) - 1, i2);
            m(i + i6, (i2 + i4) - 1, ((i + i3) - i6) - 1, (i2 + i4) - 1);
            m(i, i2 + i6, i, ((i2 + i4) - 1) - i6);
            m((i + i3) - 1, i2 + i6, (i + i3) - 1, ((i2 + i4) - 1) - i6);
            if (i5 == 1) {
                be(i + 1, i2 + 1);
                be(i + 1, (i2 + i4) - 2);
                be((i + i3) - 2, i2 + 1);
                be((i + i3) - 2, (i2 + i4) - 2);
                return;
            }
            return;
        }
        this.bkB.m(i + i6, i2, ((i + i3) - i6) - 1, i2);
        this.bkB.m(i + i6, (i2 + i4) - 1, ((i + i3) - i6) - 1, (i2 + i4) - 1);
        this.bkB.m(i, i2 + i6, i, ((i2 + i4) - 1) - i6);
        this.bkB.m((i + i3) - 1, i2 + i6, (i + i3) - 1, ((i2 + i4) - 1) - i6);
        if (i5 == 1) {
            a(i + 1, i2 + 1, false);
            a(i + 1, (i2 + i4) - 2, false);
            a((i + i3) - 2, i2 + 1, false);
            a((i + i3) - 2, (i2 + i4) - 2, false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(int, int, int, int, int[], boolean, int, int):void
     arg types: [int, int, int, int, int[], int, int, int]
     candidates:
      com.uc.c.bk.a(int, int, int, int, int, int, b.a.a.a.f, b.a.a.a.f):void
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean):void
      com.uc.c.bk.a(int[], int, int, int, int, int, int, boolean):void
      com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int):void
      com.uc.c.bk.a(int, int, int, int, int[], boolean, int, int):void */
    public final void a(int i, int i2, int i3, int i4, int[] iArr, int i5, int i6) {
        a(i, i2, i3, i4, iArr, true, i5, i6);
    }

    public final void a(int i, int i2, int i3, int i4, int[] iArr, boolean z, int i5, int i6) {
        int color = getColor();
        setColor(iArr[i5 + 1]);
        a(i, i2 + 1, i3, i4 - 1, 1, z);
        setColor(iArr[i5 + 0]);
        a(i, i2, i3, i4 - 1, 1, z);
        setColor(iArr[i5 + 2]);
        if (z) {
            m(i + 2, i2 + 1, (i + i3) - 3, i2 + 1);
        } else {
            this.bkB.m(i + 2, i2 + 1, (i + i3) - 3, i2 + 1);
        }
        setColor(color);
    }

    public void a(int i, int i2, boolean z) {
        if (z) {
            m(i, i2, i, i2);
        } else {
            this.bkB.m(i, i2, i, i2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(int, int, int[][], boolean):void
     arg types: [int, int, int[][], int]
     candidates:
      com.uc.c.bk.a(char, int, int, int):void
      com.uc.c.bk.a(b.a.a.a.f, int, int, int):void
      com.uc.c.bk.a(int, int, int[][], boolean):void */
    public void a(int i, int i2, int[][] iArr) {
        a(i, i2, iArr, true);
    }

    public void a(int i, int i2, int[][] iArr, boolean z) {
        save();
        for (int i3 = 0; i3 < iArr.length; i3++) {
            for (int i4 = 0; i4 < iArr[i3].length; i4++) {
                if (iArr[i3][i4] != -1) {
                    setColor(iArr[i3][i4]);
                    a(i + i4, i2 + i3, z);
                }
            }
        }
        reset();
    }

    public void a(f fVar, int i, int i2, int i3) {
        this.bkB.a(fVar, this.left + i, this.top + i2, i3);
    }

    public void a(f fVar, int i, int i2, int i3, int i4, boolean z) {
        Rect rect = new Rect(0, 0, fVar.getWidth(), fVar.getHeight());
        int i5 = this.left + i;
        int i6 = this.top + i2;
        this.bkB.a(fVar, rect, new Rect(i5, i6, i5 + i3, i6 + i4), z);
    }

    public void a(k kVar, boolean z) {
        this.bkB.a(kVar, z);
    }

    public final void a(u uVar, int i, int i2) {
        this.top = i;
        this.left = i2;
        this.bkB = uVar;
    }

    public void a(String str, int i, int i2, int i3, int i4, int i5) {
        this.bkB.a(str, i, i2, i3 + this.left, i4 + this.top, i5);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean):void
     arg types: [java.lang.String, int, int, int, int, int, int, int]
     candidates:
      com.uc.c.bk.a(int, int, int, int, int, int, b.a.a.a.f, b.a.a.a.f):void
      com.uc.c.bk.a(int, int, int, int, int[], boolean, int, int):void
      com.uc.c.bk.a(int[], int, int, int, int, int, int, boolean):void
      com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int):void
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean):void */
    public void a(String str, int i, int i2, int i3, int i4, int i5, int i6) {
        if (str != null && str.length() != 0) {
            int cD = DA().cD("...");
            if (DA().cD(str) > i3) {
                int a2 = bc.a(str.toCharArray(), i3 - cD, this.bkB);
                String substring = str.substring(0, a2);
                if (a2 < str.length()) {
                    substring = substring + "...";
                }
                a(substring, i, i2, i3, i4, i5, i6, true);
                return;
            }
            a(str, i, i2, i3, i4, i5, i6, true);
        }
    }

    public void a(String str, int i, int i2, int i3, int i4, int i5, int i6, boolean z) {
        a(str, i, i2, i3, i4, i5, i6, z, 0);
    }

    public void a(String str, int i, int i2, int i3, int i4, int i5, int i6, boolean z, int i7) {
        int i8;
        k DA = DA();
        int cE = DA.cE(str);
        int uN = DA.uN();
        int i9 = cE >= i3 ? 1 : i5;
        int i10 = uN >= i4 ? 3 : i6;
        int i11 = 0;
        if (i9 == 2) {
            i11 = (i3 - cE) >> 1;
        }
        int i12 = i9 == 3 ? i3 - cE : i11;
        if (i12 <= 0) {
            i12 = 0;
        }
        int i13 = i12 + i;
        int i14 = 0;
        int i15 = i4 - uN;
        if (i15 >= 0) {
            if (i10 == 2) {
                i14 = ((i4 - uN) + 1) >> 1;
            }
            i8 = i10 == 3 ? i4 - uN : i14;
        } else {
            int i16 = i15 * -1;
            i8 = ((i16 >> 1) == 0 ? 1 : i16 >> 1) * -1;
        }
        int i17 = i8 + i2;
        if (z) {
            b(str, i13, i17, 20);
        } else {
            this.bkB.b(str, i13, i17, 20);
        }
    }

    public void a(String str, int i, int i2, int i3, int i4, int i5, int i6, boolean z, int i7, k kVar) {
        int i8;
        if (str != null) {
            int a2 = kVar.a(str.toCharArray());
            int uN = kVar.uN();
            int i9 = a2 >= i3 ? 1 : i5;
            int i10 = uN >= i4 ? 3 : i6;
            int i11 = 0;
            if (i9 == 2) {
                i11 = (i3 - a2) >> 1;
            }
            int i12 = i9 == 3 ? i3 - a2 : i11;
            if (i12 <= 0) {
                i12 = 0;
            }
            int i13 = i12 + i;
            int i14 = 0;
            int i15 = i4 - uN;
            if (i15 >= 0) {
                if (i10 == 2 && (i14 = (i4 - uN) >> 1) == 0) {
                    i14 = 1;
                }
                i8 = i10 == 3 ? i4 - uN : i14;
            } else {
                int i16 = i15 * -1;
                i8 = ((i16 >> 1) == 0 ? 1 : i16 >> 1) * -1;
            }
            int i17 = i8 + i2;
            if (z) {
                b(str, i13, i17, 20);
            } else {
                this.bkB.b(str, i13, i17, 20);
            }
        }
    }

    public void a(char[] cArr, int i, int i2, int i3, int i4, int i5) {
        if (bc.beZ == 4) {
            this.bkB.b(new String(cArr, i, i2), this.left + i3, this.top + i4, i5);
            return;
        }
        this.bkB.a(cArr, i, i2, i3 + this.left, i4 + this.top, i5);
    }

    public void a(int[] iArr, int i, int i2, int i3, int i4, int i5, int i6, boolean z) {
        this.bkB.b(iArr, i, i2, i3 + this.left, i4 + this.top, i5, i6, z);
    }

    public final void a(int[] iArr, int i, int i2, int i3, int i4, boolean z) {
        a(iArr, i, i2, i3, i4, z, 1);
    }

    public final void a(int[] iArr, int i, int i2, int i3, int i4, boolean z, int i5) {
        int color = this.bkB.getColor();
        if (i5 == 1) {
            int length = i4 / iArr.length;
            int length2 = i4 % iArr.length;
            int i6 = 0;
            int i7 = i2;
            while (i6 < iArr.length) {
                int i8 = length2 - 1;
                int i9 = (length2 > 0 ? 1 : 0) + length;
                setColor(iArr[i6]);
                if (z) {
                    o(i, i7, i3, i9);
                } else {
                    this.bkB.o(i, i7, i3, i9);
                }
                i7 += i9;
                i6++;
                length2 = i8;
            }
        } else if (i5 == -1) {
            int length3 = i3 / iArr.length;
            int length4 = i3 % iArr.length;
            int i10 = 0;
            int i11 = i;
            while (i10 < iArr.length) {
                int i12 = length4 - 1;
                int i13 = (length4 > 0 ? 1 : 0) + length3;
                setColor(iArr[i10]);
                if (z) {
                    o(i11, i2, i13, i4);
                } else {
                    this.bkB.o(i11, i2, i13, i4);
                }
                i11 += i13;
                i10++;
                length4 = i12;
            }
        }
        setColor(color);
    }

    public final void a(int[] iArr, int i, int i2, int i3, int i4, boolean z, int i5, int i6) {
        a(iArr, i, i2, i3, i4, z, 1, i5, i6);
    }

    public final void a(int[] iArr, int i, int i2, int i3, int i4, boolean z, int i5, int i6, int i7) {
        int color = this.bkB.getColor();
        if (i5 == 1) {
            int i8 = i4 / i7;
            int i9 = i4 % i7;
            int i10 = 0;
            int i11 = i2;
            while (i10 < i7) {
                int i12 = i9 - 1;
                int i13 = (i9 > 0 ? 1 : 0) + i8;
                setColor(iArr[i10 + i6]);
                if (z) {
                    o(i, i11, i3, i13);
                } else {
                    this.bkB.o(i, i11, i3, i13);
                }
                i11 += i13;
                i10++;
                i9 = i12;
            }
        } else if (i5 == -1) {
            int i14 = i3 / i7;
            int i15 = i3 % i7;
            int i16 = 0;
            int i17 = i;
            while (i16 < i7) {
                int i18 = i15 - 1;
                int i19 = (i15 > 0 ? 1 : 0) + i14;
                setColor(iArr[i16 + i6]);
                if (z) {
                    o(i17, i2, i19, i4);
                } else {
                    this.bkB.o(i17, i2, i19, i4);
                }
                i17 += i19;
                i16++;
                i15 = i18;
            }
        }
        setColor(color);
    }

    public void b(u uVar) {
        this.bkB = uVar;
        this.top = 0;
        this.left = 0;
        this.bkG = -1;
        int[] iArr = this.bkK;
        int[] iArr2 = this.bkK;
        int[] iArr3 = this.bkK;
        this.bkK[3] = 0;
        iArr3[2] = 0;
        iArr2[1] = 0;
        iArr[0] = 0;
    }

    public void b(String str, int i, int i2, int i3) {
        this.bkB.b(str, this.left + i, this.top + i2, i3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean):void
     arg types: [java.lang.String, int, int, int, int, int, int, int]
     candidates:
      com.uc.c.bk.a(int, int, int, int, int, int, b.a.a.a.f, b.a.a.a.f):void
      com.uc.c.bk.a(int, int, int, int, int[], boolean, int, int):void
      com.uc.c.bk.a(int[], int, int, int, int, int, int, boolean):void
      com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int):void
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean):void */
    public void b(String str, int i, int i2, int i3, int i4, int i5, int i6) {
        a(str, i, i2, i3, i4, i5, i6, true);
    }

    public void bb(int i, int i2) {
        this.left = i;
        this.top = i2;
    }

    public void bc(int i, int i2) {
        bb(this.left + i, this.top + i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(int, int, boolean):void
     arg types: [int, int, int]
     candidates:
      com.uc.c.bk.a(int, int, int[][]):void
      com.uc.c.bk.a(b.a.a.a.u, int, int):void
      com.uc.c.bk.a(int, int, boolean):void */
    public void be(int i, int i2) {
        a(i, i2, true);
    }

    public void bf(int i, int i2) {
        this.bkB.bf(this.left + i, this.top + i2);
    }

    public void c(int i, int i2, int i3, int i4, int i5, int i6) {
        this.bkB.c(this.left + i, this.top + i2, i3, i4, i5, i6);
    }

    public void c(f fVar, int i, int i2, int i3, int i4) {
        this.bkB.c(fVar, i + this.left, i2 + this.top, i3, i4);
    }

    public void d(int i, int i2, int i3, int i4, int i5, int i6) {
        this.bkB.d(this.left + i, this.top + i2, i3, i4, i5, i6);
    }

    public void d(f fVar, int i, int i2) {
        this.bkB.a(fVar, i, i2, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(b.a.a.a.f, int, int, int, int, boolean):void
     arg types: [b.a.a.a.f, int, int, int, int, int]
     candidates:
      com.uc.c.bk.a(int, int, int, int, int, boolean):void
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int):void
      com.uc.c.bk.a(char[], int, int, int, int, int):void
      com.uc.c.bk.a(int[], int, int, int, int, boolean):void
      com.uc.c.bk.a(b.a.a.a.f, int, int, int, int, boolean):void */
    public void d(f fVar, int i, int i2, int i3, int i4) {
        a(fVar, i, i2, i3, i4, false);
    }

    public void d(k kVar) {
        this.bkB.d(kVar);
    }

    public void e(int i, int i2, int i3, int i4, int i5, int i6) {
        this.bkB.save();
        this.bkB.gW(2130706432 | i5);
        o(i, (i4 / 2) + i2, i3, (i4 * 4) / 5);
        this.bkB.restore();
    }

    public void e(f fVar, int i, int i2, int i3, int i4) {
        this.bkB.e(fVar, i + this.left, i2 + this.top, i3 + this.left, i4 + this.top);
    }

    public void f(int i, int i2, int i3, int i4, int i5, int i6) {
        this.bkB.f(this.left + i, this.top + i2, this.left + i3, this.top + i4, this.left + i5, this.top + i6);
    }

    public void f(f fVar, int i, int i2, int i3, int i4) {
        this.bkB.f(fVar, i + this.left, i2 + this.top, i3, i4);
    }

    public void g(int i, int i2, int i3, int i4, int i5, int i6) {
        this.bkB.g(this.left + i, this.top + i2, i3, i4, i5, i6);
    }

    public final int gT(int i) {
        return this.left + i;
    }

    public final int gU(int i) {
        return this.top + i;
    }

    public void gV(int i) {
        this.bkB.gV(i);
    }

    public void gW(int i) {
        this.bkB.gW(i);
    }

    public void gX(int i) {
        if (i == 2) {
            this.bkB.a(k.adO);
        } else if (i == 3) {
            this.bkB.a(k.adP);
        } else {
            this.bkB.a(null);
        }
    }

    public void gY(int i) {
        this.bkB.gY(i);
    }

    public void gZ(int i) {
        d(e.bj(i));
    }

    public int getColor() {
        return this.bkB.getColor();
    }

    public int getTextSize() {
        return (int) this.bkB.adm.getTextSize();
    }

    public void h(int i, int i2, int i3, int i4, int i5, int i6) {
        this.bkB.h(this.left + i, this.top + i2, i3, i4, i5, i6);
    }

    public void ha(int i) {
        this.bkB.ha(i);
    }

    public void hb(int i) {
        this.bkB.hb(i);
    }

    public u jU() {
        return this.bkB;
    }

    public void k(int i, int i2, int i3, int i4) {
        this.bkB.k(this.left + i, this.top + i2, i3, i4);
    }

    public void l(int i, int i2, int i3, int i4) {
        this.bkB.k(this.left + i, this.top + i2, i3, i4);
    }

    public void m(int i, int i2, int i3) {
        this.bkB.m(i, i2, i3);
    }

    public void m(int i, int i2, int i3, int i4) {
        this.bkB.p(this.left + i, this.top + i2, this.left + i3, this.top + i4);
    }

    public void n(int i, int i2, int i3, int i4) {
        this.bkB.n(this.left + i, this.top + i2, i3, i4);
    }

    public void o(int i, int i2, int i3, int i4) {
        this.bkB.o(this.left + i, this.top + i2, i3, i4);
    }

    public void p(int i, int i2, int i3, int i4) {
        this.bkB.p(this.left + i, this.top + i2, this.left + i3, this.top + i4);
    }

    public void q(int i, int i2, int i3, int i4) {
        this.bkB.q(this.left + i, this.top + i2, i3, i4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(int, int, int, int, int, boolean):void
     arg types: [int, int, int, int, int, int]
     candidates:
      com.uc.c.bk.a(b.a.a.a.f, int, int, int, int, boolean):void
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int):void
      com.uc.c.bk.a(char[], int, int, int, int, int):void
      com.uc.c.bk.a(int[], int, int, int, int, boolean):void
      com.uc.c.bk.a(int, int, int, int, int, boolean):void */
    public void r(int i, int i2, int i3, int i4) {
        a(i, i2, i3, i4, 0, true);
    }

    public final void reset() {
        if (this.bkG >= 0) {
            this.bkB.d((k) this.bkD[this.bkG]);
            this.bkB.setColor(this.bkE[this.bkG]);
            Object[] objArr = (Object[]) this.bkF[this.bkG];
            this.bkB.a((PathEffect) objArr[0]);
            this.bkB.gY(((Integer) objArr[1]).intValue());
            this.bkD[this.bkG] = null;
            this.bkE[this.bkG] = 0;
            Object[] objArr2 = this.bkF;
            int i = this.bkG;
            this.bkG = i - 1;
            objArr2[i] = 0;
        }
    }

    public final void save() {
        if (this.bkG >= this.bkD.length - 1) {
            this.bkD = bc.c(this.bkD, this.bkD.length << 1);
            this.bkE = bc.a(this.bkE, this.bkE.length << 1, 0);
            this.bkF = bc.c(this.bkF, this.bkF.length << 1);
        }
        Object[] objArr = this.bkD;
        int i = this.bkG + 1;
        this.bkG = i;
        objArr[i] = this.bkB.DA();
        this.bkE[this.bkG] = this.bkB.getColor();
        this.bkF[this.bkG] = new Object[]{this.bkB.DT(), Integer.valueOf(this.bkB.DU())};
    }

    public void scale(float f, float f2) {
        if (this.bkB != null) {
            this.bkB.scale(f, f2);
        }
    }

    public void setColor(int i) {
        this.bkB.setColor(i);
    }

    public void setTextSize(float f) {
        this.bkB.adm.setTextSize(f);
    }

    public void translate(float f, float f2) {
        if (this.bkB != null) {
            this.bkB.translate(f, f2);
        }
    }
}
