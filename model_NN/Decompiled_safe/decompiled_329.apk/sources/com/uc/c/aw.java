package com.uc.c;

import b.a.a.a.f;

class aw {
    private f aSM;
    private int aSN;
    private int aSO;
    private int aSP;
    private int aSQ;
    private int aSR;
    private int aSS;
    final /* synthetic */ m aST;

    private aw(m mVar) {
        this.aST = mVar;
    }

    public f Av() {
        return this.aSM;
    }

    public int Aw() {
        return this.aSN;
    }

    public int Ax() {
        return this.aSO;
    }

    public void aF(int i, int i2) {
        if (this.aSM != null) {
            this.aSM.recycle();
        }
        this.aSM = f.J(i, i2);
        if (this.aSM != null) {
            this.aSR = this.aSM.getWidth();
            this.aSS = this.aSM.getHeight();
        }
    }

    public boolean aG(int i, int i2) {
        return this.aSM != null && this.aSR == i && this.aSS == i2;
    }

    public void b(f fVar, int i, int i2, int i3, int i4) {
        if (fVar != null) {
            this.aSM = fVar;
            this.aSN = i;
            this.aSO = i2;
            this.aSP = i3;
            this.aSQ = i4;
            this.aSR = fVar.getWidth();
            this.aSS = fVar.getHeight();
        }
    }

    public void e(f fVar) {
        this.aSM = fVar;
    }

    public void fD(int i) {
        this.aSN = i;
    }

    public void fE(int i) {
        this.aSO = i;
    }

    public int getHeight() {
        return this.aSQ;
    }

    public int getWidth() {
        return this.aSP;
    }

    public void recycle() {
        if (this.aSM != null) {
            this.aSM.recycle();
            this.aSM = null;
        }
        this.aSN = 0;
        this.aSO = 0;
        this.aSP = 0;
        this.aSQ = 0;
        this.aSR = 0;
        this.aSS = 0;
    }

    public void setHeight(int i) {
        this.aSQ = i;
    }

    public void setWidth(int i) {
        this.aSP = i;
    }
}
