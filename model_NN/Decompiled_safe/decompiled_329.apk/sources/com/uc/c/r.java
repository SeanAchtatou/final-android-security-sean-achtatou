package com.uc.c;

import android.graphics.Bitmap;
import android.graphics.Picture;
import android.graphics.Rect;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import b.a.a.a.f;
import b.a.a.a.k;
import b.a.a.d;
import com.uc.a.n;
import com.uc.b.a;
import com.uc.b.e;
import com.uc.browser.MenuLayout;
import com.uc.plugin.Plugin;
import java.io.ByteArrayOutputStream;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Timer;
import java.util.Vector;

public class r {
    public static final int CL = -123;
    public static final int CM = -122;
    private static final int CN = 20;
    private static final int CO = 30;
    static final int CQ = 500;
    public static Object CR = new Object();
    static ca CX = null;
    static int CY = -1;
    static int CZ = -1;
    private static long Cq = 0;
    public static final short DD = 255;
    static final int DF = 50;
    public static int DI = 20;
    public static int DJ = 25;
    public static final byte DK = 0;
    public static final byte DL = 1;
    public static final byte DM = 5;
    public static final byte DN = 7;
    public static final byte DO = 8;
    public static final byte DP = 9;
    public static final byte DQ = 10;
    public static final byte DR = 11;
    public static final byte DS = 63;
    public static final byte DT = 62;
    public static final byte DU = 61;
    public static final byte DV = 60;
    public static final byte DW = 59;
    public static final byte DX = 58;
    public static final byte DY = 57;
    public static final byte DZ = 56;
    static int Da = 0;
    public static final int Dg = -1577058305;
    public static final int Dh = -1560281089;
    public static final int Di = 13639824;
    public static final int Dj = -1140850689;
    public static final int[] Dk = {15398647, 14872055, 14149879, 14084087, 16777215, 6334422, 1586291, 2895156, 2500654, 2303275, 2369068, 2763827, 3487552, 5921634};
    public static final int[] Dl = {13486537, 3552822, 16777215, 8553090, 8553090};
    public static int Dw = 50;
    public static final byte Ea = 55;
    public static final byte Eb = 54;
    public static final byte Ec = 53;
    public static final byte Ed = 52;
    public static final byte Ee = 51;
    public static final byte Ef = 50;
    public static final byte Eg = 49;
    public static final byte Eh = 48;
    public static final byte Ei = 47;
    public static final byte Ej = 46;
    public static final byte Ek = 45;
    public static final byte El = 44;
    public static final long Em = -4503599627370496L;
    public static final long En = -2199023255552000L;
    public static final long Eo = -1354598325420032L;
    public static final long Ep = 6341121051895791616L;
    public static final long Eq = 2310346608841064448L;
    public static final long Er = 1170935903116328960L;
    public static final long Es = 2328361007350546432L;
    public static final long Et = 2251799813685248L;
    public static final byte[] Eu = {2};
    public static final long Ev = 5872693914091126784L;
    public static final long Ew = 1;
    bn BK;
    r BL;
    r BM;
    bf BN;
    bf BO;
    bf BP;
    bf BQ;
    bn BR;
    public Vector BS;
    public Hashtable BT;
    int BU;
    public int BV;
    public bx BW;
    public byte BX;
    public byte BY;
    String BZ;
    private Object CA;
    private byte CB;
    private byte CC;
    private Object CD;
    public byte CE;
    private byte CF;
    public ca CG;
    private String CH;
    private bf CI;
    private bf CJ;
    public Object CK;
    long CP;
    TranslateAnimation CS;
    Transformation CT;
    public int CU;
    public int CV;
    public long CW;
    int Ca;
    int Cb;
    public bi Cc;
    Object Cd;
    byte[] Ce;
    f Cf;
    f Cg;
    short Ch;
    byte Ci;
    private boolean Cj;
    private boolean Ck;
    public boolean Cl;
    public boolean Cm;
    public boolean Cn;
    private int Co;
    private int Cp;
    private int Cr;
    private int Cs;
    private byte Ct;
    private int Cu;
    private int Cv;
    public boolean Cw;
    public boolean Cx;
    private byte Cy;
    private byte Cz;
    private Object[] DA;
    private Object[] DB;
    public Vector DC;
    private boolean DE;
    public ByteArrayOutputStream DG;
    /* access modifiers changed from: private */
    public bu DH;
    bj Db;
    ca Dc;
    av Dd;
    f De;
    int Df;
    public boolean Dm;
    int Dn;
    int Do;
    long Dp;
    int Dq;
    int Dr;
    int Ds;
    ca Dt;
    public boolean Du;
    public Timer Dv;
    boolean Dx;
    private boolean Dy;
    private String Dz;
    public String EA;
    public String EB;
    public String EC;
    public String ED;
    public int EE;
    public String Ex;
    public String Ey;
    public String Ez;
    w mR;
    public n p;

    public r() {
        this.mR = null;
        this.BK = null;
        this.BL = null;
        this.BM = null;
        this.BN = null;
        this.BO = null;
        this.BP = null;
        this.BQ = null;
        this.BR = null;
        this.BT = new Hashtable();
        this.BU = 0;
        this.BV = -1;
        this.BW = null;
        this.BX = 0;
        this.BY = 0;
        this.BZ = null;
        this.Ca = 0;
        this.Cb = 1;
        this.Cc = null;
        this.Cd = null;
        this.Ce = null;
        this.Cf = null;
        this.Cg = null;
        this.Ch = 0;
        this.Ci = 0;
        this.Cj = false;
        this.Ck = false;
        this.Cl = false;
        this.Cm = false;
        this.Cn = false;
        this.Co = 0;
        this.Cp = 0;
        this.Cr = 0;
        this.Cs = 0;
        this.Cu = 0;
        this.Cv = 0;
        this.Cw = false;
        this.Cx = false;
        this.Cy = -1;
        this.Cz = -1;
        this.CA = null;
        this.CB = -1;
        this.CC = -1;
        this.CD = null;
        this.CE = 0;
        this.CF = 0;
        this.CG = null;
        this.CH = null;
        this.CI = null;
        this.CJ = null;
        this.CP = 0;
        this.CS = null;
        this.CT = new Transformation();
        this.CU = 500;
        this.CV = 100;
        this.CW = -1;
        this.Db = null;
        this.Dc = null;
        this.Dd = null;
        this.De = null;
        this.Df = -1;
        this.p = null;
        this.Dm = false;
        this.Dn = 0;
        this.Do = 0;
        this.Dp = 0;
        this.Dq = 0;
        this.Dr = 2000;
        this.Ds = 0;
        this.Dt = null;
        this.Du = false;
        this.Dx = false;
        this.Dy = false;
        this.Dz = null;
        this.DA = null;
        this.DB = null;
        this.DC = null;
        this.DE = false;
        this.DG = null;
        this.DH = null;
        this.Ex = null;
        this.Ey = null;
        this.Ez = null;
        this.EA = null;
        this.EB = null;
        this.EC = null;
        this.ED = null;
        this.EE = -1;
    }

    public r(w wVar) {
        this(wVar, (byte) 0);
    }

    public r(w wVar, byte b2) {
        this.mR = null;
        this.BK = null;
        this.BL = null;
        this.BM = null;
        this.BN = null;
        this.BO = null;
        this.BP = null;
        this.BQ = null;
        this.BR = null;
        this.BT = new Hashtable();
        this.BU = 0;
        this.BV = -1;
        this.BW = null;
        this.BX = 0;
        this.BY = 0;
        this.BZ = null;
        this.Ca = 0;
        this.Cb = 1;
        this.Cc = null;
        this.Cd = null;
        this.Ce = null;
        this.Cf = null;
        this.Cg = null;
        this.Ch = 0;
        this.Ci = 0;
        this.Cj = false;
        this.Ck = false;
        this.Cl = false;
        this.Cm = false;
        this.Cn = false;
        this.Co = 0;
        this.Cp = 0;
        this.Cr = 0;
        this.Cs = 0;
        this.Cu = 0;
        this.Cv = 0;
        this.Cw = false;
        this.Cx = false;
        this.Cy = -1;
        this.Cz = -1;
        this.CA = null;
        this.CB = -1;
        this.CC = -1;
        this.CD = null;
        this.CE = 0;
        this.CF = 0;
        this.CG = null;
        this.CH = null;
        this.CI = null;
        this.CJ = null;
        this.CP = 0;
        this.CS = null;
        this.CT = new Transformation();
        this.CU = 500;
        this.CV = 100;
        this.CW = -1;
        this.Db = null;
        this.Dc = null;
        this.Dd = null;
        this.De = null;
        this.Df = -1;
        this.p = null;
        this.Dm = false;
        this.Dn = 0;
        this.Do = 0;
        this.Dp = 0;
        this.Dq = 0;
        this.Dr = 2000;
        this.Ds = 0;
        this.Dt = null;
        this.Du = false;
        this.Dx = false;
        this.Dy = false;
        this.Dz = null;
        this.DA = null;
        this.DB = null;
        this.DC = null;
        this.DE = false;
        this.DG = null;
        this.DH = null;
        this.Ex = null;
        this.Ey = null;
        this.Ez = null;
        this.EA = null;
        this.EB = null;
        this.EC = null;
        this.ED = null;
        this.EE = -1;
        this.mR = wVar;
        this.BY = b2;
        this.BS = new Vector();
        switch (b2) {
            case 0:
                b(wVar.Lt, (bf) null);
                return;
            case 1:
                b(wVar.Lx, wVar.Ly);
                return;
            case 9:
                b(wVar.LG, wVar.Ly);
                return;
            default:
                return;
        }
    }

    private final String[] H(int i, int i2) {
        boolean z;
        switch (i) {
            case -127:
                try {
                    z = az.du(az.baR[3]) < 5120;
                } catch (Exception e) {
                    z = true;
                }
                String[] strArr = new String[(z ? 2 : 1)];
                if (this.mR.JS == null || !((bb) this.mR.JS.Cc).BZ()) {
                    strArr[0] = ar.avM[30];
                } else {
                    strArr[0] = ar.avR[40];
                }
                if (!z) {
                    return strArr;
                }
                strArr[1] = ar.avM[13];
                return strArr;
            case CM /*-122*/:
                return new String[]{ar.avR[39]};
            case -120:
                switch (i2) {
                    case 4:
                        return new String[]{ar.avN[28]};
                    case 12:
                        return new String[]{ar.avN[25]};
                    case 33:
                    case 34:
                        return new String[]{ar.avN[32]};
                    default:
                        return null;
                }
            case -110:
            case 74:
                return null;
            case 49:
                return new String[]{ar.avM[73]};
            case 52:
                return new String[]{ar.avM[94]};
            case 60:
                return new String[]{ar.avM[18]};
            default:
                return new String[]{ar.avM[49]};
        }
    }

    public static final int I(byte[] bArr) {
        try {
            if (bArr[0] == 52) {
                w.ns().f((byte) 2);
            } else if (bArr[0] == 61) {
                w.ns().f((byte) 5);
            }
        } catch (Exception e) {
        }
        return 2;
    }

    private final boolean I(boolean z) {
        return this.BY <= 0 && !jr() && !(z ? jl() : jm()).Lt() && this.CE != 1;
    }

    private void T() {
        jE();
        this.p.sk();
        ca jj = jj();
        this.mR.b(this, jj);
        if (an.vA().vC() && jj != null && jj.ch() != null) {
            jj.ch().EG();
        }
    }

    public static int a(bk bkVar, int i, int i2, int i3, int i4, int i5, int i6, int i7, w wVar, int i8) {
        return a(bkVar, i, i2, i3, i4, i5, i6, i7, wVar, i8, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int, int):void
     arg types: [int[], int, int, int, int, int, int, int, int]
     candidates:
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, b.a.a.a.k, int):int
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean, int):void
      com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int, int):void */
    public static int a(bk bkVar, int i, int i2, int i3, int i4, int i5, int i6, int i7, w wVar, int i8, int i9) {
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        if (i3 <= 0 || i5 < i6) {
            bkVar.setColor(i8);
            bkVar.bkB.o(i, i2, i3, i4);
            return 0;
        }
        int max = Math.max(10, ((i9 == 0 ? i4 : i3) * i6) / i5);
        int i15 = (i7 * ((i9 == 0 ? i4 : i3) - max)) / (i5 - i6 == 0 ? 1 : i5 - i6);
        int i16 = 0;
        if (i9 == 0) {
            i16 = 0 + i3;
            i11 = i;
            int i17 = i15 + i2;
            i10 = 0 + max;
            i12 = i17;
        } else if (i9 == 1) {
            i16 = 0 + max;
            i11 = i15 + i;
            i10 = 0 + i4;
            i12 = i2;
        } else {
            i10 = 0;
            i11 = i;
            i12 = i2;
        }
        bkVar.save();
        int[] iArr = az.bdf;
        bkVar.save();
        bkVar.setColor(i8);
        bkVar.bkB.o(i, i2, i3 + (i9 == 1 ? w.Ke : 0), i4);
        int i18 = az.bdf[60];
        int i19 = az.bdf[61];
        if (!az.BI()) {
            i18 = 14211288;
            i13 = 0;
            iArr = new int[]{14737632, 14211288, 13684944};
            i14 = 12105912;
        } else {
            i13 = 57;
            i14 = i19;
        }
        bkVar.setColor(i18);
        bkVar.bkB.o(i11, i12, i16, i10);
        bkVar.setColor(i14);
        bkVar.bkB.o(i11, i12 + 1, i16, i10 - 2);
        bkVar.bkB.o(i11 + 1, i12, i16 - 2, i10);
        if (i16 > 4) {
            bkVar.a(iArr, i11 + 1, i12 + 1, i16 - 2, i10 - 2, false, i9 == 0 ? -1 : 1, i13, 3);
        } else {
            int[] iArr2 = az.bdf;
            for (int i20 = 1; i20 <= 2; i20++) {
                bkVar.setColor(iArr2[(i20 + 57) - 1]);
                if (i9 == 0) {
                    bkVar.bkB.m(i11 + i20, i12 + 1, i11 + i20, (i12 + i10) - 2);
                } else if (i9 == 1) {
                    bkVar.bkB.m(i11 + 1, i12 + i20, (i11 + i16) - 2, i12 + 1);
                }
            }
        }
        bkVar.reset();
        return max;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(int, int, int, int, int[], boolean, int, int):void
     arg types: [int, int, int, int, int[], int, int, int]
     candidates:
      com.uc.c.bk.a(int, int, int, int, int, int, b.a.a.a.f, b.a.a.a.f):void
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean):void
      com.uc.c.bk.a(int[], int, int, int, int, int, int, boolean):void
      com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int):void
      com.uc.c.bk.a(int, int, int, int, int[], boolean, int, int):void */
    public static final int a(bk bkVar, f fVar, int i, int i2, int i3, int i4, w wVar, int i5) {
        int i6;
        bkVar.save();
        bkVar.setColor(i5);
        bkVar.o(i + 1, i2 + 2, i3 - 2, i4 - 4);
        if (fVar != null) {
            try {
                bkVar.Du();
                bkVar.l(i, i2, i3, i4);
                bkVar.a(fVar, i + 5, ((i4 - fVar.getHeight()) >> 1) + i2, 20);
                int width = fVar.getWidth() + 5 + i;
                try {
                    int width2 = i3 - (fVar.getWidth() - 5);
                    bkVar.Dv();
                    i6 = width;
                } catch (Exception e) {
                    i6 = width;
                }
            } catch (Exception e2) {
                i6 = i;
            }
        } else {
            i6 = i;
        }
        bkVar.a(i, i2, i3, i4, az.bdf, true, 295, 3);
        bkVar.reset();
        return i6 + 5;
    }

    public static final int a(byte[] bArr, ca caVar, Object obj) {
        if (caVar.iJ(5)) {
            int[] iArr = (int[]) ((Object[]) obj)[2];
            iArr[0] = iArr[0] == 1 ? 0 : 1;
            return 3;
        }
        r(caVar);
        return 3;
    }

    private f a(ca caVar, int i, int i2, Object obj, int i3) {
        return null;
    }

    private void a(int i, bf bfVar, bf bfVar2) {
        switch (i) {
            case 0:
                this.CI = bfVar != null ? bfVar : this.mR.LB;
                this.CJ = bfVar2 != null ? bfVar2 : this.mR.LC;
                return;
            case 1:
                if (this.Cy != -1) {
                    this.CI = this.mR.LB;
                    return;
                } else {
                    this.CJ = this.mR.LC;
                    return;
                }
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int):void
     arg types: [int[], int, int, int, int, int, int, int]
     candidates:
      com.uc.c.bk.a(int, int, int, int, int, int, b.a.a.a.f, b.a.a.a.f):void
      com.uc.c.bk.a(int, int, int, int, int[], boolean, int, int):void
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean):void
      com.uc.c.bk.a(int[], int, int, int, int, int, int, boolean):void
      com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int):void */
    private final void a(bk bkVar, ca caVar, int i, int i2, int i3, int i4) {
        if (this.mR.Lc[9] == null || this.mR.Lc[9].getHeight() != i4) {
            int[] iArr = az.bdf;
            int[] iArr2 = az.bdf;
            f gb = az.gb(11);
            f gb2 = az.gb(12);
            int max = Math.max(gb.getWidth(), gb2.getWidth());
            f J = f.J(max, i4);
            bk bkVar2 = new bk(J.jU());
            bkVar2.a(iArr, 0, 0, max, i4, false, 67, 10);
            if (iArr2 != null) {
                for (int i5 = 0; i5 < 2; i5++) {
                    bkVar2.setColor(iArr2[i5 + 77]);
                    bkVar2.m(0, (i4 - iArr2.length) + i5, max, (i4 - 2) + i5);
                }
            }
            if (gb != null) {
                int width = gb.getWidth();
                for (int i6 = 0; i6 < max; i6 += width) {
                    bkVar2.a(gb, i6, 0, 20);
                }
            }
            if (gb2 != null) {
                int height = gb2.getHeight();
                int width2 = gb.getWidth();
                for (int i7 = 0; i7 < max; i7 += width2) {
                    bkVar2.a(gb2, i7, i4 - height, 20);
                }
            }
            this.mR.Lc[9] = J;
        }
        if (this.mR.Lc[9] != null) {
            int width3 = this.mR.Lc[9].getWidth();
            for (int i8 = i; i8 < i3; i8 += width3) {
                bkVar.bkB.a(this.mR.Lc[9], i8, i2, 20);
            }
        }
        int i9 = 10 + 2;
        int i10 = i + 12;
        int i11 = i2 + 2;
        int i12 = i3 - ((10 + 2) << 1);
        int i13 = i4 - 3;
        int i14 = i12 / 5;
        int i15 = i14 + (i12 % 5);
        f gb3 = az.gb(caVar.bHx == 1 ? 14 : 13);
        if (gb3 != null) {
            int width4 = gb3.getWidth();
            int height2 = gb3.getHeight();
            if (caVar.bHu != null) {
                int size = caVar.bHu.size();
                int i16 = 0;
                byte b2 = caVar.bHw;
                int i17 = i10;
                while (i16 < 5) {
                    int i18 = 2 == i16 ? i15 : i14;
                    int min = Math.min(width4, i18);
                    int i19 = ((i18 - min) >> 1) + i17;
                    if (caVar.bHv == b2) {
                        int i20 = caVar.bHx == 1 ? ((i2 + i4) - height2) - 1 : ((i4 - height2) >> 1) + i2;
                        bkVar.Du();
                        if (w.na()) {
                            bkVar.bkB.l(i19, i2, min >> 1, i4);
                            bkVar.bkB.a(gb3, i19, i20, 20);
                            bkVar.bkB.l((min >> 1) + i19, i2, (min + 1) >> 1, i4);
                            bkVar.bkB.a(gb3, (min + i19) - 1, i20, 24);
                        } else {
                            bkVar.bkB.DF();
                            bkVar.bkB.l(i19, i2 + 1, min, i4 - 2);
                            bkVar.bkB.setColor(caVar.bHx == 1 ? az.bdf[65] : az.bdf[64]);
                            bkVar.bkB.o(i19, i2, min, i4);
                            bkVar.bkB.setColor(az.bdf[62]);
                            bkVar.bkB.m(i19, i2, i19, (i2 + i4) - 1);
                            bkVar.bkB.m((i19 + min) - 1, i2, (i19 + min) - 1, (i2 + i4) - 1);
                            bkVar.bkB.setColor(az.bdf[63]);
                            bkVar.bkB.m(i19 + 1, i2, i19 + 1, (i2 + i4) - 1);
                            bkVar.bkB.m((i19 + min) - 2, i2, (min + i19) - 2, (i2 + i4) - 1);
                            bkVar.bkB.DG();
                        }
                        bkVar.Dv();
                    }
                    if (b2 < size) {
                        Object[] objArr = (Object[]) caVar.bHu.elementAt(b2);
                        String str = (String) objArr[0];
                        f fVar = (this.mR.Lb == null || !this.mR.Lb.containsKey(str)) ? (f) objArr[1] : (f) this.mR.Lb.get(str);
                        int height3 = fVar != null ? fVar.getHeight() : 0;
                        int width5 = fVar != null ? fVar.getWidth() : 0;
                        int max2 = (Math.max(i13 - height3, 0) >> 1) + i11;
                        int max3 = (Math.max(i18 - width5, 0) >> 1) + i17;
                        if (fVar != null) {
                            bkVar.bkB.a(fVar, max3, max2, 20);
                        }
                    }
                    int i21 = (2 == i16 ? i15 : i14) + i17;
                    i16++;
                    b2 = ((b2 & 1) != 1 || b2 == 1) ? b2 == 1 ? 0 : b2 + 2 : b2 - 2;
                    i17 = i21;
                }
            }
            bkVar.bkB.setColor(az.bdf[66]);
            int i22 = i + 4;
            int i23 = i2 + (i4 >> 1);
            int i24 = (i + i3) - 4;
            bkVar.bkB.f(i22, i23, i22 + 4, i23 - 4, i22 + 4, i23 + 4);
            bkVar.bkB.f(i24, i23, i24 - 4, i23 - 4, i24 - 4, i23 + 4);
        }
    }

    private final void a(bk bkVar, ca caVar, Vector vector) {
        boolean ac = caVar.ac();
        for (int i = 0; i < vector.size(); i++) {
            int[] iArr = (int[]) vector.elementAt(i);
            if (ac) {
                bkVar.setColor(caVar.Kt());
                bkVar.o(iArr[0], iArr[1], iArr[2], iArr[3]);
            } else {
                bkVar.l(iArr[0], iArr[1], iArr[2], iArr[3]);
                bkVar.setColor(caVar.Ks());
                bkVar.o(iArr[0], iArr[1], iArr[2], iArr[3]);
            }
        }
    }

    public static final void a(ca caVar, int i, int i2, int i3, Object obj) {
        int i4;
        int i5;
        int f;
        byte[] e = caVar.e(2, caVar.bHk);
        e[0] = (byte) i;
        if (i != 60 || (f = f.f(e, 1)) == -1) {
            i4 = i3;
            i5 = i2;
        } else {
            i5 = caVar.bGr;
            i4 = e.bm(f);
        }
        f.b(e, i5, i4);
        Vector vector = (Vector) caVar.bGl.get(Integer.valueOf(caVar.bS()));
        ca.b(e, caVar.bHk[0], (short) vector.size());
        caVar.c(e);
        vector.addElement(obj);
    }

    private final void a(ca caVar, int i, bf bfVar, bf bfVar2) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int):void
     arg types: [int[], int, int, int, int, int, int, int]
     candidates:
      com.uc.c.bk.a(int, int, int, int, int, int, b.a.a.a.f, b.a.a.a.f):void
      com.uc.c.bk.a(int, int, int, int, int[], boolean, int, int):void
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean):void
      com.uc.c.bk.a(int[], int, int, int, int, int, int, boolean):void
      com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int):void */
    public static final boolean a(bk bkVar, byte[] bArr, ca caVar, Object obj) {
        if (bArr == null || bkVar == null) {
            return false;
        }
        bkVar.save();
        try {
            int b2 = f.b(bArr);
            int c = f.c(bArr);
            int d = f.d(bArr);
            int e = f.e(bArr) + 1;
            String str = (String) ((Object[]) obj)[2];
            f fVar = (f) ((Object[]) obj)[0];
            int intValue = ((Integer) ((Object[]) obj)[1]).intValue();
            byte[] c2 = ca.c(fVar, intValue);
            boolean z = (c2 == null || c2[0] != 25) ? false : (f.b(((f) fVar.cE[intValue]).cH, ca.bLC, 1) & 1) == 0;
            int i = az.bdf[251];
            int i2 = az.bdf[252];
            f gb = az.gb(z ? 34 : 36);
            int i3 = caVar.bFQ;
            int f = d - e.f(i3, "  ..");
            int width = gb != null ? f - gb.getWidth() : f;
            bkVar.a(az.bdf, b2, c, d, e, true, 253, 7);
            bkVar.setColor(i);
            bkVar.m(b2, c, (b2 + d) - 1, c);
            bkVar.setColor(i2);
            bkVar.m(b2, (c + e) - 1, (b2 + d) - 1, (c + e) - 1);
            bkVar.setColor(az.bdf[267]);
            if (str != null) {
                bkVar.q(b2 + 4, ((e - 2) >> 1) + c, 2, 2);
                bkVar.b(e.bj(i3).b(str, width, (String) null), b2 + 10, c, width, e, 1, 2);
            }
            if (gb != null) {
                bkVar.a(gb, (b2 + d) - 4, ((e - gb.getHeight()) >> 1) + c, 24);
            }
        } catch (Exception e2) {
        } finally {
            bkVar.reset();
        }
        return true;
    }

    private ca aZ(int i) {
        if (i < 0 || i >= this.BS.size()) {
            return null;
        }
        return (ca) this.BS.elementAt(i);
    }

    public static int b(bk bkVar, int i, int i2, int i3, int i4, int i5, int i6, int i7, w wVar, int i8) {
        return a(bkVar, i, i2, i3, i4, i5, i6, i7, wVar, i8, 1);
    }

    public static final int b(byte[] bArr, ca caVar, Object obj) {
        f fVar = (f) ((Object[]) obj)[0];
        int intValue = ((Integer) ((Object[]) obj)[1]).intValue();
        byte[] c = ca.c(fVar, intValue);
        if (c == null || c[0] != 25) {
            return 0;
        }
        w.ns().a((Picture) null);
        f fVar2 = (f) fVar.cE[intValue];
        byte[] bArr2 = fVar2.cH;
        int d = f.d(bArr2);
        int e = f.e(bArr2);
        if (d == 0 || e == 0) {
            fVar2.d(0, (Object) null);
        }
        int b2 = f.b(bArr2, ca.bLC, 1);
        f.a(bArr2, ca.bLC, 1, 1 - b2, (byte[]) null);
        fVar.b(intValue, (Object) null);
        if (w.ns().lA() == w.ns().JR.get(0)) {
            caVar.bp(w.MK, w.MN - caVar.bGo);
        }
        caVar.cl(false);
        caVar.wx();
        caVar.cn(true);
        caVar.cm(false);
        w.ns().a((Picture) null);
        if (b2 == 1) {
            caVar.bGd.a(caVar.bHl, caVar.bHm, 1);
        } else if (w.ns().lA() == w.ns().JR.get(0)) {
            caVar.bGu = 0;
            caVar.bGv = 0;
        } else {
            caVar.g(0, 0);
        }
        caVar.ap();
        return 2;
    }

    private void b(bk bkVar, int i, int i2, boolean z, int i3, int i4, f fVar, int i5, int i6) {
        if (i5 > i3 || i6 > i4) {
            bkVar.f(fVar, i, i2, i5, i6);
        } else if (z) {
            bkVar.d(fVar, i, i2, i3, i4);
        } else {
            bkVar.a(fVar, i, i2, 20);
        }
    }

    private void b(bk bkVar, ca caVar, int i) {
        int Ks = az.BI() ? az.bdf[247] : caVar.Ks();
        a(bkVar, caVar, i, caVar.bGs, Ks);
        a(bkVar, caVar, (caVar.bGq + i) - w.Ke, Ks);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean):void
     arg types: [java.lang.String, int, int, int, int, int, int, int]
     candidates:
      com.uc.c.bk.a(int, int, int, int, int, int, b.a.a.a.f, b.a.a.a.f):void
      com.uc.c.bk.a(int, int, int, int, int[], boolean, int, int):void
      com.uc.c.bk.a(int[], int, int, int, int, int, int, boolean):void
      com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int):void
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean):void */
    public static final boolean b(bk bkVar, byte[] bArr, ca caVar, Object obj) {
        bkVar.save();
        int b2 = f.b(bArr);
        int c = f.c(bArr) + 2;
        int d = f.d(bArr);
        int e = f.e(bArr);
        Object[] objArr = (Object[]) obj;
        int[] iArr = (int[]) objArr[2];
        String str = (String) objArr[0];
        boolean iJ = caVar.iJ(5);
        byte b3 = iJ ? ca.bKc : 0;
        int i = ((e - (iJ ? ca.bKc : 0)) >> 1) + c;
        if (iJ) {
            bkVar.a(az.gb(iArr[0] == 1 ? 18 : 17), b2, i, 20);
        }
        bkVar.setColor(az.bdf[222]);
        bkVar.a(str, b2 + b3 + 4, c, (d - b3) - 6, e, 1, 2, true);
        bkVar.reset();
        return true;
    }

    private final void ba(int i) {
        if (this == this.mR.lA()) {
            if (i == 1) {
                try {
                    this.Cf = null;
                    this.Cf = bb(0);
                    ak();
                    this.Cg = null;
                    this.Cg = bb(1);
                } catch (OutOfMemoryError e) {
                    this.Cf = null;
                    this.Cg = null;
                    this.mR.mW();
                    return;
                } catch (Exception e2) {
                    this.Cf = null;
                    this.Cg = null;
                    this.mR.mW();
                    return;
                }
            } else if (i == -1) {
                this.Cg = null;
                this.Cg = bb(0);
                ak();
                this.Cf = null;
                this.Cf = bb(-1);
            }
            this.Ch = 0;
            this.Ci = (byte) i;
        }
    }

    public static void bh(int i) {
        DI = i;
        DJ = i;
    }

    private final void c(bk bkVar, ca caVar, int i) {
    }

    private final boolean c(bf bfVar, bf bfVar2) {
        if (this.BK != null && this.BK.biv == 1 && this == this.mR.lA()) {
            this.BK.bnI = bfVar;
            this.BK.bnJ = bfVar2;
            return true;
        } else if (this.BR == null || this.BR.biv != 1 || this != this.mR.lA()) {
            return false;
        } else {
            this.BR.bnI = bfVar;
            this.BR.bnJ = bfVar2;
            return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean, int):void
     arg types: [java.lang.String, int, int, int, int, int, int, int, int]
     candidates:
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, b.a.a.a.k, int):int
      com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int, int):void
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean, int):void */
    public static final boolean c(bk bkVar, byte[] bArr, ca caVar, Object obj) {
        String str = (String) obj;
        bkVar.save();
        int b2 = f.b(bArr);
        int c = f.c(bArr);
        int d = f.d(bArr);
        int e = f.e(bArr);
        int i = caVar.bFQ;
        bkVar.setColor(11604755);
        bkVar.o(b2, c, d - b2, e);
        f ax = a.ax();
        int i2 = b2 + 2;
        bkVar.a(ax, i2, (e - ax.getHeight()) / 2, 20);
        int width = i2 + ax.getWidth() + 1;
        int i3 = c + 2;
        bkVar.gZ(i);
        bkVar.gV(16777215);
        k bj = e.bj(i);
        int uN = bj.uN();
        while (true) {
            int i4 = i3;
            int n = bj.n(str, d - width);
            bkVar.a(str.substring(0, n), width, i4, d - width, uN, 1, 2, true, 1);
            int length = str.length();
            if (n == length) {
                bkVar.setColor(8060928);
                bkVar.m(b2, e, d - 1, e);
                bkVar.reset();
                return false;
            }
            str = str.substring(n, length);
            i3 = i4 + uN;
        }
    }

    public static final int e(byte[] bArr, ca caVar) {
        Object elementAt = ((Vector) caVar.bGl.get(Integer.valueOf(caVar.bS()))).elementAt(f.b(bArr, Eu, 0));
        switch (bArr[0]) {
            case 52:
            case 61:
                return I(bArr);
            case 60:
                return cd.MZ().a(caVar, bArr, elementAt);
            case 62:
                cd.MZ().Ns();
                return b(bArr, caVar, elementAt);
            case 63:
                return a(bArr, caVar, elementAt);
            default:
                return 0;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static final boolean e(bk bkVar, byte[] bArr, ca caVar, int i) {
        Object elementAt = ((Vector) caVar.bGl.get(Integer.valueOf(i))).elementAt(f.b(bArr, Eu, 0));
        switch (bArr[0]) {
            case 52:
            case 61:
                return j(bkVar, bArr, caVar);
            case 55:
            case 56:
                bb.b(bkVar, caVar, bArr, elementAt);
                break;
            case 57:
                return au.a(bkVar, caVar, bArr, elementAt, az.bdf[245]);
            case 58:
                return c(bkVar, bArr, caVar, elementAt);
            case 59:
                return n.a(bkVar, caVar, bArr, elementAt);
            case 60:
                return cd.MZ().e(bkVar, caVar, bArr, elementAt);
            case 63:
                return b(bkVar, bArr, caVar, elementAt);
        }
        return false;
    }

    public static final void g(Vector vector, int i) {
        int i2;
        f fVar;
        try {
            w ns = w.ns();
            r rVar = new r(ns, (byte) 9);
            ca Ku = ca.Ku();
            Ku.t(ar.avM[5]);
            Ku.bHs = (byte) (Ku.bHs | 8);
            Ku.KG();
            Ku.KV();
            Ku.iH(i);
            int i3 = 0;
            f fVar2 = null;
            int i4 = -1;
            while (i3 < vector.size()) {
                Object[] objArr = (Object[]) vector.elementAt(i3);
                a(Ku, 63, Ku.bGm - 6, e.bm(e.jP()) + 8, objArr);
                if (fVar2 == null && i4 == -1 && ((int[]) objArr[2])[0] == 1) {
                    fVar = Ku.bGZ;
                    i2 = Ku.bGZ.cF - 1;
                } else {
                    i2 = i4;
                    fVar = fVar2;
                }
                i3++;
                fVar2 = fVar;
                i4 = i2;
            }
            Ku.Lq();
            Ku.KH();
            Ku.bGd.a(fVar2, i4, 2);
            Ku.iH(1);
            rVar.o(Ku);
            ns.c(rVar);
        } catch (Exception e) {
        }
    }

    public static final boolean j(bk bkVar, byte[] bArr, ca caVar) {
        f fVar;
        String str;
        if (bArr == null || bkVar == null || caVar == null) {
            return false;
        }
        bkVar.save();
        try {
            int b2 = f.b(bArr);
            int c = f.c(bArr);
            int d = f.d(bArr);
            int e = f.e(bArr);
            if (bArr[0] == 52) {
                f gb = az.gb(8);
                str = ar.avM[2];
                fVar = gb;
            } else if (bArr[0] == 61) {
                f li = w.ns().li();
                if (li == null || bc.by(w.ns().Ky)) {
                    li = az.gb(7);
                }
                str = ar.avM[3];
                fVar = li;
            } else {
                fVar = null;
                str = null;
            }
            int a2 = a(bkVar, fVar, b2, c, d, e, w.ns(), az.bdf[294]);
            int n = e.bj(caVar.bFQ).n(str, d - (5 << 1));
            String Z = n != str.length() ? bc.Z(str.substring(0, n), "..") : str;
            bkVar.setColor(az.bdf[293]);
            bkVar.gZ(caVar.bFQ);
            bkVar.b(Z, a2, c, d - (a2 - b2), e, 1, 2);
        } catch (Exception e2) {
        } finally {
            bkVar.reset();
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.a(int, int, int, int, boolean):boolean
     arg types: [int, int, int, int, int]
     candidates:
      com.uc.c.r.a(com.uc.c.ca, int, int, java.lang.Object, int):b.a.a.a.f
      com.uc.c.r.a(com.uc.c.ca, int, int, int, java.lang.Object):void
      com.uc.c.r.a(com.uc.c.bk, int, int, int, int):void
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int, int):void
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, boolean, int[], boolean):void
      com.uc.c.r.a(int, int, int, int, boolean):boolean */
    private void jb() {
        ca caVar;
        String str;
        int i = 0;
        Vector vector = this.BS;
        if (vector != null) {
            int size = vector.size();
            if (size >= 1 && (caVar = (ca) vector.get(0)) != null && (str = caVar.bFA) != null && str.equals("ext:waiting")) {
                i = 1;
            }
            if (size - i >= 30) {
                a(-1, 8, i, (size - 30) + 1, true);
                ch().ho(size - vector.size());
            }
            int size2 = vector.size();
            if (size2 - i >= 20) {
                a(-1, 6, i, (size2 - 20) + 1, true);
            }
        }
    }

    private final boolean jh() {
        return this.mR.lA() == this;
    }

    private boolean jp() {
        r lA = this.mR.lA();
        bn iT = lA.iT();
        if ((iT != null && iT.biv == 1) || lA.CE == 1 || (lA.BR != null && lA.BR.biv == 1)) {
            return false;
        }
        ca jm = this.mR.lA().jm();
        if (jm.LK() || jm.Ls() || this.Cp <= w.MP || this.Cp >= w.MP + w.MN || this.Co <= w.MO || this.Co >= w.MO + w.MM) {
            return false;
        }
        if (lA.Cc == null || lA == null || lA.Cc == null || lA.Cc.eb()) {
            return jm.bHW == null || jm.bHW.eb();
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int):void
     arg types: [int[], int, int, int, int, int, int, int]
     candidates:
      com.uc.c.bk.a(int, int, int, int, int, int, b.a.a.a.f, b.a.a.a.f):void
      com.uc.c.bk.a(int, int, int, int, int[], boolean, int, int):void
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean):void
      com.uc.c.bk.a(int[], int, int, int, int, int, int, boolean):void
      com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int):void */
    public static void k(bk bkVar, byte[] bArr, ca caVar) {
        bkVar.save();
        int b2 = f.b(bArr);
        int c = f.c(bArr);
        int d = f.d(bArr);
        int e = f.e(bArr);
        bkVar.a(az.bdf, b2, c, d, e, true, 269, 10);
        bkVar.setColor(az.bdf[279]);
        bkVar.m(b2, c, (b2 + d) - 1, c);
        bkVar.setColor(az.bdf[291]);
        bkVar.m(b2, (c + e) - 1, (b2 + d) - 1, (c + e) - 1);
        bkVar.reset();
    }

    private void m(ca caVar) {
        if (caVar != null && caVar.bFR != null) {
            caVar.bGY = -1;
        }
    }

    public static final void p(ca caVar) {
        byte[] e = caVar.e(2, caVar.bHk);
        e[0] = 51;
        f fVar = new f();
        fVar.cH = e;
        caVar.c((Object) fVar);
    }

    public static final void q(ca caVar) {
        if (az.bcS <= 1) {
            int fH = cd.fH(caVar.bFA);
            ar.mR.ch().qT();
            if (fH >= 1 && fH <= 3) {
                String jT = cd.jT(fH);
                int f = (((e.f(caVar.bFQ, jT) / (caVar.bGr - 15)) + 1) * e.bm(caVar.bFQ)) + 2;
                if (f < 19) {
                    f = 19;
                }
                a(caVar, 58, caVar.bGr, f, jT);
            }
        }
    }

    public static final void r(ca caVar) {
        String str;
        char[] cArr;
        char[] cArr2;
        w ns = w.ns();
        Vector vector = (Vector) caVar.bGl.get(Integer.valueOf(caVar.bS()));
        ns.lA().close();
        if (!caVar.iJ(6)) {
            boolean iJ = caVar.iJ(5);
            char[] cArr3 = new char[(iJ ? vector.size() : 1)];
            if (iJ) {
                int i = 0;
                for (int i2 = 0; i2 < vector.size(); i2++) {
                    if (((int[]) ((Object[]) vector.elementAt(i2))[2])[0] == 1) {
                        cArr3[i] = (char) i2;
                        i++;
                    }
                }
                if (iJ) {
                    cArr2 = new char[i];
                    System.arraycopy(cArr3, 0, cArr2, 0, i);
                } else {
                    cArr2 = cArr3;
                }
                cArr = cArr2;
                str = null;
            } else {
                int i3 = caVar.bHm;
                cArr3[0] = (char) i3;
                str = (String) ((Object[]) vector.elementAt(i3))[1];
                cArr = cArr3;
            }
            ca jm = ns.lA().jm();
            jm.g(cArr);
            if (bc.dM(str)) {
                ns.f(str, 1);
            } else {
                jm.bGd.i((byte[]) jm.Lk());
            }
        } else if (((Object[]) vector.elementAt(caVar.bHm))[0].equals(ar.avM[118])) {
            w.ns().mw();
        }
    }

    public final void A(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        ca jl = jl();
        ca jm = jm();
        boolean aL = bc.aL(jl.bHs, 1);
        if (jl != null && jm != null) {
            if (i2 <= jm.bGo || i <= (jm.bGp - w.Ke) - 8 || i >= jm.bGp) {
                if (jm.jl(jm.bGm) > jm.bGs && i > jm.bGn) {
                    int i7 = jm.bGo + jm.bGq;
                    w wVar = this.mR;
                    if (i2 > (i7 - w.Ke) - 8 && i2 < jm.bGo + jm.bGq) {
                        a(i, i2, (byte) 1);
                        return;
                    }
                }
                if (!aL || i2 >= w.MP + w.MQ) {
                    this.Cn = true;
                    if (!jm.LK()) {
                        bc.CD();
                        bc.c(i, i2, System.currentTimeMillis());
                    }
                    int ji = jm.ji(i - jm.bGn);
                    int jj = jm.jj(i2 - jm.bGo);
                    if (jm.LK() || !aL) {
                        int i8 = jj;
                        i3 = ji;
                        i4 = i8;
                    } else {
                        int i9 = ji - jl.bGn;
                        int i10 = jj - jl.bGo;
                        i3 = i9;
                        i4 = i10;
                    }
                    if (aL) {
                        i6 = i3 - jl.bGn;
                        i5 = i4 - jl.bGo;
                    } else {
                        i5 = i4;
                        i6 = i3;
                    }
                    if (!jm.LK() && an.vA().vC()) {
                        an.vA().az(i6, i5);
                        if (this != this.mR.JS) {
                            jm.ap();
                        }
                    }
                    if (!an.vA().vC()) {
                        a(i6, i5, 0, jm);
                    }
                    jA();
                    if (this.p != null) {
                        if (this.mR.JS == this && this.Cc != null && (this.Cc instanceof bb)) {
                            ((bb) this.Cc).aH(i, i2);
                        }
                        this.p.Eo();
                        return;
                    }
                    return;
                }
                return;
            }
            if (this.p != null) {
                this.p.Eo();
            }
            if (jm.LK() || !an.vA().vC()) {
                a(i, aL ? i2 - w.MQ : i2, (byte) 0);
            } else {
                an.vA().az(i, i2);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:72:0x0144  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0158  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void B(int r12, int r13) {
        /*
            r11 = this;
            r10 = 0
            r9 = 1
            com.uc.c.ca r0 = r11.jj()
            boolean r1 = r0.LK()
            boolean r2 = r11.Cx
            if (r2 != 0) goto L_0x0028
            int r2 = r11.Co
            int r2 = r12 - r2
            int r3 = r11.Co
            int r3 = r12 - r3
            int r2 = r2 * r3
            int r3 = r11.Cp
            int r3 = r13 - r3
            int r4 = r11.Cp
            int r4 = r13 - r4
            int r3 = r3 * r4
            int r2 = r2 + r3
            int r3 = com.uc.c.r.DI
            int r4 = com.uc.c.r.DI
            int r3 = r3 * r4
            if (r2 <= r3) goto L_0x005f
        L_0x0028:
            r2 = r9
        L_0x0029:
            r11.Cx = r2
            int r2 = r11.Co
            int r2 = r12 - r2
            int r2 = java.lang.Math.abs(r2)
            int r3 = r11.Cp
            int r3 = r13 - r3
            int r3 = java.lang.Math.abs(r3)
            com.uc.c.an r4 = com.uc.c.an.vA()
            boolean r4 = r4.vC()
            if (r4 != 0) goto L_0x0061
            if (r1 != 0) goto L_0x0061
            int r4 = com.uc.c.w.MK
            int r4 = r4 / 4
            if (r2 < r4) goto L_0x0061
            double r4 = (double) r3
            r6 = 4609434218613702656(0x3ff8000000000000, double:1.5)
            double r2 = (double) r3
            double r2 = r2 * r6
            int r2 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r2 < 0) goto L_0x0061
            if (r0 == 0) goto L_0x0061
            int r2 = r0.bGm
            short r3 = r0.bGr
            if (r2 > r3) goto L_0x0061
        L_0x005e:
            return
        L_0x005f:
            r2 = r10
            goto L_0x0029
        L_0x0061:
            boolean r2 = r11.Cl
            if (r2 == 0) goto L_0x00d4
            int r2 = r11.Cs
            int r2 = r13 - r2
            if (r2 == 0) goto L_0x005e
            if (r1 != 0) goto L_0x0073
            int r3 = r0.ahU
            short r4 = r0.bGs
            if (r3 <= r4) goto L_0x005e
        L_0x0073:
            if (r1 == 0) goto L_0x007f
            int r3 = r0.ahU
            short r4 = r0.bGs
            int r4 = r0.jk(r4)
            if (r3 <= r4) goto L_0x005e
        L_0x007f:
            short r3 = r0.bGq
            int r4 = r11.Cu
            int r3 = r3 - r4
            if (r3 == 0) goto L_0x00cb
        L_0x0086:
            int r4 = r0.ahU
            if (r1 == 0) goto L_0x00cd
            short r5 = r0.bGs
            int r5 = r0.jk(r5)
        L_0x0090:
            int r4 = r4 - r5
            int r2 = r2 * r4
            int r2 = r2 / r3
            if (r1 == 0) goto L_0x00d0
            r0.g(r10, r2)
        L_0x0098:
            r11.Cw = r9
        L_0x009a:
            int r1 = r0.bGu
            r0.bGw = r1
            int r1 = r0.bGv
            r0.bGx = r1
            r11.jA()
            long r1 = java.lang.System.currentTimeMillis()
            int r3 = r11.Cs
            int r3 = r3 - r13
            int r3 = java.lang.Math.abs(r3)
            long r4 = r11.CW
            long r4 = r1 - r4
            int r6 = r11.CU
            long r6 = (long) r6
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 > 0) goto L_0x00bf
            int r4 = r11.CV
            if (r3 <= r4) goto L_0x00c6
        L_0x00bf:
            r11.CW = r1
            com.uc.c.w r1 = r11.mR
            r1.b(r11, r0)
        L_0x00c6:
            r11.Cr = r12
            r11.Cs = r13
            goto L_0x005e
        L_0x00cb:
            r3 = r9
            goto L_0x0086
        L_0x00cd:
            short r5 = r0.bGs
            goto L_0x0090
        L_0x00d0:
            r0.g(r10, r2)
            goto L_0x0098
        L_0x00d4:
            boolean r2 = r11.Cm
            if (r2 == 0) goto L_0x0100
            int r2 = r11.Cr
            int r2 = r12 - r2
            if (r2 == 0) goto L_0x005e
            int r3 = r0.bGm
            short r4 = r0.bGr
            if (r3 <= r4) goto L_0x005e
            short r3 = r0.bGp
            int r4 = r11.Cv
            int r3 = r3 - r4
            if (r3 == 0) goto L_0x00fa
        L_0x00eb:
            int r4 = r0.bGm
            short r5 = r0.bGr
            int r4 = r4 - r5
            int r2 = r2 * r4
            int r2 = r2 / r3
            if (r1 == 0) goto L_0x00fc
            r0.g(r2, r10)
        L_0x00f7:
            r11.Cw = r9
            goto L_0x009a
        L_0x00fa:
            r3 = r9
            goto L_0x00eb
        L_0x00fc:
            r0.g(r2, r10)
            goto L_0x00f7
        L_0x0100:
            boolean r2 = r11.Cn
            if (r2 == 0) goto L_0x009a
            int r2 = r11.Cr
            int r2 = r2 - r12
            int r3 = r11.Cs
            int r3 = r3 - r13
            if (r1 != 0) goto L_0x015c
            com.uc.c.an r4 = com.uc.c.an.vA()
            boolean r4 = r4.vC()
            if (r4 == 0) goto L_0x015c
            com.uc.c.an.ahW = r12
            com.uc.c.an.ahX = r13
            int r4 = r0.bGw
            int r5 = r0.bGx
            int r4 = r4 + r12
            int r5 = r5 + r13
            int r6 = r0.bGw
            int r7 = r0.bGx
            int r8 = r11.Cr
            int r6 = r6 + r8
            int r8 = r11.Cs
            int r7 = r7 + r8
            com.uc.c.an r8 = com.uc.c.an.vA()
            boolean r4 = r8.h(r4, r5, r6, r7)
            if (r4 == 0) goto L_0x015c
            r4 = r9
        L_0x0135:
            if (r4 != 0) goto L_0x009a
            byte r4 = r11.Ct
            if (r4 == r9) goto L_0x009a
            long r4 = java.lang.System.currentTimeMillis()
            com.uc.c.bc.c(r12, r13, r4)
            if (r1 == 0) goto L_0x0158
            r0.g(r2, r3)
        L_0x0147:
            com.uc.c.w r1 = r11.mR
            com.uc.c.r r1 = r1.JS
            if (r11 == r1) goto L_0x009a
            boolean r1 = r0.Mb()
            if (r1 == 0) goto L_0x009a
            r0.ap()
            goto L_0x009a
        L_0x0158:
            r0.g(r2, r3)
            goto L_0x0147
        L_0x015c:
            r4 = r10
            goto L_0x0135
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.r.B(int, int):void");
    }

    public final void C(int i, int i2) {
        int i3;
        if (i2 >= w.MP && i2 <= w.MP + w.MN && i <= w.MK) {
            ca jm = jm();
            ca jl = jl();
            boolean aL = bc.aL(jl.bHs, 1);
            if (jl != null && jm != null) {
                if (aL && i2 < w.MP + w.MQ) {
                    c(i, i2, jl);
                } else if (i <= w.MK - w.Ke) {
                    jl.bHx = 0;
                    int ji = jm.ji(i - jm.bGn);
                    int jj = jm.jj(i2 - jm.bGo);
                    if (jm.LK() || !aL) {
                        i3 = jj;
                    } else {
                        int i4 = ji - jl.bGn;
                        ji = i4;
                        i3 = jj - jl.bGo;
                    }
                    int i5 = ji - jl.bGn;
                    int i6 = i3 - jl.bGo;
                    if (!an.vA().vC()) {
                        a(i5, i6, 2, jm);
                        jm.bGK = i5;
                        jm.bGL = i6;
                    }
                    jA();
                }
            }
        }
    }

    public final void D(int i, int i2) {
        Cq = System.currentTimeMillis();
        this.Cx = false;
        this.Cl = false;
        this.Cm = false;
        this.Cn = false;
        this.Cj = false;
        this.Ck = false;
        this.Ct = 0;
        this.Co = i;
        this.Cr = i;
        this.Cp = i2;
        this.Cs = i2;
        ca jj = jj();
        if (!jj.LK()) {
            jj.bOA = jj.bGw + i;
            jj.bOB = jj.bGx + i2;
        }
        if (i2 > w.MP && i2 < w.MP + w.MN) {
            A(i, i2);
            w.Lj = false;
        } else if (i2 >= w.MS[1] || i2 > w.MS[0]) {
        }
        this.CW = System.currentTimeMillis();
    }

    public final void E(int i, int i2) {
        boolean z;
        int aI;
        ca jj = jj();
        if (this.Cx && jj != null) {
            if (jj.Mb()) {
                this.mR.b((f) null);
                this.mR.a((Picture) null);
            }
            jj.ap();
            if (this.p != null) {
                this.p.qW();
            }
        }
        w.Lm = false;
        w.Ln = true;
        w.Lp = Long.MAX_VALUE;
        if (i2 >= w.MT[1] || i2 <= w.MT[0] || this.mR.lA().iX()) {
            this.mR.nl();
        }
        this.Cn = false;
        this.Cm = false;
        if (!this.Cj && !this.Ck && this.Ct != 1) {
            if (this.mR.JS == this && this.p != null && jj() != null && jj().bHm >= 0 && this.Cc != null && (this.Cc instanceof bb) && -2 != (aI = ((bb) this.Cc).aI(i, i2)) && System.currentTimeMillis() - this.CP > 500) {
                this.CP = System.currentTimeMillis();
                this.p.gR(aI);
            }
            int i3 = i - this.Co;
            int abs = Math.abs(i2 - this.Cp);
            byte b2 = (byte) (i3 > 0 ? 55 : 57);
            int abs2 = Math.abs(i3);
            if (abs2 < w.MK / 4 || ((double) abs2) < ((double) abs) * 1.5d || this == this.mR.JS || an.vA().vC()) {
                if (!bc.i(i, i2, this.Co, this.Cp) || this.Cx) {
                    jq();
                } else if (!bc.i(i, i2, this.Co, this.Cp)) {
                    bc.c(i, i2, System.currentTimeMillis());
                    bc.gw(1000);
                    int i4 = bc.bgf > 0 ? -1 : 1;
                    int i5 = bc.bgg > 0 ? -1 : 1;
                    int abs3 = Math.abs(bc.bgf);
                    int abs4 = Math.abs(bc.bgg);
                    if (abs3 > 120) {
                        abs3 = 120;
                    }
                    if (abs4 > 150) {
                        abs4 = 150;
                    }
                    if (abs3 > 80) {
                        abs3 = ((abs3 - 80) >> 2) + 60;
                    } else if (abs3 > 40) {
                        abs3 = ((abs3 - 40) >> 1) + 40;
                    }
                    if (abs4 > 100) {
                        abs4 = ((abs4 - 100) >> 2) + 75;
                    } else if (abs4 > 50) {
                        abs4 = ((abs4 - 50) >> 1) + 50;
                    }
                    int i6 = (abs3 * abs3) >> 1;
                    int i7 = (abs4 * abs4) >> 1;
                    ca jm = jm();
                    if (i6 != 0 || i7 != 0) {
                        jm.f(i4 * i6, i5 * i7);
                        jC();
                    }
                } else {
                    if (i2 < w.MP) {
                        if (i2 < w.MT[1] && i2 > w.MT[0] && !this.mR.lA().iX()) {
                            F(i, i2);
                        }
                    } else if (i2 <= w.MP || i2 >= w.MP + w.MN) {
                        if (i2 > w.MW[0] && i2 < w.MW[1]) {
                            G(i, i2);
                        }
                    } else if (i <= w.MK) {
                        C(i, i2);
                    }
                    this.Cx = false;
                    w.Lj = false;
                    jq();
                }
            } else if (jj().bGm <= jj().bGr) {
                if (this.p != null) {
                    if (55 == b2) {
                        z = this.p.dY(-1);
                    } else if (57 == b2) {
                        z = this.p.dY(1);
                    }
                    if (55 != b2 && 57 != b2 && true == z) {
                        bd(b2);
                        return;
                    }
                    return;
                }
                z = true;
                if (55 != b2) {
                }
            }
        }
    }

    public final void F(int i, int i2) {
        this.mR.F(i, i2);
    }

    public final void G(int i, int i2) {
        if (i2 < w.MW[0] && i2 > w.MW[1]) {
            return;
        }
        if (i > (w.MK >> 1)) {
            String be = be(-1);
            if (be != null) {
                if (i > (w.MK - e.f(e.jP(), be)) - 2) {
                    bd(-7);
                    return;
                }
                return;
            }
            return;
        }
        String be2 = be(1);
        if (be2 != null && i < e.f(e.jP(), be2) + 2) {
            bd(-6);
        }
    }

    public final void J(boolean z) {
        if (jr() || z) {
            bf(8);
        }
    }

    public void K(boolean z) {
        this.Dx = z;
    }

    public void L(boolean z) {
        if (ch() != null) {
            ch().L(z);
        }
    }

    public int a(Vector vector, ca caVar, f fVar, int i) {
        if (fVar == null || i < 0 || i >= fVar.cF) {
            return 0;
        }
        Vector b2 = caVar.b(fVar, i, 1);
        if (b2 == null || b2.size() <= 0) {
            return 0;
        }
        int size = b2.size();
        for (int i2 = 0; i2 < size; i2++) {
            int[] iArr = new int[4];
            byte[] bArr = (byte[]) b2.elementAt(i2);
            caVar.a(fVar, bArr, iArr);
            iArr[2] = f.d(bArr);
            iArr[3] = f.e(bArr);
            vector.addElement(iArr);
        }
        return size;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, java.util.Vector, boolean):void
     arg types: [com.uc.c.bk, com.uc.c.ca, java.util.Vector, int]
     candidates:
      com.uc.c.r.a(com.uc.c.ca, int, com.uc.c.bf, com.uc.c.bf):void
      com.uc.c.r.a(com.uc.c.bk, byte[], com.uc.c.ca, java.lang.Object):boolean
      com.uc.c.r.a(java.util.Vector, com.uc.c.ca, com.uc.c.f, int):int
      com.uc.c.r.a(int, int, int, com.uc.c.ca):void
      com.uc.c.r.a(int, int, com.uc.c.ca, int[]):void
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int):void
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int[], boolean):void
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, java.util.Vector, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int, int, int, int[], boolean, boolean, boolean):void
     arg types: [com.uc.c.bk, com.uc.c.ca, int, int, int, int, ?[OBJECT, ARRAY], int, int, int]
     candidates:
      com.uc.c.r.a(com.uc.c.bk, int, int, int, int, int, int, int, com.uc.c.w, int):int
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int, int, int, int[], boolean, boolean, boolean):void */
    public final synchronized f a(ca caVar, float f, boolean z) {
        f fVar = null;
        synchronized (this) {
            if (caVar != null) {
                f J = f.J((int) (((float) w.MK) * f), (int) (((float) w.MN) * f));
                bk bkVar = new bk(J.jU());
                short s = caVar.bGn;
                int i = caVar.Kr() ? caVar.bGo + 0 : 0;
                if (!caVar.LK() || this.mR.JR.elementAt(this.mR.JU) != this) {
                    bkVar.bb(s - caVar.bGw, i - caVar.bGx);
                    a(bkVar, caVar, caVar.bGw, caVar.bGx, w.MK, w.ML, (int[]) null, true, true, false);
                } else {
                    bkVar.bkB.save();
                    bkVar.scale(caVar.bOm * f, caVar.bOm * f);
                    bkVar.translate((float) (s - caVar.bGw), (float) (i - caVar.bGx));
                    Vector vector = new Vector();
                    vector.addElement(new int[]{caVar.bGw, caVar.bGx, (int) ((((float) w.MK) / caVar.bOm) / f), (int) ((((float) w.ML) / caVar.bOm) / f)});
                    a(bkVar, caVar, vector, false);
                    bkVar.bkB.restore();
                    if (z) {
                        ak();
                    }
                }
                fVar = J;
            }
        }
        return fVar;
    }

    public final void a(int i, int i2, byte b2) {
        int i3;
        if (w.Ke > 0) {
            ca jm = jm();
            short s = 10;
            if (b2 == 0) {
                boolean LK = jm.LK();
                int i4 = jm.ahU;
                if (i4 == 0) {
                    return;
                }
                if (!LK && i4 <= jm.bGs) {
                    return;
                }
                if (!LK || i4 > jm.jk(jm.bGs)) {
                    short s2 = jm.bGo;
                    short s3 = jm.bGq;
                    int i5 = (s3 * s3) / i4;
                    if (i5 < 10) {
                        i5 = 10;
                    }
                    int i6 = i4 - s3;
                    if (i6 != 0) {
                        int jm2 = ((s3 - i5) * (LK ? jm.jm(jm.bGx) : jm.bGx)) / i6;
                        if (i2 <= s2 + jm2) {
                            if (!LK) {
                                jm.e(0, -1);
                                if (this.mR != null) {
                                    this.mR.b(this, jm);
                                }
                                jC();
                            }
                        } else if (i2 >= s2 + jm2 + i5) {
                            if (!LK) {
                                jm.e(0, 1);
                                if (this.mR != null) {
                                    this.mR.b(this, jm);
                                }
                                jC();
                            }
                        } else if (i2 >= s2 + jm2 && i2 < s2 + jm2 + i5) {
                            this.Cl = true;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            }
            if (b2 == 1 && (i3 = jm.bGm) != 0 && i3 > jm.bGr) {
                short s4 = jm.bGn;
                short s5 = jm.bGp;
                int i7 = (s5 * s5) / i3;
                if (i7 >= 10) {
                    s = i7;
                }
                int i8 = i3 - s5;
                if (i8 != 0) {
                    int i9 = ((s5 - s) * jm.bGw) / i8;
                    if (i <= s4 + i9) {
                        jm.e(-1, 0);
                        jC();
                    } else if (i >= s + i9 + s4) {
                        jm.e(1, 0);
                        jC();
                    } else {
                        this.Cm = true;
                    }
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:66:0x01d5  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01de  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r21, int r22, int r23, com.uc.c.ca r24) {
        /*
            r20 = this;
            r4 = 1
            com.uc.c.f[] r10 = new com.uc.c.f[r4]
            r4 = 0
            r0 = r24
            com.uc.c.f r0 = r0.bFR
            r5 = r0
            r10[r4] = r5
            r4 = 1
            int[] r11 = new int[r4]
            r4 = 0
            r5 = -1
            r11[r4] = r5
            r0 = r24
            com.uc.c.i r0 = r0.bGd
            r4 = r0
            r5 = 0
            r5 = r10[r5]
            r6 = 0
            r7 = -4503599341739280(0xfff00000110662f0, double:NaN)
            r9 = 4
            int[] r9 = new int[r9]
            r12 = 0
            r9[r12] = r21
            r12 = 1
            r9[r12] = r22
            r12 = 2
            r13 = 1
            r9[r12] = r13
            r12 = 3
            r13 = 1
            r9[r12] = r13
            r12 = 1
            byte[] r4 = r4.a(r5, r6, r7, r9, r10, r11, r12)
            if (r4 != 0) goto L_0x029e
            r0 = r20
            boolean r0 = r0.Cx
            r5 = r0
            if (r5 != 0) goto L_0x029e
            r4 = 1
            com.uc.c.f[] r10 = new com.uc.c.f[r4]
            r4 = 0
            r0 = r24
            com.uc.c.f r0 = r0.bFR
            r5 = r0
            r10[r4] = r5
            r4 = 1
            int[] r11 = new int[r4]
            r4 = 0
            r5 = -1
            r11[r4] = r5
            r4 = 0
            java.util.Vector r13 = new java.util.Vector     // Catch:{ Exception -> 0x0237, Error -> 0x023c }
            r5 = 1
            r13.<init>(r5)     // Catch:{ Exception -> 0x0237, Error -> 0x023c }
            r14 = r4
        L_0x0059:
            r0 = r24
            com.uc.c.i r0 = r0.bGd     // Catch:{ Exception -> 0x0275, Error -> 0x0264 }
            r4 = r0
            r5 = 0
            r5 = r10[r5]     // Catch:{ Exception -> 0x0275, Error -> 0x0264 }
            r6 = 0
            r7 = -4503599342001424(0xfff00000110262f0, double:NaN)
            r9 = 4
            int[] r9 = new int[r9]     // Catch:{ Exception -> 0x0275, Error -> 0x0264 }
            r12 = 0
            int r15 = com.uc.c.r.DJ     // Catch:{ Exception -> 0x0275, Error -> 0x0264 }
            int r15 = r21 - r15
            r9[r12] = r15     // Catch:{ Exception -> 0x0275, Error -> 0x0264 }
            r12 = 1
            int r15 = com.uc.c.r.DJ     // Catch:{ Exception -> 0x0275, Error -> 0x0264 }
            int r15 = r22 - r15
            r9[r12] = r15     // Catch:{ Exception -> 0x0275, Error -> 0x0264 }
            r12 = 2
            int r15 = com.uc.c.r.DJ     // Catch:{ Exception -> 0x0275, Error -> 0x0264 }
            int r15 = r15 * 2
            r9[r12] = r15     // Catch:{ Exception -> 0x0275, Error -> 0x0264 }
            r12 = 3
            int r15 = com.uc.c.r.DJ     // Catch:{ Exception -> 0x0275, Error -> 0x0264 }
            int r15 = r15 * 2
            r9[r12] = r15     // Catch:{ Exception -> 0x0275, Error -> 0x0264 }
            r12 = 1
            byte[] r4 = r4.a(r5, r6, r7, r9, r10, r11, r12)     // Catch:{ Exception -> 0x0275, Error -> 0x0264 }
            if (r4 == 0) goto L_0x00a6
            r5 = 3
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0237, Error -> 0x023c }
            r6 = 0
            r5[r6] = r4     // Catch:{ Exception -> 0x0237, Error -> 0x023c }
            r6 = 1
            r7 = 0
            r7 = r10[r7]     // Catch:{ Exception -> 0x0237, Error -> 0x023c }
            r5[r6] = r7     // Catch:{ Exception -> 0x0237, Error -> 0x023c }
            r6 = 2
            r7 = 0
            r7 = r11[r7]     // Catch:{ Exception -> 0x0237, Error -> 0x023c }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x0237, Error -> 0x023c }
            r5[r6] = r7     // Catch:{ Exception -> 0x0237, Error -> 0x023c }
            r13.add(r5)     // Catch:{ Exception -> 0x0237, Error -> 0x023c }
        L_0x00a6:
            if (r4 != 0) goto L_0x029b
            r6 = 0
            r4 = 32767(0x7fff, float:4.5916E-41)
            r5 = 32767(0x7fff, float:4.5916E-41)
            r7 = 32767(0x7fff, float:4.5916E-41)
            r8 = 0
            r9 = 0
            r12 = r5
            r14 = r4
            r19 = r9
            r9 = r7
            r7 = r19
        L_0x00b8:
            int r4 = r13.size()     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            if (r7 >= r4) goto L_0x0198
            java.lang.Object r4 = r13.get(r7)     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            java.lang.Object[] r4 = (java.lang.Object[]) r4     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            java.lang.Object[] r4 = (java.lang.Object[]) r4     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            r5 = 1
            r5 = r4[r5]     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            com.uc.c.f r5 = (com.uc.c.f) r5     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            r15 = 2
            r4 = r4[r15]     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            int r4 = java.lang.Integer.parseInt(r4)     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            r15 = 0
            r16 = 1
            r0 = r24
            r1 = r5
            r2 = r4
            r3 = r16
            java.util.Vector r4 = r0.b(r1, r2, r3)     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            if (r4 == 0) goto L_0x0298
            int r16 = r4.size()     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            if (r16 <= 0) goto L_0x0298
            r15 = 0
            java.lang.Object r4 = r4.get(r15)     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            byte[] r4 = (byte[]) r4     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            byte[] r4 = (byte[]) r4     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
        L_0x00f4:
            if (r4 == 0) goto L_0x028b
            r15 = 4
            int[] r15 = new int[r15]     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            r0 = r24
            r1 = r5
            r2 = r4
            r3 = r15
            r0.a(r1, r2, r3)     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            r5 = 0
            r5 = r15[r5]     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            r16 = 1
            r15 = r15[r16]     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            int r16 = com.uc.c.f.d(r4)     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            int r4 = com.uc.c.f.e(r4)     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            int r17 = r15 - r22
            int r17 = java.lang.Math.abs(r17)     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            int r18 = r15 + r4
            int r18 = r18 - r22
            int r18 = java.lang.Math.abs(r18)     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            r0 = r17
            r1 = r18
            if (r0 > r1) goto L_0x016d
            int r17 = r15 - r22
            int r17 = java.lang.Math.abs(r17)     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
        L_0x012a:
            r0 = r22
            r1 = r15
            if (r0 < r1) goto L_0x0294
            int r4 = r4 + r15
            r0 = r22
            r1 = r4
            if (r0 > r1) goto L_0x0294
            r4 = 0
        L_0x0136:
            int r15 = r5 - r21
            int r15 = java.lang.Math.abs(r15)     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            int r17 = r5 + r16
            int r17 = r17 - r21
            int r17 = java.lang.Math.abs(r17)     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            r0 = r15
            r1 = r17
            if (r0 > r1) goto L_0x0176
            int r15 = r5 - r21
            int r15 = java.lang.Math.abs(r15)     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
        L_0x014f:
            r0 = r21
            r1 = r5
            if (r0 < r1) goto L_0x0291
            int r5 = r5 + r16
            r0 = r21
            r1 = r5
            if (r0 > r1) goto L_0x0291
            r5 = 0
        L_0x015c:
            if (r4 >= r12) goto L_0x017f
            r8 = r4
            r4 = r7
            r19 = r9
            r9 = r5
            r5 = r19
        L_0x0165:
            int r7 = r7 + 1
            r12 = r8
            r14 = r9
            r8 = r4
            r9 = r5
            goto L_0x00b8
        L_0x016d:
            int r17 = r15 + r4
            int r17 = r17 - r22
            int r17 = java.lang.Math.abs(r17)     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            goto L_0x012a
        L_0x0176:
            int r15 = r5 + r16
            int r15 = r15 - r21
            int r15 = java.lang.Math.abs(r15)     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            goto L_0x014f
        L_0x017f:
            if (r4 != r12) goto L_0x028b
            if (r5 >= r14) goto L_0x018b
            r8 = r4
            r4 = r7
            r19 = r9
            r9 = r5
            r5 = r19
            goto L_0x0165
        L_0x018b:
            if (r5 != r14) goto L_0x028b
            r0 = r16
            r1 = r9
            if (r0 >= r1) goto L_0x028b
            r8 = r4
            r9 = r5
            r4 = r7
            r5 = r16
            goto L_0x0165
        L_0x0198:
            if (r8 < 0) goto L_0x0286
            int r4 = r13.size()     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            if (r8 >= r4) goto L_0x0286
            java.lang.Object r21 = r13.get(r8)     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            java.lang.Object[] r21 = (java.lang.Object[]) r21     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            java.lang.Object[] r21 = (java.lang.Object[]) r21     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            r4 = 0
            r22 = r21[r4]     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            byte[] r22 = (byte[]) r22     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            byte[] r22 = (byte[]) r22     // Catch:{ Exception -> 0x0279, Error -> 0x0268 }
            r4 = 1
            com.uc.c.f[] r5 = new com.uc.c.f[r4]     // Catch:{ Exception -> 0x027d, Error -> 0x026c }
            r6 = 0
            r4 = 1
            r4 = r21[r4]     // Catch:{ Exception -> 0x027d, Error -> 0x026c }
            com.uc.c.f r4 = (com.uc.c.f) r4     // Catch:{ Exception -> 0x027d, Error -> 0x026c }
            r5[r6] = r4     // Catch:{ Exception -> 0x027d, Error -> 0x026c }
            r4 = 1
            int[] r4 = new int[r4]     // Catch:{ Exception -> 0x0282, Error -> 0x0271 }
            r6 = 0
            r7 = 2
            r7 = r21[r7]     // Catch:{ Exception -> 0x0282, Error -> 0x0271 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x0282, Error -> 0x0271 }
            int r7 = java.lang.Integer.parseInt(r7)     // Catch:{ Exception -> 0x0282, Error -> 0x0271 }
            r4[r6] = r7     // Catch:{ Exception -> 0x0282, Error -> 0x0271 }
            r6 = r5
            r5 = r4
            r4 = r22
        L_0x01cf:
            r7 = 2
            r0 = r23
            r1 = r7
            if (r0 != r1) goto L_0x01dc
            r7 = 1
            r0 = r24
            r1 = r7
            r0.ct(r1)
        L_0x01dc:
            if (r4 == 0) goto L_0x0241
            boolean r7 = com.uc.c.ca.aC(r4)
            if (r7 == 0) goto L_0x0241
            r0 = r24
            com.uc.c.f r0 = r0.bHl
            r7 = r0
            r8 = 0
            r8 = r6[r8]
            if (r7 != r8) goto L_0x01f8
            r0 = r24
            int r0 = r0.bHm
            r7 = r0
            r8 = 0
            r8 = r5[r8]
            if (r7 == r8) goto L_0x0208
        L_0x01f8:
            r7 = 0
            r6 = r6[r7]
            r0 = r6
            r1 = r24
            r1.bHl = r0
            r6 = 0
            r5 = r5[r6]
            r0 = r5
            r1 = r24
            r1.bHm = r0
        L_0x0208:
            r5 = 0
            byte r5 = r4[r5]
            r6 = 28
            if (r5 != r6) goto L_0x0228
            byte[] r5 = com.uc.c.ca.bLF
            r6 = 1
            int r4 = com.uc.c.f.b(r4, r5, r6)
            r0 = r24
            r1 = r4
            com.uc.plugin.Plugin r4 = r0.jn(r1)
            if (r4 == 0) goto L_0x0228
            com.uc.plugin.ag r4 = r4.Qh()
            if (r4 == 0) goto L_0x0228
            r4.requestFocusFromTouch()
        L_0x0228:
            r4 = 2
            r0 = r23
            r1 = r4
            if (r0 != r1) goto L_0x0236
            r4 = 8
            r0 = r20
            r1 = r4
            r0.bd(r1)
        L_0x0236:
            return
        L_0x0237:
            r5 = move-exception
            r5 = r10
        L_0x0239:
            r6 = r5
            r5 = r11
            goto L_0x01cf
        L_0x023c:
            r5 = move-exception
            r5 = r10
        L_0x023e:
            r6 = r5
            r5 = r11
            goto L_0x01cf
        L_0x0241:
            boolean r4 = r24.Mb()
            if (r4 == 0) goto L_0x0260
            r0 = r20
            com.uc.c.w r0 = r0.mR
            r4 = r0
            if (r4 == 0) goto L_0x0260
            r0 = r20
            com.uc.c.w r0 = r0.mR
            r4 = r0
            r5 = 0
            r4.b(r5)
            r0 = r20
            com.uc.c.w r0 = r0.mR
            r4 = r0
            r5 = 0
            r4.a(r5)
        L_0x0260:
            r24.ap()
            goto L_0x0236
        L_0x0264:
            r4 = move-exception
            r4 = r14
            r5 = r10
            goto L_0x023e
        L_0x0268:
            r4 = move-exception
            r4 = r6
            r5 = r10
            goto L_0x023e
        L_0x026c:
            r4 = move-exception
            r4 = r22
            r5 = r10
            goto L_0x023e
        L_0x0271:
            r4 = move-exception
            r4 = r22
            goto L_0x023e
        L_0x0275:
            r4 = move-exception
            r4 = r14
            r5 = r10
            goto L_0x0239
        L_0x0279:
            r4 = move-exception
            r4 = r6
            r5 = r10
            goto L_0x0239
        L_0x027d:
            r4 = move-exception
            r4 = r22
            r5 = r10
            goto L_0x0239
        L_0x0282:
            r4 = move-exception
            r4 = r22
            goto L_0x0239
        L_0x0286:
            r4 = r6
            r5 = r11
            r6 = r10
            goto L_0x01cf
        L_0x028b:
            r4 = r8
            r5 = r9
            r8 = r12
            r9 = r14
            goto L_0x0165
        L_0x0291:
            r5 = r15
            goto L_0x015c
        L_0x0294:
            r4 = r17
            goto L_0x0136
        L_0x0298:
            r4 = r15
            goto L_0x00f4
        L_0x029b:
            r14 = r4
            goto L_0x0059
        L_0x029e:
            r5 = r11
            r6 = r10
            goto L_0x01cf
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.r.a(int, int, int, com.uc.c.ca):void");
    }

    public final void a(int i, int i2, ca caVar, int[] iArr) {
        int cp;
        if (caVar == null) {
            return;
        }
        if (i == 6 || i == 1 || i == 2 || i == 5 || i == 8 || i == 56 || i == 50 || i == 52 || i == 54 || i == 53) {
            caVar.LK();
            boolean LM = caVar.LM();
            int i3 = caVar.bGu;
            int i4 = caVar.bGv;
            switch (i) {
                case 1:
                case 50:
                    if (LM && this.CE == 0) {
                        if (i != 50 ? !caVar.b(i, i2, iArr) : !caVar.e(0, -1)) {
                            cp = 0;
                            break;
                        } else {
                            cp = 1;
                            break;
                        }
                    } else {
                        cp = caVar.f(-1, i2, iArr);
                        if (this.mR != null) {
                            this.mR.b(this, caVar);
                            break;
                        }
                    }
                    break;
                case 2:
                case 52:
                    if (LM && this.CE == 0) {
                        if (i != 52 ? !caVar.b(i, i2, iArr) : !caVar.e(-1, 0)) {
                            cp = 0;
                            break;
                        } else {
                            cp = 1;
                            break;
                        }
                    } else {
                        boolean e = caVar.e(0, -1);
                        if (!caVar.KE()) {
                            caVar.b(!e, -1);
                        }
                        if (this.mR != null) {
                            this.mR.b(this, caVar);
                        }
                        cp = 1;
                        break;
                    }
                case 5:
                case 54:
                    if (LM && this.CE == 0) {
                        if (i != 54 ? !caVar.b(i, i2, iArr) : !caVar.e(1, 0)) {
                            cp = 0;
                            break;
                        } else {
                            cp = 1;
                            break;
                        }
                    } else {
                        boolean e2 = caVar.e(0, 1);
                        if (!caVar.KE()) {
                            caVar.b(!e2, 1);
                        }
                        if (this.mR != null) {
                            this.mR.b(this, caVar);
                        }
                        cp = 1;
                        break;
                    }
                    break;
                case 6:
                case 56:
                    if (LM && this.CE == 0) {
                        if (i != 56 ? !caVar.b(i, i2, iArr) : !caVar.e(0, 1)) {
                            cp = 0;
                            break;
                        } else {
                            cp = 1;
                            break;
                        }
                    } else {
                        cp = caVar.f(1, i2, iArr);
                        if (this.mR != null) {
                            this.mR.b(this, caVar);
                            break;
                        }
                    }
                    break;
                case 8:
                case 53:
                    cp = caVar.cp(false);
                    break;
                default:
                    iArr[0] = 0;
                    cp = 0;
                    break;
            }
            if (!(i3 == caVar.bGu && i4 == caVar.bGv)) {
                m(caVar);
            }
            if (an.vA().vC()) {
                caVar.ap();
            }
            w ns = w.ns();
            switch (cp) {
                case 1:
                    ns.cbP |= 2;
                    break;
                case 2:
                    ns.cbP |= 2;
                    caVar.iH(1);
                    break;
                case 3:
                    ns.cbP |= 30;
                    break;
            }
            if (this.p != null) {
                this.DE = true;
                this.p.qW();
            }
        }
    }

    public void a(int i, ca caVar) {
        this.BS.setElementAt(caVar, i);
    }

    public final void a(int i, ca caVar, bk bkVar, w wVar, int i2, int i3, int i4, int i5, int i6) {
        bkVar.e(i2, i3, i4, i5, i6, 0);
    }

    public void a(Bitmap bitmap) {
        if (bitmap != null && this.DH == null) {
            this.DH = new bu(this, bitmap);
            this.DH.start();
        }
    }

    public final void a(bf bfVar) {
        if (bfVar != null) {
            ca jm = jm();
            if (this.Cc != null && this.CE != 1) {
                this.Cc.a(bfVar);
            } else if (jm == null || !jm.Li() || this.CE == 1) {
                b(bfVar);
            } else {
                jm.bHW.a(bfVar);
            }
        }
    }

    public final void a(bi biVar) {
        this.Cc = biVar;
        a(this.Cc.bjL);
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.uc.c.bk r15) {
        /*
            r14 = this;
            r13 = 20
            r12 = 1
            r11 = 0
            r2 = 0
            r1 = 0
            boolean r0 = r14.jg()
            if (r0 != 0) goto L_0x000d
        L_0x000c:
            return
        L_0x000d:
            boolean r0 = r14.Du
            if (r0 != 0) goto L_0x005f
            r14.Du = r12
            boolean r0 = com.uc.c.bc.Cr()
            if (r0 == 0) goto L_0x00d4
            android.view.animation.DecelerateInterpolator r0 = new android.view.animation.DecelerateInterpolator
            r3 = 1070386381(0x3fcccccd, float:1.6)
            r0.<init>(r3)
            r3 = 250(0xfa, float:3.5E-43)
            r9 = r3
            r10 = r0
        L_0x0025:
            android.view.animation.TranslateAnimation r0 = new android.view.animation.TranslateAnimation
            int r3 = com.uc.c.w.MK
            float r4 = (float) r3
            r3 = r1
            r5 = r1
            r6 = r2
            r7 = r1
            r8 = r2
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            r14.CS = r0
            android.view.animation.TranslateAnimation r0 = r14.CS
            r0.setInterpolator(r10)
            android.view.animation.TranslateAnimation r0 = r14.CS
            long r2 = (long) r9
            r0.setDuration(r2)
            android.view.animation.TranslateAnimation r0 = r14.CS
            long r2 = java.lang.System.currentTimeMillis()
            r0.setStartTime(r2)
            android.view.animation.TranslateAnimation r0 = r14.CS
            boolean r0 = r0.isInitialized()
            if (r0 != 0) goto L_0x005f
            android.view.animation.TranslateAnimation r0 = r14.CS
            int r2 = com.uc.c.w.MK
            int r3 = com.uc.c.w.ML
            int r4 = com.uc.c.w.MK
            int r4 = r4 * 2
            int r5 = com.uc.c.w.ML
            r0.initialize(r2, r3, r4, r5)
        L_0x005f:
            android.view.animation.TranslateAnimation r0 = r14.CS
            long r2 = java.lang.System.currentTimeMillis()
            android.view.animation.Transformation r4 = r14.CT
            boolean r0 = r0.getTransformation(r2, r4)
            android.view.animation.Transformation r2 = r14.CT
            android.graphics.Matrix r2 = r2.getMatrix()
            r3 = 9
            float[] r3 = new float[r3]
            r2.getValues(r3)
            r2 = 2
            r2 = r3[r2]
            int r2 = (int) r2
            if (r0 == 0) goto L_0x00a3
            short r3 = r14.Ch
            int r3 = com.uc.c.w.MP
            byte r4 = r14.Ci
            r5 = -1
            if (r4 != r5) goto L_0x00df
            int r4 = com.uc.c.w.MK
            int r2 = r2 - r4
        L_0x008a:
            b.a.a.a.f r4 = r14.Cf
            if (r4 == 0) goto L_0x00e1
            b.a.a.a.u r4 = r15.bkB
            b.a.a.a.f r5 = r14.Cf
            r4.a(r5, r2, r3, r13)
        L_0x0095:
            b.a.a.a.f r4 = r14.Cg
            if (r4 == 0) goto L_0x00fc
            b.a.a.a.u r4 = r15.bkB
            b.a.a.a.f r5 = r14.Cg
            int r6 = com.uc.c.w.MK
            int r2 = r2 + r6
            r4.a(r5, r2, r3, r13)
        L_0x00a3:
            if (r0 != 0) goto L_0x0112
            b.a.a.a.f r0 = r14.Cg     // Catch:{ Exception -> 0x011c, all -> 0x0110 }
            r0.recycle()     // Catch:{ Exception -> 0x011c, all -> 0x0110 }
            b.a.a.a.f r0 = r14.Cf     // Catch:{ Exception -> 0x011c, all -> 0x0110 }
            r0.recycle()     // Catch:{ Exception -> 0x011c, all -> 0x0110 }
        L_0x00af:
            r14.Cg = r11
            r14.Cf = r11
            com.uc.c.ca r0 = r14.jm()
            r0.iH(r12)
            r14.Du = r1
            com.uc.c.w r0 = r14.mR
            r0.b(r11)
            com.uc.a.n r0 = r14.p
            if (r0 == 0) goto L_0x00ca
            com.uc.a.n r0 = r14.p
            r0.qW()
        L_0x00ca:
            com.uc.c.w r0 = r14.mR
            int r1 = r0.cbP
            r1 = r1 | 30
            r0.cbP = r1
            goto L_0x000c
        L_0x00d4:
            android.view.animation.DecelerateInterpolator r0 = new android.view.animation.DecelerateInterpolator
            r0.<init>()
            r3 = 100
            r9 = r3
            r10 = r0
            goto L_0x0025
        L_0x00df:
            int r2 = -r2
            goto L_0x008a
        L_0x00e1:
            b.a.a.a.u r4 = r15.bkB
            r4.DF()
            b.a.a.a.u r4 = r15.bkB
            int r5 = com.uc.c.w.Kg
            r4.setColor(r5)
            b.a.a.a.u r4 = r15.bkB
            int r5 = com.uc.c.w.MM
            int r6 = com.uc.c.w.MN
            r4.o(r2, r3, r5, r6)
            b.a.a.a.u r4 = r15.bkB
            r4.DG()
            goto L_0x0095
        L_0x00fc:
            b.a.a.a.u r4 = r15.bkB
            int r5 = com.uc.c.w.Kg
            r4.setColor(r5)
            b.a.a.a.u r4 = r15.bkB
            int r5 = com.uc.c.w.MK
            int r2 = r2 + r5
            int r5 = com.uc.c.w.MM
            int r6 = com.uc.c.w.MN
            r4.o(r2, r3, r5, r6)
            goto L_0x00a3
        L_0x0110:
            r0 = move-exception
            throw r0
        L_0x0112:
            com.uc.a.n r0 = r14.p
            if (r0 == 0) goto L_0x00ca
            com.uc.a.n r0 = r14.p
            r0.qW()
            goto L_0x00ca
        L_0x011c:
            r0 = move-exception
            goto L_0x00af
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.r.a(com.uc.c.bk):void");
    }

    public void a(bk bkVar, int i, int i2, int i3, int i4) {
    }

    public void a(bk bkVar, ca caVar, int i, int i2) {
        if (caVar != null && caVar.bGm > caVar.bGr) {
            int jl = caVar.jl(caVar.bGm);
            int jl2 = caVar.jl(caVar.bGw);
            if (jl <= caVar.bGr) {
                this.Cv = caVar.bGr;
                return;
            }
            this.Cv = b(bkVar, 0, i, caVar.bGr, w.Ke, jl, caVar.bGr, jl2, this.mR, i2);
        }
    }

    public void a(bk bkVar, ca caVar, int i, int i2, int i3) {
        if (caVar != null && !caVar.LH()) {
            int i4 = w.MK - w.Ke;
            byte b2 = w.Ke;
            if (caVar.ahU >= i2 || !caVar.Kr()) {
                int i5 = caVar.ahU;
            }
            int i6 = caVar.bGx;
            int jl = caVar.jl(caVar.bGx);
            int jl2 = caVar.jl(caVar.ahU);
            if (jl2 <= i2) {
                this.Cu = i2;
                return;
            }
            this.Cu = a(bkVar, i4, i, b2, i2, jl2, i2, jl, this.mR, i3);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(int, int):int}
     arg types: [int, short]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int} */
    public void a(bk bkVar, ca caVar, int i, int i2, int i3, int i4, int[] iArr, boolean z, boolean z2, boolean z3) {
        if (!caVar.Lj()) {
            b(bkVar, caVar);
            return;
        }
        Vector vector = new Vector(10);
        boolean ac = caVar.ac();
        if (z) {
            vector.addElement(new int[]{i, i2, i3, i4});
        } else {
            if (caVar.LM()) {
                vector.addElement(new int[]{caVar.bGI - 6, caVar.bGJ - 2, 22, 22});
            }
            vector.addElement(new int[]{i, i2, i3, i4});
            a(vector, caVar, caVar.bHn, caVar.bHo);
        }
        int i5 = z2 ? 0 | 2 : 0;
        bkVar.gZ(caVar.bFQ);
        bkVar.save();
        bkVar.Du();
        int i6 = 0;
        while (true) {
            int i7 = i6;
            if (i7 < vector.size()) {
                int[] iArr2 = (int[]) vector.elementAt(i7);
                if (!ac) {
                    bkVar.l(iArr2[0], iArr2[1], iArr2[2], iArr2[3]);
                }
                bkVar.setColor(this.mR.JS == this ? bb.beo : caVar.ac() ? caVar.Kt() : caVar.Ks());
                int[] iArr3 = {i, i2, i3, i4};
                if (this != this.mR.ce(0)) {
                    bkVar.o(i, i2, i3, i4);
                }
                if (!ac) {
                    caVar.bFR.a(bkVar, caVar, Math.max(iArr2[0], caVar.bGw), z3 ? iArr2[1] : Math.max(iArr2[1], caVar.bGx), Math.min(iArr2[2], (int) caVar.bGr), Math.min(iArr2[3], (int) caVar.bGs), i5);
                } else {
                    caVar.bFR.a(bkVar, caVar, iArr2[0], iArr2[1], iArr2[2], iArr2[3], i5);
                }
                i6 = i7 + 1;
            } else {
                vector.removeAllElements();
                bkVar.Dv();
                bkVar.reset();
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(bk bkVar, ca caVar, Vector vector, boolean z) {
        if (this.Db == null || caVar != this.Dc) {
            ak();
            this.Db = new bj(caVar.bGm, caVar.ahU);
            this.Db.De();
            this.Dc = caVar;
            this.De = this.Dc.bHl;
            this.Df = this.Dc.bHm;
        }
        a(bkVar, caVar, vector);
        int size = vector.size();
        for (int i = 0; i < size; i++) {
            a(bkVar, caVar, (int[]) vector.get(i), z);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int, int, int, int[], boolean, boolean, boolean):void
     arg types: [com.uc.c.bk, com.uc.c.ca, int, int, int, short, int[], int, int, int]
     candidates:
      com.uc.c.r.a(com.uc.c.bk, int, int, int, int, int, int, int, com.uc.c.w, int):int
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int, int, int, int[], boolean, boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int, int, int, int[], boolean, boolean, boolean):void
     arg types: [com.uc.c.bk, com.uc.c.ca, int, int, short, int, int[], int, int, int]
     candidates:
      com.uc.c.r.a(com.uc.c.bk, int, int, int, int, int, int, int, com.uc.c.w, int):int
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int, int, int, int[], boolean, boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int, int, int, int[], boolean, boolean, boolean):void
     arg types: [com.uc.c.bk, com.uc.c.ca, int, int, int, int, int[], boolean, int, int]
     candidates:
      com.uc.c.r.a(com.uc.c.bk, int, int, int, int, int, int, int, com.uc.c.w, int):int
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int, int, int, int[], boolean, boolean, boolean):void */
    public void a(bk bkVar, ca caVar, boolean z, int[] iArr, boolean z2) {
        n ch;
        if (this == this.mR.ce(0)) {
            Picture picture = this.mR.QT;
            Rect clipBounds = bkVar.bkB.bmb.getClipBounds();
            int[] iArr2 = {caVar.bGw, caVar.bGx, clipBounds.width(), clipBounds.height()};
            if (ch() != null) {
                ch().a(bkVar.bkB.bmc, iArr2);
            }
            if (!z2 || picture == null || caVar.ac() || caVar.Mb() || an.vA().vC() || this.mR == null || this != this.mR.lB() || this.mR.QW != w.MN) {
                a(bkVar, caVar, caVar.bGw, caVar.bGx, clipBounds.width(), clipBounds.height(), iArr, z, true, false);
                return;
            }
            bkVar.bkB.bmb.save();
            bkVar.bkB.bmb.translate((float) (this.mR.QU - caVar.bGw), (float) (this.mR.QV - caVar.bGx));
            picture.draw(bkVar.bkB.bmb);
            bkVar.bkB.bmb.restore();
            if (this.mR.QU > caVar.bGw) {
                a(bkVar, caVar, caVar.bGw, caVar.bGx, this.mR.QU - caVar.bGw, (int) caVar.bGs, iArr, false, true, false);
            } else if (this.mR.QU < caVar.bGw) {
                a(bkVar, caVar, caVar.bGr + this.mR.QU, caVar.bGx, caVar.bGw - this.mR.QU, (int) caVar.bGs, iArr, false, true, false);
            }
            if (this.mR.QV > caVar.bGx) {
                a(bkVar, caVar, caVar.bGw, caVar.bGx, (int) caVar.bGr, this.mR.QV - caVar.bGx, iArr, false, true, false);
            } else if (this.mR.QV < caVar.bGx) {
                a(bkVar, caVar, caVar.bGw, picture.getHeight() + this.mR.QV, (int) caVar.bGr, caVar.bGx - this.mR.QV, iArr, false, true, false);
            }
        } else {
            f fVar = this.mR.QP;
            if (!z2 || caVar.aq() || fVar == null || caVar.ac() || caVar.Mb() || ((an.vA().vC() && an.vA().vx()) || this.mR == null || this == this.mR.ce(0) || this != this.mR.lB() || this.mR.QS != w.MN)) {
                a(bkVar, caVar, caVar.bGw, caVar.bGx, (int) (((float) w.MK) / caVar.bOn), (int) (((float) w.ML) / caVar.bOn), iArr, z, true, false);
            } else {
                bkVar.d(fVar, this.mR.QQ - caVar.bGw, this.mR.QR - caVar.bGx);
                if (this.mR.QQ > caVar.bGw) {
                    a(bkVar, caVar, caVar.bGw, caVar.bGx, this.mR.QQ - caVar.bGw, (int) caVar.bGs, iArr, false, true, false);
                } else if (this.mR.QQ < caVar.bGw) {
                    a(bkVar, caVar, caVar.bGr + this.mR.QQ, caVar.bGx, caVar.bGw - this.mR.QQ, (int) caVar.bGs, iArr, false, true, false);
                }
                if (this.mR.QR > caVar.bGx) {
                    if (!m.dq().d(bkVar, caVar, caVar.bGw, caVar.bGx, caVar.bGr, this.mR.QR - caVar.bGx, 0)) {
                        a(bkVar, caVar, caVar.bGw, caVar.bGx, (int) caVar.bGr, this.mR.QR - caVar.bGx, iArr, false, true, false);
                    }
                } else if (this.mR.QR < caVar.bGx) {
                    byte b2 = 0;
                    if (caVar.bGm > caVar.bGr) {
                        b2 = w.Ke;
                    }
                    int i = (caVar.bGs + this.mR.QR) - b2;
                    int i2 = (caVar.bGx - this.mR.QR) + b2;
                    if (!(b2 == 0 ? m.dq().d(bkVar, caVar, caVar.bGw, i, caVar.bGr, i2, 1) : false)) {
                        a(bkVar, caVar, caVar.bGw, i, (int) caVar.bGr, i2, iArr, false, true, false);
                    }
                }
            }
            if (an.vA().vC() && (ch = caVar.ch()) != null) {
                int[] K = an.vA().K(caVar);
                int[] L = an.vA().L(caVar);
                if (K != null) {
                    K[0] = K[0] + bkVar.left;
                    K[1] = K[1] + bkVar.top;
                }
                if (L != null) {
                    L[0] = L[0] + bkVar.left;
                    L[1] = L[1] + bkVar.top;
                }
                ch.a(K, L, bkVar);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(bk bkVar, ca caVar, int[] iArr, boolean z) {
        int i = iArr[0];
        int i2 = iArr[1];
        int i3 = iArr[2];
        int i4 = iArr[3];
        if (!(this.De == caVar.bHl && this.Df == caVar.bHm)) {
            caVar.f(this.De, this.Df);
            this.De = caVar.bHl;
            this.Df = caVar.bHm;
            caVar.f(this.De, this.Df);
        }
        Vector LP = caVar.LP();
        if (LP.size() > 1000) {
            this.Db.De();
        } else {
            int size = LP.size();
            for (int i5 = 0; i5 < size; i5++) {
                int[] iArr2 = (int[]) LP.get(i5);
                this.Db.invalidate(iArr2[0], iArr2[1], iArr2[2], iArr2[3]);
            }
        }
        Vector j = this.Db.j(i, i2, i3, i4);
        if (j.size() > 0 && this.Dd == null) {
            new ap(this, j, caVar, this.Db).eX(50);
        }
        this.Db.c(bkVar, i, i2, i3, i4);
    }

    public final void a(bn bnVar) {
        this.BK = bnVar;
    }

    public void a(bx bxVar, String str, ca caVar) {
        if (bxVar != null && str != null && caVar != null) {
            caVar.Kv();
            caVar.bm(2, 4);
            caVar.KG();
            caVar.KV();
            caVar.af("img", null);
            caVar.b(bc.dT(str), (byte[]) null, (byte[]) null);
            caVar.KZ();
            caVar.KW();
            if (ch() != null) {
                caVar.t(ar.avU[1].trim());
                ch().bL(ar.avU[1].trim());
            }
            bxVar.bxR = new bx(caVar);
            bxVar.bxR.start();
            bxVar.bxR.byG = 1;
            bxVar.bxR.p(str, false);
            while (bxVar.bxR.byG != 2) {
                try {
                    Thread.sleep(300);
                } catch (Exception e) {
                }
            }
            caVar.iK(5);
            caVar.wx();
            caVar.KH();
            if (this.BS != null) {
                int i = 0;
                while (i < this.BS.size() && ((ca) this.BS.get(i)) != caVar) {
                    i++;
                }
                if (i >= this.BS.size()) {
                    o(caVar);
                }
            }
            ch().sh();
        }
    }

    public void a(ca caVar, int i, k kVar, int i2, bk bkVar, int i3, boolean z, char[] cArr, int i4, int i5, int i6, int i7, int i8, int i9, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
    }

    public final void a(ca caVar, String str) {
        if (caVar != null) {
            if (this.BT.size() > 1) {
                jc();
            }
            this.BT.put(str, caVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.a(int, int, int, int, boolean):boolean
     arg types: [int, int, int, int, int]
     candidates:
      com.uc.c.r.a(com.uc.c.ca, int, int, java.lang.Object, int):b.a.a.a.f
      com.uc.c.r.a(com.uc.c.ca, int, int, int, java.lang.Object):void
      com.uc.c.r.a(com.uc.c.bk, int, int, int, int):void
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int, int):void
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, boolean, int[], boolean):void
      com.uc.c.r.a(int, int, int, int, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.d(int, boolean):void
     arg types: [int, int]
     candidates:
      com.uc.c.r.d(com.uc.c.bf, com.uc.c.bf):void
      com.uc.c.r.d(com.uc.c.ca, int):void
      com.uc.c.r.d(int, boolean):void */
    public void a(ca caVar, boolean z) {
        if (caVar != null && caVar.bFM >= 0) {
            if (this.p != null) {
                this.p.ex(caVar.bFA);
            }
            if (ch() != null) {
                caVar.e(ch().getOrientation());
            }
            int Cs2 = bc.Cs() + 1;
            if (jN()) {
            }
            a(-1, 8, this.BV + 1, -1, true);
            jb();
            this.BS.addElement(caVar);
            if (z) {
                ba(1);
            }
            d(this.BS.size() - 1, false);
        }
    }

    public void a(String str, String str2, byte b2, Object obj, String str3, byte b3, Object obj2) {
        if (this.CE == 0) {
            this.Cy = b2;
            this.CB = b2;
            this.CA = obj;
            this.CD = obj2;
            String[] strArr = new String[1];
            strArr[0] = str == null ? " " : str;
            a(str2, str3, strArr);
        }
    }

    public final void a(String str, String str2, String[] strArr) {
        a(this.mR.a(strArr, ar.avM[29]), 0, bc.dM(str) ? new bf(str, -1) : null, bc.dM(str2) ? new bf(str2, -1) : null);
    }

    public final void a(String[] strArr, int i, String str) {
        e(this.mR.a(strArr, str), i);
    }

    public final boolean a(byte b2, byte b3, Object obj) {
        return a(b2, b3, obj, (byte) -1, (byte) -1, (Object) null);
    }

    public final boolean a(byte b2, byte b3, Object obj, byte b4, byte b5, Object obj2) {
        if (this.CE != 0) {
            return false;
        }
        this.Cy = b2;
        this.Cz = b3;
        this.CA = obj;
        this.CB = b4;
        this.CC = b5;
        this.CD = obj2;
        String[] H = H(b2, b3);
        if (b2 == -110) {
            e(this.mR.mP(), 0);
            return true;
        }
        h(H);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.d(int, boolean):void
     arg types: [int, int]
     candidates:
      com.uc.c.r.d(com.uc.c.bf, com.uc.c.bf):void
      com.uc.c.r.d(com.uc.c.ca, int):void
      com.uc.c.r.d(int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.a(int, int, int, int, boolean):boolean
     arg types: [int, int, int, int, int]
     candidates:
      com.uc.c.r.a(com.uc.c.ca, int, int, java.lang.Object, int):b.a.a.a.f
      com.uc.c.r.a(com.uc.c.ca, int, int, int, java.lang.Object):void
      com.uc.c.r.a(com.uc.c.bk, int, int, int, int):void
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int, int):void
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, boolean, int[], boolean):void
      com.uc.c.r.a(int, int, int, int, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bc.b(com.uc.c.ca, boolean):boolean
     arg types: [com.uc.c.ca, int]
     candidates:
      com.uc.c.bc.b(int, char):int
      com.uc.c.bc.b(java.lang.StringBuffer, java.lang.String):void
      com.uc.c.bc.b(byte[], byte):void
      com.uc.c.bc.b(com.uc.c.ca, boolean):boolean */
    public final boolean a(byte b2, boolean z) {
        if (bc.Cp()) {
            this.mR.bW(512000);
        }
        int bc = bc(b2);
        if (bc == -1) {
            return false;
        }
        ca k = k(b2);
        if (!(k == null || k.bFA == null || !k.bFA.startsWith("https://"))) {
            k.bGd.cd();
        }
        if (z) {
            ba(b2);
        }
        ca jj = jj();
        if (jj != null && !w.OX.equals(jj.bFA) && !w.Pa.equals(jj.bFA)) {
            jj.cr(true);
        }
        k(k);
        d(bc, true);
        if (b2 == 1) {
            if (this.BV >= 20) {
                a(-1, 6, 0, (this.BV - 20) + 1, true);
            }
        } else if (b2 == -1 && (this.BS.size() - 1) - this.BV >= 20) {
            a(-1, 6, this.BV + 20, (this.BS.size() - this.BV) - 20, true);
        }
        jA();
        if (k == null || !bc.aL(k.bFP, 2)) {
            ja();
        } else {
            if (bc.b(k, true)) {
                ak();
            } else if (ch() != null) {
                ch().EC();
            } else {
                this.mR.a(this, k);
            }
            k.bFP = (byte) bc.aJ(k.bFP, 2);
        }
        this.mR.b((f) null);
        if (ch() != null) {
            ch().qW();
        }
        return true;
    }

    public final boolean a(int i, int i2, int i3, int i4, boolean z) {
        boolean z2;
        ca aZ;
        int size = i4 == -1 ? this.BS.size() : i4;
        int i5 = i3;
        boolean z3 = false;
        while (true) {
            if (i2 >= 6 || i5 >= size) {
                z2 = z3;
            } else {
                ca aZ2 = aZ(i5);
                if (aZ2 != null && !bc.aL(aZ2.bHr, i2) && (((!jr() && !jh() && i2 != 4) || i5 != this.BV) && aZ2.bFA != null && aZ2.bFA.length() > 0)) {
                    boolean z4 = z3;
                    int i6 = i2;
                    do {
                        switch (i6) {
                            case 4:
                                if (!aZ2.KE()) {
                                    if (bc.O(aZ2)) {
                                        aZ2.LU();
                                        bc.gc();
                                        break;
                                    }
                                } else {
                                    aZ2.LU();
                                    break;
                                }
                                break;
                        }
                        if (!z) {
                            z4 = !this.mR.bX(i);
                            if (z4) {
                                z2 = z4;
                            }
                        }
                        i6++;
                    } while (i6 != 5);
                    z3 = z4;
                }
                i5++;
            }
        }
        if (i2 == 6) {
            for (int i7 = (i3 + size) - 1; i7 >= i3; i7--) {
                if (!(i7 == this.BV || (aZ = aZ(i7)) == null)) {
                    bc.R(aZ);
                    aZ.LU();
                }
            }
        }
        if (i2 == 8) {
            for (int i8 = size - 1; i8 >= i3; i8--) {
                if (i8 != this.BV) {
                    ca aZ3 = aZ(i8);
                    if (aZ3 != null) {
                        bc.R(aZ3);
                        aZ3.Lp();
                    }
                    this.BS.removeElementAt(i8);
                }
            }
            this.BV = this.BV > this.BS.size() - 1 ? this.BS.size() - 1 : this.BV;
        }
        return z2;
    }

    /* JADX WARN: Type inference failed for: r7v3, types: [int] */
    /* JADX WARN: Type inference failed for: r8v3, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(com.uc.c.bk r13, com.uc.c.ca r14, com.uc.c.ca r15) {
        /*
            r12 = this;
            boolean r0 = r15.ac()
            int r1 = r15.bGv
            int r2 = r15.bGx
            int r1 = r1 - r2
            int r2 = r15.bGu
            int r3 = r15.bGw
            int r2 = r2 - r3
            if (r1 >= 0) goto L_0x00ac
            r3 = -1
        L_0x0011:
            if (r2 >= 0) goto L_0x00af
            r4 = -1
        L_0x0014:
            int r1 = java.lang.Math.abs(r1)
            int r1 = r1 >> 1
            int r2 = java.lang.Math.abs(r2)
            int r2 = r2 >> 1
            r5 = 5
            r6 = 100
            short r7 = r14.bGn
            short r8 = r14.bGo
            if (r14 == r15) goto L_0x01f5
            short r9 = r15.bGn
            int r7 = r7 + r9
            short r9 = r15.bGo
            int r8 = r8 + r9
            r11 = r8
            r8 = r7
            r7 = r11
        L_0x0032:
            if (r0 == 0) goto L_0x0122
            int r0 = r15.bGx
            int r9 = r15.bGv
            if (r0 != r9) goto L_0x0051
            int r0 = r15.bGw
            int r9 = r15.bGu
            if (r0 != r9) goto L_0x0046
            int r0 = r15.bGx
            int r9 = r15.bGv
            if (r0 == r9) goto L_0x0051
        L_0x0046:
            r0 = 1
            boolean r0 = r15.iJ(r0)
            if (r0 != 0) goto L_0x0051
            boolean r0 = r12.Cm
            if (r0 == 0) goto L_0x00b2
        L_0x0051:
            r0 = 1
        L_0x0052:
            if (r0 == 0) goto L_0x0058
            r9 = 1
            r15.iH(r9)
        L_0x0058:
            float r9 = r15.bOm
            float r10 = r15.bOn
            float r9 = r9 - r10
            float r9 = java.lang.Math.abs(r9)
            r10 = 981668463(0x3a83126f, float:0.001)
            int r10 = (r9 > r10 ? 1 : (r9 == r10 ? 0 : -1))
            if (r10 >= 0) goto L_0x00c2
            float r9 = r15.bOm
            r15.bOn = r9
            int r5 = r15.jk(r5)
            int r6 = r15.jk(r6)
            boolean r9 = r12.Cn
            if (r9 != 0) goto L_0x007e
            boolean r9 = com.uc.c.az.bch
            if (r9 == 0) goto L_0x007e
            if (r1 > r5) goto L_0x00b4
        L_0x007e:
            int r1 = r15.bGv
            r15.bGx = r1
        L_0x0082:
            if (r2 < r6) goto L_0x01f2
            r1 = r6
        L_0x0085:
            boolean r2 = r12.Cn
            if (r2 != 0) goto L_0x0093
            boolean r2 = com.uc.c.az.bch
            if (r2 == 0) goto L_0x0093
            if (r1 <= r5) goto L_0x0093
            boolean r2 = r12.Cm
            if (r2 == 0) goto L_0x00bb
        L_0x0093:
            int r1 = r15.bGu
            r15.bGw = r1
        L_0x0097:
            int r1 = r15.bGw
            int r2 = r15.bGx
            float r3 = r15.bOn
            float r4 = r15.bOn
            r13.scale(r3, r4)
            int r1 = r8 - r1
            float r1 = (float) r1
            int r2 = r7 - r2
            float r2 = (float) r2
            r13.translate(r1, r2)
        L_0x00ab:
            return r0
        L_0x00ac:
            r3 = 1
            goto L_0x0011
        L_0x00af:
            r4 = 1
            goto L_0x0014
        L_0x00b2:
            r0 = 0
            goto L_0x0052
        L_0x00b4:
            int r9 = r15.bGx
            int r1 = r1 * r3
            int r1 = r1 + r9
            r15.bGx = r1
            goto L_0x0082
        L_0x00bb:
            int r2 = r15.bGw
            int r1 = r1 * r4
            int r1 = r1 + r2
            r15.bGw = r1
            goto L_0x0097
        L_0x00c2:
            float r1 = r15.bOo
            int r1 = (r9 > r1 ? 1 : (r9 == r1 ? 0 : -1))
            if (r1 <= 0) goto L_0x0114
            float r1 = r15.bOn
            float r2 = r15.bOm
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 <= 0) goto L_0x010c
            float r1 = r15.bOn
            float r2 = r15.bOo
            float r1 = r1 - r2
            r15.bOn = r1
        L_0x00d7:
            int r1 = r15.bGu
            float r1 = (float) r1
            int r2 = com.uc.c.w.MM
            float r2 = (float) r2
            float r3 = r15.bOm
            float r2 = r2 / r3
            r3 = 1073741824(0x40000000, float:2.0)
            float r2 = r2 / r3
            float r1 = r1 + r2
            int r2 = com.uc.c.w.MM
            float r2 = (float) r2
            float r3 = r15.bOn
            float r2 = r2 / r3
            r3 = 1073741824(0x40000000, float:2.0)
            float r2 = r2 / r3
            float r1 = r1 - r2
            int r1 = (int) r1
            r15.bGw = r1
            int r1 = r15.bGv
            float r1 = (float) r1
            int r2 = com.uc.c.w.MN
            float r2 = (float) r2
            float r3 = r15.bOm
            float r2 = r2 / r3
            r3 = 1073741824(0x40000000, float:2.0)
            float r2 = r2 / r3
            float r1 = r1 + r2
            int r2 = com.uc.c.w.MN
            float r2 = (float) r2
            float r3 = r15.bOn
            float r2 = r2 / r3
            r3 = 1073741824(0x40000000, float:2.0)
            float r2 = r2 / r3
            float r1 = r1 - r2
            int r1 = (int) r1
            r15.bGx = r1
            goto L_0x0097
        L_0x010c:
            float r1 = r15.bOn
            float r2 = r15.bOo
            float r1 = r1 + r2
            r15.bOn = r1
            goto L_0x00d7
        L_0x0114:
            float r1 = r15.bOm
            r15.bOn = r1
            int r1 = r15.bGu
            r15.bGw = r1
            int r1 = r15.bGv
            r15.bGx = r1
            goto L_0x0097
        L_0x0122:
            int r0 = r15.bGu
            int r4 = r15.bGw
            if (r0 != r4) goto L_0x0181
            int r0 = r15.bGv
            int r4 = r15.bGx
            if (r0 != r4) goto L_0x0181
            com.uc.c.f r0 = r15.bHl
            com.uc.c.f r4 = r15.bHn
            if (r0 != r4) goto L_0x0140
            int r0 = r15.bHo
            int r4 = r15.bHm
            if (r0 != r4) goto L_0x0140
            boolean r0 = r15.LM()
            if (r0 == 0) goto L_0x0181
        L_0x0140:
            com.uc.c.f r0 = r15.bHl
            if (r0 == 0) goto L_0x0152
            com.uc.c.f r0 = r15.bHn
            if (r0 == 0) goto L_0x0152
            int r0 = r15.bHo
            r4 = -1
            if (r0 == r4) goto L_0x0152
            int r0 = r15.bHm
            r4 = -1
            if (r0 != r4) goto L_0x0158
        L_0x0152:
            boolean r0 = r15.LM()
            if (r0 == 0) goto L_0x0181
        L_0x0158:
            r0 = 3
            boolean r0 = r15.iJ(r0)
            if (r0 != 0) goto L_0x0181
            byte r0 = r12.BY
            r4 = 7
            if (r0 == r4) goto L_0x0181
            r0 = 1
            boolean r0 = r15.iJ(r0)
            if (r0 != 0) goto L_0x0181
            boolean r0 = r15.LM()
            if (r0 == 0) goto L_0x0177
            byte r0 = r15.bGR
            byte r4 = r15.bGS
            if (r0 == r4) goto L_0x0181
        L_0x0177:
            boolean r0 = r15.LM()
            if (r0 == 0) goto L_0x01e5
            int r0 = r15.bGJ
            if (r0 != 0) goto L_0x01e5
        L_0x0181:
            r0 = 1
        L_0x0182:
            if (r0 == 0) goto L_0x0188
            r4 = 1
            r15.iH(r4)
        L_0x0188:
            short r4 = r15.bGs
            short r6 = r15.bGr
            boolean r6 = r15.LM()
            if (r6 == 0) goto L_0x01a1
            int r6 = r15.bGL
            int r9 = r15.bGJ
            int r6 = r6 - r9
            int r6 = java.lang.Math.abs(r6)
            int r5 = java.lang.Math.max(r5, r6)
            int r5 = r5 + 2
        L_0x01a1:
            com.uc.c.w r6 = r12.mR
            short r6 = r6.Le
            r9 = 100
            if (r6 <= r9) goto L_0x01e7
            int r6 = r1 >> 1
        L_0x01ab:
            int r1 = r1 + r6
            int r1 = java.lang.Math.max(r1, r5)
            com.uc.c.w r6 = r12.mR
            short r6 = r6.Le
            r9 = 100
            if (r6 <= r9) goto L_0x01e9
            int r6 = r2 >> 1
        L_0x01ba:
            int r2 = r2 + r6
            java.lang.Math.max(r2, r5)
            boolean r2 = r12.Cn
            if (r2 != 0) goto L_0x01d4
            boolean r2 = com.uc.c.az.bch
            if (r2 == 0) goto L_0x01d4
            if (r1 <= r5) goto L_0x01d4
            com.uc.c.w r2 = r12.mR
            short r2 = r2.Le
            r5 = 200(0xc8, float:2.8E-43)
            if (r2 <= r5) goto L_0x01eb
            int r2 = r4 >> 1
            if (r1 > r2) goto L_0x01eb
        L_0x01d4:
            int r1 = r15.bGv
            r15.bGx = r1
        L_0x01d8:
            int r1 = r15.bGw
            int r1 = r8 - r1
            int r2 = r15.bGx
            int r2 = r7 - r2
            r13.bb(r1, r2)
            goto L_0x00ab
        L_0x01e5:
            r0 = 0
            goto L_0x0182
        L_0x01e7:
            r6 = 0
            goto L_0x01ab
        L_0x01e9:
            r6 = 0
            goto L_0x01ba
        L_0x01eb:
            int r2 = r15.bGx
            int r1 = r1 * r3
            int r1 = r1 + r2
            r15.bGx = r1
            goto L_0x01d8
        L_0x01f2:
            r1 = r2
            goto L_0x0085
        L_0x01f5:
            r11 = r8
            r8 = r7
            r7 = r11
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, com.uc.c.ca):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, java.util.Vector, boolean):void
     arg types: [com.uc.c.bk, com.uc.c.ca, java.util.Vector, int]
     candidates:
      com.uc.c.r.a(com.uc.c.ca, int, com.uc.c.bf, com.uc.c.bf):void
      com.uc.c.r.a(com.uc.c.bk, byte[], com.uc.c.ca, java.lang.Object):boolean
      com.uc.c.r.a(java.util.Vector, com.uc.c.ca, com.uc.c.f, int):int
      com.uc.c.r.a(int, int, int, com.uc.c.ca):void
      com.uc.c.r.a(int, int, com.uc.c.ca, int[]):void
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int):void
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int[], boolean):void
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, java.util.Vector, boolean):void */
    public boolean a(bk bkVar, boolean z, boolean z2) {
        ca jl = jl();
        if (jl == null) {
            return true;
        }
        ca jm = jm();
        if (jm == null) {
            return true;
        }
        bkVar.bkB.save();
        jm.LK();
        boolean z3 = jl != jm;
        if (jl.KF() == 1) {
            jl.t(w.MO, w.MP, w.MK, w.MN);
            jl.bp(this == this.mR.JS ? w.MK : w.MM, w.MN);
            if (jl.bGm > jl.bGr) {
                jl.bp(-1, jl.bGs - w.Ke);
            }
        }
        if (z3 && jm.KF() == 1) {
            jm.bo(w.MK, w.MN - jm.bGo);
            jm.bp(this == this.mR.JS ? w.MK : w.MM, w.MN - jm.bGo);
            if (jm.bGm > jm.bGr) {
                jm.bp(-1, (w.MN - jm.bGo) - w.Ke);
            }
        }
        short s = jl.bGn;
        short s2 = jl.bGo;
        if (z3) {
            int i = jm.bGn + s;
            int i2 = jm.bGo + s2;
        }
        boolean a2 = a(bkVar, jl, jm);
        Vector KP = jm.KP();
        try {
            if (!jm.ac() || this.mR.Qw || (an.vA().vC() && !jm.LK())) {
                jm.t(KP);
                a(bkVar, jm, z || a2 || this.DE, (int[]) null, z2);
            } else {
                Vector vector = new Vector(1);
                vector.addElement(new int[]{jm.bGw, jm.bGx, (int) (((float) w.MK) / jm.bOn), (int) (((float) w.ML) / jm.bOn)});
                a(bkVar, jm, vector, true);
            }
            jm.i(bkVar);
            this.DE = false;
            bkVar.bkB.restore();
            jm.u(KP);
            if (jm != null) {
                jm.bHn = jm.bHl;
                jm.bHo = jm.bHm;
                jm.bGI = jm.bGK;
                jm.bGJ = jm.bGL;
            }
            if (!((jm == null || jm.bGx == jm.bGv) && jm.bGw == jm.bGu) && az.bch) {
                this.mR.cbP |= 30;
            }
        } catch (Throwable th) {
            this.DE = false;
            bkVar.bkB.restore();
            jm.u(KP);
            if (jm != null) {
                jm.bHn = jm.bHl;
                jm.bHo = jm.bHm;
                jm.bGI = jm.bGK;
                jm.bGJ = jm.bGL;
            }
            if (!((jm == null || jm.bGx == jm.bGv) && jm.bGw == jm.bGu) && az.bch) {
                this.mR.cbP |= 30;
            }
            throw th;
        }
        return a2;
    }

    public final boolean a(ca caVar, int i, int i2) {
        m(caVar);
        if (caVar.LK()) {
            caVar.g(i, i2);
        } else {
            caVar.bGv += i2;
            short s = caVar.bGs;
            int i3 = caVar.bGv;
            int i4 = caVar.ahU - s;
            if (i3 > i4) {
                caVar.bGv = i4;
            }
            if (caVar.bGv < 0) {
                caVar.bGv = 0;
                return false;
            } else if (caVar.bGv >= caVar.ahU - s) {
                caVar.bGv = caVar.ahU - s;
                return false;
            }
        }
        return true;
    }

    public int[] a(int i, int i2, int i3, k kVar, int i4, char[] cArr, int i5, int i6) {
        int b2;
        int b3;
        int[] iArr = new int[4];
        if (cArr == null) {
            return null;
        }
        int i7 = i2 < 0 ? 0 : i2;
        if (i <= i7) {
            try {
                iArr[2] = i5;
                iArr[0] = i7;
            } catch (Exception e) {
                return null;
            }
        } else {
            int i8 = i - i7;
            int i9 = i5;
            while (true) {
                if (i9 >= i6 || iArr[1] >= i8) {
                    break;
                }
                if (kVar != null) {
                    b2 = kVar.a(cArr[i9]);
                    iArr[1] = iArr[1] + b2;
                } else {
                    b2 = bc.b(i4, cArr[i9]);
                    iArr[1] = iArr[1] + b2;
                }
                if (iArr[1] + i7 > i) {
                    iArr[1] = iArr[1] - b2;
                    break;
                }
                i9++;
            }
            iArr[2] = i9;
            iArr[0] = i7 + iArr[1];
        }
        iArr[1] = 0;
        int i10 = iArr[2];
        int i11 = 0;
        int i12 = 0;
        while (true) {
            if (i10 >= i6) {
                break;
            } else if (iArr[0] + iArr[1] < i + i3) {
                int i13 = i11 + 1;
                if (kVar != null) {
                    b3 = kVar.a(cArr[i10]);
                    iArr[1] = iArr[1] + b3;
                } else {
                    b3 = bc.b(i4, cArr[i10]);
                    iArr[1] = iArr[1] + b3;
                }
                i10++;
                int i14 = b3;
                i11 = i13;
                i12 = i14;
            } else if (i + i3 > (iArr[0] + iArr[1]) - i12) {
                int i15 = i10 + 1;
            }
        }
        if (iArr[2] >= cArr.length) {
            iArr[2] = 0;
        }
        iArr[3] = i11;
        return iArr;
    }

    public final void aX(int i) {
        this.BX = (byte) i;
        switch (i) {
            case 0:
            case 3:
                if (!c(this.mR.Lt, (bf) null)) {
                    b(this.mR.Lt, (bf) null);
                }
                this.BZ = null;
                return;
            case 1:
                b((bf) null, this.mR.Lw);
                this.Ca = 0;
                this.Cb = 1;
                return;
            case 2:
            default:
                return;
        }
    }

    public final void aY(int i) {
        if (this.BX != i) {
            aX(i);
            if (this.BW != null && i == 0) {
                boolean z = true;
                if (this.BW.byh != null && this.BW.byh.equals(w.PC)) {
                    if (!(this.mR == null || this.mR.ch() == null)) {
                        this.mR.ch().dO(100);
                    }
                    z = false;
                }
                this.BW.Ev();
                this.BW = null;
                if (!(this.mR == null || ch() == null)) {
                    this.mR.b((f) null);
                    ch().qW();
                }
                if (ch() != null) {
                    ch().aT();
                }
                if (z && this.p != null) {
                    this.p.dO(100);
                }
                K(false);
            }
        }
    }

    public void ak() {
        if (this.Dc != null) {
            this.Dc.cr(true);
        }
        this.Db = null;
        this.Dc = null;
        this.Dd = null;
        this.De = null;
        this.Df = -1;
    }

    public final ca aq(String str) {
        ca caVar = (ca) this.BT.get(str);
        if (caVar != null) {
            this.BT.remove(str);
        }
        return caVar;
    }

    public final boolean ar(String str) {
        ca caVar = (ca) this.BT.get(str);
        return caVar != null && caVar.Ms();
    }

    public final void as(String str) {
        jm().fl(str);
    }

    public final void at(String str) {
        jm().g(str.toCharArray());
    }

    public void au(String str) {
        this.Dz = str;
    }

    public final void b(int i, int[] iArr) {
        d(i, 0, iArr);
    }

    public void b(n nVar) {
        this.p = nVar;
    }

    public final void b(bf bfVar) {
        bn iT = iT();
        if (this.CE == 1) {
            if (bfVar == this.CI) {
                this.mR.b(this.Cy, this.Cz, this.CA);
            } else if (bfVar == this.CJ) {
                this.mR.b(this.CB, this.CC, this.CD);
            }
            this.CE = w.My;
            this.CI = null;
            this.CJ = null;
            this.Cy = -1;
            this.Cz = -1;
            this.CA = null;
            this.CB = -1;
            this.CC = -1;
            this.CD = null;
        } else if (iT != null && bfVar == this.mR.Lt) {
            iT.g(this);
        } else if (bfVar == this.mR.LA) {
            this.mR.f((byte) 12);
            b(this.mR.Lt, (bf) null);
        } else if (bfVar == this.mR.Ly) {
            w wVar = this.mR;
            this.mR.lA().close();
            wVar.Hc();
        } else if (bfVar == this.mR.Lw) {
            m(jm());
            iZ();
        } else if (bfVar == this.mR.LG) {
            r(jm());
            w.ns().Hc();
        } else if (bfVar == this.mR.Lx) {
            this.mR.b((byte) 100, Byte.MIN_VALUE, jm());
            this.mR.aY(ar.avL[25]);
            w wVar2 = this.mR;
            this.mR.lA().close();
            wVar2.Hc();
        } else if (bfVar == this.mR.Lv) {
            b(jl(), 1);
        } else if (bfVar == this.mR.LF) {
            this.mR.mx();
        } else if (bfVar == this.mR.LF) {
            w wVar3 = this.mR;
            this.mR.lA().close();
            wVar3.Hc();
        } else if (!(bfVar == this.mR.LD || bfVar == this.mR.LH || bfVar == this.mR.LI || bfVar.bhW == -1 || bfVar.bhW == -128)) {
            this.mR.f((byte) bfVar.bhW);
        }
        jA();
    }

    public final void b(bf bfVar, bf bfVar2) {
        ca jm = jm();
        if (jm == null || !jm.Li() || js()) {
            c(bfVar, bfVar2);
            d(bfVar, bfVar2);
            return;
        }
        jm.bHW.b(bfVar, bfVar2);
    }

    public void b(bk bkVar) {
        ca jl = jl();
        ca jm = jm();
        if (jl != null && jm != null && !jm.LT()) {
            boolean z = jl != jm;
            int i = jl.bGo;
            if (z) {
                i += jm.bGo;
            }
            b(bkVar, jm, i);
            ch().bh(-1, jm.ahU);
        }
    }

    public void b(bk bkVar, ca caVar) {
        bkVar.setColor(w.Kg);
        bkVar.o(caVar.bGw, caVar.bGx, w.MK, w.ML);
        if (this.BX == 0) {
        }
    }

    public final void b(bx bxVar) {
        if (!(this.BW == null || bxVar == this.BW)) {
            this.BW.Ev();
        }
        this.BW = null;
        this.BW = bxVar;
    }

    public final void b(ca caVar, int i) {
        ca jl = caVar == null ? jl() : caVar;
        int length = this.mR.Kw.length;
        int size = jl.bHu.size();
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                break;
            } else if (this.mR.Kw[i2] != jl.bHv) {
                i2++;
            } else if (this.mR.Kw[((i2 + i) + length) % length] > size - 1) {
                byte b2 = this.mR.Kw[((size - 1) * i * -1) + i2];
            }
        }
        jA();
    }

    public final void b(r rVar) {
        if (rVar == null) {
            return;
        }
        if (rVar == this) {
            throw new Exception("error handle for setting sub win");
        }
        this.BM = rVar;
        rVar.BL = this;
    }

    public final void b(String str, int i, int i2) {
        this.Ca = i;
        this.Cb = i2;
        this.BZ = str;
        if (this.BW == null || this.BW.byh == null || !this.BW.byh.equals(w.PC)) {
            if (this.p != null && this.BW != null) {
                float f = ((float) i) / ((float) i2);
                if (az.bbW == 0) {
                    this.p.dO((int) (f * 94.0f));
                } else if (this.BW != null && this.BW.bxQ <= 2) {
                    this.p.dO(((int) (f * 74.0f)) + 10 + 10);
                } else if (this.BW == null || this.BW.bxQ != 3) {
                    if (this.BW == null || 5 != this.BW.bxQ) {
                        this.p.dO((int) (f * 100.0f));
                    } else {
                        this.p.dO(100);
                    }
                } else if (((double) f) < 0.999d) {
                    this.p.dO(100);
                } else {
                    this.p.dO(100);
                }
            }
        } else if (this.mR != null && this.mR.ch() != null) {
            this.mR.ch().dO(i);
        }
    }

    public void b(List list) {
        if (list != null) {
            if (this.DC == null) {
                this.DC = new Vector(list.size());
            }
            this.DC.removeAllElements();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < list.size()) {
                    this.DC.add((String) list.get(i2));
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.e(int, boolean):int
     arg types: [int, int]
     candidates:
      com.uc.c.r.e(byte[], com.uc.c.ca):int
      com.uc.c.r.e(int, java.lang.String):void
      com.uc.c.r.e(com.uc.c.ca, int):void
      com.uc.c.r.e(int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.a(com.uc.c.ca, float, boolean):b.a.a.a.f
     arg types: [com.uc.c.ca, int, int]
     candidates:
      com.uc.c.r.a(byte[], com.uc.c.ca, java.lang.Object):int
      com.uc.c.r.a(int, com.uc.c.bf, com.uc.c.bf):void
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, java.util.Vector):void
      com.uc.c.r.a(int, int, byte):void
      com.uc.c.r.a(com.uc.c.bx, java.lang.String, com.uc.c.ca):void
      com.uc.c.r.a(java.lang.String, java.lang.String, java.lang.String[]):void
      com.uc.c.r.a(java.lang.String[], int, java.lang.String):void
      com.uc.c.r.a(byte, byte, java.lang.Object):boolean
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, com.uc.c.ca):boolean
      com.uc.c.r.a(com.uc.c.bk, boolean, boolean):boolean
      com.uc.c.r.a(com.uc.c.ca, int, int):boolean
      com.uc.c.r.a(com.uc.c.ca, float, boolean):b.a.a.a.f */
    public final synchronized f bb(int i) {
        ca jm;
        jm = i == 0 ? jm() : g(aZ(e(i, false)));
        return jm == null ? null : a(jm, 1.0f, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.e(int, boolean):int
     arg types: [int, int]
     candidates:
      com.uc.c.r.e(byte[], com.uc.c.ca):int
      com.uc.c.r.e(int, java.lang.String):void
      com.uc.c.r.e(com.uc.c.ca, int):void
      com.uc.c.r.e(int, boolean):int */
    public final int bc(int i) {
        return e(i, true);
    }

    public final void bd(int i) {
        c(i, 0, new int[]{0});
    }

    public final String be(int i) {
        String str = null;
        if (i == -1) {
            bf dx = dx();
            if (dx != null) {
                return dx.wo();
            }
            return null;
        }
        bf dw = dw();
        if (dw != null) {
            str = dw.wo();
        }
        return ((js() || iT() == null || iT().biv != 1) && (this.BR == null || this.BR.biv != 1)) ? str : ar.avG[9];
    }

    public void bf(int i) {
        if (this.mR != null && this.mR.lA() == this) {
            this.mR.cbP |= i;
            if (!bc.aL(this.mR.cbP, 2)) {
                return;
            }
        }
        if (this.p != null) {
            this.p.qW();
        }
    }

    public void bg(int i) {
        if (this.p != null && this.BW != null) {
            if (az.bbW == 0) {
                this.p.dO(i * 100);
            } else if (this.BW != null && this.BW.bxQ <= 2) {
                this.p.dO(i * 50);
            } else if (this.BW != null && this.BW.bxQ == 3) {
                this.p.dO((i * 50) + 50);
            } else if (this.BW == null || 5 != this.BW.bxQ) {
                this.p.dO(i * 100);
            } else {
                this.p.dO(100);
            }
        }
    }

    public void bi(int i) {
        if (i >= 0 && i < this.BS.size()) {
            ca aZ = aZ(i);
            if (aZ != null) {
                bc.R(aZ);
                aZ.Lp();
            }
            this.BS.removeElementAt(i);
            this.BV = this.BV > this.BS.size() - 1 ? this.BS.size() - 1 : this.BV;
        }
    }

    public void c(int i, int i2) {
        this.Dt = jj();
        if (this.Dt == null) {
            T();
        } else if (Math.abs(i2) < 200) {
            T();
        } else {
            this.Dm = true;
            this.Dn = i;
            this.Do = i2;
            this.Dp = 0;
            this.Dq = 0;
            if (i2 >= 1300 || i2 <= -1300) {
                this.Ds = this.Dr;
            } else {
                this.Ds = this.Dr + 200;
            }
            if (this.Do > 0) {
                this.Ds *= -1;
            }
            jF();
            jC();
        }
    }

    public final void c(int i, int i2, ca caVar) {
        if (!jr()) {
            int i3 = w.MO;
            int i4 = w.MP;
            if (caVar.bHu != null && !jr()) {
                if (i < 10) {
                    b(caVar, -1);
                    jA();
                } else if (i > w.MK - 10) {
                    b(caVar, 1);
                    jA();
                } else {
                    int i5 = 10 + 2;
                    int i6 = i4 + 2;
                    int i7 = w.MK - ((10 + 2) << 1);
                    w wVar = this.mR;
                    int i8 = w.MQ - 3;
                    int i9 = i7 / 5;
                    int i10 = (i7 % 5) + i9;
                    int i11 = i3 + 12;
                    int i12 = 0;
                    while (i12 < 5) {
                        if (i >= i11) {
                            if (i < (i12 == 2 ? i10 : i9) + i11) {
                                int i13 = 0;
                                while (i13 < this.mR.Kw.length) {
                                    if (this.mR.Kw[i13] != caVar.bHw) {
                                        i13++;
                                    } else if (i13 + i12 < this.mR.Kw.length && this.mR.Kw[i13 + i12] < caVar.bHu.size()) {
                                        c(caVar, this.mR.Kw[i12 + i13]);
                                        jA();
                                        return;
                                    } else {
                                        return;
                                    }
                                }
                                return;
                            }
                        }
                        i11 += i12 == 2 ? i10 : i9;
                        i12++;
                    }
                }
            }
        }
    }

    public final void c(int i, int i2, int[] iArr) {
        ca jm = jm();
        if (this.Cc != null && this.CE != 1) {
            this.Cc.a(i, iArr);
        } else if (jm == null || !jm.Li() || this.CE == 1) {
            d(i, i2, iArr);
        } else {
            jm.bHW.a(i, iArr);
        }
    }

    public void c(int i, int[] iArr) {
        int i2;
        ca jl = jl();
        bn iT = iT();
        if (jl != null && jl.Lg() && jl.Lh() && jl.bHu != null) {
            if (this.BR != null && this.BR.biv == 1) {
                return;
            }
            if (iT == null || iT.biv != 1) {
                switch (i) {
                    case 1:
                    case 50:
                        if (jl.bHx != 0) {
                            if (bc.aL(iArr[0], 1)) {
                                iArr[0] = bc.aK(iArr[0], 2);
                                jl.bHx = 0;
                                this.mR.Hc();
                            } else {
                                iArr[0] = bc.aJ(iArr[0], 2);
                            }
                            iArr[0] = bc.aK(iArr[0], 4);
                            return;
                        } else if (bc.aL(iArr[0], 2)) {
                            iArr[0] = bc.aJ(iArr[0], 2);
                            iArr[0] = bc.aK(iArr[0], 4);
                            jl.bHx = 1;
                            return;
                        } else {
                            iArr[0] = bc.aK(iArr[0], 1);
                            return;
                        }
                    case 2:
                    case 5:
                    case 52:
                    case 54:
                        if (jl.bHx == 1 && !jr()) {
                            b(jl, (i == 5 || i == 54) ? 1 : -1);
                            iArr[0] = bc.aK(iArr[0], 4);
                            this.mR.Hc();
                            return;
                        }
                        return;
                    case 6:
                    case 56:
                        if (jl.bHx != 0) {
                            iArr[0] = bc.aK(iArr[0], 2);
                            jl.bHx = 0;
                            return;
                        } else if (bc.aL(iArr[0], 2)) {
                            iArr[0] = bc.aJ(iArr[0], 2);
                            iArr[0] = bc.aK(iArr[0], 4);
                            jl.bHx = 1;
                            this.mR.Hc();
                            return;
                        } else {
                            iArr[0] = bc.aK(iArr[0], 1);
                            return;
                        }
                    case 8:
                    case 49:
                    case 53:
                        if (!jr() && jl.bHx == 1) {
                            bn bnVar = this.mR.Kk;
                            int i3 = (w.MK - 24) / 5;
                            int i4 = 0;
                            int i5 = -1;
                            while (true) {
                                if (i4 < this.mR.Kw.length) {
                                    if (this.mR.Kw[i4] == jl.bHw) {
                                        i5 = 0;
                                    }
                                    if (this.mR.Kw[i4] == jl.bHv) {
                                        i2 = i5;
                                    } else {
                                        if (i5 >= 0) {
                                            i5++;
                                        }
                                        i4++;
                                    }
                                } else {
                                    i2 = i5;
                                }
                            }
                            int i6 = (w.MK % 5) + i3;
                            int i7 = (i2 * i3) + w.MO + (i6 / 2) + (i6 > 0 ? i6 >> 1 : 0);
                            int i8 = w.MP + ((w.MQ + 8) >> 1);
                            if (this.mR.Ko != null) {
                                bn bnVar2 = this.mR.Ko;
                            }
                            iArr[0] = bc.aK(iArr[0], 4);
                            this.mR.Hc();
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int, int, int, int[], boolean, boolean, boolean):void
     arg types: [com.uc.c.bk, com.uc.c.ca, int, int, int, int, ?[OBJECT, ARRAY], int, int, int]
     candidates:
      com.uc.c.r.a(com.uc.c.bk, int, int, int, int, int, int, int, com.uc.c.w, int):int
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int, int, int, int[], boolean, boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.w.a(com.uc.c.bk, int, int, int, int, boolean):void
     arg types: [com.uc.c.bk, int, int, int, int, int]
     candidates:
      com.uc.c.w.a(java.lang.String, java.lang.String, byte, java.lang.String[][], java.lang.String[], com.uc.c.bx):boolean
      com.uc.c.w.a(byte, com.uc.c.ca, java.lang.String, byte, byte, com.uc.a.n):com.uc.c.r
      com.uc.c.w.a(com.uc.c.bk, int, int, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean):void
     arg types: [java.lang.String, int, int, int, int, int, int, int]
     candidates:
      com.uc.c.bk.a(int, int, int, int, int, int, b.a.a.a.f, b.a.a.a.f):void
      com.uc.c.bk.a(int, int, int, int, int[], boolean, int, int):void
      com.uc.c.bk.a(int[], int, int, int, int, int, int, boolean):void
      com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int):void
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean):void */
    public void c(bk bkVar) {
        if (this.CE == 1 && this.CG != null) {
            ca caVar = this.CG;
            int i = w.MX - w.MZ;
            int i2 = w.Nb - i;
            bkVar.bkB.l(w.MY, w.MZ, w.Na, w.Nb);
            caVar.bGx = caVar.bGv;
            bkVar.bb(w.MY, (-caVar.bGx) + w.MX);
            a(bkVar, caVar, caVar.bGw, caVar.bGx, w.Na, i2, (int[]) null, true, true, false);
            caVar.i(bkVar);
            String title = caVar.getTitle();
            if (this.CH != null) {
                title = this.CH;
            } else if (title == null) {
                title = ar.avM[32];
            }
            this.mR.a(bkVar, w.Na, i, w.MY, w.MZ, false);
            bkVar.setColor(az.bdf[4]);
            bkVar.gZ(e.jP());
            bkVar.a(title, w.MY + 4, w.MZ, w.Na - 4, i, 1, 2, false);
            if (caVar.ahU > i2) {
                int i3 = w.Nb - i;
                int i4 = (caVar.bGx * i3) / (caVar.ahU - i3);
                bkVar.setColor(6908265);
                int i5 = (w.MY + w.Na) - 2;
                int i6 = w.MX + i4;
                bkVar.bkB.f(i5 - 2, i6, i5 + 1, i6 + 3, i5 + 1, i6 - 3);
            }
        }
    }

    public final void c(ca caVar, int i) {
        d(caVar, i);
        i(jm());
        ja();
    }

    public void c(Object[] objArr) {
        this.DA = objArr;
    }

    public n ch() {
        return this.p;
    }

    public final void close() {
        if (this.BW != null) {
            this.BW.Ev();
            Thread.yield();
        }
        this.BW = null;
        ji();
        jc();
        this.mR = null;
        this.BK = null;
        this.BR = null;
        this.BN = null;
        this.BO = null;
        this.BS = null;
        this.BT = null;
        this.BZ = null;
        this.BX = 2;
        if (this.BM != null) {
            this.BM.close();
            this.BM = null;
        }
        if (!(this.BL == null || this.BL.BM == null)) {
            ca jj = this.BL.jj();
            if (jj != null) {
                jj.iH(1);
            }
            this.BL.BM = null;
            this.BL = null;
        }
        this.Cd = null;
        this.Ce = null;
    }

    public void d(int i, int i2, ca caVar) {
        switch (i) {
            case -1021:
                if (jv()) {
                    this.mR.f((byte) DT);
                    return;
                }
                return;
            case -1020:
                if (jv()) {
                    this.mR.f((byte) DU);
                    this.mR.Hc();
                    return;
                }
                return;
            case -1019:
                this.mR.f((byte) 37);
                return;
            case -1018:
                this.mR.f((byte) 53);
                return;
            case -1017:
                this.mR.f((byte) 45);
                return;
            case -1016:
                this.mR.f((byte) 34);
                return;
            case -1015:
                this.mR.f((byte) 33);
                return;
            case -1014:
                if (this.BN == this.mR.Lt || (this.Cc != null && this.BN == this.Cc.bjJ)) {
                    a(this.BN);
                    return;
                }
                return;
            case -1013:
                this.mR.f((byte) -100);
                return;
            case -1012:
                if (jv()) {
                    this.mR.mh();
                    return;
                }
                return;
            case -1011:
                if (jv()) {
                    this.mR.mD();
                    return;
                }
                return;
            case -1010:
                caVar.cc();
                this.mR.Hc();
                return;
            case -1009:
                if (jv()) {
                    this.mR.f((byte) 30);
                    return;
                }
                return;
            case -1008:
                if (jv()) {
                    this.mR.f((byte) 44);
                    return;
                }
                return;
            case -1007:
                this.mR.f((byte) 24);
                return;
            case -1006:
                if (jv()) {
                    this.mR.f((byte) 5);
                    return;
                }
                return;
            case -1005:
                if (jv()) {
                    this.mR.f((byte) 13);
                    return;
                }
                return;
            case -1004:
                this.mR.f((byte) 22);
                return;
            case -1003:
                if (jv()) {
                    this.mR.f((byte) 4);
                    return;
                }
                return;
            case MenuLayout.t:
                if (jv()) {
                    this.mR.f((byte) 28);
                    return;
                }
                return;
            case MenuLayout.s:
                if (jv()) {
                    this.mR.f((byte) 2);
                    return;
                }
                return;
            case -11:
                if (dx() == this.mR.Lz && i2 == 2) {
                    this.mR.f((byte) 12);
                    return;
                }
                return;
            case -8:
                this.mR.f((byte) 24);
                return;
            case -7:
                a(dx());
                return;
            case d.aaJ:
                a(dw());
                return;
            case 35:
                this.mR.f((byte) ca.bKP);
                return;
            case 42:
                if (jv()) {
                    this.mR.f((byte) 14);
                    return;
                }
                return;
            case 48:
                if (jv()) {
                    ju();
                    return;
                }
                return;
            case 49:
                this.mR.f((byte) ca.bGO);
                return;
            case 51:
                if (!jw()) {
                    this.mR.f((byte) 21);
                    return;
                }
                return;
            case 55:
                if (I(true)) {
                    this.mR.f((byte) 12);
                    return;
                }
                return;
            case 57:
                if (I(true)) {
                    this.mR.f((byte) 11);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public final void d(int i, int i2, int[] iArr) {
        if (i2 == 1) {
            this.Cw = true;
        }
        ca jm = this.CE == 1 ? this.CG : jm();
        bn iT = iT();
        if (iT != null && iT.biv == 1) {
            iT.bd(i);
            iArr[0] = 0;
        } else if (this.BR != null && this.BR.biv == 1) {
            this.BR.bd(i);
            iArr[0] = 0;
        } else if (this.CE != 1 || i == 1 || i == 6 || i == 2 || i == 5 || i == -6 || i == -7 || i == 8 || i == 50 || i == 52 || i == 53 || i == 54 || i == 56) {
            d(i, i2, jm);
            a(i, i2, jm, iArr);
            w.Lj = true;
            w.ns().PH();
        } else {
            iArr[0] = 0;
        }
    }

    public void d(int i, boolean z) {
        this.BV = i;
        if (!z) {
            try {
                if (this.BS.size() <= 2) {
                    return;
                }
            } catch (Exception e) {
                return;
            }
        }
        this.mR.lE();
    }

    public final void d(bf bfVar, bf bfVar2) {
        this.BN = null;
        this.BO = null;
        this.BN = bfVar;
        this.BO = bfVar2;
        if ((this.BO == null || this.BO == this.mR.Lz || this.BO == this.mR.Lv || this.BO == this.mR.LD) && this.BY == 0 && bfVar == this.mR.Lt) {
            ca jl = jl();
            if (jl != null && jl.Lg()) {
                this.BO = this.mR.Lv;
            } else if (jl != null && jl.ac() && jl.LJ()) {
                this.BO = this.mR.LD;
            } else if (bc(-1) != -1) {
                this.BO = this.mR.Lz;
            } else {
                this.BO = null;
            }
        }
    }

    public final void d(bk bkVar) {
        ca jl = jl();
        if (jl != null && jl.Lg()) {
            bkVar.save();
            bkVar.Du();
            try {
                a(bkVar, jl, w.MO, w.MP, w.MK, w.MQ);
            } catch (Exception e) {
            } finally {
                bkVar.Dv();
                bkVar.reset();
            }
        }
    }

    public final void d(ca caVar, int i) {
        int i2;
        int i3;
        if (caVar != null && i <= caVar.bHu.size() - 1) {
            caVar.bHv = (byte) i;
            byte b2 = caVar.bHw;
            if (b2 >= 0 && caVar.Lh() && caVar.bHu.size() > 5) {
                int length = this.mR.Kw.length;
                int i4 = 0;
                int i5 = -1;
                int i6 = -1;
                while (true) {
                    if (i4 >= length) {
                        i2 = i5;
                        i3 = i6;
                        break;
                    }
                    if (this.mR.Kw[i4] == i) {
                        i5 = i4;
                    } else if (this.mR.Kw[i4] == b2) {
                        i6 = i4;
                    }
                    if (i6 != -1 && i5 != -1) {
                        i2 = i5;
                        i3 = i6;
                        break;
                    }
                    i4++;
                }
                if (i2 < i3) {
                    caVar.bHw = this.mR.Kw[i2];
                } else if (i2 - 4 > i3) {
                    caVar.bHw = this.mR.Kw[i2 - 4];
                }
            }
        }
    }

    public void d(Object[] objArr) {
        this.DB = objArr;
    }

    public final bf dw() {
        return this.CE != 1 ? iU() : this.CI;
    }

    public final bf dx() {
        return this.CE != 1 ? iV() : this.CJ;
    }

    public final int e(int i, boolean z) {
        int size = this.BS.size();
        if (this.BV == -1) {
            return -1;
        }
        if (this.BV == 0 && i == -1) {
            return -1;
        }
        if (this.BV == size - 1 && i == 1) {
            return -1;
        }
        int i2 = this.BV + i;
        if (i2 > size - 1) {
            i2 = 0;
        }
        int i3 = i2 < 0 ? size - 1 : i2;
        ca aZ = aZ(i3);
        if (aZ == null || (z && aZ.Lr())) {
            return -1;
        }
        return i3;
    }

    public final void e(int i, String str) {
        String str2;
        String[] strArr;
        switch ((short) i) {
            case 32:
                str2 = ar.avQ[10];
                break;
            default:
                str2 = "";
                break;
        }
        if (str == null) {
            strArr = new String[]{str2};
        } else {
            strArr = new String[]{str2, str};
        }
        g(strArr);
    }

    public void e(bk bkVar) {
        ca jm;
        int i;
        int i2;
        if (w.Lj && (jm = jm()) != null) {
            jm.bGR = jm.bGS;
            if (jm.LM() && jm.bGR != 104 && jm.bGR != -1) {
                f gb = az.gb(jm.bGR);
                if (jm.ac()) {
                    i = jm.jl(jm.bGI - jm.bGu) - 6;
                    i2 = jm.jl(jm.bGJ - jm.bGv) - 2;
                } else {
                    i = jm.bGI - 6;
                    i2 = jm.bGJ - 2;
                }
                bkVar.a(gb, i, i2, 20);
            }
        }
    }

    public final void e(ca caVar, int i) {
        if (this.p == null || !this.p.Er()) {
        }
    }

    public final int f(ca caVar) {
        int size = this.BS.size();
        for (int i = 0; i < size; i++) {
            if (aZ(i) == caVar) {
                return i;
            }
        }
        return -1;
    }

    public final ca g(ca caVar) {
        if (caVar == null || caVar.bHu == null || caVar.bHu.isEmpty() || caVar.bHs != 1) {
            return caVar;
        }
        byte b2 = caVar.bHv;
        if (b2 < 0 || b2 >= caVar.bHu.size()) {
            b2 = 0;
        }
        return (ca) ((Object[]) caVar.bHu.elementAt(b2))[2];
    }

    public final void g(String[] strArr) {
        a(strArr, 1, ar.avM[70]);
    }

    public final ca h(ca caVar) {
        return caVar;
    }

    public final void h(String[] strArr) {
        a(strArr, 0, ar.avM[29]);
    }

    public final boolean i(ca caVar) {
        ca jm = caVar == null ? jm() : caVar;
        if (jm.bHQ == null || jm.bHQ.size() == 0) {
            return false;
        }
        String str = (String) jm.bHQ.elementAt(0);
        if (str == null || !str.contains(w.QZ) || jm == null) {
            this.mR.a(w.PB + str, 0, this);
        } else {
            jm.fu(str);
        }
        return true;
    }

    public final bn iT() {
        ca jm = jm();
        return (jm == null || !jm.Li()) ? this.BK : jm.bHW.bjL;
    }

    public final bf iU() {
        ca jm = jm();
        return (!jm.Li() || js()) ? this.BN : jm.bHW.bjP;
    }

    public final bf iV() {
        ca jm = jm();
        return (!jm.Li() || js()) ? this.BO : jm.bHW.bjQ;
    }

    public final r iW() {
        if (this.BM == null) {
            return null;
        }
        r iW = this.BM.iW();
        return iW == null ? this.BM : iW;
    }

    public final boolean iX() {
        return this.BL != null;
    }

    public final boolean iY() {
        return this.BM != null;
    }

    public final void iZ() {
        if (true == this.Du) {
            jH();
        }
        try {
            aY(0);
        } catch (Throwable th) {
        }
    }

    public final void j(ca caVar) {
        this.mR.w(caVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.a(byte, boolean):boolean
     arg types: [byte, int]
     candidates:
      com.uc.c.r.a(com.uc.c.r, com.uc.c.bu):com.uc.c.bu
      com.uc.c.r.a(int, com.uc.c.ca):void
      com.uc.c.r.a(com.uc.c.ca, java.lang.String):void
      com.uc.c.r.a(com.uc.c.ca, boolean):void
      com.uc.c.r.a(byte, boolean):boolean */
    public final boolean j(byte b2) {
        ca jj;
        if (jw()) {
            return false;
        }
        boolean a2 = a(b2, true);
        if (this.p == null || (jj = jj()) == null) {
            return a2;
        }
        try {
            String title = jj.getTitle();
            if (title == null || title.length() == 0) {
                title = "UC浏览器";
            }
            if (!this.p.bL(title)) {
            }
            return a2;
        } catch (Exception e) {
            return a2;
        }
    }

    public void jA() {
        bf(30);
    }

    public void jB() {
        if (this.mR != null) {
            this.mR.ku();
        }
    }

    public void jC() {
        if (this.mR != null && this.mR.lA() == this) {
            bf(2);
        }
    }

    public void jD() {
        bf(4);
    }

    public void jE() {
        this.Dm = false;
        this.Do = 0;
        this.Dp = 0;
        this.Dq = 0;
        this.Dt = null;
        this.Ds = 0;
    }

    public void jF() {
        double d;
        ca jj = jj();
        if (!this.Dm || jj == null || jj != this.Dt || jj.ahU <= w.MN) {
            T();
            return;
        }
        long currentTimeMillis = System.currentTimeMillis();
        if (this.Dp != 0) {
            d = ((double) (currentTimeMillis - this.Dp)) / 1000.0d;
        } else {
            this.Dp = currentTimeMillis - 50;
            d = 0.05d;
        }
        int i = (int) (((double) this.Do) + (((double) this.Ds) * d));
        if ((this.Do <= 0 || i > 0) && (this.Do >= 0 || i < 0)) {
            int i2 = (int) ((d * 0.5d * ((double) this.Ds) * d) + (((double) this.Do) * d));
            int i3 = i2 - this.Dq;
            this.Dq = i2;
            if ((this.Do >= 0 && i3 <= 0) || (this.Do <= 0 && i3 >= 0)) {
                T();
            } else if (!a(jj, 0, i3 * -1)) {
                T();
            }
        } else {
            T();
        }
    }

    public void jG() {
        if (this.Dv == null) {
            this.Dv = new Timer();
            this.Dv.schedule(new v(this), (long) Dw, (long) Dw);
            this.Du = true;
        }
    }

    public void jH() {
        if (this.Dv != null) {
            this.Dv.cancel();
            this.Dv = null;
            this.Du = false;
        }
    }

    public boolean jI() {
        return this.Dx;
    }

    public Object[] jJ() {
        return this.DA;
    }

    public Object[] jK() {
        return this.DB;
    }

    public String jL() {
        return this.Dz;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bc.b(com.uc.c.ca, boolean):boolean
     arg types: [com.uc.c.ca, int]
     candidates:
      com.uc.c.bc.b(int, char):int
      com.uc.c.bc.b(java.lang.StringBuffer, java.lang.String):void
      com.uc.c.bc.b(byte[], byte):void
      com.uc.c.bc.b(com.uc.c.ca, boolean):boolean */
    public boolean jM() {
        if (bc.Cp()) {
            this.mR.bW(512000);
        }
        ca jj = jj();
        if (jj == null || !bc.aL(jj.bFP, 2)) {
            return false;
        }
        if (!bc.b(jj, true)) {
            if (ch() != null) {
                ch().EC();
            } else {
                this.mR.a(this, jj);
            }
        }
        jj.bFP = (byte) bc.aJ(jj.bFP, 2);
        return true;
    }

    public boolean jN() {
        return this.BY > 0;
    }

    public boolean jO() {
        return jN();
    }

    public final void ja() {
        b(iU(), iV());
    }

    public final void jc() {
        if (this.BT != null && !this.BT.isEmpty()) {
            Enumeration elements = this.BT.elements();
            while (elements.hasMoreElements()) {
                ((ca) elements.nextElement()).Lp();
            }
            this.BT.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.a(int, int, int, int, boolean):boolean
     arg types: [int, int, int, int, int]
     candidates:
      com.uc.c.r.a(com.uc.c.ca, int, int, java.lang.Object, int):b.a.a.a.f
      com.uc.c.r.a(com.uc.c.ca, int, int, int, java.lang.Object):void
      com.uc.c.r.a(com.uc.c.bk, int, int, int, int):void
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int, int):void
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, boolean, int[], boolean):void
      com.uc.c.r.a(int, int, int, int, boolean):boolean */
    public final void jd() {
        a(-1, 1, 0, -1, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.a(int, int, int, int, boolean):boolean
     arg types: [int, int, int, int, int]
     candidates:
      com.uc.c.r.a(com.uc.c.ca, int, int, java.lang.Object, int):b.a.a.a.f
      com.uc.c.r.a(com.uc.c.ca, int, int, int, java.lang.Object):void
      com.uc.c.r.a(com.uc.c.bk, int, int, int, int):void
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int, int):void
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, boolean, int[], boolean):void
      com.uc.c.r.a(int, int, int, int, boolean):boolean */
    public final void je() {
        a(-1, 8, 0, -1, true);
    }

    public final boolean jf() {
        ca jm = jm();
        return (jm == null || jm.bGx == jm.bGv) ? false : true;
    }

    public final boolean jg() {
        return (this.Cf == null && this.Cg == null) ? false : true;
    }

    public final void ji() {
        int size = this.BS.size();
        for (int i = 0; i < size; i++) {
            ca aZ = aZ(i);
            if (aZ != null) {
                bc.R(aZ);
            }
        }
        this.BS.removeAllElements();
    }

    public ca jj() {
        return jm();
    }

    public final boolean jk() {
        try {
            ca jm = jm();
            return jm.LD() && jm.Lw() && !jr();
        } catch (Exception e) {
            return false;
        }
    }

    public ca jl() {
        try {
            return aZ(this.BV);
        } catch (Exception e) {
            return null;
        }
    }

    public ca jm() {
        return g(jl());
    }

    public void jn() {
        while (true) {
            synchronized (CR) {
                try {
                    CR.wait();
                } catch (InterruptedException e) {
                }
            }
            w.Lp = System.currentTimeMillis();
            w.Ln = false;
            while (true) {
                long currentTimeMillis = System.currentTimeMillis() - w.Lp;
                if (currentTimeMillis > 200) {
                    w.Lm = true;
                    w.ns().lA().jC();
                }
                bc.m(50);
                if (!w.Ln) {
                    if (w.Lq >= 8 && currentTimeMillis >= 600) {
                        jo();
                        break;
                    }
                } else {
                    jo();
                    break;
                }
            }
        }
    }

    public void jo() {
        w.Lm = false;
        w.Lq = 0;
        w.Lp = Long.MAX_VALUE;
        jC();
    }

    public void jq() {
        ca jm = jm();
        if (Math.abs(w.Rh) < 500 && jm != null && !jm.ac() && jm.bGm <= jm.bGr) {
            m.dq().a(this);
        }
    }

    public final boolean jr() {
        return this.BX == 1 || this.BX == 3;
    }

    public final boolean js() {
        return this.BX == 1;
    }

    public final boolean jt() {
        return this.BW != null && this.BW.Hk();
    }

    public final void ju() {
        this.mR.f((byte) 1);
    }

    public final boolean jv() {
        return I(false);
    }

    /* access modifiers changed from: package-private */
    public final boolean jw() {
        return this.BY > 0;
    }

    public final void jx() {
    }

    public final void jy() {
        J(false);
    }

    public final void jz() {
        bf(12);
    }

    public ca k(byte b2) {
        return aZ(bc(b2));
    }

    public void k(ca caVar) {
        ca jj = caVar == null ? jj() : caVar;
        if (jj != null && ch() != null) {
            ch().Ez();
            Collection<Plugin> LS = jj.LS();
            if (LS != null) {
                for (Plugin a2 : LS) {
                    ch().a(a2);
                }
            }
        }
    }

    public void l(ca caVar) {
        if (caVar != null && caVar.bFR != null) {
            caVar.bGX = caVar.bGw;
            caVar.bGY = caVar.bGx;
        }
    }

    public void n(ca caVar) {
        if (caVar != null && caVar.bGY >= 0) {
            caVar.f(caVar.bGX, caVar.bGY);
            this.mR.Hc();
            caVar.bGY = -1;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.a(com.uc.c.ca, boolean):void
     arg types: [com.uc.c.ca, int]
     candidates:
      com.uc.c.r.a(com.uc.c.r, com.uc.c.bu):com.uc.c.bu
      com.uc.c.r.a(int, com.uc.c.ca):void
      com.uc.c.r.a(com.uc.c.ca, java.lang.String):void
      com.uc.c.r.a(byte, boolean):boolean
      com.uc.c.r.a(com.uc.c.ca, boolean):void */
    public void o(ca caVar) {
        a(caVar, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.a(int, int, int, int, boolean):boolean
     arg types: [int, int, int, int, int]
     candidates:
      com.uc.c.r.a(com.uc.c.ca, int, int, java.lang.Object, int):b.a.a.a.f
      com.uc.c.r.a(com.uc.c.ca, int, int, int, java.lang.Object):void
      com.uc.c.r.a(com.uc.c.bk, int, int, int, int):void
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int, int):void
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, boolean, int[], boolean):void
      com.uc.c.r.a(int, int, int, int, boolean):boolean */
    public final boolean z(int i, int i2) {
        this.Cf = null;
        this.Cg = null;
        return a(i, i2, 0, -1, false);
    }
}
