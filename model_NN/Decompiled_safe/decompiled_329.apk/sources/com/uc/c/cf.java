package com.uc.c;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class cf extends ByteArrayOutputStream {
    byte abU;
    OutputStream bWY = null;

    public cf() {
    }

    public cf(int i) {
        super(i);
    }

    public cf(OutputStream outputStream, byte b2) {
        this.bWY = outputStream;
        this.abU = b2;
    }

    private synchronized void Of() {
        if (this.count >= 10240) {
            this.bWY.write(this.buf, 0, this.count);
            this.count = 0;
        }
    }

    private void kb(int i) {
        byte[] bArr;
        try {
            bArr = new byte[i];
        } catch (OutOfMemoryError e) {
            bArr = new byte[(this.buf.length + 1024)];
        }
        if (i > bArr.length) {
            throw new OutOfMemoryError();
        }
        System.arraycopy(this.buf, 0, bArr, 0, this.count);
        this.buf = bArr;
    }

    public final byte[] Oe() {
        return this.buf;
    }

    public void Og() {
        if (this.count > 0) {
            this.bWY.write(this.buf, 0, this.count);
            this.count = 0;
        }
    }

    public void write(int i) {
        if (this.count + 1 > this.buf.length) {
            int length = this.buf.length << 1;
            if (length < this.count + 1) {
                length = this.count + 1;
            }
            kb(length);
        }
        this.buf[this.count] = (byte) i;
        this.count++;
        if (this.abU == 1 && this.bWY != null && this.count >= 10240) {
            try {
                this.bWY.write(this.buf, 0, this.count);
                this.count = 0;
            } catch (IOException e) {
            }
        }
    }

    public void write(byte[] bArr, int i, int i2) {
        if (i < 0 || i > bArr.length || i2 < 0 || i + i2 > bArr.length || i + i2 < 0) {
            throw new IndexOutOfBoundsException();
        } else if (i2 != 0) {
            if (this.count + i2 > this.buf.length) {
                int length = this.buf.length << 1;
                if (length < this.count + i2) {
                    length = this.count + i2;
                }
                kb(length);
            }
            System.arraycopy(bArr, i, this.buf, this.count, i2);
            this.count += i2;
            if (this.abU == 1 && this.bWY != null && this.count >= 10240) {
                try {
                    this.bWY.write(this.buf, 0, this.count);
                    this.count = 0;
                } catch (IOException e) {
                }
            }
        }
    }
}
