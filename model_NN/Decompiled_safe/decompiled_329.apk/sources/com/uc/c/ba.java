package com.uc.c;

import java.util.LinkedList;

class ba {
    final /* synthetic */ am ZR;
    LinkedList bdq = null;
    LinkedList bdr = null;
    int bds = 0;

    public ba(am amVar, int i) {
        this.ZR = amVar;
        this.bds = i;
        this.bdq = new LinkedList();
        this.bdr = new LinkedList();
    }

    private final void BL() {
        this.bdq.removeFirst();
        this.bdr.removeFirst();
    }

    private final void V(String str, String str2) {
        this.bdq.addLast(str);
        this.bdr.addLast(str2);
    }

    private final int dB(String str) {
        int size = this.bdq.size();
        for (int i = 0; i < size; i++) {
            if (((String) this.bdq.get(i)).equals(str)) {
                return i;
            }
        }
        return -1;
    }

    private final void o(int i, String str) {
        this.bdr.set(i, str);
    }

    public String get(String str) {
        if (str == null) {
            return "";
        }
        int dB = dB(str);
        return dB >= 0 ? (String) this.bdr.get(dB) : "";
    }

    public void set(String str, String str2) {
        if (str != null && str2 != null) {
            int dB = dB(str);
            if (dB >= 0) {
                o(dB, str2);
                return;
            }
            if (this.bdq.size() >= this.bds) {
                BL();
            }
            V(str, str2);
        }
    }
}
