package com.uc.c;

import b.a.a.e;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

public class ab {
    public static final int SUCCESS = 0;
    private static final String VG = "ucrmdata.dat";
    private static final int VH = 20;
    private static ab VI = null;
    public static final int VJ = -1;
    public static final int VK = -2;
    private int VL = 0;
    private Hashtable VM = new Hashtable();
    private ac VN = null;

    private ab() {
    }

    public static ab sl() {
        if (VI == null) {
            synchronized (ab.class) {
                if (VI == null) {
                    VI = new ab();
                }
            }
        }
        return VI;
    }

    public int a(String str, String str2, int i, Object[] objArr) {
        ac acVar;
        boolean z;
        int i2;
        ac acVar2;
        Iterator it = this.VM.entrySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                acVar = null;
                z = false;
                break;
            }
            acVar = (ac) ((Map.Entry) it.next()).getValue();
            if (acVar.VW != null && acVar.VW.endsWith(str)) {
                z = true;
                break;
            }
        }
        if (!z) {
            if (this.VM.size() >= 20) {
                int i3 = Integer.MAX_VALUE;
                ac acVar3 = acVar;
                for (Map.Entry value : this.VM.entrySet()) {
                    ac acVar4 = (ac) value.getValue();
                    if (acVar4.VX < i3) {
                        acVar2 = acVar4;
                        i2 = acVar4.VX;
                    } else {
                        i2 = i3;
                        acVar2 = acVar3;
                    }
                    acVar3 = acVar2;
                    i3 = i2;
                }
                acVar = acVar3;
            } else {
                acVar = new ac();
            }
        }
        if (!z) {
            int i4 = this.VL + 1;
            this.VL = i4;
            acVar.VS = i4;
        }
        acVar.VV = str2;
        acVar.VW = str;
        acVar.VT = i;
        acVar.VU = i;
        acVar.VY = System.currentTimeMillis();
        synchronized (this.VM) {
            this.VM.put(Integer.valueOf(acVar.VS), acVar);
        }
        this.VN = acVar;
        return z ? acVar.VS : this.VL;
    }

    public int a(Object[] objArr, boolean z) {
        int intValue = ((Integer) objArr[0]).intValue();
        if (this.VN == null || this.VN.VS != intValue) {
            this.VN = (ac) this.VM.get(Integer.valueOf(intValue));
        }
        if (this.VN == null) {
            this.VN = new ac();
            this.VN.VS = intValue;
            synchronized (this.VM) {
                this.VM.put(Integer.valueOf(this.VN.VS), this.VN);
            }
        }
        this.VN.VU = ((Integer) objArr[3]).intValue();
        this.VN.VW = (String) objArr[1];
        this.VN.VV = (String) objArr[2];
        this.VN.VY = System.currentTimeMillis();
        if (z) {
            this.VN.VX++;
        }
        return 0;
    }

    public void clear() {
        this.VM.clear();
        this.VN = null;
        so();
    }

    public int ea(int i) {
        return ((ac) this.VM.remove(Integer.valueOf(i))) == null ? -2 : 0;
    }

    public int g(Object[] objArr) {
        return a(objArr, true);
    }

    public Vector sm() {
        if (this.VM == null) {
            return null;
        }
        Vector vector = new Vector();
        for (Map.Entry value : this.VM.entrySet()) {
            ac acVar = (ac) value.getValue();
            e eVar = new e();
            eVar.abL = acVar.VW;
            eVar.abM = acVar.VV;
            eVar.abU = 6;
            eVar.time = (long) acVar.VX;
            eVar.abQ = acVar.VS;
            eVar.abS = 1;
            vector.add(eVar);
            Collections.sort(vector, new af(this));
        }
        return vector;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:19:0x0060=Splitter:B:19:0x0060, B:43:0x0085=Splitter:B:43:0x0085} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void sn() {
        /*
            r9 = this;
            java.util.Hashtable r0 = r9.VM
            monitor-enter(r0)
            java.lang.String r1 = "ucrmdata.dat"
            java.io.InputStream r1 = b.a.cs(r1)     // Catch:{ all -> 0x0086 }
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ all -> 0x0086 }
            r2.<init>(r1)     // Catch:{ all -> 0x0086 }
            java.io.DataInputStream r3 = new java.io.DataInputStream     // Catch:{ all -> 0x0086 }
            r3.<init>(r2)     // Catch:{ all -> 0x0086 }
            int r4 = r3.readInt()     // Catch:{ IOException -> 0x0062, all -> 0x0075 }
            r9.VL = r4     // Catch:{ IOException -> 0x0062, all -> 0x0075 }
            int r4 = r3.readInt()     // Catch:{ IOException -> 0x0062, all -> 0x0075 }
            r5 = 0
        L_0x001e:
            if (r5 >= r4) goto L_0x0051
            com.uc.c.ac r6 = new com.uc.c.ac     // Catch:{ IOException -> 0x0062, all -> 0x0075 }
            r6.<init>()     // Catch:{ IOException -> 0x0062, all -> 0x0075 }
            int r7 = r3.readInt()     // Catch:{ IOException -> 0x0062, all -> 0x0075 }
            r6.VS = r7     // Catch:{ IOException -> 0x0062, all -> 0x0075 }
            int r7 = r3.readInt()     // Catch:{ IOException -> 0x0062, all -> 0x0075 }
            r6.VU = r7     // Catch:{ IOException -> 0x0062, all -> 0x0075 }
            java.lang.String r7 = r3.readUTF()     // Catch:{ IOException -> 0x0062, all -> 0x0075 }
            r6.VV = r7     // Catch:{ IOException -> 0x0062, all -> 0x0075 }
            java.lang.String r7 = r3.readUTF()     // Catch:{ IOException -> 0x0062, all -> 0x0075 }
            r6.VW = r7     // Catch:{ IOException -> 0x0062, all -> 0x0075 }
            long r7 = r3.readLong()     // Catch:{ IOException -> 0x0062, all -> 0x0075 }
            r6.VY = r7     // Catch:{ IOException -> 0x0062, all -> 0x0075 }
            java.util.Hashtable r7 = r9.VM     // Catch:{ IOException -> 0x0062, all -> 0x0075 }
            int r8 = r6.VS     // Catch:{ IOException -> 0x0062, all -> 0x0075 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ IOException -> 0x0062, all -> 0x0075 }
            r7.put(r8, r6)     // Catch:{ IOException -> 0x0062, all -> 0x0075 }
            int r5 = r5 + 1
            goto L_0x001e
        L_0x0051:
            if (r3 == 0) goto L_0x0056
            r3.close()     // Catch:{ IOException -> 0x0089 }
        L_0x0056:
            if (r2 == 0) goto L_0x005b
            r2.close()     // Catch:{ IOException -> 0x008b }
        L_0x005b:
            if (r1 == 0) goto L_0x0060
            r1.close()     // Catch:{ IOException -> 0x008d }
        L_0x0060:
            monitor-exit(r0)     // Catch:{ all -> 0x0086 }
            return
        L_0x0062:
            r4 = move-exception
            if (r3 == 0) goto L_0x0068
            r3.close()     // Catch:{ IOException -> 0x008f }
        L_0x0068:
            if (r2 == 0) goto L_0x006d
            r2.close()     // Catch:{ IOException -> 0x0091 }
        L_0x006d:
            if (r1 == 0) goto L_0x0060
            r1.close()     // Catch:{ IOException -> 0x0073 }
            goto L_0x0060
        L_0x0073:
            r1 = move-exception
            goto L_0x0060
        L_0x0075:
            r4 = move-exception
            if (r3 == 0) goto L_0x007b
            r3.close()     // Catch:{ IOException -> 0x0093 }
        L_0x007b:
            if (r2 == 0) goto L_0x0080
            r2.close()     // Catch:{ IOException -> 0x0095 }
        L_0x0080:
            if (r1 == 0) goto L_0x0085
            r1.close()     // Catch:{ IOException -> 0x0097 }
        L_0x0085:
            throw r4     // Catch:{ all -> 0x0086 }
        L_0x0086:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0086 }
            throw r1
        L_0x0089:
            r3 = move-exception
            goto L_0x0056
        L_0x008b:
            r2 = move-exception
            goto L_0x005b
        L_0x008d:
            r1 = move-exception
            goto L_0x0060
        L_0x008f:
            r3 = move-exception
            goto L_0x0068
        L_0x0091:
            r2 = move-exception
            goto L_0x006d
        L_0x0093:
            r3 = move-exception
            goto L_0x007b
        L_0x0095:
            r2 = move-exception
            goto L_0x0080
        L_0x0097:
            r1 = move-exception
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.ab.sn():void");
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:54:0x00a2=Splitter:B:54:0x00a2, B:28:0x0078=Splitter:B:28:0x0078} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void so() {
        /*
            r8 = this;
            java.lang.String r7 = ""
            java.util.Hashtable r0 = r8.VM
            monitor-enter(r0)
            java.lang.String r1 = "ucrmdata.dat"
            r2 = 0
            java.io.OutputStream r1 = b.a.l(r1, r2)     // Catch:{ all -> 0x00a3 }
            java.io.BufferedOutputStream r2 = new java.io.BufferedOutputStream     // Catch:{ all -> 0x00a3 }
            r2.<init>(r1)     // Catch:{ all -> 0x00a3 }
            java.io.DataOutputStream r3 = new java.io.DataOutputStream     // Catch:{ all -> 0x00a3 }
            r3.<init>(r2)     // Catch:{ all -> 0x00a3 }
            int r4 = r8.VL     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            r3.writeInt(r4)     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            java.util.Hashtable r4 = r8.VM     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            if (r4 == 0) goto L_0x0080
            java.util.Hashtable r4 = r8.VM     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            int r4 = r4.size()     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            r3.writeInt(r4)     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            java.util.Hashtable r4 = r8.VM     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            java.util.Set r4 = r4.entrySet()     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
        L_0x0032:
            boolean r5 = r4.hasNext()     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            if (r5 == 0) goto L_0x0080
            java.lang.Object r8 = r4.next()     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            java.util.Map$Entry r8 = (java.util.Map.Entry) r8     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            java.lang.Object r8 = r8.getValue()     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            com.uc.c.ac r8 = (com.uc.c.ac) r8     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            int r5 = r8.VS     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            r3.writeInt(r5)     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            int r5 = r8.VU     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            r3.writeInt(r5)     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            java.lang.String r5 = r8.VV     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            if (r5 != 0) goto L_0x007a
            java.lang.String r5 = ""
            r5 = r7
        L_0x0055:
            r3.writeUTF(r5)     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            java.lang.String r5 = r8.VW     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            if (r5 != 0) goto L_0x007d
            java.lang.String r5 = ""
            r5 = r7
        L_0x005f:
            r3.writeUTF(r5)     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            long r5 = r8.VY     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            r3.writeLong(r5)     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            goto L_0x0032
        L_0x0068:
            r4 = move-exception
            if (r3 == 0) goto L_0x006e
            r3.close()     // Catch:{ IOException -> 0x00aa }
        L_0x006e:
            if (r2 == 0) goto L_0x0073
            r2.close()     // Catch:{ IOException -> 0x00ac }
        L_0x0073:
            if (r1 == 0) goto L_0x0078
            r1.close()     // Catch:{ IOException -> 0x00ae }
        L_0x0078:
            monitor-exit(r0)     // Catch:{ all -> 0x00a3 }
            return
        L_0x007a:
            java.lang.String r5 = r8.VV     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            goto L_0x0055
        L_0x007d:
            java.lang.String r5 = r8.VW     // Catch:{ IOException -> 0x0068, all -> 0x0092 }
            goto L_0x005f
        L_0x0080:
            if (r3 == 0) goto L_0x0085
            r3.close()     // Catch:{ IOException -> 0x00a6 }
        L_0x0085:
            if (r2 == 0) goto L_0x008a
            r2.close()     // Catch:{ IOException -> 0x00a8 }
        L_0x008a:
            if (r1 == 0) goto L_0x0078
            r1.close()     // Catch:{ IOException -> 0x0090 }
            goto L_0x0078
        L_0x0090:
            r1 = move-exception
            goto L_0x0078
        L_0x0092:
            r4 = move-exception
            if (r3 == 0) goto L_0x0098
            r3.close()     // Catch:{ IOException -> 0x00b0 }
        L_0x0098:
            if (r2 == 0) goto L_0x009d
            r2.close()     // Catch:{ IOException -> 0x00b2 }
        L_0x009d:
            if (r1 == 0) goto L_0x00a2
            r1.close()     // Catch:{ IOException -> 0x00b4 }
        L_0x00a2:
            throw r4     // Catch:{ all -> 0x00a3 }
        L_0x00a3:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00a3 }
            throw r1
        L_0x00a6:
            r3 = move-exception
            goto L_0x0085
        L_0x00a8:
            r2 = move-exception
            goto L_0x008a
        L_0x00aa:
            r3 = move-exception
            goto L_0x006e
        L_0x00ac:
            r2 = move-exception
            goto L_0x0073
        L_0x00ae:
            r1 = move-exception
            goto L_0x0078
        L_0x00b0:
            r3 = move-exception
            goto L_0x0098
        L_0x00b2:
            r2 = move-exception
            goto L_0x009d
        L_0x00b4:
            r1 = move-exception
            goto L_0x00a2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.ab.so():void");
    }
}
