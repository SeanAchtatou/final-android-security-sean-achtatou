package com.uc.c;

import b.a.a.a.g;
import b.a.a.a.l;
import b.a.a.a.m;
import b.a.a.a.u;
import b.a.a.a.w;
import b.a.a.a.z;
import java.util.Vector;

public class bm extends g implements z {
    public static final short bmA = -9999;
    public static final char bmB = ar.avM[12].charAt(0);
    public static Object[] bmJ = null;
    private static short[][] bmO = {new short[]{48, 1}, new short[]{-1001, 2}, new short[]{-1003, 4}, new short[]{-1006, 5}, new short[]{42, 14}, new short[]{57, 11}, new short[]{55, 12}, new short[]{-1005, 13}, new short[]{-1004, 22}, new short[]{-1007, 24}, new short[]{-1009, 30}, new short[]{-1015, 33}, new short[]{-1008, 44}, new short[]{-1017, 45}, new short[]{-1013, -100}, new short[]{49, 102}, new short[]{51, 21}};
    public static final short bmz = -1111;
    private w Rp;
    private l bmC;
    private int bmD = 0;
    private int bmE = 0;
    private int bmF = 0;
    private int bmG = 0;
    private boolean bmH = false;
    private long bmI;
    private boolean bmK = false;
    private int bmL = 0;
    private int bmM;
    private int bmN;

    public bm(l lVar, w wVar) {
    }

    public static void Ec() {
        if (bmJ == null) {
            Ee();
            Ed();
        }
    }

    private static boolean Ed() {
        return az.Bt();
    }

    public static void Ee() {
        Vector vector = new Vector();
        vector.addElement(new Object[]{ar.avX[0], new short[]{48, 48}});
        vector.addElement(new Object[]{ar.avX[1], new short[]{49, 49}});
        vector.addElement(new Object[]{ar.avX[2], new short[]{50, 50}});
        vector.addElement(new Object[]{ar.avX[3], new short[]{51, 51}});
        vector.addElement(new Object[]{ar.avX[4], new short[]{52, 52}});
        vector.addElement(new Object[]{ar.avX[5], new short[]{53, 53}});
        vector.addElement(new Object[]{ar.avX[6], new short[]{54, 54}});
        vector.addElement(new Object[]{ar.avX[7], new short[]{55, 55}});
        vector.addElement(new Object[]{ar.avX[8], new short[]{56, 56}});
        vector.addElement(new Object[]{ar.avX[9], new short[]{57, 57}});
        vector.addElement(new Object[]{ar.avX[10], new short[]{42, 42}});
        vector.addElement(new Object[]{ar.avX[11], new short[]{-1001, -1048}});
        vector.addElement(new Object[]{ar.avX[12], new short[]{-1002, -1049}});
        vector.addElement(new Object[]{ar.avX[13], new short[]{-1003, -1050}});
        vector.addElement(new Object[]{ar.avX[14], new short[]{-1004, -1051}});
        vector.addElement(new Object[]{ar.avX[15], new short[]{-1005, -1052}});
        vector.addElement(new Object[]{ar.avX[16], new short[]{-1006, -1053}});
        vector.addElement(new Object[]{ar.avX[17], new short[]{-1007, -1054}});
        vector.addElement(new Object[]{ar.avX[18], new short[]{-1008, -1055}});
        vector.addElement(new Object[]{ar.avX[19], new short[]{-1009, -1056}});
        vector.addElement(new Object[]{ar.avX[20], new short[]{-1010, -1057}});
        vector.addElement(new Object[]{ar.avX[21], new short[]{-1011, -1042}});
        vector.addElement(new Object[]{ar.avX[22], new short[]{-1012, -1035}});
        vector.addElement(new Object[]{ar.avX[23], new short[]{-1013, bmz}});
        vector.addElement(new Object[]{ar.avX[24], new short[]{-1014, bmz}});
        vector.addElement(new Object[]{ar.avX[25], new short[]{-1015, bmz}});
        vector.addElement(new Object[]{ar.avX[26], new short[]{-1016, bmz}});
        vector.addElement(new Object[]{ar.avX[27], new short[]{-1017, bmz}});
        vector.addElement(new Object[]{ar.avX[28], new short[]{-1018, bmz}});
        if (w.nh()) {
            vector.addElement(new Object[]{ar.avX[29], new short[]{-1019, bmz}});
        }
        vector.addElement(new Object[]{ar.avX[30], new short[]{-1020, bmz}});
        vector.addElement(new Object[]{ar.avX[31], new short[]{-1021, bmz}});
        bmJ = new Object[vector.size()];
        vector.copyInto(bmJ);
        vector.removeAllElements();
    }

    public static void Ef() {
        if (bmJ != null && bmJ.length > 0) {
            az.Bs();
            ar.mR.mv();
        }
    }

    public static void Eg() {
        Ee();
        Ef();
    }

    private void Eh() {
        if (this.bmD * this.bmG < this.bmE) {
            this.bmE = this.bmD * this.bmG;
        }
        if ((this.bmD * this.bmG) + this.bmG > getHeight() + this.bmE) {
            this.bmE = this.bmD * this.bmG;
        }
    }

    private void a(short s, int i) {
        if (bmJ != null && bmJ.length > 0 && i >= 0 && i <= bmJ.length - 1) {
            ((short[]) ((Object[]) bmJ[i])[1])[1] = s;
        }
    }

    private void bg(int i, int i2) {
        if (i >= 0 && i <= bmJ.length - 1 && i2 >= 0 && i2 <= bmJ.length - 1 && i != i2) {
            short hj = hj(i);
            a(hj(i2), i);
            a(hj, i2);
        }
    }

    public static char[] e(short s) {
        if (s == -1111) {
            return null;
        }
        if (s > 0) {
            return new char[]{(char) s};
        }
        return new char[]{'#', (char) (-(s + 1000))};
    }

    public static String f(short s) {
        boolean z;
        if (bmJ == null || bmJ.length <= 0) {
            return null;
        }
        int i = 0;
        while (true) {
            if (i >= bmJ.length) {
                z = false;
                break;
            } else if (s == ((short[]) ((Object[]) bmJ[i])[1])[1]) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (z) {
            return (String) ((Object[]) bmJ[i])[0];
        }
        return null;
    }

    public static char[] g(short s) {
        boolean z;
        char[] e;
        int i = 0;
        while (true) {
            if (i >= bmJ.length) {
                z = false;
                break;
            } else if (hk(i) == s) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (!z || (e = e(hj(i))) == null) {
            return null;
        }
        return e;
    }

    public static char[] h(short s) {
        short s2;
        boolean z;
        int i = 0;
        while (true) {
            if (i >= bmO.length) {
                s2 = 0;
                z = false;
                break;
            } else if (s == bmO[i][1]) {
                s2 = bmO[i][0];
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (!z) {
            return null;
        }
        return g(s2);
    }

    private static short hj(int i) {
        return (bmJ == null || bmJ.length <= 0) ? bmz : (i < 0 || i > bmJ.length - 1) ? bmz : ((short[]) ((Object[]) bmJ[i])[1])[1];
    }

    public static short hk(int i) {
        return (bmJ == null || bmJ.length <= 0) ? bmA : (i < 0 || i > bmJ.length - 1) ? bmA : ((short[]) ((Object[]) bmJ[i])[1])[0];
    }

    private void hl(int i) {
        if (i == -1) {
            int i2 = this.bmD - 1;
            this.bmD = i2;
            if (i2 < 0) {
                this.bmD = bmJ.length - 1;
            }
        } else if (i == 1) {
            int i3 = this.bmD + 1;
            this.bmD = i3;
            if (i3 > bmJ.length - 1) {
                this.bmD = 0;
            }
        }
    }

    private void hm(int i) {
        if (i == -1 || i == 1) {
            int i2 = this.bmE / this.bmG;
            int height = getHeight() / this.bmG;
            this.bmD = i == -1 ? i2 - height : i2 + height;
            if (this.bmD < 0) {
                this.bmD = (((bmJ.length * this.bmG) / getHeight()) * getHeight()) / this.bmG;
            } else if (this.bmD > bmJ.length - 1) {
                this.bmD = 0;
            }
            Eh();
            cJ();
        }
    }

    public static short l(int i, boolean z) {
        boolean z2;
        short s = z ? (short) ((-i) - 1000) : (short) i;
        int i2 = 0;
        while (true) {
            if (bmJ == null || i2 >= bmJ.length) {
                z2 = false;
            } else if (hj(i2) == s) {
                z2 = true;
                break;
            } else {
                i2++;
            }
        }
        z2 = false;
        return z2 ? hk(i2) : bmA;
    }

    /* access modifiers changed from: protected */
    public void L(int i, int i2) {
        this.bmL = i2;
    }

    /* access modifiers changed from: protected */
    public void M(int i, int i2) {
        if (!this.bmK || Math.abs(i2 - this.bmL) <= 15) {
            this.bmD = (i2 / this.bmG) + (this.bmE / this.bmG);
            if (this.bmD <= bmJ.length - 1) {
                if (this.bmD < 0) {
                    this.bmD = bmJ.length - 1;
                }
                Eh();
                cJ();
            } else {
                return;
            }
        } else {
            hm(i2 - this.bmL > 1 ? 1 : -1);
        }
        this.bmK = false;
    }

    /* access modifiers changed from: protected */
    public void N(int i, int i2) {
        this.bmK = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.w.b(java.lang.String, b.a.a.a.w):void
     arg types: [java.lang.String, com.uc.c.w]
     candidates:
      com.uc.c.w.b(java.lang.String, java.lang.Object):java.lang.Object
      com.uc.c.w.b(com.uc.c.r, java.lang.String[]):void
      com.uc.c.w.b(byte, java.lang.String):void
      com.uc.c.w.b(byte, boolean):void
      com.uc.c.w.b(int, com.uc.c.ca):void
      com.uc.c.w.b(com.uc.c.ca, com.uc.c.r):void
      com.uc.c.w.b(com.uc.c.ca, java.lang.String):void
      com.uc.c.w.b(com.uc.c.r, com.uc.c.ca):void
      com.uc.c.w.b(com.uc.c.r, java.lang.String):void
      com.uc.c.w.b(java.lang.Object[], java.lang.Object[]):void
      com.uc.c.w.b(java.lang.String, com.uc.c.r):boolean
      com.uc.c.w.b(java.lang.String, b.a.a.a.w):void */
    public void a(m mVar, w wVar) {
        if (mVar.wq() == 4) {
            Ef();
            this.Rp.b(ar.avO[3], (w) this.Rp);
        } else if (mVar.wq() == 5) {
            Eg();
            cJ();
        } else if (mVar.wq() == 3) {
            Ed();
            this.bmC.a(this.Rp);
        }
    }

    /* access modifiers changed from: protected */
    public void a(u uVar) {
        if (bmJ != null) {
            uVar.DF();
            uVar.setColor(15660280);
            uVar.o(0, 0, this.bmM, this.bmN);
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < bmJ.length) {
                    int i3 = this.bmG * i2;
                    if (i3 - 1 < this.bmE + getHeight()) {
                        int i4 = i3 - this.bmE;
                        int i5 = this.bmM;
                        int i6 = this.bmG + 1;
                        if (i2 == this.bmD) {
                            uVar.setColor(3105752);
                            uVar.o(0, i4 - 1, i5, i6 + 1);
                        }
                        char[] e = e(((short[]) ((Object[]) bmJ[i2])[1])[1]);
                        String str = (String) ((Object[]) bmJ[i2])[0];
                        int i7 = 0 + 2;
                        int i8 = i4 + 1;
                        int p = bc.p(this.bmF, "国国国");
                        int length = e != null ? e.length : 1;
                        int i9 = i7;
                        for (int i10 = 0; i10 < length; i10++) {
                            uVar.setColor(2764863);
                            int i11 = length < 2 ? p - 3 : ((p - 2) / 2) - 1;
                            uVar.o(i9, i8 + 1, i11, i6 - 5);
                            uVar.o(i9 + 1, i8, i11 - 2, i6 - 3);
                            uVar.setColor(16777215);
                            uVar.d(bc.gn(this.bmF));
                            if (e == null) {
                                uVar.b("空", (i11 / 2) + i9, i8, 17);
                            } else {
                                uVar.b(new String(e, i10, 1), (i11 / 2) + i9, i8, 17);
                            }
                            i9 += i11 + 1;
                        }
                        int i12 = p + 5;
                        if (i2 == this.bmD) {
                            uVar.setColor(16777215);
                        } else {
                            uVar.setColor(2764863);
                        }
                        uVar.b(" " + str, i12, i8, 20);
                    }
                    i = i2 + 1;
                } else {
                    uVar.DG();
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void bt(int i) {
        boolean z;
        boolean z2 = this.bmH;
        if (System.currentTimeMillis() - this.bmI > 50000) {
            this.bmH = false;
            z2 = false;
        }
        if (this.bmH || i != 35) {
            this.bmH = false;
            int bs = i <= 0 ? bs(i) : i;
            if (bs == 6) {
                hl(1);
            } else if (bs == 1) {
                hl(-1);
            } else if (bs == 2) {
                hm(-1);
            } else if (bs == 5) {
                hm(1);
            } else if (i > 0) {
                int i2 = z2 ? ((-i) - 1000) + 0 : i;
                int i3 = 0;
                while (true) {
                    if (i3 >= bmJ.length) {
                        z = false;
                        break;
                    } else if (hj(i3) == i2) {
                        z = true;
                        break;
                    } else {
                        i3++;
                    }
                }
                if (z && i3 != this.bmD) {
                    bg(i3, this.bmD);
                }
            }
            Eh();
            cJ();
            return;
        }
        this.bmH = true;
        this.bmI = System.currentTimeMillis();
    }
}
