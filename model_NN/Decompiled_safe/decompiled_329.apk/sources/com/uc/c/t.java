package com.uc.c;

import b.a;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

public class t {
    private static final String Gr = "ucformdata.dat";
    private static t Gs = null;
    private static final int Gw = 512;
    private static final String Gx = "<list>";
    private static final String Gy = "<form>";
    private Hashtable Gt = new Hashtable();
    private ah Gu;
    private ArrayList Gv = new ArrayList();

    private t() {
    }

    public static void destroy() {
        if (Gs != null) {
            Gs.kc();
        }
        Gs = null;
    }

    public static t kb() {
        if (Gs == null) {
            synchronized (t.class) {
                if (Gs == null) {
                    Gs = new t();
                }
            }
        }
        return Gs;
    }

    private void kc() {
        if (this.Gt != null) {
            this.Gt.clear();
            this.Gt = null;
        }
        if (this.Gv != null) {
            this.Gv = null;
        }
        Gs = null;
    }

    public static boolean kd() {
        return kb().kf();
    }

    public static boolean ke() {
        if (Gs == null) {
            return false;
        }
        return Gs.kg();
    }

    private boolean kf() {
        InputStream cs = a.cs(Gr);
        if (cs == null) {
            return false;
        }
        Vector vector = new Vector();
        int i = 0;
        while (cs.available() > 0) {
            try {
                byte[] bArr = new byte[Gw];
                i += cs.read(bArr);
                vector.add(bArr);
            } catch (IOException e) {
                if (cs != null) {
                    try {
                        cs.close();
                    } catch (IOException e2) {
                    }
                }
            } catch (Throwable th) {
                if (cs != null) {
                    try {
                        cs.close();
                    } catch (IOException e3) {
                    }
                }
                throw th;
            }
        }
        if (cs != null) {
            try {
                cs.close();
            } catch (IOException e4) {
            }
        }
        byte[] bArr2 = new byte[i];
        if (i < Gw) {
            System.arraycopy(vector.get(0), 0, bArr2, 0, i);
        } else {
            Iterator it = vector.iterator();
            int i2 = 0;
            while (it.hasNext()) {
                byte[] bArr3 = (byte[]) it.next();
                int length = i - i2 < Gw ? i - i2 : bArr3.length;
                System.arraycopy(bArr3, 0, bArr2, i2, length);
                i2 += length;
            }
        }
        try {
            bc.e(bArr2, true);
            DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(bArr2));
            String str = null;
            ah ahVar = null;
            int i3 = 0;
            boolean z = false;
            while (dataInputStream.available() > 0) {
                String readUTF = dataInputStream.readUTF();
                if (readUTF.equals(Gx)) {
                    this.Gv = new ArrayList();
                    z = true;
                } else if (readUTF.equals(Gy)) {
                    i3 = 0;
                    z = false;
                } else if (z) {
                    this.Gv.add(readUTF);
                } else {
                    switch (i3) {
                        case 0:
                            ahVar = new ah();
                            ahVar.ds = readUTF;
                            this.Gt.put(ahVar.ds, ahVar);
                            i3++;
                            continue;
                        case 1:
                            ahVar.aaa = readUTF;
                            i3++;
                            continue;
                        case 2:
                            i3 = 3;
                            str = readUTF;
                            continue;
                        case 3:
                            ahVar.aab.add(new h(str, readUTF));
                            i3 = 2;
                            str = null;
                            continue;
                        default:
                            continue;
                    }
                }
            }
        } catch (IOException | Exception e5) {
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0073, code lost:
        r0 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00aa, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ab, code lost:
        if (r1 != null) goto L_0x00ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00b0, code lost:
        if (r2 != null) goto L_0x00b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00b2, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00b5, code lost:
        if (r3 != null) goto L_0x00b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00b7, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00ba, code lost:
        throw r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00aa A[ExcHandler: all (r0v0 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:1:0x000c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean kg() {
        /*
            r7 = this;
            r5 = 0
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r1.<init>()
            java.io.DataOutputStream r2 = new java.io.DataOutputStream
            r2.<init>(r1)
            r3 = 0
            java.util.Hashtable r0 = r7.Gt     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            if (r0 == 0) goto L_0x00c2
            java.lang.String r0 = "<list>"
            r2.writeUTF(r0)     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            java.util.ArrayList r0 = r7.Gv     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            int r4 = r0.size()     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
        L_0x001b:
            if (r5 >= r4) goto L_0x002c
            java.util.ArrayList r0 = r7.Gv     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            java.lang.Object r0 = r0.get(r5)     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            r2.writeUTF(r0)     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            int r0 = r5 + 1
            r5 = r0
            goto L_0x001b
        L_0x002c:
            java.util.Hashtable r0 = r7.Gt     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            java.util.Enumeration r0 = r0.elements()     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
        L_0x0032:
            boolean r4 = r0.hasMoreElements()     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            if (r4 == 0) goto L_0x0085
            java.lang.Object r7 = r0.nextElement()     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            com.uc.c.ah r7 = (com.uc.c.ah) r7     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            java.lang.String r4 = "<form>"
            r2.writeUTF(r4)     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            java.lang.String r4 = r7.ds     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            r2.writeUTF(r4)     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            java.lang.String r4 = r7.aaa     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            if (r4 != 0) goto L_0x0050
            java.lang.String r4 = ""
            r7.aaa = r4     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
        L_0x0050:
            java.lang.String r4 = r7.aaa     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            r2.writeUTF(r4)     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            java.util.Vector r4 = r7.aab     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
        L_0x005b:
            boolean r5 = r4.hasNext()     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            if (r5 == 0) goto L_0x0032
            java.lang.Object r7 = r4.next()     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            com.uc.c.h r7 = (com.uc.c.h) r7     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            java.lang.String r5 = r7.ds     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            java.lang.String r6 = r7.value     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            r2.writeUTF(r5)     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            r2.writeUTF(r6)     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            goto L_0x005b
        L_0x0072:
            r0 = move-exception
            r0 = r3
        L_0x0074:
            if (r1 == 0) goto L_0x0079
            r1.close()     // Catch:{ IOException -> 0x00bd }
        L_0x0079:
            if (r2 == 0) goto L_0x007e
            r2.close()     // Catch:{ IOException -> 0x00bd }
        L_0x007e:
            if (r0 == 0) goto L_0x0083
            r0.close()     // Catch:{ IOException -> 0x00bd }
        L_0x0083:
            r0 = 1
            return r0
        L_0x0085:
            byte[] r0 = r1.toByteArray()     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            byte[] r0 = com.uc.c.bc.am(r0)     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            java.lang.String r4 = "ucformdata.dat"
            r5 = 0
            java.io.OutputStream r3 = b.a.l(r4, r5)     // Catch:{ IOException -> 0x0072, all -> 0x00aa }
            r3.write(r0)     // Catch:{ IOException -> 0x00bf, all -> 0x00aa }
            r0 = r3
        L_0x0098:
            if (r1 == 0) goto L_0x009d
            r1.close()     // Catch:{ IOException -> 0x00a8 }
        L_0x009d:
            if (r2 == 0) goto L_0x00a2
            r2.close()     // Catch:{ IOException -> 0x00a8 }
        L_0x00a2:
            if (r0 == 0) goto L_0x0083
            r0.close()     // Catch:{ IOException -> 0x00a8 }
            goto L_0x0083
        L_0x00a8:
            r0 = move-exception
            goto L_0x0083
        L_0x00aa:
            r0 = move-exception
            if (r1 == 0) goto L_0x00b0
            r1.close()     // Catch:{ IOException -> 0x00bb }
        L_0x00b0:
            if (r2 == 0) goto L_0x00b5
            r2.close()     // Catch:{ IOException -> 0x00bb }
        L_0x00b5:
            if (r3 == 0) goto L_0x00ba
            r3.close()     // Catch:{ IOException -> 0x00bb }
        L_0x00ba:
            throw r0
        L_0x00bb:
            r1 = move-exception
            goto L_0x00ba
        L_0x00bd:
            r0 = move-exception
            goto L_0x0083
        L_0x00bf:
            r0 = move-exception
            r0 = r3
            goto L_0x0074
        L_0x00c2:
            r0 = r3
            goto L_0x0098
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.t.kg():boolean");
    }

    public boolean a(ah ahVar) {
        this.Gt.put(ahVar.ds, ahVar);
        if (this.Gv.contains(ahVar.ds)) {
            this.Gv.remove(ahVar.ds);
            this.Gv.add(ahVar.ds);
        } else {
            this.Gv.add(ahVar.ds);
        }
        if (this.Gv.size() > 100) {
            this.Gt.remove((String) this.Gv.remove(0));
        }
        if (this.Gu == null || !this.Gu.ds.equals(ahVar.ds)) {
            return true;
        }
        this.Gu = ahVar;
        return true;
    }

    public void clearFormData() {
        this.Gu = null;
        this.Gv.clear();
        this.Gt.clear();
        kg();
    }

    public String n(String str, String str2) {
        if (this.Gt == null) {
            return null;
        }
        if (str == null || str2 == null) {
            return null;
        }
        if (this.Gu == null || !this.Gu.ds.equals(str)) {
            this.Gu = (ah) this.Gt.get(str);
            if (this.Gu == null) {
                return null;
            }
        }
        return this.Gu.getValue(str2);
    }
}
