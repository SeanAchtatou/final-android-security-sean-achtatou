package com.uc.c;

import b.a.a.a.f;
import b.a.a.a.k;
import b.a.a.a.l;
import b.a.a.a.m;
import b.a.a.a.w;
import b.a.a.a.z;
import com.uc.a.n;

public class bo extends cg implements z, Runnable {
    boolean Lh;
    w Rp;
    int ao;
    int ap;
    public int bmG;
    public ar boD;
    public l boE;
    boolean boF;
    boolean boG;
    public int boH;
    final int boI;
    int boJ;
    int boK;
    int boL;
    long boM;
    int boN;
    int boO;
    k boP;
    public byte boQ;
    m boR;
    boolean boS;
    boolean boT;
    public boolean boU;
    byte boV;
    public k boW;
    f boX;
    f boY;
    private f[] boZ;
    private int bpa;
    private int bpb;
    private int bpc;
    public int bpd;
    private long bpe;
    private long bpf;
    private long bpg;
    private n p;

    public bo(ar arVar) {
        this(arVar, 0);
    }

    public bo(ar arVar, int i) {
        this.boD = null;
        this.boE = null;
        this.Rp = null;
        this.ao = 0;
        this.ap = 0;
        this.boF = true;
        this.boG = false;
        this.boH = 0;
        this.boI = 3;
        this.boJ = 0;
        this.boK = 0;
        this.boL = 0;
        this.boM = System.currentTimeMillis();
        this.boN = 12;
        this.boO = 0;
        this.boP = null;
        this.boQ = 0;
        this.boR = new m(ar.avG[1], 3, 0);
        this.boS = false;
        this.boT = false;
        this.boU = false;
        this.boV = 0;
        this.Lh = false;
        this.boW = k.k(0, 0, k.adr);
        this.bmG = 0;
        this.boX = null;
        this.boY = null;
        this.boZ = null;
        this.bpa = 0;
        this.bpb = 0;
        this.bpc = 0;
        this.bpd = 0;
        this.bpe = 0;
        this.bpf = 60000;
        this.bpg = 180000;
        this.p = null;
        if (i == 0) {
            bc.Ck();
        }
        this.bpe = System.currentTimeMillis();
        this.boD = arVar;
        this.boE = arVar.JQ;
        if (1 == i) {
            this.boT = true;
        } else {
            this.boT = false;
        }
        this.boV = (byte) i;
        this.Lh = false;
        try {
            if (!w.nc() || this.boV == 2) {
                O(true);
            }
        } catch (Throwable th) {
        }
        this.bmG = this.boW.getHeight() + 2;
        if (this.boW.getHeight() <= 9) {
            this.bmG = k.k(0, 0, k.adt).getHeight() + 1;
        }
        this.boP = bc.gn(bc.Cv());
        if (w.nc() && Ff()) {
            b(this.boR);
            a(this);
        }
        String dO = bc.dO("microedition.platform");
        this.boG = (dO != null ? dO.toLowerCase() : "").indexOf("sonyericsson") == -1;
        if (this.p != null && !this.p.Eu()) {
            new Thread(this).start();
        }
        this.boE.a((w) this);
    }

    private void Fd() {
        r rVar;
        bx bxVar;
        this.boQ = -1;
        ku();
        ar arVar = this.boD;
        if (ar.mR != null) {
            ar arVar2 = this.boD;
            rVar = ar.mR.lB();
        } else {
            rVar = null;
        }
        if (rVar != null && (bxVar = rVar.BW) != null && bxVar.GZ()) {
            bxVar.bC(true);
            rVar.aY(0);
            if (this.boV == 1) {
                hu(99);
            }
            if (this.boH < 51 && this.boH >= 0 && this.boV == 0) {
                try {
                    bxVar.bK(false);
                } catch (Throwable th) {
                    hu(99);
                }
            }
        }
    }

    public void Fc() {
        new Thread(this).start();
    }

    public boolean Fe() {
        return this.boU;
    }

    public final boolean Ff() {
        return this.boS || this.boV == 1;
    }

    /* access modifiers changed from: protected */
    public void L(int i, int i2) {
        if (i > this.boK && i2 > this.boL && Ff()) {
            Fd();
        }
    }

    /* access modifiers changed from: protected */
    public void R(int i, int i2) {
        this.boF = true;
    }

    public final void a(m mVar, w wVar) {
        if (this.boR == mVar) {
            Fd();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0018  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String ac(int r9, int r10) {
        /*
            r8 = this;
            r7 = 6
            r6 = 3
            r5 = 2
            r2 = 0
            r4 = 1
            java.lang.String r0 = ""
            byte r1 = r8.boV
            if (r1 != 0) goto L_0x0176
            boolean r1 = r8.boS
            if (r1 == 0) goto L_0x001e
            java.lang.String[] r0 = com.uc.c.ar.avU
            r0 = r0[r2]
            r1 = r2
        L_0x0014:
            r2 = 0
            r3 = -1
            if (r10 != r3) goto L_0x002a
            java.lang.String[] r0 = com.uc.c.ar.avU
            r1 = 7
            r0 = r0[r1]
        L_0x001d:
            return r0
        L_0x001e:
            if (r9 != r6) goto L_0x0176
            com.uc.c.cb r0 = com.uc.c.cb.MA()
            java.lang.String r0 = r0.MK()
            r1 = r4
            goto L_0x0014
        L_0x002a:
            byte r3 = r8.boV
            if (r5 != r3) goto L_0x0078
            int r0 = r8.bpd
            r1 = 100
            if (r0 == r1) goto L_0x0075
            int r0 = r8.bpd
            r1 = 60
            if (r0 < r1) goto L_0x0075
            int r0 = r8.bpd
            int r0 = r0 + 1
            r8.bpd = r0
            r1 = 99
            int r0 = java.lang.Math.min(r0, r1)
        L_0x0046:
            r8.bpd = r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            int r1 = r8.bpd
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "%"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String[] r2 = com.uc.c.ar.avU
            r3 = 12
            r2 = r2[r3]
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            goto L_0x001d
        L_0x0075:
            int r0 = r8.bpd
            goto L_0x0046
        L_0x0078:
            switch(r9) {
                case -4: goto L_0x007c;
                case -3: goto L_0x0090;
                case -2: goto L_0x0083;
                case -1: goto L_0x0086;
                case 1: goto L_0x0099;
                case 2: goto L_0x00b0;
                case 3: goto L_0x00c7;
                case 20: goto L_0x00e0;
                case 21: goto L_0x00e0;
                case 22: goto L_0x00e0;
                case 23: goto L_0x00e0;
                case 50: goto L_0x0121;
                case 51: goto L_0x0139;
                case 99: goto L_0x0151;
                default: goto L_0x007b;
            }
        L_0x007b:
            goto L_0x001d
        L_0x007c:
            java.lang.String[] r0 = com.uc.c.ar.avM
            r1 = 8
            r0 = r0[r1]
            goto L_0x001d
        L_0x0083:
            java.lang.String r0 = "更新失败"
            goto L_0x001d
        L_0x0086:
            boolean r0 = r8.boT
            if (r0 == 0) goto L_0x0173
            java.lang.String[] r0 = com.uc.c.ar.avM
            r1 = 7
            r0 = r0[r1]
            goto L_0x001d
        L_0x0090:
            boolean r0 = r8.boT
            if (r0 == 0) goto L_0x0173
            java.lang.String[] r0 = com.uc.c.ar.avM
            r0 = r0[r7]
            goto L_0x001d
        L_0x0099:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String[] r1 = com.uc.c.ar.avU
            r1 = r1[r4]
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            goto L_0x001d
        L_0x00b0:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String[] r1 = com.uc.c.ar.avU
            r1 = r1[r5]
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            goto L_0x001d
        L_0x00c7:
            if (r1 != 0) goto L_0x001d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String[] r1 = com.uc.c.ar.avU
            r1 = r1[r6]
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            goto L_0x001d
        L_0x00e0:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String[] r1 = com.uc.c.ar.avU
            r2 = 8
            r1 = r1[r2]
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "["
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String[] r1 = com.uc.c.ar.avU
            r2 = 13
            r1 = r1[r2]
            java.lang.StringBuilder r0 = r0.append(r1)
            r1 = 19
            int r1 = r9 - r1
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String[] r1 = com.uc.c.ar.avU
            r2 = 14
            r1 = r1[r2]
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "]"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            goto L_0x001d
        L_0x0121:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String[] r1 = com.uc.c.ar.avU
            r2 = 4
            r1 = r1[r2]
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            goto L_0x001d
        L_0x0139:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String[] r1 = com.uc.c.ar.avU
            r2 = 5
            r1 = r1[r2]
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            goto L_0x001d
        L_0x0151:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r0 = r1.append(r0)
            byte r1 = r8.boV
            if (r1 != r4) goto L_0x016e
            java.lang.String[] r1 = com.uc.c.ar.avU
            r2 = 11
            r1 = r1[r2]
        L_0x0164:
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            goto L_0x001d
        L_0x016e:
            java.lang.String[] r1 = com.uc.c.ar.avU
            r1 = r1[r7]
            goto L_0x0164
        L_0x0173:
            r0 = r2
            goto L_0x001d
        L_0x0176:
            r1 = r2
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.bo.ac(int, int):java.lang.String");
    }

    public void b(n nVar) {
        this.p = nVar;
    }

    public final void bd(int i) {
        if ((i == -7 || i == -22 || i == 22 || i == 67 || i == 68 || i == 106 || i == -203 || i == 112 || i == 57346) && Ff()) {
            Fd();
        }
    }

    public n ch() {
        return this.p;
    }

    public void close() {
        this.boP = null;
        this.boW = null;
        this.boR = null;
        this.boZ = null;
        this.boX = null;
        this.boY = null;
    }

    public final void f(bk bkVar) {
        int i;
        int i2 = 0;
        this.ao = getWidth();
        this.ap = getHeight();
        try {
            if (this.boV == 2) {
                if (this.ao != getWidth() || this.ap != getHeight() || this.boF || !this.boG) {
                    if (this.boX == null) {
                        this.boX = this.ap >= 200 ? bc.dP("30") : bc.dP("25");
                    }
                    bkVar.setColor(3554887);
                    bkVar.o(0, 0, this.ao, this.ap);
                    this.bpb = (this.ap - (this.boX.getHeight() + (this.bmG * 4))) >> 1;
                    bkVar.a(this.boX, (this.ao - this.boX.getWidth()) >> 1, this.bpb, 20);
                    this.bpb += this.boX.getHeight();
                    if (this.boG) {
                        this.boX = null;
                    }
                }
                int i3 = this.ao - 40;
                char[][] o = this.boW.o(ar.avU[12], i3);
                int length = (this.bpb + (((this.ap - this.bpb) - (this.bmG * (o.length + 3))) >> 1)) - 10;
                bkVar.d(this.boW);
                bkVar.setColor(10660014);
                while (i2 < o.length) {
                    int i4 = this.bmG + length;
                    bkVar.a(o[i2], 0, o[i2].length, (this.ao - this.boW.a(o[i2], 0, o[i2].length)) >> 1, i4, 20);
                    i2++;
                    length = i4;
                }
                bkVar.setColor(8882055);
                int i5 = length + this.bmG + (this.bmG >> 1);
                int i6 = i5 < this.ap - this.bmG ? i5 : (this.ap - this.bmG) - 2;
                bkVar.q(20, i6, i3, this.bmG - 2);
                if (this.bpd == 100 || this.bpd < 60) {
                    i = this.bpd;
                } else {
                    int i7 = this.bpd + 1;
                    this.bpd = i7;
                    i = Math.min(i7, 99);
                }
                this.bpd = i;
                bkVar.setColor(14775819);
                bkVar.o(20 + 2, i6 + 2, ((this.bpd * i3) / 100) - 3, this.bmG - 5);
                bkVar.setColor(8882055);
                String str = this.bpd + as.azr;
                int cD = this.boW.cD(str);
                bkVar.b(str, (this.ao - cD) - 23, i6, cD, this.bmG, 2, 2);
                return;
            }
            if (this.ao != getWidth() || this.ap != getHeight() || this.boF || !this.boG) {
                bkVar.setColor(3554887);
                bkVar.o(0, 0, this.ao, this.ap);
                boolean z = this.ap >= 220;
                if (this.boX == null) {
                    this.boX = bc.dP("30");
                }
                if (this.boY == null) {
                    this.boY = bc.dP("25");
                }
                int height = z ? this.boX.getHeight() : 0;
                int height2 = this.boY.getHeight();
                int i8 = ((this.ap - (((height + height2) + this.bmG) + 10)) - this.bmG) / 6;
                int i9 = ((i8 * 3) / 2) + (i8 > 10 ? i8 : 2);
                if (z) {
                    bkVar.a(this.boX, (this.ao - this.boX.getWidth()) >> 1, i9, 20);
                }
                int i10 = (i8 >> 1) + height + i9;
                bkVar.a(this.boY, (this.ao - this.boY.getWidth()) >> 1, i10, 20);
                int i11 = i10 + height2 + i8;
                this.bpa = i11;
                this.bpc = this.bmG + i8;
                this.bpb = i11 + this.bpc;
                this.bpc += 10;
                if (Ff()) {
                    this.boK = (this.ao - this.boW.cD(ar.avG[1])) - 2;
                    this.boL = (this.ap - this.bmG) - 2;
                    bkVar.d(this.boW);
                    bkVar.setColor(10660014);
                    bkVar.b(ar.avG[1], this.boK, this.boL, 20);
                }
                if (this.boG) {
                    this.boX = null;
                    this.boY = null;
                }
                this.boF = false;
            }
            bkVar.setColor(3554887);
            bkVar.o(0, this.bpa, this.ao, this.bpc);
            String ac = ac(this.boH, this.boQ);
            bkVar.d(this.boW);
            bkVar.setColor(10660014);
            bkVar.b(ac, (this.ao - this.boW.cD(ac)) >> 1, this.bpa, 20);
            if (this.boZ == null) {
                this.boZ = new f[2];
                this.boZ[0] = bc.dP("50");
                this.boZ[1] = bc.dP("51");
            }
            int i12 = (this.ao - 70) >> 1;
            for (int i13 = 0; i13 < 5; i13++) {
                if (i13 == this.boO) {
                    bkVar.a(this.boZ[1], (i13 * 15) + i12, this.bpb, 20);
                } else {
                    bkVar.a(this.boZ[0], (i13 * 15) + i12, this.bpb, 20);
                }
            }
            int i14 = this.boO + 1;
            this.boO = i14;
            this.boO = i14 % 5;
        } catch (Throwable th) {
        }
    }

    public final synchronized void hu(int i) {
        if (this.p != null && true == this.p.Eu()) {
            int i2 = i < 0 ? 99 : i;
            this.p.dO(i2);
            this.boH = i2;
        } else if (-4 == this.boH || this.boH >= 99) {
            Hc();
            ku();
        } else {
            this.boH = i;
            Hc();
            ku();
            if (!(2 == this.boV || this.p == null)) {
                if (!this.boT || -3 != i || this.boU) {
                    this.p.eW(this.boH);
                } else {
                    this.p.eW(-3);
                    this.boH = 99;
                    this.boU = true;
                }
            }
        }
    }

    public final void hv(int i) {
        if (this.bpd + i < 60) {
            this.bpd += i;
        } else if (this.bpd <= i) {
            this.bpd = i;
        }
        if (2 != this.boV || this.p == null || this.p.eW(this.bpd)) {
            cJ();
            ku();
        }
    }

    /* access modifiers changed from: protected */
    public void kv() {
        if (this.boV == 1 && !this.Lh) {
            w.ns().a("ucweb.com", 0, 2, w.ns().lA(), 6);
            this.Lh = true;
        }
        this.boF = true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0156, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0157, code lost:
        r10.boD.avB = true;
        close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x015e, code lost:
        throw r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0156 A[ExcHandler:  FINALLY, Splitter:B:4:0x0010] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r10 = this;
            r9 = 20
            r8 = -1
            r7 = 1
            r6 = 99
            byte r0 = r10.boV
            r1 = 2
            if (r0 != r1) goto L_0x0041
            r0 = 1000(0x3e8, double:4.94E-321)
        L_0x000d:
            android.os.Looper.prepare()
            com.uc.a.n r2 = r10.p     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r2 == 0) goto L_0x0083
            com.uc.a.n r2 = r10.p     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            boolean r2 = r2.Eu()     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r7 != r2) goto L_0x0083
        L_0x001c:
            int r0 = b.b.a.OR()     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r0 == 0) goto L_0x002f
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            long r2 = r10.bpe     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            long r0 = r0 - r2
            long r2 = r10.bpf     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x0044
        L_0x002f:
            com.uc.a.n r0 = r10.p     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            r0.Ev()     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            r0 = 99
            r10.hu(r0)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            com.uc.c.ar r0 = r10.boD
            r0.avB = r7
            r10.close()
        L_0x0040:
            return
        L_0x0041:
            r0 = 300(0x12c, double:1.48E-321)
            goto L_0x000d
        L_0x0044:
            int r0 = r10.boH     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r6 > r0) goto L_0x0050
            com.uc.c.ar r0 = r10.boD
            r0.avB = r7
            r10.close()
            goto L_0x0040
        L_0x0050:
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            long r2 = r10.bpe     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            long r0 = r0 - r2
            long r2 = r10.bpf     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            r4 = 2
            long r2 = r2 / r4
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x0065
            r0 = 20
            r10.hu(r0)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
        L_0x0065:
            r0 = 1000(0x3e8, double:4.94E-321)
            java.lang.Thread.sleep(r0)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            goto L_0x001c
        L_0x006b:
            r0 = move-exception
            com.uc.c.ar r0 = r10.boD     // Catch:{ Exception -> 0x01d1, all -> 0x0156 }
            com.uc.c.w r0 = com.uc.c.ar.mR     // Catch:{ Exception -> 0x01d1, all -> 0x0156 }
            if (r0 == 0) goto L_0x01c1
            b.a.a.a.l r0 = r10.boE     // Catch:{ Exception -> 0x01d1, all -> 0x0156 }
            com.uc.c.ar r1 = r10.boD     // Catch:{ Exception -> 0x01d1, all -> 0x0156 }
            com.uc.c.w r1 = com.uc.c.ar.mR     // Catch:{ Exception -> 0x01d1, all -> 0x0156 }
            r0.a(r1)     // Catch:{ Exception -> 0x01d1, all -> 0x0156 }
        L_0x007b:
            com.uc.c.ar r0 = r10.boD
            r0.avB = r7
            r10.close()
            goto L_0x0040
        L_0x0083:
            int r2 = b.b.a.OR()     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r2 != 0) goto L_0x00ac
            byte r2 = r10.boV     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r7 != r2) goto L_0x00ac
            com.uc.a.n r2 = r10.p     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r2 == 0) goto L_0x00ac
            r2 = 99
            r10.boH = r2     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            com.uc.a.n r2 = r10.p     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            r3 = 20
            r2.eW(r3)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            r2 = 1000(0x3e8, double:4.94E-321)
            java.lang.Thread.sleep(r2)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            com.uc.a.n r2 = r10.p     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            r3 = -1
            r2.eW(r3)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            r2 = 3000(0xbb8, double:1.482E-320)
            java.lang.Thread.sleep(r2)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
        L_0x00ac:
            java.lang.Thread.sleep(r0)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            r2 = -4
            int r3 = r10.boH     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r2 != r3) goto L_0x00bd
            r2 = 3000(0xbb8, double:1.482E-320)
            java.lang.Thread.sleep(r2)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            r2 = 99
            r10.boH = r2     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
        L_0x00bd:
            r2 = -3
            int r3 = r10.boH     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r2 != r3) goto L_0x0147
            r2 = 100
            java.lang.Thread.sleep(r2)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            r2 = 99
            r10.boH = r2     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
        L_0x00cb:
            boolean r2 = r10.boT     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r2 == 0) goto L_0x015f
            boolean r2 = r10.boU     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r2 != 0) goto L_0x015f
            int r2 = r10.boH     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r2 >= r6) goto L_0x015f
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            long r4 = r10.bpe     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            long r2 = r2 - r4
            long r4 = r10.bpf     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x010e
            r2 = -1
            r10.boH = r2     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            com.uc.c.w r2 = com.uc.c.ar.mR     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            com.uc.c.r r2 = r2.lB()     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r2 == 0) goto L_0x00fb
            com.uc.c.bx r3 = r2.BW     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r3 == 0) goto L_0x00fb
            r4 = 1
            r3.bC(r4)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            r3 = 0
            r2.aY(r3)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
        L_0x00fb:
            com.uc.a.n r2 = r10.p     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r2 == 0) goto L_0x0105
            com.uc.a.n r2 = r10.p     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            r3 = -1
            r2.eW(r3)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
        L_0x0105:
            r2 = 3000(0xbb8, double:1.482E-320)
            java.lang.Thread.sleep(r2)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            r2 = 99
            r10.boH = r2     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
        L_0x010e:
            int r2 = r10.boH     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r2 != r6) goto L_0x01bc
            r10.Hc()     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            byte r2 = r10.boV     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r2 != r7) goto L_0x011b
            r0 = 1000(0x3e8, double:4.94E-321)
        L_0x011b:
            java.lang.Thread.sleep(r0)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            b.a.a.a.l r0 = r10.boE     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            b.a.a.a.w r0 = r0.vm()     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            boolean r0 = r10.equals(r0)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r0 == 0) goto L_0x0133
            b.a.a.a.l r0 = r10.boE     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            com.uc.c.ar r1 = r10.boD     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            com.uc.c.w r1 = com.uc.c.ar.mR     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            r0.a(r1)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
        L_0x0133:
            com.uc.a.n r0 = r10.p     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r0 == 0) goto L_0x013e
            com.uc.a.n r0 = r10.p     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            int r1 = r10.boH     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            r0.eW(r1)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
        L_0x013e:
            com.uc.c.ar r0 = r10.boD
            r0.avB = r7
            r10.close()
            goto L_0x0040
        L_0x0147:
            int r2 = r10.boH     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r8 != r2) goto L_0x00cb
            r2 = 3000(0xbb8, double:1.482E-320)
            java.lang.Thread.sleep(r2)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            r2 = 99
            r10.boH = r2     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            goto L_0x00cb
        L_0x0156:
            r0 = move-exception
            com.uc.c.ar r1 = r10.boD
            r1.avB = r7
            r10.close()
            throw r0
        L_0x015f:
            int r2 = r10.boH     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r2 >= r6) goto L_0x010e
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            long r4 = r10.bpe     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            long r2 = r2 - r4
            r4 = 10
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x0183
            int r2 = r10.boH     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r2 >= r9) goto L_0x0183
            r2 = 20
            r10.boH = r2     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            com.uc.a.n r2 = r10.p     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r2 == 0) goto L_0x0183
            com.uc.a.n r2 = r10.p     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            int r3 = r10.boH     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            r2.eW(r3)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
        L_0x0183:
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            long r4 = r10.bpe     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            long r2 = r2 - r4
            long r4 = r10.bpg     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x010e
            r2 = -2
            r10.boH = r2     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            com.uc.c.w r2 = com.uc.c.ar.mR     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            com.uc.c.r r2 = r2.lB()     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r2 == 0) goto L_0x01a7
            com.uc.c.bx r3 = r2.BW     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r3 == 0) goto L_0x01a7
            r4 = 1
            r3.bC(r4)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            r3 = 0
            r2.aY(r3)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
        L_0x01a7:
            com.uc.a.n r2 = r10.p     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            if (r2 == 0) goto L_0x01b1
            com.uc.a.n r2 = r10.p     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            r3 = -2
            r2.eW(r3)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
        L_0x01b1:
            r2 = 3000(0xbb8, double:1.482E-320)
            java.lang.Thread.sleep(r2)     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            r2 = 99
            r10.boH = r2     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            goto L_0x010e
        L_0x01bc:
            r10.Hc()     // Catch:{ Throwable -> 0x006b, all -> 0x0156 }
            goto L_0x00ac
        L_0x01c1:
            r0 = 10000(0x2710, double:4.9407E-320)
            java.lang.Thread.sleep(r0)     // Catch:{ Exception -> 0x01d1, all -> 0x0156 }
            b.a.a.a.l r0 = r10.boE     // Catch:{ Exception -> 0x01d1, all -> 0x0156 }
            com.uc.c.ar r1 = r10.boD     // Catch:{ Exception -> 0x01d1, all -> 0x0156 }
            com.uc.c.w r1 = com.uc.c.ar.mR     // Catch:{ Exception -> 0x01d1, all -> 0x0156 }
            r0.a(r1)     // Catch:{ Exception -> 0x01d1, all -> 0x0156 }
            goto L_0x007b
        L_0x01d1:
            r0 = move-exception
            goto L_0x007b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.bo.run():void");
    }
}
