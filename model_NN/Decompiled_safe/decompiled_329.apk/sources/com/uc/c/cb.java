package com.uc.c;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.uc.a.n;
import com.uc.b.a;
import com.uc.browser.R;
import java.math.BigDecimal;

public class cb {
    public static final String TAG = "UCTraffic";
    public static final String bPA = "M";
    public static final String bPB = "G";
    public static final long bPC = 5242880;
    public static final long bPD = 52428800;
    public static final long bPE = -2134967296;
    public static final String bPF = "uc_ts_share";
    public static final String bPG = "uc_ts_reset";
    public static final String bPH = "#UC浏览器省流量# http://wap.uc.cn/";
    public static final String bPI = "#UC浏览器省流量#";
    private static cb bPJ = null;
    public static long bPK = 524288000;
    public static String bPL = "uct_total_save_size";
    public static String bPM = "uct_total_original_size";
    public static String bPN = "uct_last_show_time";
    public static String bPO = "uct_first_show_tip";
    public static boolean bPP = false;
    public static final int bPQ = 0;
    public static final int bPR = 1;
    public static String bPS = "uc_initial_tip";
    public static String[] bPT = {a.ae.getResources().getString(R.string.f1545uc_initial_tip1), a.ae.getResources().getString(R.string.f1546uc_initial_tip2), a.ae.getResources().getString(R.string.f1547uc_initial_tip3), a.ae.getResources().getString(R.string.f1548uc_initial_tip4)};
    public static long bPX = bPC;
    public static final String bPY = "<?xml version=\"1.0\" encoding=\"utf-8\"?> <!DOCTYPE html PUBLIC \"-//WAPFORUM//DTD XHTML Mobile 1.0//EN\" \"http://www.wapforum.org/DTD/xhtml-mobile10.dtd\" > <html xmlns=\"http://www.w3.org/1999/xhtml\"> <head><title>流量统计</title> <style type=\"text/css\">body{margin:0;padding:0;color:#444;}#n1,#n2{padding:6px;}#bar{margin:0 auto;margin:6px;width:100%;height:14px;padding:1px;background:#e2e2e2;border:1px #c4c4c4 solid;}#colorbar{height:14px;background:#5ea80a;width:";
    public static final String bPZ = "%}#n3{height:30px;padding-top:6px;text-align: center;}#n4{margin:0 auto;padding:8px 0;background:#e5e5e5;}.num{color:#4a8802;}</style></head> <body> <div id=\"n1\">UC累计为您节省流量:<span class=\"num\">";
    public static final String bPp = "origin-size";
    public static final long bPw = 1024;
    public static final long bPx = 1048576;
    public static final long bPy = 1073741824;
    public static final String bPz = "K";
    public static final String bQa = "</span></div><div id=\"n2\">其中,云端服务为您节省比例:<span class=\"num\">";
    public static final String bQb = "%</span></div><div id=\"bar\"><div id=\"colorbar\"></div></div><div id=\"n3\"><form method=\"post\"><div>";
    public static final String bQc = "<input type=\"uc_ts_share\" value=\"分享\">&nbsp;";
    public static final String bQd = "<input type=\"uc_ts_reset\" value=\"清除数据\"></div></from></div><div id=\"n4\"><img src=\"images/saving.png\"/><a href=\"http://ucguide.uc.cn/myzone/ucguide/content?course_id=269\">如何更省流量</a></div></body></html> ";
    private String bPU = null;
    int bPV = 0;
    String bPW = null;
    private long bPq = 0;
    private long bPr = 0;
    private long bPs = 0;
    private long bPt = 0;
    private boolean bPu = false;
    private boolean bPv = true;

    public static cb MA() {
        if (bPJ == null) {
            synchronized (ab.class) {
                if (bPJ == null) {
                    bPJ = new cb();
                }
            }
        }
        return bPJ;
    }

    private boolean MM() {
        return this.bPq >= bPX;
    }

    private boolean q(long j) {
        boolean z = this.bPr + j > 4611686018427387903L;
        if (z) {
            this.bPr = 0;
            this.bPq = 0;
        }
        return z;
    }

    public boolean MB() {
        return this.bPu;
    }

    public void MC() {
        this.bPu = false;
    }

    public String MD() {
        return r(this.bPq);
    }

    public String ME() {
        return r(this.bPt);
    }

    public boolean MF() {
        return this.bPt >= bPx;
    }

    public void MG() {
        if (bPP) {
            PreferenceManager.getDefaultSharedPreferences(a.ae).edit().putLong(bPL, this.bPq).putLong(bPM, this.bPr).putLong(bPN, this.bPs).putBoolean(bPO, this.bPv).commit();
        }
    }

    public void MH() {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(a.ae);
        this.bPq = defaultSharedPreferences.getLong(bPL, 0);
        this.bPr = defaultSharedPreferences.getLong(bPM, 0);
        this.bPs = defaultSharedPreferences.getLong(bPN, 0);
        this.bPv = defaultSharedPreferences.getBoolean(bPO, true);
        bPP = true;
    }

    public String MI() {
        StringBuilder sb = new StringBuilder();
        if (a.ae != null) {
            sb.append(a.ae.getResources().getString(R.string.f1538uc_traffic_uc_save));
            sb.append(MD());
            sb.append(a.ae.getResources().getString(R.string.f1539uc_traffic));
        }
        return sb.toString();
    }

    public float MJ() {
        if (this.bPr <= 0 || this.bPq >= this.bPr) {
            return 0.0f;
        }
        return ((float) this.bPq) / ((float) this.bPr);
    }

    public String MK() {
        if (this.bPU == null) {
            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(a.ae);
            int i = defaultSharedPreferences.getInt(bPS, 0);
            if (i >= 0 && i < bPT.length) {
                this.bPU = bPT[i];
            }
            defaultSharedPreferences.edit().putInt(bPS, (i + 1) % bPT.length).commit();
        }
        return this.bPU;
    }

    public String ML() {
        return a.ae.getResources().getString(R.string.f1549uc_traffic_save_text1) + this.bPW + "，" + a.ae.getResources().getString(R.string.f1550uc_traffic_save_text2) + this.bPV + "%。" + bPH;
    }

    public String MN() {
        this.bPV = (int) (MJ() * 100.0f);
        this.bPW = MD();
        StringBuilder sb = new StringBuilder();
        sb.append(bPY);
        sb.append(this.bPV);
        sb.append(bPZ);
        sb.append(this.bPW);
        sb.append(bQa);
        sb.append(this.bPV);
        sb.append(bQb);
        if (MM()) {
            sb.append(bQc);
        }
        sb.append(bQd);
        return sb.toString();
    }

    public boolean a(long j, long j2) {
        boolean z = false;
        if (j < bPK) {
            long j3 = j - j2;
            if (j3 > 0) {
                this.bPt += j3;
                if (!q(j)) {
                    long j4 = this.bPv ? bPC : bPD;
                    if (j3 >= j4 - ((this.bPq - (this.bPv ? 0 : bPC)) % j4)) {
                        this.bPu = true;
                        z = true;
                    }
                    this.bPr += j;
                    this.bPq = j3 + this.bPq;
                }
            }
        }
        return z;
    }

    public void j(n nVar) {
        boolean z;
        int i = 0;
        if (this.bPu) {
            this.bPu = false;
            long currentTimeMillis = System.currentTimeMillis();
            if (this.bPv) {
                this.bPv = false;
                MG();
                z = true;
            } else {
                z = false;
                i = 1;
            }
            if ((z || currentTimeMillis - this.bPs > bPE) && nVar != null) {
                this.bPs = currentTimeMillis;
                nVar.u(MI(), i);
            }
        }
    }

    public void kS() {
        this.bPq = 0;
        this.bPr = 0;
        this.bPt = 0;
        this.bPu = false;
        this.bPv = true;
        MG();
    }

    public String r(long j) {
        BigDecimal scale;
        String str;
        if (j > bPy) {
            scale = new BigDecimal(((double) j) / 1.073741824E9d).setScale(2, 4);
            str = bPB;
        } else if (j > bPx) {
            scale = new BigDecimal(((double) j) / 1048576.0d).setScale(2, 4);
            str = bPA;
        } else {
            scale = new BigDecimal(((double) j) / 1024.0d).setScale(2, 4);
            str = bPz;
        }
        return scale.toString() + str;
    }
}
