package com.uc.c;

import b.a.a.a.f;
import com.uc.a.n;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.Vector;

public final class bh {
    private static bh biD = new bh();
    public static final byte biE = 0;
    public static final byte biF = 1;
    public static final byte biG = 2;
    public static final byte biH = 0;
    public static final byte biI = 1;
    public static final byte biJ = 2;
    public static final byte biK = 3;
    public static final byte biL = 0;
    public static final byte biM = 1;
    public static final byte biN = 2;
    public static final byte biO = 0;
    public static final byte biP = 1;
    public static final byte biQ = 2;
    public static final byte biR = 0;
    public static final byte biS = 1;
    public static final byte biT = 2;
    public static final byte biU = 3;
    public static final byte biV = 4;
    public static final byte biW = 5;
    public static final byte biX = 6;
    public static final byte bjA = 10;
    private static final byte bjF = 80;
    private static final byte bjG = 56;
    private static final byte bjH = 40;
    private static final byte bje = 4;
    private static final byte bjg = 25;
    private static final String bjh = "N";
    private static final int bji = 46080;
    public static final String bjm = "mynav";
    public static final String bjn = "navi_icon_addr";
    public static final String bjo = "navi_icon_flag";
    public static final byte bjp = 0;
    public static final String[] bju = {"png", "gif"};
    public static final int bjv = 40;
    public static final int bjw = 56;
    public static final int bjx = 64;
    public static final int bjy = 80;
    public static final long bjz = 86400000;
    public byte biY;
    public byte biZ;
    public byte bjB = 0;
    public long bjC = 0;
    public long bjD = 0;
    public long bjE = 0;
    public int[] bja;
    protected Vector bjb;
    private Vector bjc;
    public byte bjd;
    protected Vector bjf;
    protected boolean bjj = true;
    private long bjk;
    public boolean bjl = false;
    public byte bjq = -1;
    public String bjr;
    private cd bjs = cd.MZ();
    private Vector bjt = new Vector(25);

    private bh() {
        CM();
        CN();
        if (this.bjj) {
            this.bjb = new Vector(this.bjd);
            this.bjc = new Vector(4);
        }
        this.bjf = new Vector(25);
        for (int i = 0; i < this.bjd; i++) {
            this.bjf.addElement(null);
        }
    }

    private void CM() {
        int i;
        if (ar.mR == null || ar.mR.p == null) {
            i = 56;
        } else {
            int qX = ar.mR.p.qX();
            i = qX >= 480 ? 80 : qX >= 320 ? 56 : 40;
        }
        this.bja = new int[2];
        this.bja[0] = i;
        this.bja[1] = i;
        this.biZ = 4;
        this.biY = 8;
        this.bjd = (byte) (this.biZ << 1);
        bu(false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001c, code lost:
        if (0 != 0) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        com.uc.c.az.a(null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        com.uc.c.az.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0032, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0033, code lost:
        r3 = r1;
        r1 = r0;
        r0 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x001b A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:1:0x0003] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x002a A[SYNTHETIC, Splitter:B:20:0x002a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void CN() {
        /*
            r4 = this;
            r0 = 0
            java.lang.String r1 = "N"
            b.a.a.b.d r0 = com.uc.c.az.dx(r1)     // Catch:{ Exception -> 0x001b, all -> 0x0024 }
            int r1 = r0.AA()     // Catch:{ Exception -> 0x001b, all -> 0x0032 }
            r2 = 46080(0xb400, float:6.4572E-41)
            if (r1 <= r2) goto L_0x0019
            r1 = 1
        L_0x0011:
            r4.bjj = r1     // Catch:{ Exception -> 0x001b, all -> 0x0032 }
            if (r0 == 0) goto L_0x0018
            com.uc.c.az.a(r0)     // Catch:{ Exception -> 0x002e }
        L_0x0018:
            return
        L_0x0019:
            r1 = 0
            goto L_0x0011
        L_0x001b:
            r1 = move-exception
            if (r0 == 0) goto L_0x0018
            com.uc.c.az.a(r0)     // Catch:{ Exception -> 0x0022 }
            goto L_0x0018
        L_0x0022:
            r0 = move-exception
            goto L_0x0018
        L_0x0024:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
        L_0x0028:
            if (r1 == 0) goto L_0x002d
            com.uc.c.az.a(r1)     // Catch:{ Exception -> 0x0030 }
        L_0x002d:
            throw r0
        L_0x002e:
            r0 = move-exception
            goto L_0x0018
        L_0x0030:
            r1 = move-exception
            goto L_0x002d
        L_0x0032:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.bh.CN():void");
    }

    public static bh CQ() {
        return biD;
    }

    private byte[] CX() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        at NL = this.bjs.NL();
        try {
            k.a(dataOutputStream, NL);
            return byteArrayOutputStream.toByteArray();
        } finally {
            bc.k(NL);
            byteArrayOutputStream.close();
            dataOutputStream.close();
        }
    }

    private byte[] CY() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        try {
            dataOutputStream.writeByte(97);
            dataOutputStream.writeByte(0);
            dataOutputStream.writeByte(0);
            dataOutputStream.writeByte(0);
            dataOutputStream.write(new byte[12]);
            return byteArrayOutputStream.toByteArray();
        } finally {
            byteArrayOutputStream.close();
            dataOutputStream.close();
        }
    }

    private int a(Vector vector, String str) {
        for (int i = 0; i < vector.size(); i++) {
            if (eo(str).equals(eo(((Object[]) vector.elementAt(i))[2].toString()))) {
                return i;
            }
        }
        return -1;
    }

    private void a(int i, boolean z, Object[] objArr) {
        Object[] objArr2;
        int i2;
        Object[] objArr3 = (Object[]) this.bjf.elementAt(i);
        if (objArr3 != null) {
            if (z) {
                es((String) objArr3[5]);
            }
            this.bjf.setElementAt(null, i);
            int i3 = i + 1;
            int i4 = i;
            while (i3 < this.bjf.size() && (objArr2 = (Object[]) this.bjf.elementAt(i3)) != null) {
                if (((Byte) objArr2[2]).byteValue() == 0) {
                    this.bjf.setElementAt(objArr2, i4);
                    if (i3 == this.bjd) {
                        a(i4, objArr2, objArr != null ? objArr : objArr3);
                    }
                    i2 = i3;
                } else {
                    i2 = i4;
                }
                i3++;
                i4 = i2;
            }
            if (i4 != i) {
                this.bjf.setElementAt(null, i4);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:73:0x0159  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x015c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(int r13, java.lang.Object[] r14, java.lang.Object[] r15) {
        /*
            r12 = this;
            r11 = 5
            r6 = 3
            r10 = 2
            r9 = 1
            r8 = 0
            r1 = -2
            r0 = r14[r11]
            java.lang.String r2 = r0.toString()
            boolean r0 = r12.bjj
            if (r0 == 0) goto L_0x0156
            r0 = r14[r9]
            if (r0 == 0) goto L_0x001e
            r0 = r14[r9]
            java.lang.Byte r0 = (java.lang.Byte) r0
            byte r0 = r0.byteValue()
            if (r0 == r10) goto L_0x0157
        L_0x001e:
            java.util.Vector r0 = r12.bjb
            int r3 = r12.a(r0, r2)
            if (r3 >= 0) goto L_0x0144
            java.util.Vector r0 = r12.bjc
            int r1 = r12.a(r0, r2)
            if (r1 >= 0) goto L_0x00d0
            java.lang.Object[] r0 = new java.lang.Object[r6]
            r1 = 0
            r0[r8] = r1
            r1 = 0
            r0[r9] = r1
            r0[r10] = r2
            int r1 = r12.Dc()
            if (r13 >= r1) goto L_0x016b
            r12.er(r2)
            r4 = r0
            r5 = r6
        L_0x0043:
            java.util.Vector r0 = r12.bjb
            int r0 = r0.size()
            byte r1 = r12.bjd
            if (r0 < r1) goto L_0x0137
            if (r15 == 0) goto L_0x00e3
            r0 = r15[r8]
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r1 = r0.intValue()
            r0 = r15[r9]
            java.lang.Byte r0 = (java.lang.Byte) r0
            byte r0 = r0.byteValue()
            if (r0 != r6) goto L_0x006a
            r0 = r15[r11]
            java.lang.String r0 = r0.toString()
            r12.es(r0)
        L_0x006a:
            if (r1 < 0) goto L_0x0070
            byte r0 = r12.bjd
            if (r1 < r0) goto L_0x0098
        L_0x0070:
            r2 = r8
        L_0x0071:
            byte r0 = r12.bjd
            if (r2 >= r0) goto L_0x0093
            r3 = r8
        L_0x0076:
            byte r0 = r12.bjd
            if (r3 >= r0) goto L_0x0163
            java.util.Vector r0 = r12.bjf
            java.lang.Object r0 = r0.elementAt(r3)
            java.lang.Object[] r0 = (java.lang.Object[]) r0
            java.lang.Object[] r0 = (java.lang.Object[]) r0
            if (r0 == 0) goto L_0x012d
            r0 = r0[r8]
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            if (r0 != r2) goto L_0x012d
            r0 = r9
        L_0x0091:
            if (r0 != 0) goto L_0x0132
        L_0x0093:
            byte r0 = r12.bjd
            if (r2 >= r0) goto L_0x0098
            r1 = r2
        L_0x0098:
            if (r1 < 0) goto L_0x00bb
            java.util.Vector r0 = r12.bjb
            java.lang.Object r0 = r0.elementAt(r1)
            java.lang.Object[] r0 = (java.lang.Object[]) r0
            java.lang.Object[] r0 = (java.lang.Object[]) r0
            r2 = r0[r8]
            if (r2 == 0) goto L_0x00bb
            java.util.Vector r2 = r12.bjc
            int r2 = r2.size()
            r3 = 4
            if (r2 < r3) goto L_0x00b6
            java.util.Vector r2 = r12.bjc
            r2.removeElementAt(r8)
        L_0x00b6:
            java.util.Vector r2 = r12.bjc
            r2.addElement(r0)
        L_0x00bb:
            java.util.Vector r0 = r12.bjb
            r0.setElementAt(r4, r1)
            r0 = r1
        L_0x00c1:
            r1 = r0
            r0 = r5
        L_0x00c3:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r14[r8] = r1
            java.lang.Byte r0 = java.lang.Byte.valueOf(r0)
            r14[r9] = r0
            return
        L_0x00d0:
            java.util.Vector r0 = r12.bjc
            java.lang.Object r0 = r0.elementAt(r1)
            java.lang.Object[] r0 = (java.lang.Object[]) r0
            java.lang.Object[] r0 = (java.lang.Object[]) r0
            java.util.Vector r2 = r12.bjc
            r2.removeElementAt(r1)
            r4 = r0
            r5 = r9
            goto L_0x0043
        L_0x00e3:
            r6 = r8
        L_0x00e4:
            java.util.Vector r0 = r12.bjb
            int r0 = r0.size()
            if (r6 >= r0) goto L_0x0168
            java.util.Vector r0 = r12.bjb
            java.lang.Object r0 = r0.elementAt(r6)
            java.lang.Object[] r0 = (java.lang.Object[]) r0
            java.lang.Object[] r0 = (java.lang.Object[]) r0
            r7 = r8
        L_0x00f7:
            byte r1 = r12.bjd
            if (r7 >= r1) goto L_0x0166
            java.util.Vector r1 = r12.bjf
            java.lang.Object r1 = r1.elementAt(r7)
            java.lang.Object[] r1 = (java.lang.Object[]) r1
            java.lang.Object[] r1 = (java.lang.Object[]) r1
            if (r0 == 0) goto L_0x0125
            if (r1 == 0) goto L_0x0125
            r2 = r0[r10]
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r2 = eo(r2)
            r1 = r1[r11]
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r1 = eo(r1)
            boolean r1 = r2.equals(r1)
            if (r1 == 0) goto L_0x0125
            r0 = r8
        L_0x0120:
            if (r0 == 0) goto L_0x0129
            r1 = r6
            goto L_0x006a
        L_0x0125:
            int r1 = r7 + 1
            r7 = r1
            goto L_0x00f7
        L_0x0129:
            int r0 = r6 + 1
            r6 = r0
            goto L_0x00e4
        L_0x012d:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x0076
        L_0x0132:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x0071
        L_0x0137:
            java.util.Vector r0 = r12.bjb
            int r0 = r0.size()
            java.util.Vector r1 = r12.bjb
            r1.addElement(r4)
            goto L_0x00c1
        L_0x0144:
            java.util.Vector r0 = r12.bjb
            java.lang.Object r12 = r0.elementAt(r3)
            java.lang.Object[] r12 = (java.lang.Object[]) r12
            java.lang.Object[] r12 = (java.lang.Object[]) r12
            r0 = r12[r8]
            if (r0 == 0) goto L_0x015f
            r0 = r9
            r1 = r3
            goto L_0x00c3
        L_0x0156:
            r0 = r9
        L_0x0157:
            if (r0 != r10) goto L_0x015c
            r0 = r10
            goto L_0x00c3
        L_0x015c:
            r0 = r8
            goto L_0x00c3
        L_0x015f:
            r0 = r8
            r1 = r3
            goto L_0x00c3
        L_0x0163:
            r0 = r8
            goto L_0x0091
        L_0x0166:
            r0 = r9
            goto L_0x0120
        L_0x0168:
            r1 = r3
            goto L_0x006a
        L_0x016b:
            r4 = r0
            r5 = r8
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.bh.a(int, java.lang.Object[], java.lang.Object[]):void");
    }

    private Object aY(int i, int i2) {
        Object[] gB = gB(i);
        if (gB != null) {
            return gB[i2];
        }
        return null;
    }

    private void bv(boolean z) {
        if (z && this.biY < this.bjd) {
            int i = 0;
            for (int i2 = 0; i2 < this.biY; i2++) {
                if (gE(i2) == 1 && (i = i + 1) >= this.biZ - 1) {
                    this.biY = this.bjd;
                }
            }
        }
    }

    private synchronized void d(Object[] objArr, int i) {
        Object[] objArr2;
        Object[] objArr3;
        Object[] objArr4;
        Object[] objArr5;
        if (i == this.bjf.size()) {
            this.bjf.addElement(objArr);
            objArr2 = null;
        } else {
            objArr2 = (Object[]) this.bjf.elementAt(i);
            this.bjf.setElementAt(objArr, i);
        }
        if (objArr2 != null) {
            int i2 = i + 1;
            int size = this.bjf.size();
            if (i2 != size || size >= 25) {
                Object[] objArr6 = null;
                Object[] objArr7 = objArr2;
                int i3 = i2;
                while (true) {
                    if (i3 >= size) {
                        objArr3 = objArr6;
                        break;
                    }
                    Object[] objArr8 = (Object[]) this.bjf.elementAt(i3);
                    if (objArr8 == null) {
                        this.bjf.setElementAt(objArr7, i3);
                        objArr3 = objArr6;
                        break;
                    }
                    if (((Byte) objArr8[2]).byteValue() == 0) {
                        Object[] objArr9 = i3 == this.bjd ? objArr7 : objArr6;
                        this.bjf.setElementAt(objArr7, i3);
                        if (objArr8 == objArr) {
                            objArr3 = objArr9;
                            break;
                        }
                        if (i3 == size - 1 && size < 25) {
                            this.bjf.addElement(objArr8);
                        }
                        Object[] objArr10 = objArr9;
                        objArr4 = objArr8;
                        objArr5 = objArr10;
                    } else {
                        objArr5 = objArr6;
                        objArr4 = objArr7;
                    }
                    i3++;
                    objArr6 = objArr5;
                    objArr7 = objArr4;
                }
            } else {
                this.bjf.addElement(objArr2);
                objArr3 = null;
            }
        } else {
            objArr3 = null;
        }
        if (i >= 0 && i < Dc()) {
            a(i, objArr, objArr3);
        }
    }

    private Object[] el(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.bjf.size()) {
                return null;
            }
            Object[] objArr = (Object[]) this.bjf.elementAt(i2);
            if (objArr != null && eo(str).equals(eo((String) objArr[5]))) {
                return objArr;
            }
            i = i2 + 1;
        }
    }

    private int em(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.bjf.size()) {
                return -1;
            }
            Object[] objArr = (Object[]) this.bjf.elementAt(i2);
            if (objArr != null && eo(str).equals(eo((String) objArr[5]))) {
                return i2;
            }
            i = i2 + 1;
        }
    }

    private byte[] en(String str) {
        try {
            return bc.e(str.toCharArray());
        } catch (Exception e) {
            return null;
        }
    }

    public static String eo(String str) {
        String lowerCase = str.trim().toLowerCase();
        if (lowerCase.endsWith(au.aGF)) {
            lowerCase = lowerCase.substring(0, lowerCase.length() - 1);
        }
        int indexOf = lowerCase.indexOf(":80");
        if (indexOf > 0) {
            lowerCase = lowerCase.substring(0, indexOf) + lowerCase.substring(indexOf + 3);
        }
        int indexOf2 = lowerCase.indexOf(bx.bAh);
        if (indexOf2 >= 0) {
            return lowerCase.substring(indexOf2 + bx.bAh.length());
        }
        int indexOf3 = lowerCase.toLowerCase().indexOf("https://");
        return indexOf3 >= 0 ? lowerCase.substring(indexOf3 + "https://".length()) : lowerCase;
    }

    private int r(Object[] objArr) {
        int i;
        int i2 = 0;
        while (true) {
            i = i2;
            if (i >= this.bjf.size()) {
                return -1;
            }
            Object[] objArr2 = (Object[]) this.bjf.elementAt(i);
            if (objArr == objArr2) {
                return -1;
            }
            if (objArr2 != null && (((Byte) objArr2[2]).byteValue() != 0 || ((Integer) objArr[3]).intValue() < ((Integer) objArr2[3]).intValue())) {
                i2 = i + 1;
            }
        }
        return i;
    }

    /* JADX WARN: Type inference failed for: r0v4, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void x(java.io.DataInputStream r13) {
        /*
            r12 = this;
            java.util.Vector r1 = new java.util.Vector
            r0 = 25
            r1.<init>(r0)
            r0 = 0
            byte r2 = r13.readByte()
            r3 = 0
        L_0x000d:
            if (r3 >= r2) goto L_0x003e
            boolean r4 = r13.readBoolean()
            if (r4 != 0) goto L_0x0018
        L_0x0015:
            int r3 = r3 + 1
            goto L_0x000d
        L_0x0018:
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 2
            java.lang.String r6 = r13.readUTF()
            r4[r5] = r6
            int r5 = r13.readInt()
            if (r5 <= 0) goto L_0x0036
            byte[] r5 = new byte[r5]
            r13.read(r5)
            r6 = 0
            r7 = 0
            int r8 = r5.length
            b.a.a.a.f r5 = com.uc.c.bc.g(r5, r7, r8)
            r4[r6] = r5
        L_0x0036:
            boolean r5 = r12.bjj
            if (r5 == 0) goto L_0x0015
            r1.addElement(r4)
            goto L_0x0015
        L_0x003e:
            byte r2 = r13.readByte()
            byte r3 = r12.bjd
            if (r2 <= r3) goto L_0x0048
            byte r2 = r12.bjd
        L_0x0048:
            long r3 = r13.readLong()
            r12.bjk = r3
            long r3 = java.lang.System.currentTimeMillis()
            long r5 = r12.bjk
            long r3 = r3 - r5
            r5 = 86400000(0x5265c00, double:4.2687272E-316)
            long r3 = r3 / r5
            int r3 = (int) r3
            if (r3 <= 0) goto L_0x00d1
            long r4 = java.lang.System.currentTimeMillis()
        L_0x0060:
            r12.bjk = r4
            r4 = 0
            r5 = 0
            r6 = r0
            r11 = r4
            r4 = r5
            r5 = r11
        L_0x0068:
            if (r4 >= r2) goto L_0x0118
            r0 = 0
            int r7 = r13.readInt()
            r8 = -100
            if (r7 == r8) goto L_0x00f7
            r0 = 7
            java.lang.Object[] r8 = new java.lang.Object[r0]
            java.lang.Object r0 = r1.elementAt(r7)
            java.lang.Object[] r0 = (java.lang.Object[]) r0
            java.lang.Object[] r0 = (java.lang.Object[]) r0
            java.util.Vector r9 = r12.bjb
            r9.add(r0)
            r9 = 0
            int r10 = r6 + 1
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            r8[r9] = r6
            byte r6 = r13.readByte()
            boolean r9 = r12.bjj
            if (r9 == 0) goto L_0x011e
            r9 = 2
            if (r6 == r9) goto L_0x011e
            if (r7 < 0) goto L_0x009e
            r7 = 0
            r0 = r0[r7]
            if (r0 != 0) goto L_0x011e
        L_0x009e:
            r0 = 0
        L_0x009f:
            r6 = 1
            java.lang.Byte r0 = java.lang.Byte.valueOf(r0)
            r8[r6] = r0
            byte r0 = r13.readByte()
            r6 = 1
            if (r0 != r6) goto L_0x00b5
            int r5 = r5 + 1
            r6 = 4
            if (r4 < r6) goto L_0x00b5
            byte r6 = r12.biZ
            int r5 = r5 + r6
        L_0x00b5:
            r6 = 2
            java.lang.Byte r0 = java.lang.Byte.valueOf(r0)
            r8[r6] = r0
            int r0 = r13.readInt()
            r6 = 20
            if (r4 >= r6) goto L_0x0105
            r6 = 0
            r11 = r6
            r6 = r0
            r0 = r11
        L_0x00c8:
            if (r0 >= r3) goto L_0x00d4
            int r6 = r6 * 99
            int r6 = r6 / 100
            int r0 = r0 + 1
            goto L_0x00c8
        L_0x00d1:
            long r4 = r12.bjk
            goto L_0x0060
        L_0x00d4:
            r0 = r6
        L_0x00d5:
            r6 = 3
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r8[r6] = r0
            r0 = 4
            java.lang.String r6 = r13.readUTF()
            r8[r0] = r6
            r0 = 5
            java.lang.String r6 = r13.readUTF()
            r8[r0] = r6
            r0 = 6
            byte r6 = r13.readByte()
            java.lang.Byte r6 = java.lang.Byte.valueOf(r6)
            r8[r0] = r6
            r0 = r8
            r6 = r10
        L_0x00f7:
            byte r7 = r12.bjd
            if (r4 >= r7) goto L_0x0112
            java.util.Vector r7 = r12.bjf
            r7.setElementAt(r0, r4)
        L_0x0100:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x0068
        L_0x0105:
            r6 = 20
            if (r4 != r6) goto L_0x010e
            r6 = 5
        L_0x010a:
            if (r0 < r6) goto L_0x00d5
            r0 = r6
            goto L_0x00d5
        L_0x010e:
            r6 = 24
            int r6 = r6 - r4
            goto L_0x010a
        L_0x0112:
            java.util.Vector r7 = r12.bjf
            r7.addElement(r0)
            goto L_0x0100
        L_0x0118:
            if (r6 != 0) goto L_0x011d
            r12.CO()
        L_0x011d:
            return
        L_0x011e:
            r0 = r6
            goto L_0x009f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.bh.x(java.io.DataInputStream):void");
    }

    public void CO() {
        Object[] objArr = {0, (byte) 1, (byte) 0, 1, "UC大全", "http://3g.uc.cn/portal/index.html?uc_param_str=dnvessfrpfbicp&origin=android_nav", (byte) 1};
        Object[] objArr2 = {1, (byte) 1, (byte) 0, 1, "新浪", "http://3g.sina.com.cn/?vt=3&pos=200&wm=4007", (byte) 1};
        Object[] objArr3 = {2, (byte) 1, (byte) 1, 1, "UC乐园", "http://u.uc.cn/?uc_param_str=dnsspfveligiwi&s1=4", (byte) 1};
        Object[] objArr4 = {3, (byte) 1, (byte) 1, 1, "必备", "ext:webkit:http://bibei.uc.cn/bibei/wap/page?bibei_linkid=mynav&uc_param_str=pfvefrsnuamidn", (byte) 1};
        this.bjf.setElementAt(objArr, 0);
        this.bjf.setElementAt(objArr2, 1);
        this.bjf.setElementAt(objArr3, 2);
        this.bjf.setElementAt(objArr4, 3);
        if (this.bjj) {
            try {
                f[] ra = ar.mR.p.ra();
                Object[] objArr5 = {ra[0], null, "http://3g.uc.cn/portal/index.html?uc_param_str=dnvessfrpfbicp&origin=android_nav"};
                Object[] objArr6 = {ra[1], null, "http://3g.sina.com.cn/?vt=3&pos=200&wm=4007"};
                Object[] objArr7 = {ra[2], null, "http://u.uc.cn/?uc_param_str=dnsspfveligiwi&s1=4"};
                Object[] objArr8 = {ra[3], null, "ext:webkit:http://bibei.uc.cn/bibei/wap/page?bibei_linkid=mynav&uc_param_str=pfvefrsnuamidn"};
                this.bjb.addElement(objArr5);
                this.bjb.addElement(objArr6);
                this.bjb.addElement(objArr7);
                this.bjb.addElement(objArr8);
            } catch (Exception e) {
            }
        } else {
            objArr[0] = -1;
            objArr2[0] = -1;
            objArr3[0] = -1;
            objArr4[0] = -1;
        }
        CS();
    }

    public boolean CP() {
        boolean z;
        int Dc = Dc();
        int i = 0;
        boolean z2 = false;
        while (i < Dc) {
            Object[] objArr = (Object[]) this.bjf.elementAt(i);
            if (!this.bjj || objArr == null || ((Byte) objArr[2]).byteValue() == 2 || ((Byte) objArr[1]).byteValue() != 0) {
                z = z2;
            } else {
                if (a(this.bjb, (String) objArr[5]) < 0) {
                    a(i, objArr, (Object[]) null);
                }
                objArr[1] = (byte) 3;
                er(objArr[5].toString());
                z = true;
            }
            i++;
            z2 = z;
        }
        return z2;
    }

    /* JADX WARN: Type inference failed for: r0v30, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x007f A[SYNTHETIC, Splitter:B:41:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0084 A[SYNTHETIC, Splitter:B:44:0x0084] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0152 A[SYNTHETIC, Splitter:B:88:0x0152] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0157 A[SYNTHETIC, Splitter:B:91:0x0157] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void CR() {
        /*
            r14 = this;
            r13 = 0
            r12 = 2
            r11 = 1
            r10 = 0
            boolean r0 = r14.bjl
            if (r0 == 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            java.lang.String r0 = "N"
            byte[] r0 = com.uc.c.az.dz(r0)     // Catch:{ Exception -> 0x017e, all -> 0x0177 }
            if (r0 == 0) goto L_0x0014
            int r1 = r0.length     // Catch:{ Exception -> 0x017e, all -> 0x0177 }
            if (r1 != 0) goto L_0x0026
        L_0x0014:
            r14.CO()     // Catch:{ Exception -> 0x017e, all -> 0x0177 }
            r0 = r13
            r1 = r13
        L_0x0019:
            if (r1 == 0) goto L_0x001e
            r1.close()     // Catch:{ Exception -> 0x016a }
        L_0x001e:
            if (r0 == 0) goto L_0x0023
            r0.close()     // Catch:{ Exception -> 0x016d }
        L_0x0023:
            r14.bjl = r11
            goto L_0x0008
        L_0x0026:
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x017e, all -> 0x0177 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x017e, all -> 0x0177 }
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ Exception -> 0x0183, all -> 0x017b }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0183, all -> 0x017b }
            byte r0 = com.uc.c.az.bcX     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r3 = 9
            if (r0 == r3) goto L_0x003c
            byte r0 = com.uc.c.az.bcX     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r3 = 10
            if (r0 != r3) goto L_0x0042
        L_0x003c:
            r14.x(r2)     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r0 = r1
            r1 = r2
            goto L_0x0019
        L_0x0042:
            byte r0 = r2.readByte()     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r3 = r10
        L_0x0047:
            if (r3 >= r0) goto L_0x008a
            boolean r4 = r2.readBoolean()     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            if (r4 != 0) goto L_0x0052
        L_0x004f:
            int r3 = r3 + 1
            goto L_0x0047
        L_0x0052:
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r5 = 2
            java.lang.String r6 = r2.readUTF()     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r4[r5] = r6     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            int r5 = r2.readInt()     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            if (r5 <= 0) goto L_0x0070
            byte[] r5 = new byte[r5]     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r2.read(r5)     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r6 = 0
            r7 = 0
            int r8 = r5.length     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            b.a.a.a.f r5 = com.uc.c.bc.g(r5, r7, r8)     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r4[r6] = r5     // Catch:{ Exception -> 0x007a, all -> 0x014f }
        L_0x0070:
            boolean r5 = r14.bjj     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            if (r5 == 0) goto L_0x004f
            java.util.Vector r5 = r14.bjb     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r5.addElement(r4)     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            goto L_0x004f
        L_0x007a:
            r0 = move-exception
            r0 = r1
            r1 = r2
        L_0x007d:
            if (r1 == 0) goto L_0x0082
            r1.close()     // Catch:{ Exception -> 0x0170 }
        L_0x0082:
            if (r0 == 0) goto L_0x0023
            r0.close()     // Catch:{ Exception -> 0x0088 }
            goto L_0x0023
        L_0x0088:
            r0 = move-exception
            goto L_0x0023
        L_0x008a:
            byte r3 = r2.readByte()     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            long r4 = r2.readLong()     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r14.bjk = r4     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            long r6 = r14.bjk     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            long r4 = r4 - r6
            r6 = 86400000(0x5265c00, double:4.2687272E-316)
            long r4 = r4 / r6
            int r4 = (int) r4     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            if (r4 <= 0) goto L_0x0109
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x007a, all -> 0x014f }
        L_0x00a6:
            r14.bjk = r5     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r5 = r10
            r6 = r10
        L_0x00aa:
            if (r5 >= r3) goto L_0x015b
            int r0 = r2.readInt()     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r7 = -100
            if (r0 == r7) goto L_0x018b
            r7 = 7
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r8 = 0
            java.lang.Integer r9 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r7[r8] = r9     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            byte r8 = r2.readByte()     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            boolean r9 = r14.bjj     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            if (r9 == 0) goto L_0x0188
            if (r8 == r12) goto L_0x0188
            if (r0 < 0) goto L_0x00d9
            java.util.Vector r9 = r14.bjb     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            java.lang.Object r0 = r9.elementAt(r0)     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            java.lang.Object[] r0 = (java.lang.Object[]) r0     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            java.lang.Object[] r0 = (java.lang.Object[]) r0     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r9 = 0
            r0 = r0[r9]     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            if (r0 != 0) goto L_0x0188
        L_0x00d9:
            r0 = r10
        L_0x00da:
            r8 = 1
            java.lang.Byte r0 = java.lang.Byte.valueOf(r0)     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r7[r8] = r0     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            byte r0 = r2.readByte()     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            if (r0 != r11) goto L_0x00ef
            int r6 = r6 + 1
            r8 = 4
            if (r5 < r8) goto L_0x00ef
            byte r8 = r14.biZ     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            int r6 = r6 + r8
        L_0x00ef:
            r8 = 2
            java.lang.Byte r0 = java.lang.Byte.valueOf(r0)     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r7[r8] = r0     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            int r0 = r2.readInt()     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r8 = 20
            if (r5 >= r8) goto L_0x013c
            r8 = r0
            r0 = r10
        L_0x0100:
            if (r0 >= r4) goto L_0x010c
            int r8 = r8 * 99
            int r8 = r8 / 100
            int r0 = r0 + 1
            goto L_0x0100
        L_0x0109:
            long r5 = r14.bjk     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            goto L_0x00a6
        L_0x010c:
            r0 = r8
        L_0x010d:
            r8 = 3
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r7[r8] = r0     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r0 = 4
            java.lang.String r8 = r2.readUTF()     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r7[r0] = r8     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r0 = 5
            java.lang.String r8 = r2.readUTF()     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r7[r0] = r8     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r0 = 6
            byte r8 = r2.readByte()     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            java.lang.Byte r8 = java.lang.Byte.valueOf(r8)     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r7[r0] = r8     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r0 = r7
        L_0x012e:
            byte r7 = r14.bjd     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            if (r5 >= r7) goto L_0x0149
            java.util.Vector r7 = r14.bjf     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r7.setElementAt(r0, r5)     // Catch:{ Exception -> 0x007a, all -> 0x014f }
        L_0x0137:
            int r0 = r5 + 1
            r5 = r0
            goto L_0x00aa
        L_0x013c:
            r8 = 20
            if (r5 != r8) goto L_0x0145
            r8 = 5
        L_0x0141:
            if (r0 < r8) goto L_0x010d
            r0 = r8
            goto L_0x010d
        L_0x0145:
            r8 = 24
            int r8 = r8 - r5
            goto L_0x0141
        L_0x0149:
            java.util.Vector r7 = r14.bjf     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r7.addElement(r0)     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            goto L_0x0137
        L_0x014f:
            r0 = move-exception
        L_0x0150:
            if (r2 == 0) goto L_0x0155
            r2.close()     // Catch:{ Exception -> 0x0173 }
        L_0x0155:
            if (r1 == 0) goto L_0x015a
            r1.close()     // Catch:{ Exception -> 0x0175 }
        L_0x015a:
            throw r0
        L_0x015b:
            byte r0 = r14.biZ     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            if (r6 < r0) goto L_0x0167
            byte r0 = r14.bjd     // Catch:{ Exception -> 0x007a, all -> 0x014f }
        L_0x0161:
            r14.biY = r0     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            r0 = r1
            r1 = r2
            goto L_0x0019
        L_0x0167:
            byte r0 = r14.biZ     // Catch:{ Exception -> 0x007a, all -> 0x014f }
            goto L_0x0161
        L_0x016a:
            r1 = move-exception
            goto L_0x001e
        L_0x016d:
            r0 = move-exception
            goto L_0x0023
        L_0x0170:
            r1 = move-exception
            goto L_0x0082
        L_0x0173:
            r2 = move-exception
            goto L_0x0155
        L_0x0175:
            r1 = move-exception
            goto L_0x015a
        L_0x0177:
            r0 = move-exception
            r1 = r13
            r2 = r13
            goto L_0x0150
        L_0x017b:
            r0 = move-exception
            r2 = r13
            goto L_0x0150
        L_0x017e:
            r0 = move-exception
            r0 = r13
            r1 = r13
            goto L_0x007d
        L_0x0183:
            r0 = move-exception
            r0 = r1
            r1 = r13
            goto L_0x007d
        L_0x0188:
            r0 = r8
            goto L_0x00da
        L_0x018b:
            r0 = r13
            goto L_0x012e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.bh.CR():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.az.a(java.lang.String, byte[], boolean):boolean
     arg types: [java.lang.String, byte[], int]
     candidates:
      com.uc.c.az.a(b.a.a.b.d, byte[], int):void
      com.uc.c.az.a(java.lang.String, java.util.Hashtable, boolean):boolean
      com.uc.c.az.a(java.lang.String, byte[], boolean):boolean */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0070 A[SYNTHETIC, Splitter:B:36:0x0070] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0075 A[SYNTHETIC, Splitter:B:39:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x010a A[SYNTHETIC, Splitter:B:58:0x010a] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x010f A[SYNTHETIC, Splitter:B:61:0x010f] */
    /* JADX WARNING: Removed duplicated region for block: B:96:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void CS() {
        /*
            r9 = this;
            r8 = 0
            r7 = 1
            r6 = 0
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0140, all -> 0x0138 }
            r2.<init>()     // Catch:{ Exception -> 0x0140, all -> 0x0138 }
            java.io.DataOutputStream r3 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x0145, all -> 0x013c }
            r3.<init>(r2)     // Catch:{ Exception -> 0x0145, all -> 0x013c }
            java.util.Vector r0 = r9.bjb     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            if (r0 != 0) goto L_0x0037
            r0 = r6
        L_0x0012:
            boolean r1 = r9.bjj     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            if (r1 != 0) goto L_0x003e
            r1 = r6
        L_0x0017:
            r3.writeByte(r1)     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            boolean r0 = r9.bjj     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            if (r0 == 0) goto L_0x007b
            r4 = r6
        L_0x001f:
            if (r4 >= r1) goto L_0x007b
            java.util.Vector r0 = r9.bjb     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            java.lang.Object r0 = r0.elementAt(r4)     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            java.lang.Object[] r0 = (java.lang.Object[]) r0     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            java.lang.Object[] r0 = (java.lang.Object[]) r0     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            if (r0 == 0) goto L_0x0040
            r5 = r7
        L_0x002e:
            r3.writeBoolean(r5)     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            if (r0 != 0) goto L_0x0042
        L_0x0033:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x001f
        L_0x0037:
            java.util.Vector r0 = r9.bjb     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            int r0 = r0.size()     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            goto L_0x0012
        L_0x003e:
            r1 = r0
            goto L_0x0017
        L_0x0040:
            r5 = r6
            goto L_0x002e
        L_0x0042:
            r5 = 2
            r5 = r0[r5]     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r3.writeUTF(r5)     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r5 = 0
            r0 = r0[r5]     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            b.a.a.a.f r0 = (b.a.a.a.f) r0     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            if (r0 == 0) goto L_0x014a
            java.io.ByteArrayOutputStream r5 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r5.<init>()     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r0.a(r5)     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            byte[] r0 = r5.toByteArray()     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
        L_0x005f:
            if (r0 != 0) goto L_0x0079
            r5 = r6
        L_0x0062:
            r3.writeInt(r5)     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            if (r0 == 0) goto L_0x0033
            r3.write(r0)     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            goto L_0x0033
        L_0x006b:
            r0 = move-exception
            r0 = r2
            r1 = r3
        L_0x006e:
            if (r1 == 0) goto L_0x0073
            r1.close()     // Catch:{ Exception -> 0x012e }
        L_0x0073:
            if (r0 == 0) goto L_0x0078
            r0.close()     // Catch:{ Exception -> 0x0131 }
        L_0x0078:
            return
        L_0x0079:
            int r5 = r0.length     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            goto L_0x0062
        L_0x007b:
            java.util.Vector r0 = r9.bjf     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            int r0 = r0.size()     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r3.writeByte(r0)     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            long r0 = r9.bjk     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r4 = 0
            int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r0 != 0) goto L_0x00fc
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
        L_0x0090:
            r3.writeLong(r0)     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r4 = r6
        L_0x0094:
            java.util.Vector r0 = r9.bjf     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            int r0 = r0.size()     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            if (r4 >= r0) goto L_0x0113
            java.util.Vector r0 = r9.bjf     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            java.lang.Object r0 = r0.elementAt(r4)     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            java.lang.Object[] r0 = (java.lang.Object[]) r0     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            java.lang.Object[] r0 = (java.lang.Object[]) r0     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            if (r0 == 0) goto L_0x00ff
            r1 = 0
            r1 = r0[r1]     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r3.writeInt(r1)     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r1 = 1
            r1 = r0[r1]     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            java.lang.Byte r1 = (java.lang.Byte) r1     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            byte r1 = r1.byteValue()     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r3.writeByte(r1)     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r1 = 2
            r1 = r0[r1]     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            java.lang.Byte r1 = (java.lang.Byte) r1     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            byte r1 = r1.byteValue()     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r3.writeByte(r1)     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r1 = 3
            r1 = r0[r1]     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r3.writeInt(r1)     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r1 = 4
            r1 = r0[r1]     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r3.writeUTF(r1)     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r1 = 5
            r1 = r0[r1]     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r3.writeUTF(r1)     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r1 = 6
            r0 = r0[r1]     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            java.lang.Byte r0 = (java.lang.Byte) r0     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            byte r0 = r0.byteValue()     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r3.writeByte(r0)     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
        L_0x00f8:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x0094
        L_0x00fc:
            long r0 = r9.bjk     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            goto L_0x0090
        L_0x00ff:
            r0 = -100
            r3.writeInt(r0)     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            goto L_0x00f8
        L_0x0105:
            r0 = move-exception
            r1 = r2
            r2 = r3
        L_0x0108:
            if (r2 == 0) goto L_0x010d
            r2.close()     // Catch:{ Exception -> 0x0134 }
        L_0x010d:
            if (r1 == 0) goto L_0x0112
            r1.close()     // Catch:{ Exception -> 0x0136 }
        L_0x0112:
            throw r0
        L_0x0113:
            java.lang.String r0 = "N"
            byte[] r1 = r2.toByteArray()     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            r4 = 1
            com.uc.c.az.a(r0, r1, r4)     // Catch:{ Exception -> 0x006b, all -> 0x0105 }
            if (r3 == 0) goto L_0x0122
            r3.close()     // Catch:{ Exception -> 0x012c }
        L_0x0122:
            if (r2 == 0) goto L_0x0078
            r2.close()     // Catch:{ Exception -> 0x0129 }
            goto L_0x0078
        L_0x0129:
            r0 = move-exception
            goto L_0x0078
        L_0x012c:
            r0 = move-exception
            goto L_0x0122
        L_0x012e:
            r1 = move-exception
            goto L_0x0073
        L_0x0131:
            r0 = move-exception
            goto L_0x0078
        L_0x0134:
            r2 = move-exception
            goto L_0x010d
        L_0x0136:
            r1 = move-exception
            goto L_0x0112
        L_0x0138:
            r0 = move-exception
            r1 = r8
            r2 = r8
            goto L_0x0108
        L_0x013c:
            r0 = move-exception
            r1 = r2
            r2 = r8
            goto L_0x0108
        L_0x0140:
            r0 = move-exception
            r0 = r8
            r1 = r8
            goto L_0x006e
        L_0x0145:
            r0 = move-exception
            r0 = r2
            r1 = r8
            goto L_0x006e
        L_0x014a:
            r0 = r8
            goto L_0x005f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.bh.CS():void");
    }

    public void CT() {
        boolean z;
        boolean z2 = false;
        int i = 0;
        while (true) {
            z = z2;
            if (i >= this.bjf.size()) {
                break;
            }
            Object[] objArr = (Object[]) this.bjf.elementAt(i);
            if (objArr == null || ((Byte) objArr[2]).byteValue() != 0) {
                z2 = z;
            } else {
                this.bjf.setElementAt(null, i);
                es((String) objArr[5]);
                z2 = true;
            }
            i++;
        }
        if (z) {
            CS();
        }
    }

    public int CU() {
        int i = 0;
        int i2 = 0;
        while (true) {
            int i3 = i;
            if (i2 >= this.bjf.size()) {
                return i3;
            }
            Object[] gB = gB(i2);
            i = (gB == null || ((Byte) gB[2]).byteValue() == 2) ? i3 : i3 + 1;
            i2++;
        }
    }

    public int CV() {
        int i = 0;
        int i2 = 0;
        while (true) {
            int i3 = i;
            if (i2 >= this.bjf.size()) {
                return i3;
            }
            Object[] gB = gB(i2);
            i = (gB == null || ((Byte) gB[2]).byteValue() != 1) ? i3 : i3 + 1;
            i2++;
        }
    }

    public final byte[] CW() {
        try {
            return bc.h(CY(), CX());
        } catch (Throwable th) {
            return null;
        }
    }

    public at CZ() {
        at atVar = new at();
        cd cdVar = this.bjs;
        at a2 = cd.a(1, (byte) 1, (byte) 1, (Object) null);
        cd cdVar2 = this.bjs;
        at a3 = cd.a(2, (byte) 1, (byte) 1, (Object) null);
        cd cdVar3 = this.bjs;
        at a4 = cd.a(3, (byte) 1, (byte) 12, (Object) null);
        cd cdVar4 = this.bjs;
        at a5 = cd.a(4, (byte) 2, (byte) 12, (Object) null);
        cd cdVar5 = this.bjs;
        at a6 = cd.a(5, (byte) 2, (byte) 13, (Object) null);
        cd cdVar6 = this.bjs;
        at a7 = cd.a(1, (byte) 3, (byte) 14, new Object[]{a2, a3, a4, a5, a6});
        atVar.aDu = 14;
        atVar.aDv = new Object[]{a7};
        return atVar;
    }

    public Vector Da() {
        return this.bjt;
    }

    public void Db() {
        this.bjt.removeAllElements();
    }

    public int Dc() {
        return az.bbJ == 1 ? 4 : 8;
    }

    public void a(int i, byte b2) {
        gB(i)[6] = Byte.valueOf(b2);
    }

    public void a(String str, byte[] bArr, byte b2) {
        int a2 = a(this.bjb, str);
        if (a2 >= 0) {
            if (b2 == 1) {
                ((Object[]) this.bjb.elementAt(a2))[0] = bArr != null ? bc.g(bArr, 0, bArr.length) : null;
            }
            el(str)[1] = Byte.valueOf(b2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bh.a(int, boolean, java.lang.Object[]):void
     arg types: [int, int, java.lang.Byte[]]
     candidates:
      com.uc.c.bh.a(int, java.lang.Object[], java.lang.Object[]):void
      com.uc.c.bh.a(java.lang.String, byte[], byte):void
      com.uc.c.bh.a(int, boolean, java.lang.Object[]):void */
    public boolean a(int i, String str, String str2, byte b2) {
        Byte[] bArr;
        int i2;
        Object[] objArr;
        int i3;
        int em = em(str2);
        if (em >= 0) {
            Object[] objArr2 = (Object[]) this.bjf.elementAt(em);
            if (((Byte) objArr2[2]).byteValue() == 1) {
                return false;
            }
            bArr = objArr2;
        } else {
            bArr = null;
        }
        if (-1 == i) {
            int i4 = 0;
            while (true) {
                if (i4 >= this.bjf.size()) {
                    i3 = i;
                    break;
                } else if (((Object[]) this.bjf.elementAt(i4)) == null) {
                    i3 = i4;
                    break;
                } else {
                    i4++;
                }
            }
            if (-1 == i3) {
                return false;
            }
            i2 = i3;
        } else {
            i2 = i;
        }
        if (bArr == null || bArr[2].byteValue() != 0) {
            objArr = new Object[]{null, null, (byte) 1, 0, str, str2, Byte.valueOf(b2)};
        } else {
            bArr[2] = (byte) 1;
            bArr[4] = str;
            bArr[5] = str2;
            bArr[6] = Byte.valueOf(b2);
            a(em, false, (Object[]) bArr);
            objArr = bArr;
        }
        d(objArr, i2);
        CS();
        bx.Ik();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bh.a(int, boolean, java.lang.Object[]):void
     arg types: [int, int, java.lang.Object[]]
     candidates:
      com.uc.c.bh.a(int, java.lang.Object[], java.lang.Object[]):void
      com.uc.c.bh.a(java.lang.String, byte[], byte):void
      com.uc.c.bh.a(int, boolean, java.lang.Object[]):void */
    public boolean b(int i, String str, String str2, byte b2) {
        Object[] objArr;
        if (bc.by(str2)) {
            return false;
        }
        Object[] gB = gB(i);
        if (gB == null) {
            return a(i, str, str2, b2);
        }
        String trim = str2.trim();
        if (!gB[5].equals(trim)) {
            Object[] el = el(trim);
            if (el == null || el == gB) {
                gB[2] = (byte) 1;
                es((String) gB[5]);
                gB[5] = trim;
                gB[1] = (byte) 0;
                gB[3] = 0;
                this.bjf.setElementAt(null, i);
                d(gB, i);
                bx.Ik();
                objArr = gB;
            } else if (((Byte) el[2]).byteValue() == 1) {
                return false;
            } else {
                int indexOf = this.bjf.indexOf(el);
                el[2] = (byte) 1;
                if (indexOf >= this.bjb.size()) {
                    el[0] = -2;
                    el[1] = (byte) 0;
                }
                this.bjf.setElementAt(el, i);
                a(indexOf, false, gB);
                objArr = el;
            }
        } else {
            if (!str.equals(gB[4])) {
                gB[2] = (byte) 1;
            }
            objArr = gB;
        }
        objArr[6] = Byte.valueOf(b2);
        objArr[4] = str;
        CS();
        return true;
    }

    public boolean b(String str, String str2, byte b2, boolean z) {
        int i;
        String str3;
        Object[] el = el(str2);
        if (el == null) {
            if (!bc.by(str)) {
                str3 = str;
            } else if (bc.dM(str2)) {
                int indexOf = str2.indexOf("://");
                str3 = indexOf > 0 ? str2.substring(indexOf + 3) : str2;
            } else {
                str3 = "";
            }
            Object[] objArr = {-2, (byte) 0, (byte) 0, 1, str3, str2, Byte.valueOf(b2)};
            i = r(objArr);
            if (this.bjf.size() >= 25) {
                this.bjf.setElementAt(null, 24);
            }
            if (i < 0) {
                i = this.bjf.size();
            }
            d(objArr, i);
        } else {
            el[3] = Integer.valueOf(((Integer) el[3]).intValue() + 1);
            if (((Byte) el[2]).byteValue() == 0) {
                i = r(el);
                if (i >= 0) {
                    d(el, i);
                }
            } else {
                i = -1;
            }
        }
        if (z) {
            CS();
        }
        return i >= 0 && i < Dc();
    }

    public void bu(boolean z) {
        if (z || az.bbJ == -1) {
            az.bbJ = this.bja[0] == 80 ? 2 : 1;
        }
    }

    public void ci(int i) {
        n ch;
        Object[] objArr;
        if (i >= 0 && i < this.bjf.size() && (objArr = (Object[]) this.bjf.elementAt(i)) != null) {
            bx.a(gH(i), (byte) (i + 1), ((Byte) objArr[2]).byteValue());
        }
        if (gK(i) && (ch = ar.mR.ch()) != null) {
            ch.re();
        }
        if (this.bjq != 0 && this.bjB < 10) {
            CP();
        }
    }

    public boolean ep(String str) {
        return !this.bjj || a(this.bjb, str) < 0;
    }

    public boolean eq(String str) {
        return el(str) != null;
    }

    public final synchronized byte er(String str) {
        byte b2;
        if (this.bjq != 0 && bc.dM(str)) {
            if (this.bjt.indexOf(str) != -1) {
                b2 = 2;
            } else if (this.bjt.size() < 25) {
                this.bjt.addElement(str);
                b2 = 1;
            }
        }
        b2 = 0;
        return b2;
    }

    public boolean es(String str) {
        if (bc.by(str)) {
            return false;
        }
        return this.bjt.removeElement(str);
    }

    public Object[] gB(int i) {
        return (Object[]) this.bjf.elementAt(i);
    }

    public f gC(int i) {
        int intValue;
        Object aY = aY(i, 0);
        if (aY == null || (intValue = ((Integer) aY).intValue()) < 0) {
            return null;
        }
        if (this.bjj) {
            return (f) ((Object[]) this.bjb.elementAt(intValue))[0];
        }
        return null;
    }

    public String gD(int i) {
        return (String) aY(i, 4);
    }

    public byte gE(int i) {
        Object aY = aY(i, 2);
        if (aY == null) {
            return 2;
        }
        return ((Byte) aY).byteValue();
    }

    public byte gF(int i) {
        Object aY = aY(i, 1);
        if (aY == null) {
            return 0;
        }
        return ((Byte) aY).byteValue();
    }

    public int gG(int i) {
        Object aY = aY(i, 3);
        if (aY == null) {
            return 0;
        }
        return ((Integer) aY).intValue();
    }

    public String gH(int i) {
        return (String) aY(i, 5);
    }

    public byte gI(int i) {
        Object aY = aY(i, 6);
        if (aY == null) {
            return 0;
        }
        return ((Byte) aY).byteValue();
    }

    public void gJ(int i) {
        Object[] objArr = (Object[]) this.bjf.elementAt(i);
        if (this.bjj && objArr != null && ((Byte) objArr[2]).byteValue() != 2 && ((Byte) objArr[1]).byteValue() == 0) {
            if (a(this.bjb, (String) objArr[5]) < 0) {
                a(i, objArr, (Object[]) null);
            }
            objArr[1] = (byte) 3;
            er(objArr[5].toString());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bh.b(java.lang.String, java.lang.String, byte, boolean):boolean
     arg types: [java.lang.String, java.lang.String, byte, int]
     candidates:
      com.uc.c.bh.b(int, java.lang.String, java.lang.String, byte):boolean
      com.uc.c.bh.b(java.lang.String, java.lang.String, byte, boolean):boolean */
    public boolean gK(int i) {
        Object[] objArr = (Object[]) this.bjf.elementAt(i);
        return b((String) objArr[4], (String) objArr[5], ((Byte) objArr[6]).byteValue(), false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bh.a(int, boolean, java.lang.Object[]):void
     arg types: [int, int, ?[OBJECT, ARRAY]]
     candidates:
      com.uc.c.bh.a(int, java.lang.Object[], java.lang.Object[]):void
      com.uc.c.bh.a(java.lang.String, byte[], byte):void
      com.uc.c.bh.a(int, boolean, java.lang.Object[]):void */
    public void gL(int i) {
        a(i, true, (Object[]) null);
        CS();
    }

    public void gM(int i) {
        ((Object[]) this.bjf.elementAt(i))[2] = (byte) 1;
        CS();
    }

    public void gN(int i) {
        Object[] objArr;
        boolean z;
        int i2;
        Object[] objArr2 = (Object[]) this.bjf.elementAt(i);
        objArr2[2] = (byte) 0;
        int r = r(objArr2);
        if (r >= 0) {
            d(objArr2, r);
            if (gB(i) == objArr2) {
                this.bjf.setElementAt(null, i);
            }
        } else {
            int intValue = ((Integer) objArr2[3]).intValue();
            int i3 = i + 1;
            boolean z2 = false;
            int i4 = i;
            while (i3 < this.bjf.size() && (objArr = (Object[]) this.bjf.elementAt(i3)) != null) {
                if (((Byte) objArr[2]).byteValue() == 0) {
                    if (((Integer) objArr[3]).intValue() <= intValue) {
                        break;
                    }
                    this.bjf.setElementAt(objArr, i4);
                    if (i3 == this.bjd) {
                        a(i3, objArr, objArr2);
                    }
                    z = true;
                    i2 = i3;
                } else {
                    z = z2;
                    i2 = i4;
                }
                i3++;
                z2 = z;
                i4 = i2;
            }
            if (z2) {
                this.bjf.setElementAt(objArr2, i4);
            }
        }
        CS();
    }

    public boolean gO(int i) {
        if (i >= this.bjf.size()) {
            return true;
        }
        return this.bjf.elementAt(i) == null;
    }

    public void nQ() {
        if (CP()) {
            bx.Ik();
        }
    }
}
