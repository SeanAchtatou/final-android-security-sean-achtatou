package com.uc.c;

import b.a;
import b.a.a.c.e;
import com.uc.browser.ActivityBookmarkEx;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Vector;

public class u {
    private static final String Ih = "uccookies.dat";
    private Hashtable Ig = new Hashtable();

    private void a(String str, String str2, Vector vector, Hashtable hashtable) {
        int lastIndexOf;
        if (hashtable.containsKey(str2)) {
            Vector vector2 = (Vector) hashtable.get(str2);
            for (int i = 0; i < vector2.size(); i++) {
                if (a((al) vector2.elementAt(i))) {
                    vector.addElement(vector2.elementAt(i));
                }
            }
        }
        if (!str2.equals(au.aGF) && str2.length() > 1 && (lastIndexOf = str2.lastIndexOf(47)) != -1) {
            a(str, lastIndexOf == 0 ? au.aGF : str2.substring(0, lastIndexOf), vector, hashtable);
        }
    }

    private static boolean a(al alVar) {
        long currentTimeMillis = System.currentTimeMillis();
        if (alVar.aeT <= 0 || currentTimeMillis - alVar.aeT <= 0) {
            return alVar.aeS <= 0 || currentTimeMillis - alVar.aeS <= 0;
        }
        return false;
    }

    private void b(String str, String str2, Vector vector) {
        if (!this.Ig.isEmpty()) {
            Hashtable hashtable = (Hashtable) this.Ig.get(str);
            if (hashtable != null && !hashtable.isEmpty()) {
                a(str, str2, vector, hashtable);
            }
            int indexOf = str.indexOf(46, (str.length() <= 0 || str.charAt(0) != '.') ? 0 : 1);
            if (indexOf != -1) {
                b(str.substring(indexOf, str.length()), str2, vector);
            }
        }
    }

    public void a(String str, String str2, e eVar) {
        al[] a2;
        Vector vector = new Vector();
        int i = 0;
        while (i < 16) {
            try {
                String headerFieldKey = eVar.getHeaderFieldKey(i);
                if (!(headerFieldKey == null || headerFieldKey.length() == 0 || !"set-cookie".equals(headerFieldKey.toLowerCase()) || (a2 = a(eVar.getHeaderField(i), str, str2)) == null)) {
                    vector.addElement(a2);
                }
                i++;
            } catch (Throwable th) {
                return;
            }
        }
        a(str, str2, vector);
    }

    public void a(String str, String str2, Vector vector) {
        Hashtable hashtable;
        Vector vector2;
        if (vector != null && vector.size() != 0) {
            for (int i = 0; i < vector.size(); i++) {
                al[] alVarArr = (al[]) vector.elementAt(i);
                for (al alVar : alVarArr) {
                    if (this.Ig.containsKey(alVar.aeR)) {
                        hashtable = (Hashtable) this.Ig.get(alVar.aeR);
                    } else {
                        hashtable = new Hashtable();
                        this.Ig.put(alVar.aeR, hashtable);
                    }
                    if (hashtable != null) {
                        if (hashtable.isEmpty() || !hashtable.containsKey(alVar.aeQ)) {
                            vector2 = new Vector();
                            hashtable.put(alVar.aeQ, vector2);
                        } else {
                            vector2 = (Vector) hashtable.get(alVar.aeQ);
                        }
                        int i2 = 0;
                        while (true) {
                            if (i2 >= vector2.size()) {
                                break;
                            } else if (((al) vector2.elementAt(i2)).aeO.equals(alVar.aeO)) {
                                vector2.removeElementAt(i2);
                                break;
                            } else {
                                i2++;
                            }
                        }
                        if (!"deleted".equals(alVar.aeP)) {
                            vector2.addElement(alVar);
                        }
                    }
                }
            }
        }
    }

    public al[] a(String str, String str2, String str3) {
        if (bc.by(str)) {
            return null;
        }
        al alVar = new al();
        String[] split = bc.split(str, cd.bVG);
        Vector vector = new Vector(1);
        Vector vector2 = new Vector(1);
        for (String str4 : split) {
            int indexOf = str4.indexOf("=");
            if (indexOf != -1) {
                String substring = str4.substring(0, indexOf);
                String substring2 = str4.substring(indexOf + 1, str4.length());
                String trim = substring == null ? "" : substring.trim();
                String trim2 = substring2 == null ? "" : substring2.trim();
                if (!bc.by(trim)) {
                    String lowerCase = trim.toLowerCase();
                    if (ActivityBookmarkEx.asg.equals(lowerCase)) {
                        alVar.aeQ = trim2;
                    } else if ("domain".equals(lowerCase)) {
                        alVar.aeR = trim2;
                    } else if ("expires".equals(lowerCase)) {
                        alVar.aeS = bc.dY(trim2);
                    } else if ("max-age".equals(lowerCase)) {
                        int parseInt = bc.parseInt(trim2);
                        if (parseInt > 0) {
                            alVar.aeT = System.currentTimeMillis() + ((long) (parseInt * 1000));
                        }
                    } else if (!b.VERSION.equals(lowerCase) && bc.dM(trim)) {
                        vector.addElement(trim);
                        vector2.addElement(trim2);
                    }
                }
            } else if (bc.dM(str4) && !str4.trim().equalsIgnoreCase("httponly")) {
                vector.addElement(str4);
                vector2.addElement("");
            }
        }
        if (bc.by(alVar.aeR)) {
            alVar.aeR = str2;
        }
        alVar.aeR = ax(alVar.aeR);
        if (bc.by(alVar.aeQ)) {
            alVar.aeQ = au.aGF;
        }
        al[] alVarArr = new al[vector.size()];
        for (int i = 0; i < vector.size(); i++) {
            if (alVarArr[i] == null) {
                alVarArr[i] = new al();
            }
            alVarArr[i].aeR = alVar.aeR;
            alVarArr[i].aeQ = alVar.aeQ;
            alVarArr[i].aeS = alVar.aeS;
            alVarArr[i].aeT = alVar.aeT;
            alVarArr[i].aeO = (String) vector.elementAt(i);
            alVarArr[i].aeP = (String) vector2.elementAt(i);
        }
        vector.removeAllElements();
        vector2.removeAllElements();
        return alVarArr;
    }

    public String ax(String str) {
        int indexOf = str.indexOf(58);
        return indexOf != -1 ? str.substring(0, indexOf) : str;
    }

    public void b(String str, String str2, e eVar) {
        try {
            String p = p(str, str2);
            if (bc.dM(p)) {
                eVar.setRequestProperty("Cookie", p);
            }
        } catch (Throwable th) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027 A[Catch:{ IOException -> 0x008a, all -> 0x00b5 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void kL() {
        /*
            r10 = this;
            java.lang.String r9 = ""
            java.util.Hashtable r0 = r10.Ig
            if (r0 == 0) goto L_0x000e
            java.util.Hashtable r0 = r10.Ig
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x000f
        L_0x000e:
            return
        L_0x000f:
            java.lang.String r0 = "uccookies.dat"
            r1 = 0
            java.io.OutputStream r0 = b.a.l(r0, r1)
            java.io.DataOutputStream r1 = new java.io.DataOutputStream
            r1.<init>(r0)
            java.util.Hashtable r2 = r10.Ig
            java.util.Enumeration r2 = r2.elements()
        L_0x0021:
            boolean r3 = r2.hasMoreElements()     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            if (r3 == 0) goto L_0x00a6
            java.lang.Object r10 = r2.nextElement()     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            java.util.Hashtable r10 = (java.util.Hashtable) r10     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            java.util.Enumeration r3 = r10.elements()     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
        L_0x0031:
            boolean r4 = r3.hasMoreElements()     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            if (r4 == 0) goto L_0x0021
            java.lang.Object r10 = r3.nextElement()     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            java.util.Vector r10 = (java.util.Vector) r10     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            java.util.Iterator r4 = r10.iterator()     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
        L_0x0041:
            boolean r5 = r4.hasNext()     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            if (r5 == 0) goto L_0x0031
            java.lang.Object r10 = r4.next()     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            com.uc.c.al r10 = (com.uc.c.al) r10     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            long r5 = r10.aeS     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            int r5 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r5 < 0) goto L_0x0041
            java.lang.String r5 = r10.aeR     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            if (r5 != 0) goto L_0x009a
            java.lang.String r5 = ""
            r5 = r9
        L_0x005e:
            r1.writeUTF(r5)     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            java.lang.String r5 = r10.aeQ     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            if (r5 != 0) goto L_0x009d
            java.lang.String r5 = ""
            r5 = r9
        L_0x0068:
            r1.writeUTF(r5)     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            java.lang.String r5 = r10.aeO     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            if (r5 != 0) goto L_0x00a0
            java.lang.String r5 = ""
            r5 = r9
        L_0x0072:
            r1.writeUTF(r5)     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            java.lang.String r5 = r10.aeP     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            if (r5 != 0) goto L_0x00a3
            java.lang.String r5 = ""
            r5 = r9
        L_0x007c:
            r1.writeUTF(r5)     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            long r5 = r10.aeS     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            r1.writeLong(r5)     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            long r5 = r10.aeT     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            r1.writeLong(r5)     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            goto L_0x0041
        L_0x008a:
            r2 = move-exception
            if (r0 == 0) goto L_0x0090
            r0.close()     // Catch:{ IOException -> 0x0097 }
        L_0x0090:
            if (r1 == 0) goto L_0x000e
            r1.close()     // Catch:{ IOException -> 0x0097 }
            goto L_0x000e
        L_0x0097:
            r0 = move-exception
            goto L_0x000e
        L_0x009a:
            java.lang.String r5 = r10.aeR     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            goto L_0x005e
        L_0x009d:
            java.lang.String r5 = r10.aeQ     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            goto L_0x0068
        L_0x00a0:
            java.lang.String r5 = r10.aeO     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            goto L_0x0072
        L_0x00a3:
            java.lang.String r5 = r10.aeP     // Catch:{ IOException -> 0x008a, all -> 0x00b5 }
            goto L_0x007c
        L_0x00a6:
            if (r0 == 0) goto L_0x00ab
            r0.close()     // Catch:{ IOException -> 0x00b2 }
        L_0x00ab:
            if (r1 == 0) goto L_0x000e
            r1.close()     // Catch:{ IOException -> 0x00b2 }
            goto L_0x000e
        L_0x00b2:
            r0 = move-exception
            goto L_0x000e
        L_0x00b5:
            r2 = move-exception
            if (r0 == 0) goto L_0x00bb
            r0.close()     // Catch:{ IOException -> 0x00c1 }
        L_0x00bb:
            if (r1 == 0) goto L_0x00c0
            r1.close()     // Catch:{ IOException -> 0x00c1 }
        L_0x00c0:
            throw r2
        L_0x00c1:
            r0 = move-exception
            goto L_0x00c0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.u.kL():void");
    }

    public void kM() {
        Hashtable hashtable;
        String str;
        Vector vector;
        String str2;
        String str3 = null;
        InputStream cs = a.cs(Ih);
        if (cs != null) {
            if (this.Ig == null) {
                this.Ig = new Hashtable();
            }
            DataInputStream dataInputStream = new DataInputStream(cs);
            Vector vector2 = null;
            String str4 = null;
            Hashtable hashtable2 = null;
            while (dataInputStream.available() > 0) {
                try {
                    al alVar = new al();
                    alVar.aeR = dataInputStream.readUTF();
                    alVar.aeQ = dataInputStream.readUTF();
                    alVar.aeO = dataInputStream.readUTF();
                    alVar.aeP = dataInputStream.readUTF();
                    alVar.aeS = dataInputStream.readLong();
                    alVar.aeT = dataInputStream.readLong();
                    if (alVar.aeS >= System.currentTimeMillis()) {
                        if (hashtable2 == null || !str4.equals(alVar.aeR)) {
                            str = alVar.aeR;
                            Hashtable hashtable3 = (Hashtable) this.Ig.get(str);
                            if (hashtable3 == null) {
                                Hashtable hashtable4 = new Hashtable();
                                this.Ig.put(str, hashtable4);
                                hashtable = hashtable4;
                            } else {
                                hashtable = hashtable3;
                            }
                        } else {
                            hashtable = hashtable2;
                            str = str4;
                        }
                        if (vector2 == null || str3 != alVar.aeQ) {
                            String str5 = alVar.aeQ;
                            Vector vector3 = (Vector) hashtable.get(str5);
                            if (vector3 == null) {
                                Vector vector4 = new Vector(1);
                                hashtable.put(str5, vector4);
                                String str6 = str5;
                                vector = vector4;
                                str2 = str6;
                            } else {
                                String str7 = str5;
                                vector = vector3;
                                str2 = str7;
                            }
                        } else {
                            str2 = str3;
                            vector = vector2;
                        }
                        vector.add(alVar);
                        vector2 = vector;
                        str3 = str2;
                        str4 = str;
                        hashtable2 = hashtable;
                    }
                } catch (EOFException e) {
                    if (dataInputStream != null) {
                        try {
                            dataInputStream.close();
                        } catch (IOException e2) {
                            return;
                        }
                    }
                    if (cs != null) {
                        cs.close();
                        return;
                    }
                    return;
                } catch (IOException e3) {
                    if (dataInputStream != null) {
                        try {
                            dataInputStream.close();
                        } catch (IOException e4) {
                            return;
                        }
                    }
                    if (cs != null) {
                        cs.close();
                        return;
                    }
                    return;
                } catch (Throwable th) {
                    if (dataInputStream != null) {
                        try {
                            dataInputStream.close();
                        } catch (IOException e5) {
                            throw th;
                        }
                    }
                    if (cs != null) {
                        cs.close();
                    }
                    throw th;
                }
            }
            if (dataInputStream != null) {
                try {
                    dataInputStream.close();
                } catch (IOException e6) {
                    return;
                }
            }
            if (cs != null) {
                cs.close();
            }
        }
    }

    public void kN() {
        this.Ig.clear();
        a.deleteFile(Ih);
    }

    public void o(String str, String str2) {
        this.Ig.remove(this.Ig.get(ax(str)));
    }

    public String p(String str, String str2) {
        Vector vector = new Vector();
        b(ax(str), bc.by(str2) ? au.aGF : str2, vector);
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < vector.size(); i++) {
            stringBuffer.append(((al) vector.elementAt(i)).toString());
        }
        if (stringBuffer.length() > 0) {
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        }
        return stringBuffer.toString();
    }
}
