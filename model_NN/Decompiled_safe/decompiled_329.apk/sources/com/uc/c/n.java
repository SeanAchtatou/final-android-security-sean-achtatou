package com.uc.c;

import b.a.a.a.f;
import b.a.a.d;
import com.uc.b.e;
import com.uc.browser.ActivityBookmarkEx;
import com.uc.browser.ActivityChooseFile;
import com.uc.browser.UCR;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Stack;
import java.util.Vector;
import java.util.zip.GZIPInputStream;

public class n extends bi {
    public static final String pb = "-add-bookmark";
    public static final String pc = "-add-catalog";
    public static final String pd = "-mod-bookmark";
    public static final String pe = "-mod-catalog";
    public static final String pf = "-list-catalog";
    public static final byte pg = 0;
    public static final byte ph = 1;
    public static final byte pi = 2;
    public static final byte pj = 4;
    public static final byte pk = 8;
    public static final byte pl = 16;
    public static final int pm = 1;
    public static final int pn = 2;
    public static final int po = 4;
    public static Vector pq = new Vector();
    private static StringBuffer pv = null;
    bf oY;
    bf oZ;
    bf pa;
    private boolean pp;
    public int pr;
    int ps;
    private bn pt;
    private byte pu;

    public n(w wVar, r rVar, int i) {
        this(wVar, rVar, null, i);
    }

    public n(w wVar, r rVar, Vector vector, int i) {
        super(wVar, rVar);
        this.oY = null;
        this.oZ = null;
        this.pa = null;
        this.pp = false;
        this.pr = 1;
        this.ps = 0;
        this.pt = null;
        this.pu = 0;
        this.ps = i;
        this.bjJ = new bf(ar.avN[1], -1);
        this.bjK = new bf(ar.avN[2], -1);
        this.oY = new bf(ar.avN[21], -1);
        this.oZ = new bf(ar.avN[2], -1);
        if (vector != null) {
            pq = vector;
        }
    }

    public static void F(String str) {
        if (!bc.by(str)) {
            try {
                GZIPInputStream gZIPInputStream = new GZIPInputStream(new ByteArrayInputStream(bc.e(str.toCharArray())));
                cf cfVar = new cf();
                while (true) {
                    int read = gZIPInputStream.read();
                    if (read == -1) {
                        break;
                    }
                    cfVar.write(read);
                }
                gZIPInputStream.close();
                cfVar.close();
                String trim = bc.ak(cfVar.Oe()).trim();
                if (pv == null) {
                    pv = new StringBuffer(trim);
                } else {
                    pv.append(trim);
                }
            } catch (Exception e) {
            }
        }
    }

    private void Y(int i) {
        at dK;
        if (i == 12) {
            ca dA = dA();
            byte[] c = ca.c(dA.bHl, dA.bHm);
            if (c == null) {
                c = dA.bGd.a(dA.bFR, (f) null, (long) ca.bKR, (int[]) null, new f[]{dA.bHl}, new int[]{-1}, -1);
            }
            dK = c != null ? a(dA, c) : null;
        } else {
            dK = dK();
        }
        if (i == 11 || i == 21 || i == 22 || i == 1 || i == 13 || i == 14 || i == 12 || i == 15 || i == 2 || i == 3) {
            if (i == 11 || i == 21 || i == 22 || i == 1 || i == 53 || i == 15) {
                this.pu = 1;
            } else {
                this.pu = 0;
            }
            if (dK != null) {
                this.bjS = dK.hashCode();
            }
            this.bjT = this.bjR.bGv;
        } else if (i == -7 || i == -6 || i == -1) {
            this.pu = -1;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(int, int):int}
     arg types: [short, short]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(int, int):int} */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x003d A[Catch:{ Exception -> 0x0082 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x002f A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static short a(java.lang.String r12, java.util.Vector r13, short r14) {
        /*
            r1 = 0
            r9 = 0
            java.lang.String r0 = "`"
            java.lang.String[] r0 = com.uc.c.bc.split(r12, r0)
            r2 = r14
        L_0x0009:
            int r3 = r0.length
            if (r1 >= r3) goto L_0x0091
            r3 = r0[r1]
            java.lang.String r4 = "|"
            java.lang.String[] r3 = com.uc.c.bc.split(r3, r4)
            r4 = 0
            r4 = r3[r4]     // Catch:{ Exception -> 0x0032 }
            r5 = 1
            r5 = r3[r5]     // Catch:{ Exception -> 0x0092 }
            r6 = 2
            r6 = r3[r6]     // Catch:{ Exception -> 0x0098 }
            r7 = 3
            r7 = r3[r7]     // Catch:{ Exception -> 0x009d }
            r8 = 4
            r3 = r3[r8]     // Catch:{ Exception -> 0x00a3 }
            r10 = r7
            r7 = r4
            r4 = r10
            r11 = r5
            r5 = r6
            r6 = r11
        L_0x0029:
            boolean r8 = com.uc.c.bc.by(r7)     // Catch:{ Exception -> 0x0082 }
            if (r8 == 0) goto L_0x003d
        L_0x002f:
            int r1 = r1 + 1
            goto L_0x0009
        L_0x0032:
            r3 = move-exception
            r3 = r9
            r4 = r9
            r5 = r9
            r6 = r9
        L_0x0037:
            r7 = r6
            r6 = r5
            r5 = r4
            r4 = r3
            r3 = r9
            goto L_0x0029
        L_0x003d:
            short r7 = java.lang.Short.parseShort(r7)     // Catch:{ Exception -> 0x0082 }
            boolean r8 = com.uc.c.bc.by(r6)     // Catch:{ Exception -> 0x0082 }
            if (r8 == 0) goto L_0x0084
            r6 = -1
        L_0x0048:
            if (r7 > r2) goto L_0x004c
            if (r6 <= r2) goto L_0x0051
        L_0x004c:
            int r2 = java.lang.Math.max(r7, r6)     // Catch:{ Exception -> 0x0082 }
            short r2 = (short) r2     // Catch:{ Exception -> 0x0082 }
        L_0x0051:
            com.uc.c.at r8 = new com.uc.c.at     // Catch:{ Exception -> 0x0082 }
            r8.<init>()     // Catch:{ Exception -> 0x0082 }
            r8.aDi = r7     // Catch:{ Exception -> 0x0082 }
            r8.aDj = r6     // Catch:{ Exception -> 0x0082 }
            java.lang.String r5 = com.uc.c.bc.dN(r5)     // Catch:{ Exception -> 0x0082 }
            r8.aDq = r5     // Catch:{ Exception -> 0x0082 }
            boolean r5 = com.uc.c.bc.dM(r4)     // Catch:{ Exception -> 0x0082 }
            if (r5 == 0) goto L_0x0089
            java.lang.String r4 = com.uc.c.bc.dN(r4)     // Catch:{ Exception -> 0x0082 }
            r8.aDr = r4     // Catch:{ Exception -> 0x0082 }
            r4 = 4
            r8.aDu = r4     // Catch:{ Exception -> 0x0082 }
        L_0x006f:
            java.lang.String r4 = "1"
            boolean r3 = r4.equals(r3)     // Catch:{ Exception -> 0x0082 }
            if (r3 == 0) goto L_0x007e
            byte r3 = r8.aDu     // Catch:{ Exception -> 0x0082 }
            r3 = r3 | 16
            byte r3 = (byte) r3     // Catch:{ Exception -> 0x0082 }
            r8.aDu = r3     // Catch:{ Exception -> 0x0082 }
        L_0x007e:
            r13.addElement(r8)     // Catch:{ Exception -> 0x0082 }
            goto L_0x002f
        L_0x0082:
            r3 = move-exception
            goto L_0x002f
        L_0x0084:
            short r6 = java.lang.Short.parseShort(r6)     // Catch:{ Exception -> 0x0082 }
            goto L_0x0048
        L_0x0089:
            java.lang.String r4 = ""
            r8.aDr = r4     // Catch:{ Exception -> 0x0082 }
            r4 = 5
            r8.aDu = r4     // Catch:{ Exception -> 0x0082 }
            goto L_0x006f
        L_0x0091:
            return r2
        L_0x0092:
            r3 = move-exception
            r3 = r9
            r5 = r9
            r6 = r4
            r4 = r9
            goto L_0x0037
        L_0x0098:
            r3 = move-exception
            r3 = r9
            r6 = r4
            r4 = r9
            goto L_0x0037
        L_0x009d:
            r3 = move-exception
            r3 = r9
            r10 = r6
            r6 = r4
            r4 = r10
            goto L_0x0037
        L_0x00a3:
            r3 = move-exception
            r3 = r7
            r10 = r6
            r6 = r4
            r4 = r10
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.n.a(java.lang.String, java.util.Vector, short):short");
    }

    public static short a(Vector vector) {
        return g(vector, az.Bw());
    }

    private final void a(bf bfVar, bf bfVar2) {
        b(bfVar, bfVar2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.ca.b(int, java.lang.String, java.lang.String, java.lang.String, boolean):void
     arg types: [int, java.lang.String, ?[OBJECT, ARRAY], java.lang.String, int]
     candidates:
      com.uc.c.ca.b(java.lang.Object[], int, int, int, int):int
      com.uc.c.ca.b(byte[], int, int, byte[], byte[]):byte[]
      com.uc.c.ca.b(int, java.lang.String, java.lang.String, java.lang.String, boolean):void */
    private final void a(ca caVar, int i) {
        dC();
        String[] strArr = ar.avN;
        try {
            caVar.cl(true);
            caVar.t(strArr[26]);
            caVar.fp(w.Oy + pf);
            caVar.bHD = 5;
            int size = pq.size();
            byte[] dT = bc.dT("ext:li:26");
            caVar.b(0, "catalog", (String) null, ActivityChooseFile.uz, true);
            caVar.a(dT, (byte[]) null, new byte[]{1}, 13, 15);
            caVar.f(strArr[27].toCharArray());
            caVar.KY();
            at Z = Z(i);
            for (int i2 = 1; i2 < size; i2++) {
                at atVar = (at) pq.elementAt(i2);
                if (atVar.aDi != i && (atVar.aDu & 1) == 1 && ((Z.aDu & 1) != 1 || !t(i, atVar.aDi))) {
                    caVar.b(0, "catalog", (String) null, String.valueOf(atVar.aDi), false);
                    caVar.a(dT, (byte[]) null, (byte[]) null, 13, 15);
                    caVar.f(atVar.aDq.toCharArray());
                    caVar.KY();
                }
            }
            caVar.a(0, ActivityBookmarkEx.asj, (String) null, String.valueOf(i));
            caVar.cm(true);
        } catch (Exception e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean):void
     arg types: [java.lang.String, int, int, int, int, int, int, int]
     candidates:
      com.uc.c.bk.a(int, int, int, int, int, int, b.a.a.a.f, b.a.a.a.f):void
      com.uc.c.bk.a(int, int, int, int, int[], boolean, int, int):void
      com.uc.c.bk.a(int[], int, int, int, int, int, int, boolean):void
      com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int):void
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean):void */
    public static final boolean a(bk bkVar, ca caVar, byte[] bArr, Object obj) {
        at atVar = (at) pq.elementAt(((Integer) obj).intValue());
        if (bArr == null || atVar == null) {
            return false;
        }
        int b2 = f.b(bArr);
        int c = f.c(bArr);
        int e = f.e(bArr);
        int d = f.d(bArr);
        int i = ((e - 15) >> 1) + c;
        f fVar = null;
        if ((atVar.aDu & 1) == 1) {
            if (bc.aL(atVar.aDu, 2)) {
                fVar = az.gb(27);
            } else {
                fVar = az.gb(26);
                if (fVar == null) {
                }
            }
        } else if ((atVar.aDu & 1) == 0) {
            fVar = az.gb(28);
        }
        if (fVar != null) {
            bkVar.a(fVar, b2, i, 20);
        }
        bkVar.setColor(az.bdf[245]);
        if (bc.by(atVar.aDq)) {
            atVar.aDq = " ";
        }
        int n = e.bj(caVar.bFQ).n(atVar.aDq, (d - 13) - 2);
        String Z = n != atVar.aDq.length() ? bc.Z(atVar.aDq.substring(0, n), "..") : atVar.aDq;
        bkVar.d(e.bj(caVar.bFQ));
        bkVar.a(Z, b2 + 13 + 2, c, (d - 13) - 2, e, 1, 2, true);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.n.a(java.util.Vector, boolean, boolean):java.lang.String[]
     arg types: [java.util.Vector, boolean, int]
     candidates:
      com.uc.c.n.a(java.lang.String, java.util.Vector, short):short
      com.uc.c.n.a(int, java.lang.String, java.lang.String):void
      com.uc.c.n.a(com.uc.c.ca, java.lang.String, short):void
      com.uc.c.n.a(java.util.Vector, boolean, boolean):java.lang.String[] */
    public static final String[] a(Vector vector, boolean z) {
        return a(vector, z, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static final String[] a(Vector vector, boolean z, boolean z2) {
        StringBuffer stringBuffer;
        int indexOf;
        StringBuffer stringBuffer2 = new StringBuffer();
        if (z2) {
            stringBuffer2.append("id|父id|名称|url|直连标示`");
        }
        Vector vector2 = new Vector(8);
        int i = 0;
        while (true) {
            stringBuffer = stringBuffer2;
            if (i >= vector.size()) {
                break;
            }
            at atVar = (at) vector.elementAt(i);
            if (g(atVar)) {
                stringBuffer2 = stringBuffer;
            } else {
                stringBuffer.append(atVar.aDi);
                stringBuffer.append("|");
                stringBuffer.append(atVar.aDj == -1 ? "|" : atVar.aDj + "|");
                stringBuffer.append(atVar.aDq.replace('|', 65372));
                stringBuffer.append("|");
                if (!bc.dM(atVar.aDr) || (indexOf = atVar.aDr.indexOf((int) UCR.Color.bTm)) == -1) {
                    stringBuffer.append(atVar.aDr);
                } else {
                    String str = atVar.aDr;
                    int i2 = 0;
                    while (indexOf != -1) {
                        stringBuffer.append(str.substring(i2, indexOf)).append("%7C");
                        int i3 = indexOf + 1;
                        i2 = i3;
                        indexOf = str.indexOf((int) UCR.Color.bTm, i3);
                    }
                    stringBuffer.append(str.substring(i2));
                }
                stringBuffer.append("|");
                if (bc.aL(atVar.aDu, 16)) {
                    stringBuffer.append(com.uc.a.e.RD);
                }
                stringBuffer.append("`");
                if (!z || stringBuffer.length() <= 10240) {
                    stringBuffer2 = stringBuffer;
                } else {
                    vector2.addElement(stringBuffer.toString());
                    stringBuffer.setLength(0);
                    stringBuffer2 = new StringBuffer();
                }
            }
            i++;
        }
        if (stringBuffer.length() > 0) {
            vector2.addElement(stringBuffer.toString());
        }
        String[] strArr = new String[vector2.size()];
        vector2.copyInto(strArr);
        vector2.removeAllElements();
        return strArr;
    }

    private final void ad(int i) {
        a(i, (String) null, (String) null);
    }

    private final void af(int i) {
        at Z = Z(i);
        if (Z != null && !g(Z) && !bc.aL(Z.aDu, 2)) {
            at[] aa = aa(i);
            if (aa.length > 0) {
                for (at atVar : aa) {
                    af(atVar.aDi);
                }
            }
            pq.removeElement(Z);
        }
    }

    private void b(int i, boolean z) {
        at Z = Z(i);
        int i2 = bc.aL(Z.aDu, 16) ? 1 : 0;
        if (this.ps == 4) {
            if (z) {
                this.mR.a(Z.aDr, i2, 5, this.mR.a((byte) 0, Z.aDr, (byte) i2, (byte) 1, (com.uc.a.n) null));
            } else {
                this.mR.a(Z.aDr, i2, 5, this.mR.lA());
            }
        } else if (Z.aDr != null && Z.aDr.length() > 0 && az.bcr) {
            this.mR.f(Z.aDr, 1);
        }
        dD();
        this.mR.Hc();
    }

    private final void b(ca caVar) {
        if (!dv()) {
            this.mS.o(caVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.n.a(java.util.Vector, boolean):java.lang.String[]
     arg types: [java.util.Vector, int]
     candidates:
      com.uc.c.n.a(com.uc.c.bf, com.uc.c.bf):void
      com.uc.c.n.a(com.uc.c.ca, int):void
      com.uc.c.n.a(com.uc.c.ca, byte[]):com.uc.c.at
      com.uc.c.n.a(byte, java.lang.Object):void
      com.uc.c.n.a(int, int[]):void
      com.uc.c.bi.a(byte, java.lang.Object):void
      com.uc.c.bi.a(int, int[]):void
      com.uc.c.n.a(java.util.Vector, boolean):java.lang.String[] */
    public static final String[] b(Vector vector) {
        return a(vector, true);
    }

    private at c(at atVar, int i) {
        at[] aa;
        if (atVar == null || atVar.aDq == null || bc.aL(atVar.aDu, 1) || (aa = aa(i)) == null || aa.length < 1) {
            return null;
        }
        for (int i2 = 0; i2 < aa.length; i2++) {
            if (atVar.aDq.equals(aa[i2].aDq) && !bc.aL(aa[i2].aDu, 1)) {
                return aa[i2];
            }
        }
        return null;
    }

    public static final at c(Vector vector, int i) {
        int size = vector.size();
        for (int i2 = 0; i2 < size; i2++) {
            at atVar = (at) vector.elementAt(i2);
            if (atVar.aDi == i) {
                return atVar;
            }
        }
        return null;
    }

    private final void close() {
        dD();
        if (this.pp) {
            this.mR.lp();
        }
    }

    public static void d(Vector vector, int i) {
        if (!e(vector, i)) {
            ar.mR.bW(122880);
            e(vector, i);
        }
    }

    private final ca dB() {
        return dv() ? this.bjR : ca.Ku();
    }

    private final void dC() {
        if (dv() && this.bjR != null) {
            this.bjR.cq(true);
            this.bjR.Kv();
            ca caVar = this.bjR;
            caVar.bHs = (byte) (caVar.bHs | 6);
            this.bjR.iH(9);
            this.bjR.t(0, w.MQ, w.MK, w.MN - w.MQ);
            this.bjR.bp(w.MM, w.MN - this.bjR.bGo);
            this.bjR.KB();
        }
    }

    private final void dD() {
        if (!dv()) {
            this.mS.close();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.ca.b(boolean, int):boolean
     arg types: [int, int]
     candidates:
      com.uc.c.ca.b(int, com.uc.c.f):void
      com.uc.c.ca.b(java.io.DataOutputStream, java.util.Vector):void
      com.uc.c.ca.b(java.io.DataOutputStream, byte[]):void
      com.uc.c.ca.b(com.uc.c.f, int):java.lang.String
      com.uc.c.ca.b(com.uc.c.f, byte[]):void
      com.uc.c.ca.b(java.io.DataInputStream, com.uc.c.r):void
      com.uc.c.ca.b(boolean, int):boolean */
    private final void dI() {
        try {
            at dK = dK();
            if (dK != null && !bc.aL(dK.aDu, 2)) {
                ca dB = dB();
                at Z = Z(dK.aDi);
                if (Z != null && !g(Z) && !bc.aL(Z.aDu, 2)) {
                    a(dB, (dK.aDu & 1) + 2, dK.aDq, dK.aDr, dK.aDi);
                    b(dB);
                    a(this.oY, this.oZ);
                    dB.b(true, -1);
                }
            }
        } catch (Exception e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.ca.b(boolean, int):boolean
     arg types: [int, int]
     candidates:
      com.uc.c.ca.b(int, com.uc.c.f):void
      com.uc.c.ca.b(java.io.DataOutputStream, java.util.Vector):void
      com.uc.c.ca.b(java.io.DataOutputStream, byte[]):void
      com.uc.c.ca.b(com.uc.c.f, int):java.lang.String
      com.uc.c.ca.b(com.uc.c.f, byte[]):void
      com.uc.c.ca.b(java.io.DataInputStream, com.uc.c.r):void
      com.uc.c.ca.b(boolean, int):boolean */
    private final void dJ() {
        try {
            at dK = dK();
            if (dK != null && !g(dK) && !bc.aL(dK.aDu, 2)) {
                ca dB = dB();
                a(dB, dK.aDi);
                b(dB);
                a(this.oY, this.oZ);
                dB.b(true, -1);
            }
        } catch (Exception e) {
        }
    }

    private boolean dO() {
        if (!dv()) {
            return true;
        }
        ca dA = dA();
        byte[] c = ca.c(dA.bHl, dA.bHm);
        if (c == null || c[0] != 59) {
            return false;
        }
        f((byte) 12);
        return true;
    }

    private final boolean dQ() {
        return s(false);
    }

    private void dT() {
        this.pr = a(pq);
    }

    private void dU() {
        String[] strArr = ar.avN;
        this.bjL = new bn(10, -120, 0, this.mR);
        this.bjL.a(strArr[8], (byte) 11, (at) null);
        this.bjL.a(strArr[13], (byte) 12, (at) null);
        this.bjL.a(strArr[9], (byte) 13, (at) null);
        this.bjL.a(strArr[10], (byte) 14, (at) null);
        at a2 = this.bjL.a(strArr[7], (byte) 0, (at) null);
        this.bjL.a(strArr[5], (byte) 21, a2);
        this.bjL.a(strArr[6], (byte) 22, a2);
        this.bjL.a(strArr[33], (byte) 2, a2);
        this.bjL.a(strArr[34], (byte) 3, a2);
        this.bjL.a(strArr[11], (byte) 15, a2);
        this.bjL.a(strArr[14], (byte) 4, a2);
        at a3 = this.bjL.a(strArr[35], (byte) 0, (at) null);
        this.bjL.a(ar.avF[72], (byte) 8, a3);
        this.bjL.a(ar.avF[73], (byte) 9, a3);
        this.bjL.a(strArr[2], (byte) 7, (at) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.n.a(java.lang.String, java.util.Vector, short):short
     arg types: [java.lang.String, java.util.Vector, int]
     candidates:
      com.uc.c.n.a(java.util.Vector, boolean, boolean):java.lang.String[]
      com.uc.c.n.a(int, java.lang.String, java.lang.String):void
      com.uc.c.n.a(com.uc.c.ca, java.lang.String, short):void
      com.uc.c.n.a(java.lang.String, java.util.Vector, short):short */
    public static boolean dV() {
        boolean z;
        try {
            if (pv == null || pv.length() <= 0) {
                z = false;
            } else {
                Vector vector = new Vector();
                d(vector, a(pv.toString(), vector, (short) 1));
                vector.removeAllElements();
                z = true;
            }
            pv = null;
            return z;
        } catch (Throwable th) {
            return false;
        }
    }

    public static final String[] dW() {
        Vector vector = new Vector();
        a(vector);
        return b(vector);
    }

    public static Vector dX() {
        Vector vector = new Vector();
        a(vector);
        if (vector.size() < 1) {
            return null;
        }
        Vector vector2 = new Vector();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= vector.size()) {
                return vector2;
            }
            vector2.addElement(((at) vector.elementAt(i2)).aDr);
            i = i2 + 1;
        }
    }

    private void dY() {
        String[] strArr = ar.avN;
        this.pt = new bn(10, -120, 1, this.mR);
        this.pt.a(strArr[31], (byte) 6, (at) null);
        this.pt.a(strArr[8], (byte) 11, (at) null);
        this.pt.a(strArr[13], (byte) 12, (at) null);
        this.pt.a(strArr[9], (byte) 13, (at) null);
        this.pt.a(strArr[10], (byte) 14, (at) null);
        at a2 = this.pt.a(strArr[35], (byte) 0, (at) null);
        this.pt.a(ar.avF[72], (byte) 8, a2);
        this.pt.a(ar.avF[73], (byte) 9, a2);
    }

    private final boolean dv() {
        return this.ps == 4;
    }

    private final bf dw() {
        return dz().dw();
    }

    private final bf dx() {
        return dz().dx();
    }

    private final bn dy() {
        return dv() ? this.bjL : this.mS.BK;
    }

    private final r dz() {
        return dv() ? this.mR.lA() : this.mS;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.ca.a(int, java.lang.String, java.lang.String, int, boolean, boolean, boolean, boolean):void
     arg types: [int, java.lang.String, ?[OBJECT, ARRAY], int, int, int, int, int]
     candidates:
      com.uc.c.ca.a(int, byte[], byte[], byte[], int, int, int, int):void
      com.uc.c.ca.a(int, byte[], byte[], byte[], int, int, boolean, int):void
      com.uc.c.ca.a(int, byte[], byte[], byte[], boolean, boolean, boolean, boolean):void
      com.uc.c.ca.a(com.uc.c.f, int, int, int, int, int, int, boolean):boolean
      com.uc.c.ca.a(int, java.lang.String, java.lang.String, int, boolean, boolean, boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.ca.a(int, java.lang.String, java.lang.String, boolean):void
     arg types: [int, java.lang.String, java.lang.String, int]
     candidates:
      com.uc.c.ca.a(int, int, int, byte[]):void
      com.uc.c.ca.a(int, java.lang.String, java.lang.String, java.lang.String):void
      com.uc.c.ca.a(com.uc.c.f, com.uc.c.f, byte[], int[]):void
      com.uc.c.ca.a(byte[], byte[], int, int):void
      com.uc.c.ca.a(byte[], byte[], byte[], int):void
      com.uc.c.ca.a(int, java.lang.String, java.lang.String, boolean):void */
    private void e(ca caVar) {
        caVar.a(0, "cl", (String) null, caVar.bFQ, false, false, false, false);
        caVar.a(0, ar.avN[27], ActivityChooseFile.uz, false);
        int size = pq.size();
        for (int i = 1; i < size; i++) {
            at atVar = (at) pq.elementAt(i);
            if ((atVar.aDu & 1) == 1) {
                caVar.a(0, atVar.aDq, String.valueOf(atVar.aDi), atVar.aDi == this.bjN);
            }
        }
        caVar.iQ(0);
    }

    public static boolean e(Vector vector, int i) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        int size = vector.size();
        try {
            dataOutputStream.writeInt(size);
            dataOutputStream.writeShort(i);
            for (int i2 = 0; i2 < size; i2++) {
                at atVar = (at) vector.elementAt(i2);
                dataOutputStream.writeShort(atVar.aDi);
                dataOutputStream.writeShort(atVar.aDj);
                dataOutputStream.writeByte(atVar.aDu);
                dataOutputStream.writeUTF(bc.dN(atVar.aDq));
                dataOutputStream.writeUTF(bc.dN(atVar.aDr));
            }
            az.ab(byteArrayOutputStream.toByteArray());
            try {
                dataOutputStream.close();
                byteArrayOutputStream.close();
                return true;
            } catch (Exception e) {
                return true;
            }
        } catch (Throwable th) {
            try {
                dataOutputStream.close();
                byteArrayOutputStream.close();
            } catch (Exception e2) {
            }
            throw th;
        }
    }

    private void ea() {
        this.mR.aZ(dK().aDr);
    }

    public static final int f(Vector vector, int i) {
        String[] strArr = {"UC乐园", "UC大全", "百度搜索", "UC阅读器-rss.uc.cn", "乐淘鞋城"};
        String[] strArr2 = {"http://u.uc.cn/?uc_param_str=dnsnfrpfbivesscpligiwi&s1=17", "http://3g.uc.cn/portal/index.html?uc_param_str=dnvessfrpfbicp&origin=android_bookmark", "http://mw.ucweb.com/e?id=118", "http://mw.ucweb.com/t?id=bookmarks-rss-android", "http://adsclick.uc.cn/uc_common_param/?id=53575,3"};
        byte[] bArr = {16, 0, 0, 0, 0};
        int i2 = 0;
        int i3 = i;
        while (i2 < strArr.length) {
            at atVar = new at();
            atVar.aDq = bc.dN(strArr[i2]);
            atVar.aDr = bc.dN(strArr2[i2]);
            atVar.aDu = bArr[i2];
            atVar.aDi = i3;
            vector.addElement(atVar);
            i2++;
            i3++;
        }
        return i3;
    }

    public static short g(Vector vector, byte[] bArr) {
        short s;
        short s2;
        if (bArr == null) {
            return 0;
        }
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        DataInputStream dataInputStream = new DataInputStream(byteArrayInputStream);
        try {
            int readInt = dataInputStream.readInt();
            dataInputStream.readShort();
            short s3 = (short) readInt;
            try {
                vector.removeAllElements();
                for (int i = 0; i < readInt; i++) {
                    at atVar = new at();
                    atVar.aDi = dataInputStream.readShort();
                    atVar.aDj = dataInputStream.readShort();
                    atVar.aDu = dataInputStream.readByte();
                    atVar.aDq = dataInputStream.readUTF();
                    atVar.aDr = dataInputStream.readUTF();
                    vector.addElement(atVar);
                }
                try {
                    dataInputStream.close();
                    byteArrayInputStream.close();
                } catch (Exception e) {
                }
                return s3;
            } catch (Exception e2) {
                s2 = s3;
                try {
                    dataInputStream.close();
                    byteArrayInputStream.close();
                } catch (Exception e3) {
                }
                return s2;
            } catch (Throwable th) {
                s = s3;
                try {
                    dataInputStream.close();
                    byteArrayInputStream.close();
                } catch (Exception e4) {
                }
                return s;
            }
        } catch (Exception e5) {
            s2 = 0;
            dataInputStream.close();
            byteArrayInputStream.close();
            return s2;
        } catch (Throwable th2) {
            s = 0;
            dataInputStream.close();
            byteArrayInputStream.close();
            return s;
        }
    }

    private static final boolean g(at atVar) {
        if (atVar == null) {
            return false;
        }
        return bc.aL(atVar.aDu, 8);
    }

    private final boolean t(int i, int i2) {
        int i3 = i2;
        while (true) {
            at Z = Z(i3);
            if (Z.aDj == -1) {
                return false;
            }
            if (Z.aDj == i) {
                return true;
            }
            i3 = Z.aDj;
        }
    }

    public final at Z(int i) {
        return c(pq, i);
    }

    public at a(ca caVar, byte[] bArr) {
        if (bArr == null || bArr[0] != 59) {
            return null;
        }
        int intValue = ((Integer) ((Vector) caVar.bGl.get(Integer.valueOf(caVar.bS()))).elementAt(f.b(bArr, r.Eu, 0))).intValue();
        if (intValue < 0 || intValue >= pq.size()) {
            return null;
        }
        return (at) pq.elementAt(intValue);
    }

    public final void a(byte b2, Object obj) {
        if (!(b2 == 12 || b2 == 34)) {
            Y(b2);
        }
        ca dA = dA();
        switch (b2) {
            case 1:
                a(8, new int[1]);
                return;
            case 2:
                if (dA.bHm >= 0) {
                    ah(-1);
                    dG();
                    return;
                }
                return;
            case 3:
                if (dA.bHm >= 0) {
                    ah(1);
                    dG();
                    return;
                }
                return;
            case 4:
                if (!dz().a((byte) bn.bnM, b2, (Object) null)) {
                    dN();
                    dF();
                    return;
                }
                return;
            case 5:
                dL();
                return;
            case 6:
                if (dA.bHm >= 0) {
                    dP();
                    return;
                }
                return;
            case 7:
                this.mR.f((byte) 12);
                return;
            case 8:
                this.mR.nq();
                return;
            case 9:
                ea();
                return;
            case 10:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            default:
                return;
            case 11:
                if (dA.bHm >= 0) {
                    dI();
                    return;
                }
                return;
            case 12:
                if (dA.bHm >= 0 && !dz().a((byte) bn.bnM, b2, (Object) null)) {
                    dM();
                    Y(b2);
                    dG();
                    return;
                }
                return;
            case 13:
                if (dA.bHm >= 0) {
                    ag(-1);
                    dG();
                    return;
                }
                return;
            case 14:
                if (dA.bHm >= 0) {
                    ag(1);
                    dG();
                    return;
                }
                return;
            case 15:
                if (dA.bHm >= 0) {
                    dJ();
                    return;
                }
                return;
            case 21:
                ad(0);
                return;
            case 22:
                ad(1);
                return;
            case 31:
                this.mR.f((byte) 11);
                return;
            case 32:
                this.mR.f((byte) 12);
                return;
            case 33:
                if (!dz().a((byte) bn.bnM, b2, obj)) {
                    a((at[]) obj);
                    return;
                }
                return;
            case 34:
                if (!dz().a((byte) bn.bnM, b2, obj)) {
                    Y(-1);
                    b((at[]) obj);
                    return;
                }
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.ca.b(boolean, int):boolean
     arg types: [int, int]
     candidates:
      com.uc.c.ca.b(int, com.uc.c.f):void
      com.uc.c.ca.b(java.io.DataOutputStream, java.util.Vector):void
      com.uc.c.ca.b(java.io.DataOutputStream, byte[]):void
      com.uc.c.ca.b(com.uc.c.f, int):java.lang.String
      com.uc.c.ca.b(com.uc.c.f, byte[]):void
      com.uc.c.ca.b(java.io.DataInputStream, com.uc.c.r):void
      com.uc.c.ca.b(boolean, int):boolean */
    public final void a(int i, String str, String str2) {
        ca dB = dB();
        a(dB, i, str, str2, -1);
        b(dB);
        a(this.oY, this.oZ);
        dB.b(true, -1);
    }

    public final void a(int i, int[] iArr) {
        boolean z;
        bn dy = dy();
        if (dy != null && dy.biv == 1) {
            dy.bd(i);
        } else if (this.pt != null && this.pt.biv == 1) {
            this.pt.bd(i);
        } else if (dz().BR == null || dz().BR.biv != 1) {
            switch (i) {
                case -8:
                    z = !dO();
                    dz().jA();
                    break;
                case -7:
                    bf dx = dx();
                    if (dx == this.oZ) {
                        Y(-1);
                    }
                    a(dx);
                    z = false;
                    break;
                case d.aaJ:
                    bf dw = dw();
                    if (dw == this.oY) {
                        Y(-1);
                    }
                    a(dw);
                    z = false;
                    break;
                case 8:
                case 53:
                    if (dQ()) {
                        z = false;
                        break;
                    } else {
                        z = true;
                        break;
                    }
                case 48:
                    r dz = dz();
                    if (dv() && !dz.jr()) {
                        dz.ju();
                        z = false;
                        break;
                    } else {
                        z = false;
                        break;
                    }
                    break;
                case 49:
                    dZ();
                    z = false;
                    break;
                default:
                    z = true;
                    break;
            }
            if (z) {
                dz().b(i, iArr);
            }
        } else {
            dz().BR.bd(i);
        }
    }

    public final void a(bf bfVar) {
        int i;
        if (bfVar != null) {
            if (bfVar == this.bjJ) {
                dy().g(dz());
                dz().jA();
            } else if (bfVar == this.bjK) {
                if (dy() != null && dy().biv == 1) {
                    dy().ET();
                    dz().jA();
                } else if (this.bjN != -1) {
                    ac(Z(this.bjN).aDj);
                } else {
                    close();
                    this.mR.Hc();
                }
            } else if (bfVar == this.oY) {
                ca dA = dA();
                String[][] ce = dA.bGd.ce();
                at atVar = new at();
                if (dA.bFA.equals("ext:bmk-add-bookmark")) {
                    String a2 = i.a(ce[0], ce[1], "n");
                    String a3 = i.a(ce[0], ce[1], "v");
                    String a4 = i.a(ce[0], ce[1], "w");
                    try {
                        i = Integer.parseInt(i.a(ce[0], ce[1], "cl"));
                    } catch (Exception e) {
                        i = -1;
                    }
                    if (a2 == null || a2.length() == 0 || a3 == null || a3.length() == 0) {
                        this.mR.aY(ar.avN[29]);
                        return;
                    }
                    atVar.aDq = a2;
                    atVar.aDr = a3;
                    atVar.aDu = 4;
                    if (a4 != null) {
                        atVar.aDu = (byte) (atVar.aDu | 16);
                    }
                    this.pr++;
                    atVar.aDi = this.pr;
                    atVar.aDj = (short) i;
                    at c = c(atVar, atVar.aDj);
                    if (c != null) {
                        a((byte) 33, new at[]{c, atVar});
                        this.mR.Hc();
                        return;
                    }
                    f(atVar);
                } else if (dA.bFA.equals("ext:bmk-add-catalog")) {
                    String a5 = i.a(ce[0], ce[1], "n");
                    if (a5 == null || a5.length() == 0) {
                        this.mR.aY(ar.avN[29]);
                        return;
                    }
                    atVar.aDq = bc.dN(a5);
                    atVar.aDr = "";
                    atVar.aDu = 5;
                    this.pr++;
                    atVar.aDi = this.pr;
                    atVar.aDj = this.bjN;
                    f(atVar);
                } else if (dA.bFA.equals("ext:bmk-mod-bookmark")) {
                    String a6 = i.a(ce[0], ce[1], "n");
                    String a7 = i.a(ce[0], ce[1], "v");
                    if (a6 == null || a6.length() == 0 || a7 == null || a7.length() == 0) {
                        this.mR.aY(ar.avN[29]);
                        return;
                    }
                    at Z = Z(Integer.parseInt(i.a(ce[0], ce[1], "i")));
                    Z.aDq = a6;
                    Z.aDr = a7;
                    dR();
                } else if (dA.bFA.equals("ext:bmk-mod-catalog")) {
                    String a8 = i.a(ce[0], ce[1], "n");
                    if (a8 == null || a8.length() == 0) {
                        this.mR.aY(ar.avN[29]);
                        return;
                    } else {
                        Z(Integer.parseInt(i.a(ce[0], ce[1], "i"))).aDq = a8;
                        dR();
                    }
                } else if (dA.bFA.equals("ext:bmk-list-catalog")) {
                    String a9 = i.a(ce[0], ce[1], ActivityBookmarkEx.asj);
                    String a10 = i.a(ce[0], ce[1], "catalog");
                    if (a10 != null) {
                        int parseInt = bc.parseInt(a9);
                        int parseInt2 = bc.parseInt(a10);
                        at Z2 = Z(parseInt);
                        at c2 = c(Z2, parseInt2);
                        if (c2 != null && Z2 != c2) {
                            a((byte) 34, new at[]{c2, Z2});
                            this.mR.Hc();
                            return;
                        } else if (c2 == null) {
                            u(parseInt, parseInt2);
                            ac((short) parseInt2);
                            return;
                        }
                    } else {
                        a(this.oZ);
                    }
                }
                if (this.ps != 2) {
                    dG();
                    return;
                }
                close();
                this.mR.Hc();
            } else if (bfVar == this.oZ) {
                if (this.ps != 2) {
                    dG();
                    return;
                }
                close();
                this.mR.Hc();
            } else if (bfVar == this.pa) {
                dQ();
            } else if (bfVar != null) {
                dz().b(bfVar);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.ca.a(int, java.lang.String, java.lang.String, java.lang.String, int, int, int, int, int, int, int, boolean, boolean, boolean, boolean):void
     arg types: [int, java.lang.String, ?[OBJECT, ARRAY], java.lang.String, int, int, int, int, int, int, int, int, int, int, int]
     candidates:
      com.uc.c.ca.a(int, java.lang.String, java.lang.String, java.lang.String, int, int, int, int, int, boolean, boolean, boolean, boolean, int, int):void
      com.uc.c.ca.a(int, java.lang.String, java.lang.String, java.lang.String, int, int, int, int, int, int, int, boolean, boolean, boolean, boolean):void */
    public void a(ca caVar, int i, String str, String str2, int i2) {
        boolean z;
        int i3;
        boolean z2;
        int i4;
        dC();
        String[] strArr = ar.avN;
        String str3 = null;
        try {
            caVar.cl(true);
            switch (i) {
                case 0:
                    z = false;
                    i3 = i;
                    caVar.t(strArr[17]);
                    caVar.f(strArr[19].toCharArray());
                    z2 = z;
                    i4 = i3;
                    str3 = pb;
                    break;
                case 1:
                    caVar.t(strArr[6]);
                    caVar.f(strArr[6].toCharArray());
                    str3 = pc;
                    z2 = false;
                    i4 = i;
                    break;
                case 2:
                    caVar.t(strArr[22]);
                    caVar.f(strArr[19].toCharArray());
                    str3 = pd;
                    z2 = false;
                    i4 = i;
                    break;
                case 3:
                    caVar.t(strArr[23]);
                    caVar.f(strArr[26].toCharArray());
                    str3 = pe;
                    z2 = false;
                    i4 = i;
                    break;
                case 4:
                    z = true;
                    i3 = 0;
                    caVar.t(strArr[17]);
                    caVar.f(strArr[19].toCharArray());
                    z2 = z;
                    i4 = i3;
                    str3 = pb;
                    break;
                default:
                    z2 = false;
                    i4 = i;
                    break;
            }
            caVar.fp(w.Oy + str3);
            caVar.bHD = (byte) (caVar.bHD | 7);
            caVar.KY();
            caVar.a(0, "n", (String) null, str, caVar.bFQ, 0, 1, 1024, -1, 1, 0, false, false, false, false);
            if (i4 == 0 || i4 == 2) {
                caVar.f(strArr[20].toCharArray());
                caVar.KY();
                caVar.a(0, "v", (String) null, str2, caVar.bFQ, 0, 1, 1024, -1, 1, 0, false, false, false, false);
                if (i4 == 0) {
                    caVar.KY();
                    caVar.f(strArr[18].toCharArray());
                    caVar.KY();
                    e(caVar);
                }
            }
            if (i4 == 2 || i4 == 3) {
                caVar.a(0, "i", (String) null, String.valueOf(i2));
            }
            if (z2) {
                caVar.a(0, "w", (String) null, com.uc.a.e.RD);
            }
            caVar.cm(true);
        } catch (Exception e) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.uc.c.ca r5, java.lang.String r6, short r7) {
        /*
            r4 = this;
            r4.dC()
            r4.bjM = r6
            r4.bjN = r7
            java.lang.String[] r0 = com.uc.c.ar.avN
            java.lang.String r1 = "ext:bmk"
            r5.fp(r1)     // Catch:{ Exception -> 0x003b, all -> 0x0039 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ Exception -> 0x003b, all -> 0x0039 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x003b, all -> 0x0039 }
            r1.<init>()     // Catch:{ Exception -> 0x003b, all -> 0x0039 }
            java.lang.String r2 = " "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x003b, all -> 0x0039 }
            int r2 = r4.bjN     // Catch:{ Exception -> 0x003b, all -> 0x0039 }
            r3 = -1
            if (r2 != r3) goto L_0x0036
            java.lang.String r2 = ""
        L_0x0023:
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x003b, all -> 0x0039 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x003b, all -> 0x0039 }
            java.lang.String r0 = com.uc.c.bc.Z(r0, r1)     // Catch:{ Exception -> 0x003b, all -> 0x0039 }
            r5.t(r0)     // Catch:{ Exception -> 0x003b, all -> 0x0039 }
            r4.d(r5)     // Catch:{ Exception -> 0x003b, all -> 0x0039 }
        L_0x0035:
            return
        L_0x0036:
            java.lang.String r2 = r4.bjM     // Catch:{ Exception -> 0x003b, all -> 0x0039 }
            goto L_0x0023
        L_0x0039:
            r0 = move-exception
            throw r0
        L_0x003b:
            r0 = move-exception
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.n.a(com.uc.c.ca, java.lang.String, short):void");
    }

    public final void a(at[] atVarArr) {
        at atVar = atVarArr[0];
        pq.setElementAt(atVarArr[1], pq.indexOf(atVar));
        dR();
        if (this.ps != 2) {
            dG();
            return;
        }
        close();
        this.mR.Hc();
    }

    public final at[] aa(int i) {
        int size = pq.size();
        Vector vector = new Vector();
        for (int i2 = 0; i2 < size; i2++) {
            at atVar = (at) pq.elementAt(i2);
            if (atVar.aDj == i) {
                vector.addElement(atVar);
            }
        }
        at[] atVarArr = new at[vector.size()];
        vector.copyInto(atVarArr);
        return atVarArr;
    }

    public final String ab(int i) {
        String str;
        Stack stack = new Stack();
        int i2 = i;
        while (true) {
            if (i2 == -1) {
                str = au.aGF;
                break;
            }
            at Z = Z(i2);
            if (Z == null) {
                str = au.aGF;
                break;
            }
            stack.push(Z.aDq);
            i2 = Z.aDj;
        }
        while (stack.size() != 0) {
            str = str + ((String) stack.pop()) + au.aGF;
        }
        return str;
    }

    public final void ac(int i) {
        this.bjN = i;
        this.bjM = ab(this.bjN);
        dH();
    }

    public final void ae(int i) {
        af(i);
        dR();
    }

    public final void ag(int i) {
        at dK = dK();
        if (dK != null && !g(dK)) {
            int indexOf = pq.indexOf(dK);
            int size = pq.size();
            int i2 = indexOf + i;
            while (i2 > 0 && i2 < size) {
                at atVar = (at) pq.elementAt(i2);
                if (atVar.aDj != dK.aDj || atVar == dK || g(atVar)) {
                    i2 += i;
                } else {
                    pq.setElementAt(atVar, indexOf);
                    pq.setElementAt(dK, i2);
                    dR();
                    return;
                }
            }
        }
    }

    public final void ah(int i) {
        at dK = dK();
        if (dK != null && !g(dK)) {
            pq.removeElement(dK);
            if (i == -1) {
                pq.insertElementAt(dK, 1);
            } else if (i == 1) {
                pq.insertElementAt(dK, pq.size());
            }
            dR();
        }
    }

    public final void b(bf bfVar, bf bfVar2) {
        if (dv()) {
            if (bfVar == this.mR.Lt) {
                dz().d(bfVar, bfVar2);
            } else {
                super.b(bfVar, (bfVar == this.bjJ && (bfVar2 == null || bfVar2 == this.mR.Lv)) ? this.mR.Lv : bfVar2);
            }
        } else if (this.mS != null) {
            this.mS.b(bfVar, bfVar2);
        }
    }

    public final void b(at[] atVarArr) {
        at atVar = atVarArr[0];
        at atVar2 = atVarArr[1];
        u(atVar2.aDi, atVar.aDj);
        pq.removeElement(atVar);
        if (this.ps != 2) {
            ac(atVar2.aDj);
            return;
        }
        close();
        this.mR.Hc();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.n.a(com.uc.c.ca, java.lang.String, short):void
     arg types: [com.uc.c.ca, java.lang.String, int]
     candidates:
      com.uc.c.n.a(java.lang.String, java.util.Vector, short):short
      com.uc.c.n.a(java.util.Vector, boolean, boolean):java.lang.String[]
      com.uc.c.n.a(int, java.lang.String, java.lang.String):void
      com.uc.c.n.a(com.uc.c.ca, java.lang.String, short):void */
    public void c(ca caVar) {
        dE();
        a(caVar, au.aGF, (short) -1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.ca.a(int, java.lang.String, java.lang.String, java.lang.String, int, int, boolean, boolean, boolean):void
     arg types: [int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], java.lang.String, int, int, int, int, int]
     candidates:
      com.uc.c.ca.a(int, byte[], byte[], int, int, boolean, boolean, boolean, boolean):void
      com.uc.c.ca.a(int, byte[], byte[], byte[], byte[], int, int, int, int):void
      com.uc.c.ca.a(int, byte[], byte[], byte[], byte[], byte[], int, int, int):void
      com.uc.c.ca.a(int, java.lang.String, java.lang.String, java.lang.String, int, int, boolean, boolean, boolean):void */
    public void d(ca caVar) {
        caVar.bHD = (byte) (caVar.bHD | 5);
        boolean z = !bc.aL(caVar.bFP, 32);
        if (z) {
            caVar.cl(true);
        }
        int size = pq.size();
        if (this.bjN != -1) {
            at atVar = (at) pq.elementAt(0);
            atVar.aDi = Z(this.bjN).aDj;
            atVar.aDj = this.bjN;
            atVar.aDu = 3;
            cd.MZ().j(caVar, 0);
        }
        caVar.bHl = caVar.bGZ;
        for (int i = 1; i < size; i++) {
            at atVar2 = (at) pq.elementAt(i);
            int hashCode = atVar2.hashCode();
            if (atVar2.aDj == this.bjN) {
                cd.MZ().j(caVar, i);
                if (this.bjS == hashCode && (this.pu == -1 || this.pu == 0)) {
                    caVar.bHl = caVar.bGZ;
                    caVar.bHm = caVar.bGZ.cF - 1;
                    this.bjS = 0;
                }
            }
        }
        if (this.bjN == -1) {
            caVar.a(caVar.a(bc.dT("ext:e:u:bsc"), 1, 0, (byte[]) null, (byte[]) null), (String) null, (String) null, ar.avN[30], caVar.bFQ, 1, false, false, false);
        }
        if (!(this.bjT == -1 || caVar.bHl == null || caVar.bHm == -1 || (this.pu != -1 && this.pu != 0))) {
            int i2 = this.bjT;
            caVar.bGv = i2;
            caVar.bGx = i2;
            this.bjT = -1;
        }
        if (caVar.bHl != null && caVar.bHm == -1) {
            if (caVar.bHl.cF <= 1 || this.bjN == -1) {
                caVar.bHo = 0;
                caVar.bHm = 0;
            } else {
                caVar.bHo = 1;
                caVar.bHm = 1;
            }
        }
        if (z) {
            caVar.cm(true);
            caVar.bGd.a(caVar.bHl, caVar.bHm, 0);
        }
    }

    public final ca dA() {
        return dv() ? this.bjR : dz().jm();
    }

    public void dE() {
        this.bjN = -1;
        dT();
    }

    public final void dF() {
        ac(-1);
    }

    public final void dG() {
        ac(this.bjN);
    }

    public final void dH() {
    }

    public at dK() {
        ca dA = dA();
        return a(dA, ca.c(dA.bHl, dA.bHm));
    }

    public final void dL() {
        close();
        this.mR.aG(w.Pn);
    }

    public final void dM() {
        at dK = dK();
        if (dK != null && !bc.aL(dK.aDu, 2)) {
            ae(dK.aDi);
        }
    }

    public final void dN() {
        for (int size = pq.size() - 1; size >= 0; size--) {
            if (!g((at) pq.elementAt(size))) {
                pq.removeElementAt(size);
            }
        }
        dR();
    }

    public final void dP() {
        s(true);
    }

    public void dR() {
        this.pp = true;
        d(pq, this.pr);
    }

    public void dS() {
        dT();
        if (dz() != null && dz().ch() != null) {
            dz().ch().Em();
        }
    }

    public void dZ() {
    }

    public boolean eb() {
        ca dA = dA();
        byte[] c = ca.c(dA.bHl, dA.bHm);
        return c != null && c[0] == 59;
    }

    public final void f(byte b2) {
        a(b2, (Object) null);
    }

    public final void f(at atVar) {
        pq.addElement(atVar);
        dR();
    }

    public final boolean s(boolean z) {
        ca dA = dA();
        if (!dA.d(dA.bHl, dA.bHm)) {
            return false;
        }
        at dK = dK();
        if (dK == null) {
            return false;
        }
        if ((dK.aDu & 1) == 1) {
            if (bc.aL(dK.aDu, 2)) {
                Y(-1);
            } else {
                Y(1);
            }
            ac(dK.aDi);
        } else if ((dK.aDu & 1) == 0) {
            Y(53);
            b(dK.aDi, z);
        }
        return true;
    }

    public final void u(int i, int i2) {
        Z(i).aDj = (short) i2;
        dR();
    }
}
