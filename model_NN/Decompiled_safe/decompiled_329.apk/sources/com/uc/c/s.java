package com.uc.c;

import android.os.Build;
import b.a.a.c.e;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;

public class s {
    public static final String Ge = "/data/data/com.uc.browser/crash";
    private static final s Gf = new s();
    private static boolean Gg = true;
    public static String Gh;

    private boolean a(byte[] bArr, String str) {
        if (bc.by(Gh)) {
            return false;
        }
        String[] j = ce.j(Gh);
        if (j == null || j.length < 2 || bc.by(j[0]) || bc.by(j[1])) {
            return false;
        }
        e ad = bx.ad(j[0], j[1]);
        try {
            ad.setRequestMethod("POST");
            ad.setRequestProperty("Content-Type", "multipart/form-data; boundary=----------izQ290kHh6g3Yn2IeyJCoc");
            ad.setRequestProperty("Content-Disposition", "form-data; name=\"file\"; filename=" + str);
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("--").append(ad.Xv).append("\r\n");
            stringBuffer.append("Content-Disposition: form-data; name=\"").append("file").append("\";");
            stringBuffer.append(" filename=\"").append(str).append("\"").append("\r\n");
            stringBuffer.append("Content-Type: application/octet-stream").append("\r\n");
            stringBuffer.append("\r\n");
            OutputStream cw = ad.cw();
            cw.write(stringBuffer.toString().getBytes());
            cw.write(bArr);
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append("\r\n--").append(ad.Xv).append("--\r\n");
            cw.write(stringBuffer2.toString().getBytes());
            cw.close();
            return ad.getResponseCode() == 200;
        } catch (IOException e) {
            return false;
        }
    }

    public static s jY() {
        return Gf;
    }

    public void a(Throwable th) {
        String str;
        String str2;
        File[] listFiles;
        if (!bc.by(Gh)) {
            File file = new File(Ge);
            if (file.exists() && file.isDirectory() && (listFiles = file.listFiles()) != null && listFiles.length > 10) {
                bc.m(file);
            }
            if (!file.exists()) {
                file.mkdirs();
            }
            String cls = th.getClass().toString();
            int lastIndexOf = cls.lastIndexOf(46);
            if (lastIndexOf > 0) {
                cls = cls.substring(lastIndexOf + 1);
            }
            String str3 = Build.MODEL;
            String str4 = Build.VERSION.RELEASE;
            try {
                str3 = str3.replaceAll("[^0-9a-zA-Z-.]", "-");
                String replaceAll = str4.replaceAll("[^0-9a-zA-Z-.]", "-");
                str = str3;
                str2 = replaceAll;
            } catch (Exception e) {
                String str5 = str4;
                str = str3;
                str2 = str5;
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(Ge).append("/Android_").append(w.NZ).append("_").append(cls).append("_").append(w.No).append("_").append(System.currentTimeMillis()).append("_").append(str).append("_").append(str2);
            DataOutputStream dataOutputStream = new DataOutputStream(new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(stringBuffer.toString()))));
            dataOutputStream.writeUTF("Crash log ${buildseq}.\n");
            dataOutputStream.writeUTF("Back traces starts.\n");
            String localizedMessage = th.getLocalizedMessage();
            StringBuilder append = new StringBuilder().append("LocalizedMessage: ");
            if (localizedMessage == null) {
                localizedMessage = "NULL";
            }
            dataOutputStream.writeUTF(append.append(localizedMessage).append(bx.bxN).toString());
            String message = th.getMessage();
            StringBuilder append2 = new StringBuilder().append("Message: ");
            if (message == null) {
                message = "NULL";
            }
            dataOutputStream.writeUTF(append2.append(message).append(bx.bxN).toString());
            String th2 = th.toString();
            StringBuilder append3 = new StringBuilder().append("Type: ");
            if (th2 == null) {
                th2 = "NULL";
            }
            dataOutputStream.writeUTF(append3.append(th2).append(bx.bxN).toString());
            for (StackTraceElement stackTraceElement : th.getStackTrace()) {
                dataOutputStream.writeUTF(stackTraceElement.getClassName());
                dataOutputStream.writeUTF(".");
                dataOutputStream.writeUTF(stackTraceElement.getMethodName());
                dataOutputStream.writeUTF(" (");
                dataOutputStream.writeUTF(stackTraceElement.getFileName() == null ? "Unknown Source" : stackTraceElement.getFileName());
                dataOutputStream.writeUTF(cd.bVI);
                dataOutputStream.writeUTF(String.valueOf(stackTraceElement.getLineNumber()));
                dataOutputStream.writeUTF(")");
                dataOutputStream.writeUTF(bx.bxN);
            }
            dataOutputStream.writeUTF("Back traces ends.\n");
            dataOutputStream.close();
        }
    }

    public synchronized void jZ() {
        if (Gg) {
            File file = new File(Ge);
            if (file.exists()) {
                try {
                    for (File file2 : file.listFiles()) {
                        if (file2.isFile()) {
                            int length = (int) file2.length();
                            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file2));
                            byte[] bArr = new byte[length];
                            int i = 0;
                            while (i < length) {
                                int read = bufferedInputStream.read(bArr, i, length - i);
                                if (-1 == read) {
                                    break;
                                }
                                i += read;
                            }
                            a(bArr, file2.getName());
                            bufferedInputStream.close();
                        }
                    }
                    bc.m(file);
                } catch (Throwable th) {
                }
            }
            Gg = false;
        }
    }
}
