package com.uc.c;

import android.graphics.Bitmap;
import b.a.a.a.f;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;

class bu extends Thread {
    final /* synthetic */ r Je;
    private Bitmap bwJ;

    public bu(r rVar, Bitmap bitmap) {
        this.Je = rVar;
        this.bwJ = bitmap;
    }

    public void run() {
        try {
            if (this.Je.DG == null) {
                this.Je.DG = new ByteArrayOutputStream();
            }
            synchronized (this.Je.DG) {
                this.Je.DG.reset();
                f fVar = new f(this.bwJ);
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(this.Je.DG);
                fVar.a(bufferedOutputStream);
                bufferedOutputStream.close();
                this.Je.DG.close();
            }
        } catch (Exception | OutOfMemoryError e) {
        }
        bu unused = this.Je.DH = (bu) null;
    }
}
