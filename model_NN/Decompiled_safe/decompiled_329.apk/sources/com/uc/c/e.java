package com.uc.c;

import android.os.Environment;
import android.os.StatFs;
import com.uc.a.n;
import com.uc.c.a.b;
import com.uc.c.a.f;

class e implements b {
    final /* synthetic */ bb aL;

    e(bb bbVar) {
        this.aL = bbVar;
    }

    public void a(f fVar) {
        n ch;
        if (this.aL.mS != null && (ch = this.aL.mS.ch()) != null) {
            if (this.aL.mR != null && this.aL.mR.JT == 0) {
                ch.qW();
            }
            ch.dP(fVar.Os());
            if (Environment.getExternalStorageState().equals("mounted")) {
                StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
                if (1 * ((long) statFs.getBlockSize()) * ((long) statFs.getAvailableBlocks()) < 1042441) {
                    ch.rd();
                }
            }
        }
    }

    public void a(f fVar, int i) {
        if (this.aL.mS != null && this.aL.mS.ch() != null) {
            this.aL.mS.ch().q(new Object[]{Integer.valueOf(fVar.Os()), fVar.getFileName(), Integer.valueOf(i)});
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.a.n.j(int, java.lang.String):void
     arg types: [short, java.lang.String]
     candidates:
      com.uc.a.n.j(int, java.lang.Object):void
      com.uc.a.n.j(int, java.lang.String):void */
    public void b(f fVar) {
        this.aL.gg(2);
        if (!(this.aL.mS == null || this.aL.mS.ch() == null)) {
            this.aL.mS.ch().bR(fVar.Oy() + fVar.getFileName());
            this.aL.mS.ch().rc();
            this.aL.mS.ch().j((int) fVar.Os(), fVar.getFileName());
        }
        this.aL.q(fVar);
    }

    public void ba() {
        if (this.aL.mS != null && this.aL.mS.ch() != null && this.aL.mR != null && this.aL.mR.JT == 0) {
            this.aL.mS.ch().qW();
        }
    }

    public void c(f fVar) {
    }

    public void d(f fVar) {
    }

    public void e(f fVar) {
        if (this.aL.mS.ch() != null) {
            this.aL.mS.ch().i(fVar.Os(), fVar.getFileName());
        }
    }
}
