package com.uc.c;

import b.a.a.a.f;
import b.a.a.a.t;
import b.a.a.d;
import b.b.a;
import com.uc.browser.UCR;
import com.uc.zip.TZipInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Hashtable;
import java.util.Vector;
import java.util.zip.GZIPInputStream;

public class k {
    public static final byte nA = 11;
    public static final byte nB = 12;
    public static final byte nC = 13;
    public static final byte nD = 14;
    public static final byte nE = 4;
    public static final byte nF = 8;
    public static final byte nG = 1;
    public static final byte nH = 4;
    public static final byte nI = 3;
    public static final byte nJ = 3;
    static final long nK = 400;
    static final long nL = 400;
    static final long nM = 2000;
    private static final byte nN = 100;
    private static final byte nO = 80;
    private static final byte nP = 60;
    private static final byte nQ = 40;
    private static final byte nR = 20;
    private static final byte nU = 1;
    private static final byte nV = 2;
    private static final byte nW = 3;
    private static final byte nX = 4;
    private static final byte nY = 5;
    private static final byte nZ = 6;
    public static final int nd = 50001;
    public static final int ng = 150;
    public static final byte nh = 0;
    public static final byte ni = 1;
    public static final byte nj = 2;
    public static final byte nk = 3;
    public static final byte nl = 4;
    public static final byte nm = 5;
    public static final byte nn = 1;
    public static final byte no = 2;
    public static final byte np = 3;
    public static final byte nq = 1;
    public static final byte nr = 2;
    public static final byte ns = 3;
    public static final byte nt = 4;
    public static final byte nu = 5;
    public static final byte nv = 6;
    public static final byte nw = 7;
    public static final byte nx = 8;
    public static final byte ny = 9;
    public static final byte nz = 10;
    private static final byte oa = 7;
    private static final byte ob = 8;
    private static final byte oc = 9;
    private static final byte od = 10;
    private static final byte oe = 11;
    private static final byte of = 12;
    private static final byte og = 13;
    private static final byte oh = 14;
    private static final byte oi = 32;
    private static final byte oj = 33;
    private static final byte ol = 34;
    private static final byte om = 35;
    private static final byte on = 36;
    private static final byte oo = 37;
    private static final byte op = 38;
    private static final byte oq = 39;
    private static final byte or = 40;
    private static final byte os = 112;
    private static final byte ot = 113;
    private static final byte ou = 114;
    private static final byte ov = 115;
    private static Vector ow = null;
    private ca dt;
    private DataInputStream mL;
    private DataInputStream mM;
    private cj mN;
    private int mO;
    private int mP;
    private int mQ;
    private w mR;
    private r mS;
    public bx mT;
    private String[] mU;
    int mV;
    private ad mW;
    private bt mX;
    private Hashtable mY;
    public int mZ;
    private Hashtable nS;
    private Hashtable nT;
    private Hashtable na;
    private String nb;
    boolean nc;
    TZipInputStream ne;
    private long nf;

    public k(w wVar, r rVar, ca caVar, DataInputStream dataInputStream, cj cjVar) {
        this(wVar, rVar, caVar, dataInputStream, cjVar, 0);
    }

    public k(w wVar, r rVar, ca caVar, DataInputStream dataInputStream, cj cjVar, int i) {
        this.dt = null;
        this.mL = null;
        this.mM = null;
        this.mN = null;
        this.mO = 1;
        this.mP = 1;
        this.mQ = 0;
        this.mR = null;
        this.mS = null;
        this.mT = null;
        this.mU = null;
        this.mV = -1;
        this.mW = null;
        this.mX = null;
        this.mY = null;
        this.mZ = 0;
        this.na = null;
        this.nb = null;
        this.nc = false;
        this.ne = null;
        this.nf = 0;
        this.nS = null;
        this.nT = new Hashtable();
        this.dt = caVar;
        this.mL = dataInputStream;
        this.mN = cjVar;
        this.mR = wVar;
        this.mS = rVar;
        this.mZ = i;
    }

    private static int R(int i) {
        return i & 7;
    }

    private static int S(int i) {
        return i >> 3;
    }

    private static int T(int i) {
        return (i << 1) ^ (i >> 31);
    }

    private static int U(int i) {
        return (i >>> 1) ^ (-(i & 1));
    }

    private static int V(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (-268435456 & i) == 0 ? 4 : 5;
    }

    private static int W(int i) {
        if (i < 0) {
            return 5;
        }
        return V(i);
    }

    private Object X(int i) {
        int size;
        if (i == -1 || ow == null || (size = ow.size()) == 0) {
            return null;
        }
        for (int i2 = size - 1; i2 >= 0; i2--) {
            Object[] objArr = (Object[]) ow.elementAt(i2);
            if (((Integer) objArr[0]).intValue() == i) {
                return objArr[1];
            }
        }
        return null;
    }

    private final byte a(DataInputStream dataInputStream, byte[][] bArr, int[] iArr, byte[] bArr2, boolean[] zArr, short[] sArr, byte[] bArr3) {
        for (int i = 0; i < bArr3.length; i++) {
            bArr3[i] = 0;
        }
        for (int i2 = 0; i2 < bArr.length; i2++) {
            bArr[i2] = null;
        }
        for (int i3 = 0; i3 < iArr.length; i3++) {
            iArr[i3] = 0;
        }
        for (int i4 = 0; i4 < bArr2.length; i4++) {
            bArr2[i4] = 0;
        }
        for (int i5 = 0; i5 < zArr.length; i5++) {
            zArr[i5] = false;
        }
        for (int i6 = 0; i6 < sArr.length; i6++) {
            sArr[i6] = 0;
        }
        byte readByte = dataInputStream.readByte();
        int readUnsignedShort = dataInputStream.readUnsignedShort();
        while (readUnsignedShort > 0) {
            byte readByte2 = dataInputStream.readByte();
            readUnsignedShort--;
            if (readByte2 == -1) {
                break;
            } else if (readByte2 >= 100) {
                byte b2 = (byte) (readByte2 - 100);
                short readShort = dataInputStream.readShort();
                readUnsignedShort -= 2;
                if (b2 >= 0 && b2 < sArr.length) {
                    sArr[b2] = readShort;
                    bArr3[1] = (byte) ((1 << b2) | bArr3[1]);
                }
            } else if (readByte2 >= 80) {
                byte b3 = (byte) (readByte2 - nO);
                byte[] a2 = bc.a(dataInputStream);
                readUnsignedShort -= a2.length + 2;
                if (b3 >= 0 && b3 < bArr.length) {
                    bArr[b3] = a2;
                    bArr3[4] = (byte) ((1 << b3) | bArr3[4]);
                }
            } else if (readByte2 >= 60) {
                byte b4 = (byte) (readByte2 - 60);
                int readInt = dataInputStream.readInt();
                readUnsignedShort -= 4;
                if (b4 >= 0 && b4 < iArr.length) {
                    iArr[b4] = readInt;
                    bArr3[0] = (byte) ((1 << b4) | bArr3[0]);
                }
            } else if (readByte2 >= 40) {
                byte b5 = (byte) (readByte2 - 40);
                if (b5 >= 0 && b5 < zArr.length) {
                    zArr[b5] = true;
                    bArr3[3] = (byte) ((1 << b5) | bArr3[3]);
                }
            } else if (readByte2 >= 20) {
                byte b6 = (byte) (readByte2 - 20);
                byte readByte3 = dataInputStream.readByte();
                readUnsignedShort--;
                if (b6 >= 0 && b6 < bArr2.length) {
                    bArr2[b6] = readByte3;
                    bArr3[2] = (byte) ((1 << b6) | bArr3[2]);
                }
            } else if (readByte2 >= 0) {
                int[] iArr2 = new int[dataInputStream.readInt()];
                readUnsignedShort -= 4;
                for (int i7 = 0; i7 < iArr2.length; i7++) {
                    iArr2[i7] = dataInputStream.readInt();
                    readUnsignedShort -= 4;
                }
            }
        }
        return readByte;
    }

    private static int a(int i, at atVar) {
        int a2 = a(atVar);
        return a2 + p(i, -1) + V(a2);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private static int a(at atVar) {
        int p;
        int i;
        at atVar2;
        int i2 = 0;
        int d = d(atVar);
        for (int i3 = 0; i3 < d; i3++) {
            at b2 = b(atVar, i3);
            if (b2 != null) {
                int i4 = i2;
                while (true) {
                    Object obj = b2.aDv;
                    if (obj == null) {
                        atVar2 = b2.aDp;
                        i = i4;
                    } else {
                        switch (b2.aDu) {
                            case 1:
                                if (obj instanceof Integer) {
                                    p = q(b2.aDi, ((Integer) obj).intValue()) + i4;
                                    break;
                                }
                                p = i4;
                                break;
                            case 2:
                                if (obj instanceof Integer) {
                                    p = r(b2.aDi, ((Integer) obj).intValue()) + i4;
                                    break;
                                }
                                p = i4;
                                break;
                            case 3:
                                if (obj instanceof Integer) {
                                    p = s(b2.aDi, ((Integer) obj).intValue()) + i4;
                                    break;
                                }
                                p = i4;
                                break;
                            case 4:
                                if (obj instanceof Integer) {
                                    p = p(b2.aDi, -1) + i4 + 4;
                                    break;
                                }
                                p = i4;
                                break;
                            case 5:
                                if (obj instanceof Integer) {
                                    p = p(b2.aDi, -1) + i4 + 4;
                                    break;
                                }
                                p = i4;
                                break;
                            case 6:
                            case 7:
                            case 8:
                            default:
                                p = i4;
                                break;
                            case 9:
                                if (obj instanceof Long) {
                                    p = p(b2.aDi, -1) + i4 + 8;
                                    break;
                                }
                                p = i4;
                                break;
                            case 10:
                                if (obj instanceof Long) {
                                    p = p(b2.aDi, -1) + i4 + 8;
                                    break;
                                }
                                p = i4;
                                break;
                            case 11:
                                if (obj instanceof Boolean) {
                                    p = p(b2.aDi, -1) + i4 + 1;
                                    break;
                                }
                                p = i4;
                                break;
                            case 12:
                                if (obj instanceof String) {
                                    p = c(b2.aDi, (String) obj) + i4;
                                    break;
                                }
                                p = i4;
                                break;
                            case 13:
                                if (obj instanceof byte[]) {
                                    p = c(b2.aDi, (byte[]) obj) + i4;
                                    break;
                                }
                                p = i4;
                                break;
                            case 14:
                                p = a(b2.aDi, b2) + i4;
                                break;
                        }
                        at atVar3 = b2.aDp;
                        i = p;
                        atVar2 = atVar3;
                    }
                    if (atVar2 == null) {
                        i2 = i;
                    } else {
                        i4 = i;
                        b2 = atVar2;
                    }
                }
            }
        }
        return i2;
    }

    private int a(bx bxVar) {
        int i;
        int i2 = 0;
        if (this.mT != null && this.mT.byD == 2) {
            this.mT.Ho();
        }
        if (bxVar == null || bxVar.byE == null) {
            return 0;
        }
        if (bxVar.byE.size() > 0 && az.bbW != 0) {
            this.mT.hB(3);
            try {
                if (this.dt.bHk[2] > 0) {
                    this.dt.wx();
                }
            } catch (Throwable th) {
            }
        }
        bxVar.byG = 1;
        long currentTimeMillis = System.currentTimeMillis();
        int i3 = 0;
        while (bxVar.byG != 2 && bxVar.byv != 0) {
            try {
                if (System.currentTimeMillis() - currentTimeMillis > 500 && bxVar.bzt != i2) {
                    this.dt.wx();
                    i3 = 1;
                    int i4 = bxVar.bzt;
                    currentTimeMillis = System.currentTimeMillis();
                    i2 = i4;
                }
                Thread.sleep(50);
            } catch (Throwable th2) {
                i = i3;
            }
        }
        i = i3;
        Thread.yield();
        return i;
    }

    public static at a(at atVar, int i) {
        Object[] objArr = (Object[]) atVar.aDv;
        int length = objArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (objArr[i2] instanceof at) {
                at atVar2 = (at) objArr[i2];
                if (i == atVar2.aDi) {
                    return atVar2;
                }
            }
        }
        return null;
    }

    public static Vector a(Hashtable hashtable, String str, at atVar) {
        return a(hashtable, str, atVar, null);
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x014c A[LOOP:0: B:5:0x0011->B:111:0x014c, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x0044 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0099 A[Catch:{ Exception -> 0x00ff, all -> 0x011c, Exception -> 0x0134 }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00a6 A[Catch:{ Exception -> 0x00ff, all -> 0x011c, Exception -> 0x0134 }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00ac A[SYNTHETIC, Splitter:B:54:0x00ac] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00b1 A[Catch:{ Exception -> 0x013c }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00b6 A[Catch:{ Exception -> 0x013c }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Vector a(java.util.Hashtable r13, java.lang.String r14, com.uc.c.at r15, com.uc.c.w r16) {
        /*
            r0 = 0
            boolean r1 = r13.containsKey(r14)     // Catch:{ Exception -> 0x0134 }
            if (r1 == 0) goto L_0x0135
            java.lang.Object r13 = r13.get(r14)     // Catch:{ Exception -> 0x0134 }
            com.uc.c.at r13 = (com.uc.c.at) r13     // Catch:{ Exception -> 0x0134 }
            com.uc.c.at r13 = (com.uc.c.at) r13     // Catch:{ Exception -> 0x0134 }
            r1 = r15
            r15 = r13
        L_0x0011:
            java.lang.Object r13 = r15.aDv     // Catch:{ Exception -> 0x0134 }
            byte[] r13 = (byte[]) r13     // Catch:{ Exception -> 0x0134 }
            byte[] r13 = (byte[]) r13     // Catch:{ Exception -> 0x0134 }
            if (r13 == 0) goto L_0x001d
            int r2 = r13.length     // Catch:{ Exception -> 0x0134 }
            r3 = 1
            if (r2 > r3) goto L_0x0046
        L_0x001d:
            if (r16 == 0) goto L_0x0149
            com.uc.c.cd r13 = com.uc.c.cd.MZ()     // Catch:{ Exception -> 0x0134 }
            com.uc.c.at r13 = r13.fF(r14)     // Catch:{ Exception -> 0x0134 }
        L_0x0027:
            if (r13 != 0) goto L_0x0146
            com.uc.c.at r13 = com.uc.c.az.dA(r14)     // Catch:{ Exception -> 0x0134 }
            r15 = r13
        L_0x002e:
            if (r15 != 0) goto L_0x0032
            r13 = 0
        L_0x0031:
            return r13
        L_0x0032:
            java.lang.Object r13 = r15.aDv     // Catch:{ Exception -> 0x0134 }
            byte[] r13 = (byte[]) r13     // Catch:{ Exception -> 0x0134 }
            byte[] r13 = (byte[]) r13     // Catch:{ Exception -> 0x0134 }
            if (r13 == 0) goto L_0x003e
            int r2 = r13.length     // Catch:{ Exception -> 0x0134 }
            r3 = 1
            if (r2 > r3) goto L_0x0046
        L_0x003e:
            com.uc.c.at r13 = r15.aDp     // Catch:{ Exception -> 0x0134 }
            r15 = r0
            r0 = r1
        L_0x0042:
            if (r13 != 0) goto L_0x014c
            r13 = r15
            goto L_0x0031
        L_0x0046:
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            byte r6 = r15.aDt     // Catch:{ Exception -> 0x013f }
            r7 = 10
            if (r6 != r7) goto L_0x0052
            r5 = 8
        L_0x0052:
            java.io.ByteArrayInputStream r6 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x013f }
            int r7 = r13.length     // Catch:{ Exception -> 0x013f }
            byte r8 = r15.aDt     // Catch:{ Exception -> 0x013f }
            int r7 = r7 - r8
            r6.<init>(r13, r5, r7)     // Catch:{ Exception -> 0x013f }
            boolean r2 = r15.aDy     // Catch:{ Exception -> 0x0090 }
            if (r2 == 0) goto L_0x00f8
            java.util.zip.GZIPInputStream r2 = new java.util.zip.GZIPInputStream     // Catch:{ Exception -> 0x0090 }
            r2.<init>(r6)     // Catch:{ Exception -> 0x0090 }
            java.io.ByteArrayOutputStream r7 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0090 }
            r7.<init>()     // Catch:{ Exception -> 0x0090 }
            java.io.DataOutputStream r8 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x0090 }
            r8.<init>(r7)     // Catch:{ Exception -> 0x0090 }
            r9 = 0
            byte r10 = r15.aDt     // Catch:{ Exception -> 0x0090 }
            r11 = 10
            if (r10 != r11) goto L_0x007a
            r9 = 0
            r8.write(r13, r9, r5)     // Catch:{ Exception -> 0x0090 }
            r9 = r5
        L_0x007a:
            r10 = 1024(0x400, float:1.435E-42)
            byte[] r10 = new byte[r10]     // Catch:{ Exception -> 0x0090 }
        L_0x007e:
            r11 = -1
            if (r11 == r9) goto L_0x00bd
            r9 = 0
            r11 = 1024(0x400, float:1.435E-42)
            int r9 = r2.read(r10, r9, r11)     // Catch:{ Exception -> 0x0090 }
            r11 = -1
            if (r9 == r11) goto L_0x007e
            r11 = 0
            r8.write(r10, r11, r9)     // Catch:{ Exception -> 0x0090 }
            goto L_0x007e
        L_0x0090:
            r13 = move-exception
            r13 = r6
        L_0x0092:
            r2 = r13
            r13 = r3
        L_0x0094:
            b(r13, r1)     // Catch:{ Exception -> 0x00ff, all -> 0x011c }
            if (r0 != 0) goto L_0x009f
            java.util.Vector r3 = new java.util.Vector     // Catch:{ Exception -> 0x00ff, all -> 0x011c }
            r3.<init>()     // Catch:{ Exception -> 0x00ff, all -> 0x011c }
            r0 = r3
        L_0x009f:
            r0.addElement(r1)     // Catch:{ Exception -> 0x00ff, all -> 0x011c }
            com.uc.c.at r15 = r15.aDp     // Catch:{ Exception -> 0x0134 }
            if (r15 == 0) goto L_0x00aa
            com.uc.c.at r1 = c(r1)     // Catch:{ Exception -> 0x0134 }
        L_0x00aa:
            if (r2 == 0) goto L_0x00af
            r2.close()     // Catch:{ Exception -> 0x013c }
        L_0x00af:
            if (r4 == 0) goto L_0x00b4
            r4.close()     // Catch:{ Exception -> 0x013c }
        L_0x00b4:
            if (r13 == 0) goto L_0x00b9
            r13.close()     // Catch:{ Exception -> 0x013c }
        L_0x00b9:
            r13 = r15
            r15 = r0
            r0 = r1
            goto L_0x0042
        L_0x00bd:
            int r2 = r13.length     // Catch:{ Exception -> 0x0090 }
            int r2 = r2 + r5
            byte r9 = r15.aDt     // Catch:{ Exception -> 0x0090 }
            int r2 = r2 - r9
            byte r9 = r15.aDt     // Catch:{ Exception -> 0x0090 }
            int r9 = r9 - r5
            r8.write(r13, r2, r9)     // Catch:{ Exception -> 0x0090 }
            byte r13 = r15.aDu     // Catch:{ Exception -> 0x0090 }
            r2 = 1
            if (r13 == r2) goto L_0x00f4
            byte[] r13 = r7.toByteArray()     // Catch:{ Exception -> 0x0090 }
            r15.aDv = r13     // Catch:{ Exception -> 0x0090 }
        L_0x00d3:
            r13 = 0
            r15.aDy = r13     // Catch:{ Exception -> 0x0090 }
            r13 = 1
            com.uc.c.w.aa(r13)     // Catch:{ Exception -> 0x0090 }
            java.io.ByteArrayInputStream r13 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x0090 }
            byte[] r2 = r7.toByteArray()     // Catch:{ Exception -> 0x0090 }
            byte[] r7 = r7.toByteArray()     // Catch:{ Exception -> 0x0090 }
            int r7 = r7.length     // Catch:{ Exception -> 0x0090 }
            byte r8 = r15.aDt     // Catch:{ Exception -> 0x0090 }
            int r7 = r7 - r8
            r13.<init>(r2, r5, r7)     // Catch:{ Exception -> 0x0090 }
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ Exception -> 0x0143 }
            r2.<init>(r13)     // Catch:{ Exception -> 0x0143 }
            r12 = r2
            r2 = r13
            r13 = r12
            goto L_0x0094
        L_0x00f4:
            r13 = 0
            r15.aDv = r13     // Catch:{ Exception -> 0x0090 }
            goto L_0x00d3
        L_0x00f8:
            java.io.DataInputStream r13 = new java.io.DataInputStream     // Catch:{ Exception -> 0x0090 }
            r13.<init>(r6)     // Catch:{ Exception -> 0x0090 }
            r2 = r6
            goto L_0x0094
        L_0x00ff:
            r3 = move-exception
            com.uc.c.at r15 = r15.aDp     // Catch:{ Exception -> 0x0134 }
            if (r15 == 0) goto L_0x0108
            com.uc.c.at r1 = c(r1)     // Catch:{ Exception -> 0x0134 }
        L_0x0108:
            if (r2 == 0) goto L_0x010d
            r2.close()     // Catch:{ Exception -> 0x013a }
        L_0x010d:
            if (r4 == 0) goto L_0x0112
            r4.close()     // Catch:{ Exception -> 0x013a }
        L_0x0112:
            if (r13 == 0) goto L_0x0117
            r13.close()     // Catch:{ Exception -> 0x013a }
        L_0x0117:
            r13 = r15
            r15 = r0
            r0 = r1
            goto L_0x0042
        L_0x011c:
            r14 = move-exception
            com.uc.c.at r15 = r15.aDp     // Catch:{ Exception -> 0x0134 }
            if (r15 == 0) goto L_0x0124
            c(r1)     // Catch:{ Exception -> 0x0134 }
        L_0x0124:
            if (r2 == 0) goto L_0x0129
            r2.close()     // Catch:{ Exception -> 0x0138 }
        L_0x0129:
            if (r4 == 0) goto L_0x012e
            r4.close()     // Catch:{ Exception -> 0x0138 }
        L_0x012e:
            if (r13 == 0) goto L_0x0133
            r13.close()     // Catch:{ Exception -> 0x0138 }
        L_0x0133:
            throw r14     // Catch:{ Exception -> 0x0134 }
        L_0x0134:
            r13 = move-exception
        L_0x0135:
            r13 = 0
            goto L_0x0031
        L_0x0138:
            r13 = move-exception
            goto L_0x0133
        L_0x013a:
            r13 = move-exception
            goto L_0x0117
        L_0x013c:
            r13 = move-exception
            goto L_0x00b9
        L_0x013f:
            r13 = move-exception
            r13 = r2
            goto L_0x0092
        L_0x0143:
            r2 = move-exception
            goto L_0x0092
        L_0x0146:
            r15 = r13
            goto L_0x002e
        L_0x0149:
            r13 = r15
            goto L_0x0027
        L_0x014c:
            r1 = r0
            r0 = r15
            r15 = r13
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.k.a(java.util.Hashtable, java.lang.String, com.uc.c.at, com.uc.c.w):java.util.Vector");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x005b, code lost:
        com.uc.c.aa.rH().t(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x006f, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0070, code lost:
        r9 = r1;
        r1 = r0;
        r0 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005a A[ExcHandler: Throwable (th java.lang.Throwable), PHI: r0 
      PHI: (r0v2 java.lang.Long) = (r0v3 java.lang.Long), (r0v3 java.lang.Long), (r0v3 java.lang.Long), (r0v0 java.lang.Long), (r0v0 java.lang.Long) binds: [B:12:0x0041, B:9:0x0023, B:10:?, B:3:0x0005, B:4:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:3:0x0005] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void a(int r11, int r12, int r13, int r14, int r15) {
        /*
            r10 = this;
            r0 = 0
            if (r11 == 0) goto L_0x001a
            if (r12 == 0) goto L_0x001a
            java.lang.Long r1 = new java.lang.Long     // Catch:{ Throwable -> 0x005a, all -> 0x0063 }
            long r2 = (long) r12     // Catch:{ Throwable -> 0x005a, all -> 0x0063 }
            r4 = -1
            long r2 = r2 & r4
            r4 = 32
            long r2 = r2 << r4
            long r4 = (long) r11     // Catch:{ Throwable -> 0x005a, all -> 0x0063 }
            r6 = 4294967295(0xffffffff, double:2.1219957905E-314)
            long r4 = r4 & r6
            long r2 = r2 | r4
            r1.<init>(r2)     // Catch:{ Throwable -> 0x005a, all -> 0x0063 }
            r0 = r1
        L_0x001a:
            if (r0 == 0) goto L_0x0039
            long r1 = (long) r15
            r3 = 0
            int r3 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r3 <= 0) goto L_0x0041
            java.util.Hashtable r3 = r10.na     // Catch:{ Throwable -> 0x005a, all -> 0x006f }
            java.lang.String r4 = java.lang.Integer.toString(r13)     // Catch:{ Throwable -> 0x005a, all -> 0x006f }
            r5 = 2
            long[] r5 = new long[r5]     // Catch:{ Throwable -> 0x005a, all -> 0x006f }
            r6 = 0
            long r7 = r0.longValue()     // Catch:{ Throwable -> 0x005a, all -> 0x006f }
            r5[r6] = r7     // Catch:{ Throwable -> 0x005a, all -> 0x006f }
            r6 = 1
            r5[r6] = r1     // Catch:{ Throwable -> 0x005a, all -> 0x006f }
            r3.put(r4, r5)     // Catch:{ Throwable -> 0x005a, all -> 0x006f }
        L_0x0039:
            com.uc.c.aa r1 = com.uc.c.aa.rH()
            r1.t(r0)
        L_0x0040:
            return
        L_0x0041:
            com.uc.c.aa r1 = com.uc.c.aa.rH()     // Catch:{ Throwable -> 0x005a, all -> 0x006f }
            java.lang.Object[] r1 = r1.r(r0)     // Catch:{ Throwable -> 0x005a, all -> 0x006f }
            if (r1 == 0) goto L_0x0039
            com.uc.c.ca r2 = r10.dt     // Catch:{ Throwable -> 0x005a, all -> 0x006f }
            com.uc.c.i r2 = r2.bGd     // Catch:{ Throwable -> 0x005a, all -> 0x006f }
            r3 = 0
            r10 = r1[r3]     // Catch:{ Throwable -> 0x005a, all -> 0x006f }
            byte[] r10 = (byte[]) r10     // Catch:{ Throwable -> 0x005a, all -> 0x006f }
            byte[] r10 = (byte[]) r10     // Catch:{ Throwable -> 0x005a, all -> 0x006f }
            r2.a(r14, r10)     // Catch:{ Throwable -> 0x005a, all -> 0x006f }
            goto L_0x0039
        L_0x005a:
            r1 = move-exception
            com.uc.c.aa r1 = com.uc.c.aa.rH()
            r1.t(r0)
            goto L_0x0040
        L_0x0063:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x0067:
            com.uc.c.aa r2 = com.uc.c.aa.rH()
            r2.t(r1)
            throw r0
        L_0x006f:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0067
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.k.a(int, int, int, int, int):void");
    }

    private static void a(int i, int i2, DataOutputStream dataOutputStream) {
        b(o(i, i2), dataOutputStream);
    }

    private static void a(int i, long j, DataOutputStream dataOutputStream) {
        a(i, 1, dataOutputStream);
        a(j, dataOutputStream);
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x008d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(int r6, com.uc.c.at r7, java.io.DataInputStream r8) {
        /*
            r3 = 0
            if (r7 == 0) goto L_0x000f
            int r0 = R(r6)
            byte r1 = r7.aDu
            byte r1 = e(r1)
            if (r0 == r1) goto L_0x0013
        L_0x000f:
            a(r8, r6)
        L_0x0012:
            return
        L_0x0013:
            r0 = 0
            byte r1 = r7.aDt
            r2 = 3
            if (r1 != r2) goto L_0x0028
            r1 = 1
        L_0x001a:
            if (r1 == 0) goto L_0x003c
            boolean r2 = r7.aDy
            if (r2 != 0) goto L_0x003a
            r0 = r7
        L_0x0021:
            com.uc.c.at r2 = r0.aDp
            if (r2 == 0) goto L_0x002a
            com.uc.c.at r0 = r0.aDp
            goto L_0x0021
        L_0x0028:
            r1 = r3
            goto L_0x001a
        L_0x002a:
            com.uc.c.at r2 = b(r7)
        L_0x002e:
            byte r3 = r2.aDu
            switch(r3) {
                case 1: goto L_0x005d;
                case 2: goto L_0x005d;
                case 3: goto L_0x0069;
                case 4: goto L_0x0075;
                case 5: goto L_0x0075;
                case 6: goto L_0x0033;
                case 7: goto L_0x0033;
                case 8: goto L_0x0033;
                case 9: goto L_0x008d;
                case 10: goto L_0x008d;
                case 11: goto L_0x0081;
                case 12: goto L_0x0042;
                case 13: goto L_0x0042;
                case 14: goto L_0x003e;
                default: goto L_0x0033;
            }
        L_0x0033:
            if (r1 == 0) goto L_0x0012
            if (r0 == 0) goto L_0x0012
            r0.aDp = r2
            goto L_0x0012
        L_0x003a:
            r7.aDy = r3
        L_0x003c:
            r2 = r7
            goto L_0x002e
        L_0x003e:
            a(r8, r2)
            goto L_0x0033
        L_0x0042:
            int r3 = h(r8)
            byte[] r3 = new byte[r3]
            a(r8, r3)
            byte r4 = r2.aDu
            r5 = 13
            if (r4 != r5) goto L_0x0054
            r2.aDv = r3
            goto L_0x0033
        L_0x0054:
            java.lang.String r3 = com.uc.c.bc.ak(r3)     // Catch:{ IOException -> 0x005b }
            r2.aDv = r3     // Catch:{ IOException -> 0x005b }
            goto L_0x0033
        L_0x005b:
            r3 = move-exception
            goto L_0x0033
        L_0x005d:
            java.lang.Integer r3 = new java.lang.Integer
            int r4 = c(r8)
            r3.<init>(r4)
            r2.aDv = r3
            goto L_0x0033
        L_0x0069:
            java.lang.Integer r3 = new java.lang.Integer
            int r4 = d(r8)
            r3.<init>(r4)
            r2.aDv = r3
            goto L_0x0033
        L_0x0075:
            java.lang.Integer r3 = new java.lang.Integer
            int r4 = e(r8)
            r3.<init>(r4)
            r2.aDv = r3
            goto L_0x0033
        L_0x0081:
            java.lang.Boolean r3 = new java.lang.Boolean
            boolean r4 = f(r8)
            r3.<init>(r4)
            r2.aDv = r3
            goto L_0x0033
        L_0x008d:
            java.lang.Long r3 = new java.lang.Long
            long r4 = b(r8)
            r3.<init>(r4)
            r2.aDv = r3
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.k.a(int, com.uc.c.at, java.io.DataInputStream):void");
    }

    private static void a(int i, at atVar, DataOutputStream dataOutputStream) {
        int a2 = a(atVar);
        a(i, 2, dataOutputStream);
        a(a2, dataOutputStream);
        a(dataOutputStream, atVar);
    }

    private static void a(int i, DataInputStream dataInputStream) {
        dataInputStream.skip((long) i);
    }

    private static void a(int i, DataOutputStream dataOutputStream) {
        if ((Integer.MIN_VALUE & i) == 0) {
            byte[] bArr = new byte[5];
            bArr[0] = (byte) (i | 128);
            int i2 = 0 + 1;
            if (i >= 128) {
                bArr[1] = (byte) ((i >> 7) | 128);
                i2++;
                if (i >= 16384) {
                    bArr[2] = (byte) ((i >> 14) | 128);
                    i2++;
                    if (i >= 2097152) {
                        bArr[3] = (byte) ((i >> 21) | 128);
                        i2++;
                        if (i >= 268435456) {
                            bArr[4] = (byte) (i >> 28);
                            i2++;
                        } else {
                            bArr[3] = (byte) (bArr[3] & Byte.MAX_VALUE);
                        }
                    } else {
                        bArr[2] = (byte) (bArr[2] & Byte.MAX_VALUE);
                    }
                } else {
                    bArr[1] = (byte) (bArr[1] & Byte.MAX_VALUE);
                }
            } else {
                bArr[0] = (byte) (bArr[0] & Byte.MAX_VALUE);
            }
            dataOutputStream.write(bArr, 0, i2);
            return;
        }
        int i3 = i;
        while ((((long) i3) & -128) != 0) {
            dataOutputStream.write((i3 & 127) | 128);
            i3 >>>= 7;
        }
        dataOutputStream.write(i3);
    }

    private static void a(int i, String str, DataOutputStream dataOutputStream) {
        a(i, bc.dT(str), dataOutputStream);
    }

    private static void a(int i, boolean z, DataOutputStream dataOutputStream) {
        int i2 = 0;
        a(i, 0, dataOutputStream);
        if (z) {
            i2 = 1;
        }
        a(i2, dataOutputStream);
    }

    private static void a(int i, byte[] bArr, DataOutputStream dataOutputStream) {
        a(i, 2, dataOutputStream);
        a(bArr.length, dataOutputStream);
        a(bArr, dataOutputStream);
    }

    private static void a(long j, DataOutputStream dataOutputStream) {
        dataOutputStream.write(((int) j) & 255);
        dataOutputStream.write(((int) (j >> 8)) & 255);
        dataOutputStream.write(((int) (j >> 16)) & 255);
        dataOutputStream.write(((int) (j >> 24)) & 255);
        dataOutputStream.write(((int) (j >> 32)) & 255);
        dataOutputStream.write(((int) (j >> 40)) & 255);
        dataOutputStream.write(((int) (j >> 48)) & 255);
        dataOutputStream.write(((int) (j >> 56)) & 255);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x006a, code lost:
        r2 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x006e, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x006f, code lost:
        r0 = r0.aDp;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0071, code lost:
        throw r1;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x006e A[ExcHandler:  FINALLY, Splitter:B:3:0x000b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.uc.c.at r9, int r10, boolean r11) {
        /*
            r8 = this;
            r7 = 0
            r6 = 0
            r5 = 1
            com.uc.c.at r0 = b(r9, r10)
            r1 = r11
        L_0x0008:
            if (r0 == 0) goto L_0x0072
            r2 = 0
            com.uc.c.at r2 = b(r0, r2)     // Catch:{ Exception -> 0x0065, all -> 0x006e }
            java.lang.Object r3 = r2.aDv     // Catch:{ Exception -> 0x0065, all -> 0x006e }
            boolean r3 = r3 instanceof java.lang.String     // Catch:{ Exception -> 0x0065, all -> 0x006e }
            if (r3 == 0) goto L_0x003e
            java.lang.Object r8 = r2.aDv     // Catch:{ Exception -> 0x0065, all -> 0x006e }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ Exception -> 0x0065, all -> 0x006e }
            r2 = r8
        L_0x001a:
            boolean r3 = com.uc.c.bc.dM(r2)     // Catch:{ Exception -> 0x0065, all -> 0x006e }
            if (r3 == 0) goto L_0x003b
            r3 = 1
            com.uc.c.at r3 = b(r0, r3)     // Catch:{ Exception -> 0x0065, all -> 0x006e }
            java.lang.String r4 = "sn2"
            boolean r4 = r4.equals(r2)     // Catch:{ Exception -> 0x0065, all -> 0x006e }
            if (r4 == 0) goto L_0x0042
            java.lang.Object r2 = r3.aDv     // Catch:{ Exception -> 0x0065, all -> 0x006e }
            boolean r2 = r2 instanceof java.lang.String     // Catch:{ Exception -> 0x0065, all -> 0x006e }
            if (r2 == 0) goto L_0x0040
            java.lang.Object r8 = r3.aDv     // Catch:{ Exception -> 0x0065, all -> 0x006e }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ Exception -> 0x0065, all -> 0x006e }
            r2 = r8
        L_0x0038:
            com.uc.c.w.Ne = r2     // Catch:{ Exception -> 0x0065, all -> 0x006e }
            r1 = r5
        L_0x003b:
            com.uc.c.at r0 = r0.aDp
            goto L_0x0008
        L_0x003e:
            r2 = r7
            goto L_0x001a
        L_0x0040:
            r2 = r7
            goto L_0x0038
        L_0x0042:
            java.lang.String r4 = "statistic_switch"
            boolean r2 = r4.equals(r2)     // Catch:{ Exception -> 0x0065, all -> 0x006e }
            if (r2 == 0) goto L_0x003b
            java.lang.Object r2 = r3.aDv     // Catch:{ Exception -> 0x0065, all -> 0x006e }
            boolean r2 = r2 instanceof java.lang.String     // Catch:{ Exception -> 0x0065, all -> 0x006e }
            if (r2 == 0) goto L_0x0078
            java.lang.Object r8 = r3.aDv     // Catch:{ Exception -> 0x0069, all -> 0x006e }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ Exception -> 0x0069, all -> 0x006e }
            int r2 = java.lang.Integer.parseInt(r8)     // Catch:{ Exception -> 0x0069, all -> 0x006e }
        L_0x0058:
            if (r2 != 0) goto L_0x006c
            r2 = r6
        L_0x005b:
            com.uc.c.az.bbT = r2     // Catch:{ Exception -> 0x0065, all -> 0x006e }
            int r2 = com.uc.c.az.bbT     // Catch:{ Exception -> 0x0065, all -> 0x006e }
            if (r2 != 0) goto L_0x003b
            com.uc.c.bx.Ie()     // Catch:{ Exception -> 0x0065, all -> 0x006e }
            goto L_0x003b
        L_0x0065:
            r2 = move-exception
            com.uc.c.at r0 = r0.aDp
            goto L_0x0008
        L_0x0069:
            r2 = move-exception
            r2 = r5
            goto L_0x0058
        L_0x006c:
            r2 = r5
            goto L_0x005b
        L_0x006e:
            r1 = move-exception
            com.uc.c.at r0 = r0.aDp
            throw r1
        L_0x0072:
            if (r5 != r1) goto L_0x0077
            com.uc.c.az.Bm()
        L_0x0077:
            return
        L_0x0078:
            r2 = r5
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.k.a(com.uc.c.at, int, boolean):void");
    }

    private static void a(at atVar, DataOutputStream dataOutputStream) {
        at atVar2 = atVar;
        do {
            int i = atVar2.aDi;
            Object obj = atVar2.aDv;
            if (obj == null) {
                atVar2 = atVar2.aDp;
                continue;
            } else {
                switch (atVar2.aDu) {
                    case 1:
                        if (obj instanceof Integer) {
                            b(i, ((Integer) obj).intValue(), dataOutputStream);
                            break;
                        }
                        break;
                    case 2:
                        if (obj instanceof Integer) {
                            c(i, ((Integer) obj).intValue(), dataOutputStream);
                            break;
                        }
                        break;
                    case 3:
                        if (obj instanceof Integer) {
                            d(i, ((Integer) obj).intValue(), dataOutputStream);
                            break;
                        }
                        break;
                    case 4:
                        if (obj instanceof Integer) {
                            e(i, ((Integer) obj).intValue(), dataOutputStream);
                            break;
                        }
                        break;
                    case 5:
                        if (obj instanceof Integer) {
                            f(i, ((Integer) obj).intValue(), dataOutputStream);
                            break;
                        }
                        break;
                    case 10:
                        if (obj instanceof Long) {
                            a(i, ((Long) obj).longValue(), dataOutputStream);
                            break;
                        }
                        break;
                    case 11:
                        if (obj instanceof Boolean) {
                            a(i, ((Boolean) obj).booleanValue(), dataOutputStream);
                            break;
                        }
                        break;
                    case 12:
                        if (obj instanceof String) {
                            a(i, (String) obj, dataOutputStream);
                            break;
                        }
                        break;
                    case 13:
                        if (obj instanceof byte[]) {
                            a(i, (byte[]) obj, dataOutputStream);
                            break;
                        }
                        break;
                    case 14:
                        a(i, atVar2, dataOutputStream);
                        break;
                }
                atVar2 = atVar2.aDp;
                continue;
            }
        } while (atVar2 != null);
    }

    private final void a(bt btVar) {
        String str;
        cm cmVar;
        boolean z;
        int i;
        String[] strArr;
        String[] strArr2;
        boolean z2;
        cm cmVar2;
        String str2;
        String str3;
        int indexOf;
        if (this.dt.bHg != null && this.dt.bHg.size() != 0) {
            int size = this.dt.bHg.size();
            int i2 = 0;
            String str4 = null;
            cm cmVar3 = null;
            boolean z3 = false;
            while (true) {
                if (i2 >= size) {
                    cmVar = cmVar3;
                    z = false;
                    break;
                }
                Object elementAt = this.dt.bHg.elementAt(i2);
                if (elementAt instanceof cm) {
                    cmVar = (cm) elementAt;
                    if (!"ontimer".equals(cmVar.type)) {
                        if ("onenterforward".equals(cmVar.type) && cmVar.cgC != -1) {
                            z = true;
                            break;
                        }
                        z2 = z3;
                        cm cmVar4 = cmVar;
                        str2 = str;
                        cmVar2 = cmVar4;
                    } else {
                        z = false;
                        z3 = true;
                        break;
                    }
                } else {
                    if ((elementAt instanceof String) && (indexOf = (str3 = (String) elementAt).indexOf((int) UCR.Color.bTm)) != -1) {
                        String lowerCase = str3.substring(0, indexOf).toLowerCase();
                        str2 = str3.substring(indexOf + 1, str3.length());
                        if (lowerCase.equals("refresh")) {
                            cmVar2 = cmVar3;
                            z2 = true;
                        }
                    }
                    str2 = str;
                    cmVar2 = cmVar3;
                    z2 = z3;
                }
                i2++;
                z3 = z2;
                cmVar3 = cmVar2;
                str4 = str2;
            }
            if (z3 || z) {
                if (cmVar == null || cmVar.cgC == -1) {
                    if (str != null) {
                        i = 0;
                        strArr = null;
                        strArr2 = null;
                    }
                    i = 0;
                    strArr = null;
                    strArr2 = null;
                    str = null;
                } else {
                    i iVar = this.dt.bGd;
                    Vector j = this.dt.bGd.j(cmVar.cgC, -1);
                    if (j != null) {
                        Object[] a2 = iVar.a(j, (byte[]) null);
                        String au = ce.au(this.mT.byg, (String) a2[0]);
                        if (au != null && au.startsWith(bx.bAh + a.OV())) {
                            this.mT.byL = true;
                            au = null;
                        }
                        str = (btVar == null || btVar.buv == -1 || au == null || !z3) ? au : btVar.buv + cd.bVG + au;
                        strArr = (String[]) a2[6];
                        int i3 = ((int[]) a2[9])[2];
                        strArr2 = (String[]) a2[5];
                        i = i3;
                    }
                    i = 0;
                    strArr = null;
                    strArr2 = null;
                    str = null;
                }
                this.mT.byp = this.mT.byg;
                this.mT.byg = str;
                this.mT.byK = z3;
                this.mT.byL = z;
                this.mT.byx = strArr2;
                this.mT.byy = strArr;
                this.mT.byk = (byte) i;
            }
        }
    }

    private static void a(DataInputStream dataInputStream, int i) {
        switch (R(i)) {
            case 0:
                h(dataInputStream);
                return;
            case 1:
                i(dataInputStream);
                return;
            case 2:
                a(h(dataInputStream), dataInputStream);
                return;
            case 3:
            case 4:
            default:
                throw new IOException();
            case 5:
                g(dataInputStream);
                return;
        }
    }

    private static void a(DataInputStream dataInputStream, at atVar) {
        cj cjVar = new cj(dataInputStream, h(dataInputStream) - 0);
        b(new DataInputStream(cjVar), atVar);
        if (cjVar.bzl > 0) {
            dataInputStream.skip((long) cjVar.bzl);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001f, code lost:
        com.uc.c.bc.k((com.uc.c.at) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0023, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
        r2 = r1;
        r1 = null;
        r0 = r2;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x001e A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:1:0x0003] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.io.DataInputStream r4, boolean r5, boolean r6) {
        /*
            r3 = this;
            r0 = 0
            r1 = 12
            r4.skipBytes(r1)     // Catch:{ Exception -> 0x001e, all -> 0x0023 }
            com.uc.c.cd r1 = com.uc.c.cd.MZ()     // Catch:{ Exception -> 0x001e, all -> 0x0023 }
            com.uc.c.at r0 = r1.Ni()     // Catch:{ Exception -> 0x001e, all -> 0x0023 }
            b(r4, r0)     // Catch:{ Exception -> 0x001e, all -> 0x002b }
            com.uc.c.w r1 = r3.mR     // Catch:{ Exception -> 0x001e, all -> 0x002b }
            java.util.Hashtable r1 = r1.nr()     // Catch:{ Exception -> 0x001e, all -> 0x002b }
            r3.a(r1, r0)     // Catch:{ Exception -> 0x001e, all -> 0x002b }
            com.uc.c.bc.k(r0)
        L_0x001d:
            return
        L_0x001e:
            r1 = move-exception
            com.uc.c.bc.k(r0)
            goto L_0x001d
        L_0x0023:
            r1 = move-exception
            r2 = r1
            r1 = r0
            r0 = r2
        L_0x0027:
            com.uc.c.bc.k(r1)
            throw r0
        L_0x002b:
            r1 = move-exception
            r2 = r1
            r1 = r0
            r0 = r2
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.k.a(java.io.DataInputStream, boolean, boolean):void");
    }

    public static void a(DataInputStream dataInputStream, byte[] bArr) {
        b(bArr, dataInputStream);
    }

    private static void a(DataOutputStream dataOutputStream, int i) {
        dataOutputStream.writeByte(i & 255);
        dataOutputStream.writeByte((i >> 8) & 255);
        dataOutputStream.writeByte((i >> 16) & 255);
        dataOutputStream.writeByte((i >> 24) & 255);
    }

    public static void a(DataOutputStream dataOutputStream, at atVar) {
        int d = d(atVar);
        for (int i = 0; i < d; i++) {
            at b2 = b(atVar, i);
            if (b2 != null) {
                a(b2, dataOutputStream);
            }
        }
    }

    private void a(Hashtable hashtable) {
        Vector a2;
        if (hashtable.containsKey(cd.bTY) && (a2 = a(hashtable, (String) cd.bTY, cd.MZ().Nj())) != null && !a2.isEmpty()) {
            at atVar = (at) a2.elementAt(0);
            at b2 = b(atVar, 0);
            Vector vector = new Vector();
            vector.addElement(this.mT.byg);
            do {
                at atVar2 = b2;
                at b3 = b(atVar2, 1);
                String str = b3.aDv instanceof String ? (String) b3.aDv : null;
                if (bc.dM(str)) {
                    if (str.startsWith(bx.bAh)) {
                        str = str.substring(7);
                    }
                    vector.addElement(str);
                }
                String str2 = str;
                if (bc.dM(str2) && !cd.MZ().fD(str2)) {
                    at b4 = b(atVar2, 0);
                    at b5 = b(atVar2, 2);
                    at b6 = b(atVar2, 3);
                    String str3 = (String) b4.aDv;
                    int intValue = b5.aDv instanceof Integer ? ((Integer) b5.aDv).intValue() : 86400;
                    int intValue2 = b6.aDv instanceof Integer ? ((Integer) b6.aDv).intValue() : 0;
                    at atVar3 = new at();
                    atVar3.aDq = cd.bUi;
                    atVar3.aDr = cd.bUi;
                    Object[] objArr = new Object[2];
                    objArr[0] = str2;
                    objArr[1] = Long.valueOf(intValue2 == 1 ? new Long(0).longValue() : System.currentTimeMillis());
                    atVar3.aDw = objArr;
                    atVar3.aDx = intValue;
                    this.mR.nr().put(str3, atVar3);
                }
                b2 = atVar2.aDp;
            } while (b2 != null);
            if (vector.size() > 0) {
                cd.MZ().x(vector);
            }
            vector.removeAllElements();
            bc.k(atVar);
        }
    }

    private void a(Hashtable hashtable, long j, String str, String str2, int i, boolean z, boolean z2, boolean z3, byte[] bArr) {
        at atVar;
        at atVar2;
        if (hashtable.containsKey(str)) {
            atVar2 = (at) hashtable.get(str);
            if (cd.bTX.equals(str) && atVar2.aDv != null && ((byte[]) atVar2.aDv).length > 1) {
                atVar2.aDr = str2;
                while (atVar2.aDp != null) {
                    atVar2 = atVar2.aDp;
                }
                atVar = new at();
                atVar2.aDp = atVar;
                atVar2 = null;
            } else if (cd.bUv.equals(str)) {
                atVar2.aDi = -1;
                atVar = new at();
                hashtable.put(str, atVar);
            } else if (cd.bUw.equals(str)) {
                atVar2.aDi = -1;
                atVar = new at();
                hashtable.put(str, atVar);
            } else {
                atVar = atVar2;
                atVar2 = null;
            }
        } else {
            at atVar3 = new at();
            hashtable.put(str, atVar3);
            atVar = atVar3;
            atVar2 = null;
        }
        atVar.aDq = str;
        atVar.aDr = str2;
        atVar.aDw = new Object[]{this.mT.byg, new Long(j)};
        atVar.aDx = i;
        atVar.aDu = z ? (byte) 0 : 1;
        atVar.aDy = z2;
        atVar.aDt = 0;
        if (z3) {
            int i2 = 0;
            try {
                i2 = bc.m9Decode(bArr);
            } catch (Exception e) {
            }
            atVar.aDt = (byte) i2;
        }
        atVar.aDv = bArr;
        atVar.aDi = -1;
        if (cd.bUv.equals(str)) {
            cd.MZ().b(atVar2, atVar);
        }
        if (cd.bUw.equals(str)) {
            cd.MZ().a(atVar2, atVar, (byte) 1);
        }
    }

    private void a(Hashtable hashtable, at atVar) {
        boolean z;
        boolean z2;
        b(hashtable, atVar);
        at b2 = b(atVar, 1);
        String str = b2.aDv instanceof String ? (String) b2.aDv : null;
        if (bc.dM(str)) {
            cd.MZ().bUC = str;
        }
        at b3 = b(atVar, 2);
        String str2 = b3.aDv instanceof String ? (String) b3.aDv : null;
        if (bc.dM(str2)) {
            w.Nd = str2;
            z = true;
        } else {
            z = false;
        }
        at b4 = b(atVar, 3);
        String str3 = b4.aDv instanceof String ? (String) b4.aDv : null;
        if (bc.dM(str3)) {
            w.Nf = str3;
            z = true;
        }
        at b5 = b(atVar, 4);
        String str4 = b5.aDv instanceof String ? (String) b5.aDv : null;
        if (bc.dM(str4)) {
            cd.bUx = str4;
            z = true;
        }
        at b6 = b(atVar, 5);
        String str5 = b6.aDv instanceof String ? (String) b6.aDv : null;
        if (bc.dM(str5)) {
            cd.bUy = str5;
            z = true;
        }
        at b7 = b(atVar, 6);
        Integer num = (b7 == null || !(b7.aDv instanceof Integer)) ? null : (Integer) b7.aDv;
        if (num != null) {
            cd.bUz = num.byteValue();
        }
        if (bc.by(cd.bVc) || !w.NZ.equals(cd.bVc)) {
            cd.bVb = true;
            cd.bVc = w.NZ;
            z2 = true;
        } else {
            z2 = z;
        }
        at b8 = b(atVar, 7);
        if (b(b8, 0).aDv instanceof String) {
            if (cd.bUW != null) {
                bc.k(cd.bUW);
                cd.bUW = null;
            }
            cd.bUW = c(b8);
            b(cd.bUW, 0).aDv = b(b8, 0).aDv;
            b(cd.bUW, 1).aDv = b(b8, 1).aDv;
            b(cd.bUW, 2).aDv = b(b8, 2).aDv;
        }
        a(atVar, 8, z2);
        a(hashtable);
    }

    private static void a(byte[] bArr, int i, DataOutputStream dataOutputStream) {
        dataOutputStream.write(bArr, 0, i);
    }

    private static void a(byte[] bArr, DataInputStream dataInputStream) {
        dataInputStream.readFully(bArr);
    }

    private static void a(byte[] bArr, DataOutputStream dataOutputStream) {
        a(bArr, bArr.length, dataOutputStream);
    }

    private static long b(DataInputStream dataInputStream) {
        return i(dataInputStream);
    }

    private static at b(at atVar) {
        at atVar2 = new at();
        atVar2.aDi = atVar.aDi;
        atVar2.aDu = atVar.aDu;
        atVar2.aDt = atVar.aDt;
        if (atVar.aDu == 14) {
            Object[] objArr = (Object[]) atVar.aDv;
            Object[] objArr2 = new Object[objArr.length];
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= objArr.length) {
                    break;
                }
                objArr2[i2] = b((at) objArr[i2]);
                i = i2 + 1;
            }
            atVar2.aDv = objArr2;
        }
        return atVar2;
    }

    public static at b(at atVar, int i) {
        Object[] objArr = (Object[]) atVar.aDv;
        if (i >= objArr.length || i < 0 || !(objArr[i] instanceof at)) {
            return null;
        }
        return (at) objArr[i];
    }

    public static void b(int i, int i2, DataOutputStream dataOutputStream) {
        a(i, 0, dataOutputStream);
        a(i2, dataOutputStream);
    }

    private static void b(int i, DataOutputStream dataOutputStream) {
        a(i, dataOutputStream);
    }

    public static void b(DataInputStream dataInputStream, at atVar) {
        if (dataInputStream != null && atVar != null) {
            while (true) {
                int j = j(dataInputStream);
                if (j != 0) {
                    a(j, a(atVar, S(j)), dataInputStream);
                } else {
                    return;
                }
            }
        }
    }

    private void b(Hashtable hashtable, at atVar) {
        at b2 = b(atVar, 0);
        long currentTimeMillis = System.currentTimeMillis();
        while (true) {
            at atVar2 = b2;
            try {
                at b3 = b(atVar2, 0);
                at b4 = b(atVar2, 1);
                at b5 = b(atVar2, 2);
                at b6 = b(atVar2, 3);
                at b7 = b(atVar2, 4);
                at b8 = b(atVar2, 5);
                at b9 = b(atVar2, 7);
                String str = b3.aDv instanceof String ? (String) b3.aDv : null;
                String str2 = b4.aDv instanceof String ? (String) b4.aDv : null;
                int intValue = b5.aDv instanceof Integer ? ((Integer) b5.aDv).intValue() : 86400;
                boolean z = b6.aDv instanceof Integer ? ((Integer) b6.aDv).intValue() == 1 : false;
                boolean z2 = b7.aDv instanceof Integer ? ((Integer) b7.aDv).intValue() == 1 : false;
                boolean z3 = b8.aDv instanceof Integer ? ((Integer) b8.aDv).intValue() == 1 : false;
                byte[] bArr = b9.aDv instanceof byte[] ? (byte[]) b9.aDv : null;
                if (bc.dM(str)) {
                    if (cd.bUo.equals(str2) && hashtable.containsKey(str)) {
                        bc.j((at) hashtable.get(str));
                        hashtable.remove(str);
                    } else if (bArr != null) {
                        a(hashtable, currentTimeMillis, str, str2, intValue, z, z2, z3, bArr);
                    }
                    if (this.mR != null) {
                        this.mR.Z(true);
                        if (1 != this.mR.nv() && (cd.bTT.equals(str) || str.startsWith(cd.bUl))) {
                            this.mR.n((byte) 1);
                        }
                    }
                }
            } catch (Exception e) {
            } finally {
                at atVar3 = atVar2.aDp;
            }
            if (b2 == null) {
                return;
            }
        }
    }

    private static void b(byte[] bArr, DataInputStream dataInputStream) {
        a(bArr, dataInputStream);
    }

    private static int c(int i, String str) {
        return c(i, bc.dT(str));
    }

    private static int c(int i, byte[] bArr) {
        int length = bArr.length;
        return length + p(i, -1) + V(length);
    }

    public static int c(DataInputStream dataInputStream) {
        return h(dataInputStream);
    }

    private static at c(at atVar) {
        at atVar2 = new at();
        Object[] objArr = (Object[]) atVar.aDv;
        Object[] objArr2 = new Object[objArr.length];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < objArr.length) {
                objArr2[i2] = b((at) objArr[i2]);
                i = i2 + 1;
            } else {
                atVar2.aDv = objArr2;
                return atVar2;
            }
        }
    }

    private static void c(int i, int i2, DataOutputStream dataOutputStream) {
        a(i, 0, dataOutputStream);
        a(i2, dataOutputStream);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a A[SYNTHETIC, Splitter:B:12:0x003a] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x003f A[Catch:{ Exception -> 0x0075 }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0044 A[Catch:{ Exception -> 0x0075 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void c(java.io.DataInputStream r6, com.uc.c.at r7) {
        /*
            r2 = 1
            r5 = 0
            r0 = 3
            r1 = 14
            com.uc.c.at r0 = com.uc.c.cd.b(r2, r0, r1)
            b(r6, r0)     // Catch:{ IOException -> 0x0071 }
        L_0x000c:
            r1 = 0
            r2 = 7
            com.uc.c.at r0 = b(r0, r2)     // Catch:{ Exception -> 0x0048 }
            java.lang.Object r6 = r0.aDv     // Catch:{ Exception -> 0x0048 }
            byte[] r6 = (byte[]) r6     // Catch:{ Exception -> 0x0048 }
            byte[] r6 = (byte[]) r6     // Catch:{ Exception -> 0x0048 }
            r2 = 1
            com.uc.c.bc.e(r6, r2)     // Catch:{ Exception -> 0x0048 }
            r2 = 2
            r0.aDt = r2     // Catch:{ Exception -> 0x0048 }
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x0048 }
            r3 = 0
            int r4 = r6.length     // Catch:{ Exception -> 0x0048 }
            byte r0 = r0.aDt     // Catch:{ Exception -> 0x0048 }
            int r0 = r4 - r0
            r2.<init>(r6, r3, r0)     // Catch:{ Exception -> 0x0048 }
            java.util.zip.GZIPInputStream r0 = new java.util.zip.GZIPInputStream     // Catch:{ Exception -> 0x0077 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0077 }
            java.io.DataInputStream r3 = new java.io.DataInputStream     // Catch:{ Exception -> 0x0077 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x0077 }
            r0 = r3
        L_0x0035:
            b(r0, r7)     // Catch:{ Exception -> 0x004d, all -> 0x0060 }
            if (r2 == 0) goto L_0x003d
            r2.close()     // Catch:{ Exception -> 0x0075 }
        L_0x003d:
            if (r0 == 0) goto L_0x0042
            r0.close()     // Catch:{ Exception -> 0x0075 }
        L_0x0042:
            if (r5 == 0) goto L_0x0047
            r1.close()     // Catch:{ Exception -> 0x0075 }
        L_0x0047:
            return
        L_0x0048:
            r0 = move-exception
            r0 = r5
        L_0x004a:
            r2 = r0
            r0 = r5
            goto L_0x0035
        L_0x004d:
            r3 = move-exception
            if (r2 == 0) goto L_0x0053
            r2.close()     // Catch:{ Exception -> 0x005e }
        L_0x0053:
            if (r0 == 0) goto L_0x0058
            r0.close()     // Catch:{ Exception -> 0x005e }
        L_0x0058:
            if (r5 == 0) goto L_0x0047
            r1.close()     // Catch:{ Exception -> 0x005e }
            goto L_0x0047
        L_0x005e:
            r0 = move-exception
            goto L_0x0047
        L_0x0060:
            r3 = move-exception
            if (r2 == 0) goto L_0x0066
            r2.close()     // Catch:{ Exception -> 0x0073 }
        L_0x0066:
            if (r0 == 0) goto L_0x006b
            r0.close()     // Catch:{ Exception -> 0x0073 }
        L_0x006b:
            if (r5 == 0) goto L_0x0070
            r1.close()     // Catch:{ Exception -> 0x0073 }
        L_0x0070:
            throw r3
        L_0x0071:
            r1 = move-exception
            goto L_0x000c
        L_0x0073:
            r0 = move-exception
            goto L_0x0070
        L_0x0075:
            r0 = move-exception
            goto L_0x0047
        L_0x0077:
            r0 = move-exception
            r0 = r2
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.k.c(java.io.DataInputStream, com.uc.c.at):void");
    }

    private static int d(at atVar) {
        return ((Object[]) atVar.aDv).length;
    }

    private static int d(DataInputStream dataInputStream) {
        return U(h(dataInputStream));
    }

    private static void d(int i, int i2, DataOutputStream dataOutputStream) {
        a(i, 0, dataOutputStream);
        a(T(i2), dataOutputStream);
    }

    private final void dc() {
        try {
            if (this.mL != null) {
                this.mL.close();
            }
        } catch (Exception e) {
        } finally {
            this.mL = null;
        }
        try {
            if (this.ne != null) {
                this.ne.close();
            }
        } catch (Exception e2) {
        } finally {
            this.ne = null;
        }
        try {
            if (this.mM != null) {
                this.mM.close();
            }
        } catch (Exception e3) {
        } finally {
            this.mM = null;
        }
        try {
            if (this.mN != null) {
                this.mN.close();
            }
        } catch (Exception e4) {
        } finally {
            this.mN = null;
        }
    }

    private final void dg() {
        a((bt) null);
    }

    private int dh() {
        try {
            int readInt = this.mL.readInt();
            com.uc.zip.a kh = com.uc.zip.a.kh();
            this.mL.readUTF();
            if (kh.l(this.mL)) {
                cl clVar = new cl(this.mN);
                clVar.kF(readInt - 6);
                TZipInputStream kk = kh.kk();
                kk.setInputStream(clVar);
                kk.fv(readInt - 6);
                this.mM = this.mL;
                this.mL = null;
                this.mL = new DataInputStream(kk);
                return 1;
            }
            kh.d(this.mT);
            return 0;
        } catch (Exception e) {
        }
    }

    private void di() {
        try {
            if (this.ne != null) {
                this.ne.destroy();
            }
            int readInt = this.mL.readInt();
            this.mL.readUTF();
            cl clVar = new cl(this.mN);
            clVar.kF(readInt);
            this.ne = new TZipInputStream();
            this.ne.setInputStream(clVar);
            this.ne.fv(readInt);
            this.mM = this.mL;
            this.mL = null;
            this.mL = new DataInputStream(this.ne);
        } catch (Exception e) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00d8, code lost:
        r1 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        r1 = (r1 + r2) - r12.mT.bze[0];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0162, code lost:
        if (r1 > 0) goto L_0x0164;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0164, code lost:
        r12.mN.skip((long) r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0177, code lost:
        r1 = r9;
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0187, code lost:
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00d7 A[ExcHandler: OutOfMemoryError (e java.lang.OutOfMemoryError), Splitter:B:5:0x0018] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0157 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0176 A[ExcHandler: Throwable (th java.lang.Throwable), Splitter:B:5:0x0018] */
    /* JADX WARNING: Removed duplicated region for block: B:93:? A[ADDED_TO_REGION, Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void dl() {
        /*
            r12 = this;
            r11 = 131072(0x20000, float:1.83671E-40)
            r6 = 2
            r5 = 0
            long r1 = r12.nf
            r3 = 0
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 != 0) goto L_0x0012
            long r1 = java.lang.System.currentTimeMillis()
            r12.nf = r1
        L_0x0012:
            java.io.DataInputStream r1 = r12.mL     // Catch:{ OutOfMemoryError -> 0x0182, Throwable -> 0x017a }
            int r9 = r1.readInt()     // Catch:{ OutOfMemoryError -> 0x0182, Throwable -> 0x017a }
            int r1 = r12.mP     // Catch:{ OutOfMemoryError -> 0x00d7, Throwable -> 0x0176 }
            int r1 = r1 + r9
            r12.mP = r1     // Catch:{ OutOfMemoryError -> 0x00d7, Throwable -> 0x0176 }
            int r1 = r12.mQ     // Catch:{ OutOfMemoryError -> 0x00d7, Throwable -> 0x0176 }
            int r1 = r1 + 1
            r12.mQ = r1     // Catch:{ OutOfMemoryError -> 0x00d7, Throwable -> 0x0176 }
            com.uc.c.bx r1 = r12.mT     // Catch:{ OutOfMemoryError -> 0x00d7, Throwable -> 0x0176 }
            if (r1 == 0) goto L_0x0033
            com.uc.c.bx r1 = r12.mT     // Catch:{ OutOfMemoryError -> 0x00d7, Throwable -> 0x0176 }
            byte r1 = r1.bxQ     // Catch:{ OutOfMemoryError -> 0x00d7, Throwable -> 0x0176 }
            if (r1 != r6) goto L_0x0033
            com.uc.c.bx r1 = r12.mT     // Catch:{ OutOfMemoryError -> 0x00d7, Throwable -> 0x0176 }
            r2 = 3
            r1.hB(r2)     // Catch:{ OutOfMemoryError -> 0x00d7, Throwable -> 0x0176 }
        L_0x0033:
            java.io.DataInputStream r1 = r12.mL     // Catch:{ OutOfMemoryError -> 0x00d7, Throwable -> 0x0176 }
            java.lang.String r3 = r1.readUTF()     // Catch:{ OutOfMemoryError -> 0x00d7, Throwable -> 0x0176 }
            com.uc.c.w r1 = r12.mR     // Catch:{ OutOfMemoryError -> 0x00d7, Throwable -> 0x0176 }
            int r2 = r9 << 1
            r4 = 65536(0x10000, float:9.18355E-41)
            int r2 = r2 + r4
            boolean r1 = r1.bW(r2)     // Catch:{ OutOfMemoryError -> 0x00d7, Throwable -> 0x0176 }
            if (r1 != 0) goto L_0x004e
            int r1 = r9 + 5120
            boolean r1 = com.uc.c.w.bZ(r1)     // Catch:{ OutOfMemoryError -> 0x00d7, Throwable -> 0x0176 }
            if (r1 != 0) goto L_0x016f
        L_0x004e:
            com.uc.c.bx r1 = r12.mT     // Catch:{ OutOfMemoryError -> 0x0186, Throwable -> 0x0176 }
            int[] r1 = r1.bze     // Catch:{ OutOfMemoryError -> 0x0186, Throwable -> 0x0176 }
            r2 = 0
            r10 = r1[r2]     // Catch:{ OutOfMemoryError -> 0x0186, Throwable -> 0x0176 }
            java.io.DataInputStream r1 = r12.mL     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            com.uc.c.bx r2 = r12.mT     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            byte r2 = r2.byB     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            if (r2 != r6) goto L_0x007c
            r2 = 8192(0x2000, float:1.14794E-41)
        L_0x005f:
            byte[] r4 = com.uc.c.bc.b(r1, r9, r2)     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            boolean r1 = com.uc.c.bc.dM(r3)     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            if (r1 == 0) goto L_0x007f
            java.lang.String r1 = "ext:favicon"
            boolean r1 = r3.startsWith(r1)     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            if (r1 == 0) goto L_0x007f
            com.uc.c.ca r1 = r12.dt     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            r2 = 0
            int r3 = r4.length     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            b.a.a.a.f r2 = b.a.a.a.f.g(r4, r2, r3)     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            r1.bHE = r2     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
        L_0x007b:
            return
        L_0x007c:
            r2 = 2048(0x800, float:2.87E-42)
            goto L_0x005f
        L_0x007f:
            java.util.Hashtable r1 = r12.nS     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            java.lang.Object r1 = r1.get(r3)     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            java.lang.Object[] r1 = (java.lang.Object[]) r1     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            java.lang.Object[] r1 = (java.lang.Object[]) r1     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            if (r1 == 0) goto L_0x009c
            r2 = 0
            r1 = r1[r2]     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            int[] r1 = (int[]) r1     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            int[] r1 = (int[]) r1     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            r2 = 0
            r1 = r1[r2]     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            com.uc.c.ca r2 = r12.dt     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            com.uc.c.i r2 = r2.bGd     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            r2.a(r1, r4)     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
        L_0x009c:
            java.util.Hashtable r1 = r12.nT     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            java.lang.Object r1 = r1.get(r3)     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            java.util.Vector r1 = (java.util.Vector) r1     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            if (r1 == 0) goto L_0x00e4
            java.util.Iterator r5 = r1.iterator()     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
        L_0x00aa:
            boolean r1 = r5.hasNext()     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            if (r1 == 0) goto L_0x00e4
            java.lang.Object r1 = r5.next()     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            java.lang.Object[] r1 = (java.lang.Object[]) r1     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            java.lang.Object[] r1 = (java.lang.Object[]) r1     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            r2 = 0
            r2 = r1[r2]     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            com.uc.c.f r2 = (com.uc.c.f) r2     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            r6 = 1
            r1 = r1[r6]     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            int r1 = r1.intValue()     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            com.uc.c.ca r6 = r12.dt     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            r6.f(r2, r1)     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            goto L_0x00aa
        L_0x00cc:
            r1 = move-exception
            r1 = r10
        L_0x00ce:
            com.uc.c.w r2 = r12.mR     // Catch:{ OutOfMemoryError -> 0x00d7, Throwable -> 0x017e }
            int r3 = r9 << 1
            int r3 = r3 + r11
            r2.bW(r3)     // Catch:{ OutOfMemoryError -> 0x00d7, Throwable -> 0x017e }
            goto L_0x007b
        L_0x00d7:
            r1 = move-exception
            r1 = r9
        L_0x00d9:
            com.uc.c.w r2 = r12.mR     // Catch:{ all -> 0x00e2 }
            int r1 = r1 << 1
            int r1 = r1 + r11
            r2.bW(r1)     // Catch:{ all -> 0x00e2 }
            goto L_0x007b
        L_0x00e2:
            r1 = move-exception
            throw r1
        L_0x00e4:
            com.uc.c.aa r1 = com.uc.c.aa.rH()     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            boolean r1 = r1.rJ()     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            if (r1 == 0) goto L_0x0127
            java.util.Hashtable r1 = r12.na     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            if (r1 == 0) goto L_0x0127
            java.lang.String r1 = r12.nb     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            if (r1 != 0) goto L_0x0100
            com.uc.c.bx r1 = r12.mT     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            java.lang.String r1 = r1.byg     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            java.lang.String r1 = com.uc.c.bc.ec(r1)     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            r12.nb = r1     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
        L_0x0100:
            java.util.Hashtable r1 = r12.na     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            java.lang.Object r1 = r1.get(r3)     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            long[] r1 = (long[]) r1     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            r0 = r1
            long[] r0 = (long[]) r0     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            r5 = r0
            if (r5 == 0) goto L_0x0127
            com.uc.c.aa r1 = com.uc.c.aa.rH()     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            r2 = 1
            java.lang.Long r3 = new java.lang.Long     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            r6 = 0
            r6 = r5[r6]     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            r3.<init>(r6)     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            r6 = 1
            r5 = r5[r6]     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            r7 = 1000(0x3e8, double:4.94E-321)
            long r5 = r5 * r7
            java.lang.String r7 = r12.nb     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            r8 = 1
            r1.a(r2, r3, r4, r5, r7, r8)     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
        L_0x0127:
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            long r3 = r12.nf     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            long r3 = r1 - r3
            r5 = 150(0x96, double:7.4E-322)
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x007b
            r12.nf = r1     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            com.uc.c.w r1 = r12.mR     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            if (r1 == 0) goto L_0x007b
            com.uc.c.r r1 = r12.mS     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            com.uc.a.n r1 = r1.ch()     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            if (r1 == 0) goto L_0x007b
            com.uc.c.w r2 = r12.mR     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            if (r2 == 0) goto L_0x014d
            com.uc.c.w r2 = r12.mR     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            r3 = 0
            r2.b(r3)     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
        L_0x014d:
            r1.qW()     // Catch:{ OutOfMemoryError -> 0x00cc, Throwable -> 0x0152 }
            goto L_0x007b
        L_0x0152:
            r1 = move-exception
            r1 = r9
            r2 = r10
        L_0x0155:
            if (r2 == 0) goto L_0x007b
            if (r1 == 0) goto L_0x007b
            int r1 = r1 + r2
            com.uc.c.bx r2 = r12.mT     // Catch:{ Exception -> 0x016c }
            int[] r2 = r2.bze     // Catch:{ Exception -> 0x016c }
            r3 = 0
            r2 = r2[r3]     // Catch:{ Exception -> 0x016c }
            int r1 = r1 - r2
            if (r1 <= 0) goto L_0x007b
            com.uc.c.cj r2 = r12.mN     // Catch:{ Exception -> 0x016c }
            long r3 = (long) r1     // Catch:{ Exception -> 0x016c }
            r2.skip(r3)     // Catch:{ Exception -> 0x016c }
            goto L_0x007b
        L_0x016c:
            r1 = move-exception
            goto L_0x007b
        L_0x016f:
            java.io.DataInputStream r1 = r12.mL     // Catch:{ OutOfMemoryError -> 0x00d7, Throwable -> 0x0176 }
            r1.skipBytes(r9)     // Catch:{ OutOfMemoryError -> 0x00d7, Throwable -> 0x0176 }
            goto L_0x007b
        L_0x0176:
            r1 = move-exception
            r1 = r9
            r2 = r5
            goto L_0x0155
        L_0x017a:
            r1 = move-exception
            r1 = r5
            r2 = r5
            goto L_0x0155
        L_0x017e:
            r2 = move-exception
            r2 = r1
            r1 = r9
            goto L_0x0155
        L_0x0182:
            r1 = move-exception
            r1 = r5
            goto L_0x00d9
        L_0x0186:
            r1 = move-exception
            r1 = r5
            goto L_0x00ce
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.k.dl():void");
    }

    private static byte e(byte b2) {
        switch (b2) {
            case 1:
            case 2:
            case 3:
            case 6:
            case 7:
            case 8:
            case 11:
                return 0;
            case 4:
            case 5:
                return 5;
            case 9:
            case 10:
                return 1;
            case 12:
            case 13:
            case 14:
                return 2;
            default:
                return -1;
        }
    }

    private static int e(DataInputStream dataInputStream) {
        return g(dataInputStream);
    }

    private static void e(int i, int i2, DataOutputStream dataOutputStream) {
        a(i, 5, dataOutputStream);
        a(dataOutputStream, i2);
    }

    public static byte[] e(at atVar) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        try {
            a(dataOutputStream, atVar);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            try {
                byteArrayOutputStream.close();
            } catch (IOException e) {
            }
            try {
                dataOutputStream.close();
            } catch (IOException e2) {
            }
            bc.k(atVar);
            return byteArray;
        } catch (IOException e3) {
            try {
                byteArrayOutputStream.close();
            } catch (IOException e4) {
            }
            try {
                dataOutputStream.close();
            } catch (IOException e5) {
            }
            bc.k(atVar);
            return null;
        } catch (Throwable th) {
            try {
                byteArrayOutputStream.close();
            } catch (IOException e6) {
            }
            try {
                dataOutputStream.close();
            } catch (IOException e7) {
            }
            bc.k(atVar);
            throw th;
        }
    }

    private static void f(int i, int i2, DataOutputStream dataOutputStream) {
        a(i, 5, dataOutputStream);
        a(dataOutputStream, i2);
    }

    public static boolean f(DataInputStream dataInputStream) {
        return h(dataInputStream) != 0;
    }

    private static int g(DataInputStream dataInputStream) {
        return (dataInputStream.readByte() & 255) | ((dataInputStream.readByte() & 255) << 8) | ((dataInputStream.readByte() & 255) << 16) | ((dataInputStream.readByte() & 255) << 24);
    }

    private static int h(DataInputStream dataInputStream) {
        byte readByte = dataInputStream.readByte();
        if (readByte >= 0) {
            return readByte;
        }
        byte b2 = readByte & Byte.MAX_VALUE;
        byte readByte2 = dataInputStream.readByte();
        if (readByte2 >= 0) {
            return b2 | (readByte2 << 7);
        }
        byte b3 = b2 | ((readByte2 & Byte.MAX_VALUE) << 7);
        byte readByte3 = dataInputStream.readByte();
        if (readByte3 >= 0) {
            return b3 | (readByte3 << 14);
        }
        byte b4 = b3 | ((readByte3 & Byte.MAX_VALUE) << 14);
        byte readByte4 = dataInputStream.readByte();
        if (readByte4 >= 0) {
            return b4 | (readByte4 << 21);
        }
        byte b5 = b4 | ((readByte4 & Byte.MAX_VALUE) << 21);
        byte readByte5 = dataInputStream.readByte();
        byte b6 = b5 | (readByte5 << 28);
        if (readByte5 >= 0) {
            return b6;
        }
        for (int i = 0; i < 5 && dataInputStream.readByte() < 0; i++) {
        }
        return b6;
    }

    private static long i(DataInputStream dataInputStream) {
        byte readByte = dataInputStream.readByte();
        byte readByte2 = dataInputStream.readByte();
        return ((((long) readByte2) & 255) << 8) | (((long) readByte) & 255) | ((((long) dataInputStream.readByte()) & 255) << 16) | ((((long) dataInputStream.readByte()) & 255) << 24) | ((((long) dataInputStream.readByte()) & 255) << 32) | ((((long) dataInputStream.readByte()) & 255) << 40) | ((((long) dataInputStream.readByte()) & 255) << 48) | ((((long) dataInputStream.readByte()) & 255) << 56);
    }

    private static int j(DataInputStream dataInputStream) {
        try {
            return h(dataInputStream);
        } catch (IOException e) {
            return 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x005a A[Catch:{ all -> 0x00ba }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x006b A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void k(java.io.DataInputStream r12) {
        /*
            r11 = this;
            r9 = 2
            r8 = 1
            r7 = 0
            r0 = 0
            com.uc.c.bh r1 = com.uc.c.bh.CQ()
            r2 = 12
            r12.skipBytes(r2)     // Catch:{ all -> 0x0091 }
            com.uc.c.at r0 = r1.CZ()     // Catch:{ all -> 0x0091 }
            b(r12, r0)     // Catch:{ all -> 0x00b4 }
            r2 = 0
            com.uc.c.at r2 = b(r0, r2)     // Catch:{ all -> 0x00b4 }
            r3 = r7
        L_0x001a:
            if (r2 == 0) goto L_0x0073
            r4 = 2
            com.uc.c.at r4 = b(r2, r4)     // Catch:{ all -> 0x00ba }
            java.lang.Object r4 = r4.aDv     // Catch:{ all -> 0x00ba }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ all -> 0x00ba }
            if (r4 == 0) goto L_0x00c0
            java.lang.String r4 = r4.toLowerCase()     // Catch:{ all -> 0x00ba }
            r5 = r7
        L_0x002e:
            java.lang.String[] r6 = com.uc.c.bh.bju     // Catch:{ all -> 0x00ba }
            int r6 = r6.length     // Catch:{ all -> 0x00ba }
            if (r5 >= r6) goto L_0x00c0
            java.lang.String[] r6 = com.uc.c.bh.bju     // Catch:{ all -> 0x00ba }
            r6 = r6[r5]     // Catch:{ all -> 0x00ba }
            boolean r6 = r4.equals(r6)     // Catch:{ all -> 0x00ba }
            if (r6 == 0) goto L_0x006e
            r4 = r8
        L_0x003e:
            r5 = 3
            com.uc.c.at r5 = b(r2, r5)     // Catch:{ all -> 0x00ba }
            r6 = 4
            com.uc.c.at r6 = b(r2, r6)     // Catch:{ all -> 0x00ba }
            java.lang.Object r5 = r5.aDv     // Catch:{ all -> 0x00ba }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ all -> 0x00ba }
            java.lang.Object r11 = r6.aDv     // Catch:{ all -> 0x00ba }
            byte[] r11 = (byte[]) r11     // Catch:{ all -> 0x00ba }
            byte[] r11 = (byte[]) r11     // Catch:{ all -> 0x00ba }
            boolean r6 = com.uc.c.bc.dM(r5)     // Catch:{ all -> 0x00ba }
            if (r6 == 0) goto L_0x006b
            int r6 = r11.length     // Catch:{ all -> 0x00ba }
            if (r6 == 0) goto L_0x005f
            if (r4 != 0) goto L_0x0071
        L_0x005f:
            r4 = r9
        L_0x0060:
            r1.a(r5, r11, r4)     // Catch:{ all -> 0x00ba }
            r1.es(r5)     // Catch:{ all -> 0x00ba }
            if (r3 != 0) goto L_0x006b
            if (r4 != r8) goto L_0x006b
            r3 = r8
        L_0x006b:
            com.uc.c.at r2 = r2.aDp     // Catch:{ all -> 0x00ba }
            goto L_0x001a
        L_0x006e:
            int r5 = r5 + 1
            goto L_0x002e
        L_0x0071:
            r4 = r8
            goto L_0x0060
        L_0x0073:
            com.uc.c.bc.k(r0)
            if (r3 == 0) goto L_0x0090
            com.uc.c.w r0 = com.uc.c.ar.mR
            java.util.Vector r0 = r0.JR
            java.lang.Object r11 = r0.elementAt(r7)
            com.uc.c.r r11 = (com.uc.c.r) r11
            com.uc.c.r r11 = (com.uc.c.r) r11
            com.uc.a.n r0 = r11.p
            r0.re()
            com.uc.c.bh r0 = com.uc.c.bh.CQ()
            r0.CS()
        L_0x0090:
            return
        L_0x0091:
            r1 = move-exception
            r2 = r7
            r10 = r0
            r0 = r1
            r1 = r10
        L_0x0096:
            com.uc.c.bc.k(r1)
            if (r2 == 0) goto L_0x00b3
            com.uc.c.w r1 = com.uc.c.ar.mR
            java.util.Vector r1 = r1.JR
            java.lang.Object r11 = r1.elementAt(r7)
            com.uc.c.r r11 = (com.uc.c.r) r11
            com.uc.c.r r11 = (com.uc.c.r) r11
            com.uc.a.n r1 = r11.p
            r1.re()
            com.uc.c.bh r1 = com.uc.c.bh.CQ()
            r1.CS()
        L_0x00b3:
            throw r0
        L_0x00b4:
            r1 = move-exception
            r2 = r7
            r10 = r0
            r0 = r1
            r1 = r10
            goto L_0x0096
        L_0x00ba:
            r1 = move-exception
            r2 = r3
            r10 = r0
            r0 = r1
            r1 = r10
            goto L_0x0096
        L_0x00c0:
            r4 = r7
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.k.k(java.io.DataInputStream):void");
    }

    private static int o(int i, int i2) {
        return (i << 3) | i2;
    }

    private static int p(int i, int i2) {
        return V(i << 3);
    }

    private static int q(int i, int i2) {
        return p(i, -1) + W(i2);
    }

    private static int r(int i, int i2) {
        return p(i, -1) + V(i2);
    }

    private static int s(int i, int i2) {
        return p(i, -1) + V(T(i2));
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0125, code lost:
        dd();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x012a, code lost:
        if (r11.mS == null) goto L_0x01f3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x0132, code lost:
        if (r11.mS.ch() == null) goto L_0x01f3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x0134, code lost:
        r0 = r11.mS.ch();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0140, code lost:
        if (r11.dt.KD() != false) goto L_0x0190;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x0142, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0143, code lost:
        r0.by(r1);
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x017d, code lost:
        if (r0 != true) goto L_0x018c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:?, code lost:
        r11.dt.bm(2, 3);
        r2 = a((com.uc.c.at) null, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x018c, code lost:
        if (r0 != false) goto L_0x01f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x018e, code lost:
        r2 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x0190, code lost:
        r1 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x01f3, code lost:
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x01f6, code lost:
        r2 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0070, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0071, code lost:
        r10 = r2;
        r2 = r1;
        r1 = r0;
        r0 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:?, code lost:
        r11.dt.jh(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x010c, code lost:
        if (r0 == true) goto L_0x0111;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x010f, code lost:
        if (r12 != 6) goto L_0x017d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0111, code lost:
        r11.dt.bm(2, 2);
        r2 = a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x011c, code lost:
        if (r1 == null) goto L_0x0125;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x011e, code lost:
        if (r0 == false) goto L_0x0122;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0120, code lost:
        if (r2 != -2) goto L_0x0125;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0122, code lost:
        r1.clear();
     */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x0178  */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x0192  */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x01cb  */
    /* JADX WARNING: Removed duplicated region for block: B:177:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0070 A[ExcHandler: Exception (r2v19 'e' java.lang.Exception A[CUSTOM_DECLARE]), PHI: r0 
      PHI: (r0v23 boolean) = (r0v1 boolean), (r0v1 boolean), (r0v1 boolean), (r0v26 boolean), (r0v26 boolean), (r0v26 boolean), (r0v25 boolean), (r0v35 boolean), (r0v25 boolean), (r0v25 boolean), (r0v25 boolean) binds: [B:16:0x0022, B:71:0x00ba, B:72:?, B:90:0x0106, B:126:0x017f, B:127:?, B:23:0x0038, B:38:0x0066, B:108:0x0149, B:109:?, B:84:0x00ef] A[DONT_GENERATE, DONT_INLINE], Splitter:B:38:0x0066] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00ae  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int Q(int r12) {
        /*
            r11 = this;
            r4 = 0
            r9 = -2
            r8 = 2
            r7 = 0
            r6 = 1
            if (r12 < r6) goto L_0x01f9
            r0 = 5
            if (r12 > r0) goto L_0x01f9
            r0 = r8
        L_0x000b:
            com.uc.c.w r1 = r11.mR     // Catch:{ Exception -> 0x01dd, OutOfMemoryError -> 0x0195, all -> 0x01cd }
            if (r1 == 0) goto L_0x0018
            com.uc.c.w r1 = r11.mR     // Catch:{ Exception -> 0x01dd, OutOfMemoryError -> 0x0195, all -> 0x01cd }
            if (r12 != r6) goto L_0x00b3
            r2 = 65536(0x10000, float:9.18355E-41)
        L_0x0015:
            r1.bW(r2)     // Catch:{ Exception -> 0x01dd, OutOfMemoryError -> 0x0195, all -> 0x01cd }
        L_0x0018:
            com.uc.c.ay r1 = new com.uc.c.ay     // Catch:{ Exception -> 0x01dd, OutOfMemoryError -> 0x0195, all -> 0x01cd }
            r2 = 1
            r3 = 6
            if (r12 != r3) goto L_0x00b7
            r3 = r6
        L_0x001f:
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x01dd, OutOfMemoryError -> 0x0195, all -> 0x01cd }
            com.uc.c.bx r2 = r11.mT     // Catch:{ Exception -> 0x0070, OutOfMemoryError -> 0x01db }
            if (r2 != 0) goto L_0x00ba
            r2 = r4
        L_0x0027:
            com.uc.c.cj r3 = r11.mN     // Catch:{ Exception -> 0x0070, OutOfMemoryError -> 0x01db }
            r1.setInput(r3, r2)     // Catch:{ Exception -> 0x0070, OutOfMemoryError -> 0x01db }
            r2 = 0
            r3 = 0
            r4 = 0
            r1.require(r2, r3, r4)     // Catch:{ Exception -> 0x0070, OutOfMemoryError -> 0x01db }
            r1.nextToken()     // Catch:{ Exception -> 0x0070, OutOfMemoryError -> 0x01db }
        L_0x0035:
            r2 = 6
            if (r12 == r2) goto L_0x0106
            int r2 = r1.getEventType()     // Catch:{ OutOfMemoryError -> 0x0065, Exception -> 0x0070 }
            switch(r2) {
                case 1: goto L_0x0106;
                case 2: goto L_0x00ef;
                case 3: goto L_0x0149;
                case 998: goto L_0x00c0;
                default: goto L_0x003f;
            }     // Catch:{ OutOfMemoryError -> 0x0065, Exception -> 0x0070 }
        L_0x003f:
            java.lang.String r3 = r1.getText()     // Catch:{ OutOfMemoryError -> 0x0065, Exception -> 0x0070 }
            boolean r4 = com.uc.c.bc.dM(r3)     // Catch:{ OutOfMemoryError -> 0x0065, Exception -> 0x0070 }
            if (r4 == 0) goto L_0x0061
            r4 = 10
            if (r2 != r4) goto L_0x0061
            java.lang.String r2 = "wml"
            int r2 = r3.indexOf(r2)     // Catch:{ OutOfMemoryError -> 0x0065, Exception -> 0x0070 }
            r4 = -1
            if (r2 == r4) goto L_0x0057
            r0 = r6
        L_0x0057:
            java.lang.String r2 = "xhtml-mobile10.dtd"
            int r2 = r3.indexOf(r2)     // Catch:{ OutOfMemoryError -> 0x0065, Exception -> 0x0070 }
            r3 = -1
            if (r2 == r3) goto L_0x0061
            r0 = r8
        L_0x0061:
            r1.nextToken()     // Catch:{ OutOfMemoryError -> 0x0065, Exception -> 0x0070 }
            goto L_0x0035
        L_0x0065:
            r2 = move-exception
        L_0x0066:
            com.uc.c.w r2 = r11.mR     // Catch:{ Exception -> 0x0070, OutOfMemoryError -> 0x01db }
            if (r2 == 0) goto L_0x0035
            com.uc.c.w r2 = r11.mR     // Catch:{ Exception -> 0x0070, OutOfMemoryError -> 0x01db }
            r2.mW()     // Catch:{ Exception -> 0x0070, OutOfMemoryError -> 0x01db }
            goto L_0x0035
        L_0x0070:
            r2 = move-exception
            r10 = r2
            r2 = r1
            r1 = r0
            r0 = r10
        L_0x0075:
            boolean r3 = r0 instanceof java.io.UnsupportedEncodingException     // Catch:{ all -> 0x01d8 }
            if (r3 != 0) goto L_0x0080
            r3 = 6
            if (r12 != r3) goto L_0x01f0
            boolean r0 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x01d8 }
            if (r0 == 0) goto L_0x01f0
        L_0x0080:
            r0 = r9
        L_0x0081:
            if (r2 == 0) goto L_0x008a
            if (r1 == 0) goto L_0x0087
            if (r0 != r9) goto L_0x008a
        L_0x0087:
            r2.clear()
        L_0x008a:
            r11.dd()
            com.uc.c.r r1 = r11.mS
            if (r1 == 0) goto L_0x00ab
            com.uc.c.r r1 = r11.mS
            com.uc.a.n r1 = r1.ch()
            if (r1 == 0) goto L_0x00ab
            com.uc.c.r r1 = r11.mS
            com.uc.a.n r1 = r1.ch()
            com.uc.c.ca r2 = r11.dt
            boolean r2 = r2.KD()
            if (r2 != 0) goto L_0x0192
            r2 = r6
        L_0x00a8:
            r1.by(r2)
        L_0x00ab:
            r1 = 3
            if (r12 < r1) goto L_0x00b2
            r1 = 5
            if (r12 > r1) goto L_0x00b2
            r0 = r6
        L_0x00b2:
            return r0
        L_0x00b3:
            r2 = 131072(0x20000, float:1.83671E-40)
            goto L_0x0015
        L_0x00b7:
            r3 = r7
            goto L_0x001f
        L_0x00ba:
            com.uc.c.bx r2 = r11.mT     // Catch:{ Exception -> 0x0070, OutOfMemoryError -> 0x01db }
            java.lang.String r2 = r2.bxV     // Catch:{ Exception -> 0x0070, OutOfMemoryError -> 0x01db }
            goto L_0x0027
        L_0x00c0:
            int r0 = r1.getAttributeCount()     // Catch:{ OutOfMemoryError -> 0x01e9, Exception -> 0x01e4, all -> 0x01d3 }
            r2 = r7
        L_0x00c5:
            if (r2 >= r0) goto L_0x00e9
            java.lang.String r3 = r1.getAttributeName(r2)     // Catch:{ OutOfMemoryError -> 0x01e9, Exception -> 0x01e4, all -> 0x01d3 }
            java.lang.String r4 = r1.getAttributeValue(r2)     // Catch:{ OutOfMemoryError -> 0x01e9, Exception -> 0x01e4, all -> 0x01d3 }
            java.lang.String r5 = "encoding"
            boolean r3 = r5.equals(r3)     // Catch:{ OutOfMemoryError -> 0x01e9, Exception -> 0x01e4, all -> 0x01d3 }
            if (r3 == 0) goto L_0x00e6
            java.lang.String r3 = r1.aZp     // Catch:{ OutOfMemoryError -> 0x01e9, Exception -> 0x01e4, all -> 0x01d3 }
            boolean r3 = com.uc.c.bc.by(r3)     // Catch:{ OutOfMemoryError -> 0x01e9, Exception -> 0x01e4, all -> 0x01d3 }
            if (r3 == 0) goto L_0x00e6
            com.uc.c.cj r3 = r11.mN     // Catch:{ OutOfMemoryError -> 0x01e9, Exception -> 0x01e4, all -> 0x01d3 }
            r1.b(r3, r4)     // Catch:{ OutOfMemoryError -> 0x01e9, Exception -> 0x01e4, all -> 0x01d3 }
            r1.aZp = r4     // Catch:{ OutOfMemoryError -> 0x01e9, Exception -> 0x01e4, all -> 0x01d3 }
        L_0x00e6:
            int r2 = r2 + 1
            goto L_0x00c5
        L_0x00e9:
            r1.nextToken()     // Catch:{ OutOfMemoryError -> 0x01e9, Exception -> 0x01e4, all -> 0x01d3 }
            r0 = r8
            goto L_0x0035
        L_0x00ef:
            java.lang.String r2 = r1.getName()     // Catch:{ OutOfMemoryError -> 0x0065, Exception -> 0x0070 }
            boolean r3 = com.uc.c.bc.dM(r2)     // Catch:{ OutOfMemoryError -> 0x0065, Exception -> 0x0070 }
            if (r3 == 0) goto L_0x0106
            java.lang.String r2 = r2.toLowerCase()     // Catch:{ OutOfMemoryError -> 0x0065, Exception -> 0x0070 }
            java.lang.String r3 = "wml"
            boolean r2 = r2.equals(r3)     // Catch:{ OutOfMemoryError -> 0x0065, Exception -> 0x0070 }
            if (r2 == 0) goto L_0x0106
            r0 = r6
        L_0x0106:
            com.uc.c.ca r2 = r11.dt     // Catch:{ Exception -> 0x0070, OutOfMemoryError -> 0x01db }
            r3 = 0
            r2.jh(r3)     // Catch:{ Exception -> 0x0070, OutOfMemoryError -> 0x01db }
            if (r0 == r6) goto L_0x0111
            r2 = 6
            if (r12 != r2) goto L_0x017d
        L_0x0111:
            com.uc.c.ca r2 = r11.dt     // Catch:{ Exception -> 0x0070, OutOfMemoryError -> 0x01db }
            r3 = 2
            r4 = 2
            r2.bm(r3, r4)     // Catch:{ Exception -> 0x0070, OutOfMemoryError -> 0x01db }
            int r2 = r11.a(r1)     // Catch:{ Exception -> 0x0070, OutOfMemoryError -> 0x01db }
        L_0x011c:
            if (r1 == 0) goto L_0x0125
            if (r0 == 0) goto L_0x0122
            if (r2 != r9) goto L_0x0125
        L_0x0122:
            r1.clear()
        L_0x0125:
            r11.dd()
            com.uc.c.r r0 = r11.mS
            if (r0 == 0) goto L_0x01f3
            com.uc.c.r r0 = r11.mS
            com.uc.a.n r0 = r0.ch()
            if (r0 == 0) goto L_0x01f3
            com.uc.c.r r0 = r11.mS
            com.uc.a.n r0 = r0.ch()
            com.uc.c.ca r1 = r11.dt
            boolean r1 = r1.KD()
            if (r1 != 0) goto L_0x0190
            r1 = r6
        L_0x0143:
            r0.by(r1)
            r0 = r2
            goto L_0x00ab
        L_0x0149:
            r1.nextToken()     // Catch:{ OutOfMemoryError -> 0x0065, Exception -> 0x0070 }
            goto L_0x0106
        L_0x014d:
            r2 = move-exception
            r10 = r2
            r2 = r1
            r1 = r0
            r0 = r10
        L_0x0152:
            if (r2 == 0) goto L_0x015b
            if (r1 == 0) goto L_0x0158
            if (r6 != r9) goto L_0x015b
        L_0x0158:
            r2.clear()
        L_0x015b:
            r11.dd()
            com.uc.c.r r1 = r11.mS
            if (r1 == 0) goto L_0x017c
            com.uc.c.r r1 = r11.mS
            com.uc.a.n r1 = r1.ch()
            if (r1 == 0) goto L_0x017c
            com.uc.c.r r1 = r11.mS
            com.uc.a.n r1 = r1.ch()
            com.uc.c.ca r2 = r11.dt
            boolean r2 = r2.KD()
            if (r2 != 0) goto L_0x01cb
            r2 = r6
        L_0x0179:
            r1.by(r2)
        L_0x017c:
            throw r0
        L_0x017d:
            if (r0 != r8) goto L_0x018c
            com.uc.c.ca r2 = r11.dt     // Catch:{ Exception -> 0x0070, OutOfMemoryError -> 0x01db }
            r3 = 2
            r4 = 3
            r2.bm(r3, r4)     // Catch:{ Exception -> 0x0070, OutOfMemoryError -> 0x01db }
            r2 = 0
            int r2 = r11.a(r2, r1)     // Catch:{ Exception -> 0x0070, OutOfMemoryError -> 0x01db }
            goto L_0x011c
        L_0x018c:
            if (r0 != 0) goto L_0x01f6
            r2 = -1
            goto L_0x011c
        L_0x0190:
            r1 = r7
            goto L_0x0143
        L_0x0192:
            r2 = r7
            goto L_0x00a8
        L_0x0195:
            r1 = move-exception
            r1 = r4
        L_0x0197:
            com.uc.c.w r2 = r11.mR     // Catch:{ all -> 0x014d }
            r2.mW()     // Catch:{ all -> 0x014d }
            if (r1 == 0) goto L_0x01a5
            if (r0 == 0) goto L_0x01a2
            if (r6 != r9) goto L_0x01a5
        L_0x01a2:
            r1.clear()
        L_0x01a5:
            r11.dd()
            com.uc.c.r r0 = r11.mS
            if (r0 == 0) goto L_0x01ed
            com.uc.c.r r0 = r11.mS
            com.uc.a.n r0 = r0.ch()
            if (r0 == 0) goto L_0x01ed
            com.uc.c.r r0 = r11.mS
            com.uc.a.n r0 = r0.ch()
            com.uc.c.ca r1 = r11.dt
            boolean r1 = r1.KD()
            if (r1 != 0) goto L_0x01c9
            r1 = r6
        L_0x01c3:
            r0.by(r1)
            r0 = r6
            goto L_0x00ab
        L_0x01c9:
            r1 = r7
            goto L_0x01c3
        L_0x01cb:
            r2 = r7
            goto L_0x0179
        L_0x01cd:
            r1 = move-exception
            r2 = r4
            r10 = r0
            r0 = r1
            r1 = r10
            goto L_0x0152
        L_0x01d3:
            r0 = move-exception
            r2 = r1
            r1 = r8
            goto L_0x0152
        L_0x01d8:
            r0 = move-exception
            goto L_0x0152
        L_0x01db:
            r2 = move-exception
            goto L_0x0197
        L_0x01dd:
            r1 = move-exception
            r2 = r4
            r10 = r0
            r0 = r1
            r1 = r10
            goto L_0x0075
        L_0x01e4:
            r0 = move-exception
            r2 = r1
            r1 = r8
            goto L_0x0075
        L_0x01e9:
            r0 = move-exception
            r0 = r8
            goto L_0x0066
        L_0x01ed:
            r0 = r6
            goto L_0x00ab
        L_0x01f0:
            r0 = r6
            goto L_0x0081
        L_0x01f3:
            r0 = r2
            goto L_0x00ab
        L_0x01f6:
            r2 = r6
            goto L_0x011c
        L_0x01f9:
            r0 = r7
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.k.Q(int):int");
    }

    public final int a(at atVar, ay ayVar) {
        int i;
        at atVar2;
        at atVar3;
        ad adVar = new ad(this.mR, this.mT, this.dt, this.mS);
        this.mW = adVar;
        this.mW.Wt = ayVar;
        adVar.WZ = this;
        try {
            adVar.a(this.mN, this.dt.bFA);
            if (this.dt.LC() && atVar != null) {
                atVar2 = atVar;
                while (true) {
                    at b2 = b(atVar2, 0);
                    at b3 = b(atVar2, 1);
                    at b4 = b(atVar2, 2);
                    at b5 = b(atVar2, 3);
                    if (b2.aDv == null) {
                        atVar3 = atVar2.aDp;
                    } else {
                        String str = (String) b2.aDv;
                        byte[] bArr = (byte[]) b3.aDv;
                        int intValue = b4.aDv instanceof Integer ? ((Integer) b4.aDv).intValue() : 0;
                        int intValue2 = b5.aDv instanceof Integer ? ((Integer) b5.aDv).intValue() : 0;
                        if (intValue <= 0 || intValue2 <= 0) {
                            f g = bc.g(bArr, 0, bArr.length);
                            g.getWidth();
                            g.getHeight();
                        }
                        this.dt.bGd.b(bc.dT(str), bArr);
                        atVar3 = atVar2.aDp;
                    }
                    if (atVar3 == null) {
                        break;
                    }
                    atVar2 = atVar3;
                }
                bc.k(atVar3);
            }
        } catch (Exception e) {
            atVar3 = atVar2.aDp;
        } catch (Throwable th) {
            try {
                i = th instanceof UnsupportedEncodingException ? -2 : 1;
                a(adVar.Wu);
                this.dt.bHg = adVar.WX;
                dg();
            } catch (Throwable th2) {
            }
            if (this.mW == null) {
                return i;
            }
            this.mW.close();
            this.mW = null;
            return i;
        }
        try {
            a(adVar.Wu);
            this.dt.bHg = adVar.WX;
            dg();
        } catch (Throwable th3) {
        }
        if (this.mW == null) {
            return 1;
        }
        this.mW.close();
        this.mW = null;
        return 1;
    }

    public final int a(ay ayVar) {
        int i = 1;
        bt btVar = new bt(this.mR, this.mS, this.mT, this.dt);
        this.mX = btVar;
        this.mX.Wt = ayVar;
        try {
            btVar.a(this.mN, this.dt.bFA, this.mT.byp, this.mT.byw);
            try {
                a(btVar.Wu);
                this.dt.bHg = btVar.WX;
                a(btVar);
            } catch (Throwable th) {
            }
            if (this.mX != null) {
                this.mX.close();
                this.mX = null;
            }
        } catch (Throwable th2) {
        }
        return i;
        if (this.mX != null) {
            this.mX.close();
            this.mX = null;
        }
        return i;
    }

    public void a(DataInputStream dataInputStream) {
        try {
            int readInt = dataInputStream.readInt();
            dataInputStream.readUTF();
            byte[] bArr = new byte[readInt];
            dataInputStream.read(bArr);
            String[] Y = bc.Y(new String(bArr), "\r\n");
            int i = 0;
            while (i < Y.length) {
                if (!Y[i].startsWith(cb.bPp)) {
                    i++;
                } else if (this.mT != null) {
                    this.mT.byf = bc.parseInt(Y[i].substring(Y[i].indexOf("=") + 1));
                    return;
                } else {
                    return;
                }
            }
        } catch (Exception e) {
        }
    }

    public final void a(boolean z, boolean z2) {
        if (!this.dt.iM(7)) {
            this.dt.cs(true);
        }
    }

    public void b(Object[] objArr) {
        if (objArr != null) {
            this.na = (Hashtable) objArr[0];
            this.nS = (Hashtable) objArr[1];
        }
    }

    public final void close() {
        if (this.mW != null) {
            this.mW.close();
            this.mW = null;
        }
        if (this.mX != null) {
            this.mX.close();
            this.mX = null;
        }
        this.mS = null;
        this.mR = null;
        if (this.mY != null) {
            this.mY.clear();
        }
        this.mY = null;
        this.mU = null;
        this.nb = null;
        dc();
    }

    public final void d(String str, String str2) {
        int i;
        String str3;
        int a2;
        try {
            String lowerCase = str.toLowerCase();
            if ("sleep_and_jump".equals(lowerCase)) {
                this.dt.fm(str2);
            } else if ("show_site_bar".equals(lowerCase) && (this.mT == null || cd.fG(this.mT.byg))) {
                String str4 = this.dt.bFA;
                String[] j = ce.j(str4);
                if ((bc.by(str4) || j[0].indexOf("uc.cn") > -1 || j[0].indexOf("ucweb.com") > -1 || j[0].indexOf("uc123.com") > -1) && (a2 = this.mR.a(str2, -1, this.dt)) != -1) {
                    ca caVar = this.dt;
                    caVar.bHs = (byte) (caVar.bHs | 4);
                    this.dt.t(0, w.MQ, w.MK, w.MN - w.MQ);
                    this.dt.bp(w.MM, w.MN - this.dt.bGo);
                    if (!bc.aL(this.mZ, 2)) {
                        ca ln = this.mR.ln();
                        this.mS.d(ln, a2);
                        int f = this.mS.f(this.dt);
                        if (f == -1) {
                            this.mS.o(ln);
                        } else {
                            this.mS.a(f, ln);
                        }
                    }
                }
            } else if ("show_model".equals(lowerCase)) {
                cd.MZ().a(this.dt, str2, this.mR);
            } else if ("show_ad2".equals(lowerCase)) {
                cd.MZ().a(this.mR, this.dt, str2);
            } else if (w.OG.equals(lowerCase)) {
                if (this.mU == null) {
                    this.mU = new String[8];
                }
                this.mU[0] = str2;
            } else if (lowerCase.equals("dwreferer")) {
                if (this.mU == null) {
                    this.mU = new String[8];
                }
                this.mU[1] = str2;
            } else if (lowerCase.equals("dwfilename")) {
                if (this.mU == null) {
                    this.mU = new String[8];
                }
                this.mU[3] = str2;
            } else if (lowerCase.equals("dl_method")) {
                if (this.mU == null) {
                    this.mU = new String[8];
                }
                this.mU[5] = str2;
            } else if (lowerCase.equals("dl_bodydata")) {
                if (this.mU == null) {
                    this.mU = new String[8];
                }
                this.mU[6] = str2;
            } else if (lowerCase.equals("dl_headers")) {
                if (this.mU == null) {
                    this.mU = new String[8];
                }
                this.mU[7] = str2;
            } else if (lowerCase.equals("dwcookie")) {
                if (this.mT.byx != null || (22 != d.cu(this.mU[0]) && 22 != d.cu(this.mU[3]))) {
                    if (this.mU == null) {
                        this.mU = new String[8];
                    }
                    this.mU[2] = str2;
                    this.mU[4] = "down:ser";
                    if (this.mV == 0) {
                        this.mR.b(this.mS, this.mU, (byte) 0);
                    } else if (this.mV == 1) {
                        this.mR.b(this.mS, this.mU, (byte) 1);
                    } else {
                        this.mR.b(this.mS, this.mU, (byte) 2);
                    }
                }
            } else if (lowerCase.equals("download_prompt")) {
                int i2 = str2.equals("prompt") ? 0 | 1 : 0;
                if (str2.equals("install")) {
                    i2 |= 2;
                }
                if (str2.equals("open")) {
                    i2 |= 4;
                }
                if (str2.equals("save")) {
                    i2 |= 8;
                }
                if (str2.equals("transform")) {
                    i2 |= 16;
                }
                cd.MZ().k(this.dt, i2);
            } else if (lowerCase.equals("set_para")) {
                if (str2.startsWith("upload_href")) {
                    bx.bAe = str2;
                }
            } else if (lowerCase.toLowerCase().equals("favorec")) {
                n.F(str2);
            } else if (lowerCase.equals(as.azl)) {
                cd.MZ().d(this.dt, str2);
            } else if ("download_type".equals(lowerCase)) {
                if ("download_by_ucweb".equals(str2)) {
                    this.mV = 0;
                } else if ("download_by_external_browser".equals(str2)) {
                    this.mV = 1;
                }
            } else if ("foldstart".equals(lowerCase)) {
                if (str2.indexOf(cd.bVI) != -1) {
                    String[] split = bc.split(str2, cd.bVI);
                    str3 = split[0];
                    i = "open".equals(split[1]) ? 0 : 1;
                } else {
                    i = 1;
                    str3 = str2;
                }
                cd.MZ().a(this.dt, str3, i);
            } else if ("foldend".equals(lowerCase)) {
                this.dt.Lq();
            } else if ("ucfocus".equals(lowerCase) && this.dt.bHR == -1) {
                cd.MZ().e(this.dt, str2);
            } else if ("show_lp".equals(lowerCase)) {
                cd.MZ().a(this.dt, str2, this.mR, this.mS);
            } else if (cd.bVq.equals(lowerCase) && this.dt != null && this.dt.bFB != null) {
                cd.NM();
                this.mT.byJ = cd.aj(str2, this.dt.bFB.ZQ);
            }
        } catch (Exception e) {
        }
    }

    public final void d(byte[] bArr, byte[] bArr2) {
        try {
            d(bc.ak(bArr), bc.ak(bArr2));
        } catch (Exception e) {
        }
    }

    public final void dd() {
        try {
            dj();
            if (this.dt != null && !this.dt.iM(7)) {
                this.dt.KH();
            }
        } catch (Exception e) {
        }
        dc();
    }

    public final int de() {
        return a((at) null, (ay) null);
    }

    public final int df() {
        return a((ay) null);
    }

    public final void dj() {
        dm();
        if (this.dt.bHR != -1) {
            this.dt.bGd.cg();
        }
        if (this.mS != null) {
            this.mS.jA();
        }
    }

    public final void dk() {
        try {
            int readInt = this.mL.readInt();
            this.mL.readUTF();
            cl clVar = new cl(this.mN);
            clVar.kF(readInt);
            GZIPInputStream gZIPInputStream = new GZIPInputStream(clVar, 4096);
            this.mM = this.mL;
            this.mL = null;
            this.mL = new DataInputStream(gZIPInputStream);
        } catch (Exception e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.k.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.uc.c.k.a(int, com.uc.c.at):int
      com.uc.c.k.a(com.uc.c.at, int):com.uc.c.at
      com.uc.c.k.a(int, java.io.DataInputStream):void
      com.uc.c.k.a(int, java.io.DataOutputStream):void
      com.uc.c.k.a(long, java.io.DataOutputStream):void
      com.uc.c.k.a(com.uc.c.at, java.io.DataOutputStream):void
      com.uc.c.k.a(java.io.DataInputStream, int):void
      com.uc.c.k.a(java.io.DataInputStream, com.uc.c.at):void
      com.uc.c.k.a(java.io.DataInputStream, byte[]):void
      com.uc.c.k.a(java.io.DataOutputStream, int):void
      com.uc.c.k.a(java.io.DataOutputStream, com.uc.c.at):void
      com.uc.c.k.a(java.util.Hashtable, com.uc.c.at):void
      com.uc.c.k.a(byte[], java.io.DataInputStream):void
      com.uc.c.k.a(byte[], java.io.DataOutputStream):void
      com.uc.c.k.a(com.uc.c.at, com.uc.c.ay):int
      com.uc.c.k.a(boolean, boolean):void */
    public final void dm() {
        a(true, false);
    }

    public void dn() {
        try {
            byte readByte = this.mL.readByte() & 255;
            boolean z = this.mL.readByte() == 1;
            boolean z2 = this.mL.readByte() == 11;
            byte readByte2 = this.mL.readByte() & 255;
            switch (readByte) {
                case 92:
                    if (200 == readByte2) {
                        a(this.mL, z, z2);
                        return;
                    }
                    return;
                case 93:
                case 94:
                case 95:
                case 96:
                case 97:
                default:
                    return;
                case 98:
                    k(this.mL);
                    return;
            }
        } catch (Exception e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.a(com.uc.c.ca, float, boolean):b.a.a.a.f
     arg types: [com.uc.c.ca, int, int]
     candidates:
      com.uc.c.r.a(byte[], com.uc.c.ca, java.lang.Object):int
      com.uc.c.r.a(int, com.uc.c.bf, com.uc.c.bf):void
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, java.util.Vector):void
      com.uc.c.r.a(int, int, byte):void
      com.uc.c.r.a(com.uc.c.bx, java.lang.String, com.uc.c.ca):void
      com.uc.c.r.a(java.lang.String, java.lang.String, java.lang.String[]):void
      com.uc.c.r.a(java.lang.String[], int, java.lang.String):void
      com.uc.c.r.a(byte, byte, java.lang.Object):boolean
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, com.uc.c.ca):boolean
      com.uc.c.r.a(com.uc.c.bk, boolean, boolean):boolean
      com.uc.c.r.a(com.uc.c.ca, int, int):boolean
      com.uc.c.r.a(com.uc.c.ca, float, boolean):b.a.a.a.f */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bc.d(byte[], boolean):char[]
     arg types: [byte[], int]
     candidates:
      com.uc.c.bc.d(java.lang.String, java.lang.Object):byte[]
      com.uc.c.bc.d(byte[], boolean):char[] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.ca.a(int, byte[], byte[], int, int, boolean, boolean, boolean, boolean):void
     arg types: [int, byte[], byte[], int, int, int, boolean, boolean, boolean]
     candidates:
      com.uc.c.ca.a(int, java.lang.String, java.lang.String, java.lang.String, int, int, boolean, boolean, boolean):void
      com.uc.c.ca.a(int, byte[], byte[], byte[], byte[], int, int, int, int):void
      com.uc.c.ca.a(int, byte[], byte[], byte[], byte[], byte[], int, int, int):void
      com.uc.c.ca.a(int, byte[], byte[], int, int, boolean, boolean, boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.ca.a(int, byte[], byte[], byte[], boolean, boolean, boolean, boolean):void
     arg types: [int, byte[], byte[], ?[OBJECT, ARRAY], int, int, int, boolean]
     candidates:
      com.uc.c.ca.a(int, java.lang.String, java.lang.String, int, boolean, boolean, boolean, boolean):void
      com.uc.c.ca.a(int, byte[], byte[], byte[], int, int, int, int):void
      com.uc.c.ca.a(int, byte[], byte[], byte[], int, int, boolean, int):void
      com.uc.c.ca.a(com.uc.c.f, int, int, int, int, int, int, boolean):boolean
      com.uc.c.ca.a(int, byte[], byte[], byte[], boolean, boolean, boolean, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x042b  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x0563 A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:198:0x072f A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:218:0x07c8 A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:249:0x0a21 A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:250:0x0a2f A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:259:0x0aa4 A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:260:0x0aeb A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:293:0x0bbc A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:338:0x0ca9 A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:345:0x0cf0 A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:384:0x0dc8 A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:405:0x0e67 A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:410:0x0e9a A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:411:0x0ea8 A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:412:0x0eb6 A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:413:0x0ec4 A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:414:0x0ed2 A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:415:0x0f2b A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:416:0x0f46 A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:417:0x0f54 A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:418:0x0f62 A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:424:0x0fab A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:428:0x1007 A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:443:0x0f70 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x01ba  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0209 A[Catch:{ Throwable -> 0x03b8, all -> 0x03fb }] */
    /* renamed from: do  reason: not valid java name */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m3do() {
        /*
            r43 = this;
            r5 = 4
            boolean[] r10 = new boolean[r5]
            r5 = 4
            byte[] r9 = new byte[r5]
            r5 = 7
            int[] r8 = new int[r5]
            r5 = 4
            short[] r11 = new short[r5]
            r5 = 5
            byte[][] r7 = new byte[r5][]
            r5 = -1
            r6 = -1
            r12 = 5
            byte[] r12 = new byte[r12]
            r13 = 0
            r14 = 0
            r15 = 0
            r16 = 0
            r17 = 0
            r18 = -1
            r19 = 0
            java.util.Vector r20 = new java.util.Vector
            r21 = 10
            r20.<init>(r21)
            com.uc.c.k.ow = r20
            r20 = 0
            r0 = r43
            com.uc.c.r r0 = r0.mS
            r21 = r0
            com.uc.a.n r21 = r21.ch()
            if (r21 == 0) goto L_0x0043
            r0 = r43
            com.uc.c.r r0 = r0.mS
            r21 = r0
            com.uc.a.n r21 = r21.ch()
            r21.sh()
        L_0x0043:
            long r27 = java.lang.System.currentTimeMillis()
            r21 = 400(0x190, double:1.976E-321)
            r29 = r20
            r30 = r19
            r31 = r15
            r32 = r6
            r33 = r5
            r19 = r13
            r15 = r18
            r18 = r14
            r13 = r21
            r42 = r16
            r16 = r17
            r17 = r42
        L_0x0061:
            r0 = r43
            java.io.DataInputStream r0 = r0.mL     // Catch:{ Throwable -> 0x03b8 }
            r6 = r0
            r5 = r43
            byte r5 = r5.a(r6, r7, r8, r9, r10, r11, r12)     // Catch:{ Throwable -> 0x03b8 }
            int r6 = r19 + 1
            if (r17 == 0) goto L_0x1077
            r19 = 12
            r0 = r5
            r1 = r19
            if (r0 == r1) goto L_0x1077
            r19 = 2
            r0 = r5
            r1 = r19
            if (r0 == r1) goto L_0x1077
            r15 = 0
            r17 = -1
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r19 = r0
            r19.KZ()     // Catch:{ Throwable -> 0x03b8 }
            r34 = r17
            r35 = r15
        L_0x008e:
            if (r16 == 0) goto L_0x1071
            r15 = 7
            if (r5 == r15) goto L_0x1071
            r15 = 37
            if (r5 == r15) goto L_0x1071
            r15 = 38
            if (r5 == r15) goto L_0x1071
            r15 = 0
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r16 = r0
            r0 = r16
            r1 = r18
            r0.iQ(r1)     // Catch:{ Throwable -> 0x03b8 }
            r16 = 0
            r36 = r15
            r37 = r16
        L_0x00af:
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            if (r15 == 0) goto L_0x00e7
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            boolean r15 = r15.GZ()     // Catch:{ Throwable -> 0x03b8 }
            if (r15 != 0) goto L_0x00e7
        L_0x00c1:
            r0 = r43
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            int r6 = r5.ahU
            int r6 = r6 + 45
            r5.ahU = r6
            r0 = r43
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            int r6 = r5.bFP
            r6 = r6 | 1
            r5.bFP = r6
            r0 = r43
            com.uc.c.bx r0 = r0.mT
            r5 = r0
            if (r5 == 0) goto L_0x00e6
            r0 = r43
            com.uc.c.bx r0 = r0.mT
            r5 = r0
            r5.Ho()
        L_0x00e6:
            return
        L_0x00e7:
            r15 = r6 & 127(0x7f, float:1.78E-43)
            if (r15 != 0) goto L_0x0108
            r15 = 1024(0x400, float:1.435E-42)
            boolean r15 = com.uc.c.w.bZ(r15)     // Catch:{ Throwable -> 0x03b8 }
            if (r15 == 0) goto L_0x0108
            r0 = r43
            com.uc.c.w r0 = r0.mR     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            r16 = 131072(0x20000, float:1.83671E-40)
            boolean r15 = r15.bW(r16)     // Catch:{ Throwable -> 0x03b8 }
            if (r15 != 0) goto L_0x0108
            r15 = 1024(0x400, float:1.435E-42)
            boolean r15 = com.uc.c.w.bZ(r15)     // Catch:{ Throwable -> 0x03b8 }
            if (r15 != 0) goto L_0x00c1
        L_0x0108:
            r15 = r6 & 31
            if (r15 != 0) goto L_0x106d
            long r15 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x03b8 }
            long r17 = r15 - r27
            int r17 = (r17 > r13 ? 1 : (r17 == r13 ? 0 : -1))
            if (r17 <= 0) goto L_0x0139
            r0 = r43
            com.uc.c.r r0 = r0.mS     // Catch:{ Throwable -> 0x03b8 }
            r17 = r0
            if (r17 == 0) goto L_0x0139
            r0 = r43
            com.uc.c.r r0 = r0.mS     // Catch:{ Throwable -> 0x03b8 }
            r17 = r0
            com.uc.a.n r17 = r17.ch()     // Catch:{ Throwable -> 0x03b8 }
            if (r17 == 0) goto L_0x0139
            r17.qW()     // Catch:{ Throwable -> 0x03b8 }
            r17 = 400(0x190, double:1.976E-321)
            long r13 = r13 + r17
            r17 = 2000(0x7d0, double:9.88E-321)
            int r17 = (r13 > r17 ? 1 : (r13 == r17 ? 0 : -1))
            if (r17 <= 0) goto L_0x0139
            r13 = 2000(0x7d0, double:9.88E-321)
        L_0x0139:
            long r17 = com.uc.c.w.Lg     // Catch:{ Throwable -> 0x03b8 }
            long r17 = r15 - r17
            long r19 = com.uc.c.w.QA     // Catch:{ Throwable -> 0x03b8 }
            int r17 = (r17 > r19 ? 1 : (r17 == r19 ? 0 : -1))
            if (r17 <= 0) goto L_0x106d
            r0 = r43
            com.uc.c.w r0 = r0.mR     // Catch:{ Throwable -> 0x03b8 }
            r17 = r0
            r0 = r17
            java.util.Vector r0 = r0.JR     // Catch:{ Throwable -> 0x03b8 }
            r17 = r0
            r0 = r43
            com.uc.c.w r0 = r0.mR     // Catch:{ Throwable -> 0x03b8 }
            r18 = r0
            r0 = r18
            int r0 = r0.JU     // Catch:{ Throwable -> 0x03b8 }
            r18 = r0
            java.lang.Object r17 = r17.elementAt(r18)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.r r0 = r0.mS     // Catch:{ Throwable -> 0x03b8 }
            r18 = r0
            r0 = r17
            r1 = r18
            if (r0 == r1) goto L_0x106d
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r17 = r0
            boolean r17 = r17.ac()     // Catch:{ Throwable -> 0x03b8 }
            if (r17 == 0) goto L_0x106d
            com.uc.c.w.Lg = r15     // Catch:{ OutOfMemoryError -> 0x01b2, Exception -> 0x01b6 }
            r0 = r43
            com.uc.c.r r0 = r0.mS     // Catch:{ OutOfMemoryError -> 0x01b2, Exception -> 0x01b6 }
            r15 = r0
            r0 = r43
            com.uc.c.r r0 = r0.mS     // Catch:{ OutOfMemoryError -> 0x01b2, Exception -> 0x01b6 }
            r16 = r0
            com.uc.c.ca r16 = r16.jj()     // Catch:{ OutOfMemoryError -> 0x01b2, Exception -> 0x01b6 }
            r17 = 1058642330(0x3f19999a, float:0.6)
            r18 = 1
            b.a.a.a.f r15 = r15.a(r16, r17, r18)     // Catch:{ OutOfMemoryError -> 0x01b2, Exception -> 0x01b6 }
            android.graphics.Bitmap r15 = r15.FQ     // Catch:{ OutOfMemoryError -> 0x01b2, Exception -> 0x01b6 }
            r0 = r43
            com.uc.c.r r0 = r0.mS     // Catch:{ OutOfMemoryError -> 0x01b2, Exception -> 0x01b6 }
            r16 = r0
            r0 = r16
            r1 = r15
            r0.a(r1)     // Catch:{ OutOfMemoryError -> 0x01b2, Exception -> 0x01b6 }
            r38 = r13
        L_0x01a1:
            switch(r5) {
                case 1: goto L_0x042b;
                case 2: goto L_0x07c8;
                case 3: goto L_0x0a2f;
                case 4: goto L_0x0aeb;
                case 5: goto L_0x0bbc;
                case 6: goto L_0x0cf0;
                case 7: goto L_0x0dc8;
                case 8: goto L_0x0ea8;
                case 9: goto L_0x0eb6;
                case 10: goto L_0x0ec4;
                case 11: goto L_0x0a21;
                case 12: goto L_0x042b;
                case 13: goto L_0x0ed2;
                case 14: goto L_0x0fab;
                case 32: goto L_0x0209;
                case 33: goto L_0x072f;
                case 34: goto L_0x0f2b;
                case 35: goto L_0x0aa4;
                case 36: goto L_0x0ca9;
                case 37: goto L_0x0e67;
                case 38: goto L_0x0e9a;
                case 39: goto L_0x01ba;
                case 40: goto L_0x1007;
                case 112: goto L_0x0f70;
                case 113: goto L_0x0f46;
                case 114: goto L_0x0f54;
                case 115: goto L_0x0f62;
                default: goto L_0x01a4;
            }
        L_0x01a4:
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x01b2:
            r15 = move-exception
            r38 = r13
            goto L_0x01a1
        L_0x01b6:
            r15 = move-exception
            r38 = r13
            goto L_0x01a1
        L_0x01ba:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r13 = 0
            r13 = r7[r13]     // Catch:{ Throwable -> 0x03b8 }
            r5.aB(r13)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.r r0 = r0.mS     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            if (r5 == 0) goto L_0x105f
            r0 = r43
            com.uc.c.r r0 = r0.mS     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            com.uc.a.n r5 = r5.ch()     // Catch:{ Throwable -> 0x03b8 }
            if (r5 == 0) goto L_0x105f
            r5 = 0
            r5 = r7[r5]     // Catch:{ Throwable -> 0x03b8 }
            if (r5 == 0) goto L_0x105f
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r13 = 0
            boolean r5 = r5.iJ(r13)     // Catch:{ Throwable -> 0x03b8 }
            if (r5 != 0) goto L_0x105f
            r0 = r43
            com.uc.c.r r0 = r0.mS     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            com.uc.a.n r5 = r5.ch()     // Catch:{ Throwable -> 0x03b8 }
            r13 = 0
            r13 = r7[r13]     // Catch:{ Throwable -> 0x03b8 }
            java.lang.String r13 = com.uc.c.bc.ak(r13)     // Catch:{ Throwable -> 0x03b8 }
            r5.bL(r13)     // Catch:{ Throwable -> 0x03b8 }
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x0209:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            int r13 = com.uc.c.w.MO     // Catch:{ Throwable -> 0x03b8 }
            int r14 = com.uc.c.w.MP     // Catch:{ Throwable -> 0x03b8 }
            int r15 = com.uc.c.w.MK     // Catch:{ Throwable -> 0x03b8 }
            int r16 = com.uc.c.w.MN     // Catch:{ Throwable -> 0x03b8 }
            r0 = r5
            r1 = r13
            r2 = r14
            r3 = r15
            r4 = r16
            r0.t(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            int r13 = com.uc.c.w.MM     // Catch:{ Throwable -> 0x03b8 }
            int r14 = com.uc.c.w.MN     // Catch:{ Throwable -> 0x03b8 }
            r5.bp(r13, r14)     // Catch:{ Throwable -> 0x03b8 }
            r5 = 0
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            int r5 = r5 >>> 20
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r13 = r0
            short r13 = r13.bGr     // Catch:{ Throwable -> 0x03b8 }
            int r13 = r13 << 1
            if (r5 <= r13) goto L_0x037b
            r13 = 0
            r13 = r8[r13]     // Catch:{ Throwable -> 0x03b8 }
            r14 = 1048575(0xfffff, float:1.469367E-39)
            r13 = r13 & r14
        L_0x0242:
            r14 = 2
            r14 = r8[r14]     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            r15.bq(r5, r13)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            boolean r15 = r15.ac()     // Catch:{ Throwable -> 0x03b8 }
            if (r15 == 0) goto L_0x0384
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r13 = r0
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            short r15 = r15.bGs     // Catch:{ Throwable -> 0x03b8 }
            int r13 = r13.jk(r15)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            int r15 = r15.ahU     // Catch:{ Throwable -> 0x03b8 }
            int r13 = java.lang.Math.max(r13, r15)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            r15.ahU = r13     // Catch:{ Throwable -> 0x03b8 }
        L_0x027a:
            r0 = r43
            com.uc.c.r r0 = r0.mS     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            if (r15 == 0) goto L_0x02a2
            r0 = r43
            com.uc.c.r r0 = r0.mS     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            com.uc.a.n r15 = r15.ch()     // Catch:{ Throwable -> 0x03b8 }
            if (r15 == 0) goto L_0x02a2
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            r0 = r43
            com.uc.c.r r0 = r0.mS     // Catch:{ Throwable -> 0x03b8 }
            r16 = r0
            com.uc.a.n r16 = r16.ch()     // Catch:{ Throwable -> 0x03b8 }
            int r16 = r16.getOrientation()     // Catch:{ Throwable -> 0x03b8 }
            r15.e(r16)     // Catch:{ Throwable -> 0x03b8 }
        L_0x02a2:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            boolean r15 = r15.ac()     // Catch:{ Throwable -> 0x03b8 }
            if (r15 == 0) goto L_0x03ef
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            float r16 = com.uc.c.w.Qy     // Catch:{ Throwable -> 0x03b8 }
            r15.i(r16)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            int r16 = com.uc.c.w.MM     // Catch:{ Throwable -> 0x03b8 }
            r0 = r16
            float r0 = (float) r0     // Catch:{ Throwable -> 0x03b8 }
            r16 = r0
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r17 = r0
            r0 = r17
            int r0 = r0.bGm     // Catch:{ Throwable -> 0x03b8 }
            r17 = r0
            r0 = r17
            float r0 = (float) r0     // Catch:{ Throwable -> 0x03b8 }
            r17 = r0
            float r16 = r16 / r17
            r15.j(r16)     // Catch:{ Throwable -> 0x03b8 }
        L_0x02d9:
            r0 = r43
            com.uc.c.r r0 = r0.mS     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            if (r15 == 0) goto L_0x030d
            r0 = r43
            com.uc.c.r r0 = r0.mS     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            com.uc.a.n r15 = r15.ch()     // Catch:{ Throwable -> 0x03b8 }
            if (r15 == 0) goto L_0x030d
            r0 = r43
            com.uc.c.r r0 = r0.mS     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            com.uc.a.n r15 = r15.ch()     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r16 = r0
            r0 = r16
            int r0 = r0.bGm     // Catch:{ Throwable -> 0x03b8 }
            r16 = r0
            int r17 = com.uc.c.w.MM     // Catch:{ Throwable -> 0x03b8 }
            r0 = r16
            r1 = r17
            if (r0 > r1) goto L_0x0422
            r16 = 1
        L_0x030a:
            r15.by(r16)     // Catch:{ Throwable -> 0x03b8 }
        L_0x030d:
            r15 = 3
            r15 = r8[r15]     // Catch:{ Throwable -> 0x03b8 }
            int r15 = r15 >>> 20
            r16 = 3
            r16 = r8[r16]     // Catch:{ Throwable -> 0x03b8 }
            r17 = 1048575(0xfffff, float:1.469367E-39)
            r16 = r16 & r17
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r17 = r0
            r18 = 3
            r19 = 0
            byte r19 = r12[r19]     // Catch:{ Throwable -> 0x03b8 }
            r20 = 4
            boolean r19 = com.uc.c.bc.aL(r19, r20)     // Catch:{ Throwable -> 0x03b8 }
            if (r19 == 0) goto L_0x0426
        L_0x032f:
            r0 = r17
            r1 = r18
            r2 = r14
            r0.bs(r1, r2)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r14 = r0
            r14.KV()     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r14 = r0
            r17 = 0
            r18 = 0
            r0 = r14
            r1 = r17
            r2 = r18
            r3 = r5
            r4 = r13
            r0.w(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            boolean r5 = r5.ac()     // Catch:{ Throwable -> 0x03b8 }
            if (r5 == 0) goto L_0x0369
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r0 = r5
            r1 = r15
            r2 = r16
            r0.bx(r1, r2)     // Catch:{ Throwable -> 0x03b8 }
        L_0x0369:
            r13 = r38
            r17 = r35
            r18 = r37
            r19 = r6
            r32 = r16
            r33 = r15
            r16 = r36
            r15 = r34
            goto L_0x0061
        L_0x037b:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r13 = r0
            short r13 = r13.bGq     // Catch:{ Throwable -> 0x03b8 }
            goto L_0x0242
        L_0x0384:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            int r15 = r15.bGm     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r16 = r0
            r0 = r16
            short r0 = r0.bGr     // Catch:{ Throwable -> 0x03b8 }
            r16 = r0
            r0 = r15
            r1 = r16
            if (r0 <= r1) goto L_0x027a
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            r16 = -1
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r17 = r0
            r0 = r17
            short r0 = r0.bGs     // Catch:{ Throwable -> 0x03b8 }
            r17 = r0
            byte r18 = com.uc.c.w.Ke     // Catch:{ Throwable -> 0x03b8 }
            int r17 = r17 - r18
            r15.bp(r16, r17)     // Catch:{ Throwable -> 0x03b8 }
            goto L_0x027a
        L_0x03b8:
            r5 = move-exception
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ all -> 0x03fb }
            r5 = r0
            if (r5 == 0) goto L_0x03c8
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ all -> 0x03fb }
            r5 = r0
            r6 = 0
            r5.bzc = r6     // Catch:{ all -> 0x03fb }
        L_0x03c8:
            r0 = r43
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            int r6 = r5.ahU
            int r6 = r6 + 45
            r5.ahU = r6
            r0 = r43
            com.uc.c.ca r0 = r0.dt
            r5 = r0
            int r6 = r5.bFP
            r6 = r6 | 1
            r5.bFP = r6
            r0 = r43
            com.uc.c.bx r0 = r0.mT
            r5 = r0
            if (r5 == 0) goto L_0x00e6
            r0 = r43
            com.uc.c.bx r0 = r0.mT
            r5 = r0
            r5.Ho()
            goto L_0x00e6
        L_0x03ef:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            r16 = 1065353216(0x3f800000, float:1.0)
            r15.i(r16)     // Catch:{ Throwable -> 0x03b8 }
            goto L_0x02d9
        L_0x03fb:
            r5 = move-exception
            r0 = r43
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            int r7 = r6.ahU
            int r7 = r7 + 45
            r6.ahU = r7
            r0 = r43
            com.uc.c.ca r0 = r0.dt
            r6 = r0
            int r7 = r6.bFP
            r7 = r7 | 1
            r6.bFP = r7
            r0 = r43
            com.uc.c.bx r0 = r0.mT
            r6 = r0
            if (r6 == 0) goto L_0x0421
            r0 = r43
            com.uc.c.bx r0 = r0.mT
            r6 = r0
            r6.Ho()
        L_0x0421:
            throw r5
        L_0x0422:
            r16 = 0
            goto L_0x030a
        L_0x0426:
            r14 = 16777215(0xffffff, float:2.3509886E-38)
            goto L_0x032f
        L_0x042b:
            r13 = 0
            r13 = r8[r13]     // Catch:{ Throwable -> 0x03b8 }
            int r13 = r13 >>> 20
            r14 = 0
            r14 = r8[r14]     // Catch:{ Throwable -> 0x03b8 }
            r15 = 1048575(0xfffff, float:1.469367E-39)
            r14 = r14 & r15
            r15 = 1
            r15 = r8[r15]     // Catch:{ Throwable -> 0x03b8 }
            int r15 = r15 >>> 20
            r16 = 1
            r16 = r8[r16]     // Catch:{ Throwable -> 0x03b8 }
            r17 = 1048575(0xfffff, float:1.469367E-39)
            r16 = r16 & r17
            r17 = 1
            r17 = r7[r17]     // Catch:{ Throwable -> 0x03b8 }
            r18 = 0
            char[] r17 = com.uc.c.bc.d(r17, r18)     // Catch:{ Throwable -> 0x03b8 }
            r18 = 1
            r0 = r5
            r1 = r18
            if (r0 == r1) goto L_0x0462
            r18 = 0
            byte r18 = r12[r18]     // Catch:{ Throwable -> 0x03b8 }
            r19 = 4
            boolean r18 = com.uc.c.bc.aL(r18, r19)     // Catch:{ Throwable -> 0x03b8 }
            if (r18 == 0) goto L_0x05e4
        L_0x0462:
            r18 = 2
            r18 = r8[r18]     // Catch:{ Throwable -> 0x03b8 }
        L_0x0466:
            r19 = 0
            byte r19 = r9[r19]     // Catch:{ Throwable -> 0x03b8 }
            r20 = 1
            byte r20 = r9[r20]     // Catch:{ Throwable -> 0x03b8 }
            r21 = 12
            r0 = r5
            r1 = r21
            if (r0 != r1) goto L_0x05ec
            r21 = 0
            short r21 = r11[r21]     // Catch:{ Throwable -> 0x03b8 }
        L_0x0479:
            r22 = 12
            r0 = r5
            r1 = r22
            if (r0 != r1) goto L_0x1053
            r0 = r43
            r1 = r21
            r0.X(r1)     // Catch:{ Throwable -> 0x03b8 }
            r5 = 0
            short r21 = r11[r5]     // Catch:{ Throwable -> 0x03b8 }
            if (r21 != 0) goto L_0x061e
            if (r30 == 0) goto L_0x061e
            if (r35 != 0) goto L_0x1053
            r0 = r30
            int r0 = r0.length     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r21 = 1
            int r5 = r5 - r21
            byte[] r5 = new byte[r5]     // Catch:{ Throwable -> 0x03b8 }
            r21 = 0
            r22 = 0
            r0 = r5
            int r0 = r0.length     // Catch:{ Throwable -> 0x03b8 }
            r23 = r0
            r0 = r30
            r1 = r21
            r2 = r5
            r3 = r22
            r4 = r23
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r30
            int r0 = r0.length     // Catch:{ Throwable -> 0x03b8 }
            r21 = r0
            r22 = 1
            int r21 = r21 - r22
            byte r21 = r30[r21]     // Catch:{ Throwable -> 0x03b8 }
            r0 = r21
            r0 = r0 & 255(0xff, float:3.57E-43)
            r21 = r0
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r22 = r0
            r23 = 0
            r24 = 0
            r0 = r22
            r1 = r23
            r2 = r24
            r3 = r5
            r4 = r21
            r0.a(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            boolean r0 = r0.nc     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            if (r5 == 0) goto L_0x105b
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            java.lang.String[] r5 = r5.bxL     // Catch:{ Throwable -> 0x03b8 }
            if (r5 == 0) goto L_0x05f0
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            java.lang.String[] r5 = r5.bxL     // Catch:{ Throwable -> 0x03b8 }
            r21 = 0
            r5 = r5[r21]     // Catch:{ Throwable -> 0x03b8 }
            if (r5 == 0) goto L_0x05f0
            if (r17 == 0) goto L_0x05f0
            r0 = r17
            int r0 = r0.length     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            if (r5 <= 0) goto L_0x05f0
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            java.lang.String[] r5 = r5.bxL     // Catch:{ Throwable -> 0x03b8 }
            r21 = 0
            r5 = r5[r21]     // Catch:{ Throwable -> 0x03b8 }
            java.lang.String r21 = r17.toString()     // Catch:{ Throwable -> 0x03b8 }
            r0 = r5
            r1 = r21
            boolean r5 = r0.contains(r1)     // Catch:{ Throwable -> 0x03b8 }
            if (r5 != 0) goto L_0x0514
            if (r29 != 0) goto L_0x061a
        L_0x0514:
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r21 = r0
            byte[] r21 = r21.KU()     // Catch:{ Throwable -> 0x03b8 }
            r0 = r21
            r1 = r5
            r1.bxW = r0     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r21 = r0
            r0 = r21
            com.uc.c.f r0 = r0.bGZ     // Catch:{ Throwable -> 0x03b8 }
            r21 = r0
            r0 = r21
            r1 = r5
            r1.bxX = r0     // Catch:{ Throwable -> 0x03b8 }
            r5 = 1
        L_0x053f:
            r21 = 0
            r0 = r21
            r1 = r43
            r1.nc = r0     // Catch:{ Throwable -> 0x03b8 }
        L_0x0547:
            r21 = 1
            r22 = r21
            r21 = r34
        L_0x054d:
            int r20 = com.uc.c.bc.gp(r20)     // Catch:{ Throwable -> 0x03b8 }
            int r19 = com.uc.b.e.bn(r19)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r20
            r1 = r19
            b.a.a.a.k r23 = com.uc.b.e.I(r0, r1)     // Catch:{ Throwable -> 0x03b8 }
            int r24 = r23.uN()     // Catch:{ Throwable -> 0x03b8 }
            if (r15 != 0) goto L_0x056b
            r0 = r23
            r1 = r17
            int r15 = r0.a(r1)     // Catch:{ Throwable -> 0x03b8 }
        L_0x056b:
            if (r16 == 0) goto L_0x0579
            r0 = r16
            r1 = r19
            if (r0 < r1) goto L_0x057b
            r0 = r16
            r1 = r24
            if (r0 > r1) goto L_0x057b
        L_0x0579:
            r16 = r24
        L_0x057b:
            if (r17 == 0) goto L_0x05d4
            r0 = r17
            int r0 = r0.length     // Catch:{ Throwable -> 0x03b8 }
            r23 = r0
            if (r23 <= 0) goto L_0x05d4
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r23 = r0
            r24 = 1
            r0 = r23
            r1 = r24
            r2 = r19
            r0.bs(r1, r2)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r19 = r0
            r23 = 29
            r0 = r19
            r1 = r23
            r2 = r20
            r0.bs(r1, r2)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r19 = r0
            r20 = 2
            r0 = r19
            r1 = r20
            r2 = r18
            r0.bs(r1, r2)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r18 = r0
            r0 = r18
            r1 = r17
            r0.f(r1)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r17 = r0
            r0 = r17
            r1 = r13
            r2 = r14
            r3 = r15
            r4 = r16
            r0.x(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
        L_0x05d4:
            r13 = r38
            r29 = r5
            r15 = r21
            r16 = r36
            r17 = r22
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x05e4:
            int[] r18 = com.uc.c.az.bdf     // Catch:{ Throwable -> 0x03b8 }
            r19 = 142(0x8e, float:1.99E-43)
            r18 = r18[r19]     // Catch:{ Throwable -> 0x03b8 }
            goto L_0x0466
        L_0x05ec:
            r21 = -1
            goto L_0x0479
        L_0x05f0:
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r21 = r0
            byte[] r21 = r21.KU()     // Catch:{ Throwable -> 0x03b8 }
            r0 = r21
            r1 = r5
            r1.bxW = r0     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r21 = r0
            r0 = r21
            com.uc.c.f r0 = r0.bGZ     // Catch:{ Throwable -> 0x03b8 }
            r21 = r0
            r0 = r21
            r1 = r5
            r1.bxX = r0     // Catch:{ Throwable -> 0x03b8 }
        L_0x061a:
            r5 = r29
            goto L_0x053f
        L_0x061e:
            r0 = r43
            r1 = r21
            java.lang.Object r5 = r0.X(r1)     // Catch:{ Throwable -> 0x03b8 }
            byte[] r5 = (byte[]) r5     // Catch:{ Throwable -> 0x03b8 }
            byte[] r5 = (byte[]) r5     // Catch:{ Throwable -> 0x03b8 }
            if (r5 == 0) goto L_0x1053
            r22 = 0
            byte r22 = r5[r22]     // Catch:{ Throwable -> 0x03b8 }
            r23 = 14
            r0 = r22
            r1 = r23
            if (r0 != r1) goto L_0x1053
            r0 = r34
            r1 = r21
            if (r0 == r1) goto L_0x1053
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r22 = r0
            r22.KZ()     // Catch:{ Throwable -> 0x03b8 }
            r0 = r5
            int r0 = r0.length     // Catch:{ Throwable -> 0x03b8 }
            r22 = r0
            r23 = 1
            int r22 = r22 - r23
            r0 = r22
            byte[] r0 = new byte[r0]     // Catch:{ Throwable -> 0x03b8 }
            r22 = r0
            r23 = 0
            r24 = 0
            r0 = r22
            int r0 = r0.length     // Catch:{ Throwable -> 0x03b8 }
            r25 = r0
            r0 = r5
            r1 = r23
            r2 = r22
            r3 = r24
            r4 = r25
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r5
            int r0 = r0.length     // Catch:{ Throwable -> 0x03b8 }
            r23 = r0
            r24 = 1
            int r23 = r23 - r24
            byte r5 = r5[r23]     // Catch:{ Throwable -> 0x03b8 }
            r5 = r5 & 255(0xff, float:3.57E-43)
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r23 = r0
            r24 = 0
            r25 = 0
            r0 = r23
            r1 = r24
            r2 = r25
            r3 = r22
            r4 = r5
            r0.a(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            boolean r0 = r0.nc     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            if (r5 == 0) goto L_0x104f
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            java.lang.String[] r5 = r5.bxL     // Catch:{ Throwable -> 0x03b8 }
            if (r5 == 0) goto L_0x0702
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            java.lang.String[] r5 = r5.bxL     // Catch:{ Throwable -> 0x03b8 }
            r22 = 0
            r5 = r5[r22]     // Catch:{ Throwable -> 0x03b8 }
            if (r5 == 0) goto L_0x0702
            if (r17 == 0) goto L_0x0702
            r0 = r17
            int r0 = r0.length     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            if (r5 <= 0) goto L_0x0702
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            java.lang.String[] r5 = r5.bxL     // Catch:{ Throwable -> 0x03b8 }
            r22 = 0
            r5 = r5[r22]     // Catch:{ Throwable -> 0x03b8 }
            java.lang.String r22 = r17.toString()     // Catch:{ Throwable -> 0x03b8 }
            r0 = r5
            r1 = r22
            boolean r5 = r0.contains(r1)     // Catch:{ Throwable -> 0x03b8 }
            if (r5 != 0) goto L_0x06cb
            if (r29 != 0) goto L_0x072c
        L_0x06cb:
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r22 = r0
            byte[] r22 = r22.KU()     // Catch:{ Throwable -> 0x03b8 }
            r0 = r22
            r1 = r5
            r1.bxW = r0     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r22 = r0
            r0 = r22
            com.uc.c.f r0 = r0.bGZ     // Catch:{ Throwable -> 0x03b8 }
            r22 = r0
            r0 = r22
            r1 = r5
            r1.bxX = r0     // Catch:{ Throwable -> 0x03b8 }
            r5 = 1
        L_0x06f6:
            r22 = 0
            r0 = r22
            r1 = r43
            r1.nc = r0     // Catch:{ Throwable -> 0x03b8 }
        L_0x06fe:
            r22 = 1
            goto L_0x054d
        L_0x0702:
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r22 = r0
            byte[] r22 = r22.KU()     // Catch:{ Throwable -> 0x03b8 }
            r0 = r22
            r1 = r5
            r1.bxW = r0     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r22 = r0
            r0 = r22
            com.uc.c.f r0 = r0.bGZ     // Catch:{ Throwable -> 0x03b8 }
            r22 = r0
            r0 = r22
            r1 = r5
            r1.bxX = r0     // Catch:{ Throwable -> 0x03b8 }
        L_0x072c:
            r5 = r29
            goto L_0x06f6
        L_0x072f:
            r5 = 0
            r13 = r7[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 0
            short r14 = r11[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 1
            short r5 = r11[r5]     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            r1 = r5
            java.lang.Object r5 = r0.X(r1)     // Catch:{ Throwable -> 0x03b8 }
            if (r5 == 0) goto L_0x0745
            boolean r15 = r5 instanceof java.lang.Integer     // Catch:{ Throwable -> 0x03b8 }
            if (r15 != 0) goto L_0x07be
        L_0x0745:
            r5 = 0
        L_0x0746:
            if (r5 == 0) goto L_0x074e
            java.lang.String r13 = "wml:anchor"
            byte[] r13 = com.uc.c.bc.dT(r13)     // Catch:{ Throwable -> 0x03b8 }
        L_0x074e:
            r15 = 2
            r15 = r7[r15]     // Catch:{ Throwable -> 0x03b8 }
            if (r13 == 0) goto L_0x07c5
            r0 = r13
            int r0 = r0.length     // Catch:{ Throwable -> 0x03b8 }
            r16 = r0
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r17 = r0
            r0 = r17
            int r0 = r0.bxK     // Catch:{ Throwable -> 0x03b8 }
            r17 = r0
            r0 = r16
            r1 = r17
            if (r0 != r1) goto L_0x07c5
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r16 = r0
            r0 = r16
            byte[] r0 = r0.bxJ     // Catch:{ Throwable -> 0x03b8 }
            r16 = r0
            r0 = r16
            r1 = r13
            boolean r16 = com.uc.c.bc.g(r0, r1)     // Catch:{ Throwable -> 0x03b8 }
            if (r16 == 0) goto L_0x07c5
            r16 = 1
        L_0x0780:
            r0 = r16
            r1 = r43
            r1.nc = r0     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r16 = r0
            r0 = r16
            r1 = r13
            r2 = r15
            r3 = r5
            byte[] r5 = r0.c(r1, r2, r3)     // Catch:{ Throwable -> 0x03b8 }
            if (r14 == 0) goto L_0x07ae
            java.util.Vector r13 = com.uc.c.k.ow     // Catch:{ Throwable -> 0x03b8 }
            r15 = 2
            java.lang.Object[] r15 = new java.lang.Object[r15]     // Catch:{ Throwable -> 0x03b8 }
            r16 = 0
            java.lang.Integer r17 = new java.lang.Integer     // Catch:{ Throwable -> 0x03b8 }
            r0 = r17
            r1 = r14
            r0.<init>(r1)     // Catch:{ Throwable -> 0x03b8 }
            r15[r16] = r17     // Catch:{ Throwable -> 0x03b8 }
            r14 = 1
            r15[r14] = r5     // Catch:{ Throwable -> 0x03b8 }
            r13.addElement(r15)     // Catch:{ Throwable -> 0x03b8 }
        L_0x07ae:
            r13 = r38
            r30 = r5
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x07be:
            java.lang.Integer r5 = (java.lang.Integer) r5     // Catch:{ Throwable -> 0x03b8 }
            int r5 = r5.intValue()     // Catch:{ Throwable -> 0x03b8 }
            goto L_0x0746
        L_0x07c5:
            r16 = 0
            goto L_0x0780
        L_0x07c8:
            r5 = 0
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            int r13 = r5 >>> 20
            r5 = 0
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            r14 = 1048575(0xfffff, float:1.469367E-39)
            r14 = r14 & r5
            r5 = 1
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            int r15 = r5 >>> 20
            r5 = 1
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            r16 = 1048575(0xfffff, float:1.469367E-39)
            r18 = r5 & r16
            r5 = 0
            short r16 = r11[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 0
            r19 = r7[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 1
            r17 = r7[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 1
            short r5 = r11[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 6
            r20 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 1
            short r21 = r11[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = -1
            r0 = r21
            r1 = r5
            if (r0 != r1) goto L_0x08e7
            if (r35 == 0) goto L_0x1049
            r5 = 0
            r21 = -1
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r22 = r0
            r22.KZ()     // Catch:{ Throwable -> 0x03b8 }
            r22 = r5
        L_0x0809:
            if (r20 == 0) goto L_0x0831
            int r5 = r20 >>> 20
            r23 = 1048575(0xfffff, float:1.469367E-39)
            r20 = r20 & r23
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r23 = r0
            r24 = 14
            r0 = r23
            r1 = r24
            r2 = r5
            r0.bs(r1, r2)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r23 = 15
            r0 = r5
            r1 = r23
            r2 = r20
            r0.bs(r1, r2)     // Catch:{ Throwable -> 0x03b8 }
        L_0x0831:
            r5 = 4
            byte[] r5 = new byte[r5]     // Catch:{ Throwable -> 0x03b8 }
            r20 = 0
            r0 = r5
            r1 = r20
            r2 = r16
            com.uc.c.bc.m(r0, r1, r2)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r20 = r0
            r23 = 0
            r0 = r20
            r1 = r5
            r2 = r17
            r3 = r23
            int r17 = r0.b(r1, r2, r3)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r0 = r5
            r1 = r13
            r2 = r14
            r3 = r15
            r4 = r18
            r0.x(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
            r5 = 4
            r14 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 5
            r15 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 3
            r18 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            r13 = r43
            r13.a(r14, r15, r16, r17, r18)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            java.util.Hashtable r0 = r0.nS     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            java.lang.String r13 = java.lang.Integer.toString(r16)     // Catch:{ Throwable -> 0x03b8 }
            r14 = 2
            java.lang.Object[] r14 = new java.lang.Object[r14]     // Catch:{ Throwable -> 0x03b8 }
            r15 = 0
            r18 = 1
            r0 = r18
            int[] r0 = new int[r0]     // Catch:{ Throwable -> 0x03b8 }
            r18 = r0
            r20 = 0
            r18[r20] = r17     // Catch:{ Throwable -> 0x03b8 }
            r14[r15] = r18     // Catch:{ Throwable -> 0x03b8 }
            r15 = 1
            r14[r15] = r19     // Catch:{ Throwable -> 0x03b8 }
            r5.put(r13, r14)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            com.uc.c.f r5 = r5.bGZ     // Catch:{ Throwable -> 0x03b8 }
            if (r5 == 0) goto L_0x08d9
            r0 = r43
            java.util.Hashtable r0 = r0.nT     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            java.lang.String r13 = java.lang.Integer.toString(r16)     // Catch:{ Throwable -> 0x03b8 }
            boolean r5 = r5.contains(r13)     // Catch:{ Throwable -> 0x03b8 }
            if (r5 == 0) goto L_0x0a0e
            r0 = r43
            java.util.Hashtable r0 = r0.nT     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            java.lang.String r13 = java.lang.Integer.toString(r16)     // Catch:{ Throwable -> 0x03b8 }
            java.lang.Object r5 = r5.get(r13)     // Catch:{ Throwable -> 0x03b8 }
            java.util.Vector r5 = (java.util.Vector) r5     // Catch:{ Throwable -> 0x03b8 }
        L_0x08b5:
            r13 = 2
            java.lang.Object[] r13 = new java.lang.Object[r13]     // Catch:{ Throwable -> 0x03b8 }
            r14 = 0
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            com.uc.c.f r15 = r15.bGZ     // Catch:{ Throwable -> 0x03b8 }
            r13[r14] = r15     // Catch:{ Throwable -> 0x03b8 }
            r14 = 1
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            com.uc.c.f r15 = r15.bGZ     // Catch:{ Throwable -> 0x03b8 }
            short r15 = r15.cF     // Catch:{ Throwable -> 0x03b8 }
            r16 = 1
            int r15 = r15 - r16
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)     // Catch:{ Throwable -> 0x03b8 }
            r13[r14] = r15     // Catch:{ Throwable -> 0x03b8 }
            r5.add(r13)     // Catch:{ Throwable -> 0x03b8 }
        L_0x08d9:
            r13 = r38
            r15 = r21
            r16 = r36
            r17 = r22
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x08e7:
            if (r21 != 0) goto L_0x096a
            if (r30 == 0) goto L_0x096a
            if (r35 != 0) goto L_0x1049
            r0 = r30
            int r0 = r0.length     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r21 = 1
            int r5 = r5 - r21
            byte[] r5 = new byte[r5]     // Catch:{ Throwable -> 0x03b8 }
            r21 = 0
            r22 = 0
            r0 = r5
            int r0 = r0.length     // Catch:{ Throwable -> 0x03b8 }
            r23 = r0
            r0 = r30
            r1 = r21
            r2 = r5
            r3 = r22
            r4 = r23
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r30
            int r0 = r0.length     // Catch:{ Throwable -> 0x03b8 }
            r21 = r0
            r22 = 1
            int r21 = r21 - r22
            byte r21 = r30[r21]     // Catch:{ Throwable -> 0x03b8 }
            r0 = r21
            r0 = r0 & 255(0xff, float:3.57E-43)
            r21 = r0
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r22 = r0
            r23 = 0
            r24 = 0
            r0 = r22
            r1 = r23
            r2 = r24
            r3 = r5
            r4 = r21
            r0.a(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            boolean r0 = r0.nc     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            if (r5 == 0) goto L_0x0963
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r21 = r0
            byte[] r21 = r21.KU()     // Catch:{ Throwable -> 0x03b8 }
            r0 = r21
            r1 = r5
            r1.bxW = r0     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r21 = r0
            r0 = r21
            com.uc.c.f r0 = r0.bGZ     // Catch:{ Throwable -> 0x03b8 }
            r21 = r0
            r0 = r21
            r1 = r5
            r1.bxX = r0     // Catch:{ Throwable -> 0x03b8 }
        L_0x0963:
            r5 = 1
            r21 = r34
            r22 = r5
            goto L_0x0809
        L_0x096a:
            r0 = r43
            r1 = r21
            java.lang.Object r5 = r0.X(r1)     // Catch:{ Throwable -> 0x03b8 }
            byte[] r5 = (byte[]) r5     // Catch:{ Throwable -> 0x03b8 }
            byte[] r5 = (byte[]) r5     // Catch:{ Throwable -> 0x03b8 }
            if (r5 == 0) goto L_0x1049
            r22 = 0
            byte r22 = r5[r22]     // Catch:{ Throwable -> 0x03b8 }
            r23 = 14
            r0 = r22
            r1 = r23
            if (r0 != r1) goto L_0x1049
            r0 = r34
            r1 = r21
            if (r0 == r1) goto L_0x1049
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r22 = r0
            r22.KZ()     // Catch:{ Throwable -> 0x03b8 }
            r0 = r5
            int r0 = r0.length     // Catch:{ Throwable -> 0x03b8 }
            r22 = r0
            r23 = 1
            int r22 = r22 - r23
            r0 = r22
            byte[] r0 = new byte[r0]     // Catch:{ Throwable -> 0x03b8 }
            r22 = r0
            r23 = 0
            r24 = 0
            r0 = r22
            int r0 = r0.length     // Catch:{ Throwable -> 0x03b8 }
            r25 = r0
            r0 = r5
            r1 = r23
            r2 = r22
            r3 = r24
            r4 = r25
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r5
            int r0 = r0.length     // Catch:{ Throwable -> 0x03b8 }
            r23 = r0
            r24 = 1
            int r23 = r23 - r24
            byte r5 = r5[r23]     // Catch:{ Throwable -> 0x03b8 }
            r5 = r5 & 255(0xff, float:3.57E-43)
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r23 = r0
            r24 = 0
            r25 = 0
            r0 = r23
            r1 = r24
            r2 = r25
            r3 = r22
            r4 = r5
            r0.a(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            boolean r0 = r0.nc     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            if (r5 == 0) goto L_0x0a09
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r22 = r0
            byte[] r22 = r22.KU()     // Catch:{ Throwable -> 0x03b8 }
            r0 = r22
            r1 = r5
            r1.bxW = r0     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.bx r0 = r0.mT     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r22 = r0
            r0 = r22
            com.uc.c.f r0 = r0.bGZ     // Catch:{ Throwable -> 0x03b8 }
            r22 = r0
            r0 = r22
            r1 = r5
            r1.bxX = r0     // Catch:{ Throwable -> 0x03b8 }
        L_0x0a09:
            r5 = 1
            r22 = r5
            goto L_0x0809
        L_0x0a0e:
            java.util.Vector r5 = new java.util.Vector     // Catch:{ Throwable -> 0x03b8 }
            r5.<init>()     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            java.util.Hashtable r0 = r0.nT     // Catch:{ Throwable -> 0x03b8 }
            r13 = r0
            java.lang.String r14 = java.lang.Integer.toString(r16)     // Catch:{ Throwable -> 0x03b8 }
            r13.put(r14, r5)     // Catch:{ Throwable -> 0x03b8 }
            goto L_0x08b5
        L_0x0a21:
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x0a2f:
            r5 = 0
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            int r5 = r5 >>> 20
            r13 = 0
            r13 = r8[r13]     // Catch:{ Throwable -> 0x03b8 }
            r14 = 1048575(0xfffff, float:1.469367E-39)
            r13 = r13 & r14
            r14 = 1
            r14 = r8[r14]     // Catch:{ Throwable -> 0x03b8 }
            int r14 = r14 >>> 20
            r15 = 1
            r15 = r8[r15]     // Catch:{ Throwable -> 0x03b8 }
            r16 = 1048575(0xfffff, float:1.469367E-39)
            r15 = r15 & r16
            r16 = 0
            byte r16 = r12[r16]     // Catch:{ Throwable -> 0x03b8 }
            r17 = 4
            boolean r16 = com.uc.c.bc.aL(r16, r17)     // Catch:{ Throwable -> 0x03b8 }
            if (r16 == 0) goto L_0x0a86
            r16 = 2
            r16 = r8[r16]     // Catch:{ Throwable -> 0x03b8 }
        L_0x0a58:
            r17 = -1
            r0 = r16
            r1 = r17
            if (r0 != r1) goto L_0x0a89
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r16 = r0
            r16.Le()     // Catch:{ Throwable -> 0x03b8 }
        L_0x0a69:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r16 = r0
            r0 = r16
            r1 = r5
            r2 = r13
            r3 = r14
            r4 = r15
            r0.x(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x0a86:
            r16 = -1
            goto L_0x0a58
        L_0x0a89:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r17 = r0
            r18 = 3
            r0 = r17
            r1 = r18
            r2 = r16
            r0.bs(r1, r2)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r16 = r0
            r16.Lf()     // Catch:{ Throwable -> 0x03b8 }
            goto L_0x0a69
        L_0x0aa4:
            r5 = 0
            short r5 = r11[r5]     // Catch:{ Throwable -> 0x03b8 }
            r13 = 0
            r14 = r7[r13]     // Catch:{ Throwable -> 0x03b8 }
            r13 = 0
            byte r15 = r9[r13]     // Catch:{ Throwable -> 0x03b8 }
            r13 = 1
            byte r16 = r9[r13]     // Catch:{ Throwable -> 0x03b8 }
            r13 = 1
            r17 = r7[r13]     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r13 = r0
            r18 = 0
            int r13 = r13.a(r14, r15, r16, r17, r18)     // Catch:{ Throwable -> 0x03b8 }
            java.util.Vector r14 = com.uc.c.k.ow     // Catch:{ Throwable -> 0x03b8 }
            r15 = 2
            java.lang.Integer[] r15 = new java.lang.Integer[r15]     // Catch:{ Throwable -> 0x03b8 }
            r16 = 0
            java.lang.Integer r17 = new java.lang.Integer     // Catch:{ Throwable -> 0x03b8 }
            r0 = r17
            r1 = r5
            r0.<init>(r1)     // Catch:{ Throwable -> 0x03b8 }
            r15[r16] = r17     // Catch:{ Throwable -> 0x03b8 }
            r5 = 1
            java.lang.Integer r16 = new java.lang.Integer     // Catch:{ Throwable -> 0x03b8 }
            r0 = r16
            r1 = r13
            r0.<init>(r1)     // Catch:{ Throwable -> 0x03b8 }
            r15[r5] = r16     // Catch:{ Throwable -> 0x03b8 }
            r14.addElement(r15)     // Catch:{ Throwable -> 0x03b8 }
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x0aeb:
            r5 = 0
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            int r24 = r5 >>> 20
            r5 = 0
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            r13 = 1048575(0xfffff, float:1.469367E-39)
            r25 = r5 & r13
            r5 = 1
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            int r18 = r5 >>> 20
            r5 = 1
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            r13 = 1048575(0xfffff, float:1.469367E-39)
            r19 = r5 & r13
            r5 = 0
            byte r13 = r9[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 0
            short r5 = r11[r5]     // Catch:{ Throwable -> 0x03b8 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Throwable -> 0x03b8 }
            byte[] r16 = r5.getBytes()     // Catch:{ Throwable -> 0x03b8 }
            r5 = 0
            r15 = r7[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 1
            r17 = r7[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 1
            byte r5 = r9[r5]     // Catch:{ Throwable -> 0x03b8 }
            r14 = 1
            short r14 = r11[r14]     // Catch:{ Throwable -> 0x03b8 }
            r20 = r5 & 1
            if (r20 == 0) goto L_0x0b73
            r20 = 1
        L_0x0b25:
            r5 = r5 & 2
            if (r5 == 0) goto L_0x0b76
            r5 = 1
            r21 = r5
        L_0x0b2c:
            r5 = 2
            byte r5 = r9[r5]     // Catch:{ Throwable -> 0x03b8 }
            r22 = r5 & 1
            if (r22 == 0) goto L_0x0b7a
            r22 = 1
        L_0x0b35:
            r5 = r5 & 2
            if (r5 == 0) goto L_0x0b7d
            r5 = 1
            r23 = r5
        L_0x0b3c:
            if (r13 <= 0) goto L_0x0b65
            r5 = 10
            if (r13 >= r5) goto L_0x0b65
            r0 = r43
            r1 = r14
            java.lang.Object r5 = r0.X(r1)     // Catch:{ Throwable -> 0x03b8 }
            if (r5 == 0) goto L_0x0b4f
            boolean r14 = r5 instanceof java.lang.Integer     // Catch:{ Throwable -> 0x03b8 }
            if (r14 != 0) goto L_0x0b81
        L_0x0b4f:
            r5 = 0
            r14 = r5
        L_0x0b51:
            switch(r13) {
                case 1: goto L_0x0b89;
                case 2: goto L_0x0b9f;
                case 3: goto L_0x0b94;
                case 4: goto L_0x0baa;
                case 5: goto L_0x0bb3;
                case 6: goto L_0x0b94;
                case 7: goto L_0x0b89;
                case 8: goto L_0x0b89;
                case 9: goto L_0x0b94;
                default: goto L_0x0b54;
            }     // Catch:{ Throwable -> 0x03b8 }
        L_0x0b54:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r0 = r5
            r1 = r24
            r2 = r25
            r3 = r18
            r4 = r19
            r0.x(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
        L_0x0b65:
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x0b73:
            r20 = 0
            goto L_0x0b25
        L_0x0b76:
            r5 = 0
            r21 = r5
            goto L_0x0b2c
        L_0x0b7a:
            r22 = 0
            goto L_0x0b35
        L_0x0b7d:
            r5 = 0
            r23 = r5
            goto L_0x0b3c
        L_0x0b81:
            java.lang.Integer r5 = (java.lang.Integer) r5     // Catch:{ Throwable -> 0x03b8 }
            int r5 = r5.intValue()     // Catch:{ Throwable -> 0x03b8 }
            r14 = r5
            goto L_0x0b51
        L_0x0b89:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r13 = r0
            r20 = 3
            r13.a(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23)     // Catch:{ Throwable -> 0x03b8 }
            goto L_0x0b54
        L_0x0b94:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r13 = r0
            r20 = 1
            r13.a(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23)     // Catch:{ Throwable -> 0x03b8 }
            goto L_0x0b54
        L_0x0b9f:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r13 = r0
            r20 = 2
            r13.a(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23)     // Catch:{ Throwable -> 0x03b8 }
            goto L_0x0b54
        L_0x0baa:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r13 = r0
            r13.b(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23)     // Catch:{ Throwable -> 0x03b8 }
            goto L_0x0b54
        L_0x0bb3:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r13 = r0
            r13.a(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23)     // Catch:{ Throwable -> 0x03b8 }
            goto L_0x0b54
        L_0x0bbc:
            r5 = 0
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            int r40 = r5 >>> 20
            r5 = 0
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            r13 = 1048575(0xfffff, float:1.469367E-39)
            r41 = r5 & r13
            r5 = 1
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            int r18 = r5 >>> 20
            r5 = 1
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            r13 = 1048575(0xfffff, float:1.469367E-39)
            r19 = r5 & r13
            r5 = 0
            byte r22 = r9[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 0
            short r5 = r11[r5]     // Catch:{ Throwable -> 0x03b8 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Throwable -> 0x03b8 }
            byte[] r16 = r5.getBytes()     // Catch:{ Throwable -> 0x03b8 }
            r5 = 0
            r15 = r7[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 1
            r5 = r7[r5]     // Catch:{ Throwable -> 0x03b8 }
            r13 = 1
            short r13 = r11[r13]     // Catch:{ Throwable -> 0x03b8 }
            r14 = 2
            short r20 = r11[r14]     // Catch:{ Throwable -> 0x03b8 }
            if (r5 == 0) goto L_0x1045
            java.lang.String r14 = com.uc.c.bc.aj(r5)     // Catch:{ Throwable -> 0x03b8 }
            if (r14 == 0) goto L_0x1045
            java.lang.String r17 = "\r\n"
            r0 = r17
            r1 = r14
            boolean r14 = r0.equals(r1)     // Catch:{ Throwable -> 0x03b8 }
            if (r14 == 0) goto L_0x1045
            r5 = 0
            r17 = r5
        L_0x0c06:
            r5 = 1
            byte r5 = r9[r5]     // Catch:{ Throwable -> 0x03b8 }
            r14 = r5 & 1
            if (r14 == 0) goto L_0x0c61
            r14 = 1
            r26 = r14
        L_0x0c10:
            r5 = r5 & 2
            if (r5 == 0) goto L_0x0c65
            r5 = 1
            r23 = r5
        L_0x0c17:
            r5 = 2
            byte r5 = r9[r5]     // Catch:{ Throwable -> 0x03b8 }
            r14 = r5 & 1
            if (r14 == 0) goto L_0x0c69
            r14 = 1
            r24 = r14
        L_0x0c21:
            r5 = r5 & 2
            if (r5 == 0) goto L_0x0c6d
            r5 = 1
            r25 = r5
        L_0x0c28:
            if (r22 <= 0) goto L_0x0c53
            r5 = 4
            r0 = r22
            r1 = r5
            if (r0 >= r1) goto L_0x0c53
            r0 = r43
            r1 = r13
            java.lang.Object r5 = r0.X(r1)     // Catch:{ Throwable -> 0x03b8 }
            if (r5 == 0) goto L_0x0c3d
            boolean r13 = r5 instanceof java.lang.Integer     // Catch:{ Throwable -> 0x03b8 }
            if (r13 != 0) goto L_0x0c71
        L_0x0c3d:
            r5 = 0
            r14 = r5
        L_0x0c3f:
            switch(r22) {
                case 1: goto L_0x0c79;
                case 2: goto L_0x0c79;
                case 3: goto L_0x0c91;
                default: goto L_0x0c42;
            }     // Catch:{ Throwable -> 0x03b8 }
        L_0x0c42:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r0 = r5
            r1 = r40
            r2 = r41
            r3 = r18
            r4 = r19
            r0.x(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
        L_0x0c53:
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x0c61:
            r14 = 0
            r26 = r14
            goto L_0x0c10
        L_0x0c65:
            r5 = 0
            r23 = r5
            goto L_0x0c17
        L_0x0c69:
            r14 = 0
            r24 = r14
            goto L_0x0c21
        L_0x0c6d:
            r5 = 0
            r25 = r5
            goto L_0x0c28
        L_0x0c71:
            java.lang.Integer r5 = (java.lang.Integer) r5     // Catch:{ Throwable -> 0x03b8 }
            int r5 = r5.intValue()     // Catch:{ Throwable -> 0x03b8 }
            r14 = r5
            goto L_0x0c3f
        L_0x0c79:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r13 = r0
            r21 = 1
            r5 = 2
            r0 = r22
            r1 = r5
            if (r0 != r1) goto L_0x0c8d
            r5 = 3
            r22 = r5
        L_0x0c89:
            r13.a(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26)     // Catch:{ Throwable -> 0x03b8 }
            goto L_0x0c42
        L_0x0c8d:
            r5 = 1
            r22 = r5
            goto L_0x0c89
        L_0x0c91:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r13 = r0
            r21 = 2
            r5 = 2
            r0 = r22
            r1 = r5
            if (r0 != r1) goto L_0x0ca5
            r5 = 3
            r22 = r5
        L_0x0ca1:
            r13.a(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26)     // Catch:{ Throwable -> 0x03b8 }
            goto L_0x0c42
        L_0x0ca5:
            r5 = 1
            r22 = r5
            goto L_0x0ca1
        L_0x0ca9:
            r5 = 0
            r14 = r7[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 1
            r16 = r7[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 0
            short r5 = r11[r5]     // Catch:{ Throwable -> 0x03b8 }
            r13 = 0
            short r13 = r11[r13]     // Catch:{ Throwable -> 0x03b8 }
            java.lang.String r13 = java.lang.String.valueOf(r13)     // Catch:{ Throwable -> 0x03b8 }
            byte[] r15 = r13.getBytes()     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            r1 = r5
            java.lang.Object r5 = r0.X(r1)     // Catch:{ Throwable -> 0x03b8 }
            if (r5 == 0) goto L_0x0cca
            boolean r13 = r5 instanceof java.lang.Integer     // Catch:{ Throwable -> 0x03b8 }
            if (r13 != 0) goto L_0x0ce7
        L_0x0cca:
            r5 = 0
            r19 = r5
        L_0x0ccd:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r13 = r0
            r17 = 0
            r18 = 0
            r13.a(r14, r15, r16, r17, r18, r19)     // Catch:{ Throwable -> 0x03b8 }
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x0ce7:
            java.lang.Integer r5 = (java.lang.Integer) r5     // Catch:{ Throwable -> 0x03b8 }
            int r5 = r5.intValue()     // Catch:{ Throwable -> 0x03b8 }
            r19 = r5
            goto L_0x0ccd
        L_0x0cf0:
            r5 = 0
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            int r25 = r5 >>> 20
            r5 = 0
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            r13 = 1048575(0xfffff, float:1.469367E-39)
            r26 = r5 & r13
            r5 = 1
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            int r21 = r5 >>> 20
            r5 = 1
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            r13 = 1048575(0xfffff, float:1.469367E-39)
            r22 = r5 & r13
            r5 = 0
            short r5 = r11[r5]     // Catch:{ Throwable -> 0x03b8 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Throwable -> 0x03b8 }
            byte[] r16 = r5.getBytes()     // Catch:{ Throwable -> 0x03b8 }
            r5 = 0
            r15 = r7[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 4
            r20 = r7[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 2
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            if (r5 > 0) goto L_0x0d9c
            r5 = -1
            r19 = r5
        L_0x0d23:
            r5 = 3
            r18 = r7[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 1
            r17 = r7[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 8
            r13 = 0
            byte r13 = r9[r13]     // Catch:{ Throwable -> 0x03b8 }
            r14 = 1
            if (r13 != r14) goto L_0x1041
            r5 = 0
            r24 = r5
        L_0x0d34:
            r5 = 1
            short r5 = r11[r5]     // Catch:{ Throwable -> 0x03b8 }
            r13 = 2
            r13 = r7[r13]     // Catch:{ Throwable -> 0x03b8 }
            if (r13 == 0) goto L_0x0da2
            java.lang.String r13 = "cameraModel"
            r14 = 2
            r14 = r7[r14]     // Catch:{ Throwable -> 0x03b8 }
            java.lang.String r14 = com.uc.c.bc.ak(r14)     // Catch:{ Throwable -> 0x03b8 }
            boolean r13 = r13.equals(r14)     // Catch:{ Throwable -> 0x03b8 }
            if (r13 == 0) goto L_0x0da2
            r13 = 1
        L_0x0d4c:
            if (r18 == 0) goto L_0x0da4
            r0 = r18
            int r0 = r0.length     // Catch:{ Throwable -> 0x03b8 }
            r14 = r0
            if (r14 <= 0) goto L_0x0da4
            if (r17 == 0) goto L_0x0da4
            r0 = r17
            int r0 = r0.length     // Catch:{ Throwable -> 0x03b8 }
            r14 = r0
            if (r14 <= 0) goto L_0x0da4
            r14 = 1
            r40 = r14
        L_0x0d5f:
            r0 = r43
            r1 = r5
            java.lang.Object r5 = r0.X(r1)     // Catch:{ Throwable -> 0x03b8 }
            if (r5 == 0) goto L_0x0d6c
            boolean r14 = r5 instanceof java.lang.Integer     // Catch:{ Throwable -> 0x03b8 }
            if (r14 != 0) goto L_0x0da8
        L_0x0d6c:
            r5 = 0
            r14 = r5
        L_0x0d6e:
            if (r40 == 0) goto L_0x0db0
            r5 = 4
            r23 = r5
        L_0x0d73:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r13 = r0
            if (r40 == 0) goto L_0x0dc4
        L_0x0d7a:
            r13.a(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r0 = r5
            r1 = r25
            r2 = r26
            r3 = r21
            r4 = r22
            r0.x(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x0d9c:
            r5 = 2
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            r19 = r5
            goto L_0x0d23
        L_0x0da2:
            r13 = 0
            goto L_0x0d4c
        L_0x0da4:
            r14 = 0
            r40 = r14
            goto L_0x0d5f
        L_0x0da8:
            java.lang.Integer r5 = (java.lang.Integer) r5     // Catch:{ Throwable -> 0x03b8 }
            int r5 = r5.intValue()     // Catch:{ Throwable -> 0x03b8 }
            r14 = r5
            goto L_0x0d6e
        L_0x0db0:
            boolean r5 = com.uc.c.w.ni()     // Catch:{ Throwable -> 0x03b8 }
            if (r5 == 0) goto L_0x0dc0
            if (r13 == 0) goto L_0x0dbc
            r5 = 1
            r23 = r5
            goto L_0x0d73
        L_0x0dbc:
            r5 = 2
            r23 = r5
            goto L_0x0d73
        L_0x0dc0:
            r5 = 0
            r23 = r5
            goto L_0x0d73
        L_0x0dc4:
            r5 = 0
            r17 = r5
            goto L_0x0d7a
        L_0x0dc8:
            r5 = 0
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            int r23 = r5 >>> 20
            r5 = 0
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            r13 = 1048575(0xfffff, float:1.469367E-39)
            r24 = r5 & r13
            r5 = 1
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            int r17 = r5 >>> 20
            r5 = 1
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            r13 = 1048575(0xfffff, float:1.469367E-39)
            r18 = r5 & r13
            r5 = 0
            short r5 = r11[r5]     // Catch:{ Throwable -> 0x03b8 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Throwable -> 0x03b8 }
            byte[] r16 = r5.getBytes()     // Catch:{ Throwable -> 0x03b8 }
            r5 = 0
            byte r5 = r9[r5]     // Catch:{ Throwable -> 0x03b8 }
            r13 = 1
            if (r5 != r13) goto L_0x0e53
            r5 = 1
            r22 = r5
        L_0x0df6:
            r5 = 1
            byte r5 = r9[r5]     // Catch:{ Throwable -> 0x03b8 }
            r13 = r5 & 1
            if (r13 == 0) goto L_0x0e57
            r13 = 1
            r20 = r13
        L_0x0e00:
            r5 = r5 & 2
            if (r5 == 0) goto L_0x0e5b
            r5 = 1
            r21 = r5
        L_0x0e07:
            r5 = 0
            r15 = r7[r5]     // Catch:{ Throwable -> 0x03b8 }
            r5 = 2
            short r5 = r11[r5]     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            r1 = r5
            java.lang.Object r5 = r0.X(r1)     // Catch:{ Throwable -> 0x03b8 }
            if (r5 == 0) goto L_0x0e1a
            boolean r13 = r5 instanceof java.lang.Integer     // Catch:{ Throwable -> 0x03b8 }
            if (r13 != 0) goto L_0x0e5f
        L_0x0e1a:
            r5 = 0
            r14 = r5
        L_0x0e1c:
            if (r36 == 0) goto L_0x0e29
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r0 = r5
            r1 = r37
            r0.iQ(r1)     // Catch:{ Throwable -> 0x03b8 }
        L_0x0e29:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r13 = r0
            r19 = 0
            r13.a(r14, r15, r16, r17, r18, r19, r20, r21, r22)     // Catch:{ Throwable -> 0x03b8 }
            r5 = 1
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r13 = r0
            r0 = r13
            r1 = r23
            r2 = r24
            r3 = r17
            r4 = r18
            r0.x(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
            r15 = r34
            r16 = r5
            r17 = r35
            r18 = r14
            r19 = r6
            r13 = r38
            goto L_0x0061
        L_0x0e53:
            r5 = 0
            r22 = r5
            goto L_0x0df6
        L_0x0e57:
            r13 = 0
            r20 = r13
            goto L_0x0e00
        L_0x0e5b:
            r5 = 0
            r21 = r5
            goto L_0x0e07
        L_0x0e5f:
            java.lang.Integer r5 = (java.lang.Integer) r5     // Catch:{ Throwable -> 0x03b8 }
            int r5 = r5.intValue()     // Catch:{ Throwable -> 0x03b8 }
            r14 = r5
            goto L_0x0e1c
        L_0x0e67:
            r5 = 0
            byte r5 = r9[r5]     // Catch:{ Throwable -> 0x03b8 }
            r13 = 0
            r15 = r7[r13]     // Catch:{ Throwable -> 0x03b8 }
            r13 = 1
            r16 = r7[r13]     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r13 = r0
            r17 = 0
            r18 = 0
            r19 = 0
            r20 = 0
            r14 = 1
            if (r5 != r14) goto L_0x0e96
            r5 = 1
            r21 = r5
        L_0x0e83:
            r14 = r37
            r13.a(r14, r15, r16, r17, r18, r19, r20, r21)     // Catch:{ Throwable -> 0x03b8 }
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x0e96:
            r5 = 0
            r21 = r5
            goto L_0x0e83
        L_0x0e9a:
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x0ea8:
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x0eb6:
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x0ec4:
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x0ed2:
            r5 = 0
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            int r5 = r5 >>> 20
            r13 = 0
            r13 = r8[r13]     // Catch:{ Throwable -> 0x03b8 }
            r14 = 1048575(0xfffff, float:1.469367E-39)
            r13 = r13 & r14
            r14 = 1
            r14 = r8[r14]     // Catch:{ Throwable -> 0x03b8 }
            int r14 = r14 >>> 20
            r15 = 1
            r15 = r8[r15]     // Catch:{ Throwable -> 0x03b8 }
            r16 = 1048575(0xfffff, float:1.469367E-39)
            r15 = r15 & r16
            r16 = 0
            byte r16 = r9[r16]     // Catch:{ Throwable -> 0x03b8 }
            r17 = 1
            byte r17 = r9[r17]     // Catch:{ Throwable -> 0x03b8 }
            r18 = 0
            short r18 = r11[r18]     // Catch:{ Throwable -> 0x03b8 }
            r19 = 0
            r19 = r7[r19]     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r20 = r0
            r0 = r20
            r1 = r18
            r2 = r16
            r3 = r17
            r4 = r19
            r0.a(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r16 = r0
            r0 = r16
            r1 = r5
            r2 = r13
            r3 = r14
            r4 = r15
            r0.x(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x0f2b:
            r5 = 0
            r5 = r7[r5]     // Catch:{ Throwable -> 0x03b8 }
            r13 = 1
            r13 = r7[r13]     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            r1 = r5
            r2 = r13
            r0.d(r1, r2)     // Catch:{ Throwable -> 0x03b8 }
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x0f46:
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x0f54:
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x0f62:
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x0f70:
            if (r31 == 0) goto L_0x0f7a
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r5.Lq()     // Catch:{ Throwable -> 0x03b8 }
        L_0x0f7a:
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r5.KW()     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r6 = r0
            int r6 = r6.ahU     // Catch:{ Throwable -> 0x03b8 }
            r5.iU(r6)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            boolean r5 = r5.ac()     // Catch:{ Throwable -> 0x03b8 }
            if (r5 != 0) goto L_0x00c1
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r0 = r5
            r1 = r33
            r2 = r32
            r0.bx(r1, r2)     // Catch:{ Throwable -> 0x03b8 }
            goto L_0x00c1
        L_0x0fab:
            r5 = 0
            r5 = r8[r5]     // Catch:{ Throwable -> 0x03b8 }
            int r5 = r5 >>> 20
            r13 = 0
            r13 = r8[r13]     // Catch:{ Throwable -> 0x03b8 }
            r14 = 1048575(0xfffff, float:1.469367E-39)
            r13 = r13 & r14
            r14 = 1
            r14 = r8[r14]     // Catch:{ Throwable -> 0x03b8 }
            int r14 = r14 >>> 20
            r15 = 1
            r15 = r8[r15]     // Catch:{ Throwable -> 0x03b8 }
            r16 = 1048575(0xfffff, float:1.469367E-39)
            r15 = r15 & r16
            r16 = 0
            short r16 = r11[r16]     // Catch:{ Throwable -> 0x03b8 }
            r17 = 1
            byte r17 = r9[r17]     // Catch:{ Throwable -> 0x03b8 }
            if (r31 == 0) goto L_0x0fd7
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r18 = r0
            r18.Lq()     // Catch:{ Throwable -> 0x03b8 }
        L_0x0fd7:
            r18 = 1
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r19 = r0
            r0 = r19
            r1 = r16
            r2 = r17
            r0.bt(r1, r2)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r16 = r0
            r0 = r16
            r1 = r5
            r2 = r13
            r3 = r14
            r4 = r15
            r0.w(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r31 = r18
            r19 = r6
            r18 = r37
            goto L_0x0061
        L_0x1007:
            r5 = 0
            r5 = r7[r5]     // Catch:{ Throwable -> 0x03b8 }
            r13 = 0
            r13 = r8[r13]     // Catch:{ Throwable -> 0x03b8 }
            int r13 = r13 >>> 20
            r14 = 0
            r14 = r8[r14]     // Catch:{ Throwable -> 0x03b8 }
            r15 = 1048575(0xfffff, float:1.469367E-39)
            r14 = r14 & r15
            java.lang.String r5 = com.uc.c.bc.ak(r5)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r15 = r0
            r15.fq(r5)     // Catch:{ Throwable -> 0x03b8 }
            r0 = r43
            com.uc.c.ca r0 = r0.dt     // Catch:{ Throwable -> 0x03b8 }
            r5 = r0
            r15 = 0
            r16 = 0
            r0 = r5
            r1 = r13
            r2 = r14
            r3 = r15
            r4 = r16
            r0.x(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x03b8 }
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x1041:
            r24 = r5
            goto L_0x0d34
        L_0x1045:
            r17 = r5
            goto L_0x0c06
        L_0x1049:
            r21 = r34
            r22 = r35
            goto L_0x0809
        L_0x104f:
            r5 = r29
            goto L_0x06fe
        L_0x1053:
            r5 = r29
            r21 = r34
            r22 = r35
            goto L_0x054d
        L_0x105b:
            r5 = r29
            goto L_0x0547
        L_0x105f:
            r13 = r38
            r15 = r34
            r16 = r36
            r17 = r35
            r18 = r37
            r19 = r6
            goto L_0x0061
        L_0x106d:
            r38 = r13
            goto L_0x01a1
        L_0x1071:
            r36 = r16
            r37 = r18
            goto L_0x00af
        L_0x1077:
            r34 = r15
            r35 = r17
            goto L_0x008e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.k.m3do():void");
    }

    public int q(boolean z) {
        int i;
        int i2;
        int i3;
        int i4;
        System.currentTimeMillis();
        try {
            ar.mR.bW(t.bic);
            int i5 = -1;
            int i6 = 1;
            while (true) {
                try {
                    if (this.mT == null || this.mT.GZ()) {
                        try {
                            i5 = this.mL.readInt();
                            if (i5 == -1) {
                                if (this.mM != null) {
                                    this.mL = this.mM;
                                    this.mM = null;
                                    i5 = 0;
                                }
                                if (this.mT != null && this.mT.GY()) {
                                    com.uc.zip.a kh = com.uc.zip.a.kh();
                                    if (kh.ko() && this.mT == kh.GM) {
                                        kh.m(this.mL);
                                        kh.kn();
                                        kh.GM = null;
                                        i5 = 0;
                                    }
                                }
                                if (i5 != 0) {
                                    i3 = i6;
                                }
                            } else if (i5 == 20012 || i5 == 20011 || i5 == 20008) {
                                if (i5 == 20011) {
                                    this.dt.iH(2);
                                    i5 = 20012;
                                }
                                boolean z2 = i5 == 20008;
                                this.mO = this.mL.readInt();
                                String readUTF = this.mL.readUTF();
                                if (readUTF.indexOf("media:wml") != -1) {
                                    i5 = 29913;
                                } else if (readUTF.indexOf("media:xhtml") != -1) {
                                    i5 = 29914;
                                }
                                switch (i5) {
                                    case 20011:
                                    case 20012:
                                        i4 = 1;
                                        break;
                                    case 29913:
                                        i4 = 2;
                                        break;
                                    case 29914:
                                        i4 = 3;
                                        break;
                                    default:
                                        i4 = 0;
                                        break;
                                }
                                this.dt.jh(0);
                                this.dt.bm(1, i4);
                                this.dt.KG();
                                r(z2);
                            } else if (i5 == 2 || i5 == 10010 || i5 == 10011 || i5 == 10012) {
                                dl();
                            } else if (i5 == 40001) {
                                dk();
                            } else if (i5 == 40004) {
                                try {
                                    this.mL.readInt();
                                    this.mL.readUTF();
                                    be beVar = new be(this.mN);
                                    this.mM = this.mL;
                                    this.mL = null;
                                    this.mL = new DataInputStream(beVar);
                                } catch (Exception e) {
                                }
                            } else if (i5 == 30004) {
                                int readInt = this.mL.readInt();
                                this.mL.readUTF();
                                byte[] bArr = new byte[readInt];
                                this.mN.read(bArr, 0, readInt);
                                this.mS.p.aw(bArr);
                            } else if (i5 == 40005) {
                                i6 = dh();
                                if (i6 == 0) {
                                    i3 = i6;
                                }
                            } else if (i5 == 40006) {
                                di();
                            } else if (i5 == 50001) {
                                a(this.mL);
                            } else {
                                i3 = i6;
                            }
                        } catch (EOFException e2) {
                            if (this.mM != null) {
                                this.mL = this.mM;
                                this.mM = null;
                                i5 = 0;
                            }
                            if (i5 != 0) {
                                i3 = i6;
                            }
                        }
                    } else {
                        i3 = i6;
                    }
                } catch (OutOfMemoryError e3) {
                    i = i6;
                } catch (Throwable th) {
                    i2 = i6;
                    dc();
                    return i3;
                }
            }
            dc();
        } catch (OutOfMemoryError e4) {
            i = 1;
            try {
                this.mR.mW();
                return i3;
            } finally {
                dc();
            }
        } catch (Throwable th2) {
            i2 = 1;
            dc();
            return i3;
        }
        return i3;
    }

    public final void r(boolean z) {
        try {
            this.mL.readInt();
            ar.mR.bW((this.mO << 2) + t.bib);
            if (z) {
                this.mL = null;
                this.mL = new DataInputStream(new GZIPInputStream(this.mN));
            }
            m3do();
        } catch (Throwable th) {
        }
    }

    public final void stop() {
        if (this.mW != null) {
            this.mW.stop();
        }
        if (this.mX != null) {
            this.mX.stop();
        }
    }
}
