package com.uc.c;

public class bv {
    public static synchronized boolean i(ca caVar, int i) {
        boolean Q;
        synchronized (bv.class) {
            if (caVar == null) {
                Q = false;
            } else if (i == 0) {
                bc.P(caVar);
                Q = true;
            } else {
                Q = bc.Q(caVar);
            }
        }
        return Q;
    }
}
