package com.uc.c;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.uc.a.n;
import com.uc.browser.ActivityBookmarkEx;
import com.uc.c.a.b;
import com.uc.c.a.d;
import com.uc.c.a.f;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Vector;

public class bb extends bi {
    private static final String Xv = "-----Ajzlizbchdz";
    private static d bdD = null;
    public static int bdJ = 1000;
    public static String bdK = null;
    public static String bdL = null;
    public static String bdM = null;
    public static byte bdN = 0;
    private static short bdP = 0;
    private static final int bdQ = 2;
    private static final int bdR = 50;
    public static byte bdS = 1;
    public static String bdV = au.aGF;
    public static String bdW = au.aGF;
    private static final int bdY = 0;
    private static final int bdZ = 1;
    private static final int bea = 2;
    public static int beb = 0;
    public static int bec = 0;
    public static int bed = 0;
    public static int bee = 0;
    public static int bef = 0;
    private static Vector beg;
    private static int beh;
    private static int bei;
    private static Drawable bej;
    public static Drawable bek;
    public static Drawable bel;
    public static int bem;
    public static int ben = -1;
    public static int beo;
    public static int bep;
    private static final int[] beq = {-1, 0, 1, 2, 3, 4, 5, 6, 7};
    private Vector bdE = new Vector();
    private ca bdF = null;
    private bx bdG = null;
    private byte bdH = 0;
    private byte[] bdI = null;
    private bn[] bdO = null;
    private byte bdT = 0;
    /* access modifiers changed from: private */
    public Object bdU = null;
    public int bdX = 1;
    b ber = new e(this);

    public bb(w wVar, ca caVar, int i) {
        super(wVar, null);
        bdN = 0;
        BP();
        this.bdF = caVar;
        ge(i);
    }

    bb(w wVar, r rVar) {
        super(wVar, rVar);
        bdD = d.zm();
        bdD.a(this.ber);
        Cc();
    }

    public static void BP() {
        if (bx.bAe != null && bx.bAe.length() != 0) {
            String[] split = bc.split(bx.bAe, "|");
            bdK = split[1];
            String[] split2 = bc.split(split[2], cd.bVH);
            String str = split2[0];
            String str2 = split2[1];
            bdL = split2[2];
            bdM = str + cd.bVI + str2;
        }
    }

    public static String BQ() {
        return Long.toString(System.currentTimeMillis());
    }

    private static byte[] BR() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write(13);
        byteArrayOutputStream.write(10);
        try {
            byteArrayOutputStream.write(Xv.getBytes());
        } catch (IOException e) {
        }
        byteArrayOutputStream.write(13);
        byteArrayOutputStream.write(10);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        try {
            byteArrayOutputStream.close();
        } catch (IOException e2) {
        }
        return byteArray;
    }

    private f BV() {
        ca jm = this.mS.jm();
        byte[] Aq = Aq();
        if (Aq == null || Aq.length <= 0) {
            return null;
        }
        if (Aq[0] != 55) {
            return null;
        }
        Object elementAt = ((Vector) jm.bGl.get(Integer.valueOf(jm.bS()))).elementAt(f.b(Aq, r.Eu, 0));
        if (elementAt == null) {
            return null;
        }
        return d((short) ((int[]) elementAt)[0]);
    }

    public static int BW() {
        return bdD.zv().size() + bdD.zw().size() + bdD.zz().size() + bdD.zx().size();
    }

    public static int BX() {
        return bdD.zx().size();
    }

    public static boolean BY() {
        return !gf(bdS);
    }

    private static Drawable Cd() {
        if (bej != null) {
            return bej;
        }
        return null;
    }

    public static void Ce() {
        bek = null;
        bel = null;
    }

    public static final z a(String str, String str2, z zVar) {
        if (zVar == null || bc.by(str) || bc.by(str2)) {
            return zVar;
        }
        if ("type".equals(str)) {
            zVar.TR = str2;
        } else if ("objecturi".equals(str)) {
            zVar.TT = str2;
        } else if ("size".equals(str)) {
            zVar.TS = bc.parseInt(str2);
        } else if ("installnotifyuri".equals(str)) {
            zVar.TU = str2;
        } else if ("name".endsWith(str)) {
            zVar.TX = str2;
        } else if ("description".endsWith(str)) {
            zVar.TY = str2;
        } else if ("vendor".endsWith(str)) {
            zVar.TZ = str2;
        } else if ("DDVersion".endsWith(str)) {
            zVar.TW = str2;
        } else if ("infoURL".endsWith(str)) {
            zVar.Ua = str2;
        }
        return zVar;
    }

    private void a(ca caVar, f fVar) {
        if (fVar != null) {
            for (short s = 0; s < fVar.cF; s++) {
                if (fVar.cE != null && (fVar.cE[s] instanceof byte[])) {
                    byte[] bArr = (byte[]) fVar.cE[s];
                    if (bArr[0] == 55) {
                        int i = ((int[]) ((Vector) caVar.bGl.get(Integer.valueOf(caVar.bS()))).elementAt(f.b(bArr, r.Eu, 0)))[0];
                        f.b(bArr);
                        int c = f.c(bArr);
                        int d = f.d(bArr);
                        int e = f.e(bArr);
                        b.a.a.a.f gi = gi(8);
                        int width = gi != null ? gi.getWidth() : 0;
                        ai aiVar = new ai();
                        aiVar.x = (d - width) - beb;
                        aiVar.y = c;
                        aiVar.abD = width + 15;
                        aiVar.abE = e;
                        aiVar.abG = i;
                        caVar.bOz.add(aiVar);
                    }
                } else if (fVar.cE != null) {
                    a(caVar, (f) fVar.cE[s]);
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0071 A[SYNTHETIC, Splitter:B:11:0x0071] */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x031d A[SYNTHETIC, Splitter:B:130:0x031d] */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x0322 A[SYNTHETIC, Splitter:B:133:0x0322] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:168:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x006c A[SYNTHETIC, Splitter:B:8:0x006c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.lang.String r29, java.lang.String r30, int r31, com.uc.c.ca r32) {
        /*
            r28 = this;
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            java.lang.StringBuffer r12 = new java.lang.StringBuffer
            java.lang.String[] r13 = com.uc.c.ar.avS
            r14 = 24
            r13 = r13[r14]
            r12.<init>(r13)
            r13 = 0
            java.lang.String r15 = "photo://UC_Photo_"
            r0 = r30
            r1 = r15
            boolean r15 = r0.startsWith(r1)
            r16 = 1
            int r17 = b.b.a.OR()
            r0 = r16
            r1 = r17
            if (r0 != r1) goto L_0x0080
            r16 = 40960(0xa000, float:5.7397E-41)
            com.uc.c.bb.bdJ = r16
            r16 = r4
            r4 = r30
            r24 = r11
            r11 = r7
            r7 = r24
            r25 = r8
            r8 = r10
            r10 = r25
            r26 = r6
            r27 = r5
            r5 = r13
            r13 = r26
            r14 = r27
        L_0x0046:
            r17 = 1
            r0 = r16
            r1 = r17
            if (r0 != r1) goto L_0x009c
            r12 = r11
            r11 = r10
            r10 = r9
        L_0x0051:
            r0 = r28
            com.uc.c.bx r0 = r0.bdG
            r4 = r0
            com.uc.c.r r4 = r4.mS
            com.uc.a.n r13 = r4.ch()
            com.uc.c.bs r4 = new com.uc.c.bs
            r6 = 1
            r8 = 1
            r5 = r30
            r4.<init>(r5, r6, r8)
            r13.a(r4)
            if (r11 == 0) goto L_0x006f
            r11.close()     // Catch:{ IOException -> 0x0331 }
        L_0x006f:
            if (r10 == 0) goto L_0x0074
            r10.close()     // Catch:{ IOException -> 0x0334 }
        L_0x0074:
            if (r12 == 0) goto L_0x007f
            boolean r4 = r12.fS()
            if (r4 != 0) goto L_0x007f
            r12.close()     // Catch:{ IOException -> 0x0337 }
        L_0x007f:
            return
        L_0x0080:
            r16 = 20480(0x5000, float:2.8699E-41)
            com.uc.c.bb.bdJ = r16
            r16 = r4
            r4 = r30
            r24 = r11
            r11 = r7
            r7 = r24
            r25 = r8
            r8 = r10
            r10 = r25
            r26 = r6
            r27 = r5
            r5 = r13
            r13 = r26
            r14 = r27
            goto L_0x0046
        L_0x009c:
            r0 = r28
            com.uc.c.bx r0 = r0.bdG     // Catch:{ all -> 0x0340 }
            r17 = r0
            boolean r17 = r17.GZ()     // Catch:{ all -> 0x0340 }
            if (r17 != 0) goto L_0x00ac
            r12 = r11
            r11 = r10
            r10 = r9
            goto L_0x0051
        L_0x00ac:
            int r17 = com.uc.c.bb.bdJ     // Catch:{ all -> 0x0340 }
            r0 = r28
            byte[] r0 = r0.bdI     // Catch:{ all -> 0x0340 }
            r18 = r0
            r0 = r18
            int r0 = r0.length     // Catch:{ all -> 0x0340 }
            r18 = r0
            int r17 = r17 - r18
            r18 = 0
            if (r15 == 0) goto L_0x0142
            if (r10 != 0) goto L_0x0356
            r4 = 8
            r0 = r30
            r1 = r4
            java.lang.String r19 = r0.substring(r1)     // Catch:{ Exception -> 0x034c }
            if (r32 == 0) goto L_0x036b
            r0 = r32
            java.util.Vector r0 = r0.bGH     // Catch:{ Exception -> 0x034c }
            r4 = r0
            if (r4 == 0) goto L_0x036b
            java.lang.String r4 = "photo://UC_Photo_"
            int r4 = r4.length()     // Catch:{ Exception -> 0x034c }
            java.lang.String r8 = ".png"
            r0 = r30
            r1 = r8
            int r8 = r0.indexOf(r1)     // Catch:{ Exception -> 0x034c }
            r0 = r30
            r1 = r4
            r2 = r8
            java.lang.String r4 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x034c }
            int r4 = java.lang.Integer.parseInt(r4)     // Catch:{ Exception -> 0x034c }
            r0 = r32
            java.util.Vector r0 = r0.bGH     // Catch:{ Exception -> 0x034c }
            r8 = r0
            java.lang.Object r4 = r8.elementAt(r4)     // Catch:{ Exception -> 0x034c }
            byte[] r4 = (byte[]) r4     // Catch:{ Exception -> 0x034c }
            byte[] r4 = (byte[]) r4     // Catch:{ Exception -> 0x034c }
        L_0x00fb:
            if (r4 == 0) goto L_0x0360
            java.io.ByteArrayInputStream r8 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x034c }
            r8.<init>(r4)     // Catch:{ Exception -> 0x034c }
            java.io.DataInputStream r9 = new java.io.DataInputStream     // Catch:{ Exception -> 0x0351, all -> 0x0346 }
            r9.<init>(r8)     // Catch:{ Exception -> 0x0351, all -> 0x0346 }
            r10 = r19
            r20 = r9
            r21 = r11
            r11 = r4
            r19 = r8
        L_0x0110:
            if (r7 != 0) goto L_0x0372
            r0 = r28
            r1 = r10
            byte[] r4 = r0.dE(r1)     // Catch:{ Exception -> 0x02ee }
            int r7 = r4.length     // Catch:{ Exception -> 0x02ee }
            int r7 = r17 - r7
            r0 = r28
            byte[] r0 = r0.bdI     // Catch:{ Exception -> 0x02ee }
            r8 = r0
            int r8 = r8.length     // Catch:{ Exception -> 0x02ee }
            int r7 = r7 - r8
            r0 = r28
            byte[] r0 = r0.bdI     // Catch:{ Exception -> 0x02ee }
            r8 = r0
            byte[] r4 = com.uc.c.bc.h(r8, r4)     // Catch:{ Exception -> 0x02ee }
            int r8 = r4.length     // Catch:{ Exception -> 0x02ee }
            r17 = r8
        L_0x012f:
            r0 = r28
            com.uc.c.bx r0 = r0.bdG     // Catch:{ Exception -> 0x02ee }
            r8 = r0
            boolean r8 = r8.GZ()     // Catch:{ Exception -> 0x02ee }
            if (r8 != 0) goto L_0x0157
            r10 = r19
            r11 = r20
            r12 = r21
            goto L_0x0051
        L_0x0142:
            if (r11 != 0) goto L_0x0148
            com.uc.c.q r11 = com.uc.c.q.R(r4)     // Catch:{ Exception -> 0x034c }
        L_0x0148:
            if (r10 != 0) goto L_0x0356
            java.io.InputStream r10 = r11.ez()     // Catch:{ Exception -> 0x034c }
            r19 = r9
            r20 = r10
            r21 = r11
            r11 = r8
            r10 = r4
            goto L_0x0110
        L_0x0157:
            r0 = r28
            r1 = r4
            r2 = r20
            r3 = r7
            byte[] r4 = r0.a(r1, r2, r3)     // Catch:{ Exception -> 0x02ee }
            if (r4 == 0) goto L_0x017b
            int r7 = r4.length     // Catch:{ Exception -> 0x02ee }
            int r7 = r7 + r14
            if (r15 == 0) goto L_0x016c
            if (r11 == 0) goto L_0x016c
            int r8 = r11.length     // Catch:{ Exception -> 0x02ee }
            if (r7 < r8) goto L_0x0177
        L_0x016c:
            if (r21 == 0) goto L_0x019f
            long r8 = (long) r7     // Catch:{ Exception -> 0x02ee }
            long r22 = r21.fT()     // Catch:{ Exception -> 0x02ee }
            int r8 = (r8 > r22 ? 1 : (r8 == r22 ? 0 : -1))
            if (r8 >= 0) goto L_0x019f
        L_0x0177:
            r8 = 0
            r14 = r7
            r16 = r8
        L_0x017b:
            int r18 = r13 + 1
            r7 = 1
            r0 = r16
            r1 = r7
            if (r0 != r1) goto L_0x01ad
            r7 = 1
        L_0x0184:
            r0 = r29
            r1 = r13
            r2 = r7
            java.lang.String r7 = d(r0, r1, r2)     // Catch:{ Exception -> 0x02ee }
            r0 = r28
            com.uc.c.bx r0 = r0.bdG     // Catch:{ Exception -> 0x02ee }
            r8 = r0
            boolean r8 = r8.GZ()     // Catch:{ Exception -> 0x02ee }
            if (r8 != 0) goto L_0x01af
            r10 = r19
            r11 = r20
            r12 = r21
            goto L_0x0051
        L_0x019f:
            r8 = 1
            r0 = r28
            byte[] r0 = r0.bdI     // Catch:{ Exception -> 0x02ee }
            r9 = r0
            byte[] r4 = com.uc.c.bc.h(r4, r9)     // Catch:{ Exception -> 0x02ee }
            r14 = r7
            r16 = r8
            goto L_0x017b
        L_0x01ad:
            r7 = 0
            goto L_0x0184
        L_0x01af:
            r0 = r28
            r1 = r7
            r2 = r4
            boolean r8 = r0.c(r1, r2)     // Catch:{ Exception -> 0x02ee }
            if (r8 != 0) goto L_0x01f6
            r0 = r28
            com.uc.c.bx r0 = r0.bdG     // Catch:{ Exception -> 0x02ee }
            r8 = r0
            boolean r8 = r8.GZ()     // Catch:{ Exception -> 0x02ee }
            if (r8 == 0) goto L_0x01f6
            r0 = r28
            r1 = r7
            r2 = r4
            boolean r8 = r0.c(r1, r2)     // Catch:{ Exception -> 0x02ee }
            if (r8 != 0) goto L_0x01f6
            r0 = r28
            com.uc.c.bx r0 = r0.bdG     // Catch:{ Exception -> 0x02ee }
            r8 = r0
            boolean r8 = r8.GZ()     // Catch:{ Exception -> 0x02ee }
            if (r8 == 0) goto L_0x01f6
            r0 = r28
            r1 = r7
            r2 = r4
            boolean r4 = r0.c(r1, r2)     // Catch:{ Exception -> 0x02ee }
            if (r4 != 0) goto L_0x01f6
            r0 = r28
            com.uc.c.bx r0 = r0.bdG     // Catch:{ Exception -> 0x02ee }
            r4 = r0
            boolean r4 = r4.GZ()     // Catch:{ Exception -> 0x02ee }
            if (r4 == 0) goto L_0x01f6
            r10 = r19
            r11 = r20
            r12 = r21
            goto L_0x0051
        L_0x01f6:
            r0 = r28
            com.uc.c.bx r0 = r0.bdG     // Catch:{ Exception -> 0x02ee }
            r4 = r0
            boolean r4 = r4.GZ()     // Catch:{ Exception -> 0x02ee }
            if (r4 != 0) goto L_0x0209
            r10 = r19
            r11 = r20
            r12 = r21
            goto L_0x0051
        L_0x0209:
            r0 = r28
            java.util.Vector r0 = r0.bdE     // Catch:{ Exception -> 0x02ee }
            r4 = r0
            int r4 = r4.size()     // Catch:{ Exception -> 0x02ee }
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x02ee }
            long r7 = r7 - r5
            r22 = 1000(0x3e8, double:4.94E-321)
            int r7 = (r7 > r22 ? 1 : (r7 == r22 ? 0 : -1))
            if (r7 <= 0) goto L_0x036e
            java.lang.String[] r5 = com.uc.c.ar.avS     // Catch:{ Exception -> 0x02ee }
            r6 = 24
            r5 = r5[r6]     // Catch:{ Exception -> 0x02ee }
            int r5 = r5.length()     // Catch:{ Exception -> 0x02ee }
            r12.setLength(r5)     // Catch:{ Exception -> 0x02ee }
            r5 = 1
            if (r4 <= r5) goto L_0x0248
            r5 = 91
            java.lang.StringBuffer r5 = r12.append(r5)     // Catch:{ Exception -> 0x02ee }
            int r6 = r31 + 1
            java.lang.StringBuffer r5 = r5.append(r6)     // Catch:{ Exception -> 0x02ee }
            r6 = 47
            java.lang.StringBuffer r5 = r5.append(r6)     // Catch:{ Exception -> 0x02ee }
            java.lang.StringBuffer r4 = r5.append(r4)     // Catch:{ Exception -> 0x02ee }
            java.lang.String r5 = "] "
            r4.append(r5)     // Catch:{ Exception -> 0x02ee }
        L_0x0248:
            long r4 = (long) r14     // Catch:{ Exception -> 0x02ee }
            java.lang.String r4 = com.uc.c.bc.j(r4)     // Catch:{ Exception -> 0x02ee }
            java.lang.StringBuffer r4 = r12.append(r4)     // Catch:{ Exception -> 0x02ee }
            r5 = 47
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x02ee }
            if (r15 == 0) goto L_0x02cb
            int r5 = r11.length     // Catch:{ Exception -> 0x02ee }
            long r5 = (long) r5     // Catch:{ Exception -> 0x02ee }
        L_0x025b:
            java.lang.String r5 = com.uc.c.bc.j(r5)     // Catch:{ Exception -> 0x02ee }
            r4.append(r5)     // Catch:{ Exception -> 0x02ee }
            java.lang.String r4 = r12.toString()     // Catch:{ Exception -> 0x02ee }
            r0 = r28
            com.uc.c.bx r0 = r0.bdG     // Catch:{ Exception -> 0x02ee }
            r5 = r0
            r5.eU(r4)     // Catch:{ Exception -> 0x02ee }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x02ee }
            r22 = r4
        L_0x0274:
            r0 = r28
            com.uc.c.bx r0 = r0.bdG     // Catch:{ Exception -> 0x02ec }
            r4 = r0
            if (r4 == 0) goto L_0x02b8
            r0 = r28
            com.uc.c.bx r0 = r0.bdG     // Catch:{ Exception -> 0x02ec }
            r4 = r0
            com.uc.c.r r4 = r4.mS     // Catch:{ Exception -> 0x02ec }
            if (r4 == 0) goto L_0x02b8
            r0 = r28
            com.uc.c.bx r0 = r0.bdG     // Catch:{ Exception -> 0x02ec }
            r4 = r0
            com.uc.c.r r4 = r4.mS     // Catch:{ Exception -> 0x02ec }
            com.uc.a.n r4 = r4.ch()     // Catch:{ Exception -> 0x02ec }
            if (r4 == 0) goto L_0x02b8
            if (r15 != 0) goto L_0x02d0
            if (r21 == 0) goto L_0x02b8
            long r4 = (long) r14     // Catch:{ Exception -> 0x02ec }
            long r6 = r21.fT()     // Catch:{ Exception -> 0x02ec }
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 >= 0) goto L_0x02b8
            r0 = r28
            com.uc.c.bx r0 = r0.bdG     // Catch:{ Exception -> 0x02ec }
            r4 = r0
            com.uc.c.r r4 = r4.mS     // Catch:{ Exception -> 0x02ec }
            com.uc.a.n r13 = r4.ch()     // Catch:{ Exception -> 0x02ec }
            com.uc.c.bs r4 = new com.uc.c.bs     // Catch:{ Exception -> 0x02ec }
            long r6 = (long) r14     // Catch:{ Exception -> 0x02ec }
            long r8 = r21.fT()     // Catch:{ Exception -> 0x02ec }
            r5 = r30
            r4.<init>(r5, r6, r8)     // Catch:{ Exception -> 0x02ec }
            r13.a(r4)     // Catch:{ Exception -> 0x02ec }
        L_0x02b8:
            com.uc.c.bc.gc()     // Catch:{ all -> 0x02fa }
            r5 = r22
            r7 = r17
            r4 = r10
            r8 = r11
            r9 = r19
            r13 = r18
            r11 = r21
            r10 = r20
            goto L_0x0046
        L_0x02cb:
            long r5 = r21.fT()     // Catch:{ Exception -> 0x02ee }
            goto L_0x025b
        L_0x02d0:
            int r4 = r11.length     // Catch:{ Exception -> 0x02ec }
            if (r14 >= r4) goto L_0x02b8
            r0 = r28
            com.uc.c.bx r0 = r0.bdG     // Catch:{ Exception -> 0x02ec }
            r4 = r0
            com.uc.c.r r4 = r4.mS     // Catch:{ Exception -> 0x02ec }
            com.uc.a.n r13 = r4.ch()     // Catch:{ Exception -> 0x02ec }
            com.uc.c.bs r4 = new com.uc.c.bs     // Catch:{ Exception -> 0x02ec }
            long r6 = (long) r14     // Catch:{ Exception -> 0x02ec }
            int r5 = r11.length     // Catch:{ Exception -> 0x02ec }
            long r8 = (long) r5     // Catch:{ Exception -> 0x02ec }
            r5 = r30
            r4.<init>(r5, r6, r8)     // Catch:{ Exception -> 0x02ec }
            r13.a(r4)     // Catch:{ Exception -> 0x02ec }
            goto L_0x02b8
        L_0x02ec:
            r4 = move-exception
            goto L_0x02b8
        L_0x02ee:
            r4 = move-exception
            r4 = r19
            r5 = r20
            r6 = r21
        L_0x02f5:
            r10 = r4
            r11 = r5
            r12 = r6
            goto L_0x0051
        L_0x02fa:
            r4 = move-exception
            r10 = r4
            r11 = r19
            r12 = r20
            r13 = r21
        L_0x0302:
            r0 = r28
            com.uc.c.bx r0 = r0.bdG
            r4 = r0
            com.uc.c.r r4 = r4.mS
            com.uc.a.n r14 = r4.ch()
            com.uc.c.bs r4 = new com.uc.c.bs
            r6 = 1
            r8 = 1
            r5 = r30
            r4.<init>(r5, r6, r8)
            r14.a(r4)
            if (r12 == 0) goto L_0x0320
            r12.close()     // Catch:{ IOException -> 0x033a }
        L_0x0320:
            if (r11 == 0) goto L_0x0325
            r11.close()     // Catch:{ IOException -> 0x033c }
        L_0x0325:
            if (r13 == 0) goto L_0x0330
            boolean r4 = r13.fS()
            if (r4 != 0) goto L_0x0330
            r13.close()     // Catch:{ IOException -> 0x033e }
        L_0x0330:
            throw r10
        L_0x0331:
            r4 = move-exception
            goto L_0x006f
        L_0x0334:
            r4 = move-exception
            goto L_0x0074
        L_0x0337:
            r4 = move-exception
            goto L_0x007f
        L_0x033a:
            r4 = move-exception
            goto L_0x0320
        L_0x033c:
            r4 = move-exception
            goto L_0x0325
        L_0x033e:
            r4 = move-exception
            goto L_0x0330
        L_0x0340:
            r4 = move-exception
            r12 = r10
            r13 = r11
            r10 = r4
            r11 = r9
            goto L_0x0302
        L_0x0346:
            r4 = move-exception
            r12 = r10
            r13 = r11
            r10 = r4
            r11 = r8
            goto L_0x0302
        L_0x034c:
            r4 = move-exception
            r4 = r9
            r5 = r10
            r6 = r11
            goto L_0x02f5
        L_0x0351:
            r4 = move-exception
            r4 = r8
            r5 = r10
            r6 = r11
            goto L_0x02f5
        L_0x0356:
            r19 = r9
            r20 = r10
            r21 = r11
            r11 = r8
            r10 = r4
            goto L_0x0110
        L_0x0360:
            r20 = r10
            r21 = r11
            r10 = r19
            r11 = r4
            r19 = r9
            goto L_0x0110
        L_0x036b:
            r4 = r8
            goto L_0x00fb
        L_0x036e:
            r22 = r5
            goto L_0x0274
        L_0x0372:
            r4 = r18
            r24 = r17
            r17 = r7
            r7 = r24
            goto L_0x012f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.bb.a(java.lang.String, java.lang.String, int, com.uc.c.ca):void");
    }

    private final byte[] a(byte[] bArr, InputStream inputStream, int i) {
        byte[] bArr2;
        int i2;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (bArr != null) {
            byteArrayOutputStream.write(bArr);
        }
        byte[] bArr3 = new byte[1024];
        if (1024 > i) {
            bArr2 = new byte[i];
            i2 = 0;
        } else {
            bArr2 = bArr3;
            i2 = 0;
        }
        while (true) {
            try {
                int read = inputStream.read(bArr2);
                if (-1 == read || i2 >= i) {
                    break;
                }
                i2 += read;
                byteArrayOutputStream.write(bArr2, 0, read);
                if (bArr2.length + i2 > i) {
                    bArr2 = new byte[(i - i2)];
                }
            } catch (Exception e) {
            }
        }
        return byteArrayOutputStream.toByteArray();
    }

    public static void b(Drawable drawable, Drawable drawable2, int i, int i2, int i3, int i4) {
        bek = drawable;
        bel = drawable2;
        bem = i;
        ben = i2;
        beo = i3;
        bep = i4;
    }

    public static final void b(bk bkVar, ca caVar, byte[] bArr, Object obj) {
        switch (bArr[0]) {
            case 55:
                d(bkVar, caVar, bArr, obj);
                return;
            case 56:
                c(bkVar, caVar, bArr, obj);
                return;
            case 57:
            case 58:
            case 59:
            default:
                return;
            case 60:
                cd.MZ().e(bkVar, caVar, bArr, obj);
                return;
        }
    }

    public static final bg c(short s) {
        f fVar;
        Iterator it = bdD.zv().iterator();
        while (true) {
            if (!it.hasNext()) {
                Iterator it2 = bdD.zz().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        Iterator it3 = bdD.zw().iterator();
                        while (true) {
                            if (!it3.hasNext()) {
                                Iterator it4 = bdD.zx().iterator();
                                while (true) {
                                    if (!it4.hasNext()) {
                                        fVar = null;
                                        break;
                                    }
                                    fVar = (f) it4.next();
                                    if (fVar.Os() == s) {
                                        break;
                                    }
                                }
                            } else {
                                fVar = (f) it3.next();
                                if (fVar.Os() == s) {
                                    break;
                                }
                            }
                        }
                    } else {
                        fVar = (f) it2.next();
                        if (fVar.Os() == s) {
                            break;
                        }
                    }
                }
            } else {
                fVar = (f) it.next();
                if (fVar.Os() == s) {
                    break;
                }
            }
        }
        if (fVar == null) {
            return null;
        }
        bg bgVar = new bg();
        bgVar.biv = fVar.Ov();
        bgVar.biw = fVar.Ow();
        bgVar.bix = fVar.Ox();
        bgVar.biy = fVar.getFileName();
        bgVar.biz = fVar.OF();
        bgVar.biA = fVar.OG();
        bgVar.biB = bgVar.biA ? fVar.OH() : null;
        bgVar.biC = o(fVar);
        return bgVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(b.a.a.a.f, int, int, int, int, boolean):void
     arg types: [b.a.a.a.f, int, int, int, int, int]
     candidates:
      com.uc.c.bk.a(int, int, int, int, int, boolean):void
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int):void
      com.uc.c.bk.a(char[], int, int, int, int, int):void
      com.uc.c.bk.a(int[], int, int, int, int, boolean):void
      com.uc.c.bk.a(b.a.a.a.f, int, int, int, int, boolean):void */
    private static final void c(bk bkVar, ca caVar, byte[] bArr, Object obj) {
        int textSize = bkVar.bkB.getTextSize();
        try {
            int b2 = f.b(bArr);
            int c = f.c(bArr);
            int d = f.d(bArr);
            int e = f.e(bArr);
            byte b3 = ((byte[]) obj)[0];
            bkVar.a(gi(11), b2, c, d, e, true);
            String str = (b3 == 1 ? ar.avS[28] : ar.avS[29]) + " ( " + (b3 == 1 ? BW() - BX() : BX()) + " )";
            b.a.a.a.f gi = b3 == 1 ? gi(7) : gi(6);
            int i = 0;
            if (gi != null) {
                int i2 = b2 + 8;
                bkVar.a(gi, i2, gi.getHeight() > e ? b2 : ((e - gi.getHeight()) >> 1) + c, 20);
                i = i2;
            }
            int i3 = i + 8;
            int width = gi != null ? gi.getWidth() + i3 : i3 + 22;
            bkVar.setColor(gj(0));
            bkVar.bkB.setTextSize((float) bef);
            bkVar.b(str, width, c, d - (width - b2), e, 1, 2);
            b.a.a.a.f gi2 = gi(5);
            boolean z = bdS != b3 || !BY();
            if (gi2 != null) {
                int width2 = (int) ((((double) (b2 + d)) - (1.5d * ((double) gi2.FQ.getWidth()))) - ((double) (8 / 2)));
                int height = ((e >> 1) + c) - (gi2.FQ.getHeight() / 2);
                if (z) {
                    bkVar.a(gi2, width2, height, 20);
                } else {
                    bkVar.c(gi2, width2, height, 20, 90);
                }
            }
        } catch (Exception e2) {
        } finally {
            bkVar.bkB.setTextSize((float) textSize);
            bkVar.reset();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x00cc, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x00cd, code lost:
        r10 = r2;
        r2 = r1;
        r1 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x00d2, code lost:
        r3 = r1;
        r1 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0083, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x009d, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x00a2, code lost:
        r2 = null;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x00cc A[ExcHandler: all (th java.lang.Throwable), Splitter:B:20:0x002d] */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x00d1 A[ExcHandler: Throwable (th java.lang.Throwable), Splitter:B:18:0x0026] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0074 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x007a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0096 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x00ac A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x00b1 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean c(java.lang.String r12, byte[] r13) {
        /*
            r11 = this;
            r9 = 1
            r8 = 0
            r7 = 0
            r0 = r8
            r1 = r9
            r2 = r7
            r3 = r7
            r4 = r8
        L_0x0008:
            if (r1 == 0) goto L_0x0048
            if (r4 != 0) goto L_0x0048
            java.lang.String r1 = com.uc.c.bb.bdM     // Catch:{ Throwable -> 0x008c, all -> 0x00a7 }
            b.a.a.c.e r1 = com.uc.c.bx.ad(r1, r12)     // Catch:{ Throwable -> 0x008c, all -> 0x00a7 }
            if (r1 != 0) goto L_0x0024
            if (r1 == 0) goto L_0x0019
            r1.close()     // Catch:{ Exception -> 0x00b9, all -> 0x0020 }
        L_0x0019:
            if (r2 == 0) goto L_0x001e
            r2.close()     // Catch:{ Exception -> 0x00bc, all -> 0x0022 }
        L_0x001e:
            r0 = r4
        L_0x001f:
            return r0
        L_0x0020:
            r0 = move-exception
            throw r0
        L_0x0022:
            r0 = move-exception
            throw r0
        L_0x0024:
            java.lang.String r3 = "POST"
            r1.setRequestMethod(r3)     // Catch:{ Throwable -> 0x00d1, all -> 0x00c7 }
            java.io.OutputStream r2 = r1.cw()     // Catch:{ Throwable -> 0x00d1, all -> 0x00c7 }
            r2.write(r13)     // Catch:{ Throwable -> 0x00d1, all -> 0x00cc }
            r2.close()     // Catch:{ Throwable -> 0x00d1, all -> 0x00cc }
            r1.getResponseCode()     // Catch:{ Throwable -> 0x00d1, all -> 0x00cc }
            com.uc.c.bx r3 = r11.bdG     // Catch:{ Throwable -> 0x00d1, all -> 0x00cc }
            boolean r3 = r3.GZ()     // Catch:{ Throwable -> 0x00d1, all -> 0x00cc }
            if (r3 != 0) goto L_0x004e
            if (r1 == 0) goto L_0x0043
            r1.close()     // Catch:{ Exception -> 0x00bf, all -> 0x004a }
        L_0x0043:
            if (r2 == 0) goto L_0x0048
            r2.close()     // Catch:{ Exception -> 0x00c1, all -> 0x004c }
        L_0x0048:
            r0 = r4
            goto L_0x001f
        L_0x004a:
            r0 = move-exception
            throw r0
        L_0x004c:
            r0 = move-exception
            throw r0
        L_0x004e:
            java.lang.String r3 = "Content-Type"
            java.lang.String r3 = r1.getHeaderField(r3)     // Catch:{ Throwable -> 0x00d1, all -> 0x00cc }
            if (r3 == 0) goto L_0x00da
            java.lang.String r3 = r3.toLowerCase()     // Catch:{ Throwable -> 0x00d1, all -> 0x00cc }
            if (r0 != 0) goto L_0x00da
            java.lang.String r5 = "text/vnd.wap.wml"
            int r3 = r3.indexOf(r5)     // Catch:{ Throwable -> 0x00d1, all -> 0x00cc }
            if (r3 < 0) goto L_0x00da
            r0 = r9
            r3 = r9
        L_0x0066:
            java.lang.String r5 = "upload_rst"
            java.lang.String r5 = r1.getHeaderField(r5)     // Catch:{ Throwable -> 0x00d5, all -> 0x00cc }
            java.lang.String r6 = "0"
            boolean r4 = r6.equals(r5)     // Catch:{ Throwable -> 0x00d5, all -> 0x00cc }
            if (r1 == 0) goto L_0x0077
            r1.close()     // Catch:{ Exception -> 0x0082, all -> 0x0085 }
        L_0x0077:
            r1 = r7
        L_0x0078:
            if (r2 == 0) goto L_0x007d
            r2.close()     // Catch:{ Exception -> 0x0087, all -> 0x008a }
        L_0x007d:
            r2 = r7
        L_0x007e:
            r10 = r3
            r3 = r1
            r1 = r10
            goto L_0x0008
        L_0x0082:
            r1 = move-exception
            r1 = r7
            goto L_0x0078
        L_0x0085:
            r0 = move-exception
            throw r0
        L_0x0087:
            r2 = move-exception
            r2 = r7
            goto L_0x007e
        L_0x008a:
            r0 = move-exception
            throw r0
        L_0x008c:
            r1 = move-exception
            r1 = r8
        L_0x008e:
            if (r3 == 0) goto L_0x0093
            r3.close()     // Catch:{ Exception -> 0x009c, all -> 0x009f }
        L_0x0093:
            r3 = r7
        L_0x0094:
            if (r2 == 0) goto L_0x0099
            r2.close()     // Catch:{ Exception -> 0x00a1, all -> 0x00a5 }
        L_0x0099:
            r2 = r7
            goto L_0x0008
        L_0x009c:
            r3 = move-exception
            r3 = r7
            goto L_0x0094
        L_0x009f:
            r0 = move-exception
            throw r0
        L_0x00a1:
            r2 = move-exception
            r2 = r7
            goto L_0x0008
        L_0x00a5:
            r0 = move-exception
            throw r0
        L_0x00a7:
            r0 = move-exception
            r1 = r2
            r2 = r3
        L_0x00aa:
            if (r2 == 0) goto L_0x00af
            r2.close()     // Catch:{ Exception -> 0x00c3, all -> 0x00b5 }
        L_0x00af:
            if (r1 == 0) goto L_0x00b4
            r1.close()     // Catch:{ Exception -> 0x00c5, all -> 0x00b7 }
        L_0x00b4:
            throw r0
        L_0x00b5:
            r0 = move-exception
            throw r0
        L_0x00b7:
            r0 = move-exception
            throw r0
        L_0x00b9:
            r0 = move-exception
            goto L_0x0019
        L_0x00bc:
            r0 = move-exception
            goto L_0x001e
        L_0x00bf:
            r0 = move-exception
            goto L_0x0043
        L_0x00c1:
            r0 = move-exception
            goto L_0x0048
        L_0x00c3:
            r2 = move-exception
            goto L_0x00af
        L_0x00c5:
            r1 = move-exception
            goto L_0x00b4
        L_0x00c7:
            r0 = move-exception
            r10 = r2
            r2 = r1
            r1 = r10
            goto L_0x00aa
        L_0x00cc:
            r0 = move-exception
            r10 = r2
            r2 = r1
            r1 = r10
            goto L_0x00aa
        L_0x00d1:
            r3 = move-exception
            r3 = r1
            r1 = r8
            goto L_0x008e
        L_0x00d5:
            r5 = move-exception
            r10 = r3
            r3 = r1
            r1 = r10
            goto L_0x008e
        L_0x00da:
            r3 = r8
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.bb.c(java.lang.String, byte[]):boolean");
    }

    private static final String d(String str, int i, boolean z) {
        StringBuffer stringBuffer = new StringBuffer(40);
        stringBuffer.append(bdL).append('?');
        stringBuffer.append("sn=").append(bdK).append('&');
        stringBuffer.append("session=").append(str).append('&');
        stringBuffer.append("boundary=").append(Xv).append('&');
        stringBuffer.append("seq=").append(i).append('&');
        if (z) {
            stringBuffer.append("end=1");
        } else {
            stringBuffer.append("end=0");
        }
        return stringBuffer.toString();
    }

    private static final void d(bk bkVar, ca caVar, byte[] bArr, Object obj) {
        int i;
        b.a.a.a.f gi;
        String str;
        bkVar.save();
        bkVar.bkB.DX();
        bkVar.bkB.setTextSize((float) bec);
        bkVar.bkB.h((float) bec);
        try {
            int b2 = f.b(bArr);
            int c = f.c(bArr);
            int d = f.d(bArr);
            int e = f.e(bArr) - 1;
            int i2 = e >> 1;
            int i3 = ((int[]) obj)[0];
            bg c2 = c((short) i3);
            if (c2 != null) {
                byte b3 = c2.biv;
                long j = c2.biw;
                long j2 = c2.bix;
                String str2 = c2.biy;
                int i4 = c2.biz;
                boolean z = c2.biA;
                String str3 = c2.biB;
                String[] strArr = c2.biC;
                boolean z2 = c2.biv == 2;
                int i5 = beb;
                if (z2 || j2 <= 0) {
                    i = 0;
                } else {
                    long j3 = j >> 2;
                    long j4 = j2 >> 2;
                    if (j4 == 0) {
                        j4 = 1;
                    }
                    i = (int) ((j3 * ((long) d)) / j4);
                }
                if (((byte[]) caVar.Lk()) == bArr) {
                    if (!z2) {
                        Drawable drawable = bel;
                        drawable.setBounds(b2, bkVar.top + c, b2 + i, bkVar.top + c + e);
                        bkVar.bkB.K(drawable);
                    }
                    Drawable drawable2 = bek;
                    if (drawable2 != null) {
                        drawable2.setBounds(b2 + i, bkVar.top + c, b2 + d, bkVar.top + c + e);
                        bkVar.bkB.K(drawable2);
                    }
                } else {
                    if (!z2) {
                        bkVar.setColor(bem);
                        bkVar.o(b2, c, i, e);
                    }
                    bkVar.setColor(ben);
                    bkVar.o(b2 + i, c, d - i, e);
                }
                bkVar.setColor(bep);
                bkVar.o(b2, c - 1, d, 1);
                if (2 != c2.biv) {
                    switch (c2.biv) {
                        case -1:
                            gi = gi(18);
                            break;
                        case 0:
                        case 100:
                            gi = gi(19);
                            break;
                        case 1:
                            gi = gi(20);
                            break;
                        case 3:
                            gi = gi(21);
                            break;
                        default:
                            gi = gi(1);
                            break;
                    }
                } else {
                    switch (i4) {
                        case -2:
                        case 1:
                            if (!str2.endsWith(".apk")) {
                                if (!str2.endsWith(".mp3") && !str2.endsWith(".mp4") && !str2.endsWith(".wav")) {
                                    gi = gi(1);
                                    break;
                                } else {
                                    gi = gi(3);
                                    break;
                                }
                            } else {
                                gi = gi(0);
                                break;
                            }
                            break;
                        case -1:
                            gi = gi(14);
                            break;
                        case 0:
                            gi = gi(15);
                            break;
                        case 2:
                            gi = gi(17);
                            break;
                        case 3:
                            gi = gi(17);
                            break;
                        case 4:
                            gi = gi(17);
                            break;
                        default:
                            gi = gi(13);
                            break;
                    }
                }
                if (gi != null) {
                    bkVar.a(gi, b2 + i5, gi.getHeight() > e ? c : ((e - gi.getHeight()) >> 1) + c, 20);
                }
                bkVar.setColor(-1);
                b.a.a.a.f gi2 = gi(8);
                ai h = caVar.bOz != null ? h(caVar, i3) : null;
                if (h != null && h.abH) {
                    gi2 = gi(12);
                }
                if (!(gi2 == null || 100 == c2.biv)) {
                    bkVar.a(gi2, (d - gi2.getWidth()) - i5, ((e - gi2.FQ.getHeight()) / 2) + c, 20);
                }
                bkVar.setColor(-12554863);
                if (!(h == null || !h.abH || 100 == c2.biv)) {
                    bkVar.setColor(-1);
                }
                int width = ((d - gi2.getWidth()) - i5) + ((gi2.getWidth() - bkVar.bkB.ev("继续")) / 2) + bkVar.left;
                int height = ((e - gi2.FQ.getHeight()) / 2) + c + ((gi2.FQ.getHeight() - (bkVar.bkB.getHeight() + bkVar.bkB.uF())) / 2) + bkVar.top;
                if (-1 == b3) {
                    bkVar.bkB.b("等待", width, height, 2);
                    h.abF = beq[0];
                } else if (100 == b3) {
                    bkVar.bkB.b("启动中", width - 10, height, 2);
                    h.abF = beq[8];
                } else if (b3 == 0) {
                    bkVar.bkB.b("暂停", width, height, 2);
                    h.abF = beq[1];
                } else if (1 == b3) {
                    bkVar.bkB.b("继续", width, height, 2);
                    h.abF = beq[2];
                } else if (2 == b3) {
                    switch (b.a.a.d.cu(str2)) {
                        case 0:
                        case 13:
                        case 14:
                            h.abF = beq[4];
                            bkVar.bkB.b("详细", width, height, 2);
                            break;
                        case 1:
                        case 4:
                        case 5:
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                        case 17:
                        case 18:
                        case 19:
                        case 20:
                        case 21:
                        case 22:
                        case 23:
                            h.abF = beq[6];
                            bkVar.bkB.b("打开", width, height, 2);
                            break;
                        case 2:
                        case 3:
                        case 11:
                        case 12:
                        case 15:
                        case 16:
                            h.abF = beq[5];
                            bkVar.bkB.b("播放", width, height, 2);
                            break;
                        case 6:
                        case 24:
                        case 25:
                            h.abF = beq[7];
                            bkVar.bkB.b("安装", width, height, 2);
                            break;
                        default:
                            h.abF = beq[4];
                            bkVar.bkB.b("详细", width, height, 2);
                            break;
                    }
                } else if (3 == b3) {
                    bkVar.bkB.b("重试", width, height, 2);
                    h.abF = beq[3];
                }
                int i6 = 0;
                if (gi2 != null) {
                    i6 = gi2.getWidth() + 4;
                }
                int i7 = b2 + i5 + 5;
                int width2 = gi != null ? gi.getWidth() + i7 : i7;
                bkVar.setColor(gj(1));
                bkVar.bkB.setTextSize((float) bed);
                if (str2 != null) {
                    int i8 = (((d - i6) - width2) - (i5 * 2)) + 5;
                    if (bkVar.bkB.cD(str2) > i8) {
                        String a2 = bc.a(str2, i8, bed, 0);
                        if (b3 == 2 && !z) {
                            i2 = e;
                        }
                        bkVar.b(a2, width2, c, i8, i2, 1, 2);
                    } else {
                        if (b3 == 2 && !z) {
                            i2 = e;
                        }
                        bkVar.b(str2, width2, c, i8, i2, 1, 2);
                    }
                }
                bkVar.bkB.setTextSize((float) bee);
                int i9 = 10;
                if (!(b3 == 2 || strArr[1] == null)) {
                    int ev = bkVar.bkB.ev(strArr[1]);
                    int i10 = (((d - i6) - width2) - (i5 * 2)) + 5;
                    int ev2 = bkVar.bkB.ev("...");
                    i9 = 10 + ev;
                    if (ev > i10) {
                        int a3 = bc.a(strArr[1].toCharArray(), i10 - ev2, bkVar.bkB);
                        String substring = strArr[1].substring(0, a3);
                        bkVar.b(a3 < strArr[1].length() ? substring + "..." : substring, width2, c, i10, e, 1, 3);
                    } else {
                        bkVar.b(strArr[1], width2, c, i10, e, 1, 3);
                    }
                }
                if (b3 == 0 && strArr[0] != null) {
                    int i11 = ((((d - i6) - width2) - (i5 * 2)) - i9) + 5;
                    if (bkVar.bkB.ev(strArr[0]) > i11) {
                        int a4 = bc.a(strArr[0].toCharArray(), i11 - bkVar.bkB.ev("..."), bkVar.bkB);
                        String substring2 = strArr[0].substring(0, a4);
                        bkVar.b(a4 < strArr[0].length() ? substring2 + "..." : substring2, width2 + i9, c, i11, e, 1, 3);
                    } else {
                        bkVar.b(strArr[0], width2 + i9, c, i11, e, 1, 3);
                    }
                }
                if (b3 == 2) {
                    switch (i4) {
                        case -2:
                        case 1:
                            str = null;
                            break;
                        case -1:
                            str = "扫描中...";
                            break;
                        case 0:
                            str = "安全";
                            break;
                        case 2:
                            str = "高风险";
                            break;
                        case 3:
                            str = "中风险";
                            break;
                        case 4:
                            str = "低风险";
                            break;
                        default:
                            str = "";
                            break;
                    }
                    if (z) {
                        bkVar.b(str, width2, c, bkVar.bkB.ev(str), e, 1, 3);
                        if (str3 != null) {
                            int ev3 = bkVar.bkB.ev(str3) + 5 + i5;
                            bkVar.b(str3, (d - i6) - ev3, c, ev3, e, 1, 3);
                        }
                    }
                }
            }
        } catch (Exception e2) {
        } finally {
            bkVar.reset();
        }
    }

    public static String dC(String str) {
        StringBuffer stringBuffer = new StringBuffer(20);
        stringBuffer.append("file|upload://");
        stringBuffer.append(bdK).append(':');
        stringBuffer.append(str).append(':');
        stringBuffer.append((char) ca.bNQ);
        return stringBuffer.toString();
    }

    private byte[] dE(String str) {
        byte[] bArr = new byte[0];
        byte[] dF = dF(str);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        dataOutputStream.writeShort(dF.length + 2);
        dataOutputStream.write(dF);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        dataOutputStream.close();
        byteArrayOutputStream.close();
        return byteArray;
    }

    private byte[] dF(String str) {
        byte[] bArr = new byte[0];
        String substring = str.substring(str.lastIndexOf(47) + 1, str.length());
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        dataOutputStream.writeByte(1);
        dataOutputStream.writeUTF(ActivityBookmarkEx.asf);
        dataOutputStream.writeUTF(substring);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        dataOutputStream.close();
        byteArrayOutputStream.close();
        return byteArray;
    }

    public static String dH(String str) {
        int indexOf;
        if (bc.by(str)) {
            return "";
        }
        String lowerCase = str.toLowerCase();
        if (lowerCase.indexOf("attachment") == -1 || (indexOf = lowerCase.indexOf("filename=")) == -1) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer(str.substring(indexOf + 9, str.length()).trim());
        if (stringBuffer.charAt(0) == '\"' || stringBuffer.charAt(0) == '\'') {
            stringBuffer.deleteCharAt(0);
        }
        if (stringBuffer.charAt(stringBuffer.length() - 1) == '\"' || stringBuffer.charAt(stringBuffer.length() - 1) == '\'') {
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        }
        String stringBuffer2 = stringBuffer.toString();
        stringBuffer.setLength(0);
        return stringBuffer2;
    }

    private boolean dQ() {
        ca jm = this.mS.jm();
        byte[] Aq = Aq();
        if (Aq != null) {
            switch (Aq[0]) {
                case 55:
                    return true;
                case 56:
                    byte b2 = ((byte[]) Ar())[1];
                    if (!gf(b2) || b2 == 0) {
                        if (!(this.mS == null || this.mS.ch() == null)) {
                            this.mS.ch().bw(false);
                        }
                        gg(b2);
                    }
                    return true;
                case 60:
                    cd.MZ().a(jm, Aq, S(Aq));
                    return false;
            }
        }
        return false;
    }

    private final void f(ca caVar, int i) {
        int i2;
        f fVar;
        f fVar2;
        int i3;
        f fVar3;
        int i4;
        f fVar4;
        int i5;
        try {
            bdS = (byte) i;
            caVar.fp(w.OB);
            caVar.iH(11);
            caVar.iH(9);
            caVar.iH(15);
            caVar.bHD = (byte) (caVar.bHD | 8);
            caVar.t(w.MO, w.MP, w.MK, w.MN);
            caVar.bp(w.MK, w.MN);
            caVar.bq(w.MK, -1);
            caVar.bHq = 1;
            caVar.KG();
            caVar.KV();
            int i6 = w.MK - (caVar.bHp << 1);
            int i7 = w.Qc;
            switch (i) {
                case 0:
                    r.a(caVar, 56, i6, i7, new byte[]{1, 1});
                    if (this.bdT == 1 && 0 == 0 && -1 == -1) {
                        fVar4 = caVar.bGZ;
                        i5 = caVar.bGZ.cF - 1;
                    } else {
                        fVar4 = null;
                        i5 = -1;
                    }
                    r.a(caVar, 56, i6, i7, new byte[]{2, 2});
                    if (this.bdT == 2 && fVar4 == null && i5 == -1) {
                        fVar2 = caVar.bGZ;
                        i3 = caVar.bGZ.cF - 1;
                    } else {
                        i3 = i5;
                        fVar2 = fVar4;
                    }
                    cd.MZ().a(caVar, cd.bUb, this.mR, this.mS);
                    break;
                case 1:
                    r.a(caVar, 56, i6, i7, new byte[]{1, 0});
                    if (0 == 0 && -1 == -1) {
                        fVar3 = caVar.bGZ;
                        i4 = caVar.bGZ.cF - 1;
                    } else {
                        fVar3 = null;
                        i4 = -1;
                    }
                    g(caVar, 1);
                    r.a(caVar, 56, i6, i7, new byte[]{2, 2});
                    cd.MZ().a(caVar, cd.bUb, this.mR, this.mS);
                    i3 = i4;
                    fVar2 = fVar3;
                    break;
                case 2:
                    r.a(caVar, 56, i6, i7, new byte[]{1, 1});
                    r.a(caVar, 56, i6, i7, new byte[]{2, 0});
                    if (0 == 0 && -1 == -1) {
                        fVar = caVar.bGZ;
                        i2 = caVar.bGZ.cF - 1;
                    } else {
                        i2 = -1;
                        fVar = null;
                    }
                    g(caVar, 2);
                    cd.MZ().a(caVar, cd.bUb, this.mR, this.mS);
                    break;
                default:
                    i3 = -1;
                    fVar2 = null;
                    break;
            }
            caVar.t(ar.avS[2]);
            caVar.Lq();
            caVar.KH();
            a(caVar, caVar.bFR);
            caVar.bGd.a(fVar2, i3, 0);
        } catch (Exception e) {
        }
    }

    private void g(ca caVar, int i) {
        int i2 = w.MK - (caVar.bHp << 1);
        int i3 = w.Qc;
        if (i == 1) {
            Vector zv = bdD.zv();
            for (int size = zv.size() - 1; size >= 0; size--) {
                r.a(caVar, 55, i2, i3, new int[]{((f) zv.elementAt(size)).Os()});
            }
            Vector zw = bdD.zw();
            for (int size2 = zw.size() - 1; size2 >= 0; size2--) {
                r.a(caVar, 55, i2, i3, new int[]{((f) zw.elementAt(size2)).Os()});
            }
            Vector zz = bdD.zz();
            for (int size3 = zz.size() - 1; size3 >= 0; size3--) {
                r.a(caVar, 55, i2, i3, new int[]{((f) zz.elementAt(size3)).Os()});
            }
        } else if (i == 2) {
            Vector zx = bdD.zx();
            for (int size4 = zx.size() - 1; size4 >= 0; size4--) {
                r.a(caVar, 55, i2, i3, new int[]{((f) zx.elementAt(size4)).Os()});
            }
        }
    }

    private final void ge(int i) {
        byte[] c;
        if (this.bdF != null && !this.bdF.Lw() && (c = ca.c(this.bdF.bHl, this.bdF.bHm)) != null && c[0] != 14) {
            this.bdF.bGd.a(this, i);
        }
    }

    public static final boolean gf(int i) {
        if (i == 1) {
            if (bdD.zv().size() > 0 || bdD.zz().size() > 0 || bdD.zw().size() > 0) {
                return false;
            }
        } else if (i == 2 && bdD.zx().size() > 0) {
            return false;
        }
        return true;
    }

    private final synchronized void gh(int i) {
        ca Ku = ca.Ku();
        Ku.jh(1);
        f(Ku, i);
        this.mS.o(Ku);
        this.mS.jA();
        n ch = this.mS.ch();
        if (ch != null) {
            if (i == 0) {
                ch.bw(false);
            } else {
                ch.bw(true);
            }
            ch.Dh();
        }
        this.mS.je();
    }

    public static b.a.a.a.f gi(int i) {
        if (beg != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= beg.size()) {
                    break;
                }
                l lVar = (l) beg.get(i3);
                if (i == lVar.oy) {
                    return lVar.ox;
                }
                i2 = i3 + 1;
            }
        }
        return null;
    }

    public static int gj(int i) {
        if (i == 0) {
            return beh;
        }
        if (i == 1) {
            return bei;
        }
        return -1;
    }

    public static ai h(ca caVar, int i) {
        if (caVar == null || caVar.bOz == null) {
            return null;
        }
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= caVar.bOz.size()) {
                return null;
            }
            ai aiVar = (ai) caVar.bOz.get(i3);
            if (aiVar != null && i == aiVar.abG) {
                return aiVar;
            }
            i2 = i3 + 1;
        }
    }

    private static final String[] o(f fVar) {
        String[] strArr = new String[3];
        if (fVar != null) {
            byte Ov = fVar.Ov();
            switch (Ov) {
                case -1:
                    strArr[0] = ar.avS[7];
                    break;
                case 0:
                    strArr[0] = bc.j((long) fVar.OA()) + "/S";
                    break;
                case 1:
                    strArr[0] = ar.avS[8];
                    break;
                case 2:
                    strArr[0] = bc.j(fVar.Ox());
                    break;
                case 3:
                    strArr[0] = ar.avS[10];
                    break;
            }
            if (Ov != 2) {
                long Ow = fVar.Ow();
                long Ox = fVar.Ox();
                strArr[1] = new StringBuffer().append(bc.j(Ow)).append(Ox > 0 ? au.aGF : "").append(Ox > 0 ? bc.j(Ox) : "").append(' ').toString();
            }
        }
        return strArr;
    }

    private void p(f fVar) {
        if (fVar != null) {
            bdD.a(fVar, true);
            fVar.OM();
            if (this.mS != null && this.mS.ch() != null && this.mR != null && this.mR.JT == 0) {
                this.mS.ch().qW();
                this.mS.ch().dP(fVar.Os());
            }
        }
    }

    /* access modifiers changed from: private */
    public void q(f fVar) {
        if (fVar != null) {
            if (bc.ef(fVar.getFileName())) {
                if (this.mS != null && this.mS.ch() != null) {
                    this.mS.ch().a(ar.avZ[0], fVar.Oy(), fVar.getFileName(), fVar.getFileName() + ar.avZ[5], fVar.OF());
                    this.mS.ch().dP(fVar.Os());
                }
            } else if (az.bbL == 0) {
                if (cd.bUL == 1 && az.bcH && w.aN(fVar.getFileName()) && fVar.Ov() == 2) {
                    new bx(fVar, new g(this, fVar).f(fVar)).start();
                } else if (fVar.Ov() == 2 && bc.ee(fVar.getFileName()) && this.mS != null && this.mS.ch() != null) {
                    this.mS.ch().a(ar.avZ[0], fVar.Oy(), fVar.getFileName(), fVar.getFileName() + ar.avZ[4], fVar.OF());
                    this.mS.ch().dP(fVar.Os());
                }
            } else if (1 == az.bbL) {
                if (this.mS != null && this.mS.ch() != null) {
                    this.mS.ch().bN(fVar.getFileName() + ar.avZ[6]);
                }
            } else if (2 != az.bbL) {
                if (3 == az.bbL) {
                }
            } else if (this.mS != null && this.mS.ch() != null) {
                this.mS.ch().a(ar.avZ[0], fVar.Oy(), fVar.getFileName(), fVar.getFileName() + ar.avZ[5], fVar.OF());
                this.mS.ch().dP(fVar.Os());
            }
        }
    }

    public byte[] Aq() {
        ca jm = this.mS.jm();
        return ca.c(jm.bHl, jm.bHm);
    }

    public Object Ar() {
        return S(Aq());
    }

    public int BS() {
        return this.bdX;
    }

    public short BT() {
        return BV().Os();
    }

    public String[] BU() {
        f BV = BV();
        if (BV == null) {
            return null;
        }
        String[] strArr = new String[5];
        strArr[0] = BV.getFileName();
        strArr[1] = String.valueOf(BV.Ox());
        strArr[2] = String.valueOf(BV.Ow());
        strArr[3] = BV.Oy() + strArr[0];
        strArr[4] = BV.Ot();
        return strArr;
    }

    public boolean BZ() {
        return bdD.zv().size() > 0;
    }

    public void Ca() {
        Thread.yield();
        gh(bdS);
    }

    public void Cb() {
        f fVar = (f) ((Object[]) this.bdU)[0];
        String str = (String) ((Object[]) this.bdU)[1];
        String str2 = (String) ((Object[]) this.bdU)[2];
        if (au.S(str2, str)) {
            fVar.av(str2, str);
            list();
            return;
        }
        this.mR.mN();
    }

    public final void Cc() {
        bdD.fx(az.bbE);
    }

    public Object S(byte[] bArr) {
        ca jm = this.mS.jm();
        return ((Vector) jm.bGl.get(Integer.valueOf(jm.bS()))).elementAt(f.b(bArr, r.Eu, 0));
    }

    public boolean X() {
        return this.bdH == 1;
    }

    public void a(int i, Resources resources, int i2) {
        if (beg == null) {
            beg = new Vector();
        }
        int i3 = 0;
        while (true) {
            if (i3 < beg.size()) {
                l lVar = (l) beg.get(i3);
                if (lVar != null && lVar.oy == i) {
                    beg.remove(i3);
                    break;
                }
                i3++;
            } else {
                break;
            }
        }
        if (i3 < beg.size()) {
        }
        l lVar2 = new l();
        lVar2.ox = b.a.a.a.f.a(resources, i2);
        lVar2.oy = i;
        beg.add(lVar2);
    }

    public void a(int i, Bitmap bitmap) {
        if (beg == null) {
            beg = new Vector(1);
        }
        int i2 = 0;
        while (true) {
            if (i2 < beg.size()) {
                l lVar = (l) beg.get(i2);
                if (lVar != null && lVar.oy == i) {
                    beg.remove(i2);
                    break;
                }
                i2++;
            } else {
                break;
            }
        }
        l lVar2 = new l();
        lVar2.ox = new b.a.a.a.f(bitmap);
        lVar2.oy = i;
        beg.add(lVar2);
    }

    public final void a(int i, int[] iArr) {
        switch (i) {
            case 8:
            case 53:
                if (dQ()) {
                    return;
                }
                break;
        }
        this.mS.b(i, iArr);
    }

    public void a(bx bxVar, ca caVar) {
        this.bdG = bxVar;
        if (this.bdG.GZ()) {
            int size = this.bdE.size();
            this.bdI = BR();
            for (int i = 0; i < size && this.bdG.GZ(); i++) {
                String str = ((String[]) this.bdE.elementAt(i))[0];
                String str2 = ((String[]) this.bdE.elementAt(i))[1];
                if (str2 != null) {
                    a(str, str2, i, caVar);
                }
            }
            this.bdI = null;
            bc.gc();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x00e3  */
    /* JADX WARNING: Removed duplicated region for block: B:54:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.String[] r12, java.lang.String r13, java.lang.Object r14) {
        /*
            r11 = this;
            r0 = 0
            r1 = 4
            r1 = r12[r1]
            if (r1 == 0) goto L_0x0018
            r1 = 4
            r1 = r12[r1]
            java.lang.String r2 = ":lnk"
            int r1 = r1.indexOf(r2)
            r2 = -1
            if (r1 == r2) goto L_0x0018
            r1 = 2
            int r0 = com.uc.c.bc.aK(r0, r1)
            byte r0 = (byte) r0
        L_0x0018:
            r1 = 4
            r1 = r12[r1]
            if (r1 == 0) goto L_0x002f
            r1 = 4
            r1 = r12[r1]
            java.lang.String r2 = ":webkit"
            int r1 = r1.indexOf(r2)
            r2 = -1
            if (r1 == r2) goto L_0x002f
            r1 = 5
            int r0 = com.uc.c.bc.aK(r0, r1)
            byte r0 = (byte) r0
        L_0x002f:
            boolean r1 = r14 instanceof com.uc.c.z
            if (r1 == 0) goto L_0x0039
            r1 = 4
            int r0 = com.uc.c.bc.aK(r0, r1)
            byte r0 = (byte) r0
        L_0x0039:
            java.lang.String r1 = "GET"
            r2 = 0
            r3 = 0
            r4 = 5
            r4 = r12[r4]     // Catch:{ Exception -> 0x00fc }
            boolean r4 = com.uc.c.bc.dM(r4)     // Catch:{ Exception -> 0x00fc }
            if (r4 == 0) goto L_0x010e
            r4 = 5
            r4 = r12[r4]     // Catch:{ Exception -> 0x00fc }
            java.lang.String r4 = r4.toLowerCase()     // Catch:{ Exception -> 0x00fc }
            java.lang.String r5 = "post"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x00fc }
            if (r4 == 0) goto L_0x010e
            r4 = 6
            r4 = r12[r4]     // Catch:{ Exception -> 0x00fc }
            boolean r4 = com.uc.c.bc.dM(r4)     // Catch:{ Exception -> 0x00fc }
            if (r4 == 0) goto L_0x010e
            r4 = 8
            int r0 = com.uc.c.bc.aK(r0, r4)     // Catch:{ Exception -> 0x00fc }
            byte r0 = (byte) r0     // Catch:{ Exception -> 0x00fc }
            java.lang.String r0 = "POST"
            r1 = 6
            r1 = r12[r1]     // Catch:{ Exception -> 0x0102 }
            char[] r1 = r1.toCharArray()     // Catch:{ Exception -> 0x0102 }
            byte[] r1 = com.uc.c.bc.e(r1)     // Catch:{ Exception -> 0x0102 }
            r2 = 7
            r2 = r12[r2]     // Catch:{ Exception -> 0x0106 }
            boolean r4 = com.uc.c.bc.dM(r2)     // Catch:{ Exception -> 0x0106 }
            if (r4 == 0) goto L_0x010b
            java.lang.String r4 = "content-type:"
            java.lang.String r5 = r2.toLowerCase()     // Catch:{ Exception -> 0x0106 }
            int r5 = r5.indexOf(r4)     // Catch:{ Exception -> 0x0106 }
            r6 = -1
            if (r5 == r6) goto L_0x010b
            java.lang.String r6 = r2.toLowerCase()     // Catch:{ Exception -> 0x0106 }
            java.lang.String r7 = "\r\n"
            int r6 = r6.indexOf(r7, r5)     // Catch:{ Exception -> 0x0106 }
            int r4 = r4.length()     // Catch:{ Exception -> 0x0106 }
            int r4 = r4 + r5
            if (r6 <= r5) goto L_0x00f7
            r5 = r6
        L_0x009a:
            java.lang.String r2 = r2.substring(r4, r5)     // Catch:{ Exception -> 0x0106 }
            java.lang.String r2 = r2.trim()     // Catch:{ Exception -> 0x0106 }
            r10 = r2
            r2 = r0
            r0 = r10
        L_0x00a5:
            r9 = r0
            r8 = r1
            r7 = r2
        L_0x00a8:
            com.uc.c.a.f r0 = new com.uc.c.a.f
            com.uc.c.a.d r1 = com.uc.c.bb.bdD
            short r1 = r1.zl()
            r2 = 0
            r2 = r12[r2]
            java.lang.String r2 = com.uc.c.bc.dN(r2)
            r3 = 2
            r3 = r12[r3]
            java.lang.String r3 = com.uc.c.bc.dN(r3)
            r4 = 1
            r4 = r12[r4]
            java.lang.String r4 = com.uc.c.bc.dN(r4)
            java.lang.String r5 = com.uc.c.bc.dN(r13)
            r6 = 3
            r6 = r12[r6]
            java.lang.String r6 = com.uc.c.bc.dN(r6)
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            com.uc.c.a.d r1 = com.uc.c.bb.bdD
            r1.h(r0)
            r0 = 1
            r11.gg(r0)
            com.uc.c.w r0 = r11.mR
            byte r0 = r0.JT
            r1 = 1
            if (r0 != r1) goto L_0x00f6
            com.uc.c.r r0 = r11.mS
            com.uc.a.n r0 = r0.ch()
            if (r0 == 0) goto L_0x00f6
            com.uc.c.r r0 = r11.mS
            com.uc.a.n r0 = r0.ch()
            java.lang.String r1 = "成功添加下载任务到下载列表，点击菜单“下载管理”查看"
            r0.bN(r1)
        L_0x00f6:
            return
        L_0x00f7:
            int r5 = r2.length()     // Catch:{ Exception -> 0x0106 }
            goto L_0x009a
        L_0x00fc:
            r0 = move-exception
            r0 = r2
        L_0x00fe:
            r9 = r3
            r8 = r0
            r7 = r1
            goto L_0x00a8
        L_0x0102:
            r1 = move-exception
            r1 = r0
            r0 = r2
            goto L_0x00fe
        L_0x0106:
            r2 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x00fe
        L_0x010b:
            r2 = r0
            r0 = r3
            goto L_0x00a5
        L_0x010e:
            r0 = r3
            r10 = r2
            r2 = r1
            r1 = r10
            goto L_0x00a5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.bb.a(java.lang.String[], java.lang.String, java.lang.Object):void");
    }

    public void aH(int i, int i2) {
        ca jj;
        if (this.mS != null && (jj = this.mS.jj()) != null && jj.bOz != null) {
            for (int i3 = 0; i3 < jj.bOz.size(); i3++) {
                ai aiVar = (ai) jj.bOz.get(i3);
                if (aiVar != null) {
                    if (true == bc.b(i, i2, aiVar.x, aiVar.y + (jj != null ? jj.bGo - jj.bGx : 0), aiVar.abD, aiVar.abE)) {
                        aiVar.abH = true;
                        if (this.mS != null && this.mS.ch() != null) {
                            this.mS.ch().qW();
                            return;
                        }
                        return;
                    }
                }
            }
        }
    }

    public int aI(int i, int i2) {
        if (this.mS == null) {
            return -2;
        }
        ca jj = this.mS.jj();
        if (jj == null) {
            return -2;
        }
        if (jj.bOz == null) {
            return -2;
        }
        try {
            if (jj.bHl != null && (jj.bHl.cE[jj.bHm] instanceof byte[]) && ((byte[]) jj.bHl.cE[jj.bHm])[0] == 55) {
                byte[] bArr = (byte[]) jj.bHl.cE[jj.bHm];
                if (!bc.b(i, i2, f.b(bArr), f.c(bArr) - jj.bGx, f.d(bArr), f.e(bArr))) {
                    return -2;
                }
            }
        } catch (Exception e) {
        }
        for (int i3 = 0; i3 < jj.bOz.size(); i3++) {
            ai aiVar = (ai) jj.bOz.get(i3);
            if (aiVar != null) {
                if (true == bc.b(i, i2, aiVar.x, aiVar.y + (jj != null ? jj.bGo - jj.bGx : 0), aiVar.abD, aiVar.abE)) {
                    return aiVar.abF;
                }
            }
        }
        return -2;
    }

    public boolean aI() {
        f BV = BV();
        if (BV != null) {
            return BV.OD();
        }
        return true;
    }

    public void aJ() {
        p(BV());
    }

    public void aK() {
        bdD.zr();
        if (this.mS != null && this.mS.ch() != null && this.mR != null && this.mR.JT == 0) {
            this.mS.ch().qW();
            this.mS.ch().rb();
        }
    }

    public void aL() {
        f BV = BV();
        if (BV != null) {
            bdD.j(BV);
            BV.OM();
        }
    }

    public void aM() {
        f BV = BV();
        if (BV != null) {
            bdD.l(BV);
        }
        gg(1);
    }

    public byte aP() {
        f BV = BV();
        if (BV != null) {
            return BV.Ov();
        }
        return -2;
    }

    public byte b(short s) {
        f d = d(s);
        if (d != null) {
            return d.Ov();
        }
        return 3;
    }

    public void bo(boolean z) {
        Vector zx = bdD.zx();
        for (int size = zx.size() - 1; size >= 0; size--) {
            f fVar = (f) zx.get(size);
            if (fVar != null) {
                if (this.mS.ch() != null) {
                    this.mS.ch().dP(fVar.Os());
                }
                bdD.c(fVar, z);
                if (this.mS.ch() != null) {
                    this.mS.ch().rc();
                }
            }
        }
        list();
    }

    public void bp(boolean z) {
        f BV = BV();
        if (BV != null) {
            if (this.mS.ch() != null) {
                this.mS.ch().dP(BV.Os());
            }
            bdD.c(BV, z);
            if (this.mS.ch() != null) {
                this.mS.ch().rc();
            }
            list();
        }
    }

    public void bq(boolean z) {
        bdD.bf(z);
        list();
        if (this.mS != null && this.mS.ch() != null) {
            this.mS.ch().rb();
        }
    }

    public final void br(boolean z) {
    }

    public void c(ca caVar) {
        f(caVar, 1);
    }

    public final f d(short s) {
        Iterator it = bdD.zz().iterator();
        while (it.hasNext()) {
            f fVar = (f) it.next();
            if (fVar.Os() == s) {
                return fVar;
            }
        }
        Iterator it2 = bdD.zw().iterator();
        while (it2.hasNext()) {
            f fVar2 = (f) it2.next();
            if (fVar2.Os() == s) {
                return fVar2;
            }
        }
        Iterator it3 = bdD.zv().iterator();
        while (it3.hasNext()) {
            f fVar3 = (f) it3.next();
            if (fVar3.Os() == s) {
                return fVar3;
            }
        }
        Iterator it4 = bdD.zx().iterator();
        while (it4.hasNext()) {
            f fVar4 = (f) it4.next();
            if (fVar4.Os() == s) {
                return fVar4;
            }
        }
        return null;
    }

    public String dD(String str) {
        if (str != null && str.length() > 0) {
            this.bdH = 1;
        }
        String str2 = BQ() + this.bdE.size();
        this.bdE.addElement(new String[]{str2, str});
        return dC(str2);
    }

    public void dG(String str) {
        f BV = BV();
        if (BV.Ov() == 2) {
            this.bdU = new Object[]{BV, str};
            new d(this).start();
        }
    }

    public void e(int i) {
        this.bdX = i;
    }

    public String gb() {
        f BV = BV();
        if (BV != null) {
            return BV.getFileName();
        }
        return null;
    }

    public String ge() {
        f BV = BV();
        if (BV != null) {
            return BV.Oy() + BV.getFileName();
        }
        return null;
    }

    public String gf() {
        f BV = BV();
        if (BV != null) {
            return BV.Oy();
        }
        return null;
    }

    public final void gg(int i) {
        this.bdT = bdS;
        bdS = (byte) i;
        gh(i);
    }

    public void gj() {
        if (beg != null) {
            beg.clear();
            beg = null;
        }
    }

    public int gl() {
        f BV = BV();
        if (BV == null) {
            return 0;
        }
        return BV.Ov() == 2 ? 2 : 1;
    }

    public void j(Drawable drawable) {
        bej = drawable;
    }

    public final void list() {
        gh(bdS);
    }

    public String oq() {
        f BV = BV();
        if (BV != null) {
            return BV.Ot();
        }
        return null;
    }

    public void x(int i, int i2) {
        if (i == 0) {
            beh = i2;
        } else if (i == 1) {
            bei = i2;
        }
    }
}
