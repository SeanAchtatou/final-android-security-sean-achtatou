package com.uc.c;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import b.a.a.a.k;
import com.uc.a.n;
import com.uc.b.e;
import com.uc.browser.UCR;
import com.uc.plugin.Plugin;
import java.util.Vector;

public final class f {
    private static int[] cK = new int[48];
    static final int cL = -130;
    static final int cM = 45;
    public static final byte cN = 9;
    public static final byte cO = 50;
    Object[] cE;
    short cF;
    f cG;
    byte[] cH;
    short cI;
    int cJ;
    public final int cP;
    public final int cQ;

    public f() {
        this.cE = null;
        this.cF = 0;
        this.cG = null;
        this.cH = null;
        this.cI = Short.MAX_VALUE;
        this.cJ = 0;
        this.cP = k.adr * 3;
        this.cQ = k.ads;
        this.cE = new Object[16];
    }

    public f(int i) {
        this.cE = null;
        this.cF = 0;
        this.cG = null;
        this.cH = null;
        this.cI = Short.MAX_VALUE;
        this.cJ = 0;
        this.cP = k.adr * 3;
        this.cQ = k.ads;
        this.cE = new Object[16];
        this.cJ = i;
    }

    private int a(ca caVar, int[] iArr) {
        if (iArr == null || iArr.length < 4) {
            return 0;
        }
        f[] fVarArr = {caVar.bFR};
        byte[] a2 = caVar.bGd.a(fVarArr[0], (f) null, 1, iArr, fVarArr, new int[]{-1}, 1);
        if (fVarArr == null || fVarArr.length == 0) {
            return 0;
        }
        if (a2 != null && a2.length > 0 && a2[0] == 0) {
            int e = e(a2);
            int b2 = b(a2, ca.bLe, 1);
            if (fVarArr[0] != null) {
                int i = fVarArr[0].cJ;
                if ((-1 == b2 || ((Vector) caVar.bFV.get(Integer.valueOf(i))).elementAt(b2) == null) && iArr[3] < e) {
                    return e - iArr[3];
                }
            }
        }
        return 0;
    }

    private int a(byte[] bArr, ca caVar) {
        return b(bArr);
    }

    public static final String a(byte[] bArr, byte[] bArr2, int i) {
        StringBuffer stringBuffer = new StringBuffer();
        a(bArr, bArr2, i, stringBuffer);
        return stringBuffer.toString();
    }

    private void a(bk bkVar, int i, int i2, boolean z, int i3, int i4, b.a.a.a.f fVar, int i5, int i6) {
        if (i5 > i3 || i6 > i4) {
            bkVar.f(fVar, i, i2, i5, i6);
        } else if (!z) {
            bkVar.a(fVar, i, i2, 20);
        } else if (fVar == null || !fVar.FT) {
            bkVar.d(fVar, i, i2, i3, i4);
        } else {
            bkVar.a(fVar, i, i2, 20);
        }
    }

    private void a(bk bkVar, byte[] bArr, ca caVar) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.f.a(com.uc.c.ca, byte[], boolean):boolean
     arg types: [com.uc.c.ca, byte[], int]
     candidates:
      com.uc.c.f.a(byte[], byte[], int):java.lang.String
      com.uc.c.f.a(com.uc.c.bk, byte[], com.uc.c.ca):void
      com.uc.c.f.a(byte[], int, int):void
      com.uc.c.f.a(com.uc.c.ca, byte[], int):boolean
      com.uc.c.f.a(int, int, int):byte[]
      com.uc.c.f.a(byte[], com.uc.c.ca, int):char[]
      com.uc.c.f.a(int, int, com.uc.c.ca):void
      com.uc.c.f.a(com.uc.c.bk, com.uc.c.ca, int):void
      com.uc.c.f.a(com.uc.c.ca, byte[], boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(int[], int, int, int, int, boolean):void
     arg types: [int[], int, int, int, int, int]
     candidates:
      com.uc.c.bk.a(int, int, int, int, int, boolean):void
      com.uc.c.bk.a(b.a.a.a.f, int, int, int, int, boolean):void
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int):void
      com.uc.c.bk.a(char[], int, int, int, int, int):void
      com.uc.c.bk.a(int[], int, int, int, int, boolean):void */
    private void a(bk bkVar, byte[] bArr, ca caVar, int i) {
        String str;
        int b2 = b(bArr);
        int c = c(bArr);
        int d = d(bArr);
        int e = e(bArr);
        boolean a2 = caVar.p.a(bkVar.left + b2, bkVar.top + c, d, e, 25, a(caVar, bArr, false), bkVar.bkB.bmb);
        bkVar.save();
        int[] iArr = az.BI() ? new int[]{2171691, 3487552, 5001050, 4935257, 4803929, 4738136, 3487809, 6185321, 5066844, 5399395} : new int[]{16777215, 9869722, 15592941, 15198183, 14408667, 14606046, 16777215, 4473924, 5855577, 16119285};
        int i2 = b2 + d;
        int i3 = i2 - 25;
        int i4 = ((i2 - i3) >> 1) + i3;
        int i5 = (((c + e) - c) >> 1) + c;
        boolean LK = caVar.LK();
        if (!a2) {
            bkVar.setColor(iArr[0]);
            bkVar.o(b2 + 1, c + 1, d - 2, e - 2);
            bkVar.a(new int[]{iArr[2], iArr[3], iArr[4], iArr[5]}, i3 + 1, c + 1, 23, e - 2, true);
            bkVar.setColor(iArr[1]);
            bkVar.q(b2, c, d - 1, e - 1);
            bkVar.m(i3, c, i3, (c + e) - 1);
        }
        if (!LK && !a2) {
            bkVar.setColor(iArr[7]);
            int i6 = (((i2 - i3) - 3) / 2) + i3 + 2;
            int i7 = ((e - 6) / 2) + c + 4;
            bkVar.f(i6, i7 + 3, i6 - 6, i7 - 3, i6 + 6, i7 - 3);
        }
        String a3 = caVar.bGd.a((Vector) caVar.bHd.get(Integer.valueOf(this.cJ)), caVar.bGd.n(bArr, this.cJ), bArr);
        int g = g(bArr, 1);
        if (g == -1) {
            g = caVar.bFQ;
        }
        int g2 = g(bArr, 29);
        if (g2 == -1) {
            g2 = 0;
        }
        k I = e.I(g2, g);
        if (a3 != null && a3.length() > 0) {
            int n = I.n(a3, (i3 - b2) - (5 << 1));
            if (n != a3.length()) {
                if (n - 1 > 0) {
                    n--;
                }
                str = bc.Z(a3.substring(0, n), "..");
            } else {
                str = a3;
            }
            bkVar.d(I);
            bkVar.gV(iArr[8]);
            if (LK) {
                bkVar.a(str, b2 + 5, c, (i3 - b2) - (5 << 1), e, 1, 2, true, 0, e.I(g2, g));
            } else {
                bkVar.b(str, b2 + 5, c, (i3 - b2) - (5 << 1), e, 1, 2);
            }
        }
        bkVar.reset();
    }

    private void a(bk bkVar, byte[] bArr, ca caVar, int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        bkVar.Ds();
        int b2 = b(bArr, ca.bLe, 1);
        byte[][] bArr2 = b2 == -1 ? null : (byte[][]) ((Vector) caVar.bFV.get(Integer.valueOf(this.cJ))).elementAt(b2);
        if (bArr2 == null) {
            int b3 = b(bArr);
            int c = c(bArr);
            int d = d(bArr);
            int e = e(bArr);
            if (bc.c(b3, c, d, e, i, i2, i3, i4)) {
                i7 = e;
                i6 = d;
                i5 = c;
                i8 = b3;
            } else {
                return;
            }
        } else if (bArr2.length <= 0 || (b(bArr2[0], caVar) <= i2 + i4 && b(bArr2[bArr2.length - 1], caVar) + d(bArr2[bArr2.length - 1], caVar) >= i2)) {
            i7 = 0;
            i6 = 0;
            i5 = 0;
            i8 = 0;
        } else {
            return;
        }
        try {
            synchronized (cK) {
                f(bArr);
                i9 = cK[2];
                i10 = cK[3];
                i11 = cK[1];
                i12 = cK[29];
                i13 = cK[0];
            }
            char[] a2 = a(bArr, caVar, this.cJ);
            if (bc.aL(caVar.bHz, 1)) {
                byte b4 = (byte) (caVar.bHz >> 1);
                if (b4 == 1) {
                    i14 = j.eR[3];
                } else if (b4 == 2) {
                    i14 = j.eR[5];
                } else {
                    if (i9 == -1 || az.BI()) {
                        i14 = j.eR[2];
                    }
                    i14 = i9;
                }
            } else {
                if (i9 == -1 || az.BI()) {
                    i14 = j.eR[1];
                }
                i14 = i9;
            }
            bkVar.a(e.I(i12 == -1 ? 0 : i12, i11 == -1 ? caVar.bFQ : i11), false);
            if (bArr2 == null) {
                if (i10 != -1) {
                    bkVar.ha(i10);
                    bkVar.n(i8, i5, i6, i7);
                }
                int uQ = caVar.bFM == 1 ? i5 - bkVar.DA().uQ() : i5;
                bkVar.gV(i14);
                bkVar.a(a2, 0, a2.length, i8, uQ, 20);
                if (i13 == 3) {
                    bkVar.m(i8, (i7 >> 1) + i5, (i8 + i6) - 1, (i7 >> 1) + i5);
                }
            } else {
                int i15 = 0;
                int i16 = 0;
                while (i15 < bArr2.length) {
                    int F = ca.F(bArr2[i15], 10);
                    int a3 = a(bArr2[i15], caVar);
                    int b5 = b(bArr2[i15], caVar);
                    int c2 = c(bArr2[i15], caVar);
                    int d2 = d(bArr2[i15], caVar);
                    int uQ2 = caVar.bFM == 1 ? b5 - bkVar.DA().uQ() : b5;
                    if (bc.c(a3, b5, c2, d2, i, i2, i3, i4)) {
                        if (i10 != -1) {
                            bkVar.ha(i10);
                            bkVar.n(a3, b5, c2, d2);
                        }
                        bkVar.gV(i14);
                        bkVar.a(a2, i16, F - i16, a3, uQ2, 20);
                        if (i13 == 3) {
                            bkVar.m(a3, (d2 >> 1) + b5, (c2 + a3) - 1, b5 + (d2 >> 1));
                        }
                    }
                    i15++;
                    i16 = F;
                }
            }
        } catch (Exception e2) {
        } finally {
            bkVar.Dt();
        }
    }

    private void a(bk bkVar, byte[] bArr, ca caVar, int i, int i2, int i3, int i4, int i5, int i6) {
        if (bArr[0] == 14) {
            byte b2 = (byte) b(bArr, ca.bLn, 3);
            if (b2 != -1) {
                caVar.bHz = b2;
            }
            byte b3 = (byte) (caVar.bHz << 1);
            caVar.bHz = b3;
            caVar.bHz = (byte) bc.aK(b3, 1);
        } else if (bArr[0] == 15) {
            caVar.bHz = 0;
        }
        if (ca.iV(bArr[0]) && ca.aC(bArr)) {
            int i7 = 0;
            int i8 = 0;
            int i9 = 0;
            int i10 = 0;
            if (bArr[0] != 0) {
                i7 = b(bArr);
                i8 = c(bArr);
                i9 = d(bArr);
                i10 = e(bArr);
            }
            if (i7 != 4095 || i8 != 1048575) {
                if (bArr[0] == 0 || bc.c(i7, i8, i9, i10, i2, i3, i4, i5)) {
                    switch (bArr[0]) {
                        case 0:
                            a(bkVar, bArr, caVar, i2, i3, i4, i5);
                            return;
                        case 3:
                            b(bkVar, bArr, caVar, i6);
                            return;
                        case 4:
                            e(bkVar, bArr, caVar);
                            return;
                        case 5:
                            f(bkVar, bArr, caVar);
                            return;
                        case 6:
                            d(bkVar, bArr, caVar);
                            return;
                        case 7:
                            c(bkVar, bArr, caVar);
                            return;
                        case 9:
                            a(bkVar, bArr, caVar, i);
                            return;
                        case 12:
                            h(bkVar, bArr, caVar);
                            return;
                        case 13:
                            d(bkVar, bArr, caVar, i6);
                            return;
                        case 17:
                            b(bkVar, bArr, caVar, i6);
                            return;
                        case 22:
                            g(bkVar, bArr, caVar);
                            return;
                        case 28:
                            i(bkVar, bArr, caVar);
                            return;
                        case 44:
                            a(bkVar, bArr, caVar);
                            return;
                        case 45:
                            b(bkVar, bArr, caVar);
                            return;
                        case 62:
                            int b4 = b(bArr, r.Eu, 0);
                            a(bkVar, bArr, caVar, ((Vector) caVar.bGl.get(Integer.valueOf(caVar.bS()))).elementAt(b4), b4);
                            return;
                        default:
                            r.e(bkVar, bArr, caVar, this.cJ);
                            return;
                    }
                }
            }
        }
    }

    public static void a(byte[] bArr, int i, int i2) {
        a(bArr, 1, i, i2);
    }

    public static void a(byte[] bArr, int i, int i2, int i3) {
        ca.r(bArr, i, (i2 << 20) | (1048575 & i3));
    }

    public static void a(byte[] bArr, int i, int i2, int i3, int i4, int i5) {
        a(bArr, i, i2, i3);
        a(bArr, i + 4, i4, i5);
    }

    public static void a(byte[] bArr, byte[] bArr2, int i, int i2, byte[] bArr3) {
        int F = ca.F(bArr, 10) + 12;
        for (int i3 = 0; i3 < bArr2.length; i3++) {
            switch (bArr2[i3]) {
                case 1:
                    if (i3 != i) {
                        F++;
                        break;
                    } else {
                        bArr[F] = (byte) i2;
                        return;
                    }
                case 2:
                    if (i3 != i) {
                        F += 2;
                        break;
                    } else {
                        ca.b(bArr, F, (short) i2);
                        return;
                    }
                case 5:
                    F += bc.x(bArr, F) + 2;
                    break;
            }
        }
    }

    public static final void a(byte[] bArr, byte[] bArr2, int i, StringBuffer stringBuffer) {
        int b2 = b(bArr, bArr2, i);
        bc.a(bArr, b2 + 2, bc.x(bArr, b2), stringBuffer);
    }

    private boolean a(bk bkVar, byte[] bArr, ca caVar, int i, int i2, int i3, int i4, int i5) {
        return b(bkVar, bArr, caVar, i, i2, i3, i4, i5, -1);
    }

    public static boolean a(ca caVar, byte[] bArr, int i) {
        int b2 = b(bArr, ca.bLe, 1);
        Vector vector = (Vector) caVar.bFV.get(Integer.valueOf(i));
        int size = caVar.bHj.size();
        if (size == 1) {
            System.arraycopy((byte[]) caVar.bHj.elementAt(0), 1, bArr, 1, 8);
            if (b2 != -1) {
                vector.setElementAt(null, b2);
            }
        } else {
            byte[][] bArr2 = new byte[caVar.bHj.size()][];
            for (int i2 = 0; i2 < size; i2++) {
                bArr2[i2] = (byte[]) caVar.bHj.elementAt(i2);
            }
            if (b2 == -1) {
                a(bArr, ca.bLe, 1, vector.size(), (byte[]) null);
                vector.addElement(bArr2);
            } else {
                vector.setElementAt(null, b2);
                vector.setElementAt(bArr2, b2);
            }
        }
        return size > 1;
    }

    public static boolean a(ca caVar, byte[] bArr, boolean z) {
        if (caVar == null || bArr == null) {
            return false;
        }
        Vector KP = caVar.KP();
        if (KP == null || !KP.contains(bArr)) {
            return false;
        }
        return !z || KP.size() <= 1;
    }

    public static byte[] a(int i, int i2, int i3) {
        byte[] bArr = new byte[12];
        bArr[0] = ca.bKP;
        ca.b(bArr, 10, (short) i);
        a(bArr, 5, i2, i3);
        return bArr;
    }

    public static char[] a(byte[] bArr, ca caVar, int i) {
        int b2 = b(bArr, ca.bLe, 0);
        if (b2 == -1 || b2 > ((Vector) caVar.bFT.get(Integer.valueOf(i))).size() - 1) {
            return null;
        }
        return (char[]) ((Vector) caVar.bFT.get(Integer.valueOf(i))).elementAt(b2);
    }

    private void addElement(Object obj) {
        if (this.cE.length <= this.cF) {
            Object[] objArr = new Object[(this.cE.length + (this.cF << 1))];
            System.arraycopy(this.cE, 0, objArr, 0, this.cF);
            this.cE = null;
            this.cE = objArr;
        }
        Object[] objArr2 = this.cE;
        short s = this.cF;
        this.cF = (short) (s + 1);
        objArr2[s] = obj;
    }

    public static int b(byte[] bArr) {
        return c(bArr, 1);
    }

    private int b(byte[] bArr, ca caVar) {
        return c(bArr);
    }

    public static int b(byte[] bArr, byte[] bArr2, int i) {
        int F = ca.F(bArr, 10) + 12;
        for (int i2 = 0; i2 < bArr2.length; i2++) {
            switch (bArr2[i2]) {
                case 1:
                    if (i2 != i) {
                        F++;
                        break;
                    } else {
                        return bArr[F];
                    }
                case 2:
                    if (i2 != i) {
                        F += 2;
                        break;
                    } else {
                        return (short) ca.F(bArr, F);
                    }
                case 4:
                    if (i2 != i) {
                        F += 4;
                        break;
                    } else {
                        return ca.G(bArr, F);
                    }
                case 5:
                    if (i2 != i) {
                        F += ca.F(bArr, F) + 2;
                        break;
                    } else {
                        return F;
                    }
            }
        }
        return -1;
    }

    private void b(bk bkVar, ca caVar, int i, int i2, int i3, int i4, int i5) {
        int i6 = 0;
        while (true) {
            int i7 = i6;
            if (i7 < this.cF) {
                if (this.cE[i7] instanceof byte[]) {
                    a(bkVar, (byte[]) this.cE[i7], caVar, i7, i, i2, i3, i4, i5);
                } else {
                    ((f) this.cE[i7]).a(bkVar, caVar, i, i2, i3, i4, i5);
                }
                i6 = i7 + 1;
            } else {
                return;
            }
        }
    }

    private void b(bk bkVar, byte[] bArr, ca caVar) {
        int i;
        int i2;
        if (bArr != null) {
            int b2 = b(bArr);
            int c = c(bArr);
            int d = d(bArr);
            int e = e(bArr);
            if (caVar.bFN == 2) {
                int i3 = b2 - ca.bIb;
                i = d + ca.bIb + ca.bIc;
                i2 = i3;
            } else {
                int i4 = b2 - ca.bIe;
                i = d + ca.bIe + ca.bIf;
                i2 = i4;
            }
            if (!caVar.p.a(bkVar.left + i2, bkVar.top + c, i, e, bkVar.bkB.bmb)) {
                Canvas canvas = bkVar.bkB.bmb;
                Paint paint = bkVar.bkB.adm;
                paint.setColor(-16777216);
                int i5 = bkVar.left + i2;
                int i6 = bkVar.top + c;
                canvas.drawRect(new Rect(i5, i6, i + i5, e + i6), paint);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:110:0x0241  */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x0246  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x024c  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x0267  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x026b  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0163 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0205 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(com.uc.c.bk r28, byte[] r29, com.uc.c.ca r30, int r31) {
        /*
            r27 = this;
            r28.save()
            r0 = r27
            r1 = r29
            r2 = r30
            int r15 = r0.a(r1, r2)
            r0 = r27
            r1 = r29
            r2 = r30
            int r16 = r0.b(r1, r2)
            r0 = r27
            r1 = r29
            r2 = r30
            int r17 = r0.c(r1, r2)
            r0 = r27
            r1 = r29
            r2 = r30
            int r18 = r0.d(r1, r2)
            boolean r19 = r30.LK()
            r20 = 0
            r5 = 1
            boolean[] r13 = new boolean[r5]
            r5 = 0
            r21 = 0
            r6 = 0
            byte r6 = r29[r6]
            r7 = 3
            if (r6 != r7) goto L_0x01a7
            byte[] r5 = com.uc.c.ca.bLh
            r6 = 0
            r0 = r29
            r1 = r5
            r2 = r6
            int r5 = b(r0, r1, r2)
            r9 = r5
        L_0x0049:
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r30
            byte r0 = r0.bFM
            r8 = r0
            r10 = 1
            if (r8 != r10) goto L_0x03b6
            r6 = 14
            r0 = r29
            r1 = r6
            int r6 = g(r0, r1)
            r7 = 15
            r0 = r29
            r1 = r7
            int r7 = g(r0, r1)
            if (r6 <= 0) goto L_0x03b6
            if (r7 <= 0) goto L_0x03b6
            r5 = 1
            boolean r8 = r30.LK()
            if (r8 == 0) goto L_0x03b6
            r0 = r30
            r1 = r6
            int r6 = r0.jl(r1)
            r8 = 1
            int r6 = java.lang.Math.max(r6, r8)
            r0 = r30
            r1 = r7
            int r7 = r0.jl(r1)
            r8 = 1
            int r7 = java.lang.Math.max(r7, r8)
            r22 = r7
            r23 = r6
            r24 = r5
        L_0x008f:
            r0 = r30
            java.util.HashMap r0 = r0.bGh
            r5 = r0
            if (r5 == 0) goto L_0x03ae
            r0 = r30
            java.util.HashMap r0 = r0.bGh
            r5 = r0
            r0 = r27
            int r0 = r0.cJ
            r6 = r0
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            java.lang.Object r31 = r5.get(r6)
            java.util.Vector r31 = (java.util.Vector) r31
            if (r9 < 0) goto L_0x03ae
            if (r31 == 0) goto L_0x03ae
            int r5 = r31.size()
            if (r9 >= r5) goto L_0x03ae
            r12 = 0
            r5 = 2
            int[] r6 = new int[r5]     // Catch:{ Exception -> 0x01f4 }
            r5 = 0
            if (r24 == 0) goto L_0x01bc
            r7 = r23
        L_0x00bd:
            r6[r5] = r7     // Catch:{ Exception -> 0x01f4 }
            r5 = 1
            if (r24 == 0) goto L_0x01c0
            r7 = r22
        L_0x00c4:
            r6[r5] = r7     // Catch:{ Exception -> 0x01f4 }
            r0 = r31
            r1 = r9
            java.lang.Object r5 = r0.elementAt(r1)     // Catch:{ Exception -> 0x01f4 }
            if (r5 == 0) goto L_0x01c4
            r0 = r31
            r1 = r9
            java.lang.Object r31 = r0.elementAt(r1)     // Catch:{ Exception -> 0x01f4 }
            byte[] r31 = (byte[]) r31     // Catch:{ Exception -> 0x01f4 }
            byte[] r31 = (byte[]) r31     // Catch:{ Exception -> 0x01f4 }
            r5 = 0
            r5 = r6[r5]     // Catch:{ Exception -> 0x01f4 }
            r7 = 1
            r7 = r6[r7]     // Catch:{ Exception -> 0x01f4 }
            r0 = r31
            r1 = r9
            r2 = r5
            r3 = r7
            java.lang.Long r5 = com.uc.c.bc.b(r0, r1, r2, r3)     // Catch:{ Exception -> 0x01f4 }
            r11 = r5
        L_0x00ea:
            com.uc.c.w r5 = com.uc.c.w.ns()     // Catch:{ Exception -> 0x01f4 }
            r0 = r27
            r1 = r29
            int r8 = r0.indexOf(r1)     // Catch:{ Exception -> 0x01f4 }
            if (r24 != 0) goto L_0x00fa
            if (r19 == 0) goto L_0x01c8
        L_0x00fa:
            r10 = r6
        L_0x00fb:
            r0 = r27
            int r0 = r0.cJ     // Catch:{ Exception -> 0x01f4 }
            r14 = r0
            r6 = r30
            r7 = r27
            b.a.a.a.f r5 = r5.a(r6, r7, r8, r9, r10, r11, r12, r13, r14)     // Catch:{ Exception -> 0x01f4 }
            r6 = 0
            r7 = 0
            if (r5 == 0) goto L_0x03a7
            int r6 = r5.getWidth()     // Catch:{ Exception -> 0x0375 }
            int r7 = r5.getHeight()     // Catch:{ Exception -> 0x0375 }
            r25 = r7
            r7 = r6
            r6 = r25
        L_0x0119:
            if (r5 == 0) goto L_0x03a3
            r0 = r6
            r1 = r18
            if (r0 != r1) goto L_0x03a3
            r0 = r7
            r1 = r17
            if (r0 != r1) goto L_0x03a3
            r6 = 1
        L_0x0126:
            if (r5 == 0) goto L_0x01e6
            boolean r7 = r5.FT     // Catch:{ Exception -> 0x037f }
            if (r7 != 0) goto L_0x01e6
            byte[] r7 = com.uc.c.ca.bLh     // Catch:{ Exception -> 0x037f }
            r8 = 3
            r0 = r29
            r1 = r7
            r2 = r8
            int r7 = b(r0, r1, r2)     // Catch:{ Exception -> 0x037f }
            byte r7 = (byte) r7     // Catch:{ Exception -> 0x037f }
            r8 = 1
            if (r7 != r8) goto L_0x01cc
            r6 = 1
            r0 = r27
            r1 = r29
            r2 = r30
            int r7 = r0.c(r1, r2)     // Catch:{ Exception -> 0x037f }
            r0 = r27
            r1 = r29
            r2 = r30
            int r8 = r0.d(r1, r2)     // Catch:{ Exception -> 0x038b }
            r5.K(r7, r8)     // Catch:{ Exception -> 0x0396 }
            r25 = r8
            r8 = r7
            r7 = r25
        L_0x0158:
            r12 = r5
            r14 = r7
            r17 = r8
            r5 = r6
        L_0x015d:
            boolean r18 = com.uc.c.az.BI()
            if (r12 == 0) goto L_0x0203
            if (r17 <= 0) goto L_0x0203
            if (r14 <= 0) goto L_0x0203
            b.a.a.a.f r6 = com.uc.c.bc.beI
            if (r12 == r6) goto L_0x0203
            r0 = r30
            byte r0 = r0.bFM
            r6 = r0
            r7 = 1
            if (r6 != r7) goto L_0x0203
            r5 = r27
            r6 = r28
            r7 = r15
            r8 = r16
            r9 = r24
            r10 = r23
            r11 = r22
            r13 = r17
            r5.a(r6, r7, r8, r9, r10, r11, r12, r13, r14)
            if (r18 == 0) goto L_0x01a3
            boolean r5 = r30.ac()
            if (r5 != 0) goto L_0x01a3
            int[] r5 = com.uc.c.j.eR
            r6 = 6
            r5 = r5[r6]
            r0 = r28
            r1 = r5
            r0.gW(r1)
            r0 = r28
            r1 = r15
            r2 = r16
            r3 = r17
            r4 = r14
            r0.o(r1, r2, r3, r4)
        L_0x01a3:
            r28.reset()
            return
        L_0x01a7:
            r6 = 0
            byte r6 = r29[r6]
            r7 = 17
            if (r6 != r7) goto L_0x03be
            byte[] r5 = com.uc.c.ca.bLw
            r6 = 0
            r0 = r29
            r1 = r5
            r2 = r6
            int r5 = b(r0, r1, r2)
            r9 = r5
            goto L_0x0049
        L_0x01bc:
            r7 = r17
            goto L_0x00bd
        L_0x01c0:
            r7 = r18
            goto L_0x00c4
        L_0x01c4:
            r5 = 0
            r11 = r5
            goto L_0x00ea
        L_0x01c8:
            r6 = 0
            r10 = r6
            goto L_0x00fb
        L_0x01cc:
            r7 = 1
            r0 = r24
            r1 = r7
            if (r0 != r1) goto L_0x01e0
            boolean r7 = r30.ac()     // Catch:{ Exception -> 0x037f }
            if (r7 != 0) goto L_0x01e0
            r0 = r5
            r1 = r23
            r2 = r22
            r0.K(r1, r2)     // Catch:{ Exception -> 0x037f }
        L_0x01e0:
            r7 = r18
            r8 = r17
            goto L_0x0158
        L_0x01e6:
            if (r5 == 0) goto L_0x01e0
            boolean r7 = r5.FT     // Catch:{ Exception -> 0x037f }
            r8 = 1
            if (r7 != r8) goto L_0x01e0
            r6 = 1
            r7 = r18
            r8 = r17
            goto L_0x0158
        L_0x01f4:
            r5 = move-exception
            r5 = r21
            r6 = r20
            r7 = r18
            r8 = r17
        L_0x01fd:
            r12 = r6
            r14 = r7
            r17 = r8
            goto L_0x015d
        L_0x0203:
            if (r12 == 0) goto L_0x023b
            if (r17 <= 0) goto L_0x023b
            if (r14 <= 0) goto L_0x023b
            b.a.a.a.f r6 = com.uc.c.bc.beI
            if (r12 == r6) goto L_0x023b
            if (r5 == 0) goto L_0x023b
            r5 = 20
            r0 = r28
            r1 = r12
            r2 = r15
            r3 = r16
            r4 = r5
            r0.a(r1, r2, r3, r4)
            if (r18 == 0) goto L_0x01a3
            boolean r5 = r30.ac()
            if (r5 != 0) goto L_0x01a3
            int[] r5 = com.uc.c.j.eR
            r6 = 6
            r5 = r5[r6]
            r0 = r28
            r1 = r5
            r0.gW(r1)
            r0 = r28
            r1 = r15
            r2 = r16
            r3 = r17
            r4 = r14
            r0.o(r1, r2, r3, r4)
            goto L_0x01a3
        L_0x023b:
            if (r17 <= 0) goto L_0x01a3
            if (r14 <= 0) goto L_0x01a3
            if (r18 == 0) goto L_0x0267
            r5 = 2566191(0x27282f, float:3.596E-39)
        L_0x0244:
            if (r18 == 0) goto L_0x026b
            r6 = 3033970(0x2e4b72, float:4.251498E-39)
            r7 = r6
        L_0x024a:
            if (r19 != 0) goto L_0x0251
            r6 = 0
            boolean r6 = r13[r6]
            if (r6 == 0) goto L_0x026e
        L_0x0251:
            r5 = 14474460(0xdcdcdc, float:2.0283039E-38)
            r0 = r28
            r1 = r5
            r0.setColor(r1)
            r0 = r28
            r1 = r15
            r2 = r16
            r3 = r17
            r4 = r14
            r0.o(r1, r2, r3, r4)
            goto L_0x01a3
        L_0x0267:
            r5 = 12369084(0xbcbcbc, float:1.7332778E-38)
            goto L_0x0244
        L_0x026b:
            r6 = 0
            r7 = r6
            goto L_0x024a
        L_0x026e:
            r0 = r30
            java.util.HashMap r0 = r0.bGf
            r6 = r0
            r0 = r27
            int r0 = r0.cJ
            r8 = r0
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            java.lang.Object r27 = r6.get(r8)
            java.util.Vector r27 = (java.util.Vector) r27
            r0 = r27
            r1 = r9
            java.lang.Object r6 = r0.elementAt(r1)
            if (r6 == 0) goto L_0x01a3
            r0 = r30
            int r0 = r0.bFP
            r6 = r0
            r8 = 32
            boolean r6 = com.uc.c.bc.aL(r6, r8)
            if (r6 != 0) goto L_0x029c
            byte r6 = com.uc.c.az.bbW
            if (r6 != 0) goto L_0x0348
        L_0x029c:
            com.uc.c.w r6 = com.uc.c.ar.mR
            if (r18 == 0) goto L_0x0344
            r8 = 116(0x74, float:1.63E-43)
        L_0x02a2:
            b.a.a.a.f r6 = r6.bJ(r8)
            r8 = r6
        L_0x02a7:
            r0 = r28
            r1 = r5
            r0.setColor(r1)
            r0 = r28
            r1 = r15
            r2 = r16
            r3 = r17
            r4 = r14
            r0.r(r1, r2, r3, r4)
            r28.Du()
            int r5 = r15 + 1
            int r6 = r16 + 1
            r9 = 1
            int r9 = r17 - r9
            r10 = 1
            int r10 = r14 - r10
            r0 = r28
            r1 = r5
            r2 = r6
            r3 = r9
            r4 = r10
            r0.l(r1, r2, r3, r4)
            int r5 = r15 + 1
            int r6 = r16 + 1
            r9 = 20
            r0 = r28
            r1 = r8
            r2 = r5
            r3 = r6
            r4 = r9
            r0.a(r1, r2, r3, r4)
            if (r18 == 0) goto L_0x02fb
            boolean r5 = r30.ac()
            if (r5 != 0) goto L_0x02fb
            int[] r5 = com.uc.c.j.eR
            r6 = 6
            r5 = r5[r6]
            r0 = r28
            r1 = r5
            r0.gW(r1)
            r0 = r28
            r1 = r15
            r2 = r16
            r3 = r17
            r4 = r14
            r0.o(r1, r2, r3, r4)
        L_0x02fb:
            r5 = 0
            r6 = 0
            byte r6 = r29[r6]     // Catch:{ Exception -> 0x0372 }
            r9 = 3
            if (r6 != r9) goto L_0x0358
            byte[] r6 = com.uc.c.ca.bLh     // Catch:{ Exception -> 0x0372 }
            r9 = 1
            r0 = r29
            r1 = r6
            r2 = r9
            int r6 = b(r0, r1, r2)     // Catch:{ Exception -> 0x0372 }
            r0 = r29
            r1 = r6
            java.lang.String r5 = com.uc.c.bc.w(r0, r1)     // Catch:{ Exception -> 0x0372 }
        L_0x0314:
            r6 = r5
        L_0x0315:
            boolean r5 = com.uc.c.bc.dM(r6)
            if (r5 == 0) goto L_0x033f
            r0 = r28
            r1 = r7
            r0.setColor(r1)
            int r5 = b.a.a.a.k.adr
            r0 = r28
            r1 = r5
            r0.gZ(r1)
            int r5 = r15 + 2
            int r7 = r8.getWidth()
            int r7 = r7 + r5
            int r8 = r16 + 1
            r5 = 3
            int r9 = r17 - r5
            r5 = 2
            int r10 = r14 - r5
            r11 = 1
            r12 = 1
            r5 = r28
            r5.b(r6, r7, r8, r9, r10, r11, r12)
        L_0x033f:
            r28.Dv()
            goto L_0x01a3
        L_0x0344:
            r8 = 31
            goto L_0x02a2
        L_0x0348:
            com.uc.c.w r6 = com.uc.c.ar.mR
            if (r18 == 0) goto L_0x0355
            r8 = 117(0x75, float:1.64E-43)
        L_0x034e:
            b.a.a.a.f r6 = r6.bJ(r8)
            r8 = r6
            goto L_0x02a7
        L_0x0355:
            r8 = 32
            goto L_0x034e
        L_0x0358:
            r6 = 0
            byte r6 = r29[r6]     // Catch:{ Exception -> 0x0372 }
            r9 = 17
            if (r6 != r9) goto L_0x0314
            byte[] r6 = com.uc.c.ca.bLw     // Catch:{ Exception -> 0x0372 }
            r9 = 1
            r0 = r29
            r1 = r6
            r2 = r9
            int r6 = b(r0, r1, r2)     // Catch:{ Exception -> 0x0372 }
            r0 = r29
            r1 = r6
            java.lang.String r5 = com.uc.c.bc.w(r0, r1)     // Catch:{ Exception -> 0x0372 }
            goto L_0x0314
        L_0x0372:
            r6 = move-exception
            r6 = r5
            goto L_0x0315
        L_0x0375:
            r6 = move-exception
            r6 = r5
            r7 = r18
            r8 = r17
            r5 = r21
            goto L_0x01fd
        L_0x037f:
            r7 = move-exception
            r7 = r18
            r8 = r17
            r25 = r6
            r6 = r5
            r5 = r25
            goto L_0x01fd
        L_0x038b:
            r8 = move-exception
            r8 = r7
            r7 = r18
            r25 = r5
            r5 = r6
            r6 = r25
            goto L_0x01fd
        L_0x0396:
            r10 = move-exception
            r25 = r6
            r6 = r5
            r5 = r25
            r26 = r8
            r8 = r7
            r7 = r26
            goto L_0x01fd
        L_0x03a3:
            r6 = r21
            goto L_0x0126
        L_0x03a7:
            r25 = r7
            r7 = r6
            r6 = r25
            goto L_0x0119
        L_0x03ae:
            r5 = r21
            r12 = r20
            r14 = r18
            goto L_0x015d
        L_0x03b6:
            r22 = r7
            r23 = r6
            r24 = r5
            goto L_0x008f
        L_0x03be:
            r9 = r5
            goto L_0x0049
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.f.b(com.uc.c.bk, byte[], com.uc.c.ca, int):void");
    }

    public static void b(byte[] bArr, int i) {
        if (9 < bArr.length) {
            bArr[9] = (byte) i;
        } else if (bArr == ca.bKO || bArr == ca.bKN) {
            bArr[1] = (byte) i;
        }
    }

    public static void b(byte[] bArr, int i, int i2) {
        a(bArr, 5, i, i2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:86:0x020d, code lost:
        if (r6 == 2) goto L_0x01e0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x020f, code lost:
        r11 = r11 + r8;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b(com.uc.c.bk r19, byte[] r20, com.uc.c.ca r21, int r22, int r23, int r24, int r25, int r26, int r27) {
        /*
            r18 = this;
            r5 = 4
            r0 = r20
            r1 = r5
            int r9 = f(r0, r1)
            r5 = -1
            if (r9 == r5) goto L_0x021d
            r12 = 0
            r0 = r21
            java.util.HashMap r0 = r0.bGh     // Catch:{ Exception -> 0x021c }
            r5 = r0
            r0 = r18
            int r0 = r0.cJ     // Catch:{ Exception -> 0x021c }
            r6 = r0
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x021c }
            java.lang.Object r26 = r5.get(r6)     // Catch:{ Exception -> 0x021c }
            java.util.Vector r26 = (java.util.Vector) r26     // Catch:{ Exception -> 0x021c }
            r0 = r26
            r1 = r9
            java.lang.Object r26 = r0.elementAt(r1)     // Catch:{ Exception -> 0x021c }
            byte[] r26 = (byte[]) r26     // Catch:{ Exception -> 0x021c }
            byte[] r26 = (byte[]) r26     // Catch:{ Exception -> 0x021c }
            int[] r15 = com.uc.c.bc.ao(r26)     // Catch:{ Exception -> 0x021c }
            r0 = r21
            float r0 = r0.bGy     // Catch:{ Exception -> 0x021c }
            r5 = r0
            r6 = 1065353216(0x3f800000, float:1.0)
            int r5 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r5 > 0) goto L_0x0045
            r0 = r21
            float r0 = r0.bGA     // Catch:{ Exception -> 0x021c }
            r5 = r0
            r6 = 1065353216(0x3f800000, float:1.0)
            int r5 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r5 <= 0) goto L_0x011a
        L_0x0045:
            r5 = 1
        L_0x0046:
            r6 = 4
            r0 = r27
            r1 = r6
            if (r0 != r1) goto L_0x011d
            r5 = 0
            r6 = 0
            r6 = r15[r6]     // Catch:{ Exception -> 0x021c }
            float r6 = (float) r6     // Catch:{ Exception -> 0x021c }
            r0 = r21
            r1 = r6
            float r6 = r0.k(r1)     // Catch:{ Exception -> 0x021c }
            int r6 = (int) r6     // Catch:{ Exception -> 0x021c }
            r15[r5] = r6     // Catch:{ Exception -> 0x021c }
            r5 = 1
            r6 = 1
            r6 = r15[r6]     // Catch:{ Exception -> 0x021c }
            float r6 = (float) r6     // Catch:{ Exception -> 0x021c }
            r0 = r21
            r1 = r6
            float r6 = r0.k(r1)     // Catch:{ Exception -> 0x021c }
            int r6 = (int) r6     // Catch:{ Exception -> 0x021c }
            r15[r5] = r6     // Catch:{ Exception -> 0x021c }
            r5 = 1
            r16 = r5
        L_0x006d:
            r5 = 0
            r0 = r21
            java.util.HashMap r0 = r0.bGh     // Catch:{ Exception -> 0x021c }
            r6 = r0
            r0 = r18
            int r0 = r0.cJ     // Catch:{ Exception -> 0x021c }
            r7 = r0
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x021c }
            java.lang.Object r26 = r6.get(r7)     // Catch:{ Exception -> 0x021c }
            java.util.Vector r26 = (java.util.Vector) r26     // Catch:{ Exception -> 0x021c }
            r0 = r26
            r1 = r9
            java.lang.Object r6 = r0.elementAt(r1)     // Catch:{ Exception -> 0x021c }
            if (r6 == 0) goto L_0x022e
            r0 = r21
            java.util.HashMap r0 = r0.bGh     // Catch:{ Exception -> 0x021c }
            r5 = r0
            r0 = r18
            int r0 = r0.cJ     // Catch:{ Exception -> 0x021c }
            r6 = r0
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x021c }
            java.lang.Object r26 = r5.get(r6)     // Catch:{ Exception -> 0x021c }
            java.util.Vector r26 = (java.util.Vector) r26     // Catch:{ Exception -> 0x021c }
            r0 = r26
            r1 = r9
            java.lang.Object r26 = r0.elementAt(r1)     // Catch:{ Exception -> 0x021c }
            byte[] r26 = (byte[]) r26     // Catch:{ Exception -> 0x021c }
            byte[] r26 = (byte[]) r26     // Catch:{ Exception -> 0x021c }
            r5 = 0
            r5 = r15[r5]     // Catch:{ Exception -> 0x021c }
            r6 = 1
            r6 = r15[r6]     // Catch:{ Exception -> 0x021c }
            r0 = r26
            r1 = r9
            r2 = r5
            r3 = r6
            java.lang.Long r5 = com.uc.c.bc.b(r0, r1, r2, r3)     // Catch:{ Exception -> 0x021c }
            r11 = r5
        L_0x00ba:
            com.uc.c.w r5 = com.uc.c.w.ns()     // Catch:{ Exception -> 0x021c }
            r0 = r18
            com.uc.c.f r0 = r0.cG     // Catch:{ Exception -> 0x021c }
            r7 = r0
            r0 = r18
            com.uc.c.f r0 = r0.cG     // Catch:{ Exception -> 0x021c }
            r6 = r0
            r0 = r6
            r1 = r18
            int r8 = r0.indexOf(r1)     // Catch:{ Exception -> 0x021c }
            if (r16 == 0) goto L_0x0174
            r10 = r15
        L_0x00d2:
            r6 = 1
            boolean[] r13 = new boolean[r6]     // Catch:{ Exception -> 0x021c }
            r0 = r18
            int r0 = r0.cJ     // Catch:{ Exception -> 0x021c }
            r14 = r0
            r6 = r21
            b.a.a.a.f r5 = r5.a(r6, r7, r8, r9, r10, r11, r12, r13, r14)     // Catch:{ Exception -> 0x021c }
            b.a.a.a.f r6 = com.uc.c.bc.beI     // Catch:{ Exception -> 0x021c }
            if (r5 != r6) goto L_0x00e5
            r5 = 0
        L_0x00e5:
            r6 = 5
            r0 = r20
            r1 = r6
            int r6 = f(r0, r1)     // Catch:{ Exception -> 0x021c }
            if (r5 == 0) goto L_0x021d
            r19.Du()     // Catch:{ Exception -> 0x021c }
            r0 = r19
            r1 = r22
            r2 = r23
            r3 = r24
            r4 = r25
            r0.l(r1, r2, r3, r4)     // Catch:{ Exception -> 0x021c }
            int r7 = r5.getWidth()     // Catch:{ Exception -> 0x021c }
            int r8 = r5.getHeight()     // Catch:{ Exception -> 0x021c }
            if (r16 == 0) goto L_0x0227
            r7 = 0
            r7 = r15[r7]     // Catch:{ Exception -> 0x021c }
            r8 = 1
            r8 = r15[r8]     // Catch:{ Exception -> 0x021c }
            r17 = r8
            r8 = r7
            r7 = r17
        L_0x0114:
            if (r8 <= 0) goto L_0x0118
            if (r7 > 0) goto L_0x0178
        L_0x0118:
            r5 = 0
        L_0x0119:
            return r5
        L_0x011a:
            r5 = 0
            goto L_0x0046
        L_0x011d:
            r6 = 2
            r0 = r27
            r1 = r6
            if (r0 != r1) goto L_0x0146
            r5 = 0
            r6 = 0
            r6 = r15[r6]     // Catch:{ Exception -> 0x021c }
            float r6 = (float) r6     // Catch:{ Exception -> 0x021c }
            r0 = r21
            r1 = r6
            float r6 = r0.m(r1)     // Catch:{ Exception -> 0x021c }
            int r6 = (int) r6     // Catch:{ Exception -> 0x021c }
            r15[r5] = r6     // Catch:{ Exception -> 0x021c }
            r5 = 1
            r6 = 1
            r6 = r15[r6]     // Catch:{ Exception -> 0x021c }
            float r6 = (float) r6     // Catch:{ Exception -> 0x021c }
            r0 = r21
            r1 = r6
            float r6 = r0.m(r1)     // Catch:{ Exception -> 0x021c }
            int r6 = (int) r6     // Catch:{ Exception -> 0x021c }
            r15[r5] = r6     // Catch:{ Exception -> 0x021c }
            r5 = 1
            r16 = r5
            goto L_0x006d
        L_0x0146:
            if (r5 == 0) goto L_0x016a
            r0 = r21
            float r0 = r0.bGy     // Catch:{ Exception -> 0x021c }
            r6 = r0
            r7 = 1065353216(0x3f800000, float:1.0)
            int r6 = (r6 > r7 ? 1 : (r6 == r7 ? 0 : -1))
            if (r6 <= 0) goto L_0x016e
            r0 = r21
            float r0 = r0.bGy     // Catch:{ Exception -> 0x021c }
            r6 = r0
        L_0x0158:
            r7 = 0
            r8 = 0
            r8 = r15[r8]     // Catch:{ Exception -> 0x021c }
            float r8 = (float) r8     // Catch:{ Exception -> 0x021c }
            float r8 = r8 * r6
            int r8 = (int) r8     // Catch:{ Exception -> 0x021c }
            r15[r7] = r8     // Catch:{ Exception -> 0x021c }
            r7 = 1
            r8 = 1
            r8 = r15[r8]     // Catch:{ Exception -> 0x021c }
            float r8 = (float) r8     // Catch:{ Exception -> 0x021c }
            float r6 = r6 * r8
            int r6 = (int) r6     // Catch:{ Exception -> 0x021c }
            r15[r7] = r6     // Catch:{ Exception -> 0x021c }
        L_0x016a:
            r16 = r5
            goto L_0x006d
        L_0x016e:
            r0 = r21
            float r0 = r0.bGA     // Catch:{ Exception -> 0x021c }
            r6 = r0
            goto L_0x0158
        L_0x0174:
            r6 = 0
            r10 = r6
            goto L_0x00d2
        L_0x0178:
            r9 = 6
            r0 = r20
            r1 = r9
            int r9 = f(r0, r1)     // Catch:{ Exception -> 0x021c }
            r10 = 27
            r0 = r20
            r1 = r10
            int r10 = f(r0, r1)     // Catch:{ Exception -> 0x021c }
            r11 = -1
            if (r9 == r11) goto L_0x0223
            int r11 = com.uc.c.ca.jp(r9)     // Catch:{ Exception -> 0x021c }
            int r9 = com.uc.c.ca.jq(r9)     // Catch:{ Exception -> 0x021c }
            r12 = 1
            if (r12 != r11) goto L_0x01a1
            int r11 = r24 - r8
            float r11 = (float) r11     // Catch:{ Exception -> 0x021c }
            float r9 = (float) r9     // Catch:{ Exception -> 0x021c }
            r12 = 1176256512(0x461c4000, float:10000.0)
            float r9 = r9 / r12
            float r9 = r9 * r11
            int r9 = (int) r9     // Catch:{ Exception -> 0x021c }
        L_0x01a1:
            int r9 = r9 + r22
        L_0x01a3:
            r11 = -1
            if (r10 == r11) goto L_0x0220
            int r11 = com.uc.c.ca.jp(r10)     // Catch:{ Exception -> 0x021c }
            int r10 = com.uc.c.ca.jq(r10)     // Catch:{ Exception -> 0x021c }
            r12 = 1
            if (r12 != r11) goto L_0x01bb
            int r11 = r25 - r7
            float r11 = (float) r11     // Catch:{ Exception -> 0x021c }
            float r10 = (float) r10     // Catch:{ Exception -> 0x021c }
            r12 = 1176256512(0x461c4000, float:10000.0)
            float r10 = r10 / r12
            float r10 = r10 * r11
            int r10 = (int) r10     // Catch:{ Exception -> 0x021c }
        L_0x01bb:
            int r10 = r10 + r23
        L_0x01bd:
            if (r16 == 0) goto L_0x01c8
            if (r5 == 0) goto L_0x01c8
            boolean r11 = r5.FT     // Catch:{ Exception -> 0x021c }
            if (r11 != 0) goto L_0x01c8
            r5.K(r8, r7)     // Catch:{ Exception -> 0x021c }
        L_0x01c8:
            r11 = r9
        L_0x01c9:
            int r12 = r22 + r24
            if (r11 >= r12) goto L_0x01e0
            r12 = r10
        L_0x01ce:
            int r13 = r23 + r25
            if (r12 >= r13) goto L_0x020c
            r13 = 20
            r0 = r19
            r1 = r5
            r2 = r11
            r3 = r12
            r4 = r13
            r0.a(r1, r2, r3, r4)     // Catch:{ Exception -> 0x021c }
            r13 = 3
            if (r6 != r13) goto L_0x0209
        L_0x01e0:
            r11 = 3
            if (r6 == r11) goto L_0x0203
            r11 = 2
            if (r6 != r11) goto L_0x0213
        L_0x01e6:
            int r11 = r22 - r8
            if (r9 < r11) goto L_0x0203
            r11 = 1
            if (r6 != r11) goto L_0x0215
            r11 = r10
        L_0x01ee:
            int r12 = r23 - r7
            if (r11 < r12) goto L_0x0200
            r12 = 20
            r0 = r19
            r1 = r5
            r2 = r9
            r3 = r11
            r4 = r12
            r0.a(r1, r2, r3, r4)     // Catch:{ Exception -> 0x021c }
            r12 = 1
            if (r6 != r12) goto L_0x0218
        L_0x0200:
            r11 = 2
            if (r6 != r11) goto L_0x021a
        L_0x0203:
            r19.Dv()     // Catch:{ Exception -> 0x021c }
            r5 = 1
            goto L_0x0119
        L_0x0209:
            r13 = 1
            if (r6 != r13) goto L_0x0211
        L_0x020c:
            r12 = 2
            if (r6 == r12) goto L_0x01e0
            int r11 = r11 + r8
            goto L_0x01c9
        L_0x0211:
            int r12 = r12 + r7
            goto L_0x01ce
        L_0x0213:
            int r9 = r9 - r8
            goto L_0x01e6
        L_0x0215:
            int r11 = r10 - r7
            goto L_0x01ee
        L_0x0218:
            int r11 = r11 - r7
            goto L_0x01ee
        L_0x021a:
            int r9 = r9 - r8
            goto L_0x01e6
        L_0x021c:
            r5 = move-exception
        L_0x021d:
            r5 = 0
            goto L_0x0119
        L_0x0220:
            r10 = r23
            goto L_0x01bd
        L_0x0223:
            r9 = r22
            goto L_0x01a3
        L_0x0227:
            r17 = r8
            r8 = r7
            r7 = r17
            goto L_0x0114
        L_0x022e:
            r11 = r5
            goto L_0x00ba
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.f.b(com.uc.c.bk, byte[], com.uc.c.ca, int, int, int, int, int, int):boolean");
    }

    public static byte[][] b(ca caVar, byte[] bArr, int i) {
        if (caVar == null || bArr == null) {
            return null;
        }
        int b2 = b(bArr, ca.bLe, 1);
        if (b2 == -1) {
            return null;
        }
        return (byte[][]) ((Vector) caVar.bFV.get(Integer.valueOf(i))).elementAt(b2);
    }

    public static int c(byte[] bArr) {
        return d(bArr, 1);
    }

    public static int c(byte[] bArr, int i) {
        return (ca.G(bArr, i) >> 20) & ca.bNY;
    }

    private int c(byte[] bArr, ca caVar) {
        return caVar.LK() ? Math.max(1, d(bArr)) : d(bArr);
    }

    private final void c(bk bkVar, ca caVar, int i, int i2, int i3, int i4, int i5) {
        bkVar.Du();
        bkVar.l(i, i2, i3, i4);
        bkVar.e(i, i2, i3, i4, i5, 0);
        bkVar.Dv();
    }

    private void c(bk bkVar, byte[] bArr, ca caVar) {
        int a2;
        int a3 = a(bArr, caVar);
        int b2 = b(bArr, caVar);
        int c = c(bArr, caVar);
        int d = d(bArr, caVar);
        caVar.LK();
        int g = g(bArr, 1);
        int i = g == -1 ? caVar.bFQ : g;
        int g2 = g(bArr, 29);
        if (g2 == -1) {
            g2 = 0;
        }
        k I = e.I(g2, i);
        int b3 = b(bArr, ca.bLr, 1);
        int b4 = b(bArr, ca.bLr, 2);
        String d2 = caVar.bGd.d((Vector) caVar.bHd.get(Integer.valueOf(this.cJ)), bArr);
        boolean z = caVar.p != null && caVar.p.a(bkVar.left + a3, bkVar.top + b2, c, d, i, d2, 5, a(caVar, bArr, false), bkVar.bkB.bmb);
        bkVar.save();
        bkVar.Du();
        bkVar.l(a3, b2, c, d);
        if (!z) {
            bkVar.setColor(az.bdf[147]);
            bkVar.o(a3, b2, c, d);
        }
        if (d2 != null && d2.length() > 0) {
            bkVar.d(I);
            bkVar.gV(az.bdf[146]);
            if (b4 == 3) {
                StringBuffer stringBuffer = new StringBuffer();
                for (int i2 = 0; i2 < d2.length(); i2++) {
                    stringBuffer.append("•");
                }
                bkVar.b(stringBuffer.toString(), a3 + 5, b2, c - (5 << 1), d, 1, 2);
            } else if (b3 == 1) {
                int n = I.n(d2, c - (5 << 1));
                if (n != d2.length()) {
                    d2 = bc.Z(d2.substring(0, n), "..");
                }
                bkVar.b(d2, a3 + 5, b2, c - (5 << 1), d, 1, 2);
            } else if (b3 == 2) {
                int i3 = b2 + 2;
                int uN = I.uN();
                String[] split = d2.split(bx.bxN);
                if (split == null || split.length <= 1) {
                    bkVar.a(d2, a3, b2, c, d, i3, 5, I, uN);
                } else {
                    int i4 = 0;
                    while (i4 < split.length && (a2 = bkVar.a(split[i4], a3, b2, c, d, i3, 5, I, uN)) <= b2 + d) {
                        i4++;
                        i3 = a2;
                    }
                }
            }
        }
        bkVar.Dv();
        if (!z) {
            bkVar.d(e.bj(caVar.bFQ));
            bkVar.setColor(az.bdf[148]);
            bkVar.q(a3, b2, c - 1, d - 1);
        }
        bkVar.reset();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0054, code lost:
        r27.save();
        r27.Dw();
        r27.Dy();
        r6 = b(r28);
        r14 = c(r28);
        r8 = d(r28);
        r16 = e(r28);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006d, code lost:
        if (r7 == false) goto L_0x0104;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0073, code lost:
        if (com.uc.c.az.BI() == false) goto L_0x00ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0075, code lost:
        r5 = com.uc.c.az.bdf;
        r12 = r5[243(0xf3, float:3.4E-43)];
        r13 = r5[244(0xf4, float:3.42E-43)];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r15 = r27.bkB.DQ();
        r27.gY(2);
        r27.setColor(r13);
        r27.h(r6, r14 + 9, r8, r16 - 9, 6, 6);
        r27.setColor(r12);
        r27.g(r6, r14 + 9, r8, r16 - 9, 6, 6);
        r27.gY(r15);
        r5 = r6 + 50;
        r6 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00c2, code lost:
        if (r6 >= 10) goto L_0x0262;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00c4, code lost:
        r27.setColor(r12);
        r27.m(r5 - r6, r14 + r6, r5 + r6, r14 + r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00dc, code lost:
        if (r6 <= 2) goto L_0x00f9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00de, code lost:
        r27.setColor(r13);
        r27.m((r5 - r6) + 3, r14 + r6, (r5 + r6) - 3, r14 + r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00f9, code lost:
        r6 = r6 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00ff, code lost:
        r12 = r22;
        r13 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0105, code lost:
        if (r9 == -1) goto L_0x0127;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x010b, code lost:
        if (com.uc.c.az.BI() == false) goto L_0x01de;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x010d, code lost:
        r5 = r29.Ks();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0111, code lost:
        r27.setColor(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x011b, code lost:
        if (r29.ac() != false) goto L_0x0127;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x011d, code lost:
        r27.o(r6, r14, r8, r16);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0127, code lost:
        a(r27, r28, r29, r6, r14, r8, r16, r30);
        r26 = (int[][]) java.lang.reflect.Array.newInstance(java.lang.Integer.TYPE, 3, 4);
        r5 = r26[0];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x014c, code lost:
        if (r18 != -1) goto L_0x01e1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x014e, code lost:
        r9 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x014f, code lost:
        r5[0] = r9;
        r5 = r26[0];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0159, code lost:
        if (r19 != -1) goto L_0x01e5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x015b, code lost:
        r9 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x015c, code lost:
        r5[1] = r9;
        r5 = r26[0];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0166, code lost:
        if (r20 != -1) goto L_0x01e9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0168, code lost:
        r9 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0169, code lost:
        r5[2] = r9;
        r5 = r26[0];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0173, code lost:
        if (r21 != -1) goto L_0x01ed;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0175, code lost:
        r9 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0176, code lost:
        r5[3] = r9;
        r26[2][0] = r29.aE(r28);
        r26[2][1] = r29.aH(r28);
        r26[2][2] = r29.aG(r28);
        r26[2][3] = r29.aF(r28);
        r26[1][0] = r22;
        r26[1][1] = r23;
        r26[1][2] = r24;
        r26[1][3] = r25;
        r5 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x01ca, code lost:
        if (r5 >= 4) goto L_0x0262;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01d2, code lost:
        if (r26[1][r5] == -1) goto L_0x01db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x01d9, code lost:
        if (r26[0][r5] > 0) goto L_0x01f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x01db, code lost:
        r5 = r5 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01de, code lost:
        r5 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x01e1, code lost:
        r9 = r18;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x01e5, code lost:
        r9 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x01e9, code lost:
        r9 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x01ed, code lost:
        r9 = r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x01f0, code lost:
        r27.gX(r26[0][r5]);
        r27.gY(r26[2][r5]);
        r27.setColor(r26[1][r5]);
        r7 = ((float) r26[2][r5]) / 2.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x021a, code lost:
        switch(r5) {
            case 0: goto L_0x021e;
            case 1: goto L_0x022d;
            case 2: goto L_0x023c;
            case 3: goto L_0x024e;
            default: goto L_0x021d;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x021e, code lost:
        r7 = (int) (r7 + ((float) r14));
        r27.m(r6, r7, r6 + r8, r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x022d, code lost:
        r7 = (int) (r7 + ((float) r6));
        r27.m(r7, r14, r7, r14 + r16);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x023c, code lost:
        r7 = (int) (((float) (r14 + r16)) - r7);
        r27.m(r6, r7, r6 + r8, r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x024e, code lost:
        r7 = (int) (((float) (r6 + r8)) - r7);
        r27.m(r7, r14, r7, r14 + r16);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void c(com.uc.c.bk r27, byte[] r28, com.uc.c.ca r29, int r30) {
        /*
            r26 = this;
            int[] r5 = com.uc.c.f.cK
            monitor-enter(r5)
            f(r28)     // Catch:{ all -> 0x00fc }
            int[] r6 = com.uc.c.f.cK     // Catch:{ all -> 0x00fc }
            r7 = 26
            r6 = r6[r7]     // Catch:{ all -> 0x00fc }
            r7 = 1
            if (r6 != r7) goto L_0x001b
            r6 = 1
            r7 = r6
        L_0x0011:
            boolean r6 = com.uc.c.az.BI()     // Catch:{ all -> 0x00fc }
            if (r6 == 0) goto L_0x001e
            if (r7 != 0) goto L_0x001e
            monitor-exit(r5)     // Catch:{ all -> 0x00fc }
        L_0x001a:
            return
        L_0x001b:
            r6 = 0
            r7 = r6
            goto L_0x0011
        L_0x001e:
            int[] r6 = com.uc.c.f.cK     // Catch:{ all -> 0x00fc }
            r8 = 3
            r9 = r6[r8]     // Catch:{ all -> 0x00fc }
            int[] r6 = com.uc.c.f.cK     // Catch:{ all -> 0x00fc }
            r8 = 42
            r18 = r6[r8]     // Catch:{ all -> 0x00fc }
            int[] r6 = com.uc.c.f.cK     // Catch:{ all -> 0x00fc }
            r8 = 43
            r19 = r6[r8]     // Catch:{ all -> 0x00fc }
            int[] r6 = com.uc.c.f.cK     // Catch:{ all -> 0x00fc }
            r8 = 45
            r20 = r6[r8]     // Catch:{ all -> 0x00fc }
            int[] r6 = com.uc.c.f.cK     // Catch:{ all -> 0x00fc }
            r8 = 44
            r21 = r6[r8]     // Catch:{ all -> 0x00fc }
            int[] r6 = com.uc.c.f.cK     // Catch:{ all -> 0x00fc }
            r8 = 16
            r22 = r6[r8]     // Catch:{ all -> 0x00fc }
            int[] r6 = com.uc.c.f.cK     // Catch:{ all -> 0x00fc }
            r8 = 17
            r23 = r6[r8]     // Catch:{ all -> 0x00fc }
            int[] r6 = com.uc.c.f.cK     // Catch:{ all -> 0x00fc }
            r8 = 18
            r24 = r6[r8]     // Catch:{ all -> 0x00fc }
            int[] r6 = com.uc.c.f.cK     // Catch:{ all -> 0x00fc }
            r8 = 19
            r25 = r6[r8]     // Catch:{ all -> 0x00fc }
            monitor-exit(r5)     // Catch:{ all -> 0x00fc }
            r27.save()
            r27.Dw()
            r27.Dy()
            int r6 = b(r28)
            int r14 = c(r28)
            int r8 = d(r28)
            int r16 = e(r28)
            if (r7 == 0) goto L_0x0104
            boolean r5 = com.uc.c.az.BI()
            if (r5 == 0) goto L_0x00ff
            int[] r5 = com.uc.c.az.bdf
            r7 = 243(0xf3, float:3.4E-43)
            r7 = r5[r7]
            r9 = 244(0xf4, float:3.42E-43)
            r5 = r5[r9]
            r12 = r7
            r13 = r5
        L_0x0081:
            r0 = r27
            b.a.a.a.u r0 = r0.bkB     // Catch:{ Exception -> 0x0261 }
            r5 = r0
            int r15 = r5.DQ()     // Catch:{ Exception -> 0x0261 }
            r5 = 2
            r0 = r27
            r1 = r5
            r0.gY(r1)     // Catch:{ Exception -> 0x0261 }
            r0 = r27
            r1 = r13
            r0.setColor(r1)     // Catch:{ Exception -> 0x0261 }
            int r7 = r14 + 9
            r5 = 9
            int r9 = r16 - r5
            r10 = 6
            r11 = 6
            r5 = r27
            r5.h(r6, r7, r8, r9, r10, r11)     // Catch:{ Exception -> 0x0261 }
            r0 = r27
            r1 = r12
            r0.setColor(r1)     // Catch:{ Exception -> 0x0261 }
            int r7 = r14 + 9
            r5 = 9
            int r9 = r16 - r5
            r10 = 6
            r11 = 6
            r5 = r27
            r5.g(r6, r7, r8, r9, r10, r11)     // Catch:{ Exception -> 0x0261 }
            r0 = r27
            r1 = r15
            r0.gY(r1)     // Catch:{ Exception -> 0x0261 }
            int r5 = r6 + 50
            r6 = 0
        L_0x00c0:
            r7 = 10
            if (r6 >= r7) goto L_0x0262
            r0 = r27
            r1 = r12
            r0.setColor(r1)     // Catch:{ Exception -> 0x0261 }
            int r7 = r5 - r6
            int r8 = r14 + r6
            int r9 = r5 + r6
            int r10 = r14 + r6
            r0 = r27
            r1 = r7
            r2 = r8
            r3 = r9
            r4 = r10
            r0.m(r1, r2, r3, r4)     // Catch:{ Exception -> 0x0261 }
            r7 = 2
            if (r6 <= r7) goto L_0x00f9
            r0 = r27
            r1 = r13
            r0.setColor(r1)     // Catch:{ Exception -> 0x0261 }
            int r7 = r5 - r6
            int r7 = r7 + 3
            int r8 = r14 + r6
            int r9 = r5 + r6
            r10 = 3
            int r9 = r9 - r10
            int r10 = r14 + r6
            r0 = r27
            r1 = r7
            r2 = r8
            r3 = r9
            r4 = r10
            r0.m(r1, r2, r3, r4)     // Catch:{ Exception -> 0x0261 }
        L_0x00f9:
            int r6 = r6 + 1
            goto L_0x00c0
        L_0x00fc:
            r6 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x00fc }
            throw r6
        L_0x00ff:
            r12 = r22
            r13 = r9
            goto L_0x0081
        L_0x0104:
            r5 = -1
            if (r9 == r5) goto L_0x0127
            boolean r5 = com.uc.c.az.BI()
            if (r5 == 0) goto L_0x01de
            int r5 = r29.Ks()
        L_0x0111:
            r0 = r27
            r1 = r5
            r0.setColor(r1)
            boolean r5 = r29.ac()
            if (r5 != 0) goto L_0x0127
            r0 = r27
            r1 = r6
            r2 = r14
            r3 = r8
            r4 = r16
            r0.o(r1, r2, r3, r4)
        L_0x0127:
            r9 = r26
            r10 = r27
            r11 = r28
            r12 = r29
            r13 = r6
            r15 = r8
            r17 = r30
            r9.a(r10, r11, r12, r13, r14, r15, r16, r17)
            r5 = 3
            r7 = 4
            int[] r5 = new int[]{r5, r7}
            java.lang.Class r7 = java.lang.Integer.TYPE
            java.lang.Object r26 = java.lang.reflect.Array.newInstance(r7, r5)
            int[][] r26 = (int[][]) r26
            r5 = 0
            r5 = r26[r5]
            r7 = 0
            r9 = -1
            r0 = r18
            r1 = r9
            if (r0 != r1) goto L_0x01e1
            r9 = 0
        L_0x014f:
            r5[r7] = r9
            r5 = 0
            r5 = r26[r5]
            r7 = 1
            r9 = -1
            r0 = r19
            r1 = r9
            if (r0 != r1) goto L_0x01e5
            r9 = 0
        L_0x015c:
            r5[r7] = r9
            r5 = 0
            r5 = r26[r5]
            r7 = 2
            r9 = -1
            r0 = r20
            r1 = r9
            if (r0 != r1) goto L_0x01e9
            r9 = 0
        L_0x0169:
            r5[r7] = r9
            r5 = 0
            r5 = r26[r5]
            r7 = 3
            r9 = -1
            r0 = r21
            r1 = r9
            if (r0 != r1) goto L_0x01ed
            r9 = 0
        L_0x0176:
            r5[r7] = r9
            r5 = 2
            r5 = r26[r5]
            r7 = 0
            r0 = r29
            r1 = r28
            int r9 = r0.aE(r1)
            r5[r7] = r9
            r5 = 2
            r5 = r26[r5]
            r7 = 1
            r0 = r29
            r1 = r28
            int r9 = r0.aH(r1)
            r5[r7] = r9
            r5 = 2
            r5 = r26[r5]
            r7 = 2
            r0 = r29
            r1 = r28
            int r9 = r0.aG(r1)
            r5[r7] = r9
            r5 = 2
            r5 = r26[r5]
            r7 = 3
            r0 = r29
            r1 = r28
            int r9 = r0.aF(r1)
            r5[r7] = r9
            r5 = 1
            r5 = r26[r5]
            r7 = 0
            r5[r7] = r22
            r5 = 1
            r5 = r26[r5]
            r7 = 1
            r5[r7] = r23
            r5 = 1
            r5 = r26[r5]
            r7 = 2
            r5[r7] = r24
            r5 = 1
            r5 = r26[r5]
            r7 = 3
            r5[r7] = r25
            r5 = 0
        L_0x01c9:
            r7 = 4
            if (r5 >= r7) goto L_0x0262
            r7 = 1
            r7 = r26[r7]
            r7 = r7[r5]
            r9 = -1
            if (r7 == r9) goto L_0x01db
            r7 = 0
            r7 = r26[r7]
            r7 = r7[r5]
            if (r7 > 0) goto L_0x01f0
        L_0x01db:
            int r5 = r5 + 1
            goto L_0x01c9
        L_0x01de:
            r5 = r9
            goto L_0x0111
        L_0x01e1:
            r9 = r18
            goto L_0x014f
        L_0x01e5:
            r9 = r19
            goto L_0x015c
        L_0x01e9:
            r9 = r20
            goto L_0x0169
        L_0x01ed:
            r9 = r21
            goto L_0x0176
        L_0x01f0:
            r7 = 0
            r7 = r26[r7]
            r7 = r7[r5]
            r0 = r27
            r1 = r7
            r0.gX(r1)
            r7 = 2
            r7 = r26[r7]
            r7 = r7[r5]
            r0 = r27
            r1 = r7
            r0.gY(r1)
            r7 = 1
            r7 = r26[r7]
            r7 = r7[r5]
            r0 = r27
            r1 = r7
            r0.setColor(r1)
            r7 = 2
            r7 = r26[r7]
            r7 = r7[r5]
            float r7 = (float) r7
            r9 = 1073741824(0x40000000, float:2.0)
            float r7 = r7 / r9
            switch(r5) {
                case 0: goto L_0x021e;
                case 1: goto L_0x022d;
                case 2: goto L_0x023c;
                case 3: goto L_0x024e;
                default: goto L_0x021d;
            }
        L_0x021d:
            goto L_0x01db
        L_0x021e:
            float r9 = (float) r14
            float r7 = r7 + r9
            int r7 = (int) r7
            int r9 = r6 + r8
            r0 = r27
            r1 = r6
            r2 = r7
            r3 = r9
            r4 = r7
            r0.m(r1, r2, r3, r4)
            goto L_0x01db
        L_0x022d:
            float r9 = (float) r6
            float r7 = r7 + r9
            int r7 = (int) r7
            int r9 = r14 + r16
            r0 = r27
            r1 = r7
            r2 = r14
            r3 = r7
            r4 = r9
            r0.m(r1, r2, r3, r4)
            goto L_0x01db
        L_0x023c:
            int r9 = r14 + r16
            float r9 = (float) r9
            float r7 = r9 - r7
            int r7 = (int) r7
            int r9 = r6 + r8
            r0 = r27
            r1 = r6
            r2 = r7
            r3 = r9
            r4 = r7
            r0.m(r1, r2, r3, r4)
            goto L_0x01db
        L_0x024e:
            int r9 = r6 + r8
            float r9 = (float) r9
            float r7 = r9 - r7
            int r7 = (int) r7
            int r9 = r14 + r16
            r0 = r27
            r1 = r7
            r2 = r14
            r3 = r7
            r4 = r9
            r0.m(r1, r2, r3, r4)
            goto L_0x01db
        L_0x0261:
            r5 = move-exception
        L_0x0262:
            r27.Dz()
            r27.Dx()
            r27.reset()
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.f.c(com.uc.c.bk, byte[], com.uc.c.ca, int):void");
    }

    public static void c(byte[] bArr, int i, int i2) {
        int F = ca.F(bArr, 10) + 12;
        int i3 = 12;
        while (i3 < F) {
            byte[] bArr2 = ca.bJS;
            byte b2 = bArr[i3];
            switch (bArr2[b2]) {
                case 1:
                    i3 += 2;
                    continue;
                case 2:
                    i3 += 3;
                    continue;
                case 3:
                    i3 += 4;
                    continue;
                case 4:
                    if (bArr[i3] == i) {
                        ca.r(bArr, i3 + 1, i2);
                    }
                    i3 += 5;
                    continue;
                case 5:
                    i3 += ca.F(bArr, i3 + 1) + 3;
                    continue;
            }
            if (b2 == i) {
                return;
            }
        }
    }

    public static int d(byte[] bArr) {
        return c(bArr, 5);
    }

    public static int d(byte[] bArr, int i) {
        return ca.G(bArr, i) & ca.bNZ;
    }

    private int d(byte[] bArr, ca caVar) {
        return caVar.LK() ? Math.max(1, e(bArr)) : e(bArr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.f.a(com.uc.c.ca, byte[], boolean):boolean
     arg types: [com.uc.c.ca, byte[], int]
     candidates:
      com.uc.c.f.a(byte[], byte[], int):java.lang.String
      com.uc.c.f.a(com.uc.c.bk, byte[], com.uc.c.ca):void
      com.uc.c.f.a(byte[], int, int):void
      com.uc.c.f.a(com.uc.c.ca, byte[], int):boolean
      com.uc.c.f.a(int, int, int):byte[]
      com.uc.c.f.a(byte[], com.uc.c.ca, int):char[]
      com.uc.c.f.a(int, int, com.uc.c.ca):void
      com.uc.c.f.a(com.uc.c.bk, com.uc.c.ca, int):void
      com.uc.c.f.a(com.uc.c.ca, byte[], boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int):void
     arg types: [int[], int, int, int, int, int, int, int]
     candidates:
      com.uc.c.bk.a(int, int, int, int, int, int, b.a.a.a.f, b.a.a.a.f):void
      com.uc.c.bk.a(int, int, int, int, int[], boolean, int, int):void
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean):void
      com.uc.c.bk.a(int[], int, int, int, int, int, int, boolean):void
      com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int):void */
    private void d(bk bkVar, byte[] bArr, ca caVar) {
        int i;
        int a2 = a(bArr, caVar);
        int b2 = b(bArr, caVar);
        int c = c(bArr, caVar);
        int d = d(bArr, caVar);
        String str = ar.avM[51];
        int i2 = caVar.bFQ;
        int f = e.f(i2, str) + 4;
        int b3 = b(bArr, ca.bLB, 1);
        n ch = caVar.ch();
        b.a.a.a.f sg = ch != null ? ch.sg() : null;
        int width = (b3 != 1 || sg == null) ? f : sg.getWidth();
        boolean b4 = caVar.p.b(bkVar.left + a2, bkVar.top + b2, c, d, width, a(caVar, bArr, false), bkVar.bkB.bmb);
        String str2 = new String(caVar.bGd.e((Vector) caVar.bHd.get(Integer.valueOf(this.cJ)), bArr));
        boolean LK = caVar.LK();
        bkVar.save();
        if (LK) {
            try {
                i = caVar.jl(width);
            } catch (Exception e) {
                bkVar.reset();
                return;
            } catch (Throwable th) {
                bkVar.reset();
                throw th;
            }
        } else {
            i = width;
        }
        int[] iArr = az.bdf;
        int i3 = a2 + c;
        int i4 = i3 - i;
        int i5 = i4 - 1;
        if (!b4) {
            bkVar.setColor(iArr[133]);
            bkVar.o(a2, b2, c, d);
            bkVar.setColor(iArr[134]);
            bkVar.q(a2, b2, c, d);
            if (b3 != 1 || LK) {
                bkVar.m(i4 - 1, b2, i4 - 1, b2 + d);
            }
        }
        if (str2 != null && !"".equals(str2)) {
            bkVar.setColor(iArr[140]);
            if (str2.startsWith(bx.bAk)) {
                int parseInt = Integer.parseInt(str2.substring(bx.bAk.length(), str2.indexOf(".png")));
                if (caVar.bGH == null || caVar.bGH.elementAt(parseInt) == null) {
                    caVar.bGd.a((short) caVar.bGd.v(bArr), (char[]) null, this.cJ);
                    str2 = null;
                } else {
                    str2 = str2.substring(8);
                }
            } else if (str2.startsWith(cd.bVd)) {
                str2 = a(bArr, ca.bLB, 2);
            }
            if (str2 != null) {
                bkVar.b(bc.c(str2, (i5 - a2) - 1, i2), a2 + 1, b2, (i5 - a2) - 1, d, 1, 2);
            }
        } else if (b3 == 1) {
            bkVar.setColor(iArr[146]);
            bkVar.a(" 拍照上传文件", a2 + 1, b2, (i5 - a2) - 1, d, 1, 2);
        } else {
            bkVar.setColor(iArr[134]);
            bkVar.a(str, a2 + 5, b2, (i5 - a2) - 1, d, 1, 2);
        }
        if ((b3 != 1 || LK) && !b4) {
            bkVar.a(iArr, i4, b2 + 1, i - 2, d - 2, true, 135, 4);
            bkVar.setColor(iArr[139]);
            bkVar.q(i4, b2 + 1, i - 2, d - 2);
        }
        if (b3 != 1) {
            bkVar.setColor(iArr[140]);
            bkVar.b(str, i4, b2, i, d, 2, 2);
        } else if (sg != null) {
            bkVar.a(sg, (i3 - sg.getWidth()) - ((d - sg.getHeight()) / 2), ((d - sg.getHeight()) / 2) + b2 + 1, 3);
        }
        bkVar.reset();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.f.a(com.uc.c.ca, byte[], boolean):boolean
     arg types: [com.uc.c.ca, byte[], int]
     candidates:
      com.uc.c.f.a(byte[], byte[], int):java.lang.String
      com.uc.c.f.a(com.uc.c.bk, byte[], com.uc.c.ca):void
      com.uc.c.f.a(byte[], int, int):void
      com.uc.c.f.a(com.uc.c.ca, byte[], int):boolean
      com.uc.c.f.a(int, int, int):byte[]
      com.uc.c.f.a(byte[], com.uc.c.ca, int):char[]
      com.uc.c.f.a(int, int, com.uc.c.ca):void
      com.uc.c.f.a(com.uc.c.bk, com.uc.c.ca, int):void
      com.uc.c.f.a(com.uc.c.ca, byte[], boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(int[], int, int, int, int, boolean):void
     arg types: [int[], int, int, int, int, int]
     candidates:
      com.uc.c.bk.a(int, int, int, int, int, boolean):void
      com.uc.c.bk.a(b.a.a.a.f, int, int, int, int, boolean):void
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int):void
      com.uc.c.bk.a(char[], int, int, int, int, int):void
      com.uc.c.bk.a(int[], int, int, int, int, boolean):void */
    private void d(bk bkVar, byte[] bArr, ca caVar, int i) {
        int g;
        int a2 = a(bArr, caVar);
        int b2 = b(bArr, caVar);
        int c = c(bArr, caVar);
        int d = d(bArr, caVar);
        caVar.LK();
        int[] iArr = az.bdf;
        String d2 = caVar.bGd.d((Vector) caVar.bHd.get(Integer.valueOf(this.cJ)), bArr);
        int b3 = b(bArr, ca.bLv, 0);
        boolean BI = az.BI();
        int i2 = (BI || (g = g(bArr, 2)) == -1) ? iArr[242] : g;
        boolean b4 = b(bkVar, bArr, caVar, a2, b2, c, d, i, g(bArr, 8));
        if (!b4) {
            b4 = caVar.p.a(a2 + bkVar.left, b2 + bkVar.top, c, d, a(caVar, bArr, false), bkVar.bkB.bmb);
        }
        bkVar.save();
        if (!b4) {
            int g2 = g(bArr, 3);
            bkVar.a(BI ? new int[]{r.Dk[7], r.Dk[8], r.Dk[9], r.Dk[10]} : g2 == -1 ? new int[]{r.Dk[0], r.Dk[1], r.Dk[2], r.Dk[3]} : new int[]{g2}, a2 + 1, b2 + 1, c - 2, d - 2, true);
            if (BI) {
                g2 = r.Dk[12];
            } else if (g2 == -1) {
                g2 = r.Dk[5];
            }
            bkVar.setColor(g2);
            bkVar.q(a2, b2, c - 1, d - 1);
        }
        if (d2 != null && (!caVar.Lv() || b3 != 3)) {
            int g3 = g(bArr, 1);
            int g4 = g(bArr, 29);
            if (g4 == -1) {
                g4 = 0;
            }
            int i3 = g3 == -1 ? caVar.bFQ : g3;
            k I = e.I(g4, i3);
            int measureText = (int) I.getPaint().measureText(d2);
            if (g3 != -1 || ((i3 <= d && measureText <= c) || !caVar.Lv())) {
                bkVar.d(I);
                bkVar.gV(i2);
                bkVar.b(new String(d2), a2, b2, c, d, 2, 2);
            }
        }
        bkVar.reset();
    }

    public static int e(byte[] bArr) {
        return d(bArr, 5);
    }

    public static final String e(byte[] bArr, int i) {
        StringBuffer stringBuffer = new StringBuffer(bc.x(bArr, i) >> 1);
        bc.a(bArr, i + 2, bc.x(bArr, i), stringBuffer);
        return stringBuffer.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.f.a(com.uc.c.ca, byte[], boolean):boolean
     arg types: [com.uc.c.ca, byte[], int]
     candidates:
      com.uc.c.f.a(byte[], byte[], int):java.lang.String
      com.uc.c.f.a(com.uc.c.bk, byte[], com.uc.c.ca):void
      com.uc.c.f.a(byte[], int, int):void
      com.uc.c.f.a(com.uc.c.ca, byte[], int):boolean
      com.uc.c.f.a(int, int, int):byte[]
      com.uc.c.f.a(byte[], com.uc.c.ca, int):char[]
      com.uc.c.f.a(int, int, com.uc.c.ca):void
      com.uc.c.f.a(com.uc.c.bk, com.uc.c.ca, int):void
      com.uc.c.f.a(com.uc.c.ca, byte[], boolean):boolean */
    private void e(bk bkVar, byte[] bArr, ca caVar) {
        boolean z = caVar.bGd.e((Vector) caVar.bHd.get(Integer.valueOf(this.cJ)), bArr)[0] == '1';
        int a2 = a(bArr, caVar);
        int b2 = b(bArr, caVar);
        if (!caVar.p.a(bkVar.left + a2, bkVar.top + b2, z, a(caVar, bArr, false), bkVar.bkB.bmb)) {
            b.a.a.a.f bJ = w.ns().bJ(az.BI() ? z ? 240 == w.Qr ? 123 : 480 == w.Qr ? UCR.Color.bTn : UCR.Color.bTm : 240 == w.Qr ? 120 : 480 == w.Qr ? 122 : 121 : z ? 240 == w.Qr ? 113 : 480 == w.Qr ? 115 : 114 : 240 == w.Qr ? 110 : 480 == w.Qr ? 112 : 111);
            if (bJ != null) {
                bkVar.a(bJ, a2, b2, 0);
                if (a(caVar, bArr, false)) {
                    bkVar.save();
                    bkVar.setColor(az.bdf[25]);
                    bkVar.q(a2 - 1, b2 - 1, bJ.getWidth() + 1, bJ.getHeight() + 1);
                    bkVar.q(a2, b2, bJ.getWidth() - 1, bJ.getHeight() - 1);
                    bkVar.reset();
                }
            }
        }
    }

    public static int f(byte[] bArr, int i) {
        int g = g(bArr, i);
        if (g != -1) {
            return g;
        }
        if (0 != 0 || 0 != 0 || i == 11 || i == 10 || i == 9 || i == 14 || i == 15) {
            return 0;
        }
        return g;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.f.a(com.uc.c.ca, byte[], boolean):boolean
     arg types: [com.uc.c.ca, byte[], int]
     candidates:
      com.uc.c.f.a(byte[], byte[], int):java.lang.String
      com.uc.c.f.a(com.uc.c.bk, byte[], com.uc.c.ca):void
      com.uc.c.f.a(byte[], int, int):void
      com.uc.c.f.a(com.uc.c.ca, byte[], int):boolean
      com.uc.c.f.a(int, int, int):byte[]
      com.uc.c.f.a(byte[], com.uc.c.ca, int):char[]
      com.uc.c.f.a(int, int, com.uc.c.ca):void
      com.uc.c.f.a(com.uc.c.bk, com.uc.c.ca, int):void
      com.uc.c.f.a(com.uc.c.ca, byte[], boolean):boolean */
    private void f(bk bkVar, byte[] bArr, ca caVar) {
        boolean z = caVar.bGd.e((Vector) caVar.bHd.get(Integer.valueOf(this.cJ)), bArr)[0] == '1';
        int a2 = a(bArr, caVar);
        int b2 = b(bArr, caVar);
        if (!caVar.p.b(bkVar.left + a2, bkVar.top + b2, z, a(caVar, bArr, false), bkVar.bkB.bmb)) {
            b.a.a.a.f gb = az.gb(z ? 19 : 18);
            if (gb != null) {
                bkVar.a(gb, a2, b2, 0);
                if (a(caVar, bArr, false)) {
                    bkVar.save();
                    bkVar.setColor(az.bdf[25]);
                    bkVar.q(a2 - 1, b2 - 1, gb.getWidth() + 1, gb.getHeight() + 1);
                    bkVar.q(a2, b2, gb.getWidth() - 1, gb.getHeight() - 1);
                    bkVar.reset();
                }
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v11, resolved type: int[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void f(byte[] r6) {
        /*
            r0 = 10
            int r0 = com.uc.c.ca.F(r6, r0)
            int r0 = r0 + 12
            r1 = -1
            r2 = 0
        L_0x000a:
            int[] r3 = com.uc.c.f.cK
            int r3 = r3.length
            if (r2 >= r3) goto L_0x0016
            int[] r3 = com.uc.c.f.cK
            r3[r2] = r1
            int r2 = r2 + 1
            goto L_0x000a
        L_0x0016:
            r1 = 12
        L_0x0018:
            if (r1 >= r0) goto L_0x0082
            byte[] r2 = com.uc.c.ca.bJS
            byte r3 = r6[r1]
            byte r2 = r2[r3]
            switch(r2) {
                case 1: goto L_0x0024;
                case 2: goto L_0x0031;
                case 3: goto L_0x0040;
                case 4: goto L_0x0061;
                case 5: goto L_0x0070;
                default: goto L_0x0023;
            }
        L_0x0023:
            goto L_0x0018
        L_0x0024:
            int[] r2 = com.uc.c.f.cK
            byte r3 = r6[r1]
            int r4 = r1 + 1
            byte r4 = r6[r4]
            r2[r3] = r4
            int r1 = r1 + 2
            goto L_0x0018
        L_0x0031:
            int[] r2 = com.uc.c.f.cK
            byte r3 = r6[r1]
            int r4 = r1 + 1
            int r4 = com.uc.c.ca.F(r6, r4)
            r2[r3] = r4
            int r1 = r1 + 3
            goto L_0x0018
        L_0x0040:
            int[] r2 = com.uc.c.f.cK
            byte r3 = r6[r1]
            int r4 = r1 + 1
            byte r4 = r6[r4]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << 16
            int r5 = r1 + 2
            byte r5 = r6[r5]
            r5 = r5 & 255(0xff, float:3.57E-43)
            int r5 = r5 << 8
            int r4 = r4 + r5
            int r5 = r1 + 3
            byte r5 = r6[r5]
            r5 = r5 & 255(0xff, float:3.57E-43)
            int r4 = r4 + r5
            r2[r3] = r4
            int r1 = r1 + 4
            goto L_0x0018
        L_0x0061:
            int[] r2 = com.uc.c.f.cK
            byte r3 = r6[r1]
            int r4 = r1 + 1
            int r4 = com.uc.c.ca.G(r6, r4)
            r2[r3] = r4
            int r1 = r1 + 5
            goto L_0x0018
        L_0x0070:
            int[] r2 = com.uc.c.f.cK
            byte r3 = r6[r1]
            int r4 = r1 + 1
            r2[r3] = r4
            int r2 = r1 + 1
            int r2 = com.uc.c.ca.F(r6, r2)
            int r2 = r2 + 3
            int r1 = r1 + r2
            goto L_0x0018
        L_0x0082:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.f.f(byte[]):void");
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:30:0x0019 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    public static int g(byte[] bArr, int i) {
        int F = ca.F(bArr, 10) + 12;
        int i2 = -1;
        int i3 = 12;
        while (i3 < F) {
            byte[] bArr2 = ca.bJS;
            byte b2 = bArr[i3];
            switch (bArr2[b2]) {
                case 1:
                    if (bArr[i3] == i) {
                        i2 = bArr[i3 + 1];
                    }
                    i3 += 2;
                    continue;
                case 2:
                    if (bArr[i3] == i) {
                        i2 = ca.F(bArr, i3 + 1);
                    }
                    i3 += 3;
                    continue;
                case 3:
                    if (bArr[i3] == i) {
                        i2 = ((bArr[i3 + 1] & 255) << 16) + ((bArr[i3 + 2] & 255) << 8) + (bArr[i3 + 3] & 255);
                    }
                    i3 += 4;
                    continue;
                case 4:
                    if (bArr[i3] == i) {
                        i2 = ca.G(bArr, i3 + 1);
                    }
                    i3 += 5;
                    continue;
                case 5:
                    if (bArr[i3] == i) {
                        i2 = i3 + 1;
                    }
                    i3 += ca.F(bArr, i3 + 1) + 3;
                    continue;
            }
            if (b2 == i) {
                return i2;
            }
        }
        return i2;
    }

    public static int h(byte[] bArr, int i) {
        int f = f(bArr, 15);
        return f < 0 ? ((-f) * i) / 100 : f;
    }

    private final void i(bk bkVar, byte[] bArr, ca caVar) {
        bkVar.save();
        int b2 = b(bArr);
        int c = c(bArr);
        int d = d(bArr);
        int e = e(bArr);
        int b3 = b(bArr, ca.bLF, 1);
        bkVar.setColor(14606046);
        bkVar.o(b2 + 1, c + 1, d - 2, e - 2);
        Plugin jn = caVar.jn(b3);
        if (jn != null) {
            caVar.ch().a(jn, bkVar.gT(b2), bkVar.gU(c), d, e);
        }
        bkVar.setColor(13027014);
        bkVar.q(b2, c, d - 1, e - 1);
        bkVar.reset();
    }

    public final int a(ca caVar) {
        if (this.cG == null) {
            return caVar.bGr;
        }
        int c = this.cG.cG == null ? caVar.bGr : c(this.cG.cG.cH, caVar);
        byte[] bArr = this.cG.cH;
        int d = d(this.cG.cH);
        int O = caVar.O(bArr, c);
        int M = caVar.M(bArr, c);
        int aH = (((((d - O) - M) - caVar.aH(bArr)) - caVar.aF(bArr)) - caVar.K(this.cH, d)) - caVar.I(this.cH, d);
        return aH <= 0 ? d : aH;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.f.a(com.uc.c.ca, int, int, int, int, int, int, boolean, int):int
     arg types: [com.uc.c.ca, int, int, int, int, int, int, int, int]
     candidates:
      com.uc.c.f.a(com.uc.c.bk, int, int, boolean, int, int, b.a.a.a.f, int, int):void
      com.uc.c.f.a(com.uc.c.bk, byte[], com.uc.c.ca, int, int, int, int, int, int):void
      com.uc.c.f.a(com.uc.c.ca, int, int, int, int, int, int, boolean, int):int */
    public final int a(ca caVar, int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        return a(caVar, i, i2, i3, i4, i5, i6, true, i7);
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x007f A[LOOP:1: B:28:0x007d->B:29:0x007f, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0017  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a(com.uc.c.ca r6, int r7, int r8, int r9, int r10, int r11, int r12, boolean r13, int r14) {
        /*
            r5 = this;
            java.util.Vector r0 = r6.bHi
            int r0 = r0.size()
            if (r11 <= 0) goto L_0x00a0
            if (r11 >= r9) goto L_0x00a0
            r1 = 2
            boolean r1 = com.uc.c.bc.aL(r14, r1)
            if (r1 == 0) goto L_0x0059
            int r1 = r8 + r11
        L_0x0013:
            r2 = r1
            r1 = r12
        L_0x0015:
            if (r1 >= r0) goto L_0x007a
            java.util.Vector r3 = r6.bHi
            java.lang.Object r8 = r3.elementAt(r1)
            byte[] r8 = (byte[]) r8
            byte[] r8 = (byte[]) r8
            r3 = 32
            boolean r3 = com.uc.c.bc.aL(r14, r3)
            if (r3 == 0) goto L_0x0064
            int r3 = e(r8)
            int r3 = r10 - r3
            int r3 = r3 + r7
        L_0x0030:
            a(r8, r2, r3)
            if (r8 == 0) goto L_0x0048
            r3 = 0
            byte r3 = r8[r3]
            r4 = 3
            if (r3 != r4) goto L_0x0048
            int r3 = d(r8)
            int r3 = r3 + r2
            int r4 = r6.bGt
            int r3 = java.lang.Math.max(r3, r4)
            r6.bGt = r3
        L_0x0048:
            int r3 = d(r8)
            int r2 = r2 + r3
            int r3 = d(r8)
            if (r3 == 0) goto L_0x0056
            byte r3 = r6.bHp
            int r2 = r2 + r3
        L_0x0056:
            int r1 = r1 + 1
            goto L_0x0015
        L_0x0059:
            r1 = 4
            boolean r1 = com.uc.c.bc.aL(r14, r1)
            if (r1 == 0) goto L_0x00a0
            int r1 = r11 >> 1
            int r1 = r1 + r8
            goto L_0x0013
        L_0x0064:
            r3 = 8
            boolean r3 = com.uc.c.bc.aL(r14, r3)
            if (r3 == 0) goto L_0x006e
            r3 = r7
            goto L_0x0030
        L_0x006e:
            int r3 = e(r8)
            int r3 = r10 - r3
            int r3 = r3 + 1
            int r3 = r3 >> 1
            int r3 = r3 + r7
            goto L_0x0030
        L_0x007a:
            r1 = 1
            int r1 = r0 - r1
        L_0x007d:
            if (r1 < r12) goto L_0x0087
            java.util.Vector r2 = r6.bHi
            r2.removeElementAt(r1)
            int r1 = r1 + -1
            goto L_0x007d
        L_0x0087:
            if (r0 <= 0) goto L_0x0094
            byte[] r0 = r5.cH
            byte[] r1 = r5.cH
            int r1 = d(r1)
            b(r0, r1, r7)
        L_0x0094:
            int r0 = r7 + r10
            if (r10 <= 0) goto L_0x009e
            if (r13 == 0) goto L_0x009e
            byte r1 = r6.bHq
        L_0x009c:
            int r0 = r0 + r1
            return r0
        L_0x009e:
            r1 = 0
            goto L_0x009c
        L_0x00a0:
            r1 = r8
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.f.a(com.uc.c.ca, int, int, int, int, int, int, boolean, int):int");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.f.a(com.uc.c.ca, int, int, int, int, int, int, boolean, int):int
     arg types: [com.uc.c.ca, int, int, int, int, int, int, int, int]
     candidates:
      com.uc.c.f.a(com.uc.c.bk, int, int, boolean, int, int, b.a.a.a.f, int, int):void
      com.uc.c.f.a(com.uc.c.bk, byte[], com.uc.c.ca, int, int, int, int, int, int):void
      com.uc.c.f.a(com.uc.c.ca, int, int, int, int, int, int, boolean, int):int */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x0547 A[Catch:{ Exception -> 0x06e0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x05b7 A[Catch:{ Exception -> 0x06e0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:236:0x07a2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r39, int r40, com.uc.c.ca r41) {
        /*
            r38 = this;
            r0 = r38
            short r0 = r0.cI
            r5 = r0
            r6 = 32767(0x7fff, float:4.5916E-41)
            if (r5 != r6) goto L_0x000a
        L_0x0009:
            return
        L_0x000a:
            r0 = r38
            short r0 = r0.cI
            r5 = r0
            r6 = 32767(0x7fff, float:4.5916E-41)
            r0 = r6
            r1 = r38
            r1.cI = r0
            r0 = r38
            byte[] r0 = r0.cH
            r6 = r0
            int r6 = d(r6)
            r7 = 0
            r8 = -1
            r0 = r39
            r1 = r8
            if (r0 == r1) goto L_0x0163
            r30 = r39
        L_0x0028:
            r0 = r6
            r1 = r30
            if (r0 == r1) goto L_0x002d
        L_0x002d:
            r0 = r38
            byte[] r0 = r0.cH
            r6 = r0
            r0 = r38
            byte[] r0 = r0.cH
            r8 = r0
            int r8 = e(r8)
            r0 = r6
            r1 = r30
            r2 = r8
            b(r0, r1, r2)
            r6 = -32768(0xffffffffffff8000, float:NaN)
            if (r5 != r6) goto L_0x0181
            r5 = 32767(0x7fff, float:4.5916E-41)
        L_0x0048:
            r6 = 0
            r8 = 0
            r10 = 0
            r11 = -1
            r0 = r38
            byte[] r0 = r0.cH
            r9 = r0
            r12 = 0
            r0 = r41
            r1 = r9
            r2 = r12
            int r9 = r0.L(r1, r2)
            int r6 = r6 + r9
            r0 = r38
            byte[] r0 = r0.cH
            r9 = r0
            r0 = r41
            r1 = r9
            int r9 = r0.aE(r1)
            int r6 = r6 + r9
            r0 = r38
            byte[] r0 = r0.cH
            r9 = r0
            r0 = r41
            r1 = r9
            r2 = r30
            int r9 = r0.O(r1, r2)
            int r8 = r8 + r9
            r0 = r38
            byte[] r0 = r0.cH
            r9 = r0
            r0 = r41
            r1 = r9
            int r9 = r0.aH(r1)
            int r8 = r8 + r9
            int r9 = r30 - r8
            r0 = r38
            byte[] r0 = r0.cH
            r12 = r0
            r0 = r41
            r1 = r12
            r2 = r30
            int r12 = r0.M(r1, r2)
            int r9 = r9 - r12
            r0 = r38
            byte[] r0 = r0.cH
            r12 = r0
            r0 = r41
            r1 = r12
            int r12 = r0.aF(r1)
            int r9 = r9 - r12
            r0 = r38
            byte[] r0 = r0.cH
            r12 = r0
            r13 = 8
            int r12 = g(r12, r13)
            r13 = -1
            if (r12 != r13) goto L_0x00b1
            r12 = 1
        L_0x00b1:
            r0 = r38
            byte[] r0 = r0.cH
            r13 = r0
            r14 = 7
            int r13 = g(r13, r14)
            r14 = -1
            if (r13 != r14) goto L_0x00c0
            r13 = 16
        L_0x00c0:
            r13 = r13 | r12
            r0 = r38
            byte[] r0 = r0.cH
            r12 = r0
            r0 = r41
            r1 = r12
            int r31 = r0.aI(r1)
            r0 = r38
            byte[] r0 = r0.cH
            r12 = r0
            r14 = 0
            byte r12 = r12[r14]
            r14 = 19
            if (r12 != r14) goto L_0x08ef
            r0 = r38
            byte[] r0 = r0.cH
            r12 = r0
            byte[] r14 = com.uc.c.ca.bLs
            r15 = 0
            int r12 = b(r12, r14, r15)
            r14 = 1
            if (r12 != r14) goto L_0x08ef
            r12 = 1
            r0 = r40
            r1 = r12
            boolean r12 = com.uc.c.bc.aL(r0, r1)
            if (r12 != 0) goto L_0x08ef
            r12 = 2
            r0 = r40
            r1 = r12
            int r12 = com.uc.c.bc.aK(r0, r1)
            r14 = r12
        L_0x00fb:
            r0 = r38
            byte[] r0 = r0.cH
            r12 = r0
            r15 = 0
            byte r12 = r12[r15]
            r15 = 21
            if (r12 == r15) goto L_0x010e
            r12 = 4
            boolean r12 = com.uc.c.bc.aL(r14, r12)
            if (r12 == 0) goto L_0x018a
        L_0x010e:
            r12 = 2
            boolean r12 = com.uc.c.bc.aL(r14, r12)
            if (r12 == 0) goto L_0x018a
            r12 = -1
            r0 = r39
            r1 = r12
            if (r0 != r1) goto L_0x018a
            r0 = r7
            r1 = r30
            if (r0 == r1) goto L_0x018a
            r7 = 0
        L_0x0121:
            r15 = 0
            r16 = 0
            r0 = r41
            java.util.Vector r0 = r0.bHi
            r12 = r0
            int r12 = r12.size()
            r17 = 1
            r19 = r5
            r32 = r17
            r33 = r16
            r34 = r15
            r35 = r11
            r5 = r7
            r11 = r9
            r7 = r6
            r6 = r10
        L_0x013d:
            r0 = r38
            short r0 = r0.cF
            r10 = r0
            r0 = r19
            r1 = r10
            if (r0 >= r1) goto L_0x0838
            r0 = r38
            r1 = r19
            byte[] r17 = com.uc.c.ca.c(r0, r1)
            r10 = 0
            byte r10 = r17[r10]
            boolean r10 = com.uc.c.ca.iW(r10)
            if (r10 == 0) goto L_0x015e
            boolean r10 = com.uc.c.ca.aC(r17)
            if (r10 != 0) goto L_0x018c
        L_0x015e:
            int r10 = r19 + 1
            r19 = r10
            goto L_0x013d
        L_0x0163:
            r0 = r38
            r1 = r41
            int r7 = r0.a(r1)
            r0 = r38
            byte[] r0 = r0.cH
            r8 = r0
            r0 = r41
            r1 = r8
            r2 = r7
            int r8 = r0.P(r1, r2)
            if (r8 <= 0) goto L_0x08f3
            if (r8 >= r7) goto L_0x08f3
            r7 = r8
            r30 = r8
            goto L_0x0028
        L_0x0181:
            r0 = r38
            r1 = r5
            int r5 = r0.t(r1)
            goto L_0x0048
        L_0x018a:
            r7 = -1
            goto L_0x0121
        L_0x018c:
            r10 = 0
            byte r10 = r17[r10]
            switch(r10) {
                case 0: goto L_0x01ac;
                case 1: goto L_0x0192;
                case 2: goto L_0x0344;
                default: goto L_0x0192;
            }
        L_0x0192:
            r10 = 0
            byte r10 = r17[r10]
            r15 = 25
            if (r10 != r15) goto L_0x0369
            byte[] r10 = com.uc.c.ca.bLC
            r15 = 1
            r0 = r17
            r1 = r10
            r2 = r15
            int r10 = b(r0, r1, r2)
            r15 = 1
            if (r10 != r15) goto L_0x0369
            int r10 = r19 + 1
            r19 = r10
            goto L_0x013d
        L_0x01ac:
            if (r34 != 0) goto L_0x08dc
            r0 = r38
            int r0 = r0.cJ
            r10 = r0
            r0 = r17
            r1 = r41
            r2 = r10
            char[] r10 = a(r0, r1, r2)
            r15 = 0
            r16 = 1
            r0 = r17
            r1 = r16
            int r16 = f(r0, r1)
            r18 = 29
            int r18 = f(r17, r18)
            r20 = -1
            r0 = r18
            r1 = r20
            if (r0 != r1) goto L_0x01d7
            r18 = 0
        L_0x01d7:
            r20 = -1
            r0 = r16
            r1 = r20
            if (r0 != r1) goto L_0x01e5
            r0 = r41
            int r0 = r0.bFQ
            r16 = r0
        L_0x01e5:
            r0 = r18
            r1 = r16
            b.a.a.a.k r16 = com.uc.b.e.I(r0, r1)
            r18 = r15
            r15 = r16
            r16 = r10
        L_0x01f3:
            r0 = r16
            int r0 = r0.length
            r10 = r0
            r0 = r18
            r1 = r10
            if (r0 != r1) goto L_0x02a9
            r10 = 0
            r20 = r10
        L_0x01ff:
            r0 = r20
            r1 = r11
            if (r0 <= r1) goto L_0x08eb
            if (r6 <= 0) goto L_0x08e8
            r0 = r6
            r1 = r31
            int r6 = java.lang.Math.max(r0, r1)
            r10 = r6
        L_0x020e:
            if (r5 < 0) goto L_0x08e4
            int r5 = java.lang.Math.max(r5, r9)
            r21 = r5
        L_0x0216:
            r5 = r38
            r6 = r41
            int r5 = r5.a(r6, r7, r8, r9, r10, r11, r12, r13)
            r6 = 0
            r7 = r9
            r10 = r5
            r5 = r21
        L_0x0223:
            r11 = 0
            int r21 = r15.uN()
            r0 = r6
            r1 = r21
            int r6 = java.lang.Math.max(r0, r1)
            r21 = r7
            r7 = r32
            r37 = r18
            r18 = r20
            r20 = r37
        L_0x0239:
            r0 = r16
            int r0 = r0.length
            r22 = r0
            r0 = r20
            r1 = r22
            if (r0 != r1) goto L_0x02cc
            r0 = r41
            java.util.Vector r0 = r0.bHj
            r7 = r0
            int r16 = r15.uN()
            r0 = r20
            r1 = r11
            r2 = r16
            byte[] r11 = a(r0, r1, r2)
            r7.addElement(r11)
            int r7 = r19 + 1
            r11 = 1
            r16 = 0
            r0 = r38
            int r0 = r0.cJ
            r18 = r0
            r0 = r41
            r1 = r17
            r2 = r18
            boolean r18 = a(r0, r1, r2)
            if (r18 == 0) goto L_0x02b3
            r0 = r41
            java.util.Vector r0 = r0.bHi
            r17 = r0
            r0 = r41
            java.util.Vector r0 = r0.bHj
            r18 = r0
            java.lang.Object r18 = r18.lastElement()
            r17.addElement(r18)
            if (r5 < 0) goto L_0x0289
            int r5 = java.lang.Math.max(r5, r9)
        L_0x0289:
            r0 = r41
            java.util.Vector r0 = r0.bHj
            r17 = r0
            r17.removeAllElements()
            r0 = r41
            byte r0 = r0.bHp
            r17 = r0
            int r17 = r21 - r17
            r19 = r7
            r32 = r11
            r33 = r15
            r34 = r16
            r35 = r20
            r11 = r17
            r7 = r10
            goto L_0x013d
        L_0x02a9:
            char r10 = r16[r18]
            int r10 = r15.b(r10)
            r20 = r10
            goto L_0x01ff
        L_0x02b3:
            r0 = r41
            java.util.Vector r0 = r0.bHi
            r18 = r0
            r0 = r18
            r1 = r17
            r0.addElement(r1)
            if (r5 < 0) goto L_0x0289
            int r17 = r9 - r21
            r0 = r5
            r1 = r17
            int r5 = java.lang.Math.max(r0, r1)
            goto L_0x0289
        L_0x02cc:
            r0 = r18
            r1 = r21
            if (r0 > r1) goto L_0x02f6
            int r7 = r21 - r18
            int r11 = r11 + r18
            int r20 = r20 + 1
            r0 = r16
            int r0 = r0.length
            r21 = r0
            r0 = r20
            r1 = r21
            if (r0 == r1) goto L_0x02ec
            char r18 = r16[r20]
            r0 = r15
            r1 = r18
            int r18 = r0.b(r1)
        L_0x02ec:
            r21 = 0
            r37 = r21
            r21 = r7
            r7 = r37
            goto L_0x0239
        L_0x02f6:
            r0 = r41
            java.util.Vector r0 = r0.bHj
            r17 = r0
            int r18 = r15.uN()
            r0 = r20
            r1 = r11
            r2 = r18
            byte[] r11 = a(r0, r1, r2)
            r0 = r17
            r1 = r11
            r0.addElement(r1)
            if (r7 == 0) goto L_0x0322
            int r11 = r19 + 1
            r19 = r11
            r32 = r7
            r33 = r15
            r34 = r16
            r35 = r20
            r11 = r21
            r7 = r10
            goto L_0x013d
        L_0x0322:
            r0 = r41
            java.util.Vector r0 = r0.bHi
            r11 = r0
            r0 = r41
            java.util.Vector r0 = r0.bHj
            r17 = r0
            java.lang.Object r17 = r17.lastElement()
            r0 = r11
            r1 = r17
            r0.addElement(r1)
            r32 = r7
            r33 = r15
            r34 = r16
            r35 = r20
            r11 = r21
            r7 = r10
            goto L_0x013d
        L_0x0344:
            if (r6 <= 0) goto L_0x08d9
            r0 = r6
            r1 = r31
            int r6 = java.lang.Math.max(r0, r1)
            r10 = r6
        L_0x034e:
            if (r5 < 0) goto L_0x08d6
            int r6 = r9 - r11
            int r5 = java.lang.Math.max(r5, r6)
            r15 = r5
        L_0x0357:
            r5 = r38
            r6 = r41
            int r5 = r5.a(r6, r7, r8, r9, r10, r11, r12, r13)
            r6 = 0
            int r7 = r19 + 1
            r19 = r7
            r11 = r9
            r7 = r5
            r5 = r15
            goto L_0x013d
        L_0x0369:
            r10 = 0
            byte r10 = r17[r10]
            boolean r10 = com.uc.c.ca.jb(r10)
            if (r10 == 0) goto L_0x08d2
            r10 = r14 & 1
            r15 = 1
            if (r10 != r15) goto L_0x047c
            r10 = 0
            byte r10 = r17[r10]
            r15 = 19
            if (r10 != r15) goto L_0x047c
            byte[] r10 = com.uc.c.ca.bLs
            r15 = 0
            r0 = r17
            r1 = r10
            r2 = r15
            int r10 = b(r0, r1, r2)
            r15 = 1
            if (r10 != r15) goto L_0x047c
            r0 = r38
            java.lang.Object[] r0 = r0.cE
            r10 = r0
            r40 = r10[r19]
            com.uc.c.f r40 = (com.uc.c.f) r40
            r0 = r40
            r1 = r39
            r2 = r14
            r3 = r41
            r0.b(r1, r2, r3)
            r36 = r14
        L_0x03a1:
            r10 = 0
            byte r10 = r17[r10]
            r14 = 21
            if (r10 == r14) goto L_0x04a8
            r10 = r36 & 4
            if (r10 != 0) goto L_0x04a8
            r10 = 0
            byte r10 = r17[r10]
            boolean r10 = com.uc.c.ca.jb(r10)
            if (r10 == 0) goto L_0x04a8
            if (r6 <= 0) goto L_0x08cc
            r0 = r6
            r1 = r31
            int r6 = java.lang.Math.max(r0, r1)
            r10 = r6
        L_0x03bf:
            if (r5 < 0) goto L_0x08c9
            int r6 = d(r17)
            int r5 = java.lang.Math.max(r5, r6)
            r14 = r5
        L_0x03ca:
            r5 = r38
            r6 = r41
            int r5 = r5.a(r6, r7, r8, r9, r10, r11, r12, r13)
            if (r19 == 0) goto L_0x03e2
            r0 = r38
            java.lang.Object[] r0 = r0.cE
            r6 = r0
            r7 = 1
            int r7 = r19 - r7
            r6 = r6[r7]
            boolean r6 = r6 instanceof com.uc.c.f
            if (r6 != 0) goto L_0x03ed
        L_0x03e2:
            r6 = 0
            r0 = r41
            r1 = r17
            r2 = r6
            int r6 = r0.H(r1, r2)
            int r5 = r5 + r6
        L_0x03ed:
            r0 = r41
            r1 = r17
            r2 = r30
            int r6 = r0.I(r1, r2)
            r0 = r41
            r1 = r17
            r2 = r30
            int r7 = r0.K(r1, r2)
            int r7 = r7 + r8
            int r10 = d(r17)
            r11 = 28
            r0 = r17
            r1 = r11
            int r11 = g(r0, r1)
            r15 = 4
            if (r11 != r15) goto L_0x08c6
            int r11 = r7 + r10
            int r11 = r11 + r6
            r0 = r11
            r1 = r30
            if (r0 >= r1) goto L_0x08c6
            int r11 = r30 - r7
            int r10 = r11 - r10
            int r6 = r10 - r6
            int r6 = r6 >> 1
            int r6 = r6 + r7
        L_0x0423:
            r0 = r17
            r1 = r6
            r2 = r5
            a(r0, r1, r2)
            int r6 = e(r17)
            int r5 = r5 + r6
            r6 = 0
            r0 = r41
            r1 = r17
            r2 = r6
            int r6 = r0.J(r1, r2)
            r0 = r38
            short r0 = r0.cF
            r7 = r0
            r10 = 1
            int r7 = r7 - r10
            r0 = r19
            r1 = r7
            if (r0 == r1) goto L_0x046f
            r0 = r38
            java.lang.Object[] r0 = r0.cE
            r7 = r0
            int r10 = r19 + 1
            r7 = r7[r10]
            boolean r7 = r7 instanceof com.uc.c.f
            if (r7 == 0) goto L_0x046f
            r0 = r38
            java.lang.Object[] r0 = r0.cE
            r7 = r0
            int r10 = r19 + 1
            r40 = r7[r10]
            com.uc.c.f r40 = (com.uc.c.f) r40
            r0 = r40
            byte[] r0 = r0.cH
            r7 = r0
            r10 = 0
            r0 = r41
            r1 = r7
            r2 = r10
            int r7 = r0.H(r1, r2)
            int r6 = java.lang.Math.max(r6, r7)
        L_0x046f:
            int r5 = r5 + r6
            r6 = 0
            int r7 = r19 + 1
            r19 = r7
            r11 = r9
            r7 = r5
            r5 = r14
            r14 = r36
            goto L_0x013d
        L_0x047c:
            r0 = r38
            byte[] r0 = r0.cH
            r10 = r0
            r15 = 0
            byte r10 = r10[r15]
            r15 = 21
            if (r10 != r15) goto L_0x08cf
            r10 = 2
            boolean r10 = com.uc.c.bc.aL(r14, r10)
            if (r10 == 0) goto L_0x08cf
            r10 = r14 | 4
        L_0x0491:
            r0 = r38
            java.lang.Object[] r0 = r0.cE
            r14 = r0
            r40 = r14[r19]
            com.uc.c.f r40 = (com.uc.c.f) r40
            r0 = r40
            r1 = r39
            r2 = r10
            r3 = r41
            r0.a(r1, r2, r3)
            r36 = r10
            goto L_0x03a1
        L_0x04a8:
            r10 = 0
            byte r10 = r17[r10]
            boolean r10 = com.uc.c.ca.jc(r10)
            if (r10 == 0) goto L_0x07a7
            r10 = 0
            r14 = 0
            r15 = 0
            byte r15 = r17[r15]     // Catch:{ Exception -> 0x06e0 }
            r16 = 3
            r0 = r15
            r1 = r16
            if (r0 != r1) goto L_0x0679
            byte[] r10 = com.uc.c.ca.bLh     // Catch:{ Exception -> 0x06e0 }
            r15 = 0
            r0 = r17
            r1 = r10
            r2 = r15
            int r10 = b(r0, r1, r2)     // Catch:{ Exception -> 0x06e0 }
            r37 = r14
            r14 = r10
            r10 = r37
        L_0x04cd:
            r0 = r41
            java.util.HashMap r0 = r0.bGh     // Catch:{ Exception -> 0x06e0 }
            r15 = r0
            r0 = r38
            int r0 = r0.cJ     // Catch:{ Exception -> 0x06e0 }
            r16 = r0
            java.lang.Integer r16 = java.lang.Integer.valueOf(r16)     // Catch:{ Exception -> 0x06e0 }
            java.lang.Object r40 = r15.get(r16)     // Catch:{ Exception -> 0x06e0 }
            java.util.Vector r40 = (java.util.Vector) r40     // Catch:{ Exception -> 0x06e0 }
            r0 = r40
            r1 = r14
            java.lang.Object r40 = r0.elementAt(r1)     // Catch:{ Exception -> 0x06e0 }
            byte[] r40 = (byte[]) r40     // Catch:{ Exception -> 0x06e0 }
            byte[] r40 = (byte[]) r40     // Catch:{ Exception -> 0x06e0 }
            if (r40 == 0) goto L_0x04fc
            r0 = r40
            int r0 = r0.length     // Catch:{ Exception -> 0x06e0 }
            r14 = r0
            r15 = 1
            if (r14 != r15) goto L_0x04fc
            r14 = 0
            byte r14 = r40[r14]     // Catch:{ Exception -> 0x06e0 }
            r15 = 1
            if (r14 == r15) goto L_0x05d8
        L_0x04fc:
            if (r40 == 0) goto L_0x0696
            int[] r14 = com.uc.c.bc.ao(r40)     // Catch:{ Exception -> 0x06e0 }
        L_0x0502:
            byte[] r15 = com.uc.c.ca.bLh     // Catch:{ Exception -> 0x06e0 }
            r16 = 3
            r0 = r17
            r1 = r15
            r2 = r16
            int r15 = b(r0, r1, r2)     // Catch:{ Exception -> 0x06e0 }
            byte r15 = (byte) r15     // Catch:{ Exception -> 0x06e0 }
            r16 = 0
            if (r40 == 0) goto L_0x08b7
            r18 = 1
            r0 = r15
            r1 = r18
            if (r0 == r1) goto L_0x08b7
            r18 = 0
            if (r10 != 0) goto L_0x08bb
            r10 = 14
            r0 = r17
            r1 = r10
            int r10 = g(r0, r1)     // Catch:{ Exception -> 0x06e0 }
            r20 = 15
            r0 = r17
            r1 = r20
            int r20 = g(r0, r1)     // Catch:{ Exception -> 0x06e0 }
            if (r10 <= 0) goto L_0x08bb
            if (r20 <= 0) goto L_0x08bb
            r18 = 0
            r14[r18] = r10     // Catch:{ Exception -> 0x06e0 }
            r10 = 1
            r14[r10] = r20     // Catch:{ Exception -> 0x06e0 }
            r10 = 1
        L_0x053e:
            r18 = 0
            r18 = r14[r18]     // Catch:{ Exception -> 0x06e0 }
            r0 = r18
            r1 = r9
            if (r0 >= r1) goto L_0x07a2
            r0 = r41
            float r0 = r0.bGy     // Catch:{ Exception -> 0x06e0 }
            r18 = r0
            r20 = 1065353216(0x3f800000, float:1.0)
            int r18 = (r18 > r20 ? 1 : (r18 == r20 ? 0 : -1))
            if (r18 <= 0) goto L_0x0732
            r10 = 0
            r16 = 0
            r16 = r14[r16]     // Catch:{ Exception -> 0x06e0 }
            r0 = r16
            float r0 = (float) r0     // Catch:{ Exception -> 0x06e0 }
            r16 = r0
            r0 = r41
            float r0 = r0.bGy     // Catch:{ Exception -> 0x06e0 }
            r18 = r0
            float r16 = r16 * r18
            r0 = r16
            int r0 = (int) r0     // Catch:{ Exception -> 0x06e0 }
            r16 = r0
            r14[r10] = r16     // Catch:{ Exception -> 0x06e0 }
            r10 = 1
            r16 = 1
            r16 = r14[r16]     // Catch:{ Exception -> 0x06e0 }
            r0 = r16
            float r0 = (float) r0     // Catch:{ Exception -> 0x06e0 }
            r16 = r0
            r0 = r41
            float r0 = r0.bGy     // Catch:{ Exception -> 0x06e0 }
            r18 = r0
            float r16 = r16 * r18
            r0 = r16
            int r0 = (int) r0     // Catch:{ Exception -> 0x06e0 }
            r16 = r0
            r14[r10] = r16     // Catch:{ Exception -> 0x06e0 }
            r10 = 0
            r10 = r14[r10]     // Catch:{ Exception -> 0x06e0 }
            if (r10 <= r9) goto L_0x05af
            r10 = 1
            r16 = 1
            r16 = r14[r16]     // Catch:{ Exception -> 0x06e0 }
            r0 = r16
            float r0 = (float) r0     // Catch:{ Exception -> 0x06e0 }
            r16 = r0
            r0 = r9
            float r0 = (float) r0     // Catch:{ Exception -> 0x06e0 }
            r18 = r0
            r20 = 0
            r20 = r14[r20]     // Catch:{ Exception -> 0x06e0 }
            r0 = r20
            float r0 = (float) r0     // Catch:{ Exception -> 0x06e0 }
            r20 = r0
            float r18 = r18 / r20
            float r16 = r16 * r18
            r0 = r16
            int r0 = (int) r0     // Catch:{ Exception -> 0x06e0 }
            r16 = r0
            r14[r10] = r16     // Catch:{ Exception -> 0x06e0 }
            r10 = 0
            r14[r10] = r9     // Catch:{ Exception -> 0x06e0 }
        L_0x05af:
            r10 = 1
        L_0x05b0:
            r16 = 1
            r0 = r15
            r1 = r16
            if (r0 == r1) goto L_0x05d8
            r15 = 0
            r15 = r14[r15]     // Catch:{ Exception -> 0x06e0 }
            r16 = 1
            r14 = r14[r16]     // Catch:{ Exception -> 0x06e0 }
            r0 = r17
            r1 = r15
            r2 = r14
            b(r0, r1, r2)     // Catch:{ Exception -> 0x06e0 }
            r14 = 1
            if (r10 != r14) goto L_0x05d8
            byte[] r10 = com.uc.c.ca.bLh     // Catch:{ Exception -> 0x06e0 }
            r14 = 3
            r15 = 1
            r16 = 0
            r0 = r17
            r1 = r10
            r2 = r14
            r3 = r15
            r4 = r16
            a(r0, r1, r2, r3, r4)     // Catch:{ Exception -> 0x06e0 }
        L_0x05d8:
            r10 = 0
            byte r10 = r17[r10]
            boolean r10 = com.uc.c.ca.iX(r10)
            if (r10 == 0) goto L_0x05ec
            int r10 = e(r17)
            r0 = r17
            r1 = r9
            r2 = r10
            b(r0, r1, r2)
        L_0x05ec:
            r10 = 0
            byte r10 = r17[r10]
            boolean r10 = com.uc.c.ca.iY(r10)
            if (r10 == 0) goto L_0x0614
            int r10 = d(r17)
            if (r10 <= r9) goto L_0x0614
            r10 = 0
            byte r10 = r17[r10]
            r14 = 61
            if (r10 == r14) goto L_0x0609
            r10 = 0
            byte r10 = r17[r10]
            r14 = 52
            if (r10 != r14) goto L_0x0826
        L_0x0609:
            int r10 = e(r17)
            r0 = r17
            r1 = r11
            r2 = r10
            b(r0, r1, r2)
        L_0x0614:
            int r10 = d(r17)
            if (r10 <= r11) goto L_0x08b0
            if (r6 <= 0) goto L_0x08ac
            r0 = r6
            r1 = r31
            int r6 = java.lang.Math.max(r0, r1)
            r25 = r6
        L_0x0625:
            if (r5 < 0) goto L_0x062b
            int r5 = java.lang.Math.max(r5, r9)
        L_0x062b:
            r6 = 0
            byte r6 = r17[r6]
            r14 = 62
            if (r6 == r14) goto L_0x0833
            r6 = 1
            r28 = r6
        L_0x0635:
            r20 = r38
            r21 = r41
            r22 = r7
            r23 = r8
            r24 = r9
            r26 = r11
            r27 = r12
            r29 = r13
            int r6 = r20.a(r21, r22, r23, r24, r25, r26, r27, r28, r29)
            r7 = 0
            r11 = r6
            r6 = r7
            r7 = r9
        L_0x064d:
            int r14 = e(r17)
            int r6 = java.lang.Math.max(r6, r14)
            r0 = r41
            java.util.Vector r0 = r0.bHi
            r14 = r0
            r0 = r14
            r1 = r17
            r0.addElement(r1)
            if (r5 < 0) goto L_0x0663
            int r5 = r5 + r10
        L_0x0663:
            if (r10 <= 0) goto L_0x066c
            int r7 = r7 - r10
            r0 = r41
            byte r0 = r0.bHp
            r10 = r0
            int r7 = r7 - r10
        L_0x066c:
            int r10 = r19 + 1
            r19 = r10
            r14 = r36
            r37 = r11
            r11 = r7
            r7 = r37
            goto L_0x013d
        L_0x0679:
            r15 = 0
            byte r15 = r17[r15]     // Catch:{ Exception -> 0x06e0 }
            r16 = 17
            r0 = r15
            r1 = r16
            if (r0 != r1) goto L_0x08bf
            byte[] r10 = com.uc.c.ca.bLw     // Catch:{ Exception -> 0x06e0 }
            r14 = 0
            r0 = r17
            r1 = r10
            r2 = r14
            int r10 = b(r0, r1, r2)     // Catch:{ Exception -> 0x06e0 }
            r14 = 1
            r37 = r14
            r14 = r10
            r10 = r37
            goto L_0x04cd
        L_0x0696:
            r0 = r41
            int r0 = r0.bFP     // Catch:{ Exception -> 0x06e0 }
            r14 = r0
            r15 = 32
            boolean r14 = com.uc.c.bc.aL(r14, r15)     // Catch:{ Exception -> 0x06e0 }
            if (r14 == 0) goto L_0x06ac
            r0 = r38
            r1 = r19
            r2 = r17
            r0.b(r1, r2)     // Catch:{ Exception -> 0x06e0 }
        L_0x06ac:
            r14 = 2
            int[] r14 = new int[r14]     // Catch:{ Exception -> 0x06e0 }
            r15 = 0
            r16 = 0
            byte r16 = r17[r16]     // Catch:{ Exception -> 0x06e0 }
            r18 = 3
            r0 = r16
            r1 = r18
            if (r0 != r1) goto L_0x06e3
            byte[] r15 = com.uc.c.ca.bLh     // Catch:{ Exception -> 0x06e0 }
            r16 = 1
            r0 = r17
            r1 = r15
            r2 = r16
            int r15 = b(r0, r1, r2)     // Catch:{ Exception -> 0x06e0 }
            r0 = r17
            r1 = r15
            java.lang.String r15 = com.uc.c.bc.w(r0, r1)     // Catch:{ Exception -> 0x06e0 }
        L_0x06d0:
            r16 = 22
            boolean r18 = com.uc.c.bc.by(r15)     // Catch:{ Exception -> 0x06e0 }
            if (r18 == 0) goto L_0x0704
            r15 = 0
            r14[r15] = r16     // Catch:{ Exception -> 0x06e0 }
            r15 = 1
            r14[r15] = r16     // Catch:{ Exception -> 0x06e0 }
            goto L_0x0502
        L_0x06e0:
            r10 = move-exception
            goto L_0x05d8
        L_0x06e3:
            r16 = 0
            byte r16 = r17[r16]     // Catch:{ Exception -> 0x06e0 }
            r18 = 17
            r0 = r16
            r1 = r18
            if (r0 != r1) goto L_0x06d0
            byte[] r15 = com.uc.c.ca.bLw     // Catch:{ Exception -> 0x06e0 }
            r16 = 1
            r0 = r17
            r1 = r15
            r2 = r16
            int r15 = b(r0, r1, r2)     // Catch:{ Exception -> 0x06e0 }
            r0 = r17
            r1 = r15
            java.lang.String r15 = com.uc.c.bc.w(r0, r1)     // Catch:{ Exception -> 0x06e0 }
            goto L_0x06d0
        L_0x0704:
            r18 = 0
            r0 = r41
            int r0 = r0.bFQ     // Catch:{ Exception -> 0x06e0 }
            r20 = r0
            r0 = r20
            r1 = r15
            int r15 = com.uc.b.e.f(r0, r1)     // Catch:{ Exception -> 0x06e0 }
            int r15 = r15 + r16
            int r15 = java.lang.Math.min(r15, r9)     // Catch:{ Exception -> 0x06e0 }
            r14[r18] = r15     // Catch:{ Exception -> 0x06e0 }
            r15 = 1
            r0 = r41
            int r0 = r0.bFQ     // Catch:{ Exception -> 0x06e0 }
            r18 = r0
            int r18 = com.uc.b.e.bm(r18)     // Catch:{ Exception -> 0x06e0 }
            r0 = r16
            r1 = r18
            int r16 = java.lang.Math.max(r0, r1)     // Catch:{ Exception -> 0x06e0 }
            r14[r15] = r16     // Catch:{ Exception -> 0x06e0 }
            goto L_0x0502
        L_0x0732:
            r0 = r41
            float r0 = r0.bGA     // Catch:{ Exception -> 0x06e0 }
            r18 = r0
            r20 = 1065353216(0x3f800000, float:1.0)
            int r18 = (r18 > r20 ? 1 : (r18 == r20 ? 0 : -1))
            if (r18 <= 0) goto L_0x079d
            r10 = 0
            r16 = 0
            r16 = r14[r16]     // Catch:{ Exception -> 0x06e0 }
            r0 = r16
            float r0 = (float) r0     // Catch:{ Exception -> 0x06e0 }
            r16 = r0
            r0 = r41
            float r0 = r0.bGA     // Catch:{ Exception -> 0x06e0 }
            r18 = r0
            float r16 = r16 * r18
            r0 = r16
            int r0 = (int) r0     // Catch:{ Exception -> 0x06e0 }
            r16 = r0
            r14[r10] = r16     // Catch:{ Exception -> 0x06e0 }
            r10 = 1
            r16 = 1
            r16 = r14[r16]     // Catch:{ Exception -> 0x06e0 }
            r0 = r16
            float r0 = (float) r0     // Catch:{ Exception -> 0x06e0 }
            r16 = r0
            r0 = r41
            float r0 = r0.bGA     // Catch:{ Exception -> 0x06e0 }
            r18 = r0
            float r16 = r16 * r18
            r0 = r16
            int r0 = (int) r0     // Catch:{ Exception -> 0x06e0 }
            r16 = r0
            r14[r10] = r16     // Catch:{ Exception -> 0x06e0 }
            r10 = 0
            r10 = r14[r10]     // Catch:{ Exception -> 0x06e0 }
            if (r10 <= r9) goto L_0x079a
            r10 = 1
            r16 = 1
            r16 = r14[r16]     // Catch:{ Exception -> 0x06e0 }
            r0 = r16
            float r0 = (float) r0     // Catch:{ Exception -> 0x06e0 }
            r16 = r0
            r0 = r9
            float r0 = (float) r0     // Catch:{ Exception -> 0x06e0 }
            r18 = r0
            r20 = 0
            r20 = r14[r20]     // Catch:{ Exception -> 0x06e0 }
            r0 = r20
            float r0 = (float) r0     // Catch:{ Exception -> 0x06e0 }
            r20 = r0
            float r18 = r18 / r20
            float r16 = r16 * r18
            r0 = r16
            int r0 = (int) r0     // Catch:{ Exception -> 0x06e0 }
            r16 = r0
            r14[r10] = r16     // Catch:{ Exception -> 0x06e0 }
            r10 = 0
            r14[r10] = r9     // Catch:{ Exception -> 0x06e0 }
        L_0x079a:
            r10 = 1
            goto L_0x05b0
        L_0x079d:
            if (r10 == 0) goto L_0x08b7
            r10 = 1
            goto L_0x05b0
        L_0x07a2:
            if (r10 == 0) goto L_0x08b7
            r10 = 1
            goto L_0x05b0
        L_0x07a7:
            r10 = 0
            byte r10 = r17[r10]
            r14 = 48
            if (r10 != r14) goto L_0x05d8
            byte[] r10 = com.uc.c.ca.bLm
            r14 = 0
            r0 = r17
            r1 = r10
            r2 = r14
            int r10 = b(r0, r1, r2)
            r0 = r41
            java.util.HashMap r0 = r0.bGh
            r14 = r0
            r0 = r38
            int r0 = r0.cJ
            r15 = r0
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)
            java.lang.Object r40 = r14.get(r15)
            java.util.Vector r40 = (java.util.Vector) r40
            r0 = r40
            r1 = r10
            java.lang.Object r40 = r0.elementAt(r1)
            byte[] r40 = (byte[]) r40
            byte[] r40 = (byte[]) r40
            if (r40 == 0) goto L_0x07e1
            r0 = r40
            int r0 = r0.length
            r10 = r0
            r14 = 1
            if (r10 == r14) goto L_0x05d8
        L_0x07e1:
            if (r40 == 0) goto L_0x080e
            r0 = r40
            int r0 = r0.length
            r10 = r0
            r14 = 1
            if (r10 <= r14) goto L_0x080e
            java.lang.String[] r10 = com.uc.c.bc.al(r40)
            if (r10 == 0) goto L_0x05d8
            r14 = 0
            r14 = r10[r14]
            if (r14 == 0) goto L_0x05d8
            r14 = 1
            r14 = r10[r14]
            if (r14 == 0) goto L_0x05d8
            r14 = 0
            r15 = r10[r14]
            r14 = 1
            r16 = r10[r14]
            r14 = r41
            r18 = r38
            boolean r10 = r14.a(r15, r16, r17, r18, r19)
            if (r10 == 0) goto L_0x05d8
            r14 = r36
            goto L_0x013d
        L_0x080e:
            r0 = r41
            int r0 = r0.bFP
            r10 = r0
            r14 = 32
            boolean r10 = com.uc.c.bc.aL(r10, r14)
            if (r10 == 0) goto L_0x05d8
            r0 = r38
            r1 = r19
            r2 = r17
            r0.b(r1, r2)
            goto L_0x05d8
        L_0x0826:
            int r10 = e(r17)
            r0 = r17
            r1 = r9
            r2 = r10
            b(r0, r1, r2)
            goto L_0x0614
        L_0x0833:
            r6 = 0
            r28 = r6
            goto L_0x0635
        L_0x0838:
            if (r6 <= 0) goto L_0x08a9
            r0 = r6
            r1 = r31
            int r6 = java.lang.Math.max(r0, r1)
            r19 = r6
        L_0x0843:
            r22 = 0
            r14 = r38
            r15 = r41
            r16 = r7
            r17 = r8
            r18 = r9
            r20 = r11
            r21 = r12
            r23 = r13
            int r6 = r14.a(r15, r16, r17, r18, r19, r20, r21, r22, r23)
            r0 = r38
            byte[] r0 = r0.cH
            r7 = r0
            r8 = 0
            r0 = r41
            r1 = r7
            r2 = r8
            int r7 = r0.N(r1, r2)
            int r6 = r6 + r7
            r0 = r38
            byte[] r0 = r0.cH
            r7 = r0
            r0 = r41
            r1 = r7
            int r7 = r0.aG(r1)
            int r6 = r6 + r7
            r0 = r38
            byte[] r0 = r0.cH
            r7 = r0
            r8 = 0
            r0 = r41
            r1 = r7
            r2 = r8
            int r7 = r0.Q(r1, r2)
            int r6 = java.lang.Math.max(r6, r7)
            if (r5 <= 0) goto L_0x08a6
            r7 = 1
            boolean r7 = com.uc.c.bc.aL(r13, r7)
            if (r7 != 0) goto L_0x0892
            if (r13 != 0) goto L_0x08a6
        L_0x0892:
            int r5 = r5 + r30
            int r5 = r5 - r9
            r0 = r30
            r1 = r5
            int r5 = java.lang.Math.min(r0, r1)
        L_0x089c:
            r0 = r38
            byte[] r0 = r0.cH
            r7 = r0
            b(r7, r5, r6)
            goto L_0x0009
        L_0x08a6:
            r5 = r30
            goto L_0x089c
        L_0x08a9:
            r19 = r6
            goto L_0x0843
        L_0x08ac:
            r25 = r6
            goto L_0x0625
        L_0x08b0:
            r37 = r11
            r11 = r7
            r7 = r37
            goto L_0x064d
        L_0x08b7:
            r10 = r16
            goto L_0x05b0
        L_0x08bb:
            r10 = r18
            goto L_0x053e
        L_0x08bf:
            r37 = r14
            r14 = r10
            r10 = r37
            goto L_0x04cd
        L_0x08c6:
            r6 = r7
            goto L_0x0423
        L_0x08c9:
            r14 = r5
            goto L_0x03ca
        L_0x08cc:
            r10 = r6
            goto L_0x03bf
        L_0x08cf:
            r10 = r14
            goto L_0x0491
        L_0x08d2:
            r36 = r14
            goto L_0x03a1
        L_0x08d6:
            r15 = r5
            goto L_0x0357
        L_0x08d9:
            r10 = r6
            goto L_0x034e
        L_0x08dc:
            r15 = r33
            r16 = r34
            r18 = r35
            goto L_0x01f3
        L_0x08e4:
            r21 = r5
            goto L_0x0216
        L_0x08e8:
            r10 = r6
            goto L_0x020e
        L_0x08eb:
            r10 = r7
            r7 = r11
            goto L_0x0223
        L_0x08ef:
            r14 = r40
            goto L_0x00fb
        L_0x08f3:
            r30 = r7
            r7 = r8
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.f.a(int, int, com.uc.c.ca):void");
    }

    public void a(bk bkVar, ca caVar) {
        int i;
        int i2;
        b.a.a.a.f fVar;
        if (caVar.bHl != null && caVar.bHm != -1) {
            if (caVar.d(caVar.bHl, caVar.bHm)) {
                byte[] bArr = (byte[]) caVar.bHl.cE[caVar.bHm];
                if (!ca.ja(bArr[0])) {
                    bkVar.save();
                    int[] iArr = new int[4];
                    Vector vector = new Vector();
                    boolean z = false;
                    int bm = (e.bm(caVar.bFQ) << 1) + 2;
                    Vector b2 = caVar.b(caVar.bHl, caVar.bHm, 1);
                    int size = b2.size();
                    if (size > 0) {
                        int[] iArr2 = new int[(size * 4)];
                        int i3 = bkVar.top;
                        if (caVar.bFM == 1 && bArr != null && bArr[0] == 14) {
                            i3 -= e.bj(k.ads).uQ();
                        }
                        for (int i4 = 0; i4 < size; i4++) {
                            bArr = (byte[]) b2.elementAt(i4);
                            caVar.a(caVar.bHl, bArr, iArr);
                            iArr[2] = c(bArr, caVar);
                            iArr[3] = d(bArr, caVar);
                            iArr2[i4 * 4] = iArr[0] + bkVar.left;
                            iArr2[(i4 * 4) + 1] = iArr[1] + i3;
                            iArr2[(i4 * 4) + 2] = iArr[2] + iArr[0] + bkVar.left;
                            iArr2[(i4 * 4) + 3] = iArr[3] + iArr[1] + i3;
                            int a2 = a(caVar, iArr);
                            if (a2 > 0) {
                                int i5 = (i4 * 4) + 1;
                                iArr2[i5] = iArr2[i5] - 1;
                                int i6 = (i4 * 4) + 3;
                                iArr2[i6] = a2 + 1 + iArr2[i6];
                            }
                        }
                        if (!caVar.p.a(iArr2, bArr[0] == 3, bkVar.bkB.bmb)) {
                            int i7 = size == 1 ? 2 : 3;
                            int i8 = Integer.MAX_VALUE;
                            for (int i9 = 0; i9 < size; i9++) {
                                iArr[0] = iArr2[i9 * 4] - bkVar.left;
                                iArr[1] = iArr2[(i9 * 4) + 1] - bkVar.top;
                                iArr[2] = iArr2[(i9 * 4) + 2] - bkVar.left;
                                iArr[3] = iArr2[(i9 * 4) + 3] - bkVar.top;
                                if (bArr[0] == 3 && iArr[2] > w.MM && size == 1) {
                                    z = true;
                                }
                                if (ca.je(bArr[0])) {
                                    iArr[0] = iArr[0] - 1;
                                    iArr[1] = iArr[1] - 1;
                                } else {
                                    if (iArr[0] >= i7) {
                                        iArr[0] = iArr[0] - (i7 - 1);
                                        iArr[2] = iArr[2] + (((i7 - 1) * 2) - 2);
                                    } else {
                                        iArr[2] = iArr[2] + ((iArr[0] * 2) - 2);
                                        iArr[0] = 0;
                                    }
                                    if (iArr[1] >= i7) {
                                        iArr[1] = iArr[1] - (i7 - 1);
                                        iArr[3] = iArr[3] + (((i7 - 1) * 2) - 2);
                                    } else {
                                        iArr[3] = iArr[3] + ((iArr[1] * 2) - 2);
                                        iArr[1] = 0;
                                    }
                                }
                                i8 = Math.min(i8, iArr[1]);
                                int i10 = iArr[0];
                                int i11 = iArr[2] + i10;
                                int i12 = i10;
                                int i13 = 0;
                                while (true) {
                                    int i14 = 2147483646;
                                    if (i13 < vector.size()) {
                                        i14 = ((Integer) vector.elementAt(i13)).intValue();
                                    }
                                    if (i14 >= i12) {
                                        int i15 = i13 + 1;
                                        vector.insertElementAt(new Integer(i12), i13);
                                        int i16 = i15 + 1;
                                        vector.insertElementAt(new Integer(iArr[1]), i15);
                                        int i17 = i16 + 1;
                                        vector.insertElementAt(new Integer(iArr[1] + iArr[3]), i16);
                                        int i18 = i17 + 1;
                                        vector.insertElementAt(new Integer(1), i17);
                                        vector.insertElementAt(new Integer((iArr[2] <= 40 || iArr[3] <= bm) ? 0 : 1), i18);
                                        i12 = Integer.MAX_VALUE;
                                        i13 = i18 + 1;
                                    }
                                    if (i14 > i11) {
                                        break;
                                    }
                                    i13 += 5;
                                }
                                vector.insertElementAt(new Integer((iArr[2] <= 40 || iArr[3] <= bm) ? 0 : -1), i13);
                                vector.insertElementAt(new Integer(-1), i13);
                                vector.insertElementAt(new Integer(iArr[1] + iArr[3]), i13);
                                vector.insertElementAt(new Integer(iArr[1]), i13);
                                vector.insertElementAt(new Integer(i11), i13);
                            }
                            int i19 = i8;
                            int i20 = i8;
                            while (i20 != Integer.MAX_VALUE) {
                                int i21 = 0;
                                int i22 = Integer.MAX_VALUE;
                                int i23 = 0;
                                int i24 = 0;
                                int i25 = 0;
                                int i26 = 0;
                                int i27 = 0;
                                while (i23 < vector.size()) {
                                    int intValue = ((Integer) vector.elementAt(i23)).intValue() + 1;
                                    int i28 = i26 + i25;
                                    int i29 = 0;
                                    int i30 = i24;
                                    int i31 = i22;
                                    int i32 = i21;
                                    while (true) {
                                        int i33 = i23 + 1;
                                        int i34 = i33 + 1;
                                        int intValue2 = ((Integer) vector.elementAt(i33)).intValue();
                                        int i35 = i34 + 1;
                                        int intValue3 = ((Integer) vector.elementAt(i34)).intValue();
                                        int i36 = i35 + 1;
                                        int intValue4 = ((Integer) vector.elementAt(i35)).intValue();
                                        i = i36 + 1;
                                        int intValue5 = ((Integer) vector.elementAt(i36)).intValue();
                                        if (intValue2 > i20) {
                                            i31 = Math.min(i31, intValue2);
                                        }
                                        if (intValue3 > i20) {
                                            i31 = Math.min(i31, intValue3);
                                        }
                                        if (intValue2 < i20 && intValue3 >= i20) {
                                            i30 += intValue4;
                                            i29 += intValue5;
                                        }
                                        i2 = (intValue2 > i20 || intValue3 <= i20) ? i32 : i32 + intValue4;
                                        if (i >= vector.size() || ((Integer) vector.elementAt(i)).intValue() > intValue) {
                                            int i37 = intValue - 1;
                                        } else {
                                            i32 = i2;
                                            i23 = i;
                                        }
                                    }
                                    int i372 = intValue - 1;
                                    if (i28 == 0 && i24 > 0 && (fVar = null) != null) {
                                        bkVar.Du();
                                        bkVar.l(i27 + 1, i19 + 1, i372 - i27, i20 - i19);
                                        for (int i38 = i19; i38 < i20; i38 += fVar.getHeight()) {
                                            for (int i39 = i27; i39 < i372; i39 += fVar.getWidth()) {
                                                bkVar.a(fVar, i39, i38, 20);
                                            }
                                        }
                                        bkVar.Dv();
                                    }
                                    bkVar.setColor(az.bdf[25]);
                                    if ((((((-i24) | i24) >>> 31) ^ 1) ^ ((((-i30) | i30) >>> 31) ^ 1)) != 0) {
                                        bkVar.o(i372, i19, 2, (i20 - i19) + 2);
                                    }
                                    if (((((i21 | (-i21)) >>> 31) ^ 1) ^ (((i24 | (-i24)) >>> 31) ^ 1)) != 0) {
                                        bkVar.o(i27, i20, (i372 - i27) + 2, 2);
                                    }
                                    i21 = i2;
                                    i24 = i30;
                                    i27 = i372;
                                    i23 = i;
                                    i22 = i31;
                                    int i40 = i28;
                                    i26 = i29;
                                    i25 = i40;
                                }
                                i19 = i20;
                                i20 = i22;
                            }
                            if (z && !caVar.iJ(3) && caVar.bFM == 2) {
                                bkVar.Du();
                                bkVar.l(iArr[0], iArr[1], iArr[2], iArr[3]);
                                String str = ar.avM[95];
                                bkVar.setColor(az.bdf[298]);
                                bkVar.o(iArr[0] + 3, iArr[1] + 3, bc.p(0, str) + 2, e.bm(50) + 4);
                                bkVar.setColor(16777215);
                                bkVar.gZ(bc.aU(bc.bfK, 0));
                                bkVar.b(str, iArr[0] + 4, iArr[1] + 4, 20);
                                bkVar.Dv();
                            }
                            bkVar.reset();
                        }
                    }
                }
            }
        }
    }

    public void a(bk bkVar, ca caVar, int i) {
        Plugin jn;
        for (short s = 0; s < this.cF; s++) {
            if (this.cE[s] instanceof byte[]) {
                byte[] bArr = (byte[]) this.cE[s];
                if (bArr[0] == 28 && (jn = caVar.jn(b(bArr, ca.bLF, 1))) != null) {
                    caVar.ch().a(jn, -1, -1, -1, -1);
                }
            } else {
                ((f) this.cE[s]).a(bkVar, caVar, i);
            }
        }
    }

    public void a(bk bkVar, ca caVar, int i, int i2, int i3, int i4, int i5) {
        int b2 = b(this.cH);
        int c = c(this.cH);
        int d = d(this.cH);
        int e = e(this.cH);
        if ((d == 0 && e == 0) || !ca.aC(this.cH)) {
            return;
        }
        if ((this.cH[0] != 25 || (b(this.cH, ca.bLC, 1) & 1) == 0) && f(this.cH, 46) != 1) {
            if (caVar.bGt > caVar.bGr && caVar.bGt > d) {
                d = caVar.bGt;
            }
            if (bc.c(b2, c, d, e, i, i2, i3, i4)) {
                if (this.cH[0] == 51) {
                    r.k(bkVar, this.cH, caVar);
                } else {
                    c(bkVar, this.cH, caVar, i5);
                }
                if (!caVar.LK() || this != caVar.bFR) {
                    bkVar.bc(b2, c);
                }
                b(bkVar, caVar, i - b2, i2 - c, i3, i4, i5);
                if (!caVar.LK() || this != caVar.bFR) {
                    bkVar.bc(-b2, -c);
                    return;
                }
                return;
            }
            a(bkVar, caVar, i5);
        }
    }

    public final boolean a(bk bkVar, byte[] bArr, ca caVar, Object obj) {
        return a(bkVar, bArr, caVar, obj, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.f.a(com.uc.c.ca, byte[], boolean):boolean
     arg types: [com.uc.c.ca, byte[], int]
     candidates:
      com.uc.c.f.a(byte[], byte[], int):java.lang.String
      com.uc.c.f.a(com.uc.c.bk, byte[], com.uc.c.ca):void
      com.uc.c.f.a(byte[], int, int):void
      com.uc.c.f.a(com.uc.c.ca, byte[], int):boolean
      com.uc.c.f.a(int, int, int):byte[]
      com.uc.c.f.a(byte[], com.uc.c.ca, int):char[]
      com.uc.c.f.a(int, int, com.uc.c.ca):void
      com.uc.c.f.a(com.uc.c.bk, com.uc.c.ca, int):void
      com.uc.c.f.a(com.uc.c.ca, byte[], boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int):void
     arg types: [int[], int, int, int, int, int, int, int]
     candidates:
      com.uc.c.bk.a(int, int, int, int, int, int, b.a.a.a.f, b.a.a.a.f):void
      com.uc.c.bk.a(int, int, int, int, int[], boolean, int, int):void
      com.uc.c.bk.a(java.lang.String, int, int, int, int, int, int, boolean):void
      com.uc.c.bk.a(int[], int, int, int, int, int, int, boolean):void
      com.uc.c.bk.a(int[], int, int, int, int, boolean, int, int):void */
    public final boolean a(bk bkVar, byte[] bArr, ca caVar, Object obj, int i) {
        boolean z;
        if (bArr == null || bkVar == null) {
            return false;
        }
        bkVar.save();
        try {
            int b2 = b(bArr);
            int c = c(bArr);
            int d = d(bArr);
            int e = e(bArr) + 1;
            String str = (String) ((Object[]) obj)[2];
            f fVar = (f) ((Object[]) obj)[0];
            int intValue = ((Integer) ((Object[]) obj)[1]).intValue();
            byte[] c2 = ca.c(fVar, intValue);
            if (c2 == null || c2[0] != 25) {
                z = false;
            } else {
                z = (b(((f) fVar.cE[intValue]).cH, ca.bLC, 1) & 1) == 0;
            }
            if (!caVar.LT() || caVar.ch() == null) {
                int i2 = az.bdf[251];
                int i3 = az.bdf[252];
                b.a.a.a.f gb = az.gb(z ? 34 : 36);
                int i4 = caVar.bFQ;
                int f = d - e.f(i4, "  ..");
                int width = gb != null ? f - gb.getWidth() : f;
                bkVar.a(az.bdf, b2, c, d, e, true, 253, 7);
                bkVar.setColor(i2);
                bkVar.m(b2, c, (b2 + d) - 1, c);
                bkVar.setColor(i3);
                bkVar.m(b2, (c + e) - 1, (b2 + d) - 1, (c + e) - 1);
                bkVar.setColor(az.bdf[267]);
                if (str != null) {
                    bkVar.q(b2 + 4, ((e - 2) >> 1) + c, 2, 2);
                    bkVar.b(e.bj(i4).b(str, width, (String) null), b2 + 10, c, width, e, 1, 2);
                }
                if (gb != null) {
                    bkVar.a(gb, (b2 + d) - 4, ((e - gb.getHeight()) >> 1) + c, 24);
                }
                bkVar.reset();
                return true;
            }
            try {
                caVar.ch().a(bkVar, new int[]{d, e - 1}, str, caVar.bFQ, a(caVar, bArr, false), z, b2, c, i);
            } catch (Throwable th) {
            }
            return true;
        } catch (Exception e2) {
        } finally {
            bkVar.reset();
        }
    }

    /* JADX WARN: Type inference failed for: r0v12, types: [java.lang.Object[]] */
    /* JADX WARN: Type inference failed for: r0v29, types: [java.lang.Object[]] */
    /* JADX WARN: Type inference failed for: r0v52, types: [java.lang.Object[]] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(int r24, int r25, com.uc.c.ca r26) {
        /*
            r23 = this;
            r4 = 1
            com.uc.c.f[] r13 = new com.uc.c.f[r4]
            r4 = 1
            com.uc.c.f[] r10 = new com.uc.c.f[r4]
            r4 = 1
            int[] r11 = new int[r4]
            r4 = 0
            java.util.Vector r14 = new java.util.Vector
            r5 = 2
            r14.<init>(r5)
            r5 = -1
            r6 = 1
            r7 = 0
            r15 = r5
            r5 = r6
            r6 = r7
        L_0x0016:
            r0 = r23
            short r0 = r0.cF
            r7 = r0
            if (r6 >= r7) goto L_0x00ee
            r0 = r23
            java.lang.Object[] r0 = r0.cE
            r7 = r0
            r7 = r7[r6]
            boolean r7 = r7 instanceof com.uc.c.f
            if (r7 == 0) goto L_0x0464
            r0 = r23
            java.lang.Object[] r0 = r0.cE
            r7 = r0
            r24 = r7[r6]
            com.uc.c.f r24 = (com.uc.c.f) r24
            r0 = r24
            byte[] r0 = r0.cH
            r7 = r0
            r8 = 0
            byte r7 = r7[r8]
            r8 = 20
            if (r7 == r8) goto L_0x0042
            r7 = r15
        L_0x003e:
            int r6 = r6 + 1
            r15 = r7
            goto L_0x0016
        L_0x0042:
            r7 = 0
            r0 = r23
            java.lang.Object[] r0 = r0.cE
            r8 = r0
            r24 = r8[r6]
            com.uc.c.f r24 = (com.uc.c.f) r24
            r13[r7] = r24
            r7 = 0
            r8 = 0
            r22 = r8
            r8 = r7
            r7 = r22
        L_0x0055:
            r9 = 0
            r9 = r13[r9]
            short r9 = r9.cF
            if (r7 >= r9) goto L_0x00cd
            r9 = 0
            r9 = r13[r9]
            java.lang.Object[] r9 = r9.cE
            r9 = r9[r7]
            boolean r9 = r9 instanceof com.uc.c.f
            if (r9 == 0) goto L_0x007c
            r9 = 0
            r9 = r13[r9]
            java.lang.Object[] r9 = r9.cE
            r24 = r9[r7]
            com.uc.c.f r24 = (com.uc.c.f) r24
            r0 = r24
            byte[] r0 = r0.cH
            r9 = r0
            r12 = 0
            byte r9 = r9[r12]
            r12 = 21
            if (r9 == r12) goto L_0x007f
        L_0x007c:
            int r7 = r7 + 1
            goto L_0x0055
        L_0x007f:
            r9 = 0
            r12 = 0
            r12 = r13[r12]
            java.lang.Object[] r12 = r12.cE
            r24 = r12[r7]
            com.uc.c.f r24 = (com.uc.c.f) r24
            r10[r9] = r24
            r9 = 0
            r9 = r10[r9]
            byte[] r9 = r9.cH
            int r9 = d(r9)
            if (r5 == 0) goto L_0x00a8
            r12 = 2
            int[] r12 = new int[r12]
            r16 = 0
            r12[r16] = r9
            r9 = 1
            r16 = 0
            r12[r9] = r16
            r14.addElement(r12)
        L_0x00a5:
            int r8 = r8 + 1
            goto L_0x007c
        L_0x00a8:
            int r12 = r14.size()
            if (r8 >= r12) goto L_0x00f4
            java.lang.Object r24 = r14.elementAt(r7)
            int[] r24 = (int[]) r24
            int[] r24 = (int[]) r24
            java.lang.Object r25 = r14.elementAt(r8)
            int[] r25 = (int[]) r25
            int[] r25 = (int[]) r25
            r12 = 0
            r16 = 0
            r16 = r24[r16]
            r0 = r16
            r1 = r9
            int r9 = java.lang.Math.max(r0, r1)
            r25[r12] = r9
            goto L_0x00a5
        L_0x00cd:
            r5 = -1
            if (r15 != r5) goto L_0x0467
            r5 = 0
            r5 = r13[r5]
            byte[] r5 = r5.cH
            int r5 = d(r5)
            r0 = r26
            byte r0 = r0.bHp
            r7 = r0
            int r8 = r14.size()
            r9 = 1
            int r8 = r8 - r9
            int r7 = r7 * r8
            int r5 = r5 - r7
        L_0x00e6:
            r7 = 0
            r22 = r7
            r7 = r5
            r5 = r22
            goto L_0x003e
        L_0x00ee:
            int r6 = r14.size()
            if (r6 > 0) goto L_0x00f5
        L_0x00f4:
            return
        L_0x00f5:
            r6 = 0
            r22 = r6
            r6 = r4
            r4 = r22
        L_0x00fb:
            int r7 = r14.size()
            if (r4 >= r7) goto L_0x0110
            java.lang.Object r24 = r14.elementAt(r4)
            int[] r24 = (int[]) r24
            int[] r24 = (int[]) r24
            r7 = 0
            r7 = r24[r7]
            int r6 = r6 + r7
            int r4 = r4 + 1
            goto L_0x00fb
        L_0x0110:
            if (r6 <= r15) goto L_0x0461
            r4 = 1
        L_0x0113:
            if (r4 == 0) goto L_0x02ec
            r5 = 0
            r16 = r4
            r17 = r5
        L_0x011a:
            r0 = r23
            short r0 = r0.cF
            r4 = r0
            r0 = r17
            r1 = r4
            if (r0 >= r1) goto L_0x027b
            r0 = r23
            java.lang.Object[] r0 = r0.cE
            r4 = r0
            r4 = r4[r17]
            boolean r4 = r4 instanceof com.uc.c.f
            if (r4 == 0) goto L_0x045d
            r0 = r23
            java.lang.Object[] r0 = r0.cE
            r4 = r0
            r24 = r4[r17]
            com.uc.c.f r24 = (com.uc.c.f) r24
            r0 = r24
            byte[] r0 = r0.cH
            r4 = r0
            r5 = 0
            byte r4 = r4[r5]
            r5 = 20
            if (r4 == r5) goto L_0x014d
            r4 = r16
        L_0x0146:
            int r5 = r17 + 1
            r16 = r4
            r17 = r5
            goto L_0x011a
        L_0x014d:
            r4 = 0
            r0 = r23
            java.lang.Object[] r0 = r0.cE
            r5 = r0
            r24 = r5[r17]
            com.uc.c.f r24 = (com.uc.c.f) r24
            r13[r4] = r24
            r4 = 0
            r5 = 0
            r18 = r5
            r19 = r4
        L_0x015f:
            r4 = 0
            r4 = r13[r4]
            short r4 = r4.cF
            r0 = r18
            r1 = r4
            if (r0 >= r1) goto L_0x0278
            r4 = 0
            r4 = r13[r4]
            java.lang.Object[] r4 = r4.cE
            r4 = r4[r18]
            boolean r4 = r4 instanceof com.uc.c.f
            if (r4 == 0) goto L_0x0459
            r4 = 0
            r4 = r13[r4]
            java.lang.Object[] r4 = r4.cE
            r24 = r4[r18]
            com.uc.c.f r24 = (com.uc.c.f) r24
            r0 = r24
            byte[] r0 = r0.cH
            r4 = r0
            r5 = 0
            byte r4 = r4[r5]
            r5 = 21
            if (r4 == r5) goto L_0x0192
            r4 = r19
        L_0x018b:
            int r5 = r18 + 1
            r18 = r5
            r19 = r4
            goto L_0x015f
        L_0x0192:
            r4 = 0
            r5 = 0
            r5 = r13[r5]
            java.lang.Object[] r5 = r5.cE
            r24 = r5[r18]
            com.uc.c.f r24 = (com.uc.c.f) r24
            r10[r4] = r24
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = -1
            r11[r6] = r7
            r20 = r5
            r21 = r4
        L_0x01a8:
            r0 = r26
            com.uc.c.i r0 = r0.bGd
            r4 = r0
            r5 = 0
            r5 = r10[r5]
            r6 = 0
            r7 = 268579833(0x100233f9, double:1.326960687E-315)
            r9 = 0
            r12 = 1
            byte[] r4 = r4.a(r5, r6, r7, r9, r10, r11, r12)
            if (r4 == 0) goto L_0x022e
            int r5 = e(r4)
            r6 = 0
            byte r6 = r4[r6]
            if (r6 != 0) goto L_0x01e3
            r6 = 1
            int r6 = f(r4, r6)
            r7 = 29
            int r7 = f(r4, r7)
            b.a.a.a.k r6 = com.uc.b.e.I(r7, r6)
            r0 = r23
            int r0 = r0.cJ
            r7 = r0
            r0 = r26
            r1 = r4
            r2 = r7
            byte[][] r7 = b(r0, r1, r2)
            if (r7 != 0) goto L_0x01f6
        L_0x01e3:
            int r4 = d(r4)
            int r5 = r5 * r4
            int r5 = r5 + r20
            r0 = r21
            r1 = r4
            int r4 = java.lang.Math.max(r0, r1)
            r20 = r5
            r21 = r4
            goto L_0x01a8
        L_0x01f6:
            r4 = 0
            r5 = 0
            r8 = r20
            r22 = r4
            r4 = r5
            r5 = r22
        L_0x01ff:
            int r9 = r7.length
            if (r4 >= r9) goto L_0x0217
            r9 = r7[r4]
            int r9 = d(r9)
            r12 = r7[r4]
            int r12 = e(r12)
            int r12 = r12 * r9
            int r8 = r8 + r12
            int r5 = java.lang.Math.max(r5, r9)
            int r4 = r4 + 1
            goto L_0x01ff
        L_0x0217:
            int r4 = r6.uP()
            int r4 = r4 << 1
            int r4 = java.lang.Math.min(r4, r5)
            r0 = r21
            r1 = r4
            int r4 = java.lang.Math.max(r0, r1)
            r20 = r8
            r21 = r4
            goto L_0x01a8
        L_0x022e:
            if (r16 != 0) goto L_0x0269
            r0 = r14
            r1 = r19
            java.lang.Object r24 = r0.elementAt(r1)
            int[] r24 = (int[]) r24
            int[] r24 = (int[]) r24
            r4 = 0
            r0 = r14
            r1 = r19
            java.lang.Object r25 = r0.elementAt(r1)
            int[] r25 = (int[]) r25
            int[] r25 = (int[]) r25
            r5 = 0
            r5 = r25[r5]
            r0 = r5
            r1 = r21
            int r5 = java.lang.Math.max(r0, r1)
            r24[r4] = r5
        L_0x0253:
            r0 = r14
            r1 = r19
            java.lang.Object r24 = r0.elementAt(r1)
            int[] r24 = (int[]) r24
            int[] r24 = (int[]) r24
            r4 = 1
            r5 = r24[r4]
            int r5 = r5 + r20
            r24[r4] = r5
            int r4 = r19 + 1
            goto L_0x018b
        L_0x0269:
            r0 = r14
            r1 = r19
            java.lang.Object r24 = r0.elementAt(r1)
            int[] r24 = (int[]) r24
            int[] r24 = (int[]) r24
            r4 = 0
            r24[r4] = r21
            goto L_0x0253
        L_0x0278:
            r4 = 0
            goto L_0x0146
        L_0x027b:
            r4 = 0
            r5 = 0
        L_0x027d:
            int r6 = r14.size()
            if (r5 >= r6) goto L_0x0292
            java.lang.Object r24 = r14.elementAt(r5)
            int[] r24 = (int[]) r24
            int[] r24 = (int[]) r24
            r6 = 0
            r6 = r24[r6]
            int r4 = r4 + r6
            int r5 = r5 + 1
            goto L_0x027d
        L_0x0292:
            if (r4 > r15) goto L_0x00f4
            r5 = 0
            r6 = 0
        L_0x0296:
            int r7 = r14.size()
            if (r6 >= r7) goto L_0x02ab
            java.lang.Object r24 = r14.elementAt(r6)
            int[] r24 = (int[]) r24
            int[] r24 = (int[]) r24
            r7 = 1
            r7 = r24[r7]
            int r5 = r5 + r7
            int r6 = r6 + 1
            goto L_0x0296
        L_0x02ab:
            int r6 = r15 - r4
            r7 = 0
            r8 = r15
        L_0x02af:
            int r9 = r14.size()
            r11 = 1
            int r9 = r9 - r11
            if (r7 >= r9) goto L_0x02d6
            java.lang.Object r24 = r14.elementAt(r7)
            int[] r24 = (int[]) r24
            int[] r24 = (int[]) r24
            r9 = 1
            r9 = r24[r9]
            int r9 = r9 * r6
            int r9 = r9 / r5
            java.lang.Object r24 = r14.elementAt(r7)
            int[] r24 = (int[]) r24
            int[] r24 = (int[]) r24
            r11 = 0
            r12 = r24[r11]
            int r12 = r12 + r9
            r24[r11] = r12
            int r8 = r8 - r9
            int r7 = r7 + 1
            goto L_0x02af
        L_0x02d6:
            int r5 = r14.size()
            r6 = 1
            int r5 = r5 - r6
            java.lang.Object r24 = r14.elementAt(r5)
            int[] r24 = (int[]) r24
            int[] r24 = (int[]) r24
            r5 = 0
            r6 = r24[r5]
            int r4 = r8 - r4
            int r4 = r4 + r6
            r24[r5] = r4
        L_0x02ec:
            r4 = 0
            r11 = r4
        L_0x02ee:
            r0 = r23
            short r0 = r0.cF
            r4 = r0
            if (r11 >= r4) goto L_0x0444
            r4 = 0
            r5 = 2147483647(0x7fffffff, float:NaN)
            r0 = r23
            java.lang.Object[] r0 = r0.cE
            r6 = r0
            r6 = r6[r11]
            boolean r6 = r6 instanceof com.uc.c.f
            if (r6 == 0) goto L_0x0319
            r0 = r23
            java.lang.Object[] r0 = r0.cE
            r6 = r0
            r24 = r6[r11]
            com.uc.c.f r24 = (com.uc.c.f) r24
            r0 = r24
            byte[] r0 = r0.cH
            r6 = r0
            r7 = 0
            byte r6 = r6[r7]
            r7 = 20
            if (r6 == r7) goto L_0x031d
        L_0x0319:
            int r4 = r11 + 1
            r11 = r4
            goto L_0x02ee
        L_0x031d:
            r6 = 0
            r0 = r23
            java.lang.Object[] r0 = r0.cE
            r7 = r0
            r24 = r7[r11]
            com.uc.c.f r24 = (com.uc.c.f) r24
            r13[r6] = r24
            r6 = 0
            r7 = 0
            r9 = r4
            r4 = r7
            r7 = r5
            r5 = r6
        L_0x032f:
            r6 = 0
            r6 = r13[r6]
            short r6 = r6.cF
            if (r4 >= r6) goto L_0x03ce
            r6 = 0
            r6 = r13[r6]
            java.lang.Object[] r6 = r6.cE
            r6 = r6[r4]
            boolean r6 = r6 instanceof com.uc.c.f
            if (r6 == 0) goto L_0x0451
            r6 = 0
            r6 = r13[r6]
            java.lang.Object[] r6 = r6.cE
            r24 = r6[r4]
            com.uc.c.f r24 = (com.uc.c.f) r24
            r0 = r24
            byte[] r0 = r0.cH
            r6 = r0
            r8 = 0
            byte r6 = r6[r8]
            r8 = 21
            if (r6 == r8) goto L_0x0365
            r6 = r9
            r22 = r7
            r7 = r5
            r5 = r22
        L_0x035c:
            int r4 = r4 + 1
            r9 = r6
            r22 = r5
            r5 = r7
            r7 = r22
            goto L_0x032f
        L_0x0365:
            r6 = 0
            r8 = 0
            r8 = r13[r8]
            java.lang.Object[] r8 = r8.cE
            r24 = r8[r4]
            com.uc.c.f r24 = (com.uc.c.f) r24
            r10[r6] = r24
            r6 = 0
            r6 = r10[r6]
            byte[] r6 = r6.cH
            int r6 = d(r6)
            java.lang.Object r24 = r14.elementAt(r5)
            int[] r24 = (int[]) r24
            int[] r24 = (int[]) r24
            r8 = 0
            r8 = r24[r8]
            if (r6 == r8) goto L_0x03c6
            r6 = 0
            r6 = r10[r6]
            r8 = 0
            r6.cI = r8
            r6 = 0
            r6 = r10[r6]
            java.lang.Object r24 = r14.elementAt(r5)
            int[] r24 = (int[]) r24
            int[] r24 = (int[]) r24
            r8 = 0
            r8 = r24[r8]
            r12 = 3
            r0 = r6
            r1 = r8
            r2 = r12
            r3 = r26
            r0.a(r1, r2, r3)
        L_0x03a4:
            int r5 = r5 + 1
            r6 = 0
            r6 = r10[r6]
            byte[] r6 = r6.cH
            int r6 = e(r6)
            int r6 = java.lang.Math.max(r9, r6)
            r8 = 0
            r8 = r10[r8]
            byte[] r8 = r8.cH
            int r8 = c(r8)
            int r7 = java.lang.Math.min(r7, r8)
            r22 = r7
            r7 = r5
            r5 = r22
            goto L_0x035c
        L_0x03c6:
            r6 = 0
            r6 = r10[r6]
            r8 = 32767(0x7fff, float:4.5916E-41)
            r6.cI = r8
            goto L_0x03a4
        L_0x03ce:
            r5 = 1
            int r4 = r4 - r5
            r12 = r4
        L_0x03d1:
            if (r12 < 0) goto L_0x0435
            r4 = 0
            r4 = r13[r4]
            java.lang.Object[] r4 = r4.cE
            r4 = r4[r12]
            boolean r4 = r4 instanceof com.uc.c.f
            if (r4 == 0) goto L_0x03f3
            r4 = 0
            r4 = r13[r4]
            java.lang.Object[] r4 = r4.cE
            r24 = r4[r12]
            com.uc.c.f r24 = (com.uc.c.f) r24
            r0 = r24
            byte[] r0 = r0.cH
            r4 = r0
            r5 = 0
            byte r4 = r4[r5]
            r5 = 21
            if (r4 == r5) goto L_0x03f7
        L_0x03f3:
            int r4 = r12 + -1
            r12 = r4
            goto L_0x03d1
        L_0x03f7:
            r4 = 0
            r5 = 0
            r5 = r13[r5]
            java.lang.Object[] r5 = r5.cE
            r24 = r5[r12]
            com.uc.c.f r24 = (com.uc.c.f) r24
            r10[r4] = r24
            r4 = 0
            r4 = r10[r4]
            byte[] r4 = r4.cH
            int r4 = e(r4)
            if (r9 != r4) goto L_0x0419
            r4 = 0
            r4 = r10[r4]
            byte[] r4 = r4.cH
            int r4 = c(r4)
            if (r7 == r4) goto L_0x03f3
        L_0x0419:
            r4 = 0
            r4 = r10[r4]
            byte[] r4 = r4.cH
            r5 = 1
            r6 = 0
            r6 = r10[r6]
            byte[] r6 = r6.cH
            int r6 = b(r6)
            r8 = 0
            r8 = r10[r8]
            byte[] r8 = r8.cH
            int r8 = d(r8)
            a(r4, r5, r6, r7, r8, r9)
            goto L_0x03f3
        L_0x0435:
            r4 = 0
            r4 = r13[r4]
            r5 = -1
            r6 = 0
            r0 = r4
            r1 = r5
            r2 = r6
            r3 = r26
            r0.a(r1, r2, r3)
            goto L_0x0319
        L_0x0444:
            r4 = -1
            r5 = 0
            r0 = r23
            r1 = r4
            r2 = r5
            r3 = r26
            r0.a(r1, r2, r3)
            goto L_0x00f4
        L_0x0451:
            r6 = r9
            r22 = r7
            r7 = r5
            r5 = r22
            goto L_0x035c
        L_0x0459:
            r4 = r19
            goto L_0x018b
        L_0x045d:
            r4 = r16
            goto L_0x0146
        L_0x0461:
            r4 = r5
            goto L_0x0113
        L_0x0464:
            r7 = r15
            goto L_0x003e
        L_0x0467:
            r5 = r15
            goto L_0x00e6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.f.b(int, int, com.uc.c.ca):void");
    }

    public void b(int i, Object obj) {
        if (i < this.cI) {
            this.cI = (short) i;
            if (this.cG != null) {
                this.cG.b(this.cG.indexOf(this), this);
            }
        }
    }

    public int bS() {
        return this.cJ;
    }

    public int bT() {
        int length = this.cE.length;
        if (this.cE.length != this.cF) {
            Object[] objArr = new Object[this.cF];
            System.arraycopy(this.cE, 0, objArr, 0, this.cF);
            this.cE = null;
            this.cE = objArr;
        }
        return length - this.cF;
    }

    public Object bU() {
        if (this.cE == null || this.cE.length < 1) {
            return null;
        }
        return this.cE[0];
    }

    public byte[] bV() {
        return this.cH;
    }

    public void c(int i, Object obj) {
        if (i < this.cI) {
            this.cI = (short) i;
        }
        for (short s = 0; s < this.cF; s++) {
            if (this.cE[s] instanceof f) {
                ((f) this.cE[s]).cI = 0;
                ((f) this.cE[s]).c(0, obj);
            }
        }
    }

    public void c(Object obj) {
        addElement(obj);
        b(this.cF - 1, obj);
        if (obj instanceof f) {
            ((f) obj).cG = this;
        }
    }

    public void d(int i, Object obj) {
        b(i, obj);
        c(i, obj);
    }

    public void d(Object obj) {
        e(obj);
        b(this.cF - 1, obj);
        if (obj instanceof f) {
            ((f) obj).cG = this;
        }
    }

    public void e(Object obj) {
        if (this.cE.length <= this.cF) {
            Object[] objArr = new Object[(this.cE.length + (this.cF << 1))];
            System.arraycopy(this.cE, 0, objArr, 0, this.cF);
            this.cE = null;
            this.cE = objArr;
        }
        if (this.cF < 2 || this.cE[this.cF - 1] == null || !(this.cE[this.cF - 1] instanceof byte[])) {
            Object[] objArr2 = this.cE;
            short s = this.cF;
            this.cF = (short) (s + 1);
            objArr2[s] = obj;
            return;
        }
        Object obj2 = this.cE[this.cF - 1];
        this.cE[this.cF - 1] = obj;
        Object[] objArr3 = this.cE;
        short s2 = this.cF;
        this.cF = (short) (s2 + 1);
        objArr3[s2] = obj2;
    }

    public void g(bk bkVar, byte[] bArr, ca caVar) {
        if (!az.BI()) {
            bkVar.save();
            int a2 = a(bArr, caVar);
            int b2 = b(bArr, caVar);
            int c = c(bArr, caVar);
            int d = d(bArr, caVar);
            bkVar.setColor(f(bArr, 3));
            bkVar.o(a2, b2, c, d);
            bkVar.reset();
        }
    }

    public void h(bk bkVar, byte[] bArr, ca caVar) {
        bkVar.save();
        bkVar.Dw();
        int b2 = b(bArr);
        int c = c(bArr);
        int d = d(bArr);
        e(bArr);
        bkVar.setColor(az.bdf[299]);
        bkVar.gX(1 == 1 ? 0 : 1);
        bkVar.m(b2, c, d + b2, c);
        bkVar.Dx();
        bkVar.reset();
    }

    public int indexOf(Object obj) {
        for (int i = this.cF - 1; i >= 0; i--) {
            if (this.cE[i] == obj) {
                return i;
            }
        }
        return -1;
    }

    public final int t(int i) {
        return 0;
    }
}
