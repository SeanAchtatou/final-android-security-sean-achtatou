package com.uc.c;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Picture;
import java.util.Vector;

public class bj {
    public static final int bke = 800;
    public static final int bkf = 800;
    int aSP = 0;
    int aSQ = 0;
    Vector bkg = new Vector();
    Vector bkh = new Vector();

    public bj(int i, int i2) {
        this.aSP = i;
        this.aSQ = i2;
        int ba = ba(i - 1, i2 - 1) + 1;
        this.bkg.setSize(ba);
        Dd();
        this.bkh.setSize(ba);
    }

    /* access modifiers changed from: package-private */
    public void Dd() {
        int gP = gP(this.aSP - 1);
        int gQ = gQ(0);
        int gQ2 = gQ(this.aSQ - 1);
        int gP2 = gP(this.aSP - 1) + 1;
        new Vector();
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Integer.MAX_VALUE);
        Paint paint2 = new Paint();
        paint2.setStyle(Paint.Style.FILL);
        paint2.setColor(2130706432);
        for (int gP3 = gP(0); gP3 <= gP; gP3++) {
            for (int i = gQ; i <= gQ2; i++) {
                Picture picture = new Picture();
                picture.beginRecording(this.aSP, this.aSQ);
                picture.endRecording();
                this.bkg.set((gP2 * i) + gP3, picture);
            }
        }
    }

    public void De() {
        int size = this.bkg.size();
        for (int i = 0; i < size; i++) {
            this.bkh.set(i, false);
        }
    }

    public void a(int i, int i2, Picture picture) {
        int ba = ba(i, i2);
        this.bkg.set(ba, picture);
        this.bkh.set(ba, true);
    }

    /* access modifiers changed from: package-private */
    public Picture aZ(int i, int i2) {
        return (Picture) this.bkg.get(ba(i, i2));
    }

    /* access modifiers changed from: package-private */
    public int ba(int i, int i2) {
        return ((gP(this.aSP - 1) + 1) * gQ(i2)) + gP(i);
    }

    public synchronized void c(bk bkVar, int i, int i2, int i3, int i4) {
        if (bkVar != null) {
            if (!(bkVar.bkB == null || bkVar.bkB.bmb == null)) {
                Canvas canvas = bkVar.bkB.bmb;
                int gP = gP(i);
                int gP2 = gP((i + i3) - 1);
                int gQ = gQ((i2 + i4) - 1);
                int gP3 = gP(this.aSP - 1) + 1;
                for (int gQ2 = gQ(i2); gQ2 <= gQ; gQ2++) {
                    for (int i5 = gP; i5 <= gP2; i5++) {
                        Vector vector = this.bkg;
                        canvas.save();
                        canvas.clipRect(i5 * 800, gQ2 * 800, (i5 + 1) * 800, (gQ2 + 1) * 800);
                        canvas.drawPicture((Picture) vector.get((gP3 * gQ2) + i5));
                        canvas.restore();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int gP(int i) {
        return (i > this.aSP ? this.aSP : i) / 800;
    }

    /* access modifiers changed from: package-private */
    public int gQ(int i) {
        return (i > this.aSQ ? this.aSQ : i) / 800;
    }

    public void invalidate(int i, int i2, int i3, int i4) {
        int gP = gP((i + i3) - 1);
        int gQ = gQ(i2);
        int gQ2 = gQ((i2 + i4) - 1);
        int gP2 = gP(this.aSP) + 1;
        for (int gP3 = gP(i); gP3 <= gP; gP3++) {
            for (int i5 = gQ; i5 <= gQ2; i5++) {
                this.bkh.set((gP2 * i5) + gP3, false);
            }
        }
    }

    public synchronized Vector j(int i, int i2, int i3, int i4) {
        Vector vector;
        vector = new Vector();
        int gP = gP(i);
        int gP2 = gP((i + i3) - 1);
        int gQ = gQ(i2);
        int gQ2 = gQ((i2 + i4) - 1);
        int gP3 = gP(this.aSP - 1) + 1;
        for (int i5 = gQ; i5 <= gQ2; i5++) {
            for (int i6 = gP; i6 <= gP2; i6++) {
                if (!((Boolean) this.bkh.get((gP3 * i5) + i6)).booleanValue()) {
                    vector.add(new int[]{i6 * 800, i5 * 800, 800, 800});
                }
            }
        }
        return vector;
    }
}
