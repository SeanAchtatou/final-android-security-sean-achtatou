package com.uc.c;

import b.a.a.a.f;

public class m {
    public static String TAG = "BoostCache";
    private static m oC = null;
    public static final float oF = 0.75f;
    public static float oG = 0.75f;
    public static final int oI = 0;
    public static final int oJ = 1;
    public aw oD = new aw(this);
    public aw oE = new aw(this);
    private boolean oH = false;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int, int, int, int[], boolean, boolean, boolean):void
     arg types: [com.uc.c.bk, com.uc.c.ca, int, int, short, int, ?[OBJECT, ARRAY], int, int, int]
     candidates:
      com.uc.c.r.a(com.uc.c.bk, int, int, int, int, int, int, int, com.uc.c.w, int):int
      com.uc.c.r.a(com.uc.c.bk, com.uc.c.ca, int, int, int, int, int[], boolean, boolean, boolean):void */
    private void a(r rVar, aw awVar, int i) {
        ca jm;
        int i2;
        int i3 = 0;
        if (rVar != null && (jm = rVar.jm()) != null && jm.ahU > jm.bGs && awVar != null) {
            if (!awVar.aG(jm.bGr, i)) {
                awVar.aF(jm.bGr, i);
            }
            f Av = awVar.Av();
            if (Av != null) {
                bk bkVar = new bk(Av.jU());
                int i4 = jm.bGw;
                short s = jm.bGr;
                if (awVar == this.oD) {
                    if (jm.bGx > 0) {
                        if (jm.bGx - i >= 0) {
                            i3 = i;
                            i2 = jm.bGx - i;
                        } else {
                            i2 = 0;
                            i3 = jm.bGx;
                        }
                    }
                    i2 = -1;
                } else {
                    if (jm.bGx + jm.bGs < jm.ahU) {
                        if (jm.bGx + jm.bGs + i <= jm.ahU) {
                            i3 = i;
                            i2 = jm.bGx + jm.bGs;
                        } else {
                            int i5 = jm.bGx + jm.bGs;
                            i3 = jm.ahU - (jm.bGx + jm.bGs);
                            i2 = i5;
                        }
                    }
                    i2 = -1;
                }
                if (i2 != -1) {
                    bkVar.bb(-i4, -i2);
                    rVar.a(bkVar, jm, i4, i2, (int) s, i3, (int[]) null, true, true, true);
                    awVar.b(Av, i4, i2, s, i3);
                }
            }
        }
    }

    public static m dq() {
        if (oC == null) {
            synchronized (m.class) {
                if (oC == null) {
                    oC = new m();
                }
            }
        }
        return oC;
    }

    public void a(r rVar) {
        if (!this.oH) {
            this.oH = true;
            int i = (int) (((float) w.ML) * oG);
            try {
                if (this.oD == null) {
                    this.oD = new aw(this);
                }
                if (this.oE == null) {
                    this.oE = new aw(this);
                }
                a(rVar, this.oD, i);
                a(rVar, this.oE, i);
            } catch (Exception e) {
            }
            this.oH = false;
        }
    }

    public void bz() {
        if (this.oD != null) {
            this.oD.recycle();
        }
        if (this.oE != null) {
            this.oE.recycle();
        }
    }

    public boolean d(bk bkVar, ca caVar, int i, int i2, int i3, int i4, int i5) {
        if (this.oH) {
            return false;
        }
        aw awVar = null;
        switch (i5) {
            case 0:
                awVar = this.oD;
                break;
            case 1:
                awVar = this.oE;
                break;
        }
        if (awVar == null) {
            return false;
        }
        try {
            if (awVar.Av() == null || awVar.getWidth() != i3 || i != awVar.Aw() || i2 < awVar.Ax() || i2 + i4 > awVar.Ax() + awVar.getHeight()) {
                return false;
            }
            bkVar.Du();
            bkVar.k(i, i2, i3, i4);
            bkVar.a(awVar.Av(), awVar.Aw(), awVar.Ax(), 20);
            bkVar.Dv();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
