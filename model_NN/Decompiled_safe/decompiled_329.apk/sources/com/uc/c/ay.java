package com.uc.c;

import a.a.a.c.a.y;
import com.uc.browser.ActivityBookmarkEx;
import com.uc.browser.ActivityEditMyNavi;
import com.uc.browser.ActivityFlash;
import com.uc.browser.ActivitySelector;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Hashtable;

public class ay {
    public static final int CDSECT = 5;
    public static final int COMMENT = 9;
    public static final int DOCDECL = 10;
    public static final int END_DOCUMENT = 1;
    public static final int END_TAG = 3;
    public static final int ENTITY_REF = 6;
    public static final int IGNORABLE_WHITESPACE = 7;
    public static final int PROCESSING_INSTRUCTION = 8;
    public static final int START_DOCUMENT = 0;
    public static final int START_TAG = 2;
    public static final int TEXT = 4;
    public static final byte aZL = 0;
    public static final byte aZM = 1;
    public static final byte aZN = 2;
    public static final byte aZO = 3;
    public static final byte aZP = 4;
    public static final byte aZQ = 64;
    public static final byte aZR = 65;
    public static final byte aZS = 66;
    public static final byte aZT = 67;
    public static final byte aZU = 68;
    public static final short aZV = 128;
    public static final short aZW = 129;
    public static final short aZX = 130;
    public static final short aZY = 131;
    public static final short aZZ = 132;
    public static final int aZf = 999;
    public static final int aZg = 998;
    private static Hashtable aZi = null;
    public static final short baa = 192;
    public static final short bab = 193;
    public static final short bac = 194;
    public static final short bad = 195;
    public static final short bae = 196;
    public static final byte baf = 64;
    private static String[] bal = null;
    private static String[] bam = null;
    private static String[] ban = null;
    static final String bao = "0123456789abcdef";
    private boolean aZA;
    private int aZB;
    private String[] aZC = new String[16];
    private int aZD = 0;
    private String aZE;
    private int[] aZF = new int[2];
    private int aZG;
    private boolean aZH;
    private boolean aZI;
    private boolean aZJ;
    public int aZK = 0;
    private boolean aZh;
    private int aZj;
    private String[] aZk = new String[16];
    private int[] aZl = new int[4];
    private InputStreamReader aZm = null;
    public InputStream aZn = null;
    private InputStream aZo = null;
    public String aZp;
    private String aZq;
    public char[] aZr;
    private int aZs;
    private int aZt;
    private int aZu;
    private char[] aZv = null;
    private int aZw;
    private boolean aZx;
    private String aZy;
    private String aZz;
    private int bag = -2;
    private byte[] bah = null;
    private Hashtable bai = null;
    public int baj;
    public Object bak;
    private cf bap = null;
    private int column;
    private String name;
    private int type;

    public ay(boolean z, int i) {
        this.aZh = z;
        this.aZK = i;
        if (!Bb()) {
            this.bap = new cf(128);
            if (bal == null) {
                bal = new String[]{null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, "a", "td", "tr", "table", "p", "postfield", "anchor", "access", "b", "big", "br", "card", "do", as.azq, "fieldset", "go", "head", "i", "img", "input", "meta", "noop", "prev", "onevent", "optgroup", "option", "refresh", ActivitySelector.aSI, "small", "strong", null, "template", "timer", "u", "setvar", "wml"};
            }
            if (bam == null) {
                bam = new String[]{"accept-charset", "align=bottom", "align=center", "align=left", "align=middle", "align=right", "align=top", "alt", "content", null, "domain", "emptyok=false", "emptyok=true", "format", "height", "hspace", "ivalue", "iname", null, "label", "localsrc", "maxlength", "method=get", "method=post", "mode=nowrap", "mode=wrap", "multiple=false", "multiple=true", "name", "newcontext=false", "newcontext=true", "onpick", "onenterbackward", "onenterforward", "ontimer", "optimal=false", "optimal=true", ActivityBookmarkEx.asg, null, null, null, "scheme", "sendreferer=false", "sendreferer=true", "size", ActivityFlash.cak, "ordered=true", "ordered=false", "tabindex", ActivityEditMyNavi.bvj, "type", "type=accept", "type=delete", "type=help", "type=password", "type=onpick", "type=onenterbackward", "type=onenterforward", "type=ontimer", null, null, null, null, null, "type=options", "type=prev", "type=reset", "type=text", "type=vnd.", "href", "href=http://", "href=https://", "value", "vspace", "width", "xml:lang", null, "align", "columns", "class", ActivityBookmarkEx.asj, "forua=false", "forua=true", "src=http://", "src=https://", "http-equiv", "http-equiv=Content-Type", "content=application/vnd.wap.wmlc;charset=", "http-equiv=Expires", null, null};
            }
            if (ban == null) {
                ban = new String[]{".com/", ".edu/", ".net/", ".org/", "accept", as.azu, "clear", "delete", "help", bx.bAh, "http://www.", "https://", "https://www.", null, "middle", "nowrap", "onpick", "onenterbackward", "onenterforward", "ontimer", "options", "password", "reset", null, "text", as.azt, "unknown", "wrap", "www."};
                return;
            }
            return;
        }
        this.aZr = new char[512];
        this.aZv = new char[128];
    }

    private final void AN() {
        if (this.aZm == null && this.aZn == null) {
            ds("");
        }
        if (this.type == 3) {
            this.aZj--;
        }
        this.aZB = -1;
        if (this.aZA) {
            this.aZA = false;
            this.type = 3;
        } else if (this.aZE != null) {
            for (int i = 0; i < this.aZE.length(); i++) {
                fP(this.aZE.charAt(i));
            }
            this.aZE = null;
            this.type = 9;
        } else if (!this.aZh || (this.aZD <= 0 && (fQ(0) != -1 || this.aZj <= 0))) {
            this.aZz = null;
            this.name = null;
            this.aZy = null;
            this.type = AP();
            switch (this.type) {
                case 1:
                    return;
                case 2:
                    bl(false);
                    return;
                case 3:
                    AO();
                    return;
                case 4:
                    k(60, !this.aZJ);
                    if (this.aZj == 0 && this.aZx) {
                        this.type = 7;
                        return;
                    }
                    return;
                case 5:
                default:
                    this.type = bj(this.aZJ);
                    return;
                case 6:
                    AQ();
                    return;
            }
        } else {
            int i2 = (this.aZj - 1) << 2;
            this.type = 3;
            this.aZy = this.aZk[i2];
            this.aZz = this.aZk[i2 + 1];
            this.name = this.aZk[i2 + 2];
            if (this.aZD != 1) {
                this.aZE = "missing end tag /" + this.name + " inserted";
            }
            if (this.aZD > 0) {
                this.aZD--;
            }
        }
    }

    private final void AO() {
        read();
        read();
        this.name = AT();
        ro();
        j('>');
        int i = (this.aZj - 1) << 2;
        if (this.aZj == 0) {
            dr("");
            this.type = 9;
            return;
        }
        if (!this.name.equals(this.aZk[i + 3])) {
            dr("expected: /" + this.aZk[i + 3] + " read: " + this.name);
            int i2 = i;
            while (i2 >= 0 && !this.name.toLowerCase().equals(this.aZk[i2 + 3].toLowerCase())) {
                this.aZD++;
                i2 -= 4;
            }
            if (i2 < 0) {
                this.aZD = 0;
                this.type = 9;
                return;
            }
        }
        this.aZy = this.aZk[i];
        this.aZz = this.aZk[i + 1];
        this.name = this.aZk[i + 2];
    }

    private final int AP() {
        switch (fQ(0)) {
            case -1:
                return 1;
            case 38:
                return 6;
            case 60:
                switch (fQ(1)) {
                    case 33:
                    case 63:
                        return aZf;
                    case 47:
                        return 3;
                    default:
                        return 2;
                }
            default:
                return 4;
        }
    }

    private final void AQ() {
        fP(read());
        int i = this.aZw;
        while (true) {
            int fQ = fQ(0);
            if (fQ == 59) {
                read();
                String fO = fO(i);
                this.aZw = i - 1;
                if (this.aZJ && this.type == 6) {
                    this.name = fO;
                }
                if (fO.charAt(0) == '#') {
                    fP(fO.charAt(1) == 'x' ? Integer.parseInt(fO.substring(2), 16) : Integer.parseInt(fO.substring(1)));
                    return;
                }
                String str = (String) aZi.get(fO);
                this.aZI = str == null;
                if (!this.aZI) {
                    for (int i2 = 0; i2 < str.length(); i2++) {
                        fP(str.charAt(i2));
                    }
                    return;
                } else if (!this.aZJ) {
                    dr("unresolved: &" + fO + cd.bVG);
                    return;
                } else {
                    return;
                }
            } else if (fQ >= 128 || ((fQ >= 48 && fQ <= 57) || ((fQ >= 97 && fQ <= 122) || ((fQ >= 65 && fQ <= 90) || fQ == 95 || fQ == 45 || fQ == 35)))) {
                read();
                fP(fQ);
            } else {
                if (!this.aZh) {
                    dr("");
                }
                if (fQ != -1) {
                }
                return;
            }
        }
    }

    private int AR() {
        char[] cArr = new char[1];
        d(cArr);
        return cArr[0];
    }

    private int AS() {
        return this.aZm != null ? this.aZm.read() : g(this.aZn);
    }

    private final String AT() {
        int i = this.aZw;
        int fQ = fQ(0);
        if ((fQ < 97 || fQ > 122) && ((fQ < 65 || fQ > 90) && fQ != 95 && fQ != 58 && fQ < 192 && !this.aZh)) {
            dr("");
        }
        while (true) {
            fP(read());
            int fQ2 = fQ(0);
            if ((fQ2 < 97 || fQ2 > 122) && ((fQ2 < 65 || fQ2 > 90) && !((fQ2 >= 48 && fQ2 <= 57) || fQ2 == 95 || fQ2 == 45 || fQ2 == 58 || fQ2 == 46 || fQ2 >= 183))) {
                String fO = fO(i);
                this.aZw = i;
                return fO;
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private final void AW() {
        if (this.type == 3) {
            this.aZj--;
        }
        if (this.aZA) {
            this.type = 3;
            this.aZA = false;
            return;
        }
        this.aZw = 0;
        this.aZz = null;
        this.name = null;
        int AY = AY();
        this.bag = -2;
        switch (AY) {
            case -1:
                this.type = 1;
                return;
            case 1:
                int i = (this.aZj - 1) << 2;
                this.type = 3;
                this.aZy = this.aZk[i];
                this.aZz = this.aZk[i + 1];
                this.name = this.aZk[i + 2];
                return;
            case 2:
                this.type = 6;
                char readInt = (char) readInt();
                fP(readInt);
                this.name = "#" + ((int) readInt);
                return;
            case 3:
                this.type = 4;
                this.aZv = Bc().toCharArray();
                this.aZw = this.aZv.length;
                return;
            case 64:
            case 65:
            case 66:
            case 128:
            case 129:
            case 130:
            case y.YW:
            case 193:
            case 194:
            case 195:
                this.type = 64;
                this.baj = AY;
                this.bak = fS(AY);
                return;
            case 67:
                ds("");
                break;
            case 131:
                break;
            default:
                fR(AY);
                return;
        }
        this.type = 4;
        this.aZv = AX().toCharArray();
        this.aZw = this.aZv.length;
    }

    private int AY() {
        if (this.bag == -2) {
            this.bag = this.aZn.read();
        }
        return this.bag;
    }

    private final int a(InputStream inputStream, char[] cArr) {
        int i;
        int read;
        int i2 = 0;
        boolean z = false;
        int i3 = 0;
        while (i3 < cArr.length) {
            try {
                if (!z) {
                    i = inputStream.read();
                    if (i == -1) {
                        return i3;
                    }
                } else {
                    z = false;
                    i = i2;
                }
                switch ((i & 255) >> 4) {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        int i4 = i3 + 1;
                        try {
                            cArr[i3] = (char) i;
                            i3 = i4;
                            break;
                        } catch (ArrayIndexOutOfBoundsException e) {
                            return i4;
                        }
                    case 12:
                    case 13:
                        int read2 = inputStream.read();
                        if (read2 == -1) {
                            break;
                        } else if ((read2 & y.YW) != 128) {
                            int i5 = i3 + 1;
                            try {
                                cArr[i3] = (char) i;
                                z = true;
                                i3 = i5;
                                i2 = read2;
                                break;
                            } catch (ArrayIndexOutOfBoundsException e2) {
                                return i5;
                            }
                        } else {
                            int i6 = i3 + 1;
                            try {
                                cArr[i3] = (char) (((i & 31) << 6) | (read2 & 63));
                                i3 = i6;
                                break;
                            } catch (ArrayIndexOutOfBoundsException e3) {
                                return i6;
                            }
                        }
                    case 14:
                        int read3 = inputStream.read();
                        if (!(read3 == -1 || (read = inputStream.read()) == -1)) {
                            if ((read3 & y.YW) != 128 || (read & y.YW) != 128) {
                                return i3;
                            }
                            int i7 = i3 + 1;
                            try {
                                cArr[i3] = (char) (((i & 15) << 12) | ((read3 & 63) << 6) | ((read & 63) << 0));
                                i3 = i7;
                                break;
                            } catch (ArrayIndexOutOfBoundsException e4) {
                                return i7;
                            }
                        }
                        break;
                }
            } catch (ArrayIndexOutOfBoundsException e5) {
                return i3;
            }
        }
        return i3;
    }

    private int a(InputStream inputStream, char[] cArr, String str) {
        int i;
        int i2;
        boolean equals = str.equals("utf-16le");
        int i3 = 0;
        while (i3 < cArr.length) {
            int read = inputStream.read();
            int read2 = inputStream.read();
            if (read == -1 || read2 == -1) {
                break;
            }
            int i4 = i3 + 1;
            if (equals) {
                i = (read2 & 255) << 8;
                i2 = read & 255;
            } else {
                i = read2 & 255;
                i2 = (read & 255) << 8;
            }
            cArr[i3] = (char) (i2 | i);
            i3 = i4;
        }
        return i3;
    }

    private final String[] a(String[] strArr, int i) {
        if (strArr.length >= i) {
            return strArr;
        }
        String[] strArr2 = new String[(i + 16)];
        System.arraycopy(strArr, 0, strArr2, 0, strArr.length);
        return strArr2;
    }

    private int b(InputStream inputStream, char[] cArr) {
        String lowerCase = this.aZq.toLowerCase();
        if (lowerCase.equals("utf-8")) {
            return a(inputStream, cArr);
        }
        if (lowerCase.startsWith("utf-16")) {
            return a(inputStream, cArr, lowerCase);
        }
        return 0;
    }

    private final int bj(boolean z) {
        int i;
        boolean z2;
        int i2;
        String str;
        read();
        int read = read();
        if (read == 63) {
            if ((fQ(0) == 120 || fQ(0) == 88) && (fQ(1) == 109 || fQ(1) == 77)) {
                if (z) {
                    fP(fQ(0));
                    fP(fQ(1));
                }
                read();
                read();
                if ((fQ(0) == 108 || fQ(0) == 76) && fQ(1) <= 32) {
                    if (this.aZu != 1 || this.column > 4) {
                        dr("");
                    }
                    bl(true);
                    if (this.aZB < 1 || !b.VERSION.equals(this.aZC[2])) {
                        dr("version expected");
                    }
                    String str2 = this.aZC[3];
                    int i3 = (1 >= this.aZB || !"encoding".equals(this.aZC[6])) ? 1 : 1 + 1;
                    if (i3 < this.aZB && "standalone".equals(this.aZC[(i3 * 4) + 2])) {
                        String str3 = this.aZC[(i3 * 4) + 3];
                        if (!"yes".equals(str3) && !"no".equals(str3)) {
                            dr("illegal standalone value: " + str3);
                        }
                        i3++;
                    }
                    if (i3 != this.aZB) {
                        dr("illegal xmldecl");
                    }
                    this.aZx = true;
                    this.aZw = 0;
                    return aZg;
                }
            }
            str = "";
            z2 = z;
            i = 8;
            i2 = 63;
        } else if (read != 33) {
            dr("illegal: <" + read);
            return 9;
        } else if (fQ(0) == 45) {
            i2 = 45;
            str = "--";
            z2 = z;
            i = 9;
        } else if (fQ(0) == 91) {
            i = 5;
            z2 = true;
            i2 = 93;
            str = "[CDATA[";
        } else {
            i = 10;
            z2 = z;
            i2 = -1;
            str = "DOCTYPE";
        }
        for (int i4 = 0; i4 < str.length(); i4++) {
            j(str.charAt(i4));
        }
        if (i == 10) {
            bk(z2);
            return i;
        }
        int i5 = 0;
        while (true) {
            int read2 = read();
            if (read2 == -1) {
                return 9;
            }
            if (z2) {
                fP(read2);
            }
            if ((i2 == 63 || read2 == i2) && fQ(0) == i2 && fQ(1) == 62) {
                if (i2 == 45 && i5 == 45) {
                    dr("illegal comment delimiter: --->");
                }
                read();
                read();
                if (!z2 || i2 == 63) {
                    return i;
                }
                this.aZw--;
                return i;
            }
            i5 = read2;
        }
    }

    private final void bk(boolean z) {
        boolean z2 = false;
        int i = 1;
        while (true) {
            int read = read();
            switch (read) {
                case -1:
                    dr("");
                    return;
                case 39:
                    if (z2) {
                        z2 = false;
                        break;
                    } else {
                        z2 = true;
                        break;
                    }
                case 60:
                    if (!z2) {
                        i++;
                        break;
                    }
                    break;
                case 62:
                    if (!z2 && i - 1 == 0) {
                        return;
                    }
            }
            if (z) {
                fP(read);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x009c A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void bl(boolean r11) {
        /*
            r10 = this;
            r9 = 32
            r7 = 62
            r6 = 1
            r5 = 0
            java.lang.String r8 = ""
            if (r11 != 0) goto L_0x000d
            r10.read()
        L_0x000d:
            java.lang.String r0 = r10.AT()
            r10.name = r0
            r10.aZB = r5
        L_0x0015:
            r10.ro()
            int r0 = r10.fQ(r5)
            if (r11 == 0) goto L_0x0029
            r1 = 63
            if (r0 != r1) goto L_0x0099
            r10.read()
            r10.j(r7)
        L_0x0028:
            return
        L_0x0029:
            r1 = 47
            if (r0 != r1) goto L_0x0091
            r10.aZA = r6
            r10.read()
            r10.ro()
            r10.j(r7)
        L_0x0038:
            int r0 = r10.aZj
            int r1 = r0 + 1
            r10.aZj = r1
            int r0 = r0 << 2
            java.lang.String[] r1 = r10.aZk
            int r2 = r0 + 4
            java.lang.String[] r1 = r10.a(r1, r2)
            r10.aZk = r1
            java.lang.String[] r1 = r10.aZk
            int r2 = r0 + 3
            java.lang.String r3 = r10.name
            r1[r2] = r3
            int r1 = r10.aZj
            int[] r2 = r10.aZl
            int r2 = r2.length
            if (r1 < r2) goto L_0x0069
            int r1 = r10.aZj
            int r1 = r1 + 4
            int[] r1 = new int[r1]
            int[] r2 = r10.aZl
            int[] r3 = r10.aZl
            int r3 = r3.length
            java.lang.System.arraycopy(r2, r5, r1, r5, r3)
            r10.aZl = r1
        L_0x0069:
            int[] r1 = r10.aZl
            int r2 = r10.aZj
            int[] r3 = r10.aZl
            int r4 = r10.aZj
            int r4 = r4 - r6
            r3 = r3[r4]
            r1[r2] = r3
            java.lang.String r1 = ""
            r10.aZy = r8
            java.lang.String[] r1 = r10.aZk
            java.lang.String r2 = r10.aZy
            r1[r0] = r2
            java.lang.String[] r1 = r10.aZk
            int r2 = r0 + 1
            java.lang.String r3 = r10.aZz
            r1[r2] = r3
            java.lang.String[] r1 = r10.aZk
            int r0 = r0 + 2
            java.lang.String r2 = r10.name
            r1[r0] = r2
            goto L_0x0028
        L_0x0091:
            if (r0 != r7) goto L_0x0099
            if (r11 != 0) goto L_0x0099
            r10.read()
            goto L_0x0038
        L_0x0099:
            r1 = -1
            if (r0 != r1) goto L_0x00a2
            java.lang.String r0 = ""
            r10.dr(r8)
            goto L_0x0028
        L_0x00a2:
            java.lang.String r0 = r10.AT()
            int r1 = r0.length()
            if (r1 != 0) goto L_0x00b2
            java.lang.String r0 = "attr name expected"
            r10.dr(r0)
            goto L_0x0038
        L_0x00b2:
            int r1 = r10.aZB
            int r2 = r1 + 1
            r10.aZB = r2
            int r1 = r1 << 2
            java.lang.String[] r2 = r10.aZC
            int r3 = r1 + 4
            java.lang.String[] r2 = r10.a(r2, r3)
            r10.aZC = r2
            java.lang.String[] r2 = r10.aZC
            int r3 = r1 + 1
            java.lang.String r4 = ""
            r2[r1] = r8
            java.lang.String[] r1 = r10.aZC
            int r2 = r3 + 1
            r4 = 0
            r1[r3] = r4
            java.lang.String[] r1 = r10.aZC
            int r3 = r2 + 1
            r1[r2] = r0
            r10.ro()
            int r1 = r10.fQ(r5)
            r2 = 61
            if (r1 == r2) goto L_0x0102
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Attr.value missing f. "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            r10.dr(r0)
            java.lang.String[] r0 = r10.aZC
            java.lang.String r1 = "1"
            r0[r3] = r1
            goto L_0x0015
        L_0x0102:
            r0 = 61
            r10.j(r0)
            r10.ro()
            int r0 = r10.fQ(r5)
            r1 = 39
            if (r0 == r1) goto L_0x0132
            r1 = 34
            if (r0 == r1) goto L_0x0132
            java.lang.String r0 = "attr value delimiter missing!"
            r10.dr(r0)
            r0 = r9
        L_0x011c:
            int r1 = r10.aZw
            r10.k(r0, r6)
            java.lang.String[] r2 = r10.aZC
            java.lang.String r4 = r10.fO(r1)
            r2[r3] = r4
            r10.aZw = r1
            if (r0 == r9) goto L_0x0015
            r10.read()
            goto L_0x0015
        L_0x0132:
            r10.read()
            goto L_0x011c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.ay.bl(boolean):void");
    }

    private int d(char[] cArr) {
        return a(this.aZo, cArr);
    }

    private int d(char[] cArr, int i, int i2) {
        return this.aZm != null ? this.aZm.read(cArr, i, i2) : b(this.aZn, cArr);
    }

    private final void dr(String str) {
        if (!this.aZh) {
            ds(str);
        } else if (this.aZE == null) {
            this.aZE = "ERR: " + str;
        }
    }

    private final void ds(String str) {
        if (Bb()) {
            throw new IOException(str.length() < 100 ? str : str.substring(0, 100) + bx.bxN);
        }
        throw new RuntimeException(str);
    }

    private final String fO(int i) {
        return new String(this.aZv, i, this.aZw - i);
    }

    private final void fP(int i) {
        this.aZx &= i <= 32;
        if (this.aZw == this.aZv.length) {
            char[] cArr = new char[(((this.aZw * 4) / 3) + 4)];
            System.arraycopy(this.aZv, 0, cArr, 0, this.aZw);
            this.aZv = cArr;
        }
        char[] cArr2 = this.aZv;
        int i2 = this.aZw;
        this.aZw = i2 + 1;
        cArr2[i2] = (char) (65535 & i);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x001d  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0071  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final int fQ(int r9) {
        /*
            r8 = this;
            r7 = -1
            r6 = 10
            r5 = 1
            r4 = 0
        L_0x0005:
            int r0 = r8.aZG
            if (r9 < r0) goto L_0x008f
            java.io.InputStream r0 = r8.aZo
            if (r0 == 0) goto L_0x003f
            java.io.InputStream r0 = r8.aZo
            int r0 = r0.available()
            if (r0 <= 0) goto L_0x0037
            int r0 = r8.AR()
        L_0x0019:
            java.io.InputStream r1 = r8.aZo
            if (r1 != 0) goto L_0x0026
            char[] r0 = r8.aZr
            int r0 = r0.length
            if (r0 > r5) goto L_0x0041
            int r0 = r8.AS()
        L_0x0026:
            r1 = 13
            if (r0 != r1) goto L_0x0071
            r8.aZH = r5
            int[] r0 = r8.aZF
            int r1 = r8.aZG
            int r2 = r1 + 1
            r8.aZG = r2
            r0[r1] = r6
            goto L_0x0005
        L_0x0037:
            java.io.InputStream r0 = r8.aZo
            r0.close()
            r0 = 0
            r8.aZo = r0
        L_0x003f:
            r0 = r7
            goto L_0x0019
        L_0x0041:
            int r0 = r8.aZs
            int r1 = r8.aZt
            if (r0 >= r1) goto L_0x0058
            char[] r0 = r8.aZr
            int r1 = r8.aZs
            int r2 = r1 + 1
            r8.aZs = r2
            char r0 = r0[r1]
            goto L_0x0026
        L_0x0052:
            r0 = move-exception
            r0 = 1
            r8.k(r0)
        L_0x0058:
            char[] r0 = r8.aZr     // Catch:{ Exception -> 0x0052 }
            r1 = 0
            char[] r2 = r8.aZr     // Catch:{ Exception -> 0x0052 }
            int r2 = r2.length     // Catch:{ Exception -> 0x0052 }
            int r0 = r8.d(r0, r1, r2)     // Catch:{ Exception -> 0x0052 }
            r8.aZt = r0     // Catch:{ Exception -> 0x0052 }
            int r0 = r8.aZt
            if (r0 > 0) goto L_0x006c
            r0 = r7
        L_0x0069:
            r8.aZs = r5
            goto L_0x0026
        L_0x006c:
            char[] r0 = r8.aZr
            char r0 = r0[r4]
            goto L_0x0069
        L_0x0071:
            if (r0 != r6) goto L_0x0084
            boolean r0 = r8.aZH
            if (r0 != 0) goto L_0x0081
            int[] r0 = r8.aZF
            int r1 = r8.aZG
            int r2 = r1 + 1
            r8.aZG = r2
            r0[r1] = r6
        L_0x0081:
            r8.aZH = r4
            goto L_0x0005
        L_0x0084:
            int[] r1 = r8.aZF
            int r2 = r8.aZG
            int r3 = r2 + 1
            r8.aZG = r3
            r1[r2] = r0
            goto L_0x0081
        L_0x008f:
            int[] r0 = r8.aZF
            r0 = r0[r9]
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.ay.fQ(int):int");
    }

    private int g(InputStream inputStream) {
        char[] cArr = new char[1];
        b(inputStream, cArr);
        return cArr[0];
    }

    private final void j(char c) {
        if (read() != c) {
            dr("");
        }
    }

    private long k(long j) {
        return this.aZm != null ? this.aZm.skip(j) : this.aZn.skip(j);
    }

    private final void k(int i, boolean z) {
        boolean z2 = false;
        int i2 = 0;
        int fQ = fQ(0);
        boolean z3 = false;
        while (fQ != -1 && fQ != i) {
            if (i != 32 || (fQ > 32 && fQ != 62)) {
                if (!z2) {
                    z2 = fQ > 32;
                }
                if (fQ == 38) {
                    if (z) {
                        AQ();
                    } else {
                        return;
                    }
                } else if (fQ > 32) {
                    if (z3) {
                        fP(32);
                    }
                    fP(read());
                } else {
                    read();
                }
                if (fQ == 62 && i2 >= 2 && i != 93) {
                    dr("");
                }
                int i3 = fQ == 93 ? i2 + 1 : 0;
                boolean z4 = z2 && fQ <= 32;
                fQ = fQ(0);
                if (fQ == 10 && this.type == 2) {
                    fQ = 32;
                    int i4 = i3;
                    z3 = z4;
                    i2 = i4;
                } else {
                    boolean z5 = z4;
                    i2 = i3;
                    z3 = z5;
                }
            } else {
                return;
            }
        }
    }

    private final int read() {
        int i;
        if (this.aZG == 0) {
            i = fQ(0);
        } else {
            i = this.aZF[0];
            this.aZF[0] = this.aZF[1];
        }
        this.aZG--;
        this.column++;
        if (i == 10) {
            this.aZu++;
            this.column = 1;
        }
        return i;
    }

    private final void ro() {
        while (true) {
            int fQ = fQ(0);
            if (fQ <= 32 && fQ != -1) {
                read();
            } else {
                return;
            }
        }
    }

    public void AU() {
        this.aZu = 1;
        this.column = 0;
        this.type = 0;
        this.name = null;
        this.aZy = null;
        this.aZA = false;
        this.aZB = -1;
        this.aZp = null;
        this.aZs = 0;
        this.aZt = 0;
        this.aZG = 0;
        this.aZj = 0;
        if (aZi == null) {
            aZi = new Hashtable();
            aZi.put("amp", b.XF);
            aZi.put("apos", "'");
            aZi.put("gt", ">");
            aZi.put("lt", "<");
            aZi.put("quot", "\"");
            aZi.put("nbsp", " ");
            aZi.put("shy", "-");
            aZi.put("copy", "©");
            aZi.put("raquo", "»");
            aZi.put("laquo", "«");
            aZi.put("reg", "®");
            aZi.put("yen", "¥");
            aZi.put(b.XF, b.XF);
            aZi.put("uarr", "↑");
            aZi.put("darr", "↓");
            aZi.put("radic", "√");
            aZi.put("ldquo", "“");
            aZi.put("rdquo", "”");
            aZi.put("lsquo", "‘");
            aZi.put("rsquo", "’");
            aZi.put("oline", "￣");
            aZi.put("lsaquo", "<");
            aZi.put("rsaquo", ">");
            aZi.put("laquo", "«");
            aZi.put("raquo", "»");
            aZi.put("ndash", "-");
            aZi.put("mdash", "—");
            aZi.put("larr", "←");
            aZi.put("rarr", "→");
            aZi.put("middot", "·");
            aZi.put("hellip", "…");
            aZi.put("bull", "•");
            aZi.put("sim", "∼");
            aZi.put("sect", "§");
            aZi.put("trade", "™");
            aZi.put("times", "×");
            aZi.put("divide", "÷");
            aZi.put("deg", "°");
        }
    }

    public void AV() {
        this.aZA = true;
        try {
            nextToken();
        } catch (IOException e) {
        }
    }

    /* access modifiers changed from: package-private */
    public String AX() {
        int readInt = readInt();
        if (this.bai == null) {
            this.bai = new Hashtable();
        }
        String str = (String) this.bai.get(new Integer(readInt));
        if (str != null) {
            return str;
        }
        int i = readInt;
        while (i < this.bah.length && this.bah[i] != 0) {
            i++;
        }
        String str2 = new String(this.bah, readInt, i - readInt, this.aZp);
        this.bai.put(new Integer(readInt), str2);
        return str2;
    }

    public void AZ() {
        String substring;
        StringBuffer stringBuffer;
        int Ba = Ba();
        int i = 0;
        while (Ba != 1) {
            String b2 = b(bam, Ba);
            int indexOf = b2.indexOf(61);
            if (indexOf == -1) {
                substring = b2;
                stringBuffer = new StringBuffer();
            } else {
                StringBuffer stringBuffer2 = new StringBuffer(b2.substring(indexOf + 1));
                substring = b2.substring(0, indexOf);
                stringBuffer = stringBuffer2;
            }
            int Ba2 = Ba();
            while (true) {
                if (Ba2 > 128 || Ba2 == 0 || Ba2 == 2 || Ba2 == 3 || Ba2 == 131 || ((Ba2 >= 64 && Ba2 <= 66) || (Ba2 >= 128 && Ba2 <= 130))) {
                    switch (Ba2) {
                        case 0:
                            break;
                        case 2:
                            stringBuffer.append((char) readInt());
                            break;
                        case 3:
                            stringBuffer.append(Bc());
                            break;
                        case 64:
                        case 65:
                        case 66:
                        case 128:
                        case 129:
                        case 130:
                        case y.YW:
                        case 193:
                        case 194:
                        case 195:
                            stringBuffer.append(i(Ba2, fS(Ba2)));
                            break;
                        case 131:
                            stringBuffer.append(AX());
                            break;
                        default:
                            stringBuffer.append(b(ban, Ba2));
                            break;
                    }
                    Ba2 = Ba();
                }
            }
            this.aZC = a(this.aZC, i + 4);
            int i2 = i + 1;
            this.aZC[i] = "";
            int i3 = i2 + 1;
            this.aZC[i2] = null;
            int i4 = i3 + 1;
            this.aZC[i3] = substring;
            this.aZC[i4] = stringBuffer.toString();
            this.aZB++;
            i = i4 + 1;
            Ba = Ba2;
        }
    }

    /* access modifiers changed from: package-private */
    public int Ba() {
        int read = this.aZn.read();
        if (read != -1) {
            return read;
        }
        throw new IOException("");
    }

    public final boolean Bb() {
        return this.aZK == 0;
    }

    /* access modifiers changed from: package-private */
    public String Bc() {
        boolean z = true;
        while (true) {
            int read = this.aZn.read();
            if (read == 0) {
                this.aZx = z;
                String str = new String(this.bap.toByteArray(), this.aZp);
                this.bap.reset();
                return str;
            } else if (read == -1) {
                throw new IOException("");
            } else {
                if (read > 32) {
                    z = false;
                }
                this.bap.write(read);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00fd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.io.InputStream r12, java.lang.String r13, int r14) {
        /*
            r11 = this;
            r10 = -1
            r9 = 60
            r8 = 2
            r7 = 1
            r6 = 0
            r11.aZs = r6
            r11.aZt = r14
            r0 = 0
            if (r12 != 0) goto L_0x0013
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>()
            throw r0
        L_0x0013:
            if (r0 != 0) goto L_0x017c
            boolean r1 = r11.Bb()
            if (r1 == 0) goto L_0x0131
            r1 = r6
        L_0x001c:
            int r2 = r11.aZt
            r3 = 4
            if (r2 >= r3) goto L_0x0027
            int r2 = r12.read()
            if (r2 != r10) goto L_0x006a
        L_0x0027:
            int r2 = r11.aZt
            r3 = 4
            if (r2 != r3) goto L_0x004b
            switch(r1) {
                case -131072: goto L_0x007e;
                case 60: goto L_0x0083;
                case 65279: goto L_0x0079;
                case 3932223: goto L_0x0095;
                case 1006632960: goto L_0x008c;
                case 1006649088: goto L_0x00a4;
                case 1010792557: goto L_0x00b3;
                default: goto L_0x002f;
            }
        L_0x002f:
            r2 = -65536(0xffffffffffff0000, float:NaN)
            r2 = r2 & r1
            r3 = -16842752(0xfffffffffeff0000, float:-1.6947657E38)
            if (r2 != r3) goto L_0x00fd
            java.lang.String r0 = "UTF-16BE"
            char[] r1 = r11.aZr
            char[] r2 = r11.aZr
            char r2 = r2[r8]
            int r2 = r2 << 8
            char[] r3 = r11.aZr
            r4 = 3
            char r3 = r3[r4]
            r2 = r2 | r3
            char r2 = (char) r2
            r1[r6] = r2
            r11.aZt = r7
        L_0x004b:
            r1 = r13
        L_0x004c:
            if (r0 != 0) goto L_0x0056
            boolean r0 = com.uc.c.bc.by(r1)
            if (r0 == 0) goto L_0x016e
            java.lang.String r0 = "UTF-8"
        L_0x0056:
            int r2 = r11.aZt
            boolean r3 = r11.Bb()
            if (r3 == 0) goto L_0x0061
            r11.b(r12, r0)
        L_0x0061:
            java.io.InputStreamReader r3 = r11.aZm
            if (r3 != 0) goto L_0x0171
            java.io.InputStream r3 = r11.aZn
            if (r3 != 0) goto L_0x0171
        L_0x0069:
            return
        L_0x006a:
            int r1 = r1 << 8
            r1 = r1 | r2
            char[] r3 = r11.aZr
            int r4 = r11.aZt
            int r5 = r4 + 1
            r11.aZt = r5
            char r2 = (char) r2
            r3[r4] = r2
            goto L_0x001c
        L_0x0079:
            java.lang.String r0 = "UTF-32BE"
            r11.aZt = r6
            goto L_0x004b
        L_0x007e:
            java.lang.String r0 = "UTF-32LE"
            r11.aZt = r6
            goto L_0x004b
        L_0x0083:
            java.lang.String r0 = "UTF-32BE"
            char[] r1 = r11.aZr
            r1[r6] = r9
            r11.aZt = r7
            goto L_0x004b
        L_0x008c:
            java.lang.String r0 = "UTF-32LE"
            char[] r1 = r11.aZr
            r1[r6] = r9
            r11.aZt = r7
            goto L_0x004b
        L_0x0095:
            java.lang.String r0 = "UTF-16BE"
            char[] r1 = r11.aZr
            r1[r6] = r9
            char[] r1 = r11.aZr
            r2 = 63
            r1[r7] = r2
            r11.aZt = r8
            goto L_0x004b
        L_0x00a4:
            java.lang.String r0 = "UTF-16LE"
            char[] r1 = r11.aZr
            r1[r6] = r9
            char[] r1 = r11.aZr
            r2 = 63
            r1[r7] = r2
            r11.aZt = r8
            goto L_0x004b
        L_0x00b3:
            int r2 = r12.read()
            if (r2 == r10) goto L_0x002f
            char[] r3 = r11.aZr
            int r4 = r11.aZt
            int r5 = r4 + 1
            r11.aZt = r5
            char r5 = (char) r2
            r3[r4] = r5
            r3 = 62
            if (r2 != r3) goto L_0x00b3
            java.lang.String r2 = new java.lang.String
            char[] r3 = r11.aZr
            int r4 = r11.aZt
            r2.<init>(r3, r6, r4)
            java.lang.String r3 = "encoding"
            int r3 = r2.indexOf(r3)
            if (r3 == r10) goto L_0x002f
            r0 = r3
        L_0x00da:
            char r3 = r2.charAt(r0)
            r4 = 34
            if (r3 == r4) goto L_0x00ed
            char r3 = r2.charAt(r0)
            r4 = 39
            if (r3 == r4) goto L_0x00ed
            int r0 = r0 + 1
            goto L_0x00da
        L_0x00ed:
            int r3 = r0 + 1
            char r0 = r2.charAt(r0)
            int r0 = r2.indexOf(r0, r3)
            java.lang.String r0 = r2.substring(r3, r0)
            goto L_0x002f
        L_0x00fd:
            r2 = -65536(0xffffffffffff0000, float:NaN)
            r2 = r2 & r1
            r3 = -131072(0xfffffffffffe0000, float:NaN)
            if (r2 != r3) goto L_0x011b
            java.lang.String r0 = "UTF-16LE"
            char[] r1 = r11.aZr
            char[] r2 = r11.aZr
            r3 = 3
            char r2 = r2[r3]
            int r2 = r2 << 8
            char[] r3 = r11.aZr
            char r3 = r3[r8]
            r2 = r2 | r3
            char r2 = (char) r2
            r1[r6] = r2
            r11.aZt = r7
            goto L_0x004b
        L_0x011b:
            r1 = r1 & -256(0xffffffffffffff00, float:NaN)
            r2 = -272908544(0xffffffffefbbbf00, float:-1.162092E29)
            if (r1 != r2) goto L_0x004b
            java.lang.String r0 = "UTF-8"
            char[] r1 = r11.aZr
            char[] r2 = r11.aZr
            r3 = 3
            char r2 = r2[r3]
            r1[r6] = r2
            r11.aZt = r7
            goto L_0x004b
        L_0x0131:
            r11.aZn = r12
            r11.Ba()
            int r1 = r11.readInt()
            if (r1 != 0) goto L_0x013f
            r11.readInt()
        L_0x013f:
            int r1 = r11.readInt()
            switch(r1) {
                case 4: goto L_0x0159;
                case 106: goto L_0x015c;
                case 2025: goto L_0x015f;
                default: goto L_0x0146;
            }
        L_0x0146:
            r1 = r13
        L_0x0147:
            int r2 = r11.readInt()
            if (r2 <= 0) goto L_0x0168
            java.io.InputStream r3 = r11.aZn
            r4 = 512(0x200, float:7.175E-43)
            byte[] r2 = com.uc.c.bc.b(r3, r2, r4)
            r11.bah = r2
            goto L_0x004c
        L_0x0159:
            java.lang.String r1 = "ISO-8859-1"
            goto L_0x0147
        L_0x015c:
            java.lang.String r1 = "UTF-8"
            goto L_0x0147
        L_0x015f:
            boolean r1 = com.uc.c.w.nb()
            if (r1 == 0) goto L_0x0146
            java.lang.String r1 = "GB2312"
            goto L_0x0147
        L_0x0168:
            byte[] r2 = new byte[r2]
            r11.bah = r2
            goto L_0x004c
        L_0x016e:
            r0 = r1
            goto L_0x0056
        L_0x0171:
            r11.AU()
            r11.aZq = r0
            r11.aZp = r1
            r11.aZt = r2
            goto L_0x0069
        L_0x017c:
            r1 = r13
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.c.ay.a(java.io.InputStream, java.lang.String, int):void");
    }

    public void aa(byte[] bArr) {
        if (bArr != null && bArr.length > 0) {
            this.aZo = new ByteArrayInputStream(bArr);
        }
    }

    /* access modifiers changed from: package-private */
    public String b(String[] strArr, int i) {
        int i2 = (i & 127) - 5;
        if (i2 == -1) {
            this.baj = -1;
            return AX();
        } else if (i2 < 0 || strArr == null || i2 >= strArr.length || strArr[i2] == null) {
            return "";
        } else {
            this.baj = i2 + 5;
            return strArr[i2];
        }
    }

    public void b(InputStream inputStream, String str) {
        Exception e;
        Exception e2;
        String str2;
        String str3;
        String str4;
        try {
            str2 = str.toUpperCase();
            try {
                str3 = str2;
                str4 = str.toLowerCase();
            } catch (Exception e3) {
                str3 = str2;
                str4 = str;
                if (!ca.bNE.equals(str3)) {
                }
                this.aZn = inputStream;
            }
        } catch (Exception e4) {
            str2 = str;
        }
        try {
            if (!ca.bNE.equals(str3) || str3.startsWith("UTF-16")) {
                this.aZn = inputStream;
            } else {
                this.aZm = new InputStreamReader(inputStream, str);
            }
        } catch (Exception e5) {
            boolean z = true;
            if (str3 == null || str.equals(str3)) {
                e = null;
            } else {
                try {
                    this.aZm = new InputStreamReader(inputStream, str3);
                    z = false;
                    e = null;
                } catch (Exception e6) {
                    e = e6;
                }
            }
            if (!z || str4 == null || str.equals(str4)) {
                e2 = null;
            } else {
                try {
                    this.aZm = new InputStreamReader(inputStream, str4);
                    e2 = null;
                    e = null;
                } catch (Exception e7) {
                    e2 = e7;
                }
            }
            if ((e2 != null && (e2 instanceof UnsupportedEncodingException)) || (e != null && (e instanceof UnsupportedEncodingException))) {
                throw new UnsupportedEncodingException();
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public void clear() {
        try {
            if (this.aZm != null) {
                this.aZm.close();
            }
            if (this.aZm != null) {
                try {
                    this.aZm.close();
                } catch (Exception e) {
                }
            }
        } catch (Exception e2) {
            if (this.aZm != null) {
                try {
                    this.aZm.close();
                } catch (Exception e3) {
                }
            }
        } catch (Throwable th) {
            if (this.aZm != null) {
                try {
                    this.aZm.close();
                } catch (Exception e4) {
                }
            }
            throw th;
        }
        try {
            if (this.aZo != null) {
                this.aZo.close();
            }
        } catch (Exception e5) {
        }
        this.aZo = null;
        try {
            if (this.aZn != null) {
                this.aZn.close();
            }
        } catch (Exception e6) {
        }
        this.aZn = null;
        this.aZm = null;
        this.aZr = null;
        this.aZC = null;
        this.aZv = null;
        this.aZk = null;
        this.aZl = null;
        this.aZF = null;
        this.bah = null;
        this.bai = null;
        this.bak = null;
        if (this.bap != null) {
            try {
                this.bap.close();
            } catch (Exception e7) {
            }
        }
        this.bap = null;
    }

    /* access modifiers changed from: package-private */
    public void fR(int i) {
        this.type = 2;
        this.name = b(bal, i & 63);
        this.aZB = 0;
        if ((i & 128) != 0) {
            AZ();
        }
        this.aZA = (i & 64) == 0;
        int i2 = this.aZj;
        this.aZj = i2 + 1;
        int i3 = i2 << 2;
        this.aZk = a(this.aZk, i3 + 4);
        this.aZk[i3 + 3] = this.name;
        if (this.aZj >= this.aZl.length) {
            int[] iArr = new int[(this.aZj + 4)];
            System.arraycopy(this.aZl, 0, iArr, 0, this.aZl.length);
            this.aZl = iArr;
        }
        this.aZl[this.aZj] = this.aZl[this.aZj - 1];
        for (int i4 = this.aZB - 1; i4 > 0; i4--) {
            for (int i5 = 0; i5 < i4; i5++) {
                if (getAttributeName(i4).equals(getAttributeName(i5))) {
                    ds("");
                }
            }
        }
        this.aZy = "";
        this.aZk[i3] = this.aZy;
        this.aZk[i3 + 1] = this.aZz;
        this.aZk[i3 + 2] = this.name;
    }

    public Object fS(int i) {
        switch (i) {
            case 64:
            case 65:
            case 66:
                return Bc();
            case 128:
            case 129:
            case 130:
                return new Integer(readInt());
            case y.YW:
            case 193:
            case 194:
                return null;
            case 195:
                int readInt = readInt();
                byte[] bArr = new byte[readInt];
                while (readInt > 0) {
                    readInt -= this.aZn.read(bArr, bArr.length - readInt, readInt);
                }
                return bArr;
            default:
                ds("");
                return null;
        }
    }

    public int getAttributeCount() {
        return this.aZB;
    }

    public String getAttributeName(int i) {
        if (i < this.aZB) {
            return this.aZC[(i << 2) + 2].toLowerCase();
        }
        throw new IndexOutOfBoundsException();
    }

    public String getAttributeValue(int i) {
        if (i < this.aZB) {
            return this.aZC[(i << 2) + 3];
        }
        throw new IndexOutOfBoundsException();
    }

    public String getAttributeValue(String str, String str2) {
        for (int i = (this.aZB << 2) - 4; i >= 0; i -= 4) {
            if (this.aZC[i + 2].toLowerCase().equals(str2) && (str == null || this.aZC[i].equals(str))) {
                return this.aZC[i + 3];
            }
        }
        return null;
    }

    public int getEventType() {
        return this.type;
    }

    public String getInputEncoding() {
        return this.aZp;
    }

    public String getName() {
        return this.name;
    }

    public int getNamespaceCount(int i) {
        if (i <= this.aZj) {
            return this.aZl[i];
        }
        throw new IndexOutOfBoundsException();
    }

    public String getText() {
        if (this.type < 4 || (this.type == 6 && this.aZI)) {
            return null;
        }
        return fO(0);
    }

    /* access modifiers changed from: protected */
    public String i(int i, Object obj) {
        if (!(obj instanceof byte[])) {
            return "$(" + obj + ")";
        }
        StringBuffer stringBuffer = new StringBuffer();
        byte[] bArr = (byte[]) obj;
        for (int i2 = 0; i2 < bArr.length; i2++) {
            stringBuffer.append(bao.charAt((bArr[i2] >> 4) & 15));
            stringBuffer.append(bao.charAt(bArr[i2] & 15));
        }
        return stringBuffer.toString();
    }

    public boolean isEmptyElementTag() {
        if (this.type != 2) {
            ds("");
        }
        return this.aZA;
    }

    public int nextToken() {
        this.aZx = true;
        this.aZw = 0;
        this.aZJ = true;
        if (Bb()) {
            AN();
        } else {
            AW();
        }
        return this.type;
    }

    /* access modifiers changed from: package-private */
    public int readInt() {
        int Ba;
        int i = 0;
        do {
            Ba = Ba();
            i = (i << 7) | (Ba & 127);
        } while ((Ba & 128) != 0);
        return i;
    }

    public void require(int i, String str, String str2) {
        if (i != this.type || (str2 != null && !str2.equals(getName()))) {
            ds("");
        }
    }

    public void setInput(InputStream inputStream, String str) {
        a(inputStream, str, 0);
    }
}
