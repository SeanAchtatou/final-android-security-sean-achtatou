package com.uc.h;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.NinePatchDrawable;
import android.graphics.drawable.StateListDrawable;
import android.util.DisplayMetrics;
import android.view.Display;
import b.a.a.a.u;
import com.uc.b.d;
import com.uc.browser.ModelBrowser;
import com.uc.browser.UCR;
import com.uc.c.w;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.WeakHashMap;

public class e {
    public static final byte bZA = 3;
    public static final byte bZB = 4;
    private static final int bZC = 153600;
    private static final boolean bZD = false;
    private static int[] bZJ = null;
    private static e bZP = new e();
    static File bZQ = null;
    private static Method bZT = null;
    private static Method bZU = null;
    private static Method bZV = null;
    private static Constructor bZW = null;
    public static final Bitmap bZa = Bitmap.createBitmap(1, 1, Bitmap.Config.RGB_565);
    public static final int bZb = 17;
    public static final int bZc = 0;
    public static final int bZd = 1;
    public static final int bZe = 2;
    public static final int bZf = 3;
    public static final int bZg = 4;
    public static final int bZh = 5;
    public static final int bZi = 6;
    public static final int bZj = 7;
    public static final int bZk = 8;
    public static final int bZl = 9;
    public static final int bZm = 10;
    public static final int bZn = 11;
    public static final int bZo = 12;
    public static final int bZp = 13;
    public static final int bZq = 14;
    public static final int bZr = 15;
    public static final int bZs = 16;
    private static int[] bZt = new int[17];
    public static final int bZu = 0;
    public static final int bZv = 10000;
    public static final int bZw = 20000;
    public static final byte bZx = 0;
    public static final byte bZy = 1;
    public static final byte bZz = 2;
    private static int[] eR = null;
    public static final String tag = "ResManager";
    private boolean bWF = false;
    private int bZE = ModelBrowser.zX;
    private int bZF = ModelBrowser.zX;
    private int bZG = bZC;
    private LinkedList bZH = new LinkedList();
    private WeakHashMap bZI = new WeakHashMap();
    private byte[][] bZK = null;
    private byte[][] bZL = null;
    private byte[][] bZM = null;
    private byte[][] bZN = null;
    private byte[][] bZO = null;
    private AssetManager bZR = null;
    private DisplayMetrics bZS = new DisplayMetrics();
    private Hashtable bZX;
    private Context cr = null;
    private boolean ua = false;

    private e() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c6, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00c7, code lost:
        r9 = r2;
        r2 = r0;
        r0 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00d1, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x017e, code lost:
        r2 = r0;
        r0 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0052 A[SYNTHETIC, Splitter:B:25:0x0052] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0080 A[Catch:{ IOException -> 0x00a6, all -> 0x00c6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00c6 A[ExcHandler: all (r2v8 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:11:0x001c] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00cc A[SYNTHETIC, Splitter:B:49:0x00cc] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00d1 A[Catch:{ IOException -> 0x0163 }] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0154 A[SYNTHETIC, Splitter:B:64:0x0154] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0159 A[Catch:{ IOException -> 0x015f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int[] C(java.lang.String r11, int r12) {
        /*
            r10 = this;
            r0 = 160(0xa0, float:2.24E-43)
            r6 = 0
            switch(r12) {
                case 1: goto L_0x003e;
                case 2: goto L_0x003b;
                case 3: goto L_0x0036;
                default: goto L_0x0006;
            }
        L_0x0006:
            r10.bZE = r0
        L_0x0008:
            if (r11 == 0) goto L_0x0043
            java.lang.String r0 = "data/data"
            boolean r0 = r11.contains(r0)     // Catch:{ IOException -> 0x0171, all -> 0x0166 }
            if (r0 == 0) goto L_0x0043
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0171, all -> 0x0166 }
            r0.<init>(r11)     // Catch:{ IOException -> 0x0171, all -> 0x0166 }
        L_0x0017:
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ IOException -> 0x0177, all -> 0x016b }
            r1.<init>(r0)     // Catch:{ IOException -> 0x0177, all -> 0x016b }
            int[] r2 = com.uc.h.e.bZt     // Catch:{ IOException -> 0x017d, all -> 0x00c6 }
            r2 = r2[r12]     // Catch:{ IOException -> 0x017d, all -> 0x00c6 }
            long r3 = (long) r2     // Catch:{ IOException -> 0x017d, all -> 0x00c6 }
            r1.skip(r3)     // Catch:{ IOException -> 0x017d, all -> 0x00c6 }
            int r3 = r1.readInt()     // Catch:{ IOException -> 0x017d, all -> 0x00c6 }
            if (r3 != 0) goto L_0x0052
            if (r0 == 0) goto L_0x002f
            r0.close()     // Catch:{ IOException -> 0x0182 }
        L_0x002f:
            if (r1 == 0) goto L_0x0034
            r1.close()     // Catch:{ IOException -> 0x0182 }
        L_0x0034:
            r0 = r6
        L_0x0035:
            return r0
        L_0x0036:
            r0 = 120(0x78, float:1.68E-43)
            r10.bZE = r0
            goto L_0x0008
        L_0x003b:
            r10.bZE = r0
            goto L_0x0008
        L_0x003e:
            int r0 = r10.bZF
            r10.bZE = r0
            goto L_0x0008
        L_0x0043:
            android.content.Context r0 = r10.cr     // Catch:{ IOException -> 0x0171, all -> 0x0166 }
            android.content.res.AssetManager r0 = r0.getAssets()     // Catch:{ IOException -> 0x0171, all -> 0x0166 }
            r10.bZR = r0     // Catch:{ IOException -> 0x0171, all -> 0x0166 }
            android.content.res.AssetManager r0 = r10.bZR     // Catch:{ IOException -> 0x0171, all -> 0x0166 }
            java.io.InputStream r0 = r0.open(r11)     // Catch:{ IOException -> 0x0171, all -> 0x0166 }
            goto L_0x0017
        L_0x0052:
            byte r4 = r1.readByte()     // Catch:{ IOException -> 0x017d, all -> 0x00c6 }
            int r5 = r1.readInt()     // Catch:{ IOException -> 0x017d, all -> 0x00c6 }
            int[] r6 = new int[r3]     // Catch:{ IOException -> 0x017d, all -> 0x00c6 }
            byte[][] r7 = new byte[r3][]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r10.bZK = r7     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            byte[][] r7 = new byte[r3][]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r10.bZL = r7     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            byte[][] r7 = new byte[r3][]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r10.bZM = r7     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            byte[][] r7 = new byte[r3][]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r10.bZN = r7     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            byte[][] r7 = new byte[r3][]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r10.bZO = r7     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            switch(r4) {
                case 0: goto L_0x0096;
                case 1: goto L_0x00d5;
                case 2: goto L_0x00e5;
                case 3: goto L_0x00f6;
                case 4: goto L_0x00b6;
                default: goto L_0x0073;
            }     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
        L_0x0073:
            r4 = 0
            int r7 = r2 + 4
            r6[r4] = r7     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            int r2 = r2 + 4
            r4 = 1
            r9 = r4
            r4 = r2
            r2 = r9
        L_0x007e:
            if (r2 >= r3) goto L_0x0152
            int r4 = r4 + 4
            int r4 = r4 + 1
            int r4 = r4 + r5
            r6[r2] = r4     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            byte r5 = r1.readByte()     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            int r7 = r1.readInt()     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            switch(r5) {
                case 0: goto L_0x0107;
                case 1: goto L_0x0125;
                case 2: goto L_0x0134;
                case 3: goto L_0x0143;
                case 4: goto L_0x0116;
                default: goto L_0x0092;
            }     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
        L_0x0092:
            int r2 = r2 + 1
            r5 = r7
            goto L_0x007e
        L_0x0096:
            byte[][] r4 = r10.bZK     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r7 = 0
            byte[] r8 = new byte[r5]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r4[r7] = r8     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            byte[][] r4 = r10.bZK     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r7 = 0
            r4 = r4[r7]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r1.read(r4)     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            goto L_0x0073
        L_0x00a6:
            r2 = move-exception
            r2 = r0
            r0 = r6
        L_0x00a9:
            if (r2 == 0) goto L_0x00ae
            r2.close()     // Catch:{ IOException -> 0x00b4 }
        L_0x00ae:
            if (r1 == 0) goto L_0x0035
            r1.close()     // Catch:{ IOException -> 0x00b4 }
            goto L_0x0035
        L_0x00b4:
            r1 = move-exception
            goto L_0x0035
        L_0x00b6:
            byte[][] r4 = r10.bZL     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r7 = 0
            byte[] r8 = new byte[r5]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r4[r7] = r8     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            byte[][] r4 = r10.bZL     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r7 = 0
            r4 = r4[r7]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r1.read(r4)     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            goto L_0x0073
        L_0x00c6:
            r2 = move-exception
            r9 = r2
            r2 = r0
            r0 = r9
        L_0x00ca:
            if (r2 == 0) goto L_0x00cf
            r2.close()     // Catch:{ IOException -> 0x0163 }
        L_0x00cf:
            if (r1 == 0) goto L_0x00d4
            r1.close()     // Catch:{ IOException -> 0x0163 }
        L_0x00d4:
            throw r0
        L_0x00d5:
            byte[][] r4 = r10.bZM     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r7 = 0
            byte[] r8 = new byte[r5]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r4[r7] = r8     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            byte[][] r4 = r10.bZM     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r7 = 0
            r4 = r4[r7]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r1.read(r4)     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            goto L_0x0073
        L_0x00e5:
            byte[][] r4 = r10.bZN     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r7 = 0
            byte[] r8 = new byte[r5]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r4[r7] = r8     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            byte[][] r4 = r10.bZN     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r7 = 0
            r4 = r4[r7]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r1.read(r4)     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            goto L_0x0073
        L_0x00f6:
            byte[][] r4 = r10.bZO     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r7 = 0
            byte[] r8 = new byte[r5]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r4[r7] = r8     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            byte[][] r4 = r10.bZO     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r7 = 0
            r4 = r4[r7]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r1.read(r4)     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            goto L_0x0073
        L_0x0107:
            byte[][] r5 = r10.bZK     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            byte[] r8 = new byte[r7]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r5[r2] = r8     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            byte[][] r5 = r10.bZK     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r5 = r5[r2]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r1.read(r5)     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            goto L_0x0092
        L_0x0116:
            byte[][] r5 = r10.bZL     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            byte[] r8 = new byte[r7]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r5[r2] = r8     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            byte[][] r5 = r10.bZL     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r5 = r5[r2]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r1.read(r5)     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            goto L_0x0092
        L_0x0125:
            byte[][] r5 = r10.bZM     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            byte[] r8 = new byte[r7]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r5[r2] = r8     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            byte[][] r5 = r10.bZM     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r5 = r5[r2]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r1.read(r5)     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            goto L_0x0092
        L_0x0134:
            byte[][] r5 = r10.bZN     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            byte[] r8 = new byte[r7]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r5[r2] = r8     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            byte[][] r5 = r10.bZN     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r5 = r5[r2]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r1.read(r5)     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            goto L_0x0092
        L_0x0143:
            byte[][] r5 = r10.bZO     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            byte[] r8 = new byte[r7]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r5[r2] = r8     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            byte[][] r5 = r10.bZO     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r5 = r5[r2]     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            r1.read(r5)     // Catch:{ IOException -> 0x00a6, all -> 0x00c6 }
            goto L_0x0092
        L_0x0152:
            if (r0 == 0) goto L_0x0157
            r0.close()     // Catch:{ IOException -> 0x015f }
        L_0x0157:
            if (r1 == 0) goto L_0x015c
            r1.close()     // Catch:{ IOException -> 0x015f }
        L_0x015c:
            r0 = r6
            goto L_0x0035
        L_0x015f:
            r0 = move-exception
            r0 = r6
            goto L_0x0035
        L_0x0163:
            r1 = move-exception
            goto L_0x00d4
        L_0x0166:
            r0 = move-exception
            r1 = r6
            r2 = r6
            goto L_0x00ca
        L_0x016b:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r6
            goto L_0x00ca
        L_0x0171:
            r0 = move-exception
            r0 = r6
            r1 = r6
            r2 = r6
            goto L_0x00a9
        L_0x0177:
            r1 = move-exception
            r1 = r6
            r2 = r0
            r0 = r6
            goto L_0x00a9
        L_0x017d:
            r2 = move-exception
            r2 = r0
            r0 = r6
            goto L_0x00a9
        L_0x0182:
            r0 = move-exception
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.h.e.C(java.lang.String, int):int[]");
    }

    public static e Pb() {
        return bZP;
    }

    private Drawable S(byte[] bArr, int i) {
        BitmapDrawable bitmapDrawable = new BitmapDrawable(kl(i));
        N(bitmapDrawable);
        return bitmapDrawable;
    }

    private int[] S(DataInputStream dataInputStream) {
        int[] iArr = null;
        try {
            int readInt = dataInputStream.readInt();
            iArr = new int[readInt];
            for (int i = 0; i < readInt; i++) {
                iArr[i] = dataInputStream.readInt();
            }
            if (dataInputStream != null) {
                try {
                    dataInputStream.close();
                } catch (IOException e) {
                }
            }
        } catch (IOException e2) {
            if (dataInputStream != null) {
                try {
                    dataInputStream.close();
                } catch (IOException e3) {
                }
            }
        } catch (Throwable th) {
            if (dataInputStream != null) {
                try {
                    dataInputStream.close();
                } catch (IOException e4) {
                }
            }
            throw th;
        }
        return iArr;
    }

    private Drawable T(byte[] bArr, int i) {
        byte[] bArr2;
        byte[] bArr3;
        Bitmap bitmap;
        DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(bArr));
        byte[] bArr4 = new byte[4];
        try {
            bArr2 = new byte[(dataInputStream.readInt() - 4)];
            try {
                dataInputStream.read(bArr2);
                dataInputStream.read(bArr4);
            } catch (IOException e) {
            }
        } catch (IOException e2) {
            bArr2 = null;
        }
        if (this.bZI.get(Integer.valueOf(i)) != null) {
            bitmap = kl(i);
        } else {
            try {
                byte[] bArr5 = new byte[dataInputStream.readInt()];
                try {
                    dataInputStream.read(bArr5);
                    bArr3 = bArr5;
                } catch (IOException e3) {
                    bArr3 = bArr5;
                }
            } catch (IOException e4) {
                bArr3 = null;
            }
            try {
                bitmap = BitmapFactory.decodeByteArray(bArr3, 0, bArr3.length);
                b(bitmap, this.bZE);
            } catch (Throwable th) {
                bitmap = bZa;
            }
        }
        new Rect();
        NinePatchDrawable a2 = a(bitmap, bArr2, new Rect(bArr4[0], bArr4[1], bArr4[2], bArr4[3]));
        N(a2);
        return a2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0029 A[SYNTHETIC, Splitter:B:8:0x0029] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.drawable.Drawable aN(byte[] r21) {
        /*
            r20 = this;
            java.io.ByteArrayInputStream r5 = new java.io.ByteArrayInputStream
            r0 = r5
            r1 = r21
            r0.<init>(r1)
            java.io.DataInputStream r6 = new java.io.DataInputStream
            r6.<init>(r5)
            r5 = 0
            r7 = 0
            r8 = 0
            byte r7 = r6.readByte()     // Catch:{ IOException -> 0x007c }
            byte r8 = r6.readByte()     // Catch:{ IOException -> 0x0184 }
            r19 = r8
            r8 = r7
            r7 = r19
        L_0x001d:
            byte[] r9 = new byte[r7]
            android.graphics.Bitmap[] r10 = new android.graphics.Bitmap[r7]
            r11 = 0
            r19 = r11
            r11 = r5
            r5 = r19
        L_0x0027:
            if (r5 >= r7) goto L_0x0176
            int r12 = r6.readInt()     // Catch:{ IOException -> 0x008e }
            byte r13 = r6.readByte()     // Catch:{ IOException -> 0x008e }
            if (r13 != 0) goto L_0x0090
            r13 = 1
            int r12 = r12 - r13
            byte[] r12 = new byte[r12]     // Catch:{ IOException -> 0x008e }
            r6.read(r12)     // Catch:{ IOException -> 0x008e }
            r13 = 0
            int r14 = r12.length     // Catch:{ Throwable -> 0x0088 }
            android.graphics.Bitmap r12 = android.graphics.BitmapFactory.decodeByteArray(r12, r13, r14)     // Catch:{ Throwable -> 0x0088 }
            r10[r5] = r12     // Catch:{ Throwable -> 0x0088 }
            r12 = r10[r5]     // Catch:{ Throwable -> 0x0088 }
            r0 = r20
            int r0 = r0.bZE     // Catch:{ Throwable -> 0x0088 }
            r13 = r0
            r0 = r20
            r1 = r12
            r2 = r13
            r0.b(r1, r2)     // Catch:{ Throwable -> 0x0088 }
        L_0x0050:
            byte r12 = r6.readByte()     // Catch:{ IOException -> 0x008e }
            r9[r5] = r12     // Catch:{ IOException -> 0x008e }
            r12 = 0
            int r13 = r6.available()     // Catch:{ IOException -> 0x008e }
            r14 = 4
            if (r13 != r14) goto L_0x0073
            android.graphics.Rect r12 = new android.graphics.Rect     // Catch:{ IOException -> 0x008e }
            byte r13 = r6.readByte()     // Catch:{ IOException -> 0x008e }
            byte r14 = r6.readByte()     // Catch:{ IOException -> 0x008e }
            byte r15 = r6.readByte()     // Catch:{ IOException -> 0x008e }
            byte r16 = r6.readByte()     // Catch:{ IOException -> 0x008e }
            r12.<init>(r13, r14, r15, r16)     // Catch:{ IOException -> 0x008e }
        L_0x0073:
            com.uc.e.b.k r13 = new com.uc.e.b.k     // Catch:{ IOException -> 0x008e }
            r13.<init>(r10, r9, r8, r12)     // Catch:{ IOException -> 0x008e }
            r11 = r13
        L_0x0079:
            int r5 = r5 + 1
            goto L_0x0027
        L_0x007c:
            r9 = move-exception
            r19 = r9
            r9 = r7
            r7 = r19
        L_0x0082:
            r7.printStackTrace()
            r7 = r8
            r8 = r9
            goto L_0x001d
        L_0x0088:
            r12 = move-exception
            android.graphics.Bitmap r12 = com.uc.h.e.bZa     // Catch:{ IOException -> 0x008e }
            r10[r5] = r12     // Catch:{ IOException -> 0x008e }
            goto L_0x0050
        L_0x008e:
            r12 = move-exception
            goto L_0x0079
        L_0x0090:
            r14 = 4
            if (r13 != r14) goto L_0x0079
            byte r13 = r6.readByte()     // Catch:{ IOException -> 0x008e }
            r14 = 4
            int r14 = r13 - r14
            byte[] r14 = new byte[r14]     // Catch:{ IOException -> 0x008e }
            r6.read(r14)     // Catch:{ IOException -> 0x008e }
            r15 = 4
            byte[] r15 = new byte[r15]     // Catch:{ IOException -> 0x008e }
            r6.read(r15)     // Catch:{ IOException -> 0x008e }
            r16 = 1
            int r12 = r12 - r16
            int r12 = r12 - r13
            byte[] r12 = new byte[r12]     // Catch:{ IOException -> 0x008e }
            r6.read(r12)     // Catch:{ IOException -> 0x008e }
            r13 = 0
            r0 = r12
            int r0 = r0.length     // Catch:{ Throwable -> 0x0153 }
            r16 = r0
            r0 = r12
            r1 = r13
            r2 = r16
            android.graphics.Bitmap r12 = android.graphics.BitmapFactory.decodeByteArray(r0, r1, r2)     // Catch:{ Throwable -> 0x0153 }
            r10[r5] = r12     // Catch:{ Throwable -> 0x0153 }
            r12 = r10[r5]     // Catch:{ Throwable -> 0x0153 }
            r0 = r20
            int r0 = r0.bZE     // Catch:{ Throwable -> 0x0153 }
            r13 = r0
            r0 = r20
            r1 = r12
            r2 = r13
            r0.b(r1, r2)     // Catch:{ Throwable -> 0x0153 }
        L_0x00cc:
            android.graphics.Rect r12 = new android.graphics.Rect     // Catch:{ IOException -> 0x008e }
            r12.<init>()     // Catch:{ IOException -> 0x008e }
            r0 = r20
            int r0 = r0.bZG     // Catch:{ IOException -> 0x008e }
            r12 = r0
            r13 = 153600(0x25800, float:2.1524E-40)
            if (r12 >= r13) goto L_0x0107
            android.graphics.Rect r12 = new android.graphics.Rect     // Catch:{ IOException -> 0x008e }
            r13 = 0
            byte r13 = r15[r13]     // Catch:{ IOException -> 0x008e }
            int r13 = r13 * 3
            int r13 = r13 / 4
            r16 = 1
            byte r16 = r15[r16]     // Catch:{ IOException -> 0x008e }
            int r16 = r16 * 3
            int r16 = r16 / 4
            r17 = 2
            byte r17 = r15[r17]     // Catch:{ IOException -> 0x008e }
            int r17 = r17 * 3
            int r17 = r17 / 4
            r18 = 3
            byte r18 = r15[r18]     // Catch:{ IOException -> 0x008e }
            int r18 = r18 * 3
            int r18 = r18 / 4
            r0 = r12
            r1 = r13
            r2 = r16
            r3 = r17
            r4 = r18
            r0.<init>(r1, r2, r3, r4)     // Catch:{ IOException -> 0x008e }
        L_0x0107:
            r0 = r20
            int r0 = r0.bZG     // Catch:{ IOException -> 0x008e }
            r12 = r0
            r13 = 153600(0x25800, float:2.1524E-40)
            if (r12 <= r13) goto L_0x015a
            android.graphics.Rect r12 = new android.graphics.Rect     // Catch:{ IOException -> 0x008e }
            r13 = 0
            byte r13 = r15[r13]     // Catch:{ IOException -> 0x008e }
            int r13 = r13 * 3
            int r13 = r13 / 2
            r16 = 1
            byte r16 = r15[r16]     // Catch:{ IOException -> 0x008e }
            int r16 = r16 * 3
            int r16 = r16 / 2
            r17 = 2
            byte r17 = r15[r17]     // Catch:{ IOException -> 0x008e }
            int r17 = r17 * 3
            int r17 = r17 / 2
            r18 = 3
            byte r15 = r15[r18]     // Catch:{ IOException -> 0x008e }
            int r15 = r15 * 3
            int r15 = r15 / 2
            r0 = r12
            r1 = r13
            r2 = r16
            r3 = r17
            r4 = r15
            r0.<init>(r1, r2, r3, r4)     // Catch:{ IOException -> 0x008e }
        L_0x013c:
            android.graphics.drawable.NinePatchDrawable r13 = new android.graphics.drawable.NinePatchDrawable     // Catch:{ IOException -> 0x008e }
            r15 = r10[r5]     // Catch:{ IOException -> 0x008e }
            r16 = 0
            r0 = r13
            r1 = r15
            r2 = r14
            r3 = r12
            r4 = r16
            r0.<init>(r1, r2, r3, r4)     // Catch:{ IOException -> 0x008e }
            com.uc.e.b.k r12 = new com.uc.e.b.k     // Catch:{ IOException -> 0x008e }
            r12.<init>(r13)     // Catch:{ IOException -> 0x008e }
            r11 = r12
            goto L_0x0079
        L_0x0153:
            r12 = move-exception
            android.graphics.Bitmap r12 = com.uc.h.e.bZa     // Catch:{ IOException -> 0x008e }
            r10[r5] = r12     // Catch:{ IOException -> 0x008e }
            goto L_0x00cc
        L_0x015a:
            android.graphics.Rect r12 = new android.graphics.Rect     // Catch:{ IOException -> 0x008e }
            r13 = 0
            byte r13 = r15[r13]     // Catch:{ IOException -> 0x008e }
            r16 = 1
            byte r16 = r15[r16]     // Catch:{ IOException -> 0x008e }
            r17 = 2
            byte r17 = r15[r17]     // Catch:{ IOException -> 0x008e }
            r18 = 3
            byte r15 = r15[r18]     // Catch:{ IOException -> 0x008e }
            r0 = r12
            r1 = r13
            r2 = r16
            r3 = r17
            r4 = r15
            r0.<init>(r1, r2, r3, r4)     // Catch:{ IOException -> 0x008e }
            goto L_0x013c
        L_0x0176:
            r6.close()     // Catch:{ IOException -> 0x0182 }
        L_0x0179:
            r0 = r20
            int r0 = r0.bZF
            r5 = r0
            r11.setTargetDensity(r5)
            return r11
        L_0x0182:
            r5 = move-exception
            goto L_0x0179
        L_0x0184:
            r9 = move-exception
            r19 = r9
            r9 = r7
            r7 = r19
            goto L_0x0082
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.h.e.aN(byte[]):android.graphics.drawable.Drawable");
    }

    private Drawable aO(byte[] bArr) {
        GradientDrawable gradientDrawable = null;
        float[] fArr = new float[8];
        int[] iArr = new int[28];
        for (int i = 0; i < 28; i++) {
            iArr[i] = ((bArr[i * 4] & 255) << 24) | ((bArr[(i * 4) + 1] & 255) << 16) | ((bArr[(i * 4) + 2] & 255) << 8) | (bArr[(i * 4) + 3] & 255);
        }
        int[] iArr2 = {iArr[12], iArr[10]};
        switch (iArr[6]) {
            case 0:
                gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, iArr2);
                break;
            case UCR.Color.bSo:
                gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.BL_TR, iArr2);
                break;
            case 90:
                gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, iArr2);
                break;
            case 135:
                gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.BR_TL, iArr2);
                break;
            case u.bmk:
                gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, iArr2);
                break;
            case 225:
                gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TR_BL, iArr2);
                break;
            case u.bml:
                gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, iArr2);
                break;
            case 315:
                gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TL_BR, iArr2);
                break;
            case 360:
                gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, iArr2);
                break;
        }
        gradientDrawable.setShape(iArr[0]);
        gradientDrawable.setCornerRadius((float) iArr[1]);
        for (int i2 = 0; i2 < 4; i2++) {
            fArr[i2 * 2] = (float) iArr[i2 + 2];
            fArr[(i2 * 2) + 1] = (float) iArr[i2 + 2];
        }
        gradientDrawable.setGradientCenter((float) iArr[7], (float) iArr[8]);
        gradientDrawable.setGradientRadius((float) iArr[11]);
        gradientDrawable.setGradientType(iArr[13]);
        if (iArr[14] == 1) {
            gradientDrawable.setUseLevel(true);
        } else if (iArr[14] == 0) {
            gradientDrawable.setUseLevel(false);
        }
        gradientDrawable.setSize(iArr[19], iArr[20]);
        gradientDrawable.setStroke(iArr[24], iArr[25], (float) iArr[26], (float) iArr[27]);
        return gradientDrawable;
    }

    private Drawable aP(byte[] bArr) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        byte[] bArr2 = new byte[3];
        int[][] iArr = {new int[]{16842919}, new int[]{16842908}, new int[]{16842913}, new int[]{16842911}, new int[]{16842912}, new int[]{16842910}, new int[]{16842909}};
        for (int i = 0; i < 3; i++) {
            bArr2[i] = bArr[i];
        }
        byte b2 = bArr[3];
        int[] iArr2 = new int[b2];
        byte[][] bArr3 = (byte[][]) Array.newInstance(Byte.TYPE, b2, 7);
        for (int i2 = 0; i2 < b2; i2++) {
            iArr2[i2] = ((bArr[(i2 * 11) + 4] & 255) << 24) | ((bArr[(i2 * 11) + 5] & 255) << 16) | ((bArr[(i2 * 11) + 6] & 255) << 8) | (bArr[(i2 * 11) + 7] & 255);
            Drawable drawable = null;
            if (iArr2[i2] >= 0 && iArr2[i2] < 10000) {
                drawable = new ColorDrawable(getColor(iArr2[i2]));
            } else if (iArr2[i2] >= 10000) {
                drawable = getDrawable(iArr2[i2]);
            }
            for (int i3 = 0; i3 < 7; i3++) {
                bArr3[i2][i3] = bArr[(i2 * 11) + 8 + i3];
            }
            boolean z = false;
            for (int i4 = 0; i4 < bArr3[i2].length; i4++) {
                if (bArr3[i2][i4] == 1) {
                    stateListDrawable.addState(iArr[i4], drawable);
                    z = true;
                }
            }
            if (!z) {
                stateListDrawable.addState(new int[0], drawable);
            }
        }
        return stateListDrawable;
    }

    private void b(Drawable drawable, int i) {
        this.bZX.put(Integer.valueOf(i), new SoftReference(drawable));
    }

    public String Dp() {
        switch (this.bZF) {
            case 120:
                return "ldpi";
            case ModelBrowser.zX:
                return "mdpi";
            case 240:
                return "hdpi";
            case w.Qt /*320*/:
                return this.bZG == 614400 ? "mdpi" : "hdpi";
            default:
                return "mdpi";
        }
    }

    public void N(Drawable drawable) {
        Class<DisplayMetrics> cls = DisplayMetrics.class;
        if (!this.bWF) {
            if (drawable instanceof NinePatchDrawable) {
                try {
                    if (bZT == null) {
                        bZT = NinePatchDrawable.class.getMethod("setTargetDensity", DisplayMetrics.class);
                    }
                    bZT.invoke(drawable, this.bZS);
                } catch (Exception e) {
                    this.bWF = true;
                }
            } else {
                try {
                    if (bZU == null) {
                        bZU = BitmapDrawable.class.getMethod("setTargetDensity", DisplayMetrics.class);
                    }
                    bZU.invoke(drawable, this.bZS);
                } catch (Exception e2) {
                    this.bWF = true;
                }
            }
        }
    }

    public boolean Pc() {
        this.bZS = this.cr.getApplicationContext().getResources().getDisplayMetrics();
        Display defaultDisplay = ((Activity) this.cr).getWindowManager().getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        this.bZG = width * height;
        this.ua = width > height;
        return this.ua;
    }

    public void Pd() {
        this.bZS = this.cr.getApplicationContext().getResources().getDisplayMetrics();
        Display defaultDisplay = ((Activity) this.cr).getWindowManager().getDefaultDisplay();
        this.ua = defaultDisplay.getWidth() > defaultDisplay.getHeight();
    }

    public void Pe() {
        this.bZI.clear();
    }

    public String Pf() {
        switch (this.bZG) {
            case 76800:
            case 96000:
                return "_l";
            case bZC /*153600*/:
                return "_m";
            case 384000:
            case 409920:
            case 518400:
            case 614400:
                return "_h";
            default:
                return "_m";
        }
    }

    public Bitmap a(Resources resources, int i, BitmapFactory.Options options) {
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeResource(resources, i, options);
        } catch (Exception e) {
            return kl(i);
        } catch (OutOfMemoryError e2) {
        }
        return bitmap == null ? kl(i) : bitmap;
    }

    public NinePatchDrawable a(Bitmap bitmap, byte[] bArr, Rect rect) {
        if (this.bWF) {
            return new NinePatchDrawable(bitmap, bArr, rect, null);
        }
        try {
            if (bZW == null) {
                bZW = NinePatchDrawable.class.getConstructor(Resources.class, Bitmap.class, byte[].class, Rect.class, String.class);
            }
            return (NinePatchDrawable) bZW.newInstance(this.cr.getResources(), bitmap, bArr, rect, null);
        } catch (Exception e) {
            this.bWF = true;
            return new NinePatchDrawable(bitmap, bArr, rect, null);
        }
    }

    public void a(a aVar) {
        this.bZH.add(new WeakReference(aVar));
    }

    public void b(Bitmap bitmap, int i) {
        if (!this.bWF) {
            try {
                if (bZV == null) {
                    bZV = Bitmap.class.getMethod("setDensity", Integer.TYPE);
                }
                bZV.invoke(bitmap, Integer.valueOf(i));
            } catch (Exception e) {
                this.bWF = true;
            }
        }
    }

    public void b(a aVar) {
        a aVar2;
        int size = this.bZH.size() - 1;
        while (size >= 0) {
            WeakReference weakReference = (WeakReference) this.bZH.get(size);
            if (weakReference == null || !((aVar2 = (a) weakReference.get()) == null || aVar == aVar2)) {
                size--;
            } else {
                this.bZH.remove(size);
                return;
            }
        }
    }

    public void d(Context context) {
        this.cr = context;
        int i = ModelBrowser.zX;
        try {
            i = DisplayMetrics.class.getField("densityDpi").getInt(context.getResources().getDisplayMetrics());
        } catch (Exception e) {
            this.bWF = true;
        }
        this.bZF = i;
        this.ua = Pc();
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0041 A[Catch:{ IOException -> 0x0106, all -> 0x00ff }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0067 A[Catch:{ IOException -> 0x0106, all -> 0x00ff }, LOOP:1: B:22:0x0063->B:24:0x0067, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x007a A[SYNTHETIC, Splitter:B:27:0x007a] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00b9 A[SYNTHETIC, Splitter:B:48:0x00b9] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00bd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void fY(java.lang.String r10) {
        /*
            r9 = this;
            r4 = 0
            r7 = 3
            r6 = 2
            r5 = 1
            android.content.Context r0 = r9.cr
            if (r0 != 0) goto L_0x0008
        L_0x0008:
            java.util.Hashtable r0 = r9.bZX
            if (r0 == 0) goto L_0x0011
            java.util.Hashtable r0 = r9.bZX
            r0.clear()
        L_0x0011:
            java.util.Hashtable r0 = new java.util.Hashtable
            r0.<init>()
            r9.bZX = r0
            r0 = 0
            if (r10 == 0) goto L_0x0047
            java.lang.String r1 = "data/data"
            boolean r1 = r10.contains(r1)     // Catch:{ IOException -> 0x00a4, all -> 0x00b3 }
            if (r1 == 0) goto L_0x0047
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ IOException -> 0x00a4, all -> 0x00b3 }
            r1.<init>(r10)     // Catch:{ IOException -> 0x00a4, all -> 0x00b3 }
            r0 = r1
        L_0x0029:
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ IOException -> 0x0106, all -> 0x00ff }
            r1.<init>(r0)     // Catch:{ IOException -> 0x0106, all -> 0x00ff }
            r1.readInt()     // Catch:{ IOException -> 0x0106, all -> 0x00ff }
            r1.readUTF()     // Catch:{ IOException -> 0x0106, all -> 0x00ff }
            r1.readInt()     // Catch:{ IOException -> 0x0106, all -> 0x00ff }
            r1.readUTF()     // Catch:{ IOException -> 0x0106, all -> 0x00ff }
            int r2 = r1.readInt()     // Catch:{ IOException -> 0x0106, all -> 0x00ff }
            r3 = r4
        L_0x003f:
            if (r3 >= r2) goto L_0x0056
            r1.readUTF()     // Catch:{ IOException -> 0x0106, all -> 0x00ff }
            int r3 = r3 + 1
            goto L_0x003f
        L_0x0047:
            android.content.Context r1 = r9.cr     // Catch:{ IOException -> 0x00a4, all -> 0x00b3 }
            android.content.res.AssetManager r1 = r1.getAssets()     // Catch:{ IOException -> 0x00a4, all -> 0x00b3 }
            r9.bZR = r1     // Catch:{ IOException -> 0x00a4, all -> 0x00b3 }
            android.content.res.AssetManager r1 = r9.bZR     // Catch:{ IOException -> 0x00a4, all -> 0x00b3 }
            java.io.InputStream r0 = r1.open(r10)     // Catch:{ IOException -> 0x00a4, all -> 0x00b3 }
            goto L_0x0029
        L_0x0056:
            r1.readUTF()     // Catch:{ IOException -> 0x0106, all -> 0x00ff }
            int r2 = r1.readInt()     // Catch:{ IOException -> 0x0106, all -> 0x00ff }
            byte[] r2 = new byte[r2]     // Catch:{ IOException -> 0x0106, all -> 0x00ff }
            r1.read(r2)     // Catch:{ IOException -> 0x0106, all -> 0x00ff }
            r2 = r4
        L_0x0063:
            r3 = 17
            if (r2 >= r3) goto L_0x0072
            int[] r3 = com.uc.h.e.bZt     // Catch:{ IOException -> 0x0106, all -> 0x00ff }
            int r4 = r1.readInt()     // Catch:{ IOException -> 0x0106, all -> 0x00ff }
            r3[r2] = r4     // Catch:{ IOException -> 0x0106, all -> 0x00ff }
            int r2 = r2 + 1
            goto L_0x0063
        L_0x0072:
            int[] r1 = r9.S(r1)     // Catch:{ IOException -> 0x0106, all -> 0x00ff }
            com.uc.h.e.eR = r1     // Catch:{ IOException -> 0x0106, all -> 0x00ff }
            if (r0 == 0) goto L_0x007d
            r0.close()     // Catch:{ IOException -> 0x00fb }
        L_0x007d:
            java.lang.String r0 = r9.Dp()
            java.lang.String r1 = "hdpi"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x00bd
            int[] r0 = r9.C(r10, r5)
            com.uc.h.e.bZJ = r0
            int[] r0 = com.uc.h.e.bZJ
            if (r0 != 0) goto L_0x0099
            int[] r0 = r9.C(r10, r6)
            com.uc.h.e.bZJ = r0
        L_0x0099:
            int[] r0 = com.uc.h.e.bZJ
            if (r0 != 0) goto L_0x00a3
            int[] r0 = r9.C(r10, r7)
            com.uc.h.e.bZJ = r0
        L_0x00a3:
            return
        L_0x00a4:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x00a8:
            r0.printStackTrace()     // Catch:{ all -> 0x0104 }
            if (r1 == 0) goto L_0x007d
            r1.close()     // Catch:{ IOException -> 0x00b1 }
            goto L_0x007d
        L_0x00b1:
            r0 = move-exception
            goto L_0x007d
        L_0x00b3:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x00b7:
            if (r1 == 0) goto L_0x00bc
            r1.close()     // Catch:{ IOException -> 0x00fd }
        L_0x00bc:
            throw r0
        L_0x00bd:
            java.lang.String r1 = "ldpi"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x00e0
            int[] r0 = r9.C(r10, r7)
            com.uc.h.e.bZJ = r0
            int[] r0 = com.uc.h.e.bZJ
            if (r0 != 0) goto L_0x00d5
            int[] r0 = r9.C(r10, r6)
            com.uc.h.e.bZJ = r0
        L_0x00d5:
            int[] r0 = com.uc.h.e.bZJ
            if (r0 != 0) goto L_0x00a3
            int[] r0 = r9.C(r10, r5)
            com.uc.h.e.bZJ = r0
            goto L_0x00a3
        L_0x00e0:
            int[] r0 = r9.C(r10, r6)
            com.uc.h.e.bZJ = r0
            int[] r0 = com.uc.h.e.bZJ
            if (r0 != 0) goto L_0x00f0
            int[] r0 = r9.C(r10, r5)
            com.uc.h.e.bZJ = r0
        L_0x00f0:
            int[] r0 = com.uc.h.e.bZJ
            if (r0 != 0) goto L_0x00a3
            int[] r0 = r9.C(r10, r7)
            com.uc.h.e.bZJ = r0
            goto L_0x00a3
        L_0x00fb:
            r0 = move-exception
            goto L_0x007d
        L_0x00fd:
            r1 = move-exception
            goto L_0x00bc
        L_0x00ff:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00b7
        L_0x0104:
            r0 = move-exception
            goto L_0x00b7
        L_0x0106:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00a8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.h.e.fY(java.lang.String):void");
    }

    public Bitmap fZ(String str) {
        try {
            return BitmapFactory.decodeStream(this.cr.getAssets().open("res/" + str + Pf() + ".png"));
        } catch (IOException e) {
            return null;
        }
    }

    public int getColor(int i) {
        if (eR == null) {
            return -7829368;
        }
        return eR[i];
    }

    public Drawable getDrawable(int i) {
        Drawable drawable;
        int i2 = i - 10000;
        Drawable drawable2 = null;
        SoftReference softReference = (SoftReference) this.bZX.get(Integer.valueOf(i2));
        if (softReference != null) {
            if (!(this.bZK[i2] == null || i == 10202)) {
                drawable2 = (Drawable) softReference.get();
            }
            if (!(this.bZM[i2] == null || (drawable2 = (Drawable) softReference.get()) == null)) {
                drawable2 = drawable2.mutate();
            }
            if (this.bZO[i2] != null) {
                drawable = (Drawable) softReference.get();
                if (drawable != null) {
                    drawable = drawable.mutate();
                }
            } else {
                drawable = drawable2;
            }
        } else {
            drawable = null;
        }
        if (drawable != null) {
            return drawable;
        }
        if (this.bZK[i2] != null) {
            Drawable S = S(this.bZK[i2], i);
            b(S, i2);
            return S;
        } else if (this.bZL[i2] != null) {
            return T(this.bZL[i2], i);
        } else {
            if (this.bZO[i2] != null) {
                Drawable aN = aN(this.bZO[i2]);
                b(aN, i2);
                return aN;
            } else if (this.bZN[i2] != null) {
                return aP(this.bZN[i2]);
            } else {
                if (this.bZM[i2] != null) {
                    Drawable aO = aO(this.bZM[i2]);
                    b(aO, i2);
                    return aO;
                }
                throw new IllegalArgumentException("Resources not found!");
            }
        }
    }

    public String getString(int i) {
        if (i == -1) {
            return null;
        }
        return this.cr.getResources().getString(i);
    }

    public Bitmap kl(int i) {
        int i2 = i - 10000;
        Bitmap bitmap = (Bitmap) this.bZI.get(Integer.valueOf(i));
        if (bitmap == null) {
            try {
                bitmap = BitmapFactory.decodeByteArray(this.bZK[i2], 0, this.bZK[i2].length, d.fL());
                if (this.bZE != this.bZF) {
                    Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap, (bitmap.getWidth() * this.bZF) / this.bZE, (bitmap.getHeight() * this.bZF) / this.bZE, true);
                    bitmap.recycle();
                    bitmap = createScaledBitmap;
                }
                b(bitmap, this.bZF);
            } catch (Throwable th) {
                bitmap = bZa;
            }
            this.bZI.put(Integer.valueOf(i), bitmap);
        }
        return bitmap;
    }

    public Drawable km(int i) {
        try {
            return this.cr.getResources().getDrawable(i);
        } catch (Throwable th) {
            return new BitmapDrawable();
        }
    }

    public int kn(int i) {
        return this.cr.getResources().getColor(i);
    }

    public Bitmap ko(int i) {
        return BitmapFactory.decodeResource(this.cr.getResources(), i);
    }

    public int kp(int i) {
        return (int) this.cr.getResources().getDimension(i);
    }

    public float kq(int i) {
        return this.cr.getResources().getDimension(i);
    }

    public String[] kr(int i) {
        return this.cr.getResources().getStringArray(i);
    }

    public void update() {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.bZH.size()) {
                break;
            }
            WeakReference weakReference = (WeakReference) this.bZH.get(i2);
            if (weakReference != null) {
                a aVar = (a) weakReference.get();
                if (aVar != null) {
                    aVar.k();
                } else {
                    arrayList.add(weakReference);
                }
            }
            i = i2 + 1;
        }
        this.bZH.removeAll(arrayList);
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(133, (Object) null);
        }
    }
}
