package com.uc.h;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.uc.c.cd;

public class c {
    private String bkr;
    private int bks;
    private String bkt;
    private int bku;
    private String bkv;
    private byte[] bkw;
    private String bkx;
    /* access modifiers changed from: private */
    public boolean bky = false;
    final /* synthetic */ d bkz;
    private String name;

    public c(d dVar, String str, String str2, int i, String str3, int i2, String str4, byte[] bArr, Boolean bool) {
        this.bkz = dVar;
        this.name = str;
        this.bkr = str2;
        this.bks = i;
        this.bkt = str3;
        this.bkw = bArr;
        this.bky = bool.booleanValue();
        this.bku = i2;
        this.bkv = str4;
    }

    public boolean Dj() {
        return this.bky;
    }

    public Bitmap Dk() {
        if (this.bkw == null) {
            return null;
        }
        try {
            return BitmapFactory.decodeByteArray(this.bkw, 0, this.bkw.length);
        } catch (Throwable th) {
            return null;
        }
    }

    public String Dl() {
        return this.bkr;
    }

    public int Dm() {
        return this.bks;
    }

    public String Dn() {
        return this.bkt;
    }

    public int Do() {
        return this.bku;
    }

    public String Dp() {
        return this.bkv;
    }

    public String Dq() {
        return this.bkx;
    }

    public void et(String str) {
        this.bkv = str;
    }

    public void eu(String str) {
        this.bkx = str;
    }

    public void gS(int i) {
        this.bku = i;
    }

    public String getName() {
        return this.name;
    }

    public String toString() {
        return this.name + cd.bVH + this.bkr + cd.bVH + this.bks + cd.bVH + this.bkt + cd.bVH + this.bkx;
    }
}
