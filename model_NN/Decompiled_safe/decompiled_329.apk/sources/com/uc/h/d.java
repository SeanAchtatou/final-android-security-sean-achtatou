package com.uc.h;

import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;
import com.uc.a.e;
import com.uc.a.h;
import com.uc.browser.ModelBrowser;
import com.uc.browser.UCR;
import com.uc.c.au;
import com.uc.c.w;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;

public class d {
    public static final String bxm = "theme/UCSkin.uct";
    public static String bxn = null;
    public static final String bxp = "pref_currskin";
    public static int bxq = 0;
    public static int bxr = 1;
    public static int bxs = 2;
    public static int bxt = 3;
    public static int bxu = 4;
    public static int bxv = 5;
    public static int bxw = 6;
    public static final String tag = "SkinManager";
    String aaP = "";
    String bkx = "";
    private Vector bxo = new Vector();
    private String bxx = "";
    private b bxy;
    private Context cr;

    public d(Context context) {
        this.cr = context;
        bxn = "/data/data/" + this.cr.getPackageName() + "/theme";
        GT();
    }

    private boolean GT() {
        DataInputStream dataInputStream;
        try {
            dataInputStream = new DataInputStream(this.cr.getAssets().open(bxm));
        } catch (IOException e) {
            dataInputStream = null;
        }
        c a2 = a(dataInputStream, true);
        if (a2 != null) {
            a2.eu(bxm);
            this.bxo.add(a2);
        }
        File file = new File(bxn);
        if (!file.exists() || !file.isDirectory()) {
            return false;
        }
        File[] listFiles = file.listFiles();
        for (int i = 0; i < listFiles.length; i++) {
            c o = o(listFiles[i]);
            if (o != null) {
                o.eu(listFiles[i].getPath());
                this.bxo.add(o);
            }
        }
        return true;
    }

    private void GU() {
        if (this.bxo != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.bxo.size()) {
                    Log.d(tag, ((c) this.bxo.get(i2)).toString());
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    private c a(DataInputStream dataInputStream, boolean z) {
        boolean z2;
        if (dataInputStream == null) {
            return null;
        }
        try {
            int readInt = dataInputStream.readInt();
            String readUTF = dataInputStream.readUTF();
            int readInt2 = dataInputStream.readInt();
            String readUTF2 = dataInputStream.readUTF();
            int parseInt = Integer.parseInt(w.NQ);
            if (z) {
                z2 = true;
            } else if (readInt == 14 && readUTF.equals(w.ND) && readInt2 == parseInt) {
                if (readUTF2.contains(e.Pb().Dp())) {
                }
                z2 = true;
            } else {
                z2 = false;
            }
            int readInt3 = dataInputStream.readInt();
            String str = null;
            for (int i = 0; i < readInt3; i++) {
                if (i == bxq) {
                    str = dataInputStream.readUTF();
                } else {
                    dataInputStream.readUTF();
                }
            }
            String readUTF3 = dataInputStream.readUTF();
            byte[] bArr = new byte[dataInputStream.readInt()];
            dataInputStream.read(bArr);
            try {
                dataInputStream.close();
            } catch (IOException e) {
            }
            if (str == null) {
                return null;
            }
            return new c(this, str, readUTF3, readInt, readUTF, readInt2, readUTF2, bArr, Boolean.valueOf(z2));
        } catch (IOException e2) {
            try {
                dataInputStream.close();
            } catch (IOException e3) {
            }
            return null;
        } catch (Throwable th) {
            try {
                dataInputStream.close();
            } catch (IOException e4) {
            }
            throw th;
        }
    }

    private boolean eQ(String str) {
        c o = o(new File(str));
        if (o != null) {
            int Dm = o.Dm();
            String Dn = o.Dn();
            int Do = o.Do();
            String Dp = o.Dp();
            int parseInt = Integer.parseInt(w.NQ);
            if (Dm != 14) {
                Toast.makeText(this.cr, "该皮肤不适用与Android平台,安装失败", 1).show();
            } else if (!Dn.equals(w.ND)) {
                Toast.makeText(this.cr, "皮肤版本不兼容，安装失败", 1).show();
            } else if (parseInt != Do) {
                Toast.makeText(this.cr, "皮肤安装失败", 1).show();
                return false;
            } else if (Dp.contains(e.Pb().Dp())) {
                return true;
            } else {
                Toast.makeText(this.cr, "皮肤分辨率不兼容，安装失败", 1).show();
                return false;
            }
        }
        return false;
    }

    private String eR(String str) {
        String str2;
        FileOutputStream fileOutputStream;
        FileInputStream fileInputStream;
        FileInputStream fileInputStream2;
        FileOutputStream fileOutputStream2;
        BufferedInputStream bufferedInputStream;
        FileOutputStream fileOutputStream3;
        String[] split = str.split(au.aGF);
        if (!eS(str) || this.bxx.equals("")) {
            str2 = bxn + File.separator + split[split.length - 1];
        } else {
            str2 = this.bxx;
            this.bxx = "";
        }
        try {
            FileInputStream fileInputStream3 = new FileInputStream(str);
            try {
                bufferedInputStream = new BufferedInputStream(fileInputStream3);
                fileOutputStream3 = new FileOutputStream(new File(str2));
            } catch (Exception e) {
                fileInputStream = fileInputStream3;
                fileOutputStream = null;
                try {
                    fileInputStream.close();
                } catch (Exception e2) {
                }
                try {
                    fileOutputStream.close();
                } catch (Exception e3) {
                }
                return str2;
            } catch (Throwable th) {
                th = th;
                fileInputStream2 = fileInputStream3;
                fileOutputStream2 = null;
                try {
                    fileInputStream2.close();
                } catch (Exception e4) {
                }
                try {
                    fileOutputStream2.close();
                } catch (Exception e5) {
                }
                throw th;
            }
            try {
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream3);
                byte[] bArr = new byte[UCR.drawable.aXd];
                while (true) {
                    int read = bufferedInputStream.read(bArr, 0, bArr.length);
                    if (read == -1) {
                        break;
                    }
                    bufferedOutputStream.write(bArr, 0, read);
                }
                bufferedOutputStream.flush();
                try {
                    fileInputStream3.close();
                } catch (Exception e6) {
                }
                try {
                    fileOutputStream3.close();
                } catch (Exception e7) {
                }
            } catch (Exception e8) {
                fileInputStream = fileInputStream3;
                fileOutputStream = fileOutputStream3;
                fileInputStream.close();
                fileOutputStream.close();
                return str2;
            } catch (Throwable th2) {
                th = th2;
                fileInputStream2 = fileInputStream3;
                fileOutputStream2 = fileOutputStream3;
                fileInputStream2.close();
                fileOutputStream2.close();
                throw th;
            }
        } catch (Exception e9) {
            fileOutputStream = null;
            fileInputStream = null;
            fileInputStream.close();
            fileOutputStream.close();
            return str2;
        } catch (Throwable th3) {
            th = th3;
            fileOutputStream2 = null;
            fileInputStream2 = null;
            fileInputStream2.close();
            fileOutputStream2.close();
            throw th;
        }
        return str2;
    }

    private c o(File file) {
        DataInputStream dataInputStream = null;
        try {
            dataInputStream = new DataInputStream(new FileInputStream(file));
        } catch (FileNotFoundException e) {
        }
        return a(dataInputStream, false);
    }

    public Vector GS() {
        return this.bxo;
    }

    public void GV() {
        if (this.bxy != null) {
            this.bxy.d(this.bxo);
        }
    }

    public void a(b bVar) {
        this.bxy = bVar;
    }

    public boolean eO(String str) {
        this.aaP = str;
        if (eQ(this.aaP)) {
            this.bkx = eR(this.aaP);
            if (eT(this.bkx)) {
                e.nR().w(h.afa, String.valueOf(eP(this.bkx)));
                Toast.makeText(this.cr, "皮肤安装成功！", 1).show();
            } else {
                Toast.makeText(this.cr, "皮肤安装失败！", 1).show();
            }
            return true;
        }
        Toast.makeText(this.cr, "皮肤安装失败！", 1).show();
        return false;
    }

    public int eP(String str) {
        this.bxo.clear();
        GT();
        for (int i = 0; i < this.bxo.size(); i++) {
            if (str.equals(((c) this.bxo.get(i)).Dq())) {
                return i;
            }
        }
        return 0;
    }

    public boolean eS(String str) {
        c o = o(new File(str));
        if (o != null) {
            for (int i = 0; i < this.bxo.size(); i++) {
                if (o.getName().equals(((c) this.bxo.get(i)).getName())) {
                    this.bxx = ((c) this.bxo.get(i)).Dq();
                    return true;
                }
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.browser.ModelBrowser.a(int, java.lang.Object):void
     arg types: [int, int]
     candidates:
      com.uc.browser.ModelBrowser.a(android.view.View, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.content.Context):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Bundle):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.os.Message):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, android.view.View):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.Object):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, java.lang.String):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser, boolean):void
      com.uc.browser.ModelBrowser.a(java.lang.Object, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.WindowUCWeb, java.lang.String):android.view.View
      com.uc.browser.ModelBrowser.a(int, long):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$DownloadDlgData, int):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ModelBrowser$YesOrNoDlgData, boolean):void
      com.uc.browser.ModelBrowser.a(com.uc.browser.ViewZoomControls$ZoomController, int):void
      com.uc.browser.ModelBrowser.a(java.lang.String, java.lang.String):void
      com.uc.browser.ModelBrowser.a(boolean, java.lang.String):void
      com.uc.browser.ModelBrowser.a(int, java.lang.Object):void */
    public boolean eT(String str) {
        if (ModelBrowser.gD() != null) {
            ModelBrowser.gD().a(33, (Object) 0);
        }
        e.Pb().Pe();
        e.Pb().fY(str);
        try {
            e.Pb().update();
            PreferenceManager.getDefaultSharedPreferences(this.cr).edit().putString(bxp, str).commit();
            return true;
        } catch (Exception e) {
            hy(0);
            Toast.makeText(this.cr, "安装的皮肤可能数据不完整", 1).show();
            return false;
        }
    }

    public boolean hx(int i) {
        if (i == 0) {
            return true;
        }
        if (Integer.parseInt(e.nR().nY().bw(h.afa)) == i) {
            hy(0);
        }
        File file = new File(((c) this.bxo.get(i)).Dq());
        if (file.exists()) {
            file.delete();
        }
        this.bxo.remove(i);
        Toast.makeText(this.cr, "皮肤卸载成功", 1).show();
        return true;
    }

    public boolean hy(int i) {
        if (!((c) this.bxo.get(i)).bky) {
            return false;
        }
        Integer.parseInt(e.nR().nY().bw(h.afa));
        e.nR().w(h.afa, String.valueOf(i));
        try {
            return eT(((c) this.bxo.get(i)).Dq());
        } catch (Exception e) {
            hy(0);
            Toast.makeText(this.cr, "安装的皮肤可能数据不完整", 1).show();
            return false;
        }
    }
}
