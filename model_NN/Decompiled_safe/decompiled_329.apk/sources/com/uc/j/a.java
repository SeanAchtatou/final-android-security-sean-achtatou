package com.uc.j;

import android.util.DisplayMetrics;
import android.view.WindowManager;

public class a {
    public static double Pk() {
        WindowManager windowManager = (WindowManager) com.uc.b.a.ae.getSystemService("window");
        DisplayMetrics displayMetrics = new DisplayMetrics();
        if (windowManager != null) {
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        }
        return Math.sqrt(Math.pow(((double) displayMetrics.heightPixels) / ((double) displayMetrics.densityDpi), 2.0d) + Math.pow(((double) displayMetrics.widthPixels) / ((double) displayMetrics.densityDpi), 2.0d));
    }
}
