package com.uc.b;

import android.os.Environment;
import android.os.StatFs;

public class h {
    public static long QK() {
        if (!QL()) {
            return -1;
        }
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return 1 * ((long) statFs.getBlockSize()) * ((long) statFs.getAvailableBlocks());
    }

    public static final boolean QL() {
        return Environment.getExternalStorageState().equals("mounted");
    }
}
