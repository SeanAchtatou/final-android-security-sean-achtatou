package com.uc.b;

import android.os.Build;

public final class b {
    public static final byte pA = 4;
    public static final byte pB = 5;
    public static final byte pC = 99;
    private static byte pD = -1;
    public static String pE = null;
    public static final byte pw = 0;
    public static final byte px = 1;
    public static final byte py = 2;
    public static final byte pz = 3;

    public static byte ec() {
        if (-1 != pD) {
            return pD;
        }
        String str = Build.VERSION.RELEASE;
        if (str == null) {
            pD = pC;
        } else if (str.startsWith("1.5")) {
            pD = 0;
        } else if (str.startsWith("1.6")) {
            pD = 1;
        } else if (str.startsWith("2.0")) {
            pD = 2;
        } else if (str.startsWith("2.1")) {
            pD = 3;
        } else if (str.startsWith("2.2")) {
            pD = 4;
        } else if (str.startsWith("2.3")) {
            pD = 5;
        }
        return pD;
    }
}
