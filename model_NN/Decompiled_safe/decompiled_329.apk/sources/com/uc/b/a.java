package com.uc.b;

import android.content.Context;
import android.view.WindowManager;
import b.a.a.a.f;
import com.uc.a.h;
import com.uc.browser.R;
import com.uc.h.e;

public class a {
    public static Context ae;

    public static void a(Context context) {
        ae = context;
    }

    public static final int aA() {
        return ((WindowManager) ae.getSystemService("window")).getDefaultDisplay().getHeight();
    }

    public static int aB() {
        return e.Pb().kp(R.dimen.f211navigation_channel_item_height);
    }

    public static int aC() {
        return (com.uc.a.e.nR().bw(h.afA).equals(com.uc.a.e.RD) || az() - aA() > 20) ? e.Pb().kp(R.dimen.f193mynavi_bar_height) : e.Pb().kp(R.dimen.f194mynavi_bar_height_double);
    }

    public static f ax() {
        try {
            return f.a(ae.getResources(), R.drawable.f650webview_safe_popup);
        } catch (Exception e) {
            return null;
        }
    }

    public static int ay() {
        return (int) ae.getResources().getDimension(R.dimen.f348juc_resolution);
    }

    public static final int az() {
        return ((WindowManager) ae.getSystemService("window")).getDefaultDisplay().getWidth();
    }
}
