package com.uc.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.util.Iterator;
import java.util.Vector;

public class c extends BroadcastReceiver {
    private static c uN = new c();
    private Vector uO = new Vector();

    private c() {
    }

    public static c fA() {
        return uN;
    }

    public void a(f fVar) {
        if (fVar != null && !this.uO.contains(fVar)) {
            this.uO.add(fVar);
        }
    }

    public void b(f fVar) {
        int indexOf;
        if (fVar != null && (indexOf = this.uO.indexOf(fVar)) >= 0) {
            this.uO.remove(indexOf);
        }
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.MEDIA_EJECT")) {
            Iterator it = this.uO.iterator();
            while (it.hasNext()) {
                ((f) it.next()).Ei();
            }
        }
    }
}
