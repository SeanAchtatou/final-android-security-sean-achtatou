package com.uc.b;

import b.a.a.a.k;
import com.uc.c.az;
import com.uc.c.bc;
import java.lang.reflect.Array;

public class e {
    public static final int EF = 100;
    public static final int EG = 15;
    public static final int EH = 4;
    public static final int EI = 0;
    public static final int EJ = 1;
    public static final int EK = 2;
    public static final int EL = 3;
    private static k[][] EM = ((k[][]) Array.newInstance(k.class, 100, 4));

    public static final k I(int i, int i2) {
        return c(32, i, i2);
    }

    public static final int a(int i, char c) {
        return bj(i).b(c);
    }

    public static final k bj(int i) {
        return c(64, 0, i);
    }

    private static final int bk(int i) {
        return Math.max(1, Math.min(100, i)) - 1;
    }

    private static final int bl(int i) {
        switch (i) {
            case 0:
            default:
                return 0;
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
        }
    }

    public static final int bm(int i) {
        return bj(i).uN();
    }

    public static final int bn(int i) {
        return i == 0 ? k.adr : i == 1 ? k.ads : i == 2 ? k.adt : k.ads;
    }

    public static final k c(int i, int i2, int i3) {
        int bk = bk(i3);
        int bl = bl(i2);
        if (EM[bk][bl] == null) {
            EM[bk][bl] = d(i, i2, bk + 1);
        }
        return EM[bk][bl];
    }

    private static final k d(int i, int i2, int i3) {
        return k.k(i, i2, i3);
    }

    public static final int f(int i, String str) {
        if (bc.by(str)) {
            return 0;
        }
        int length = str.length();
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            i2 += a(i, str.charAt(i3));
        }
        return i2;
    }

    public static final int jP() {
        switch (az.baZ) {
            case 0:
                return k.adr;
            case 1:
                return k.ads;
            case 2:
                return k.adt;
            default:
                return k.ads;
        }
    }
}
