package com.uc.b;

import android.graphics.BitmapFactory;
import java.lang.reflect.Field;

public class d {
    static Field uT;
    static Field uU;

    static {
        Class<BitmapFactory.Options> cls = BitmapFactory.Options.class;
        try {
            uT = BitmapFactory.Options.class.getDeclaredField("inPurgeable");
            uU = BitmapFactory.Options.class.getDeclaredField("inInputShareable");
        } catch (Throwable th) {
        }
    }

    public static BitmapFactory.Options fL() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        if (b.ec() != 0) {
            try {
                uT.setBoolean(options, true);
                uU.setBoolean(options, true);
            } catch (Throwable th) {
            }
        }
        return options;
    }
}
