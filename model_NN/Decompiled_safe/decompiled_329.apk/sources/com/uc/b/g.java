package com.uc.b;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import java.util.Iterator;
import java.util.List;

public class g {
    public static final String bwZ = "uc.ucplayer";
    public static final Uri bxa = Uri.parse("content://uc.ucplayer.provider.InfoProvider/getsupport");
    public static final String bxb = "support";

    public static boolean i(Context context, String str) {
        try {
            context.getPackageManager().getPackageInfo(str, 1);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean i(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            if (((ResolveInfo) it.next()).activityInfo.packageName.equals("uc.ucplayer")) {
                return true;
            }
        }
        return false;
    }

    public static boolean x(Context context) {
        Cursor query = context.getContentResolver().query(bxa, null, null, null, null);
        boolean z = false;
        if (query != null) {
            query.moveToFirst();
            if (query.getString(query.getColumnIndex(bxb)).contains("RTSP")) {
                z = true;
            }
            query.close();
        }
        return z;
    }

    public static boolean y(Context context) {
        return i(context, "uc.ucplayer");
    }
}
