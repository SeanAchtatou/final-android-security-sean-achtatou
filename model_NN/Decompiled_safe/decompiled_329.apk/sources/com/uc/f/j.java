package com.uc.f;

import android.content.Intent;
import com.uc.browser.ActivityChooseFile;
import com.uc.browser.ActivitySetting;
import com.uc.browser.R;

class j extends c {
    final /* synthetic */ a cea;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    j(a aVar, int i, int i2) {
        super(i, i2);
        this.cea = aVar;
    }

    public void a(d dVar) {
        ActivitySetting activitySetting = (ActivitySetting) this.cea.getContext();
        Intent intent = new Intent(activitySetting, ActivityChooseFile.class);
        intent.putExtra(ActivityChooseFile.uv, 1);
        intent.putExtra(ActivityChooseFile.ux, ".upp");
        intent.putExtra(ActivityChooseFile.uA, (int) R.string.f979flash_file_choose_title);
        activitySetting.startActivityForResult(intent, 1);
    }
}
