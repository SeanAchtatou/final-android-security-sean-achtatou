package com.uc.f;

import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import com.uc.browser.R;
import com.uc.browser.UCR;
import com.uc.e.a.aa;
import com.uc.e.a.y;
import com.uc.e.ac;
import com.uc.e.ad;
import com.uc.e.b.f;
import com.uc.e.h;
import com.uc.e.i;
import com.uc.e.s;
import com.uc.e.z;
import com.uc.h.e;
import java.util.Stack;

public class d extends i implements aa, h {
    private static final int Hx = 1;
    private s HA;
    private Stack aAU;
    private k aAV;
    private h aAW;
    private int aAX = -1;
    private b aAY;
    private int uI = -1;

    public d(ad adVar) {
        super(adVar);
        ym();
        this.aAU = new Stack();
    }

    private void ym() {
        e Pb = e.Pb();
        this.HA = new s();
        this.HA.c(this);
        this.HA.fm(Pb.kp(R.dimen.f219bookmark_item_height));
        this.HA.f(new Drawable[]{null, Pb.getDrawable(UCR.drawable.aWH), Pb.getDrawable(UCR.drawable.aWH)});
        this.HA.s(new f(Pb.getColor(54), Pb.getColor(55), 0));
        this.HA.bn(true);
        y yVar = new y(this.HA);
        int kp = Pb.kp(R.dimen.f183controlbar_item_width_2);
        int kp2 = Pb.kp(R.dimen.f177controlbar_height);
        yVar.M(Pb.getDrawable(UCR.drawable.aWt));
        yVar.n(Pb.getDrawable(UCR.drawable.aXp));
        yVar.ki(Pb.kp(R.dimen.f177controlbar_height));
        int kp3 = Pb.kp(R.dimen.f188controlbar_text_size);
        int kp4 = Pb.kp(R.dimen.f187controlbar_item_paddingTop);
        yVar.bz(kp, kp2);
        yVar.a(this);
        yVar.F((byte) 1);
        ac A = yVar.A(Pb.getString(R.string.f1017controlbar_back), 1);
        int color = Pb.getColor(10);
        int color2 = Pb.getColor(11);
        A.setTextColor(color);
        A.ks(color2);
        A.aV(kp3);
        A.setPadding(0, kp4, 0, 4);
        yVar.OP();
        d(yVar);
    }

    private boolean yn() {
        if (!this.aAU.isEmpty()) {
            k kVar = this.aAV;
            this.aAV = (k) this.aAU.pop();
            this.uI = -1;
            this.aAX = -1;
            c(this.aAV);
            a(kVar, this.aAV);
            return true;
        }
        if (this.aAW != null) {
            this.aAW.cL();
        }
        return false;
    }

    public void a(b bVar) {
        this.aAY = bVar;
    }

    public void a(h hVar) {
        this.aAW = hVar;
    }

    /* access modifiers changed from: protected */
    public void a(k kVar, k kVar2) {
        if (this.aAY != null) {
            this.aAY.a(kVar2);
        }
    }

    public void b(z zVar, int i) {
        c cVar = (c) this.HA.gd(i);
        if (cVar != null) {
            if (this.aAU == null || this.aAU.size() != 0) {
                this.aAX = i;
            } else {
                this.uI = i;
            }
            cVar.b(this);
        }
    }

    public void b(k kVar) {
        this.aAU.push(this.aAV);
        c(kVar);
        a((k) this.aAU.peek(), kVar);
        this.HA.ze();
        this.HA.clearFocus();
        if (this.aAY != null) {
            this.aAY.a(kVar);
        }
    }

    public void bA(int i) {
        if (i == 1) {
            yn();
        }
    }

    public void c(k kVar) {
        this.aAV = kVar;
        this.HA.clear();
        for (int i = 0; i < kVar.getCount(); i++) {
            c kI = kVar.kI(i);
            kI.z((byte) 0);
            this.HA.a(kI);
        }
        cJ();
    }

    public int cQ() {
        if (this.uI != 7 || fi()) {
            return this.uI;
        }
        return -1;
    }

    public int cR() {
        if (this.uI == 7 || this.uI == -1 || !fi()) {
            return -1;
        }
        return this.aAX;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (fi() || !b(keyEvent) || !yn()) {
            return super.dispatchKeyEvent(keyEvent);
        }
        return true;
    }

    public h yo() {
        return this.aAW;
    }

    public b yp() {
        return this.aAY;
    }
}
