package com.uc.f;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebViewDatabase;
import android.widget.Toast;
import b.a.a.a.t;
import com.uc.browser.ActivityBrowser;
import com.uc.browser.ActivityChooseFile;
import com.uc.browser.MenuDialog;
import com.uc.browser.ModelBrowser;
import com.uc.browser.R;
import com.uc.c.bc;
import com.uc.c.bx;
import com.uc.e.ad;
import com.uc.e.x;
import com.uc.f.a.b;
import com.uc.f.a.f;
import com.uc.f.a.g;
import com.uc.f.a.h;
import com.uc.f.a.j;
import com.uc.h.e;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class a extends View implements ad, x, com.uc.f.a.a, g, b, f, h {
    private static final String lt = "reset";
    public static final int lu = 1;
    private int index;
    private int lA;
    private d lv;
    private g lw;
    private c lx;
    private i ly;
    public boolean lz;

    public a(Context context) {
        this(context, null);
    }

    public a(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.lz = false;
        this.index = -1;
        this.lA = -1;
        a();
    }

    private void a() {
        setFocusable(true);
        setFocusableInTouchMode(true);
        cK();
    }

    public static boolean b(File file) {
        if (file != null && file.isDirectory()) {
            String[] list = file.list();
            for (String file2 : list) {
                if (!b(new File(file, file2))) {
                    return false;
                }
            }
        }
        return file.delete();
    }

    private k cM() {
        e Pb = e.Pb();
        k kVar = new k("", (String) null);
        k kVar2 = new k((int) R.string.f1213pref_title_display, (String) null);
        kVar2.a(new h(com.uc.a.h.aeU, (int) R.string.f1214pref_item_title_textsize, Integer.valueOf(this.lw.getTextSize()), new Integer[]{0, 1, 2}, Pb.kr(R.array.f28pref_textsize), this));
        kVar2.a(new h(com.uc.a.h.aeW, (int) R.string.f1215pref_item_title_word_subsection, Integer.valueOf(this.lw.Jt()), new Integer[]{0, 50, 100, 150}, Pb.kr(R.array.f32pref_word_subsection), this));
        kVar2.a(new h(com.uc.a.h.aeV, (int) R.string.f1218pref_item_title_picture_quality, Integer.valueOf(this.lw.Js()), new Integer[]{0, 2, 3, 4}, Pb.kr(R.array.f30pref_picture_quality), this));
        kVar2.a(new i(com.uc.a.h.aeY, (int) R.string.f1221pref_item_title_folding_mode, -1, this.lw.Jv(), this));
        kVar2.a(new h(com.uc.a.h.afA, (int) R.string.f1254pref_item_title_mynavi_lines, Integer.valueOf(this.lw.Kd()), new Integer[]{1, 2}, Pb.kr(R.array.f42pref_mynavi_lines), this));
        kVar2.a(new i(com.uc.a.h.afR, (int) R.string.f1225pref_item_title_show_avatar, -1, this.lw.JW(), this));
        k kVar3 = new k((int) R.string.f1226pref_title_network, (String) null);
        kVar3.a(new h(com.uc.a.h.USER_AGENT, (int) R.string.f1227pref_item_title_useragent, this.lw.pw(), Pb.kr(R.array.f36pref_user_agent), Pb.kr(R.array.f37pref_user_agent_values), this));
        kVar3.a(new h(com.uc.a.h.aff, (int) R.string.f1228pref_item_title_use_proxy, Integer.valueOf(this.lw.JB()), new Integer[]{0, 1}, Pb.kr(R.array.f38pref_proxy_server), this));
        kVar3.a(new i(com.uc.a.h.afg, (int) R.string.f1229pref_item_title_wifi_mode, (int) R.string.f1230pref_item_subtitle_wifi_mode, this.lw.JX(), this));
        k kVar4 = new k(R.string.f1231pref_title_download, (String) null);
        kVar4.a(new h(com.uc.a.h.afl, (int) R.string.f1238pref_item_title_download_app, Integer.valueOf(this.lw.JZ()), new Integer[]{0, 1}, Pb.kr(R.array.f26pref_download_app), this));
        this.lx = new c(Pb.getString(R.string.f1233pref_item_title_download_path), this.lw.pa(), com.uc.a.h.afm, this.lw.pa(), this);
        if (!Environment.getExternalStorageState().equals("mounted")) {
            this.lx.disable();
        }
        kVar4.a(this.lx);
        kVar4.a(new i(com.uc.a.h.afM, (int) R.string.f1234pref_item_title_taskrename, -1, this.lw.JO(), this));
        kVar4.a(new h(com.uc.a.h.afn, (int) R.string.f1235pref_item_title_taskcount, Integer.valueOf(this.lw.Ka()), new Integer[]{1, 2, 3, 4, 5, 6}, new String[]{com.uc.a.e.RD, com.uc.a.e.RE, "3", "4", "5", "6"}, this));
        kVar4.a(new i(com.uc.a.h.afo, (int) R.string.f1236pref_item_title_download_in_background, -1, this.lw.Kb(), this));
        kVar4.a(new h(com.uc.a.h.afp, (int) R.string.f1237pref_item_title_download_finish_tips, Integer.valueOf(this.lw.Kc()), new Integer[]{0, 1, 2, 3}, Pb.kr(R.array.f27pref_download_finish_tips), this));
        k kVar5 = new k(R.string.f1240pref_title_browser, (String) null);
        b bVar = new b((int) R.string.f1301pref_title_preread, -1);
        kVar5.a(bVar);
        bVar.a((g) this);
        kVar5.a(new i(com.uc.a.h.afd, (int) R.string.f1243pref_item_title_popupinfo, -1, this.lw.Jy(), this));
        kVar5.a(new i(com.uc.a.h.afe, (int) R.string.f1245pref_item_title_wap, -1, this.lw.Jz(), this));
        cN();
        this.ly = new i(g.bEP, (int) R.string.f1246pref_item_title_set_default, -1, this.lw.JA(), this);
        switch (com.uc.b.b.ec()) {
            case 0:
            case 1:
            case 2:
            case 3:
                kVar5.a(this.ly);
                break;
        }
        kVar5.a(new i(com.uc.a.h.afz, (int) R.string.f1253pref_item_title_enable_sound, -1, this.lw.JK(), this));
        kVar5.a(new i(com.uc.a.h.aeZ, (int) R.string.f1224pref_item_title_titlebar_fullscreen, -1, this.lw.JR(), this));
        kVar5.a(new h(com.uc.a.h.afw, (int) R.string.f1250pref_item_title_page_up_down_location, Integer.valueOf(this.lw.JI()), new Integer[]{0, 1, 2}, Pb.kr(R.array.f40pref_page_up_down_location), this));
        f fVar = new f(com.uc.a.h.afv, (int) R.string.f1248pref_item_title_night_mode_brightness, -1, 0.3f, this);
        kVar5.a(fVar);
        fVar.a((x) this);
        kVar5.a(new i(ActivityBrowser.bqW, (int) R.string.f1255pref_item_title_need_popup_anim, -1, ModelBrowser.gD() != null && ModelBrowser.gD().gR() == 2, this));
        kVar5.a(new i(com.uc.a.h.afQ, (int) R.string.f1256pref_item_title_tips_exit, -1, this.lw.JV(), this));
        k kVar6 = new k(R.string.f1257pref_title_zoom_settings, (String) null);
        h hVar = new h(com.uc.a.h.afq, (int) R.string.f1241pref_item_title_browse_model, Integer.valueOf(this.lw.JS()), new Integer[]{0, 1, 2}, Pb.kr(R.array.f46pref_zoom_mode), this);
        kVar6.a(hVar);
        i iVar = new i(com.uc.a.h.afr, (int) R.string.f1262pref_item_title_fit_screen, -1, this.lw.JE(), this);
        kVar6.a(iVar);
        i iVar2 = new i(com.uc.a.h.afs, (int) R.string.f1260pref_item_title_enablejs, -1, this.lw.JD(), this);
        kVar6.a(iVar2);
        hVar.a(iVar, 2);
        hVar.a(iVar2, 2);
        hVar.tW();
        k kVar7 = new k((int) R.string.f1293pref_title_flash, (String) null);
        kVar7.a(new j(this, R.string.f1294pref_item_title_install_flash, -1));
        k kVar8 = new k(R.string.f1295pref_title_security_settings, (String) null);
        kVar8.a(new h(com.uc.a.h.aft, (int) R.string.f1252pref_item_title_save_psw_juc, Integer.valueOf(this.lw.JJ()), new Integer[]{0, 1, 2}, Pb.kr(R.array.f60pref_save_password), this));
        kVar8.a(new h(com.uc.a.h.afD, (int) R.string.f1296pref_item_title_website_hint, Integer.valueOf(this.lw.JL()), new Integer[]{0, 1, 2}, Pb.kr(R.array.f53pref_website_safe), this));
        kVar8.a(new i(com.uc.a.h.afE, (int) R.string.f1297pref_item_title_download_prehint, -1, this.lw.JM(), this));
        kVar8.a(new i(com.uc.a.h.afF, (int) R.string.f1298pref_item_title_download_posthint, -1, this.lw.JN(), this));
        kVar8.a(new i(com.uc.a.h.afG, (int) R.string.f1299pref_item_title_history_record, -1, this.lw.Ke(), this));
        kVar8.a(new i(com.uc.a.h.afH, (int) R.string.f1300pref_item_title_most_visit_record, -1, this.lw.Kf(), this));
        com.uc.f.a.e eVar = new com.uc.f.a.e((int) R.string.f1284pref_item_clear_record, -1);
        kVar8.a(eVar);
        eVar.a((com.uc.f.a.a) this);
        j jVar = new j(lt, (int) R.string.f1290pref_item_title_reset, -1, (int) R.string.f1291pref_item_msg_reset, this);
        kVar.a(kVar2);
        kVar.a(kVar3);
        kVar.a(kVar4);
        kVar.a(kVar5);
        kVar.a(kVar6);
        kVar.a(kVar7);
        kVar.a(kVar8);
        kVar.a(jVar);
        return kVar;
    }

    private void cN() {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.BROWSABLE");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.setDataAndType(Uri.parse(bx.bAh), null);
        if (!getContext().getPackageManager().resolveActivity(intent, t.bib).activityInfo.packageName.equals(getContext().getPackageName())) {
            this.lw.bR(false);
        } else {
            this.lw.bR(true);
        }
    }

    public static void e(Context context) {
        try {
            File cacheDir = context.getCacheDir();
            if (cacheDir != null && cacheDir.isDirectory()) {
                b(cacheDir);
            }
        } catch (Exception e) {
        }
    }

    private void p(boolean z) {
        PackageManager packageManager = getContext().getPackageManager();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.BROWSABLE");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.setDataAndType(Uri.parse(bx.bAh), null);
        List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 32);
        ArrayList arrayList = new ArrayList();
        for (ResolveInfo resolveInfo : queryIntentActivities) {
            ActivityInfo activityInfo = resolveInfo.activityInfo;
            arrayList.add(new ComponentName(activityInfo.packageName, activityInfo.name));
        }
        if (!z) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                packageManager.clearPackagePreferredActivities(((ComponentName) it.next()).getPackageName());
            }
            return;
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.VIEW");
        intentFilter.addCategory("android.intent.category.BROWSABLE");
        intentFilter.addCategory("android.intent.category.DEFAULT");
        intentFilter.addDataScheme("http");
        String str = "com.uc.browser.ActivityUpdate";
        Iterator it2 = arrayList.iterator();
        while (true) {
            String str2 = str;
            if (it2.hasNext()) {
                ComponentName componentName = (ComponentName) it2.next();
                packageManager.clearPackagePreferredActivities(componentName.getPackageName());
                str = componentName.getPackageName().equals(getContext().getPackageName()) ? componentName.getClassName() : str2;
            } else {
                packageManager.addPreferredActivity(intentFilter, t.big, (ComponentName[]) arrayList.toArray(new ComponentName[0]), new ComponentName(getContext().getPackageName(), str2));
                return;
            }
        }
    }

    public void B(String str) {
        if (str.equals(lt)) {
            this.lw.reset();
            this.lv.c(cM());
            ActivityBrowser.d((Activity) getContext());
            com.uc.a.e.nR().oo();
            p(false);
        }
    }

    public void C(String str) {
        if (str.equals(com.uc.a.h.afm)) {
            Intent intent = new Intent(getContext(), ActivityChooseFile.class);
            intent.putExtra(ActivityChooseFile.ux, ActivityChooseFile.uz);
            ((Activity) getContext()).startActivityForResult(intent, 2);
        }
    }

    public void D(String str) {
        this.lw.D(str);
        this.lx.Yq = str;
        this.lx.cq(str);
        invalidate();
    }

    public void a(k kVar) {
        ((Activity) getContext()).setTitle(kVar.getTitle());
        if (kVar.getGroupId() == R.string.f1231pref_title_download) {
            if (!Environment.getExternalStorageState().equals("mounted")) {
                this.lx.disable();
            } else {
                this.lx.enable();
            }
        } else if (kVar.getGroupId() == R.string.f1240pref_title_browser) {
            cN();
            this.ly.setSelected(this.lw.JA());
        }
    }

    public void a(String str, Object obj) {
        if (str.equals(com.uc.a.h.aeU)) {
            this.lw.aV(((Integer) obj).intValue());
        } else if (str.equals(com.uc.a.h.aeW)) {
            int intValue = ((Integer) obj).intValue();
            this.lw.ip(intValue);
            com.uc.a.e.nR().w(com.uc.a.h.afj, String.valueOf(intValue));
        } else if (str.equals(com.uc.a.h.aeV)) {
            int intValue2 = ((Integer) obj).intValue();
            PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putInt(MenuDialog.bBV, intValue2).commit();
            com.uc.a.e.nR().w(com.uc.a.h.afh, String.valueOf(intValue2));
            this.lw.io(intValue2);
        } else if (str.equals(com.uc.a.h.aeX)) {
            this.lw.bS(((Boolean) obj).booleanValue());
            ActivityBrowser.d((Activity) getContext());
        } else if (str.equals(com.uc.a.h.aeY)) {
            this.lw.bW(((Boolean) obj).booleanValue());
        } else if (str.equals(com.uc.a.h.aeZ)) {
            this.lw.bV(((Boolean) obj).booleanValue());
        } else if (str.equals(com.uc.a.h.USER_AGENT)) {
            this.lw.setUserAgent((String) obj);
        } else if (str.equals(com.uc.a.h.afg)) {
            this.lw.cc(((Boolean) obj).booleanValue());
            this.lz = true;
        } else if (str.equals(com.uc.a.h.afl)) {
            this.lw.iu(((Integer) obj).intValue());
        } else if (str.equals(com.uc.a.h.afM)) {
            this.lw.am(((Boolean) obj).booleanValue());
        } else if (str.equals(com.uc.a.h.afo)) {
            this.lw.cf(((Boolean) obj).booleanValue());
        } else if (str.equals(com.uc.a.h.afn)) {
            int intValue3 = ((Integer) obj).intValue();
            this.lw.iv(intValue3);
            com.uc.a.e.nR().w(com.uc.a.h.afn, String.valueOf(intValue3));
            com.uc.a.e.nR().w(com.uc.a.h.afi, String.valueOf(intValue3));
            com.uc.a.e.nR().oo();
        } else if (str.equals(com.uc.a.h.afp)) {
            this.lw.iw(((Integer) obj).intValue());
        } else if (str.equals(com.uc.a.h.afc)) {
            this.lw.bX(((Boolean) obj).booleanValue());
        } else if (str.equals(com.uc.a.h.afd)) {
            this.lw.bY(((Boolean) obj).booleanValue());
        } else if (str.equals(com.uc.a.h.afe)) {
            this.lw.as(((Boolean) obj).booleanValue());
        } else if (str.equals(g.bEP)) {
            boolean booleanValue = ((Boolean) obj).booleanValue();
            this.lw.bZ(booleanValue);
            p(booleanValue);
        } else if (str.equals(com.uc.a.h.afz)) {
            this.lw.aA(((Boolean) obj).booleanValue());
        } else if (str.equals(com.uc.a.h.afQ)) {
            this.lw.ca(((Boolean) obj).booleanValue());
        } else if (str.equals(com.uc.a.h.afR)) {
            this.lw.cb(((Boolean) obj).booleanValue());
        } else if (str.equals(com.uc.a.h.afw)) {
            this.lw.il(((Integer) obj).intValue());
        } else if (str.equals(com.uc.a.h.afv)) {
            int floatValue = ((int) (((Float) obj).floatValue() * 230.0f)) + 25;
            if (((Float) obj).floatValue() >= 0.0f) {
                com.uc.a.e.nR().w(com.uc.a.h.afv, "" + floatValue);
            }
            if (!ActivityBrowser.Fz()) {
                ActivityBrowser.a((Activity) getContext(), -1);
            } else {
                ActivityBrowser.e((Activity) getContext());
            }
        } else if (str.equals(com.uc.a.h.afA)) {
            this.lw.ix(((Integer) obj).intValue());
        } else if (str.equals(com.uc.a.h.afq)) {
            this.lw.ik(((Integer) obj).intValue());
        } else if (str.equals(com.uc.a.h.afr)) {
            this.lw.af(((Boolean) obj).booleanValue());
        } else if (str.equals(com.uc.a.h.afs)) {
            this.lw.bT(((Boolean) obj).booleanValue());
        } else if (str.equals(com.uc.a.h.afD)) {
            this.lw.it(((Integer) obj).intValue());
        } else if (str.equals(com.uc.a.h.afE)) {
            this.lw.cd(((Boolean) obj).booleanValue());
        } else if (str.equals(com.uc.a.h.afF)) {
            this.lw.ce(((Boolean) obj).booleanValue());
        } else if (str.equals(com.uc.a.h.aft)) {
            int intValue4 = ((Integer) obj).intValue();
            this.lw.is(intValue4);
            this.lw.cP(intValue4);
        } else if (str.equals(com.uc.a.h.afG)) {
            this.lw.cg(((Boolean) obj).booleanValue());
        } else if (str.equals(com.uc.a.h.afH)) {
            this.lw.ch(((Boolean) obj).booleanValue());
        } else if (ActivityBrowser.bqW.equals(str) && ModelBrowser.gD() != null) {
            ModelBrowser.gD().aN(((Boolean) obj).booleanValue() ? 2 : 0);
        }
    }

    public void a(boolean[] zArr) {
        int aJ;
        int aJ2;
        int aJ3;
        int aJ4;
        int aJ5;
        int pj = this.lw != null ? this.lw.pj() : 0;
        if (zArr[0]) {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(29);
                e(getContext());
            }
            if (com.uc.a.e.nR() != null) {
                com.uc.a.e.nR().o(getContext());
                com.uc.a.e.nR().p(getContext());
            }
            aJ = bc.aK(pj, 1);
        } else {
            aJ = bc.aJ(pj, 1);
        }
        if (zArr[1]) {
            com.uc.a.e.nR().nY().oR();
            CookieManager.getInstance().removeAllCookie();
            aJ2 = bc.aK(aJ, 2);
        } else {
            aJ2 = bc.aJ(aJ, 2);
        }
        if (zArr[2]) {
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(47);
            }
            WebViewDatabase instance = WebViewDatabase.getInstance(getContext());
            instance.clearUsernamePassword();
            instance.clearHttpAuthUsernamePassword();
            com.uc.a.e.nR().clearFormData();
            aJ3 = bc.aK(aJ2, 4);
        } else {
            aJ3 = bc.aJ(aJ2, 4);
        }
        if (zArr[3]) {
            com.uc.a.e.nR().nZ().ov();
            com.uc.a.e.nR().nJ();
            aJ4 = bc.aK(aJ3, 8);
        } else {
            aJ4 = bc.aJ(aJ3, 8);
        }
        if (zArr[4]) {
            com.uc.a.e.nR().ob().ox();
            com.uc.a.e.nR().oa().clearHistory();
            com.uc.a.e.nR().nK();
            if (ModelBrowser.gD() != null) {
                ModelBrowser.gD().aS(131);
            }
            aJ5 = bc.aK(aJ4, 16);
        } else {
            aJ5 = bc.aJ(aJ4, 16);
        }
        if (this.lw != null) {
            this.lw.cF(aJ5);
        }
        Toast.makeText(getContext(), (int) R.string.f1286clear_all_success, 0).show();
    }

    public void b(float f) {
        ActivityBrowser.a((Activity) getContext(), ((int) (230.0f * f)) + 25);
    }

    public void b(boolean[] zArr) {
        if (zArr != null) {
            int JQ = this.lw != null ? this.lw.JQ() : 0;
            int aK = zArr[0] ? bc.aK(JQ, 1) : bc.aJ(JQ, 1);
            int aK2 = zArr[1] ? bc.aK(aK, 2) : bc.aJ(aK, 2);
            if (this.lw != null) {
                this.lw.im(aK2);
                com.uc.a.e.nR().w(com.uc.a.h.afk, String.valueOf(aK2));
            }
        }
    }

    public void cJ() {
        invalidate();
    }

    public void cK() {
        this.lw = g.Jn();
        this.lv = new d(this);
        this.lv.c(cM());
        this.lv.a((h) this);
        this.lv.a((b) this);
    }

    public void cL() {
        ((Activity) getContext()).finish();
    }

    public void cO() {
        if (this.lx != null) {
            this.lx.disable();
        }
    }

    public void cP() {
        if (this.lx != null) {
            this.lx.enable();
        }
    }

    public int cQ() {
        return this.lv.cQ();
    }

    public int cR() {
        return this.lv.cR();
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return this.lv.dispatchKeyEvent(keyEvent);
    }

    public void m(int i, int i2) {
        this.index = i;
        this.lA = i2;
    }

    public void onDraw(Canvas canvas) {
        this.lv.dispatchDraw(canvas);
    }

    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (z) {
            this.lv.layout(i, i2, i3, i4);
            if (this.index != -1 && this.index != 7) {
                this.lv.b(null, this.index);
                if (this.lA != -1) {
                    this.lv.b(null, this.lA);
                }
            }
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.lv.dispatchTouchEvent(motionEvent);
    }
}
