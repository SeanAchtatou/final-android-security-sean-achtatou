package com.uc.f;

import android.webkit.WebSettings;
import com.uc.a.e;
import com.uc.a.h;
import com.uc.browser.ModelBrowser;
import com.uc.c.az;
import com.uc.c.bx;
import java.util.HashMap;
import java.util.Observable;

public class g extends Observable {
    /* access modifiers changed from: private */
    public static int bEG = 13;
    /* access modifiers changed from: private */
    public static int bEH = 16;
    /* access modifiers changed from: private */
    public static int bEI = 8;
    /* access modifiers changed from: private */
    public static int bEJ = 8;
    private static g bEM = null;
    public static final String bEP = "uc_pref_set_default";
    public static final int bEQ = 25;
    /* access modifiers changed from: private */
    public int bDU = 3;
    private int bDV = 100;
    private boolean bDW = true;
    private boolean bDX = true;
    private boolean bDY = true;
    private boolean bDZ = true;
    private boolean bEA = true;
    private boolean bEB = true;
    private boolean bEC = true;
    private boolean bED = true;
    /* access modifiers changed from: private */
    public String bEE = null;
    private boolean bEF = false;
    private int bEK = 1;
    private int bEL = 1;
    private h bEN = e.nR().nY();
    private HashMap bEO = new HashMap();
    private boolean bEa = true;
    private boolean bEb = false;
    private boolean bEc = true;
    private String bEd = "";
    private int bEe = 0;
    private int bEf = 0;
    private boolean bEg = false;
    private int bEh = 1;
    private boolean bEi = true;
    private int bEj = 1;
    private boolean bEk = true;
    private int bEl = 0;
    private boolean bEm = true;
    private int bEn = 0;
    public final int bEo = 0;
    public final int bEp = 1;
    private String bEq = "/sdcard/UCDownloads/";
    private boolean bEr = false;
    private int bEs = 3;
    private boolean bEt = false;
    private int bEu = 0;
    private int bEv = 0;
    /* access modifiers changed from: private */
    public boolean bEw = false;
    /* access modifiers changed from: private */
    public boolean bEx = true;
    /* access modifiers changed from: private */
    public boolean bEy = true;
    private int bEz = 0;
    /* access modifiers changed from: private */
    public int textSize = 2;

    private g() {
    }

    public static g JF() {
        return bEM;
    }

    public static g Jn() {
        if (bEM == null) {
            synchronized (g.class) {
                if (bEM == null) {
                    bEM = new g();
                }
            }
        }
        return bEM;
    }

    private void Jp() {
        this.bEN.w(h.afh, String.valueOf(this.bDU));
        this.bEN.w(h.afj, String.valueOf(this.bDV));
        az.bcN = this.bEs;
    }

    public void D(String str) {
        this.bEq = str;
    }

    public boolean JA() {
        return this.bEg;
    }

    public int JB() {
        return this.bEe;
    }

    public boolean JC() {
        return this.bEw;
    }

    public boolean JD() {
        return this.bEx;
    }

    public boolean JE() {
        return this.bEy;
    }

    public h JG() {
        return this.bEN;
    }

    public HashMap JH() {
        return this.bEO;
    }

    public int JI() {
        return this.bEf;
    }

    public int JJ() {
        return this.bEh;
    }

    public boolean JK() {
        return this.bEi;
    }

    public int JL() {
        return this.bEz;
    }

    public boolean JM() {
        return this.bEA;
    }

    public boolean JN() {
        return this.bEB;
    }

    public boolean JO() {
        return this.bEr;
    }

    public boolean JP() {
        return this.bEF;
    }

    public int JQ() {
        return this.bEL;
    }

    public boolean JR() {
        return this.bDY;
    }

    public int JS() {
        return this.bEv;
    }

    public String JT() {
        return this.bEE;
    }

    public HashMap JU() {
        return this.bEO;
    }

    public boolean JV() {
        return this.bEk;
    }

    public boolean JW() {
        return this.bEm;
    }

    public boolean JX() {
        return this.bEc;
    }

    public int JY() {
        return this.bEl;
    }

    public int JZ() {
        return this.bEn;
    }

    public void Jo() {
        this.bDU = Integer.valueOf(this.bEN.bw(h.aeV)).intValue();
        this.textSize = Integer.valueOf(this.bEN.bw(h.aeU)).intValue();
        this.bEv = Integer.valueOf(this.bEN.bw(h.afq)).intValue();
        this.bDV = Integer.valueOf(this.bEN.bw(h.aeW)).intValue();
        this.bDW = this.bEN.bw(h.aeX).contains(e.RD);
        this.bDX = this.bEN.bw(h.aeY).contains(e.RD);
        this.bDY = this.bEN.bw(h.aeZ).contains(e.RD);
        this.bDZ = !this.bEN.bw(h.afc).contains("0");
        this.bEa = this.bEN.bw(h.afd).contains(e.RD);
        this.bEb = this.bEN.bw(h.afe).contains(e.RD);
        this.bEc = this.bEN.bw(h.afg).contains(e.RD);
        this.bEf = Integer.valueOf(this.bEN.bw(h.afw)).intValue();
        String bw = this.bEN.bw(h.afx);
        this.bEw = !bw.contains("0");
        this.bEh = Integer.valueOf(bw).intValue();
        this.bEi = this.bEN.bw(h.afz).contains(e.RD);
        this.bEj = Integer.valueOf(this.bEN.bw(h.afA)).intValue();
        this.bEk = this.bEN.bw(h.afQ).contains(e.RD);
        this.bEm = this.bEN.bw(h.afR).contains(e.RD);
        this.bEN.bw(h.afb);
        this.bEl = Integer.valueOf(this.bEl).intValue();
        this.bEd = this.bEN.bw(h.USER_AGENT);
        this.bEe = Integer.valueOf(this.bEN.bw(h.aff)).intValue();
        this.bEy = this.bEN.bw(h.afr).contains(e.RD);
        this.bEx = this.bEN.bw(h.afs).contains(e.RD);
        this.bEn = Integer.valueOf(this.bEN.bw(h.afl)).intValue();
        this.bEq = this.bEN.bw(h.afm);
        this.bEt = this.bEN.bw(h.afo).contains(e.RD);
        this.bEs = Integer.valueOf(this.bEN.bw(h.afn)).intValue();
        this.bEu = Integer.valueOf(this.bEN.bw(h.afp)).intValue();
        this.bEz = Integer.valueOf(this.bEN.bw(h.afD)).intValue();
        this.bEA = this.bEN.bw(h.afE).contains(e.RD);
        this.bEB = this.bEN.bw(h.afF).contains(e.RD);
        this.bEC = this.bEN.bw(h.afG).contains(e.RD);
        this.bED = this.bEN.bw(h.afH).contains(e.RD);
        this.bEF = this.bEN.bw(h.afL).contains(e.RD);
        this.bEr = this.bEN.bw(h.afM).contains(e.RD);
        this.bEK = Integer.valueOf(this.bEN.bw(h.afO)).intValue();
        this.bEL = Integer.valueOf(this.bEN.bw(h.afP)).intValue();
    }

    public void Jq() {
        this.bEN.w(h.aeV, String.valueOf(this.bDU));
        this.bEN.w(h.aeU, String.valueOf(this.textSize));
        this.bEN.w(h.aeW, String.valueOf(this.bDV));
        if (this.bEv == 2) {
            az.bbo = true;
        } else {
            az.bbo = false;
            az.bcE = (byte) this.bEv;
            bx.bAE = 0;
        }
        if (this.bDW) {
            this.bEN.w(h.aeX, e.RD);
        } else {
            this.bEN.w(h.aeX, "0");
        }
        if (this.bDX) {
            this.bEN.w(h.aeY, e.RD);
        } else {
            this.bEN.w(h.aeY, "0");
        }
        if (this.bDY) {
            this.bEN.w(h.aeZ, e.RD);
        } else {
            this.bEN.w(h.aeZ, "0");
        }
        if (this.bDZ) {
            this.bEN.w(h.afc, e.RD);
        } else {
            this.bEN.w(h.afc, "0");
        }
        if (this.bEa) {
            this.bEN.w(h.afd, e.RD);
        } else {
            this.bEN.w(h.afd, "0");
        }
        if (this.bEb) {
            this.bEN.w(h.afe, e.RD);
        } else {
            this.bEN.w(h.afe, "0");
        }
        if (this.bEC) {
            this.bEN.w(h.afG, e.RD);
        } else {
            this.bEN.w(h.afG, "0");
        }
        if (this.bED) {
            this.bEN.w(h.afH, e.RD);
        } else {
            this.bEN.w(h.afH, "0");
        }
        az.bbJ = this.bEj;
        this.bEN.w(h.afw, String.valueOf(this.bEf));
        az.bbV = this.bEh;
        az.bcx = this.bEi;
        az.bcJ = this.bEk;
        az.bcR = this.bEm ? 1 : 0;
        az.bcL = this.bEc;
        this.bEN.w(h.USER_AGENT, this.bEd);
        this.bEN.w(h.aff, String.valueOf(this.bEe));
        if (this.bEy) {
            this.bEN.w(h.afr, e.RD);
        } else {
            this.bEN.w(h.afr, "0");
        }
        if (this.bEx) {
            this.bEN.w(h.afs, e.RD);
        } else {
            this.bEN.w(h.afs, "0");
        }
        if (this.bEw) {
            this.bEN.w(h.aft, e.RD);
        } else {
            this.bEN.w(h.aft, "0");
        }
        az.bcS = this.bEz;
        az.bcG = this.bEA;
        az.bcH = this.bEB;
        if (this.bEF) {
            this.bEN.w(h.afL, e.RD);
        } else {
            this.bEN.w(h.afL, "0");
        }
        if (this.bEr) {
            this.bEN.w(h.afM, e.RD);
        } else {
            this.bEN.w(h.afM, "0");
        }
        this.bEN.w(h.afl, String.valueOf(this.bEn));
        az.bbL = this.bEu;
        this.bEN.w(h.afm, this.bEq);
        az.bbF = this.bEt;
        az.bbE = this.bEs;
        az.bbR = this.bEK;
        az.bbS = this.bEL;
        this.bEN.pp();
    }

    public int Jr() {
        return this.bEv;
    }

    public int Js() {
        return this.bDU;
    }

    public int Jt() {
        return this.bDV;
    }

    public boolean Ju() {
        return this.bDW;
    }

    public boolean Jv() {
        return this.bDX;
    }

    public boolean Jw() {
        return this.bDY;
    }

    public boolean Jx() {
        return this.bDZ;
    }

    public boolean Jy() {
        return this.bEa;
    }

    public boolean Jz() {
        return this.bEb;
    }

    public int Ka() {
        return this.bEs;
    }

    public boolean Kb() {
        return this.bEt;
    }

    public int Kc() {
        return this.bEu;
    }

    public int Kd() {
        return this.bEj;
    }

    public boolean Ke() {
        return this.bEC;
    }

    public boolean Kf() {
        return this.bED;
    }

    public e a(WebSettings webSettings) {
        e eVar = (e) this.bEO.get(webSettings);
        if (eVar != null) {
            super.deleteObserver(eVar);
        }
        e eVar2 = new e(this, webSettings);
        this.bEO.put(webSettings, eVar2);
        super.addObserver(eVar2);
        eVar2.update(this, eVar2);
        return eVar2;
    }

    public void a(HashMap hashMap) {
        this.bEO = hashMap;
    }

    public void aA(boolean z) {
        this.bEi = z;
    }

    public void aV(int i) {
        this.textSize = i;
    }

    public void af(boolean z) {
        this.bEy = z;
    }

    public void al(boolean z) {
        this.bEF = z;
        if (this.bEF) {
            this.bEN.w(h.afL, e.RD);
        } else {
            this.bEN.w(h.afL, "0");
        }
        this.bEN.pp();
    }

    public void am(boolean z) {
        this.bEr = z;
    }

    public void as(boolean z) {
        this.bEb = z;
    }

    public void b(WebSettings webSettings) {
        e eVar = (e) this.bEO.get(webSettings);
        if (eVar != null) {
            this.bEO.remove(webSettings);
            super.deleteObserver(eVar);
        }
    }

    public void bR(boolean z) {
        this.bEg = z;
    }

    public void bS(boolean z) {
        this.bDW = z;
        update();
    }

    public boolean bT(boolean z) {
        this.bEx = z;
        return z;
    }

    public boolean bU(boolean z) {
        this.bEy = z;
        return z;
    }

    public void bV(boolean z) {
        this.bDY = z;
    }

    public void bW(boolean z) {
        this.bDX = z;
    }

    public void bX(boolean z) {
        this.bDZ = z;
    }

    public void bY(boolean z) {
        this.bEa = z;
    }

    public void bZ(boolean z) {
        this.bEg = z;
    }

    public void cF(int i) {
        this.bEK = i;
    }

    public void cP(int i) {
        this.bEh = i;
    }

    public void ca(boolean z) {
        this.bEk = z;
    }

    public void cb(boolean z) {
        this.bEm = z;
    }

    public void cc(boolean z) {
        this.bEc = z;
    }

    public void cd(boolean z) {
        this.bEA = z;
    }

    public void ce(boolean z) {
        this.bEB = z;
    }

    public void cf(boolean z) {
        this.bEt = z;
    }

    public void cg(boolean z) {
        this.bEC = z;
    }

    public void ch(boolean z) {
        this.bED = z;
    }

    public void fh(String str) {
        this.bEE = str;
    }

    public int getTextSize() {
        return this.textSize;
    }

    public void ik(int i) {
        this.bEv = i;
    }

    public void il(int i) {
        this.bEf = i;
        update();
    }

    public void im(int i) {
        this.bEL = i;
    }

    public void in(int i) {
        this.bEv = i;
    }

    public void io(int i) {
        this.bDU = i;
    }

    public void ip(int i) {
        this.bDV = i;
    }

    public void iq(int i) {
        this.bEe = i;
    }

    public void ir(int i) {
        this.bEl = i;
    }

    public void is(int i) {
        this.bEw = i != 0;
    }

    public void it(int i) {
        this.bEz = i;
    }

    public void iu(int i) {
        this.bEn = i;
    }

    public void iv(int i) {
        this.bEs = i;
    }

    public void iw(int i) {
        this.bEu = i;
    }

    public void ix(int i) {
        this.bEj = i;
        update();
    }

    public void o(int i, boolean z) {
        this.bEf = i;
        if (z) {
            update();
        }
    }

    public String pa() {
        return this.bEq;
    }

    public int pj() {
        return this.bEK;
    }

    public String pw() {
        return this.bEd;
    }

    public void reset() {
        this.bEN.oQ();
        ModelBrowser.gD().gJ();
        Jo();
        ModelBrowser.gD().aS(ModelBrowser.zW);
        ModelBrowser.gD().aN(1);
        ModelBrowser.gD().iH();
    }

    public void setUserAgent(String str) {
        this.bEd = str;
    }

    public void update() {
        setChanged();
        notifyObservers();
        Jq();
    }
}
