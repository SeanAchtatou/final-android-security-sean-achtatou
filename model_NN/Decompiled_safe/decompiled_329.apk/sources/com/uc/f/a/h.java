package com.uc.f.a;

import android.graphics.drawable.Drawable;
import com.uc.browser.R;
import com.uc.browser.UCR;
import com.uc.e.p;
import com.uc.e.s;
import com.uc.e.z;
import com.uc.f.f;
import com.uc.h.e;

public class h extends d implements com.uc.e.h {
    private Object[] bgT;
    private int bgU;
    private String[] choices;
    private int index;
    private p tJ;

    public h(String str, int i, Object obj, Object[] objArr, String[] strArr, f fVar) {
        this(str, e.Pb().getString(i), obj, objArr, strArr, fVar);
    }

    public h(String str, String str2, Object obj, Object[] objArr, String[] strArr, f fVar) {
        super(str2, null, str, obj, fVar);
        this.bgU = e.Pb().kp(R.dimen.f219bookmark_item_height);
        this.bgT = objArr;
        this.choices = strArr;
        this.index = cQ();
        cq(strArr[this.index]);
    }

    private int cQ() {
        for (int i = 0; i < this.bgT.length; i++) {
            if (this.bgT[i].equals(this.Yq)) {
                return i;
            }
        }
        return 0;
    }

    public void b(z zVar, int i) {
        gy(i);
        this.tJ.dismiss();
    }

    public p ev() {
        e Pb = e.Pb();
        s sVar = new s();
        sVar.a(Pb.getDrawable(UCR.drawable.aUo), 2);
        sVar.B(Pb.getDrawable(UCR.drawable.aUO));
        sVar.f(new Drawable[]{null, Pb.getDrawable(UCR.drawable.aXu), Pb.getDrawable(UCR.drawable.aXu)});
        sVar.fm(this.bgU);
        sVar.setSize((getWidth() - Pb.kp(R.dimen.f310dialog_frame_padding_left)) - Pb.kp(R.dimen.f311dialog_frame_padding_right), Math.min(Pb.kp(R.dimen.f302dialog_content_max_height), this.bgU * this.choices.length));
        sVar.c(this);
        int i = 0;
        while (i < this.choices.length) {
            sVar.a(new i(this, this.choices[i], i == this.index));
            i++;
        }
        this.tJ = new p(getTitle(), sVar, e.Pb().getString(R.string.cancel));
        return this.tJ;
    }

    public void gy(int i) {
        this.index = i;
        cq(this.choices[i]);
        x(this.bgT[i]);
    }
}
