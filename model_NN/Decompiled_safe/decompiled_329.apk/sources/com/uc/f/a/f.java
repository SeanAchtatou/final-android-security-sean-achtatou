package com.uc.f.a;

import com.uc.a.h;
import com.uc.browser.R;
import com.uc.browser.UCR;
import com.uc.e.d;
import com.uc.e.p;
import com.uc.e.x;
import com.uc.h.e;

public class f extends d {
    private d auT;
    private x pT;

    public f(String str, int i, int i2, float f, com.uc.f.f fVar) {
        this(str, e.Pb().getString(i), e.Pb().getString(i2), f, fVar);
    }

    public f(String str, String str2, String str3, float f, com.uc.f.f fVar) {
        super(str2, str3, str, Float.valueOf(f), fVar);
    }

    public void a(x xVar) {
        this.pT = xVar;
        if (this.auT != null) {
            this.auT.a(xVar);
        }
    }

    public void au(int i) {
        if (i == 1) {
            x(Float.valueOf(-1.0f));
        } else if (i == 0) {
            x(Float.valueOf(this.auT.eh()));
        }
    }

    public p ev() {
        this.auT = new d(((Float) this.Yq).floatValue(), getWidth() - 30, 50);
        this.auT.c((((float) Integer.valueOf(com.uc.a.e.nR().bw(h.afv)).intValue()) - 25.0f) / 230.0f);
        e Pb = e.Pb();
        this.auT.e(Pb.getDrawable(UCR.drawable.kU));
        p pVar = new p(getTitle(), this.auT, Pb.getString(R.string.ok), Pb.getString(R.string.cancel));
        if (this.pT != null) {
            this.auT.a(this.pT);
        }
        return pVar;
    }
}
