package com.uc.f.a;

import android.graphics.drawable.Drawable;
import com.uc.browser.R;
import com.uc.browser.UCR;
import com.uc.c.bc;
import com.uc.e.a.g;
import com.uc.e.h;
import com.uc.e.p;
import com.uc.e.s;
import com.uc.e.z;
import com.uc.f.f;
import com.uc.h.e;

public class b extends d implements h {
    private g qQ;
    private g[] qR;

    public b(int i, int i2) {
        this(e.Pb().getString(i), e.Pb().getString(i2));
    }

    public b(String str, String str2) {
        super(str, str2);
        this.qR = new g[2];
    }

    public b(String str, String str2, String str3, Object obj, f fVar) {
        super(str, str2, str3, obj, fVar);
        this.qR = new g[2];
    }

    private boolean[] ew() {
        boolean[] zArr = new boolean[this.qR.length];
        for (int i = 0; i < zArr.length; i++) {
            zArr[i] = this.qR[i].wn();
        }
        return zArr;
    }

    public void a(g gVar) {
        this.qQ = gVar;
    }

    public void au(int i) {
        if (i == 0 && this.qQ != null) {
            this.qQ.b(ew());
        }
    }

    public void b(z zVar, int i) {
        this.qR[i].wm();
    }

    public p ev() {
        e Pb = e.Pb();
        int kp = Pb.kp(R.dimen.f221bookmark_webname_font);
        s sVar = new s();
        sVar.bc(false);
        sVar.a(Pb.getDrawable(UCR.drawable.aUo), 2);
        sVar.f(new Drawable[]{null, Pb.getDrawable(UCR.drawable.aXu), Pb.getDrawable(UCR.drawable.aXu)});
        int color = e.Pb().getColor(78);
        int JQ = com.uc.f.g.Jn() != null ? com.uc.f.g.Jn().JQ() : 1;
        this.qR[0] = new g(Pb.getString(R.string.f1302pref_item_title_wap_preread), bc.aL(JQ, 1));
        this.qR[0].setTextColor(color);
        this.qR[0].aV(kp);
        sVar.a(this.qR[0]);
        this.qR[1] = new g(Pb.getString(R.string.f1303pref_item_title_www_preread), bc.aL(JQ, 2));
        this.qR[1].setTextColor(color);
        this.qR[1].aV(kp);
        sVar.a(this.qR[1]);
        int kp2 = Pb.kp(R.dimen.f219bookmark_item_height);
        sVar.setSize((getWidth() - Pb.kp(R.dimen.f310dialog_frame_padding_left)) - Pb.kp(R.dimen.f311dialog_frame_padding_right), Math.min(Pb.kp(R.dimen.f302dialog_content_max_height), this.qR.length * kp2));
        sVar.fm(kp2);
        sVar.c(this);
        return new p(Pb.getString(R.string.f1301pref_title_preread), sVar, Pb.getString(R.string.ok), Pb.getString(R.string.cancel));
    }
}
