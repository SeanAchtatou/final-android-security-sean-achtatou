package com.uc.f.a;

import android.graphics.Canvas;
import com.uc.browser.R;
import com.uc.e.z;
import com.uc.h.e;

class c extends z {
    private String Bo;
    final /* synthetic */ j It;

    public c(j jVar, String str, int i, int i2) {
        this.It = jVar;
        this.Bo = str;
        this.pL.setColor(e.Pb().getColor(78));
        this.pL.setTextSize((float) e.Pb().kp(R.dimen.f392set_tilte_font));
        setSize((int) this.pL.measureText(str), (int) ((-this.pL.ascent()) * 3.0f));
    }

    public void draw(Canvas canvas) {
        canvas.drawText(this.Bo, (float) e.Pb().kp(R.dimen.f393set_summary_font), ((((float) getHeight()) - this.pL.descent()) - this.pL.ascent()) / 2.0f, this.pL);
    }
}
