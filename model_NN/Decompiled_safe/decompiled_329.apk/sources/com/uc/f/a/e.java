package com.uc.f.a;

import android.graphics.drawable.Drawable;
import com.uc.browser.R;
import com.uc.browser.UCR;
import com.uc.c.bc;
import com.uc.e.a.g;
import com.uc.e.h;
import com.uc.e.p;
import com.uc.e.s;
import com.uc.e.z;
import com.uc.f.f;

public class e extends d implements h {
    private a aim;
    private g[] qR;
    private p tJ;

    public e(int i, int i2) {
        this(com.uc.h.e.Pb().getString(i), com.uc.h.e.Pb().getString(i2));
    }

    public e(String str, String str2) {
        super(str, str2);
        this.qR = new g[5];
    }

    public e(String str, String str2, String str3, Object obj, f fVar) {
        super(str, str2, str3, obj, fVar);
        this.qR = new g[5];
    }

    private boolean[] ew() {
        boolean[] zArr = new boolean[this.qR.length];
        for (int i = 0; i < zArr.length; i++) {
            zArr[i] = this.qR[i].wn();
        }
        return zArr;
    }

    public void a(a aVar) {
        this.aim = aVar;
    }

    public void au(int i) {
        if (i == 0 && this.aim != null) {
            this.aim.a(ew());
        }
    }

    public void b(z zVar, int i) {
        this.qR[i].wm();
        vY();
    }

    public p ev() {
        com.uc.h.e Pb = com.uc.h.e.Pb();
        int kp = Pb.kp(R.dimen.f221bookmark_webname_font);
        s sVar = new s();
        sVar.bc(false);
        sVar.a(Pb.getDrawable(UCR.drawable.aUo), 2);
        sVar.f(new Drawable[]{null, Pb.getDrawable(UCR.drawable.aXu), Pb.getDrawable(UCR.drawable.aXu)});
        int color = com.uc.h.e.Pb().getColor(78);
        int pj = com.uc.f.g.Jn() != null ? com.uc.f.g.Jn().pj() : 1;
        this.qR[0] = new g(Pb.getString(R.string.f1274pref_item_title_clear_cache), bc.aL(pj, 1));
        this.qR[0].setTextColor(color);
        this.qR[0].aV(kp);
        this.qR[0].bL(false);
        sVar.a(this.qR[0]);
        this.qR[1] = new g(Pb.getString(R.string.f1277pref_item_title_clear_cookie), bc.aL(pj, 2));
        this.qR[1].setTextColor(color);
        this.qR[1].aV(kp);
        this.qR[1].bL(false);
        sVar.a(this.qR[1]);
        this.qR[2] = new g(Pb.getString(R.string.f1280pref_item_title_clear_form), bc.aL(pj, 4));
        this.qR[2].setTextColor(color);
        this.qR[2].aV(kp);
        this.qR[2].bL(false);
        sVar.a(this.qR[2]);
        this.qR[3] = new g(Pb.getString(R.string.f1287pref_item_clear_input_history), bc.aL(pj, 8));
        this.qR[3].setTextColor(color);
        this.qR[3].aV(kp);
        this.qR[3].bL(false);
        sVar.a(this.qR[3]);
        this.qR[4] = new g(Pb.getString(R.string.f1288pref_item_clear_visit_history), bc.aL(pj, 16));
        this.qR[4].setTextColor(color);
        this.qR[4].aV(kp);
        this.qR[4].bL(false);
        sVar.a(this.qR[4]);
        int kp2 = Pb.kp(R.dimen.f219bookmark_item_height);
        sVar.setSize((getWidth() - Pb.kp(R.dimen.f310dialog_frame_padding_left)) - Pb.kp(R.dimen.f311dialog_frame_padding_right), Math.min(Pb.kp(R.dimen.f302dialog_content_max_height), this.qR.length * kp2));
        sVar.fm(kp2);
        sVar.c(this);
        p pVar = new p(Pb.getString(R.string.f1284pref_item_clear_record), sVar, Pb.getString(R.string.ok), Pb.getString(R.string.cancel));
        this.tJ = pVar;
        vY();
        return pVar;
    }

    public void vY() {
        boolean z;
        if (this.tJ != null && this.qR != null) {
            int i = 0;
            boolean z2 = false;
            while (true) {
                if (i >= this.qR.length) {
                    z = z2;
                    break;
                }
                z2 |= this.qR[i].wn();
                if (z2) {
                    z = z2;
                    break;
                }
                i++;
            }
            this.tJ.eR(0).H(z);
        }
    }
}
