package com.uc.f.a;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import com.uc.browser.R;
import com.uc.browser.UCR;
import com.uc.e.z;
import com.uc.h.e;

class i extends z {
    private static final int bno = 15;
    private String Bo;
    private Drawable Yt = e.Pb().getDrawable(UCR.drawable.aWi);
    private boolean aGa;
    private Drawable bnp = e.Pb().getDrawable(UCR.drawable.aWh);
    final /* synthetic */ h bnq;

    public i(h hVar, String str, boolean z) {
        this.bnq = hVar;
        this.Bo = str;
        this.aGa = z;
        this.pL.setTextSize((float) e.Pb().kp(R.dimen.f221bookmark_webname_font));
    }

    public void draw(Canvas canvas) {
        this.pL.setColor(Iz() == 0 ? e.Pb().getColor(57) : e.Pb().getColor(58));
        canvas.drawText(this.Bo, 15.0f, ((((float) getHeight()) - this.pL.descent()) - this.pL.ascent()) / 2.0f, this.pL);
        this.bnp.setBounds((getWidth() - this.bnp.getIntrinsicWidth()) - 15, (getHeight() - this.bnp.getIntrinsicHeight()) / 2, getWidth() - 15, (getHeight() + this.bnp.getIntrinsicHeight()) / 2);
        this.bnp.draw(canvas);
        if (this.aGa) {
            this.Yt.setBounds((getWidth() - this.Yt.getIntrinsicWidth()) - 15, (getHeight() - this.Yt.getIntrinsicHeight()) / 2, getWidth() - 15, (getHeight() + this.Yt.getIntrinsicHeight()) / 2);
            this.Yt.draw(canvas);
        }
    }

    public void setSelected(boolean z) {
        this.aGa = z;
    }
}
