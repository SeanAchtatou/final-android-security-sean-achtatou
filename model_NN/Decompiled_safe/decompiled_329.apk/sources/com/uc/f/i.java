package com.uc.f;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import com.uc.browser.UCR;
import com.uc.h.e;

public class i extends c {
    public static final int cas = 0;
    public static final int cat = 1;
    private boolean aGa;
    private int adF;
    private Drawable cau;

    public i(String str, int i, int i2, boolean z, f fVar) {
        this(str, e.Pb().getString(i), e.Pb().getString(i2), z, fVar);
    }

    public i(String str, String str2, String str3, boolean z, int i, f fVar) {
        super(str2, str3, str, Boolean.valueOf(z), fVar);
        this.aGa = z;
        if (i == 0) {
            this.Yt = e.Pb().getDrawable(UCR.drawable.checkbox);
            this.cau = e.Pb().getDrawable(UCR.drawable.aTZ);
        } else {
            this.Yt = e.Pb().getDrawable(UCR.drawable.aWh);
            this.cau = e.Pb().getDrawable(UCR.drawable.aWi);
        }
        this.adF = i;
    }

    public i(String str, String str2, String str3, boolean z, f fVar) {
        this(str, str2, str3, z, 0, fVar);
    }

    public void a(d dVar) {
        wm();
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.aGa && isEnabled()) {
            this.cau.setBounds((getWidth() - this.cau.getIntrinsicWidth()) - this.Yo, (getHeight() - this.cau.getIntrinsicHeight()) / 2, getWidth() - this.Yo, (getHeight() + this.cau.getIntrinsicHeight()) / 2);
            this.cau.draw(canvas);
        }
    }

    public void setSelected(boolean z) {
        this.aGa = z;
    }

    public void wm() {
        if (this.adF == 0 || !this.aGa) {
            this.aGa = !this.aGa;
            x(Boolean.valueOf(this.aGa));
            Ix();
        }
    }
}
