package com.uc.f;

import com.uc.browser.UCR;
import com.uc.h.e;
import java.util.ArrayList;
import java.util.List;

public class k extends c {
    private int aV;
    private List bdy;

    public k(int i, String str) {
        this(e.Pb().getString(i), str, i);
    }

    public k(String str, String str2) {
        this(str, str2, -1);
    }

    public k(String str, String str2, int i) {
        super(str, str2);
        this.bdy = new ArrayList(6);
        this.aV = i;
        this.Yt = e.Pb().getDrawable(UCR.drawable.aTD);
    }

    public List QU() {
        return this.bdy;
    }

    public void a(c cVar) {
        this.bdy.add(cVar);
    }

    public void a(d dVar) {
        dVar.b(this);
    }

    public int getCount() {
        return this.bdy.size();
    }

    public int getGroupId() {
        return this.aV;
    }

    public c kI(int i) {
        return (c) this.bdy.get(i);
    }
}
