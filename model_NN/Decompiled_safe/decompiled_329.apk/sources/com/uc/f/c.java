package com.uc.f;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import com.uc.browser.R;
import com.uc.e.z;
import com.uc.h.e;
import java.util.HashMap;
import java.util.Map;

public class c extends z {
    private int Yf;
    private int Yg;
    private int Yh;
    private int Yi;
    private int Yj;
    private int Yk;
    private int Yl;
    private int Ym;
    public int Yn;
    public int Yo;
    private String Yp;
    protected Object Yq;
    private f Yr;
    private Drawable Ys;
    protected Drawable Yt;
    private Map Yu;
    private boolean aP;
    private String aU;
    protected String ds;

    public c(int i, int i2) {
        this(e.Pb().getString(i), e.Pb().getString(i2));
    }

    public c(String str, String str2) {
        this.Yf = e.Pb().kp(R.dimen.f221bookmark_webname_font);
        this.Yg = e.Pb().kp(R.dimen.f222bookmark_weburl_font);
        this.Yh = e.Pb().getColor(50);
        this.Yi = e.Pb().getColor(51);
        this.Yj = e.Pb().getColor(52);
        this.Yk = e.Pb().getColor(53);
        this.Yl = e.Pb().getColor(22);
        this.Ym = e.Pb().getColor(24);
        this.Yn = e.Pb().kp(R.dimen.f394set_text_margin);
        this.Yo = e.Pb().kp(R.dimen.f395set_arrow_margin);
        this.aP = true;
        this.aU = str;
        this.Yp = str2;
        bL(false);
    }

    public c(String str, String str2, String str3, Object obj, f fVar) {
        this(str, str2);
        this.ds = str3;
        this.Yq = obj;
        this.Yr = fVar;
    }

    private static float a(Paint paint) {
        return paint.descent() - paint.ascent();
    }

    public void a(c cVar, Object obj) {
        if (this.Yu == null) {
            this.Yu = new HashMap();
        }
        this.Yu.put(cVar, obj);
    }

    public void a(d dVar) {
        z((byte) 0);
        if (this.Yr != null) {
            this.Yr.C(this.ds);
        }
    }

    public void a(f fVar) {
        this.Yr = fVar;
    }

    public void b(d dVar) {
        z((byte) 0);
        if (this.aP) {
            a(dVar);
        }
    }

    public void cq(String str) {
        this.Yp = str;
    }

    public void disable() {
        if (this.aP) {
            this.aP = false;
            Ix();
        }
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.Yp != null) {
            this.pL.setTextSize((float) this.Yf);
            this.pL.setColor(getTitleColor());
            Paint paint = new Paint(1);
            paint.setTextSize((float) this.Yg);
            paint.setColor(tY());
            float a2 = a(paint) + a(this.pL);
            canvas.drawText(this.aU, (float) this.Yn, ((((float) getHeight()) - a2) / 2.0f) - this.pL.ascent(), this.pL);
            canvas.drawText(this.Yp, (float) this.Yn, ((a2 + ((float) getHeight())) / 2.0f) - paint.descent(), paint);
        } else {
            this.pL.setTextSize((float) this.Yf);
            this.pL.setColor(getTitleColor());
            canvas.drawText(this.aU, (float) this.Yn, ((((float) getHeight()) - this.pL.descent()) - this.pL.ascent()) / 2.0f, this.pL);
        }
        if (this.Yt != null) {
            this.Yt.setBounds((getWidth() - this.Yt.getIntrinsicWidth()) - this.Yo, (getHeight() - this.Yt.getIntrinsicHeight()) / 2, getWidth() - this.Yo, (getHeight() + this.Yt.getIntrinsicHeight()) / 2);
            this.Yt.draw(canvas);
        }
    }

    public void enable() {
        if (!this.aP) {
            this.aP = true;
            Ix();
        }
    }

    public String getKey() {
        return this.ds;
    }

    public String getTitle() {
        return this.aU;
    }

    public int getTitleColor() {
        return this.aP ? Iz() == 2 ? this.Yl : this.Yh : this.Yj;
    }

    public boolean isEnabled() {
        return this.aP;
    }

    public void setKey(String str) {
        this.ds = str;
    }

    public void t(String str) {
        this.aU = str;
    }

    public String tV() {
        return this.Yp;
    }

    public void tW() {
        if (this.Yu != null) {
            for (c cVar : this.Yu.keySet()) {
                if (this.Yu.get(cVar).equals(this.Yq)) {
                    cVar.enable();
                } else {
                    cVar.disable();
                }
            }
        }
    }

    public void tX() {
        if (this.Yr != null) {
            this.Yr.B(this.ds);
        }
    }

    public int tY() {
        return this.aP ? Iz() == 2 ? this.Ym : this.Yi : this.Yk;
    }

    public String toString() {
        return "[" + this.aU + ", " + this.Yp + "]";
    }

    public void x(Object obj) {
        this.Yq = obj;
        tW();
        if (this.Yr != null) {
            this.Yr.a(this.ds, this.Yq);
        }
    }
}
