package com.uc.f;

import android.webkit.WebSettings;
import com.uc.a.h;
import java.util.Observable;
import java.util.Observer;

public class e implements Observer {
    WebSettings bki;
    final /* synthetic */ g bkj;

    e(g gVar, WebSettings webSettings) {
        this.bkj = gVar;
        this.bki = webSettings;
    }

    public void update(Observable observable, Object obj) {
        WebSettings.TextSize textSize;
        g gVar = (g) observable;
        WebSettings webSettings = this.bki;
        webSettings.setUseWideViewPort(true);
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(!com.uc.a.e.nR().bw(h.afK).contains(com.uc.a.e.RD));
        webSettings.setMinimumFontSize(g.bEI);
        webSettings.setMinimumLogicalFontSize(g.bEJ);
        webSettings.setDefaultFontSize(g.bEH);
        webSettings.setDefaultFixedFontSize(g.bEG);
        webSettings.setNeedInitialFocus(false);
        webSettings.setSupportMultipleWindows(true);
        webSettings.setLightTouchEnabled(false);
        if (gVar.bDU == 0) {
            webSettings.setLoadsImagesAutomatically(false);
        } else {
            webSettings.setLoadsImagesAutomatically(true);
        }
        webSettings.setLayoutAlgorithm(gVar.bEy ? WebSettings.LayoutAlgorithm.NARROW_COLUMNS : WebSettings.LayoutAlgorithm.NORMAL);
        webSettings.setJavaScriptEnabled(gVar.bEx);
        webSettings.setPluginsEnabled(gVar.bEx);
        switch (gVar.textSize) {
            case 0:
                textSize = WebSettings.TextSize.SMALLER;
                break;
            case 1:
                textSize = WebSettings.TextSize.NORMAL;
                break;
            case 2:
                textSize = WebSettings.TextSize.LARGER;
                break;
            default:
                textSize = WebSettings.TextSize.NORMAL;
                break;
        }
        webSettings.setTextSize(textSize);
        webSettings.setSavePassword(gVar.bEw);
        if (gVar.bEE != null) {
            webSettings.setUserAgentString(gVar.bEE);
        }
    }
}
