package com.uc.d;

import android.content.Context;
import android.view.ContextMenu;
import com.uc.h.e;

public class g {
    public static void a(Context context, b[] bVarArr, ContextMenu contextMenu) {
        if (bVarArr != null && bVarArr.length != 0) {
            e Pb = e.Pb();
            for (b bVar : bVarArr) {
                if (bVar.el != null) {
                    contextMenu.add(bVar.em, bVar.id, 0, bVar.el).setIcon(Pb.getDrawable(bVar.aR));
                } else if (bVar.ek != -1) {
                    contextMenu.add(bVar.em, bVar.id, 0, bVar.ek).setIcon(Pb.getDrawable(bVar.aR));
                }
            }
        }
    }
}
