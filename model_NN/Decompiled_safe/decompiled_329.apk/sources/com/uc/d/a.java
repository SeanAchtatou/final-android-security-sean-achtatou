package com.uc.d;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

public class a implements MenuItem {
    c aM;
    Resources aN;
    boolean aO;
    boolean aP;
    boolean aQ;
    int aR;
    Drawable aS;
    int aT;
    String aU;
    int aV;

    public a(Context context, Menu menu, int i) {
        this.aN = context.getResources();
        this.aT = i;
        this.aM = (c) menu;
        setEnabled(true);
        setVisible(true);
    }

    public char getAlphabeticShortcut() {
        return 0;
    }

    public int getGroupId() {
        return this.aV;
    }

    public Drawable getIcon() {
        return this.aS;
    }

    public Intent getIntent() {
        return null;
    }

    public int getItemId() {
        return this.aT;
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return null;
    }

    public char getNumericShortcut() {
        return 0;
    }

    public int getOrder() {
        return 0;
    }

    public SubMenu getSubMenu() {
        return null;
    }

    public CharSequence getTitle() {
        return this.aU;
    }

    public CharSequence getTitleCondensed() {
        return this.aU;
    }

    public boolean hasSubMenu() {
        return false;
    }

    public boolean isCheckable() {
        return false;
    }

    public boolean isChecked() {
        return this.aO;
    }

    public boolean isEnabled() {
        return this.aP;
    }

    public boolean isVisible() {
        return this.aQ;
    }

    public void l(int i) {
        this.aV = i;
    }

    public MenuItem setAlphabeticShortcut(char c) {
        return this;
    }

    public MenuItem setCheckable(boolean z) {
        return this;
    }

    public MenuItem setChecked(boolean z) {
        this.aO = z;
        return this;
    }

    public MenuItem setEnabled(boolean z) {
        this.aP = z;
        return this;
    }

    public MenuItem setIcon(int i) {
        this.aR = i;
        try {
            this.aS = this.aN.getDrawable(i);
        } catch (Exception e) {
        }
        return this;
    }

    public MenuItem setIcon(Drawable drawable) {
        this.aS = drawable;
        return this;
    }

    public MenuItem setIntent(Intent intent) {
        return this;
    }

    public MenuItem setNumericShortcut(char c) {
        return this;
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        return this;
    }

    public MenuItem setShortcut(char c, char c2) {
        return this;
    }

    public MenuItem setTitle(int i) {
        this.aU = this.aN.getString(i);
        return this;
    }

    public MenuItem setTitle(CharSequence charSequence) {
        if (charSequence != null) {
            this.aU = charSequence.toString();
        }
        return this;
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        if (charSequence != null) {
            this.aU = charSequence.toString();
        }
        return this;
    }

    public MenuItem setVisible(boolean z) {
        this.aQ = z;
        this.aM.notifyDataSetChanged();
        return this;
    }
}
