package com.uc.d;

public final class b {
    public final int aR;
    public final int ek;
    public final String el;
    public final int em;
    public final int id;

    public b(int i, int i2, int i3) {
        this.ek = i2;
        this.el = null;
        this.id = i;
        this.aR = i3;
        this.em = 0;
    }

    public b(int i, int i2, int i3, int i4) {
        this.ek = i3;
        this.el = null;
        this.id = i2;
        this.aR = i4;
        this.em = i;
    }

    public b(int i, String str, int i2) {
        this.ek = -1;
        this.el = str;
        this.id = i;
        this.aR = i2;
        this.em = 0;
    }
}
