package com.uc.d;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.uc.browser.R;
import com.uc.h.e;
import java.util.Iterator;
import java.util.Vector;

public class c extends BaseAdapter implements ContextMenu {
    Context cr;
    Vector lO;
    int lP;
    Vector lQ;
    f lR;

    public c(Context context) {
        this.cr = context;
    }

    public MenuItem I(int i) {
        if (this.lO == null) {
            return null;
        }
        return (MenuItem) this.lO.elementAt(i);
    }

    public void a(f fVar) {
        this.lR = fVar;
    }

    public MenuItem add(int i) {
        return add(-1, -1, -1, i);
    }

    public MenuItem add(int i, int i2, int i3, int i4) {
        return add(i, i2, i3, this.cr.getResources().getString(i4));
    }

    public MenuItem add(int i, int i2, int i3, CharSequence charSequence) {
        a aVar = new a(this.cr, this, i2);
        aVar.setTitle(charSequence);
        aVar.l(i);
        if (this.lO == null) {
            this.lO = new Vector();
        }
        this.lO.add(aVar);
        notifyDataSetChanged();
        return aVar;
    }

    public MenuItem add(CharSequence charSequence) {
        return add(-1, -1, -1, charSequence);
    }

    public int addIntentOptions(int i, int i2, int i3, ComponentName componentName, Intent[] intentArr, Intent intent, int i4, MenuItem[] menuItemArr) {
        return 0;
    }

    public SubMenu addSubMenu(int i) {
        return null;
    }

    public SubMenu addSubMenu(int i, int i2, int i3, int i4) {
        return null;
    }

    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        return null;
    }

    public SubMenu addSubMenu(CharSequence charSequence) {
        return null;
    }

    public MenuItem b(MenuItem menuItem) {
        if (this.lO == null) {
            this.lO = new Vector();
        }
        this.lO.add(menuItem);
        notifyDataSetChanged();
        return menuItem;
    }

    public void cV() {
        int count = getCount();
        if (this.lR != null) {
            switch (count) {
                case 1:
                case 2:
                    break;
                case 3:
                case 5:
                case 6:
                case 9:
                    count = 3;
                    break;
                case 4:
                    count = 2;
                    break;
                case 7:
                case 8:
                    count = 4;
                    break;
                default:
                    count = 4;
                    break;
            }
            this.lR.buP.setNumColumns(count);
            ViewGroup.LayoutParams layoutParams = this.lR.buP.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = new ViewGroup.LayoutParams(-2, -2);
            }
            layoutParams.width = ((this.lR.buR + this.lR.buS) * count) + this.lR.buS;
            if (count < 4 && count > 0) {
                layoutParams.width += this.lR.buR / 2;
            }
            this.lR.buP.setLayoutParams(layoutParams);
            this.lR.buP.setPadding(13, 24, 13, 12);
        }
    }

    public void clear() {
        if (this.lO != null) {
            this.lO.clear();
        }
    }

    public void clearHeader() {
    }

    public void close() {
    }

    public MenuItem findItem(int i) {
        if (this.lO == null) {
            return null;
        }
        Iterator it = this.lO.iterator();
        while (it.hasNext()) {
            MenuItem menuItem = (MenuItem) it.next();
            if (menuItem.getItemId() == i) {
                return menuItem;
            }
        }
        return null;
    }

    public int getCount() {
        if (this.lO == null) {
            return 0;
        }
        if (this.lQ == null) {
            this.lQ = new Vector();
        }
        this.lQ.clear();
        this.lP = 0;
        Iterator it = this.lO.iterator();
        while (it.hasNext()) {
            MenuItem menuItem = (MenuItem) it.next();
            if (menuItem.isVisible()) {
                this.lP++;
                this.lQ.add(menuItem);
            }
        }
        return this.lP;
    }

    public MenuItem getItem(int i) {
        if (this.lQ == null) {
            return null;
        }
        return (MenuItem) this.lQ.elementAt(i);
    }

    public long getItemId(int i) {
        if (this.lO == null || this.lO.size() == 0) {
            return -1;
        }
        return (long) ((MenuItem) this.lO.elementAt(i)).getItemId();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView textView;
        TextView textView2 = (TextView) view;
        if (textView2 == null) {
            textView = (TextView) LayoutInflater.from(viewGroup.getContext()).inflate((int) R.layout.f944uccontextmenu_item, viewGroup, false);
            textView.setSingleLine();
        } else {
            textView = textView2;
        }
        MenuItem item = getItem(i);
        if (item != null) {
            textView.setText(item.getTitleCondensed());
            Drawable icon = item.getIcon();
            icon.setBounds(0, 0, (int) viewGroup.getResources().getDimension(R.dimen.f260menu_item_icon_width), (int) viewGroup.getResources().getDimension(R.dimen.f260menu_item_icon_width));
            if (item.isEnabled()) {
                icon.setAlpha(255);
            } else {
                icon.setAlpha(90);
            }
            textView.setCompoundDrawables(null, icon, null, null);
            if (!item.isVisible()) {
                textView.setVisibility(8);
            } else {
                textView.setVisibility(0);
            }
            textView.setTextColor(e.Pb().getColor(78));
        }
        return textView;
    }

    public boolean hasVisibleItems() {
        return getCount() > 0;
    }

    public boolean isShortcutKey(int i, KeyEvent keyEvent) {
        return false;
    }

    public void notifyDataSetChanged() {
        cV();
        super.notifyDataSetChanged();
        if (this.lR != null) {
            if (this.lQ == null || this.lQ.size() == 0) {
                this.lR.dismiss();
            } else {
                this.lR.show();
            }
        }
    }

    public boolean performIdentifierAction(int i, int i2) {
        return false;
    }

    public boolean performShortcut(int i, KeyEvent keyEvent, int i2) {
        return false;
    }

    public void removeGroup(int i) {
    }

    public void removeItem(int i) {
    }

    public void setGroupCheckable(int i, boolean z, boolean z2) {
    }

    public void setGroupEnabled(int i, boolean z) {
    }

    public void setGroupVisible(int i, boolean z) {
        if (this.lO != null) {
            Iterator it = this.lO.iterator();
            while (it.hasNext()) {
                MenuItem menuItem = (MenuItem) it.next();
                if (menuItem.getGroupId() == i) {
                    menuItem.setVisible(z);
                }
            }
        }
    }

    public ContextMenu setHeaderIcon(int i) {
        return this;
    }

    public ContextMenu setHeaderIcon(Drawable drawable) {
        return this;
    }

    public ContextMenu setHeaderTitle(int i) {
        return this;
    }

    public ContextMenu setHeaderTitle(CharSequence charSequence) {
        return this;
    }

    public ContextMenu setHeaderView(View view) {
        return this;
    }

    public void setQwertyMode(boolean z) {
    }

    public int size() {
        return 0;
    }
}
