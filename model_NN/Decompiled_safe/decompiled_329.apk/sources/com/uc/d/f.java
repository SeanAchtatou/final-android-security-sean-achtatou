package com.uc.d;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.StateListDrawable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import com.uc.browser.R;
import com.uc.browser.UCR;
import com.uc.h.e;

public class f extends Dialog implements AdapterView.OnItemClickListener {
    public GridView buP;
    private d buQ;
    protected int buR = 0;
    protected int buS = 0;
    private e buT = null;
    private c lE;

    public f(Context context) {
        super(context, R.style.f1575context_menu);
        a();
    }

    public void GH() {
        super.dismiss();
        if (this.buT != null) {
            this.buT.onDismiss();
        }
    }

    public void a() {
        if (this.buP == null) {
            Resources resources = getContext().getResources();
            RelativeLayout relativeLayout = new RelativeLayout(getContext());
            relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(-2, -1));
            relativeLayout.setPadding(2, 30, 2, 30);
            this.buR = (int) resources.getDimension(R.dimen.f253menu_column_width);
            this.buS = (int) resources.getDimension(R.dimen.f254menu_v_space);
            this.buP = new GridView(getContext());
            this.buP.setColumnWidth(this.buR);
            this.buP.setPadding(13, 60, 13, 12);
            this.buP.setGravity(17);
            this.buP.setStretchMode(2);
            this.buP.setHorizontalSpacing((int) resources.getDimension(R.dimen.f255menu_h_space));
            this.buP.setVerticalSpacing(this.buS);
            this.buP.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aUm));
            this.buP.setOnItemClickListener(this);
            relativeLayout.addView(this.buP);
            setContentView(relativeLayout);
        }
    }

    public void a(c cVar) {
        this.lE = cVar;
        if (this.buP != null) {
            this.buP.setAdapter((ListAdapter) cVar);
        }
    }

    public void a(d dVar) {
        this.buQ = dVar;
    }

    public void a(e eVar) {
        this.buT = eVar;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() != 84) {
            return super.dispatchKeyEvent(keyEvent);
        }
        dismiss();
        return true;
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (!(this.buQ == null || this.lE == null)) {
            this.buQ.a(this.lE.getItem(i));
        }
        dismiss();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        GH();
        return super.onTouchEvent(motionEvent);
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (!z) {
            dismiss();
        }
    }

    public void show() {
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842919}, e.Pb().getDrawable(UCR.drawable.aVI));
        stateListDrawable.addState(new int[]{16842908}, e.Pb().getDrawable(UCR.drawable.aVK));
        this.buP.setSelector(stateListDrawable);
        this.buP.setBackgroundDrawable(e.Pb().getDrawable(UCR.drawable.aUm));
        this.buP.setPadding(13, 24, 13, 12);
        try {
            super.show();
        } catch (Exception e) {
            Log.e("UCMenuDlg", "show():" + e.toString());
        }
    }
}
