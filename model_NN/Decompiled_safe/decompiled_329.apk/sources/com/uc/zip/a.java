package com.uc.zip;

import com.uc.c.ay;
import com.uc.c.bx;
import com.uc.c.w;
import java.io.DataInputStream;
import java.io.IOException;

public class a {
    private static final a GB = new a();
    private static final int GJ = 2;
    private static final int GK = 300000;
    public static final int GL = 60000;
    private TZipInputStream GC;
    private short GD;
    private boolean GE = false;
    private long GF;
    private long GG;
    private byte GH;
    private boolean GI = false;
    public bx GM = null;

    private a() {
        ki();
    }

    public static a kh() {
        return GB;
    }

    public void N(boolean z) {
        if (z) {
            try {
                if (this.GC != null) {
                    this.GC.destroy();
                }
            } catch (IOException e) {
            } finally {
                this.GC = null;
            }
        } else if (this.GC != null) {
            this.GC.reset();
        }
    }

    public synchronized boolean c(bx bxVar) {
        boolean z;
        if (bxVar != null) {
            if (bxVar.GZ()) {
                if (!w.Nv || this.GD < 0 || bxVar.GX() || bxVar.Hb()) {
                    z = false;
                } else if (this.GH >= 2) {
                    if (!this.GE) {
                        N(true);
                        w.Nv = false;
                        kn();
                        this.GM = null;
                    }
                    z = false;
                } else if (!this.GE) {
                    km();
                    this.GF = 300000;
                    this.GM = bxVar;
                    z = true;
                } else if (System.currentTimeMillis() - this.GG < this.GF) {
                    z = false;
                } else {
                    this.GF = 300000;
                    N(false);
                    km();
                    this.GM = bxVar;
                    z = true;
                }
            }
        }
        z = false;
        return z;
    }

    public void d(bx bxVar) {
        if (this.GI) {
            this.GI = false;
            return;
        }
        kn();
        this.GM = bxVar;
        this.GH = (byte) (this.GH + 1);
        if (bxVar == null || !bxVar.GZ()) {
            this.GI = false;
            return;
        }
        this.GI = true;
        bxVar.HG();
    }

    public void f(int i, boolean z) {
        if (this.GE) {
            if (!z) {
                if (this.GF - (System.currentTimeMillis() - this.GG) <= ((long) i)) {
                    return;
                }
            }
            this.GF = (long) i;
            this.GG = System.currentTimeMillis();
        }
    }

    public void ki() {
        long maxMemory = Runtime.getRuntime().maxMemory();
        if (maxMemory < 8388608) {
            this.GD = -1;
        } else if (maxMemory < 16777216) {
            this.GD = ay.aZV;
        } else {
            this.GD = 512;
        }
    }

    public short kj() {
        return this.GD;
    }

    public TZipInputStream kk() {
        if (this.GC == null) {
            this.GC = new TZipInputStream();
        }
        return this.GC;
    }

    public byte[] kl() {
        byte[] bArr = new byte[3];
        bArr[0] = 84;
        bArr[1] = 1;
        if (this.GC == null) {
            bArr[2] = 0;
        } else {
            bArr[2] = this.GC.zi();
        }
        return bArr;
    }

    public void km() {
        this.GE = true;
        this.GG = System.currentTimeMillis();
    }

    public void kn() {
        this.GE = false;
    }

    public boolean ko() {
        return this.GE;
    }

    public boolean l(DataInputStream dataInputStream) {
        byte[] bArr = new byte[3];
        try {
            dataInputStream.read(bArr, 0, 3);
        } catch (IOException e) {
        }
        if (bArr[0] != 84 || bArr[1] != 1) {
            return false;
        }
        byte b2 = bArr[2];
        if (b2 == 0) {
            N(true);
            return true;
        }
        if ((this.GC != null ? this.GC.zi() : 0) != b2) {
            N(true);
            return false;
        }
        N(false);
        return true;
    }

    public boolean m(DataInputStream dataInputStream) {
        byte[] bArr = new byte[3];
        try {
            dataInputStream.read(bArr, 0, 3);
        } catch (IOException e) {
        }
        if (bArr[0] != 84 || bArr[1] != 1) {
            return false;
        }
        byte b2 = bArr[2];
        if (this.GC == null) {
            return false;
        }
        this.GC.w(b2);
        this.GH = 0;
        return true;
    }
}
