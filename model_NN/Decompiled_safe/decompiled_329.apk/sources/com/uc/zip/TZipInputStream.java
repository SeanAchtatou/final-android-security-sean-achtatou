package com.uc.zip;

import java.io.IOException;
import java.io.InputStream;

public class TZipInputStream extends InputStream {
    private byte aEr = 0;
    private int aEs = 0;
    private InputStream aEt;
    private int aEu;

    static {
        System.loadLibrary("CTZip");
    }

    public TZipInputStream() {
        if (this.aEs != 0) {
            try {
                destroy();
            } catch (IOException e) {
            }
        }
        this.aEs = create();
    }

    private native int create();

    private native void delete(int i);

    private int fw(int i) {
        int i2 = 1024;
        if (i > 1024) {
            i2 = i;
        }
        return this.aEu < 2048 ? this.aEu : i2;
    }

    private native boolean needReadMore(int i);

    private native int readImp(int i, byte[] bArr, int i2, int i3);

    private native void reset(int i);

    private native void setSourceData(int i, byte[] bArr, int i2);

    private void zj() {
        if (this.aEt != null) {
            try {
                this.aEt.close();
            } catch (IOException e) {
            } finally {
                this.aEt = null;
            }
        }
    }

    public void close() {
        if (this != a.kh().kk()) {
            destroy();
        }
    }

    public void destroy() {
        zj();
        if (this.aEs != 0) {
            delete(this.aEs);
            this.aEs = 0;
        }
    }

    public void fv(int i) {
        this.aEu = i;
    }

    public int read() {
        byte[] bArr = new byte[1];
        if (read(bArr, 0, 1) == -1) {
            return -1;
        }
        return bArr[0] & 255;
    }

    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public int read(byte[] bArr, int i, int i2) {
        byte[] bArr2;
        int read;
        if (this.aEu > 0 && needReadMore(this.aEs) && (read = this.aEt.read(bArr2, 0, (r0 = fw(i2)))) != -1) {
            this.aEu -= read;
            setSourceData(this.aEs, (bArr2 = new byte[(r0 = fw(i2))]), read);
        }
        int readImp = readImp(this.aEs, bArr, i, i2);
        if (readImp == -2) {
            throw new IOException();
        } else if (readImp != -3) {
            return readImp;
        } else {
            throw new IOException();
        }
    }

    public void reset() {
        zj();
        this.aEr = 0;
        this.aEu = 0;
        if (this.aEs != 0) {
            reset(this.aEs);
        }
    }

    public void setInputStream(InputStream inputStream) {
        if (this.aEt != null) {
            try {
                this.aEt.close();
            } catch (IOException e) {
            }
            this.aEt = null;
        }
        this.aEt = inputStream;
    }

    public void w(byte b2) {
        this.aEr = b2;
    }

    public byte zi() {
        return this.aEr;
    }
}
