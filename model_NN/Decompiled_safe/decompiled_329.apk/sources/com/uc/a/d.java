package com.uc.a;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

public class d {
    private String vu = null;
    private String vv = null;

    public void S(String str) {
        e.nR().S(str);
    }

    public boolean T(String str) {
        return e.nR().T(str);
    }

    public boolean U(String str) {
        return e.nR().U(str);
    }

    public String V(String str) {
        return e.nR().V(str);
    }

    public void a(int i, Resources resources, int i2) {
        e.nR().a(i, resources, i2);
    }

    public void a(int i, Bitmap bitmap) {
        e.nR().a(i, bitmap);
    }

    public void a(String[] strArr) {
        e.nR().b(strArr, e.nR().nY().bw(h.afm));
    }

    public void aH() {
    }

    public boolean aI() {
        return e.nR().aI();
    }

    public void aJ() {
        e.nR().aJ();
    }

    public void aK() {
        e.nR().aK();
    }

    public void aL() {
        e.nR().aL();
    }

    public void aM() {
        e.nR().aM();
    }

    public void aN() {
        e.nR().aN();
    }

    public byte aO() {
        return e.nR().aO();
    }

    public byte aP() {
        return e.nR().aP();
    }

    public byte b(short s) {
        return e.nR().b(s);
    }

    public void b(int i, int i2) {
        e.nR().b(i, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.uc.a.e.a(boolean, com.uc.a.n):int
     arg types: [int, com.uc.a.n]
     candidates:
      com.uc.a.e.a(java.lang.String, java.util.Hashtable):java.util.Vector
      com.uc.a.e.a(b.a.a.e, b.a.a.e):int
      com.uc.a.e.a(float, int):android.graphics.Bitmap
      com.uc.a.e.a(java.lang.String, byte):java.lang.String
      com.uc.a.e.a(int, android.graphics.Bitmap):void
      com.uc.a.e.a(int, short):void
      com.uc.a.e.a(android.graphics.Bitmap, int):void
      com.uc.a.e.a(com.uc.a.n, int):void
      com.uc.a.e.a(java.io.File, int):void
      com.uc.a.e.a(java.util.List, int):void
      com.uc.a.e.a(boolean, int):void
      com.uc.a.e.a(int, int):boolean
      com.uc.a.e.a(int, b.a.a.e):boolean
      com.uc.a.e.a(java.lang.String, b.a.a.c.e):boolean
      com.uc.a.e.a(boolean, com.uc.a.n):int */
    public a c(n nVar) {
        return new a(e.nR().a(true, nVar));
    }

    public void d(boolean z) {
        e.nR().d(z);
    }

    public void e(boolean z) {
        e.nR().e(z);
    }

    public void fZ() {
        e.nR().fZ();
    }

    public int ga() {
        return e.nR().ga();
    }

    public String gb() {
        return e.nR().gb();
    }

    public short gd() {
        return e.nR().gd();
    }

    public String ge() {
        return e.nR().ge();
    }

    public String gf() {
        return e.nR().gf();
    }

    public boolean gg() {
        if (!e.nR().or()) {
            return false;
        }
        this.vu = e.nR().gh();
        this.vv = e.nR().gi();
        return true;
    }

    public String gh() {
        return this.vu;
    }

    public String gi() {
        return this.vv;
    }

    public void gj() {
        e.nR().gj();
    }

    public void gk() {
        e.nR().gk();
    }

    public int gl() {
        return e.nR().gl();
    }

    public void j(Drawable drawable) {
        e.nR().j(drawable);
    }

    public void u(boolean z) {
        e.nR().u(z);
    }

    public void x(int i, int i2) {
        e.nR().x(i, i2);
    }
}
