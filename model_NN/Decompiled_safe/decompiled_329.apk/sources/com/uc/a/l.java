package com.uc.a;

import b.a.a.a.f;
import b.a.a.e;
import com.uc.c.cd;
import com.uc.c.w;
import java.util.Vector;

public final class l {
    public static String bgF = "ext:history_rec";
    public static String bgG = "ext:show_nav:ln_hothis";
    Vector bgH = null;

    public void CI() {
        e.nR().oU();
    }

    public Vector CJ() {
        return e.nR().nO();
    }

    public String a(String str, byte b2) {
        return e.nR().a(str, b2);
    }

    public String ac(int i, int i2) {
        return e.nR().ac(i, i2);
    }

    public void bo(String str) {
        e.nR().bo(str);
    }

    public void bp(String str) {
        e.nR().bp(str);
    }

    public Vector bq(String str) {
        f fVar;
        if (this.bgH == null || str == null) {
            return null;
        }
        if (str.equals("ext:show_nav:ln_hothis")) {
            Vector ow = e.nR().ob().ow();
            if (ow == null || ow.isEmpty()) {
                return null;
            }
            Vector vector = new Vector(ow.size());
            for (int i = 0; i < ow.size(); i++) {
                e eVar = (e) ow.elementAt(i);
                String str2 = eVar.abM;
                String str3 = eVar.abL;
                if (str2 != null && str2.length() > 0) {
                    e eVar2 = new e();
                    eVar2.abM = str2;
                    eVar2.abL = str3;
                    vector.addElement(eVar2);
                }
            }
            if (vector.size() > 0) {
                return vector;
            }
            return null;
        }
        Vector bq = e.nR().bq(str);
        if (bq == null || bq.isEmpty()) {
            return null;
        }
        Vector vector2 = new Vector(bq.size());
        for (int i2 = 0; i2 < bq.size(); i2++) {
            Object[] objArr = (Object[]) bq.elementAt(i2);
            String str4 = (String) objArr[0];
            String str5 = (String) objArr[1];
            byte[] bArr = (byte[]) objArr[2];
            try {
                fVar = f.g(bArr, 0, bArr.length);
            } catch (Exception e) {
                fVar = null;
            }
            if (str4 != null && str4.length() > 0) {
                e eVar3 = new e();
                eVar3.abM = str4;
                eVar3.abL = str5;
                if (fVar != null) {
                    eVar3.abN = fVar.FQ;
                }
                j(eVar3);
                vector2.addElement(eVar3);
            }
        }
        if (vector2.size() > 0) {
            return vector2;
        }
        return null;
    }

    public boolean cm(int i) {
        return e.nR().cm(i);
    }

    public void f(n nVar) {
        e.nR().f(nVar);
    }

    public void g(n nVar) {
        e.nR().g(nVar);
    }

    public boolean h(n nVar) {
        return e.nR().h(nVar);
    }

    public void i(n nVar) {
        e.nR().i(nVar);
    }

    public void j(e eVar) {
        int indexOf;
        try {
            eVar.abO = null;
            int indexOf2 = eVar.abL.indexOf("&siteimg=");
            if (indexOf2 > 0) {
                eVar.abO = eVar.abL.substring(indexOf2 + 9) + ".png";
                eVar.abL = eVar.abL.substring(0, indexOf2);
            }
            String[] split = eVar.abL.split(cd.bVI);
            int i = -1;
            for (int i2 = 0; i2 < split.length; i2++) {
                split[i2] = split[i2].trim();
                if (split[i2].startsWith("siteimg") && (indexOf = split[i2].indexOf("=")) >= 7) {
                    eVar.abO = split[i2].substring(indexOf + 1) + ".png";
                    i = i2;
                }
            }
            if (i < split.length) {
                eVar.abL = null;
                for (int i3 = 0; i3 < split.length; i3++) {
                    if (i3 != i) {
                        if (eVar.abL == null) {
                            eVar.abL = split[i3] + cd.bVI;
                        } else {
                            eVar.abL += split[i3] + cd.bVI;
                        }
                    }
                }
            }
            if (eVar.abL != null && eVar.abL.endsWith(cd.bVI)) {
                eVar.abL = eVar.abL.substring(0, eVar.abL.length() - 1);
            }
            if (eVar.abL != null && eVar.abL.startsWith("ext:p:")) {
                eVar.abL = eVar.abL.substring(6);
            }
        } catch (Exception e) {
        }
    }

    public Vector oA() {
        return e.nR().oA();
    }

    public Vector oB() {
        f fVar;
        Vector oB = e.nR().oB();
        Vector vector = new Vector(oB.size());
        if (oB == null || oB.isEmpty()) {
            return null;
        }
        vector.clear();
        for (int i = 0; i < oB.size(); i++) {
            Object[] objArr = (Object[]) oB.elementAt(i);
            String str = (String) objArr[0];
            String str2 = (String) objArr[1];
            byte[] bArr = (byte[]) objArr[2];
            try {
                fVar = f.g(bArr, 0, bArr.length);
            } catch (Exception e) {
                fVar = null;
            }
            j jVar = new j();
            jVar.aFV = (String) objArr[0];
            jVar.aFU = (String) objArr[1];
            if (fVar != null) {
                jVar.aFW = fVar.FQ;
            }
            try {
                f bz = e.nR().bz(jVar.aFU);
                if (bz != null) {
                    jVar.aFW = bz.FQ;
                }
            } catch (Exception e2) {
            }
            if (true == jVar.aFU.equals(w.Pa)) {
                jVar.aFX = 1;
            } else if (true == jVar.aFU.equals("ext:lp:lp_favor")) {
                jVar.aFX = 2;
            } else if (true == jVar.aFU.equals("ext:lp:lp_news")) {
                jVar.aFX = 0;
            } else if (true == jVar.aFU.equals("ext:lp:lp_life")) {
                jVar.aFX = 0;
            } else if (true == jVar.aFU.equals("ext:lp:lp_myzone")) {
                jVar.aFX = 0;
            } else if (true == jVar.aFU.equals("ext:lp:lp_game")) {
                jVar.aFX = 0;
            } else if (true == jVar.aFU.equals("ext:lp:lp_music")) {
                jVar.aFX = 0;
            } else if (true == jVar.aFU.equals("ext:lp:lp_video")) {
                jVar.aFX = 0;
            }
            vector.addElement(jVar);
        }
        if (vector.size() > 0) {
            return vector;
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x005c A[SYNTHETIC, Splitter:B:19:0x005c] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0070 A[Catch:{ Exception -> 0x0088 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0076 A[Catch:{ Exception -> 0x0088 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Vector oC() {
        /*
            r9 = this;
            r8 = 0
            r2 = 0
            com.uc.a.e r0 = com.uc.a.e.nR()
            java.util.Vector r4 = r0.oC()
            if (r4 == 0) goto L_0x0012
            boolean r0 = r4.isEmpty()
            if (r0 == 0) goto L_0x0014
        L_0x0012:
            r0 = r8
        L_0x0013:
            return r0
        L_0x0014:
            java.util.Vector r0 = r9.bgH
            if (r0 != 0) goto L_0x0023
            java.util.Vector r0 = new java.util.Vector
            int r1 = r4.size()
            r0.<init>(r1)
            r9.bgH = r0
        L_0x0023:
            java.util.Vector r0 = r9.bgH
            r0.clear()
            r5 = r2
        L_0x0029:
            int r0 = r4.size()     // Catch:{ Exception -> 0x0088 }
            if (r5 >= r0) goto L_0x0089
            java.lang.Object r0 = r4.elementAt(r5)     // Catch:{ Exception -> 0x0088 }
            java.lang.Object[] r0 = (java.lang.Object[]) r0     // Catch:{ Exception -> 0x0088 }
            java.lang.Object[] r0 = (java.lang.Object[]) r0     // Catch:{ Exception -> 0x0088 }
            r1 = 0
            r1 = r0[r1]     // Catch:{ Exception -> 0x0088 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x0088 }
            r2 = 1
            r2 = r0[r2]     // Catch:{ Exception -> 0x0088 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x0088 }
            r3 = 2
            r3 = r0[r3]     // Catch:{ Exception -> 0x0088 }
            byte[] r3 = (byte[]) r3     // Catch:{ Exception -> 0x0088 }
            byte[] r3 = (byte[]) r3     // Catch:{ Exception -> 0x0088 }
            r6 = 3
            r0 = r0[r6]     // Catch:{ Exception -> 0x0088 }
            byte[] r0 = (byte[]) r0     // Catch:{ Exception -> 0x0088 }
            byte[] r0 = (byte[]) r0     // Catch:{ Exception -> 0x0088 }
            if (r3 == 0) goto L_0x009c
            int r6 = r3.length     // Catch:{ Exception -> 0x0083 }
            if (r6 <= 0) goto L_0x009c
            r6 = 0
            int r7 = r3.length     // Catch:{ Exception -> 0x0083 }
            b.a.a.a.f r3 = b.a.a.a.f.g(r3, r6, r7)     // Catch:{ Exception -> 0x0083 }
        L_0x005a:
            if (r0 == 0) goto L_0x009a
            int r6 = r0.length     // Catch:{ Exception -> 0x0097 }
            if (r6 <= 0) goto L_0x009a
            r6 = 0
            int r7 = r0.length     // Catch:{ Exception -> 0x0097 }
            b.a.a.a.f r0 = b.a.a.a.f.g(r0, r6, r7)     // Catch:{ Exception -> 0x0097 }
        L_0x0065:
            com.uc.a.b r6 = new com.uc.a.b     // Catch:{ Exception -> 0x0088 }
            r6.<init>()     // Catch:{ Exception -> 0x0088 }
            r6.oL = r1     // Catch:{ Exception -> 0x0088 }
            r6.oK = r2     // Catch:{ Exception -> 0x0088 }
            if (r3 == 0) goto L_0x0074
            android.graphics.Bitmap r1 = r3.FQ     // Catch:{ Exception -> 0x0088 }
            r6.oM = r1     // Catch:{ Exception -> 0x0088 }
        L_0x0074:
            if (r0 == 0) goto L_0x007a
            android.graphics.Bitmap r0 = r0.FQ     // Catch:{ Exception -> 0x0088 }
            r6.oN = r0     // Catch:{ Exception -> 0x0088 }
        L_0x007a:
            java.util.Vector r0 = r9.bgH     // Catch:{ Exception -> 0x0088 }
            r0.addElement(r6)     // Catch:{ Exception -> 0x0088 }
            int r0 = r5 + 1
            r5 = r0
            goto L_0x0029
        L_0x0083:
            r0 = move-exception
            r0 = r8
        L_0x0085:
            r3 = r0
            r0 = r8
            goto L_0x0065
        L_0x0088:
            r0 = move-exception
        L_0x0089:
            java.util.Vector r0 = r9.bgH
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x0094
            java.util.Vector r0 = r9.bgH
            goto L_0x0013
        L_0x0094:
            r0 = r8
            goto L_0x0013
        L_0x0097:
            r0 = move-exception
            r0 = r3
            goto L_0x0085
        L_0x009a:
            r0 = r8
            goto L_0x0065
        L_0x009c:
            r3 = r8
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uc.a.l.oC():java.util.Vector");
    }

    public int oL() {
        return e.nR().oL();
    }

    public int oM() {
        return e.nR().oM();
    }

    public String oN() {
        return e.nR().oN();
    }

    public String oO() {
        return e.nR().oO();
    }

    public void ov() {
        e.nR().ov();
    }

    public Vector oy() {
        return e.nR().oy();
    }

    public Vector oz() {
        return e.nR().oz();
    }

    public void pN() {
        e.nR().pN();
    }

    public Vector qc() {
        try {
            Vector qc = e.nR().qc();
            if (qc == null || qc.isEmpty()) {
                return null;
            }
            Vector vector = new Vector(qc.size());
            for (int i = 0; i < qc.size(); i++) {
                try {
                    Object[] objArr = (Object[]) qc.elementAt(i);
                    f fVar = (f) objArr[2];
                    e eVar = new e();
                    eVar.abM = (String) objArr[0];
                    eVar.abL = (String) objArr[1];
                    if (fVar != null) {
                        eVar.abN = fVar.FQ;
                    }
                    vector.addElement(eVar);
                } catch (Exception e) {
                }
            }
            return vector;
        } catch (Exception e2) {
            return null;
        }
    }
}
