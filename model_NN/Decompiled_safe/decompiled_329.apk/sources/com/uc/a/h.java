package com.uc.a;

public class h {
    public static final String USER_AGENT = "uc_pref_user_agent";
    public static final String aeU = "uc_pref_textsize";
    public static final String aeV = "uc_pref_picture_quality";
    public static final String aeW = "uc_pref_word_subsection";
    public static final String aeX = "uc_pref_auto_landscape";
    public static final String aeY = "uc_pref_folding_mode";
    public static final String aeZ = "uc_pref_titlebar_fullscreen";
    public static final String afA = "uc_pref_mynavi_line";
    public static final String afB = "uc_pref_auto_landscape_lock";
    public static final String afC = "uc_pref_preread_type";
    public static final String afD = "uc_pref_website_security_hint";
    public static final String afE = "uc_pref_download_security_prehint";
    public static final String afF = "uc_pref_download_security_posthint";
    public static final String afG = "uc_pref_need_history_record";
    public static final String afH = "uc_pref_need_most_visit_record";
    public static final String afI = "text_size";
    public static final String afJ = "should_create_shortcut";
    public static final String afK = "uc_full_screen";
    public static final String afL = "clear_all_on_quit";
    public static final String afM = "uc_pref_taskrename";
    public static final String afN = "uc_pref_night_mode";
    public static final String afO = "clear_option_flag";
    public static final String afP = "pre_read_flag";
    public static final String afQ = "never_tips_exit";
    public static final String afR = "show_avatar";
    public static final String afa = "uc_pref_skin_index";
    public static final String afb = "uc_pref_preread_total";
    public static final String afc = "uc_pref_usepreread_official";
    public static final String afd = "uc_pref_popupinfo";
    public static final String afe = "uc_pref_wap_transit";
    public static final String aff = "uc_pref_proxy_server";
    public static final String afg = "uc_pref_wifi_mode";
    public static final String afh = "bak_pic_quality";
    public static final String afi = "bak_dl_task_count";
    public static final String afj = "bak_page_size";
    public static final String afk = "bak_prerede_type";
    public static final String afl = "uc_pref_download_app";
    public static final String afm = "uc_pref_download_path";
    public static final String afn = "uc_pref_download_task_count";
    public static final String afo = "uc_pref_download_in_background";
    public static final String afp = "uc_pref_download_finish_tips";
    public static final String afq = "uc_pref_browser_model";
    public static final String afr = "uc_pref_fit_screen";
    public static final String afs = "uc_pref_enablejs";
    public static final String aft = "uc_pref_save_psw";
    public static final String afu = "uc_pref_clear_psw";
    public static final String afv = "uc_pref_night_mode_brightness";
    public static final String afw = "uc_pref_page_up_down_location";
    public static final String afx = "uc_pref_save_psw_juc";
    public static final String afy = "uc_pref_novel_mode";
    public static final String afz = "uc_pref_enable_sound";
    public static final String xc = "should_show_zoom_mode_tips";
    public static final String xd = "should_show_novel_mode_tips";
    public static final String xe = "should_show_page_up_down_mode_tips";
    public static final String xf = "should_show_nightmode_tips";

    public String bw(String str) {
        return e.nR().bw(str);
    }

    public void oQ() {
        e.nR().oQ();
    }

    public void oR() {
        e.nR().oR();
    }

    public void oS() {
        e.nR().oS();
    }

    public void pp() {
        e.nR().pp();
    }

    public void setTheme(int i) {
        e.nR().setTheme(i);
    }

    public void w(String str, String str2) {
        e.nR().w(str, str2);
    }
}
