package com.uc.a;

import android.graphics.Bitmap;
import android.view.KeyEvent;
import android.view.MotionEvent;
import b.a.a.a.u;
import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Vector;

public class a {
    private int ad = 0;

    public a(int i) {
        this.ad = i;
    }

    public boolean A() {
        return e.nR().cJ(this.ad);
    }

    public boolean B() {
        return e.nR().cK(this.ad);
    }

    public int C() {
        return e.nR().cL(this.ad);
    }

    public String D() {
        return e.nR().cM(this.ad);
    }

    public int E() {
        return e.nR().cE(this.ad);
    }

    public String F() {
        return e.nR().cQ(this.ad);
    }

    public int G() {
        return e.nR().cR(this.ad);
    }

    public void H() {
        e.nR().cS(this.ad);
    }

    public void I() {
        e.nR().cT(this.ad);
    }

    public void J() {
        e.nR().cU(this.ad);
    }

    public void K() {
        e.nR().cV(this.ad);
    }

    public void L() {
        e.nR().cW(this.ad);
    }

    public boolean M() {
        return e.nR().cX(this.ad);
    }

    public int N() {
        return e.nR().cY(this.ad);
    }

    public boolean O() {
        return e.nR().cZ(this.ad);
    }

    public void P() {
        e.nR().db(this.ad);
    }

    public void Q() {
        e.nR().dc(this.ad);
    }

    public void R() {
        e.nR().cw(this.ad);
    }

    public int S() {
        return e.nR().cx(this.ad);
    }

    public void T() {
        e.nR().dd(this.ad);
    }

    public boolean U() {
        return e.nR().de(this.ad);
    }

    public boolean V() {
        return e.nR().df(this.ad);
    }

    public void W() {
        e.nR().dg(this.ad);
    }

    public boolean X() {
        return e.nR().dh(this.ad);
    }

    public void Y() {
        e.nR().di(this.ad);
    }

    public short Z() {
        return e.nR().dj(this.ad);
    }

    public Bitmap a(float f) {
        return e.nR().a(f, this.ad);
    }

    public void a(float f, float f2, float f3, boolean z) {
        e.nR().a(this.ad, f, f2, f3, z);
    }

    public void a(int i, float f, float f2) {
        e.nR().a(this.ad, i, f, f2);
    }

    public void a(int i, Vector vector, boolean z) {
        e.nR().a(i, vector, this.ad, z);
    }

    public void a(Bitmap bitmap) {
        e.nR().a(bitmap, this.ad);
    }

    public void a(n nVar) {
        e.nR().a(nVar, this.ad);
    }

    public void a(File file) {
        e.nR().a(file, this.ad);
    }

    public void a(Object obj) {
        e.nR().b(obj, this.ad);
    }

    public void a(String str) {
        e.nR().h(str, this.ad);
    }

    public void a(String str, int i, boolean z) {
        try {
            e.nR().a(str, this.ad, i, z);
        } catch (Throwable th) {
        }
    }

    public void a(String str, String str2) {
        e.nR().a(str, str2, this.ad);
    }

    public void a(String str, String str2, String str3, String str4) {
        e.nR().b(this.ad, str, str2, str3, str4);
    }

    public void a(List list) {
        e.nR().a(list, this.ad);
    }

    public void a(short s) {
        e.nR().a(this.ad, s);
    }

    public void a(boolean z) {
        e.nR().a(z, this.ad);
    }

    public void a(byte[] bArr) {
        e.nR().t(bArr, this.ad);
    }

    public boolean a(int i, int i2) {
        return e.nR().g(i, this.ad, i2);
    }

    public boolean a(int i, KeyEvent keyEvent) {
        return e.nR().aa(i, this.ad);
    }

    public boolean a(MotionEvent motionEvent) {
        try {
            return e.nR().d(motionEvent.getAction(), (int) motionEvent.getX(), (int) motionEvent.getY(), this.ad);
        } catch (Throwable th) {
            return false;
        }
    }

    public boolean a(u uVar, int i, int i2, boolean z, boolean z2) {
        return e.nR().a(uVar, this.ad, i, i2, z, z2);
    }

    public boolean a(File file, byte b2) {
        return e.nR().a(this.ad, file, b2);
    }

    public boolean a(String str, String str2, n nVar, boolean z, String str3) {
        return e.nR().a(str, str2, nVar, z, str3);
    }

    public byte[] a(InputStream inputStream) {
        return e.nR().a(inputStream);
    }

    public boolean aa() {
        return e.nR().dn(this.ad);
    }

    public float ab() {
        return e.nR().m0do(this.ad);
    }

    public boolean ac() {
        return e.nR().dp(this.ad);
    }

    public boolean ad() {
        return e.nR().dq(this.ad);
    }

    public boolean ae() {
        return e.nR().dr(this.ad);
    }

    public void af() {
        e.nR().dE(this.ad);
    }

    public void ag() {
        e.nR().dF(this.ad);
    }

    public int ah() {
        return e.nR().j(this.ad);
    }

    public Bitmap ai() {
        return e.nR().dv(this.ad);
    }

    public boolean aj() {
        return e.nR().dw(this.ad);
    }

    public void ak() {
        e.nR().dx(this.ad);
    }

    public int al() {
        return e.nR().dz(this.ad);
    }

    public int am() {
        return e.nR().dy(this.ad);
    }

    public int an() {
        return e.nR().dA(this.ad);
    }

    public boolean ao() {
        return e.nR().dB(this.ad);
    }

    public void ap() {
        e.nR().dC(this.ad);
    }

    public boolean aq() {
        return e.nR().dG(this.ad);
    }

    public boolean ar() {
        return e.nR().dH(this.ad);
    }

    public boolean as() {
        return e.nR().dI(this.ad);
    }

    public boolean at() {
        return e.nR().dJ(this.ad);
    }

    public boolean au() {
        return e.nR().dK(this.ad);
    }

    public boolean av() {
        return e.nR().dL(this.ad);
    }

    public void aw() {
        e.nR().dM(this.ad);
    }

    public void b(int i) {
        e.nR().ad(i, this.ad);
    }

    public void b(int i, int i2) {
        e.nR().b(i, i2);
    }

    public void b(String str) {
        e.nR().g(this.ad, str);
    }

    public void b(String str, String str2) {
        e.nR().b(this.ad, str, str2);
    }

    public boolean b(int i, KeyEvent keyEvent) {
        return e.nR().ab(i, this.ad);
    }

    public void c(int i, int i2) {
        e.nR().h(i, i2, this.ad);
    }

    public void c(String str) {
        e.nR().h(this.ad, str);
    }

    public boolean c(int i) {
        return e.nR().ae(i, this.ad);
    }

    public void d(int i) {
        e.nR().ai(this.ad, i);
    }

    public void e(int i) {
        e.nR().af(i, this.ad);
    }

    public void f(int i) {
        try {
            e.nR().ag(i, this.ad);
        } catch (Throwable th) {
        }
    }

    public void g(int i) {
        e.nR().ah(this.ad, i);
    }

    public String getTitle() {
        return e.nR().cn(this.ad);
    }

    public void goBack() {
        try {
            e.nR().cj(this.ad);
        } catch (Throwable th) {
        }
    }

    public void goForward() {
        e.nR().ck(this.ad);
    }

    public void h(int i) {
        e.nR().aj(i, this.ad);
    }

    public void i(int i) {
        e.nR().ak(i, this.ad);
    }

    public int j(int i) {
        return e.nR().al(this.ad, i);
    }

    public int m() {
        return this.ad;
    }

    public void measure(int i, int i2) {
        e.nR().f(i, i2, this.ad);
    }

    public int n() {
        return e.nR().co(this.ad);
    }

    public String o() {
        return e.nR().cp(this.ad);
    }

    public byte p() {
        return e.nR().cq(this.ad);
    }

    public int q() {
        return e.nR().cr(this.ad);
    }

    public boolean r() {
        return e.nR().cy(this.ad);
    }

    public void s() {
        e.nR().ct(this.ad);
    }

    public void stopLoading() {
        e.nR().cl(this.ad);
    }

    public int t() {
        return e.nR().cz(this.ad);
    }

    public void u() {
        e.nR().cA(this.ad);
    }

    public void v() {
        e.nR().cB(this.ad);
    }

    public boolean w() {
        return e.nR().cC(this.ad);
    }

    public int x() {
        return e.nR().cD(this.ad);
    }

    public String y() {
        return e.nR().cH(this.ad);
    }

    public boolean z() {
        return e.nR().cI(this.ad);
    }
}
