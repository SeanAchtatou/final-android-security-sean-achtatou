package com.uc.g;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import com.uc.browser.R;
import com.uc.browser.UCR;
import com.uc.e.z;
import com.uc.h.e;

class c extends z {
    private static final int aFY = 10;
    private static final int aFZ = 10;
    private int Yh = e.Pb().getColor(50);
    private int Yl = e.Pb().getColor(22);
    private Drawable Yt = e.Pb().getDrawable(UCR.drawable.aWi);
    private boolean aGa;
    private Drawable aGb = e.Pb().getDrawable(UCR.drawable.aWh);
    private Bitmap aGc;
    private Boolean aGd = false;
    final /* synthetic */ b aGe;
    private String aU;

    public c(b bVar, String str, Bitmap bitmap, Boolean bool) {
        this.aGe = bVar;
        this.aU = str;
        this.aGc = bitmap;
        this.aGd = bool;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        int kp = e.Pb().kp(R.dimen.f392set_tilte_font);
        int kp2 = e.Pb().kp(R.dimen.f393set_summary_font);
        this.pL.setTextSize((float) kp);
        this.pL.setColor(getTitleColor());
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setTextSize((float) kp2);
        paint.setColor(getTitleColor());
        if (this.aGc != null) {
            Bitmap bitmap = this.aGc;
            try {
                this.aGc = Bitmap.createScaledBitmap(bitmap, (getHeight() * 4) / 5, (getHeight() * 4) / 5, true);
                bitmap.recycle();
                canvas.drawBitmap(this.aGc, (float) 10, (float) ((getHeight() - this.aGc.getHeight()) / 2), (Paint) null);
            } catch (Throwable th) {
            }
        }
        if (!this.aGd.booleanValue() && this.aGc != null) {
            Drawable km = e.Pb().km(R.drawable.f638skin_disable);
            int height = (getHeight() * 3) / 5;
            int width = (this.aGc.getWidth() + 10) - ((height * 4) / 5);
            int height2 = ((height * 2) / 5) + ((getHeight() - height) / 2);
            km.setBounds(width, height2, ((getHeight() * 3) / 5) + width, ((getHeight() * 3) / 5) + height2);
            km.draw(canvas);
        }
        int width2 = this.aGc != null ? ((this.aGc.getWidth() * 6) / 5) + 10 : (int) (((float) (10 + 10)) + this.pL.descent());
        if (this.aGd.booleanValue()) {
            canvas.drawText(this.aU, (float) width2, ((((float) getHeight()) - this.pL.descent()) - this.pL.ascent()) / 2.0f, this.pL);
        } else {
            float b2 = b.a(paint) + b.a(this.pL);
            canvas.drawText(this.aU, (float) width2, ((((float) getHeight()) - b2) / 2.0f) - this.pL.ascent(), this.pL);
            canvas.drawText("(此版本不兼容)", (float) width2, ((b2 + ((float) getHeight())) / 2.0f) - paint.descent(), paint);
        }
        this.aGb.setBounds((getWidth() - this.aGb.getIntrinsicWidth()) - 10, (getHeight() - this.aGb.getIntrinsicHeight()) / 2, getWidth() - 10, (getHeight() + this.aGb.getIntrinsicHeight()) / 2);
        this.aGb.draw(canvas);
        if (this.aGa) {
            this.Yt.setBounds((getWidth() - this.Yt.getIntrinsicWidth()) - 10, (getHeight() - this.Yt.getIntrinsicHeight()) / 2, getWidth() - 10, (getHeight() + this.Yt.getIntrinsicHeight()) / 2);
            this.Yt.draw(canvas);
        }
    }

    public int getTitleColor() {
        return Iz() == 2 ? this.Yl : this.Yh;
    }

    public void setSelected(boolean z) {
        this.aGa = z;
    }
}
