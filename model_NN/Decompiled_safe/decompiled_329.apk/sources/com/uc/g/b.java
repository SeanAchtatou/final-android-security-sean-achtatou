package com.uc.g;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.widget.Toast;
import com.uc.browser.ActivityChooseFile;
import com.uc.browser.ActivitySkinManage;
import com.uc.browser.ModelBrowser;
import com.uc.browser.R;
import com.uc.browser.UCAlertDialog;
import com.uc.browser.UCR;
import com.uc.e.a.aa;
import com.uc.e.a.o;
import com.uc.e.a.y;
import com.uc.e.ac;
import com.uc.e.ad;
import com.uc.e.b.f;
import com.uc.e.h;
import com.uc.e.i;
import com.uc.e.s;
import com.uc.e.w;
import com.uc.e.z;
import com.uc.h.c;
import com.uc.h.d;
import com.uc.h.e;
import java.util.Vector;

public class b extends i implements aa, h, w {
    private static final int Hx = 0;
    private static final int Hy = 1;
    private static final int Hz = 2;
    s HA;
    d HB;
    y HC;
    int HD = 0;
    /* access modifiers changed from: private */
    public int HE = -1;
    int HF;
    /* access modifiers changed from: private */
    public Context cr;
    d lC;
    Vector lO;
    int uI = -1;

    public b(ad adVar, d dVar, int i, Context context) {
        super(adVar);
        this.lC = dVar;
        this.uI = i;
        this.cr = context;
        kA();
    }

    /* access modifiers changed from: private */
    public static float a(Paint paint) {
        return paint.descent() - paint.ascent();
    }

    /* access modifiers changed from: private */
    public c bB(int i) {
        return (c) this.HA.gd(i);
    }

    private void kA() {
        e Pb = e.Pb();
        this.HA = new s();
        this.HA.fm(Pb.kp(R.dimen.f219bookmark_item_height));
        this.HA.c(this);
        this.HA.a(this);
        this.HC = new y(this.HA);
        int kp = Pb.kp(R.dimen.f183controlbar_item_width_2);
        int kp2 = Pb.kp(R.dimen.f177controlbar_height);
        int kp3 = Pb.kp(R.dimen.f188controlbar_text_size);
        int kp4 = Pb.kp(R.dimen.f187controlbar_item_paddingTop);
        this.HC.ki(Pb.kp(R.dimen.f177controlbar_height));
        this.HC.bz(kp, kp2);
        this.HC.a((aa) this);
        ac A = this.HC.A("添加皮肤", 1);
        A.setTextColor(-1);
        A.aV(kp3);
        A.setPadding(0, kp4, 0, 4);
        ac A2 = this.HC.A("下载皮肤", 2);
        A2.setTextColor(-1);
        A2.aV(kp3);
        A2.setPadding(0, kp4, 0, 4);
        ac A3 = this.HC.A("返回", 0);
        A3.setTextColor(-1);
        A3.aV(kp3);
        A3.setPadding(0, kp4, 0, 4);
        this.HC.OP();
        d(this.HC);
        k();
    }

    private void kz() {
        this.lO = this.lC.GS();
        this.HA.clear();
        for (int i = 0; i < this.lO.size(); i++) {
            c cVar = (c) this.lO.get(i);
            this.HA.a(new c(this, cVar.getName(), cVar.Dk(), Boolean.valueOf(cVar.Dj())));
        }
        if (bB(this.uI) != null) {
            bB(this.uI).setSelected(true);
            return;
        }
        this.uI = 0;
        bB(0).setSelected(true);
    }

    public void a(z zVar, int i) {
        this.HE = i;
        if (this.HB != null) {
            this.HB.g();
        }
    }

    public void a(d dVar) {
        this.HB = dVar;
    }

    public void b(z zVar, int i) {
        this.HD = i;
        if (((c) this.lO.get(this.HD)).Dj()) {
            o oVar = new o("使用皮肤", "使用所选皮肤吗？");
            oVar.a(new f(this));
            a(oVar);
        }
    }

    public void bA(int i) {
        switch (i) {
            case 0:
                ((ActivitySkinManage) this.cr).finish();
                return;
            case 1:
                Intent intent = new Intent(this.cr, ActivityChooseFile.class);
                intent.putExtra(ActivityChooseFile.ux, ".uct");
                intent.putExtra(ActivityChooseFile.uA, (int) R.string.f980skin_file_choose_title);
                ((Activity) this.cr).startActivityForResult(intent, 0);
                return;
            case 2:
                ModelBrowser.gD().a(11, com.uc.a.e.nR().qA());
                ((ActivitySkinManage) this.cr).finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public boolean bz(int i) {
        if (!this.lC.hy(i)) {
            Toast.makeText(this.cr, "不能使用该皮肤", 1).show();
            return false;
        }
        Toast.makeText(this.cr, "皮肤应用成功", 1).show();
        return true;
    }

    public void k() {
        e Pb = e.Pb();
        this.HA.s(new f(Pb.getColor(54), Pb.getColor(55), 0));
        this.HA.f(new Drawable[]{null, Pb.getDrawable(UCR.drawable.aWH), Pb.getDrawable(UCR.drawable.aWH)});
        this.HC.M(Pb.getDrawable(UCR.drawable.aWt));
        this.HC.n(Pb.getDrawable(UCR.drawable.aXp));
        this.HC.bA(Pb.getColor(10), Pb.getColor(11));
        kz();
    }

    public int kB() {
        return this.HE;
    }

    public d kC() {
        return this.HB;
    }

    public void kD() {
        if (this.lC.hy(this.HE)) {
            bB(this.uI).setSelected(false);
            bB(this.HE).setSelected(true);
            this.uI = this.HE;
            cJ();
            return;
        }
        Toast.makeText(this.cr, "不能使用该皮肤", 1).show();
    }

    public void kE() {
        UCAlertDialog.Builder builder = new UCAlertDialog.Builder(this.cr);
        builder.L("皮肤卸载");
        this.HF = Integer.parseInt(com.uc.a.e.nR().nY().bw(com.uc.a.h.afa));
        if (this.HF == this.HE) {
            builder.M("你卸载正在使用的皮肤,系统将恢复为默认皮肤");
        } else {
            builder.M("确认卸载所选皮肤吗？");
        }
        builder.a((int) R.string.ok, new e(this));
        builder.c((int) R.string.cancel, (DialogInterface.OnClickListener) null);
        builder.fh().show();
    }

    public Vector ky() {
        return this.lO;
    }
}
