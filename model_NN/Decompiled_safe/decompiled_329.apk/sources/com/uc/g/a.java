package com.uc.g;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import com.uc.a.e;
import com.uc.d.c;
import com.uc.d.d;
import com.uc.d.f;
import com.uc.d.g;
import com.uc.d.h;
import com.uc.e.ad;

public class a extends View implements d, ad, d, com.uc.h.a {
    private static View lD;
    private b lB;
    private com.uc.h.d lC;
    private c lE;
    private f lF;

    public a(Context context, AttributeSet attributeSet, com.uc.h.d dVar) {
        super(context, attributeSet);
        this.lC = dVar;
        a();
    }

    public a(Context context, com.uc.h.d dVar) {
        this(context, null, dVar);
    }

    private void a() {
        setFocusable(true);
        setFocusableInTouchMode(true);
        lD = this;
        cK();
    }

    public static Drawable getDrawable(int i) {
        return lD.getContext().getResources().getDrawable(i);
    }

    public boolean a(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case h.cdT:
                this.lB.kD();
                return true;
            case h.cdU:
                this.lB.kE();
                return true;
            default:
                return true;
        }
    }

    public void cJ() {
        invalidate();
    }

    public void cK() {
        String string = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(com.uc.h.d.bxp, null);
        int eP = string != null ? this.lC.eP(string) : Integer.parseInt(e.nR().nY().bw(com.uc.a.h.afa));
        if (eP >= this.lC.GS().size()) {
            eP = 0;
        }
        this.lB = new b(this, this.lC, eP, getContext());
        this.lB.a(this);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return this.lB.dispatchKeyEvent(keyEvent);
    }

    public void g() {
        if (this.lE == null) {
            this.lE = new c(getContext());
        }
        this.lE.clear();
        if (this.lF == null) {
            this.lF = new f(getContext());
            this.lF.a();
        }
        this.lF.a(this.lE);
        this.lE.a(this.lF);
        this.lF.a(this);
        this.lE.cV();
        this.lF.show();
        g.a(getContext(), h.cdV, this.lE);
        if (this.lB.kB() == 0) {
            this.lE.findItem(h.cdU).setVisible(false);
        }
        if (!((com.uc.h.c) this.lB.ky().get(this.lB.kB())).Dj()) {
            this.lE.findItem(h.cdT).setVisible(false);
        }
    }

    public void k() {
        this.lB.k();
    }

    public void onDraw(Canvas canvas) {
        this.lB.dispatchDraw(canvas);
    }

    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (z) {
            this.lB.layout(i, i2, i3, i4);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.lB.dispatchTouchEvent(motionEvent);
    }
}
