package org.anddev.andengine.util.modifier;

import org.anddev.andengine.util.SmartList;
import org.anddev.andengine.util.modifier.IModifier;

public abstract class BaseModifier<T> implements IModifier<T> {
    protected boolean mFinished;
    private final SmartList<IModifier.IModifierListener<T>> mModifierListeners = new SmartList<>(2);
    private boolean mRemoveWhenFinished = true;

    public abstract IModifier<T> clone() throws IModifier.CloneNotSupportedException;

    public BaseModifier() {
    }

    public BaseModifier(IModifier.IModifierListener<T> pModifierListener) {
        addModifierListener(pModifierListener);
    }

    public boolean isFinished() {
        return this.mFinished;
    }

    public final boolean isRemoveWhenFinished() {
        return this.mRemoveWhenFinished;
    }

    public final void setRemoveWhenFinished(boolean pRemoveWhenFinished) {
        this.mRemoveWhenFinished = pRemoveWhenFinished;
    }

    public void addModifierListener(IModifier.IModifierListener<T> pModifierListener) {
        if (pModifierListener != null) {
            this.mModifierListeners.add(pModifierListener);
        }
    }

    public boolean removeModifierListener(IModifier.IModifierListener<T> pModifierListener) {
        if (pModifierListener == null) {
            return false;
        }
        return this.mModifierListeners.remove(pModifierListener);
    }

    /* access modifiers changed from: protected */
    public void onModifierStarted(T pItem) {
        SmartList<IModifier.IModifierListener<T>> modifierListeners = this.mModifierListeners;
        for (int i = modifierListeners.size() - 1; i >= 0; i--) {
            ((IModifier.IModifierListener) modifierListeners.get(i)).onModifierStarted(this, pItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onModifierFinished(T pItem) {
        SmartList<IModifier.IModifierListener<T>> modifierListeners = this.mModifierListeners;
        for (int i = modifierListeners.size() - 1; i >= 0; i--) {
            ((IModifier.IModifierListener) modifierListeners.get(i)).onModifierFinished(this, pItem);
        }
    }
}
