package org.anddev.andengine.util.sort;

import java.util.Comparator;
import java.util.List;

public class InsertionSorter<T> extends Sorter<T> {
    public void sort(Object[] pArray, int pStart, int pEnd, Comparator comparator) {
        int j;
        for (int i = pStart + 1; i < pEnd; i++) {
            Object obj = pArray[i];
            Object obj2 = pArray[i - 1];
            if (comparator.compare(obj, obj2) < 0) {
                int j2 = i;
                while (true) {
                    j = j2 - 1;
                    pArray[j2] = obj2;
                    if (j <= pStart) {
                        break;
                    }
                    obj2 = pArray[j - 1];
                    if (comparator.compare(obj, obj2) >= 0) {
                        break;
                    }
                    j2 = j;
                }
                pArray[j] = obj;
            }
        }
    }

    public void sort(List list, int pStart, int pEnd, Comparator comparator) {
        int j;
        for (int i = pStart + 1; i < pEnd; i++) {
            T current = list.get(i);
            T prev = list.get(i - 1);
            if (comparator.compare(current, prev) < 0) {
                int j2 = i;
                while (true) {
                    j = j2 - 1;
                    list.set(j2, prev);
                    if (j <= pStart) {
                        break;
                    }
                    prev = list.get(j - 1);
                    if (comparator.compare(current, prev) >= 0) {
                        break;
                    }
                    j2 = j;
                }
                list.set(j, current);
            }
        }
    }
}
