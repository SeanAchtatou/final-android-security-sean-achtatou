package org.anddev.andengine.util;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Scanner;

public class StreamUtils {
    public static final int IO_BUFFER_SIZE = 8192;

    public static final String readFully(InputStream pInputStream) throws IOException {
        StringBuilder sb = new StringBuilder();
        Scanner sc = new Scanner(pInputStream);
        while (sc.hasNextLine()) {
            sb.append(sc.nextLine());
        }
        return sb.toString();
    }

    public static byte[] streamToBytes(InputStream pInputStream) throws IOException {
        return streamToBytes(pInputStream, -1);
    }

    public static byte[] streamToBytes(InputStream pInputStream, int pReadLimit) throws IOException {
        int i;
        if (pReadLimit == -1) {
            i = 8192;
        } else {
            i = pReadLimit;
        }
        ByteArrayOutputStream os = new ByteArrayOutputStream(i);
        copy(pInputStream, os, (long) pReadLimit);
        return os.toByteArray();
    }

    public static void copy(InputStream pInputStream, OutputStream pOutputStream) throws IOException {
        copy(pInputStream, pOutputStream, -1);
    }

    public static boolean copyAndClose(InputStream pInputStream, OutputStream pOutputStream) {
        try {
            copy(pInputStream, pOutputStream, -1);
            close(pInputStream);
            close(pOutputStream);
            return true;
        } catch (IOException e) {
            close(pInputStream);
            close(pOutputStream);
            return false;
        } catch (Throwable th) {
            close(pInputStream);
            close(pOutputStream);
            throw th;
        }
    }

    public static void copy(InputStream pInputStream, OutputStream pOutputStream, long pByteLimit) throws IOException {
        if (pByteLimit >= 0) {
            byte[] b = new byte[8192];
            int bufferReadLimit = Math.min((int) pByteLimit, 8192);
            long pBytesLeftToRead = pByteLimit;
            while (true) {
                int read = pInputStream.read(b, 0, bufferReadLimit);
                if (read != -1) {
                    if (pBytesLeftToRead <= ((long) read)) {
                        pOutputStream.write(b, 0, (int) pBytesLeftToRead);
                        break;
                    } else {
                        pOutputStream.write(b, 0, read);
                        pBytesLeftToRead -= (long) read;
                    }
                } else {
                    break;
                }
            }
        } else {
            byte[] b2 = new byte[8192];
            while (true) {
                int read2 = pInputStream.read(b2);
                if (read2 == -1) {
                    break;
                }
                pOutputStream.write(b2, 0, read2);
            }
        }
        pOutputStream.flush();
    }

    public static void close(Closeable pCloseable) {
        if (pCloseable != null) {
            try {
                pCloseable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void flushCloseStream(OutputStream pOutputStream) {
        if (pOutputStream != null) {
            try {
                pOutputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                close(pOutputStream);
            }
        }
    }

    public static void flushCloseWriter(Writer pWriter) {
        if (pWriter != null) {
            try {
                pWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                close(pWriter);
            }
        }
    }
}
