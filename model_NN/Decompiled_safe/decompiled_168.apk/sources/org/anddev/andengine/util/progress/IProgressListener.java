package org.anddev.andengine.util.progress;

public interface IProgressListener {
    void onProgressChanged(int i);
}
