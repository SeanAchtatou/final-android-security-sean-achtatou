package org.anddev.andengine.engine.options.resolutionpolicy;

import android.view.View;
import org.anddev.andengine.opengl.view.RenderSurfaceView;

public class FillResolutionPolicy extends BaseResolutionPolicy {
    public void onMeasure(RenderSurfaceView pRenderSurfaceView, int pWidthMeasureSpec, int pHeightMeasureSpec) {
        BaseResolutionPolicy.throwOnNotMeasureSpecEXACTLY(pWidthMeasureSpec, pHeightMeasureSpec);
        pRenderSurfaceView.setMeasuredDimensionProxy(View.MeasureSpec.getSize(pWidthMeasureSpec), View.MeasureSpec.getSize(pHeightMeasureSpec));
    }
}
