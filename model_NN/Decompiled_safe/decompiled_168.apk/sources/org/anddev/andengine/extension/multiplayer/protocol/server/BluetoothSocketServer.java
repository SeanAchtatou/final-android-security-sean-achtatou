package org.anddev.andengine.extension.multiplayer.protocol.server;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import java.io.IOException;
import java.util.UUID;
import org.anddev.andengine.extension.multiplayer.protocol.exception.BluetoothException;
import org.anddev.andengine.extension.multiplayer.protocol.server.Server;
import org.anddev.andengine.extension.multiplayer.protocol.server.connector.BluetoothSocketConnectionClientConnector;
import org.anddev.andengine.extension.multiplayer.protocol.server.connector.ClientConnector;
import org.anddev.andengine.extension.multiplayer.protocol.shared.BluetoothSocketConnection;
import org.anddev.andengine.extension.multiplayer.protocol.util.Bluetooth;
import org.anddev.andengine.util.Debug;

public abstract class BluetoothSocketServer<CC extends ClientConnector<BluetoothSocketConnection>> extends Server<BluetoothSocketConnection, CC> {
    private BluetoothServerSocket mBluetoothServerSocket;
    private final String mUUID;

    /* access modifiers changed from: protected */
    public abstract CC newClientConnector(BluetoothSocketConnection bluetoothSocketConnection) throws IOException;

    public BluetoothSocketServer(String pUUID) throws BluetoothException {
        this(pUUID, new BluetoothSocketConnectionClientConnector.DefaultBluetoothSocketClientConnectorListener());
    }

    public BluetoothSocketServer(String pUUID, ClientConnector.IClientConnectorListener<BluetoothSocketConnection> pClientConnectorListener) throws BluetoothException {
        this(pUUID, pClientConnectorListener, new IBluetoothSocketServerListener.DefaultBluetoothSocketServerListener());
    }

    public BluetoothSocketServer(String pUUID, IBluetoothSocketServerListener<CC> pBluetoothSocketServerListener) throws BluetoothException {
        this(pUUID, new BluetoothSocketConnectionClientConnector.DefaultBluetoothSocketClientConnectorListener(), pBluetoothSocketServerListener);
    }

    public BluetoothSocketServer(String pUUID, ClientConnector.IClientConnectorListener<BluetoothSocketConnection> pClientConnectorListener, IBluetoothSocketServerListener<CC> pBluetoothSocketServerListener) throws BluetoothException {
        super(pClientConnectorListener, pBluetoothSocketServerListener);
        this.mUUID = pUUID;
        if (!Bluetooth.isSupportedByAndroidVersion()) {
            throw new BluetoothException();
        }
    }

    public String getUUID() {
        return this.mUUID;
    }

    public IBluetoothSocketServerListener<CC> getServerListener() {
        return (IBluetoothSocketServerListener) super.getServerListener();
    }

    /* access modifiers changed from: protected */
    public void onStart() throws IOException {
        this.mBluetoothServerSocket = BluetoothAdapter.getDefaultAdapter().listenUsingRfcommWithServiceRecord(getClass().getName(), UUID.fromString(this.mUUID));
    }

    /* access modifiers changed from: protected */
    public CC acceptClientConnector() throws IOException {
        try {
            return newClientConnector(new BluetoothSocketConnection(this.mBluetoothServerSocket.accept()));
        } catch (BluetoothException e) {
            return null;
        }
    }

    public void onTerminate() {
        try {
            this.mBluetoothServerSocket.close();
        } catch (IOException e) {
            Debug.e(e);
        }
        getServerListener().onTerminated(this);
    }

    /* access modifiers changed from: protected */
    public void onException(Throwable pThrowable) {
        getServerListener().onException(this, pThrowable);
    }

    public interface IBluetoothSocketServerListener<CC extends ClientConnector<BluetoothSocketConnection>> extends Server.IServerListener<BluetoothSocketServer<CC>> {
        void onException(BluetoothSocketServer<CC> bluetoothSocketServer, Throwable th);

        void onStarted(BluetoothSocketServer<CC> bluetoothSocketServer);

        void onTerminated(BluetoothSocketServer<CC> bluetoothSocketServer);

        public static class DefaultBluetoothSocketServerListener<CC extends ClientConnector<BluetoothSocketConnection>> implements IBluetoothSocketServerListener<CC> {
            public void onStarted(BluetoothSocketServer<CC> pBluetoothSocketServer) {
                Debug.d("Server started on port: " + pBluetoothSocketServer.getUUID());
            }

            public void onTerminated(BluetoothSocketServer<CC> pBluetoothSocketServer) {
                Debug.d("Server terminated on port: " + pBluetoothSocketServer.getUUID());
            }

            public void onException(BluetoothSocketServer<CC> bluetoothSocketServer, Throwable pThrowable) {
                Debug.e(pThrowable);
            }
        }
    }
}
