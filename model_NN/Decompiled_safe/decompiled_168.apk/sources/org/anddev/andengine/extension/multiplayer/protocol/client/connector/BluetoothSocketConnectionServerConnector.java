package org.anddev.andengine.extension.multiplayer.protocol.client.connector;

import java.io.IOException;
import org.anddev.andengine.extension.multiplayer.protocol.client.IServerMessageReader;
import org.anddev.andengine.extension.multiplayer.protocol.client.connector.ServerConnector;
import org.anddev.andengine.extension.multiplayer.protocol.shared.BluetoothSocketConnection;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connector;
import org.anddev.andengine.util.Debug;

public class BluetoothSocketConnectionServerConnector extends ServerConnector<BluetoothSocketConnection> {

    public interface IBluetoothSocketConnectionServerConnectorListener extends ServerConnector.IServerConnectorListener<BluetoothSocketConnection> {
    }

    public BluetoothSocketConnectionServerConnector(BluetoothSocketConnection pBluetoothSocketConnection, IBluetoothSocketConnectionServerConnectorListener pBlutetoothSocketConnectionServerConnectorListener) throws IOException {
        super(pBluetoothSocketConnection, pBlutetoothSocketConnectionServerConnectorListener);
    }

    public BluetoothSocketConnectionServerConnector(BluetoothSocketConnection pBluetoothSocketConnection, IServerMessageReader<BluetoothSocketConnection> pServerMessageReader, IBluetoothSocketConnectionServerConnectorListener pBlutetoothSocketConnectionServerConnectorListener) throws IOException {
        super(pBluetoothSocketConnection, pServerMessageReader, pBlutetoothSocketConnectionServerConnectorListener);
    }

    public static class DefaultBluetoothConnectionSocketServerConnectorListener implements IBluetoothSocketConnectionServerConnectorListener {
        public /* bridge */ /* synthetic */ void onStarted(Connector connector) {
            onStarted((ServerConnector<BluetoothSocketConnection>) ((ServerConnector) connector));
        }

        public /* bridge */ /* synthetic */ void onTerminated(Connector connector) {
            onTerminated((ServerConnector<BluetoothSocketConnection>) ((ServerConnector) connector));
        }

        public void onStarted(ServerConnector<BluetoothSocketConnection> pServerConnector) {
            Debug.d("Accepted Server-Connection from: '" + pServerConnector.getConnection().getBluetoothSocket().getRemoteDevice().getAddress());
        }

        public void onTerminated(ServerConnector<BluetoothSocketConnection> pServerConnector) {
            Debug.d("Closed Server-Connection from: '" + pServerConnector.getConnection().getBluetoothSocket().getRemoteDevice().getAddress());
        }
    }
}
