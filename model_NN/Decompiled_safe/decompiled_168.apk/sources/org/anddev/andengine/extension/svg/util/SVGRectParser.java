package org.anddev.andengine.extension.svg.util;

import android.graphics.Canvas;
import android.graphics.RectF;
import org.anddev.andengine.extension.svg.adt.SVGPaint;
import org.anddev.andengine.extension.svg.adt.SVGProperties;
import org.anddev.andengine.extension.svg.util.constants.ISVGConstants;

public class SVGRectParser implements ISVGConstants {
    /* JADX INFO: Multiple debug info for r2v8 float: [D('ry' float), D('rXSpecified' boolean)] */
    /* JADX INFO: Multiple debug info for r0v12 boolean: [D('rx' float), D('fill' boolean)] */
    /* JADX INFO: Multiple debug info for r9v1 boolean: [D('pSVGProperties' org.anddev.andengine.extension.svg.adt.SVGProperties), D('stroke' boolean)] */
    public static void parse(SVGProperties pSVGProperties, Canvas pCanvas, SVGPaint pSVGPaint, RectF pRect) {
        float ry;
        float ry2;
        float x = pSVGProperties.getFloatAttribute("x", 0.0f);
        float y = pSVGProperties.getFloatAttribute("y", 0.0f);
        float width = pSVGProperties.getFloatAttribute("width", 0.0f);
        float height = pSVGProperties.getFloatAttribute("height", 0.0f);
        pRect.set(x, y, x + width, y + height);
        Float rX = pSVGProperties.getFloatAttribute(ISVGConstants.ATTRIBUTE_RADIUS_X);
        Float rY = pSVGProperties.getFloatAttribute(ISVGConstants.ATTRIBUTE_RADIUS_Y);
        boolean rXSpecified = rX != null && rX.floatValue() >= 0.0f;
        boolean rYSpecified = rY != null && rY.floatValue() >= 0.0f;
        boolean rounded = rXSpecified || rYSpecified;
        if (rXSpecified && rYSpecified) {
            float rx = Math.min(rX.floatValue(), 0.5f * width);
            ry = Math.min(rY.floatValue(), 0.5f * height);
            ry2 = rx;
        } else if (rXSpecified) {
            float rx2 = Math.min(rX.floatValue(), 0.5f * width);
            ry = rx2;
            ry2 = rx2;
        } else if (rYSpecified) {
            float ry3 = Math.min(rY.floatValue(), 0.5f * height);
            ry = ry3;
            ry2 = ry3;
        } else {
            ry = 0.0f;
            ry2 = 0.0f;
        }
        boolean fill = pSVGPaint.setFill(pSVGProperties);
        if (fill) {
            if (rounded) {
                pCanvas.drawRoundRect(pRect, ry2, ry, pSVGPaint.getPaint());
            } else {
                pCanvas.drawRect(pRect, pSVGPaint.getPaint());
            }
        }
        boolean stroke = pSVGPaint.setStroke(pSVGProperties);
        if (stroke) {
            if (rounded) {
                pCanvas.drawRoundRect(pRect, ry2, ry, pSVGPaint.getPaint());
            } else {
                pCanvas.drawRect(pRect, pSVGPaint.getPaint());
            }
        }
        if (fill || stroke) {
            pSVGPaint.ensureComputedBoundsInclude(x, y, width, height);
        }
    }
}
