package org.anddev.andengine.extension.svg.adt.filter.element;

import android.graphics.Paint;

public interface ISVGFilterElement {
    void apply(Paint paint);
}
