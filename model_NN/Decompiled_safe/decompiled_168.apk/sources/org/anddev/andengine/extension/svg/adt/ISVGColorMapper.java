package org.anddev.andengine.extension.svg.adt;

public interface ISVGColorMapper {
    Integer mapColor(Integer num);
}
