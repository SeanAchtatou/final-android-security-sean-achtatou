package org.anddev.andengine.extension.multiplayer.protocol.server;

import java.io.DataInputStream;
import java.io.IOException;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.IMessage;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.IClientMessage;
import org.anddev.andengine.extension.multiplayer.protocol.server.connector.ClientConnector;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connection;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connector;
import org.anddev.andengine.extension.multiplayer.protocol.shared.IMessageHandler;
import org.anddev.andengine.extension.multiplayer.protocol.shared.IMessageReader;
import org.anddev.andengine.extension.multiplayer.protocol.shared.MessageReader;

public interface IClientMessageReader<C extends Connection> extends IMessageReader<C, ClientConnector<C>, IClientMessage> {

    public static class ClientMessageReader<C extends Connection> extends MessageReader<C, ClientConnector<C>, IClientMessage> implements IClientMessageReader<C> {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.anddev.andengine.extension.multiplayer.protocol.shared.MessageReader.handleMessage(org.anddev.andengine.extension.multiplayer.protocol.shared.Connector, org.anddev.andengine.extension.multiplayer.protocol.adt.message.IMessage):void
         arg types: [org.anddev.andengine.extension.multiplayer.protocol.server.connector.ClientConnector, org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.IClientMessage]
         candidates:
          org.anddev.andengine.extension.multiplayer.protocol.server.IClientMessageReader.ClientMessageReader.handleMessage(org.anddev.andengine.extension.multiplayer.protocol.server.connector.ClientConnector, org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.IClientMessage):void
          org.anddev.andengine.extension.multiplayer.protocol.server.IClientMessageReader.handleMessage(org.anddev.andengine.extension.multiplayer.protocol.server.connector.ClientConnector, org.anddev.andengine.extension.multiplayer.protocol.adt.message.client.IClientMessage):void
          org.anddev.andengine.extension.multiplayer.protocol.shared.MessageReader.handleMessage(org.anddev.andengine.extension.multiplayer.protocol.shared.Connector, org.anddev.andengine.extension.multiplayer.protocol.adt.message.IMessage):void */
        public /* bridge */ /* synthetic */ void handleMessage(ClientConnector clientConnector, IClientMessage iClientMessage) throws IOException {
            handleMessage((Connector) clientConnector, (IMessage) iClientMessage);
        }

        public /* bridge */ /* synthetic */ IClientMessage readMessage(DataInputStream dataInputStream) throws IOException {
            return (IClientMessage) readMessage(dataInputStream);
        }

        public /* bridge */ /* synthetic */ void recycleMessage(IClientMessage iClientMessage) {
            recycleMessage((IMessage) iClientMessage);
        }
    }

    void handleMessage(ClientConnector clientConnector, IClientMessage iClientMessage) throws IOException;

    IClientMessage readMessage(DataInputStream dataInputStream) throws IOException;

    void recycleMessage(IClientMessage iClientMessage);

    void registerMessage(short s, Class<? extends IClientMessage> cls);

    void registerMessage(short s, Class<? extends IClientMessage> cls, IMessageHandler<C, ClientConnector<C>, IClientMessage> iMessageHandler);

    void registerMessageHandler(short s, IMessageHandler<C, ClientConnector<C>, IClientMessage> iMessageHandler);
}
