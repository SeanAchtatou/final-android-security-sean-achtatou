package org.anddev.andengine.extension.multiplayer.protocol.server.connector;

import java.io.IOException;
import org.anddev.andengine.extension.multiplayer.protocol.exception.BluetoothException;
import org.anddev.andengine.extension.multiplayer.protocol.server.IClientMessageReader;
import org.anddev.andengine.extension.multiplayer.protocol.server.connector.ClientConnector;
import org.anddev.andengine.extension.multiplayer.protocol.shared.BluetoothSocketConnection;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connector;
import org.anddev.andengine.extension.multiplayer.protocol.util.Bluetooth;
import org.anddev.andengine.util.Debug;

public class BluetoothSocketConnectionClientConnector extends ClientConnector<BluetoothSocketConnection> {

    public interface IBluetoothSocketConnectionClientConnectorListener extends ClientConnector.IClientConnectorListener<BluetoothSocketConnection> {
    }

    public BluetoothSocketConnectionClientConnector(BluetoothSocketConnection pBluetoothSocketConnection) throws IOException, BluetoothException {
        super(pBluetoothSocketConnection);
        if (!Bluetooth.isSupportedByAndroidVersion()) {
            throw new BluetoothException();
        }
    }

    public BluetoothSocketConnectionClientConnector(BluetoothSocketConnection pBluetoothSocketConnection, IClientMessageReader<BluetoothSocketConnection> pClientMessageReader) throws IOException, BluetoothException {
        super(pBluetoothSocketConnection, pClientMessageReader);
        if (!Bluetooth.isSupportedByAndroidVersion()) {
            throw new BluetoothException();
        }
    }

    public static class DefaultBluetoothSocketClientConnectorListener implements IBluetoothSocketConnectionClientConnectorListener {
        public /* bridge */ /* synthetic */ void onStarted(Connector connector) {
            onStarted((ClientConnector<BluetoothSocketConnection>) ((ClientConnector) connector));
        }

        public /* bridge */ /* synthetic */ void onTerminated(Connector connector) {
            onTerminated((ClientConnector<BluetoothSocketConnection>) ((ClientConnector) connector));
        }

        public void onStarted(ClientConnector<BluetoothSocketConnection> pClientConnector) {
            Debug.d("Accepted Client-Connection from: '" + pClientConnector.getConnection().getBluetoothSocket().getRemoteDevice().getAddress());
        }

        public void onTerminated(ClientConnector<BluetoothSocketConnection> pClientConnector) {
            Debug.d("Closed Client-Connection from: '" + pClientConnector.getConnection().getBluetoothSocket().getRemoteDevice().getAddress());
        }
    }
}
