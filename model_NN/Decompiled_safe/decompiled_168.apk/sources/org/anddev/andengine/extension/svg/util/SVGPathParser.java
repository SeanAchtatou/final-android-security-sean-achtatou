package org.anddev.andengine.extension.svg.util;

import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.FloatMath;
import java.util.LinkedList;
import java.util.Queue;
import org.anddev.andengine.extension.svg.adt.SVGPaint;
import org.anddev.andengine.extension.svg.adt.SVGProperties;
import org.anddev.andengine.extension.svg.util.constants.ISVGConstants;
import org.anddev.andengine.util.MathUtils;

public class SVGPathParser implements ISVGConstants {
    private final RectF mArcRect = new RectF();
    private Character mCommand = null;
    private final Queue<Float> mCommandParameters = new LinkedList();
    private int mCommandStart = 0;
    /* access modifiers changed from: private */
    public char mCurrentChar;
    private float mLastCubicBezierX2;
    private float mLastCubicBezierY2;
    private float mLastQuadraticBezierX2;
    private float mLastQuadraticBezierY2;
    private float mLastX;
    private float mLastY;
    /* access modifiers changed from: private */
    public int mLength;
    private Path mPath;
    private final PathParserHelper mPathParserHelper = new PathParserHelper();
    /* access modifiers changed from: private */
    public int mPosition;
    /* access modifiers changed from: private */
    public String mString;
    private float mSubPathStartX;
    private float mSubPathStartY;

    public void parse(SVGProperties pSVGProperties, Canvas pCanvas, SVGPaint pSVGPaint) {
        Path path = parse(pSVGProperties);
        boolean fill = pSVGPaint.setFill(pSVGProperties);
        if (fill) {
            pCanvas.drawPath(path, pSVGPaint.getPaint());
        }
        boolean stroke = pSVGPaint.setStroke(pSVGProperties);
        if (stroke) {
            pCanvas.drawPath(path, pSVGPaint.getPaint());
        }
        if (fill || stroke) {
            pSVGPaint.ensureComputedBoundsInclude(path);
        }
    }

    private Path parse(SVGProperties pSVGProperties) {
        String pathString = pSVGProperties.getStringProperty(ISVGConstants.ATTRIBUTE_PATHDATA);
        if (pathString == null) {
            return null;
        }
        this.mString = pathString.trim();
        this.mLastX = 0.0f;
        this.mLastY = 0.0f;
        this.mLastCubicBezierX2 = 0.0f;
        this.mLastCubicBezierY2 = 0.0f;
        this.mCommand = null;
        this.mCommandParameters.clear();
        this.mPath = new Path();
        if (this.mString.length() == 0) {
            return this.mPath;
        }
        String fillrule = pSVGProperties.getStringProperty(ISVGConstants.ATTRIBUTE_FILLRULE);
        if (fillrule != null) {
            if (ISVGConstants.ATTRIBUTE_FILLRULE_VALUE_EVENODD.equals(fillrule)) {
                this.mPath.setFillType(Path.FillType.EVEN_ODD);
            } else {
                this.mPath.setFillType(Path.FillType.WINDING);
            }
        }
        this.mCurrentChar = this.mString.charAt(0);
        this.mPosition = 0;
        this.mLength = this.mString.length();
        while (this.mPosition < this.mLength) {
            try {
                this.mPathParserHelper.skipWhitespace();
                if (!Character.isLetter(this.mCurrentChar) || this.mCurrentChar == 'e' || this.mCurrentChar == 'E') {
                    this.mCommandParameters.add(Float.valueOf(this.mPathParserHelper.nextFloat()));
                } else {
                    processCommand();
                    this.mCommand = Character.valueOf(this.mCurrentChar);
                    this.mCommandStart = this.mPosition;
                    this.mPathParserHelper.advance();
                }
            } catch (Throwable th) {
                throw new IllegalArgumentException("Error parsing: '" + this.mString.substring(this.mCommandStart, this.mPosition) + "'. Command: '" + this.mCommand + "'. Parameters: '" + this.mCommandParameters.size() + "'.", th);
            }
        }
        processCommand();
        return this.mPath;
    }

    private void processCommand() {
        if (this.mCommand != null) {
            generatePathElement();
            this.mCommandParameters.clear();
        }
    }

    private void generatePathElement() {
        boolean wasCubicBezierCurve = false;
        boolean wasQuadraticBezierCurve = false;
        switch (this.mCommand.charValue()) {
            case 'A':
                generateArc(true);
                break;
            case 'C':
                generateCubicBezierCurve(true);
                wasCubicBezierCurve = true;
                break;
            case 'H':
                generateHorizontalLine(true);
                break;
            case 'L':
                generateLine(true);
                break;
            case 'M':
                generateMove(true);
                break;
            case 'Q':
                generateQuadraticBezierCurve(true);
                wasQuadraticBezierCurve = true;
                break;
            case 'S':
                generateSmoothCubicBezierCurve(true);
                wasCubicBezierCurve = true;
                break;
            case 'T':
                generateSmoothQuadraticBezierCurve(true);
                wasQuadraticBezierCurve = true;
                break;
            case 'V':
                generateVerticalLine(true);
                break;
            case 'Z':
            case 'z':
                generateClose();
                break;
            case 'a':
                generateArc(false);
                break;
            case 'c':
                generateCubicBezierCurve(false);
                wasCubicBezierCurve = true;
                break;
            case 'h':
                generateHorizontalLine(false);
                break;
            case 'l':
                generateLine(false);
                break;
            case 'm':
                generateMove(false);
                break;
            case 'q':
                generateQuadraticBezierCurve(false);
                wasQuadraticBezierCurve = true;
                break;
            case 's':
                generateSmoothCubicBezierCurve(false);
                wasCubicBezierCurve = true;
                break;
            case 't':
                generateSmoothQuadraticBezierCurve(false);
                wasQuadraticBezierCurve = true;
                break;
            case 'v':
                generateVerticalLine(false);
                break;
            default:
                throw new RuntimeException("Unexpected SVG command: " + this.mCommand);
        }
        if (!wasCubicBezierCurve) {
            this.mLastCubicBezierX2 = this.mLastX;
            this.mLastCubicBezierY2 = this.mLastY;
        }
        if (!wasQuadraticBezierCurve) {
            this.mLastQuadraticBezierX2 = this.mLastX;
            this.mLastQuadraticBezierY2 = this.mLastY;
        }
    }

    private void assertParameterCountMinimum(int pParameterCount) {
        if (this.mCommandParameters.size() < pParameterCount) {
            throw new RuntimeException("Incorrect parameter count: '" + this.mCommandParameters.size() + "'. Expected at least: '" + pParameterCount + "'.");
        }
    }

    private void assertParameterCount(int pParameterCount) {
        if (this.mCommandParameters.size() != pParameterCount) {
            throw new RuntimeException("Incorrect parameter count: '" + this.mCommandParameters.size() + "'. Expected: '" + pParameterCount + "'.");
        }
    }

    private void generateMove(boolean pAbsolute) {
        assertParameterCountMinimum(2);
        float x = this.mCommandParameters.poll().floatValue();
        float y = this.mCommandParameters.poll().floatValue();
        if (pAbsolute) {
            this.mPath.moveTo(x, y);
            this.mLastX = x;
            this.mLastY = y;
        } else {
            this.mPath.rMoveTo(x, y);
            this.mLastX += x;
            this.mLastY += y;
        }
        this.mSubPathStartX = this.mLastX;
        this.mSubPathStartY = this.mLastY;
        if (this.mCommandParameters.size() >= 2) {
            generateLine(pAbsolute);
        }
    }

    private void generateLine(boolean pAbsolute) {
        assertParameterCountMinimum(2);
        if (pAbsolute) {
            while (this.mCommandParameters.size() >= 2) {
                float x = this.mCommandParameters.poll().floatValue();
                float y = this.mCommandParameters.poll().floatValue();
                this.mPath.lineTo(x, y);
                this.mLastX = x;
                this.mLastY = y;
            }
            return;
        }
        while (this.mCommandParameters.size() >= 2) {
            float x2 = this.mCommandParameters.poll().floatValue();
            float y2 = this.mCommandParameters.poll().floatValue();
            this.mPath.rLineTo(x2, y2);
            this.mLastX += x2;
            this.mLastY += y2;
        }
    }

    private void generateHorizontalLine(boolean pAbsolute) {
        assertParameterCountMinimum(1);
        if (pAbsolute) {
            while (this.mCommandParameters.size() >= 1) {
                float x = this.mCommandParameters.poll().floatValue();
                this.mPath.lineTo(x, this.mLastY);
                this.mLastX = x;
            }
            return;
        }
        while (this.mCommandParameters.size() >= 1) {
            float x2 = this.mCommandParameters.poll().floatValue();
            this.mPath.rLineTo(x2, 0.0f);
            this.mLastX += x2;
        }
    }

    private void generateVerticalLine(boolean pAbsolute) {
        assertParameterCountMinimum(1);
        if (pAbsolute) {
            while (this.mCommandParameters.size() >= 1) {
                float y = this.mCommandParameters.poll().floatValue();
                this.mPath.lineTo(this.mLastX, y);
                this.mLastY = y;
            }
            return;
        }
        while (this.mCommandParameters.size() >= 1) {
            float y2 = this.mCommandParameters.poll().floatValue();
            this.mPath.rLineTo(0.0f, y2);
            this.mLastY += y2;
        }
    }

    private void generateCubicBezierCurve(boolean pAbsolute) {
        assertParameterCountMinimum(6);
        if (pAbsolute) {
            while (this.mCommandParameters.size() >= 6) {
                float x1 = this.mCommandParameters.poll().floatValue();
                float y1 = this.mCommandParameters.poll().floatValue();
                float x2 = this.mCommandParameters.poll().floatValue();
                float y2 = this.mCommandParameters.poll().floatValue();
                float x = this.mCommandParameters.poll().floatValue();
                float y = this.mCommandParameters.poll().floatValue();
                this.mPath.cubicTo(x1, y1, x2, y2, x, y);
                this.mLastCubicBezierX2 = x2;
                this.mLastCubicBezierY2 = y2;
                this.mLastX = x;
                this.mLastY = y;
            }
            return;
        }
        while (this.mCommandParameters.size() >= 6) {
            float x12 = this.mCommandParameters.poll().floatValue() + this.mLastX;
            float y12 = this.mCommandParameters.poll().floatValue() + this.mLastY;
            float x22 = this.mCommandParameters.poll().floatValue() + this.mLastX;
            float y22 = this.mCommandParameters.poll().floatValue() + this.mLastY;
            float x3 = this.mCommandParameters.poll().floatValue() + this.mLastX;
            float y3 = this.mCommandParameters.poll().floatValue() + this.mLastY;
            this.mPath.cubicTo(x12, y12, x22, y22, x3, y3);
            this.mLastCubicBezierX2 = x22;
            this.mLastCubicBezierY2 = y22;
            this.mLastX = x3;
            this.mLastY = y3;
        }
    }

    private void generateSmoothCubicBezierCurve(boolean pAbsolute) {
        assertParameterCountMinimum(4);
        if (pAbsolute) {
            while (this.mCommandParameters.size() >= 4) {
                float x1 = (this.mLastX * 2.0f) - this.mLastCubicBezierX2;
                float y1 = (this.mLastY * 2.0f) - this.mLastCubicBezierY2;
                float x2 = this.mCommandParameters.poll().floatValue();
                float y2 = this.mCommandParameters.poll().floatValue();
                float x = this.mCommandParameters.poll().floatValue();
                float y = this.mCommandParameters.poll().floatValue();
                this.mPath.cubicTo(x1, y1, x2, y2, x, y);
                this.mLastCubicBezierX2 = x2;
                this.mLastCubicBezierY2 = y2;
                this.mLastX = x;
                this.mLastY = y;
            }
            return;
        }
        while (this.mCommandParameters.size() >= 4) {
            float x12 = (this.mLastX * 2.0f) - this.mLastCubicBezierX2;
            float y12 = (this.mLastY * 2.0f) - this.mLastCubicBezierY2;
            float x22 = this.mCommandParameters.poll().floatValue() + this.mLastX;
            float y22 = this.mCommandParameters.poll().floatValue() + this.mLastY;
            float x3 = this.mCommandParameters.poll().floatValue() + this.mLastX;
            float y3 = this.mCommandParameters.poll().floatValue() + this.mLastY;
            this.mPath.cubicTo(x12, y12, x22, y22, x3, y3);
            this.mLastCubicBezierX2 = x22;
            this.mLastCubicBezierY2 = y22;
            this.mLastX = x3;
            this.mLastY = y3;
        }
    }

    private void generateQuadraticBezierCurve(boolean pAbsolute) {
        assertParameterCountMinimum(4);
        if (pAbsolute) {
            while (this.mCommandParameters.size() >= 4) {
                float x1 = this.mCommandParameters.poll().floatValue();
                float y1 = this.mCommandParameters.poll().floatValue();
                float x2 = this.mCommandParameters.poll().floatValue();
                float y2 = this.mCommandParameters.poll().floatValue();
                this.mPath.quadTo(x1, y1, x2, y2);
                this.mLastQuadraticBezierX2 = x2;
                this.mLastQuadraticBezierY2 = y2;
                this.mLastX = x2;
                this.mLastY = y2;
            }
            return;
        }
        while (this.mCommandParameters.size() >= 4) {
            float x12 = this.mCommandParameters.poll().floatValue() + this.mLastX;
            float y12 = this.mCommandParameters.poll().floatValue() + this.mLastY;
            float x22 = this.mCommandParameters.poll().floatValue() + this.mLastX;
            float y22 = this.mCommandParameters.poll().floatValue() + this.mLastY;
            this.mPath.quadTo(x12, y12, x22, y22);
            this.mLastQuadraticBezierX2 = x22;
            this.mLastQuadraticBezierY2 = y22;
            this.mLastX = x22;
            this.mLastY = y22;
        }
    }

    private void generateSmoothQuadraticBezierCurve(boolean pAbsolute) {
        assertParameterCountMinimum(2);
        if (pAbsolute) {
            while (this.mCommandParameters.size() >= 2) {
                float x1 = (this.mLastX * 2.0f) - this.mLastQuadraticBezierX2;
                float y1 = (this.mLastY * 2.0f) - this.mLastQuadraticBezierY2;
                float x2 = this.mCommandParameters.poll().floatValue();
                float y2 = this.mCommandParameters.poll().floatValue();
                this.mPath.quadTo(x1, y1, x2, y2);
                this.mLastQuadraticBezierX2 = x2;
                this.mLastQuadraticBezierY2 = y2;
                this.mLastX = x2;
                this.mLastY = y2;
            }
            return;
        }
        while (this.mCommandParameters.size() >= 2) {
            float x12 = (this.mLastX * 2.0f) - this.mLastQuadraticBezierX2;
            float y12 = (this.mLastY * 2.0f) - this.mLastQuadraticBezierY2;
            float x22 = this.mCommandParameters.poll().floatValue() + this.mLastX;
            float y22 = this.mCommandParameters.poll().floatValue() + this.mLastY;
            this.mPath.quadTo(x12, y12, x22, y22);
            this.mLastQuadraticBezierX2 = x22;
            this.mLastQuadraticBezierY2 = y22;
            this.mLastX = x22;
            this.mLastY = y22;
        }
    }

    private void generateArc(boolean pAbsolute) {
        boolean largeArcFlag;
        boolean sweepFlag;
        boolean largeArcFlag2;
        boolean sweepFlag2;
        assertParameterCountMinimum(7);
        if (pAbsolute) {
            while (this.mCommandParameters.size() >= 7) {
                float rx = this.mCommandParameters.poll().floatValue();
                float ry = this.mCommandParameters.poll().floatValue();
                float theta = this.mCommandParameters.poll().floatValue();
                if (this.mCommandParameters.poll().intValue() == 1) {
                    largeArcFlag2 = true;
                } else {
                    largeArcFlag2 = false;
                }
                if (this.mCommandParameters.poll().intValue() == 1) {
                    sweepFlag2 = true;
                } else {
                    sweepFlag2 = false;
                }
                float x = this.mCommandParameters.poll().floatValue();
                float y = this.mCommandParameters.poll().floatValue();
                generateArc(rx, ry, theta, largeArcFlag2, sweepFlag2, x, y);
                this.mLastX = x;
                this.mLastY = y;
            }
            return;
        }
        while (this.mCommandParameters.size() >= 7) {
            float rx2 = this.mCommandParameters.poll().floatValue();
            float ry2 = this.mCommandParameters.poll().floatValue();
            float theta2 = this.mCommandParameters.poll().floatValue();
            if (this.mCommandParameters.poll().intValue() == 1) {
                largeArcFlag = true;
            } else {
                largeArcFlag = false;
            }
            if (this.mCommandParameters.poll().intValue() == 1) {
                sweepFlag = true;
            } else {
                sweepFlag = false;
            }
            float x2 = this.mCommandParameters.poll().floatValue() + this.mLastX;
            float y2 = this.mCommandParameters.poll().floatValue() + this.mLastY;
            generateArc(rx2, ry2, theta2, largeArcFlag, sweepFlag, x2, y2);
            this.mLastX = x2;
            this.mLastY = y2;
        }
    }

    private void generateArc(float rx, float ry, float pTheta, boolean pLargeArcFlag, boolean pSweepFlag, float pX, float pY) {
        float dx = (this.mLastX - pX) * 0.5f;
        float dy = (this.mLastY - pY) * 0.5f;
        float thetaRad = MathUtils.degToRad(pTheta % 360.0f);
        float cosAngle = FloatMath.cos(thetaRad);
        float sinAngle = FloatMath.sin(thetaRad);
        float x1 = (cosAngle * dx) + (sinAngle * dy);
        float y1 = ((-sinAngle) * dx) + (cosAngle * dy);
        float radiusX = Math.abs(rx);
        float radiusY = Math.abs(ry);
        float Prx = radiusX * radiusX;
        float Pry = radiusY * radiusY;
        float Px1 = x1 * x1;
        float Py1 = y1 * y1;
        float radiiCheck = (Px1 / Prx) + (Py1 / Pry);
        if (radiiCheck > 1.0f) {
            radiusX *= FloatMath.sqrt(radiiCheck);
            radiusY *= FloatMath.sqrt(radiiCheck);
            Prx = radiusX * radiusX;
            Pry = radiusY * radiusY;
        }
        float sign = (float) (pLargeArcFlag == pSweepFlag ? -1 : 1);
        float sq = (((Prx * Pry) - (Prx * Py1)) - (Pry * Px1)) / ((Prx * Py1) + (Pry * Px1));
        if (sq < 0.0f) {
            sq = 0.0f;
        }
        float coef = sign * FloatMath.sqrt(sq);
        float cx_dash = coef * ((radiusX * y1) / radiusY);
        float cy_dash = coef * (-((radiusY * x1) / radiusX));
        float cx = ((this.mLastX + pX) * 0.5f) + ((cosAngle * cx_dash) - (sinAngle * cy_dash));
        float cy = ((this.mLastY + pY) * 0.5f) + (sinAngle * cx_dash) + (cosAngle * cy_dash);
        float ux = (x1 - cx_dash) / radiusX;
        float uy = (y1 - cy_dash) / radiusY;
        float vx = ((-x1) - cx_dash) / radiusX;
        float vy = ((-y1) - cy_dash) / radiusY;
        float startAngle = MathUtils.radToDeg(((float) Math.acos((double) (ux / FloatMath.sqrt((ux * ux) + (uy * uy))))) * (uy < 0.0f ? -1.0f : 1.0f));
        float sweepAngle = MathUtils.radToDeg(((float) Math.acos((double) (((ux * vx) + (uy * vy)) / FloatMath.sqrt(((ux * ux) + (uy * uy)) * ((vx * vx) + (vy * vy)))))) * ((ux * vy) - (uy * vx) < 0.0f ? -1.0f : 1.0f));
        if (!pSweepFlag && sweepAngle > 0.0f) {
            sweepAngle -= 360.0f;
        } else if (pSweepFlag && sweepAngle < 0.0f) {
            sweepAngle += 360.0f;
        }
        this.mArcRect.set(cx - radiusX, cy - radiusY, cx + radiusX, cy + radiusY);
        this.mPath.arcTo(this.mArcRect, startAngle % 360.0f, sweepAngle % 360.0f);
    }

    private void generateClose() {
        assertParameterCount(0);
        this.mPath.close();
        this.mLastX = this.mSubPathStartX;
        this.mLastY = this.mSubPathStartY;
    }

    public class PathParserHelper {
        public PathParserHelper() {
        }

        private char read() {
            if (SVGPathParser.this.mPosition < SVGPathParser.this.mLength) {
                SVGPathParser sVGPathParser = SVGPathParser.this;
                sVGPathParser.mPosition = sVGPathParser.mPosition + 1;
            }
            if (SVGPathParser.this.mPosition == SVGPathParser.this.mLength) {
                return 0;
            }
            return SVGPathParser.this.mString.charAt(SVGPathParser.this.mPosition);
        }

        public void skipWhitespace() {
            while (SVGPathParser.this.mPosition < SVGPathParser.this.mLength && Character.isWhitespace(SVGPathParser.this.mString.charAt(SVGPathParser.this.mPosition))) {
                advance();
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:2:0x000f  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void skipNumberSeparator() {
            /*
                r3 = this;
            L_0x0000:
                org.anddev.andengine.extension.svg.util.SVGPathParser r1 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                int r1 = r1.mPosition
                org.anddev.andengine.extension.svg.util.SVGPathParser r2 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                int r2 = r2.mLength
                if (r1 < r2) goto L_0x000f
            L_0x000e:
                return
            L_0x000f:
                org.anddev.andengine.extension.svg.util.SVGPathParser r1 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                java.lang.String r1 = r1.mString
                org.anddev.andengine.extension.svg.util.SVGPathParser r2 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                int r2 = r2.mPosition
                char r0 = r1.charAt(r2)
                switch(r0) {
                    case 9: goto L_0x0023;
                    case 10: goto L_0x0023;
                    case 32: goto L_0x0023;
                    case 44: goto L_0x0023;
                    default: goto L_0x0022;
                }
            L_0x0022:
                goto L_0x000e
            L_0x0023:
                r3.advance()
                goto L_0x0000
            */
            throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.extension.svg.util.SVGPathParser.PathParserHelper.skipNumberSeparator():void");
        }

        public void advance() {
            SVGPathParser.this.mCurrentChar = read();
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
            jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
            */
        /* JADX WARNING: Removed duplicated region for block: B:59:0x0040 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:60:0x0042 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:61:0x0064 A[SYNTHETIC] */
        private float parseFloat() {
            /*
                r13 = this;
                r12 = 9
                r11 = 0
                r10 = 48
                r5 = 0
                r6 = 0
                r4 = 1
                r7 = 0
                r0 = 0
                r2 = 0
                r1 = 0
                r3 = 1
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r8 = r8.mCurrentChar
                switch(r8) {
                    case 43: goto L_0x0023;
                    case 44: goto L_0x0016;
                    case 45: goto L_0x0022;
                    default: goto L_0x0016;
                }
            L_0x0016:
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r8 = r8.mCurrentChar
                switch(r8) {
                    case 46: goto L_0x0064;
                    case 47: goto L_0x001f;
                    case 48: goto L_0x002d;
                    case 49: goto L_0x0042;
                    case 50: goto L_0x0042;
                    case 51: goto L_0x0042;
                    case 52: goto L_0x0042;
                    case 53: goto L_0x0042;
                    case 54: goto L_0x0042;
                    case 55: goto L_0x0042;
                    case 56: goto L_0x0042;
                    case 57: goto L_0x0042;
                    default: goto L_0x001f;
                }
            L_0x001f:
                r8 = 2143289344(0x7fc00000, float:NaN)
            L_0x0021:
                return r8
            L_0x0022:
                r4 = 0
            L_0x0023:
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r9 = r13.read()
                r8.mCurrentChar = r9
                goto L_0x0016
            L_0x002d:
                r7 = 1
            L_0x002e:
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r9 = r13.read()
                r8.mCurrentChar = r9
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r8 = r8.mCurrentChar
                switch(r8) {
                    case 46: goto L_0x0064;
                    case 48: goto L_0x002e;
                    case 49: goto L_0x0042;
                    case 50: goto L_0x0042;
                    case 51: goto L_0x0042;
                    case 52: goto L_0x0042;
                    case 53: goto L_0x0042;
                    case 54: goto L_0x0042;
                    case 55: goto L_0x0042;
                    case 56: goto L_0x0042;
                    case 57: goto L_0x0042;
                    case 69: goto L_0x0064;
                    case 101: goto L_0x0064;
                    default: goto L_0x0040;
                }
            L_0x0040:
                r8 = r11
                goto L_0x0021
            L_0x0042:
                r7 = 1
            L_0x0043:
                if (r6 >= r12) goto L_0x00a3
                int r6 = r6 + 1
                int r8 = r5 * 10
                org.anddev.andengine.extension.svg.util.SVGPathParser r9 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r9 = r9.mCurrentChar
                int r9 = r9 - r10
                int r5 = r8 + r9
            L_0x0052:
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r9 = r13.read()
                r8.mCurrentChar = r9
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r8 = r8.mCurrentChar
                switch(r8) {
                    case 48: goto L_0x0043;
                    case 49: goto L_0x0043;
                    case 50: goto L_0x0043;
                    case 51: goto L_0x0043;
                    case 52: goto L_0x0043;
                    case 53: goto L_0x0043;
                    case 54: goto L_0x0043;
                    case 55: goto L_0x0043;
                    case 56: goto L_0x0043;
                    case 57: goto L_0x0043;
                    default: goto L_0x0064;
                }
            L_0x0064:
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r8 = r8.mCurrentChar
                r9 = 46
                if (r8 != r9) goto L_0x00e4
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r9 = r13.read()
                r8.mCurrentChar = r9
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r8 = r8.mCurrentChar
                switch(r8) {
                    case 48: goto L_0x00a6;
                    case 49: goto L_0x00c1;
                    case 50: goto L_0x00c1;
                    case 51: goto L_0x00c1;
                    case 52: goto L_0x00c1;
                    case 53: goto L_0x00c1;
                    case 54: goto L_0x00c1;
                    case 55: goto L_0x00c1;
                    case 56: goto L_0x00c1;
                    case 57: goto L_0x00c1;
                    default: goto L_0x0080;
                }
            L_0x0080:
                if (r7 != 0) goto L_0x00e4
                java.lang.IllegalArgumentException r8 = new java.lang.IllegalArgumentException
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                java.lang.String r10 = "Unexpected char '"
                r9.<init>(r10)
                org.anddev.andengine.extension.svg.util.SVGPathParser r10 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r10 = r10.mCurrentChar
                java.lang.StringBuilder r9 = r9.append(r10)
                java.lang.String r10 = "'."
                java.lang.StringBuilder r9 = r9.append(r10)
                java.lang.String r9 = r9.toString()
                r8.<init>(r9)
                throw r8
            L_0x00a3:
                int r1 = r1 + 1
                goto L_0x0052
            L_0x00a6:
                if (r6 != 0) goto L_0x00c1
            L_0x00a8:
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r9 = r13.read()
                r8.mCurrentChar = r9
                int r1 = r1 + -1
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r8 = r8.mCurrentChar
                switch(r8) {
                    case 48: goto L_0x00a8;
                    case 49: goto L_0x00c1;
                    case 50: goto L_0x00c1;
                    case 51: goto L_0x00c1;
                    case 52: goto L_0x00c1;
                    case 53: goto L_0x00c1;
                    case 54: goto L_0x00c1;
                    case 55: goto L_0x00c1;
                    case 56: goto L_0x00c1;
                    case 57: goto L_0x00c1;
                    default: goto L_0x00bc;
                }
            L_0x00bc:
                if (r7 != 0) goto L_0x00e4
                r8 = r11
                goto L_0x0021
            L_0x00c1:
                if (r6 >= r12) goto L_0x00d2
                int r6 = r6 + 1
                int r8 = r5 * 10
                org.anddev.andengine.extension.svg.util.SVGPathParser r9 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r9 = r9.mCurrentChar
                int r9 = r9 - r10
                int r5 = r8 + r9
                int r1 = r1 + -1
            L_0x00d2:
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r9 = r13.read()
                r8.mCurrentChar = r9
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r8 = r8.mCurrentChar
                switch(r8) {
                    case 48: goto L_0x00c1;
                    case 49: goto L_0x00c1;
                    case 50: goto L_0x00c1;
                    case 51: goto L_0x00c1;
                    case 52: goto L_0x00c1;
                    case 53: goto L_0x00c1;
                    case 54: goto L_0x00c1;
                    case 55: goto L_0x00c1;
                    case 56: goto L_0x00c1;
                    case 57: goto L_0x00c1;
                    default: goto L_0x00e4;
                }
            L_0x00e4:
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r8 = r8.mCurrentChar
                switch(r8) {
                    case 69: goto L_0x00fa;
                    case 101: goto L_0x00fa;
                    default: goto L_0x00ed;
                }
            L_0x00ed:
                if (r3 != 0) goto L_0x00f0
                int r0 = -r0
            L_0x00f0:
                int r0 = r0 + r1
                if (r4 != 0) goto L_0x00f4
                int r5 = -r5
            L_0x00f4:
                float r8 = r13.buildFloat(r5, r0)
                goto L_0x0021
            L_0x00fa:
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r9 = r13.read()
                r8.mCurrentChar = r9
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r8 = r8.mCurrentChar
                switch(r8) {
                    case 43: goto L_0x012e;
                    case 44: goto L_0x010c;
                    case 45: goto L_0x012d;
                    case 46: goto L_0x010c;
                    case 47: goto L_0x010c;
                    case 48: goto L_0x0161;
                    case 49: goto L_0x0161;
                    case 50: goto L_0x0161;
                    case 51: goto L_0x0161;
                    case 52: goto L_0x0161;
                    case 53: goto L_0x0161;
                    case 54: goto L_0x0161;
                    case 55: goto L_0x0161;
                    case 56: goto L_0x0161;
                    case 57: goto L_0x0161;
                    default: goto L_0x010c;
                }
            L_0x010c:
                java.lang.IllegalArgumentException r8 = new java.lang.IllegalArgumentException
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                java.lang.String r10 = "Unexpected char '"
                r9.<init>(r10)
                org.anddev.andengine.extension.svg.util.SVGPathParser r10 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r10 = r10.mCurrentChar
                java.lang.StringBuilder r9 = r9.append(r10)
                java.lang.String r10 = "'."
                java.lang.StringBuilder r9 = r9.append(r10)
                java.lang.String r9 = r9.toString()
                r8.<init>(r9)
                throw r8
            L_0x012d:
                r3 = 0
            L_0x012e:
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r9 = r13.read()
                r8.mCurrentChar = r9
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r8 = r8.mCurrentChar
                switch(r8) {
                    case 48: goto L_0x0161;
                    case 49: goto L_0x0161;
                    case 50: goto L_0x0161;
                    case 51: goto L_0x0161;
                    case 52: goto L_0x0161;
                    case 53: goto L_0x0161;
                    case 54: goto L_0x0161;
                    case 55: goto L_0x0161;
                    case 56: goto L_0x0161;
                    case 57: goto L_0x0161;
                    default: goto L_0x0140;
                }
            L_0x0140:
                java.lang.IllegalArgumentException r8 = new java.lang.IllegalArgumentException
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                java.lang.String r10 = "Unexpected char '"
                r9.<init>(r10)
                org.anddev.andengine.extension.svg.util.SVGPathParser r10 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r10 = r10.mCurrentChar
                java.lang.StringBuilder r9 = r9.append(r10)
                java.lang.String r10 = "'."
                java.lang.StringBuilder r9 = r9.append(r10)
                java.lang.String r9 = r9.toString()
                r8.<init>(r9)
                throw r8
            L_0x0161:
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r8 = r8.mCurrentChar
                switch(r8) {
                    case 48: goto L_0x016b;
                    case 49: goto L_0x017f;
                    case 50: goto L_0x017f;
                    case 51: goto L_0x017f;
                    case 52: goto L_0x017f;
                    case 53: goto L_0x017f;
                    case 54: goto L_0x017f;
                    case 55: goto L_0x017f;
                    case 56: goto L_0x017f;
                    case 57: goto L_0x017f;
                    default: goto L_0x016a;
                }
            L_0x016a:
                goto L_0x00ed
            L_0x016b:
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r9 = r13.read()
                r8.mCurrentChar = r9
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r8 = r8.mCurrentChar
                switch(r8) {
                    case 48: goto L_0x016b;
                    case 49: goto L_0x017f;
                    case 50: goto L_0x017f;
                    case 51: goto L_0x017f;
                    case 52: goto L_0x017f;
                    case 53: goto L_0x017f;
                    case 54: goto L_0x017f;
                    case 55: goto L_0x017f;
                    case 56: goto L_0x017f;
                    case 57: goto L_0x017f;
                    default: goto L_0x017d;
                }
            L_0x017d:
                goto L_0x00ed
            L_0x017f:
                r8 = 3
                if (r2 >= r8) goto L_0x018f
                int r2 = r2 + 1
                int r8 = r0 * 10
                org.anddev.andengine.extension.svg.util.SVGPathParser r9 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r9 = r9.mCurrentChar
                int r9 = r9 - r10
                int r0 = r8 + r9
            L_0x018f:
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r9 = r13.read()
                r8.mCurrentChar = r9
                org.anddev.andengine.extension.svg.util.SVGPathParser r8 = org.anddev.andengine.extension.svg.util.SVGPathParser.this
                char r8 = r8.mCurrentChar
                switch(r8) {
                    case 48: goto L_0x017f;
                    case 49: goto L_0x017f;
                    case 50: goto L_0x017f;
                    case 51: goto L_0x017f;
                    case 52: goto L_0x017f;
                    case 53: goto L_0x017f;
                    case 54: goto L_0x017f;
                    case 55: goto L_0x017f;
                    case 56: goto L_0x017f;
                    case 57: goto L_0x017f;
                    default: goto L_0x01a1;
                }
            L_0x01a1:
                goto L_0x00ed
            */
            throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.extension.svg.util.SVGPathParser.PathParserHelper.parseFloat():float");
        }

        public float nextFloat() {
            skipWhitespace();
            float f = parseFloat();
            skipNumberSeparator();
            return f;
        }

        public float buildFloat(int pMantissa, int pExponent) {
            if (pExponent < -125 || pMantissa == 0) {
                return 0.0f;
            }
            if (pExponent >= 128) {
                if (pMantissa > 0) {
                    return Float.POSITIVE_INFINITY;
                }
                return Float.NEGATIVE_INFINITY;
            } else if (pExponent == 0) {
                return (float) pMantissa;
            } else {
                if (pMantissa >= 67108864) {
                    pMantissa++;
                }
                return (float) (pExponent > 0 ? ((double) pMantissa) * org.anddev.andengine.extension.svg.util.constants.MathUtils.POWERS_OF_10[pExponent] : ((double) pMantissa) / org.anddev.andengine.extension.svg.util.constants.MathUtils.POWERS_OF_10[-pExponent]);
            }
        }
    }
}
