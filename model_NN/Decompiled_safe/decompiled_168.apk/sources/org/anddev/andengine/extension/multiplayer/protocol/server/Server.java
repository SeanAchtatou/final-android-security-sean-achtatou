package org.anddev.andengine.extension.multiplayer.protocol.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.server.IServerMessage;
import org.anddev.andengine.extension.multiplayer.protocol.server.connector.ClientConnector;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connection;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.SmartList;

public abstract class Server<C extends Connection, CC extends ClientConnector<C>> extends Thread {
    protected ClientConnector.IClientConnectorListener<C> mClientConnectorListener;
    protected final SmartList<CC> mClientConnectors = new SmartList<>();
    private final AtomicBoolean mRunning = new AtomicBoolean(false);
    protected IServerListener<? extends Server<C, CC>> mServerListener;
    private final AtomicBoolean mTerminated = new AtomicBoolean(false);

    public interface IServerListener<S extends Server<?, ?>> {
        void onException(Server server, Throwable th);

        void onStarted(Server server);

        void onTerminated(Server server);
    }

    /* access modifiers changed from: protected */
    public abstract CC acceptClientConnector() throws IOException;

    /* access modifiers changed from: protected */
    public abstract void onException(Throwable th);

    /* access modifiers changed from: protected */
    public abstract void onStart() throws IOException;

    /* access modifiers changed from: protected */
    public abstract void onTerminate();

    public Server(ClientConnector.IClientConnectorListener<C> pClientConnectorListener, IServerListener<? extends Server<C, CC>> pServerListener) {
        this.mServerListener = pServerListener;
        this.mClientConnectorListener = pClientConnectorListener;
        initName();
    }

    private void initName() {
        setName(getClass().getName());
    }

    public boolean isRunning() {
        return this.mRunning.get();
    }

    public boolean isTerminated() {
        return this.mTerminated.get();
    }

    public ClientConnector.IClientConnectorListener<C> getClientConnectorListener() {
        return this.mClientConnectorListener;
    }

    public void setClientConnectorListener(ClientConnector.IClientConnectorListener<C> pClientConnectorListener) {
        this.mClientConnectorListener = pClientConnectorListener;
    }

    public IServerListener<? extends Server<C, CC>> getServerListener() {
        return this.mServerListener;
    }

    /* access modifiers changed from: protected */
    public void setServerListener(IServerListener<? extends Server<C, CC>> pServerListener) {
        this.mServerListener = pServerListener;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: CC
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void run() {
        /*
            r4 = this;
            r4.onStart()     // Catch:{ Throwable -> 0x0042 }
            java.util.concurrent.atomic.AtomicBoolean r2 = r4.mRunning     // Catch:{ Throwable -> 0x0042 }
            r3 = 1
            r2.set(r3)     // Catch:{ Throwable -> 0x0042 }
            r2 = 0
            android.os.Process.setThreadPriority(r2)     // Catch:{ Throwable -> 0x0042 }
        L_0x000d:
            boolean r2 = java.lang.Thread.interrupted()     // Catch:{ Throwable -> 0x0042 }
            if (r2 != 0) goto L_0x0023
            java.util.concurrent.atomic.AtomicBoolean r2 = r4.mRunning     // Catch:{ Throwable -> 0x0042 }
            boolean r2 = r2.get()     // Catch:{ Throwable -> 0x0042 }
            if (r2 == 0) goto L_0x0023
            java.util.concurrent.atomic.AtomicBoolean r2 = r4.mTerminated     // Catch:{ Throwable -> 0x0042 }
            boolean r2 = r2.get()     // Catch:{ Throwable -> 0x0042 }
            if (r2 == 0) goto L_0x0027
        L_0x0023:
            r4.terminate()
        L_0x0026:
            return
        L_0x0027:
            org.anddev.andengine.extension.multiplayer.protocol.server.connector.ClientConnector r0 = r4.acceptClientConnector()     // Catch:{ Throwable -> 0x003c }
            org.anddev.andengine.extension.multiplayer.protocol.server.Server$1 r2 = new org.anddev.andengine.extension.multiplayer.protocol.server.Server$1     // Catch:{ Throwable -> 0x003c }
            r2.<init>(r0)     // Catch:{ Throwable -> 0x003c }
            r0.addClientConnectorListener(r2)     // Catch:{ Throwable -> 0x003c }
            org.anddev.andengine.extension.multiplayer.protocol.server.connector.ClientConnector$IClientConnectorListener<C> r2 = r4.mClientConnectorListener     // Catch:{ Throwable -> 0x003c }
            r0.addClientConnectorListener(r2)     // Catch:{ Throwable -> 0x003c }
            r0.start()     // Catch:{ Throwable -> 0x003c }
            goto L_0x000d
        L_0x003c:
            r2 = move-exception
            r1 = r2
            r4.onException(r1)     // Catch:{ Throwable -> 0x0042 }
            goto L_0x000d
        L_0x0042:
            r2 = move-exception
            r1 = r2
            r4.onException(r1)     // Catch:{ all -> 0x004b }
            r4.terminate()
            goto L_0x0026
        L_0x004b:
            r2 = move-exception
            r4.terminate()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.extension.multiplayer.protocol.server.Server.run():void");
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        terminate();
        super.finalize();
    }

    /* access modifiers changed from: private */
    public synchronized void onAddClientConnector(CC pClientConnector) {
        this.mClientConnectors.add(pClientConnector);
    }

    /* access modifiers changed from: private */
    public synchronized void onRemoveClientConnector(CC pClientConnector) {
        this.mClientConnectors.remove((Object) pClientConnector);
    }

    public void terminate() {
        if (!this.mTerminated.getAndSet(true)) {
            this.mRunning.set(false);
            try {
                ArrayList<CC> clientConnectors = this.mClientConnectors;
                for (int i = 0; i < clientConnectors.size(); i++) {
                    ((ClientConnector) clientConnectors.get(i)).terminate();
                }
                clientConnectors.clear();
            } catch (Exception e) {
                onException(e);
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e2) {
                Debug.e(e2);
            }
            interrupt();
            onTerminate();
        }
    }

    public synchronized void sendBroadcastServerMessage(IServerMessage pServerMessage) throws IOException {
        if (this.mRunning.get()) {
            ArrayList<CC> clientConnectors = this.mClientConnectors;
            for (int i = 0; i < clientConnectors.size(); i++) {
                try {
                    ((ClientConnector) clientConnectors.get(i)).sendServerMessage(pServerMessage);
                } catch (IOException e) {
                    onException(e);
                }
            }
        }
    }
}
