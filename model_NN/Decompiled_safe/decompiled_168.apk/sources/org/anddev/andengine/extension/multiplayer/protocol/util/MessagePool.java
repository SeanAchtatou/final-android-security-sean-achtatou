package org.anddev.andengine.extension.multiplayer.protocol.util;

import java.util.List;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.IMessage;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.pool.GenericPool;
import org.anddev.andengine.util.pool.MultiPool;

public class MessagePool<M extends IMessage> {
    private final MultiPool<M> mMessageMultiPool = new MultiPool<>();

    public void registerMessage(short pFlag, final Class<? extends M> pMessageClass) {
        this.mMessageMultiPool.registerPool(pFlag, new GenericPool<M>() {
            /* access modifiers changed from: protected */
            public M onAllocatePoolItem() {
                try {
                    return (IMessage) pMessageClass.newInstance();
                } catch (Throwable th) {
                    Debug.e(th);
                    return null;
                }
            }
        });
    }

    public M obtainMessage(short pFlag) {
        return (IMessage) this.mMessageMultiPool.obtainPoolItem(pFlag);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: M
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public M obtainMessage(short r5, java.io.DataInputStream r6) throws java.io.IOException {
        /*
            r4 = this;
            org.anddev.andengine.util.pool.MultiPool<M> r1 = r4.mMessageMultiPool
            java.lang.Object r0 = r1.obtainPoolItem(r5)
            org.anddev.andengine.extension.multiplayer.protocol.adt.message.IMessage r0 = (org.anddev.andengine.extension.multiplayer.protocol.adt.message.IMessage) r0
            if (r0 == 0) goto L_0x000e
            r0.read(r6)
            return r0
        L_0x000e:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "No message found for pFlag='"
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.String r3 = "'."
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.extension.multiplayer.protocol.util.MessagePool.obtainMessage(short, java.io.DataInputStream):org.anddev.andengine.extension.multiplayer.protocol.adt.message.IMessage");
    }

    public void recycleMessage(M pMessage) {
        this.mMessageMultiPool.recyclePoolItem(pMessage.getFlag(), pMessage);
    }

    public void recycleMessages(List<? extends M> pMessages) {
        MultiPool<M> messageMultiPool = this.mMessageMultiPool;
        for (int i = pMessages.size() - 1; i >= 0; i--) {
            M message = (IMessage) pMessages.get(i);
            messageMultiPool.recyclePoolItem(message.getFlag(), message);
        }
    }
}
