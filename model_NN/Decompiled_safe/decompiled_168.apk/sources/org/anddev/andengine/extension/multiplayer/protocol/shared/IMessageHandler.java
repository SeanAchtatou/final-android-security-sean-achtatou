package org.anddev.andengine.extension.multiplayer.protocol.shared;

import java.io.IOException;
import org.anddev.andengine.extension.multiplayer.protocol.adt.message.IMessage;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connection;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connector;

public interface IMessageHandler<C extends Connection, CC extends Connector<C>, M extends IMessage> {
    void onHandleMessage(CC cc, M m) throws IOException;
}
