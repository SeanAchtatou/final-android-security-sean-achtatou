package org.anddev.andengine.opengl.texture.source;

public interface ITextureAtlasSource extends Cloneable {
    ITextureAtlasSource clone();

    int getHeight();

    int getTexturePositionX();

    int getTexturePositionY();

    int getWidth();

    void setTexturePositionX(int i);

    void setTexturePositionY(int i);
}
