package org.anddev.andengine.opengl.texture.buildable;

import org.anddev.andengine.opengl.texture.atlas.ITextureAtlas;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.opengl.texture.source.ITextureAtlasSource;
import org.anddev.andengine.util.Callback;

public class BuildableTextureAtlasTextureRegionFactory {
    public static <T extends ITextureAtlasSource, A extends ITextureAtlas<T>> TextureRegion createFromSource(BuildableTextureAtlas<T, A> pBuildableTextureAtlas, T pTextureAtlasSource, boolean pTextureRegionBufferManaged) {
        final TextureRegion textureRegion = new TextureRegion(pBuildableTextureAtlas, 0, 0, pTextureAtlasSource.getWidth(), pTextureAtlasSource.getHeight());
        pBuildableTextureAtlas.addTextureAtlasSource(pTextureAtlasSource, new Callback<T>() {
            public void onCallback(T pCallbackValue) {
                TextureRegion.this.setTexturePosition(pCallbackValue.getTexturePositionX(), pCallbackValue.getTexturePositionY());
            }
        });
        textureRegion.setTextureRegionBufferManaged(pTextureRegionBufferManaged);
        return textureRegion;
    }

    public static <T extends ITextureAtlasSource, A extends ITextureAtlas<T>> TiledTextureRegion createTiledFromSource(BuildableTextureAtlas<T, A> pBuildableTextureAtlas, T pTextureAtlasSource, int pTileColumns, int pTileRows, boolean pTextureRegionBufferManaged) {
        final TiledTextureRegion tiledTextureRegion = new TiledTextureRegion(pBuildableTextureAtlas, 0, 0, pTextureAtlasSource.getWidth(), pTextureAtlasSource.getHeight(), pTileColumns, pTileRows);
        pBuildableTextureAtlas.addTextureAtlasSource(pTextureAtlasSource, new Callback<T>() {
            public void onCallback(T pCallbackValue) {
                TiledTextureRegion.this.setTexturePosition(pCallbackValue.getTexturePositionX(), pCallbackValue.getTexturePositionY());
            }
        });
        tiledTextureRegion.setTextureRegionBufferManaged(pTextureRegionBufferManaged);
        return tiledTextureRegion;
    }
}
