package org.anddev.andengine.opengl.texture.compressed.pvr;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.zip.GZIPInputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;
import org.anddev.andengine.opengl.texture.ITexture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.util.ArrayUtils;
import org.anddev.andengine.util.StreamUtils;

public abstract class PVRCCZTexture extends PVRTexture {
    /* access modifiers changed from: protected */
    public abstract InputStream getInputStream() throws IOException;

    public PVRCCZTexture(PVRTexture.PVRTextureFormat pPVRTextureFormat) throws IllegalArgumentException, IOException {
        super(pPVRTextureFormat);
    }

    public PVRCCZTexture(PVRTexture.PVRTextureFormat pPVRTextureFormat, ITexture.ITextureStateListener pTextureStateListener) throws IllegalArgumentException, IOException {
        super(pPVRTextureFormat, pTextureStateListener);
    }

    public PVRCCZTexture(PVRTexture.PVRTextureFormat pPVRTextureFormat, TextureOptions pTextureOptions) throws IllegalArgumentException, IOException {
        super(pPVRTextureFormat, pTextureOptions);
    }

    public PVRCCZTexture(PVRTexture.PVRTextureFormat pPVRTextureFormat, TextureOptions pTextureOptions, ITexture.ITextureStateListener pTextureStateListener) throws IllegalArgumentException, IOException {
        super(pPVRTextureFormat, pTextureOptions, pTextureStateListener);
    }

    /* access modifiers changed from: protected */
    public final InputStream onGetInputStream() throws IOException {
        InputStream inputStream = getInputStream();
        CCZHeader cczHeader = new CCZHeader(StreamUtils.streamToBytes(inputStream, 16));
        return cczHeader.getCCZCompressionFormat().wrap(inputStream, cczHeader.getUncompressedSize());
    }

    public static class CCZHeader {
        public static final byte[] MAGIC_IDENTIFIER = {67, 67, 90, 33};
        public static final int SIZE = 16;
        private final CCZCompressionFormat mCCZCompressionFormat;
        private final ByteBuffer mDataByteBuffer;

        public CCZHeader(byte[] pData) {
            this.mDataByteBuffer = ByteBuffer.wrap(pData);
            this.mDataByteBuffer.rewind();
            this.mDataByteBuffer.order(ByteOrder.BIG_ENDIAN);
            if (!ArrayUtils.equals(pData, 0, MAGIC_IDENTIFIER, 0, MAGIC_IDENTIFIER.length)) {
                throw new IllegalArgumentException("Invalid " + getClass().getSimpleName() + "!");
            }
            this.mCCZCompressionFormat = CCZCompressionFormat.fromID(getCCZCompressionFormatID());
        }

        private short getCCZCompressionFormatID() {
            return this.mDataByteBuffer.getShort(4);
        }

        public CCZCompressionFormat getCCZCompressionFormat() {
            return this.mCCZCompressionFormat;
        }

        public short getVersion() {
            return this.mDataByteBuffer.getShort(6);
        }

        public int getUserdata() {
            return this.mDataByteBuffer.getInt(8);
        }

        public int getUncompressedSize() {
            return this.mDataByteBuffer.getInt(12);
        }
    }

    public enum CCZCompressionFormat {
        ZLIB(0),
        BZIP2(1),
        GZIP(2),
        NONE(3);
        
        private static /* synthetic */ int[] $SWITCH_TABLE$org$anddev$andengine$opengl$texture$compressed$pvr$PVRCCZTexture$CCZCompressionFormat;
        private final short mID;

        private CCZCompressionFormat(short pID) {
            this.mID = pID;
        }

        public InputStream wrap(InputStream pInputStream, int pUncompressedSize) throws IOException {
            switch ($SWITCH_TABLE$org$anddev$andengine$opengl$texture$compressed$pvr$PVRCCZTexture$CCZCompressionFormat()[ordinal()]) {
                case 1:
                    return new InflaterInputStream(pInputStream, new Inflater(), pUncompressedSize);
                case 2:
                default:
                    throw new IllegalArgumentException("Unexpected " + CCZCompressionFormat.class.getSimpleName() + ": '" + this + "'.");
                case 3:
                    return new GZIPInputStream(pInputStream, pUncompressedSize);
            }
        }

        public static CCZCompressionFormat fromID(short pID) {
            for (CCZCompressionFormat cczCompressionFormat : values()) {
                if (cczCompressionFormat.mID == pID) {
                    return cczCompressionFormat;
                }
            }
            throw new IllegalArgumentException("Unexpected " + CCZCompressionFormat.class.getSimpleName() + "-ID: '" + ((int) pID) + "'.");
        }
    }
}
