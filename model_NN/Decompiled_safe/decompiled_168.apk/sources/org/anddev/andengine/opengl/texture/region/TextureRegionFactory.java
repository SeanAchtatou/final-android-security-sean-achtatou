package org.anddev.andengine.opengl.texture.region;

import org.anddev.andengine.opengl.texture.ITexture;
import org.anddev.andengine.opengl.texture.atlas.ITextureAtlas;
import org.anddev.andengine.opengl.texture.source.ITextureAtlasSource;

public class TextureRegionFactory {
    public static TextureRegion extractFromTexture(ITexture pTexture, int pTexturePositionX, int pTexturePositionY, int pWidth, int pHeight, boolean pTextureRegionBufferManaged) {
        TextureRegion textureRegion = new TextureRegion(pTexture, pTexturePositionX, pTexturePositionY, pWidth, pHeight);
        textureRegion.setTextureRegionBufferManaged(pTextureRegionBufferManaged);
        return textureRegion;
    }

    public static <T extends ITextureAtlasSource> TextureRegion createFromSource(ITextureAtlas iTextureAtlas, ITextureAtlasSource iTextureAtlasSource, int pTexturePositionX, int pTexturePositionY, boolean pCreateTextureRegionBuffersManaged) {
        TextureRegion textureRegion = new TextureRegion(iTextureAtlas, pTexturePositionX, pTexturePositionY, iTextureAtlasSource.getWidth(), iTextureAtlasSource.getHeight());
        iTextureAtlas.addTextureAtlasSource(iTextureAtlasSource, textureRegion.getTexturePositionX(), textureRegion.getTexturePositionY());
        textureRegion.setTextureRegionBufferManaged(pCreateTextureRegionBuffersManaged);
        return textureRegion;
    }

    public static <T extends ITextureAtlasSource> TiledTextureRegion createTiledFromSource(ITextureAtlas iTextureAtlas, ITextureAtlasSource iTextureAtlasSource, int pTexturePositionX, int pTexturePositionY, int pTileColumns, int pTileRows, boolean pCreateTextureRegionBuffersManaged) {
        TiledTextureRegion tiledTextureRegion = new TiledTextureRegion(iTextureAtlas, pTexturePositionX, pTexturePositionY, iTextureAtlasSource.getWidth(), iTextureAtlasSource.getHeight(), pTileColumns, pTileRows);
        iTextureAtlas.addTextureAtlasSource(iTextureAtlasSource, tiledTextureRegion.getTexturePositionX(), tiledTextureRegion.getTexturePositionY());
        tiledTextureRegion.setTextureRegionBufferManaged(pCreateTextureRegionBuffersManaged);
        return tiledTextureRegion;
    }
}
