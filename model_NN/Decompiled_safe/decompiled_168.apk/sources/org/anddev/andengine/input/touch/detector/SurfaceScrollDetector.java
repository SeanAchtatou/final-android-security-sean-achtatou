package org.anddev.andengine.input.touch.detector;

import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.input.touch.detector.ScrollDetector;

public class SurfaceScrollDetector extends ScrollDetector {
    public SurfaceScrollDetector(float pTriggerScrollMinimumDistance, ScrollDetector.IScrollDetectorListener pScrollDetectorListener) {
        super(pTriggerScrollMinimumDistance, pScrollDetectorListener);
    }

    public SurfaceScrollDetector(ScrollDetector.IScrollDetectorListener pScrollDetectorListener) {
        super(pScrollDetectorListener);
    }

    /* access modifiers changed from: protected */
    public float getX(TouchEvent pTouchEvent) {
        return pTouchEvent.getMotionEvent().getX();
    }

    /* access modifiers changed from: protected */
    public float getY(TouchEvent pTouchEvent) {
        return pTouchEvent.getMotionEvent().getY();
    }
}
