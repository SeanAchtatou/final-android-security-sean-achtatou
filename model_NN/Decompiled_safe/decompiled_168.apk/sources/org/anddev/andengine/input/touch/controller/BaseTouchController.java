package org.anddev.andengine.input.touch.controller;

import android.view.MotionEvent;
import org.anddev.andengine.engine.options.TouchOptions;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.input.touch.controller.ITouchController;
import org.anddev.andengine.util.pool.RunnablePoolItem;
import org.anddev.andengine.util.pool.RunnablePoolUpdateHandler;

public abstract class BaseTouchController implements ITouchController {
    private boolean mRunOnUpdateThread;
    /* access modifiers changed from: private */
    public ITouchController.ITouchEventCallback mTouchEventCallback;
    private final RunnablePoolUpdateHandler<TouchEventRunnablePoolItem> mTouchEventRunnablePoolUpdateHandler = new RunnablePoolUpdateHandler<TouchEventRunnablePoolItem>() {
        /* access modifiers changed from: protected */
        public TouchEventRunnablePoolItem onAllocatePoolItem() {
            return new TouchEventRunnablePoolItem();
        }
    };

    public void setTouchEventCallback(ITouchController.ITouchEventCallback pTouchEventCallback) {
        this.mTouchEventCallback = pTouchEventCallback;
    }

    public void reset() {
        if (this.mRunOnUpdateThread) {
            this.mTouchEventRunnablePoolUpdateHandler.reset();
        }
    }

    public void onUpdate(float pSecondsElapsed) {
        if (this.mRunOnUpdateThread) {
            this.mTouchEventRunnablePoolUpdateHandler.onUpdate(pSecondsElapsed);
        }
    }

    /* access modifiers changed from: protected */
    public boolean fireTouchEvent(float pX, float pY, int pAction, int pPointerID, MotionEvent pMotionEvent) {
        if (this.mRunOnUpdateThread) {
            TouchEvent touchEvent = TouchEvent.obtain(pX, pY, pAction, pPointerID, MotionEvent.obtain(pMotionEvent));
            TouchEventRunnablePoolItem touchEventRunnablePoolItem = this.mTouchEventRunnablePoolUpdateHandler.obtainPoolItem();
            touchEventRunnablePoolItem.set(touchEvent);
            this.mTouchEventRunnablePoolUpdateHandler.postPoolItem(touchEventRunnablePoolItem);
            return true;
        }
        TouchEvent touchEvent2 = TouchEvent.obtain(pX, pY, pAction, pPointerID, pMotionEvent);
        boolean handled = this.mTouchEventCallback.onTouchEvent(touchEvent2);
        touchEvent2.recycle();
        return handled;
    }

    public void applyTouchOptions(TouchOptions pTouchOptions) {
        this.mRunOnUpdateThread = pTouchOptions.isRunOnUpdateThread();
    }

    class TouchEventRunnablePoolItem extends RunnablePoolItem {
        private TouchEvent mTouchEvent;

        TouchEventRunnablePoolItem() {
        }

        public void set(TouchEvent pTouchEvent) {
            this.mTouchEvent = pTouchEvent;
        }

        public void run() {
            BaseTouchController.this.mTouchEventCallback.onTouchEvent(this.mTouchEvent);
        }

        /* access modifiers changed from: protected */
        public void onRecycle() {
            super.onRecycle();
            TouchEvent touchEvent = this.mTouchEvent;
            touchEvent.getMotionEvent().recycle();
            touchEvent.recycle();
        }
    }
}
