package com.androwk.classifyball;

import android.graphics.Typeface;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import java.util.ArrayList;
import java.util.List;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.modifier.ScaleModifier;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.entity.util.FPSLogger;
import org.anddev.andengine.extension.physics.box2d.PhysicsConnector;
import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;
import org.anddev.andengine.extension.physics.box2d.util.Vector2Pool;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.sensor.accelerometer.AccelerometerData;
import org.anddev.andengine.sensor.accelerometer.IAccelerometerListener;
import org.anddev.andengine.ui.activity.BaseGameActivity;
import org.anddev.andengine.util.HorizontalAlign;

public class ClassifyBallActivity extends BaseGameActivity implements IAccelerometerListener {
    private static final int CAMERA_HEIGHT = 480;
    private static final int CAMERA_WIDTH = 720;
    private static final FixtureDef FIXTURE_DEF = PhysicsFactory.createFixtureDef(3.0f, 0.1f, 0.1f);
    private TextureRegion mBall1Region1;
    private TextureRegion mBall1Region2;
    private BitmapTextureAtlas mBitmapTextureAtlas;
    private Camera mCamera;
    boolean mFlagClear = false;
    boolean mFlagGameOver = false;
    private Font mFont;
    private BitmapTextureAtlas mFontTexture;
    Shape mGameoverShape;
    int mLevel = 1;
    /* access modifiers changed from: private */
    public List<Sprite> mListSpriteBall1 = new ArrayList();
    /* access modifiers changed from: private */
    public List<Sprite> mListSpriteBall2 = new ArrayList();
    private List<Body> mListSpriteBallBody1 = new ArrayList();
    private List<Body> mListSpriteBallBody2 = new ArrayList();
    private PhysicsWorld mPhysicsWorld;
    Scene mScene;
    Text mStartText;
    Text mTitleText;

    public Engine onLoadEngine() {
        this.mCamera = new Camera(0.0f, 0.0f, 720.0f, 480.0f);
        return new Engine(new EngineOptions(true, EngineOptions.ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy(720.0f, 480.0f), this.mCamera));
    }

    public void onLoadResources() {
        this.mBitmapTextureAtlas = new BitmapTextureAtlas((int) PVRTexture.FLAG_TWIDDLE, (int) PVRTexture.FLAG_TWIDDLE, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mBall1Region1 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlas, this, "ball1.png", 0, 0);
        this.mBall1Region2 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlas, this, "ball2.png", 50, 0);
        this.mEngine.getTextureManager().loadTexture(this.mBitmapTextureAtlas);
        this.mFontTexture = new BitmapTextureAtlas((int) PVRTexture.FLAG_MIPMAP, (int) PVRTexture.FLAG_MIPMAP, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFont = new Font(this.mFontTexture, Typeface.create(Typeface.DEFAULT, 1), 64.0f, true, -1);
        this.mEngine.getTextureManager().loadTexture(this.mFontTexture);
        this.mEngine.getFontManager().loadFont(this.mFont);
    }

    public Scene onLoadScene() {
        this.mEngine.registerUpdateHandler(new FPSLogger());
        this.mScene = new Scene();
        this.mPhysicsWorld = new PhysicsWorld(new Vector2(0.0f, 9.80665f), false);
        Rectangle rectangle = new Rectangle(0.0f, 0.0f, 360.0f, 480.0f);
        Rectangle rectangle2 = new Rectangle(360.0f, 0.0f, 360.0f, 480.0f);
        this.mScene.attachChild(rectangle);
        this.mScene.attachChild(rectangle2);
        rectangle.setColor(1.0f, 0.39215687f, 0.6666667f);
        rectangle2.setColor(0.22745098f, 0.78431374f, 0.9647059f);
        Rectangle rectangle3 = new Rectangle(0.0f, 472.0f, 720.0f, 8.0f);
        Rectangle rectangle4 = new Rectangle(0.0f, 0.0f, 720.0f, 8.0f);
        Rectangle rectangle5 = new Rectangle(0.0f, 0.0f, 8.0f, 480.0f);
        Rectangle rectangle6 = new Rectangle(712.0f, 0.0f, 8.0f, 480.0f);
        Rectangle rectangle7 = new Rectangle(360.0f, 0.0f, 8.0f, 185.0f);
        Rectangle rectangle8 = new Rectangle(360.0f, 295.0f, 8.0f, 185.0f);
        FixtureDef wallFixtureDef = PhysicsFactory.createFixtureDef(0.0f, 0.5f, 0.5f);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, rectangle3, BodyDef.BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, rectangle4, BodyDef.BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, rectangle5, BodyDef.BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, rectangle6, BodyDef.BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, rectangle7, BodyDef.BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, rectangle8, BodyDef.BodyType.StaticBody, wallFixtureDef);
        this.mScene.attachChild(rectangle3);
        this.mScene.attachChild(rectangle4);
        this.mScene.attachChild(rectangle5);
        this.mScene.attachChild(rectangle6);
        this.mScene.attachChild(rectangle7);
        this.mScene.attachChild(rectangle8);
        Rectangle rectangle9 = new Rectangle((float) 30, (float) 30, (float) (360 - (30 * 2)), (float) (CAMERA_HEIGHT - (30 * 2)));
        Rectangle rectangle10 = new Rectangle((float) (30 + 360), (float) 30, (float) (360 - (30 * 2)), (float) (CAMERA_HEIGHT - (30 * 2)));
        rectangle9.setVisible(false);
        rectangle10.setVisible(false);
        this.mScene.attachChild(rectangle9);
        this.mScene.attachChild(rectangle10);
        rectangle9.setColor(0.98039216f, 0.98039216f, 0.50980395f);
        rectangle10.setColor(0.98039216f, 0.9019608f, 0.98039216f);
        makeBalls(this.mLevel);
        this.mScene.registerUpdateHandler(this.mPhysicsWorld);
        final Rectangle rectangle11 = rectangle9;
        final Rectangle rectangle12 = rectangle10;
        this.mScene.registerUpdateHandler(new IUpdateHandler() {
            public void reset() {
            }

            public void onUpdate(float pSecondsElapsed) {
                if (!ClassifyBallActivity.this.mFlagGameOver) {
                    if (ClassifyBallActivity.this.mFlagClear) {
                        ClassifyBallActivity.this.mFlagGameOver = true;
                        ClassifyBallActivity.this.makeGameOver();
                        ClassifyBallActivity.this.mScene.attachChild(ClassifyBallActivity.this.mGameoverShape);
                        ClassifyBallActivity.this.mScene.attachChild(ClassifyBallActivity.this.mTitleText);
                        ClassifyBallActivity.this.mScene.attachChild(ClassifyBallActivity.this.mStartText);
                        ClassifyBallActivity.this.mScene.registerTouchArea(ClassifyBallActivity.this.mStartText);
                        return;
                    }
                    for (Sprite sprite1 : ClassifyBallActivity.this.mListSpriteBall1) {
                        if (sprite1.collidesWith(rectangle11)) {
                            ClassifyBallActivity.this.mFlagClear = true;
                        } else {
                            ClassifyBallActivity.this.mFlagClear = false;
                            return;
                        }
                    }
                    for (Sprite sprite2 : ClassifyBallActivity.this.mListSpriteBall2) {
                        if (sprite2.collidesWith(rectangle12)) {
                            ClassifyBallActivity.this.mFlagClear = true;
                        } else {
                            ClassifyBallActivity.this.mFlagClear = false;
                            return;
                        }
                    }
                }
            }
        });
        return this.mScene;
    }

    /* access modifiers changed from: private */
    public void makeGameOver() {
        this.mGameoverShape = new Rectangle(0.0f, 0.0f, 720.0f, 480.0f);
        this.mGameoverShape.setColor(0.0f, 0.0f, 0.0f);
        this.mGameoverShape.setAlpha(0.7f);
        this.mTitleText = new Text(0.0f, 0.0f, this.mFont, "LEVEL " + this.mLevel + " CLEAR!!", HorizontalAlign.CENTER);
        this.mTitleText.setPosition((720.0f - this.mTitleText.getWidth()) * 0.5f, ((480.0f - this.mTitleText.getHeight()) * 0.5f) - 50.0f);
        this.mTitleText.registerEntityModifier(new ScaleModifier(1.0f, 0.0f, 1.0f));
        this.mStartText = new Text(0.0f, 0.0f, this.mFont, "NEXT", HorizontalAlign.CENTER) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() == 1) {
                    ClassifyBallActivity.this.runOnUpdateThread(new Runnable() {
                        public void run() {
                            ClassifyBallActivity.this.initGame();
                        }
                    });
                }
                return true;
            }
        };
        this.mStartText.setPosition((720.0f - this.mStartText.getWidth()) * 0.5f, ((480.0f - this.mTitleText.getHeight()) * 0.5f) + 50.0f);
        this.mStartText.registerEntityModifier(new ScaleModifier(1.0f, 0.0f, 0.6f));
    }

    /* access modifiers changed from: private */
    public void initGame() {
        this.mScene.detachChild(this.mGameoverShape);
        this.mScene.detachChild(this.mTitleText);
        this.mScene.unregisterTouchArea(this.mStartText);
        this.mScene.detachChild(this.mStartText);
        for (Sprite sprite1 : this.mListSpriteBall1) {
            removeFace(sprite1);
        }
        for (Sprite sprite2 : this.mListSpriteBall2) {
            removeFace(sprite2);
        }
        this.mListSpriteBall1.clear();
        this.mListSpriteBall2.clear();
        this.mListSpriteBallBody1.clear();
        this.mListSpriteBallBody2.clear();
        this.mFlagClear = false;
        this.mFlagGameOver = false;
        this.mLevel++;
        makeBalls(this.mLevel);
    }

    private void removeFace(Sprite face) {
        PhysicsConnector facePhysicsConnector = this.mPhysicsWorld.getPhysicsConnectorManager().findPhysicsConnectorByShape(face);
        this.mPhysicsWorld.unregisterPhysicsConnector(facePhysicsConnector);
        this.mPhysicsWorld.destroyBody(facePhysicsConnector.getBody());
        this.mScene.unregisterTouchArea(face);
        this.mScene.detachChild(face);
    }

    private void makeBalls(int ballNum) {
        for (int i = 0; i < ballNum; i++) {
            Sprite sprite2 = new Sprite(50.0f, 50.0f, this.mBall1Region2);
            this.mListSpriteBall2.add(sprite2);
            Body bodyBall2 = PhysicsFactory.createCircleBody(this.mPhysicsWorld, sprite2, BodyDef.BodyType.DynamicBody, FIXTURE_DEF);
            this.mListSpriteBallBody2.add(bodyBall2);
            this.mScene.attachChild(sprite2);
            this.mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(sprite2, bodyBall2, true, true));
        }
        for (int i2 = 0; i2 < ballNum; i2++) {
            Sprite sprite1 = new Sprite(50.0f, 50.0f, this.mBall1Region1);
            this.mListSpriteBall1.add(sprite1);
            Body bodyBall1 = PhysicsFactory.createCircleBody(this.mPhysicsWorld, sprite1, BodyDef.BodyType.DynamicBody, FIXTURE_DEF);
            this.mListSpriteBallBody1.add(bodyBall1);
            this.mScene.attachChild(sprite1);
            this.mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(sprite1, bodyBall1, true, true));
        }
    }

    public void onLoadComplete() {
    }

    public void onAccelerometerChanged(AccelerometerData pAccelerometerData) {
        if (!this.mFlagGameOver) {
            Vector2 gravity = Vector2Pool.obtain(pAccelerometerData.getX(), pAccelerometerData.getY());
            this.mPhysicsWorld.setGravity(gravity);
            Vector2Pool.recycle(gravity);
        }
    }

    public void onResumeGame() {
        super.onResumeGame();
        enableAccelerometerSensor(this);
    }

    public void onPauseGame() {
        super.onPauseGame();
        disableAccelerometerSensor();
    }
}
