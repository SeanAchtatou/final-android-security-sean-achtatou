package com.aquos.blockpopperlite;

import android.graphics.Point;
import com.aquos.blockpopperlite.Globals;
import java.lang.reflect.Array;
import java.util.LinkedList;
import java.util.List;

public class BlockOperations {
    private static Globals.BlockType bt;
    private static int i;
    private static int j;
    private static int k;
    private static int l;
    private static int m;
    private static int maxComboCounter;
    private static int p1;
    private static int p2;
    private static List<Point> points;
    private static Boolean somethingExploded;
    private static Point tPointA;
    private static Point tPointB;
    private static Tile tTileA;

    public static Tile[][] makeInitialBlocks() {
        int numGoldBlock = 0;
        int numGarbageBlock = 0;
        Tile[][] result = (Tile[][]) Array.newInstance(Tile.class, Globals.MAXBLOCKHORIZONTAL, Globals.MAXBLOCKVERTICAL);
        i = 0;
        while (i < Globals.MAXBLOCKHORIZONTAL) {
            j = 0;
            while (j < Globals.MAXBLOCKVERTICAL) {
                if (j > Globals.MAXBLOCKVERTICAL / 2) {
                    if (numGoldBlock >= 2 && numGarbageBlock > 2) {
                        result[i][j] = new Tile(Globals.getRandomBlockType(Globals.BlockType.gold, Globals.BlockType.garbage, true), i * Globals.TILESX, j * Globals.TILESY, -1);
                    } else if (numGoldBlock >= 2) {
                        result[i][j] = new Tile(Globals.getRandomBlockType(Globals.BlockType.gold, null, true), i * Globals.TILESX, j * Globals.TILESY, -1);
                    } else if (numGarbageBlock >= 2) {
                        result[i][j] = new Tile(Globals.getRandomBlockType(Globals.BlockType.garbage, null, true), i * Globals.TILESX, j * Globals.TILESY, -1);
                    } else {
                        result[i][j] = new Tile(Globals.getRandomBlockType(), i * Globals.TILESX, j * Globals.TILESY, -1);
                    }
                    if (result[i][j].blockType == Globals.BlockType.gold) {
                        numGoldBlock++;
                    }
                    if (result[i][j].blockType == Globals.BlockType.garbage) {
                        numGarbageBlock++;
                    }
                } else {
                    result[i][j] = new Tile(i * Globals.TILESX, j * Globals.TILESY, -1);
                }
                j++;
            }
            i++;
        }
        return result;
    }

    public static void makeLoseTile(Tile[][] t, int row, List<Tile> explodeSequence) {
        if (row >= 0 && row < Globals.MAXBLOCKVERTICAL) {
            i = 0;
            while (i < Globals.MAXBLOCKHORIZONTAL) {
                if (t[i][row].isAlive()) {
                    t[i][row].loseTile();
                    explodeSequence.add(t[i][row]);
                }
                i++;
            }
        }
    }

    public static void randomExplode(List<Tile> explodeSequence) {
        if (!explodeSequence.isEmpty()) {
            i = Globals.rand.nextInt(explodeSequence.size());
            if (explodeSequence.get(i).isAlive() && !explodeSequence.get(i).isMoving) {
                explodeSequence.get(i).isExploding = true;
                explodeSequence.remove(i);
            }
        }
    }

    public static int findTopRow(Tile[][] t) {
        j = 0;
        while (j < t[0].length) {
            i = 0;
            while (i < t.length) {
                if (!t[i][j].isRemoved) {
                    return j;
                }
                i++;
            }
            j++;
        }
        return j;
    }

    public static void resetComboCounter(Tile[][] t) {
        i = 0;
        while (i < t.length) {
            j = 0;
            while (j < t[0].length) {
                t[i][j].comboCounter = 0;
                t[i][j].nextComboCounter = 0;
                j++;
            }
            i++;
        }
    }

    public static void checkComboCounter(Tile[][] t) {
        i = 0;
        while (i < t.length) {
            checkComboCounter(t[i]);
            i++;
        }
    }

    private static void checkComboCounter(Tile[] t) {
        j = t.length - 1;
        while (j >= 0 && !t[j].isExploding && !t[j].isMovingVertical && !t[j].isDestroyed && !t[j].isRemoved && !t[j].verticalSwapPulse) {
            t[j].comboCounter = 0;
            t[j].nextComboCounter = 0;
            j--;
        }
    }

    public static void executeColorChange(Tile[][] t, Globals.BlockType blockFrom, Globals.BlockType blockTo) {
        for (Tile[] tt : t) {
            for (Tile ttt : t[r3]) {
                if (ttt.blockType == blockFrom) {
                    ttt.changeColor(blockTo);
                }
            }
        }
    }

    public static boolean isCleared(Tile[][] t) {
        for (Tile[] tt : t) {
            for (Tile ttt : t[r3]) {
                if (!ttt.isRemoved) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean isAllIdle(Tile[][] t) {
        for (Tile[] tt : t) {
            for (Tile ttt : t[r3]) {
                if (ttt.hasActivity() && !ttt.isRemoved) {
                    return false;
                }
            }
        }
        return true;
    }

    public static Tile[] createNewRow(Tile[][] t, int chanceGold, int chanceGarbage) {
        Tile[] result = new Tile[t.length];
        for (int i2 = 0; i2 < result.length; i2++) {
            Globals.BlockType exclude1 = findSimilarVertical(t, i2);
            Globals.BlockType exclude2 = findSimilarHorizontal(result, i2);
            if (exclude1 == Globals.BlockType.garbage) {
                exclude1 = null;
            }
            if (exclude2 == Globals.BlockType.gold) {
                exclude2 = null;
            }
            result[i2] = new Tile(Globals.getRandomBlockType(exclude1, exclude2, false), Globals.TILESX * i2, t[0].length * Globals.TILESY, Globals.totalShift);
        }
        i = 0;
        while (i < result.length) {
            if (!Globals.disableGarbageBlock && Globals.rand.nextInt(chanceGarbage) == 0) {
                result[i] = new Tile(Globals.BlockType.garbage, i * Globals.TILESX, t[0].length * Globals.TILESY, Globals.totalShift);
            } else if (!Globals.disableGoldBlock && Globals.rand.nextInt(chanceGold) == 0) {
                result[i] = new Tile(Globals.BlockType.gold, i * Globals.TILESX, t[0].length * Globals.TILESY, Globals.totalShift);
            }
            i++;
        }
        return result;
    }

    public static Globals.BlockType findSimilarVertical(Tile[][] t, int column) {
        Globals.BlockType bt2 = t[column][t[0].length - 1].blockType;
        j = 2;
        while (j <= 2) {
            if (t[column][t[0].length - j].blockType != bt2) {
                return null;
            }
            j++;
        }
        return bt2;
    }

    public static Globals.BlockType findSimilarHorizontal(Tile[] t, int row) {
        if (row < 2) {
            return null;
        }
        Globals.BlockType bt2 = t[row - 1].blockType;
        i = 2;
        while (i < 2) {
            if (t[row - i].blockType != bt2) {
                return null;
            }
            i++;
        }
        return bt2;
    }

    public static boolean executeSwap(int diffx, Point selectedTile, Tile[][] t) {
        Point switchTile;
        if (selectedTile != null) {
            if (diffx > Globals.TILESX / 4) {
                Point switchTile2 = findRightTile(selectedTile);
                if (switchTile2 != null) {
                    return swapTileHorizontal(selectedTile, switchTile2, t);
                }
            } else if (diffx < (-Globals.TILESX) / 4 && (switchTile = findLeftTile(selectedTile)) != null) {
                return swapTileHorizontal(selectedTile, switchTile, t);
            }
        }
        return false;
    }

    public static boolean specialPopTile(Point a, Tile[][] t, List<Text> comboText) {
        if (!t[a.x][a.y].isSelectable()) {
            return false;
        }
        m = 1;
        if (!t[a.x][a.y].isExploding) {
            t[a.x][a.y].isExploding = true;
            Globals.reducePowerUp(100);
            if (Globals.gameMode == 2) {
                Globals.clearPowerUp();
            }
            return true;
        }
        k = ((a.x * Globals.TILESX) / m) + (Globals.TILESX / 2) + Globals.BLOCKLEFTOFFSET;
        l = (((a.y * Globals.TILESY) - (Globals.TILESY / 2)) + Globals.BLOCKTOPOFFSET) - ((int) Globals.scrollOffset);
        comboText.add(new Text((float) k, (float) l, m * 10, m, 1));
        return false;
    }

    public static boolean specialBombTile(Point a, Tile[][] t, List<Text> comboText) {
        if (!t[a.x][a.y].isSelectable()) {
            return false;
        }
        m = 0;
        i = a.x - 1;
        while (i <= a.x + 1) {
            j = a.y - 1;
            while (j <= a.y + 1) {
                if (i >= 0 && i < Globals.MAXBLOCKHORIZONTAL && j >= 0 && j < Globals.MAXBLOCKVERTICAL && !t[i][j].isExploding && !t[i][j].isRemoved) {
                    m++;
                    t[i][j].isExploding = true;
                }
                j++;
            }
            i++;
        }
        k = (a.x * Globals.TILESX) + (Globals.TILESX / 2) + Globals.BLOCKLEFTOFFSET;
        l = (((a.y * Globals.TILESY) - (Globals.TILESY / 2)) + Globals.BLOCKTOPOFFSET) - ((int) Globals.scrollOffset);
        comboText.add(new Text((float) k, (float) l, m * 10, m, 1));
        Globals.reducePowerUp(Globals.EXPLODEPOWER);
        if (Globals.gameMode == 2) {
            Globals.clearPowerUp();
        }
        return true;
    }

    public static boolean swapTileHorizontal(Point a, Point b, Tile[][] t) {
        t[a.x][a.y].checkGarbageError();
        t[b.x][b.y].checkGarbageError();
        if (!t[a.x][a.y].isDraggable() || !t[b.x][b.y].isDraggable()) {
            return false;
        }
        tPointA = t[b.x][b.y].dest;
        tPointB = t[a.x][a.y].dest;
        t[a.x][a.y].move(tPointA);
        t[b.x][b.y].move(tPointB);
        tTileA = t[a.x][a.y];
        t[a.x][a.y] = t[b.x][b.y];
        t[b.x][b.y] = tTileA;
        t[a.x][a.y].isMovingHorizontal = true;
        t[b.x][b.y].isMovingHorizontal = true;
        t[a.x][a.y].comboCounter = 0;
        t[a.x][a.y].nextComboCounter = 0;
        t[b.x][b.y].comboCounter = 0;
        t[b.x][b.y].nextComboCounter = 0;
        InfoBar.reduceMoves();
        return true;
    }

    public static void swapTileVertical(Point a, Point b, Tile[][] t) {
        tPointA = t[b.x][b.y].dest;
        tPointB = t[a.x][a.y].dest;
        t[a.x][a.y].move(tPointA);
        t[b.x][b.y].move(tPointB);
        tTileA = t[a.x][a.y];
        t[a.x][a.y] = t[b.x][b.y];
        t[b.x][b.y] = tTileA;
        t[a.x][a.y].verticalSwapPulse = true;
        t[b.x][b.y].verticalSwapPulse = true;
        if (Globals.selectedTile != null && Globals.selectedTile.x == a.x && Globals.selectedTile.y == a.y) {
            Globals.selectedTile.x = b.x;
            Globals.selectedTile.y = b.y;
        } else if (Globals.selectedTile != null && Globals.selectedTile.x == b.x && Globals.selectedTile.y == b.y) {
            Globals.selectedTile.x = a.x;
            Globals.selectedTile.y = a.y;
        }
    }

    public static Point findClickedTile(int x, int y, float scrollOffset) {
        int y2 = (int) (((float) y) + scrollOffset);
        if (x < 0 || x > Globals.MAXBLOCKHORIZONTAL * Globals.TILESX || y2 < 0 || y2 > Globals.MAXBLOCKVERTICAL * Globals.TILESY) {
            return null;
        }
        Point result = new Point();
        result.x = x / Globals.TILESX;
        result.y = y2 / Globals.TILESY;
        return result;
    }

    public static boolean isBeside(Point a, Point b) {
        if (Math.abs(a.x - b.x) == 1 && a.y == b.y) {
            return true;
        }
        return false;
    }

    public static Point findLeftTile(Point center) {
        if (center.x - 1 >= 0) {
            return new Point(center.x - 1, center.y);
        }
        return null;
    }

    public static Point findRightTile(Point center) {
        if (center.x + 1 < Globals.MAXBLOCKHORIZONTAL) {
            return new Point(center.x + 1, center.y);
        }
        return null;
    }

    public static void selectTile(Point p, Tile[][] t) {
        if (p != null && p.x >= 0 && p.x < Globals.MAXBLOCKHORIZONTAL && p.y >= 0 && p.y < Globals.MAXBLOCKVERTICAL) {
            deselectTile(t);
            if (t[p.x][p.y].isSelectable()) {
                t[p.x][p.y].isSelected = true;
                Globals.selectedTile = p;
                SoundHandler.playWave(SoundHandler.swapSound);
                return;
            }
            t[p.x][p.y].checkGarbageError();
            Globals.selectedTile = null;
        }
    }

    public static void deselectTile(Tile[][] t) {
        if (Globals.selectedTile != null) {
            if (Globals.selectedTile.x >= Globals.MAXBLOCKHORIZONTAL || Globals.selectedTile.x < 0 || Globals.selectedTile.y >= Globals.MAXBLOCKVERTICAL || Globals.selectedTile.y < 0) {
                Globals.selectedTile = null;
                return;
            }
            t[Globals.selectedTile.x][Globals.selectedTile.y].isSelected = false;
            Globals.selectedTile = null;
        }
    }

    public static void clearAllSelected(Tile[][] ttt) {
        for (Tile[] tt : ttt) {
            for (Tile t : ttt[r3]) {
                t.isSelected = false;
            }
        }
        Globals.selectedTile = null;
    }

    public static boolean checkForGravity(Tile[][] t, boolean quickDrop) {
        boolean somethingDropped = false;
        i = 0;
        while (i < t.length) {
            p1 = t[0].length - 1;
            j = t[0].length - 1;
            while (j >= 0) {
                boolean empty = t[i][j].isRemoved;
                if (quickDrop) {
                    empty = t[i][j].isDestroyed;
                }
                if (t[i][j] != null && !empty) {
                    if (p1 - j >= 1) {
                        swapTileVertical(new Point(i, j), new Point(i, p1), t);
                        somethingDropped = true;
                    }
                    p1--;
                }
                j--;
            }
            i++;
        }
        return somethingDropped;
    }

    public static boolean checkForExplosion(Tile[][] t, List<Text> comboText) {
        points = new LinkedList();
        p1 = 0;
        p2 = 0;
        maxComboCounter = 0;
        somethingExploded = false;
        bt = null;
        i = 0;
        while (i < t[0].length) {
            p1 = 0;
            p2 = 0;
            bt = null;
            j = 0;
            while (j < t.length + 1) {
                p2 = j;
                if (j >= t.length || t[j][i].isDestroyed || t[j][i].isExploding || t[j][i].blockType != bt || t[j][i].blockType == Globals.BlockType.garbage) {
                    p2 = j;
                    maxComboCounter = 0;
                    if (p2 - p1 >= 3) {
                        k = p1;
                        while (k < p2) {
                            points.add(new Point(k, i));
                            if (t[k][i].comboCounter > maxComboCounter) {
                                maxComboCounter = t[k][i].comboCounter;
                            }
                            k++;
                        }
                        l = p1;
                        while (l < p2) {
                            m = i - 1;
                            while (m >= 0) {
                                t[l][m].nextComboCounter = maxComboCounter + 1;
                                m--;
                            }
                            l++;
                        }
                        somethingExploded = true;
                    }
                    if (j >= t.length || t[j][i].isDestroyed) {
                        bt = null;
                        p1 = j;
                    } else {
                        bt = t[j][i].blockType;
                        p1 = j;
                    }
                }
                j++;
            }
            i++;
        }
        i = 0;
        while (i < t.length) {
            p1 = 0;
            p2 = 0;
            bt = null;
            j = 0;
            while (j < t[0].length + 1) {
                p2 = j;
                if (j >= t[0].length || t[i][j].isDestroyed || t[i][j].isExploding || t[i][j].blockType != bt || t[i][j].blockType == Globals.BlockType.garbage) {
                    p2 = j;
                    if (p2 - p1 >= 3) {
                        k = p1;
                        while (k < p2) {
                            points.add(new Point(i, k));
                            if (t[i][k].comboCounter > maxComboCounter) {
                                maxComboCounter = t[i][k].comboCounter;
                            }
                            k++;
                        }
                        l = p1 - 1;
                        while (l >= 0) {
                            t[i][l].nextComboCounter = maxComboCounter + 1;
                            l--;
                        }
                        somethingExploded = true;
                    }
                    if (j >= t[0].length || t[i][j].isDestroyed) {
                        bt = null;
                        p1 = j;
                    } else {
                        bt = t[i][j].blockType;
                        p1 = j;
                    }
                }
                j++;
            }
            i++;
        }
        j = 0;
        k = 0;
        l = 9999;
        m = points.size();
        maxComboCounter = 0;
        i = 0;
        while (i < m) {
            tPointA = points.get(i);
            if (!t[tPointA.x][tPointA.y].isExploding) {
                if (t[tPointA.x][tPointA.y].blockType == Globals.BlockType.gold) {
                    t[tPointA.x][tPointA.y].powerupPending = true;
                }
                k += tPointA.x;
                if (tPointA.y < l) {
                    l = tPointA.y;
                }
                t[tPointA.x][tPointA.y].isExploding = true;
                if (t[tPointA.x][tPointA.y].comboCounter > maxComboCounter) {
                    maxComboCounter = t[tPointA.x][tPointA.y].comboCounter;
                }
                j++;
            }
            i++;
        }
        i = 0;
        while (i < m) {
            tPointA = points.get(i);
            makeNearbyLookAtMe(t, tPointA.x, tPointA.y);
            makeNearbyGarbageExplode(t, tPointA.x, tPointA.y);
            i++;
        }
        if (m > 0) {
            k = ((k * Globals.TILESX) / m) + (Globals.TILESX / 2) + Globals.BLOCKLEFTOFFSET;
            l = (((l * Globals.TILESY) - (Globals.TILESY / 2)) + Globals.BLOCKTOPOFFSET) - ((int) Globals.scrollOffset);
        }
        if (somethingExploded.booleanValue()) {
            if (maxComboCounter >= 1 && j >= 4) {
                comboText.add(new Text((float) k, (float) l, m * 10 * (maxComboCounter + 1), j, maxComboCounter + 1));
                int stopTimer = m * (maxComboCounter + 1) * 10;
                if (stopTimer > 120) {
                    stopTimer = 120;
                }
                InfoBar.incrementStopTimer(stopTimer);
                InfoBar.level_PERFORM_CHAINS++;
                InfoBar.level_PERFORM_COMBO++;
                InfoBar.level_PERFORM_MATCHES++;
            } else if (maxComboCounter >= 1) {
                comboText.add(new Text((float) k, (float) l, m * 10 * (maxComboCounter + 1), j, maxComboCounter + 1));
                int stopTimer2 = m * (maxComboCounter + 1) * 10;
                if (stopTimer2 > 120) {
                    stopTimer2 = 120;
                }
                InfoBar.incrementStopTimer(stopTimer2);
                InfoBar.level_PERFORM_CHAINS++;
                InfoBar.level_PERFORM_MATCHES++;
            } else if (j >= 4) {
                comboText.add(new Text((float) k, (float) l, m * 10 * (maxComboCounter + 1), j, maxComboCounter + 1));
                int stopTimer3 = m * (maxComboCounter + 1) * 10;
                if (stopTimer3 > 120) {
                    stopTimer3 = 120;
                }
                InfoBar.incrementStopTimer(stopTimer3);
                InfoBar.level_PERFORM_COMBO++;
                InfoBar.level_PERFORM_MATCHES++;
            } else {
                comboText.add(new Text((float) k, (float) l, m * 10 * (maxComboCounter + 1), j, maxComboCounter + 1));
                int stopTimer4 = m * (maxComboCounter + 1) * 10;
                if (stopTimer4 > 120) {
                    stopTimer4 = 120;
                }
                InfoBar.incrementStopTimer(stopTimer4);
                InfoBar.level_PERFORM_MATCHES++;
            }
            SoundHandler.playWave(SoundHandler.matchSound);
        }
        checkComboCounter(t);
        return somethingExploded.booleanValue();
    }

    private static void makeNearbyLookAtMe(Tile[][] t, int x, int y) {
        if (x >= 0 && x < Globals.MAXBLOCKHORIZONTAL && y >= 0 && y < Globals.MAXBLOCKVERTICAL) {
            if (x - 1 >= 0) {
                t[x - 1][y].forceLook(Globals.LOOKDIR.RIGHT);
            }
            if (x + 1 < Globals.MAXBLOCKHORIZONTAL) {
                t[x + 1][y].forceLook(Globals.LOOKDIR.LEFT);
            }
            if (y + 1 < Globals.MAXBLOCKVERTICAL) {
                t[x][y + 1].forceLook(Globals.LOOKDIR.UP);
            }
        }
    }

    private static void makeNearbyGarbageExplode(Tile[][] t, int x, int y) {
        if (x >= 0 && x < Globals.MAXBLOCKHORIZONTAL && y >= 0 && y < Globals.MAXBLOCKVERTICAL) {
            if (x - 1 >= 0 && t[x - 1][y].blockType == Globals.BlockType.garbage && !t[x - 1][y].isExploding && !t[x - 1][y].isRemoved) {
                InfoBar.level_BREAK_GARBAGE++;
                t[x - 1][y].isExploding = true;
            }
            if (x + 1 < Globals.MAXBLOCKHORIZONTAL && t[x + 1][y].blockType == Globals.BlockType.garbage && !t[x + 1][y].isExploding && !t[x + 1][y].isRemoved) {
                InfoBar.level_BREAK_GARBAGE++;
                t[x + 1][y].isExploding = true;
            }
            if (y - 1 >= 0 && t[x][y - 1].blockType == Globals.BlockType.garbage && !t[x][y - 1].isExploding && !t[x][y - 1].isRemoved) {
                InfoBar.level_BREAK_GARBAGE++;
                t[x][y - 1].isExploding = true;
            }
            if (y + 1 < Globals.MAXBLOCKVERTICAL && t[x][y + 1].blockType == Globals.BlockType.garbage && !t[x][y + 1].isExploding && !t[x][y + 1].isRemoved) {
                InfoBar.level_BREAK_GARBAGE++;
                t[x][y + 1].isExploding = true;
            }
        }
    }
}
