package com.aquos.blockpopperlite;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class ScreenCharSelect {
    private static CustomButton buttonConfirm;
    private static CustomButton buttonLeft;
    private static CustomButton buttonRight;
    private static final String[][] cardDesc = {new String[]{"Tim likes to dig, bash, and destroy", "stuff. He will challenge the", "thousand-eyed beast because he", "especially loves killing cute", "animals."}, new String[]{"Bob has only one useful skill -", "hunting wild beastes with his bow.", "He wants to kill the thousand-", "eyed beast and sell its skin at the", "flea market."}, new String[]{"Sam has the amazing ability to", "stop time. Since he has too much", "time to spare, he decided to", "travel around the world and take", "on every adventure."}};
    private static Card[] cards;
    private static int currentShowing = 1;
    private static int i;
    private static int screenResult = 0;

    public static void initialize() {
        cards = new Card[3];
        cards[0] = new Card(0, 1);
        cards[1] = new Card(1, 2);
        cards[2] = new Card(2, 3);
        buttonLeft = new CustomButton(ImageHandler.charLeft[0], ImageHandler.charLeft[1], 30, 600, 142, 122, false, SoundHandler.slideSound);
        buttonRight = new CustomButton(ImageHandler.charRight[0], ImageHandler.charRight[1], MessageHandler.ABILITYX3, 600, 142, 122, false, SoundHandler.slideSound);
        buttonConfirm = new CustomButton(ImageHandler.levelSelectButton[0], ImageHandler.levelSelectButton[1], 175, 580, 140, 140, false, SoundHandler.buttonSound);
    }

    public static void update() {
        for (Card update : cards) {
            update.update();
        }
        if (buttonConfirm.checkTouchUp()) {
            screenResult = currentShowing;
        }
        if (buttonLeft.checkTouchUp()) {
            currentShowing++;
            if (currentShowing >= 4) {
                currentShowing = 1;
            }
            changeRanks();
        }
        if (buttonRight.checkTouchUp()) {
            currentShowing--;
            if (currentShowing <= 0) {
                currentShowing = 3;
            }
            changeRanks();
        }
    }

    private static void changeRanks() {
        if (currentShowing == 1) {
            cards[0].changeRank(0);
            cards[1].changeRank(1);
            cards[2].changeRank(2);
        } else if (currentShowing == 2) {
            cards[0].changeRank(2);
            cards[1].changeRank(0);
            cards[2].changeRank(1);
        } else {
            cards[0].changeRank(1);
            cards[1].changeRank(2);
            cards[2].changeRank(0);
        }
    }

    public static int getResult() {
        if (Globals.checkUnlocked(screenResult)) {
            i = screenResult;
            screenResult = 0;
            return i;
        }
        screenResult = 0;
        return 0;
    }

    public static void processTouchEvents(int eventAction, float touchX, float touchY) {
        buttonLeft.checkPress(touchX, touchY, eventAction);
        buttonRight.checkPress(touchX, touchY, eventAction);
        buttonConfirm.checkPress(touchX, touchY, eventAction);
    }

    public static void draw(Canvas canvas) {
        canvas.drawColor(Color.argb(255, 204, 221, 229));
        canvas.drawBitmap(ImageHandler.charSelectText, 10.0f, ((float) (Globals.GAMEY - 90)) * Globals.SCALEY, (Paint) null);
        buttonLeft.draw(canvas);
        buttonRight.draw(canvas);
        buttonConfirm.draw(canvas);
        for (int i2 = 0; i2 < cards.length; i2++) {
            if (cards[i2].getRank() != 0) {
                cards[i2].draw(canvas);
            }
        }
        for (int i3 = 0; i3 < cards.length; i3++) {
            if (cards[i3].getRank() == 0) {
                cards[i3].draw(canvas);
                if (cards[i3].getReady()) {
                    drawText(canvas);
                }
            }
        }
        if (Globals.checkUnlocked(currentShowing)) {
            canvas.drawText("GO", ((float) (buttonConfirm.getX() + 68)) * Globals.SCALEX, ((float) (buttonConfirm.getY() + 90)) * Globals.SCALEY, ImageHandler.fontLevelSel);
            return;
        }
        ImageHandler.fontLevelLock.setAlpha(255);
        canvas.drawBitmap(ImageHandler.padlock, ((float) (buttonConfirm.getX() + 30)) * Globals.SCALEX, ((float) (buttonConfirm.getY() + 15)) * Globals.SCALEY, (Paint) null);
        if (currentShowing == 2) {
            canvas.drawText("Beat level 8 on Miner to unlock", ((float) (Globals.GAMEX / 2)) * Globals.SCALEX, Globals.SCALEY * 50.0f, ImageHandler.fontLevelLock);
        }
        if (currentShowing == 3) {
            canvas.drawText("Only available in full version", ((float) (Globals.GAMEX / 2)) * Globals.SCALEX, Globals.SCALEY * 50.0f, ImageHandler.fontLevelLock);
        }
    }

    private static void drawText(Canvas canvas) {
        ImageHandler.fontText.setTextSize(16.0f * Globals.SCALEX);
        ImageHandler.fontText.setAlpha(255);
        int j = 430;
        for (String drawText : cardDesc[currentShowing - 1]) {
            canvas.drawText(drawText, 120.0f * Globals.SCALEX, ((float) j) * Globals.SCALEY, ImageHandler.fontText);
            j += 20;
        }
    }
}
