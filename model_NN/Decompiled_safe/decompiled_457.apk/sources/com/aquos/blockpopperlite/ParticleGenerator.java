package com.aquos.blockpopperlite;

import android.graphics.Canvas;
import java.util.LinkedList;
import java.util.Queue;

public class ParticleGenerator {
    public static int MAXSIZE = 20;
    private static int i;
    private static float l;
    private static float m;
    private static float n;
    private boolean needGC = false;
    private Queue<Particle> particles = new LinkedList();
    private boolean somethingActive = false;

    public void destroy() {
        this.particles = null;
    }

    public Particle removeParticle() {
        return this.particles.remove();
    }

    public void addParticle(int locx, int locy, float velx, float vely, float scale, int type) {
        if (this.particles.size() >= MAXSIZE) {
            Particle pa = this.particles.remove();
            pa.reset(locx, locy, velx, vely, scale, type);
            this.particles.add(pa);
            return;
        }
        this.particles.add(new Particle(locx, locy, velx, vely, scale, type));
    }

    public void update() {
        this.somethingActive = false;
        for (Particle p : this.particles) {
            p.update();
            if (p.isActive) {
                this.somethingActive = true;
                this.needGC = true;
            }
        }
        if (this.needGC && !this.somethingActive) {
            System.gc();
            this.needGC = false;
        }
    }

    public void draw(Canvas c) {
        for (Particle p : this.particles) {
            p.draw(c);
        }
    }

    public void pulse(int locx, int locy, int color) {
        i = 0;
        while (i < 3) {
            l = (Globals.rand.nextFloat() * 60.0f) - 30.0f;
            m = (Globals.rand.nextFloat() * 60.0f) - 30.0f;
            n = (Globals.rand.nextFloat() * 0.3f) + 0.3f;
            addParticle(((int) l) + locx, ((int) m) + locy, l / 3.0f, m / 3.0f, n, 1);
            i++;
        }
    }

    public void star(int locx, int locy) {
        i = 0;
        while (i < 2) {
            l = (Globals.rand.nextFloat() * 60.0f) - 30.0f;
            m = (Globals.rand.nextFloat() * 60.0f) - 30.0f;
            n = (Globals.rand.nextFloat() * 0.3f) + 0.3f;
            addParticle(((int) l) + locx, ((int) m) + locy, l / 3.0f, m / 3.0f, 1.0f, 2);
            i++;
        }
    }
}
