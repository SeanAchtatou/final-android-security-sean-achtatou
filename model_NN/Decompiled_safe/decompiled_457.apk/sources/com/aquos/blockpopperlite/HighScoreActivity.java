package com.aquos.blockpopperlite;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.EditText;

public class HighScoreActivity extends Activity {
    HighScoreSurfaceView highScoreSurfaceView;
    EditText nameTextField;
    Button submitButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.highscore);
        this.highScoreSurfaceView = (HighScoreSurfaceView) findViewById(R.id.highScoreSurfaceView);
        this.submitButton = (Button) findViewById(R.id.submitButton);
        this.nameTextField = (EditText) findViewById(R.id.nameTextField);
        this.submitButton.setOnClickListener(new clickSubmitButton());
    }

    class clickSubmitButton implements View.OnClickListener {
        clickSubmitButton() {
        }

        public void onClick(View view) {
            ConnectionHandler.postHighScore(HighScoreActivity.this.nameTextField.getText().toString(), Globals.getScore());
        }
    }

    public void onStart() {
        super.onStart();
    }

    public void onRestart() {
        super.onRestart();
    }

    public void onResume() {
        super.onResume();
        this.highScoreSurfaceView.Initialize();
        this.nameTextField.setLayoutParams(new AbsoluteLayout.LayoutParams((int) (270.0f * Globals.SCALEX), (int) (70.0f * Globals.SCALEY), (int) (Globals.SCALEX * 10.0f), (int) (20.0f * Globals.SCALEY)));
        this.submitButton.setLayoutParams(new AbsoluteLayout.LayoutParams((int) (120.0f * Globals.SCALEX), (int) (60.0f * Globals.SCALEY), (int) (Globals.SCALEX * 10.0f), (int) (85.0f * Globals.SCALEY)));
    }

    public void onPause() {
        super.onPause();
        this.highScoreSurfaceView.Destroy();
        finish();
    }
}
