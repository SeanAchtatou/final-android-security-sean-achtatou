package com.aquos.blockpopperlite;

import android.graphics.Canvas;
import android.graphics.Matrix;

public class ColorSelector {
    private static int locx;
    private static int locy;
    private static boolean open;
    private static Matrix tmpMatrix;

    public static void initialize(int x, int y) {
        locx = x;
        locy = y;
        open = true;
        tmpMatrix = new Matrix();
    }

    public static void close() {
        open = false;
    }

    public static boolean isOpen() {
        return open;
    }

    public static void draw(Canvas canvas, int scrollOffset, int leftOffset, int topOffset) {
    }
}
