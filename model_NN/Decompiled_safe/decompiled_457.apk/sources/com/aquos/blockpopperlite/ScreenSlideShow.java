package com.aquos.blockpopperlite;

import android.graphics.Canvas;
import android.graphics.Paint;

public class ScreenSlideShow {
    private static final int ANISTATECLOSED = 0;
    private static final int ANISTATEOPENED = 2;
    private static final int ANISTATESWITCHINLEFT = 4;
    private static final int ANISTATESWITCHINRIGHT = 6;
    private static final int ANISTATESWITCHOUTLEFT = 3;
    private static final int ANISTATESWITCHOUTRIGHT = 5;
    private static final int NUMSLIDESBEGIN = 3;
    private static final int NUMSLIDESFINAL = 1;
    private static final float SLIDEFADESPEED = 100.0f;
    private static final int SLIDEPOSX = 11;
    private static final int SLIDEPOSY = 11;
    private static int aniCounter = 0;
    private static int aniState = 2;
    private static CustomButton bNext;
    private static CustomButton bPrevious;
    private static int buttonAlpha = 0;
    private static int i;
    private static boolean isFinalPage = false;
    private static int j;
    private static int page = 0;
    private static int screenResult = 0;
    private static float slidePosX = 0.0f;
    private static String[][] storyLine = {new String[]{"Long time ago, in the village", "of Alcatraz, there existed a", "thousand-eyed beast called", "Shamu."}, new String[]{"Although Shamu was a tame", "beast, the people had to kill", "it because it pooped all over", "the village and made a mess."}, new String[]{"Elders of Alcatraz gathered", "all villagers to find brave", "heroes to kill the beast...", ""}, new String[]{"At last, the great thousand-", "eyed beast was slained. The", "villagers lived happily ever", "after."}};

    public static void initialize(boolean finalPage) {
        if (finalPage) {
            isFinalPage = true;
            page = 3;
        } else {
            isFinalPage = false;
            page = 0;
        }
        slidePosX = 11.0f;
        aniState = 2;
        buttonAlpha = 0;
        screenResult = 0;
        bNext = new CustomButton(ImageHandler.charRight[0], ImageHandler.charRight[1], MessageHandler.ABILITYX3, 650, 142, 122, false, SoundHandler.slideSound);
        bPrevious = new CustomButton(ImageHandler.charLeft[0], ImageHandler.charLeft[1], 30, 650, 142, 122, false, SoundHandler.slideSound);
    }

    public static int getResult() {
        i = screenResult;
        screenResult = 0;
        return i;
    }

    public static void update() {
        if (page <= 0) {
            bPrevious.setVisible(false);
        } else if (isFinalPage) {
            bPrevious.setVisible(false);
        } else {
            bPrevious.setVisible(true);
        }
        updateAnimation();
    }

    private static void updateAnimation() {
        aniCounter++;
        if (aniCounter >= 40) {
            aniCounter = 0;
        }
        switch (aniState) {
            case 0:
                buttonAlpha = 0;
                return;
            case 1:
            default:
                return;
            case 2:
                buttonAlpha += 50;
                if (buttonAlpha >= 255) {
                    buttonAlpha = 255;
                }
                if (bPrevious.checkTouchUp()) {
                    aniState = 5;
                    aniCounter = 0;
                    return;
                } else if (bNext.checkTouchUp()) {
                    aniState = 3;
                    aniCounter = 0;
                    return;
                } else {
                    return;
                }
            case 3:
                slidePosX -= 100.0f;
                buttonAlpha -= 50;
                if (buttonAlpha <= 0) {
                    buttonAlpha = 0;
                }
                if (slidePosX < ((float) (-Globals.GAMEX)) - 100.0f) {
                    slidePosX = (float) Globals.GAMEX;
                    aniState = 4;
                    aniCounter = 0;
                    page++;
                    if (isFinalPage) {
                        if (page >= 1) {
                            page = 0;
                        }
                        screenResult = 1;
                        aniState = 0;
                        return;
                    } else if (page >= 3) {
                        page = 2;
                        screenResult = 3;
                        aniState = 0;
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 4:
                slidePosX -= 100.0f;
                if (slidePosX <= 11.0f) {
                    slidePosX = 11.0f;
                    aniState = 2;
                    return;
                }
                return;
            case 5:
                slidePosX += 100.0f;
                buttonAlpha -= 50;
                if (buttonAlpha <= 0) {
                    buttonAlpha = 0;
                }
                if (slidePosX > ((float) Globals.GAMEX) + 100.0f) {
                    slidePosX = (float) (-Globals.GAMEX);
                    aniState = 6;
                    aniCounter = 0;
                    page--;
                    if (!isFinalPage) {
                        if (page <= 0) {
                            page = 0;
                            return;
                        }
                        return;
                    } else if (page <= 3) {
                        page = 3;
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 6:
                slidePosX += 100.0f;
                if (slidePosX >= 11.0f) {
                    slidePosX = 11.0f;
                    aniState = 2;
                    return;
                }
                return;
        }
    }

    public static void processTouchEvents(int eventAction, float touchX, float touchY) {
        bNext.checkPress(touchX, touchY, eventAction);
        bPrevious.checkPress(touchX, touchY, eventAction);
    }

    public static void draw(Canvas canvas) {
        BackgroundHandler.draw(canvas, Globals.GAMEY, false);
        canvas.drawBitmap(ImageHandler.getSlideShowFrame(page + 1), slidePosX * Globals.SCALEX, 11.0f * Globals.SCALEY, (Paint) null);
        drawText(canvas);
        bNext.draw(canvas);
        bPrevious.draw(canvas);
    }

    private static void drawText(Canvas canvas) {
        ImageHandler.fontText.setTextSize(33.0f * Globals.SCALEX);
        ImageHandler.fontText.setAlpha(buttonAlpha);
        j = 175;
        i = storyLine[page].length - 1;
        while (i >= 0) {
            canvas.drawText(storyLine[page][i], 30.0f * Globals.SCALEX, ((float) (Globals.GAMEY - j)) * Globals.SCALEY, ImageHandler.fontText);
            j += 40;
            i--;
        }
    }
}
