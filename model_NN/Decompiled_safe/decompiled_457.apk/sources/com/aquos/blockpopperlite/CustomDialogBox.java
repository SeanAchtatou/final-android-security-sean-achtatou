package com.aquos.blockpopperlite;

import android.graphics.Canvas;
import android.graphics.Paint;

public class CustomDialogBox {
    public static int CHOOSE_NO = 0;
    public static int CHOOSE_NOTHING = -1;
    public static int CHOOSE_YES = 1;
    public static int DIALOG_CLOSE = 0;
    public static int DIALOG_LOSE = 1;
    public static int DIALOG_RATE = 3;
    public static int DIALOG_WIN = 2;
    public static CustomButton bno;
    public static CustomButton byes;
    private static String completeTime = "";
    private static int completeTimeInt = 0;
    public static int dialogType = DIALOG_CLOSE;

    public static void show(int type) {
        if (type == DIALOG_LOSE) {
            dialogType = DIALOG_LOSE;
            byes = new CustomButton(ImageHandler.levelSelectButton[0], ImageHandler.levelSelectButton[1], 50, 200, 140, 140, false, SoundHandler.buttonSound);
            bno = new CustomButton(ImageHandler.levelSelectButton[0], ImageHandler.levelSelectButton[1], 290, 200, 140, 140, false, SoundHandler.buttonSound);
            SoundHandler.playWave(SoundHandler.loseSound);
        }
        if (type == DIALOG_WIN) {
            dialogType = DIALOG_WIN;
            byes = new CustomButton(ImageHandler.levelSelectButton[0], ImageHandler.levelSelectButton[1], 290, 200, 140, 140, false, SoundHandler.buttonSound);
            bno = new CustomButton(ImageHandler.levelSelectButton[0], ImageHandler.levelSelectButton[1], 50, 200, 140, 140, false, SoundHandler.buttonSound);
            completeTime = convertTimeString(InfoBar.level_SURVIVE);
            completeTimeInt = (InfoBar.level_SURVIVE * Globals.FRAMESECOND) / 1000;
            SoundHandler.playWave(SoundHandler.winSound);
        }
        if (type == DIALOG_RATE) {
            dialogType = DIALOG_RATE;
            byes = new CustomButton(ImageHandler.levelSelectButton[0], ImageHandler.levelSelectButton[1], 290, 200, 140, 140, false, SoundHandler.buttonSound);
            bno = new CustomButton(ImageHandler.levelSelectButton[0], ImageHandler.levelSelectButton[1], 50, 200, 140, 140, false, SoundHandler.buttonSound);
        }
    }

    private static String convertTimeString(int frames) {
        int seconds = (Globals.FRAMESECOND * frames) / 1000;
        return (seconds / 60) + " minutes " + (seconds % 60) + " seconds";
    }

    public static int getClick(float touchx, float touchy, int event) {
        if (dialogType == DIALOG_CLOSE) {
            return CHOOSE_NOTHING;
        }
        byes.checkPress(touchx, touchy, event);
        bno.checkPress(touchx, touchy, event);
        if (byes.checkTouchUp()) {
            return CHOOSE_YES;
        }
        if (!bno.checkTouchUp() || (dialogType != DIALOG_LOSE && dialogType != DIALOG_RATE)) {
            return CHOOSE_NOTHING;
        }
        return CHOOSE_NO;
    }

    public static void hide() {
        dialogType = DIALOG_CLOSE;
    }

    public static void drawStars(Canvas canvas, int x, int y) {
        int numStars = 2;
        if (Globals.gameMode == 1) {
            if (((double) completeTimeInt) < ((double) LevelData.levelTimeSeconds[Globals.currentLevel - 1]) * 0.6d) {
                numStars = 3;
            } else if (completeTimeInt < LevelData.levelTimeSeconds[Globals.currentLevel - 1]) {
                numStars = 2;
            } else {
                numStars = 1;
            }
        } else if (Globals.gameMode == 2) {
            if (completeTimeInt < LevelData.puzzleParameter[Globals.currentLevel - 1] * 4) {
                numStars = 3;
            } else if (completeTimeInt < LevelData.puzzleParameter[Globals.currentLevel - 1] * 6) {
                numStars = 2;
            } else {
                numStars = 1;
            }
        }
        canvas.drawBitmap(ImageHandler.star[0], ((float) x) * Globals.SCALEX, ((float) y) * Globals.SCALEY, (Paint) null);
        if (numStars >= 2) {
            canvas.drawBitmap(ImageHandler.star[0], ((float) (x + 70)) * Globals.SCALEX, ((float) y) * Globals.SCALEY, (Paint) null);
        } else {
            canvas.drawBitmap(ImageHandler.star[1], ((float) (x + 70)) * Globals.SCALEX, ((float) y) * Globals.SCALEY, (Paint) null);
        }
        if (numStars >= 3) {
            canvas.drawBitmap(ImageHandler.star[0], ((float) (x + 140)) * Globals.SCALEX, ((float) y) * Globals.SCALEY, (Paint) null);
        } else {
            canvas.drawBitmap(ImageHandler.star[1], ((float) (x + 140)) * Globals.SCALEX, ((float) y) * Globals.SCALEY, (Paint) null);
        }
    }

    public static void draw(Canvas canvas) {
        if (dialogType != DIALOG_CLOSE) {
            if (dialogType == DIALOG_LOSE) {
                ImageHandler.fontLevelLock.setAlpha(255);
                canvas.drawBitmap(ImageHandler.messageBG, Globals.SCALEX * 40.0f, Globals.SCALEY * 100.0f, (Paint) null);
                canvas.drawText("YOU LOSE", ((float) (Globals.GAMEX / 2)) * Globals.SCALEX, Globals.SCALEY * 150.0f, ImageHandler.fontLevelLock);
                canvas.drawText("Would you like to retry?", ((float) (Globals.GAMEX / 2)) * Globals.SCALEX, 180.0f * Globals.SCALEY, ImageHandler.fontLevelLock);
                byes.draw(canvas);
                canvas.drawBitmap(ImageHandler.yes, ((float) (byes.getX() + 20)) * Globals.SCALEX, ((float) (byes.getY() + 20)) * Globals.SCALEY, (Paint) null);
                bno.draw(canvas);
                canvas.drawBitmap(ImageHandler.no, ((float) (bno.getX() + 20)) * Globals.SCALEX, ((float) (bno.getY() + 20)) * Globals.SCALEY, (Paint) null);
            } else if (dialogType == DIALOG_WIN) {
                ImageHandler.fontLevelLock.setAlpha(255);
                canvas.drawBitmap(ImageHandler.messageBG, Globals.SCALEX * 40.0f, Globals.SCALEY * 100.0f, (Paint) null);
                canvas.drawText("YOU WIN", ((float) (Globals.GAMEX / 2)) * Globals.SCALEX, Globals.SCALEY * 150.0f, ImageHandler.fontLevelLock);
                canvas.drawText("Time: " + completeTime, ((float) (Globals.GAMEX / 2)) * Globals.SCALEX, 180.0f * Globals.SCALEY, ImageHandler.fontLevelLock);
                byes.draw(canvas);
                canvas.drawBitmap(ImageHandler.yes, ((float) (byes.getX() + 20)) * Globals.SCALEX, ((float) (byes.getY() + 20)) * Globals.SCALEY, (Paint) null);
                drawStars(canvas, 70, 220);
            } else if (dialogType == DIALOG_RATE) {
                ImageHandler.fontLevelLock.setAlpha(255);
                canvas.drawBitmap(ImageHandler.messageBG, Globals.SCALEX * 40.0f, Globals.SCALEY * 100.0f, (Paint) null);
                canvas.drawText("Hope you enjoyed EyesBreaker", ((float) (Globals.GAMEX / 2)) * Globals.SCALEX, 125.0f * Globals.SCALEY, ImageHandler.fontLevelLock);
                canvas.drawText("Please take a moment to rate", ((float) (Globals.GAMEX / 2)) * Globals.SCALEX, 155.0f * Globals.SCALEY, ImageHandler.fontLevelLock);
                canvas.drawText("the game. Thank's for supporting", ((float) (Globals.GAMEX / 2)) * Globals.SCALEX, 185.0f * Globals.SCALEY, ImageHandler.fontLevelLock);
                byes.draw(canvas);
                canvas.drawBitmap(ImageHandler.yes, ((float) (byes.getX() + 20)) * Globals.SCALEX, ((float) (byes.getY() + 20)) * Globals.SCALEY, (Paint) null);
                bno.draw(canvas);
                canvas.drawBitmap(ImageHandler.no, ((float) (bno.getX() + 20)) * Globals.SCALEX, ((float) (bno.getY() + 20)) * Globals.SCALEY, (Paint) null);
            }
        }
    }
}
