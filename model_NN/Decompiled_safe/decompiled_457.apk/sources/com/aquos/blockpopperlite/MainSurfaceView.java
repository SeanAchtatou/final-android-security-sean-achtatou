package com.aquos.blockpopperlite;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MainSurfaceView extends SurfaceView implements SurfaceHolder.Callback {
    public static final int SCREEN_ENDLESS_CHAR_SELECT = 8;
    public static final int SCREEN_ENDLESS_GAME = 9;
    public static final int SCREEN_HIGH_SCORES = 10;
    public static final int SCREEN_LOAD = 0;
    public static final int SCREEN_LOAD_STATE = 11;
    public static final int SCREEN_MAIN_MENU = 1;
    public static final int SCREEN_PUZZLE_GAME = 7;
    public static final int SCREEN_PUZZLE_LEVEL_SELECT = 6;
    public static final int SCREEN_STORY_CHAR_SELECT = 3;
    public static final int SCREEN_STORY_FINISH = 12;
    public static final int SCREEN_STORY_GAME = 5;
    public static final int SCREEN_STORY_LEVEL_SELECT = 4;
    public static final int SCREEN_STORY_SLIDE_SHOW = 2;
    public static final int SCREEN_UNLOCK = 13;
    private static final int TRANSITION = 20;
    private static int i;
    private static int loadCounter = 0;
    private static Context mainContext;
    private static Paint transitionPaint;
    private MediaPlayer currentMusic = SoundHandler.mainTheme;
    private int currentScreen = 0;
    private boolean isSurfaceCreated = false;
    private MainThread mainThread;
    private int nextScreen = 0;
    private int transitionCount = 0;

    public MainSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().addCallback(this);
        setFocusable(false);
        mainContext = context;
    }

    public void Initialize() {
        if (this.mainThread == null) {
            this.mainThread = new MainThread(getHolder(), this);
            if (this.isSurfaceCreated) {
                this.mainThread.setRunning(true);
                this.mainThread.start();
            }
        }
        loadCounter = 0;
        transitionPaint = new Paint();
        transitionPaint.setColor(-16777216);
        this.currentScreen = 0;
        this.nextScreen = 0;
        FileHandler.readSettings(mainContext);
        FileHandler.writeSettings(mainContext);
    }

    public void Destroy() {
        this.mainThread.setRunning(false);
        this.mainThread.interrupt();
        this.mainThread = null;
        System.gc();
    }

    public int getCurrentScreen() {
        return this.currentScreen;
    }

    public void update() {
        switch (this.currentScreen) {
            case 0:
                this.currentMusic = SoundHandler.mainTheme;
                loadCounter++;
                if (loadCounter == 10) {
                    ImageHandler.Initialize(mainContext);
                    SoundHandler.Initialize(mainContext);
                    ScreenMainMenu.initialize();
                    ScreenGame.Initialize(mainContext, null);
                    ScreenGame.loadState();
                    this.nextScreen = 1;
                    this.transitionCount = 22;
                    break;
                }
                break;
            case 1:
                this.currentMusic = SoundHandler.mainTheme;
                ScreenMainMenu.update();
                i = ScreenMainMenu.getResult();
                if (i == 2) {
                    Globals.gameMode = 1;
                    ScreenSlideShow.initialize(false);
                    ScreenCharSelect.initialize();
                    ScreenLevelSelect.initialize(false);
                }
                if (i == 6) {
                    Globals.gameMode = 2;
                    ScreenLevelSelect.initialize(true);
                }
                if (i == 8) {
                    Globals.gameMode = 3;
                    ScreenCharSelect.initialize();
                    ScreenGame.Initialize(mainContext, null);
                }
                if (i == 2 || i == 6 || i == 8) {
                    this.nextScreen = i;
                    this.transitionCount = 40;
                    SoundHandler.fadeOutMusic();
                }
                if (i == 10) {
                    mainContext.startActivity(new Intent(mainContext, HighScoreActivity.class));
                }
                if (i == 13) {
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setData(Uri.parse("market://details?id=com.aquos.blockpopper"));
                    mainContext.startActivity(intent);
                }
                if (i == 11) {
                    ScreenGame.Initialize(mainContext, null);
                    ScreenGame.loadState();
                    this.nextScreen = 9;
                    this.transitionCount = 40;
                    SoundHandler.fadeOutMusic();
                    break;
                }
                break;
            case 2:
                this.currentMusic = SoundHandler.selectionTheme;
                ScreenSlideShow.update();
                i = ScreenSlideShow.getResult();
                if (i == 3) {
                    this.nextScreen = 3;
                    this.transitionCount = 40;
                    break;
                }
                break;
            case 3:
                this.currentMusic = SoundHandler.selectionTheme;
                ScreenCharSelect.update();
                i = ScreenCharSelect.getResult();
                if (i > 0) {
                    Globals.characterType = i;
                    ScreenLevelSelect.initialize(false);
                    this.nextScreen = 4;
                    this.transitionCount = 40;
                    break;
                }
                break;
            case 4:
                this.currentMusic = SoundHandler.selectionTheme;
                ScreenLevelSelect.update();
                i = ScreenLevelSelect.getResult();
                if (i > 0) {
                    Globals.currentLevel = i;
                    ScreenGame.Initialize(mainContext, null);
                    this.nextScreen = 5;
                    this.transitionCount = 40;
                    SoundHandler.fadeOutMusic();
                    break;
                }
                break;
            case 5:
                this.currentMusic = null;
                ScreenGame.update();
                i = ScreenGame.getResult();
                if (i != 1) {
                    if (i == 12) {
                        this.nextScreen = 12;
                        ScreenSlideShow.initialize(true);
                        this.transitionCount = 40;
                        break;
                    }
                } else {
                    backToMainMenu();
                    break;
                }
                break;
            case 6:
                this.currentMusic = SoundHandler.selectionTheme;
                ScreenLevelSelect.update();
                i = ScreenLevelSelect.getResult();
                if (i > 0) {
                    Globals.currentLevel = i;
                    ScreenGame.Initialize(mainContext, null);
                    this.nextScreen = 7;
                    SoundHandler.fadeOutMusic();
                    this.transitionCount = 40;
                    break;
                }
                break;
            case 7:
                this.currentMusic = null;
                ScreenGame.update();
                i = ScreenGame.getResult();
                if (i == 1) {
                    backToMainMenu();
                }
                if (i == 6) {
                    Globals.gameMode = 2;
                    ScreenLevelSelect.initialize(true);
                    this.nextScreen = 6;
                    this.transitionCount = 40;
                    break;
                }
                break;
            case SCREEN_ENDLESS_CHAR_SELECT /*8*/:
                this.currentMusic = SoundHandler.selectionTheme;
                ScreenCharSelect.update();
                i = ScreenCharSelect.getResult();
                if (i > 0) {
                    Globals.characterType = i;
                    this.nextScreen = 9;
                    SoundHandler.fadeOutMusic();
                    this.transitionCount = 40;
                    break;
                }
                break;
            case SCREEN_ENDLESS_GAME /*9*/:
                this.currentMusic = null;
                ScreenGame.update();
                i = ScreenGame.getResult();
                if (i != 1) {
                    if (i == 8) {
                        Globals.gameMode = 3;
                        ScreenCharSelect.initialize();
                        ScreenGame.Initialize(mainContext, null);
                        this.nextScreen = i;
                        this.transitionCount = 40;
                        break;
                    }
                } else {
                    backToMainMenu();
                    break;
                }
                break;
            case 10:
                this.currentMusic = SoundHandler.mainTheme;
                break;
            case SCREEN_STORY_FINISH /*12*/:
                this.currentMusic = SoundHandler.selectionTheme;
                ScreenSlideShow.update();
                i = ScreenSlideShow.getResult();
                if (i == 1) {
                    SoundHandler.fadeOutMusic();
                    backToMainMenu();
                    break;
                }
                break;
            case SCREEN_UNLOCK /*13*/:
                this.currentMusic = SoundHandler.mainTheme;
                break;
        }
        if (this.transitionCount > 0) {
            this.transitionCount--;
            if (this.transitionCount == 20) {
                this.currentScreen = this.nextScreen;
            }
        }
        SoundHandler.updateMusic(this.currentMusic);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 82) {
            return false;
        }
        processKeyEvent(keyCode, event);
        return true;
    }

    private void backToMainMenu() {
        this.nextScreen = 1;
        ScreenGame.loadState();
        this.transitionCount = 40;
    }

    public boolean processKeyEvent(int keyCode, KeyEvent msg) {
        if (keyCode == 24) {
            Globals.audio.adjustStreamVolume(3, 1, 1);
        }
        if (keyCode == 25) {
            Globals.audio.adjustStreamVolume(3, -1, 1);
        }
        switch (this.currentScreen) {
            case 2:
                if (keyCode == 4) {
                    backToMainMenu();
                    break;
                }
                break;
            case 3:
                if (keyCode == 4) {
                    backToMainMenu();
                    break;
                }
                break;
            case 4:
                if (keyCode == 4) {
                    backToMainMenu();
                    break;
                }
                break;
            case 5:
                return ScreenGame.processKeyEvent(keyCode, msg);
            case 6:
                if (keyCode == 4) {
                    backToMainMenu();
                    break;
                }
                break;
            case 7:
                return ScreenGame.processKeyEvent(keyCode, msg);
            case SCREEN_ENDLESS_CHAR_SELECT /*8*/:
                if (keyCode == 4) {
                    backToMainMenu();
                    break;
                }
                break;
            case SCREEN_ENDLESS_GAME /*9*/:
                return ScreenGame.processKeyEvent(keyCode, msg);
        }
        return true;
    }

    public boolean onTouchEvent(MotionEvent event) {
        processTouchEvents(event.getAction(), event.getX(), event.getY());
        try {
            Thread.sleep(50);
            return true;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return true;
        }
    }

    private void processTouchEvents(int eventAction, float touchX, float touchY) {
        float touchX2 = touchX / Globals.SCALEX;
        float touchY2 = touchY / Globals.SCALEY;
        switch (this.currentScreen) {
            case 0:
            case 10:
            case SCREEN_LOAD_STATE /*11*/:
            default:
                return;
            case 1:
                ScreenMainMenu.processTouchEvents(eventAction, touchX2, touchY2);
                return;
            case 2:
                ScreenSlideShow.processTouchEvents(eventAction, touchX2, touchY2);
                return;
            case 3:
                ScreenCharSelect.processTouchEvents(eventAction, touchX2, touchY2);
                return;
            case 4:
                ScreenLevelSelect.processTouchEvents(eventAction, touchX2, touchY2);
                return;
            case 5:
                ScreenGame.processTouchEvents(eventAction, touchX2, touchY2);
                return;
            case 6:
                ScreenLevelSelect.processTouchEvents(eventAction, touchX2, touchY2);
                return;
            case 7:
                ScreenGame.processTouchEvents(eventAction, touchX2, touchY2);
                return;
            case SCREEN_ENDLESS_CHAR_SELECT /*8*/:
                ScreenCharSelect.processTouchEvents(eventAction, touchX2, touchY2);
                return;
            case SCREEN_ENDLESS_GAME /*9*/:
                ScreenGame.processTouchEvents(eventAction, touchX2, touchY2);
                return;
            case SCREEN_STORY_FINISH /*12*/:
                ScreenSlideShow.processTouchEvents(eventAction, touchX2, touchY2);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        switch (this.currentScreen) {
            case 0:
                canvas.drawColor(Color.argb(255, 0, 0, 0));
                break;
            case 1:
                ScreenMainMenu.draw(canvas);
                break;
            case 2:
                ScreenSlideShow.draw(canvas);
                break;
            case 3:
                ScreenCharSelect.draw(canvas);
                break;
            case 4:
                ScreenLevelSelect.draw(canvas);
                break;
            case 5:
                ScreenGame.draw(canvas);
                break;
            case 6:
                ScreenLevelSelect.draw(canvas);
                break;
            case 7:
                ScreenGame.draw(canvas);
                break;
            case SCREEN_ENDLESS_CHAR_SELECT /*8*/:
                ScreenCharSelect.draw(canvas);
                break;
            case SCREEN_ENDLESS_GAME /*9*/:
                ScreenGame.draw(canvas);
                break;
            case SCREEN_STORY_FINISH /*12*/:
                ScreenSlideShow.draw(canvas);
                break;
        }
        drawTransitionShade(canvas);
        if (this.currentScreen == 0 || (this.nextScreen == 1 && this.transitionCount > 0)) {
            canvas.drawBitmap(ImageHandler.main_title, 13.0f * Globals.SCALEX, 15.0f * Globals.SCALEY, (Paint) null);
        }
    }

    private void drawTransitionShade(Canvas canvas) {
        if (this.transitionCount > 0) {
            if (this.transitionCount >= 20) {
                transitionPaint.setAlpha((int) ((((float) (-this.transitionCount)) * 12.75f) + 510.0f));
            }
            if (this.transitionCount < 20) {
                transitionPaint.setAlpha((int) (((float) this.transitionCount) * 12.75f));
            }
            canvas.drawPaint(transitionPaint);
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.isSurfaceCreated = true;
        if (this.mainThread != null) {
            this.mainThread.setRunning(true);
            if (this.mainThread != null && !this.mainThread.isAlive()) {
                this.mainThread.start();
            }
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    public void onPause() {
        switch (this.currentScreen) {
            case 5:
                ScreenGame.saveState();
                return;
            case 6:
            case SCREEN_ENDLESS_CHAR_SELECT /*8*/:
            default:
                return;
            case 7:
                ScreenGame.saveState();
                return;
            case SCREEN_ENDLESS_GAME /*9*/:
                ScreenGame.saveState();
                return;
        }
    }

    public static void goToRatingPage() {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("market://details?id=com.aquos.blockpopperlite"));
        mainContext.startActivity(intent);
    }
}
