package com.aquos.blockpopperlite;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;

public class MessageHandler {
    public static final int ABILITYX1 = 90;
    public static final int ABILITYX2 = 205;
    public static final int ABILITYX3 = 320;
    public static final int ABILITYY = 200;
    private static final int ANISTATECLOSED = 0;
    private static final int ANISTATECLOSING = 7;
    private static final int ANISTATEOPENED = 2;
    private static final int ANISTATEOPENING = 1;
    private static final int ANISTATESWITCHINLEFT = 4;
    private static final int ANISTATESWITCHINRIGHT = 6;
    private static final int ANISTATESWITCHOUTLEFT = 3;
    private static final int ANISTATESWITCHOUTRIGHT = 5;
    public static final int BACKBUTTONX = 15;
    public static final int BACKBUTTONY = 75;
    public static final int CHOOSE_BACK = 1;
    public static final int CHOOSE_CLOSE = 3;
    public static final int CHOOSE_FRONT = 2;
    public static final int CHOOSE_NOTHING = -1;
    public static final int CLOSEBUTTONX = 405;
    public static final int CLOSEBUTTONY = 75;
    public static final int DOODLEX = 26;
    public static final int DOODLEY = 30;
    public static final int ENDLESSPAGE = 16;
    public static final int FRONTBUTTONX = 405;
    public static final int FRONTBUTTONY = 315;
    public static final int GOALX = 110;
    public static final int GOALY = 210;
    public static final String[] INFOTEXT = {"Move", "Drag an eye horizontally to move", "Match 3 eyes in a row to break", "them", "Swap", "Two eyes next to each other", "can be swapped horizontally", "", "Metal Blocks", "Metal Block cannot be moved", "However it can be destroyed if", "an eye breaks next to it", "Gravity", "Eyes will fall down when you", "move them off the edge", "", "Scroll", "Eyes come from the bottom and", "move upwards. Don't let them", "reach the top!", "Combo", "Match 4 or more blocks to", "perform a combo. Combo gives", "extra points", "Chain", "Chain one match after another", "to gain extra points", "", "Fairy Blocks", "Match 3 or more of these blocks", "to activate your special ability", "", "Bomber Ability", "Touch any block to plant a bomb", "All blocks surrounding the bomb", "will explode", "Archer Ability", "Archer shoots arrows at blocks", "Touch any single block to", "destroy it", "Time Freeze Ability", "Time Mage temporarily stops time", "Use this chance to carefully", "plan your next moves", "Unlock", "When you beat level 8, you will", "unlock the next hero for the", "game", "Unlock", "You have unlocked a new hero.", "Use him in ADVENTURE, PUZZLE,", "or ENDLESS mode", "Boss", "This is the final level. Break", "as many eyes as possible to kill", "the thousand-eyed beast", "PUZZLE", "Break all eyes with a limited", "number of moves as indicated", "in the dial", "Pick your special ability", "", "", "", "ENDLESS", "Score as many points as possible", "and compete in global ranking", ""};
    public static final int MESSAGEBUBBLEOFFSETX = 20;
    public static final int MESSAGEBUBBLEOFFSETY = 5;
    public static final float MESSAGEFADESPEED = 100.0f;
    public static final int MESSAGEX = 40;
    public static final int MESSAGEY = 100;
    public static final int PUZZLEPAGE = 14;
    public static final int SPACE = 30;
    public static final int TEXTX = 110;
    public static final int TEXTY = 50;
    private static int aniCounter = 0;
    private static int aniState = 0;
    private static CustomButton archerButton;
    private static CustomButton backButton;
    private static CustomButton bomberButton;
    private static int buttonAlpha = 255;
    private static CustomButton closeButton;
    private static CustomButton frontButton;
    private static CustomButton mageButton;
    private static float messagePosX = 40.0f;
    private static float messagePosY = ((float) (Globals.GAMEY - (Globals.INFOBARHEIGHT / 2)));
    private static int page = 0;
    private static int[] pageSequence;
    private static Matrix tmpMatrix;
    private static Paint tmpPaint;

    static {
        int[] iArr = new int[10];
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[3] = 3;
        iArr[4] = 4;
        iArr[5] = 5;
        iArr[6] = 6;
        iArr[7] = 7;
        iArr[8] = 8;
        iArr[9] = 9;
        pageSequence = iArr;
    }

    public static int getClick(float touchx, float touchy, int event) {
        return -1;
    }

    private static void initializeButtons() {
        backButton = new CustomButton(ImageHandler.backButton[0], ImageHandler.backButton[1], 15, 75, 64, 64, true, null);
        frontButton = new CustomButton(ImageHandler.frontButton[0], ImageHandler.frontButton[1], 405, FRONTBUTTONY, 64, 64, true, null);
        closeButton = new CustomButton(ImageHandler.closeButton[0], ImageHandler.closeButton[1], 405, 75, 64, 64, true, null);
        bomberButton = new CustomButton(ImageHandler.doodles[8], null, 90, 200, 70, 70, true, SoundHandler.buttonSound);
        archerButton = new CustomButton(ImageHandler.doodles[9], null, ABILITYX2, 200, 70, 70, true, SoundHandler.buttonSound);
        mageButton = new CustomButton(ImageHandler.doodles[10], null, ABILITYX3, 200, 70, 70, true, SoundHandler.buttonSound);
    }

    private static String getGoalText(int level) {
        if (Globals.gameMode == 2) {
            return "Goal: Destroy all blocks";
        }
        if (Globals.gameMode == 3) {
            return "";
        }
        switch (LevelData.levelWinCondition[level]) {
            case 1:
                return "Goal: Perform " + LevelData.levelWinParameter[level] + " Matches";
            case 2:
                return "Goal: Score " + LevelData.levelWinParameter[level] + " Points";
            case 3:
                return "Goal: Destroy " + LevelData.levelWinParameter[level] + " Metal Blocks";
            case 4:
                return "Goal: Perform " + LevelData.levelWinParameter[level] + " Combos";
            case 5:
                return "Goal: Execute " + LevelData.levelWinParameter[level] + " Abilites";
            case 6:
                return "Goal: Destroy all blocks";
            default:
                return "";
        }
    }

    public static void update() {
        if (backButton == null || frontButton == null || closeButton == null) {
            initializeButtons();
        }
        if (bomberButton == null || archerButton == null || mageButton == null) {
            initializeButtons();
        }
        if (aniState == 0 || aniState == 1 || aniState == 7) {
            backButton.setVisible(false);
        } else if (page <= 0) {
            backButton.setVisible(false);
        } else {
            backButton.setVisible(true);
        }
        if (aniState == 0 || aniState == 1 || aniState == 7) {
            frontButton.setVisible(false);
        } else if (page >= pageSequence.length - 1) {
            frontButton.setVisible(false);
        } else {
            frontButton.setVisible(true);
        }
        if (aniState == 0 || aniState == 1 || aniState == 7) {
            closeButton.setVisible(false);
        } else {
            closeButton.setVisible(true);
        }
        bomberButton.setVisible(false);
        archerButton.setVisible(false);
        mageButton.setVisible(false);
        if (!(aniState == 0 || aniState == 1 || aniState == 7 || pageSequence[page] != 15)) {
            if (Globals.checkUnlocked(1)) {
                bomberButton.setVisible(true);
            }
            if (Globals.checkUnlocked(2)) {
                archerButton.setVisible(true);
            }
            if (Globals.checkUnlocked(3)) {
                mageButton.setVisible(true);
            }
        }
        if (Globals.getCharacterPowerUp() > 0) {
            if (Globals.gameMode == 2) {
                if (!Globals.seenPUtutorialPuzzle) {
                    load(Globals.currentLevel, true, true);
                    Globals.seenPUtutorialPuzzle = true;
                }
            } else if (Globals.gameMode == 1 && (Globals.seenPUtutorial & (1 << Globals.characterType)) == 0) {
                load(Globals.currentLevel, true, true);
                Globals.seenPUtutorial = (char) (Globals.seenPUtutorial | (1 << Globals.characterType));
            }
        }
        updateAnimation();
    }

    public static boolean isClosed() {
        return aniState == 0;
    }

    private static void updateAnimation() {
        aniCounter++;
        if (aniCounter >= 40) {
            aniCounter = 0;
        }
        switch (aniState) {
            case 0:
            default:
                return;
            case 1:
                messagePosY -= 100.0f;
                if (messagePosY <= 100.0f) {
                    ScreenGame.Pause();
                    messagePosY = 100.0f;
                    aniState = 2;
                    return;
                }
                return;
            case 2:
                buttonAlpha += 50;
                if (buttonAlpha >= 255) {
                    buttonAlpha = 255;
                }
                if (backButton.checkTouchUp()) {
                    aniState = 5;
                    aniCounter = 0;
                } else if (frontButton.checkTouchUp()) {
                    aniState = 3;
                    aniCounter = 0;
                } else if (closeButton.checkTouchUp()) {
                    aniState = 7;
                    SoundHandler.playWave(SoundHandler.slideOutSound);
                }
                if (bomberButton.checkTouchUp()) {
                    Globals.characterType = 1;
                    aniState = 7;
                    return;
                } else if (archerButton.checkTouchUp()) {
                    Globals.characterType = 2;
                    aniState = 7;
                    return;
                } else if (mageButton.checkTouchUp()) {
                    Globals.characterType = 3;
                    aniState = 7;
                    return;
                } else {
                    return;
                }
            case 3:
                messagePosX -= 100.0f;
                buttonAlpha -= 50;
                if (buttonAlpha <= 0) {
                    buttonAlpha = 0;
                }
                if (messagePosX <= ((float) (-Globals.GAMEX))) {
                    messagePosX = (float) Globals.GAMEX;
                    aniState = 4;
                    aniCounter = 0;
                    page++;
                    if (page >= pageSequence.length) {
                        page = pageSequence.length - 1;
                        return;
                    }
                    return;
                }
                return;
            case 4:
                messagePosX -= 100.0f;
                if (messagePosX <= 40.0f) {
                    messagePosX = 40.0f;
                    aniState = 2;
                    return;
                }
                return;
            case 5:
                messagePosX += 100.0f;
                buttonAlpha -= 50;
                if (buttonAlpha <= 0) {
                    buttonAlpha = 0;
                }
                if (messagePosX >= ((float) Globals.GAMEX)) {
                    messagePosX = (float) (-Globals.GAMEX);
                    aniState = 6;
                    aniCounter = 0;
                    page--;
                    if (page <= 0) {
                        page = 0;
                        return;
                    }
                    return;
                }
                return;
            case 6:
                messagePosX += 100.0f;
                if (messagePosX >= 40.0f) {
                    messagePosX = 40.0f;
                    aniState = 2;
                    return;
                }
                return;
            case 7:
                messagePosY += 100.0f;
                if (messagePosY >= ((float) (Globals.GAMEY - (Globals.INFOBARHEIGHT / 2)))) {
                    aniState = 0;
                    ScreenGame.Resume();
                    return;
                }
                return;
        }
    }

    public static void processTouchEvents(int eventAction, float touchX, float touchY) {
        if (aniState != 0) {
            backButton.checkPress(touchX, touchY, eventAction);
            frontButton.checkPress(touchX, touchY, eventAction);
            closeButton.checkPress(touchX, touchY, eventAction);
            bomberButton.checkPress(touchX, touchY, eventAction);
            archerButton.checkPress(touchX, touchY, eventAction);
            mageButton.checkPress(touchX, touchY, eventAction);
        } else if (eventAction == 1 && touchX < 140.0f && touchY > ((float) (InfoBar.LOCY + 5))) {
            aniState = 1;
            aniCounter = 0;
            SoundHandler.playWave(SoundHandler.slideSound);
        }
    }

    public static void load(int currentLevel, boolean powerUp, boolean show) {
        int powerUpPage = 8;
        if (Globals.characterType == 1) {
            powerUpPage = 8;
        }
        if (Globals.characterType == 2) {
            powerUpPage = 9;
        }
        if (Globals.characterType == 3) {
            powerUpPage = 10;
        }
        if (Globals.gameMode == 1) {
            switch (currentLevel) {
                case 1:
                    pageSequence = new int[1];
                    break;
                case 2:
                    int[] iArr = new int[2];
                    iArr[1] = 1;
                    pageSequence = iArr;
                    break;
                case 3:
                    int[] iArr2 = new int[3];
                    iArr2[1] = 1;
                    iArr2[2] = 2;
                    pageSequence = iArr2;
                    break;
                case 4:
                    int[] iArr3 = new int[4];
                    iArr3[1] = 1;
                    iArr3[2] = 2;
                    iArr3[3] = 3;
                    pageSequence = iArr3;
                    break;
                case 5:
                    int[] iArr4 = new int[5];
                    iArr4[1] = 1;
                    iArr4[2] = 2;
                    iArr4[3] = 3;
                    iArr4[4] = 4;
                    pageSequence = iArr4;
                    break;
                case 6:
                    int[] iArr5 = new int[6];
                    iArr5[1] = 1;
                    iArr5[2] = 2;
                    iArr5[3] = 3;
                    iArr5[4] = 4;
                    iArr5[5] = 11;
                    pageSequence = iArr5;
                    break;
                case 7:
                    int[] iArr6 = new int[6];
                    iArr6[0] = 11;
                    iArr6[2] = 1;
                    iArr6[3] = 3;
                    iArr6[4] = 4;
                    iArr6[5] = 2;
                    pageSequence = iArr6;
                    break;
                case MainSurfaceView.SCREEN_ENDLESS_CHAR_SELECT:
                    if (!powerUp) {
                        int[] iArr7 = new int[7];
                        iArr7[0] = 11;
                        iArr7[2] = 1;
                        iArr7[3] = 2;
                        iArr7[4] = 3;
                        iArr7[5] = 4;
                        iArr7[6] = 7;
                        pageSequence = iArr7;
                        break;
                    } else {
                        int[] iArr8 = new int[8];
                        iArr8[0] = 11;
                        iArr8[2] = 1;
                        iArr8[3] = 2;
                        iArr8[4] = 3;
                        iArr8[5] = 4;
                        iArr8[6] = 7;
                        iArr8[7] = powerUpPage;
                        pageSequence = iArr8;
                        break;
                    }
                case MainSurfaceView.SCREEN_ENDLESS_GAME:
                    int[] iArr9 = new int[9];
                    iArr9[0] = 11;
                    iArr9[2] = 1;
                    iArr9[3] = 2;
                    iArr9[4] = 3;
                    iArr9[5] = 4;
                    iArr9[6] = 7;
                    iArr9[7] = powerUpPage;
                    iArr9[8] = 12;
                    pageSequence = iArr9;
                    break;
                case 10:
                    int[] iArr10 = new int[10];
                    iArr10[0] = 11;
                    iArr10[1] = 12;
                    iArr10[3] = 1;
                    iArr10[4] = 2;
                    iArr10[5] = 3;
                    iArr10[6] = 4;
                    iArr10[7] = 7;
                    iArr10[8] = powerUpPage;
                    iArr10[9] = 6;
                    pageSequence = iArr10;
                    break;
                case MainSurfaceView.SCREEN_LOAD_STATE:
                    int[] iArr11 = new int[10];
                    iArr11[0] = 11;
                    iArr11[1] = 12;
                    iArr11[3] = 1;
                    iArr11[4] = 3;
                    iArr11[5] = 4;
                    iArr11[6] = 7;
                    iArr11[7] = powerUpPage;
                    iArr11[8] = 6;
                    iArr11[9] = 2;
                    pageSequence = iArr11;
                    break;
                case MainSurfaceView.SCREEN_STORY_FINISH:
                    int[] iArr12 = new int[11];
                    iArr12[0] = 11;
                    iArr12[1] = 12;
                    iArr12[3] = 1;
                    iArr12[4] = 2;
                    iArr12[5] = 3;
                    iArr12[6] = 4;
                    iArr12[7] = 7;
                    iArr12[8] = powerUpPage;
                    iArr12[9] = 6;
                    iArr12[10] = 5;
                    pageSequence = iArr12;
                    break;
                case MainSurfaceView.SCREEN_UNLOCK:
                    int[] iArr13 = new int[12];
                    iArr13[0] = 11;
                    iArr13[1] = 12;
                    iArr13[3] = 1;
                    iArr13[4] = 2;
                    iArr13[5] = 3;
                    iArr13[6] = 4;
                    iArr13[7] = 5;
                    iArr13[8] = 6;
                    iArr13[9] = 7;
                    iArr13[10] = powerUpPage;
                    iArr13[11] = 13;
                    pageSequence = iArr13;
                    break;
            }
        } else if (Globals.gameMode == 2) {
            if (currentLevel >= 18) {
                pageSequence = new int[]{15};
            } else {
                pageSequence = new int[]{14};
            }
        } else if (Globals.gameMode == 3) {
            pageSequence = new int[]{16};
        }
        page = pageSequence.length - 1;
        if (show && aniState == 0) {
            aniState = 1;
            aniCounter = 0;
        }
    }

    public static void draw(Canvas canvas) {
        if (backButton == null || frontButton == null || closeButton == null) {
            initializeButtons();
        }
        if (aniState == 0) {
            if (Globals.getCharacterPowerUp() <= 0) {
                int frame = aniCounter / 10;
                if (frame >= ImageHandler.messageBubble.length) {
                    frame = ImageHandler.messageBubble.length - 1;
                }
                canvas.drawBitmap(ImageHandler.messageBubble[frame], 20.0f * Globals.SCALEX, ((float) (InfoBar.LOCY + 5)) * Globals.SCALEY, (Paint) null);
            }
        } else if (aniState == 7 || aniState == 1) {
            float mscale = 1.0f - ((messagePosY - 100.0f) / ((float) (Globals.GAMEY - 100)));
            tmpMatrix = new Matrix();
            tmpMatrix.postScale(mscale, mscale);
            tmpMatrix.postTranslate(messagePosX * Globals.SCALEX, messagePosY * Globals.SCALEY);
            canvas.drawBitmap(ImageHandler.messageBG, tmpMatrix, null);
        } else {
            canvas.drawBitmap(ImageHandler.messageBG, messagePosX * Globals.SCALEX, Globals.SCALEY * 100.0f, (Paint) null);
            drawDoodle(canvas, pageSequence[page]);
            drawInfo(canvas, pageSequence[page]);
            drawGoalText(canvas);
            tmpPaint.setAlpha(buttonAlpha);
            backButton.draw(canvas, tmpPaint);
            frontButton.draw(canvas, tmpPaint);
            closeButton.draw(canvas, tmpPaint);
            bomberButton.draw(canvas, tmpPaint);
            archerButton.draw(canvas, tmpPaint);
            mageButton.draw(canvas, tmpPaint);
            if (pageSequence[page] == 15) {
                if (!Globals.checkUnlocked(1)) {
                    canvas.drawBitmap(ImageHandler.padlock_s, 90.0f * Globals.SCALEX, Globals.SCALEY * 200.0f, tmpPaint);
                }
                if (!Globals.checkUnlocked(2)) {
                    canvas.drawBitmap(ImageHandler.padlock_s, 205.0f * Globals.SCALEX, Globals.SCALEY * 200.0f, tmpPaint);
                }
                if (!Globals.checkUnlocked(3)) {
                    canvas.drawBitmap(ImageHandler.padlock_s, 320.0f * Globals.SCALEX, Globals.SCALEY * 200.0f, tmpPaint);
                }
            }
        }
    }

    public static void drawInfo(Canvas canvas, int i) {
        tmpPaint = new Paint(1);
        tmpPaint.setTypeface(Typeface.create("DroidSerif-Bold", 1));
        tmpPaint.setTextSize(Globals.SCALEX * 22.0f);
        tmpPaint.setColor(-16777216);
        tmpPaint.setTextScaleX(0.9f);
        tmpPaint.setAlpha(buttonAlpha);
        canvas.drawText(INFOTEXT[(i * 4) + 0], Globals.SCALEX * 150.0f, Globals.SCALEY * 150.0f, tmpPaint);
        tmpPaint.setTypeface(Typeface.create("DroidSerif-Bold", 0));
        tmpPaint.setTextSize(Globals.SCALEX * 22.0f);
        tmpPaint.setColor(-7829368);
        tmpPaint.setTextScaleX(0.8f);
        tmpPaint.setAlpha(buttonAlpha);
        canvas.drawText(INFOTEXT[(i * 4) + 1], Globals.SCALEX * 150.0f, 180.0f * Globals.SCALEY, tmpPaint);
        canvas.drawText(INFOTEXT[(i * 4) + 2], Globals.SCALEX * 150.0f, 210.0f * Globals.SCALEY, tmpPaint);
        canvas.drawText(INFOTEXT[(i * 4) + 3], Globals.SCALEX * 150.0f, 240.0f * Globals.SCALEY, tmpPaint);
    }

    public static void drawDoodle(Canvas canvas, int i) {
        if (aniState == 2) {
            canvas.drawBitmap(ImageHandler.doodles[i], 66.0f * Globals.SCALEX, 130.0f * Globals.SCALEY, (Paint) null);
        }
    }

    public static void drawGoalText(Canvas canvas) {
        if (aniCounter % 20 >= 8) {
            tmpPaint = new Paint(1);
            tmpPaint.setTypeface(Typeface.create("DroidSerif-Bold", 1));
            tmpPaint.setTextSize(22.0f * Globals.SCALEX);
            tmpPaint.setColor(-7864320);
            tmpPaint.setTextScaleX(0.9f);
            tmpPaint.setAlpha(buttonAlpha);
            canvas.drawText(getGoalText(Globals.currentLevel - 1), 150.0f * Globals.SCALEX, 310.0f * Globals.SCALEY, tmpPaint);
        }
    }
}
