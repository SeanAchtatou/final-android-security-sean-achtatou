package com.aquos.blockpopperlite;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int b_adventure_down = 2130837504;
        public static final int b_adventure_up = 2130837505;
        public static final int b_continue = 2130837506;
        public static final int b_endless_down = 2130837507;
        public static final int b_endless_up = 2130837508;
        public static final int b_hs_down = 2130837509;
        public static final int b_hs_up = 2130837510;
        public static final int b_puzzle_down = 2130837511;
        public static final int b_puzzle_up = 2130837512;
        public static final int b_unlock_down = 2130837513;
        public static final int b_unlock_up = 2130837514;
        public static final int backbutton_down = 2130837515;
        public static final int backbutton_up = 2130837516;
        public static final int background_bright = 2130837517;
        public static final int background_dark = 2130837518;
        public static final int background_rays = 2130837519;
        public static final int bigeye_blue = 2130837520;
        public static final int bigeye_green = 2130837521;
        public static final int bigeye_indigo = 2130837522;
        public static final int bigeye_red = 2130837523;
        public static final int bigeye_shine = 2130837524;
        public static final int bigeye_yellow = 2130837525;
        public static final int blackoverlay = 2130837526;
        public static final int blink_blue = 2130837527;
        public static final int blink_green = 2130837528;
        public static final int blink_indigo = 2130837529;
        public static final int blink_red = 2130837530;
        public static final int blink_yellow = 2130837531;
        public static final int block_blue = 2130837532;
        public static final int block_garbage = 2130837533;
        public static final int block_gold = 2130837534;
        public static final int block_green = 2130837535;
        public static final int block_indigo = 2130837536;
        public static final int block_red = 2130837537;
        public static final int block_yellow = 2130837538;
        public static final int bomb = 2130837539;
        public static final int bonustime00 = 2130837540;
        public static final int bonustime01 = 2130837541;
        public static final int bonustime02 = 2130837542;
        public static final int bonustime03 = 2130837543;
        public static final int bonustime04 = 2130837544;
        public static final int bonustime05 = 2130837545;
        public static final int bonustime06 = 2130837546;
        public static final int bonustime07 = 2130837547;
        public static final int bonustime08 = 2130837548;
        public static final int bonustime09 = 2130837549;
        public static final int bonustime10 = 2130837550;
        public static final int bonustime11 = 2130837551;
        public static final int bonustime12 = 2130837552;
        public static final int bonustime13 = 2130837553;
        public static final int bonustime14 = 2130837554;
        public static final int bonustime15 = 2130837555;
        public static final int bonustime16 = 2130837556;
        public static final int bonustime17 = 2130837557;
        public static final int bonustime18 = 2130837558;
        public static final int bonustime19 = 2130837559;
        public static final int bonustime20 = 2130837560;
        public static final int bow = 2130837561;
        public static final int bubble0 = 2130837562;
        public static final int bubble1 = 2130837563;
        public static final int bubble2 = 2130837564;
        public static final int bubble3 = 2130837565;
        public static final int button_a = 2130837566;
        public static final int button_story_mode_down = 2130837567;
        public static final int button_story_mode_up = 2130837568;
        public static final int card_archer = 2130837569;
        public static final int card_mage = 2130837570;
        public static final int card_miner = 2130837571;
        public static final int chain_x = 2130837572;
        public static final int charleft_down = 2130837573;
        public static final int charleft_up = 2130837574;
        public static final int charright_down = 2130837575;
        public static final int charright_up = 2130837576;
        public static final int closebutton_down = 2130837577;
        public static final int closebutton_up = 2130837578;
        public static final int crown = 2130837579;
        public static final int cursor = 2130837580;
        public static final int dialogbg = 2130837581;
        public static final int doodle0 = 2130837582;
        public static final int doodle1 = 2130837583;
        public static final int doodle1112 = 2130837584;
        public static final int doodle13 = 2130837585;
        public static final int doodle14 = 2130837586;
        public static final int doodle15 = 2130837587;
        public static final int doodle3 = 2130837588;
        public static final int doodle4 = 2130837589;
        public static final int doodle5 = 2130837590;
        public static final int doodle6 = 2130837591;
        public static final int doodle7 = 2130837592;
        public static final int explode_back = 2130837593;
        public static final int explode_back_blue = 2130837594;
        public static final int explode_back_green = 2130837595;
        public static final int explode_back_indigo = 2130837596;
        public static final int explode_back_orange = 2130837597;
        public static final int explode_back_purple = 2130837598;
        public static final int explode_back_yellow = 2130837599;
        public static final int explode_front = 2130837600;
        public static final int explosion_glow_blue = 2130837601;
        public static final int explosion_glow_green = 2130837602;
        public static final int explosion_glow_indigo = 2130837603;
        public static final int explosion_glow_red = 2130837604;
        public static final int explosion_glow_yellow = 2130837605;
        public static final int eyewhite_reg = 2130837606;
        public static final int frontbutton_down = 2130837607;
        public static final int frontbutton_up = 2130837608;
        public static final int gold_star = 2130837609;
        public static final int heroselect = 2130837610;
        public static final int hourglass = 2130837611;
        public static final int icon = 2130837612;
        public static final int infobar = 2130837613;
        public static final int infobg = 2130837614;
        public static final int levelbutton_down = 2130837615;
        public static final int levelbutton_up = 2130837616;
        public static final int levelselect = 2130837617;
        public static final int main_eye = 2130837618;
        public static final int main_menu = 2130837619;
        public static final int main_name = 2130837620;
        public static final int main_pupil = 2130837621;
        public static final int menubg = 2130837622;
        public static final int messagebg = 2130837623;
        public static final int no = 2130837624;
        public static final int padlock = 2130837625;
        public static final int padlock_s = 2130837626;
        public static final int particle_dust = 2130837627;
        public static final int particle_pentagon = 2130837628;
        public static final int pupil_blue = 2130837629;
        public static final int pupil_green = 2130837630;
        public static final int pupil_indigo = 2130837631;
        public static final int pupil_red = 2130837632;
        public static final int pupil_yellow = 2130837633;
        public static final int star = 2130837634;
        public static final int starframe = 2130837635;
        public static final int storyframe1 = 2130837636;
        public static final int storyframe2 = 2130837637;
        public static final int storyframe3 = 2130837638;
        public static final int storyframe4 = 2130837639;
        public static final int super_sign = 2130837640;
        public static final int tick = 2130837641;
        public static final int uparrow = 2130837642;
        public static final int yes = 2130837643;
    }

    public static final class id {
        public static final int bEndlessMode = 2131165191;
        public static final int bHighScore = 2131165193;
        public static final int bPuzzleMode = 2131165192;
        public static final int bStoryMode = 2131165190;
        public static final int gameSurfaceView = 2131165185;
        public static final int highScoreSurfaceView = 2131165186;
        public static final int mainSurfaceView = 2131165194;
        public static final int nameTextField = 2131165187;
        public static final int submitButton = 2131165188;
        public static final int widget0 = 2131165184;
        public static final int widget28 = 2131165189;
    }

    public static final class layout {
        public static final int game = 2130903040;
        public static final int highscore = 2130903041;
        public static final int main = 2130903042;
        public static final int mainlayout = 2130903043;
    }

    public static final class raw {
        public static final int button = 2131034112;
        public static final int glassbreak = 2131034113;
        public static final int lose = 2131034114;
        public static final int mainmusic = 2131034115;
        public static final int match = 2131034116;
        public static final int pop = 2131034117;
        public static final int slide = 2131034118;
        public static final int slideout = 2131034119;
        public static final int swap = 2131034120;
        public static final int title2 = 2131034121;
        public static final int title3 = 2131034122;
        public static final int win = 2131034123;
    }

    public static final class string {
        public static final int app_name = 2131099648;
        public static final int game_name = 2131099649;
    }

    public static final class xml {
        public static final int preferences = 2130968576;
    }
}
