package com.aquos.blockpopperlite;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;

public class ScreenLevelSelect {
    private static final int ANISTATECLOSED = 0;
    private static final int ANISTATEOPENED = 2;
    private static final int ANISTATESWITCHINLEFT = 4;
    private static final int ANISTATESWITCHINRIGHT = 6;
    private static final int ANISTATESWITCHOUTLEFT = 3;
    private static final int ANISTATESWITCHOUTRIGHT = 5;
    private static final int BUTTONSIZE = 140;
    private static final Point[] POSITIONS = {new Point(20, 20), new Point(BackgroundHandler.DARKCLOUDY, 0), new Point(MessageHandler.ABILITYX3, 10), new Point(0, BackgroundHandler.DARKCLOUDY), new Point(BackgroundHandler.DARKCLOUDY, 190), new Point(310, 170), new Point(0, 380), new Point(BUTTONSIZE, 340), new Point(MessageHandler.ABILITYX3, 360)};
    private static final float SLIDEFADESPEED = 100.0f;
    private static final int SLIDEPOSX = 0;
    private static int aniCounter = 0;
    private static int aniState = 2;
    private static CustomButton[] bLevel = new CustomButton[9];
    private static CustomButton bNext = null;
    private static CustomButton bPrevious = null;
    private static int buttonAlpha = 0;
    private static String errorMsg = "";
    private static int i = 0;
    private static int numLevels = 9;
    private static int numPages = 2;
    private static final int offsetx = 20;
    private static final int offsety = 80;
    private static int page = 0;
    private static int screenResult = 0;
    private static float slidePosX = 0.0f;
    private static Paint transPaint;

    public static void initialize(boolean puzzleLevel) {
        transPaint = new Paint();
        transPaint.setAlpha(128);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= 9) {
                break;
            }
            bLevel[i3] = new CustomButton(ImageHandler.levelSelectButton[0], ImageHandler.levelSelectButton[1], POSITIONS[i3].x + 20, POSITIONS[i3].y + 80, BUTTONSIZE, BUTTONSIZE, false, SoundHandler.buttonSound);
            i2 = i3 + 1;
        }
        bNext = new CustomButton(ImageHandler.charRight[0], ImageHandler.charRight[1], MessageHandler.ABILITYX3, 600, 142, 122, false, SoundHandler.slideSound);
        bPrevious = new CustomButton(ImageHandler.charLeft[0], ImageHandler.charLeft[1], 30, 600, 142, 122, false, SoundHandler.slideSound);
        page = 0;
        if (puzzleLevel) {
            numPages = 4;
            numLevels = LevelData.puzzleLevels.length;
            int best = Globals.solvedPuzzle.length - 1;
            while (best >= 0 && (!Globals.solvedPuzzle[best] || best > LevelData.puzzleLevels.length)) {
                best--;
            }
            if (best >= 18) {
                page = 2;
            } else if (best >= 9) {
                page = 1;
            } else {
                page = 0;
            }
        } else {
            numPages = 2;
            numLevels = LevelData.levelWinCondition.length;
            if (Globals.solvedStory[Globals.characterType] >= 9) {
                page = 1;
            } else {
                page = 0;
            }
        }
    }

    public static void update() {
        BackgroundHandler.update();
        for (int i2 = 0; i2 < 9; i2++) {
            if (i2 + 1 + (page * 9) > numLevels) {
                bLevel[i2].setVisible(false);
            } else {
                bLevel[i2].setVisible(true);
            }
            if (bLevel[i2].checkTouchUp()) {
                screenResult = i2 + 1 + (page * 9);
            }
            bLevel[i2].setPos(((int) slidePosX) + 20 + POSITIONS[i2].x, POSITIONS[i2].y + 80);
        }
        if (screenResult > numLevels) {
            screenResult = 0;
        }
        if (page <= 0) {
            bPrevious.setVisible(false);
        } else {
            bPrevious.setVisible(true);
        }
        if (page >= numPages - 1) {
            bNext.setVisible(false);
        } else {
            bNext.setVisible(true);
        }
        updateAnimation();
    }

    private static void updateAnimation() {
        aniCounter++;
        if (aniCounter >= 40) {
            aniCounter = 0;
        }
        switch (aniState) {
            case 0:
                buttonAlpha = 0;
                return;
            case 1:
            default:
                return;
            case 2:
                buttonAlpha += 50;
                if (buttonAlpha >= 255) {
                    buttonAlpha = 255;
                }
                if (bPrevious.checkTouchUp()) {
                    aniState = 5;
                    aniCounter = 0;
                    return;
                } else if (bNext.checkTouchUp()) {
                    aniState = 3;
                    aniCounter = 0;
                    return;
                } else {
                    return;
                }
            case 3:
                slidePosX -= 100.0f;
                buttonAlpha -= 50;
                if (buttonAlpha <= 0) {
                    buttonAlpha = 0;
                }
                if (slidePosX < ((float) (-Globals.GAMEX)) - 100.0f) {
                    slidePosX = (float) Globals.GAMEX;
                    aniState = 4;
                    aniCounter = 0;
                    page++;
                    if (page >= numPages) {
                        page = numPages - 1;
                        return;
                    }
                    return;
                }
                return;
            case 4:
                slidePosX -= 100.0f;
                if (slidePosX <= 0.0f) {
                    slidePosX = 0.0f;
                    aniState = 2;
                    return;
                }
                return;
            case 5:
                slidePosX += 100.0f;
                buttonAlpha -= 50;
                if (buttonAlpha <= 0) {
                    buttonAlpha = 0;
                }
                if (slidePosX > ((float) Globals.GAMEX) + 100.0f) {
                    slidePosX = (float) (-Globals.GAMEX);
                    aniState = 6;
                    aniCounter = 0;
                    page--;
                    if (page <= 0) {
                        page = 0;
                        return;
                    }
                    return;
                }
                return;
            case 6:
                slidePosX += 100.0f;
                if (slidePosX >= 0.0f) {
                    slidePosX = 0.0f;
                    aniState = 2;
                    return;
                }
                return;
        }
    }

    private static boolean checkSolved(int i2) {
        if (Globals.gameMode == 1 && i2 <= Globals.solvedStory[Globals.characterType]) {
            return true;
        }
        if (Globals.gameMode != 2 || !Globals.solvedPuzzle[i2]) {
            return false;
        }
        return true;
    }

    private static boolean checkUnlocked(int i2, boolean displayError) {
        if (Globals.gameMode == 1) {
            if (i2 <= Globals.solvedStory[Globals.characterType] + 1) {
                return true;
            }
            if (!displayError) {
                return false;
            }
            errorMsg = "Beat level " + (i2 - 1) + " to unlock";
            ImageHandler.fontLevelLock.setAlpha(255);
            return false;
        } else if (Globals.gameMode != 2) {
            return false;
        } else {
            if (i2 < 2) {
                return true;
            }
            if (i2 > 20) {
                if (!displayError) {
                    return false;
                }
                errorMsg = "Only available in full version";
                ImageHandler.fontLevelLock.setAlpha(255);
                return false;
            } else if (Globals.solvedPuzzle[i2 - 1] || Globals.solvedPuzzle[i2 - 2]) {
                return true;
            } else {
                if (!displayError) {
                    return false;
                }
                errorMsg = "Beat previous levels to unlock";
                ImageHandler.fontLevelLock.setAlpha(255);
                return false;
            }
        }
    }

    public static int getResult() {
        if (checkUnlocked(screenResult, true)) {
            i = screenResult;
            screenResult = 0;
            return i;
        }
        screenResult = 0;
        return 0;
    }

    public static void processTouchEvents(int eventAction, float touchX, float touchY) {
        for (int i2 = 0; i2 < 9; i2++) {
            bLevel[i2].checkPress(touchX, touchY, eventAction);
        }
        bNext.checkPress(touchX, touchY, eventAction);
        bPrevious.checkPress(touchX, touchY, eventAction);
    }

    public static void draw(Canvas canvas) {
        BackgroundHandler.draw(canvas, Globals.GAMEY, false);
        canvas.drawBitmap(ImageHandler.levelSelectText, 10.0f, ((float) (Globals.GAMEY - 90)) * Globals.SCALEY, (Paint) null);
        for (int i2 = 0; i2 < 9; i2++) {
            bLevel[i2].draw(canvas);
        }
        for (int i3 = 0; i3 < 9; i3++) {
            if (i3 + 1 + (page * 9) <= numLevels) {
                if (checkUnlocked(i3 + 1 + (page * 9), false)) {
                    canvas.drawText(new StringBuilder().append(i3 + 1 + (page * 9)).toString(), ((float) (bLevel[i3].getX() + 66)) * Globals.SCALEX, ((float) (bLevel[i3].getY() + 85)) * Globals.SCALEY, ImageHandler.fontLevelSel);
                } else {
                    canvas.drawBitmap(ImageHandler.padlock, ((float) (bLevel[i3].getX() + 30)) * Globals.SCALEX, ((float) (bLevel[i3].getY() + 15)) * Globals.SCALEY, (Paint) null);
                }
                if (checkSolved(i3 + 1 + (page * 9))) {
                    canvas.drawBitmap(ImageHandler.tick, ((float) (bLevel[i3].getX() + 60)) * Globals.SCALEX, ((float) (bLevel[i3].getY() + 55)) * Globals.SCALEY, (Paint) null);
                }
            }
        }
        if (ImageHandler.fontLevelLock.getAlpha() > 10) {
            ImageHandler.fontLevelLock.setAlpha(ImageHandler.fontLevelLock.getAlpha() - 5);
            canvas.drawText(errorMsg, ((float) (Globals.GAMEX / 2)) * Globals.SCALEX, 50.0f * Globals.SCALEY, ImageHandler.fontLevelLock);
        }
        bNext.draw(canvas);
        bPrevious.draw(canvas);
    }
}
