package com.aquos.blockpopperlite;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class HighScoreSurfaceView extends SurfaceView implements SurfaceHolder.Callback {
    private static final int ANISTATECLOSED = 0;
    private static final int ANISTATEOPENED = 2;
    private static final int ANISTATESWITCHINLEFT = 4;
    private static final int ANISTATESWITCHINRIGHT = 6;
    private static final int ANISTATESWITCHOUTLEFT = 3;
    private static final int ANISTATESWITCHOUTRIGHT = 5;
    private static final int LINESPACE = 40;
    private static int NUMPAGES = 10;
    private static final int NUMPERPAGE = 10;
    private static final int OFFSETY = 200;
    private static final int SCORELABELSPACE = 20;
    private static final int SCORELABELX = 290;
    private static final int SCORELABELY = 75;
    private static final int SCORENUMBERY = 55;
    private static final float SLIDEFADESPEED = 100.0f;
    private static final int SLIDEPOSX = 0;
    private static int aniCounter = 0;
    private static int aniState = 2;
    private static CustomButton bNext;
    private static CustomButton bPrevious;
    private static CustomButton bTop;
    private static int buttonAlpha = 0;
    private static int page = 0;
    private static float slidePosX = 0.0f;
    private DrawThread drawThread;
    private Paint fontPaint;
    private boolean isSurfaceCreated = false;
    private Context mainContext;

    public HighScoreSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().addCallback(this);
        setFocusable(false);
        this.mainContext = context;
    }

    public void Initialize() {
        page = 0;
        ConnectionHandler.initialize();
        ConnectionHandler.seeMyScore();
        ImageHandler.loadEssential(this.mainContext);
        ImageHandler.initializeBackground();
        if (this.drawThread == null) {
            this.drawThread = new DrawThread(getHolder(), this);
            if (this.isSurfaceCreated) {
                this.drawThread.setRunning(true);
                this.drawThread.start();
            }
        }
        bNext = new CustomButton(ImageHandler.charRight[0], ImageHandler.charRight[1], MessageHandler.ABILITYX3, 600, 142, 122, false, SoundHandler.slideSound);
        bPrevious = new CustomButton(ImageHandler.charLeft[0], ImageHandler.charLeft[1], 30, 600, 142, 122, false, SoundHandler.slideSound);
        bTop = new CustomButton(ImageHandler.doodles[16], null, 206, 625, 80, 80, false, SoundHandler.buttonSound);
        this.fontPaint = new Paint(1);
        this.fontPaint.setTypeface(Typeface.create("DroidSerif-Bold", 0));
        this.fontPaint.setTextSize(26.0f * Globals.SCALEX);
        this.fontPaint.setColor(-16777216);
        this.fontPaint.setTextAlign(Paint.Align.LEFT);
    }

    public boolean onTouchEvent(MotionEvent event) {
        processTouchEvents(event.getAction(), event.getX(), event.getY());
        try {
            Thread.sleep(200);
            return true;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return true;
        }
    }

    private void processTouchEvents(int eventAction, float touchX, float touchY) {
        float touchX2 = touchX / Globals.SCALEX;
        float touchY2 = touchY / Globals.SCALEY;
        bNext.checkPress(touchX2, touchY2, eventAction);
        bPrevious.checkPress(touchX2, touchY2, eventAction);
        bTop.checkPress(touchX2, touchY2, eventAction);
    }

    public void Destroy() {
        this.drawThread.setRunning(false);
        this.drawThread = null;
    }

    public void update() {
        if (page <= 0) {
            bPrevious.setVisible(false);
        } else {
            bPrevious.setVisible(true);
        }
        if (page >= NUMPAGES - 1) {
            bNext.setVisible(false);
        } else {
            bNext.setVisible(true);
        }
        BackgroundHandler.update();
        updateAnimation();
    }

    public static void gotoRank(int r) {
        page = (r - 1) / 10;
    }

    private static void updateAnimation() {
        aniCounter++;
        if (aniCounter >= 40) {
            aniCounter = 0;
        }
        switch (aniState) {
            case 0:
                buttonAlpha = 0;
                return;
            case 1:
            default:
                return;
            case 2:
                buttonAlpha += 50;
                if (buttonAlpha >= 255) {
                    buttonAlpha = 255;
                }
                if (bPrevious.checkTouchUp()) {
                    aniState = 5;
                    aniCounter = 0;
                    ConnectionHandler.getHighScore(((page - 1) * 10) + 5);
                    return;
                } else if (bNext.checkTouchUp()) {
                    aniState = 3;
                    aniCounter = 0;
                    ConnectionHandler.getHighScore(((page + 1) * 10) + 5);
                    return;
                } else if (bTop.checkTouchUp()) {
                    aniState = 5;
                    aniCounter = 0;
                    page = 0;
                    ConnectionHandler.getHighScore(1);
                    return;
                } else {
                    return;
                }
            case 3:
                slidePosX -= 100.0f;
                buttonAlpha -= 50;
                if (buttonAlpha <= 0) {
                    buttonAlpha = 0;
                }
                if (slidePosX < ((float) (-Globals.GAMEX)) - 100.0f) {
                    slidePosX = (float) Globals.GAMEX;
                    aniState = 4;
                    aniCounter = 0;
                    page++;
                    if (page >= NUMPAGES) {
                        page = NUMPAGES - 1;
                        return;
                    }
                    return;
                }
                return;
            case 4:
                slidePosX -= 100.0f;
                if (slidePosX <= 0.0f) {
                    slidePosX = 0.0f;
                    aniState = 2;
                    return;
                }
                return;
            case 5:
                slidePosX += 100.0f;
                buttonAlpha -= 50;
                if (buttonAlpha <= 0) {
                    buttonAlpha = 0;
                }
                if (slidePosX > ((float) Globals.GAMEX) + 100.0f) {
                    slidePosX = (float) (-Globals.GAMEX);
                    aniState = 6;
                    aniCounter = 0;
                    page--;
                    if (page <= 0) {
                        page = 0;
                        return;
                    }
                    return;
                }
                return;
            case 6:
                slidePosX += 100.0f;
                if (slidePosX >= 0.0f) {
                    slidePosX = 0.0f;
                    aniState = 2;
                    return;
                }
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int row = 0;
        try {
            BackgroundHandler.draw(canvas, Globals.GAMEY, true);
            int i = page * 10;
            while (i < ConnectionHandler.globalHighScores.length && i < (page + 1) * 10) {
                if (ConnectionHandler.globalRanks[i].equals(ConnectionHandler.myRank)) {
                    this.fontPaint.setColor(Color.argb(255, 255, 96, 96));
                } else {
                    this.fontPaint.setColor(Color.argb(255, 0, 0, 0));
                }
                canvas.drawText(ConnectionHandler.globalRanks[i], (slidePosX + 20.0f) * Globals.SCALEX, ((float) ((row * 40) + 200)) * Globals.SCALEY, this.fontPaint);
                canvas.drawText(ConnectionHandler.globalNames[i], (slidePosX + 80.0f) * Globals.SCALEX, ((float) ((row * 40) + 200)) * Globals.SCALEY, this.fontPaint);
                canvas.drawText(ConnectionHandler.globalHighScores[i], (slidePosX + 300.0f) * Globals.SCALEX, ((float) ((row * 40) + 200)) * Globals.SCALEY, this.fontPaint);
                row++;
                i++;
            }
            this.fontPaint.setColor(Color.argb(255, 0, 0, 0));
            drawScore(canvas, Globals.hScoreEndless);
            bNext.draw(canvas);
            bPrevious.draw(canvas);
            bTop.draw(canvas);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void drawScore(Canvas canvas, int score) {
        char[] m = new char[1];
        for (int i = 6; i >= 0; i--) {
            if (score > 0) {
                ImageHandler.fontNumber.setColor(-1);
                m[0] = (char) ((score % 10) + 48);
                canvas.drawText(m, 0, 1, Globals.SCALEX * ((float) ((i * 20) + SCORELABELX)), 55.0f * Globals.SCALEY, ImageHandler.fontNumber);
                ImageHandler.fontNumber.setColor(-40864);
                canvas.drawText(m, 0, 1, Globals.SCALEX * ((float) (((i * 20) + SCORELABELX) - 2)), Globals.SCALEY * 53.0f, ImageHandler.fontNumber);
            } else {
                m[0] = '0';
                ImageHandler.fontNumber.setColor(-1);
                canvas.drawText(m, 0, 1, Globals.SCALEX * ((float) ((i * 20) + SCORELABELX)), 55.0f * Globals.SCALEY, ImageHandler.fontNumber);
            }
            score /= 10;
        }
        canvas.drawText("RECORD", 290.0f * Globals.SCALEX, 75.0f * Globals.SCALEY, ImageHandler.fontScore);
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.isSurfaceCreated = true;
        if (this.drawThread != null) {
            this.drawThread.setRunning(true);
            if (this.drawThread != null && !this.drawThread.isAlive()) {
                this.drawThread.start();
            }
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    }
}
