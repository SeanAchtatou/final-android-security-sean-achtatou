package com.aquos.blockpopperlite;

import android.content.Context;
import android.widget.Toast;
import com.aquos.blockpopperlite.Globals;
import java.lang.reflect.Array;

public class LevelData {
    private static final char B = ((char) Globals.BlockType.blue.ordinal());
    public static final char BREMOVED = '';
    private static final char G = ((char) Globals.BlockType.green.ordinal());
    private static final char I = ((char) Globals.BlockType.indigo.ordinal());
    public static final int LITE_MAX_PUZZLE = 20;
    private static final char R = ((char) Globals.BlockType.red.ordinal());
    private static final char S = ((char) Globals.BlockType.gold.ordinal());
    public static final int WC_BREAK_GARBAGE = 3;
    public static final int WC_CLEAR_ALL = 6;
    public static final int WC_GOLD_BLOCKS = 5;
    public static final int WC_PERFORM_COMBO = 4;
    public static final int WC_PERFORM_MATCHES = 1;
    public static final int WC_SCORE = 2;
    private static final char X = ((char) Globals.BlockType.garbage.ordinal());
    private static final char Y = ((char) Globals.BlockType.yellow.ordinal());
    private static int i = 0;
    private static int j = 0;
    public static final int[] levelChanceGarbage = {8, 8, 8, 8, 8, 8, 12, 22, 19, 18, 12, 25, 10};
    public static final int[] levelChanceGold = {8, 8, 8, 8, 8, 8, 8, 8, 10, 12, 14, 16, 18};
    public static final float[] levelScrollSpeed = {0.0f, 0.0f, 0.0f, 0.0f, 0.15f, 0.15f, 0.2f, 0.2f, 0.2f, 0.3f, 0.3f, 0.2f, 0.4f};
    public static final int[] levelTimeSeconds = {10, 15, 10, 10, 40, 40, 50, 60, 80, 90, 100, 60, 200};
    public static final int[] levelWinCondition = {6, 6, 6, 6, 1, 2, 3, 5, 1, 2, 3, 4, 2};
    public static final int[] levelWinParameter;
    private static final char o = '';
    public static final char[][] puzzleLevels = {new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, R, R, 128, R, 128}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, R, 128, Y, Y, 128, 128, G, 128, I, I, 128, 128, B, 128, B, B, 128, 128, Y, 128, R, R, 128, 128, I, 128, G, G, 128}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, R, 128, 128, 128, 128, R, B, R, 128, 128, B, B, G, G, 128, G}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, B, 128, 128, 128, 128, 128, G, 128, Y, 128, 128, 128, B, 128, B, 128, 128, 128, G, 128, G, 128, 128, 128, Y, 128, Y, 128}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, G, G, B, 128, 128, B, G, G, B, G, G}, new char[]{128, 128, 128, 128, 128, 128, 128, Y, 128, 128, 128, 128, 128, Y, 128, 128, 128, 128, 128, G, 128, B, 128, 128, 128, G, B, B, 128, 128, 128, Y, G, R, 128, 128, 128, G, R, B, 128, 128, 128, G, R, B, 128, 128}, new char[]{128, 128, 128, G, 128, 128, 128, 128, G, R, 128, 128, 128, 128, B, R, 128, 128, 128, 128, Y, B, 128, 128, 128, 128, Y, B, 128, 128, 128, 128, G, Y, 128, 128, 128, 128, R, R, 128, 128, 128, 128, R, R, 128, 128}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, Y, Y, 128, 128, 128, B, B, X, 128, 128, 128, G, X, B, 128, 128, 128, X, G, G, 128, 128, 128, R, R, Y, R, 128}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, R, 128, 128, 128, 128, 128, I, R, R, 128, 128, 128, I, R, R, 128, 128, 128, R, I, X, 128, 128}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, Y, I, 128, 128, 128, 128, I, X, X, 128, Y, Y, I, X}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, B, R, G, 128, 128, 128, X, G, X, 128, 128, 128, G, R, B, 128, 128, 128, X, G, X, 128, 128, 128, R, G, B, 128}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, R, 128, 128, 128, 128, Y, R, 128, 128, 128, 128, Y, I, 128, 128, 128, 128, B, I, 128, 128, 128, 128, B, R, I, 128, 128, B, Y}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, Y, 128, 128, 128, 128, B, Y, 128, 128, 128, 128, B, R, 128, 128, 128, 128, Y, R, 128, 128, 128, 128, B, G, R, G, G}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, Y, 128, 128, 128, 128, 128, R, 128, 128, 128, 128, 128, X, 128, 128, 128, 128, 128, X, Y, Y, 128, 128, B, R, G, R, 128, 128, Y, G, X, X, 128, 128, R, G, B, B, 128}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, R, B, Y, B, Y, G, R, G, G, Y, B, R, G, R, G, R, G, R, B, Y, B, Y, B, Y}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, R, G, B, I, Y, 128, Y, R, G, B, I, 128, G, B, I, Y, R, 128, Y, R, G, B, I, 128, R, G, B, I, Y, 128}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, G, 128, 128, I, I, R, G, I, I, Y, Y, G, Y, R, R}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, Y, 128, 128, 128, 128, 128, I, 128, 128, 128, 128, 128, G, 128, 128, 128, 128, Y, I, 128, 128, 128, 128, R, G, B, 128, 128, S, S, I, S, 128}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, B, 128, 128, 128, 128, 128, G, 128, 128, 128, 128, 128, B, Y, 128, 128, 128, R, S, Y, 128, 128, 128, S, R, B, 128, 128, 128, R, S, G, R, R, 128}, new char[]{128, G, S, 128, 128, 128, 128, S, B, 128, 128, 128, 128, S, B, 128, 128, 128, 128, G, I, R, 128, 128, G, X, X, I, R, 128, X, Y, X, B, G, 128, X, X, X, B, Y, 128, X, S, S, Y, S, R}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, R, 128, 128, 128, R, R, G, 128, 128, 128, S, S, R, S, 128}, new char[]{128, S, 128, 128, S, 128, 128, S, 128, 128, S, 128, 128, R, S, S, R, 128, 128, R, G, G, R, 128, 128, G, R, R, G, 128, 128, R, G, G, R, 128, 128, G, R, R, G, 128, 128, R, G, G, R, 128}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, S, R, 128, 128, 128, 128, S, R, 128, 128, 128, 128, G, G, 128, 128, 128, 128, S, R, 128, 128, S, S, R, S, 128}, new char[]{128, 128, S, 128, 128, 128, 128, 128, R, R, 128, 128, 128, 128, G, G, R, 128, 128, 128, G, S, Y, 128, 128, 128, Y, B, Y, 128, 128, 128, B, R, B, 128, 128, 128, R, I, R, 128, 128, S, I, G, I, 128}, new char[]{128, 128, 128, 128, 128, 128, 128, I, I, 128, S, 128, 128, R, G, B, Y, 128, 128, R, G, B, Y, 128, 128, G, B, Y, R, 128, 128, R, G, B, Y, 128, 128, R, G, B, Y, S, 128, X, I, X, I, S}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, B, Y, B, 128, 128, 128, B, Y, B, R, G, R, R, R, G, R, G, R, S, S, Y, S, B, B, B, Y, B, R, G, R, B, Y, B, R, G, R}, new char[]{128, 128, 128, 128, 128, 128, S, 128, S, 128, S, 128, X, G, X, I, X, 128, G, B, I, Y, R, 128, Y, R, G, B, I, 128, G, B, I, Y, R, 128, Y, R, G, B, I, 128, R, X, B, X, Y, 128}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, R, 128, 128, 128, 128, G, I, 128, 128, 128, 128, G, I, S, S, 128, 128, S, R, B, R, 128, 128, G, S, R, B, 128, 128, G, S, Y, Y, S}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, B, 128, 128, 128, 128, 128, G, S, R, 128, 128, 128, R, B, S, 128, 128, 128, S, G, S, 128, 128, 128, S, S, R, 128, 128, 128, R, G, B, 128, 128}, new char[]{128, B, 128, 128, 128, 128, 128, G, 128, I, 128, 128, 128, R, 128, B, 128, 128, 128, S, 128, G, 128, 128, 128, S, 128, S, S, 128, 128, Y, R, Y, I, 128, S, S, Y, S, S, 128, S, S, I, S, S, 128}};
    public static final int[] puzzleParameter = {1, 5, 1, 3, 2, 3, 4, 1, 2, 4, 3, 4, 2, 2, 50, 50, 50, 3, 4, 2, 1, 2, 1, 4, 5, 20, 50, 1, 3, 1};
    public static final char[][] tutorialLevels = {new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, R, 128, R, 128, R, 128}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, B, G, B, G, B, G, R, Y, R, Y, R, Y}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, X, R, G, X, 128, 128, X, G, R, X, 128, 128, X, G, R, X, 128}, new char[]{128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, I, 128, 128, 128, 128, Y, X, 128, X, X, 128, X, X, I, X, X, Y, X, X, I, X, X, Y, X}};

    static {
        int[] iArr = new int[13];
        iArr[4] = 10;
        iArr[5] = 300;
        iArr[6] = 5;
        iArr[7] = 4;
        iArr[8] = 20;
        iArr[9] = 800;
        iArr[10] = 10;
        iArr[11] = 5;
        iArr[12] = 1500;
        levelWinParameter = iArr;
    }

    public static void updateMessages(int counter, Context context) {
        if (Globals.gameMode == 1) {
            switch (Globals.currentLevel) {
                case 1:
                    if (counter == 10) {
                        Toast.makeText(context, "Level 1 - Make 10 Matches", 5);
                        return;
                    }
                    return;
                case 2:
                    if (counter == 10) {
                        Toast.makeText(context, "Level 2 - Score 500 Points", 5);
                        return;
                    }
                    return;
                case 3:
                    if (counter == 10) {
                        Toast.makeText(context, "Level 3 - Destroy 50 Garbage Blocks", 5);
                        return;
                    }
                    return;
                case 4:
                    if (counter == 10) {
                        Toast.makeText(context, "Level 4 - Perform 5 Combos", 5);
                        return;
                    }
                    return;
                case 5:
                    if (counter == 10) {
                        Toast.makeText(context, "Level 5 - Perform 5 Chains", 5);
                        return;
                    }
                    return;
                case 6:
                    if (counter == 10) {
                        Toast.makeText(context, "Level 6 - Survive for 5 Minutes", 5);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public static Tile[][] initializePuzzleTiles(char[] input) {
        Tile[][] t = (Tile[][]) Array.newInstance(Tile.class, Globals.MAXBLOCKHORIZONTAL, Globals.MAXBLOCKVERTICAL);
        i = 0;
        while (i < Globals.MAXBLOCKHORIZONTAL) {
            j = 0;
            while (j < Globals.MAXBLOCKVERTICAL) {
                if (input[(j * Globals.MAXBLOCKHORIZONTAL) + i] == 128) {
                    t[i][j] = new Tile(i * Globals.TILESX, j * Globals.TILESY, -1);
                } else {
                    t[i][j] = new Tile(Globals.blockTypes[input[(j * Globals.MAXBLOCKHORIZONTAL) + i]], i * Globals.TILESX, j * Globals.TILESY, -1);
                }
                j++;
            }
            i++;
        }
        return t;
    }
}
