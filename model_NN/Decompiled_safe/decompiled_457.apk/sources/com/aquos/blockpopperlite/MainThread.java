package com.aquos.blockpopperlite;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class MainThread extends Thread {
    private MainSurfaceView _panel;
    private boolean _run = false;
    private SurfaceHolder _surfaceHolder;
    private long gameTock = 0;

    public MainThread(SurfaceHolder surfaceHolder, MainSurfaceView panel) {
        this._surfaceHolder = surfaceHolder;
        this._panel = panel;
    }

    public SurfaceHolder getSurfaceHolder() {
        return this._surfaceHolder;
    }

    public void setRunning(boolean run) {
        this._run = run;
    }

    public void run() {
        this.gameTock = System.currentTimeMillis();
        while (this._run) {
            try {
                Canvas c = this._surfaceHolder.lockCanvas(null);
                if (this._panel != null) {
                    this._panel.update();
                    this._panel.onDraw(c);
                }
                this.gameTock = (this.gameTock + ((long) Globals.FRAMESECOND)) - System.currentTimeMillis();
                if (this.gameTock > 0) {
                    sleep(this.gameTock);
                }
                this.gameTock = System.currentTimeMillis();
                if (c != null) {
                    this._surfaceHolder.unlockCanvasAndPost(c);
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (0 != 0) {
                    this._surfaceHolder.unlockCanvasAndPost(null);
                }
            } catch (Throwable th) {
                if (0 != 0) {
                    this._surfaceHolder.unlockCanvasAndPost(null);
                }
                throw th;
            }
        }
    }
}
