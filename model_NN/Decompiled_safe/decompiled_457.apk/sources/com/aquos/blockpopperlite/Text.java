package com.aquos.blockpopperlite;

import android.graphics.Canvas;
import android.graphics.Paint;

public class Text {
    public int chain = 0;
    public int combo = 0;
    public int lifeTime = 0;
    public float posx;
    public float posy;
    public int score = 0;
    public String text;
    public float vely;

    public Text(float x, float y, int scr, int cmb, int chn) {
        this.posx = x;
        this.posy = y;
        this.vely = -1.0f;
        this.score = scr;
        this.chain = chn;
        this.combo = cmb;
    }

    public void update() {
        this.lifeTime++;
        this.posy += this.vely;
        this.vely *= 0.95f;
    }

    public void draw(Canvas c) {
        try {
            if (this.chain > 1) {
                c.drawBitmap(ImageHandler.chainX, this.posx * Globals.SCALEX, (this.posy - 18.0f) * Globals.SCALEY, (Paint) null);
                ImageHandler.fontCombo.setColor(-16777216);
                c.drawText(new StringBuilder(String.valueOf(this.chain)).toString(), (this.posx + 20.0f) * Globals.SCALEX, (this.posy + 2.0f) * Globals.SCALEY, ImageHandler.fontCombo);
                ImageHandler.fontCombo.setColor(-39322);
                c.drawText(new StringBuilder(String.valueOf(this.chain)).toString(), (this.posx + 18.0f) * Globals.SCALEX, this.posy * Globals.SCALEY, ImageHandler.fontCombo);
            }
            if (this.combo > 3) {
                ImageHandler.fontCombo.setColor(-16777216);
                c.drawText(new StringBuilder(String.valueOf(this.combo)).toString(), (this.posx - 18.0f) * Globals.SCALEX, (this.posy + 2.0f) * Globals.SCALEY, ImageHandler.fontCombo);
                ImageHandler.fontCombo.setColor(-6689212);
                c.drawText(new StringBuilder(String.valueOf(this.combo)).toString(), (this.posx - 20.0f) * Globals.SCALEX, this.posy * Globals.SCALEY, ImageHandler.fontCombo);
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}
