package com.aquos.blockpopperlite;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;

public class Card {
    private static final Point[] CARDPOS = {new Point(80, 110), new Point(236, 50), new Point(20, 50)};
    private static final float[] CARDSCALE = {1.0f, 0.7f, 0.7f};
    private static final int SWITCHFRAME = 6;
    private Point cardPos = CARDPOS[0];
    private float cardScale = CARDSCALE[0];
    private int hero;
    private boolean isReady = true;
    private Point nextCardPos = CARDPOS[0];
    private float nextCardScale = CARDSCALE[0];
    private Point posSpeed = new Point(0, 0);
    private int rank;
    private float scaleSpeed = 0.0f;
    private Matrix tmpMatrix = new Matrix();

    public Card(int rank2, int hero2) {
        rank2 = rank2 > 2 ? 2 : rank2;
        rank2 = rank2 < 0 ? 0 : rank2;
        this.rank = rank2;
        this.hero = hero2;
        this.cardPos = CARDPOS[rank2];
        this.nextCardPos = CARDPOS[rank2];
        this.cardScale = CARDSCALE[rank2];
        this.nextCardScale = CARDSCALE[rank2];
    }

    public void changeRank(int ra) {
        this.posSpeed = new Point(CARDPOS[ra].x - this.cardPos.x, CARDPOS[ra].y - this.cardPos.y);
        this.scaleSpeed = CARDSCALE[ra] - this.cardScale;
        this.rank = ra;
        this.nextCardPos = CARDPOS[ra];
        this.nextCardScale = CARDSCALE[ra];
        this.isReady = false;
    }

    public void update() {
        if (Math.abs(this.cardPos.x - this.nextCardPos.x) < Math.abs((this.posSpeed.x * 2) / 6)) {
            this.cardPos = new Point(this.nextCardPos.x, this.nextCardPos.y);
            this.cardScale = this.nextCardScale;
            this.posSpeed = new Point(0, 0);
            this.scaleSpeed = 0.0f;
            this.isReady = true;
            return;
        }
        this.cardPos = new Point(this.cardPos.x + (this.posSpeed.x / 6), this.cardPos.y + (this.posSpeed.y / 6));
        this.cardScale += this.scaleSpeed / 6.0f;
    }

    public boolean getReady() {
        return this.isReady;
    }

    public int getRank() {
        return this.rank;
    }

    public void draw(Canvas canvas) {
        this.tmpMatrix.setScale(this.cardScale, this.cardScale);
        this.tmpMatrix.postTranslate(((float) this.cardPos.x) * Globals.SCALEX, ((float) this.cardPos.y) * Globals.SCALEY);
        canvas.drawBitmap(ImageHandler.charCard[this.hero - 1], this.tmpMatrix, null);
    }
}
