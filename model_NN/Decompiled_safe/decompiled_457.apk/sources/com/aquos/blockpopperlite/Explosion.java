package com.aquos.blockpopperlite;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import com.aquos.blockpopperlite.Globals;

public class Explosion {
    private static int i;
    private static float j;
    private static Matrix tmpMatrix = new Matrix();
    private static Paint tmpPaint = new Paint();
    public int bt;
    public int lifeTime = 0;
    public int posx;
    public int posy;

    public Explosion(int x, int y, Globals.BlockType bl) {
        this.posx = x;
        this.posy = y;
        this.bt = bl.ordinal();
    }

    public void update() {
        this.lifeTime++;
    }

    public void draw(Canvas c) {
        try {
            if (this.lifeTime >= 0 && this.lifeTime < 25) {
                tmpPaint.reset();
                j = 1.0f;
                i = (this.lifeTime - 5) * 25;
                if (this.lifeTime < 5) {
                    i = 0;
                    j = 1.0f;
                } else if (this.lifeTime <= 15) {
                    i = (this.lifeTime - 5) * 25;
                    j = 1.0f;
                } else {
                    i = (255 - this.lifeTime) + 15;
                    j = (((float) (this.lifeTime - 15)) * 0.03f) + 1.0f;
                }
                tmpPaint.setARGB(i, 255, 255, 255);
                tmpMatrix.reset();
                tmpMatrix.postScale(j, j);
                tmpMatrix.postTranslate((((float) ((this.posx + (Globals.TILESX / 2)) + Globals.BLOCKLEFTOFFSET)) * Globals.SCALEX) - (((float) (ImageHandler.explodeSize.x / 2)) * j), ((((float) ((this.posy + (Globals.TILESY / 2)) + Globals.BLOCKTOPOFFSET)) - Globals.scrollOffset) * Globals.SCALEY) - (((float) (ImageHandler.explodeSize.y / 2)) * j));
                c.drawBitmap(ImageHandler.explodeBack[this.bt], tmpMatrix, tmpPaint);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void shiftUp(int tilesy) {
        this.posy -= tilesy;
    }
}
