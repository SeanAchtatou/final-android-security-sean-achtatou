package com.aquos.blockpopperlite;

import android.content.Context;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.util.UUID;

public class FileHandler {
    private static final int BLOCKS_NUM = 48;
    private static final int BLOCKS_START = 22;
    private static final int CHARACTER = 19;
    private static final int CURRENT_SCORE_NUM = 2;
    private static final int CURRENT_SCORE_START = 16;
    private static final int GAME_MODE = 18;
    private static final int HIGH_SCORE_ENDLESS_NUM = 2;
    private static final int HIGH_SCORE_ENDLESS_START = 2;
    private static final int HIGH_SCORE_STORY_NUM = 2;
    private static final int HIGH_SCORE_STORY_START = 0;
    private static final int LEVEL = 20;
    private static final int SEEN_PU_TUTORIAL = 21;
    private static final int UNLOCKED_PUZZLE_NUM = 8;
    private static final int UNLOCKED_PUZZLE_START = 8;
    private static final int UNLOCKED_STORY_NUM = 4;
    private static final int UNLOCKED_STORY_START = 4;
    private static final int UUIDMAX = 36;

    public static char[] getBlocksChar(Tile[][] t) {
        char[] result = new char[(Globals.MAXBLOCKHORIZONTAL * Globals.MAXBLOCKVERTICAL)];
        for (int i = 0; i < Globals.MAXBLOCKHORIZONTAL; i++) {
            for (int j = 0; j < Globals.MAXBLOCKVERTICAL; j++) {
                if (t[i][j].isExploding || t[i][j].isDestroyed || t[i][j].isRemoved) {
                    result[(Globals.MAXBLOCKVERTICAL * i) + j] = LevelData.BREMOVED;
                } else {
                    result[(Globals.MAXBLOCKVERTICAL * i) + j] = (char) t[i][j].blockType.ordinal();
                }
            }
        }
        return result;
    }

    public static Tile[][] restoreBlocksChar(char[] input) {
        try {
            Tile[][] t = (Tile[][]) Array.newInstance(Tile.class, Globals.MAXBLOCKHORIZONTAL, Globals.MAXBLOCKVERTICAL);
            for (int i = 0; i < Globals.MAXBLOCKHORIZONTAL; i++) {
                for (int j = 0; j < Globals.MAXBLOCKVERTICAL; j++) {
                    if (input[(Globals.MAXBLOCKVERTICAL * i) + j] == 128) {
                        t[i][j] = new Tile(Globals.TILESX * i, Globals.TILESY * j, -1);
                    } else {
                        t[i][j] = new Tile(Globals.blockTypes[input[(Globals.MAXBLOCKVERTICAL * i) + j]], Globals.TILESX * i, Globals.TILESY * j, -1);
                    }
                }
            }
            return t;
        } catch (Exception e) {
            return null;
        }
    }

    private static boolean[] convertChar2Boolean(char[] input) {
        int k;
        int k2 = 0;
        boolean[] result = new boolean[(input.length * 8)];
        int i = 0;
        while (i < input.length) {
            int j = 0;
            while (true) {
                k = k2;
                if (j >= 8) {
                    break;
                }
                k2 = k + 1;
                result[k] = (input[i] & (1 << j)) != 0;
                j++;
            }
            i++;
            k2 = k;
        }
        return result;
    }

    private static char[] convertBoolean2Char(boolean[] input) {
        int k;
        int k2 = 0;
        char[] result = new char[(input.length / 8)];
        int i = 0;
        while (i < result.length) {
            result[i] = 0;
            int j = 0;
            while (true) {
                k = k2;
                if (j >= 8) {
                    break;
                }
                k2 = k + 1;
                if (input[k]) {
                    result[i] = (char) (result[i] | (1 << j));
                }
                j++;
            }
            i++;
            k2 = k;
        }
        return result;
    }

    /* JADX INFO: Multiple debug info for r19v1 char[]: [D('tiles' com.aquos.blockpopperlite.Tile[][]), D('cstate' char[])] */
    public static void writeState(Context context, Tile[][] tiles) {
        FileOutputStream fOut;
        OutputStreamWriter osw;
        OutputStreamWriter osw2;
        FileOutputStream fOut2;
        Exception exc;
        char[] cstate = getBlocksChar(tiles);
        FileOutputStream fOut3 = null;
        int savehScoreStory = Globals.hScoreStory;
        int savehScoreEndless = Globals.hScoreEndless;
        char[] savesolvedStory = (char[]) Globals.solvedStory.clone();
        char[] savesolvedPuzzle = convertBoolean2Char(Globals.solvedPuzzle);
        int saveSeenPUtutorial = Globals.seenPUtutorial;
        int saveGameMode = Globals.gameMode;
        int saveCurrentScore = Globals.getScore();
        int saveCharacterType = Globals.characterType;
        int saveCurrentLevel = Globals.currentLevel;
        Tile[][] savedTiles = restoreState(context);
        try {
            fOut3 = context.openFileOutput("settings.dat", 1);
            OutputStreamWriter osw3 = new OutputStreamWriter(fOut3);
            try {
                osw3.write((char) (savehScoreStory >> 16));
                osw3.write((char) (65535 & savehScoreStory));
                osw3.write((char) (savehScoreEndless >> 16));
                osw3.write((char) (65535 & savehScoreEndless));
                osw3.write(savesolvedStory[0]);
                osw3.write(savesolvedStory[1]);
                osw3.write(savesolvedStory[2]);
                osw3.write(savesolvedStory[3]);
                osw3.write(savesolvedPuzzle[0]);
                osw3.write(savesolvedPuzzle[1]);
                osw3.write(savesolvedPuzzle[2]);
                osw3.write(savesolvedPuzzle[3]);
                osw3.write(savesolvedPuzzle[4]);
                osw3.write(savesolvedPuzzle[5]);
                osw3.write(savesolvedPuzzle[6]);
                osw3.write(savesolvedPuzzle[7]);
                if (saveGameMode == 3) {
                    osw3.write((char) (saveCurrentScore >> 16));
                    osw3.write((char) (65535 & saveCurrentScore));
                    osw3.write((char) (65535 & saveGameMode));
                    osw3.write((char) (65535 & saveCharacterType));
                    osw3.write((char) (65535 & saveCurrentLevel));
                    osw3.write(saveSeenPUtutorial);
                    osw3.write(cstate);
                } else {
                    osw3.write((char) (Globals.getScore() >> 16));
                    osw3.write((char) (Globals.getScore() & 65535));
                    osw3.write((char) (Globals.gameMode & 65535));
                    osw3.write((char) (Globals.characterType & 65535));
                    osw3.write((char) (Globals.currentLevel & 65535));
                    osw3.write(saveSeenPUtutorial);
                    osw3.write(getBlocksChar(savedTiles));
                }
                osw3.flush();
                try {
                    osw3.close();
                    fOut3.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e2) {
                e = e2;
                fOut = fOut3;
                osw = osw3;
            } catch (Throwable th) {
                osw2 = osw3;
                Exception exc2 = th;
                fOut2 = fOut3;
                exc = exc2;
                try {
                    osw2.close();
                    fOut2.close();
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
                throw exc;
            }
        } catch (Exception e4) {
            e = e4;
            fOut = fOut3;
            osw = null;
            try {
                e.printStackTrace();
                try {
                    osw.close();
                    fOut.close();
                } catch (Exception e5) {
                    e5.printStackTrace();
                }
            } catch (Throwable e6) {
                Exception exc3 = e6;
                fOut2 = fOut;
                osw2 = osw;
                exc = exc3;
                osw2.close();
                fOut2.close();
                throw exc;
            }
        } catch (Throwable th2) {
            osw2 = null;
            Exception exc4 = th2;
            fOut2 = fOut3;
            exc = exc4;
            osw2.close();
            fOut2.close();
            throw exc;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00b7 A[LOOP:0: B:12:0x0048->B:35:0x00b7, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00c2 A[LOOP:1: B:15:0x004e->B:36:0x00c2, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00cb A[LOOP:2: B:18:0x007f->B:37:0x00cb, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.aquos.blockpopperlite.Tile[][] restoreState(android.content.Context r10) {
        /*
            r3 = 0
            r5 = 0
            int r8 = com.aquos.blockpopperlite.Globals.MAXBLOCKHORIZONTAL
            int r9 = com.aquos.blockpopperlite.Globals.MAXBLOCKVERTICAL
            int r8 = r8 * r9
            int r8 = r8 + 22
            char[] r1 = new char[r8]
            boolean[] r8 = com.aquos.blockpopperlite.Globals.solvedPuzzle
            int r8 = r8.length
            int r8 = r8 / 8
            char[] r7 = new char[r8]
            java.lang.String r8 = "settings.dat"
            java.io.FileInputStream r3 = r10.openFileInput(r8)     // Catch:{ Exception -> 0x0087, all -> 0x0099 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0087, all -> 0x0099 }
            r6.<init>(r3)     // Catch:{ Exception -> 0x0087, all -> 0x0099 }
            r6.read(r1)     // Catch:{ Exception -> 0x00d7, all -> 0x00d4 }
            r6.close()     // Catch:{ Exception -> 0x00ab }
            r3.close()     // Catch:{ Exception -> 0x00ab }
            r5 = r6
        L_0x0027:
            boolean r8 = checkFileFormat(r1)
            if (r8 != 0) goto L_0x0031
            char[] r1 = makeDefaultFile()
        L_0x0031:
            r8 = 0
            char r8 = r1[r8]
            int r8 = r8 << 16
            r9 = 1
            char r9 = r1[r9]
            r8 = r8 | r9
            com.aquos.blockpopperlite.Globals.hScoreStory = r8
            r8 = 2
            char r8 = r1[r8]
            int r8 = r8 << 16
            r9 = 3
            char r9 = r1[r9]
            r8 = r8 | r9
            com.aquos.blockpopperlite.Globals.hScoreEndless = r8
            r4 = 0
        L_0x0048:
            char[] r8 = com.aquos.blockpopperlite.Globals.solvedStory
            int r8 = r8.length
            if (r4 < r8) goto L_0x00b7
            r4 = 0
        L_0x004e:
            int r8 = r7.length
            if (r4 < r8) goto L_0x00c2
            boolean[] r8 = convertChar2Boolean(r7)
            com.aquos.blockpopperlite.Globals.solvedPuzzle = r8
            r8 = 16
            char r8 = r1[r8]
            int r8 = r8 << 16
            r9 = 17
            char r9 = r1[r9]
            r8 = r8 | r9
            com.aquos.blockpopperlite.Globals.setScore(r8)
            r8 = 18
            char r8 = r1[r8]
            com.aquos.blockpopperlite.Globals.gameMode = r8
            r8 = 19
            char r8 = r1[r8]
            com.aquos.blockpopperlite.Globals.characterType = r8
            r8 = 20
            char r8 = r1[r8]
            com.aquos.blockpopperlite.Globals.currentLevel = r8
            int r8 = com.aquos.blockpopperlite.Globals.MAXBLOCKHORIZONTAL
            int r9 = com.aquos.blockpopperlite.Globals.MAXBLOCKVERTICAL
            int r8 = r8 * r9
            char[] r0 = new char[r8]
            r4 = 0
        L_0x007f:
            int r8 = r0.length
            if (r4 < r8) goto L_0x00cb
            com.aquos.blockpopperlite.Tile[][] r8 = restoreBlocksChar(r0)
            return r8
        L_0x0087:
            r8 = move-exception
        L_0x0088:
            r5.close()     // Catch:{ Exception -> 0x008f }
            r3.close()     // Catch:{ Exception -> 0x008f }
            goto L_0x0027
        L_0x008f:
            r8 = move-exception
            r2 = r8
            char[] r1 = makeDefaultFile()
            r2.printStackTrace()
            goto L_0x0027
        L_0x0099:
            r8 = move-exception
        L_0x009a:
            r5.close()     // Catch:{ Exception -> 0x00a1 }
            r3.close()     // Catch:{ Exception -> 0x00a1 }
        L_0x00a0:
            throw r8
        L_0x00a1:
            r9 = move-exception
            r2 = r9
            char[] r1 = makeDefaultFile()
            r2.printStackTrace()
            goto L_0x00a0
        L_0x00ab:
            r8 = move-exception
            r2 = r8
            char[] r1 = makeDefaultFile()
            r2.printStackTrace()
            r5 = r6
            goto L_0x0027
        L_0x00b7:
            char[] r8 = com.aquos.blockpopperlite.Globals.solvedStory
            int r9 = r4 + 4
            char r9 = r1[r9]
            r8[r4] = r9
            int r4 = r4 + 1
            goto L_0x0048
        L_0x00c2:
            int r8 = r4 + 8
            char r8 = r1[r8]
            r7[r4] = r8
            int r4 = r4 + 1
            goto L_0x004e
        L_0x00cb:
            int r8 = r4 + 22
            char r8 = r1[r8]
            r0[r4] = r8
            int r4 = r4 + 1
            goto L_0x007f
        L_0x00d4:
            r8 = move-exception
            r5 = r6
            goto L_0x009a
        L_0x00d7:
            r8 = move-exception
            r5 = r6
            goto L_0x0088
        */
        throw new UnsupportedOperationException("Method not decompiled: com.aquos.blockpopperlite.FileHandler.restoreState(android.content.Context):com.aquos.blockpopperlite.Tile[][]");
    }

    private static boolean checkFileFormat(char[] state) {
        if (state.length != (Globals.MAXBLOCKHORIZONTAL * Globals.MAXBLOCKVERTICAL) + 22) {
            return false;
        }
        return true;
    }

    private static char[] makeDefaultFile() {
        char[] cstate = new char[((Globals.MAXBLOCKHORIZONTAL * Globals.MAXBLOCKVERTICAL) + 22)];
        for (int i = 0; i < SEEN_PU_TUTORIAL; i++) {
            cstate[i] = 0;
        }
        for (int i2 = 22; i2 < (Globals.MAXBLOCKHORIZONTAL * Globals.MAXBLOCKVERTICAL) + 22; i2++) {
            cstate[i2] = LevelData.BREMOVED;
        }
        return cstate;
    }

    public static void writeSettings(Context context) {
        Exception e;
        FileOutputStream fOut = null;
        OutputStreamWriter osw = null;
        try {
            fOut = context.openFileOutput("settings1.dat", 1);
            OutputStreamWriter osw2 = new OutputStreamWriter(fOut);
            int i = 0;
            while (i < Globals.settings.length) {
                try {
                    if (Globals.settings[i]) {
                        osw2.write(1);
                    } else {
                        osw2.write(0);
                    }
                    i++;
                } catch (Exception e2) {
                    e = e2;
                    osw = osw2;
                    try {
                        e.printStackTrace();
                        try {
                            osw.close();
                            fOut.close();
                        } catch (Exception e3) {
                            e3.printStackTrace();
                            return;
                        }
                    } catch (Throwable th) {
                        th = th;
                        try {
                            osw.close();
                            fOut.close();
                        } catch (Exception e4) {
                            e4.printStackTrace();
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    osw = osw2;
                    osw.close();
                    fOut.close();
                    throw th;
                }
            }
            for (int i2 = 0; i2 < Globals.phoneID.length(); i2++) {
                osw2.write(Globals.phoneID.charAt(i2));
            }
            osw2.flush();
            try {
                osw2.close();
                fOut.close();
            } catch (Exception e5) {
                e5.printStackTrace();
            }
        } catch (Exception e6) {
            e = e6;
            e.printStackTrace();
            osw.close();
            fOut.close();
        }
    }

    public static void readSettings(Context context) {
        FileInputStream fIn = null;
        InputStreamReader isr = null;
        char[] out = new char[(Globals.settings.length + UUIDMAX)];
        try {
            fIn = context.openFileInput("settings1.dat");
            InputStreamReader isr2 = new InputStreamReader(fIn);
            try {
                isr2.read(out);
                for (int i = 0; i < Globals.settings.length; i++) {
                    if (out[i] == 1) {
                        Globals.settings[i] = true;
                    } else {
                        Globals.settings[i] = false;
                    }
                }
                Globals.phoneID = String.copyValueOf(out, Globals.settings.length, out.length - Globals.settings.length);
                if (Globals.phoneID.length() < 5) {
                    Globals.phoneID = UUID.randomUUID().toString();
                }
                try {
                    isr2.close();
                    fIn.close();
                } catch (Exception e) {
                    char[] out2 = makeDefaultSettings();
                    e.printStackTrace();
                }
            } catch (Exception e2) {
                isr = isr2;
                try {
                    isr.close();
                    fIn.close();
                } catch (Exception e3) {
                    char[] out3 = makeDefaultSettings();
                    e3.printStackTrace();
                }
            } catch (Throwable th) {
                th = th;
                isr = isr2;
                try {
                    isr.close();
                    fIn.close();
                } catch (Exception e4) {
                    char[] out4 = makeDefaultSettings();
                    e4.printStackTrace();
                }
                throw th;
            }
        } catch (Exception e5) {
            isr.close();
            fIn.close();
        } catch (Throwable th2) {
            th = th2;
            isr.close();
            fIn.close();
            throw th;
        }
    }

    private static char[] makeDefaultSettings() {
        char[] result = new char[Globals.settings.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = 1;
        }
        Globals.phoneID = UUID.randomUUID().toString();
        return result;
    }
}
