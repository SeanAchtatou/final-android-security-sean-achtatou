package com.aquos.blockpopperlite;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

public class InfoBar {
    private static final int ANISTATENONE = 0;
    private static final int ANISTATERUN = 2;
    private static final int ANISTATESTART = 1;
    private static final int ANISTATESTOP = 3;
    private static int DIALNUMX = 267;
    private static int DIALNUMY = 70;
    private static int DIALX = 220;
    private static int DIALY = 8;
    public static final int LOCY = (Globals.GAMEY - Globals.INFOBARHEIGHT);
    private static int SCORELABELSPACE = 20;
    private static int SCORELABELX = MessageHandler.ABILITYX3;
    private static int SCORELABELY = 76;
    private static int SCORENUMBERY = 56;
    private static final int SHAKESTATTIME = 20;
    private static final int SUPERLOCX = 10;
    private static final int SUPERLOCY = (Globals.GAMEY - 130);
    private static final int SUPERSPEED = 20;
    private static Paint backPaint = new Paint();
    public static int level_BREAK_GARBAGE = 0;
    public static int level_PERFORM_ABILITY = 0;
    public static int level_PERFORM_CHAINS = 0;
    public static int level_PERFORM_COMBO = 0;
    public static int level_PERFORM_MATCHES = 0;
    public static int level_SURVIVE = 0;
    private static char[] m = new char[1];
    private static int prevStat = 0;
    private static int puzzleMovesLeft = 0;
    private static int shakeStat = 0;
    private static int stopTimer = 0;
    private static int superAniCounter = 0;
    private static int superAniState = 0;
    private static int superx = 0;
    private static int supery = 0;

    public static void initialize() {
        stopTimer = 0;
        level_PERFORM_MATCHES = 0;
        level_BREAK_GARBAGE = 0;
        level_PERFORM_COMBO = 0;
        level_PERFORM_CHAINS = 0;
        level_PERFORM_ABILITY = 0;
        level_SURVIVE = 0;
        superAniCounter = 0;
        superAniState = 0;
        superx = 0;
        supery = 0;
    }

    public static void setMovesLeft(int a) {
        puzzleMovesLeft = a;
    }

    public static void reduceMoves() {
        if (Globals.gameMode == 2) {
            puzzleMovesLeft--;
        }
    }

    public static int getMovesLeft() {
        return puzzleMovesLeft;
    }

    public static void setStopTimer(int a) {
        stopTimer = a;
    }

    public static void incrementStopTimer(int a) {
        stopTimer += a;
    }

    public static void update() {
        updateAnimation();
        if (stopTimer > 0) {
            stopTimer--;
        }
        level_SURVIVE++;
    }

    public static boolean isStopped() {
        return stopTimer > 0;
    }

    public static void destroy() {
    }

    private static void updateAnimationSuper() {
        if (superAniState == 1) {
            if (Globals.getCharacterPowerUp() < 0) {
                superAniState = 3;
            } else {
                supery -= 20;
                if (supery <= SUPERLOCY) {
                    supery = SUPERLOCY;
                    superAniState = 2;
                    superAniCounter = 0;
                }
            }
        } else if (superAniState == 2) {
            if (superAniCounter >= 10) {
                superAniCounter = 0;
            }
            if (Globals.getCharacterPowerUp() <= 0) {
                superAniState = 3;
            }
        } else if (superAniState == 3) {
            supery += 20;
            if (supery >= Globals.GAMEY) {
                supery = Globals.GAMEY;
                superAniState = 0;
            }
        } else if (Globals.getCharacterPowerUp() > 0) {
            level_PERFORM_ABILITY++;
            superAniCounter = 0;
            superAniState = 1;
            superx = 10;
            supery = Globals.GAMEY;
        }
        superAniCounter++;
    }

    private static void updateAnimation() {
        updateAnimationSuper();
    }

    public static void drawScore(Canvas canvas, int score) {
        for (int i = 6; i >= 0; i--) {
            if (score > 0) {
                ImageHandler.fontNumber.setColor(-4473908);
                m[0] = (char) ((score % 10) + 48);
                canvas.drawText(m, 0, 1, Globals.SCALEX * ((float) (SCORELABELX + (SCORELABELSPACE * i))), Globals.SCALEY * ((float) (LOCY + SCORENUMBERY)), ImageHandler.fontNumber);
                ImageHandler.fontNumber.setColor(-12085249);
                canvas.drawText(m, 0, 1, Globals.SCALEX * ((float) ((SCORELABELX + (SCORELABELSPACE * i)) - 2)), Globals.SCALEY * ((float) ((LOCY + SCORENUMBERY) - 2)), ImageHandler.fontNumber);
            } else {
                m[0] = '0';
                ImageHandler.fontNumber.setColor(-4473908);
                canvas.drawText(m, 0, 1, Globals.SCALEX * ((float) (SCORELABELX + (SCORELABELSPACE * i))), Globals.SCALEY * ((float) (LOCY + SCORENUMBERY)), ImageHandler.fontNumber);
            }
            score /= 10;
        }
        canvas.drawText("SCORE", ((float) SCORELABELX) * Globals.SCALEX, ((float) (LOCY + SCORELABELY)) * Globals.SCALEY, ImageHandler.fontScore);
    }

    public static void drawDial(Canvas canvas) {
        if (Globals.gameMode == 2) {
            if (Globals.characterType != 3 || Globals.getCharacterPowerUp() <= 0) {
                canvas.drawBitmap(ImageHandler.bonusTime[20], ((float) DIALX) * Globals.SCALEX, ((float) (DIALY + LOCY)) * Globals.SCALEY, (Paint) null);
            } else {
                canvas.drawBitmap(ImageHandler.bonusTime[(stopTimer / 20) + 1], ((float) DIALX) * Globals.SCALEX, ((float) (DIALY + LOCY)) * Globals.SCALEY, (Paint) null);
            }
            canvas.drawText(new StringBuilder(String.valueOf(puzzleMovesLeft)).toString(), ((float) DIALNUMX) * Globals.SCALEX, ((float) (DIALNUMY + LOCY)) * Globals.SCALEY, ImageHandler.fontBonus);
        } else if (Globals.getCharacterPowerUp() > 0) {
            if (Globals.characterType == 1) {
                canvas.drawBitmap(ImageHandler.bonusTime[20], ((float) DIALX) * Globals.SCALEX, ((float) (DIALY + LOCY)) * Globals.SCALEY, (Paint) null);
                canvas.drawText(new StringBuilder(String.valueOf(((Globals.getCharacterPowerUp() - 1) / Globals.EXPLODEPOWER) + 1)).toString(), ((float) DIALNUMX) * Globals.SCALEX, ((float) (DIALNUMY + LOCY)) * Globals.SCALEY, ImageHandler.fontBonus);
            } else if (Globals.characterType == 2) {
                canvas.drawBitmap(ImageHandler.bonusTime[20], ((float) DIALX) * Globals.SCALEX, ((float) (DIALY + LOCY)) * Globals.SCALEY, (Paint) null);
                canvas.drawText(new StringBuilder(String.valueOf(((Globals.getCharacterPowerUp() - 1) / 100) + 1)).toString(), ((float) DIALNUMX) * Globals.SCALEX, ((float) (DIALNUMY + LOCY)) * Globals.SCALEY, ImageHandler.fontBonus);
            } else if (Globals.characterType == 3) {
                canvas.drawBitmap(ImageHandler.bonusTime[((stopTimer % 40) / 2) + 1], ((float) DIALX) * Globals.SCALEX, ((float) (DIALY + LOCY)) * Globals.SCALEY, (Paint) null);
                canvas.drawText(new StringBuilder(String.valueOf(Globals.getCharacterPowerUp() / 80)).toString(), ((float) DIALNUMX) * Globals.SCALEX, ((float) (DIALNUMY + LOCY)) * Globals.SCALEY, ImageHandler.fontBonus);
            }
        } else if (stopTimer > 0) {
            canvas.drawBitmap(ImageHandler.bonusTime[((stopTimer % 40) / 2) + 1], ((float) DIALX) * Globals.SCALEX, ((float) (DIALY + LOCY)) * Globals.SCALEY, (Paint) null);
            canvas.drawText(new StringBuilder(String.valueOf(stopTimer / 40)).toString(), ((float) DIALNUMX) * Globals.SCALEX, ((float) (DIALNUMY + LOCY)) * Globals.SCALEY, ImageHandler.fontBonus);
        }
    }

    public static void drawGameMode(Canvas canvas) {
        ImageHandler.fontScore.setColor(-4473908);
        if (Globals.gameMode == 1) {
            canvas.drawText("ADVENTURE " + Globals.currentLevel, ((float) SCORELABELX) * Globals.SCALEX, ((float) (LOCY + SCORELABELY + 26)) * Globals.SCALEY, ImageHandler.fontScore);
        }
        if (Globals.gameMode == 2) {
            canvas.drawText("PUZZLE " + Globals.currentLevel, ((float) SCORELABELX) * Globals.SCALEX, ((float) (LOCY + SCORELABELY + 26)) * Globals.SCALEY, ImageHandler.fontScore);
        }
        if (Globals.gameMode == 3) {
            canvas.drawText("ENDLESS " + Globals.currentLevel, ((float) SCORELABELX) * Globals.SCALEX, ((float) (LOCY + SCORELABELY + 26)) * Globals.SCALEY, ImageHandler.fontScore);
        }
        ImageHandler.fontScore.setColor(-15198184);
    }

    public static void drawDebugText(Canvas canvas) {
        canvas.drawText("Combos  : " + level_PERFORM_COMBO, Globals.SCALEX * 20.0f, ((float) (LOCY + 17)) * Globals.SCALEY, ImageHandler.fontScore);
        canvas.drawText("Chains  : " + level_PERFORM_CHAINS, Globals.SCALEX * 20.0f, ((float) (LOCY + 34)) * Globals.SCALEY, ImageHandler.fontScore);
        canvas.drawText("Matches : " + level_PERFORM_MATCHES, Globals.SCALEX * 20.0f, ((float) (LOCY + 51)) * Globals.SCALEY, ImageHandler.fontScore);
        canvas.drawText("Garbage : " + level_BREAK_GARBAGE, Globals.SCALEX * 20.0f, ((float) (LOCY + 68)) * Globals.SCALEY, ImageHandler.fontScore);
        canvas.drawText("Time    : " + level_SURVIVE, Globals.SCALEX * 20.0f, ((float) (LOCY + 85)) * Globals.SCALEY, ImageHandler.fontScore);
        canvas.drawText("Current Level : " + Globals.currentLevel, Globals.SCALEX * 20.0f, ((float) (LOCY + 102)) * Globals.SCALEY, ImageHandler.fontScore);
    }

    private static void drawShakingText(Canvas canvas, String txt, int shakeStat2) {
        int shiftup;
        int shiftleft;
        if (shakeStat2 <= 0) {
            shiftup = 0;
            shiftleft = 0;
        } else {
            if (shakeStat2 % 4 < 2) {
                shiftup = -3;
            } else {
                shiftup = 3;
            }
            if (shakeStat2 % 6 < 2) {
                shiftleft = -3;
            } else if (shakeStat2 % 6 < 4) {
                shiftleft = 3;
            } else {
                shiftleft = 0;
            }
        }
        canvas.drawText(txt, ((float) (shiftleft + 20)) * Globals.SCALEX, ((float) (LOCY + SCORELABELY + shiftup + 26)) * Globals.SCALEY, ImageHandler.fontScore);
    }

    public static void drawWinCondition(Canvas canvas) {
        if (shakeStat > 0) {
            shakeStat--;
        }
        if (Globals.gameMode == 1) {
            if (LevelData.levelWinCondition[Globals.currentLevel - 1] == 3) {
                if (prevStat != level_BREAK_GARBAGE) {
                    shakeStat = 20;
                }
                drawShakingText(canvas, "Metal Blocks : " + level_BREAK_GARBAGE + " / " + LevelData.levelWinParameter[Globals.currentLevel - 1], shakeStat);
                prevStat = level_BREAK_GARBAGE;
            }
            if (LevelData.levelWinCondition[Globals.currentLevel - 1] == 5) {
                if (prevStat != level_PERFORM_ABILITY) {
                    shakeStat = 20;
                }
                drawShakingText(canvas, "Abilities : " + level_PERFORM_ABILITY + " / " + LevelData.levelWinParameter[Globals.currentLevel - 1], shakeStat);
                prevStat = level_PERFORM_ABILITY;
            }
            if (LevelData.levelWinCondition[Globals.currentLevel - 1] == 4) {
                if (prevStat != level_PERFORM_COMBO) {
                    shakeStat = 20;
                }
                drawShakingText(canvas, "Combos : " + level_PERFORM_COMBO + " / " + LevelData.levelWinParameter[Globals.currentLevel - 1], shakeStat);
                prevStat = level_PERFORM_COMBO;
            }
            if (LevelData.levelWinCondition[Globals.currentLevel - 1] == 1) {
                if (prevStat != level_PERFORM_MATCHES) {
                    shakeStat = 20;
                }
                drawShakingText(canvas, "Matches : " + level_PERFORM_MATCHES + " / " + LevelData.levelWinParameter[Globals.currentLevel - 1], shakeStat);
                prevStat = level_PERFORM_MATCHES;
            }
            if (LevelData.levelWinCondition[Globals.currentLevel - 1] == 2) {
                if (prevStat != Globals.getScore()) {
                    shakeStat = 20;
                }
                drawShakingText(canvas, "Score : " + Globals.getScore() + " / " + LevelData.levelWinParameter[Globals.currentLevel - 1], shakeStat);
                prevStat = Globals.getScore();
            }
            if (LevelData.levelWinCondition[Globals.currentLevel - 1] == 6) {
                shakeStat = 0;
                drawShakingText(canvas, "Tutorial", shakeStat);
            }
        } else if (Globals.gameMode == 3) {
            shakeStat = 0;
            canvas.drawText("Record : " + Globals.hScoreEndless, 20.0f * Globals.SCALEX, ((float) (LOCY + SCORELABELY + 26)) * Globals.SCALEY, ImageHandler.fontScore);
        }
    }

    public static void drawSuperSign(Canvas canvas) {
        if (superAniState != 0) {
            if (superAniState == 2) {
                Matrix tmpMatrix = new Matrix();
                if (superAniCounter < 5) {
                    tmpMatrix.postRotate(5.0f, (float) (ImageHandler.superSign.getWidth() / 2), (float) (ImageHandler.superSign.getHeight() / 2));
                    tmpMatrix.postTranslate(((float) superx) * Globals.SCALEX, ((float) supery) * Globals.SCALEY);
                } else {
                    tmpMatrix.postRotate(-5.0f, (float) (ImageHandler.superSign.getWidth() / 2), (float) (ImageHandler.superSign.getHeight() / 2));
                    tmpMatrix.postTranslate(((float) superx) * Globals.SCALEX, ((float) supery) * Globals.SCALEY);
                }
                canvas.drawBitmap(ImageHandler.superSign, tmpMatrix, null);
                if (Globals.rand.nextInt(10) <= 1) {
                    return;
                }
                if (Globals.characterType == 1) {
                    canvas.drawBitmap(ImageHandler.bomb, Globals.SCALEX * 180.0f, ((float) (supery + 40)) * Globals.SCALEY, (Paint) null);
                } else if (Globals.characterType == 2) {
                    canvas.drawBitmap(ImageHandler.bow, Globals.SCALEX * 180.0f, ((float) (supery + 40)) * Globals.SCALEY, (Paint) null);
                } else if (Globals.characterType == 3) {
                    canvas.drawBitmap(ImageHandler.hourglass, Globals.SCALEX * 180.0f, ((float) (supery + 40)) * Globals.SCALEY, (Paint) null);
                }
            } else {
                canvas.drawBitmap(ImageHandler.superSign, ((float) superx) * Globals.SCALEX, ((float) supery) * Globals.SCALEY, (Paint) null);
            }
        }
    }

    public static void draw(Canvas canvas) {
        if (Globals.getCharacterPowerUp() > 0) {
            backPaint.setARGB(255, 255, 125, 125);
        } else {
            backPaint.setARGB(255, 255, 255, 255);
        }
        Canvas canvas2 = canvas;
        canvas2.drawRect(0.0f, Globals.SCALEY * ((float) LOCY), Globals.SCALEX * ((float) Globals.GAMEX), Globals.SCALEY * ((float) Globals.GAMEY), backPaint);
        canvas.drawBitmap(ImageHandler.infoBG, 0.0f, ((float) LOCY) * Globals.SCALEY, (Paint) null);
        drawDial(canvas);
        drawScore(canvas, Globals.getScore());
        drawGameMode(canvas);
        drawWinCondition(canvas);
        drawSuperSign(canvas);
    }
}
