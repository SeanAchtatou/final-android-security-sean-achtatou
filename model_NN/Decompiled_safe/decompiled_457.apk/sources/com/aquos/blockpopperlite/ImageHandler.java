package com.aquos.blockpopperlite;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import com.aquos.blockpopperlite.Globals;
import java.lang.reflect.Array;

public class ImageHandler {
    public static final int BIGPUPILOFFSETX = 22;
    public static final int BIGPUPILOFFSETY = 17;
    private static final int EYEWHITEOFFSETX = 10;
    private static final int EYEWHITEOFFSETY = 9;
    private static final int PUPILOFFSETX = 27;
    private static final int PUPILOFFSETY = 23;
    private static final int PUPILRANGE = 5;
    public static Bitmap[] b_adventure = new Bitmap[2];
    public static Bitmap b_continue;
    public static Bitmap[] b_endless = new Bitmap[2];
    public static Bitmap[] b_hs = new Bitmap[2];
    public static Bitmap[] b_puzzle = new Bitmap[2];
    public static Bitmap[] b_unlock = new Bitmap[2];
    public static Bitmap[] backButton = new Bitmap[2];
    public static Canvas backgroundCanvas;
    public static Bitmap background_bright;
    public static Bitmap background_dark;
    public static Bitmap background_mm;
    public static Bitmap background_rays;
    public static Bitmap[] bigPupilBitmap = new Bitmap[7];
    public static Bitmap[] bigPupilTile = new Bitmap[7];
    public static Bitmap bigeyeShine;
    public static Bitmap blackOverlay;
    public static Bitmap[] blinkBitmap = new Bitmap[7];
    public static Bitmap[] blinkTile = new Bitmap[7];
    public static Bitmap bomb;
    public static Bitmap[] bonusTime = new Bitmap[21];
    public static Bitmap bow;
    public static Bitmap button_story_mode_down;
    public static Bitmap button_story_mode_up;
    public static Bitmap chainX;
    public static Bitmap[] charCard = new Bitmap[4];
    public static Bitmap[] charLeft = new Bitmap[2];
    public static Bitmap[] charRight = new Bitmap[2];
    public static Bitmap charSelectText;
    public static Bitmap[] closeButton = new Bitmap[2];
    public static Paint comboPaint;
    public static Bitmap cursor;
    public static Bitmap cursor1;
    public static Bitmap[] doodles = new Bitmap[17];
    public static Bitmap[] explodeBack = new Bitmap[7];
    public static Bitmap explodeFront;
    public static Paint explodePaint;
    public static Point explodeSize;
    public static Bitmap eyeWhite_reg;
    public static Paint fontBonus;
    public static Paint fontCombo;
    public static Paint fontLevelLock;
    public static Paint fontLevelSel;
    public static Paint fontNumber;
    public static Paint fontScore;
    public static Paint fontText;
    public static Paint fontUpArrow;
    public static Bitmap[] frontButton = new Bitmap[2];
    public static Bitmap goldStar;
    public static Paint highlightPaint;
    public static Bitmap hourglass;
    public static Bitmap infoBG;
    public static Bitmap[] levelSelectButton = new Bitmap[2];
    public static Bitmap levelSelectText;
    public static Resources mRes;
    public static Bitmap main_eye;
    public static Bitmap main_pupil;
    public static Bitmap main_title;
    public static Bitmap messageBG;
    public static Bitmap[] messageBubble = new Bitmap[4];
    public static Bitmap no;
    public static Paint normalPaint;
    public static Bitmap[][] normalTile = ((Bitmap[][]) Array.newInstance(Bitmap.class, 7, 5));
    public static Bitmap padlock;
    public static Bitmap padlock_s;
    public static Bitmap particleDust;
    public static Point particleDustSize;
    public static Bitmap particlePentagon;
    public static Point particlePentagonSize;
    public static Bitmap[] pupilBitmap = new Bitmap[7];
    public static Paint selectPaint;
    private static int slideShowFrame;
    public static Bitmap[] star = new Bitmap[2];
    public static Bitmap storySlideShow;
    public static Bitmap superSign;
    public static Bitmap tick;
    public static Bitmap[] tileBitmap = new Bitmap[7];
    public static int[] tileColor = new int[7];
    public static Bitmap upArrow;
    public static Bitmap yes;

    public static void loadEssential(Context context) {
        if (mRes == null) {
            mRes = context.getResources();
        }
        if (main_title == null) {
            main_title = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.main_name), 454, 261);
        }
    }

    public static void Initialize(Context context) {
        if (!Globals.isInitialized) {
            Globals.initialize();
        }
        initializeMainMenu();
        initializeBackground();
        initializeInfoPanel();
        initializeTiles();
        initializePaints();
        initializeParticles();
        initializeDialog();
        initializeTimeDial();
        initializeButtons();
        initializeSlideShow();
        initializeCharSelect();
        initializeLevelSelect();
        initializeFonts(context);
        initializeMessages();
    }

    private static void initializeLevelSelect() {
        levelSelectButton[0] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.levelbutton_up), 140, 140);
        levelSelectButton[1] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.levelbutton_down), 140, 140);
        levelSelectText = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.levelselect), 460, 90);
        padlock = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.padlock), 80, 110);
        tick = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.tick), 60, 60);
    }

    private static void initializeCharSelect() {
        charLeft[0] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.charleft_up), 142, 122);
        charLeft[1] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.charleft_down), 142, 122);
        charRight[0] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.charright_up), 142, 122);
        charRight[1] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.charright_down), 142, 122);
        charSelectText = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.heroselect), 439, 89);
        charCard[0] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.card_miner), MessageHandler.ABILITYX3, 480);
        charCard[1] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.card_archer), MessageHandler.ABILITYX3, 480);
        charCard[2] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.card_mage), MessageHandler.ABILITYX3, 480);
    }

    private static void initializeMainMenu() {
        background_mm = makeScaledBitmapOpague(BitmapFactory.decodeResource(mRes, R.drawable.main_menu), 480, 800);
        if (main_title == null) {
            main_title = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.main_name), 432, 248);
        }
        b_adventure[0] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.b_adventure_up), 239, 93);
        b_adventure[1] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.b_adventure_down), 239, 93);
        b_puzzle[0] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.b_puzzle_up), 159, 83);
        b_puzzle[1] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.b_puzzle_down), 159, 83);
        b_endless[0] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.b_endless_up), 183, 86);
        b_endless[1] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.b_endless_down), 183, 86);
        b_hs[0] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.b_hs_up), 266, 96);
        b_hs[1] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.b_hs_down), 266, 96);
        b_unlock[0] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.b_unlock_up), 342, 105);
        b_unlock[1] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.b_unlock_down), 342, 105);
        b_continue = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.b_continue), 125, 67);
        main_eye = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.main_eye), 103, 103);
        main_pupil = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.main_pupil), 44, 44);
    }

    public static void initializeBackground() {
        if (background_rays == null || background_dark == null || background_bright == null) {
            background_rays = makeScaledBitmapOpague(BitmapFactory.decodeResource(mRes, R.drawable.background_rays), 380, 380);
            background_dark = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.background_dark), 480, 150);
            background_bright = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.background_bright), 480, 130);
        }
    }

    private static void initializeInfoPanel() {
        infoBG = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.infobar), 480, 120);
        cursor = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.cursor), BackgroundHandler.DARKCLOUDY, 80);
        cursor1 = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.cursor), 80, 80);
        superSign = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.super_sign), 200, 100);
        bomb = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bomb), 60, 60);
        hourglass = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.hourglass), 60, 60);
        bow = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bow), 60, 60);
        upArrow = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.uparrow), 40, 40);
    }

    private static void initializeSlideShow() {
        storySlideShow = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.storyframe1), 458, 458);
        slideShowFrame = 1;
    }

    public static Bitmap getSlideShowFrame(int i) {
        if (i != slideShowFrame) {
            if (i == 1) {
                storySlideShow = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.storyframe1), 458, 458);
                slideShowFrame = 1;
                System.gc();
            } else if (i == 2) {
                storySlideShow = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.storyframe2), 458, 458);
                slideShowFrame = 2;
                System.gc();
            } else if (i == 3) {
                storySlideShow = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.storyframe3), 458, 458);
                slideShowFrame = 3;
                System.gc();
            } else if (i == 4) {
                storySlideShow = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.storyframe4), 458, 458);
                slideShowFrame = 4;
                System.gc();
            } else {
                storySlideShow = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.storyframe1), 458, 458);
                slideShowFrame = 1;
                System.gc();
            }
        }
        return storySlideShow;
    }

    private static void initializeButtons() {
        button_story_mode_up = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.button_story_mode_up), 120, 60);
        button_story_mode_down = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.button_story_mode_down), 120, 60);
    }

    private static void initializeTimeDial() {
        bonusTime[0] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime00), 98, 98);
        bonusTime[1] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime01), 98, 98);
        bonusTime[2] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime02), 98, 98);
        bonusTime[3] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime03), 98, 98);
        bonusTime[4] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime04), 98, 98);
        bonusTime[5] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime05), 98, 98);
        bonusTime[6] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime06), 98, 98);
        bonusTime[7] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime07), 98, 98);
        bonusTime[8] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime08), 98, 98);
        bonusTime[9] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime09), 98, 98);
        bonusTime[10] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime10), 98, 98);
        bonusTime[11] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime11), 98, 98);
        bonusTime[12] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime12), 98, 98);
        bonusTime[13] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime13), 98, 98);
        bonusTime[14] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime14), 98, 98);
        bonusTime[15] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime15), 98, 98);
        bonusTime[16] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime16), 98, 98);
        bonusTime[17] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime17), 98, 98);
        bonusTime[18] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime18), 98, 98);
        bonusTime[19] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime19), 98, 98);
        bonusTime[20] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bonustime20), 98, 98);
    }

    private static void initializeFonts(Context context) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/paperj.ttf");
        fontCombo = new Paint(1);
        fontCombo.setTypeface(typeface);
        fontCombo.setTextSize(40.0f * Globals.SCALEX);
        fontCombo.setColor(-16777216);
        Typeface typeface2 = Typeface.createFromAsset(context.getAssets(), "fonts/rockb.ttf");
        fontNumber = new Paint(1);
        fontNumber.setTypeface(typeface2);
        fontNumber.setTextSize(38.0f * Globals.SCALEX);
        fontNumber.setColor(-1);
        Typeface typeface3 = Typeface.createFromAsset(context.getAssets(), "fonts/sugo.ttf");
        fontBonus = new Paint(1);
        fontBonus.setTypeface(typeface3);
        fontBonus.setTextSize(48.0f * Globals.SCALEX);
        fontBonus.setColor(-1);
        fontBonus.setTextAlign(Paint.Align.CENTER);
        fontScore = new Paint(1);
        fontScore.setTypeface(Typeface.create("DroidSerif-Bold", 1));
        fontScore.setTextSize(19.0f * Globals.SCALEX);
        fontScore.setColor(-15198184);
        Typeface typeface4 = Typeface.createFromAsset(context.getAssets(), "fonts/sugo.ttf");
        fontLevelSel = new Paint(1);
        fontLevelSel.setTypeface(Typeface.create("DroidSerif-Bold", 1));
        fontLevelSel.setTextSize(62.0f * Globals.SCALEX);
        fontLevelSel.setColor(-1);
        fontLevelSel.setTextAlign(Paint.Align.CENTER);
        fontLevelLock = new Paint(1);
        fontLevelLock.setTypeface(Typeface.create("DroidSerif-Bold", 1));
        fontLevelLock.setTextSize(25.0f * Globals.SCALEX);
        fontLevelLock.setColor(0);
        fontLevelLock.setTextAlign(Paint.Align.CENTER);
        Typeface typeface5 = Typeface.createFromAsset(context.getAssets(), "fonts/sugo.ttf");
        fontUpArrow = new Paint(1);
        fontUpArrow.setTypeface(Typeface.create("DroidSerif-Bold", 0));
        fontUpArrow.setTextSize(24.0f * Globals.SCALEX);
        fontUpArrow.setColor(0);
        fontUpArrow.setTextAlign(Paint.Align.LEFT);
        Typeface typeface6 = Typeface.createFromAsset(context.getAssets(), "fonts/bellgothicstdblack.ttf");
        fontText = new Paint(1);
        fontText.setTypeface(typeface6);
        fontText.setTextSize(16.0f * Globals.SCALEX);
        fontText.setColor(-16777216);
        fontText.setTextAlign(Paint.Align.LEFT);
    }

    private static void initializeMessages() {
        padlock_s = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.padlock), 70, 70);
        backButton[0] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.backbutton_up), 64, 64);
        backButton[1] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.backbutton_down), 64, 64);
        frontButton[0] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.frontbutton_up), 64, 64);
        frontButton[1] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.frontbutton_down), 64, 64);
        closeButton[0] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.closebutton_up), 64, 64);
        closeButton[1] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.closebutton_down), 64, 64);
        star[0] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.star), 80, 80);
        star[1] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.starframe), 80, 80);
        messageBubble[0] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bubble0), 120, 77);
        messageBubble[1] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bubble1), 120, 77);
        messageBubble[2] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bubble2), 120, 77);
        messageBubble[3] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bubble3), 120, 77);
        messageBG = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.messagebg), Globals.EXPLODEPOWER, 250);
        doodles[0] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.doodle0), 80, 200);
        doodles[1] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.doodle1), 65, 73);
        doodles[2] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.block_garbage), 70, 70);
        doodles[3] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.doodle3), 65, 97);
        doodles[4] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.doodle4), 64, 119);
        doodles[5] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.doodle5), 80, 200);
        doodles[6] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.doodle6), 80, 200);
        doodles[7] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.doodle7), 80, 200);
        doodles[8] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bomb), 70, 70);
        doodles[9] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bow), 70, 70);
        doodles[10] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.hourglass), 70, 70);
        doodles[11] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.doodle1112), 80, 80);
        doodles[12] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.doodle1112), 80, 80);
        doodles[13] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.doodle13), 80, 80);
        doodles[14] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.doodle14), 80, 80);
        doodles[15] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.doodle15), 50, 60);
        doodles[16] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.crown), 80, 80);
    }

    private static void initializeTiles() {
        int offsetx;
        int offsety;
        Rect dr;
        int offsetx2;
        int offsety2;
        tileBitmap[Globals.BlockType.blue.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.block_blue), 80, 80);
        tileBitmap[Globals.BlockType.green.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.block_green), 80, 80);
        tileBitmap[Globals.BlockType.red.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.block_red), 80, 80);
        tileBitmap[Globals.BlockType.indigo.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.block_indigo), 80, 80);
        tileBitmap[Globals.BlockType.yellow.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.block_yellow), 80, 80);
        tileBitmap[Globals.BlockType.gold.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.block_gold), 80, 80);
        tileBitmap[Globals.BlockType.garbage.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.block_garbage), 80, 80);
        blinkBitmap[Globals.BlockType.blue.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.blink_blue), 66, 66);
        blinkBitmap[Globals.BlockType.green.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.blink_green), 66, 66);
        blinkBitmap[Globals.BlockType.red.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.blink_red), 66, 66);
        blinkBitmap[Globals.BlockType.indigo.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.blink_indigo), 66, 66);
        blinkBitmap[Globals.BlockType.yellow.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.blink_yellow), 66, 66);
        blinkBitmap[Globals.BlockType.gold.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.blink_yellow), 66, 66);
        blinkBitmap[Globals.BlockType.garbage.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.blink_yellow), 66, 66);
        pupilBitmap[Globals.BlockType.blue.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.pupil_blue), 26, 26);
        pupilBitmap[Globals.BlockType.green.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.pupil_green), 26, 26);
        pupilBitmap[Globals.BlockType.red.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.pupil_red), 26, 26);
        pupilBitmap[Globals.BlockType.indigo.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.pupil_indigo), 26, 26);
        pupilBitmap[Globals.BlockType.yellow.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.pupil_yellow), 26, 26);
        pupilBitmap[Globals.BlockType.gold.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.pupil_yellow), 26, 26);
        pupilBitmap[Globals.BlockType.garbage.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.pupil_yellow), 26, 26);
        bigPupilBitmap[Globals.BlockType.blue.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bigeye_blue), 36, 36);
        bigPupilBitmap[Globals.BlockType.green.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bigeye_green), 36, 36);
        bigPupilBitmap[Globals.BlockType.red.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bigeye_red), 36, 36);
        bigPupilBitmap[Globals.BlockType.indigo.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bigeye_indigo), 36, 36);
        bigPupilBitmap[Globals.BlockType.yellow.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bigeye_yellow), 36, 36);
        bigPupilBitmap[Globals.BlockType.gold.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bigeye_yellow), 36, 36);
        bigPupilBitmap[Globals.BlockType.garbage.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bigeye_yellow), 36, 36);
        tileColor[Globals.BlockType.blue.ordinal()] = -8409089;
        tileColor[Globals.BlockType.green.ordinal()] = -8388801;
        tileColor[Globals.BlockType.red.ordinal()] = -49281;
        tileColor[Globals.BlockType.indigo.ordinal()] = -8437761;
        tileColor[Globals.BlockType.yellow.ordinal()] = -193;
        tileColor[Globals.BlockType.gold.ordinal()] = -8409089;
        tileColor[Globals.BlockType.garbage.ordinal()] = -16777216;
        bigeyeShine = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.bigeye_shine), 36, 36);
        eyeWhite_reg = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.eyewhite_reg), 60, 60);
        goldStar = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.gold_star), 46, 46);
        for (int i = 0; i < Globals.blockTypes.length; i++) {
            for (int j = 0; j < 5; j++) {
                normalTile[i][j] = Bitmap.createBitmap(tileBitmap[i].getWidth(), tileBitmap[i].getHeight(), Bitmap.Config.ARGB_8888);
                Canvas c = new Canvas(normalTile[i][j]);
                if (i == Globals.BlockType.gold.ordinal()) {
                    c.drawBitmap(tileBitmap[i], 0.0f, 0.0f, (Paint) null);
                    if (j == 0) {
                        offsetx2 = 0;
                        offsety2 = 0;
                    } else if (j == 1) {
                        offsetx2 = 26;
                        offsety2 = 26;
                    } else if (j == 2) {
                        offsetx2 = 0;
                        offsety2 = 0;
                    } else if (j == 3) {
                        offsetx2 = 22;
                        offsety2 = 7;
                    } else if (j == 4) {
                        offsetx2 = 5;
                        offsety2 = 8;
                    } else {
                        offsetx2 = 13;
                        offsety2 = 13;
                    }
                    if (j != 0) {
                        c.drawBitmap(goldStar, ((float) offsetx2) * Globals.SCALEX, ((float) offsety2) * Globals.SCALEY, (Paint) null);
                    }
                } else if (i != Globals.BlockType.garbage.ordinal()) {
                    c.drawBitmap(tileBitmap[i], 0.0f, 0.0f, (Paint) null);
                    c.drawBitmap(eyeWhite_reg, 10.0f * Globals.SCALEX, 9.0f * Globals.SCALEY, (Paint) null);
                    if (j == Globals.LOOKDIR.LEFT.ordinal()) {
                        offsetx = 22;
                        offsety = PUPILOFFSETY;
                    } else if (j == Globals.LOOKDIR.RIGHT.ordinal()) {
                        offsetx = 32;
                        offsety = PUPILOFFSETY;
                    } else if (j == Globals.LOOKDIR.UP.ordinal()) {
                        offsetx = PUPILOFFSETX;
                        offsety = 18;
                    } else if (j == Globals.LOOKDIR.DOWN.ordinal()) {
                        offsetx = PUPILOFFSETX;
                        offsety = 28;
                    } else {
                        offsetx = PUPILOFFSETX;
                        offsety = PUPILOFFSETY;
                    }
                    c.drawBitmap(pupilBitmap[i], ((float) offsetx) * Globals.SCALEX, ((float) offsety) * Globals.SCALEY, (Paint) null);
                } else if (j == 0) {
                    c.drawBitmap(tileBitmap[i], 0.0f, 0.0f, (Paint) null);
                } else {
                    Rect sr = new Rect(0, 0, tileBitmap[i].getWidth(), tileBitmap[i].getHeight());
                    if (j == 1) {
                        dr = new Rect(0, 0, tileBitmap[i].getWidth() - 4, tileBitmap[i].getHeight() - 4);
                    } else if (j == 2) {
                        dr = new Rect(4, 4, tileBitmap[i].getWidth(), tileBitmap[i].getHeight());
                    } else if (j == 3) {
                        dr = new Rect(0, 4, tileBitmap[i].getWidth() - 4, tileBitmap[i].getHeight());
                    } else if (j == 4) {
                        dr = new Rect(4, 0, tileBitmap[i].getWidth(), tileBitmap[i].getHeight() - 4);
                    } else {
                        dr = new Rect(0, 0, tileBitmap[i].getWidth() - 4, tileBitmap[i].getHeight() - 4);
                    }
                    c.drawBitmap(tileBitmap[i], sr, dr, (Paint) null);
                }
            }
        }
        for (int i2 = 0; i2 < Globals.blockTypes.length; i2++) {
            blinkTile[i2] = Bitmap.createBitmap(tileBitmap[i2].getWidth(), tileBitmap[i2].getHeight(), Bitmap.Config.ARGB_8888);
            Canvas c2 = new Canvas(blinkTile[i2]);
            c2.drawBitmap(tileBitmap[i2], 0.0f, 0.0f, (Paint) null);
            c2.drawBitmap(blinkBitmap[i2], 8.0f * Globals.SCALEX, 7.0f * Globals.SCALEY, (Paint) null);
        }
        for (int i3 = 0; i3 < Globals.blockTypes.length; i3++) {
            bigPupilTile[i3] = Bitmap.createBitmap(tileBitmap[i3].getWidth(), tileBitmap[i3].getHeight(), Bitmap.Config.ARGB_8888);
            Canvas c3 = new Canvas(bigPupilTile[i3]);
            c3.drawBitmap(tileBitmap[i3], 0.0f, 0.0f, (Paint) null);
            c3.drawBitmap(eyeWhite_reg, 10.0f * Globals.SCALEX, 9.0f * Globals.SCALEY, (Paint) null);
            c3.drawBitmap(bigPupilBitmap[i3], 22.0f * Globals.SCALEX, 17.0f * Globals.SCALEY, (Paint) null);
        }
    }

    private static Bitmap makeScaledBitmap(Bitmap ori, int w, int h) {
        int w2 = (int) (((float) w) * Globals.SCALEX);
        int h2 = (int) (((float) h) * Globals.SCALEY);
        int width = ori.getWidth();
        int height = ori.getHeight();
        Bitmap result = Bitmap.createBitmap(w2, h2, Bitmap.Config.ARGB_8888);
        new Canvas(result).drawBitmap(ori, new Rect(0, 0, width, height), new Rect(0, 0, w2, h2), (Paint) null);
        return result;
    }

    private static Bitmap makeScaledBitmapOpague(Bitmap ori, int w, int h) {
        int w2 = (int) (((float) w) * Globals.SCALEX);
        int h2 = (int) (((float) h) * Globals.SCALEY);
        int width = ori.getWidth();
        int height = ori.getHeight();
        Bitmap result = Bitmap.createBitmap(w2, h2, Bitmap.Config.RGB_565);
        new Canvas(result).drawBitmap(ori, new Rect(0, 0, width, height), new Rect(0, 0, w2, h2), (Paint) null);
        return result;
    }

    private static void initializeDialog() {
        yes = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.yes), 100, 100);
        no = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.no), 100, 100);
    }

    private static void initializePaints() {
        selectPaint = new Paint();
        highlightPaint = new Paint();
        explodePaint = new Paint();
        normalPaint = new Paint();
        selectPaint.setARGB(200, 255, 100, 100);
        highlightPaint.setARGB(100, 255, 255, 255);
        explodePaint.setARGB(50, 255, 255, 255);
        comboPaint = new Paint();
        comboPaint.setColor(-16777216);
        comboPaint.setTextSize(20.0f);
    }

    private static void initializeParticles() {
        particlePentagon = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.particle_pentagon), 12, 12);
        particlePentagonSize = new Point(particlePentagon.getWidth(), particlePentagon.getHeight());
        particleDust = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.particle_dust), 22, 22);
        particleDustSize = new Point(particlePentagon.getWidth(), particleDust.getHeight());
        explodeFront = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.explode_front), 80, 80);
        explodeBack[Globals.BlockType.blue.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.explosion_glow_blue), 112, 112);
        explodeBack[Globals.BlockType.green.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.explosion_glow_green), 112, 112);
        explodeBack[Globals.BlockType.red.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.explosion_glow_red), 112, 112);
        explodeBack[Globals.BlockType.indigo.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.explosion_glow_indigo), 112, 112);
        explodeBack[Globals.BlockType.yellow.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.explosion_glow_yellow), 112, 112);
        explodeBack[Globals.BlockType.gold.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.explosion_glow_yellow), 112, 112);
        explodeBack[Globals.BlockType.garbage.ordinal()] = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.explosion_glow_yellow), 112, 112);
        explodeSize = new Point(explodeBack[0].getWidth(), explodeBack[0].getHeight());
        blackOverlay = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.blackoverlay), 80, 80);
        chainX = makeScaledBitmap(BitmapFactory.decodeResource(mRes, R.drawable.chain_x), 22, 22);
    }
}
