package com.aquos.blockpopperlite;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import com.aquos.blockpopperlite.Globals;
import java.util.List;

public class Tile {
    private static final int ANI_STATE_FALLING = 2;
    private static final int ANI_STATE_FORCELOOK = 4;
    private static final int ANI_STATE_LOST = 5;
    private static final int ANI_STATE_NORMAL = 0;
    private static final int ANI_STATE_POSTFALL = 3;
    private static final int ANI_STATE_PREFALL = 1;
    private static final long[] ERRORVIBRATE = {20, 20, 30, 20, 30, 20};
    private static final long[] LONGVIBRATE = {20, 20};
    public static final int NERVOUS_THRESHOLD = 120;
    public static final int PUPIL_DELAY_GOLDSTAR = 20;
    public static final int PUPIL_DELAY_NERVOUS = 7;
    public static final int PUPIL_DELAY_NORMAL = 80;
    private static final int SELECTEDSHIFT = 6;
    private static final long[] SHORTVIBRATE = {20, 10};
    public static final int TIME_CREATE_EXPLODE = 5;
    public static final int TIME_EXPLODE = 15;
    public static final int TIME_EXPLODE_DURATION = 25;
    public static final int TIME_REMOVE = 45;
    private static int i;
    private static Matrix tmpMatrix = new Matrix();
    private static Paint tmpPaint = new Paint();
    private int aniCounter;
    private boolean aniIsBlinking;
    private boolean aniIsShaking;
    private Globals.LOOKDIR aniLookDir;
    private int aniState;
    public Globals.BlockType blockType;
    public int comboCounter;
    public Point dest;
    public float dropSpeed;
    public int explodeTimer;
    public boolean isDestroyed;
    public boolean isExploding;
    public boolean isLost;
    public boolean isMoving;
    public boolean isMovingHorizontal;
    public boolean isMovingHorizontal_d;
    public boolean isMovingVertical;
    public boolean isRemoved;
    public boolean isSelected;
    public float locx;
    public float locy;
    public int nextComboCounter;
    public boolean powerupPending;
    private int pupilDelay;
    public boolean verticalSwapPulse;

    public boolean isDraggable() {
        return this.isRemoved || ((!this.isExploding || this.isDestroyed) && !this.isMovingVertical && !this.isMovingHorizontal && this.blockType != Globals.BlockType.garbage);
    }

    public boolean isSelectable() {
        return !this.isDestroyed && !this.isRemoved && isDraggable();
    }

    public boolean isAlive() {
        return !this.isRemoved && !this.isExploding && !this.isDestroyed;
    }

    public boolean hasActivity() {
        return this.isMovingVertical || this.isExploding;
    }

    public void reset() {
        this.isLost = false;
        this.isDestroyed = false;
        this.isRemoved = false;
        this.isExploding = false;
        this.isMovingHorizontal = false;
        this.isMovingHorizontal_d = false;
        this.isMovingVertical = false;
        this.isMoving = false;
        this.powerupPending = false;
        this.verticalSwapPulse = false;
        this.isSelected = false;
        this.explodeTimer = 0;
        this.dropSpeed = 0.0f;
        this.comboCounter = 0;
        this.nextComboCounter = 0;
        this.aniLookDir = Globals.LOOKDIR.CENTER;
        this.aniCounter = 0;
        this.aniIsBlinking = true;
        this.aniIsShaking = false;
        this.pupilDelay = 80;
        this.aniState = 0;
    }

    public Tile(Globals.BlockType b, int x, int y, int shf) {
        this.isLost = false;
        this.isDestroyed = false;
        this.isExploding = false;
        this.isRemoved = false;
        this.isMovingHorizontal = false;
        this.isMovingHorizontal_d = false;
        this.isMovingVertical = false;
        this.isMoving = false;
        this.powerupPending = false;
        this.verticalSwapPulse = false;
        this.isSelected = false;
        this.explodeTimer = 0;
        this.dropSpeed = 0.0f;
        this.comboCounter = 0;
        this.nextComboCounter = 0;
        this.aniLookDir = Globals.LOOKDIR.CENTER;
        this.aniCounter = 0;
        this.aniIsBlinking = true;
        this.aniIsShaking = false;
        this.pupilDelay = 80;
        this.aniState = 0;
        this.blockType = b;
        this.locx = (float) x;
        this.locy = (float) y;
        this.dest = new Point(x, y);
    }

    public Tile(int x, int y, int shf) {
        this.isLost = false;
        this.isDestroyed = false;
        this.isExploding = false;
        this.isRemoved = false;
        this.isMovingHorizontal = false;
        this.isMovingHorizontal_d = false;
        this.isMovingVertical = false;
        this.isMoving = false;
        this.powerupPending = false;
        this.verticalSwapPulse = false;
        this.isSelected = false;
        this.explodeTimer = 0;
        this.dropSpeed = 0.0f;
        this.comboCounter = 0;
        this.nextComboCounter = 0;
        this.aniLookDir = Globals.LOOKDIR.CENTER;
        this.aniCounter = 0;
        this.aniIsBlinking = true;
        this.aniIsShaking = false;
        this.pupilDelay = 80;
        this.aniState = 0;
        this.blockType = Globals.blockTypes[0];
        this.locx = (float) x;
        this.locy = (float) y;
        this.dest = new Point(x, y);
        removeTile();
    }

    public void loseTile() {
        this.isLost = true;
        this.aniState = 5;
    }

    public void reuseTile(Globals.BlockType b, int x, int y, int shf) {
        reset();
        this.blockType = b;
        this.locx = (float) x;
        this.locy = (float) y;
        this.dest = new Point(x, y);
    }

    public void changeColor(Globals.BlockType changeTo) {
        this.blockType = changeTo;
    }

    public void move(Point newDest) {
        if (newDest.x != this.dest.x) {
            this.isMovingHorizontal = true;
            this.isMoving = true;
        }
        if (newDest.y != this.dest.y) {
            this.isMovingVertical = true;
            this.isMoving = true;
        }
        this.dest = newDest;
        this.dropSpeed = (((float) newDest.y) - this.locy) / ((float) Globals.TILESY);
    }

    public void dragMove(int diff) {
        int netMove = (int) ((((float) diff) - this.locx) + ((float) this.dest.x));
        if (netMove > 0 && netMove > 20) {
            this.locx += 20.0f;
        } else if (netMove >= 0 || netMove >= -20) {
            this.locx += (float) netMove;
        } else {
            this.locx -= 20.0f;
        }
    }

    public void shiftTileUp() {
        this.locy -= (float) Globals.TILESY;
        this.dest = new Point(this.dest.x, this.dest.y - Globals.TILESY);
    }

    public void update(List<Explosion> explosions) {
        updateAnimation();
        updateMove();
        updateExplode(explosions);
    }

    public void forceLook(Globals.LOOKDIR dir) {
        if (isSelectable()) {
            this.aniLookDir = dir;
            this.aniState = 4;
            this.aniCounter = 0;
        }
    }

    public void updateAnimationNormal() {
        Globals.LOOKDIR lookdir;
        Globals.LOOKDIR lookdir2;
        Globals.LOOKDIR lookdir3;
        if (this.aniState == 5) {
            this.aniLookDir = Globals.LOOKDIR.CENTER;
            this.aniIsBlinking = true;
        }
        if (this.aniState == 0 && this.comboCounter > 0) {
            this.aniState = 1;
            this.aniCounter = 0;
        }
        if (this.aniState == 1 && this.isMovingVertical) {
            this.aniState = 2;
            this.aniCounter = 0;
        }
        if (this.aniState == 2 && !this.isMovingVertical) {
            this.aniState = 3;
            this.aniCounter = 0;
        }
        if (this.aniState == 3 && this.aniCounter >= 5) {
            this.aniState = 0;
        }
        if (this.aniState == 4 && this.aniCounter >= 30) {
            this.aniState = 0;
        }
        if (this.aniState == 0) {
            if (this.locy >= 120.0f) {
                this.pupilDelay = 80;
                if (this.aniCounter >= this.pupilDelay && Globals.rand.nextInt(this.pupilDelay / 3) == 0) {
                    this.aniCounter = 0;
                    switch (Globals.rand.nextInt(5)) {
                        case 0:
                            this.aniLookDir = Globals.LOOKDIR.CENTER;
                            break;
                        case 1:
                            this.aniLookDir = Globals.LOOKDIR.LEFT;
                            break;
                        case 2:
                            this.aniLookDir = Globals.LOOKDIR.RIGHT;
                            break;
                        case 3:
                            this.aniLookDir = Globals.LOOKDIR.UP;
                            break;
                        case 4:
                            this.aniLookDir = Globals.LOOKDIR.DOWN;
                            break;
                        default:
                            this.aniLookDir = Globals.LOOKDIR.CENTER;
                            break;
                    }
                }
            } else {
                this.pupilDelay = 7;
                if (this.aniCounter >= this.pupilDelay && Globals.rand.nextInt(this.pupilDelay / 3) == 0) {
                    this.aniCounter = 0;
                    i = Globals.rand.nextInt(5);
                    if (this.aniLookDir == Globals.LOOKDIR.LEFT) {
                        if (i == 0) {
                            lookdir3 = Globals.LOOKDIR.UP;
                        } else if (i == 1) {
                            lookdir3 = Globals.LOOKDIR.DOWN;
                        } else {
                            lookdir3 = Globals.LOOKDIR.RIGHT;
                        }
                        this.aniLookDir = lookdir3;
                    } else if (this.aniLookDir == Globals.LOOKDIR.RIGHT) {
                        this.aniLookDir = i == 0 ? Globals.LOOKDIR.UP : i == 1 ? Globals.LOOKDIR.DOWN : Globals.LOOKDIR.LEFT;
                    } else if (this.aniLookDir == Globals.LOOKDIR.UP) {
                        if (i == 0) {
                            lookdir2 = Globals.LOOKDIR.LEFT;
                        } else {
                            lookdir2 = i == 1 ? Globals.LOOKDIR.RIGHT : Globals.LOOKDIR.DOWN;
                        }
                        this.aniLookDir = lookdir2;
                    } else if (this.aniLookDir == Globals.LOOKDIR.DOWN) {
                        if (i == 0) {
                            lookdir = Globals.LOOKDIR.LEFT;
                        } else {
                            lookdir = i == 1 ? Globals.LOOKDIR.RIGHT : Globals.LOOKDIR.UP;
                        }
                        this.aniLookDir = lookdir;
                    } else {
                        this.aniLookDir = i == 0 ? Globals.LOOKDIR.UP : i == 1 ? Globals.LOOKDIR.DOWN : i == 3 ? Globals.LOOKDIR.LEFT : Globals.LOOKDIR.RIGHT;
                    }
                }
            }
            this.aniIsBlinking = false;
        }
        if (this.aniState == 1) {
            if (this.aniCounter <= 30) {
                this.aniLookDir = Globals.LOOKDIR.DOWN;
                this.aniIsBlinking = false;
            } else {
                this.aniLookDir = Globals.LOOKDIR.CENTER;
                this.aniIsBlinking = false;
            }
        }
        if (this.aniState == 2) {
            this.aniLookDir = Globals.LOOKDIR.CENTER;
            this.aniIsBlinking = false;
        }
        if (this.aniState == 3 && this.aniCounter <= 5) {
            this.aniIsBlinking = true;
        }
        if (this.aniState == 4) {
            this.aniIsBlinking = false;
        }
    }

    public void updateAnimationGold() {
        if (this.aniLookDir.ordinal() == 0) {
            if (this.aniCounter >= 10 && Globals.rand.nextInt(3) == 0) {
                this.aniCounter = 0;
                i = Globals.rand.nextInt(4);
                this.aniLookDir = Globals.lookDirs[i + 1];
            }
        } else if (this.aniCounter >= 20 && Globals.rand.nextInt(6) == 0) {
            this.aniCounter = 0;
            this.aniLookDir = Globals.lookDirs[0];
        }
    }

    public void updateAnimationGarbage() {
        if (this.aniIsShaking && this.aniCounter > 4) {
            this.aniIsShaking = false;
        }
    }

    public void checkGarbageError() {
        if (!this.isRemoved && !this.isDestroyed && this.blockType == Globals.BlockType.garbage) {
            if (Globals.settings[1]) {
                Globals.vibrator.vibrate(ERRORVIBRATE, -1);
            }
            this.aniIsShaking = true;
            this.aniCounter = 0;
        }
    }

    public void updateAnimation() {
        this.aniCounter++;
        if (this.blockType == Globals.BlockType.gold) {
            updateAnimationGold();
        } else if (this.blockType == Globals.BlockType.garbage) {
            updateAnimationGarbage();
        } else {
            updateAnimationNormal();
        }
    }

    public void removeTile() {
        this.isRemoved = true;
        this.isExploding = false;
        this.isDestroyed = true;
    }

    public void updateExplode(List<Explosion> explosions) {
        if (this.isExploding) {
            this.explodeTimer++;
            if (this.isSelected) {
                BlockOperations.clearAllSelected(ScreenGame.tiles);
            }
            if (this.explodeTimer == 30) {
                if (this.blockType == Globals.BlockType.gold) {
                    SoundHandler.playWave(SoundHandler.glassSound);
                } else {
                    SoundHandler.playWave(SoundHandler.popSound);
                }
            }
            if (this.explodeTimer >= 45) {
                removeTile();
                if (this.powerupPending && this.blockType == Globals.BlockType.gold) {
                    Globals.addPowerUp(200);
                }
                if (this.blockType == Globals.BlockType.garbage) {
                    Globals.addScore(50);
                }
                Globals.triggerPostExplode = true;
            } else if (this.explodeTimer == 15) {
                this.isDestroyed = true;
                Globals.particleGenerator.pulse(((int) this.locx) + (Globals.TILESX / 2) + Globals.BLOCKLEFTOFFSET, ((((int) this.locy) + (Globals.TILESY / 2)) + Globals.BLOCKTOPOFFSET) - ((int) Globals.scrollOffset), ImageHandler.tileColor[this.blockType.ordinal()]);
                if (this.blockType == Globals.BlockType.gold) {
                    Globals.particleGenerator.star(((int) this.locx) + (Globals.TILESX / 2) + Globals.BLOCKLEFTOFFSET, ((((int) this.locy) + (Globals.TILESY / 2)) + Globals.BLOCKTOPOFFSET) - ((int) Globals.scrollOffset));
                }
            } else if (this.explodeTimer == 5) {
                explosions.add(new Explosion((int) this.locx, (int) this.locy, this.blockType));
            }
        }
    }

    public void updateMove() {
        boolean z;
        this.verticalSwapPulse = false;
        if (((float) this.dest.x) > this.locx + 40.0f) {
            this.locx += 20.0f;
            this.isMovingHorizontal = true;
        } else if (((float) this.dest.x) < this.locx - 40.0f) {
            this.locx -= 20.0f;
            this.isMovingHorizontal = true;
        } else if (this.isMovingHorizontal) {
            this.locx = (float) this.dest.x;
            this.isMovingHorizontal = false;
            Globals.triggerPostSwap = true;
        } else {
            this.locx = (float) this.dest.x;
            this.isMovingHorizontal = false;
        }
        if (!this.isMovingHorizontal && !this.isMovingHorizontal_d) {
            if (((float) this.dest.y) > this.locy + (this.dropSpeed * 20.0f)) {
                this.locy += this.dropSpeed * 10.0f;
                this.comboCounter = this.nextComboCounter;
                this.isMovingVertical = true;
            } else if (((float) this.dest.y) < this.locy - (this.dropSpeed * 20.0f)) {
                this.locy -= Math.abs(this.dropSpeed) * 10.0f;
                this.comboCounter = this.nextComboCounter;
                this.isMovingVertical = true;
            } else if (this.isMovingVertical) {
                this.locy = (float) this.dest.y;
                this.comboCounter = this.nextComboCounter;
                this.isMovingVertical = false;
                Globals.triggerPostFall = true;
                if (Globals.settings[1]) {
                    if (this.dropSpeed > 2.0f) {
                        Globals.vibrator.vibrate(LONGVIBRATE, -1);
                    } else {
                        Globals.vibrator.vibrate(SHORTVIBRATE, -1);
                    }
                }
            } else {
                this.locy = (float) this.dest.y;
                this.isMovingVertical = false;
            }
        }
        this.isMovingHorizontal_d = this.isMovingHorizontal;
        if (this.isMovingHorizontal || this.isMovingVertical) {
            z = true;
        } else {
            z = false;
        }
        this.isMoving = z;
    }

    public void draw(Canvas c, int scrollOffset, int leftOffset, int topOffset, boolean overlay, boolean isDanger) {
        if (!this.isDestroyed && !this.isRemoved) {
            tmpPaint.reset();
            drawMe(c, tmpPaint, scrollOffset, leftOffset, topOffset, overlay, isDanger);
        }
    }

    private void drawMe(Canvas c, Paint p, int scrollOffset, int leftOffset, int topOffset, boolean overlay, boolean isDanger) {
        i = this.blockType.ordinal();
        float drawx = (this.locx + ((float) leftOffset)) * Globals.SCALEX;
        float drawy = ((this.locy - ((float) scrollOffset)) + ((float) topOffset)) * Globals.SCALEY;
        if (this.blockType != Globals.BlockType.garbage && this.isSelected) {
            p.setAlpha(80);
            c.drawBitmap(ImageHandler.blackOverlay, (Globals.SCALEX * 6.0f) + drawx, (Globals.SCALEY * 6.0f) + drawy, p);
            p.reset();
            drawx -= Globals.SCALEX * 6.0f;
            drawy -= Globals.SCALEY * 6.0f;
        }
        if (isDanger && this.aniCounter % 4 < 2) {
            drawx -= 2.0f * Globals.SCALEX;
        }
        if (this.blockType == Globals.BlockType.gold) {
            if (!this.isExploding || Globals.counter % 4 != 0) {
                c.drawBitmap(ImageHandler.normalTile[i][this.aniLookDir.ordinal()], drawx, drawy, p);
            }
        } else if (this.blockType == Globals.BlockType.garbage) {
            if (this.aniIsShaking) {
                c.drawBitmap(ImageHandler.normalTile[i][this.aniCounter], drawx, drawy, p);
            } else {
                c.drawBitmap(ImageHandler.normalTile[i][0], drawx, drawy, p);
            }
        } else if (this.isExploding) {
            if (Globals.counter % 4 != 0) {
                c.drawBitmap(ImageHandler.bigPupilTile[i], drawx, drawy, p);
                if (this.aniCounter % 8 < 4) {
                    c.drawBitmap(ImageHandler.bigeyeShine, (22.0f * Globals.SCALEX) + drawx, (17.0f * Globals.SCALEY) + drawy, p);
                }
            }
        } else if (this.aniIsBlinking) {
            c.drawBitmap(ImageHandler.blinkTile[i], drawx, drawy, p);
        } else {
            c.drawBitmap(ImageHandler.normalTile[i][this.aniLookDir.ordinal()], drawx, drawy, p);
        }
        if (overlay) {
            p.setAlpha(80);
            c.drawBitmap(ImageHandler.blackOverlay, drawx, drawy, p);
        }
    }
}
