package com.aquos.blockpopperlite;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.MediaPlayer;

public class CustomButton {
    private static final int EASYRANGE = 10;
    private int easyRange = 0;
    private Bitmap imageDown;
    private Bitmap imageUp;
    private MediaPlayer sound = null;
    private int sx;
    private int sy;
    private boolean touchDown = false;
    private boolean touchPressed = false;
    private boolean touchUp = false;
    private boolean visible = true;
    private int x;
    private int y;

    public CustomButton(Bitmap iUp, Bitmap iDown, int x2, int y2, int sx2, int sy2, boolean easyClick, MediaPlayer sound2) {
        this.imageUp = iUp;
        this.imageDown = iDown;
        this.x = x2;
        this.y = y2;
        this.sx = sx2;
        this.sy = sy2;
        if (easyClick) {
            this.easyRange = 10;
        } else {
            this.easyRange = 0;
        }
        this.sound = sound2;
    }

    public boolean checkTouchUp() {
        if (!this.visible) {
            return false;
        }
        if (!this.touchUp) {
            return false;
        }
        this.touchUp = false;
        if (this.sound != null) {
            SoundHandler.playWave(this.sound);
        }
        return true;
    }

    public void checkPress(float touchX, float touchY, int eventAction) {
        if (this.visible) {
            if (eventAction == 0 && touchX > ((float) (this.x - this.easyRange)) && touchX < ((float) (this.x + this.sx + this.easyRange)) && touchY > ((float) (this.y - this.easyRange)) && touchY < ((float) (this.y + this.sy + this.easyRange))) {
                this.touchDown = true;
                this.touchPressed = true;
            }
            if (eventAction == 2) {
                if (touchX <= ((float) (this.x - this.easyRange)) || touchX >= ((float) (this.x + this.sx + this.easyRange)) || touchY <= ((float) (this.y - this.easyRange)) || touchY >= ((float) (this.y + this.sy + this.easyRange))) {
                    this.touchPressed = false;
                } else if (this.touchDown) {
                    this.touchPressed = true;
                }
            }
            if (eventAction == 1) {
                if (touchX > ((float) (this.x - this.easyRange)) && touchX < ((float) (this.x + this.sx + this.easyRange)) && touchY > ((float) (this.y - this.easyRange)) && touchY < ((float) (this.y + this.sy + this.easyRange)) && this.touchPressed) {
                    this.touchUp = true;
                }
                this.touchDown = false;
                this.touchPressed = false;
            }
        }
    }

    public void draw(Canvas canvas) {
        if (this.visible) {
            draw(canvas, null);
        }
    }

    public void draw(Canvas canvas, Paint paint) {
        if (!this.visible) {
            return;
        }
        if (!this.touchPressed) {
            canvas.drawBitmap(this.imageUp, ((float) this.x) * Globals.SCALEX, ((float) this.y) * Globals.SCALEY, paint);
        } else if (this.imageDown == null) {
            canvas.drawBitmap(this.imageUp, ((float) (this.x - 5)) * Globals.SCALEX, ((float) (this.y - 5)) * Globals.SCALEY, paint);
        } else {
            canvas.drawBitmap(this.imageDown, ((float) this.x) * Globals.SCALEX, ((float) this.y) * Globals.SCALEY, paint);
        }
    }

    public void setVisible(boolean b) {
        this.visible = b;
    }

    public boolean getVisibility() {
        return this.visible;
    }

    public void setPos(int x2, int y2) {
        this.x = x2;
        this.y = y2;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }
}
