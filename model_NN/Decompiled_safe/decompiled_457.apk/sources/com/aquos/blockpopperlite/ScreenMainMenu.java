package com.aquos.blockpopperlite;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;

public class ScreenMainMenu {
    private static final int BUTTONSPACEX = 50;
    private static final int BUTTONSPACEY = 90;
    private static final int BUTTONX = 50;
    private static final int BUTTONY = 320;
    private static CustomButton bEndlessMode;
    private static CustomButton bHighScore;
    private static CustomButton bPuzzleMode;
    private static CustomButton bStoryMode;
    private static CustomButton bUnlock;
    private static boolean canContinue = false;
    private static final Point[] eyePos = {new Point(-36, 502), new Point(38, 523), new Point(-29, 575), new Point(89, 576), new Point(39, 602), new Point(119, 622), new Point(47, 655), new Point(-29, 658), new Point(177, 677), new Point(100, 708), new Point(20, 713), new Point(-53, 734), new Point(193, 736), new Point(113, 747), new Point(220, 775), new Point(296, 783)};
    private static int i;
    private static int screenResult = 0;

    public static void initialize() {
        bStoryMode = new CustomButton(ImageHandler.b_adventure[0], ImageHandler.b_adventure[1], 50, 320, 239, 93, false, SoundHandler.buttonSound);
        bPuzzleMode = new CustomButton(ImageHandler.b_puzzle[0], ImageHandler.b_puzzle[1], 100, 415, 159, 83, false, SoundHandler.buttonSound);
        bEndlessMode = new CustomButton(ImageHandler.b_endless[0], ImageHandler.b_endless[1], 150, 502, 183, 86, false, SoundHandler.buttonSound);
        bHighScore = new CustomButton(ImageHandler.b_hs[0], ImageHandler.b_hs[1], 200, 590, 266, 96, false, SoundHandler.buttonSound);
        bUnlock = new CustomButton(ImageHandler.b_unlock[0], ImageHandler.b_unlock[1], 100, 680, 342, 105, false, SoundHandler.buttonSound);
        bUnlock.setVisible(true);
    }

    public static void update() {
        if (Globals.gameMode != 3 || Globals.getScore() <= 0) {
            canContinue = false;
        } else {
            canContinue = true;
        }
        if (bStoryMode.checkTouchUp()) {
            screenResult = 2;
        }
        if (bPuzzleMode.checkTouchUp()) {
            screenResult = 6;
        }
        if (bEndlessMode.checkTouchUp()) {
            if (canContinue) {
                screenResult = 11;
            } else {
                screenResult = 8;
            }
        }
        if (bHighScore.checkTouchUp()) {
            screenResult = 10;
        }
        if (bUnlock.checkTouchUp()) {
            screenResult = 13;
        }
        BackgroundHandler.update();
    }

    public static int getResult() {
        i = screenResult;
        screenResult = 0;
        return i;
    }

    public static void processTouchEvents(int eventAction, float touchX, float touchY) {
        bStoryMode.checkPress(touchX, touchY, eventAction);
        bPuzzleMode.checkPress(touchX, touchY, eventAction);
        bEndlessMode.checkPress(touchX, touchY, eventAction);
        bHighScore.checkPress(touchX, touchY, eventAction);
        bUnlock.checkPress(touchX, touchY, eventAction);
    }

    public static void draw(Canvas canvas) {
        canvas.drawBitmap(ImageHandler.background_mm, 0.0f, 0.0f, (Paint) null);
        bStoryMode.draw(canvas);
        bPuzzleMode.draw(canvas);
        bEndlessMode.draw(canvas);
        bHighScore.draw(canvas);
        bUnlock.draw(canvas);
        if (canContinue) {
            canvas.drawBitmap(ImageHandler.b_continue, ((float) (bEndlessMode.getX() + 96)) * Globals.SCALEX, ((float) (bEndlessMode.getY() - 21)) * Globals.SCALEY, (Paint) null);
        }
    }
}
