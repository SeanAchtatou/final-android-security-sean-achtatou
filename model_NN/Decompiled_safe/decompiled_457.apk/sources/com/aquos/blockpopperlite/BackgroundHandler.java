package com.aquos.blockpopperlite;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;

public class BackgroundHandler {
    public static final float BRIGHTCLOUDSPEED = 2.0f;
    public static final float DARKCLOUDSPEED = 1.0f;
    public static final int DARKCLOUDY = 160;
    public static final int FILLY = 359;
    public static final float RAYCENTERX = ((float) (Globals.GAMEX / 2));
    public static final float RAYCENTERY = ((float) (Globals.GAMEX / 2));
    public static final float RAYSPEED = 0.2f;
    private static float brightCloudX = 0.0f;
    private static float darkCloudX = 0.0f;
    private static float rayRotation = 0.0f;
    private static Matrix tmpMatrix = new Matrix();
    private static Paint tmpPaint = new Paint();

    public static void update() {
        rayRotation += 0.2f;
        if (rayRotation >= 90.0f) {
            rayRotation -= 90.0f;
        }
        darkCloudX += 1.0f;
        if (darkCloudX >= ((float) Globals.GAMEX)) {
            darkCloudX -= (float) Globals.GAMEX;
        }
        brightCloudX += 2.0f;
        if (brightCloudX >= ((float) Globals.GAMEX)) {
            brightCloudX -= (float) Globals.GAMEX;
        }
    }

    public static void drawClouds(Canvas canvas, int y) {
        canvas.drawBitmap(ImageHandler.background_dark, darkCloudX * Globals.SCALEX, ((float) y) * Globals.SCALEY, (Paint) null);
        canvas.drawBitmap(ImageHandler.background_dark, (darkCloudX - ((float) Globals.GAMEX)) * Globals.SCALEX, ((float) y) * Globals.SCALEY, (Paint) null);
        canvas.drawBitmap(ImageHandler.background_bright, brightCloudX * Globals.SCALEX, ((float) (y + 70)) * Globals.SCALEY, (Paint) null);
        canvas.drawBitmap(ImageHandler.background_bright, (brightCloudX - ((float) Globals.GAMEX)) * Globals.SCALEX, ((float) (y + 70)) * Globals.SCALEY, (Paint) null);
    }

    public static void draw(Canvas canvas, int bottomLimit, boolean mustDraw) {
        if (Globals.settings[2] || mustDraw) {
            tmpMatrix.reset();
            tmpMatrix.postRotate(rayRotation, 0.0f, (float) ImageHandler.background_rays.getHeight());
            tmpMatrix.postTranslate(RAYCENTERX * Globals.SCALEX, (RAYCENTERY * Globals.SCALEY) - ((float) ImageHandler.background_rays.getHeight()));
            canvas.drawBitmap(ImageHandler.background_rays, tmpMatrix, null);
            tmpMatrix.reset();
            tmpMatrix.postRotate(rayRotation - 90.0f, 0.0f, (float) ImageHandler.background_rays.getHeight());
            tmpMatrix.postTranslate(RAYCENTERX * Globals.SCALEX, (RAYCENTERY * Globals.SCALEY) - ((float) ImageHandler.background_rays.getHeight()));
            canvas.drawBitmap(ImageHandler.background_rays, tmpMatrix, null);
            tmpMatrix.reset();
            tmpMatrix.postRotate(rayRotation - 180.0f, 0.0f, (float) ImageHandler.background_rays.getHeight());
            tmpMatrix.postTranslate(RAYCENTERX * Globals.SCALEX, (RAYCENTERY * Globals.SCALEY) - ((float) ImageHandler.background_rays.getHeight()));
            canvas.drawBitmap(ImageHandler.background_rays, tmpMatrix, null);
            drawClouds(canvas, DARKCLOUDY);
            tmpPaint.setARGB(255, 204, 221, 229);
            Canvas canvas2 = canvas;
            canvas2.drawRect(0.0f, Globals.SCALEY * 359.0f, Globals.SCALEX * ((float) Globals.GAMEX), Globals.SCALEY * ((float) bottomLimit), tmpPaint);
            return;
        }
        canvas.drawColor(Color.argb(255, 204, 221, 229));
    }
}
