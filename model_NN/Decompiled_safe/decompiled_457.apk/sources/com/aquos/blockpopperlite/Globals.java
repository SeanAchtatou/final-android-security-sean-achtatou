package com.aquos.blockpopperlite;

import android.graphics.Point;
import android.media.AudioManager;
import android.os.Vibrator;
import java.util.Random;

public class Globals {
    public static int BLOCKLEFTOFFSET = 0;
    public static int BLOCKTOPOFFSET = 40;
    public static final int CHAR_EXPLODER = 1;
    public static final int CHAR_NONE = 5;
    public static final int CHAR_SINGLEPOPPER = 2;
    public static final int CHAR_TIMEFREEZER = 3;
    public static final int COMBOTEXTLIFE = 60;
    public static int DANGERTRESHOLD = 1;
    public static final int EXPLODEPOWER = 400;
    public static final int FASTSCROLLMAXSPEED = 10;
    public static int FRAMESECOND = 32;
    public static final int FREEZEPOWER = 2;
    public static final int GAMEMODE_ENDLESS = 3;
    public static final int GAMEMODE_PUZZLE = 2;
    public static final int GAMEMODE_STORY = 1;
    public static int GAMEX = 480;
    public static int GAMEY = 800;
    public static final int GARBAGEBLOCK = 6;
    public static final int GARBAGESCORE = 50;
    public static final int GOLDBLOCK = 5;
    public static int INFOBARHEIGHT = 120;
    public static final boolean LITE = true;
    public static int MAXBLOCKHORIZONTAL = 6;
    public static int MAXBLOCKVERTICAL = 8;
    public static final int NUMNORMALBLOCK = 5;
    public static final int NUMTOEXPLODE = 3;
    public static final int POPPOWER = 100;
    public static final int POWERUPPERGOLDBLOCK = 200;
    public static float SCALEX = 1.0f;
    public static float SCALEY = 1.0f;
    public static int SCREENX = 480;
    public static int SCREENY = 800;
    public static final int SETTING_EFFECTS = 2;
    public static final int SETTING_SOUND = 0;
    public static final int SETTING_VIBRATION = 1;
    public static final int STOPTIMER = 10;
    public static final int STOPTIMERCAP = 120;
    public static final int TILEFALLSPEED = 10;
    public static final int TILEMOVESPEED = 20;
    public static final int TILESCORE = 10;
    public static int TILESX = 80;
    public static int TILESY = 80;
    public static AudioManager audio = null;
    public static BlockType[] blockTypes = BlockType.values();
    private static int characterPowerUp = 0;
    public static int characterType = 1;
    public static int counter = 0;
    public static int currentLevel = 0;
    public static boolean disableGarbageBlock = false;
    public static boolean disableGoldBlock = false;
    public static int gameMode = 0;
    public static int hScoreEndless = 0;
    public static int hScoreStory = 0;
    public static boolean isInitialized = false;
    public static LOOKDIR[] lookDirs = LOOKDIR.values();
    private static int myScore = 0;
    public static final int nBLOCKTYPE = 7;
    public static final int nLOOKDIR = 5;
    public static int num0;
    public static int num1;
    public static int num2 = 0;
    public static ParticleGenerator particleGenerator;
    public static String phoneID = null;
    public static Random rand = new Random();
    public static float scrollOffset = 0.0f;
    public static char seenPUtutorial = 0;
    public static boolean seenPUtutorialPuzzle = false;
    public static Point selectedTile = null;
    public static boolean[] settings = {true, true, true};
    public static boolean[] solvedPuzzle = new boolean[64];
    public static char[] solvedStory = new char[4];
    private static int targetScore;
    public static float totalScrollOffset = 0.0f;
    public static int totalShift = 0;
    public static boolean triggerPostExplode = false;
    public static boolean triggerPostFall = false;
    public static boolean triggerPostScroll = false;
    public static boolean triggerPostSwap = false;
    public static Vibrator vibrator;

    public enum BlockType {
        blue,
        green,
        red,
        indigo,
        yellow,
        gold,
        garbage
    }

    public enum LOOKDIR {
        LEFT,
        RIGHT,
        UP,
        DOWN,
        CENTER
    }

    public static void initialize() {
        totalShift = 0;
        counter = 0;
        scrollOffset = 0.0f;
        totalScrollOffset = 0.0f;
        selectedTile = null;
        particleGenerator = new ParticleGenerator();
        rand = new Random();
        blockTypes = BlockType.values();
        characterType = 1;
        gameMode = 0;
        isInitialized = true;
    }

    public static void destroy() {
        selectedTile = null;
        particleGenerator = null;
        rand = null;
        blockTypes = null;
        isInitialized = false;
    }

    public static BlockType getRandomBlockType() {
        int type = rand.nextInt(blockTypes.length);
        if (disableGoldBlock && type == 5) {
            type = rand.nextInt(5);
        }
        if (disableGarbageBlock && type == 6) {
            type = rand.nextInt(5);
        }
        return blockTypes[type];
    }

    public static BlockType getRandomBlockType(BlockType exclude1, BlockType exclude2, boolean includeSpecial) {
        int numExclude = 0;
        int picked = -1;
        if (exclude1 != null) {
            numExclude = 0 + 1;
        }
        if (exclude2 != null) {
            numExclude++;
        }
        int candidate = rand.nextInt((includeSpecial ? blockTypes.length : 5) - numExclude);
        for (int i = 0; i <= candidate; i++) {
            while (true) {
                picked++;
                if (blockTypes[picked] != exclude1 && blockTypes[picked] != exclude2) {
                    break;
                }
            }
        }
        return blockTypes[picked];
    }

    public static boolean checkUnlocked(int hero) {
        switch (hero) {
            case 1:
                return true;
            case 2:
                return solvedStory[1] >= 8;
            case 3:
                char c = solvedStory[2];
                return false;
            default:
                return false;
        }
    }

    public static void update() {
        counter++;
        if (counter >= 240) {
            counter = 0;
        }
        if (targetScore <= myScore) {
            myScore = targetScore;
        } else if (targetScore < myScore + 10) {
            myScore++;
        } else if (targetScore < myScore + 100) {
            myScore += 5;
        } else {
            myScore += 10;
        }
        if (gameMode == 3 && myScore >= hScoreEndless) {
            hScoreEndless = myScore;
        }
    }

    public static void setScore(int score) {
        myScore = score;
        targetScore = score;
    }

    public static int getScore() {
        return myScore;
    }

    public static void addScore(int score) {
        targetScore += score;
    }

    public static void addPowerUp(int magnitude) {
        characterPowerUp += magnitude;
    }

    public static int getCharacterPowerUp() {
        return characterPowerUp;
    }

    public static void reducePowerUp(int magnitude) {
        characterPowerUp -= magnitude;
        if (characterPowerUp < 0) {
            characterPowerUp = 0;
        }
    }

    public static void clearPowerUp() {
        characterPowerUp = 0;
    }
}
