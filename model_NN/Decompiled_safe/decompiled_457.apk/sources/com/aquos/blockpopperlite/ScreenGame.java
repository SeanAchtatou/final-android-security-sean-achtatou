package com.aquos.blockpopperlite;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.KeyEvent;
import com.aquos.blockpopperlite.Globals;
import java.lang.reflect.Array;
import java.util.LinkedList;
import java.util.List;

public class ScreenGame {
    public static int GS_LOSE = 5;
    public static int GS_NORMAL = 2;
    public static int GS_PRESTART = 1;
    public static int GS_RATE = 7;
    public static int GS_RESTART = 6;
    public static int GS_WIN = 4;
    private static int PRESTARTDURATION = 40;
    private static int chanceGarbage = 18;
    private static int chanceGold = 18;
    private static Globals.BlockType colorChangeFrom;
    private static boolean colorChangeMode = false;
    private static Globals.BlockType colorChangeTo;
    private static List<Text> comboText = new LinkedList();
    private static Context context;
    private static boolean counterFirstRun = true;
    private static Point cursor = new Point(Globals.MAXBLOCKHORIZONTAL / 2, Globals.MAXBLOCKVERTICAL - 1);
    private static int diffx;
    private static int diffy;
    private static boolean disableAutoScroll = false;
    private static boolean disableExplode = false;
    private static boolean explodePopMode = false;
    private static List<Tile> explodeSequence = new LinkedList();
    private static List<Explosion> explosions = new LinkedList();
    private static boolean fastScrollEnabled = true;
    private static int fastScrollY = 0;
    private static int fastScrollYend = 0;
    private static boolean fastScrolling = false;
    private static int gameState = GS_NORMAL;
    private static int i;
    private static boolean[] isDangerColumn = new boolean[Globals.MAXBLOCKHORIZONTAL];
    private static boolean isPaused = false;
    private static int j;
    private static int k;
    private static boolean keyControl = false;
    private static int l = 0;
    private static float m = 0.0f;
    private static Tile[] newRow = new Tile[Globals.MAXBLOCKHORIZONTAL];
    private static boolean newRowCreated = false;
    private static int puzzleLoseCountdown = 0;
    private static float rawx;
    private static float rawy;
    private static int screenResult;
    private static float scrollSpeed = 0.2f;
    public static boolean showLoseDialog = false;
    private static boolean singlePopMode = false;
    private static boolean somethingFalling = false;
    private static int stateCounter = 0;
    public static Tile[][] tiles = ((Tile[][]) Array.newInstance(Tile.class, Globals.MAXBLOCKHORIZONTAL, Globals.MAXBLOCKVERTICAL));
    private static Paint tmpPaint;
    private static float touchDownX;
    private static float touchDownY;
    private static int upArrowCounter = 0;

    public static void Initialize(Context con, Tile[][] t) {
        if (con != null) {
            context = con;
        }
        if (!Globals.isInitialized) {
            Globals.initialize();
        }
        Globals.scrollOffset = 0.0f;
        InfoBar.initialize();
        tiles = (Tile[][]) Array.newInstance(Tile.class, Globals.MAXBLOCKHORIZONTAL, Globals.MAXBLOCKVERTICAL);
        newRow = new Tile[Globals.MAXBLOCKHORIZONTAL];
        explodeSequence = new LinkedList();
        explosions = new LinkedList();
        Globals.disableGarbageBlock = false;
        Globals.disableGoldBlock = false;
        if (Globals.gameMode == 1) {
            if (Globals.currentLevel <= 6) {
                Globals.disableGoldBlock = true;
                Globals.disableGarbageBlock = true;
            }
            if (Globals.currentLevel <= 7) {
                Globals.disableGoldBlock = true;
            }
        }
        initializeLevelDifficulty();
        if (t == null) {
            Globals.setScore(0);
            if (Globals.gameMode == 2) {
                tiles = LevelData.initializePuzzleTiles(LevelData.puzzleLevels[Globals.currentLevel - 1]);
                InfoBar.setMovesLeft(LevelData.puzzleParameter[Globals.currentLevel - 1]);
            } else if (Globals.gameMode == 1 && LevelData.levelWinCondition[Globals.currentLevel - 1] == 6) {
                tiles = LevelData.initializePuzzleTiles(LevelData.tutorialLevels[Globals.currentLevel - 1]);
            } else if (Globals.gameMode == 3 || Globals.gameMode == 1) {
                tiles = BlockOperations.makeInitialBlocks();
            }
        } else {
            tiles = t;
        }
        for (int i2 = 0; i2 < Globals.MAXBLOCKHORIZONTAL; i2++) {
            newRow[i2] = new Tile(Globals.getRandomBlockType(), Globals.TILESX * i2, tiles[0].length * Globals.TILESY, Globals.totalShift);
        }
        for (int i3 = 0; i3 < isDangerColumn.length; i3++) {
            isDangerColumn[i3] = false;
        }
        newRowCreated = false;
        comboText = new LinkedList();
        cursor = new Point(Globals.MAXBLOCKHORIZONTAL / 2, Globals.MAXBLOCKVERTICAL - 1);
        somethingFalling = false;
        fastScrollY = 0;
        fastScrolling = false;
        fastScrollYend = 0;
        gameState = GS_PRESTART;
        stateCounter = 0;
        counterFirstRun = true;
        showLoseDialog = false;
        isPaused = false;
        disableExplode = false;
        disableAutoScroll = false;
        singlePopMode = false;
        explodePopMode = false;
        colorChangeMode = false;
        colorChangeFrom = null;
        colorChangeTo = null;
        puzzleLoseCountdown = 0;
        Globals.triggerPostExplode = false;
        Globals.triggerPostSwap = true;
        Globals.triggerPostFall = false;
        Globals.triggerPostScroll = false;
        Globals.clearPowerUp();
        CustomDialogBox.hide();
    }

    public static void initializeLevelDifficulty() {
        if (Globals.gameMode == 1) {
            scrollSpeed = LevelData.levelScrollSpeed[Globals.currentLevel - 1];
            chanceGold = LevelData.levelChanceGold[Globals.currentLevel - 1];
            chanceGarbage = LevelData.levelChanceGarbage[Globals.currentLevel - 1];
        } else if (Globals.gameMode == 3) {
            setEndlessDifficulty();
        } else {
            scrollSpeed = 0.2f;
            chanceGold = 15;
            chanceGarbage = 15;
        }
    }

    public static void setEndlessDifficulty() {
        Globals.currentLevel = (Globals.getScore() / 500) + 1;
        scrollSpeed = 0.2f + (((float) Globals.currentLevel) * 0.07f);
        chanceGold = 18;
        chanceGarbage = 25 - Globals.currentLevel;
        if (chanceGarbage < 8) {
            chanceGarbage = 8;
        }
    }

    public static void Destroy() {
        tiles = null;
        newRow = null;
        comboText = null;
        cursor = null;
        explosions = null;
        explodeSequence = null;
        InfoBar.destroy();
        System.gc();
    }

    public static int getResult() {
        i = screenResult;
        screenResult = 0;
        return i;
    }

    public static void Pause() {
        isPaused = true;
    }

    public static void Resume() {
        isPaused = false;
    }

    public static void loseGame() {
        if (Globals.gameMode == 3) {
            Globals.setScore(0);
            gameState = GS_RATE;
            stateCounter = 0;
            BlockOperations.clearAllSelected(tiles);
            return;
        }
        stateCounter = 0;
        gameState = GS_LOSE;
        BlockOperations.clearAllSelected(tiles);
    }

    public static void winGame() {
        stateCounter = 0;
        gameState = GS_WIN;
        BlockOperations.clearAllSelected(tiles);
        if (Globals.gameMode == 1) {
            if (Globals.solvedStory[Globals.characterType] < Globals.currentLevel) {
                Globals.solvedStory[Globals.characterType] = (char) Globals.currentLevel;
            }
            if (Globals.currentLevel <= 4) {
                if (Globals.solvedStory[1] < Globals.currentLevel) {
                    Globals.solvedStory[1] = (char) Globals.currentLevel;
                }
                if (Globals.solvedStory[2] < Globals.currentLevel) {
                    Globals.solvedStory[2] = (char) Globals.currentLevel;
                }
                if (Globals.solvedStory[3] < Globals.currentLevel) {
                    Globals.solvedStory[3] = (char) Globals.currentLevel;
                }
            }
        } else if (Globals.gameMode == 2) {
            Globals.solvedPuzzle[Globals.currentLevel] = true;
        }
    }

    public static void detectWinLoseCondition() {
        switch (Globals.gameMode) {
            case 1:
                switch (LevelData.levelWinCondition[Globals.currentLevel - 1]) {
                    case 1:
                        if (InfoBar.level_PERFORM_MATCHES >= LevelData.levelWinParameter[Globals.currentLevel - 1]) {
                            winGame();
                            return;
                        }
                        return;
                    case 2:
                        if (Globals.getScore() >= LevelData.levelWinParameter[Globals.currentLevel - 1]) {
                            winGame();
                            return;
                        }
                        return;
                    case 3:
                        if (InfoBar.level_BREAK_GARBAGE >= LevelData.levelWinParameter[Globals.currentLevel - 1]) {
                            winGame();
                            return;
                        }
                        return;
                    case 4:
                        if (InfoBar.level_PERFORM_COMBO >= LevelData.levelWinParameter[Globals.currentLevel - 1]) {
                            winGame();
                            return;
                        }
                        return;
                    case 5:
                        if (InfoBar.level_PERFORM_ABILITY >= LevelData.levelWinParameter[Globals.currentLevel - 1]) {
                            winGame();
                            return;
                        }
                        return;
                    case 6:
                        if (BlockOperations.isCleared(tiles)) {
                            winGame();
                            return;
                        }
                        return;
                    default:
                        return;
                }
            case 2:
                if (BlockOperations.isCleared(tiles)) {
                    winGame();
                    return;
                } else if (InfoBar.getMovesLeft() <= 0 && gameState != GS_WIN && Globals.getCharacterPowerUp() <= 0) {
                    if (BlockOperations.isAllIdle(tiles)) {
                        puzzleLoseCountdown++;
                        if (puzzleLoseCountdown >= 40) {
                            puzzleLoseCountdown = 0;
                            loseGame();
                            return;
                        }
                        return;
                    }
                    puzzleLoseCountdown = 0;
                    return;
                } else {
                    return;
                }
            case 3:
            default:
                return;
        }
    }

    public static void storyFinish() {
        saveState();
        screenResult = 12;
    }

    public static void backToCharSelect() {
        saveState();
        loadState();
        screenResult = 8;
    }

    public static void backToLevelSelect() {
        saveState();
        loadState();
        screenResult = 6;
    }

    public static void quitGame() {
        saveState();
        screenResult = 1;
    }

    public static void draw(Canvas canvas) {
        try {
            if (gameState == GS_PRESTART || gameState == GS_NORMAL || gameState == GS_LOSE || gameState == GS_WIN || gameState == GS_RATE) {
                BackgroundHandler.draw(canvas, Globals.GAMEY - Globals.INFOBARHEIGHT, false);
                if (screenResult == 0) {
                    k = 0;
                    while (k < tiles.length) {
                        l = 0;
                        while (l < tiles[0].length) {
                            if (Globals.selectedTile == null || Globals.selectedTile.x != k || Globals.selectedTile.y != l) {
                                tiles[k][l].draw(canvas, (int) Globals.scrollOffset, Globals.BLOCKLEFTOFFSET, Globals.BLOCKTOPOFFSET, false, isDangerColumn[k] || Globals.getCharacterPowerUp() > 0);
                            }
                            l++;
                        }
                        k++;
                    }
                    if (Globals.selectedTile != null) {
                        tiles[Globals.selectedTile.x][Globals.selectedTile.y].draw(canvas, (int) Globals.scrollOffset, Globals.BLOCKLEFTOFFSET, Globals.BLOCKTOPOFFSET, false, isDangerColumn[Globals.selectedTile.x]);
                    }
                    k = 0;
                    while (k < newRow.length) {
                        newRow[k].draw(canvas, (int) Globals.scrollOffset, Globals.BLOCKLEFTOFFSET, Globals.BLOCKTOPOFFSET, true, false);
                        k++;
                    }
                }
                Globals.particleGenerator.draw(canvas);
                k = 0;
                while (k < comboText.size()) {
                    comboText.get(k).draw(canvas);
                    k++;
                }
                k = 0;
                while (k < explosions.size()) {
                    explosions.get(k).draw(canvas);
                    k++;
                }
                if (keyControl) {
                    if (singlePopMode || explodePopMode) {
                        canvas.drawBitmap(ImageHandler.cursor1, ((float) ((cursor.x * Globals.TILESX) + Globals.BLOCKLEFTOFFSET)) * Globals.SCALEX, (((float) ((cursor.y * Globals.TILESY) + Globals.BLOCKTOPOFFSET)) - Globals.scrollOffset) * Globals.SCALEY, (Paint) null);
                    } else {
                        canvas.drawBitmap(ImageHandler.cursor, ((float) ((cursor.x * Globals.TILESX) + Globals.BLOCKLEFTOFFSET)) * Globals.SCALEX, (((float) ((cursor.y * Globals.TILESY) + Globals.BLOCKTOPOFFSET)) - Globals.scrollOffset) * Globals.SCALEY, (Paint) null);
                    }
                }
                CustomDialogBox.draw(canvas);
                InfoBar.draw(canvas);
                MessageHandler.draw(canvas);
                if (Globals.gameMode != 2) {
                    drawUpArrow(canvas);
                }
                if (gameState == GS_PRESTART) {
                    tmpPaint = new Paint(ImageHandler.normalPaint);
                    tmpPaint.setARGB(255 - ((stateCounter * 255) / PRESTARTDURATION), 0, 0, 0);
                    canvas.drawRect(0.0f, 0.0f, (float) Globals.SCREENX, (float) Globals.SCREENY, tmpPaint);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateLevelEvents() {
        if (Globals.gameMode == 1) {
            if (stateCounter == 5 && counterFirstRun) {
                MessageHandler.load(Globals.currentLevel, false, true);
            }
        } else if (Globals.gameMode == 2) {
            if (stateCounter == 5 && counterFirstRun) {
                if (Globals.currentLevel == 1) {
                    MessageHandler.load(Globals.currentLevel, false, true);
                } else if (Globals.currentLevel == 21) {
                    MessageHandler.load(Globals.currentLevel, false, true);
                } else if (Globals.currentLevel == 25) {
                    MessageHandler.load(Globals.currentLevel, false, true);
                } else {
                    MessageHandler.load(Globals.currentLevel, false, false);
                }
            }
        } else if (Globals.gameMode == 3 && stateCounter == 5 && counterFirstRun) {
            MessageHandler.load(Globals.currentLevel, false, true);
        }
    }

    private static void updateUpArrow() {
        upArrowCounter += 5;
        if (upArrowCounter > 905) {
            upArrowCounter -= 150;
        }
        if (BlockOperations.findTopRow(tiles) <= 2) {
            upArrowCounter = 0;
        }
        if (fastScrolling) {
            upArrowCounter = 0;
        }
    }

    private static void drawUpArrow(Canvas canvas) {
        int alpha = upArrowCounter - 500;
        if (alpha >= 255) {
            alpha = 255;
        }
        if (alpha > 10) {
            ImageHandler.fontUpArrow.setAlpha(alpha);
            canvas.drawText("Drag up to see more tiles", 55.0f * Globals.SCALEX, 35.0f * Globals.SCALEY, ImageHandler.fontUpArrow);
            if (upArrowCounter % 150 < 75) {
                canvas.drawBitmap(ImageHandler.upArrow, Globals.SCALEX * 5.0f, Globals.SCALEY * 5.0f, ImageHandler.fontUpArrow);
            } else {
                canvas.drawBitmap(ImageHandler.upArrow, Globals.SCALEX * 5.0f, 10.0f * Globals.SCALEY, ImageHandler.fontUpArrow);
            }
        }
    }

    public static void update() {
        stateCounter++;
        if (stateCounter >= 1000) {
            stateCounter = 0;
            counterFirstRun = false;
        }
        if (gameState == GS_PRESTART) {
            Globals.update();
            BackgroundHandler.update();
            InfoBar.update();
            Globals.particleGenerator.update();
            if (stateCounter >= PRESTARTDURATION) {
                gameState = GS_NORMAL;
                stateCounter = 0;
            }
        } else if (gameState == GS_NORMAL) {
            if (!isPaused) {
                Globals.update();
                BackgroundHandler.update();
                updatePowerUp();
                if (!(Globals.gameMode == 2 || (Globals.gameMode == 1 && LevelData.levelWinCondition[Globals.currentLevel - 1] == 6))) {
                    updateUpArrow();
                    updateScroll();
                }
                if (Globals.gameMode == 3) {
                    setEndlessDifficulty();
                }
                updateTiles();
                updateComboText();
                updateExplosions();
                checkCursor();
                detectWinLoseCondition();
                InfoBar.update();
                Globals.particleGenerator.update();
            }
            MessageHandler.update();
            updateLevelEvents();
        } else if (gameState == GS_LOSE) {
            if (stateCounter == 40) {
                CustomDialogBox.show(CustomDialogBox.DIALOG_LOSE);
            }
            if (stateCounter / 2 < Globals.MAXBLOCKVERTICAL && stateCounter % 2 == 1) {
                BlockOperations.makeLoseTile(tiles, stateCounter / 2, explodeSequence);
            }
            if (stateCounter / 2 > Globals.MAXBLOCKVERTICAL && stateCounter % 5 == 1) {
                BlockOperations.randomExplode(explodeSequence);
            }
            Globals.update();
            BackgroundHandler.update();
            updateTiles();
            updateComboText();
            updateExplosions();
            InfoBar.update();
            Globals.particleGenerator.update();
        } else if (gameState == GS_WIN) {
            if (stateCounter == 40) {
                CustomDialogBox.show(CustomDialogBox.DIALOG_WIN);
            }
            Globals.update();
            BackgroundHandler.update();
            updateTiles();
            updateComboText();
            updateExplosions();
            InfoBar.update();
            Globals.particleGenerator.update();
        } else if (gameState == GS_RESTART) {
            gameState = GS_PRESTART;
            Initialize(null, null);
        } else if (gameState == GS_RATE) {
            if (stateCounter == 10) {
                CustomDialogBox.show(CustomDialogBox.DIALOG_RATE);
            }
            Globals.update();
            BackgroundHandler.update();
        }
    }

    public static void synchronousRestart() {
        gameState = GS_RESTART;
        if (Globals.gameMode == 3) {
            backToCharSelect();
        }
    }

    private static void updatePowerUp() {
        if (Globals.getCharacterPowerUp() > 0) {
            if (Globals.characterType == 1) {
                explodePopMode = true;
            }
            if (Globals.characterType == 3) {
                InfoBar.setStopTimer(Globals.getCharacterPowerUp() / 2);
                disableExplode = true;
                disableAutoScroll = true;
                Globals.reducePowerUp(2);
                if (Globals.getCharacterPowerUp() <= 0) {
                    Globals.triggerPostFall = true;
                    disableExplode = false;
                    disableAutoScroll = false;
                }
            }
            if (Globals.characterType == 2) {
                singlePopMode = true;
                return;
            }
            return;
        }
        disableExplode = false;
        disableAutoScroll = false;
        singlePopMode = false;
        explodePopMode = false;
        colorChangeMode = false;
    }

    private static void updateExplosions() {
        i = explosions.size() - 1;
        while (i >= 0) {
            explosions.get(i).update();
            if (explosions.get(i).lifeTime > 40) {
                explosions.remove(i);
            }
            i--;
        }
    }

    private static void updateComboText() {
        i = comboText.size() - 1;
        while (i >= 0) {
            comboText.get(i).update();
            if (comboText.get(i).lifeTime > 60) {
                Globals.addScore(comboText.get(i).score);
                comboText.remove(i);
            }
            i--;
        }
    }

    private static void updateDangerColumns() {
        i = 0;
        while (i < tiles.length) {
            if (tiles[i][Globals.DANGERTRESHOLD].isRemoved) {
                isDangerColumn[i] = false;
            } else {
                isDangerColumn[i] = true;
            }
            i++;
        }
    }

    private static void updateScroll() {
        boolean z;
        boolean z2;
        int topRow = BlockOperations.findTopRow(tiles);
        if (topRow < 1) {
            fastScrollEnabled = false;
        } else if (!colorChangeMode) {
            fastScrollEnabled = true;
        }
        updateDangerColumns();
        if (InfoBar.isStopped()) {
            m = 0.0f;
        } else if (disableAutoScroll) {
            m = 0.0f;
        } else {
            m = scrollSpeed;
        }
        if (fastScrollYend > 20) {
            fastScrolling = true;
        }
        if (fastScrolling) {
            z = false;
        } else {
            z = true;
        }
        if (fastScrollEnabled) {
            z2 = false;
        } else {
            z2 = true;
        }
        if (z || z2) {
            fastScrollY = 0;
            fastScrollYend = 0;
        }
        if (fastScrolling && fastScrollY < fastScrollYend) {
            fastScrollY += 10;
            Globals.scrollOffset += 10.0f;
            Globals.totalScrollOffset += 10.0f;
        }
        Globals.scrollOffset += m;
        Globals.totalScrollOffset += m;
        if (Globals.scrollOffset >= ((float) Globals.TILESY)) {
            for (Explosion e : explosions) {
                e.shiftUp(Globals.TILESY);
            }
            Globals.triggerPostScroll = true;
            Globals.totalShift++;
            if (topRow == 0) {
                loseGame();
                return;
            }
            j = 0;
            while (j < tiles[0].length - 1) {
                i = 0;
                while (i < tiles.length) {
                    tiles[i][j] = tiles[i][j + 1];
                    tiles[i][j].shiftTileUp();
                    i++;
                }
                j++;
            }
            i = 0;
            while (i < tiles.length) {
                tiles[i][tiles[0].length - 1] = newRow[i];
                tiles[i][tiles[0].length - 1].shiftTileUp();
                i++;
            }
            cursor.y--;
            checkCursor();
            if (!(Globals.selectedTile == null || Globals.selectedTile.y == 0)) {
                Globals.selectedTile = new Point(Globals.selectedTile.x, Globals.selectedTile.y - 1);
            }
            Globals.scrollOffset -= (float) Globals.TILESY;
            newRowCreated = false;
        }
        if (Globals.scrollOffset > 1.0f && !newRowCreated) {
            newRowCreated = true;
            newRow = BlockOperations.createNewRow(tiles, chanceGold, chanceGarbage);
        }
    }

    private static void checkCursor() {
        if (cursor.x < 0) {
            cursor.x = 0;
        }
        if (singlePopMode || explodePopMode) {
            if (cursor.x > Globals.MAXBLOCKHORIZONTAL - 1) {
                cursor.x = Globals.MAXBLOCKHORIZONTAL - 1;
            }
        } else if (cursor.x > Globals.MAXBLOCKHORIZONTAL - 2) {
            cursor.x = Globals.MAXBLOCKHORIZONTAL - 2;
        }
        if (cursor.y < 0) {
            cursor.y = 0;
        }
        if (cursor.y > Globals.MAXBLOCKVERTICAL - 1) {
            cursor.y = Globals.MAXBLOCKVERTICAL - 1;
        }
    }

    private static void updateTiles() {
        somethingFalling = false;
        i = 0;
        while (i < tiles.length) {
            j = 0;
            while (j < tiles[0].length) {
                if (!tiles[i][j].isRemoved) {
                    tiles[i][j].update(explosions);
                    if (tiles[i][j].isMovingVertical) {
                        somethingFalling = true;
                    }
                }
                j++;
            }
            i++;
        }
        if (Globals.triggerPostSwap) {
            Globals.triggerPostSwap = false;
            if (!BlockOperations.checkForGravity(tiles, true) && !disableExplode && BlockOperations.checkForExplosion(tiles, comboText)) {
                upArrowCounter = 0;
            }
        }
        if (Globals.triggerPostExplode) {
            Globals.triggerPostExplode = false;
            BlockOperations.checkForGravity(tiles, false);
        }
        if (Globals.triggerPostFall && !somethingFalling) {
            Globals.triggerPostFall = false;
            if (!disableExplode && BlockOperations.checkForExplosion(tiles, comboText)) {
                upArrowCounter = 0;
            }
        }
        if (Globals.triggerPostScroll) {
            Globals.triggerPostScroll = false;
            if (!disableExplode && BlockOperations.checkForExplosion(tiles, comboText)) {
                upArrowCounter = 0;
            }
        }
    }

    public static boolean processKeyEvent(int keyCode, KeyEvent msg) {
        if (keyCode == 4) {
            if (Globals.gameMode == 2) {
                backToLevelSelect();
            } else {
                quitGame();
            }
        }
        if (keyCode == 21) {
            keyControl = true;
            cursor.x--;
        }
        if (keyCode == 22) {
            keyControl = true;
            cursor.x++;
        }
        if (keyCode == 19) {
            keyControl = true;
            cursor.y--;
        }
        if (keyCode == 20) {
            keyControl = true;
            cursor.y++;
        }
        checkCursor();
        if (keyCode == 23) {
            keyControl = true;
            if (singlePopMode) {
                BlockOperations.specialPopTile(cursor, tiles, comboText);
            } else if (explodePopMode) {
                BlockOperations.specialBombTile(cursor, tiles, comboText);
            } else if (Globals.gameMode != 2 || InfoBar.getMovesLeft() > 0) {
                BlockOperations.swapTileHorizontal(cursor, new Point(cursor.x + 1, cursor.y), tiles);
            }
        }
        return true;
    }

    public static void processTouchEvents(int eventAction, float touchX, float touchY) {
        keyControl = false;
        rawx = touchX - ((float) Globals.BLOCKLEFTOFFSET);
        rawy = touchY - ((float) Globals.BLOCKTOPOFFSET);
        diffx = (int) (rawx - touchDownX);
        processDialogBoxClick(eventAction, touchX, touchY);
        MessageHandler.processTouchEvents(eventAction, touchX, touchY);
        if (MessageHandler.isClosed()) {
            switch (eventAction) {
                case 0:
                    touchDownX = rawx;
                    touchDownY = rawy;
                    Point t = BlockOperations.findClickedTile((int) rawx, (int) rawy, Globals.scrollOffset);
                    if (t != null) {
                        if (Globals.selectedTile != null) {
                            BlockOperations.selectTile(t, tiles);
                        } else if (Globals.selectedTile == null) {
                            BlockOperations.selectTile(t, tiles);
                        }
                    }
                    if (!colorChangeMode) {
                        return;
                    }
                    if (colorChangeFrom == null) {
                        colorChangeFrom = tiles[Globals.selectedTile.x][Globals.selectedTile.y].blockType;
                        return;
                    }
                    colorChangeTo = tiles[Globals.selectedTile.x][Globals.selectedTile.y].blockType;
                    BlockOperations.executeColorChange(tiles, colorChangeFrom, colorChangeTo);
                    colorChangeMode = false;
                    colorChangeFrom = null;
                    Globals.triggerPostScroll = true;
                    Globals.clearPowerUp();
                    return;
                case 1:
                    if (!fastScrolling) {
                        if (singlePopMode && Globals.selectedTile != null) {
                            BlockOperations.specialPopTile(Globals.selectedTile, tiles, comboText);
                        }
                        if (explodePopMode && Globals.selectedTile != null) {
                            BlockOperations.specialBombTile(Globals.selectedTile, tiles, comboText);
                        }
                    }
                    fastScrolling = false;
                    fastScrollYend = 0;
                    BlockOperations.clearAllSelected(tiles);
                    return;
                case 2:
                    if (Globals.gameMode != 2 || InfoBar.getMovesLeft() > 0) {
                        if (fastScrollEnabled) {
                            diffy = (int) (rawy - touchDownY);
                            fastScrollYend = -diffy;
                        }
                        if (Globals.selectedTile != null && !colorChangeMode && !singlePopMode && !explodePopMode) {
                            if (diffx < (-Globals.TILESX) / 2) {
                                if (BlockOperations.executeSwap(diffx, Globals.selectedTile, tiles)) {
                                    Globals.selectedTile = BlockOperations.findLeftTile(Globals.selectedTile);
                                    touchDownX = (float) (tiles[Globals.selectedTile.x][Globals.selectedTile.y].dest.x + (Globals.TILESX / 2));
                                    return;
                                }
                                return;
                            } else if (diffx > Globals.TILESX / 2 && BlockOperations.executeSwap(diffx, Globals.selectedTile, tiles)) {
                                Globals.selectedTile = BlockOperations.findRightTile(Globals.selectedTile);
                                touchDownX = (float) (tiles[Globals.selectedTile.x][Globals.selectedTile.y].dest.x + (Globals.TILESX / 2));
                                return;
                            } else {
                                return;
                            }
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                default:
                    return;
            }
        }
    }

    public static void incrementLevel() {
        if (Globals.gameMode == 2 && Globals.currentLevel < LevelData.puzzleLevels.length) {
            Globals.currentLevel++;
        }
        if (Globals.gameMode == 1 && Globals.currentLevel < LevelData.levelWinCondition.length) {
            Globals.currentLevel++;
        }
    }

    public static void processDialogBoxClick(int eventAction, float touchX, float touchY) {
        if (CustomDialogBox.dialogType == CustomDialogBox.DIALOG_LOSE) {
            int x = CustomDialogBox.getClick(touchX, touchY, eventAction);
            if (x == CustomDialogBox.CHOOSE_YES) {
                synchronousRestart();
            }
            if (x == CustomDialogBox.CHOOSE_NO) {
                if (Globals.gameMode == 2) {
                    backToLevelSelect();
                } else {
                    quitGame();
                }
            }
        }
        if (CustomDialogBox.dialogType == CustomDialogBox.DIALOG_WIN) {
            int x2 = CustomDialogBox.getClick(touchX, touchY, eventAction);
            if (x2 == CustomDialogBox.CHOOSE_YES) {
                if (Globals.gameMode == 1 && Globals.currentLevel == LevelData.levelWinCondition.length) {
                    storyFinish();
                } else if (Globals.gameMode != 2 || Globals.currentLevel < 20) {
                    incrementLevel();
                    synchronousRestart();
                } else {
                    gameState = GS_RATE;
                    stateCounter = 0;
                }
            }
            if (x2 == CustomDialogBox.CHOOSE_NO) {
                quitGame();
            }
        }
        if (CustomDialogBox.dialogType == CustomDialogBox.DIALOG_RATE) {
            int x3 = CustomDialogBox.getClick(touchX, touchY, eventAction);
            if (x3 == CustomDialogBox.CHOOSE_YES) {
                MainSurfaceView.goToRatingPage();
            }
            if (x3 == CustomDialogBox.CHOOSE_NO) {
                quitGame();
            }
        }
    }

    public static void saveState() {
        if (gameState == GS_LOSE) {
            CustomDialogBox.hide();
            Globals.setScore(0);
            FileHandler.writeState(context, BlockOperations.makeInitialBlocks());
            return;
        }
        FileHandler.writeState(context, tiles);
    }

    public static void loadState() {
        Initialize(context, FileHandler.restoreState(context));
    }
}
