package com.aquos.blockpopperlite;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

public class Particle {
    private static final float STARVEL = 40.0f;
    public static final int TYPE_DUST = 1;
    public static final int TYPE_STAR = 2;
    private static Matrix tmpMatrix = new Matrix();
    private static Paint tmpPaint = new Paint();
    float accx;
    float accy;
    int counter;
    boolean isActive;
    float locx;
    float locy;
    int opacity;
    int phase;
    boolean phaseUp;
    float rotation;
    float scale;
    int type;
    float velx;
    float vely;

    public Particle(int locx2, int locy2, float velx2, float vely2, float scale2, int type2) {
        reset(locx2, locy2, velx2, vely2, scale2, type2);
    }

    public void reset(int locationx, int locationy, float velox, float veloy, float sca, int typ) {
        this.locx = (float) locationx;
        this.locy = (float) locationy;
        this.scale = sca;
        this.rotation = 0.0f;
        this.velx = velox;
        this.vely = veloy;
        this.accx = 0.0f;
        this.accy = 0.0f;
        this.opacity = 255;
        this.isActive = true;
        this.counter = 0;
        this.phaseUp = Globals.rand.nextBoolean();
        this.phase = Globals.rand.nextInt(this.opacity);
        this.type = typ;
    }

    public void update() {
        if (this.isActive) {
            if (this.type == 1) {
                this.locx += this.velx;
                this.locy += this.vely;
                this.velx += this.accx;
                this.vely += this.accy;
                if (this.counter >= 20) {
                    this.velx *= 0.85f;
                } else if (this.counter >= 5) {
                    this.velx *= 0.85f;
                    this.vely *= 0.85f;
                }
                this.accy = Globals.rand.nextFloat() / 30.0f;
                this.counter++;
                if (this.counter >= 60) {
                    this.isActive = false;
                }
            } else if (this.type == 2) {
                this.locx += this.velx;
                this.locy += this.vely;
                this.velx += this.accx;
                this.vely += this.accy;
                if (this.counter == 20) {
                    float dirx = ((float) (Globals.GAMEX / 2)) - this.locx;
                    float diry = ((float) Globals.GAMEY) - this.locy;
                    float len = (float) Math.sqrt((double) ((dirx * dirx) + (diry * diry)));
                    this.velx = (dirx / len) * STARVEL;
                    this.vely = (diry / len) * STARVEL;
                } else if (this.counter >= 20) {
                    this.velx *= 0.95f;
                    this.vely *= 0.95f;
                } else if (this.counter >= 5) {
                    this.velx *= 0.85f;
                    this.vely *= 0.85f;
                }
                this.accy = Globals.rand.nextFloat() / 30.0f;
                this.counter++;
                if (this.counter >= 60) {
                    this.isActive = false;
                }
            }
        }
    }

    public void draw(Canvas c) {
        if (this.isActive) {
            if (this.type == 1) {
                tmpPaint.reset();
                tmpPaint.setARGB((int) (255.0d - ((0.07d * ((double) this.counter)) * ((double) this.counter))), 255, 255, 255);
                tmpMatrix.reset();
                tmpMatrix.postScale(this.scale, this.scale);
                tmpMatrix.postTranslate((this.locx * Globals.SCALEX) - (((float) (ImageHandler.particleDustSize.x / 2)) * this.scale), (this.locy * Globals.SCALEY) - (((float) (ImageHandler.particleDustSize.y / 2)) * this.scale));
                c.drawBitmap(ImageHandler.particleDust, tmpMatrix, tmpPaint);
            } else if (this.type == 2 && Globals.rand.nextInt(10) > 1) {
                tmpPaint.reset();
                tmpMatrix.reset();
                tmpMatrix.postTranslate((this.locx * Globals.SCALEX) - ((float) (ImageHandler.goldStar.getWidth() / 2)), (this.locy * Globals.SCALEY) - ((float) (ImageHandler.particleDustSize.y / 2)));
                c.drawBitmap(ImageHandler.goldStar, tmpMatrix, tmpPaint);
            }
        }
    }
}
