package com.aquos.blockpopperlite;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

public class BlockPopper extends Activity {
    private static final int D_QUIT = 1;
    private static final int D_RESTART = 0;
    private static final int D_SETTING = 2;
    private static final int M_QUIT = 2;
    private static final int M_RESTART = 0;
    private static final int M_SAVEnQUIT = 3;
    private static final int M_SETTINGS = 1;
    /* access modifiers changed from: private */
    public static boolean dialogOpen = false;
    private static MainSurfaceView mainSurfaceView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.mainlayout);
        mainSurfaceView = (MainSurfaceView) findViewById(R.id.mainSurfaceView);
        manageScreenProperties();
        ImageHandler.loadEssential(this);
        SoundHandler.loadEssential(this);
        ImageHandler.Initialize(this);
        SoundHandler.Initialize(this);
        Globals.initialize();
        Globals.vibrator = (Vibrator) getSystemService("vibrator");
        Globals.audio = (AudioManager) getSystemService("audio");
    }

    public void manageScreenProperties() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Globals.SCREENY = dm.heightPixels;
        Globals.SCREENX = dm.widthPixels;
        if (Globals.SCREENX >= 480) {
            Globals.SCREENX = 480;
            Globals.SCALEX = 1.0f;
        } else {
            Globals.SCALEX = ((float) Globals.SCREENX) / 480.0f;
        }
        if (Globals.SCREENY >= 800) {
            Globals.SCREENY = 800;
            Globals.SCALEY = 1.0f;
            return;
        }
        Globals.SCALEY = ((float) Globals.SCREENY) / 800.0f;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return mainSurfaceView.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: private */
    public void writeSettings() {
        FileHandler.writeSettings(this);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        switch (id) {
            case 0:
                builder.setMessage("Are you sure you want to restart?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ScreenGame.synchronousRestart();
                        ScreenGame.Resume();
                        BlockPopper.dialogOpen = false;
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ScreenGame.Resume();
                        BlockPopper.dialogOpen = false;
                        dialog.cancel();
                    }
                });
                return builder.create();
            case 1:
                builder.setMessage("Are you sure you want to exit?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ScreenGame.quitGame();
                        ScreenGame.Resume();
                        BlockPopper.dialogOpen = false;
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ScreenGame.Resume();
                        BlockPopper.dialogOpen = false;
                        dialog.cancel();
                    }
                });
                return builder.create();
            case 2:
                builder.setTitle("Settings");
                builder.setMultiChoiceItems(new CharSequence[]{"Sound", "Vibration", "Effects"}, Globals.settings, new DialogInterface.OnMultiChoiceClickListener() {
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        Globals.settings[which] = isChecked;
                    }
                });
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        BlockPopper.this.writeSettings();
                        ScreenGame.Resume();
                        BlockPopper.dialogOpen = false;
                    }
                });
                return builder.create();
            default:
                return null;
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if (mainSurfaceView.getCurrentScreen() == 5 || mainSurfaceView.getCurrentScreen() == 7) {
            menu.add(0, 0, 0, "Restart").setIcon(17301580);
            menu.add(0, 1, 0, "Settings").setIcon(17301577);
            menu.add(0, 2, 0, "Quit").setIcon(17301560);
            ScreenGame.Pause();
        } else if (mainSurfaceView.getCurrentScreen() == 9) {
            menu.add(0, 0, 0, "Restart").setIcon(17301580);
            menu.add(0, 1, 0, "Settings").setIcon(17301577);
            menu.add(0, 2, 0, "Save & Quit").setIcon(17301560);
            ScreenGame.Pause();
        } else {
            menu.add(0, 1, 0, "Settings").setIcon(17301577);
        }
        return super.onCreateOptionsMenu(menu);
    }

    public void onOptionsMenuClosed(Menu menu) {
        if (!dialogOpen) {
            ScreenGame.Resume();
        }
    }

    public boolean onMenuOpened(int featureId, Menu menu) {
        return super.onMenuOpened(featureId, menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                dialogOpen = true;
                showDialog(0);
                return true;
            case 1:
                dialogOpen = true;
                showDialog(2);
                return true;
            case 2:
                dialogOpen = true;
                showDialog(1);
                return true;
            default:
                return false;
        }
    }

    public void onStart() {
        super.onStart();
    }

    public void onRestart() {
        super.onRestart();
    }

    public void onResume() {
        super.onResume();
        mainSurfaceView.Initialize();
    }

    public void onPause() {
        mainSurfaceView.onPause();
        mainSurfaceView.Destroy();
        SoundHandler.stopMusic();
        super.onPause();
    }
}
