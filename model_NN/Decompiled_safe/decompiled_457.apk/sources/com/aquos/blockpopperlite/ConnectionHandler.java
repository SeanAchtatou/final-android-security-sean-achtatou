package com.aquos.blockpopperlite;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ConnectionHandler {
    public static String[] globalHighScores = new String[100];
    public static String[] globalNames = new String[100];
    public static String[] globalRanks = new String[100];
    public static String myRank = "101";

    public static void initialize() {
        for (int i = 0; i < globalRanks.length; i++) {
            globalRanks[i] = new StringBuilder().append(i + 1).toString();
            globalHighScores[i] = "0";
            globalNames[i] = "connecting..";
        }
    }

    public static void getHighScore(int rank) {
        connect("http://www.aquosgames.com/BP/highscore.php?rank=" + rank + "&pid=" + Globals.phoneID);
    }

    public static void seeMyScore() {
        connect("http://www.aquosgames.com/BP/highscore.php?action=getrank&pid=" + Globals.phoneID);
        int myRankInt = Integer.parseInt(myRank);
        if (myRankInt > 100) {
            HighScoreSurfaceView.gotoRank(91);
        } else if (myRankInt > 0) {
            HighScoreSurfaceView.gotoRank(myRankInt);
        } else {
            HighScoreSurfaceView.gotoRank(1);
        }
    }

    public static void postHighScore(String name, int score) {
        connect("http://www.aquosgames.com/BP/highscore.php?action=postscore&name=" + forURL(name) + "&score=" + score + "&pid=" + Globals.phoneID);
        int myRankInt = Integer.parseInt(myRank);
        if (myRankInt > 100) {
            HighScoreSurfaceView.gotoRank(91);
        } else if (myRankInt > 0) {
            HighScoreSurfaceView.gotoRank(myRankInt);
        } else {
            HighScoreSurfaceView.gotoRank(1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static String forURL(String aURLFragment) {
        try {
            return URLEncoder.encode(aURLFragment.replace('\\', ' ').replace('\'', ' ').replace('\"', ' '), "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            } catch (IOException e2) {
                e2.printStackTrace();
                try {
                    is.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            } catch (Throwable th) {
                try {
                    is.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                throw th;
            }
        }
        is.close();
        return sb.toString();
    }

    public static void connect(String url) {
        try {
            HttpResponse response = new DefaultHttpClient().execute(new HttpGet(url));
            System.out.println(response.getStatusLine().toString());
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream instream = entity.getContent();
                String result = convertStreamToString(instream);
                System.out.println(result);
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.length() > 0) {
                    myRank = jsonArray.getJSONObject(0).getString("myrank");
                }
                for (int i = 1; i < jsonArray.length(); i++) {
                    JSONObject json = jsonArray.getJSONObject(i);
                    int rank = Integer.parseInt(json.getString("rank"));
                    globalRanks[rank - 1] = json.getString("rank");
                    globalNames[rank - 1] = json.getString("name");
                    globalHighScores[rank - 1] = json.getString("score");
                }
                instream.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (JSONException e3) {
            e3.printStackTrace();
        }
    }
}
