package com.aquos.blockpopperlite;

import android.content.Context;
import android.media.MediaPlayer;

public class SoundHandler {
    private static final float FULLVOL = 2.0f;
    public static MediaPlayer buttonSound;
    private static boolean fadeOut = false;
    public static MediaPlayer glassSound;
    public static MediaPlayer loseSound;
    public static MediaPlayer mainTheme;
    public static MediaPlayer matchSound;
    public static MediaPlayer nowPlaying;
    public static MediaPlayer popSound;
    public static MediaPlayer selectionTheme;
    public static MediaPlayer slideOutSound;
    public static MediaPlayer slideSound;
    public static MediaPlayer swapSound;
    private static float volume = 2.0f;
    public static MediaPlayer winSound;

    public static void loadEssential(Context context) {
        mainTheme = MediaPlayer.create(context, (int) R.raw.title2);
    }

    public static void Initialize(Context context) {
        if (mainTheme == null) {
            mainTheme = MediaPlayer.create(context, (int) R.raw.title2);
        }
        selectionTheme = MediaPlayer.create(context, (int) R.raw.title3);
        buttonSound = MediaPlayer.create(context, (int) R.raw.button);
        popSound = MediaPlayer.create(context, (int) R.raw.pop);
        swapSound = MediaPlayer.create(context, (int) R.raw.swap);
        slideSound = MediaPlayer.create(context, (int) R.raw.slide);
        slideOutSound = MediaPlayer.create(context, (int) R.raw.slideout);
        winSound = MediaPlayer.create(context, (int) R.raw.win);
        loseSound = MediaPlayer.create(context, (int) R.raw.lose);
        matchSound = MediaPlayer.create(context, (int) R.raw.match);
        glassSound = MediaPlayer.create(context, (int) R.raw.glassbreak);
    }

    public static void updateMusic(MediaPlayer mp) {
        if (mp == null) {
            if (nowPlaying != null && nowPlaying.isPlaying()) {
                stopMusic();
            }
        } else if (Globals.settings[0]) {
            if (fadeOut && ((double) volume) >= 0.2d) {
                volume = (float) (((double) volume) - 0.1d);
            }
            if (nowPlaying != null) {
                nowPlaying.setVolume(volume, volume);
            }
            if (!mp.isPlaying()) {
                playMusic(mp, true);
            }
        } else if (nowPlaying != null && nowPlaying.isPlaying()) {
            stopMusic();
        }
    }

    public static void playMusic(MediaPlayer mp, boolean loop) {
        stopMusic();
        nowPlaying = mp;
        nowPlaying.setVolume(volume, volume);
        nowPlaying.setLooping(loop);
        nowPlaying.start();
    }

    public static void playWave(MediaPlayer mp) {
        if (Globals.settings[0]) {
            mp.start();
        }
    }

    public static void stopMusic() {
        fadeOut = false;
        volume = 2.0f;
        if (nowPlaying != null && nowPlaying.isPlaying()) {
            try {
                nowPlaying.stop();
                nowPlaying.prepare();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void resetAllMusic() {
        try {
            mainTheme.stop();
            mainTheme.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
        volume = 2.0f;
    }

    public static void fadeOutMusic() {
        fadeOut = true;
    }
}
