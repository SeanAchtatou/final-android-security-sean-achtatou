package com.aquos.blockpopperlite;

import android.graphics.Canvas;
import android.graphics.Paint;

public class Eye {
    private static final int offsetx = 50;
    private static final int offsety = 10;
    float posx;
    float posy;
    float pupilx = 0.0f;
    float pupily = 0.0f;
    float targetx = 0.0f;
    float targety = 0.0f;

    public Eye(float x, float y) {
        this.posx = x;
        this.posy = y;
    }

    public void update() {
        if (Math.abs(this.targetx - this.pupilx) < 2.0f && Math.abs(this.targety - this.pupily) < 2.0f) {
            getNewTarget();
        }
        this.pupilx += (this.targetx - this.pupilx) / 50.0f;
        this.pupily += (this.targety - this.pupily) / 50.0f;
    }

    private void getNewTarget() {
        switch (Globals.rand.nextInt(4)) {
            case 0:
                this.targetx = (float) (-6);
                this.targety = 0.0f;
                return;
            case 1:
                this.targetx = (float) 6;
                this.targety = 0.0f;
                return;
            case 2:
                this.targetx = 0.0f;
                this.targety = (float) (-6);
                return;
            case 3:
                this.targetx = 0.0f;
                this.targety = (float) 6;
                return;
            default:
                this.targetx = 0.0f;
                this.targety = 0.0f;
                return;
        }
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(ImageHandler.main_eye, this.posx * Globals.SCALEX, this.posy * Globals.SCALEY, (Paint) null);
        canvas.drawBitmap(ImageHandler.main_pupil, (this.posx + 50.0f + this.pupilx) * Globals.SCALEX, (this.posy + 10.0f + this.pupily) * Globals.SCALEY, (Paint) null);
    }
}
