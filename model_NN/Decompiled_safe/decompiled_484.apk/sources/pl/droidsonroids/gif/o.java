package pl.droidsonroids.gif;

import com.qihoo360.daily.f.d;
import com.qihoo360.daily.widget.TrafficDialog;

class o implements TrafficDialog.OnDialogViewClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ n f1398a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f1399b = d.m(this.f1398a.f1396a);

    o(n nVar) {
        this.f1398a = nVar;
    }

    public void onChooseClick(boolean z) {
        this.f1399b = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.d(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.qihoo360.daily.f.d.d(android.content.Context, java.lang.String):int
      com.qihoo360.daily.f.d.d(android.content.Context, long):void
      com.qihoo360.daily.f.d.d(android.content.Context, boolean):void */
    public void onLeftButtonClick() {
        d.d(this.f1398a.f1396a, false);
    }

    public void onRightButtonClick() {
        d.d(this.f1398a.f1396a, this.f1399b);
        this.f1398a.f1397b.b();
        if (this.f1399b) {
            d.d(this.f1398a.f1396a, System.currentTimeMillis());
        }
    }
}
