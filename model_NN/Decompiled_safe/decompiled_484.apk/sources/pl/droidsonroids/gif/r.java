package pl.droidsonroids.gif;

import android.graphics.drawable.Drawable;
import com.qihoo360.daily.R;
import com.qihoo360.daily.c.l;

class r implements l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GifView f1402a;

    r(GifView gifView) {
        this.f1402a = gifView;
    }

    public int getViewID() {
        return this.f1402a.f1382b.getId();
    }

    public Object getViewTag() {
        return this.f1402a.f1382b.getTag();
    }

    public void imageLoaded(Drawable drawable, String str, boolean z) {
        if (this.f1402a.f1382b.getTag() != null && !"".equals((String) this.f1402a.f1382b.getTag()) && str.trim().equals((String) this.f1402a.f1382b.getTag())) {
            this.f1402a.f1382b.setImageDrawable(drawable);
            this.f1402a.f1382b.setVisibility(0);
            this.f1402a.c.setImageResource(R.drawable.iv_pause_gif);
        }
    }

    public void setViewTag(String str) {
        this.f1402a.f1382b.setTag(str);
    }
}
