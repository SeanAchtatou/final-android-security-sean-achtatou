package pl.droidsonroids.gif;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.Locale;

public class GifAnimationMetaData implements Parcelable, Serializable {
    public static final Parcelable.Creator<GifAnimationMetaData> CREATOR = new b();

    /* renamed from: a  reason: collision with root package name */
    private final int f1376a;

    /* renamed from: b  reason: collision with root package name */
    private final int f1377b;
    private final int c;
    private final int d;
    private final int e;

    private GifAnimationMetaData(Parcel parcel) {
        this.f1376a = parcel.readInt();
        this.f1377b = parcel.readInt();
        this.c = parcel.readInt();
        this.d = parcel.readInt();
        this.e = parcel.readInt();
    }

    /* synthetic */ GifAnimationMetaData(Parcel parcel, b bVar) {
        this(parcel);
    }

    public boolean a() {
        return this.e > 1 && this.f1377b > 0;
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        String format = String.format(Locale.US, "GIF: size: %dx%d, frames: %d, loops: %s, duration: %d", Integer.valueOf(this.d), Integer.valueOf(this.c), Integer.valueOf(this.e), this.f1376a == 0 ? "Infinity" : Integer.toString(this.f1376a), Integer.valueOf(this.f1377b));
        return a() ? "Animated " + format : format;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f1376a);
        parcel.writeInt(this.f1377b);
        parcel.writeInt(this.c);
        parcel.writeInt(this.d);
        parcel.writeInt(this.e);
    }
}
