package pl.droidsonroids.gif;

import java.lang.Thread;

abstract class k implements Runnable {
    final /* synthetic */ c c;

    private k(c cVar) {
        this.c = cVar;
    }

    /* synthetic */ k(c cVar, d dVar) {
        this(cVar);
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    public final void run() {
        try {
            if (!this.c.b()) {
                a();
            }
        } catch (Throwable th) {
            Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
            if (defaultUncaughtExceptionHandler != null) {
                defaultUncaughtExceptionHandler.uncaughtException(Thread.currentThread(), th);
            }
            throw th;
        }
    }
}
