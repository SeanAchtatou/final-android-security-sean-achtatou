package pl.droidsonroids.gif;

import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import com.qihoo360.daily.R;
import com.qihoo360.daily.c.l;
import com.qihoo360.daily.h.ay;

class q implements l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GifView f1401a;

    q(GifView gifView) {
        this.f1401a = gifView;
    }

    public int getViewID() {
        return this.f1401a.f1381a.getId();
    }

    public Object getViewTag() {
        return this.f1401a.f1381a.getTag();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: pl.droidsonroids.gif.GifView.a(pl.droidsonroids.gif.GifView, boolean):boolean
     arg types: [pl.droidsonroids.gif.GifView, int]
     candidates:
      pl.droidsonroids.gif.GifView.a(pl.droidsonroids.gif.GifView, pl.droidsonroids.gif.c):pl.droidsonroids.gif.c
      pl.droidsonroids.gif.GifView.a(pl.droidsonroids.gif.GifView, boolean):boolean */
    public void imageLoaded(Drawable drawable, String str, boolean z) {
        if (!TextUtils.isEmpty((String) this.f1401a.f1381a.getTag()) && str.equals((String) this.f1401a.f1381a.getTag()) && !this.f1401a.k) {
            c unused = this.f1401a.e = (c) drawable;
            if (this.f1401a.e != null) {
                if (this.f1401a.i != null) {
                    this.f1401a.i.a();
                }
                this.f1401a.f1381a.setImageDrawable(this.f1401a.e);
                this.f1401a.f1381a.setVisibility(0);
                this.f1401a.c.setVisibility(8);
                if (this.f1401a.f1382b.isShown()) {
                    this.f1401a.f1382b.setVisibility(8);
                }
            } else {
                this.f1401a.c.setVisibility(0);
                ay.a(this.f1401a.j).a((int) R.string.net_err);
            }
            if (this.f1401a.d.isShown()) {
                this.f1401a.d.setVisibility(8);
            }
        }
        boolean unused2 = this.f1401a.l = false;
    }

    public void setViewTag(String str) {
        this.f1401a.f1381a.setTag(str);
    }
}
