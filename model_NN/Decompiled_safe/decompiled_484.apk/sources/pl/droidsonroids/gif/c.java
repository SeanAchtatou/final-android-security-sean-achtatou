package pl.droidsonroids.gif;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.widget.MediaController;
import java.io.File;
import java.util.Locale;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class c extends Drawable implements Animatable, MediaController.MediaPlayerControl {

    /* renamed from: a  reason: collision with root package name */
    protected final Paint f1383a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final ScheduledThreadPoolExecutor f1384b;
    /* access modifiers changed from: private */
    public volatile boolean c;
    private final long d;
    private final Rect e;
    private final Rect f;
    /* access modifiers changed from: private */
    public final Bitmap g;
    /* access modifiers changed from: private */
    public final GifInfoHandle h;
    /* access modifiers changed from: private */
    public final ConcurrentLinkedQueue<a> i;
    private ColorStateList j;
    private PorterDuffColorFilter k;
    private PorterDuff.Mode l;
    /* access modifiers changed from: private */
    public final Runnable m;
    /* access modifiers changed from: private */
    public final Runnable n;
    /* access modifiers changed from: private */
    public final Runnable o;

    public c(ContentResolver contentResolver, Uri uri) {
        this(contentResolver.openAssetFileDescriptor(uri, "r"));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: pl.droidsonroids.gif.GifInfoHandle.a(android.content.res.AssetFileDescriptor, boolean):pl.droidsonroids.gif.GifInfoHandle
     arg types: [android.content.res.AssetFileDescriptor, int]
     candidates:
      pl.droidsonroids.gif.GifInfoHandle.a(int, android.graphics.Bitmap):void
      pl.droidsonroids.gif.GifInfoHandle.a(android.content.res.AssetFileDescriptor, boolean):pl.droidsonroids.gif.GifInfoHandle */
    public c(AssetFileDescriptor assetFileDescriptor) {
        this(GifInfoHandle.a(assetFileDescriptor, false), assetFileDescriptor.getLength(), null, null);
    }

    public c(Resources resources, int i2) {
        this(resources.openRawResourceFd(i2));
    }

    public c(File file) {
        this(GifInfoHandle.openFile(file.getPath(), false), file.length(), null, null);
    }

    @TargetApi(19)
    c(GifInfoHandle gifInfoHandle, long j2, c cVar, ScheduledThreadPoolExecutor scheduledThreadPoolExecutor) {
        this.c = true;
        this.e = new Rect();
        this.f1383a = new Paint(6);
        this.i = new ConcurrentLinkedQueue<>();
        this.m = new d(this);
        this.n = new e(this);
        this.o = new f(this);
        this.f1384b = scheduledThreadPoolExecutor == null ? m.a() : scheduledThreadPoolExecutor;
        this.h = gifInfoHandle;
        this.d = j2;
        Bitmap bitmap = null;
        if (Build.VERSION.SDK_INT >= 19 && cVar != null) {
            synchronized (cVar.h) {
                if (!cVar.h.h()) {
                    int height = cVar.g.getHeight();
                    int width = cVar.g.getWidth();
                    if (height >= this.h.f1380b && width >= this.h.f1379a) {
                        cVar.e();
                        bitmap = cVar.g;
                        bitmap.eraseColor(0);
                        bitmap.reconfigure(this.h.f1379a, this.h.f1380b, Bitmap.Config.ARGB_8888);
                    }
                }
            }
        }
        if (bitmap == null) {
            this.g = Bitmap.createBitmap(this.h.f1379a, this.h.f1380b, Bitmap.Config.ARGB_8888);
        } else {
            this.g = bitmap;
        }
        this.f = new Rect(0, 0, this.h.f1379a, this.h.f1380b);
        this.f1384b.execute(this.n);
    }

    private PorterDuffColorFilter a(ColorStateList colorStateList, PorterDuff.Mode mode) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return new PorterDuffColorFilter(colorStateList.getColorForState(getState(), 0), mode);
    }

    private void e() {
        this.c = false;
        unscheduleSelf(this.m);
        this.h.a();
    }

    public void a() {
        e();
        this.g.recycle();
    }

    public boolean b() {
        return this.h.h();
    }

    public void c() {
        this.f1384b.execute(new h(this));
    }

    public boolean canPause() {
        return true;
    }

    public boolean canSeekBackward() {
        return false;
    }

    public boolean canSeekForward() {
        return d() > 1;
    }

    public int d() {
        return this.h.c;
    }

    public void draw(Canvas canvas) {
        boolean z;
        if (this.k == null || this.f1383a.getColorFilter() != null) {
            z = false;
        } else {
            this.f1383a.setColorFilter(this.k);
            z = true;
        }
        if (this.f1383a.getShader() == null) {
            canvas.drawBitmap(this.g, this.f, this.e, this.f1383a);
        } else {
            canvas.drawRect(this.e, this.f1383a);
        }
        if (z) {
            this.f1383a.setColorFilter(null);
        }
    }

    public int getAlpha() {
        return this.f1383a.getAlpha();
    }

    public int getAudioSessionId() {
        return 0;
    }

    public int getBufferPercentage() {
        return 100;
    }

    public ColorFilter getColorFilter() {
        return this.f1383a.getColorFilter();
    }

    public int getCurrentPosition() {
        return this.h.g();
    }

    public int getDuration() {
        return this.h.f();
    }

    public int getIntrinsicHeight() {
        return this.h.f1380b;
    }

    public int getIntrinsicWidth() {
        return this.h.f1379a;
    }

    public int getMinimumHeight() {
        return this.h.f1380b;
    }

    public int getMinimumWidth() {
        return this.h.f1379a;
    }

    public int getOpacity() {
        return -2;
    }

    public boolean isPlaying() {
        return this.c;
    }

    public boolean isRunning() {
        return this.c;
    }

    public boolean isStateful() {
        return super.isStateful() || (this.j != null && this.j.isStateful());
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        this.e.set(getBounds());
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        if (this.j == null || this.l == null) {
            return false;
        }
        this.k = a(this.j, this.l);
        return true;
    }

    public void pause() {
        stop();
    }

    public void seekTo(int i2) {
        if (i2 < 0) {
            throw new IllegalArgumentException("Position is not positive");
        }
        this.f1384b.execute(new j(this, i2));
    }

    public void setAlpha(int i2) {
        this.f1383a.setAlpha(i2);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f1383a.setColorFilter(colorFilter);
    }

    public void setDither(boolean z) {
        this.f1383a.setDither(z);
        invalidateSelf();
    }

    public void setFilterBitmap(boolean z) {
        this.f1383a.setFilterBitmap(z);
        invalidateSelf();
    }

    public void setTintList(ColorStateList colorStateList) {
        this.j = colorStateList;
        this.k = a(colorStateList, this.l);
        invalidateSelf();
    }

    public void setTintMode(PorterDuff.Mode mode) {
        this.l = mode;
        this.k = a(this.j, mode);
        invalidateSelf();
    }

    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        if (z) {
            if (z2) {
                c();
            }
            if (visible) {
                start();
            }
        } else if (visible) {
            stop();
        }
        return visible;
    }

    public void start() {
        this.c = true;
        this.f1384b.execute(new g(this));
    }

    public void stop() {
        this.c = false;
        unscheduleSelf(this.m);
        this.f1384b.execute(new i(this));
    }

    public String toString() {
        return String.format(Locale.US, "GIF: size: %dx%d, frames: %d, error: %d", Integer.valueOf(this.h.f1379a), Integer.valueOf(this.h.f1380b), Integer.valueOf(this.h.c), Integer.valueOf(this.h.e()));
    }
}
