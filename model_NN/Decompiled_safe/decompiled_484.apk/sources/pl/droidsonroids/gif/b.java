package pl.droidsonroids.gif;

import android.os.Parcel;
import android.os.Parcelable;

final class b implements Parcelable.Creator<GifAnimationMetaData> {
    b() {
    }

    /* renamed from: a */
    public GifAnimationMetaData createFromParcel(Parcel parcel) {
        return new GifAnimationMetaData(parcel, null);
    }

    /* renamed from: a */
    public GifAnimationMetaData[] newArray(int i) {
        return new GifAnimationMetaData[i];
    }
}
