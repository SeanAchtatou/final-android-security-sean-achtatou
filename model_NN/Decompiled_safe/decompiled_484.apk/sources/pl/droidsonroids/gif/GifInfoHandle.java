package pl.droidsonroids.gif;

import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import java.io.FileDescriptor;

final class GifInfoHandle {

    /* renamed from: a  reason: collision with root package name */
    final int f1379a;

    /* renamed from: b  reason: collision with root package name */
    final int f1380b;
    final int c;
    private volatile long d;

    static {
        System.loadLibrary("gif");
    }

    GifInfoHandle(long j, int i, int i2, int i3) {
        this.d = j;
        this.f1379a = i;
        this.f1380b = i2;
        this.c = i3;
    }

    static GifInfoHandle a(AssetFileDescriptor assetFileDescriptor, boolean z) {
        try {
            return openFd(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), z);
        } finally {
            assetFileDescriptor.close();
        }
    }

    private static native void free(long j);

    private static native int getCurrentPosition(long j);

    private static native int getDuration(long j);

    private static native int getNativeErrorCode(long j);

    static native GifInfoHandle openFd(FileDescriptor fileDescriptor, long j, boolean z);

    static native GifInfoHandle openFile(String str, boolean z);

    private static native long renderFrame(Bitmap bitmap, long j);

    private static native void reset(long j);

    private static native void restoreRemainder(long j);

    private static native void saveRemainder(long j);

    private static native void seekToTime(long j, int i, Bitmap bitmap);

    /* access modifiers changed from: package-private */
    public synchronized long a(Bitmap bitmap) {
        return renderFrame(bitmap, this.d);
    }

    /* access modifiers changed from: package-private */
    public synchronized void a() {
        free(this.d);
        this.d = 0;
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(int i, Bitmap bitmap) {
        seekToTime(this.d, i, bitmap);
    }

    /* access modifiers changed from: package-private */
    public synchronized void b() {
        restoreRemainder(this.d);
    }

    /* access modifiers changed from: package-private */
    public synchronized void c() {
        reset(this.d);
    }

    /* access modifiers changed from: package-private */
    public synchronized void d() {
        saveRemainder(this.d);
    }

    /* access modifiers changed from: package-private */
    public synchronized int e() {
        return getNativeErrorCode(this.d);
    }

    /* access modifiers changed from: package-private */
    public synchronized int f() {
        return getDuration(this.d);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        try {
            a();
        } finally {
            super.finalize();
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized int g() {
        return getCurrentPosition(this.d);
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean h() {
        return this.d == 0;
    }
}
