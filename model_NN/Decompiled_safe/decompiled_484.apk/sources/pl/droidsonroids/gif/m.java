package pl.droidsonroids.gif;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;

final class m extends ScheduledThreadPoolExecutor {

    /* renamed from: a  reason: collision with root package name */
    private static volatile m f1395a = null;

    private m() {
        super(1, new ThreadPoolExecutor.DiscardPolicy());
    }

    public static m a() {
        if (f1395a == null) {
            synchronized (m.class) {
                if (f1395a == null) {
                    f1395a = new m();
                }
            }
        }
        return f1395a;
    }
}
