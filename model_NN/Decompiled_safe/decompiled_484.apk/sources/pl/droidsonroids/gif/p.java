package pl.droidsonroids.gif;

import android.view.View;
import com.qihoo360.daily.R;

class p implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GifView f1400a;

    p(GifView gifView) {
        this.f1400a = gifView;
    }

    public void onClick(View view) {
        if (this.f1400a.e == null) {
            return;
        }
        if (this.f1400a.e.isPlaying()) {
            this.f1400a.e.pause();
            this.f1400a.c.setImageResource(R.drawable.iv_pause_gif);
            this.f1400a.c.setVisibility(0);
            return;
        }
        if (this.f1400a.i != null) {
            this.f1400a.i.a();
        }
        this.f1400a.e.start();
        this.f1400a.c.setVisibility(8);
    }
}
