package pl.droidsonroids.gif;

import com.qihoo.messenger.util.QDefine;
import com.qihoo360.daily.model.Result;
import java.util.Locale;

public enum l {
    NO_ERROR(0, "No error"),
    OPEN_FAILED(101, "Failed to open given input"),
    READ_FAILED(102, "Failed to read from given input"),
    NOT_GIF_FILE(103, "Data is not in GIF format"),
    NO_SCRN_DSCR(104, "No screen descriptor detected"),
    NO_IMAG_DSCR(105, "No image descriptor detected"),
    NO_COLOR_MAP(106, "Neither global nor local color map found"),
    WRONG_RECORD(107, "Wrong record type detected"),
    DATA_TOO_BIG(Result.HTTP_DIGGED, "Number of pixels bigger than width * height"),
    NOT_ENOUGH_MEM(109, "Failed to allocate required memory"),
    CLOSE_FAILED(110, "Failed to close given input"),
    NOT_READABLE(111, "Given file was not opened for read"),
    IMAGE_DEFECT(112, "Image is defective, decoding aborted"),
    EOF_TOO_SOON(113, "Image EOF detected before image complete"),
    NO_FRAMES(QDefine.ONE_SECOND, "No frames found, at least one frame required"),
    INVALID_SCR_DIMS(1001, "Invalid screen size, dimensions must be positive"),
    INVALID_IMG_DIMS(1002, "Invalid image size, dimensions must be positive"),
    IMG_NOT_CONFINED(1003, "Image size exceeds screen size"),
    REWIND_FAILED(1004, "Input source rewind has failed, animation is stopped"),
    UNKNOWN(-1, "Unknown error");
    
    public final String u;
    private int v;

    private l(int i, String str) {
        this.v = i;
        this.u = str;
    }

    static l a(int i) {
        for (l lVar : values()) {
            if (lVar.v == i) {
                return lVar;
            }
        }
        l lVar2 = UNKNOWN;
        lVar2.v = i;
        return lVar2;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return String.format(Locale.US, "GifError %d: %s", Integer.valueOf(this.v), this.u);
    }
}
