package pl.droidsonroids.gif;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import com.qihoo.messenger.util.QDefine;
import com.qihoo360.daily.R;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.widget.TrafficDialog;

class n implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f1396a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ GifView f1397b;

    n(GifView gifView, Context context) {
        this.f1397b = gifView;
        this.f1396a = context;
    }

    public void onClick(View view) {
        boolean z = true;
        if (!this.f1397b.f1382b.isShown()) {
            return;
        }
        if (b.a(this.f1396a)) {
            if (d.m(this.f1396a)) {
                if (System.currentTimeMillis() - d.e(this.f1396a, 0) <= QDefine.ONE_DAY) {
                    z = false;
                }
            }
            if (!z || this.f1396a == null || !(this.f1396a instanceof Activity)) {
                this.f1397b.b();
                return;
            }
            TrafficDialog trafficDialog = new TrafficDialog((Activity) this.f1396a, d.m(this.f1396a));
            trafficDialog.setContentText((int) R.string.is_play_gif);
            trafficDialog.showDialog();
            trafficDialog.setOnDialogViewClickListener(new o(this));
            return;
        }
        this.f1397b.b();
    }
}
