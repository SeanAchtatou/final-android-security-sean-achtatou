package pl.droidsonroids.gif;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;
import java.io.IOException;

public class GifImageView extends ImageView {
    public GifImageView(Context context) {
        super(context);
    }

    public GifImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(attributeSet, getResources());
    }

    public GifImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(attributeSet, getResources());
    }

    @TargetApi(21)
    public GifImageView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        a(attributeSet, getResources());
    }

    /* access modifiers changed from: package-private */
    public void a(AttributeSet attributeSet, Resources resources) {
        if (attributeSet != null && resources != null && !isInEditMode()) {
            int attributeResourceValue = attributeSet.getAttributeResourceValue("http://schemas.android.com/apk/res/android", "src", -1);
            if (attributeResourceValue > 0 && "drawable".equals(resources.getResourceTypeName(attributeResourceValue))) {
                a(true, attributeResourceValue, resources);
            }
            int attributeResourceValue2 = attributeSet.getAttributeResourceValue("http://schemas.android.com/apk/res/android", "background", -1);
            if (attributeResourceValue2 > 0 && "drawable".equals(resources.getResourceTypeName(attributeResourceValue2))) {
                a(false, attributeResourceValue2, resources);
            }
        }
    }

    /* access modifiers changed from: package-private */
    @TargetApi(16)
    public void a(boolean z, int i, Resources resources) {
        try {
            c cVar = new c(resources, i);
            if (z) {
                setImageDrawable(cVar);
            } else if (Build.VERSION.SDK_INT >= 16) {
                setBackground(cVar);
            } else {
                setBackgroundDrawable(cVar);
            }
        } catch (Resources.NotFoundException | IOException e) {
            if (z) {
                super.setImageResource(i);
            } else {
                super.setBackgroundResource(i);
            }
        }
    }

    public void setBackgroundResource(int i) {
        a(false, i, getResources());
    }

    public void setImageResource(int i) {
        a(true, i, getResources());
    }

    public void setImageURI(Uri uri) {
        if (uri != null) {
            try {
                setImageDrawable(new c(getContext().getContentResolver(), uri));
                return;
            } catch (IOException e) {
            }
        }
        super.setImageURI(uri);
    }
}
