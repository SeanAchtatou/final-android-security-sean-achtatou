package pl.droidsonroids.gif;

import java.io.IOException;

public class GifIOException extends IOException {

    /* renamed from: a  reason: collision with root package name */
    public final l f1378a;

    GifIOException(int i) {
        this(l.a(i));
    }

    private GifIOException(l lVar) {
        super(lVar.a());
        this.f1378a = lVar;
    }
}
