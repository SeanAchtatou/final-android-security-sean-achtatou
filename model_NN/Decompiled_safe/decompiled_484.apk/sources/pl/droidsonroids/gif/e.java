package pl.droidsonroids.gif;

import java.util.concurrent.TimeUnit;

class e extends k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f1386a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    e(c cVar) {
        super(cVar, null);
        this.f1386a = cVar;
    }

    public void a() {
        long a2 = this.f1386a.h.a(this.f1386a.g);
        int i = (int) (a2 >> 1);
        if (((int) (a2 & 1)) == 1 && !this.f1386a.i.isEmpty()) {
            this.f1386a.scheduleSelf(this.f1386a.o, 0);
        }
        if (i >= 0) {
            if (this.f1386a.isVisible() && this.f1386a.c) {
                this.f1386a.f1384b.schedule(this, (long) i, TimeUnit.MILLISECONDS);
            }
            this.f1386a.unscheduleSelf(this.f1386a.m);
            this.f1386a.scheduleSelf(this.f1386a.m, 0);
        }
    }
}
