package pl.droidsonroids.gif;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.qihoo360.daily.R;
import com.qihoo360.daily.c.e;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.h.a;

public class GifView extends FrameLayout {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public GifImageView f1381a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public ImageView f1382b;
    /* access modifiers changed from: private */
    public ImageView c;
    /* access modifiers changed from: private */
    public ProgressBar d;
    /* access modifiers changed from: private */
    public c e;
    private String f;
    private int g;
    private int h;
    /* access modifiers changed from: private */
    public s i;
    /* access modifiers changed from: private */
    public Context j;
    /* access modifiers changed from: private */
    public boolean k = false;
    /* access modifiers changed from: private */
    public boolean l = false;

    public GifView(Context context) {
        super(context);
        a(context);
    }

    public GifView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public GifView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context);
    }

    private void a(Context context) {
        this.j = context;
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        layoutParams.gravity = 17;
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-2, -2);
        layoutParams2.gravity = 17;
        this.f1381a = new GifImageView(context);
        this.f1381a.setId(R.id.drawable_gif);
        addView(this.f1381a, layoutParams);
        this.f1382b = new ImageView(context);
        this.f1382b.setId(R.id.thumb_git);
        addView(this.f1382b, layoutParams);
        this.c = new ImageView(context);
        addView(this.c, layoutParams2);
        this.d = new ProgressBar(context);
        this.d.setIndeterminateDrawable(getResources().getDrawable(R.drawable.loading_video_progress));
        FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(-2, -2);
        layoutParams3.gravity = 17;
        layoutParams3.width = (int) a.a(getContext(), 30.0f);
        layoutParams3.height = layoutParams3.width;
        addView(this.d, layoutParams3);
        this.d.setVisibility(8);
        this.f1382b.setBackgroundColor(getContext().getResources().getColor(R.color.bg_gif));
        this.c.setImageResource(R.drawable.iv_thumb_gif);
        if (!d.f(context)) {
            this.f1382b.setOnClickListener(new n(this, context));
        }
        this.f1381a.setOnClickListener(new p(this));
    }

    /* access modifiers changed from: private */
    public void b() {
        if (!TextUtils.isEmpty(this.f)) {
            this.c.setVisibility(8);
            this.d.setVisibility(0);
            this.k = false;
            setGifDrawable(this.f);
        }
    }

    private void setGifDrawable(String str) {
        if (!this.l) {
            this.l = true;
            e.a().a(this.j, str, new q(this));
        }
    }

    public void a() {
        this.k = true;
        if (this.e != null) {
            this.e.stop();
            this.e.a();
        }
        this.c.setVisibility(0);
        this.c.setImageResource(R.drawable.iv_pause_gif);
        this.f1382b.setVisibility(0);
        this.f1382b.setBackgroundColor(getContext().getResources().getColor(R.color.bg_gif));
        this.f1381a.setVisibility(8);
        this.d.setVisibility(8);
    }

    public void a(String str, String str2, int i2, int i3, s sVar) {
        this.f1382b.setBackgroundColor(getContext().getResources().getColor(R.color.bg_gif));
        this.f1382b.setImageDrawable(null);
        this.f1382b.setVisibility(0);
        this.c.setVisibility(0);
        this.d.setVisibility(8);
        this.c.setImageResource(R.drawable.iv_thumb_gif);
        this.f1381a.setVisibility(8);
        if (!TextUtils.isEmpty(str)) {
            setThumb(str);
        }
        this.f = str2;
        this.g = i2;
        this.h = i3;
        this.i = sVar;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int size = View.MeasureSpec.getSize(i2);
        View.MeasureSpec.getSize(i3);
        int i4 = (int) (((((float) size) * 1.0f) * ((float) this.h)) / ((float) this.g));
        ViewGroup.LayoutParams layoutParams = this.f1381a.getLayoutParams();
        layoutParams.width = size;
        layoutParams.height = i4;
        this.f1381a.setLayoutParams(layoutParams);
        ViewGroup.LayoutParams layoutParams2 = this.f1382b.getLayoutParams();
        layoutParams2.width = size;
        layoutParams2.height = i4;
        this.f1382b.setLayoutParams(layoutParams2);
        measureChild(this.f1382b, View.MeasureSpec.makeMeasureSpec(size, 1073741824), View.MeasureSpec.makeMeasureSpec(i4, 1073741824));
        setMeasuredDimension(size, i4);
    }

    public void setThumb(String str) {
        if (!TextUtils.isEmpty(str)) {
            e.a().a(this.j, str, new r(this), 0);
        }
    }
}
