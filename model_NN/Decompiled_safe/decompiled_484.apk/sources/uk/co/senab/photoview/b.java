package uk.co.senab.photoview;

import android.graphics.RectF;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ImageView;

public class b implements GestureDetector.OnDoubleTapListener {

    /* renamed from: a  reason: collision with root package name */
    private d f1408a;

    public b(d dVar) {
        a(dVar);
    }

    public void a(d dVar) {
        this.f1408a = dVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: uk.co.senab.photoview.d.a(float, float, float, boolean):void
     arg types: [float, float, float, int]
     candidates:
      uk.co.senab.photoview.d.a(float, float, float, float):void
      uk.co.senab.photoview.a.f.a(float, float, float, float):void
      uk.co.senab.photoview.d.a(float, float, float, boolean):void */
    public boolean onDoubleTap(MotionEvent motionEvent) {
        if (this.f1408a == null) {
            return false;
        }
        try {
            float g = this.f1408a.g();
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            if (g < this.f1408a.e()) {
                this.f1408a.a(this.f1408a.e(), x, y, true);
                return true;
            } else if (g < this.f1408a.e() || g >= this.f1408a.f()) {
                this.f1408a.a(this.f1408a.d(), x, y, true);
                return true;
            } else {
                this.f1408a.a(this.f1408a.f(), x, y, true);
                return true;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return true;
        }
    }

    public boolean onDoubleTapEvent(MotionEvent motionEvent) {
        return false;
    }

    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        RectF b2;
        if (this.f1408a == null) {
            return false;
        }
        ImageView c = this.f1408a.c();
        if (!(this.f1408a.i() == null || (b2 = this.f1408a.b()) == null)) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            if (b2.contains(x, y)) {
                this.f1408a.i().a(c, (x - b2.left) / b2.width(), (y - b2.top) / b2.height());
                return true;
            }
        }
        if (this.f1408a.j() == null) {
            return false;
        }
        this.f1408a.j().onViewTap(c, motionEvent.getX(), motionEvent.getY());
        return false;
    }
}
