package uk.co.senab.photoview.a;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.MotionEvent;
import uk.co.senab.photoview.a;

@TargetApi(5)
public class b extends a {
    private int f = -1;
    private int g = 0;

    public b(Context context) {
        super(context);
    }

    /* access modifiers changed from: package-private */
    public float a(MotionEvent motionEvent) {
        try {
            return motionEvent.getX(this.g);
        } catch (Exception e) {
            return motionEvent.getX();
        }
    }

    /* access modifiers changed from: package-private */
    public float b(MotionEvent motionEvent) {
        try {
            return motionEvent.getY(this.g);
        } catch (Exception e) {
            return motionEvent.getY();
        }
    }

    public boolean c(MotionEvent motionEvent) {
        int i = 0;
        switch (motionEvent.getAction() & 255) {
            case 0:
                this.f = motionEvent.getPointerId(0);
                break;
            case 1:
            case 3:
                this.f = -1;
                break;
            case 6:
                int a2 = a.a(motionEvent.getAction());
                if (motionEvent.getPointerId(a2) == this.f) {
                    int i2 = a2 == 0 ? 1 : 0;
                    this.f = motionEvent.getPointerId(i2);
                    this.f1406b = motionEvent.getX(i2);
                    this.c = motionEvent.getY(i2);
                    break;
                }
                break;
        }
        if (this.f != -1) {
            i = this.f;
        }
        this.g = motionEvent.findPointerIndex(i);
        return super.c(motionEvent);
    }
}
