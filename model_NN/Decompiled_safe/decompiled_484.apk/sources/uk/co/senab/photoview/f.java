package uk.co.senab.photoview;

import android.widget.ImageView;

/* synthetic */ class f {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f1416a = new int[ImageView.ScaleType.values().length];

    static {
        try {
            f1416a[ImageView.ScaleType.MATRIX.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f1416a[ImageView.ScaleType.FIT_START.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f1416a[ImageView.ScaleType.FIT_END.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f1416a[ImageView.ScaleType.FIT_CENTER.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f1416a[ImageView.ScaleType.FIT_XY.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
    }
}
