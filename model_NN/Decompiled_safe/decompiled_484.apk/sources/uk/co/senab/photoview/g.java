package uk.co.senab.photoview;

import android.widget.ImageView;

class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f1417a;

    /* renamed from: b  reason: collision with root package name */
    private final float f1418b;
    private final float c;
    private final long d = System.currentTimeMillis();
    private final float e;
    private final float f;

    public g(d dVar, float f2, float f3, float f4, float f5) {
        this.f1417a = dVar;
        this.f1418b = f4;
        this.c = f5;
        this.e = f2;
        this.f = f3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private float a() {
        return d.f1413a.getInterpolation(Math.min(1.0f, (((float) (System.currentTimeMillis() - this.d)) * 1.0f) / ((float) this.f1417a.f1414b)));
    }

    public void run() {
        ImageView c2 = this.f1417a.c();
        if (c2 != null) {
            float a2 = a();
            this.f1417a.a((this.e + ((this.f - this.e) * a2)) / this.f1417a.g(), this.f1418b, this.c);
            if (a2 < 1.0f) {
                a.a(c2, this);
            }
        }
    }
}
