package uk.co.senab.photoview;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import java.lang.ref.WeakReference;
import uk.co.senab.photoview.a.e;
import uk.co.senab.photoview.a.f;
import uk.co.senab.photoview.a.g;
import uk.co.senab.photoview.b.a;

public class d implements View.OnTouchListener, ViewTreeObserver.OnGlobalLayoutListener, f, c {

    /* renamed from: a  reason: collision with root package name */
    static final Interpolator f1413a = new AccelerateDecelerateInterpolator();
    /* access modifiers changed from: private */
    public static final boolean c = Log.isLoggable("PhotoViewAttacher", 3);
    private int A;
    private boolean B;
    private ImageView.ScaleType C;

    /* renamed from: b  reason: collision with root package name */
    int f1414b;
    private float d;
    private float e;
    private float f;
    private boolean g;
    private boolean h;
    private WeakReference<ImageView> i;
    private GestureDetector j;
    private e k;
    private final Matrix l;
    private final Matrix m;
    /* access modifiers changed from: private */
    public final Matrix n;
    private final RectF o;
    private final float[] p;
    private i q;
    private j r;
    private l s;
    /* access modifiers changed from: private */
    public View.OnLongClickListener t;
    private k u;
    private int v;
    private int w;
    private int x;
    private int y;
    private h z;

    public d(ImageView imageView) {
        this(imageView, true);
    }

    public d(ImageView imageView, boolean z2) {
        this.f1414b = 200;
        this.d = 1.0f;
        this.e = 1.75f;
        this.f = 3.0f;
        this.g = true;
        this.h = false;
        this.l = new Matrix();
        this.m = new Matrix();
        this.n = new Matrix();
        this.o = new RectF();
        this.p = new float[9];
        this.A = 2;
        this.C = ImageView.ScaleType.FIT_CENTER;
        this.i = new WeakReference<>(imageView);
        imageView.setDrawingCacheEnabled(true);
        imageView.setOnTouchListener(this);
        ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
        if (viewTreeObserver != null) {
            viewTreeObserver.addOnGlobalLayoutListener(this);
        }
        b(imageView);
        if (!imageView.isInEditMode()) {
            this.k = g.a(imageView.getContext(), this);
            this.j = new GestureDetector(imageView.getContext(), new e(this));
            this.j.setOnDoubleTapListener(new b(this));
            b(z2);
        }
    }

    private float a(Matrix matrix, int i2) {
        matrix.getValues(this.p);
        return this.p[i2];
    }

    private RectF a(Matrix matrix) {
        Drawable drawable;
        ImageView c2 = c();
        if (c2 == null || (drawable = c2.getDrawable()) == null) {
            return null;
        }
        this.o.set(0.0f, 0.0f, (float) drawable.getIntrinsicWidth(), (float) drawable.getIntrinsicHeight());
        matrix.mapRect(this.o);
        return this.o;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private void a(Drawable drawable) {
        ImageView c2 = c();
        if (c2 != null && drawable != null) {
            float c3 = (float) c(c2);
            float d2 = (float) d(c2);
            int intrinsicWidth = drawable.getIntrinsicWidth();
            int intrinsicHeight = drawable.getIntrinsicHeight();
            this.l.reset();
            float f2 = c3 / ((float) intrinsicWidth);
            float f3 = d2 / ((float) intrinsicHeight);
            if (this.C != ImageView.ScaleType.CENTER) {
                if (this.C != ImageView.ScaleType.CENTER_CROP) {
                    if (this.C != ImageView.ScaleType.CENTER_INSIDE) {
                        RectF rectF = new RectF(0.0f, 0.0f, (float) intrinsicWidth, (float) intrinsicHeight);
                        RectF rectF2 = new RectF(0.0f, 0.0f, c3, d2);
                        switch (f.f1416a[this.C.ordinal()]) {
                            case 2:
                                this.l.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.START);
                                break;
                            case 3:
                                this.l.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.END);
                                break;
                            case 4:
                                this.l.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.CENTER);
                                break;
                            case 5:
                                this.l.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.FILL);
                                break;
                        }
                    } else {
                        float min = Math.min(1.0f, Math.min(f2, f3));
                        this.l.postScale(min, min);
                        this.l.postTranslate((c3 - (((float) intrinsicWidth) * min)) / 2.0f, (d2 - (((float) intrinsicHeight) * min)) / 2.0f);
                    }
                } else {
                    float max = Math.max(f2, f3);
                    this.l.postScale(max, max);
                    this.l.postTranslate((c3 - (((float) intrinsicWidth) * max)) / 2.0f, (d2 - (((float) intrinsicHeight) * max)) / 2.0f);
                }
            } else {
                this.l.postTranslate((c3 - ((float) intrinsicWidth)) / 2.0f, (d2 - ((float) intrinsicHeight)) / 2.0f);
            }
            t();
        }
    }

    private static boolean a(ImageView imageView) {
        return (imageView == null || imageView.getDrawable() == null) ? false : true;
    }

    private static void b(float f2, float f3, float f4) {
        if (f2 >= f3) {
            throw new IllegalArgumentException("MinZoom has to be less than MidZoom");
        } else if (f3 >= f4) {
            throw new IllegalArgumentException("MidZoom has to be less than MaxZoom");
        }
    }

    /* access modifiers changed from: private */
    public void b(Matrix matrix) {
        RectF a2;
        ImageView c2 = c();
        if (c2 != null) {
            r();
            c2.setImageMatrix(matrix);
            if (this.q != null && (a2 = a(matrix)) != null) {
                this.q.a(a2);
            }
        }
    }

    private static void b(ImageView imageView) {
        if (imageView != null && !(imageView instanceof c) && !ImageView.ScaleType.MATRIX.equals(imageView.getScaleType())) {
            imageView.setScaleType(ImageView.ScaleType.MATRIX);
        }
    }

    private static boolean b(ImageView.ScaleType scaleType) {
        if (scaleType == null) {
            return false;
        }
        switch (f.f1416a[scaleType.ordinal()]) {
            case 1:
                throw new IllegalArgumentException(scaleType.name() + " is not supported in PhotoView");
            default:
                return true;
        }
    }

    private int c(ImageView imageView) {
        if (imageView == null) {
            return 0;
        }
        return (imageView.getWidth() - imageView.getPaddingLeft()) - imageView.getPaddingRight();
    }

    private int d(ImageView imageView) {
        if (imageView == null) {
            return 0;
        }
        return (imageView.getHeight() - imageView.getPaddingTop()) - imageView.getPaddingBottom();
    }

    private void p() {
        if (this.z != null) {
            this.z.a();
            this.z = null;
        }
    }

    private void q() {
        if (s()) {
            b(m());
        }
    }

    private void r() {
        ImageView c2 = c();
        if (c2 != null && !(c2 instanceof c) && !ImageView.ScaleType.MATRIX.equals(c2.getScaleType())) {
            throw new IllegalStateException("The ImageView's ScaleType has been changed since attaching a PhotoViewAttacher");
        }
    }

    private boolean s() {
        float f2;
        float f3 = 0.0f;
        ImageView c2 = c();
        if (c2 == null) {
            return false;
        }
        RectF a2 = a(m());
        if (a2 == null) {
            return false;
        }
        float height = a2.height();
        float width = a2.width();
        int d2 = d(c2);
        if (height <= ((float) d2)) {
            switch (f.f1416a[this.C.ordinal()]) {
                case 2:
                    f2 = -a2.top;
                    break;
                case 3:
                    f2 = (((float) d2) - height) - a2.top;
                    break;
                default:
                    f2 = ((((float) d2) - height) / 2.0f) - a2.top;
                    break;
            }
        } else {
            f2 = a2.top > 0.0f ? -a2.top : a2.bottom < ((float) d2) ? ((float) d2) - a2.bottom : 0.0f;
        }
        int c3 = c(c2);
        if (width <= ((float) c3)) {
            switch (f.f1416a[this.C.ordinal()]) {
                case 2:
                    f3 = -a2.left;
                    break;
                case 3:
                    f3 = (((float) c3) - width) - a2.left;
                    break;
                default:
                    f3 = ((((float) c3) - width) / 2.0f) - a2.left;
                    break;
            }
            this.A = 2;
        } else if (a2.left > 0.0f) {
            this.A = 0;
            f3 = -a2.left;
        } else if (a2.right < ((float) c3)) {
            f3 = ((float) c3) - a2.right;
            this.A = 1;
        } else {
            this.A = -1;
        }
        this.n.postTranslate(f3, f2);
        return true;
    }

    private void t() {
        this.n.reset();
        b(m());
        s();
    }

    public void a() {
        if (this.i != null) {
            ImageView imageView = this.i.get();
            if (imageView != null) {
                ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
                if (viewTreeObserver != null && viewTreeObserver.isAlive()) {
                    viewTreeObserver.removeGlobalOnLayoutListener(this);
                }
                imageView.setOnTouchListener(null);
                p();
            }
            if (this.j != null) {
                this.j.setOnDoubleTapListener(null);
            }
            this.q = null;
            this.r = null;
            this.s = null;
            this.i = null;
        }
    }

    public void a(float f2) {
        this.n.setRotate(f2 % 360.0f);
        q();
    }

    public void a(float f2, float f3) {
        if (!this.k.a()) {
            if (c) {
                a.a().a("PhotoViewAttacher", String.format("onDrag: dx: %.2f. dy: %.2f", Float.valueOf(f2), Float.valueOf(f3)));
            }
            ImageView c2 = c();
            this.n.postTranslate(f2, f3);
            q();
            ViewParent parent = c2.getParent();
            if (!this.g || this.k.a() || this.h) {
                if (parent != null) {
                    parent.requestDisallowInterceptTouchEvent(true);
                }
            } else if ((this.A == 2 || ((this.A == 0 && f2 >= 1.0f) || (this.A == 1 && f2 <= -1.0f))) && parent != null) {
                parent.requestDisallowInterceptTouchEvent(false);
            }
        }
    }

    public void a(float f2, float f3, float f4) {
        if (c) {
            a.a().a("PhotoViewAttacher", String.format("onScale: scale: %.2f. fX: %.2f. fY: %.2f", Float.valueOf(f2), Float.valueOf(f3), Float.valueOf(f4)));
        }
        if (g() < this.f || f2 < 1.0f) {
            if (this.u != null) {
                this.u.a(f2, f3, f4);
            }
            this.n.postScale(f2, f2, f3, f4);
            q();
        }
    }

    public void a(float f2, float f3, float f4, float f5) {
        if (c) {
            a.a().a("PhotoViewAttacher", "onFling. sX: " + f2 + " sY: " + f3 + " Vx: " + f4 + " Vy: " + f5);
        }
        ImageView c2 = c();
        this.z = new h(this, c2.getContext());
        this.z.a(c(c2), d(c2), (int) f4, (int) f5);
        c2.post(this.z);
    }

    public void a(float f2, float f3, float f4, boolean z2) {
        ImageView c2 = c();
        if (c2 == null) {
            return;
        }
        if (f2 < this.d || f2 > this.f) {
            a.a().b("PhotoViewAttacher", "Scale must be within the range of minScale and maxScale");
        } else if (z2) {
            c2.post(new g(this, g(), f2, f3, f4));
        } else {
            this.n.setScale(f2, f2, f3, f4);
            q();
        }
    }

    public void a(float f2, boolean z2) {
        ImageView c2 = c();
        if (c2 != null) {
            a(f2, (float) (c2.getRight() / 2), (float) (c2.getBottom() / 2), z2);
        }
    }

    public void a(int i2) {
        if (i2 < 0) {
            i2 = 200;
        }
        this.f1414b = i2;
    }

    public void a(GestureDetector.OnDoubleTapListener onDoubleTapListener) {
        if (onDoubleTapListener != null) {
            this.j.setOnDoubleTapListener(onDoubleTapListener);
        } else {
            this.j.setOnDoubleTapListener(new b(this));
        }
    }

    public void a(View.OnLongClickListener onLongClickListener) {
        this.t = onLongClickListener;
    }

    public void a(ImageView.ScaleType scaleType) {
        if (b(scaleType) && scaleType != this.C) {
            this.C = scaleType;
            k();
        }
    }

    public void a(i iVar) {
        this.q = iVar;
    }

    public void a(j jVar) {
        this.r = jVar;
    }

    public void a(k kVar) {
        this.u = kVar;
    }

    public void a(l lVar) {
        this.s = lVar;
    }

    public void a(boolean z2) {
        this.g = z2;
    }

    public RectF b() {
        s();
        return a(m());
    }

    public void b(float f2) {
        this.n.postRotate(f2 % 360.0f);
        q();
    }

    public void b(boolean z2) {
        this.B = z2;
        k();
    }

    public ImageView c() {
        ImageView imageView = null;
        if (this.i != null) {
            imageView = this.i.get();
        }
        if (imageView == null) {
            a();
            a.a().b("PhotoViewAttacher", "ImageView no longer exists. You should not use this PhotoViewAttacher any more.");
        }
        return imageView;
    }

    public void c(float f2) {
        b(f2, this.e, this.f);
        this.d = f2;
    }

    public float d() {
        return this.d;
    }

    public void d(float f2) {
        b(this.d, f2, this.f);
        this.e = f2;
    }

    public float e() {
        return this.e;
    }

    public void e(float f2) {
        b(this.d, this.e, f2);
        this.f = f2;
    }

    public float f() {
        return this.f;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: uk.co.senab.photoview.d.a(float, boolean):void
     arg types: [float, int]
     candidates:
      uk.co.senab.photoview.d.a(android.graphics.Matrix, int):float
      uk.co.senab.photoview.d.a(uk.co.senab.photoview.d, android.graphics.Matrix):void
      uk.co.senab.photoview.d.a(float, float):void
      uk.co.senab.photoview.a.f.a(float, float):void
      uk.co.senab.photoview.d.a(float, boolean):void */
    public void f(float f2) {
        a(f2, false);
    }

    public float g() {
        return (float) Math.sqrt((double) (((float) Math.pow((double) a(this.n, 0), 2.0d)) + ((float) Math.pow((double) a(this.n, 3), 2.0d))));
    }

    public ImageView.ScaleType h() {
        return this.C;
    }

    public j i() {
        return this.r;
    }

    public l j() {
        return this.s;
    }

    public void k() {
        ImageView c2 = c();
        if (c2 == null) {
            return;
        }
        if (this.B) {
            b(c2);
            a(c2.getDrawable());
            return;
        }
        t();
    }

    public Matrix l() {
        return new Matrix(m());
    }

    public Matrix m() {
        this.m.set(this.l);
        this.m.postConcat(this.n);
        return this.m;
    }

    public Bitmap n() {
        ImageView c2 = c();
        if (c2 == null) {
            return null;
        }
        return c2.getDrawingCache();
    }

    public void onGlobalLayout() {
        ImageView c2 = c();
        if (c2 == null) {
            return;
        }
        if (this.B) {
            int top = c2.getTop();
            int right = c2.getRight();
            int bottom = c2.getBottom();
            int left = c2.getLeft();
            if (top != this.v || bottom != this.x || left != this.y || right != this.w) {
                a(c2.getDrawable());
                this.v = top;
                this.w = right;
                this.x = bottom;
                this.y = left;
                return;
            }
            return;
        }
        a(c2.getDrawable());
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouch(View view, MotionEvent motionEvent) {
        RectF b2;
        boolean z2;
        boolean z3 = false;
        if (!this.B || !a((ImageView) view)) {
            return false;
        }
        ViewParent parent = view.getParent();
        switch (motionEvent.getAction()) {
            case 0:
                if (parent != null) {
                    parent.requestDisallowInterceptTouchEvent(true);
                } else {
                    a.a().b("PhotoViewAttacher", "onTouch getParent() returned null");
                }
                p();
                z2 = false;
                break;
            case 1:
            case 3:
                if (g() < this.d && (b2 = b()) != null) {
                    view.post(new g(this, g(), this.d, b2.centerX(), b2.centerY()));
                    z2 = true;
                    break;
                }
            case 2:
            default:
                z2 = false;
                break;
        }
        if (this.k != null) {
            boolean a2 = this.k.a();
            boolean b3 = this.k.b();
            z2 = this.k.c(motionEvent);
            boolean z4 = !a2 && !this.k.a();
            boolean z5 = !b3 && !this.k.b();
            if (z4 && z5) {
                z3 = true;
            }
            this.h = z3;
        }
        if (this.j == null || !this.j.onTouchEvent(motionEvent)) {
            return z2;
        }
        return true;
    }
}
