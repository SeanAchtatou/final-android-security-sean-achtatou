package uk.co.senab.photoview.c;

import android.content.Context;
import android.widget.Scroller;

public class c extends d {

    /* renamed from: a  reason: collision with root package name */
    private final Scroller f1412a;

    public c(Context context) {
        this.f1412a = new Scroller(context);
    }

    public void a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10) {
        this.f1412a.fling(i, i2, i3, i4, i5, i6, i7, i8);
    }

    public void a(boolean z) {
        this.f1412a.forceFinished(z);
    }

    public boolean a() {
        return this.f1412a.computeScrollOffset();
    }

    public boolean b() {
        return this.f1412a.isFinished();
    }

    public int c() {
        return this.f1412a.getCurrX();
    }

    public int d() {
        return this.f1412a.getCurrY();
    }
}
