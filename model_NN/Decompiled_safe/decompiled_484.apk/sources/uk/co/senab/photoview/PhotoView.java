package uk.co.senab.photoview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.View;
import android.widget.ImageView;

public class PhotoView extends ImageView implements c {

    /* renamed from: a  reason: collision with root package name */
    private d f1403a;

    /* renamed from: b  reason: collision with root package name */
    private ImageView.ScaleType f1404b;

    public PhotoView(Context context) {
        this(context, null);
    }

    public PhotoView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public PhotoView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        super.setScaleType(ImageView.ScaleType.MATRIX);
        a();
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.f1403a == null || this.f1403a.c() == null) {
            this.f1403a = new d(this);
        }
        if (this.f1404b != null) {
            setScaleType(this.f1404b);
            this.f1404b = null;
        }
    }

    public Matrix getDisplayMatrix() {
        return this.f1403a.l();
    }

    public RectF getDisplayRect() {
        return this.f1403a.b();
    }

    public c getIPhotoViewImplementation() {
        return this.f1403a;
    }

    @Deprecated
    public float getMaxScale() {
        return getMaximumScale();
    }

    public float getMaximumScale() {
        return this.f1403a.f();
    }

    public float getMediumScale() {
        return this.f1403a.e();
    }

    @Deprecated
    public float getMidScale() {
        return getMediumScale();
    }

    @Deprecated
    public float getMinScale() {
        return getMinimumScale();
    }

    public float getMinimumScale() {
        return this.f1403a.d();
    }

    public j getOnPhotoTapListener() {
        return this.f1403a.i();
    }

    public l getOnViewTapListener() {
        return this.f1403a.j();
    }

    public float getScale() {
        return this.f1403a.g();
    }

    public ImageView.ScaleType getScaleType() {
        return this.f1403a.h();
    }

    public Bitmap getVisibleRectangleBitmap() {
        return this.f1403a.n();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        a();
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.f1403a.a();
        super.onDetachedFromWindow();
    }

    public void setAllowParentInterceptOnEdge(boolean z) {
        this.f1403a.a(z);
    }

    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        if (this.f1403a != null) {
            this.f1403a.k();
        }
    }

    public void setImageResource(int i) {
        super.setImageResource(i);
        if (this.f1403a != null) {
            this.f1403a.k();
        }
    }

    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        if (this.f1403a != null) {
            this.f1403a.k();
        }
    }

    @Deprecated
    public void setMaxScale(float f) {
        setMaximumScale(f);
    }

    public void setMaximumScale(float f) {
        this.f1403a.e(f);
    }

    public void setMediumScale(float f) {
        this.f1403a.d(f);
    }

    @Deprecated
    public void setMidScale(float f) {
        setMediumScale(f);
    }

    @Deprecated
    public void setMinScale(float f) {
        setMinimumScale(f);
    }

    public void setMinimumScale(float f) {
        this.f1403a.c(f);
    }

    public void setOnDoubleTapListener(GestureDetector.OnDoubleTapListener onDoubleTapListener) {
        this.f1403a.a(onDoubleTapListener);
    }

    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.f1403a.a(onLongClickListener);
    }

    public void setOnMatrixChangeListener(i iVar) {
        this.f1403a.a(iVar);
    }

    public void setOnPhotoTapListener(j jVar) {
        this.f1403a.a(jVar);
    }

    public void setOnScaleChangeListener(k kVar) {
        this.f1403a.a(kVar);
    }

    public void setOnViewTapListener(l lVar) {
        this.f1403a.a(lVar);
    }

    public void setPhotoViewRotation(float f) {
        this.f1403a.a(f);
    }

    public void setRotationBy(float f) {
        this.f1403a.b(f);
    }

    public void setRotationTo(float f) {
        this.f1403a.a(f);
    }

    public void setScale(float f) {
        this.f1403a.f(f);
    }

    public void setScaleType(ImageView.ScaleType scaleType) {
        if (this.f1403a != null) {
            this.f1403a.a(scaleType);
        } else {
            this.f1404b = scaleType;
        }
    }

    public void setZoomTransitionDuration(int i) {
        this.f1403a.a(i);
    }

    public void setZoomable(boolean z) {
        this.f1403a.b(z);
    }
}
