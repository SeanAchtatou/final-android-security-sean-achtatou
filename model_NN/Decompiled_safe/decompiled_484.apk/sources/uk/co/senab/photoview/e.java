package uk.co.senab.photoview;

import android.view.GestureDetector;
import android.view.MotionEvent;

class e extends GestureDetector.SimpleOnGestureListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f1415a;

    e(d dVar) {
        this.f1415a = dVar;
    }

    public void onLongPress(MotionEvent motionEvent) {
        if (this.f1415a.t != null) {
            this.f1415a.t.onLongClick(this.f1415a.c());
        }
    }
}
