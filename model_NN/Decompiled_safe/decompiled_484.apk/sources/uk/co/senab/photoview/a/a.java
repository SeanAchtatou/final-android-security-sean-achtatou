package uk.co.senab.photoview.a;

import android.content.Context;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import com.qihoo.messenger.util.QDefine;

public class a implements e {

    /* renamed from: a  reason: collision with root package name */
    protected f f1405a;

    /* renamed from: b  reason: collision with root package name */
    float f1406b;
    float c;
    final float d;
    final float e;
    private VelocityTracker f;
    private boolean g;

    public a(Context context) {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        this.e = (float) viewConfiguration.getScaledMinimumFlingVelocity();
        this.d = (float) viewConfiguration.getScaledTouchSlop();
    }

    /* access modifiers changed from: package-private */
    public float a(MotionEvent motionEvent) {
        return motionEvent.getX();
    }

    public void a(f fVar) {
        this.f1405a = fVar;
    }

    public boolean a() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public float b(MotionEvent motionEvent) {
        return motionEvent.getY();
    }

    public boolean b() {
        return this.g;
    }

    public boolean c(MotionEvent motionEvent) {
        boolean z = false;
        switch (motionEvent.getAction()) {
            case 0:
                this.f = VelocityTracker.obtain();
                if (this.f != null) {
                    this.f.addMovement(motionEvent);
                } else {
                    uk.co.senab.photoview.b.a.a().b("CupcakeGestureDetector", "Velocity tracker is null");
                }
                this.f1406b = a(motionEvent);
                this.c = b(motionEvent);
                this.g = false;
                break;
            case 1:
                if (this.g && this.f != null) {
                    this.f1406b = a(motionEvent);
                    this.c = b(motionEvent);
                    this.f.addMovement(motionEvent);
                    this.f.computeCurrentVelocity(QDefine.ONE_SECOND);
                    float xVelocity = this.f.getXVelocity();
                    float yVelocity = this.f.getYVelocity();
                    if (Math.max(Math.abs(xVelocity), Math.abs(yVelocity)) >= this.e) {
                        this.f1405a.a(this.f1406b, this.c, -xVelocity, -yVelocity);
                    }
                }
                if (this.f != null) {
                    this.f.recycle();
                    this.f = null;
                    break;
                }
                break;
            case 2:
                float a2 = a(motionEvent);
                float b2 = b(motionEvent);
                float f2 = a2 - this.f1406b;
                float f3 = b2 - this.c;
                if (!this.g) {
                    if (Math.sqrt((double) ((f2 * f2) + (f3 * f3))) >= ((double) this.d)) {
                        z = true;
                    }
                    this.g = z;
                }
                if (this.g) {
                    this.f1405a.a(f2, f3);
                    this.f1406b = a2;
                    this.c = b2;
                    if (this.f != null) {
                        this.f.addMovement(motionEvent);
                        break;
                    }
                }
                break;
            case 3:
                if (this.f != null) {
                    this.f.recycle();
                    this.f = null;
                    break;
                }
                break;
        }
        return true;
    }
}
