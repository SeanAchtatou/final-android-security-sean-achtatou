package uk.co.senab.photoview;

import android.content.Context;
import android.graphics.RectF;
import android.widget.ImageView;
import uk.co.senab.photoview.b.a;
import uk.co.senab.photoview.c.d;

class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f1419a;

    /* renamed from: b  reason: collision with root package name */
    private final d f1420b;
    private int c;
    private int d;

    public h(d dVar, Context context) {
        this.f1419a = dVar;
        this.f1420b = d.a(context);
    }

    public void a() {
        if (d.c) {
            a.a().a("PhotoViewAttacher", "Cancel Fling");
        }
        this.f1420b.a(true);
    }

    public void a(int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int i8;
        RectF b2 = this.f1419a.b();
        if (b2 != null) {
            int round = Math.round(-b2.left);
            if (((float) i) < b2.width()) {
                i5 = Math.round(b2.width() - ((float) i));
                i6 = 0;
            } else {
                i5 = round;
                i6 = round;
            }
            int round2 = Math.round(-b2.top);
            if (((float) i2) < b2.height()) {
                i7 = Math.round(b2.height() - ((float) i2));
                i8 = 0;
            } else {
                i7 = round2;
                i8 = round2;
            }
            this.c = round;
            this.d = round2;
            if (d.c) {
                a.a().a("PhotoViewAttacher", "fling. StartX:" + round + " StartY:" + round2 + " MaxX:" + i5 + " MaxY:" + i7);
            }
            if (round != i5 || round2 != i7) {
                this.f1420b.a(round, round2, i3, i4, i6, i5, i8, i7, 0, 0);
            }
        }
    }

    public void run() {
        ImageView c2;
        if (!this.f1420b.b() && (c2 = this.f1419a.c()) != null && this.f1420b.a()) {
            int c3 = this.f1420b.c();
            int d2 = this.f1420b.d();
            if (d.c) {
                a.a().a("PhotoViewAttacher", "fling run(). CurrentX:" + this.c + " CurrentY:" + this.d + " NewX:" + c3 + " NewY:" + d2);
            }
            this.f1419a.n.postTranslate((float) (this.c - c3), (float) (this.d - d2));
            this.f1419a.b(this.f1419a.m());
            this.c = c3;
            this.d = d2;
            a.a(c2, this);
        }
    }
}
