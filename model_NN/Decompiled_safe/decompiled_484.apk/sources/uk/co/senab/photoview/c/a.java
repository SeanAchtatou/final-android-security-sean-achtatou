package uk.co.senab.photoview.c;

import android.annotation.TargetApi;
import android.content.Context;
import android.widget.OverScroller;

@TargetApi(9)
public class a extends d {

    /* renamed from: a  reason: collision with root package name */
    protected final OverScroller f1410a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f1411b = false;

    public a(Context context) {
        this.f1410a = new OverScroller(context);
    }

    public void a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10) {
        this.f1410a.fling(i, i2, i3, i4, i5, i6, i7, i8, i9, i10);
    }

    public void a(boolean z) {
        this.f1410a.forceFinished(z);
    }

    public boolean a() {
        if (this.f1411b) {
            this.f1410a.computeScrollOffset();
            this.f1411b = false;
        }
        return this.f1410a.computeScrollOffset();
    }

    public boolean b() {
        return this.f1410a.isFinished();
    }

    public int c() {
        return this.f1410a.getCurrX();
    }

    public int d() {
        return this.f1410a.getCurrY();
    }
}
