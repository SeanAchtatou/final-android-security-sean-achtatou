package uk.co.senab.photoview.a;

import android.content.Context;
import android.os.Build;

public final class g {
    public static e a(Context context, f fVar) {
        int i = Build.VERSION.SDK_INT;
        e aVar = i < 5 ? new a(context) : i < 8 ? new b(context) : new c(context);
        aVar.a(fVar);
        return aVar;
    }
}
