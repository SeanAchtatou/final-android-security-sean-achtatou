package com.a.a;

import com.a.a.b.a.j;
import com.a.a.d.a;
import com.a.a.d.d;
import java.io.IOException;

public abstract class af<T> {
    public final t a(T t) {
        try {
            j jVar = new j();
            a(jVar, t);
            return jVar.a();
        } catch (IOException e) {
            throw new u(e);
        }
    }

    public abstract void a(d dVar, T t);

    public abstract T b(a aVar);
}
