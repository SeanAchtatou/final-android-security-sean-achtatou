package com.a.a;

import com.a.a.b.ag;
import com.a.a.d.d;
import java.io.IOException;
import java.io.StringWriter;

public abstract class t {
    public Number a() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public String b() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public double c() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public long d() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public int e() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public boolean f() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public boolean g() {
        return this instanceof r;
    }

    public boolean h() {
        return this instanceof w;
    }

    public boolean i() {
        return this instanceof z;
    }

    public boolean j() {
        return this instanceof v;
    }

    public w k() {
        if (h()) {
            return (w) this;
        }
        throw new IllegalStateException("Not a JSON Object: " + this);
    }

    public r l() {
        if (g()) {
            return (r) this;
        }
        throw new IllegalStateException("This is not a JSON Array.");
    }

    public z m() {
        if (i()) {
            return (z) this;
        }
        throw new IllegalStateException("This is not a JSON Primitive.");
    }

    /* access modifiers changed from: package-private */
    public Boolean n() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public String toString() {
        try {
            StringWriter stringWriter = new StringWriter();
            d dVar = new d(stringWriter);
            dVar.b(true);
            ag.a(this, dVar);
            return stringWriter.toString();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }
}
