package com.a.a.b.a;

import com.a.a.af;
import com.a.a.ag;
import com.a.a.b.w;
import com.a.a.d.a;
import com.a.a.d.d;
import com.a.a.j;
import java.util.ArrayList;

public final class n extends af<Object> {

    /* renamed from: a  reason: collision with root package name */
    public static final ag f262a = new o();

    /* renamed from: b  reason: collision with root package name */
    private final j f263b;

    private n(j jVar) {
        this.f263b = jVar;
    }

    /* synthetic */ n(j jVar, o oVar) {
        this(jVar);
    }

    public void a(d dVar, Object obj) {
        if (obj == null) {
            dVar.f();
            return;
        }
        af a2 = this.f263b.a((Class) obj.getClass());
        if (a2 instanceof n) {
            dVar.d();
            dVar.e();
            return;
        }
        a2.a(dVar, obj);
    }

    public Object b(a aVar) {
        switch (p.f264a[aVar.f().ordinal()]) {
            case 1:
                ArrayList arrayList = new ArrayList();
                aVar.a();
                while (aVar.e()) {
                    arrayList.add(b(aVar));
                }
                aVar.b();
                return arrayList;
            case 2:
                w wVar = new w();
                aVar.c();
                while (aVar.e()) {
                    wVar.put(aVar.g(), b(aVar));
                }
                aVar.d();
                return wVar;
            case 3:
                return aVar.h();
            case 4:
                return Double.valueOf(aVar.k());
            case 5:
                return Boolean.valueOf(aVar.i());
            case 6:
                aVar.j();
                return null;
            default:
                throw new IllegalStateException();
        }
    }
}
