package com.a.a.b.a;

import com.a.a.af;
import com.a.a.c.a;
import com.a.a.d.d;
import com.a.a.j;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

final class y<T> extends af<T> {

    /* renamed from: a  reason: collision with root package name */
    private final j f275a;

    /* renamed from: b  reason: collision with root package name */
    private final af<T> f276b;
    private final Type c;

    y(j jVar, af<T> afVar, Type type) {
        this.f275a = jVar;
        this.f276b = afVar;
        this.c = type;
    }

    private Type a(Type type, Object obj) {
        return obj != null ? (type == Object.class || (type instanceof TypeVariable) || (type instanceof Class)) ? obj.getClass() : type : type;
    }

    public void a(d dVar, T t) {
        af<T> afVar = this.f276b;
        Type a2 = a(this.c, t);
        if (a2 != this.c) {
            afVar = this.f275a.a((a) a.get(a2));
            if ((afVar instanceof s) && !(this.f276b instanceof s)) {
                afVar = this.f276b;
            }
        }
        afVar.a(dVar, t);
    }

    public T b(com.a.a.d.a aVar) {
        return this.f276b.b(aVar);
    }
}
