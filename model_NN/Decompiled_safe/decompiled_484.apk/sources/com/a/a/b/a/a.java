package com.a.a.b.a;

import com.a.a.af;
import com.a.a.ag;
import com.a.a.d.c;
import com.a.a.d.d;
import com.a.a.j;
import java.lang.reflect.Array;
import java.util.ArrayList;

public final class a<E> extends af<Object> {

    /* renamed from: a  reason: collision with root package name */
    public static final ag f233a = new b();

    /* renamed from: b  reason: collision with root package name */
    private final Class<E> f234b;
    private final af<E> c;

    public a(j jVar, af<E> afVar, Class<E> cls) {
        this.c = new y(jVar, afVar, cls);
        this.f234b = cls;
    }

    public void a(d dVar, Object obj) {
        if (obj == null) {
            dVar.f();
            return;
        }
        dVar.b();
        int length = Array.getLength(obj);
        for (int i = 0; i < length; i++) {
            this.c.a(dVar, Array.get(obj, i));
        }
        dVar.c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<E>, int]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException} */
    public Object b(com.a.a.d.a aVar) {
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        ArrayList arrayList = new ArrayList();
        aVar.a();
        while (aVar.e()) {
            arrayList.add(this.c.b(aVar));
        }
        aVar.b();
        Object newInstance = Array.newInstance((Class<?>) this.f234b, arrayList.size());
        for (int i = 0; i < arrayList.size(); i++) {
            Array.set(newInstance, i, arrayList.get(i));
        }
        return newInstance;
    }
}
