package com.a.a.b.a;

import com.a.a.af;
import com.a.a.ag;
import com.a.a.c.a;
import com.a.a.j;
import java.sql.Timestamp;
import java.util.Date;

final class ao implements ag {
    ao() {
    }

    public <T> af<T> a(j jVar, a<T> aVar) {
        if (aVar.getRawType() != Timestamp.class) {
            return null;
        }
        return new ap(this, jVar.a(Date.class));
    }
}
