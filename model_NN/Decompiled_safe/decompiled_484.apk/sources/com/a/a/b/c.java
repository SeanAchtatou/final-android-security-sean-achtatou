package com.a.a.b;

import java.io.Serializable;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;

final class c implements Serializable, GenericArrayType {

    /* renamed from: a  reason: collision with root package name */
    private final Type f295a;

    public c(Type type) {
        this.f295a = b.d(type);
    }

    public boolean equals(Object obj) {
        return (obj instanceof GenericArrayType) && b.a(this, (GenericArrayType) obj);
    }

    public Type getGenericComponentType() {
        return this.f295a;
    }

    public int hashCode() {
        return this.f295a.hashCode();
    }

    public String toString() {
        return b.f(this.f295a) + "[]";
    }
}
