package com.a.a.b;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;

final class e implements Serializable, WildcardType {

    /* renamed from: a  reason: collision with root package name */
    private final Type f298a;

    /* renamed from: b  reason: collision with root package name */
    private final Type f299b;

    public e(Type[] typeArr, Type[] typeArr2) {
        boolean z = true;
        a.a(typeArr2.length <= 1);
        a.a(typeArr.length == 1);
        if (typeArr2.length == 1) {
            a.a(typeArr2[0]);
            b.i(typeArr2[0]);
            a.a(typeArr[0] != Object.class ? false : z);
            this.f299b = b.d(typeArr2[0]);
            this.f298a = Object.class;
            return;
        }
        a.a(typeArr[0]);
        b.i(typeArr[0]);
        this.f299b = null;
        this.f298a = b.d(typeArr[0]);
    }

    public boolean equals(Object obj) {
        return (obj instanceof WildcardType) && b.a(this, (WildcardType) obj);
    }

    public Type[] getLowerBounds() {
        if (this.f299b == null) {
            return b.f294a;
        }
        return new Type[]{this.f299b};
    }

    public Type[] getUpperBounds() {
        return new Type[]{this.f298a};
    }

    public int hashCode() {
        return (this.f299b != null ? this.f299b.hashCode() + 31 : 1) ^ (this.f298a.hashCode() + 31);
    }

    public String toString() {
        return this.f299b != null ? "? super " + b.f(this.f299b) : this.f298a == Object.class ? "?" : "? extends " + b.f(this.f298a);
    }
}
