package com.a.a.b.a;

import com.a.a.ab;
import com.a.a.af;
import com.a.a.b.ae;
import com.a.a.b.ag;
import com.a.a.b.u;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;
import com.a.a.j;
import com.a.a.t;
import com.a.a.z;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;

final class m<K, V> extends af<Map<K, V>> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f260a;

    /* renamed from: b  reason: collision with root package name */
    private final af<K> f261b;
    private final af<V> c;
    private final ae<? extends Map<K, V>> d;

    public m(l lVar, j jVar, Type type, af<K> afVar, Type type2, af<V> afVar2, ae<? extends Map<K, V>> aeVar) {
        this.f260a = lVar;
        this.f261b = new y(jVar, afVar, type);
        this.c = new y(jVar, afVar2, type2);
        this.d = aeVar;
    }

    private String a(t tVar) {
        if (tVar.i()) {
            z m = tVar.m();
            if (m.p()) {
                return String.valueOf(m.a());
            }
            if (m.o()) {
                return Boolean.toString(m.f());
            }
            if (m.q()) {
                return m.b();
            }
            throw new AssertionError();
        } else if (tVar.j()) {
            return "null";
        } else {
            throw new AssertionError();
        }
    }

    /* renamed from: a */
    public Map<K, V> b(a aVar) {
        c f = aVar.f();
        if (f == c.NULL) {
            aVar.j();
            return null;
        }
        Map<K, V> map = (Map) this.d.a();
        if (f == c.BEGIN_ARRAY) {
            aVar.a();
            while (aVar.e()) {
                aVar.a();
                K b2 = this.f261b.b(aVar);
                if (map.put(b2, this.c.b(aVar)) != null) {
                    throw new ab("duplicate key: " + ((Object) b2));
                }
                aVar.b();
            }
            aVar.b();
            return map;
        }
        aVar.c();
        while (aVar.e()) {
            u.f322a.a(aVar);
            K b3 = this.f261b.b(aVar);
            if (map.put(b3, this.c.b(aVar)) != null) {
                throw new ab("duplicate key: " + ((Object) b3));
            }
        }
        aVar.d();
        return map;
    }

    public void a(d dVar, Map<K, V> map) {
        int i = 0;
        if (map == null) {
            dVar.f();
        } else if (!this.f260a.f259b) {
            dVar.d();
            for (Map.Entry next : map.entrySet()) {
                dVar.a(String.valueOf(next.getKey()));
                this.c.a(dVar, next.getValue());
            }
            dVar.e();
        } else {
            ArrayList arrayList = new ArrayList(map.size());
            ArrayList arrayList2 = new ArrayList(map.size());
            boolean z = false;
            for (Map.Entry next2 : map.entrySet()) {
                t a2 = this.f261b.a(next2.getKey());
                arrayList.add(a2);
                arrayList2.add(next2.getValue());
                z = (a2.g() || a2.h()) | z;
            }
            if (z) {
                dVar.b();
                while (i < arrayList.size()) {
                    dVar.b();
                    ag.a((t) arrayList.get(i), dVar);
                    this.c.a(dVar, arrayList2.get(i));
                    dVar.c();
                    i++;
                }
                dVar.c();
                return;
            }
            dVar.d();
            while (i < arrayList.size()) {
                dVar.a(a((t) arrayList.get(i)));
                this.c.a(dVar, arrayList2.get(i));
                i++;
            }
            dVar.e();
        }
    }
}
