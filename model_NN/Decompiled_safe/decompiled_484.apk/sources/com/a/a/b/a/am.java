package com.a.a.b.a;

import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;
import java.net.InetAddress;

final class am extends af<InetAddress> {
    am() {
    }

    /* renamed from: a */
    public InetAddress b(a aVar) {
        if (aVar.f() != c.NULL) {
            return InetAddress.getByName(aVar.h());
        }
        aVar.j();
        return null;
    }

    public void a(d dVar, InetAddress inetAddress) {
        dVar.b(inetAddress == null ? null : inetAddress.getHostAddress());
    }
}
