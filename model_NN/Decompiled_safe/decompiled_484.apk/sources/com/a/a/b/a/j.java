package com.a.a.b.a;

import com.a.a.d.d;
import com.a.a.r;
import com.a.a.t;
import com.a.a.v;
import com.a.a.w;
import com.a.a.z;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public final class j extends d {

    /* renamed from: a  reason: collision with root package name */
    private static final Writer f256a = new k();

    /* renamed from: b  reason: collision with root package name */
    private static final z f257b = new z("closed");
    private final List<t> c = new ArrayList();
    private String d;
    private t e = v.f345a;

    public j() {
        super(f256a);
    }

    private void a(t tVar) {
        if (this.d != null) {
            if (!tVar.j() || i()) {
                ((w) j()).a(this.d, tVar);
            }
            this.d = null;
        } else if (this.c.isEmpty()) {
            this.e = tVar;
        } else {
            t j = j();
            if (j instanceof r) {
                ((r) j).a(tVar);
                return;
            }
            throw new IllegalStateException();
        }
    }

    private t j() {
        return this.c.get(this.c.size() - 1);
    }

    public d a(long j) {
        a(new z(Long.valueOf(j)));
        return this;
    }

    public d a(Number number) {
        if (number == null) {
            return f();
        }
        if (!g()) {
            double doubleValue = number.doubleValue();
            if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                throw new IllegalArgumentException("JSON forbids NaN and infinities: " + number);
            }
        }
        a(new z(number));
        return this;
    }

    public d a(String str) {
        if (this.c.isEmpty() || this.d != null) {
            throw new IllegalStateException();
        } else if (j() instanceof w) {
            this.d = str;
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    public d a(boolean z) {
        a(new z(Boolean.valueOf(z)));
        return this;
    }

    public t a() {
        if (this.c.isEmpty()) {
            return this.e;
        }
        throw new IllegalStateException("Expected one JSON element but was " + this.c);
    }

    public d b() {
        r rVar = new r();
        a(rVar);
        this.c.add(rVar);
        return this;
    }

    public d b(String str) {
        if (str == null) {
            return f();
        }
        a(new z(str));
        return this;
    }

    public d c() {
        if (this.c.isEmpty() || this.d != null) {
            throw new IllegalStateException();
        } else if (j() instanceof r) {
            this.c.remove(this.c.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    public void close() {
        if (!this.c.isEmpty()) {
            throw new IOException("Incomplete document");
        }
        this.c.add(f257b);
    }

    public d d() {
        w wVar = new w();
        a(wVar);
        this.c.add(wVar);
        return this;
    }

    public d e() {
        if (this.c.isEmpty() || this.d != null) {
            throw new IllegalStateException();
        } else if (j() instanceof w) {
            this.c.remove(this.c.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    public d f() {
        a(v.f345a);
        return this;
    }

    public void flush() {
    }
}
