package com.a.a.b;

import java.math.BigDecimal;

public final class v extends Number {

    /* renamed from: a  reason: collision with root package name */
    private final String f323a;

    public v(String str) {
        this.f323a = str;
    }

    public double doubleValue() {
        return Double.parseDouble(this.f323a);
    }

    public float floatValue() {
        return Float.parseFloat(this.f323a);
    }

    public int intValue() {
        try {
            return Integer.parseInt(this.f323a);
        } catch (NumberFormatException e) {
            try {
                return (int) Long.parseLong(this.f323a);
            } catch (NumberFormatException e2) {
                return new BigDecimal(this.f323a).intValue();
            }
        }
    }

    public long longValue() {
        try {
            return Long.parseLong(this.f323a);
        } catch (NumberFormatException e) {
            return new BigDecimal(this.f323a).longValue();
        }
    }

    public String toString() {
        return this.f323a;
    }
}
