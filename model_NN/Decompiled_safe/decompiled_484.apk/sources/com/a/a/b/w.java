package com.a.a.b;

import java.io.Serializable;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;

public final class w<K, V> extends AbstractMap<K, V> implements Serializable {
    static final /* synthetic */ boolean f = (!w.class.desiredAssertionStatus());
    private static final Comparator<Comparable> g = new x();

    /* renamed from: a  reason: collision with root package name */
    Comparator<? super K> f324a;

    /* renamed from: b  reason: collision with root package name */
    ad<K, V> f325b;
    int c;
    int d;
    final ad<K, V> e;
    private w<K, V>.y h;
    private w<K, V>.aa i;

    public w() {
        this(g);
    }

    public w(Comparator<? super K> comparator) {
        this.c = 0;
        this.d = 0;
        this.e = new ad<>();
        this.f324a = comparator == null ? g : comparator;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.w.a(com.a.a.b.ad, com.a.a.b.ad):void
     arg types: [com.a.a.b.ad<K, V>, com.a.a.b.ad<K, V>]
     candidates:
      com.a.a.b.w.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.w.a(java.lang.Object, boolean):com.a.a.b.ad<K, V>
      com.a.a.b.w.a(com.a.a.b.ad, boolean):void
      com.a.a.b.w.a(com.a.a.b.ad, com.a.a.b.ad):void */
    private void a(ad<K, V> adVar) {
        int i2 = 0;
        ad<K, V> adVar2 = adVar.f283b;
        ad<K, V> adVar3 = adVar.c;
        ad<K, V> adVar4 = adVar3.f283b;
        ad<K, V> adVar5 = adVar3.c;
        adVar.c = adVar4;
        if (adVar4 != null) {
            adVar4.f282a = adVar;
        }
        a((ad) adVar, (ad) adVar3);
        adVar3.f283b = adVar;
        adVar.f282a = adVar3;
        adVar.h = Math.max(adVar2 != null ? adVar2.h : 0, adVar4 != null ? adVar4.h : 0) + 1;
        int i3 = adVar.h;
        if (adVar5 != null) {
            i2 = adVar5.h;
        }
        adVar3.h = Math.max(i3, i2) + 1;
    }

    private void a(ad<K, V> adVar, ad<K, V> adVar2) {
        ad<K, V> adVar3 = adVar.f282a;
        adVar.f282a = null;
        if (adVar2 != null) {
            adVar2.f282a = adVar3;
        }
        if (adVar3 == null) {
            this.f325b = adVar2;
        } else if (adVar3.f283b == adVar) {
            adVar3.f283b = adVar2;
        } else if (f || adVar3.c == adVar) {
            adVar3.c = adVar2;
        } else {
            throw new AssertionError();
        }
    }

    private boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.w.a(com.a.a.b.ad, com.a.a.b.ad):void
     arg types: [com.a.a.b.ad<K, V>, com.a.a.b.ad<K, V>]
     candidates:
      com.a.a.b.w.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.w.a(java.lang.Object, boolean):com.a.a.b.ad<K, V>
      com.a.a.b.w.a(com.a.a.b.ad, boolean):void
      com.a.a.b.w.a(com.a.a.b.ad, com.a.a.b.ad):void */
    private void b(ad<K, V> adVar) {
        int i2 = 0;
        ad<K, V> adVar2 = adVar.f283b;
        ad<K, V> adVar3 = adVar.c;
        ad<K, V> adVar4 = adVar2.f283b;
        ad<K, V> adVar5 = adVar2.c;
        adVar.f283b = adVar5;
        if (adVar5 != null) {
            adVar5.f282a = adVar;
        }
        a((ad) adVar, (ad) adVar2);
        adVar2.c = adVar;
        adVar.f282a = adVar2;
        adVar.h = Math.max(adVar3 != null ? adVar3.h : 0, adVar5 != null ? adVar5.h : 0) + 1;
        int i3 = adVar.h;
        if (adVar4 != null) {
            i2 = adVar4.h;
        }
        adVar2.h = Math.max(i3, i2) + 1;
    }

    private void b(ad<K, V> adVar, boolean z) {
        while (adVar != null) {
            ad<K, V> adVar2 = adVar.f283b;
            ad<K, V> adVar3 = adVar.c;
            int i2 = adVar2 != null ? adVar2.h : 0;
            int i3 = adVar3 != null ? adVar3.h : 0;
            int i4 = i2 - i3;
            if (i4 == -2) {
                ad<K, V> adVar4 = adVar3.f283b;
                ad<K, V> adVar5 = adVar3.c;
                int i5 = (adVar4 != null ? adVar4.h : 0) - (adVar5 != null ? adVar5.h : 0);
                if (i5 == -1 || (i5 == 0 && !z)) {
                    a((ad) adVar);
                } else if (f || i5 == 1) {
                    b((ad) adVar3);
                    a((ad) adVar);
                } else {
                    throw new AssertionError();
                }
                if (z) {
                    return;
                }
            } else if (i4 == 2) {
                ad<K, V> adVar6 = adVar2.f283b;
                ad<K, V> adVar7 = adVar2.c;
                int i6 = (adVar6 != null ? adVar6.h : 0) - (adVar7 != null ? adVar7.h : 0);
                if (i6 == 1 || (i6 == 0 && !z)) {
                    b((ad) adVar);
                } else if (f || i6 == -1) {
                    a((ad) adVar2);
                    b((ad) adVar);
                } else {
                    throw new AssertionError();
                }
                if (z) {
                    return;
                }
            } else if (i4 == 0) {
                adVar.h = i2 + 1;
                if (z) {
                    return;
                }
            } else if (f || i4 == -1 || i4 == 1) {
                adVar.h = Math.max(i2, i3) + 1;
                if (!z) {
                    return;
                }
            } else {
                throw new AssertionError();
            }
            adVar = adVar.f282a;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.w.a(java.lang.Object, boolean):com.a.a.b.ad<K, V>
     arg types: [java.lang.Object, int]
     candidates:
      com.a.a.b.w.a(com.a.a.b.ad, com.a.a.b.ad):void
      com.a.a.b.w.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.w.a(com.a.a.b.ad, boolean):void
      com.a.a.b.w.a(java.lang.Object, boolean):com.a.a.b.ad<K, V> */
    /* access modifiers changed from: package-private */
    public ad<K, V> a(Object obj) {
        if (obj == null) {
            return null;
        }
        try {
            return a(obj, false);
        } catch (ClassCastException e2) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public ad<K, V> a(K k, boolean z) {
        ad<K, V> adVar;
        int i2;
        ad<K, V> adVar2;
        Comparator<? super K> comparator = this.f324a;
        ad<K, V> adVar3 = this.f325b;
        if (adVar3 != null) {
            Comparable comparable = comparator == g ? (Comparable) k : null;
            while (true) {
                int compareTo = comparable != null ? comparable.compareTo(adVar3.f) : comparator.compare(k, adVar3.f);
                if (compareTo == 0) {
                    return adVar3;
                }
                ad<K, V> adVar4 = compareTo < 0 ? adVar3.f283b : adVar3.c;
                if (adVar4 == null) {
                    int i3 = compareTo;
                    adVar = adVar3;
                    i2 = i3;
                    break;
                }
                adVar3 = adVar4;
            }
        } else {
            adVar = adVar3;
            i2 = 0;
        }
        if (!z) {
            return null;
        }
        ad<K, V> adVar5 = this.e;
        if (adVar != null) {
            adVar2 = new ad<>(adVar, k, adVar5, adVar5.e);
            if (i2 < 0) {
                adVar.f283b = adVar2;
            } else {
                adVar.c = adVar2;
            }
            b(adVar, true);
        } else if (comparator != g || (k instanceof Comparable)) {
            adVar2 = new ad<>(adVar, k, adVar5, adVar5.e);
            this.f325b = adVar2;
        } else {
            throw new ClassCastException(k.getClass().getName() + " is not Comparable");
        }
        this.c++;
        this.d++;
        return adVar2;
    }

    /* access modifiers changed from: package-private */
    public ad<K, V> a(Map.Entry<?, ?> entry) {
        ad<K, V> a2 = a(entry.getKey());
        if (a2 != null && a(a2.g, entry.getValue())) {
            return a2;
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.w.a(com.a.a.b.ad, boolean):void
     arg types: [com.a.a.b.ad<K, V>, int]
     candidates:
      com.a.a.b.w.a(com.a.a.b.ad, com.a.a.b.ad):void
      com.a.a.b.w.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.w.a(java.lang.Object, boolean):com.a.a.b.ad<K, V>
      com.a.a.b.w.a(com.a.a.b.ad, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.w.a(com.a.a.b.ad, com.a.a.b.ad):void
     arg types: [com.a.a.b.ad<K, V>, com.a.a.b.ad<K, V>]
     candidates:
      com.a.a.b.w.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.w.a(java.lang.Object, boolean):com.a.a.b.ad<K, V>
      com.a.a.b.w.a(com.a.a.b.ad, boolean):void
      com.a.a.b.w.a(com.a.a.b.ad, com.a.a.b.ad):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.w.a(com.a.a.b.ad, com.a.a.b.ad):void
     arg types: [com.a.a.b.ad<K, V>, ?[OBJECT, ARRAY]]
     candidates:
      com.a.a.b.w.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.w.a(java.lang.Object, boolean):com.a.a.b.ad<K, V>
      com.a.a.b.w.a(com.a.a.b.ad, boolean):void
      com.a.a.b.w.a(com.a.a.b.ad, com.a.a.b.ad):void */
    /* access modifiers changed from: package-private */
    public void a(ad<K, V> adVar, boolean z) {
        int i2;
        int i3 = 0;
        if (z) {
            adVar.e.d = adVar.d;
            adVar.d.e = adVar.e;
        }
        ad<K, V> adVar2 = adVar.f283b;
        ad<K, V> adVar3 = adVar.c;
        ad<K, V> adVar4 = adVar.f282a;
        if (adVar2 == null || adVar3 == null) {
            if (adVar2 != null) {
                a((ad) adVar, (ad) adVar2);
                adVar.f283b = null;
            } else if (adVar3 != null) {
                a((ad) adVar, (ad) adVar3);
                adVar.c = null;
            } else {
                a((ad) adVar, (ad) null);
            }
            b(adVar4, false);
            this.c--;
            this.d++;
            return;
        }
        ad<K, V> b2 = adVar2.h > adVar3.h ? adVar2.b() : adVar3.a();
        a((ad) b2, false);
        ad<K, V> adVar5 = adVar.f283b;
        if (adVar5 != null) {
            i2 = adVar5.h;
            b2.f283b = adVar5;
            adVar5.f282a = b2;
            adVar.f283b = null;
        } else {
            i2 = 0;
        }
        ad<K, V> adVar6 = adVar.c;
        if (adVar6 != null) {
            i3 = adVar6.h;
            b2.c = adVar6;
            adVar6.f282a = b2;
            adVar.c = null;
        }
        b2.h = Math.max(i2, i3) + 1;
        a((ad) adVar, (ad) b2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.w.a(com.a.a.b.ad, boolean):void
     arg types: [com.a.a.b.ad<K, V>, int]
     candidates:
      com.a.a.b.w.a(com.a.a.b.ad, com.a.a.b.ad):void
      com.a.a.b.w.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.w.a(java.lang.Object, boolean):com.a.a.b.ad<K, V>
      com.a.a.b.w.a(com.a.a.b.ad, boolean):void */
    /* access modifiers changed from: package-private */
    public ad<K, V> b(Object obj) {
        ad<K, V> a2 = a(obj);
        if (a2 != null) {
            a((ad) a2, true);
        }
        return a2;
    }

    public void clear() {
        this.f325b = null;
        this.c = 0;
        this.d++;
        ad<K, V> adVar = this.e;
        adVar.e = adVar;
        adVar.d = adVar;
    }

    public boolean containsKey(Object obj) {
        return a(obj) != null;
    }

    public Set<Map.Entry<K, V>> entrySet() {
        w<K, V>.y yVar = this.h;
        if (yVar != null) {
            return yVar;
        }
        y yVar2 = new y(this);
        this.h = yVar2;
        return yVar2;
    }

    public V get(Object obj) {
        ad a2 = a(obj);
        if (a2 != null) {
            return a2.g;
        }
        return null;
    }

    public Set<K> keySet() {
        w<K, V>.aa aaVar = this.i;
        if (aaVar != null) {
            return aaVar;
        }
        aa aaVar2 = new aa(this);
        this.i = aaVar2;
        return aaVar2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.w.a(java.lang.Object, boolean):com.a.a.b.ad<K, V>
     arg types: [K, int]
     candidates:
      com.a.a.b.w.a(com.a.a.b.ad, com.a.a.b.ad):void
      com.a.a.b.w.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.w.a(com.a.a.b.ad, boolean):void
      com.a.a.b.w.a(java.lang.Object, boolean):com.a.a.b.ad<K, V> */
    public V put(K k, V v) {
        if (k == null) {
            throw new NullPointerException("key == null");
        }
        ad a2 = a((Object) k, true);
        V v2 = a2.g;
        a2.g = v;
        return v2;
    }

    public V remove(Object obj) {
        ad b2 = b(obj);
        if (b2 != null) {
            return b2.g;
        }
        return null;
    }

    public int size() {
        return this.c;
    }
}
