package com.a.a.b;

import com.a.a.a;
import com.a.a.a.d;
import com.a.a.a.e;
import com.a.a.af;
import com.a.a.ag;
import com.a.a.b;
import com.a.a.j;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.List;

public final class s implements ag, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    public static final s f318a = new s();

    /* renamed from: b  reason: collision with root package name */
    private double f319b = -1.0d;
    private int c = 136;
    private boolean d = true;
    private boolean e;
    private List<a> f = Collections.emptyList();
    private List<a> g = Collections.emptyList();

    private boolean a(d dVar) {
        return dVar == null || dVar.a() <= this.f319b;
    }

    private boolean a(d dVar, e eVar) {
        return a(dVar) && a(eVar);
    }

    private boolean a(e eVar) {
        return eVar == null || eVar.a() > this.f319b;
    }

    private boolean a(Class<?> cls) {
        return !Enum.class.isAssignableFrom(cls) && (cls.isAnonymousClass() || cls.isLocalClass());
    }

    private boolean b(Class<?> cls) {
        return cls.isMemberClass() && !c(cls);
    }

    private boolean c(Class<?> cls) {
        return (cls.getModifiers() & 8) != 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.s.a(java.lang.Class<?>, boolean):boolean
     arg types: [java.lang.Class<? super T>, int]
     candidates:
      com.a.a.b.s.a(com.a.a.a.d, com.a.a.a.e):boolean
      com.a.a.b.s.a(com.a.a.j, com.a.a.c.a):com.a.a.af<T>
      com.a.a.b.s.a(java.lang.reflect.Field, boolean):boolean
      com.a.a.ag.a(com.a.a.j, com.a.a.c.a):com.a.a.af<T>
      com.a.a.b.s.a(java.lang.Class<?>, boolean):boolean */
    public <T> af<T> a(j jVar, com.a.a.c.a<T> aVar) {
        Class<? super T> rawType = aVar.getRawType();
        boolean a2 = a((Class<?>) rawType, true);
        boolean a3 = a((Class<?>) rawType, false);
        if (a2 || a3) {
            return new t(this, a3, a2, jVar, aVar);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public s clone() {
        try {
            return (s) super.clone();
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError();
        }
    }

    public boolean a(Class<?> cls, boolean z) {
        if (this.f319b != -1.0d && !a((d) cls.getAnnotation(d.class), (e) cls.getAnnotation(e.class))) {
            return true;
        }
        if (!this.d && b(cls)) {
            return true;
        }
        if (a(cls)) {
            return true;
        }
        for (a a2 : z ? this.f : this.g) {
            if (a2.a(cls)) {
                return true;
            }
        }
        return false;
    }

    public boolean a(Field field, boolean z) {
        com.a.a.a.a aVar;
        if ((this.c & field.getModifiers()) != 0) {
            return true;
        }
        if (this.f319b != -1.0d && !a((d) field.getAnnotation(d.class), (e) field.getAnnotation(e.class))) {
            return true;
        }
        if (field.isSynthetic()) {
            return true;
        }
        if (this.e && ((aVar = (com.a.a.a.a) field.getAnnotation(com.a.a.a.a.class)) == null || (!z ? !aVar.b() : !aVar.a()))) {
            return true;
        }
        if (!this.d && b(field.getType())) {
            return true;
        }
        if (a(field.getType())) {
            return true;
        }
        List<a> list = z ? this.f : this.g;
        if (!list.isEmpty()) {
            b bVar = new b(field);
            for (a a2 : list) {
                if (a2.a(bVar)) {
                    return true;
                }
            }
        }
        return false;
    }
}
