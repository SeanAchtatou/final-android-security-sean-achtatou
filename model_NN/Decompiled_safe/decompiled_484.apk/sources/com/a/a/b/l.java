package com.a.a.b;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

class l implements ae<T> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Constructor f309a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ f f310b;

    l(f fVar, Constructor constructor) {
        this.f310b = fVar;
        this.f309a = constructor;
    }

    public T a() {
        try {
            return this.f309a.newInstance(null);
        } catch (InstantiationException e) {
            throw new RuntimeException("Failed to invoke " + this.f309a + " with no args", e);
        } catch (InvocationTargetException e2) {
            throw new RuntimeException("Failed to invoke " + this.f309a + " with no args", e2.getTargetException());
        } catch (IllegalAccessException e3) {
            throw new AssertionError(e3);
        }
    }
}
