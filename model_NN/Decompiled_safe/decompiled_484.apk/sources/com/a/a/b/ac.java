package com.a.a.b;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

abstract class ac<T> implements Iterator<T> {

    /* renamed from: b  reason: collision with root package name */
    ad<K, V> f281b;
    ad<K, V> c;
    int d;
    final /* synthetic */ w e;

    private ac(w wVar) {
        this.e = wVar;
        this.f281b = this.e.e.d;
        this.c = null;
        this.d = this.e.d;
    }

    /* synthetic */ ac(w wVar, x xVar) {
        this(wVar);
    }

    /* access modifiers changed from: package-private */
    public final ad<K, V> b() {
        ad<K, V> adVar = this.f281b;
        if (adVar == this.e.e) {
            throw new NoSuchElementException();
        } else if (this.e.d != this.d) {
            throw new ConcurrentModificationException();
        } else {
            this.f281b = adVar.d;
            this.c = adVar;
            return adVar;
        }
    }

    public final boolean hasNext() {
        return this.f281b != this.e.e;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.w.a(com.a.a.b.ad, boolean):void
     arg types: [com.a.a.b.ad<K, V>, int]
     candidates:
      com.a.a.b.w.a(com.a.a.b.ad, com.a.a.b.ad):void
      com.a.a.b.w.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.w.a(java.lang.Object, boolean):com.a.a.b.ad<K, V>
      com.a.a.b.w.a(com.a.a.b.ad, boolean):void */
    public final void remove() {
        if (this.c == null) {
            throw new IllegalStateException();
        }
        this.e.a((ad) this.c, true);
        this.c = null;
        this.d = this.e.d;
    }
}
