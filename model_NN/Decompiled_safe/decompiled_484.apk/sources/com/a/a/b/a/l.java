package com.a.a.b.a;

import com.a.a.af;
import com.a.a.ag;
import com.a.a.b.b;
import com.a.a.b.f;
import com.a.a.c.a;
import com.a.a.j;
import java.lang.reflect.Type;
import java.util.Map;

public final class l implements ag {

    /* renamed from: a  reason: collision with root package name */
    private final f f258a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final boolean f259b;

    public l(f fVar, boolean z) {
        this.f258a = fVar;
        this.f259b = z;
    }

    private af<?> a(j jVar, Type type) {
        return (type == Boolean.TYPE || type == Boolean.class) ? z.f : jVar.a((a) a.get(type));
    }

    public <T> af<T> a(j jVar, a<T> aVar) {
        Type type = aVar.getType();
        if (!Map.class.isAssignableFrom(aVar.getRawType())) {
            return null;
        }
        Type[] b2 = b.b(type, b.e(type));
        return new m(this, jVar, b2[0], a(jVar, b2[0]), b2[1], jVar.a((a) a.get(b2[1])), this.f258a.a(aVar));
    }
}
