package com.a.a.b;

import com.a.a.ab;
import com.a.a.b.a.z;
import com.a.a.d.a;
import com.a.a.d.d;
import com.a.a.d.e;
import com.a.a.t;
import com.a.a.u;
import com.a.a.v;
import java.io.EOFException;
import java.io.IOException;
import java.io.Writer;

public final class ag {
    public static t a(a aVar) {
        boolean z = true;
        try {
            aVar.f();
            z = false;
            return z.P.b(aVar);
        } catch (EOFException e) {
            if (z) {
                return v.f345a;
            }
            throw new ab(e);
        } catch (e e2) {
            throw new ab(e2);
        } catch (IOException e3) {
            throw new u(e3);
        } catch (NumberFormatException e4) {
            throw new ab(e4);
        }
    }

    public static Writer a(Appendable appendable) {
        return appendable instanceof Writer ? (Writer) appendable : new ai(appendable);
    }

    public static void a(t tVar, d dVar) {
        z.P.a(dVar, tVar);
    }
}
