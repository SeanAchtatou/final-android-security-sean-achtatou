package com.a.a.b;

import com.a.a.u;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.EnumSet;

class n implements ae<T> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Type f312a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ f f313b;

    n(f fVar, Type type) {
        this.f313b = fVar;
        this.f312a = type;
    }

    public T a() {
        if (this.f312a instanceof ParameterizedType) {
            Type type = ((ParameterizedType) this.f312a).getActualTypeArguments()[0];
            if (type instanceof Class) {
                return EnumSet.noneOf((Class) type);
            }
            throw new u("Invalid EnumSet type: " + this.f312a.toString());
        }
        throw new u("Invalid EnumSet type: " + this.f312a.toString());
    }
}
