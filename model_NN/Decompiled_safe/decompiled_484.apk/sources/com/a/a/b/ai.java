package com.a.a.b;

import java.io.Writer;

final class ai extends Writer {

    /* renamed from: a  reason: collision with root package name */
    private final Appendable f286a;

    /* renamed from: b  reason: collision with root package name */
    private final aj f287b;

    private ai(Appendable appendable) {
        this.f287b = new aj();
        this.f286a = appendable;
    }

    public void close() {
    }

    public void flush() {
    }

    public void write(int i) {
        this.f286a.append((char) i);
    }

    public void write(char[] cArr, int i, int i2) {
        this.f287b.f288a = cArr;
        this.f286a.append(this.f287b, i, i + i2);
    }
}
