package com.a.a.b.a;

import com.a.a.ab;
import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;

final class ad extends af<Character> {
    ad() {
    }

    /* renamed from: a */
    public Character b(a aVar) {
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        String h = aVar.h();
        if (h.length() == 1) {
            return Character.valueOf(h.charAt(0));
        }
        throw new ab("Expecting character, got: " + h);
    }

    public void a(d dVar, Character ch) {
        dVar.b(ch == null ? null : String.valueOf(ch));
    }
}
