package com.a.a.b.a;

import com.a.a.af;
import com.a.a.ag;
import com.a.a.c.a;
import com.a.a.j;

final class ax implements ag {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Class f241a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Class f242b;
    final /* synthetic */ af c;

    ax(Class cls, Class cls2, af afVar) {
        this.f241a = cls;
        this.f242b = cls2;
        this.c = afVar;
    }

    public <T> af<T> a(j jVar, a<T> aVar) {
        Class<? super T> rawType = aVar.getRawType();
        if (rawType == this.f241a || rawType == this.f242b) {
            return this.c;
        }
        return null;
    }

    public String toString() {
        return "Factory[type=" + this.f241a.getName() + "+" + this.f242b.getName() + ",adapter=" + this.c + "]";
    }
}
