package com.a.a.b;

import java.util.AbstractSet;
import java.util.Iterator;

final class aa extends AbstractSet<K> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ w f279a;

    aa(w wVar) {
        this.f279a = wVar;
    }

    public void clear() {
        this.f279a.clear();
    }

    public boolean contains(Object obj) {
        return this.f279a.containsKey(obj);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [com.a.a.b.ab, java.util.Iterator<K>] */
    public Iterator<K> iterator() {
        return new ab(this);
    }

    public boolean remove(Object obj) {
        return this.f279a.b(obj) != null;
    }

    public int size() {
        return this.f279a.c;
    }
}
