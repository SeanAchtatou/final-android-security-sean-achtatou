package com.a.a.b.a;

import com.a.a.af;
import com.a.a.ag;
import com.a.a.c.a;
import com.a.a.j;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;

final class b implements ag {
    b() {
    }

    public <T> af<T> a(j jVar, a<T> aVar) {
        Type type = aVar.getType();
        if (!(type instanceof GenericArrayType) && (!(type instanceof Class) || !((Class) type).isArray())) {
            return null;
        }
        Type g = com.a.a.b.b.g(type);
        return new a(jVar, jVar.a((a) a.get(g)), com.a.a.b.b.e(g));
    }
}
