package com.a.a.b;

import java.util.Map;

final class ad<K, V> implements Map.Entry<K, V> {

    /* renamed from: a  reason: collision with root package name */
    ad<K, V> f282a;

    /* renamed from: b  reason: collision with root package name */
    ad<K, V> f283b;
    ad<K, V> c;
    ad<K, V> d;
    ad<K, V> e;
    final K f;
    V g;
    int h;

    ad() {
        this.f = null;
        this.e = this;
        this.d = this;
    }

    ad(ad<K, V> adVar, K k, ad<K, V> adVar2, ad<K, V> adVar3) {
        this.f282a = adVar;
        this.f = k;
        this.h = 1;
        this.d = adVar2;
        this.e = adVar3;
        adVar3.d = this;
        adVar2.e = this;
    }

    public ad<K, V> a() {
        for (ad<K, V> adVar = this.f283b; adVar != null; adVar = adVar.f283b) {
            this = adVar;
        }
        return this;
    }

    public ad<K, V> b() {
        for (ad<K, V> adVar = this.c; adVar != null; adVar = adVar.c) {
            this = adVar;
        }
        return this;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x001b A[ORIG_RETURN, RETURN, SYNTHETIC] */
    public boolean equals(java.lang.Object r4) {
        /*
            r3 = this;
            r0 = 0
            boolean r1 = r4 instanceof java.util.Map.Entry
            if (r1 == 0) goto L_0x001c
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4
            K r1 = r3.f
            if (r1 != 0) goto L_0x001d
            java.lang.Object r1 = r4.getKey()
            if (r1 != 0) goto L_0x001c
        L_0x0011:
            V r1 = r3.g
            if (r1 != 0) goto L_0x002a
            java.lang.Object r1 = r4.getValue()
            if (r1 != 0) goto L_0x001c
        L_0x001b:
            r0 = 1
        L_0x001c:
            return r0
        L_0x001d:
            K r1 = r3.f
            java.lang.Object r2 = r4.getKey()
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x001c
            goto L_0x0011
        L_0x002a:
            V r1 = r3.g
            java.lang.Object r2 = r4.getValue()
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x001c
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.b.ad.equals(java.lang.Object):boolean");
    }

    public K getKey() {
        return this.f;
    }

    public V getValue() {
        return this.g;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = this.f == null ? 0 : this.f.hashCode();
        if (this.g != null) {
            i = this.g.hashCode();
        }
        return hashCode ^ i;
    }

    public V setValue(V v) {
        V v2 = this.g;
        this.g = v;
        return v2;
    }

    public String toString() {
        return ((Object) this.f) + "=" + ((Object) this.g);
    }
}
