package com.a.a.b.a;

import com.a.a.af;
import com.a.a.c.a;
import com.a.a.d.d;
import com.a.a.j;
import java.lang.reflect.Field;

class r extends t {

    /* renamed from: a  reason: collision with root package name */
    final af<?> f267a = this.f.a(this.f268b, this.c, this.d);

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ j f268b;
    final /* synthetic */ Field c;
    final /* synthetic */ a d;
    final /* synthetic */ boolean e;
    final /* synthetic */ q f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    r(q qVar, String str, boolean z, boolean z2, j jVar, Field field, a aVar, boolean z3) {
        super(str, z, z2);
        this.f = qVar;
        this.f268b = jVar;
        this.c = field;
        this.d = aVar;
        this.e = z3;
    }

    /* access modifiers changed from: package-private */
    public void a(com.a.a.d.a aVar, Object obj) {
        Object b2 = this.f267a.b(aVar);
        if (b2 != null || !this.e) {
            this.c.set(obj, b2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(d dVar, Object obj) {
        new y(this.f268b, this.f267a, this.d.getType()).a(dVar, this.c.get(obj));
    }
}
