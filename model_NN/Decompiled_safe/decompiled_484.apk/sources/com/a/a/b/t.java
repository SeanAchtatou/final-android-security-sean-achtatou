package com.a.a.b;

import com.a.a.af;
import com.a.a.c.a;
import com.a.a.d.d;
import com.a.a.j;

class t extends af<T> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f320a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ boolean f321b;
    final /* synthetic */ j c;
    final /* synthetic */ a d;
    final /* synthetic */ s e;
    private af<T> f;

    t(s sVar, boolean z, boolean z2, j jVar, a aVar) {
        this.e = sVar;
        this.f320a = z;
        this.f321b = z2;
        this.c = jVar;
        this.d = aVar;
    }

    private af<T> a() {
        af<T> afVar = this.f;
        if (afVar != null) {
            return afVar;
        }
        af<T> a2 = this.c.a(this.e, this.d);
        this.f = a2;
        return a2;
    }

    public void a(d dVar, T t) {
        if (this.f321b) {
            dVar.f();
        } else {
            a().a(dVar, t);
        }
    }

    public T b(com.a.a.d.a aVar) {
        if (!this.f320a) {
            return a().b(aVar);
        }
        aVar.n();
        return null;
    }
}
