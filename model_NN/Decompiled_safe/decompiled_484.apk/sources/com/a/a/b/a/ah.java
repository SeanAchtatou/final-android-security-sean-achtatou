package com.a.a.b.a;

import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;

final class ah extends af<StringBuilder> {
    ah() {
    }

    /* renamed from: a */
    public StringBuilder b(a aVar) {
        if (aVar.f() != c.NULL) {
            return new StringBuilder(aVar.h());
        }
        aVar.j();
        return null;
    }

    public void a(d dVar, StringBuilder sb) {
        dVar.b(sb == null ? null : sb.toString());
    }
}
