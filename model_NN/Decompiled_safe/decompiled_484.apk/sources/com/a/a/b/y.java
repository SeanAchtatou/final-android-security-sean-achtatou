package com.a.a.b;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;

class y extends AbstractSet<Map.Entry<K, V>> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ w f326a;

    y(w wVar) {
        this.f326a = wVar;
    }

    public void clear() {
        this.f326a.clear();
    }

    public boolean contains(Object obj) {
        return (obj instanceof Map.Entry) && this.f326a.a((Map.Entry) obj) != null;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.util.Iterator<java.util.Map$Entry<K, V>>, com.a.a.b.z] */
    public Iterator<Map.Entry<K, V>> iterator() {
        return new z(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.w.a(com.a.a.b.ad, boolean):void
     arg types: [com.a.a.b.ad, int]
     candidates:
      com.a.a.b.w.a(com.a.a.b.ad, com.a.a.b.ad):void
      com.a.a.b.w.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.w.a(java.lang.Object, boolean):com.a.a.b.ad<K, V>
      com.a.a.b.w.a(com.a.a.b.ad, boolean):void */
    public boolean remove(Object obj) {
        ad a2;
        if (!(obj instanceof Map.Entry) || (a2 = this.f326a.a((Map.Entry<?, ?>) ((Map.Entry) obj))) == null) {
            return false;
        }
        this.f326a.a(a2, true);
        return true;
    }

    public int size() {
        return this.f326a.c;
    }
}
