package com.a.a.b.a;

import com.a.a.af;
import com.a.a.ag;
import com.a.a.c.a;
import com.a.a.j;

final class av implements ag {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Class f239a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Class f240b;
    final /* synthetic */ af c;

    av(Class cls, Class cls2, af afVar) {
        this.f239a = cls;
        this.f240b = cls2;
        this.c = afVar;
    }

    public <T> af<T> a(j jVar, a<T> aVar) {
        Class<? super T> rawType = aVar.getRawType();
        if (rawType == this.f239a || rawType == this.f240b) {
            return this.c;
        }
        return null;
    }

    public String toString() {
        return "Factory[type=" + this.f240b.getName() + "+" + this.f239a.getName() + ",adapter=" + this.c + "]";
    }
}
