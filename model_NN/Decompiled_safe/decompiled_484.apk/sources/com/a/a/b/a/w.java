package com.a.a.b.a;

import com.a.a.ab;
import com.a.a.af;
import com.a.a.ag;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class w extends af<Time> {

    /* renamed from: a  reason: collision with root package name */
    public static final ag f273a = new x();

    /* renamed from: b  reason: collision with root package name */
    private final DateFormat f274b = new SimpleDateFormat("hh:mm:ss a");

    /* renamed from: a */
    public synchronized Time b(a aVar) {
        Time time;
        if (aVar.f() == c.NULL) {
            aVar.j();
            time = null;
        } else {
            try {
                time = new Time(this.f274b.parse(aVar.h()).getTime());
            } catch (ParseException e) {
                throw new ab(e);
            }
        }
        return time;
    }

    public synchronized void a(d dVar, Time time) {
        dVar.b(time == null ? null : this.f274b.format(time));
    }
}
