package com.a.a.b.a;

import com.a.a.ab;
import com.a.a.af;
import com.a.a.b.v;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;

final class ac extends af<Number> {
    ac() {
    }

    /* renamed from: a */
    public Number b(a aVar) {
        c f = aVar.f();
        switch (az.f245a[f.ordinal()]) {
            case 1:
                return new v(aVar.h());
            case 2:
            case 3:
            default:
                throw new ab("Expecting number, got: " + f);
            case 4:
                aVar.j();
                return null;
        }
    }

    public void a(d dVar, Number number) {
        dVar.a(number);
    }
}
