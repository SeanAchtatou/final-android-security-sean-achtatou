package com.a.a.b.a;

import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.r;
import com.a.a.t;
import com.a.a.v;
import com.a.a.w;
import com.a.a.z;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class h extends a {

    /* renamed from: a  reason: collision with root package name */
    private static final Reader f254a = new i();

    /* renamed from: b  reason: collision with root package name */
    private static final Object f255b = new Object();
    private final List<Object> c = new ArrayList();

    public h(t tVar) {
        super(f254a);
        this.c.add(tVar);
    }

    private void a(c cVar) {
        if (f() != cVar) {
            throw new IllegalStateException("Expected " + cVar + " but was " + f());
        }
    }

    private Object r() {
        return this.c.get(this.c.size() - 1);
    }

    private Object s() {
        return this.c.remove(this.c.size() - 1);
    }

    public void a() {
        a(c.BEGIN_ARRAY);
        this.c.add(((r) r()).iterator());
    }

    public void b() {
        a(c.END_ARRAY);
        s();
        s();
    }

    public void c() {
        a(c.BEGIN_OBJECT);
        this.c.add(((w) r()).o().iterator());
    }

    public void close() {
        this.c.clear();
        this.c.add(f255b);
    }

    public void d() {
        a(c.END_OBJECT);
        s();
        s();
    }

    public boolean e() {
        c f = f();
        return (f == c.END_OBJECT || f == c.END_ARRAY) ? false : true;
    }

    public c f() {
        if (this.c.isEmpty()) {
            return c.END_DOCUMENT;
        }
        Object r = r();
        if (r instanceof Iterator) {
            boolean z = this.c.get(this.c.size() - 2) instanceof w;
            Iterator it = (Iterator) r;
            if (!it.hasNext()) {
                return z ? c.END_OBJECT : c.END_ARRAY;
            }
            if (z) {
                return c.NAME;
            }
            this.c.add(it.next());
            return f();
        } else if (r instanceof w) {
            return c.BEGIN_OBJECT;
        } else {
            if (r instanceof r) {
                return c.BEGIN_ARRAY;
            }
            if (r instanceof z) {
                z zVar = (z) r;
                if (zVar.q()) {
                    return c.STRING;
                }
                if (zVar.o()) {
                    return c.BOOLEAN;
                }
                if (zVar.p()) {
                    return c.NUMBER;
                }
                throw new AssertionError();
            } else if (r instanceof v) {
                return c.NULL;
            } else {
                if (r == f255b) {
                    throw new IllegalStateException("JsonReader is closed");
                }
                throw new AssertionError();
            }
        }
    }

    public String g() {
        a(c.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) r()).next();
        this.c.add(entry.getValue());
        return (String) entry.getKey();
    }

    public String h() {
        c f = f();
        if (f == c.STRING || f == c.NUMBER) {
            return ((z) s()).b();
        }
        throw new IllegalStateException("Expected " + c.STRING + " but was " + f);
    }

    public boolean i() {
        a(c.BOOLEAN);
        return ((z) s()).f();
    }

    public void j() {
        a(c.NULL);
        s();
    }

    public double k() {
        c f = f();
        if (f == c.NUMBER || f == c.STRING) {
            double c2 = ((z) r()).c();
            if (p() || (!Double.isNaN(c2) && !Double.isInfinite(c2))) {
                s();
                return c2;
            }
            throw new NumberFormatException("JSON forbids NaN and infinities: " + c2);
        }
        throw new IllegalStateException("Expected " + c.NUMBER + " but was " + f);
    }

    public long l() {
        c f = f();
        if (f == c.NUMBER || f == c.STRING) {
            long d = ((z) r()).d();
            s();
            return d;
        }
        throw new IllegalStateException("Expected " + c.NUMBER + " but was " + f);
    }

    public int m() {
        c f = f();
        if (f == c.NUMBER || f == c.STRING) {
            int e = ((z) r()).e();
            s();
            return e;
        }
        throw new IllegalStateException("Expected " + c.NUMBER + " but was " + f);
    }

    public void n() {
        if (f() == c.NAME) {
            g();
        } else {
            s();
        }
    }

    public void o() {
        a(c.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) r()).next();
        this.c.add(entry.getValue());
        this.c.add(new z((String) entry.getKey()));
    }

    public String toString() {
        return getClass().getSimpleName();
    }
}
