package com.a.a.b.a;

import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;

final class aw extends af<Boolean> {
    aw() {
    }

    /* renamed from: a */
    public Boolean b(a aVar) {
        if (aVar.f() != c.NULL) {
            return aVar.f() == c.STRING ? Boolean.valueOf(Boolean.parseBoolean(aVar.h())) : Boolean.valueOf(aVar.i());
        }
        aVar.j();
        return null;
    }

    public void a(d dVar, Boolean bool) {
        if (bool == null) {
            dVar.f();
        } else {
            dVar.a(bool.booleanValue());
        }
    }
}
