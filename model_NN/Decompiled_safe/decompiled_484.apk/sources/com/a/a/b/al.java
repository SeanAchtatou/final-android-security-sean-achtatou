package com.a.a.b;

import java.lang.reflect.Method;

final class al extends ak {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Method f289a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Object f290b;

    al(Method method, Object obj) {
        this.f289a = method;
        this.f290b = obj;
    }

    public <T> T a(Class<T> cls) {
        return this.f289a.invoke(this.f290b, cls);
    }
}
