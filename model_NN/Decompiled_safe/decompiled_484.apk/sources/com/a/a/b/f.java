package com.a.a.b;

import com.a.a.c.a;
import com.a.a.q;
import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private final Map<Type, q<?>> f300a;

    public f(Map<Type, q<?>> map) {
        this.f300a = map;
    }

    private <T> ae<T> a(Class cls) {
        try {
            Constructor declaredConstructor = cls.getDeclaredConstructor(new Class[0]);
            if (!declaredConstructor.isAccessible()) {
                declaredConstructor.setAccessible(true);
            }
            return new l(this, declaredConstructor);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    private <T> ae<T> a(Type type, Class<? super T> cls) {
        if (Collection.class.isAssignableFrom(cls)) {
            return SortedSet.class.isAssignableFrom(cls) ? new m(this) : EnumSet.class.isAssignableFrom(cls) ? new n(this, type) : Set.class.isAssignableFrom(cls) ? new o(this) : Queue.class.isAssignableFrom(cls) ? new p(this) : new q(this);
        }
        if (Map.class.isAssignableFrom(cls)) {
            return SortedMap.class.isAssignableFrom(cls) ? new r(this) : (!(type instanceof ParameterizedType) || String.class.isAssignableFrom(a.get(((ParameterizedType) type).getActualTypeArguments()[0]).getRawType())) ? new i(this) : new h(this);
        }
        return null;
    }

    private <T> ae<T> b(Type type, Class<? super T> cls) {
        return new j(this, cls, type);
    }

    public <T> ae<T> a(a aVar) {
        Type type = aVar.getType();
        Class rawType = aVar.getRawType();
        q qVar = this.f300a.get(type);
        if (qVar != null) {
            return new g(this, qVar, type);
        }
        q qVar2 = this.f300a.get(rawType);
        if (qVar2 != null) {
            return new k(this, qVar2, type);
        }
        ae<T> a2 = a(rawType);
        if (a2 != null) {
            return a2;
        }
        ae<T> a3 = a(type, rawType);
        return a3 == null ? b(type, rawType) : a3;
    }

    public String toString() {
        return this.f300a.toString();
    }
}
