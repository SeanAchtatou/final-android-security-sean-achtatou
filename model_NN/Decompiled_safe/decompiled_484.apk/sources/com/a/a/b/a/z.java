package com.a.a.b.a;

import com.a.a.af;
import com.a.a.ag;
import com.a.a.t;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URI;
import java.net.URL;
import java.util.BitSet;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.UUID;

public final class z {
    public static final af<StringBuffer> A = new ai();
    public static final ag B = a(StringBuffer.class, A);
    public static final af<URL> C = new aj();
    public static final ag D = a(URL.class, C);
    public static final af<URI> E = new ak();
    public static final ag F = a(URI.class, E);
    public static final af<InetAddress> G = new am();
    public static final ag H = b(InetAddress.class, G);
    public static final af<UUID> I = new an();
    public static final ag J = a(UUID.class, I);
    public static final ag K = new ao();
    public static final af<Calendar> L = new aq();
    public static final ag M = b(Calendar.class, GregorianCalendar.class, L);
    public static final af<Locale> N = new ar();
    public static final ag O = a(Locale.class, N);
    public static final af<t> P = new as();
    public static final ag Q = b(t.class, P);
    public static final ag R = a();

    /* renamed from: a  reason: collision with root package name */
    public static final af<Class> f277a = new aa();

    /* renamed from: b  reason: collision with root package name */
    public static final ag f278b = a(Class.class, f277a);
    public static final af<BitSet> c = new al();
    public static final ag d = a(BitSet.class, c);
    public static final af<Boolean> e = new aw();
    public static final af<Boolean> f = new ba();
    public static final ag g = a(Boolean.TYPE, Boolean.class, e);
    public static final af<Number> h = new bb();
    public static final ag i = a(Byte.TYPE, Byte.class, h);
    public static final af<Number> j = new bc();
    public static final ag k = a(Short.TYPE, Short.class, j);
    public static final af<Number> l = new bd();
    public static final ag m = a(Integer.TYPE, Integer.class, l);
    public static final af<Number> n = new be();
    public static final af<Number> o = new bf();
    public static final af<Number> p = new ab();
    public static final af<Number> q = new ac();
    public static final ag r = a(Number.class, q);
    public static final af<Character> s = new ad();
    public static final ag t = a(Character.TYPE, Character.class, s);
    public static final af<String> u = new ae();
    public static final af<BigDecimal> v = new af();
    public static final af<BigInteger> w = new ag();
    public static final ag x = a(String.class, u);
    public static final af<StringBuilder> y = new ah();
    public static final ag z = a(StringBuilder.class, y);

    public static ag a() {
        return new at();
    }

    public static <TT> ag a(Class<TT> cls, af<TT> afVar) {
        return new au(cls, afVar);
    }

    public static <TT> ag a(Class<TT> cls, Class<TT> cls2, af<? super TT> afVar) {
        return new av(cls, cls2, afVar);
    }

    public static <TT> ag b(Class<TT> cls, af<TT> afVar) {
        return new ay(cls, afVar);
    }

    public static <TT> ag b(Class<TT> cls, Class<? extends TT> cls2, af<? super TT> afVar) {
        return new ax(cls, cls2, afVar);
    }
}
