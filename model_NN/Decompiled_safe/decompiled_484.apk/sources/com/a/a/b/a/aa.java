package com.a.a.b.a;

import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;

final class aa extends af<Class> {
    aa() {
    }

    /* renamed from: a */
    public Class b(a aVar) {
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        throw new UnsupportedOperationException("Attempted to deserialize a java.lang.Class. Forgot to register a type adapter?");
    }

    public void a(d dVar, Class cls) {
        if (cls == null) {
            dVar.f();
            return;
        }
        throw new UnsupportedOperationException("Attempted to serialize java.lang.Class: " + cls.getName() + ". Forgot to register a type adapter?");
    }
}
