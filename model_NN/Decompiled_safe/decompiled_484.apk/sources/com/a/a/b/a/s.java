package com.a.a.b.a;

import com.a.a.ab;
import com.a.a.af;
import com.a.a.b.ae;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;
import java.util.Map;

public final class s<T> extends af<T> {

    /* renamed from: a  reason: collision with root package name */
    private final ae<T> f269a;

    /* renamed from: b  reason: collision with root package name */
    private final Map<String, t> f270b;

    private s(ae<T> aeVar, Map<String, t> map) {
        this.f269a = aeVar;
        this.f270b = map;
    }

    /* synthetic */ s(ae aeVar, Map map, r rVar) {
        this(aeVar, map);
    }

    public void a(d dVar, T t) {
        if (t == null) {
            dVar.f();
            return;
        }
        dVar.d();
        try {
            for (t next : this.f270b.values()) {
                if (next.h) {
                    dVar.a(next.g);
                    next.a(dVar, t);
                }
            }
            dVar.e();
        } catch (IllegalAccessException e) {
            throw new AssertionError();
        }
    }

    public T b(a aVar) {
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        T a2 = this.f269a.a();
        try {
            aVar.c();
            while (aVar.e()) {
                t tVar = this.f270b.get(aVar.g());
                if (tVar == null || !tVar.i) {
                    aVar.n();
                } else {
                    tVar.a(aVar, a2);
                }
            }
            aVar.d();
            return a2;
        } catch (IllegalStateException e) {
            throw new ab(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        }
    }
}
