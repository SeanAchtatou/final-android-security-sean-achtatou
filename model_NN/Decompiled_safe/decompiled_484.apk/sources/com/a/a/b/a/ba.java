package com.a.a.b.a;

import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;

final class ba extends af<Boolean> {
    ba() {
    }

    /* renamed from: a */
    public Boolean b(a aVar) {
        if (aVar.f() != c.NULL) {
            return Boolean.valueOf(aVar.h());
        }
        aVar.j();
        return null;
    }

    public void a(d dVar, Boolean bool) {
        dVar.b(bool == null ? "null" : bool.toString());
    }
}
