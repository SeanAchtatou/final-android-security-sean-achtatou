package com.a.a.b.a;

import com.a.a.ab;
import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;
import java.util.BitSet;

final class al extends af<BitSet> {
    al() {
    }

    /* renamed from: a */
    public BitSet b(a aVar) {
        boolean z;
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        BitSet bitSet = new BitSet();
        aVar.a();
        c f = aVar.f();
        int i = 0;
        while (f != c.END_ARRAY) {
            switch (f) {
                case NUMBER:
                    if (aVar.m() == 0) {
                        z = false;
                        break;
                    } else {
                        z = true;
                        break;
                    }
                case BOOLEAN:
                    z = aVar.i();
                    break;
                case STRING:
                    String h = aVar.h();
                    try {
                        if (Integer.parseInt(h) == 0) {
                            z = false;
                            break;
                        } else {
                            z = true;
                            break;
                        }
                    } catch (NumberFormatException e) {
                        throw new ab("Error: Expecting: bitset number value (1, 0), Found: " + h);
                    }
                default:
                    throw new ab("Invalid bitset value type: " + f);
            }
            if (z) {
                bitSet.set(i);
            }
            i++;
            f = aVar.f();
        }
        aVar.b();
        return bitSet;
    }

    public void a(d dVar, BitSet bitSet) {
        if (bitSet == null) {
            dVar.f();
            return;
        }
        dVar.b();
        for (int i = 0; i < bitSet.length(); i++) {
            dVar.a((long) (bitSet.get(i) ? 1 : 0));
        }
        dVar.c();
    }
}
