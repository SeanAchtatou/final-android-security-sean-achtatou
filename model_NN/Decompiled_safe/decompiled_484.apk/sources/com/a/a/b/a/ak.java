package com.a.a.b.a;

import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;
import com.a.a.u;
import java.net.URI;
import java.net.URISyntaxException;

final class ak extends af<URI> {
    ak() {
    }

    /* renamed from: a */
    public URI b(a aVar) {
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        try {
            String h = aVar.h();
            if (!"null".equals(h)) {
                return new URI(h);
            }
            return null;
        } catch (URISyntaxException e) {
            throw new u(e);
        }
    }

    public void a(d dVar, URI uri) {
        dVar.b(uri == null ? null : uri.toASCIIString());
    }
}
