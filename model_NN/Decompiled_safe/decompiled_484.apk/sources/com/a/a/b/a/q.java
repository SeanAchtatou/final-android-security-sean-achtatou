package com.a.a.b.a;

import com.a.a.a.b;
import com.a.a.a.c;
import com.a.a.af;
import com.a.a.ag;
import com.a.a.b.f;
import com.a.a.b.s;
import com.a.a.c.a;
import com.a.a.i;
import com.a.a.j;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;

public final class q implements ag {

    /* renamed from: a  reason: collision with root package name */
    private final f f265a;

    /* renamed from: b  reason: collision with root package name */
    private final i f266b;
    private final s c;

    public q(f fVar, i iVar, s sVar) {
        this.f265a = fVar;
        this.f266b = iVar;
        this.c = sVar;
    }

    /* access modifiers changed from: private */
    public af<?> a(j jVar, Field field, a<?> aVar) {
        af<?> a2;
        b bVar = (b) field.getAnnotation(b.class);
        return (bVar == null || (a2 = g.a(this.f265a, jVar, aVar, bVar)) == null) ? jVar.a((a) aVar) : a2;
    }

    private t a(j jVar, Field field, String str, a<?> aVar, boolean z, boolean z2) {
        return new r(this, str, z, z2, jVar, field, aVar, com.a.a.b.af.a((Type) aVar.getRawType()));
    }

    private String a(Field field) {
        c cVar = (c) field.getAnnotation(c.class);
        return cVar == null ? this.f266b.a(field) : cVar.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.a.q.a(java.lang.reflect.Field, boolean):boolean
     arg types: [java.lang.reflect.Field, int]
     candidates:
      com.a.a.b.a.q.a(com.a.a.j, com.a.a.c.a):com.a.a.af<T>
      com.a.a.ag.a(com.a.a.j, com.a.a.c.a):com.a.a.af<T>
      com.a.a.b.a.q.a(java.lang.reflect.Field, boolean):boolean */
    private Map<String, t> a(j jVar, a<?> aVar, Class<?> cls) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (cls.isInterface()) {
            return linkedHashMap;
        }
        Type type = aVar.getType();
        while (cls != Object.class) {
            for (Field field : cls.getDeclaredFields()) {
                boolean a2 = a(field, true);
                boolean a3 = a(field, false);
                if (a2 || a3) {
                    field.setAccessible(true);
                    t a4 = a(jVar, field, a(field), a.get(com.a.a.b.b.a(aVar.getType(), cls, field.getGenericType())), a2, a3);
                    t tVar = (t) linkedHashMap.put(a4.g, a4);
                    if (tVar != null) {
                        throw new IllegalArgumentException(type + " declares multiple JSON fields named " + tVar.g);
                    }
                }
            }
            aVar = a.get(com.a.a.b.b.a(aVar.getType(), cls, cls.getGenericSuperclass()));
            cls = aVar.getRawType();
        }
        return linkedHashMap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.a.q.a(com.a.a.j, com.a.a.c.a<?>, java.lang.Class<?>):java.util.Map<java.lang.String, com.a.a.b.a.t>
     arg types: [com.a.a.j, com.a.a.c.a<T>, java.lang.Class<? super T>]
     candidates:
      com.a.a.b.a.q.a(com.a.a.j, java.lang.reflect.Field, com.a.a.c.a<?>):com.a.a.af<?>
      com.a.a.b.a.q.a(com.a.a.j, com.a.a.c.a<?>, java.lang.Class<?>):java.util.Map<java.lang.String, com.a.a.b.a.t> */
    public <T> af<T> a(j jVar, a<T> aVar) {
        Class<? super T> rawType = aVar.getRawType();
        if (!Object.class.isAssignableFrom(rawType)) {
            return null;
        }
        return new s(this.f265a.a(aVar), a(jVar, (a<?>) aVar, (Class<?>) rawType), null);
    }

    public boolean a(Field field, boolean z) {
        return !this.c.a(field.getType(), z) && !this.c.a(field, z);
    }
}
