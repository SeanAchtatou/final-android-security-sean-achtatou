package com.a.a.b.a;

import com.a.a.a.b;
import com.a.a.af;
import com.a.a.ag;
import com.a.a.b.f;
import com.a.a.c.a;
import com.a.a.j;

public final class g implements ag {

    /* renamed from: a  reason: collision with root package name */
    private final f f253a;

    public g(f fVar) {
        this.f253a = fVar;
    }

    static af<?> a(f fVar, j jVar, a<?> aVar, b bVar) {
        Class<?> a2 = bVar.a();
        if (af.class.isAssignableFrom(a2)) {
            return (af) fVar.a(a.get((Class) a2)).a();
        }
        if (ag.class.isAssignableFrom(a2)) {
            return ((ag) fVar.a(a.get((Class) a2)).a()).a(jVar, aVar);
        }
        throw new IllegalArgumentException("@JsonAdapter value must be TypeAdapter or TypeAdapterFactory reference.");
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [com.a.a.c.a, com.a.a.c.a<T>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> com.a.a.af<T> a(com.a.a.j r3, com.a.a.c.a<T> r4) {
        /*
            r2 = this;
            java.lang.Class r0 = r4.getRawType()
            java.lang.Class<com.a.a.a.b> r1 = com.a.a.a.b.class
            java.lang.annotation.Annotation r0 = r0.getAnnotation(r1)
            com.a.a.a.b r0 = (com.a.a.a.b) r0
            if (r0 != 0) goto L_0x0010
            r0 = 0
        L_0x000f:
            return r0
        L_0x0010:
            com.a.a.b.f r1 = r2.f253a
            com.a.a.af r0 = a(r1, r3, r4, r0)
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.b.a.g.a(com.a.a.j, com.a.a.c.a):com.a.a.af");
    }
}
