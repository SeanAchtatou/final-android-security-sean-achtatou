package com.a.a.b.a;

import com.a.a.a.c;
import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.d;
import java.lang.Enum;
import java.util.HashMap;
import java.util.Map;

final class bg<T extends Enum<T>> extends af<T> {

    /* renamed from: a  reason: collision with root package name */
    private final Map<String, T> f246a = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    private final Map<T, String> f247b = new HashMap();

    public bg(Class<T> cls) {
        try {
            for (Enum enumR : (Enum[]) cls.getEnumConstants()) {
                String name = enumR.name();
                c cVar = (c) cls.getField(name).getAnnotation(c.class);
                String a2 = cVar != null ? cVar.a() : name;
                this.f246a.put(a2, enumR);
                this.f247b.put(enumR, a2);
            }
        } catch (NoSuchFieldException e) {
            throw new AssertionError();
        }
    }

    /* renamed from: a */
    public T b(a aVar) {
        if (aVar.f() != com.a.a.d.c.NULL) {
            return (Enum) this.f246a.get(aVar.h());
        }
        aVar.j();
        return null;
    }

    public void a(d dVar, T t) {
        dVar.b(t == null ? null : this.f247b.get(t));
    }
}
