package com.a.a.b.a;

import com.a.a.ab;
import com.a.a.af;
import com.a.a.ag;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public final class e extends af<Date> {

    /* renamed from: a  reason: collision with root package name */
    public static final ag f251a = new f();

    /* renamed from: b  reason: collision with root package name */
    private final DateFormat f252b = DateFormat.getDateTimeInstance(2, 2, Locale.US);
    private final DateFormat c = DateFormat.getDateTimeInstance(2, 2);
    private final DateFormat d = a();

    private static DateFormat a() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return simpleDateFormat;
    }

    private synchronized Date a(String str) {
        Date parse;
        try {
            parse = this.c.parse(str);
        } catch (ParseException e) {
            try {
                parse = this.f252b.parse(str);
            } catch (ParseException e2) {
                try {
                    parse = this.d.parse(str);
                } catch (ParseException e3) {
                    throw new ab(str, e3);
                }
            }
        }
        return parse;
    }

    /* renamed from: a */
    public Date b(a aVar) {
        if (aVar.f() != c.NULL) {
            return a(aVar.h());
        }
        aVar.j();
        return null;
    }

    public synchronized void a(d dVar, Date date) {
        if (date == null) {
            dVar.f();
        } else {
            dVar.b(this.f252b.format(date));
        }
    }
}
