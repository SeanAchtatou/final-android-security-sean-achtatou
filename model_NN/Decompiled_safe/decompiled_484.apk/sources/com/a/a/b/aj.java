package com.a.a.b;

class aj implements CharSequence {

    /* renamed from: a  reason: collision with root package name */
    char[] f288a;

    aj() {
    }

    public char charAt(int i) {
        return this.f288a[i];
    }

    public int length() {
        return this.f288a.length;
    }

    public CharSequence subSequence(int i, int i2) {
        return new String(this.f288a, i, i2 - i);
    }
}
