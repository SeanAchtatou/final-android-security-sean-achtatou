package com.a.a.b.a;

import com.a.a.ab;
import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;
import java.math.BigInteger;

final class ag extends af<BigInteger> {
    ag() {
    }

    /* renamed from: a */
    public BigInteger b(a aVar) {
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        try {
            return new BigInteger(aVar.h());
        } catch (NumberFormatException e) {
            throw new ab(e);
        }
    }

    public void a(d dVar, BigInteger bigInteger) {
        dVar.a(bigInteger);
    }
}
