package com.a.a.b.a;

import com.a.a.ab;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;
import java.math.BigDecimal;

final class af extends com.a.a.af<BigDecimal> {
    af() {
    }

    /* renamed from: a */
    public BigDecimal b(a aVar) {
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        try {
            return new BigDecimal(aVar.h());
        } catch (NumberFormatException e) {
            throw new ab(e);
        }
    }

    public void a(d dVar, BigDecimal bigDecimal) {
        dVar.a(bigDecimal);
    }
}
