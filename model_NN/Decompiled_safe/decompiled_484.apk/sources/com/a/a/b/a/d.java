package com.a.a.b.a;

import com.a.a.af;
import com.a.a.b.ae;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.j;
import java.lang.reflect.Type;
import java.util.Collection;

final class d<E> extends af<Collection<E>> {

    /* renamed from: a  reason: collision with root package name */
    private final af<E> f249a;

    /* renamed from: b  reason: collision with root package name */
    private final ae<? extends Collection<E>> f250b;

    public d(j jVar, Type type, af<E> afVar, ae<? extends Collection<E>> aeVar) {
        this.f249a = new y(jVar, afVar, type);
        this.f250b = aeVar;
    }

    /* renamed from: a */
    public Collection<E> b(a aVar) {
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        Collection<E> collection = (Collection) this.f250b.a();
        aVar.a();
        while (aVar.e()) {
            collection.add(this.f249a.b(aVar));
        }
        aVar.b();
        return collection;
    }

    public void a(com.a.a.d.d dVar, Collection<E> collection) {
        if (collection == null) {
            dVar.f();
            return;
        }
        dVar.b();
        for (E a2 : collection) {
            this.f249a.a(dVar, a2);
        }
        dVar.c();
    }
}
