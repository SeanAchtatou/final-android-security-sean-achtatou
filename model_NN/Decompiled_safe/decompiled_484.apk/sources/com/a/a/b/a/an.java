package com.a.a.b.a;

import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;
import java.util.UUID;

final class an extends af<UUID> {
    an() {
    }

    /* renamed from: a */
    public UUID b(a aVar) {
        if (aVar.f() != c.NULL) {
            return UUID.fromString(aVar.h());
        }
        aVar.j();
        return null;
    }

    public void a(d dVar, UUID uuid) {
        dVar.b(uuid == null ? null : uuid.toString());
    }
}
