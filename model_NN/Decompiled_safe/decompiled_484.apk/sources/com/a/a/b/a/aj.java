package com.a.a.b.a;

import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;
import java.net.URL;

final class aj extends af<URL> {
    aj() {
    }

    /* renamed from: a */
    public URL b(a aVar) {
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        String h = aVar.h();
        if (!"null".equals(h)) {
            return new URL(h);
        }
        return null;
    }

    public void a(d dVar, URL url) {
        dVar.b(url == null ? null : url.toExternalForm());
    }
}
