package com.a.a.b.a;

import com.a.a.af;
import com.a.a.ag;
import com.a.a.c.a;
import com.a.a.j;

final class ay implements ag {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Class f243a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ af f244b;

    ay(Class cls, af afVar) {
        this.f243a = cls;
        this.f244b = afVar;
    }

    public <T> af<T> a(j jVar, a<T> aVar) {
        if (this.f243a.isAssignableFrom(aVar.getRawType())) {
            return this.f244b;
        }
        return null;
    }

    public String toString() {
        return "Factory[typeHierarchy=" + this.f243a.getName() + ",adapter=" + this.f244b + "]";
    }
}
