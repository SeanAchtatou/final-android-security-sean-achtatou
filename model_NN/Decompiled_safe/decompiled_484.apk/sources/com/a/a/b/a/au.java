package com.a.a.b.a;

import com.a.a.af;
import com.a.a.ag;
import com.a.a.c.a;
import com.a.a.j;

final class au implements ag {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Class f237a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ af f238b;

    au(Class cls, af afVar) {
        this.f237a = cls;
        this.f238b = afVar;
    }

    public <T> af<T> a(j jVar, a<T> aVar) {
        if (aVar.getRawType() == this.f237a) {
            return this.f238b;
        }
        return null;
    }

    public String toString() {
        return "Factory[type=" + this.f237a.getName() + ",adapter=" + this.f238b + "]";
    }
}
