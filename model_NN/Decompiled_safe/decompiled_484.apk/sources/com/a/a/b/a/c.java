package com.a.a.b.a;

import com.a.a.af;
import com.a.a.ag;
import com.a.a.b.b;
import com.a.a.b.f;
import com.a.a.c.a;
import com.a.a.j;
import java.lang.reflect.Type;
import java.util.Collection;

public final class c implements ag {

    /* renamed from: a  reason: collision with root package name */
    private final f f248a;

    public c(f fVar) {
        this.f248a = fVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.b.a(java.lang.reflect.Type, java.lang.Class<?>):java.lang.reflect.Type
     arg types: [java.lang.reflect.Type, java.lang.Class<? super T>]
     candidates:
      com.a.a.b.b.a(java.lang.Object[], java.lang.Object):int
      com.a.a.b.b.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.b.a(java.lang.reflect.Type, java.lang.reflect.Type):boolean
      com.a.a.b.b.a(java.lang.reflect.Type, java.lang.Class<?>):java.lang.reflect.Type */
    public <T> af<T> a(j jVar, a<T> aVar) {
        Type type = aVar.getType();
        Class<? super T> rawType = aVar.getRawType();
        if (!Collection.class.isAssignableFrom(rawType)) {
            return null;
        }
        Type a2 = b.a(type, (Class<?>) rawType);
        return new d(jVar, a2, jVar.a((a) a.get(a2)), this.f248a.a(aVar));
    }
}
