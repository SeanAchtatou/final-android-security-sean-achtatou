package com.a.a.b.a;

import com.a.a.af;
import com.a.a.b.v;
import com.a.a.d.a;
import com.a.a.d.d;
import com.a.a.r;
import com.a.a.t;
import com.a.a.w;
import com.a.a.z;
import java.util.Iterator;
import java.util.Map;

final class as extends af<t> {
    as() {
    }

    /* renamed from: a */
    public t b(a aVar) {
        switch (aVar.f()) {
            case NUMBER:
                return new z(new v(aVar.h()));
            case BOOLEAN:
                return new z(Boolean.valueOf(aVar.i()));
            case STRING:
                return new z(aVar.h());
            case NULL:
                aVar.j();
                return com.a.a.v.f345a;
            case BEGIN_ARRAY:
                r rVar = new r();
                aVar.a();
                while (aVar.e()) {
                    rVar.a(b(aVar));
                }
                aVar.b();
                return rVar;
            case BEGIN_OBJECT:
                w wVar = new w();
                aVar.c();
                while (aVar.e()) {
                    wVar.a(aVar.g(), b(aVar));
                }
                aVar.d();
                return wVar;
            default:
                throw new IllegalArgumentException();
        }
    }

    public void a(d dVar, t tVar) {
        if (tVar == null || tVar.j()) {
            dVar.f();
        } else if (tVar.i()) {
            z m = tVar.m();
            if (m.p()) {
                dVar.a(m.a());
            } else if (m.o()) {
                dVar.a(m.f());
            } else {
                dVar.b(m.b());
            }
        } else if (tVar.g()) {
            dVar.b();
            Iterator<t> it = tVar.l().iterator();
            while (it.hasNext()) {
                a(dVar, it.next());
            }
            dVar.c();
        } else if (tVar.h()) {
            dVar.d();
            for (Map.Entry next : tVar.k().o()) {
                dVar.a((String) next.getKey());
                a(dVar, (t) next.getValue());
            }
            dVar.e();
        } else {
            throw new IllegalArgumentException("Couldn't write " + tVar.getClass());
        }
    }
}
