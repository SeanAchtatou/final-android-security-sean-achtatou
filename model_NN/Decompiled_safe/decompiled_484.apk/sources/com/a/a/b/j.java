package com.a.a.b;

import java.lang.reflect.Type;

class j implements ae<T> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Class f305a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Type f306b;
    final /* synthetic */ f c;
    private final ak d = ak.a();

    j(f fVar, Class cls, Type type) {
        this.c = fVar;
        this.f305a = cls;
        this.f306b = type;
    }

    public T a() {
        try {
            return this.d.a(this.f305a);
        } catch (Exception e) {
            throw new RuntimeException("Unable to invoke no-args constructor for " + this.f306b + ". " + "Register an InstanceCreator with Gson for this type may fix this problem.", e);
        }
    }
}
