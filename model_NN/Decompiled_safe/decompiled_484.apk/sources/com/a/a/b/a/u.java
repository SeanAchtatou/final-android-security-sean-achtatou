package com.a.a.b.a;

import com.a.a.ab;
import com.a.a.af;
import com.a.a.ag;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class u extends af<Date> {

    /* renamed from: a  reason: collision with root package name */
    public static final ag f271a = new v();

    /* renamed from: b  reason: collision with root package name */
    private final DateFormat f272b = new SimpleDateFormat("MMM d, yyyy");

    /* renamed from: a */
    public synchronized Date b(a aVar) {
        Date date;
        if (aVar.f() == c.NULL) {
            aVar.j();
            date = null;
        } else {
            try {
                date = new Date(this.f272b.parse(aVar.h()).getTime());
            } catch (ParseException e) {
                throw new ab(e);
            }
        }
        return date;
    }

    public synchronized void a(d dVar, Date date) {
        dVar.b(date == null ? null : this.f272b.format(date));
    }
}
