package com.a.a.b.a;

import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.d;
import java.sql.Timestamp;
import java.util.Date;

class ap extends af<Timestamp> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ af f235a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ao f236b;

    ap(ao aoVar, af afVar) {
        this.f236b = aoVar;
        this.f235a = afVar;
    }

    /* renamed from: a */
    public Timestamp b(a aVar) {
        Date date = (Date) this.f235a.b(aVar);
        if (date != null) {
            return new Timestamp(date.getTime());
        }
        return null;
    }

    public void a(d dVar, Timestamp timestamp) {
        this.f235a.a(dVar, timestamp);
    }
}
