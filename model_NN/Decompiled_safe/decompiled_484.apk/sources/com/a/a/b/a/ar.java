package com.a.a.b.a;

import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;
import java.util.Locale;
import java.util.StringTokenizer;

final class ar extends af<Locale> {
    ar() {
    }

    /* renamed from: a */
    public Locale b(a aVar) {
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        StringTokenizer stringTokenizer = new StringTokenizer(aVar.h(), "_");
        String nextToken = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
        String nextToken2 = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
        String nextToken3 = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
        return (nextToken2 == null && nextToken3 == null) ? new Locale(nextToken) : nextToken3 == null ? new Locale(nextToken, nextToken2) : new Locale(nextToken, nextToken2, nextToken3);
    }

    public void a(d dVar, Locale locale) {
        dVar.b(locale == null ? null : locale.toString());
    }
}
