package com.a.a;

import com.a.a.b.ag;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.e;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public final class y {
    public t a(a aVar) {
        boolean p = aVar.p();
        aVar.a(true);
        try {
            t a2 = ag.a(aVar);
            aVar.a(p);
            return a2;
        } catch (StackOverflowError e) {
            throw new x("Failed parsing JSON source: " + aVar + " to Json", e);
        } catch (OutOfMemoryError e2) {
            throw new x("Failed parsing JSON source: " + aVar + " to Json", e2);
        } catch (Throwable th) {
            aVar.a(p);
            throw th;
        }
    }

    public t a(Reader reader) {
        try {
            a aVar = new a(reader);
            t a2 = a(aVar);
            if (a2.j() || aVar.f() == c.END_DOCUMENT) {
                return a2;
            }
            throw new ab("Did not consume the entire document.");
        } catch (e e) {
            throw new ab(e);
        } catch (IOException e2) {
            throw new u(e2);
        } catch (NumberFormatException e3) {
            throw new ab(e3);
        }
    }

    public t a(String str) {
        return a(new StringReader(str));
    }
}
