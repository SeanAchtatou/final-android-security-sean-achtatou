package com.a.a;

import java.util.Map;
import java.util.Set;

public final class w extends t {

    /* renamed from: a  reason: collision with root package name */
    private final com.a.a.b.w<String, t> f346a = new com.a.a.b.w<>();

    public t a(String str) {
        return this.f346a.get(str);
    }

    public void a(String str, t tVar) {
        if (tVar == null) {
            tVar = v.f345a;
        }
        this.f346a.put(str, tVar);
    }

    public r b(String str) {
        return (r) this.f346a.get(str);
    }

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof w) && ((w) obj).f346a.equals(this.f346a));
    }

    public int hashCode() {
        return this.f346a.hashCode();
    }

    public Set<Map.Entry<String, t>> o() {
        return this.f346a.entrySet();
    }
}
