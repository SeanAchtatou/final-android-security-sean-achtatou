package com.a.a;

import com.a.a.d.a;
import com.a.a.d.d;

class p<T> extends af<T> {

    /* renamed from: a  reason: collision with root package name */
    private af<T> f343a;

    p() {
    }

    public void a(af afVar) {
        if (this.f343a != null) {
            throw new AssertionError();
        }
        this.f343a = afVar;
    }

    public void a(d dVar, T t) {
        if (this.f343a == null) {
            throw new IllegalStateException();
        }
        this.f343a.a(dVar, t);
    }

    public T b(a aVar) {
        if (this.f343a != null) {
            return this.f343a.b(aVar);
        }
        throw new IllegalStateException();
    }
}
