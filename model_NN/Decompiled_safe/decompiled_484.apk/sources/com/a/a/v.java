package com.a.a;

public final class v extends t {

    /* renamed from: a  reason: collision with root package name */
    public static final v f345a = new v();

    public boolean equals(Object obj) {
        return this == obj || (obj instanceof v);
    }

    public int hashCode() {
        return v.class.hashCode();
    }
}
