package com.a.a;

public class x extends RuntimeException {
    public x(String str) {
        super(str);
    }

    public x(String str, Throwable th) {
        super(str, th);
    }

    public x(Throwable th) {
        super(th);
    }
}
