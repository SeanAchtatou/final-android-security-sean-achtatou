package com.a.a;

import com.a.a.b.a.c;
import com.a.a.b.a.e;
import com.a.a.b.a.g;
import com.a.a.b.a.h;
import com.a.a.b.a.l;
import com.a.a.b.a.n;
import com.a.a.b.a.q;
import com.a.a.b.a.u;
import com.a.a.b.a.w;
import com.a.a.b.a.z;
import com.a.a.b.af;
import com.a.a.b.ag;
import com.a.a.b.f;
import com.a.a.b.s;
import com.a.a.c.a;
import com.a.a.d.d;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class j {

    /* renamed from: a  reason: collision with root package name */
    final s f336a;

    /* renamed from: b  reason: collision with root package name */
    final aa f337b;
    private final ThreadLocal<Map<a<?>, p<?>>> c;
    private final Map<a<?>, af<?>> d;
    private final List<ag> e;
    private final f f;
    private final boolean g;
    private final boolean h;
    private final boolean i;
    private final boolean j;

    public j() {
        this(s.f318a, c.IDENTITY, Collections.emptyMap(), false, false, false, true, false, false, ac.DEFAULT, Collections.emptyList());
    }

    j(s sVar, i iVar, Map<Type, q<?>> map, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, ac acVar, List<ag> list) {
        this.c = new ThreadLocal<>();
        this.d = Collections.synchronizedMap(new HashMap());
        this.f336a = new k(this);
        this.f337b = new l(this);
        this.f = new f(map);
        this.g = z;
        this.i = z3;
        this.h = z4;
        this.j = z5;
        ArrayList arrayList = new ArrayList();
        arrayList.add(z.Q);
        arrayList.add(n.f262a);
        arrayList.add(sVar);
        arrayList.addAll(list);
        arrayList.add(z.x);
        arrayList.add(z.m);
        arrayList.add(z.g);
        arrayList.add(z.i);
        arrayList.add(z.k);
        arrayList.add(z.a(Long.TYPE, Long.class, a(acVar)));
        arrayList.add(z.a(Double.TYPE, Double.class, a(z6)));
        arrayList.add(z.a(Float.TYPE, Float.class, b(z6)));
        arrayList.add(z.r);
        arrayList.add(z.t);
        arrayList.add(z.z);
        arrayList.add(z.B);
        arrayList.add(z.a(BigDecimal.class, z.v));
        arrayList.add(z.a(BigInteger.class, z.w));
        arrayList.add(z.D);
        arrayList.add(z.F);
        arrayList.add(z.J);
        arrayList.add(z.O);
        arrayList.add(z.H);
        arrayList.add(z.d);
        arrayList.add(e.f251a);
        arrayList.add(z.M);
        arrayList.add(w.f273a);
        arrayList.add(u.f271a);
        arrayList.add(z.K);
        arrayList.add(com.a.a.b.a.a.f233a);
        arrayList.add(z.R);
        arrayList.add(z.f278b);
        arrayList.add(new c(this.f));
        arrayList.add(new l(this.f, z2));
        arrayList.add(new g(this.f));
        arrayList.add(new q(this.f, iVar, sVar));
        this.e = Collections.unmodifiableList(arrayList);
    }

    private af<Number> a(ac acVar) {
        return acVar == ac.DEFAULT ? z.n : new o(this);
    }

    private af<Number> a(boolean z) {
        return z ? z.p : new m(this);
    }

    private d a(Writer writer) {
        if (this.i) {
            writer.write(")]}'\n");
        }
        d dVar = new d(writer);
        if (this.j) {
            dVar.c("  ");
        }
        dVar.d(this.g);
        return dVar;
    }

    /* access modifiers changed from: private */
    public void a(double d2) {
        if (Double.isNaN(d2) || Double.isInfinite(d2)) {
            throw new IllegalArgumentException(d2 + " is not a valid double value as per JSON specification. To override this" + " behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method.");
        }
    }

    private static void a(Object obj, com.a.a.d.a aVar) {
        if (obj != null) {
            try {
                if (aVar.f() != com.a.a.d.c.END_DOCUMENT) {
                    throw new u("JSON document was not fully consumed.");
                }
            } catch (com.a.a.d.e e2) {
                throw new ab(e2);
            } catch (IOException e3) {
                throw new u(e3);
            }
        }
    }

    private af<Number> b(boolean z) {
        return z ? z.o : new n(this);
    }

    public <T> af<T> a(ag agVar, a<T> aVar) {
        boolean z = false;
        for (ag next : this.e) {
            if (z) {
                af<T> a2 = next.a(this, aVar);
                if (a2 != null) {
                    return a2;
                }
            } else if (next == agVar) {
                z = true;
            }
        }
        throw new IllegalArgumentException("GSON cannot serialize " + aVar);
    }

    public <T> af<T> a(a aVar) {
        HashMap hashMap;
        af<T> afVar = this.d.get(aVar);
        if (afVar == null) {
            Map map = this.c.get();
            boolean z = false;
            if (map == null) {
                HashMap hashMap2 = new HashMap();
                this.c.set(hashMap2);
                hashMap = hashMap2;
                z = true;
            } else {
                hashMap = map;
            }
            afVar = (p) hashMap.get(aVar);
            if (afVar == null) {
                try {
                    p pVar = new p();
                    hashMap.put(aVar, pVar);
                    for (ag a2 : this.e) {
                        afVar = a2.a(this, aVar);
                        if (afVar != null) {
                            pVar.a((af) afVar);
                            this.d.put(aVar, afVar);
                            hashMap.remove(aVar);
                            if (z) {
                                this.c.remove();
                            }
                        }
                    }
                    throw new IllegalArgumentException("GSON cannot handle " + aVar);
                } catch (Throwable th) {
                    hashMap.remove(aVar);
                    if (z) {
                        this.c.remove();
                    }
                    throw th;
                }
            }
        }
        return afVar;
    }

    public <T> af<T> a(Class cls) {
        return a(a.get(cls));
    }

    public <T> T a(com.a.a.d.a aVar, Type type) {
        boolean z = true;
        boolean p = aVar.p();
        aVar.a(true);
        try {
            aVar.f();
            z = false;
            T b2 = a((a) a.get(type)).b(aVar);
            aVar.a(p);
            return b2;
        } catch (EOFException e2) {
            if (z) {
                aVar.a(p);
                return null;
            }
            throw new ab(e2);
        } catch (IllegalStateException e3) {
            throw new ab(e3);
        } catch (IOException e4) {
            throw new ab(e4);
        } catch (Throwable th) {
            aVar.a(p);
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.j.a(com.a.a.d.a, java.lang.reflect.Type):T
     arg types: [com.a.a.b.a.h, java.lang.reflect.Type]
     candidates:
      com.a.a.j.a(com.a.a.j, double):void
      com.a.a.j.a(java.lang.Object, com.a.a.d.a):void
      com.a.a.j.a(com.a.a.ag, com.a.a.c.a):com.a.a.af<T>
      com.a.a.j.a(com.a.a.t, java.lang.reflect.Type):T
      com.a.a.j.a(java.io.Reader, java.lang.reflect.Type):T
      com.a.a.j.a(java.lang.String, java.lang.Class):T
      com.a.a.j.a(java.lang.String, java.lang.reflect.Type):T
      com.a.a.j.a(java.lang.Object, java.lang.reflect.Type):java.lang.String
      com.a.a.j.a(com.a.a.t, com.a.a.d.d):void
      com.a.a.j.a(com.a.a.t, java.lang.Appendable):void
      com.a.a.j.a(com.a.a.d.a, java.lang.reflect.Type):T */
    public <T> T a(t tVar, Type type) {
        if (tVar == null) {
            return null;
        }
        return a((com.a.a.d.a) new h(tVar), type);
    }

    public <T> T a(Reader reader, Type type) {
        com.a.a.d.a aVar = new com.a.a.d.a(reader);
        T a2 = a(aVar, type);
        a(a2, aVar);
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.j.a(java.lang.String, java.lang.reflect.Type):T
     arg types: [java.lang.String, java.lang.Class<T>]
     candidates:
      com.a.a.j.a(com.a.a.j, double):void
      com.a.a.j.a(java.lang.Object, com.a.a.d.a):void
      com.a.a.j.a(com.a.a.ag, com.a.a.c.a):com.a.a.af<T>
      com.a.a.j.a(com.a.a.d.a, java.lang.reflect.Type):T
      com.a.a.j.a(com.a.a.t, java.lang.reflect.Type):T
      com.a.a.j.a(java.io.Reader, java.lang.reflect.Type):T
      com.a.a.j.a(java.lang.String, java.lang.Class):T
      com.a.a.j.a(java.lang.Object, java.lang.reflect.Type):java.lang.String
      com.a.a.j.a(com.a.a.t, com.a.a.d.d):void
      com.a.a.j.a(com.a.a.t, java.lang.Appendable):void
      com.a.a.j.a(java.lang.String, java.lang.reflect.Type):T */
    public <T> T a(String str, Class<T> cls) {
        return af.a((Class) cls).cast(a(str, (Type) cls));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.j.a(java.io.Reader, java.lang.reflect.Type):T
     arg types: [java.io.StringReader, java.lang.reflect.Type]
     candidates:
      com.a.a.j.a(com.a.a.j, double):void
      com.a.a.j.a(java.lang.Object, com.a.a.d.a):void
      com.a.a.j.a(com.a.a.ag, com.a.a.c.a):com.a.a.af<T>
      com.a.a.j.a(com.a.a.d.a, java.lang.reflect.Type):T
      com.a.a.j.a(com.a.a.t, java.lang.reflect.Type):T
      com.a.a.j.a(java.lang.String, java.lang.Class):T
      com.a.a.j.a(java.lang.String, java.lang.reflect.Type):T
      com.a.a.j.a(java.lang.Object, java.lang.reflect.Type):java.lang.String
      com.a.a.j.a(com.a.a.t, com.a.a.d.d):void
      com.a.a.j.a(com.a.a.t, java.lang.Appendable):void
      com.a.a.j.a(java.io.Reader, java.lang.reflect.Type):T */
    public <T> T a(String str, Type type) {
        if (str == null) {
            return null;
        }
        return a((Reader) new StringReader(str), type);
    }

    public String a(t tVar) {
        StringWriter stringWriter = new StringWriter();
        a(tVar, stringWriter);
        return stringWriter.toString();
    }

    public String a(Object obj) {
        return obj == null ? a((t) v.f345a) : a(obj, obj.getClass());
    }

    public String a(Object obj, Type type) {
        StringWriter stringWriter = new StringWriter();
        a(obj, type, stringWriter);
        return stringWriter.toString();
    }

    public void a(t tVar, d dVar) {
        boolean g2 = dVar.g();
        dVar.b(true);
        boolean h2 = dVar.h();
        dVar.c(this.h);
        boolean i2 = dVar.i();
        dVar.d(this.g);
        try {
            ag.a(tVar, dVar);
            dVar.b(g2);
            dVar.c(h2);
            dVar.d(i2);
        } catch (IOException e2) {
            throw new u(e2);
        } catch (Throwable th) {
            dVar.b(g2);
            dVar.c(h2);
            dVar.d(i2);
            throw th;
        }
    }

    public void a(t tVar, Appendable appendable) {
        try {
            a(tVar, a(ag.a(appendable)));
        } catch (IOException e2) {
            throw new RuntimeException(e2);
        }
    }

    public void a(Object obj, Type type, d dVar) {
        af a2 = a((a) a.get(type));
        boolean g2 = dVar.g();
        dVar.b(true);
        boolean h2 = dVar.h();
        dVar.c(this.h);
        boolean i2 = dVar.i();
        dVar.d(this.g);
        try {
            a2.a(dVar, obj);
            dVar.b(g2);
            dVar.c(h2);
            dVar.d(i2);
        } catch (IOException e2) {
            throw new u(e2);
        } catch (Throwable th) {
            dVar.b(g2);
            dVar.c(h2);
            dVar.d(i2);
            throw th;
        }
    }

    public void a(Object obj, Type type, Appendable appendable) {
        try {
            a(obj, type, a(ag.a(appendable)));
        } catch (IOException e2) {
            throw new u(e2);
        }
    }

    public String toString() {
        return "{serializeNulls:" + this.g + "factories:" + this.e + ",instanceCreators:" + this.f + "}";
    }
}
