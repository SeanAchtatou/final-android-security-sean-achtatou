package com.a.a;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class r extends t implements Iterable<t> {

    /* renamed from: a  reason: collision with root package name */
    private final List<t> f344a = new ArrayList();

    public Number a() {
        if (this.f344a.size() == 1) {
            return this.f344a.get(0).a();
        }
        throw new IllegalStateException();
    }

    public void a(t tVar) {
        if (tVar == null) {
            tVar = v.f345a;
        }
        this.f344a.add(tVar);
    }

    public String b() {
        if (this.f344a.size() == 1) {
            return this.f344a.get(0).b();
        }
        throw new IllegalStateException();
    }

    public double c() {
        if (this.f344a.size() == 1) {
            return this.f344a.get(0).c();
        }
        throw new IllegalStateException();
    }

    public long d() {
        if (this.f344a.size() == 1) {
            return this.f344a.get(0).d();
        }
        throw new IllegalStateException();
    }

    public int e() {
        if (this.f344a.size() == 1) {
            return this.f344a.get(0).e();
        }
        throw new IllegalStateException();
    }

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof r) && ((r) obj).f344a.equals(this.f344a));
    }

    public boolean f() {
        if (this.f344a.size() == 1) {
            return this.f344a.get(0).f();
        }
        throw new IllegalStateException();
    }

    public int hashCode() {
        return this.f344a.hashCode();
    }

    public Iterator<t> iterator() {
        return this.f344a.iterator();
    }
}
