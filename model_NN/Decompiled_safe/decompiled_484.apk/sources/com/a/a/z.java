package com.a.a;

import com.a.a.b.a;
import com.a.a.b.v;
import java.math.BigInteger;

public final class z extends t {

    /* renamed from: a  reason: collision with root package name */
    private static final Class<?>[] f347a = {Integer.TYPE, Long.TYPE, Short.TYPE, Float.TYPE, Double.TYPE, Byte.TYPE, Boolean.TYPE, Character.TYPE, Integer.class, Long.class, Short.class, Float.class, Double.class, Byte.class, Boolean.class, Character.class};

    /* renamed from: b  reason: collision with root package name */
    private Object f348b;

    public z(Boolean bool) {
        a(bool);
    }

    public z(Number number) {
        a(number);
    }

    public z(String str) {
        a(str);
    }

    private static boolean a(z zVar) {
        if (!(zVar.f348b instanceof Number)) {
            return false;
        }
        Number number = (Number) zVar.f348b;
        return (number instanceof BigInteger) || (number instanceof Long) || (number instanceof Integer) || (number instanceof Short) || (number instanceof Byte);
    }

    private static boolean b(Object obj) {
        if (obj instanceof String) {
            return true;
        }
        Class<?> cls = obj.getClass();
        for (Class<?> isAssignableFrom : f347a) {
            if (isAssignableFrom.isAssignableFrom(cls)) {
                return true;
            }
        }
        return false;
    }

    public Number a() {
        return this.f348b instanceof String ? new v((String) this.f348b) : (Number) this.f348b;
    }

    /* access modifiers changed from: package-private */
    public void a(Object obj) {
        if (obj instanceof Character) {
            this.f348b = String.valueOf(((Character) obj).charValue());
            return;
        }
        a.a((obj instanceof Number) || b(obj));
        this.f348b = obj;
    }

    public String b() {
        return p() ? a().toString() : o() ? n().toString() : (String) this.f348b;
    }

    public double c() {
        return p() ? a().doubleValue() : Double.parseDouble(b());
    }

    public long d() {
        return p() ? a().longValue() : Long.parseLong(b());
    }

    public int e() {
        return p() ? a().intValue() : Integer.parseInt(b());
    }

    public boolean equals(Object obj) {
        boolean z = false;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        z zVar = (z) obj;
        if (this.f348b == null) {
            return zVar.f348b == null;
        }
        if (a(this) && a(zVar)) {
            return a().longValue() == zVar.a().longValue();
        }
        if (!(this.f348b instanceof Number) || !(zVar.f348b instanceof Number)) {
            return this.f348b.equals(zVar.f348b);
        }
        double doubleValue = a().doubleValue();
        double doubleValue2 = zVar.a().doubleValue();
        if (doubleValue == doubleValue2 || (Double.isNaN(doubleValue) && Double.isNaN(doubleValue2))) {
            z = true;
        }
        return z;
    }

    public boolean f() {
        return o() ? n().booleanValue() : Boolean.parseBoolean(b());
    }

    public int hashCode() {
        if (this.f348b == null) {
            return 31;
        }
        if (a(this)) {
            long longValue = a().longValue();
            return (int) (longValue ^ (longValue >>> 32));
        } else if (!(this.f348b instanceof Number)) {
            return this.f348b.hashCode();
        } else {
            long doubleToLongBits = Double.doubleToLongBits(a().doubleValue());
            return (int) (doubleToLongBits ^ (doubleToLongBits >>> 32));
        }
    }

    /* access modifiers changed from: package-private */
    public Boolean n() {
        return (Boolean) this.f348b;
    }

    public boolean o() {
        return this.f348b instanceof Boolean;
    }

    public boolean p() {
        return this.f348b instanceof Number;
    }

    public boolean q() {
        return this.f348b instanceof String;
    }
}
