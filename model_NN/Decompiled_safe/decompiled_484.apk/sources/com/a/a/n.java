package com.a.a;

import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;

class n extends af<Number> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ j f341a;

    n(j jVar) {
        this.f341a = jVar;
    }

    /* renamed from: a */
    public Float b(a aVar) {
        if (aVar.f() != c.NULL) {
            return Float.valueOf((float) aVar.k());
        }
        aVar.j();
        return null;
    }

    public void a(d dVar, Number number) {
        if (number == null) {
            dVar.f();
            return;
        }
        this.f341a.a((double) number.floatValue());
        dVar.a(number);
    }
}
