package com.a.a;

import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.d.d;

class m extends af<Number> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ j f340a;

    m(j jVar) {
        this.f340a = jVar;
    }

    /* renamed from: a */
    public Double b(a aVar) {
        if (aVar.f() != c.NULL) {
            return Double.valueOf(aVar.k());
        }
        aVar.j();
        return null;
    }

    public void a(d dVar, Number number) {
        if (number == null) {
            dVar.f();
            return;
        }
        this.f340a.a(number.doubleValue());
        dVar.a(number);
    }
}
