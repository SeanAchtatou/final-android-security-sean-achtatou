package com.b.a;

import android.content.Context;

final class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Context f506a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ c f507b;

    b(Context context, c cVar) {
        this.f506a = context;
        this.f507b = cVar;
    }

    public final void run() {
        if (f.a(this.f506a).a() == null) {
            f.a(this.f506a).b();
        }
        if (t.i(this.f506a)) {
            long a2 = g.a();
            if (t.f534a == 0 || a2 > t.f534a) {
                t.f534a = a2 + (900 * ((long) Math.pow(2.0d, (double) t.f535b)));
                if (t.f535b < 2) {
                    t.f535b++;
                }
                t.l(this.f506a);
            }
        }
        new s(this.f506a, this.f507b).a();
    }
}
