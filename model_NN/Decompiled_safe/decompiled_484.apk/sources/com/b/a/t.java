package com.b.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.mediav.ads.sdk.adcore.HttpCacher;

public final class t {

    /* renamed from: a  reason: collision with root package name */
    static long f534a;

    /* renamed from: b  reason: collision with root package name */
    static int f535b = 0;
    /* access modifiers changed from: private */
    public static Thread c;

    public static int a(Context context) {
        return context.getSharedPreferences("mzSdkProfilePrefs", 0).getInt("mzMaxLogItems", 100);
    }

    static void a(Context context, String str, String str2) {
        if (str != null) {
            SharedPreferences.Editor edit = context.getSharedPreferences("mzSdkProfilePrefs", 0).edit();
            edit.putString("mzLatestLocation", str);
            edit.putString("latestLocation", str2);
            edit.putLong("mzLocationUpdateTimestamp", g.a());
            edit.commit();
        }
    }

    public static int b(Context context) {
        return context.getSharedPreferences("mzSdkProfilePrefs", 0).getInt("mzMaxLogRetryTime", 20);
    }

    public static int c(Context context) {
        return context.getSharedPreferences("mzSdkProfilePrefs", 0).getInt("mzProfileVersion", 1);
    }

    public static String d(Context context) {
        return context.getSharedPreferences("mzSdkProfilePrefs", 0).getString("mzSignVersion", "1.1");
    }

    public static int e(Context context) {
        return context.getSharedPreferences("mzSdkProfilePrefs", 0).getInt("mzLogExpiresIn", 604800);
    }

    public static int f(Context context) {
        return context.getSharedPreferences("mzSdkProfilePrefs", 0).getInt("mzLocationServiceTimeout", 15);
    }

    public static String g(Context context) {
        return context.getSharedPreferences("mzSdkProfilePrefs", 0).getString("mzLatestLocation", "[UNKNOWN]");
    }

    static String h(Context context) {
        return context.getSharedPreferences("mzSdkProfilePrefs", 0).getString("latestLocation", "0x0");
    }

    public static boolean i(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("mzSdkProfilePrefs", 0);
        if (sharedPreferences.getString("mzConfigFile", null) == null) {
            return true;
        }
        long a2 = g.a();
        return a2 - sharedPreferences.getLong("mzProfileUpdateTimestamp", a2) >= ((long) sharedPreferences.getInt("mzProfileExpiresIn", HttpCacher.TIME_DAY));
    }

    public static boolean j(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("mzSdkProfilePrefs", 0);
        long a2 = g.a();
        long j = sharedPreferences.getLong("mzLocationUpdateTimestamp", a2);
        return j == a2 || a2 - j >= ((long) sharedPreferences.getInt("mzLocationExpiresIn", 300)) || sharedPreferences.getString("mzLatestLocation", "[UNKNOWN]").equals("[UNKNOWN]");
    }

    static String k(Context context) {
        String string = context.getSharedPreferences("mzSdkProfilePrefs", 0).getString("mzConfigFile", null);
        return string == null ? l.f522a : string;
    }

    static void l(Context context) {
        if (c == null || !c.isAlive()) {
            u uVar = new u(context);
            c = uVar;
            uVar.start();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x012d A[SYNTHETIC, Splitter:B:43:0x012d] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x015f A[SYNTHETIC, Splitter:B:60:0x015f] */
    /* JADX WARNING: Removed duplicated region for block: B:73:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void m(android.content.Context r9) {
        /*
            r2 = 0
            r5 = 1
            r0 = 0
            java.lang.String r1 = "mzSdkProfilePrefs"
            r3 = 0
            android.content.SharedPreferences r6 = r9.getSharedPreferences(r1, r3)     // Catch:{ Exception -> 0x016f, all -> 0x015b }
            java.lang.String r1 = "mzConfigFile"
            r3 = 0
            java.lang.String r3 = r6.getString(r1, r3)     // Catch:{ Exception -> 0x016f, all -> 0x015b }
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x016f, all -> 0x015b }
            java.lang.String r4 = "UTF-8"
            byte[] r3 = r3.getBytes(r4)     // Catch:{ Exception -> 0x016f, all -> 0x015b }
            r1.<init>(r3)     // Catch:{ Exception -> 0x016f, all -> 0x015b }
            org.xmlpull.v1.XmlPullParser r7 = android.util.Xml.newPullParser()     // Catch:{ Exception -> 0x0116 }
            java.lang.String r3 = "UTF-8"
            r7.setInput(r1, r3)     // Catch:{ Exception -> 0x0116 }
            int r3 = r7.getEventType()     // Catch:{ Exception -> 0x0116 }
            r4 = r2
            r2 = r0
        L_0x002b:
            if (r3 != r5) goto L_0x0105
        L_0x002d:
            android.content.SharedPreferences$Editor r2 = r6.edit()     // Catch:{ Exception -> 0x0116 }
            java.lang.String r0 = "configVersion"
            boolean r0 = r4.containsKey(r0)     // Catch:{ Exception -> 0x0116 }
            if (r0 == 0) goto L_0x004a
            java.lang.String r3 = "mzProfileVersion"
            java.lang.String r0 = "configVersion"
            java.lang.Object r0 = r4.get(r0)     // Catch:{ Exception -> 0x0116 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0116 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x0116 }
            r2.putInt(r3, r0)     // Catch:{ Exception -> 0x0116 }
        L_0x004a:
            java.lang.String r0 = "signVersion"
            boolean r0 = r4.containsKey(r0)     // Catch:{ Exception -> 0x0116 }
            if (r0 == 0) goto L_0x005f
            java.lang.String r3 = "mzSignVersion"
            java.lang.String r0 = "signVersion"
            java.lang.Object r0 = r4.get(r0)     // Catch:{ Exception -> 0x0116 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0116 }
            r2.putString(r3, r0)     // Catch:{ Exception -> 0x0116 }
        L_0x005f:
            java.lang.String r0 = "configExpiration"
            boolean r0 = r4.containsKey(r0)     // Catch:{ Exception -> 0x0116 }
            if (r0 == 0) goto L_0x0078
            java.lang.String r3 = "mzProfileExpiresIn"
            java.lang.String r0 = "configExpiration"
            java.lang.Object r0 = r4.get(r0)     // Catch:{ Exception -> 0x0116 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0116 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x0116 }
            r2.putInt(r3, r0)     // Catch:{ Exception -> 0x0116 }
        L_0x0078:
            java.lang.String r0 = "cacheExpiration"
            boolean r0 = r4.containsKey(r0)     // Catch:{ Exception -> 0x0116 }
            if (r0 == 0) goto L_0x0091
            java.lang.String r3 = "mzLogExpiresIn"
            java.lang.String r0 = "cacheExpiration"
            java.lang.Object r0 = r4.get(r0)     // Catch:{ Exception -> 0x0116 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0116 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x0116 }
            r2.putInt(r3, r0)     // Catch:{ Exception -> 0x0116 }
        L_0x0091:
            java.lang.String r0 = "cacheNumbers"
            boolean r0 = r4.containsKey(r0)     // Catch:{ Exception -> 0x0116 }
            if (r0 == 0) goto L_0x00aa
            java.lang.String r3 = "mzMaxLogItems"
            java.lang.String r0 = "cacheNumbers"
            java.lang.Object r0 = r4.get(r0)     // Catch:{ Exception -> 0x0116 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0116 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x0116 }
            r2.putInt(r3, r0)     // Catch:{ Exception -> 0x0116 }
        L_0x00aa:
            java.lang.String r0 = "retryTimes"
            boolean r0 = r4.containsKey(r0)     // Catch:{ Exception -> 0x0116 }
            if (r0 == 0) goto L_0x00c3
            java.lang.String r3 = "mzMaxLogRetryTime"
            java.lang.String r0 = "retryTimes"
            java.lang.Object r0 = r4.get(r0)     // Catch:{ Exception -> 0x0116 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0116 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x0116 }
            r2.putInt(r3, r0)     // Catch:{ Exception -> 0x0116 }
        L_0x00c3:
            java.lang.String r0 = "locationExpiration"
            boolean r0 = r4.containsKey(r0)     // Catch:{ Exception -> 0x0116 }
            if (r0 == 0) goto L_0x00dc
            java.lang.String r3 = "mzLocationExpiresIn"
            java.lang.String r0 = "locationExpiration"
            java.lang.Object r0 = r4.get(r0)     // Catch:{ Exception -> 0x0116 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0116 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x0116 }
            r2.putInt(r3, r0)     // Catch:{ Exception -> 0x0116 }
        L_0x00dc:
            java.lang.String r0 = "locationServiceTimeout"
            boolean r0 = r4.containsKey(r0)     // Catch:{ Exception -> 0x0116 }
            if (r0 == 0) goto L_0x00f5
            java.lang.String r3 = "mzLocationServiceTimeout"
            java.lang.String r0 = "locationServiceTimeout"
            java.lang.Object r0 = r4.get(r0)     // Catch:{ Exception -> 0x0116 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0116 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x0116 }
            r2.putInt(r3, r0)     // Catch:{ Exception -> 0x0116 }
        L_0x00f5:
            java.lang.String r0 = "mzProfileUpdateTimestamp"
            long r4 = com.b.a.g.a()     // Catch:{ Exception -> 0x0116 }
            r2.putLong(r0, r4)     // Catch:{ Exception -> 0x0116 }
            r2.commit()     // Catch:{ Exception -> 0x0116 }
            r1.close()     // Catch:{ IOException -> 0x0168 }
        L_0x0104:
            return
        L_0x0105:
            if (r0 != 0) goto L_0x002d
            switch(r3) {
                case 0: goto L_0x0110;
                case 1: goto L_0x010a;
                case 2: goto L_0x0136;
                case 3: goto L_0x014d;
                default: goto L_0x010a;
            }
        L_0x010a:
            int r3 = r7.next()     // Catch:{ Exception -> 0x0116 }
            goto L_0x002b
        L_0x0110:
            java.util.HashMap r4 = new java.util.HashMap     // Catch:{ Exception -> 0x0116 }
            r4.<init>()     // Catch:{ Exception -> 0x0116 }
            goto L_0x010a
        L_0x0116:
            r0 = move-exception
        L_0x0117:
            java.lang.String r2 = "MZSDK:20141030"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x016d }
            java.lang.String r4 = " Exception:"
            r3.<init>(r4)     // Catch:{ all -> 0x016d }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x016d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x016d }
            android.util.Log.d(r2, r0)     // Catch:{ all -> 0x016d }
            if (r1 == 0) goto L_0x0104
            r1.close()     // Catch:{ IOException -> 0x0131 }
            goto L_0x0104
        L_0x0131:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0104
        L_0x0136:
            java.lang.String r3 = r7.getName()     // Catch:{ Exception -> 0x0116 }
            if (r2 == 0) goto L_0x0143
            java.lang.String r8 = r7.nextText()     // Catch:{ Exception -> 0x0116 }
            r4.put(r3, r8)     // Catch:{ Exception -> 0x0116 }
        L_0x0143:
            java.lang.String r8 = "common"
            boolean r3 = r3.equals(r8)     // Catch:{ Exception -> 0x0116 }
            if (r3 == 0) goto L_0x010a
            r2 = r5
            goto L_0x010a
        L_0x014d:
            java.lang.String r3 = r7.getName()     // Catch:{ Exception -> 0x0116 }
            java.lang.String r8 = "common"
            boolean r3 = r3.equals(r8)     // Catch:{ Exception -> 0x0116 }
            if (r3 == 0) goto L_0x010a
            r0 = r5
            goto L_0x010a
        L_0x015b:
            r0 = move-exception
            r1 = r2
        L_0x015d:
            if (r1 == 0) goto L_0x0162
            r1.close()     // Catch:{ IOException -> 0x0163 }
        L_0x0162:
            throw r0
        L_0x0163:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0162
        L_0x0168:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0104
        L_0x016d:
            r0 = move-exception
            goto L_0x015d
        L_0x016f:
            r0 = move-exception
            r1 = r2
            goto L_0x0117
        */
        throw new UnsupportedOperationException("Method not decompiled: com.b.a.t.m(android.content.Context):void");
    }
}
