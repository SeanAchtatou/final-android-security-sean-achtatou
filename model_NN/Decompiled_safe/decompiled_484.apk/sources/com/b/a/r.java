package com.b.a;

import java.util.Comparator;

final class r implements Comparator {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ q f531a;

    private r(q qVar) {
        this.f531a = qVar;
    }

    /* synthetic */ r(q qVar, byte b2) {
        this(qVar);
    }

    public final int compare(Object obj, Object obj2) {
        if (((Integer) this.f531a.c.get(obj)).intValue() < ((Integer) this.f531a.c.get(obj2)).intValue()) {
            return 1;
        }
        return this.f531a.c.get(obj) == this.f531a.c.get(obj2) ? 0 : -1;
    }
}
