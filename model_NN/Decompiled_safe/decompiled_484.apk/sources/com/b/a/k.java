package com.b.a;

final class k {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f521a = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};

    public static String a(String str) {
        int i = 0;
        if (str == null) {
            return "";
        }
        byte[] bytes = str.getBytes();
        int length = bytes.length;
        byte[] bArr = new byte[(((length + 2) / 3) << 2)];
        int i2 = length;
        int i3 = 0;
        while (i2 > 2) {
            int i4 = i + 1;
            bArr[i] = f521a[bytes[i3] >> 2];
            int i5 = i4 + 1;
            bArr[i4] = f521a[((bytes[i3] & 3) << 4) + (bytes[i3 + 1] >> 4)];
            int i6 = i5 + 1;
            bArr[i5] = f521a[((bytes[i3 + 1] & 15) << 2) + (bytes[i3 + 2] >> 6)];
            i = i6 + 1;
            bArr[i6] = f521a[bytes[i3 + 2] & 63];
            i3 += 3;
            i2 -= 3;
        }
        if (i2 != 0) {
            int i7 = i + 1;
            bArr[i] = f521a[bytes[i3] >> 2];
            if (i2 > 1) {
                int i8 = i7 + 1;
                bArr[i7] = f521a[((bytes[i3] & 3) << 4) + (bytes[i3 + 1] >> 4)];
                bArr[i8] = f521a[(bytes[i3 + 1] & 15) << 2];
                bArr[i8 + 1] = 61;
            } else {
                int i9 = i7 + 1;
                bArr[i7] = f521a[(bytes[i3] & 3) << 4];
                bArr[i9] = 61;
                bArr[i9 + 1] = 61;
            }
        }
        return new String(bArr);
    }
}
