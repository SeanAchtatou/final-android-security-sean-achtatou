package com.b.a;

import android.content.Context;
import android.database.Cursor;
import com.qihoo360.daily.activity.SearchActivity;
import java.util.ArrayList;
import java.util.List;

class d {

    /* renamed from: b  reason: collision with root package name */
    private static d f510b = null;

    /* renamed from: a  reason: collision with root package name */
    private Context f511a;

    private d(Context context) {
        this.f511a = context;
    }

    public static d a(Context context) {
        d dVar;
        synchronized (d.class) {
            if (f510b == null) {
                f510b = new d(context.getApplicationContext());
            }
            dVar = f510b;
        }
        return dVar;
    }

    private void a(c cVar) {
        e eVar = new e(this, this.f511a, "mzmonitor", null, 6);
        eVar.getWritableDatabase().delete("mzcaches", "cacheId = ? AND url = ?", new String[]{cVar.e(), cVar.a()});
        eVar.close();
    }

    private int b() {
        Exception exc;
        int i;
        try {
            e eVar = new e(this, this.f511a, "mzmonitor", null, 6);
            Cursor rawQuery = eVar.getReadableDatabase().rawQuery("select count(*) from mzcaches", null);
            rawQuery.moveToFirst();
            int i2 = rawQuery.getInt(0);
            try {
                rawQuery.close();
                eVar.close();
                return i2;
            } catch (Exception e) {
                Exception exc2 = e;
                i = i2;
                exc = exc2;
                exc.printStackTrace();
                return i;
            }
        } catch (Exception e2) {
            exc = e2;
            i = 0;
        }
    }

    private boolean b(c cVar) {
        e eVar = new e(this, this.f511a, "mzmonitor", null, 6);
        Cursor rawQuery = eVar.getReadableDatabase().rawQuery("SELECT * FROM mzcaches WHERE cacheId = ? and url = ?", new String[]{cVar.e(), cVar.a()});
        boolean z = rawQuery.moveToNext();
        rawQuery.close();
        eVar.close();
        return z;
    }

    public final List<c> a() {
        ArrayList arrayList = new ArrayList();
        try {
            e eVar = new e(this, this.f511a, "mzmonitor", null, 6);
            Cursor query = eVar.getReadableDatabase().query("mzcaches", new String[]{"cacheId", SearchActivity.TAG_URL, "timestamp", "times"}, null, null, null, null, null);
            if (query.getCount() > 0) {
                while (query.moveToNext()) {
                    c cVar = new c();
                    cVar.b(query.getString(query.getColumnIndex("cacheId")));
                    cVar.a(query.getString(query.getColumnIndex(SearchActivity.TAG_URL)));
                    cVar.a(query.getLong(query.getColumnIndex("timestamp")));
                    cVar.a((int) query.getShort(query.getColumnIndex("times")));
                    arrayList.add(cVar);
                }
            }
            query.close();
            eVar.close();
            return arrayList;
        } catch (Exception e) {
            return null;
        }
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(com.b.a.c r10, boolean r11) {
        /*
            r9 = this;
            r1 = 0
            r0 = 1
            monitor-enter(r9)
            if (r11 == 0) goto L_0x0010
            boolean r0 = r9.b(r10)     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            if (r0 == 0) goto L_0x000e
            r9.a(r10)     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
        L_0x000e:
            monitor-exit(r9)
            return
        L_0x0010:
            boolean r2 = r9.b(r10)     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            if (r2 == 0) goto L_0x008a
            int r2 = r10.h()     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            android.content.Context r3 = r9.f511a     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            int r3 = com.b.a.t.b(r3)     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            if (r2 < r3) goto L_0x002a
        L_0x0022:
            if (r0 == 0) goto L_0x0041
            r9.a(r10)     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            goto L_0x000e
        L_0x0028:
            r0 = move-exception
            goto L_0x000e
        L_0x002a:
            long r2 = r10.g()     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            android.content.Context r4 = r9.f511a     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            int r4 = com.b.a.t.e(r4)     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            long r4 = (long) r4     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            long r6 = com.b.a.g.a()     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            long r2 = r6 - r2
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 > 0) goto L_0x0022
            r0 = r1
            goto L_0x0022
        L_0x0041:
            com.b.a.e r0 = new com.b.a.e     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            android.content.Context r2 = r9.f511a     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            java.lang.String r3 = "mzmonitor"
            r4 = 0
            r5 = 6
            r1 = r9
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            android.database.sqlite.SQLiteDatabase r1 = r0.getWritableDatabase()     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            int r2 = r10.h()     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            int r2 = r2 + 1
            r10.a(r2)     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            java.lang.String r2 = "mzcaches"
            android.content.ContentValues r3 = r10.j()     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            java.lang.String r4 = "cacheId = ? AND url = ?"
            r5 = 2
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            r6 = 0
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            r7.<init>()     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            java.lang.String r8 = r10.e()     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            r5[r6] = r7     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            r6 = 1
            java.lang.String r7 = r10.a()     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            r5[r6] = r7     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            r1.update(r2, r3, r4, r5)     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            r0.close()     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            goto L_0x000e
        L_0x0087:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x008a:
            int r2 = r9.b()     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            android.content.Context r3 = r9.f511a     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            int r3 = com.b.a.t.a(r3)     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            if (r2 < r3) goto L_0x0121
        L_0x0096:
            if (r0 == 0) goto L_0x00ea
            com.b.a.e r0 = new com.b.a.e     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            android.content.Context r2 = r9.f511a     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            java.lang.String r3 = "mzmonitor"
            r4 = 0
            r5 = 6
            r1 = r9
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            android.database.sqlite.SQLiteDatabase r1 = r0.getReadableDatabase()     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            java.lang.String r2 = "SELECT * FROM mzcaches ORDER BY timestamp ASC LIMIT 1"
            r3 = 0
            android.database.Cursor r2 = r1.rawQuery(r2, r3)     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            boolean r3 = r2.moveToNext()     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            if (r3 == 0) goto L_0x00e4
            java.lang.String r3 = "DELETE FROM mzcaches WHERE cacheId = ? AND url = ?"
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            r5 = 0
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            r6.<init>()     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            java.lang.String r7 = "cacheId"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            java.lang.String r7 = r2.getString(r7)     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            r4[r5] = r6     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            r5 = 1
            java.lang.String r6 = "url"
            int r6 = r2.getColumnIndex(r6)     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            java.lang.String r6 = r2.getString(r6)     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            r4[r5] = r6     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            r1.execSQL(r3, r4)     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
        L_0x00e4:
            r2.close()     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            r0.close()     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
        L_0x00ea:
            com.b.a.e r0 = new com.b.a.e     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            android.content.Context r2 = r9.f511a     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            java.lang.String r3 = "mzmonitor"
            r4 = 0
            r5 = 6
            r1 = r9
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            android.database.sqlite.SQLiteDatabase r1 = r0.getWritableDatabase()     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            int r2 = r10.h()     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            int r2 = r2 + 1
            r10.a(r2)     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            java.lang.String r2 = "mzcaches"
            r3 = 0
            android.content.ContentValues r4 = r10.j()     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            r1.insert(r2, r3, r4)     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            java.lang.String r1 = "insert Cache"
            java.lang.String r2 = r10.toString()     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            r0.close()     // Catch:{ Exception -> 0x011b, all -> 0x0087 }
            goto L_0x000e
        L_0x011b:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x0028, all -> 0x0087 }
            goto L_0x000e
        L_0x0121:
            r0 = r1
            goto L_0x0096
        */
        throw new UnsupportedOperationException("Method not decompiled: com.b.a.d.a(com.b.a.c, boolean):void");
    }
}
