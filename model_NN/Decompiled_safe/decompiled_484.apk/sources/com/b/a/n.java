package com.b.a;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.util.Log;
import com.amap.api.location.LocationManagerProxy;

class n {
    private static n d;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f525a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f526b;
    private LocationManager c;
    private final LocationListener e = new o(this);

    private n(Context context) {
        this.f525a = context;
        this.f526b = false;
    }

    public static n a(Context context) {
        n nVar;
        synchronized (n.class) {
            if (d == null) {
                d = new n(context);
            }
            nVar = d;
        }
        return nVar;
    }

    static /* synthetic */ void a(n nVar, Location location) {
        String a2;
        String str;
        Log.d("MZSDK:20141030", location.toString());
        Context context = nVar.f525a;
        if (!location.hasAccuracy()) {
            a2 = "[UNKNOWN]";
        } else {
            a2 = k.a(String.format("%.7f,%.7f,%f,%d", Double.valueOf(location.getLatitude()), Double.valueOf(location.getLongitude()), Float.valueOf(location.getAccuracy()), Long.valueOf(g.a())));
        }
        if (!location.hasAccuracy()) {
            str = "0x0";
        } else {
            String sb = new StringBuilder(String.valueOf(location.getAccuracy())).toString();
            if (sb.length() >= 10) {
                sb = sb.substring(0, 10);
            }
            str = String.valueOf(String.format("%.6fx%.6fx", Double.valueOf(location.getLatitude()), Double.valueOf(location.getLongitude()))) + sb;
        }
        t.a(context, a2, str);
        nVar.a();
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (this.c != null) {
            this.c.removeUpdates(this.e);
            this.c = null;
            this.f526b = false;
        }
    }

    public final void b(Context context) {
        if (!context.getSharedPreferences("mzSdkProfilePrefs", 0).getBoolean("0a9896360edb4c54030c25b12f447fb0", false)) {
            if (!this.f526b) {
                this.c = (LocationManager) context.getSystemService(LocationManagerProxy.KEY_LOCATION_CHANGED);
                Criteria criteria = new Criteria();
                criteria.setAltitudeRequired(false);
                criteria.setBearingRequired(false);
                criteria.setSpeedRequired(false);
                criteria.setCostAllowed(false);
                String bestProvider = this.c.getBestProvider(criteria, true);
                if (bestProvider != null) {
                    this.f526b = true;
                    this.c.requestLocationUpdates(bestProvider, 500, 0.0f, this.e);
                    new p(this).start();
                    return;
                }
                return;
            }
            Log.d("MZSDK:20141030", "MZLocationManager is still running...");
        }
    }
}
