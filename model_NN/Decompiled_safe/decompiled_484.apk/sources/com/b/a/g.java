package com.b.a;

import com.qihoo.dynamic.util.Md5Util;
import java.security.MessageDigest;

public class g {

    /* renamed from: a  reason: collision with root package name */
    String f514a;

    /* renamed from: b  reason: collision with root package name */
    String f515b;
    String c = "raw";
    boolean d = true;

    g(f fVar) {
    }

    public static long a() {
        return System.currentTimeMillis() / 1000;
    }

    public static String a(String str) {
        if (str == null || str.length() == 0) {
            return "";
        }
        try {
            MessageDigest instance = MessageDigest.getInstance(Md5Util.ALGORITHM);
            instance.reset();
            instance.update(str.getBytes(Md5Util.DEFAULT_CHARSET));
            return a(instance.digest());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (byte b2 : bArr) {
            String hexString = Integer.toHexString(b2 & 255);
            if (hexString.length() == 1) {
                sb.append("0").append(hexString);
            } else {
                sb.append(hexString);
            }
        }
        return sb.toString();
    }

    public static String b(String str) {
        if (str == null || str.length() == 0) {
            return "";
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.update(str.getBytes(Md5Util.DEFAULT_CHARSET), 0, str.length());
            return a(instance.digest());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
