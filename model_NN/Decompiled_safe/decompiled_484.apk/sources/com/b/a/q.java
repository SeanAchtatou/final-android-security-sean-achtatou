package com.b.a;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.provider.Settings;
import android.util.Log;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

final class q implements ServiceConnection {
    private static String f = null;
    private static boolean g = false;

    /* renamed from: a  reason: collision with root package name */
    private final Context f529a;

    /* renamed from: b  reason: collision with root package name */
    private List<ResolveInfo> f530b;
    /* access modifiers changed from: private */
    public Map<String, Integer> c = new HashMap();
    private final SharedPreferences d;
    private final Random e = new Random();

    private q(Context context) {
        this.d = context.getSharedPreferences("openudid_prefs", 0);
        this.f529a = context;
    }

    public static String a() {
        if (!g) {
            Log.e("OpenUDID", "Initialisation isn't done");
        }
        return f;
    }

    public static void a(Context context) {
        q qVar = new q(context);
        String string = qVar.d.getString("openudid", null);
        f = string;
        if (string == null) {
            qVar.f530b = context.getPackageManager().queryIntentServices(new Intent("org.OpenUDID.GETUDID"), 0);
            Log.d("OpenUDID", String.valueOf(qVar.f530b.size()) + " services matches OpenUDID");
            if (qVar.f530b != null) {
                qVar.c();
                return;
            }
            return;
        }
        Log.d("OpenUDID", "OpenUDID: " + f);
        g = true;
    }

    public static boolean b() {
        return g;
    }

    private void c() {
        if (this.f530b.size() > 0) {
            Log.d("OpenUDID", "Trying service " + ((Object) this.f530b.get(0).loadLabel(this.f529a.getPackageManager())));
            ServiceInfo serviceInfo = this.f530b.get(0).serviceInfo;
            Intent intent = new Intent();
            intent.setComponent(new ComponentName(serviceInfo.applicationInfo.packageName, serviceInfo.name));
            this.f529a.bindService(intent, this, 1);
            this.f530b.remove(0);
            return;
        }
        if (!this.c.isEmpty()) {
            TreeMap treeMap = new TreeMap(new r(this, (byte) 0));
            treeMap.putAll(this.c);
            f = (String) treeMap.firstKey();
        }
        if (f == null) {
            Log.d("OpenUDID", "Generating openUDID");
            String string = Settings.Secure.getString(this.f529a.getContentResolver(), "android_id");
            f = string;
            if (string == null || f.equals("9774d56d682e549c") || f.length() < 15) {
                f = new BigInteger(64, new SecureRandom()).toString(16);
            }
        }
        Log.d("OpenUDID", "OpenUDID: " + f);
        SharedPreferences.Editor edit = this.d.edit();
        edit.putString("openudid", f);
        edit.commit();
        g = true;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        String readString;
        try {
            Parcel obtain = Parcel.obtain();
            obtain.writeInt(this.e.nextInt());
            Parcel obtain2 = Parcel.obtain();
            iBinder.transact(1, Parcel.obtain(), obtain2, 0);
            if (obtain.readInt() == obtain2.readInt() && (readString = obtain2.readString()) != null) {
                Log.d("OpenUDID", "Received " + readString);
                if (this.c.containsKey(readString)) {
                    this.c.put(readString, Integer.valueOf(this.c.get(readString).intValue() + 1));
                } else {
                    this.c.put(readString, 1);
                }
            }
        } catch (RemoteException e2) {
            Log.e("OpenUDID", "RemoteException: " + e2.getMessage());
        }
        this.f529a.unbindService(this);
        c();
    }

    public final void onServiceDisconnected(ComponentName componentName) {
    }
}
