package com.b.a;

import android.content.Context;

final class u extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Context f536a;

    u(Context context) {
        this.f536a = context;
    }

    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.io.BufferedReader] */
    /* JADX WARN: Type inference failed for: r2v2, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* JADX WARN: Type inference failed for: r2v4, types: [java.io.BufferedReader] */
    /* JADX WARN: Type inference failed for: r2v5 */
    /* JADX WARN: Type inference failed for: r2v6 */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARN: Type inference failed for: r2v10 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a2 A[SYNTHETIC, Splitter:B:32:0x00a2] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a7 A[Catch:{ IOException -> 0x00ab }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00b7 A[SYNTHETIC, Splitter:B:45:0x00b7] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00bc A[Catch:{ IOException -> 0x00c0 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:21:0x0084=Splitter:B:21:0x0084, B:49:0x00bf=Splitter:B:49:0x00bf} */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r7 = this;
            r2 = 0
            java.lang.Thread r4 = com.b.a.t.c
            monitor-enter(r4)
            android.content.Context r0 = r7.f536a     // Catch:{ Exception -> 0x00d3, all -> 0x00b3 }
            java.lang.String r1 = "mzSdkProfilePrefs"
            r3 = 0
            android.content.SharedPreferences r5 = r0.getSharedPreferences(r1, r3)     // Catch:{ Exception -> 0x00d3, all -> 0x00b3 }
            java.lang.String r0 = "mzProfileURI"
            java.lang.String r1 = "http://s.mzfile.com/sdk/mz_sdk_config.xml"
            java.lang.String r0 = r5.getString(r0, r1)     // Catch:{ Exception -> 0x00d3, all -> 0x00b3 }
            java.net.URL r1 = new java.net.URL     // Catch:{ Exception -> 0x00d3, all -> 0x00b3 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x00d3, all -> 0x00b3 }
            java.net.URLConnection r0 = r1.openConnection()     // Catch:{ Exception -> 0x00d3, all -> 0x00b3 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00d3, all -> 0x00b3 }
            r1 = 5000(0x1388, float:7.006E-42)
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x00d3, all -> 0x00b3 }
            r1 = 0
            r0.setUseCaches(r1)     // Catch:{ Exception -> 0x00d3, all -> 0x00b3 }
            java.lang.String r1 = "GET"
            r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x00d3, all -> 0x00b3 }
            r0.connect()     // Catch:{ Exception -> 0x00d3, all -> 0x00b3 }
            int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x00d3, all -> 0x00b3 }
            if (r1 <= 0) goto L_0x00da
            int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x00d3, all -> 0x00b3 }
            r3 = 400(0x190, float:5.6E-43)
            if (r1 >= r3) goto L_0x00da
            java.io.InputStream r3 = r0.getInputStream()     // Catch:{ Exception -> 0x00d3, all -> 0x00b3 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00d6, all -> 0x00ca }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00d6, all -> 0x00ca }
            r0.<init>(r3)     // Catch:{ Exception -> 0x00d6, all -> 0x00ca }
            r1.<init>(r0)     // Catch:{ Exception -> 0x00d6, all -> 0x00ca }
            java.lang.StringBuffer r0 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x008a, all -> 0x00cc }
            r0.<init>()     // Catch:{ Exception -> 0x008a, all -> 0x00cc }
        L_0x0054:
            java.lang.String r2 = r1.readLine()     // Catch:{ Exception -> 0x008a, all -> 0x00cc }
            if (r2 != 0) goto L_0x0086
            android.content.SharedPreferences$Editor r2 = r5.edit()     // Catch:{ Exception -> 0x008a, all -> 0x00cc }
            java.lang.String r5 = "mzConfigFile"
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x008a, all -> 0x00cc }
            android.content.SharedPreferences$Editor r0 = r2.putString(r5, r0)     // Catch:{ Exception -> 0x008a, all -> 0x00cc }
            r0.commit()     // Catch:{ Exception -> 0x008a, all -> 0x00cc }
            android.content.Context r0 = r7.f536a     // Catch:{ Exception -> 0x008a, all -> 0x00cc }
            com.b.a.t.m(r0)     // Catch:{ Exception -> 0x008a, all -> 0x00cc }
            android.content.Context r0 = r7.f536a     // Catch:{ Exception -> 0x008a, all -> 0x00cc }
            com.b.a.f r0 = com.b.a.f.a(r0)     // Catch:{ Exception -> 0x008a, all -> 0x00cc }
            r0.b()     // Catch:{ Exception -> 0x008a, all -> 0x00cc }
            r2 = r1
        L_0x007a:
            if (r3 == 0) goto L_0x007f
            r3.close()     // Catch:{ IOException -> 0x00c5 }
        L_0x007f:
            if (r2 == 0) goto L_0x0084
            r2.close()     // Catch:{ IOException -> 0x00c5 }
        L_0x0084:
            monitor-exit(r4)     // Catch:{ all -> 0x00b0 }
            return
        L_0x0086:
            r0.append(r2)     // Catch:{ Exception -> 0x008a, all -> 0x00cc }
            goto L_0x0054
        L_0x008a:
            r0 = move-exception
            r2 = r3
        L_0x008c:
            java.lang.String r3 = "MZSDK:20141030"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00cf }
            java.lang.String r6 = " Exception:"
            r5.<init>(r6)     // Catch:{ all -> 0x00cf }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ all -> 0x00cf }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00cf }
            android.util.Log.d(r3, r0)     // Catch:{ all -> 0x00cf }
            if (r2 == 0) goto L_0x00a5
            r2.close()     // Catch:{ IOException -> 0x00ab }
        L_0x00a5:
            if (r1 == 0) goto L_0x0084
            r1.close()     // Catch:{ IOException -> 0x00ab }
            goto L_0x0084
        L_0x00ab:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00b0 }
            goto L_0x0084
        L_0x00b0:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x00b3:
            r0 = move-exception
            r3 = r2
        L_0x00b5:
            if (r3 == 0) goto L_0x00ba
            r3.close()     // Catch:{ IOException -> 0x00c0 }
        L_0x00ba:
            if (r2 == 0) goto L_0x00bf
            r2.close()     // Catch:{ IOException -> 0x00c0 }
        L_0x00bf:
            throw r0     // Catch:{ all -> 0x00b0 }
        L_0x00c0:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x00b0 }
            goto L_0x00bf
        L_0x00c5:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00b0 }
            goto L_0x0084
        L_0x00ca:
            r0 = move-exception
            goto L_0x00b5
        L_0x00cc:
            r0 = move-exception
            r2 = r1
            goto L_0x00b5
        L_0x00cf:
            r0 = move-exception
            r3 = r2
            r2 = r1
            goto L_0x00b5
        L_0x00d3:
            r0 = move-exception
            r1 = r2
            goto L_0x008c
        L_0x00d6:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x008c
        L_0x00da:
            r3 = r2
            goto L_0x007a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.b.a.u.run():void");
    }
}
