package com.b.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

final class e extends SQLiteOpenHelper {
    public e(d dVar, Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 6);
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS mzcaches (cacheId VARCHAR PRIMARY KEY, url VARCHAR, timestamp LONG, times INTEGER)");
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS mzcaches");
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS mzcaches (cacheId VARCHAR PRIMARY KEY, url VARCHAR, timestamp LONG, times INTEGER)");
    }
}
