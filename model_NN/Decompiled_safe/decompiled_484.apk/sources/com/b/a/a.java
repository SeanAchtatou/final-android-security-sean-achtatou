package com.b.a;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import com.qihoo.messenger.util.QDefine;
import java.util.List;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static Handler f504a = null;

    /* renamed from: b  reason: collision with root package name */
    private static HandlerThread f505b = null;
    private static long c;

    public static synchronized void a(Context context) {
        synchronized (a.class) {
            long a2 = g.a();
            if (m.a(context).l() != "0" && (c == 0 || a2 >= c)) {
                List<c> a3 = d.a(context).a();
                if (!(a3 == null || a3.size() == 0)) {
                    c = a2 + ((long) ((a3.size() * 10000) / QDefine.ONE_SECOND));
                    for (c a4 : a3) {
                        a(context, a4);
                    }
                }
            }
        }
    }

    private static void a(Context context, c cVar) {
        synchronized (a.class) {
            if (f505b == null) {
                HandlerThread handlerThread = new HandlerThread("MZMonitor");
                f505b = handlerThread;
                handlerThread.start();
                f504a = new Handler(f505b.getLooper());
            }
        }
        f504a.post(new b(context, cVar));
    }

    public static void a(Context context, String str) {
        a(context, new c(str));
    }
}
