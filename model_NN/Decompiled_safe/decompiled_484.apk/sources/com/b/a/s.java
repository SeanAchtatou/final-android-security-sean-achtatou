package com.b.a;

import android.content.Context;
import java.net.HttpURLConnection;
import java.net.URL;

final class s {

    /* renamed from: a  reason: collision with root package name */
    private c f532a;

    /* renamed from: b  reason: collision with root package name */
    private Context f533b;

    public s(Context context, c cVar) {
        this.f533b = context.getApplicationContext();
        this.f532a = cVar;
    }

    private static void a(String str) {
        if (str != null) {
            try {
                URL url = new URL(str);
                HttpURLConnection.setFollowRedirects(true);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                if (httpURLConnection != null) {
                    httpURLConnection.setDefaultUseCaches(false);
                    httpURLConnection.setUseCaches(false);
                    httpURLConnection.setConnectTimeout(5000);
                    httpURLConnection.setReadTimeout(5000);
                    httpURLConnection.connect();
                    httpURLConnection.getResponseCode();
                    httpURLConnection.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:17:0x0080 */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r2v2, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r2v3, types: [java.net.HttpURLConnection] */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r2v5 */
    /* JADX WARN: Type inference failed for: r2v6 */
    /* JADX WARN: Type inference failed for: r2v7, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r2v8 */
    /* JADX WARN: Type inference failed for: r2v9 */
    /* JADX WARN: Type inference failed for: r2v11 */
    /* JADX WARN: Type inference failed for: r2v12 */
    /* JADX WARN: Type inference failed for: r2v13 */
    /* JADX WARN: Type inference failed for: r2v14 */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0100, code lost:
        r1.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x010b, code lost:
        r1.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x010f, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0110, code lost:
        r7 = r1;
        r1 = r0;
        r0 = r7;
        r2 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x011b, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x011c, code lost:
        r7 = r1;
        r1 = r0;
        r0 = r7;
        r2 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0123, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0124, code lost:
        r7 = r1;
        r1 = r2;
        r2 = r0;
        r0 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x010b  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x010f A[ExcHandler: all (r1v22 'th' java.lang.Throwable A[CUSTOM_DECLARE]), PHI: r2 
      PHI: (r2v6 ?) = (r2v7 ?), (r2v0 ?), (r2v0 ?), (r2v0 ?) binds: [B:17:0x0080, B:11:0x0060, B:26:0x00a3, B:27:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:11:0x0060] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x011b A[ExcHandler: NullPointerException (r1v20 'e' java.lang.NullPointerException A[CUSTOM_DECLARE]), PHI: r2 
      PHI: (r2v5 ?) = (r2v7 ?), (r2v0 ?), (r2v0 ?), (r2v0 ?) binds: [B:17:0x0080, B:11:0x0060, B:26:0x00a3, B:27:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:11:0x0060] */
    /* JADX WARNING: Removed duplicated region for block: B:63:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r8 = this;
            r2 = 0
            android.content.Context r0 = r8.f533b
            com.b.a.d r3 = com.b.a.d.a(r0)
            android.content.Context r0 = r8.f533b     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            com.b.a.f r0 = com.b.a.f.a(r0)     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            com.b.a.c r1 = r8.f532a     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            java.net.URL r4 = r0.a(r1)     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            java.net.URLConnection r0 = r4.openConnection()     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            android.content.Context r1 = r8.f533b     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            com.b.a.m r1 = com.b.a.m.a(r1)     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            java.lang.String r1 = r1.l()     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            java.lang.String r5 = "1"
            boolean r1 = r1.equals(r5)     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            if (r1 == 0) goto L_0x009c
            r1 = 5000(0x1388, float:7.006E-42)
        L_0x002d:
            r0.setReadTimeout(r1)     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            r0.setConnectTimeout(r1)     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            r1 = 0
            r0.setUseCaches(r1)     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            r1 = 0
            r0.setInstanceFollowRedirects(r1)     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            android.content.Context r1 = r8.f533b     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            com.b.a.f r1 = com.b.a.f.a(r1)     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            com.b.a.h r1 = r1.a(r4)     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            java.lang.String r1 = r1.f516a     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            if (r1 == 0) goto L_0x005e
            java.lang.String r5 = "miaozhen"
            boolean r1 = r1.equals(r5)     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            if (r1 == 0) goto L_0x005e
            java.lang.String r1 = "X-MZ-UIC"
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            java.lang.String r4 = com.b.a.g.a(r4)     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
            r0.setRequestProperty(r1, r4)     // Catch:{ IOException -> 0x0120, NullPointerException -> 0x00dc, all -> 0x0104 }
        L_0x005e:
            if (r0 == 0) goto L_0x0080
            r0.connect()     // Catch:{ IOException -> 0x00ad, NullPointerException -> 0x011b, all -> 0x010f }
            int r1 = r0.getResponseCode()     // Catch:{ IOException -> 0x00ad, NullPointerException -> 0x011b, all -> 0x010f }
            if (r1 < 0) goto L_0x009f
            r4 = 400(0x190, float:5.6E-43)
            if (r1 >= r4) goto L_0x009f
            java.lang.String r1 = "MZSDK:20141030"
            java.lang.String r4 = "Request has been successfully sent"
            android.util.Log.d(r1, r4)     // Catch:{ IOException -> 0x00ad, NullPointerException -> 0x011b, all -> 0x010f }
            com.b.a.c r1 = r8.f532a     // Catch:{ IOException -> 0x00ad, NullPointerException -> 0x011b, all -> 0x010f }
            r4 = 1
            r3.a(r1, r4)     // Catch:{ IOException -> 0x00ad, NullPointerException -> 0x011b, all -> 0x010f }
            java.lang.String r1 = "Location"
            java.lang.String r2 = r0.getHeaderField(r1)     // Catch:{ IOException -> 0x00ad, NullPointerException -> 0x011b, all -> 0x010f }
        L_0x0080:
            android.content.Context r1 = r8.f533b     // Catch:{ IOException -> 0x0123, NullPointerException -> 0x011b, all -> 0x010f }
            boolean r1 = com.b.a.t.j(r1)     // Catch:{ IOException -> 0x0123, NullPointerException -> 0x011b, all -> 0x010f }
            if (r1 == 0) goto L_0x0093
            android.content.Context r1 = r8.f533b     // Catch:{ IOException -> 0x0123, NullPointerException -> 0x011b, all -> 0x010f }
            com.b.a.n r1 = com.b.a.n.a(r1)     // Catch:{ IOException -> 0x0123, NullPointerException -> 0x011b, all -> 0x010f }
            android.content.Context r4 = r8.f533b     // Catch:{ IOException -> 0x0123, NullPointerException -> 0x011b, all -> 0x010f }
            r1.b(r4)     // Catch:{ IOException -> 0x0123, NullPointerException -> 0x011b, all -> 0x010f }
        L_0x0093:
            a(r2)
            if (r0 == 0) goto L_0x009b
            r0.disconnect()
        L_0x009b:
            return
        L_0x009c:
            r1 = 10000(0x2710, float:1.4013E-41)
            goto L_0x002d
        L_0x009f:
            java.lang.String r1 = "MZSDK:20141030"
            java.lang.String r4 = "Failed to send request"
            android.util.Log.d(r1, r4)     // Catch:{ IOException -> 0x00ad, NullPointerException -> 0x011b, all -> 0x010f }
            com.b.a.c r1 = r8.f532a     // Catch:{ IOException -> 0x00ad, NullPointerException -> 0x011b, all -> 0x010f }
            r4 = 0
            r3.a(r1, r4)     // Catch:{ IOException -> 0x00ad, NullPointerException -> 0x011b, all -> 0x010f }
            goto L_0x0080
        L_0x00ad:
            r1 = move-exception
            r7 = r1
            r1 = r2
            r2 = r0
            r0 = r7
        L_0x00b2:
            r0.printStackTrace()     // Catch:{ all -> 0x0114 }
            java.lang.String r4 = "MZSDK:20141030"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0114 }
            java.lang.String r6 = "Connection Error:"
            r5.<init>(r6)     // Catch:{ all -> 0x0114 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0114 }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ all -> 0x0114 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0114 }
            android.util.Log.d(r4, r0)     // Catch:{ all -> 0x0114 }
            com.b.a.c r0 = r8.f532a     // Catch:{ all -> 0x0114 }
            r4 = 0
            r3.a(r0, r4)     // Catch:{ all -> 0x0114 }
            a(r1)
            if (r2 == 0) goto L_0x009b
            r2.disconnect()
            goto L_0x009b
        L_0x00dc:
            r0 = move-exception
            r1 = r2
        L_0x00de:
            r0.printStackTrace()     // Catch:{ all -> 0x0119 }
            java.lang.String r0 = "MZSDK:20141030"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0119 }
            java.lang.String r4 = "NullPointerException Error:"
            r3.<init>(r4)     // Catch:{ all -> 0x0119 }
            com.b.a.c r4 = r8.f532a     // Catch:{ all -> 0x0119 }
            java.lang.String r4 = r4.a()     // Catch:{ all -> 0x0119 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0119 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0119 }
            android.util.Log.d(r0, r3)     // Catch:{ all -> 0x0119 }
            a(r2)
            if (r1 == 0) goto L_0x009b
            r1.disconnect()
            goto L_0x009b
        L_0x0104:
            r0 = move-exception
            r1 = r2
        L_0x0106:
            a(r2)
            if (r1 == 0) goto L_0x010e
            r1.disconnect()
        L_0x010e:
            throw r0
        L_0x010f:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x0106
        L_0x0114:
            r0 = move-exception
            r7 = r1
            r1 = r2
            r2 = r7
            goto L_0x0106
        L_0x0119:
            r0 = move-exception
            goto L_0x0106
        L_0x011b:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x00de
        L_0x0120:
            r0 = move-exception
            r1 = r2
            goto L_0x00b2
        L_0x0123:
            r1 = move-exception
            r7 = r1
            r1 = r2
            r2 = r0
            r0 = r7
            goto L_0x00b2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.b.a.s.a():void");
    }
}
