package com.b.a;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

final class o implements LocationListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ n f527a;

    o(n nVar) {
        this.f527a = nVar;
    }

    public final void onLocationChanged(Location location) {
        n.a(this.f527a, location);
    }

    public final void onProviderDisabled(String str) {
        this.f527a.a();
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
