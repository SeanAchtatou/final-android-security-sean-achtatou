package com.b.a;

import android.content.Context;
import android.util.Log;
import android.util.Xml;
import com.mediav.ads.sdk.adcore.Config;
import com.qihoo.dynamic.util.Md5Util;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

class f {

    /* renamed from: a  reason: collision with root package name */
    private static f f512a;

    /* renamed from: b  reason: collision with root package name */
    private Context f513b;
    private i c;
    private String d;

    private f(Context context) {
        this.f513b = context;
    }

    public static f a(Context context) {
        synchronized (f.class) {
            if (f512a == null) {
                f512a = new f(context.getApplicationContext());
            }
        }
        return f512a;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private i a(InputStream inputStream) {
        g gVar;
        h hVar;
        j jVar;
        i iVar = new i(this);
        try {
            XmlPullParser newPullParser = Xml.newPullParser();
            newPullParser.setInput(inputStream, Md5Util.DEFAULT_CHARSET);
            int eventType = newPullParser.getEventType();
            boolean z = false;
            j jVar2 = null;
            h hVar2 = null;
            g gVar2 = null;
            while (eventType != 1) {
                switch (eventType) {
                    case 0:
                        gVar = gVar2;
                        j jVar3 = jVar2;
                        hVar = hVar2;
                        jVar = jVar3;
                        break;
                    case 1:
                    default:
                        gVar = gVar2;
                        j jVar4 = jVar2;
                        hVar = hVar2;
                        jVar = jVar4;
                        break;
                    case 2:
                        this.d = newPullParser.getName();
                        if (a("companies")) {
                            iVar.f518a = new ArrayList();
                        }
                        if (iVar.f518a != null && a("company")) {
                            hVar2 = new h(this);
                        }
                        if (hVar2 != null) {
                            if (!a("name") || z || jVar2 != null) {
                                if (!a("domain")) {
                                    if (!a("separator")) {
                                        if (!a("equalizer")) {
                                            if (!a("redirect")) {
                                                if (!a("useSecond")) {
                                                    if (!a("status")) {
                                                        if (!a("arguments")) {
                                                            if (!z) {
                                                                if (!a("events")) {
                                                                    if (!a("event")) {
                                                                        if (jVar2 != null) {
                                                                            if (!a("type")) {
                                                                                if (!a("name")) {
                                                                                    if (!a("value")) {
                                                                                        if (a("urlEncode")) {
                                                                                            String upperCase = newPullParser.nextText().toUpperCase();
                                                                                            if (upperCase.equals("NO") || upperCase.equals("FALSE")) {
                                                                                                jVar2.d = false;
                                                                                                gVar = gVar2;
                                                                                                j jVar5 = jVar2;
                                                                                                hVar = hVar2;
                                                                                                jVar = jVar5;
                                                                                                break;
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        jVar2.c = newPullParser.nextText();
                                                                                        gVar = gVar2;
                                                                                        j jVar6 = jVar2;
                                                                                        hVar = hVar2;
                                                                                        jVar = jVar6;
                                                                                        break;
                                                                                    }
                                                                                } else {
                                                                                    jVar2.f520b = newPullParser.nextText();
                                                                                    gVar = gVar2;
                                                                                    j jVar7 = jVar2;
                                                                                    hVar = hVar2;
                                                                                    jVar = jVar7;
                                                                                    break;
                                                                                }
                                                                            } else {
                                                                                jVar2.f519a = newPullParser.nextText();
                                                                                gVar = gVar2;
                                                                                j jVar8 = jVar2;
                                                                                hVar = hVar2;
                                                                                jVar = jVar8;
                                                                                break;
                                                                            }
                                                                        }
                                                                    } else {
                                                                        gVar = gVar2;
                                                                        hVar = hVar2;
                                                                        jVar = new j(this);
                                                                        break;
                                                                    }
                                                                } else {
                                                                    hVar2.h = new ArrayList();
                                                                    gVar = gVar2;
                                                                    j jVar9 = jVar2;
                                                                    hVar = hVar2;
                                                                    jVar = jVar9;
                                                                    break;
                                                                }
                                                            } else if (!a("encrypt")) {
                                                                if (!a("name")) {
                                                                    if (!a("urlEncode")) {
                                                                        g gVar3 = new g(this);
                                                                        gVar3.f514a = newPullParser.getName();
                                                                        gVar = gVar3;
                                                                        j jVar10 = jVar2;
                                                                        hVar = hVar2;
                                                                        jVar = jVar10;
                                                                        break;
                                                                    } else {
                                                                        String upperCase2 = newPullParser.nextText().toUpperCase();
                                                                        if (upperCase2.equals("NO") || upperCase2.equals("FALSE")) {
                                                                            gVar2.d = false;
                                                                            gVar = gVar2;
                                                                            j jVar11 = jVar2;
                                                                            hVar = hVar2;
                                                                            jVar = jVar11;
                                                                            break;
                                                                        }
                                                                    }
                                                                } else {
                                                                    gVar2.f515b = newPullParser.nextText();
                                                                    gVar = gVar2;
                                                                    j jVar12 = jVar2;
                                                                    hVar = hVar2;
                                                                    jVar = jVar12;
                                                                    break;
                                                                }
                                                            } else {
                                                                gVar2.c = newPullParser.nextText();
                                                                gVar = gVar2;
                                                                j jVar13 = jVar2;
                                                                hVar = hVar2;
                                                                jVar = jVar13;
                                                                break;
                                                            }
                                                        } else {
                                                            hVar2.g = new ArrayList();
                                                            z = true;
                                                            gVar = gVar2;
                                                            j jVar14 = jVar2;
                                                            hVar = hVar2;
                                                            jVar = jVar14;
                                                            break;
                                                        }
                                                    } else {
                                                        String upperCase3 = newPullParser.nextText().toUpperCase();
                                                        if (upperCase3.equals("YES") || upperCase3.equals("TRUE")) {
                                                            hVar2.i = true;
                                                            gVar = gVar2;
                                                            j jVar15 = jVar2;
                                                            hVar = hVar2;
                                                            jVar = jVar15;
                                                            break;
                                                        }
                                                    }
                                                } else {
                                                    String upperCase4 = newPullParser.nextText().toUpperCase();
                                                    if (upperCase4.equals("NO") || upperCase4.equals("FALSE")) {
                                                        hVar2.j = false;
                                                        gVar = gVar2;
                                                        j jVar16 = jVar2;
                                                        hVar = hVar2;
                                                        jVar = jVar16;
                                                        break;
                                                    }
                                                }
                                            } else {
                                                hVar2.e = newPullParser.nextText();
                                                gVar = gVar2;
                                                j jVar17 = jVar2;
                                                hVar = hVar2;
                                                jVar = jVar17;
                                                break;
                                            }
                                        } else {
                                            hVar2.d = newPullParser.nextText();
                                            gVar = gVar2;
                                            j jVar18 = jVar2;
                                            hVar = hVar2;
                                            jVar = jVar18;
                                            break;
                                        }
                                    } else {
                                        hVar2.c = newPullParser.nextText();
                                        gVar = gVar2;
                                        j jVar19 = jVar2;
                                        hVar = hVar2;
                                        jVar = jVar19;
                                        break;
                                    }
                                } else {
                                    if (hVar2.f517b == null) {
                                        hVar2.f517b = new ArrayList();
                                    }
                                    hVar2.f517b.add(newPullParser.nextText());
                                    gVar = gVar2;
                                    j jVar20 = jVar2;
                                    hVar = hVar2;
                                    jVar = jVar20;
                                    break;
                                }
                            } else {
                                hVar2.f516a = newPullParser.nextText();
                                gVar = gVar2;
                                j jVar21 = jVar2;
                                hVar = hVar2;
                                jVar = jVar21;
                                break;
                            }
                        }
                        gVar = gVar2;
                        j jVar42 = jVar2;
                        hVar = hVar2;
                        jVar = jVar42;
                        break;
                    case 3:
                        this.d = newPullParser.getName();
                        if (a("company")) {
                            iVar.f518a.add(hVar2);
                            hVar2 = null;
                        }
                        if (a("arguments")) {
                            z = false;
                        }
                        if (z && !a("encrypt") && !a("name") && !a("urlEncode") && !a("useSecond")) {
                            hVar2.g.add(gVar2);
                            gVar2 = null;
                        }
                        if (a("event")) {
                            hVar2.h.add(jVar2);
                            gVar = gVar2;
                            hVar = hVar2;
                            jVar = null;
                            break;
                        }
                        gVar = gVar2;
                        j jVar422 = jVar2;
                        hVar = hVar2;
                        jVar = jVar422;
                        break;
                }
                gVar2 = gVar;
                eventType = newPullParser.next();
                j jVar22 = jVar;
                hVar2 = hVar;
                jVar2 = jVar22;
            }
            this.d = null;
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (XmlPullParserException e2) {
            e2.printStackTrace();
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            }
        } catch (IOException e4) {
            e4.printStackTrace();
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e5) {
                    e5.printStackTrace();
                }
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e6) {
                    e6.printStackTrace();
                }
            }
            throw th;
        }
        return iVar;
    }

    private static j a(String str, h hVar) {
        if (!(str == null || hVar.h == null)) {
            for (j next : hVar.h) {
                if (str.equals(next.f519a)) {
                    return next;
                }
            }
        }
        return null;
    }

    private String a(c cVar, h hVar) {
        m a2 = m.a(this.f513b);
        StringBuilder sb = new StringBuilder();
        String str = hVar.c;
        String str2 = hVar.d;
        try {
            sb.append(String.valueOf(str) + "mv" + str2 + URLEncoder.encode("a3.2", Md5Util.DEFAULT_CHARSET));
            sb.append(String.valueOf(str) + "mr" + str2 + URLEncoder.encode(new StringBuilder().append(cVar.h()).toString(), Md5Util.DEFAULT_CHARSET));
            sb.append(String.valueOf(str) + "mc" + str2 + URLEncoder.encode(new StringBuilder().append(t.c(this.f513b)).toString(), Md5Util.DEFAULT_CHARSET));
            sb.append(String.valueOf(str) + "mw" + str2 + URLEncoder.encode(a2.l(), Md5Util.DEFAULT_CHARSET));
            String m = a2.m();
            if (m != null) {
                sb.append(String.valueOf(hVar.c) + "mj" + hVar.d + URLEncoder.encode(m, Md5Util.DEFAULT_CHARSET));
            }
            if (cVar.h() > 0) {
                sb.append(String.valueOf(str) + "mu" + str2 + URLEncoder.encode(new StringBuilder(String.valueOf(g.a() - cVar.g())).toString(), Md5Util.DEFAULT_CHARSET));
            } else {
                sb.append(String.valueOf(str) + "mu" + str2 + URLEncoder.encode("0", Md5Util.DEFAULT_CHARSET));
            }
            String g = t.g(this.f513b);
            if (g != null && !g.contains("UNKNOWN")) {
                sb.append(String.valueOf(str) + "mg" + str2 + URLEncoder.encode(g, Md5Util.DEFAULT_CHARSET));
            }
            String h = a2.h();
            if (h != null && !h.equals("")) {
                sb.append(String.valueOf(str) + "m6" + str2 + URLEncoder.encode(g.a(h).toUpperCase(), Md5Util.DEFAULT_CHARSET));
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    private String a(String str, c cVar, h hVar) {
        List<g> list = hVar.g;
        String str2 = hVar.c;
        String str3 = hVar.d;
        for (g next : list) {
            if ((!next.f514a.equals("PANELID") || cVar.b() != null) && ((!next.f514a.equals("MUID") || cVar.c() != null) && ((!next.f514a.equals("IESID") || cVar.i() != null) && str.contains(String.valueOf(str2) + next.f515b + str3)))) {
                str = str.replaceAll(String.valueOf(str2) + next.f515b + str3 + "[^" + str2 + "]*", "");
            }
        }
        j a2 = a(cVar.d(), hVar);
        return a2 != null ? str.replaceAll(String.valueOf(str2) + a2.f520b + str3 + "[^" + str2 + "]*", "") : str;
    }

    private static String a(String str, g gVar) {
        if (str == null || str.equals("")) {
            return "";
        }
        if (gVar.c.equals("md5")) {
            str = g.a(str);
        } else if (gVar.c.equals("sha1")) {
            str = g.b(str);
        }
        if (!gVar.d) {
            return str;
        }
        try {
            return URLEncoder.encode(str, Md5Util.DEFAULT_CHARSET);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return str;
        }
    }

    private boolean a(String str) {
        return str.equals(this.d);
    }

    public final h a(URL url) {
        if (!(this.c == null || this.c.f518a == null)) {
            String host = url.getHost();
            for (h next : this.c.f518a) {
                Iterator<String> it = next.f517b.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (host.endsWith(it.next())) {
                            return next;
                        }
                    }
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final synchronized i a() {
        return this.c;
    }

    public final synchronized URL a(c cVar) {
        URL url;
        Exception exc;
        String str;
        m a2 = m.a(this.f513b);
        StringBuilder sb = new StringBuilder();
        String a3 = cVar.a();
        try {
            URL url2 = new URL(a3);
            try {
                h a4 = a(url2);
                if (a4 == null || !a4.i) {
                    url = url2;
                } else {
                    String substring = (a4.e == null || a4.e.equals("") || !a3.contains(a4.e)) ? "" : a3.substring(a3.indexOf(a4.e));
                    if (a4.e != null && !a4.e.equals("") && a3.contains(a4.e)) {
                        a3 = a3.substring(0, a3.indexOf(a4.e));
                    }
                    String a5 = a(a3, cVar, a4);
                    sb.append(a5);
                    if (a4.f516a.equals("miaozhen")) {
                        sb.append(a(cVar, a4));
                    }
                    for (g next : a4.g) {
                        String str2 = String.valueOf(a4.c) + next.f515b + a4.d;
                        String str3 = next.f514a;
                        String str4 = "";
                        if (str3.equals("APPNAME")) {
                            str4 = a2.d();
                        } else if (str3.equals("PACKAGENAME")) {
                            str4 = a2.e();
                        } else if (str3.equals("ANDROIDID")) {
                            str4 = a2.c();
                        } else if (str3.equals("MAC")) {
                            String h = a2.h();
                            if (h != null && !h.equals("")) {
                                str4 = h.replaceAll(":", "").toUpperCase();
                            }
                        } else if (str3.equals("IMEI")) {
                            str4 = a2.b();
                        } else if (str3.equals("WIFI")) {
                            str4 = a2.k() ? Config.CHANNEL_ID : "0";
                        } else if (str3.equals("MODEL")) {
                            str4 = m.a();
                        } else if (str3.equals("TS")) {
                            long f = cVar.f();
                            str4 = a4.j ? new StringBuilder(String.valueOf(f / 1000)).toString() : new StringBuilder(String.valueOf(f)).toString();
                        } else if (str3.equals("LOCATION")) {
                            String h2 = t.h(this.f513b);
                            if (!h2.contains("UNKNOWN")) {
                                str4 = h2;
                            }
                        } else if (str3.equals("OPENUDID")) {
                            str4 = a2.f();
                        } else if (str3.equals("OS")) {
                            str4 = "0";
                        } else if (str3.equals("OSVS")) {
                            str4 = m.g();
                        } else if (str3.equals("SCWH")) {
                            str4 = a2.i();
                        } else if (str3.equals("ODIN")) {
                            str4 = a2.j();
                        } else if (str3.equals("PANELID")) {
                            str4 = cVar.b();
                        } else if (str3.equals("MUID")) {
                            str4 = cVar.c();
                        } else if (str3.equals("IESID")) {
                            str4 = cVar.i();
                        } else if (str3.equals("CARRIER")) {
                            str4 = m.b(this.f513b);
                        } else if (str3.equals("LACOLE")) {
                            str4 = m.n();
                        } else if (str3.equals("IP")) {
                            Context context = this.f513b;
                            str4 = m.o();
                        } else if (str3.equals("SIGNATURE")) {
                            if (a4.f == null) {
                                a4.f = next.f515b;
                            }
                        }
                        String a6 = a(str4, next);
                        if (str3.equals("PANELID") || str3.equals("MUID") || str3.equals("IESID")) {
                            if (!a5.contains(str2) && (!a6.equals("") || !a4.f516a.equals("miaozhen"))) {
                                a5 = String.valueOf(a5) + str2 + a6;
                            }
                        }
                        if (!a6.equals("") || !a4.f516a.equals("miaozhen")) {
                            sb.append(String.valueOf(str2) + a6);
                        }
                    }
                    if (cVar.d() != null) {
                        String str5 = a4.c;
                        String encode = URLEncoder.encode(cVar.d(), Md5Util.DEFAULT_CHARSET);
                        j a7 = a(cVar.d(), a4);
                        if (a7 != null) {
                            str5 = String.valueOf(str5) + a7.f520b + a4.d;
                            encode = a7.d ? URLEncoder.encode(a7.c, Md5Util.DEFAULT_CHARSET) : a7.c;
                        }
                        sb.append(String.valueOf(str5) + encode);
                        str = String.valueOf(a5) + str5 + encode;
                    } else {
                        str = a5;
                    }
                    if (a4.f != null) {
                        sb.append(String.valueOf(a4.c) + a4.f + a4.d + v.a(this.f513b, sb.toString()));
                    }
                    cVar.a(String.valueOf(str) + substring);
                    Log.d("MZSDK:20141030", " url:" + str + substring);
                    url = new URL(sb.append(substring).toString());
                }
            } catch (Exception e) {
                exc = e;
                url = url2;
                Log.d("MZSDK:20141030", " Exception:" + exc);
                return url;
            }
        } catch (Exception e2) {
            Exception exc2 = e2;
            url = null;
            exc = exc2;
            Log.d("MZSDK:20141030", " Exception:" + exc);
            return url;
        }
        return url;
    }

    public final synchronized void b() {
        try {
            this.c = a(new ByteArrayInputStream(t.k(this.f513b).getBytes(Md5Util.DEFAULT_CHARSET)));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return;
    }
}
