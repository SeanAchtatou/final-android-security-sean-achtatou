package com.b.a;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.mediav.ads.sdk.adcore.Config;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.Locale;

public class m {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f523a = false;
    private static m c = null;

    /* renamed from: b  reason: collision with root package name */
    private Context f524b;

    private m(Context context) {
        this.f524b = context;
    }

    public static m a(Context context) {
        m mVar;
        synchronized (m.class) {
            if (c == null) {
                c = new m(context.getApplicationContext());
            }
            mVar = c;
        }
        return mVar;
    }

    public static String a() {
        return Build.MODEL;
    }

    public static String b(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager != null) {
            String networkOperatorName = telephonyManager.getNetworkOperatorName();
            if (telephonyManager.getNetworkOperatorName() != null && !telephonyManager.getNetworkOperatorName().equals("")) {
                return networkOperatorName;
            }
        }
        return null;
    }

    public static String g() {
        return Build.VERSION.RELEASE;
    }

    public static String n() {
        Locale locale = Locale.getDefault();
        return String.valueOf(locale.getLanguage()) + "_" + locale.getCountry();
    }

    public static String o() {
        try {
            Enumeration<InetAddress> inetAddresses = NetworkInterface.getNetworkInterfaces().nextElement().getInetAddresses();
            while (inetAddresses.hasMoreElements()) {
                InetAddress nextElement = inetAddresses.nextElement();
                if (!nextElement.isLinkLocalAddress()) {
                    return nextElement.getHostAddress();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public final String b() {
        return ((TelephonyManager) this.f524b.getSystemService("phone")).getDeviceId();
    }

    public final String c() {
        return Settings.Secure.getString(this.f524b.getContentResolver(), "android_id");
    }

    public final String d() {
        try {
            ApplicationInfo applicationInfo = this.f524b.getPackageManager().getApplicationInfo(this.f524b.getPackageName(), 128);
            if (applicationInfo == null) {
                return "";
            }
            if (applicationInfo.labelRes != 0) {
                return this.f524b.getResources().getString(applicationInfo.labelRes);
            }
            if (applicationInfo.nonLocalizedLabel == null) {
                return null;
            }
            return applicationInfo.nonLocalizedLabel.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public final String e() {
        PackageInfo packageInfo;
        try {
            PackageManager packageManager = this.f524b.getPackageManager();
            return (packageManager == null || (packageInfo = packageManager.getPackageInfo(this.f524b.getPackageName(), 0)) == null) ? "" : packageInfo.packageName;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /* access modifiers changed from: protected */
    public final String f() {
        q.a(this.f524b);
        return q.b() ? q.a() : "";
    }

    public final String h() {
        WifiInfo connectionInfo = ((WifiManager) this.f524b.getSystemService("wifi")).getConnectionInfo();
        return connectionInfo != null ? connectionInfo.getMacAddress() : "";
    }

    public final String i() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) this.f524b.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return String.valueOf(displayMetrics.widthPixels) + "x" + displayMetrics.heightPixels;
    }

    public final String j() {
        String string;
        try {
            string = Settings.System.getString(this.f524b.getContentResolver(), "android_id");
        } catch (Exception e) {
            try {
                string = Settings.System.getString(this.f524b.getContentResolver(), "android_id");
            } catch (Exception e2) {
                return null;
            }
        }
        return g.b(string);
    }

    public final boolean k() {
        return ((ConnectivityManager) this.f524b.getSystemService("connectivity")).getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED;
    }

    public final String l() {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.f524b.getSystemService("connectivity")).getActiveNetworkInfo();
            return activeNetworkInfo != null ? activeNetworkInfo.getType() == 1 ? Config.CHANNEL_ID : "2" : "0";
        } catch (Exception e) {
            e.printStackTrace();
            return "0";
        }
    }

    public final String m() {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.f524b.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.getType() == 1) {
                WifiInfo connectionInfo = ((WifiManager) this.f524b.getSystemService("wifi")).getConnectionInfo();
                return connectionInfo == null ? "NULL" : connectionInfo.getSSID();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
