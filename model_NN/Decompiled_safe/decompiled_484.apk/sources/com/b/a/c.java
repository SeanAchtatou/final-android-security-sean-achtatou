package com.b.a;

import android.content.ContentValues;
import com.qihoo360.daily.activity.SearchActivity;

final class c {

    /* renamed from: a  reason: collision with root package name */
    private String f508a;

    /* renamed from: b  reason: collision with root package name */
    private String f509b;
    private String c;
    private String d;
    private String e;
    private String f;
    private long g;
    private int h;

    public c() {
    }

    public c(String str) {
        this.f509b = str;
        this.g = System.currentTimeMillis();
        this.h = 0;
        this.f508a = g.a(String.valueOf(str) + this.g + ((int) (Math.random() * 10000.0d)));
    }

    public final String a() {
        return this.f509b;
    }

    public final void a(int i) {
        this.h = i;
    }

    public final void a(long j) {
        this.g = j;
    }

    public final void a(String str) {
        this.f509b = str;
    }

    public final String b() {
        return this.c;
    }

    public final void b(String str) {
        this.f508a = str;
    }

    public final String c() {
        return this.d;
    }

    public final String d() {
        return this.e;
    }

    public final String e() {
        return this.f508a;
    }

    public final long f() {
        return this.g;
    }

    public final long g() {
        return this.g / 1000;
    }

    public final int h() {
        return this.h;
    }

    public final String i() {
        return this.f;
    }

    public final ContentValues j() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("cacheId", this.f508a);
        contentValues.put(SearchActivity.TAG_URL, this.f509b);
        contentValues.put("timestamp", Long.valueOf(this.g));
        contentValues.put("times", Integer.valueOf(this.h));
        return contentValues;
    }

    public final String toString() {
        return "cacheId: " + this.f508a + ", url: " + this.f509b + ", eventType:" + this.e + ", userId: " + this.d + ", panelId: " + this.c + ", timestamp: " + this.g + ", times: " + this.h;
    }
}
