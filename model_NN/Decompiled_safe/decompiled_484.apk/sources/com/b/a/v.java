package com.b.a;

import android.content.Context;

public final class v {

    /* renamed from: a  reason: collision with root package name */
    private static String[] f537a = {"ec0fe20119c925a8", "d304fd244a67db36", "5aeae0e31c86ab84", "6c64a61e7edb9b54", "3b044c3507f0899c", "ecbe51ab2c79224d", "bdb4a3a756759e1c", "0f29a61dac5c227e", "8c77f8b8f5a8f2e9", "0e000ebd6e211ef4", "5bcbcbd118632aff", "381712371a7e869e", "5ff31aaf8e07b74d", "55a3ac9a01d1d2b0", "2317d45ce0048f87", "c03a3e2a68d851f0"};

    public static String a(Context context, String str) {
        m a2 = m.a(context);
        long a3 = g.a();
        String e = a2.e();
        StringBuffer stringBuffer = new StringBuffer();
        StringBuffer stringBuffer2 = new StringBuffer();
        stringBuffer2.append(str.toLowerCase());
        stringBuffer2.append(f537a[(int) (a3 % 16)]);
        stringBuffer2.append(e.toLowerCase());
        stringBuffer.append(g.a(stringBuffer2.toString()));
        stringBuffer.append("v");
        stringBuffer.append(t.d(context));
        stringBuffer.append("t");
        stringBuffer.append(a3);
        stringBuffer.append("k");
        stringBuffer.append(e.toLowerCase());
        return stringBuffer.toString();
    }
}
