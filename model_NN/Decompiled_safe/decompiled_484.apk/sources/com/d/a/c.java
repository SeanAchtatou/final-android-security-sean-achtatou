package com.d.a;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.ViewConfiguration;
import com.mediav.ads.sdk.adcore.Config;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f554a;

    /* renamed from: b  reason: collision with root package name */
    private final boolean f555b;
    private final int c;
    private final int d;
    private final boolean e;
    private final int f;
    private final int g;
    private final boolean h;
    private final float i;

    private c(Activity activity, boolean z, boolean z2) {
        boolean z3 = true;
        Resources resources = activity.getResources();
        this.h = resources.getConfiguration().orientation == 1;
        this.i = a(activity);
        this.c = a(resources, "status_bar_height");
        this.d = a((Context) activity);
        this.f = b(activity);
        this.g = c(activity);
        this.e = this.f <= 0 ? false : z3;
        this.f554a = z;
        this.f555b = z2;
    }

    @SuppressLint({"NewApi"})
    private float a(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        if (Build.VERSION.SDK_INT >= 16) {
            activity.getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
        } else {
            activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        }
        return Math.min(((float) displayMetrics.widthPixels) / displayMetrics.density, ((float) displayMetrics.heightPixels) / displayMetrics.density);
    }

    @TargetApi(14)
    private int a(Context context) {
        if (Build.VERSION.SDK_INT < 14) {
            return 0;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(16843499, typedValue, true);
        return context.getResources().getDimensionPixelSize(typedValue.resourceId);
    }

    private int a(Resources resources, String str) {
        int identifier = resources.getIdentifier(str, "dimen", "android");
        if (identifier > 0) {
            return resources.getDimensionPixelSize(identifier);
        }
        return 0;
    }

    @TargetApi(14)
    private int b(Context context) {
        Resources resources = context.getResources();
        if (Build.VERSION.SDK_INT < 14 || !d(context)) {
            return 0;
        }
        return a(resources, this.h ? "navigation_bar_height" : "navigation_bar_height_landscape");
    }

    @TargetApi(14)
    private int c(Context context) {
        Resources resources = context.getResources();
        if (Build.VERSION.SDK_INT < 14 || !d(context)) {
            return 0;
        }
        return a(resources, "navigation_bar_width");
    }

    @TargetApi(14)
    private boolean d(Context context) {
        boolean z = true;
        Resources resources = context.getResources();
        int identifier = resources.getIdentifier("config_showNavigationBar", "bool", "android");
        if (identifier != 0) {
            boolean z2 = resources.getBoolean(identifier);
            if (Config.CHANNEL_ID.equals(a.f552a)) {
                return false;
            }
            if ("0".equals(a.f552a)) {
                return true;
            }
            return z2;
        }
        if (ViewConfiguration.get(context).hasPermanentMenuKey()) {
            z = false;
        }
        return z;
    }

    public int a(boolean z) {
        int i2 = 0;
        int i3 = this.f554a ? this.c : 0;
        if (z) {
            i2 = this.d;
        }
        return i2 + i3;
    }

    public boolean a() {
        return this.i >= 600.0f || this.h;
    }

    public int b() {
        return this.c;
    }

    public boolean c() {
        return this.e;
    }

    public int d() {
        return this.f;
    }

    public int e() {
        return this.g;
    }
}
