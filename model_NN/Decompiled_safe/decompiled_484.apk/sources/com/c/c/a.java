package com.c.c;

import android.view.View;

public final class a {
    public static float a(View view) {
        return com.c.c.a.a.f550a ? com.c.c.a.a.a(view).m() : b.a(view);
    }

    public static void a(View view, float f) {
        if (com.c.c.a.a.f550a) {
            com.c.c.a.a.a(view).a(f);
        } else {
            b.a(view, f);
        }
    }

    public static void b(View view, float f) {
        if (com.c.c.a.a.f550a) {
            com.c.c.a.a.a(view).g(f);
        } else {
            b.b(view, f);
        }
    }

    public static void c(View view, float f) {
        if (com.c.c.a.a.f550a) {
            com.c.c.a.a.a(view).h(f);
        } else {
            b.c(view, f);
        }
    }

    public static void d(View view, float f) {
        if (com.c.c.a.a.f550a) {
            com.c.c.a.a.a(view).k(f);
        } else {
            b.d(view, f);
        }
    }
}
