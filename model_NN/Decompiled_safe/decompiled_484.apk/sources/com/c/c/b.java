package com.c.c;

import android.view.View;

final class b {
    static float a(View view) {
        return view.getX();
    }

    static void a(View view, float f) {
        view.setAlpha(f);
    }

    static void b(View view, float f) {
        view.setScaleX(f);
    }

    static void c(View view, float f) {
        view.setScaleY(f);
    }

    static void d(View view, float f) {
        view.setX(f);
    }
}
