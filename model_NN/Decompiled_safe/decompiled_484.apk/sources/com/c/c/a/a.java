package com.c.c.a;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.os.Build;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

public final class a extends Animation {

    /* renamed from: a  reason: collision with root package name */
    public static final boolean f550a = (Integer.valueOf(Build.VERSION.SDK).intValue() < 11);

    /* renamed from: b  reason: collision with root package name */
    private static final WeakHashMap<View, a> f551b = new WeakHashMap<>();
    private final WeakReference<View> c;
    private final Camera d = new Camera();
    private boolean e;
    private float f = 1.0f;
    private float g;
    private float h;
    private float i;
    private float j;
    private float k;
    private float l = 1.0f;
    private float m = 1.0f;
    private float n;
    private float o;
    private final RectF p = new RectF();
    private final RectF q = new RectF();
    private final Matrix r = new Matrix();

    private a(View view) {
        setDuration(0);
        setFillAfter(true);
        view.setAnimation(this);
        this.c = new WeakReference<>(view);
    }

    public static a a(View view) {
        a aVar = f551b.get(view);
        if (aVar != null && aVar == view.getAnimation()) {
            return aVar;
        }
        a aVar2 = new a(view);
        f551b.put(view, aVar2);
        return aVar2;
    }

    private void a(Matrix matrix, View view) {
        float width = (float) view.getWidth();
        float height = (float) view.getHeight();
        boolean z = this.e;
        float f2 = z ? this.g : width / 2.0f;
        float f3 = z ? this.h : height / 2.0f;
        float f4 = this.i;
        float f5 = this.j;
        float f6 = this.k;
        if (!(f4 == 0.0f && f5 == 0.0f && f6 == 0.0f)) {
            Camera camera = this.d;
            camera.save();
            camera.rotateX(f4);
            camera.rotateY(f5);
            camera.rotateZ(-f6);
            camera.getMatrix(matrix);
            camera.restore();
            matrix.preTranslate(-f2, -f3);
            matrix.postTranslate(f2, f3);
        }
        float f7 = this.l;
        float f8 = this.m;
        if (!(f7 == 1.0f && f8 == 1.0f)) {
            matrix.postScale(f7, f8);
            matrix.postTranslate((-(f2 / width)) * ((f7 * width) - width), (-(f3 / height)) * ((f8 * height) - height));
        }
        matrix.postTranslate(this.n, this.o);
    }

    private void a(RectF rectF, View view) {
        rectF.set(0.0f, 0.0f, (float) view.getWidth(), (float) view.getHeight());
        Matrix matrix = this.r;
        matrix.reset();
        a(matrix, view);
        this.r.mapRect(rectF);
        rectF.offset((float) view.getLeft(), (float) view.getTop());
        if (rectF.right < rectF.left) {
            float f2 = rectF.right;
            rectF.right = rectF.left;
            rectF.left = f2;
        }
        if (rectF.bottom < rectF.top) {
            float f3 = rectF.top;
            rectF.top = rectF.bottom;
            rectF.bottom = f3;
        }
    }

    private void o() {
        View view = this.c.get();
        if (view != null) {
            a(this.p, view);
        }
    }

    private void p() {
        View view = this.c.get();
        if (view != null && view.getParent() != null) {
            RectF rectF = this.q;
            a(rectF, view);
            rectF.union(this.p);
            ((View) view.getParent()).invalidate((int) Math.floor((double) rectF.left), (int) Math.floor((double) rectF.top), (int) Math.ceil((double) rectF.right), (int) Math.ceil((double) rectF.bottom));
        }
    }

    public float a() {
        return this.f;
    }

    public void a(float f2) {
        if (this.f != f2) {
            this.f = f2;
            View view = this.c.get();
            if (view != null) {
                view.invalidate();
            }
        }
    }

    public void a(int i2) {
        View view = this.c.get();
        if (view != null) {
            view.scrollTo(i2, view.getScrollY());
        }
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f2, Transformation transformation) {
        View view = this.c.get();
        if (view != null) {
            transformation.setAlpha(this.f);
            a(transformation.getMatrix(), view);
        }
    }

    public float b() {
        return this.g;
    }

    public void b(float f2) {
        if (!this.e || this.g != f2) {
            o();
            this.e = true;
            this.g = f2;
            p();
        }
    }

    public void b(int i2) {
        View view = this.c.get();
        if (view != null) {
            view.scrollTo(view.getScrollX(), i2);
        }
    }

    public float c() {
        return this.h;
    }

    public void c(float f2) {
        if (!this.e || this.h != f2) {
            o();
            this.e = true;
            this.h = f2;
            p();
        }
    }

    public float d() {
        return this.k;
    }

    public void d(float f2) {
        if (this.k != f2) {
            o();
            this.k = f2;
            p();
        }
    }

    public float e() {
        return this.i;
    }

    public void e(float f2) {
        if (this.i != f2) {
            o();
            this.i = f2;
            p();
        }
    }

    public float f() {
        return this.j;
    }

    public void f(float f2) {
        if (this.j != f2) {
            o();
            this.j = f2;
            p();
        }
    }

    public float g() {
        return this.l;
    }

    public void g(float f2) {
        if (this.l != f2) {
            o();
            this.l = f2;
            p();
        }
    }

    public float h() {
        return this.m;
    }

    public void h(float f2) {
        if (this.m != f2) {
            o();
            this.m = f2;
            p();
        }
    }

    public int i() {
        View view = this.c.get();
        if (view == null) {
            return 0;
        }
        return view.getScrollX();
    }

    public void i(float f2) {
        if (this.n != f2) {
            o();
            this.n = f2;
            p();
        }
    }

    public int j() {
        View view = this.c.get();
        if (view == null) {
            return 0;
        }
        return view.getScrollY();
    }

    public void j(float f2) {
        if (this.o != f2) {
            o();
            this.o = f2;
            p();
        }
    }

    public float k() {
        return this.n;
    }

    public void k(float f2) {
        View view = this.c.get();
        if (view != null) {
            i(f2 - ((float) view.getLeft()));
        }
    }

    public float l() {
        return this.o;
    }

    public void l(float f2) {
        View view = this.c.get();
        if (view != null) {
            j(f2 - ((float) view.getTop()));
        }
    }

    public float m() {
        View view = this.c.get();
        if (view == null) {
            return 0.0f;
        }
        return ((float) view.getLeft()) + this.n;
    }

    public float n() {
        View view = this.c.get();
        if (view == null) {
            return 0.0f;
        }
        return ((float) view.getTop()) + this.o;
    }
}
