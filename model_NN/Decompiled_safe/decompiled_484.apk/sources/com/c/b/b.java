package com.c.b;

public abstract class b<T> extends c<T, Integer> {
    public b(String str) {
        super(Integer.class, str);
    }

    public abstract void a(Object obj, int i);

    public final void a(Object obj, Integer num) {
        a(obj, Integer.valueOf(num.intValue()));
    }
}
