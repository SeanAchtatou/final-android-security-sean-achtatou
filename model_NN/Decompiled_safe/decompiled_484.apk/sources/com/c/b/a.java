package com.c.b;

public abstract class a<T> extends c<T, Float> {
    public a(String str) {
        super(Float.class, str);
    }

    public abstract void a(Object obj, float f);

    public final void a(Object obj, Float f) {
        a(obj, f.floatValue());
    }
}
