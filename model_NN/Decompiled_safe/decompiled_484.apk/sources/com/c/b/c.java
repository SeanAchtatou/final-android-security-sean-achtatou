package com.c.b;

public abstract class c<T, V> {

    /* renamed from: a  reason: collision with root package name */
    private final String f548a;

    /* renamed from: b  reason: collision with root package name */
    private final Class<V> f549b;

    public c(Class<V> cls, String str) {
        this.f548a = str;
        this.f549b = cls;
    }

    public abstract V a(T t);

    public String a() {
        return this.f548a;
    }

    public void a(Object obj, Object obj2) {
        throw new UnsupportedOperationException("Property " + a() + " is read-only");
    }
}
