package com.c.a;

import android.util.Log;
import com.c.b.b;
import com.c.b.c;
import java.lang.reflect.InvocationTargetException;

class ad extends aa {
    f h;
    int i;
    private b j;

    public ad(c cVar, int... iArr) {
        super(cVar);
        a(iArr);
        if (cVar instanceof b) {
            this.j = (b) this.f540b;
        }
    }

    public ad(String str, int... iArr) {
        super(str);
        a(iArr);
    }

    /* access modifiers changed from: package-private */
    public void a(float f) {
        this.i = this.h.b(f);
    }

    /* access modifiers changed from: package-private */
    public void a(Class cls) {
        if (this.f540b == null) {
            super.a(cls);
        }
    }

    public void a(int... iArr) {
        super.a(iArr);
        this.h = (f) this.e;
    }

    /* access modifiers changed from: package-private */
    public void b(Object obj) {
        if (this.j != null) {
            this.j.a(obj, this.i);
        } else if (this.f540b != null) {
            this.f540b.a(obj, Integer.valueOf(this.i));
        } else if (this.c != null) {
            try {
                this.g[0] = Integer.valueOf(this.i);
                this.c.invoke(obj, this.g);
            } catch (InvocationTargetException e) {
                Log.e("PropertyValuesHolder", e.toString());
            } catch (IllegalAccessException e2) {
                Log.e("PropertyValuesHolder", e2.toString());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Object d() {
        return Integer.valueOf(this.i);
    }

    /* renamed from: e */
    public ad clone() {
        ad adVar = (ad) super.clone();
        adVar.h = (f) adVar.e;
        return adVar;
    }
}
