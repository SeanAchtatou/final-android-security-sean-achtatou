package com.c.a;

import android.os.Looper;
import android.util.AndroidRuntimeException;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import java.util.ArrayList;
import java.util.HashMap;

public class af extends a {
    private static ThreadLocal<al> h = new ThreadLocal<>();
    /* access modifiers changed from: private */
    public static final ThreadLocal<ArrayList<af>> i = new ag();
    /* access modifiers changed from: private */
    public static final ThreadLocal<ArrayList<af>> j = new ah();
    /* access modifiers changed from: private */
    public static final ThreadLocal<ArrayList<af>> k = new ai();
    /* access modifiers changed from: private */
    public static final ThreadLocal<ArrayList<af>> l = new aj();
    /* access modifiers changed from: private */
    public static final ThreadLocal<ArrayList<af>> m = new ak();
    private static final Interpolator n = new AccelerateDecelerateInterpolator();
    private static final ae o = new e();
    private static final ae p = new c();
    /* access modifiers changed from: private */
    public static long z = 10;
    private int A = 0;
    private int B = 1;
    private Interpolator C = n;
    private ArrayList<am> D = null;

    /* renamed from: b  reason: collision with root package name */
    long f541b;
    long c = -1;
    int d = 0;
    boolean e = false;
    aa[] f;
    HashMap<String, aa> g;
    private boolean q = false;
    private int r = 0;
    private float s = 0.0f;
    private boolean t = false;
    private long u;
    /* access modifiers changed from: private */
    public boolean v = false;
    private boolean w = false;
    private long x = 300;
    /* access modifiers changed from: private */
    public long y = 0;

    private void a(boolean z2) {
        if (Looper.myLooper() == null) {
            throw new AndroidRuntimeException("Animators may only be run on Looper threads");
        }
        this.q = z2;
        this.r = 0;
        this.d = 0;
        this.w = true;
        this.t = false;
        j.get().add(this);
        if (this.y == 0) {
            c(f());
            this.d = 0;
            this.v = true;
            if (this.f538a != null) {
                ArrayList arrayList = (ArrayList) this.f538a.clone();
                int size = arrayList.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ((b) arrayList.get(i2)).onAnimationStart(this);
                }
            }
        }
        al alVar = h.get();
        if (alVar == null) {
            alVar = new al(null);
            h.set(alVar);
        }
        alVar.sendEmptyMessage(0);
    }

    /* access modifiers changed from: private */
    public boolean a(long j2) {
        if (!this.t) {
            this.t = true;
            this.u = j2;
        } else {
            long j3 = j2 - this.u;
            if (j3 > this.y) {
                this.f541b = j2 - (j3 - this.y);
                this.d = 1;
                return true;
            }
        }
        return false;
    }

    public static af b(int... iArr) {
        af afVar = new af();
        afVar.a(iArr);
        return afVar;
    }

    /* access modifiers changed from: private */
    public void d() {
        i.get().remove(this);
        j.get().remove(this);
        k.get().remove(this);
        this.d = 0;
        if (this.v && this.f538a != null) {
            ArrayList arrayList = (ArrayList) this.f538a.clone();
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                ((b) arrayList.get(i2)).onAnimationEnd(this);
            }
        }
        this.v = false;
        this.w = false;
    }

    /* access modifiers changed from: private */
    public void n() {
        c();
        i.get().add(this);
        if (this.y > 0 && this.f538a != null) {
            ArrayList arrayList = (ArrayList) this.f538a.clone();
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                ((b) arrayList.get(i2)).onAnimationStart(this);
            }
        }
    }

    public void a() {
        a(false);
    }

    /* access modifiers changed from: package-private */
    public void a(float f2) {
        float interpolation = this.C.getInterpolation(f2);
        this.s = interpolation;
        for (aa a2 : this.f) {
            a2.a(interpolation);
        }
        if (this.D != null) {
            int size = this.D.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.D.get(i2).onAnimationUpdate(this);
            }
        }
    }

    public void a(Interpolator interpolator) {
        if (interpolator != null) {
            this.C = interpolator;
        } else {
            this.C = new LinearInterpolator();
        }
    }

    public void a(am amVar) {
        if (this.D == null) {
            this.D = new ArrayList<>();
        }
        this.D.add(amVar);
    }

    public void a(float... fArr) {
        if (fArr != null && fArr.length != 0) {
            if (this.f == null || this.f.length == 0) {
                a(aa.a("", fArr));
            } else {
                this.f[0].a(fArr);
            }
            this.e = false;
        }
    }

    public void a(int... iArr) {
        if (iArr != null && iArr.length != 0) {
            if (this.f == null || this.f.length == 0) {
                a(aa.a("", iArr));
            } else {
                this.f[0].a(iArr);
            }
            this.e = false;
        }
    }

    public void a(aa... aaVarArr) {
        this.f = aaVarArr;
        this.g = new HashMap<>(r2);
        for (aa aaVar : aaVarArr) {
            this.g.put(aaVar.c(), aaVar);
        }
        this.e = false;
    }

    public af b(long j2) {
        if (j2 < 0) {
            throw new IllegalArgumentException("Animators cannot have negative duration: " + j2);
        }
        this.x = j2;
        return this;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (!this.e) {
            for (aa b2 : this.f) {
                b2.b();
            }
            this.e = true;
        }
    }

    public void c(long j2) {
        c();
        long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
        if (this.d != 1) {
            this.c = j2;
            this.d = 2;
        }
        this.f541b = currentAnimationTimeMillis - j2;
        d(currentAnimationTimeMillis);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: package-private */
    public boolean d(long j2) {
        float f2;
        boolean z2 = false;
        if (this.d == 0) {
            this.d = 1;
            if (this.c < 0) {
                this.f541b = j2;
            } else {
                this.f541b = j2 - this.c;
                this.c = -1;
            }
        }
        switch (this.d) {
            case 1:
            case 2:
                float f3 = this.x > 0 ? ((float) (j2 - this.f541b)) / ((float) this.x) : 1.0f;
                if (f3 < 1.0f) {
                    f2 = f3;
                } else if (this.r < this.A || this.A == -1) {
                    if (this.f538a != null) {
                        int size = this.f538a.size();
                        for (int i2 = 0; i2 < size; i2++) {
                            ((b) this.f538a.get(i2)).onAnimationRepeat(this);
                        }
                    }
                    if (this.B == 2) {
                        this.q = !this.q;
                    }
                    this.r += (int) f3;
                    f2 = f3 % 1.0f;
                    this.f541b += this.x;
                } else {
                    f2 = Math.min(f3, 1.0f);
                    z2 = true;
                }
                if (this.q) {
                    f2 = 1.0f - f2;
                }
                a(f2);
                break;
        }
        return z2;
    }

    /* renamed from: e */
    public af clone() {
        af afVar = (af) super.clone();
        if (this.D != null) {
            ArrayList<am> arrayList = this.D;
            afVar.D = new ArrayList<>();
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                afVar.D.add(arrayList.get(i2));
            }
        }
        afVar.c = -1;
        afVar.q = false;
        afVar.r = 0;
        afVar.e = false;
        afVar.d = 0;
        afVar.t = false;
        aa[] aaVarArr = this.f;
        if (aaVarArr != null) {
            int length = aaVarArr.length;
            afVar.f = new aa[length];
            afVar.g = new HashMap<>(length);
            for (int i3 = 0; i3 < length; i3++) {
                aa a2 = aaVarArr[i3].clone();
                afVar.f[i3] = a2;
                afVar.g.put(a2.c(), a2);
            }
        }
        return afVar;
    }

    public long f() {
        if (!this.e || this.d == 0) {
            return 0;
        }
        return AnimationUtils.currentAnimationTimeMillis() - this.f541b;
    }

    public Object g() {
        if (this.f == null || this.f.length <= 0) {
            return null;
        }
        return this.f[0].d();
    }

    public String toString() {
        String str = "ValueAnimator@" + Integer.toHexString(hashCode());
        if (this.f != null) {
            for (int i2 = 0; i2 < this.f.length; i2++) {
                str = str + "\n    " + this.f[i2].toString();
            }
        }
        return str;
    }
}
