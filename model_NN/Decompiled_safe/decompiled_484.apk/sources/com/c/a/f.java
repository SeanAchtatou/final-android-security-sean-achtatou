package com.c.a;

import android.view.animation.Interpolator;
import java.util.ArrayList;

class f extends j {
    private int g;
    private int h;
    private int i;
    private boolean j = true;

    public f(i... iVarArr) {
        super(iVarArr);
    }

    /* renamed from: a */
    public f clone() {
        ArrayList arrayList = this.e;
        int size = this.e.size();
        i[] iVarArr = new i[size];
        for (int i2 = 0; i2 < size; i2++) {
            iVarArr[i2] = (i) ((g) arrayList.get(i2)).clone();
        }
        return new f(iVarArr);
    }

    public Object a(float f) {
        return Integer.valueOf(b(f));
    }

    public int b(float f) {
        int i2 = 1;
        if (this.f544a == 2) {
            if (this.j) {
                this.j = false;
                this.g = ((i) this.e.get(0)).f();
                this.h = ((i) this.e.get(1)).f();
                this.i = this.h - this.g;
            }
            if (this.d != null) {
                f = this.d.getInterpolation(f);
            }
            return this.f == null ? this.g + ((int) (((float) this.i) * f)) : ((Number) this.f.a(f, Integer.valueOf(this.g), Integer.valueOf(this.h))).intValue();
        } else if (f <= 0.0f) {
            i iVar = (i) this.e.get(0);
            i iVar2 = (i) this.e.get(1);
            int f2 = iVar.f();
            int f3 = iVar2.f();
            float c = iVar.c();
            float c2 = iVar2.c();
            Interpolator d = iVar2.d();
            if (d != null) {
                f = d.getInterpolation(f);
            }
            float f4 = (f - c) / (c2 - c);
            return this.f == null ? ((int) (f4 * ((float) (f3 - f2)))) + f2 : ((Number) this.f.a(f4, Integer.valueOf(f2), Integer.valueOf(f3))).intValue();
        } else if (f >= 1.0f) {
            i iVar3 = (i) this.e.get(this.f544a - 2);
            i iVar4 = (i) this.e.get(this.f544a - 1);
            int f5 = iVar3.f();
            int f6 = iVar4.f();
            float c3 = iVar3.c();
            float c4 = iVar4.c();
            Interpolator d2 = iVar4.d();
            if (d2 != null) {
                f = d2.getInterpolation(f);
            }
            float f7 = (f - c3) / (c4 - c3);
            return this.f == null ? ((int) (f7 * ((float) (f6 - f5)))) + f5 : ((Number) this.f.a(f7, Integer.valueOf(f5), Integer.valueOf(f6))).intValue();
        } else {
            i iVar5 = (i) this.e.get(0);
            while (true) {
                i iVar6 = iVar5;
                if (i2 >= this.f544a) {
                    return ((Number) ((g) this.e.get(this.f544a - 1)).b()).intValue();
                }
                iVar5 = (i) this.e.get(i2);
                if (f < iVar5.c()) {
                    Interpolator d3 = iVar5.d();
                    if (d3 != null) {
                        f = d3.getInterpolation(f);
                    }
                    float c5 = (f - iVar6.c()) / (iVar5.c() - iVar6.c());
                    int f8 = iVar6.f();
                    int f9 = iVar5.f();
                    return this.f == null ? ((int) (((float) (f9 - f8)) * c5)) + f8 : ((Number) this.f.a(c5, Integer.valueOf(f8), Integer.valueOf(f9))).intValue();
                }
                i2++;
            }
        }
    }
}
