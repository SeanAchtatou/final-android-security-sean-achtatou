package com.c.a;

import android.view.View;
import com.c.b.c;

final class l {

    /* renamed from: a  reason: collision with root package name */
    static c<View, Float> f546a = new m("alpha");

    /* renamed from: b  reason: collision with root package name */
    static c<View, Float> f547b = new s("pivotX");
    static c<View, Float> c = new t("pivotY");
    static c<View, Float> d = new u("translationX");
    static c<View, Float> e = new v("translationY");
    static c<View, Float> f = new w("rotation");
    static c<View, Float> g = new x("rotationX");
    static c<View, Float> h = new y("rotationY");
    static c<View, Float> i = new z("scaleX");
    static c<View, Float> j = new n("scaleY");
    static c<View, Integer> k = new o("scrollX");
    static c<View, Integer> l = new p("scrollY");
    static c<View, Float> m = new q("x");
    static c<View, Float> n = new r("y");
}
