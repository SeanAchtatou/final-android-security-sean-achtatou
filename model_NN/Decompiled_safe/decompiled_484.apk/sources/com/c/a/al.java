package com.c.a;

import android.os.Handler;
import android.os.Message;
import android.view.animation.AnimationUtils;
import java.util.ArrayList;

class al extends Handler {
    private al() {
    }

    /* synthetic */ al(ag agVar) {
        this();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.c.a.af.a(com.c.a.af, boolean):boolean
     arg types: [com.c.a.af, int]
     candidates:
      com.c.a.af.a(com.c.a.af, long):boolean
      com.c.a.af.a(com.c.a.af, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public void handleMessage(Message message) {
        boolean z;
        int i;
        ArrayList arrayList = (ArrayList) af.i.get();
        ArrayList arrayList2 = (ArrayList) af.k.get();
        switch (message.what) {
            case 0:
                ArrayList arrayList3 = (ArrayList) af.j.get();
                z = arrayList.size() <= 0 && arrayList2.size() <= 0;
                while (arrayList3.size() > 0) {
                    ArrayList arrayList4 = (ArrayList) arrayList3.clone();
                    arrayList3.clear();
                    int size = arrayList4.size();
                    for (int i2 = 0; i2 < size; i2++) {
                        af afVar = (af) arrayList4.get(i2);
                        if (afVar.y == 0) {
                            afVar.n();
                        } else {
                            arrayList2.add(afVar);
                        }
                    }
                }
                break;
            case 1:
                z = true;
                break;
            default:
                return;
        }
        long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
        ArrayList arrayList5 = (ArrayList) af.m.get();
        ArrayList arrayList6 = (ArrayList) af.l.get();
        int size2 = arrayList2.size();
        for (int i3 = 0; i3 < size2; i3++) {
            af afVar2 = (af) arrayList2.get(i3);
            if (afVar2.a(currentAnimationTimeMillis)) {
                arrayList5.add(afVar2);
            }
        }
        int size3 = arrayList5.size();
        if (size3 > 0) {
            for (int i4 = 0; i4 < size3; i4++) {
                af afVar3 = (af) arrayList5.get(i4);
                afVar3.n();
                boolean unused = afVar3.v = true;
                arrayList2.remove(afVar3);
            }
            arrayList5.clear();
        }
        int size4 = arrayList.size();
        int i5 = 0;
        while (i5 < size4) {
            af afVar4 = (af) arrayList.get(i5);
            if (afVar4.d(currentAnimationTimeMillis)) {
                arrayList6.add(afVar4);
            }
            if (arrayList.size() == size4) {
                i = i5 + 1;
            } else {
                size4--;
                arrayList6.remove(afVar4);
                i = i5;
            }
            size4 = size4;
            i5 = i;
        }
        if (arrayList6.size() > 0) {
            int i6 = 0;
            while (true) {
                int i7 = i6;
                if (i7 < arrayList6.size()) {
                    ((af) arrayList6.get(i7)).d();
                    i6 = i7 + 1;
                } else {
                    arrayList6.clear();
                }
            }
        }
        if (!z) {
            return;
        }
        if (!arrayList.isEmpty() || !arrayList2.isEmpty()) {
            sendEmptyMessageDelayed(1, Math.max(0L, af.z - (AnimationUtils.currentAnimationTimeMillis() - currentAnimationTimeMillis)));
        }
    }
}
