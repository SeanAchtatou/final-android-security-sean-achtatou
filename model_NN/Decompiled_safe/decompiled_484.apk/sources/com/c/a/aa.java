package com.c.a;

import android.util.Log;
import com.c.b.c;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class aa implements Cloneable {
    private static final ae i = new e();
    private static final ae j = new c();
    private static Class[] k = {Float.TYPE, Float.class, Double.TYPE, Integer.TYPE, Double.class, Integer.class};
    private static Class[] l = {Integer.TYPE, Integer.class, Float.TYPE, Double.TYPE, Float.class, Double.class};
    private static Class[] m = {Double.TYPE, Double.class, Float.TYPE, Integer.TYPE, Float.class, Integer.class};
    private static final HashMap<Class, HashMap<String, Method>> n = new HashMap<>();
    private static final HashMap<Class, HashMap<String, Method>> o = new HashMap<>();

    /* renamed from: a  reason: collision with root package name */
    String f539a;

    /* renamed from: b  reason: collision with root package name */
    protected c f540b;
    Method c;
    Class d;
    j e;
    final ReentrantReadWriteLock f;
    final Object[] g;
    private Method h;
    private ae p;
    private Object q;

    private aa(c cVar) {
        this.c = null;
        this.h = null;
        this.e = null;
        this.f = new ReentrantReadWriteLock();
        this.g = new Object[1];
        this.f540b = cVar;
        if (cVar != null) {
            this.f539a = cVar.a();
        }
    }

    private aa(String str) {
        this.c = null;
        this.h = null;
        this.e = null;
        this.f = new ReentrantReadWriteLock();
        this.g = new Object[1];
        this.f539a = str;
    }

    public static aa a(c<?, Float> cVar, float... fArr) {
        return new ac(cVar, fArr);
    }

    public static aa a(c<?, Integer> cVar, int... iArr) {
        return new ad(cVar, iArr);
    }

    public static aa a(String str, float... fArr) {
        return new ac(str, fArr);
    }

    public static aa a(String str, int... iArr) {
        return new ad(str, iArr);
    }

    static String a(String str, String str2) {
        if (str2 == null || str2.length() == 0) {
            return str;
        }
        char upperCase = Character.toUpperCase(str2.charAt(0));
        return str + upperCase + str2.substring(1);
    }

    private Method a(Class cls, String str, Class cls2) {
        Method method;
        Method method2 = null;
        String a2 = a(str, this.f539a);
        if (cls2 == null) {
            try {
                return cls.getMethod(a2, null);
            } catch (NoSuchMethodException e2) {
                try {
                    method = cls.getDeclaredMethod(a2, null);
                    try {
                        method.setAccessible(true);
                        return method;
                    } catch (NoSuchMethodException e3) {
                    }
                } catch (NoSuchMethodException e4) {
                    method = null;
                    Log.e("PropertyValuesHolder", "Couldn't find no-arg method for property " + this.f539a + ": " + e2);
                    return method;
                }
            }
        } else {
            Class[] clsArr = new Class[1];
            Class[] clsArr2 = this.d.equals(Float.class) ? k : this.d.equals(Integer.class) ? l : this.d.equals(Double.class) ? m : new Class[]{this.d};
            int length = clsArr2.length;
            int i2 = 0;
            while (i2 < length) {
                Class cls3 = clsArr2[i2];
                clsArr[0] = cls3;
                try {
                    method2 = cls.getMethod(a2, clsArr);
                    this.d = cls3;
                    return method2;
                } catch (NoSuchMethodException e5) {
                    try {
                        method2 = cls.getDeclaredMethod(a2, clsArr);
                        method2.setAccessible(true);
                        this.d = cls3;
                        return method2;
                    } catch (NoSuchMethodException e6) {
                        i2++;
                    }
                }
            }
            Log.e("PropertyValuesHolder", "Couldn't find setter/getter for property " + this.f539a + " with value type " + this.d);
            return method2;
        }
    }

    private Method a(Class cls, HashMap<Class, HashMap<String, Method>> hashMap, String str, Class cls2) {
        Method method = null;
        try {
            this.f.writeLock().lock();
            HashMap hashMap2 = hashMap.get(cls);
            if (hashMap2 != null) {
                method = (Method) hashMap2.get(this.f539a);
            }
            if (method == null) {
                method = a(cls, str, cls2);
                if (hashMap2 == null) {
                    hashMap2 = new HashMap();
                    hashMap.put(cls, hashMap2);
                }
                hashMap2.put(this.f539a, method);
            }
            Method method2 = method;
            return method2;
        } finally {
            this.f.writeLock().unlock();
        }
    }

    private void b(Class cls) {
        this.h = a(cls, o, "get", null);
    }

    /* renamed from: a */
    public aa clone() {
        try {
            aa aaVar = (aa) super.clone();
            aaVar.f539a = this.f539a;
            aaVar.f540b = this.f540b;
            aaVar.e = this.e.clone();
            aaVar.p = this.p;
            return aaVar;
        } catch (CloneNotSupportedException e2) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(float f2) {
        this.q = this.e.a(f2);
    }

    public void a(c cVar) {
        this.f540b = cVar;
    }

    /* access modifiers changed from: package-private */
    public void a(Class cls) {
        this.c = a(cls, n, "set", this.d);
    }

    /* access modifiers changed from: package-private */
    public void a(Object obj) {
        if (this.f540b != null) {
            try {
                this.f540b.a(obj);
                Iterator<g> it = this.e.e.iterator();
                while (it.hasNext()) {
                    g next = it.next();
                    if (!next.a()) {
                        next.a(this.f540b.a(obj));
                    }
                }
                return;
            } catch (ClassCastException e2) {
                Log.e("PropertyValuesHolder", "No such property (" + this.f540b.a() + ") on target object " + obj + ". Trying reflection instead");
                this.f540b = null;
            }
        }
        Class<?> cls = obj.getClass();
        if (this.c == null) {
            a((Class) cls);
        }
        Iterator<g> it2 = this.e.e.iterator();
        while (it2.hasNext()) {
            g next2 = it2.next();
            if (!next2.a()) {
                if (this.h == null) {
                    b((Class) cls);
                }
                try {
                    next2.a(this.h.invoke(obj, new Object[0]));
                } catch (InvocationTargetException e3) {
                    Log.e("PropertyValuesHolder", e3.toString());
                } catch (IllegalAccessException e4) {
                    Log.e("PropertyValuesHolder", e4.toString());
                }
            }
        }
    }

    public void a(String str) {
        this.f539a = str;
    }

    public void a(float... fArr) {
        this.d = Float.TYPE;
        this.e = j.a(fArr);
    }

    public void a(int... iArr) {
        this.d = Integer.TYPE;
        this.e = j.a(iArr);
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.p == null) {
            this.p = this.d == Integer.class ? i : this.d == Float.class ? j : null;
        }
        if (this.p != null) {
            this.e.a(this.p);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Object obj) {
        if (this.f540b != null) {
            this.f540b.a(obj, d());
        }
        if (this.c != null) {
            try {
                this.g[0] = d();
                this.c.invoke(obj, this.g);
            } catch (InvocationTargetException e2) {
                Log.e("PropertyValuesHolder", e2.toString());
            } catch (IllegalAccessException e3) {
                Log.e("PropertyValuesHolder", e3.toString());
            }
        }
    }

    public String c() {
        return this.f539a;
    }

    /* access modifiers changed from: package-private */
    public Object d() {
        return this.q;
    }

    public String toString() {
        return this.f539a + ": " + this.e.toString();
    }
}
