package com.c.a;

import android.view.View;
import com.c.b.c;
import com.c.c.a.a;
import java.util.HashMap;
import java.util.Map;

public final class k extends af {
    private static final Map<String, c> h = new HashMap();
    private Object i;
    private String j;
    private c k;

    static {
        h.put("alpha", l.f546a);
        h.put("pivotX", l.f547b);
        h.put("pivotY", l.c);
        h.put("translationX", l.d);
        h.put("translationY", l.e);
        h.put("rotation", l.f);
        h.put("rotationX", l.g);
        h.put("rotationY", l.h);
        h.put("scaleX", l.i);
        h.put("scaleY", l.j);
        h.put("scrollX", l.k);
        h.put("scrollY", l.l);
        h.put("x", l.m);
        h.put("y", l.n);
    }

    public k() {
    }

    private k(Object obj, String str) {
        this.i = obj;
        a(str);
    }

    public static k a(Object obj, String str, float... fArr) {
        k kVar = new k(obj, str);
        kVar.a(fArr);
        return kVar;
    }

    /* renamed from: a */
    public k b(long j2) {
        super.b(j2);
        return this;
    }

    public void a() {
        super.a();
    }

    /* access modifiers changed from: package-private */
    public void a(float f) {
        super.a(f);
        for (aa b2 : this.f) {
            b2.b(this.i);
        }
    }

    public void a(c cVar) {
        if (this.f != null) {
            aa aaVar = this.f[0];
            String c = aaVar.c();
            aaVar.a(cVar);
            this.g.remove(c);
            this.g.put(this.j, aaVar);
        }
        if (this.k != null) {
            this.j = cVar.a();
        }
        this.k = cVar;
        this.e = false;
    }

    public void a(String str) {
        if (this.f != null) {
            aa aaVar = this.f[0];
            String c = aaVar.c();
            aaVar.a(str);
            this.g.remove(c);
            this.g.put(str, aaVar);
        }
        this.j = str;
        this.e = false;
    }

    public void a(float... fArr) {
        if (this.f != null && this.f.length != 0) {
            super.a(fArr);
        } else if (this.k != null) {
            a(aa.a(this.k, fArr));
        } else {
            a(aa.a(this.j, fArr));
        }
    }

    public void a(int... iArr) {
        if (this.f != null && this.f.length != 0) {
            super.a(iArr);
        } else if (this.k != null) {
            a(aa.a((c<?, Integer>) this.k, iArr));
        } else {
            a(aa.a(this.j, iArr));
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (!this.e) {
            if (this.k == null && a.f550a && (this.i instanceof View) && h.containsKey(this.j)) {
                a(h.get(this.j));
            }
            for (aa a2 : this.f) {
                a2.a(this.i);
            }
            super.c();
        }
    }

    /* renamed from: d */
    public k e() {
        return (k) super.clone();
    }

    public String toString() {
        String str = "ObjectAnimator@" + Integer.toHexString(hashCode()) + ", target " + this.i;
        if (this.f != null) {
            for (int i2 = 0; i2 < this.f.length; i2++) {
                str = str + "\n    " + this.f[i2].toString();
            }
        }
        return str;
    }
}
