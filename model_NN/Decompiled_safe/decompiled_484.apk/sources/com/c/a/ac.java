package com.c.a;

import android.util.Log;
import com.c.b.a;
import com.c.b.c;
import java.lang.reflect.InvocationTargetException;

class ac extends aa {
    d h;
    float i;
    private a j;

    public ac(c cVar, float... fArr) {
        super(cVar);
        a(fArr);
        if (cVar instanceof a) {
            this.j = (a) this.f540b;
        }
    }

    public ac(String str, float... fArr) {
        super(str);
        a(fArr);
    }

    /* access modifiers changed from: package-private */
    public void a(float f) {
        this.i = this.h.b(f);
    }

    /* access modifiers changed from: package-private */
    public void a(Class cls) {
        if (this.f540b == null) {
            super.a(cls);
        }
    }

    public void a(float... fArr) {
        super.a(fArr);
        this.h = (d) this.e;
    }

    /* access modifiers changed from: package-private */
    public void b(Object obj) {
        if (this.j != null) {
            this.j.a(obj, this.i);
        } else if (this.f540b != null) {
            this.f540b.a(obj, Float.valueOf(this.i));
        } else if (this.c != null) {
            try {
                this.g[0] = Float.valueOf(this.i);
                this.c.invoke(obj, this.g);
            } catch (InvocationTargetException e) {
                Log.e("PropertyValuesHolder", e.toString());
            } catch (IllegalAccessException e2) {
                Log.e("PropertyValuesHolder", e2.toString());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Object d() {
        return Float.valueOf(this.i);
    }

    /* renamed from: e */
    public ac clone() {
        ac acVar = (ac) super.clone();
        acVar.h = (d) acVar.e;
        return acVar;
    }
}
