package com.c.a;

import android.view.animation.Interpolator;

public abstract class g implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    float f542a;

    /* renamed from: b  reason: collision with root package name */
    Class f543b;
    boolean c = false;
    private Interpolator d = null;

    public static g a(float f) {
        return new i(f);
    }

    public static g a(float f, float f2) {
        return new h(f, f2);
    }

    public static g a(float f, int i) {
        return new i(f, i);
    }

    public static g b(float f) {
        return new h(f);
    }

    public void a(Interpolator interpolator) {
        this.d = interpolator;
    }

    public abstract void a(Object obj);

    public boolean a() {
        return this.c;
    }

    public abstract Object b();

    public float c() {
        return this.f542a;
    }

    public Interpolator d() {
        return this.d;
    }

    /* renamed from: e */
    public abstract g clone();
}
