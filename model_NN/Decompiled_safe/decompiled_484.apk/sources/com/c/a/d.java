package com.c.a;

import android.view.animation.Interpolator;
import java.util.ArrayList;

class d extends j {
    private float g;
    private float h;
    private float i;
    private boolean j = true;

    public d(h... hVarArr) {
        super(hVarArr);
    }

    /* renamed from: a */
    public d clone() {
        ArrayList arrayList = this.e;
        int size = this.e.size();
        h[] hVarArr = new h[size];
        for (int i2 = 0; i2 < size; i2++) {
            hVarArr[i2] = (h) ((g) arrayList.get(i2)).clone();
        }
        return new d(hVarArr);
    }

    public Object a(float f) {
        return Float.valueOf(b(f));
    }

    public float b(float f) {
        int i2 = 1;
        if (this.f544a == 2) {
            if (this.j) {
                this.j = false;
                this.g = ((h) this.e.get(0)).f();
                this.h = ((h) this.e.get(1)).f();
                this.i = this.h - this.g;
            }
            if (this.d != null) {
                f = this.d.getInterpolation(f);
            }
            return this.f == null ? this.g + (this.i * f) : ((Number) this.f.a(f, Float.valueOf(this.g), Float.valueOf(this.h))).floatValue();
        } else if (f <= 0.0f) {
            h hVar = (h) this.e.get(0);
            h hVar2 = (h) this.e.get(1);
            float f2 = hVar.f();
            float f3 = hVar2.f();
            float c = hVar.c();
            float c2 = hVar2.c();
            Interpolator d = hVar2.d();
            if (d != null) {
                f = d.getInterpolation(f);
            }
            float f4 = (f - c) / (c2 - c);
            return this.f == null ? (f4 * (f3 - f2)) + f2 : ((Number) this.f.a(f4, Float.valueOf(f2), Float.valueOf(f3))).floatValue();
        } else if (f >= 1.0f) {
            h hVar3 = (h) this.e.get(this.f544a - 2);
            h hVar4 = (h) this.e.get(this.f544a - 1);
            float f5 = hVar3.f();
            float f6 = hVar4.f();
            float c3 = hVar3.c();
            float c4 = hVar4.c();
            Interpolator d2 = hVar4.d();
            if (d2 != null) {
                f = d2.getInterpolation(f);
            }
            float f7 = (f - c3) / (c4 - c3);
            return this.f == null ? (f7 * (f6 - f5)) + f5 : ((Number) this.f.a(f7, Float.valueOf(f5), Float.valueOf(f6))).floatValue();
        } else {
            h hVar5 = (h) this.e.get(0);
            while (true) {
                h hVar6 = hVar5;
                if (i2 >= this.f544a) {
                    return ((Number) ((g) this.e.get(this.f544a - 1)).b()).floatValue();
                }
                hVar5 = (h) this.e.get(i2);
                if (f < hVar5.c()) {
                    Interpolator d3 = hVar5.d();
                    if (d3 != null) {
                        f = d3.getInterpolation(f);
                    }
                    float c5 = (f - hVar6.c()) / (hVar5.c() - hVar6.c());
                    float f8 = hVar6.f();
                    float f9 = hVar5.f();
                    return this.f == null ? ((f9 - f8) * c5) + f8 : ((Number) this.f.a(c5, Float.valueOf(f8), Float.valueOf(f9))).floatValue();
                }
                i2++;
            }
        }
    }
}
