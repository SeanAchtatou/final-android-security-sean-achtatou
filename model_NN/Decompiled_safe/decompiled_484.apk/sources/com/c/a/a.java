package com.c.a;

import java.util.ArrayList;

public abstract class a implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    ArrayList<b> f538a = null;

    public void a() {
    }

    public void a(b bVar) {
        if (this.f538a == null) {
            this.f538a = new ArrayList<>();
        }
        this.f538a.add(bVar);
    }

    /* renamed from: b */
    public a clone() {
        try {
            a aVar = (a) super.clone();
            if (this.f538a != null) {
                ArrayList<b> arrayList = this.f538a;
                aVar.f538a = new ArrayList<>();
                int size = arrayList.size();
                for (int i = 0; i < size; i++) {
                    aVar.f538a.add(arrayList.get(i));
                }
            }
            return aVar;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
