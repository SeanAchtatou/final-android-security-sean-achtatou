package com.c.a;

class i extends g {
    int d;

    i(float f) {
        this.f542a = f;
        this.f543b = Integer.TYPE;
    }

    i(float f, int i) {
        this.f542a = f;
        this.d = i;
        this.f543b = Integer.TYPE;
        this.c = true;
    }

    public void a(Object obj) {
        if (obj != null && obj.getClass() == Integer.class) {
            this.d = ((Integer) obj).intValue();
            this.c = true;
        }
    }

    public Object b() {
        return Integer.valueOf(this.d);
    }

    public int f() {
        return this.d;
    }

    /* renamed from: g */
    public i e() {
        i iVar = new i(c(), this.d);
        iVar.a(d());
        return iVar;
    }
}
