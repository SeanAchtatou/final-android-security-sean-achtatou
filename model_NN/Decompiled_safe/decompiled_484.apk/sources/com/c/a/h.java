package com.c.a;

class h extends g {
    float d;

    h(float f) {
        this.f542a = f;
        this.f543b = Float.TYPE;
    }

    h(float f, float f2) {
        this.f542a = f;
        this.d = f2;
        this.f543b = Float.TYPE;
        this.c = true;
    }

    public void a(Object obj) {
        if (obj != null && obj.getClass() == Float.class) {
            this.d = ((Float) obj).floatValue();
            this.c = true;
        }
    }

    public Object b() {
        return Float.valueOf(this.d);
    }

    public float f() {
        return this.d;
    }

    /* renamed from: g */
    public h e() {
        h hVar = new h(c(), this.d);
        hVar.a(d());
        return hVar;
    }
}
