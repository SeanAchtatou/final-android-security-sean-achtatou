package com.c.a;

import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Arrays;

class j {

    /* renamed from: a  reason: collision with root package name */
    int f544a;

    /* renamed from: b  reason: collision with root package name */
    g f545b;
    g c;
    Interpolator d;
    ArrayList<g> e = new ArrayList<>();
    ae f;

    public j(g... gVarArr) {
        this.f544a = gVarArr.length;
        this.e.addAll(Arrays.asList(gVarArr));
        this.f545b = this.e.get(0);
        this.c = this.e.get(this.f544a - 1);
        this.d = this.c.d();
    }

    public static j a(float... fArr) {
        int length = fArr.length;
        h[] hVarArr = new h[Math.max(length, 2)];
        if (length == 1) {
            hVarArr[0] = (h) g.b(0.0f);
            hVarArr[1] = (h) g.a(1.0f, fArr[0]);
        } else {
            hVarArr[0] = (h) g.a(0.0f, fArr[0]);
            for (int i = 1; i < length; i++) {
                hVarArr[i] = (h) g.a(((float) i) / ((float) (length - 1)), fArr[i]);
            }
        }
        return new d(hVarArr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.c.a.g.a(float, int):com.c.a.g
     arg types: [int, int]
     candidates:
      com.c.a.g.a(float, float):com.c.a.g
      com.c.a.g.a(float, int):com.c.a.g */
    public static j a(int... iArr) {
        int length = iArr.length;
        i[] iVarArr = new i[Math.max(length, 2)];
        if (length == 1) {
            iVarArr[0] = (i) g.a(0.0f);
            iVarArr[1] = (i) g.a(1.0f, iArr[0]);
        } else {
            iVarArr[0] = (i) g.a(0.0f, iArr[0]);
            for (int i = 1; i < length; i++) {
                iVarArr[i] = (i) g.a(((float) i) / ((float) (length - 1)), iArr[i]);
            }
        }
        return new f(iVarArr);
    }

    public Object a(float f2) {
        if (this.f544a == 2) {
            if (this.d != null) {
                f2 = this.d.getInterpolation(f2);
            }
            return this.f.a(f2, this.f545b.b(), this.c.b());
        } else if (f2 <= 0.0f) {
            g gVar = this.e.get(1);
            Interpolator d2 = gVar.d();
            if (d2 != null) {
                f2 = d2.getInterpolation(f2);
            }
            float c2 = this.f545b.c();
            return this.f.a((f2 - c2) / (gVar.c() - c2), this.f545b.b(), gVar.b());
        } else if (f2 >= 1.0f) {
            g gVar2 = this.e.get(this.f544a - 2);
            Interpolator d3 = this.c.d();
            if (d3 != null) {
                f2 = d3.getInterpolation(f2);
            }
            float c3 = gVar2.c();
            return this.f.a((f2 - c3) / (this.c.c() - c3), gVar2.b(), this.c.b());
        } else {
            g gVar3 = this.f545b;
            int i = 1;
            while (i < this.f544a) {
                g gVar4 = this.e.get(i);
                if (f2 < gVar4.c()) {
                    Interpolator d4 = gVar4.d();
                    if (d4 != null) {
                        f2 = d4.getInterpolation(f2);
                    }
                    float c4 = gVar3.c();
                    return this.f.a((f2 - c4) / (gVar4.c() - c4), gVar3.b(), gVar4.b());
                }
                i++;
                gVar3 = gVar4;
            }
            return this.c.b();
        }
    }

    public void a(ae aeVar) {
        this.f = aeVar;
    }

    /* renamed from: b */
    public j clone() {
        ArrayList<g> arrayList = this.e;
        int size = this.e.size();
        g[] gVarArr = new g[size];
        for (int i = 0; i < size; i++) {
            gVarArr[i] = arrayList.get(i).clone();
        }
        return new j(gVarArr);
    }

    public String toString() {
        String str = " ";
        int i = 0;
        while (i < this.f544a) {
            String str2 = str + this.e.get(i).b() + "  ";
            i++;
            str = str2;
        }
        return str;
    }
}
