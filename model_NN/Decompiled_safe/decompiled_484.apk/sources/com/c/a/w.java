package com.c.a;

import android.view.View;
import com.c.b.a;

final class w extends a<View> {
    w(String str) {
        super(str);
    }

    public Float a(View view) {
        return Float.valueOf(com.c.c.a.a.a(view).d());
    }

    public void a(View view, float f) {
        com.c.c.a.a.a(view).d(f);
    }
}
