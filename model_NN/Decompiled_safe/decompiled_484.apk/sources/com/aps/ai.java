package com.aps;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;

final class ai implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    protected int f416a = 0;

    /* renamed from: b  reason: collision with root package name */
    protected int f417b = 0;
    protected short c = 0;
    protected short d = 0;
    protected int e = 0;
    protected byte f = 0;
    private byte g = 4;

    ai() {
    }

    /* access modifiers changed from: protected */
    public final Boolean a(DataOutputStream dataOutputStream) {
        boolean z = false;
        try {
            dataOutputStream.writeByte(this.g);
            dataOutputStream.writeInt(this.f416a);
            dataOutputStream.writeInt(this.f417b);
            dataOutputStream.writeShort(this.c);
            dataOutputStream.writeShort(this.d);
            dataOutputStream.writeInt(this.e);
            dataOutputStream.writeByte(this.f);
            return true;
        } catch (IOException e2) {
            return z;
        }
    }
}
