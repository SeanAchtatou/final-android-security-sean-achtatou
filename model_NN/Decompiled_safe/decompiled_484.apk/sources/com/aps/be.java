package com.aps;

import com.qihoo360.daily.activity.SplashActivity;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.zip.GZIPInputStream;

public final class be {

    /* renamed from: a  reason: collision with root package name */
    private RandomAccessFile f449a;

    /* renamed from: b  reason: collision with root package name */
    private ah f450b;
    private File c = null;

    protected be(ah ahVar) {
        this.f450b = ahVar;
    }

    private static byte a(byte[] bArr) {
        byte[] bArr2 = null;
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
            byte[] bArr3 = new byte[1024];
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            while (true) {
                int read = gZIPInputStream.read(bArr3, 0, bArr3.length);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(bArr3, 0, read);
            }
            bArr2 = byteArrayOutputStream.toByteArray();
            byteArrayOutputStream.flush();
            byteArrayOutputStream.close();
            gZIPInputStream.close();
            byteArrayInputStream.close();
        } catch (Exception e) {
        }
        return bArr2[0];
    }

    private static int a(int i, int i2, int i3) {
        int i4 = ((i3 - 1) * SplashActivity.SPLASH_TYPE_OP_TIME) + i;
        while (i4 >= i2) {
            i4 -= 1500;
        }
        return i4;
    }

    private int a(BitSet bitSet) {
        for (int i = 0; i < bitSet.length(); i++) {
            if (bitSet.get(i)) {
                return this.f450b.a() + (i * SplashActivity.SPLASH_TYPE_OP_TIME) + 4;
            }
        }
        return 0;
    }

    private ArrayList a(int i, int i2) {
        ArrayList arrayList = new ArrayList();
        while (i <= i2) {
            try {
                this.f449a.seek((long) i);
                int readInt = this.f449a.readInt();
                this.f449a.readLong();
                if (readInt <= 0 || readInt > 1500) {
                    return null;
                }
                byte[] bArr = new byte[readInt];
                this.f449a.read(bArr);
                byte a2 = a(bArr);
                if (a2 != 3 && a2 != 4 && a2 != 41) {
                    return null;
                }
                arrayList.add(bArr);
                i += SplashActivity.SPLASH_TYPE_OP_TIME;
            } catch (IOException e) {
            }
        }
        return arrayList;
    }

    private BitSet b() {
        byte[] bArr = new byte[this.f450b.a()];
        try {
            this.f449a.read(bArr);
            return ah.b(bArr);
        } catch (IOException e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:14:0x0040=Splitter:B:14:0x0040, B:50:0x007f=Splitter:B:50:0x007f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a() {
        /*
            r4 = this;
            r0 = 0
            monitor-enter(r4)
            com.aps.ah r1 = r4.f450b     // Catch:{ all -> 0x0080 }
            java.io.File r1 = r1.b()     // Catch:{ all -> 0x0080 }
            r4.c = r1     // Catch:{ all -> 0x0080 }
            java.io.File r1 = r4.c     // Catch:{ FileNotFoundException -> 0x004e, IOException -> 0x005b, NullPointerException -> 0x0068, all -> 0x0075 }
            if (r1 == 0) goto L_0x0040
            java.io.RandomAccessFile r1 = new java.io.RandomAccessFile     // Catch:{ FileNotFoundException -> 0x004e, IOException -> 0x005b, NullPointerException -> 0x0068, all -> 0x0075 }
            com.aps.ah r2 = r4.f450b     // Catch:{ FileNotFoundException -> 0x004e, IOException -> 0x005b, NullPointerException -> 0x0068, all -> 0x0075 }
            java.io.File r2 = r2.b()     // Catch:{ FileNotFoundException -> 0x004e, IOException -> 0x005b, NullPointerException -> 0x0068, all -> 0x0075 }
            java.lang.String r3 = "rw"
            r1.<init>(r2, r3)     // Catch:{ FileNotFoundException -> 0x004e, IOException -> 0x005b, NullPointerException -> 0x0068, all -> 0x0075 }
            r4.f449a = r1     // Catch:{ FileNotFoundException -> 0x004e, IOException -> 0x005b, NullPointerException -> 0x0068, all -> 0x0075 }
            com.aps.ah r1 = r4.f450b     // Catch:{ FileNotFoundException -> 0x004e, IOException -> 0x005b, NullPointerException -> 0x0068, all -> 0x0075 }
            int r1 = r1.a()     // Catch:{ FileNotFoundException -> 0x004e, IOException -> 0x005b, NullPointerException -> 0x0068, all -> 0x0075 }
            byte[] r1 = new byte[r1]     // Catch:{ FileNotFoundException -> 0x004e, IOException -> 0x005b, NullPointerException -> 0x0068, all -> 0x0075 }
            java.io.RandomAccessFile r2 = r4.f449a     // Catch:{ FileNotFoundException -> 0x004e, IOException -> 0x005b, NullPointerException -> 0x0068, all -> 0x0075 }
            r2.read(r1)     // Catch:{ FileNotFoundException -> 0x004e, IOException -> 0x005b, NullPointerException -> 0x0068, all -> 0x0075 }
            java.util.BitSet r2 = com.aps.ah.b(r1)     // Catch:{ FileNotFoundException -> 0x004e, IOException -> 0x005b, NullPointerException -> 0x0068, all -> 0x0075 }
            r1 = r0
        L_0x002f:
            int r3 = r2.size()     // Catch:{ FileNotFoundException -> 0x004e, IOException -> 0x005b, NullPointerException -> 0x0068, all -> 0x0075 }
            if (r1 >= r3) goto L_0x0040
            boolean r3 = r2.get(r1)     // Catch:{ FileNotFoundException -> 0x004e, IOException -> 0x005b, NullPointerException -> 0x0068, all -> 0x0075 }
            if (r3 == 0) goto L_0x003d
            int r0 = r0 + 1
        L_0x003d:
            int r1 = r1 + 1
            goto L_0x002f
        L_0x0040:
            java.io.RandomAccessFile r1 = r4.f449a     // Catch:{ all -> 0x0080 }
            if (r1 == 0) goto L_0x0049
            java.io.RandomAccessFile r1 = r4.f449a     // Catch:{ IOException -> 0x0085 }
            r1.close()     // Catch:{ IOException -> 0x0085 }
        L_0x0049:
            r1 = 0
            r4.c = r1     // Catch:{ all -> 0x0080 }
            monitor-exit(r4)     // Catch:{ all -> 0x0080 }
            return r0
        L_0x004e:
            r1 = move-exception
            java.io.RandomAccessFile r1 = r4.f449a     // Catch:{ all -> 0x0080 }
            if (r1 == 0) goto L_0x0049
            java.io.RandomAccessFile r1 = r4.f449a     // Catch:{ IOException -> 0x0059 }
            r1.close()     // Catch:{ IOException -> 0x0059 }
            goto L_0x0049
        L_0x0059:
            r1 = move-exception
            goto L_0x0049
        L_0x005b:
            r1 = move-exception
            java.io.RandomAccessFile r1 = r4.f449a     // Catch:{ all -> 0x0080 }
            if (r1 == 0) goto L_0x0049
            java.io.RandomAccessFile r1 = r4.f449a     // Catch:{ IOException -> 0x0066 }
            r1.close()     // Catch:{ IOException -> 0x0066 }
            goto L_0x0049
        L_0x0066:
            r1 = move-exception
            goto L_0x0049
        L_0x0068:
            r1 = move-exception
            java.io.RandomAccessFile r1 = r4.f449a     // Catch:{ all -> 0x0080 }
            if (r1 == 0) goto L_0x0049
            java.io.RandomAccessFile r1 = r4.f449a     // Catch:{ IOException -> 0x0073 }
            r1.close()     // Catch:{ IOException -> 0x0073 }
            goto L_0x0049
        L_0x0073:
            r1 = move-exception
            goto L_0x0049
        L_0x0075:
            r0 = move-exception
            java.io.RandomAccessFile r1 = r4.f449a     // Catch:{ all -> 0x0080 }
            if (r1 == 0) goto L_0x007f
            java.io.RandomAccessFile r1 = r4.f449a     // Catch:{ IOException -> 0x0083 }
            r1.close()     // Catch:{ IOException -> 0x0083 }
        L_0x007f:
            throw r0     // Catch:{ all -> 0x0080 }
        L_0x0080:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0083:
            r1 = move-exception
            goto L_0x007f
        L_0x0085:
            r1 = move-exception
            goto L_0x0049
        */
        throw new UnsupportedOperationException("Method not decompiled: com.aps.be.a():int");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:40:0x0061=Splitter:B:40:0x0061, B:82:0x00d7=Splitter:B:82:0x00d7, B:29:0x003c=Splitter:B:29:0x003c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.aps.ag a(int r8) {
        /*
            r7 = this;
            r0 = 0
            monitor-enter(r7)
            com.aps.ah r1 = r7.f450b     // Catch:{ all -> 0x001a }
            if (r1 != 0) goto L_0x0008
        L_0x0006:
            monitor-exit(r7)
            return r0
        L_0x0008:
            monitor-enter(r7)     // Catch:{ all -> 0x001a }
            com.aps.ah r1 = r7.f450b     // Catch:{ all -> 0x0017 }
            java.io.File r1 = r1.b()     // Catch:{ all -> 0x0017 }
            r7.c = r1     // Catch:{ all -> 0x0017 }
            java.io.File r1 = r7.c     // Catch:{ all -> 0x0017 }
            if (r1 != 0) goto L_0x001d
            monitor-exit(r7)     // Catch:{ all -> 0x0017 }
            goto L_0x0006
        L_0x0017:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x001a }
            throw r0     // Catch:{ all -> 0x001a }
        L_0x001a:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        L_0x001d:
            java.io.RandomAccessFile r1 = new java.io.RandomAccessFile     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            java.io.File r2 = r7.c     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            java.lang.String r3 = "rw"
            r1.<init>(r2, r3)     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            r7.f449a = r1     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            java.util.BitSet r1 = r7.b()     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            if (r1 != 0) goto L_0x003e
            java.io.File r1 = r7.c     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            r1.delete()     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            java.io.RandomAccessFile r1 = r7.f449a     // Catch:{ all -> 0x0017 }
            if (r1 == 0) goto L_0x003c
            java.io.RandomAccessFile r1 = r7.f449a     // Catch:{ Exception -> 0x00e2 }
            r1.close()     // Catch:{ Exception -> 0x00e2 }
        L_0x003c:
            monitor-exit(r7)     // Catch:{ all -> 0x0017 }
            goto L_0x0006
        L_0x003e:
            int r1 = r7.a(r1)     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            java.io.File r2 = r7.c     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            long r2 = r2.length()     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            int r2 = (int) r2     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            int r2 = a(r1, r2, r8)     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            java.util.ArrayList r3 = r7.a(r1, r2)     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            if (r3 != 0) goto L_0x0063
            java.io.File r1 = r7.c     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            r1.delete()     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            java.io.RandomAccessFile r1 = r7.f449a     // Catch:{ all -> 0x0017 }
            if (r1 == 0) goto L_0x0061
            java.io.RandomAccessFile r1 = r7.f449a     // Catch:{ Exception -> 0x00e0 }
            r1.close()     // Catch:{ Exception -> 0x00e0 }
        L_0x0061:
            monitor-exit(r7)     // Catch:{ all -> 0x0017 }
            goto L_0x0006
        L_0x0063:
            r4 = 2
            int[] r4 = new int[r4]     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            r5 = 0
            com.aps.ah r6 = r7.f450b     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            int r6 = r6.a()     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            int r1 = r1 - r6
            int r1 = r1 + -4
            int r1 = r1 / 1500
            r4[r5] = r1     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            r1 = 1
            com.aps.ah r5 = r7.f450b     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            int r5 = r5.a()     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            int r2 = r2 - r5
            int r2 = r2 + -4
            int r2 = r2 / 1500
            r4[r1] = r2     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            com.aps.ag r1 = new com.aps.ag     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            java.io.File r2 = r7.c     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            r1.<init>(r2, r3, r4)     // Catch:{ FileNotFoundException -> 0x00af, Exception -> 0x00be, all -> 0x00cd }
            java.io.RandomAccessFile r2 = r7.f449a     // Catch:{ all -> 0x0017 }
            if (r2 == 0) goto L_0x0092
            java.io.RandomAccessFile r2 = r7.f449a     // Catch:{ Exception -> 0x00de }
            r2.close()     // Catch:{ Exception -> 0x00de }
        L_0x0092:
            if (r1 == 0) goto L_0x00a4
            int r2 = r1.c()     // Catch:{ all -> 0x0017 }
            r3 = 100
            if (r2 <= r3) goto L_0x00a4
            int r2 = r1.c()     // Catch:{ all -> 0x0017 }
            r3 = 5242880(0x500000, float:7.34684E-39)
            if (r2 < r3) goto L_0x00d8
        L_0x00a4:
            java.io.File r1 = r7.c     // Catch:{ all -> 0x0017 }
            r1.delete()     // Catch:{ all -> 0x0017 }
            r1 = 0
            r7.c = r1     // Catch:{ all -> 0x0017 }
            monitor-exit(r7)     // Catch:{ all -> 0x0017 }
            goto L_0x0006
        L_0x00af:
            r1 = move-exception
            java.io.RandomAccessFile r1 = r7.f449a     // Catch:{ all -> 0x0017 }
            if (r1 == 0) goto L_0x00e5
            java.io.RandomAccessFile r1 = r7.f449a     // Catch:{ Exception -> 0x00bb }
            r1.close()     // Catch:{ Exception -> 0x00bb }
            r1 = r0
            goto L_0x0092
        L_0x00bb:
            r1 = move-exception
            r1 = r0
            goto L_0x0092
        L_0x00be:
            r1 = move-exception
            java.io.RandomAccessFile r1 = r7.f449a     // Catch:{ all -> 0x0017 }
            if (r1 == 0) goto L_0x00e5
            java.io.RandomAccessFile r1 = r7.f449a     // Catch:{ Exception -> 0x00ca }
            r1.close()     // Catch:{ Exception -> 0x00ca }
            r1 = r0
            goto L_0x0092
        L_0x00ca:
            r1 = move-exception
            r1 = r0
            goto L_0x0092
        L_0x00cd:
            r0 = move-exception
            java.io.RandomAccessFile r1 = r7.f449a     // Catch:{ all -> 0x0017 }
            if (r1 == 0) goto L_0x00d7
            java.io.RandomAccessFile r1 = r7.f449a     // Catch:{ Exception -> 0x00dc }
            r1.close()     // Catch:{ Exception -> 0x00dc }
        L_0x00d7:
            throw r0     // Catch:{ all -> 0x0017 }
        L_0x00d8:
            monitor-exit(r7)     // Catch:{ all -> 0x001a }
            r0 = r1
            goto L_0x0006
        L_0x00dc:
            r1 = move-exception
            goto L_0x00d7
        L_0x00de:
            r2 = move-exception
            goto L_0x0092
        L_0x00e0:
            r1 = move-exception
            goto L_0x0061
        L_0x00e2:
            r1 = move-exception
            goto L_0x003c
        L_0x00e5:
            r1 = r0
            goto L_0x0092
        */
        throw new UnsupportedOperationException("Method not decompiled: com.aps.be.a(int):com.aps.ag");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.BitSet.set(int, boolean):void}
     arg types: [int, int]
     candidates:
      ClspMth{java.util.BitSet.set(int, int):void}
      ClspMth{java.util.BitSet.set(int, boolean):void} */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:18:0x0053=Splitter:B:18:0x0053, B:56:0x0096=Splitter:B:56:0x0096, B:23:0x005c=Splitter:B:23:0x005c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(com.aps.ag r5) {
        /*
            r4 = this;
            r0 = 0
            monitor-enter(r4)
            monitor-enter(r4)     // Catch:{ all -> 0x006f }
            java.io.File r1 = r5.f412a     // Catch:{ all -> 0x006c }
            r4.c = r1     // Catch:{ all -> 0x006c }
            java.io.File r1 = r4.c     // Catch:{ all -> 0x006c }
            if (r1 != 0) goto L_0x000e
            monitor-exit(r4)     // Catch:{ all -> 0x006c }
        L_0x000c:
            monitor-exit(r4)
            return
        L_0x000e:
            java.io.RandomAccessFile r1 = new java.io.RandomAccessFile     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
            java.io.File r2 = r4.c     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
            java.lang.String r3 = "rw"
            r1.<init>(r2, r3)     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
            r4.f449a = r1     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
            com.aps.ah r1 = r4.f450b     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
            int r1 = r1.a()     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
            byte[] r1 = new byte[r1]     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
            java.io.RandomAccessFile r2 = r4.f449a     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
            r2.read(r1)     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
            java.util.BitSet r0 = com.aps.ah.b(r1)     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
            boolean r1 = r5.b()     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
            if (r1 == 0) goto L_0x0053
            int[] r1 = r5.f413b     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
            r2 = 0
            r1 = r1[r2]     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
        L_0x0035:
            int[] r2 = r5.f413b     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
            r3 = 1
            r2 = r2[r3]     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
            if (r1 > r2) goto L_0x0043
            r2 = 0
            r0.set(r1, r2)     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
            int r1 = r1 + 1
            goto L_0x0035
        L_0x0043:
            java.io.RandomAccessFile r1 = r4.f449a     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
            r2 = 0
            r1.seek(r2)     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
            java.io.RandomAccessFile r1 = r4.f449a     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
            byte[] r2 = com.aps.ah.a(r0)     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
            r1.write(r2)     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x007f, all -> 0x008c }
        L_0x0053:
            java.io.RandomAccessFile r1 = r4.f449a     // Catch:{ all -> 0x006c }
            if (r1 == 0) goto L_0x005c
            java.io.RandomAccessFile r1 = r4.f449a     // Catch:{ IOException -> 0x0099 }
            r1.close()     // Catch:{ IOException -> 0x0099 }
        L_0x005c:
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x006c }
            if (r0 == 0) goto L_0x0067
            java.io.File r0 = r4.c     // Catch:{ all -> 0x006c }
            r0.delete()     // Catch:{ all -> 0x006c }
        L_0x0067:
            r0 = 0
            r4.c = r0     // Catch:{ all -> 0x006c }
            monitor-exit(r4)     // Catch:{ all -> 0x006c }
            goto L_0x000c
        L_0x006c:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x006f }
            throw r0     // Catch:{ all -> 0x006f }
        L_0x006f:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0072:
            r1 = move-exception
            java.io.RandomAccessFile r1 = r4.f449a     // Catch:{ all -> 0x006c }
            if (r1 == 0) goto L_0x005c
            java.io.RandomAccessFile r1 = r4.f449a     // Catch:{ IOException -> 0x007d }
            r1.close()     // Catch:{ IOException -> 0x007d }
            goto L_0x005c
        L_0x007d:
            r1 = move-exception
            goto L_0x005c
        L_0x007f:
            r1 = move-exception
            java.io.RandomAccessFile r1 = r4.f449a     // Catch:{ all -> 0x006c }
            if (r1 == 0) goto L_0x005c
            java.io.RandomAccessFile r1 = r4.f449a     // Catch:{ IOException -> 0x008a }
            r1.close()     // Catch:{ IOException -> 0x008a }
            goto L_0x005c
        L_0x008a:
            r1 = move-exception
            goto L_0x005c
        L_0x008c:
            r0 = move-exception
            java.io.RandomAccessFile r1 = r4.f449a     // Catch:{ all -> 0x006c }
            if (r1 == 0) goto L_0x0096
            java.io.RandomAccessFile r1 = r4.f449a     // Catch:{ IOException -> 0x0097 }
            r1.close()     // Catch:{ IOException -> 0x0097 }
        L_0x0096:
            throw r0     // Catch:{ all -> 0x006c }
        L_0x0097:
            r1 = move-exception
            goto L_0x0096
        L_0x0099:
            r1 = move-exception
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.aps.be.a(com.aps.ag):void");
    }
}
