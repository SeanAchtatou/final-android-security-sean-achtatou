package com.aps;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import java.io.File;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private static d f456a = null;

    /* renamed from: b  reason: collision with root package name */
    private LinkedHashMap<String, List<a>> f457b = new LinkedHashMap<>();
    private p c = null;
    private long d = 0;

    class a {

        /* renamed from: a  reason: collision with root package name */
        private c f458a = null;

        /* renamed from: b  reason: collision with root package name */
        private String f459b = null;

        protected a() {
        }

        public c a() {
            return this.f458a;
        }

        public void a(c cVar) {
            this.f458a = cVar;
        }

        public void a(String str) {
            this.f459b = str.replace("##", "#");
        }

        public String b() {
            return this.f459b;
        }
    }

    private d(Context context) {
        if (context != null) {
            try {
                File b2 = b(context);
                if (b2 != null) {
                    this.c = p.a(b2, 1, 1048576);
                }
            } catch (Throwable th) {
            }
        }
    }

    private double a(double[] dArr, double[] dArr2) {
        double d2 = 0.0d;
        double d3 = 0.0d;
        double d4 = 0.0d;
        for (int i = 0; i < dArr.length; i++) {
            d3 += dArr[i] * dArr[i];
            d2 += dArr2[i] * dArr2[i];
            d4 += dArr[i] * dArr2[i];
        }
        return d4 / (Math.sqrt(d3) * Math.sqrt(d2));
    }

    private a a(String str, StringBuilder sb, String str2, String str3) {
        String str4;
        List list;
        boolean z;
        a aVar;
        a aVar2 = null;
        Hashtable hashtable = new Hashtable();
        Hashtable hashtable2 = new Hashtable();
        Hashtable hashtable3 = new Hashtable();
        Iterator<Map.Entry<String, List<a>>> it = str3.equals("mem") ? this.f457b.entrySet().iterator() : null;
        boolean z2 = true;
        while (it != null && it.hasNext()) {
            if (z2) {
                str4 = str;
                list = this.f457b.get(str);
                z = false;
            } else {
                Map.Entry entry = (Map.Entry) it.next();
                str4 = (String) entry.getKey();
                list = (List) entry.getValue();
                z = z2;
            }
            if (aVar2 != null) {
                break;
            } else if (list == null) {
                z2 = z;
            } else {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= list.size()) {
                        aVar = aVar2;
                        break;
                    }
                    aVar = (a) list.get(i2);
                    if (!TextUtils.isEmpty(aVar.b()) && !TextUtils.isEmpty(sb) && str4.indexOf(str2) != -1) {
                        boolean z3 = a(aVar.b(), sb) ? aVar.a().g() <= 300.0f : false;
                        a(aVar.b(), hashtable);
                        a(sb.toString(), hashtable2);
                        hashtable3.clear();
                        for (String put : hashtable.keySet()) {
                            hashtable3.put(put, "");
                        }
                        for (String put2 : hashtable2.keySet()) {
                            hashtable3.put(put2, "");
                        }
                        Set keySet = hashtable3.keySet();
                        double[] dArr = new double[keySet.size()];
                        double[] dArr2 = new double[keySet.size()];
                        int i3 = 0;
                        Iterator it2 = keySet.iterator();
                        while (true) {
                            int i4 = i3;
                            if (!it2.hasNext()) {
                                break;
                            }
                            String str5 = (String) it2.next();
                            dArr[i4] = hashtable.containsKey(str5) ? 1.0d : 0.0d;
                            dArr2[i4] = hashtable2.containsKey(str5) ? 1.0d : 0.0d;
                            i3 = i4 + 1;
                        }
                        keySet.clear();
                        double a2 = a(dArr, dArr2);
                        if (!str3.equals("mem")) {
                            if (str3.equals("db") && a2 > 0.8500000238418579d) {
                                break;
                            }
                        } else if ((z3 && a2 > 0.8500000238418579d) || a2 > 0.8500000238418579d) {
                            break;
                        }
                    }
                    i = i2 + 1;
                }
                z2 = z;
                aVar2 = aVar;
            }
        }
        hashtable.clear();
        hashtable2.clear();
        hashtable3.clear();
        return aVar2;
    }

    static synchronized d a(Context context) {
        d dVar;
        synchronized (d.class) {
            if (f456a == null) {
                f456a = new d(context);
            }
            dVar = f456a;
        }
        return dVar;
    }

    private void a(String str, Hashtable<String, String> hashtable) {
        hashtable.clear();
        for (String str2 : str.split("#")) {
            if (str2.length() > 0) {
                hashtable.put(str2, "");
            }
        }
    }

    private boolean a(String str, String str2) {
        Hashtable hashtable = new Hashtable();
        Hashtable hashtable2 = new Hashtable();
        Hashtable hashtable3 = new Hashtable();
        a(str2, hashtable);
        a(str, hashtable2);
        hashtable3.clear();
        for (String put : hashtable.keySet()) {
            hashtable3.put(put, "");
        }
        for (String put2 : hashtable2.keySet()) {
            hashtable3.put(put2, "");
        }
        Set keySet = hashtable3.keySet();
        double[] dArr = new double[keySet.size()];
        double[] dArr2 = new double[keySet.size()];
        int i = 0;
        Iterator it = keySet.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                break;
            }
            String str3 = (String) it.next();
            dArr[i2] = hashtable.containsKey(str3) ? 1.0d : 0.0d;
            dArr2[i2] = hashtable2.containsKey(str3) ? 1.0d : 0.0d;
            i = i2 + 1;
        }
        keySet.clear();
        double a2 = a(dArr, dArr2);
        hashtable.clear();
        hashtable2.clear();
        hashtable3.clear();
        return a2 > 0.8500000238418579d;
    }

    private boolean a(String str, StringBuilder sb) {
        int indexOf = str.indexOf(",access");
        if (indexOf == -1 || indexOf < 17 || sb.indexOf(",access") == -1) {
            return false;
        }
        return sb.toString().indexOf(new StringBuilder().append(str.substring(indexOf + -17, indexOf)).append(",access").toString()) != -1;
    }

    private File b(Context context) {
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(context.getExternalCacheDir().getAbsolutePath()).append(File.separator);
        sb.append("locationCache");
        return new File(sb.toString());
    }

    private static void c() {
        f456a = null;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0030, code lost:
        if (r8.length() == 0) goto L_0x0032;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.aps.c a(java.lang.String r7, java.lang.StringBuilder r8, java.lang.String r9) {
        /*
            r6 = this;
            r1 = -1
            r3 = 0
            java.lang.String r0 = "mem"
            boolean r0 = r9.equals(r0)
            if (r0 == 0) goto L_0x0012
            boolean r0 = com.aps.f.k
            if (r0 != 0) goto L_0x0012
            r6.a()
        L_0x0011:
            return r3
        L_0x0012:
            java.lang.String r0 = ""
            if (r7 == 0) goto L_0x00a6
            java.lang.String r0 = "#cellwifi"
            int r0 = r7.indexOf(r0)
            if (r0 == r1) goto L_0x00a6
            java.lang.String r0 = "#cellwifi"
            com.aps.d$a r2 = r6.a(r7, r8, r0, r9)
            if (r2 == 0) goto L_0x00a3
            java.lang.String r0 = "found#cellwifi"
        L_0x0028:
            if (r2 != 0) goto L_0x009d
            if (r8 == 0) goto L_0x0032
            int r0 = r8.length()     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            if (r0 != 0) goto L_0x0039
        L_0x0032:
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            java.lang.String r0 = "cell#"
            r8.<init>(r0)     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
        L_0x0039:
            com.aps.p r0 = r6.c     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            if (r0 == 0) goto L_0x0151
            com.aps.p r0 = r6.c     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            java.util.Map r0 = r0.a(r7)     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
        L_0x0043:
            if (r0 == 0) goto L_0x009d
            java.util.Set r0 = r0.entrySet()     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
        L_0x004d:
            if (r4 == 0) goto L_0x009d
            boolean r0 = r4.hasNext()     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            if (r0 == 0) goto L_0x009d
            java.lang.Object r0 = r4.next()     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            java.lang.Object r1 = r0.getKey()     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            java.lang.String r5 = r8.toString()     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            boolean r1 = r6.a(r1, r5)     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            if (r1 == 0) goto L_0x014f
            java.lang.Object r0 = r0.getValue()     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            r1.<init>(r0)     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            com.aps.c r0 = new com.aps.c     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            r0.<init>(r1)     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            java.lang.String r1 = "mem"
            r0.g(r1)     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            com.aps.d$a r1 = new com.aps.d$a     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            r1.<init>()     // Catch:{ JSONException -> 0x0148, IOException -> 0x0141, Throwable -> 0x013a }
            r1.a(r0)     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
            java.lang.String r0 = r8.toString()     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
            r1.a(r0)     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
            java.util.LinkedHashMap<java.lang.String, java.util.List<com.aps.d$a>> r0 = r6.f457b     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
            if (r0 != 0) goto L_0x009a
            java.util.LinkedHashMap r0 = new java.util.LinkedHashMap     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
            r0.<init>()     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
            r6.f457b = r0     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
        L_0x009a:
            if (r7 != 0) goto L_0x00f9
            r2 = r1
        L_0x009d:
            if (r2 != 0) goto L_0x0134
            r0 = r3
        L_0x00a0:
            r3 = r0
            goto L_0x0011
        L_0x00a3:
            java.lang.String r0 = "no found"
            goto L_0x0028
        L_0x00a6:
            if (r7 == 0) goto L_0x00c0
            java.lang.String r0 = "#wifi"
            int r0 = r7.indexOf(r0)
            if (r0 == r1) goto L_0x00c0
            java.lang.String r0 = "#wifi"
            com.aps.d$a r2 = r6.a(r7, r8, r0, r9)
            if (r2 == 0) goto L_0x00bc
            java.lang.String r0 = "found#wifi"
            goto L_0x0028
        L_0x00bc:
            java.lang.String r0 = "no found"
            goto L_0x0028
        L_0x00c0:
            if (r7 == 0) goto L_0x0156
            java.lang.String r0 = "#cell"
            int r0 = r7.indexOf(r0)
            if (r0 == r1) goto L_0x0156
            java.lang.String r0 = "mem"
            boolean r0 = r9.equals(r0)
            if (r0 == 0) goto L_0x0154
            java.util.LinkedHashMap<java.lang.String, java.util.List<com.aps.d$a>> r0 = r6.f457b
            java.lang.Object r0 = r0.get(r7)
            java.util.List r0 = (java.util.List) r0
            if (r0 == 0) goto L_0x0154
            int r1 = r0.size()
            if (r1 <= 0) goto L_0x0154
            int r1 = r0.size()
            int r1 = r1 + -1
            java.lang.Object r0 = r0.get(r1)
            com.aps.d$a r0 = (com.aps.d.a) r0
            r2 = r0
        L_0x00ef:
            if (r2 == 0) goto L_0x00f5
            java.lang.String r0 = "found#cell"
            goto L_0x0028
        L_0x00f5:
            java.lang.String r0 = "no found"
            goto L_0x0028
        L_0x00f9:
            java.util.LinkedHashMap<java.lang.String, java.util.List<com.aps.d$a>> r0 = r6.f457b     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
            boolean r0 = r0.containsKey(r7)     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
            if (r0 == 0) goto L_0x0125
            java.util.LinkedHashMap<java.lang.String, java.util.List<com.aps.d$a>> r0 = r6.f457b     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
            java.lang.Object r0 = r0.get(r7)     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
            java.util.List r0 = (java.util.List) r0     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
            if (r0 == 0) goto L_0x0115
            boolean r2 = r0.contains(r1)     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
            if (r2 != 0) goto L_0x0115
            r2 = 0
            r0.add(r2, r1)     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
        L_0x0115:
            if (r0 == 0) goto L_0x0121
            java.util.LinkedHashMap<java.lang.String, java.util.List<com.aps.d$a>> r2 = r6.f457b     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
            r2.remove(r7)     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
            java.util.LinkedHashMap<java.lang.String, java.util.List<com.aps.d$a>> r2 = r6.f457b     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
            r2.put(r7, r0)     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
        L_0x0121:
            r0 = r1
        L_0x0122:
            r2 = r0
            goto L_0x004d
        L_0x0125:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
            r0.<init>()     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
            r0.add(r1)     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
            java.util.LinkedHashMap<java.lang.String, java.util.List<com.aps.d$a>> r2 = r6.f457b     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
            r2.put(r7, r0)     // Catch:{ JSONException -> 0x014b, IOException -> 0x0144, Throwable -> 0x013d }
            r0 = r1
            goto L_0x0122
        L_0x0134:
            com.aps.c r0 = r2.a()
            goto L_0x00a0
        L_0x013a:
            r0 = move-exception
            goto L_0x009d
        L_0x013d:
            r0 = move-exception
            r2 = r1
            goto L_0x009d
        L_0x0141:
            r0 = move-exception
            goto L_0x009d
        L_0x0144:
            r0 = move-exception
            r2 = r1
            goto L_0x009d
        L_0x0148:
            r0 = move-exception
            goto L_0x009d
        L_0x014b:
            r0 = move-exception
            r2 = r1
            goto L_0x009d
        L_0x014f:
            r0 = r2
            goto L_0x0122
        L_0x0151:
            r0 = r3
            goto L_0x0043
        L_0x0154:
            r2 = r3
            goto L_0x00ef
        L_0x0156:
            r2 = r3
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.aps.d.a(java.lang.String, java.lang.StringBuilder, java.lang.String):com.aps.c");
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.d = 0;
        this.f457b.clear();
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00ac, code lost:
        if (r10.length() == 0) goto L_0x00ae;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r8, com.aps.c r9, java.lang.StringBuilder r10) {
        /*
            r7 = this;
            r2 = 0
            boolean r0 = com.aps.f.k
            if (r0 != 0) goto L_0x0009
            r7.a()
        L_0x0008:
            return
        L_0x0009:
            boolean r0 = r7.a(r8, r9)
            if (r0 == 0) goto L_0x0008
            java.lang.String r0 = r9.i()
            java.lang.String r1 = "mem"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0008
            if (r8 == 0) goto L_0x011f
            java.lang.String r0 = "wifi"
            boolean r0 = r8.contains(r0)
            if (r0 == 0) goto L_0x011f
            boolean r0 = android.text.TextUtils.isEmpty(r10)
            if (r0 != 0) goto L_0x0008
            float r0 = r9.g()
            r1 = 1133903872(0x43960000, float:300.0)
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0113
            java.lang.String r0 = r10.toString()
            java.lang.String r1 = "#"
            java.lang.String[] r3 = r0.split(r1)
            int r4 = r3.length
            r1 = r2
            r0 = r2
        L_0x0042:
            if (r1 >= r4) goto L_0x0053
            r5 = r3[r1]
            java.lang.String r6 = ","
            boolean r5 = r5.contains(r6)
            if (r5 == 0) goto L_0x0050
            int r0 = r0 + 1
        L_0x0050:
            int r1 = r1 + 1
            goto L_0x0042
        L_0x0053:
            r1 = 6
            if (r0 >= r1) goto L_0x0008
        L_0x0056:
            long r0 = com.aps.t.a()
            r7.d = r0
            com.aps.d$a r1 = new com.aps.d$a
            r1.<init>()
            java.lang.String r0 = "mem"
            r9.g(r0)
            r1.a(r9)
            if (r10 == 0) goto L_0x0072
            java.lang.String r0 = r10.toString()
            r1.a(r0)
        L_0x0072:
            java.util.LinkedHashMap<java.lang.String, java.util.List<com.aps.d$a>> r0 = r7.f457b
            if (r0 != 0) goto L_0x007d
            java.util.LinkedHashMap r0 = new java.util.LinkedHashMap
            r0.<init>()
            r7.f457b = r0
        L_0x007d:
            if (r8 == 0) goto L_0x0008
            java.util.LinkedHashMap<java.lang.String, java.util.List<com.aps.d$a>> r0 = r7.f457b
            boolean r0 = r0.containsKey(r8)
            if (r0 == 0) goto L_0x0134
            java.util.LinkedHashMap<java.lang.String, java.util.List<com.aps.d$a>> r0 = r7.f457b
            java.lang.Object r0 = r0.get(r8)
            java.util.List r0 = (java.util.List) r0
            if (r0 == 0) goto L_0x009a
            boolean r3 = r0.contains(r1)
            if (r3 != 0) goto L_0x009a
            r0.add(r2, r1)
        L_0x009a:
            if (r0 == 0) goto L_0x00a6
            java.util.LinkedHashMap<java.lang.String, java.util.List<com.aps.d$a>> r1 = r7.f457b
            r1.remove(r8)
            java.util.LinkedHashMap<java.lang.String, java.util.List<com.aps.d$a>> r1 = r7.f457b
            r1.put(r8, r0)
        L_0x00a6:
            if (r10 == 0) goto L_0x00ae
            int r0 = r10.length()     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            if (r0 != 0) goto L_0x00b5
        L_0x00ae:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            java.lang.String r0 = "cell#"
            r10.<init>(r0)     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
        L_0x00b5:
            r0 = 0
            com.aps.p r1 = r7.c     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            if (r1 == 0) goto L_0x019e
            com.aps.p r0 = r7.c     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            java.util.Map r0 = r0.a(r8)     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            r4 = r0
        L_0x00c1:
            if (r4 == 0) goto L_0x00c9
            int r0 = r4.size()     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            if (r0 != 0) goto L_0x0143
        L_0x00c9:
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            r0.<init>()     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            java.lang.String r1 = r10.toString()     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            java.lang.String r2 = r9.u()     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            r0.put(r1, r2)     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            com.aps.p r1 = r7.c     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            if (r1 == 0) goto L_0x00e2
            com.aps.p r1 = r7.c     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            r1.b(r8, r0)     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
        L_0x00e2:
            java.lang.String r0 = ""
            java.util.LinkedHashMap<java.lang.String, java.util.List<com.aps.d$a>> r0 = r7.f457b
            int r0 = r0.size()
            r1 = 360(0x168, float:5.04E-43)
            if (r0 <= r1) goto L_0x0008
            java.util.LinkedHashMap<java.lang.String, java.util.List<com.aps.d$a>> r0 = r7.f457b
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r0 = r0.iterator()
            if (r0 == 0) goto L_0x0008
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0008
            java.lang.Object r0 = r0.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r0 = r0.getKey()
            java.lang.String r0 = (java.lang.String) r0
            java.util.LinkedHashMap<java.lang.String, java.util.List<com.aps.d$a>> r1 = r7.f457b
            r1.remove(r0)
            goto L_0x0008
        L_0x0113:
            float r0 = r9.g()
            r1 = 1092616192(0x41200000, float:10.0)
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x0056
            goto L_0x0008
        L_0x011f:
            if (r8 == 0) goto L_0x0056
            java.lang.String r0 = "cell"
            boolean r0 = r8.contains(r0)
            if (r0 == 0) goto L_0x0056
            java.lang.String r0 = ","
            int r0 = r10.indexOf(r0)
            r1 = -1
            if (r0 == r1) goto L_0x0056
            goto L_0x0008
        L_0x0134:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r0.add(r1)
            java.util.LinkedHashMap<java.lang.String, java.util.List<com.aps.d$a>> r1 = r7.f457b
            r1.put(r8, r0)
            goto L_0x00a6
        L_0x0143:
            java.util.Set r0 = r4.entrySet()     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            java.util.Iterator r5 = r0.iterator()     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            r3 = 1
        L_0x014c:
            if (r5 == 0) goto L_0x019c
            boolean r0 = r5.hasNext()     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            if (r0 == 0) goto L_0x019c
            java.lang.Object r0 = r5.next()     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            java.lang.Object r1 = r0.getKey()     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            java.lang.String r6 = r10.toString()     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            boolean r1 = r7.a(r1, r6)     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            if (r1 == 0) goto L_0x014c
            java.lang.Object r0 = r0.getKey()     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            r4.remove(r0)     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            java.lang.String r0 = r10.toString()     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            java.lang.String r1 = r9.u()     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            r4.put(r0, r1)     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            com.aps.p r0 = r7.c     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            r0.b(r8, r4)     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            r0 = r2
        L_0x0182:
            if (r0 == 0) goto L_0x00e2
            java.lang.String r0 = r10.toString()     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            java.lang.String r1 = r9.u()     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            r4.put(r0, r1)     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            com.aps.p r0 = r7.c     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            r0.b(r8, r4)     // Catch:{ IOException -> 0x0196, Exception -> 0x0199 }
            goto L_0x00e2
        L_0x0196:
            r0 = move-exception
            goto L_0x00e2
        L_0x0199:
            r0 = move-exception
            goto L_0x00e2
        L_0x019c:
            r0 = r3
            goto L_0x0182
        L_0x019e:
            r4 = r0
            goto L_0x00c1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.aps.d.a(java.lang.String, com.aps.c, java.lang.StringBuilder):void");
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str, c cVar) {
        return (str == null || cVar == null || str.indexOf("#network") == -1 || cVar.e() == 0.0d) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.c != null) {
            this.c.a();
        }
        c();
    }
}
