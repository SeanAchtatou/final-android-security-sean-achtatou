package com.aps;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

final class aa implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    protected byte[] f402a = new byte[16];

    /* renamed from: b  reason: collision with root package name */
    protected byte[] f403b = new byte[16];
    protected byte[] c = new byte[16];
    protected short d = 0;
    protected short e = 0;
    protected byte f = 0;
    protected byte[] g = new byte[16];
    protected byte[] h = new byte[32];
    protected short i = 0;
    protected ArrayList j = new ArrayList();
    private byte k = 41;
    private short l = 0;

    aa() {
    }

    private Boolean a(DataOutputStream dataOutputStream) {
        Boolean.valueOf(true);
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream2 = new DataOutputStream(byteArrayOutputStream);
            dataOutputStream2.flush();
            dataOutputStream2.write(this.f402a);
            dataOutputStream2.write(this.f403b);
            dataOutputStream2.write(this.c);
            dataOutputStream2.writeShort(this.d);
            dataOutputStream2.writeShort(this.e);
            dataOutputStream2.writeByte(this.f);
            this.g[15] = 0;
            dataOutputStream2.write(ah.a(this.g, this.g.length));
            this.h[31] = 0;
            dataOutputStream2.write(ah.a(this.h, this.h.length));
            dataOutputStream2.writeShort(this.i);
            for (short s = 0; s < this.i; s = (short) (s + 1)) {
                ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
                DataOutputStream dataOutputStream3 = new DataOutputStream(byteArrayOutputStream2);
                dataOutputStream3.flush();
                x xVar = (x) this.j.get(s);
                if (xVar.c != null && !xVar.c.a(dataOutputStream3).booleanValue()) {
                    Boolean.valueOf(false);
                }
                if (xVar.d != null && !xVar.d.a(dataOutputStream3).booleanValue()) {
                    Boolean.valueOf(false);
                }
                if (xVar.e != null && !xVar.e.a(dataOutputStream3).booleanValue()) {
                    Boolean.valueOf(false);
                }
                if (xVar.f != null && !xVar.f.a(dataOutputStream3).booleanValue()) {
                    Boolean.valueOf(false);
                }
                if (xVar.g != null && !xVar.g.a(dataOutputStream3).booleanValue()) {
                    Boolean.valueOf(false);
                }
                xVar.f498a = Integer.valueOf(byteArrayOutputStream2.size() + 4).shortValue();
                dataOutputStream2.writeShort(xVar.f498a);
                dataOutputStream2.writeInt(xVar.f499b);
                dataOutputStream2.write(byteArrayOutputStream2.toByteArray());
            }
            this.l = Integer.valueOf(byteArrayOutputStream.size()).shortValue();
            dataOutputStream.writeByte(this.k);
            dataOutputStream.writeShort(this.l);
            dataOutputStream.write(byteArrayOutputStream.toByteArray());
            return true;
        } catch (IOException e2) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public final byte[] a() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        a(new DataOutputStream(byteArrayOutputStream));
        return byteArrayOutputStream.toByteArray();
    }
}
