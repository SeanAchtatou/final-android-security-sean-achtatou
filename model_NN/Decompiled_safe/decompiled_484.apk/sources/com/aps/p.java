package com.aps;

import com.aps.g;
import com.qihoo.dynamic.util.Md5Util;
import java.io.File;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class p {

    /* renamed from: a  reason: collision with root package name */
    private static List<File> f483a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    private g f484b;
    private int c;

    class a extends FilterOutputStream {

        /* renamed from: a  reason: collision with root package name */
        private final g.a f485a;

        /* renamed from: b  reason: collision with root package name */
        private boolean f486b;

        private a(OutputStream outputStream, g.a aVar) {
            super(outputStream);
            this.f486b = false;
            this.f485a = aVar;
        }

        public void close() {
            IOException e = null;
            try {
                super.close();
            } catch (IOException e2) {
                e = e2;
            }
            if (this.f486b) {
                this.f485a.b();
            } else {
                this.f485a.a();
            }
            if (e != null) {
                throw e;
            }
        }

        public void flush() {
            try {
                super.flush();
            } catch (IOException e) {
                this.f486b = true;
                throw e;
            }
        }

        public void write(int i) {
            try {
                super.write(i);
            } catch (IOException e) {
                this.f486b = true;
                throw e;
            }
        }

        public void write(byte[] bArr) {
            try {
                super.write(bArr);
            } catch (IOException e) {
                this.f486b = true;
                throw e;
            }
        }

        public void write(byte[] bArr, int i, int i2) {
            try {
                super.write(bArr, i, i2);
            } catch (IOException e) {
                this.f486b = true;
                throw e;
            }
        }
    }

    private p(File file, int i, long j) {
        this.c = i;
        this.f484b = g.a(file, i, 1, j);
    }

    public static synchronized p a(File file, int i, long j) {
        p pVar;
        synchronized (p.class) {
            if (f483a.contains(file)) {
                throw new IllegalStateException("Cache dir " + file.getAbsolutePath() + " was used before.");
            }
            f483a.add(file);
            pVar = new p(file, i, j);
        }
        return pVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0027  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.Map<java.lang.String, java.io.Serializable> a(com.aps.g.c r5) {
        /*
            r4 = this;
            r2 = 0
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch:{ ClassNotFoundException -> 0x001c, all -> 0x002b }
            java.io.BufferedInputStream r0 = new java.io.BufferedInputStream     // Catch:{ ClassNotFoundException -> 0x001c, all -> 0x002b }
            r3 = 0
            java.io.InputStream r3 = r5.a(r3)     // Catch:{ ClassNotFoundException -> 0x001c, all -> 0x002b }
            r0.<init>(r3)     // Catch:{ ClassNotFoundException -> 0x001c, all -> 0x002b }
            r1.<init>(r0)     // Catch:{ ClassNotFoundException -> 0x001c, all -> 0x002b }
            java.lang.Object r0 = r1.readObject()     // Catch:{ ClassNotFoundException -> 0x002e }
            java.util.Map r0 = (java.util.Map) r0     // Catch:{ ClassNotFoundException -> 0x002e }
            if (r1 == 0) goto L_0x001b
            r1.close()
        L_0x001b:
            return r0
        L_0x001c:
            r0 = move-exception
            r1 = r2
        L_0x001e:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException     // Catch:{ all -> 0x0024 }
            r2.<init>(r0)     // Catch:{ all -> 0x0024 }
            throw r2     // Catch:{ all -> 0x0024 }
        L_0x0024:
            r0 = move-exception
        L_0x0025:
            if (r1 == 0) goto L_0x002a
            r1.close()
        L_0x002a:
            throw r0
        L_0x002b:
            r0 = move-exception
            r1 = r2
            goto L_0x0025
        L_0x002e:
            r0 = move-exception
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.aps.p.a(com.aps.g$c):java.util.Map");
    }

    private String b(String str) {
        return c(str);
    }

    private String c(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance(Md5Util.ALGORITHM);
            instance.update(str.getBytes(Md5Util.DEFAULT_CHARSET));
            return new BigInteger(1, instance.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError();
        } catch (UnsupportedEncodingException e2) {
            throw new AssertionError();
        }
    }

    public OutputStream a(String str, Map<String, ? extends Serializable> map) {
        g.a b2 = this.f484b.b(b(str));
        if (b2 == null) {
            return null;
        }
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(b2.a(0));
            objectOutputStream.writeObject(map);
            return new a(objectOutputStream, b2);
        } catch (IOException e) {
            b2.b();
            throw e;
        }
    }

    public Map<String, Serializable> a(String str) {
        g.c a2 = this.f484b.a(b(str));
        if (a2 == null) {
            return null;
        }
        try {
            return a(a2);
        } finally {
            a2.close();
        }
    }

    public void a() {
        try {
            if (f483a != null) {
                f483a.clear();
            }
            if (this.f484b != null) {
                this.f484b.close();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void b(String str, Map<String, ? extends Serializable> map) {
        OutputStream outputStream = null;
        try {
            outputStream = a(str, map);
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }
}
