package com.aps;

import android.location.Location;
import android.net.wifi.ScanResult;
import android.telephony.CellLocation;
import java.util.List;

public final class ay {
    private static int c = 10;
    private static int d = 100;
    private static float f = 0.5f;

    /* renamed from: a  reason: collision with root package name */
    protected bc f436a = new bc(this);

    /* renamed from: b  reason: collision with root package name */
    protected az f437b = new az(this);
    private ak e;

    protected ay(ak akVar) {
        this.e = akVar;
    }

    protected static void a() {
    }

    protected static void a(int i) {
        c = i;
    }

    protected static void b(int i) {
        d = i;
    }

    /* access modifiers changed from: protected */
    public final boolean a(Location location) {
        List j;
        boolean z = false;
        if (!(this.e == null || (j = this.e.j()) == null || location == null)) {
            "cell.list.size: " + j.size();
            ba baVar = null;
            if (j.size() >= 2) {
                ba baVar2 = new ba((CellLocation) j.get(1));
                if (this.f437b.f439b == null) {
                    baVar = baVar2;
                    z = true;
                } else {
                    boolean z2 = location.distanceTo(this.f437b.f439b) > ((float) d);
                    if (!z2) {
                        ba baVar3 = this.f437b.f438a;
                        z2 = !(baVar2.e == baVar3.e && baVar2.d == baVar3.d && baVar2.c == baVar3.c && baVar2.f443b == baVar3.f443b && baVar2.f442a == baVar3.f442a);
                    }
                    "collect cell?: " + z2;
                    z = z2;
                    baVar = baVar2;
                }
            }
            if (z) {
                this.f437b.f438a = baVar;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public final boolean b(Location location) {
        List list;
        boolean z;
        boolean z2;
        int i;
        if (this.e == null) {
            return false;
        }
        List k = this.e.k();
        if (k.size() >= 2) {
            List list2 = (List) k.get(1);
            if (this.f436a.f446b == null) {
                z = true;
            } else if (list2 == null || list2.size() <= 0) {
                list = list2;
                z = false;
            } else {
                z = location.distanceTo(this.f436a.f446b) > ((float) c);
                if (!z) {
                    List list3 = this.f436a.f445a;
                    float f2 = f;
                    if (list2 == null || list3 == null) {
                        z2 = false;
                    } else if (list2 == null || list3 == null) {
                        z2 = false;
                    } else {
                        int size = list2.size();
                        int size2 = list3.size();
                        float f3 = (float) (size + size2);
                        if (size == 0 && size2 == 0) {
                            z2 = true;
                        } else if (size == 0 || size2 == 0) {
                            z2 = false;
                        } else {
                            int i2 = 0;
                            int i3 = 0;
                            while (i2 < size) {
                                String str = ((ScanResult) list2.get(i2)).BSSID;
                                if (str != null) {
                                    int i4 = 0;
                                    while (true) {
                                        if (i4 >= size2) {
                                            break;
                                        } else if (str.equals(((bb) list3.get(i4)).f444a)) {
                                            i = i3 + 1;
                                            break;
                                        } else {
                                            i4++;
                                        }
                                    }
                                    i2++;
                                    i3 = i;
                                }
                                i = i3;
                                i2++;
                                i3 = i;
                            }
                            z2 = ((float) (i3 << 1)) >= f3 * f2;
                        }
                    }
                    z = !z2;
                } else {
                    list = list2;
                }
            }
            list = list2;
        } else {
            list = null;
            z = false;
        }
        if (z) {
            this.f436a.f445a.clear();
            int size3 = list.size();
            for (int i5 = 0; i5 < size3; i5++) {
                this.f436a.f445a.add(new bb(((ScanResult) list.get(i5)).BSSID));
            }
        }
        return z;
    }
}
