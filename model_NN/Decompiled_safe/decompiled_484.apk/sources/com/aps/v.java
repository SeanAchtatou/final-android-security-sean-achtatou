package com.aps;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

final class v implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    protected short f494a = 0;

    /* renamed from: b  reason: collision with root package name */
    protected int f495b = 0;
    protected byte c = 0;
    protected byte d = 0;
    protected ArrayList e = new ArrayList();
    private byte f = 2;

    v() {
    }

    /* access modifiers changed from: protected */
    public final Boolean a(DataOutputStream dataOutputStream) {
        try {
            dataOutputStream.writeByte(this.f);
            dataOutputStream.writeShort(this.f494a);
            dataOutputStream.writeInt(this.f495b);
            dataOutputStream.writeByte(this.c);
            dataOutputStream.writeByte(this.d);
            for (byte b2 = 0; b2 < this.d; b2++) {
                dataOutputStream.writeShort(((aj) this.e.get(b2)).f418a);
                dataOutputStream.writeInt(((aj) this.e.get(b2)).f419b);
                dataOutputStream.writeByte(((aj) this.e.get(b2)).c);
            }
            return true;
        } catch (IOException e2) {
            return null;
        }
    }
}
