package com.aps;

import com.amap.api.location.LocationManagerProxy;
import com.qihoo.dynamic.util.Md5Util;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import javax.xml.parsers.SAXParserFactory;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class m {

    class a extends DefaultHandler {

        /* renamed from: a  reason: collision with root package name */
        public c f477a;

        /* renamed from: b  reason: collision with root package name */
        private String f478b;

        private a() {
            this.f477a = new c();
            this.f478b = "";
        }

        public void characters(char[] cArr, int i, int i2) {
            this.f478b = String.valueOf(cArr, i, i2);
        }

        public void endElement(String str, String str2, String str3) {
            if (str2.equals("retype")) {
                this.f477a.h(this.f478b);
            } else if (str2.equals("adcode")) {
                this.f477a.k(this.f478b);
            } else if (str2.equals("citycode")) {
                this.f477a.i(this.f478b);
            } else if (str2.equals("radius")) {
                try {
                    this.f477a.a(Float.valueOf(this.f478b).floatValue());
                } catch (Throwable th) {
                    th.printStackTrace();
                    t.a(th);
                    this.f477a.a(3891.0f);
                }
            } else if (str2.equals("cenx")) {
                try {
                    this.f478b = q.a(Double.valueOf(this.f478b), "#.000000");
                    this.f477a.a(Double.valueOf(this.f478b).doubleValue());
                } catch (Throwable th2) {
                    th2.printStackTrace();
                    t.a(th2);
                    this.f477a.a(0.0d);
                }
            } else if (str2.equals("ceny")) {
                try {
                    this.f478b = q.a(Double.valueOf(this.f478b), "#.000000");
                    this.f477a.b(Double.valueOf(this.f478b).doubleValue());
                } catch (Throwable th3) {
                    th3.printStackTrace();
                    t.a(th3);
                    this.f477a.b(0.0d);
                }
            } else if (str2.equals("desc")) {
                this.f477a.j(this.f478b);
            } else if (str2.equals("country")) {
                this.f477a.l(this.f478b);
            } else if (str2.equals("province")) {
                this.f477a.m(this.f478b);
            } else if (str2.equals("city")) {
                this.f477a.n(this.f478b);
            } else if (str2.equals("road")) {
                this.f477a.o(this.f478b);
            } else if (str2.equals("street")) {
                this.f477a.p(this.f478b);
            } else if (str2.equals("poiname")) {
                this.f477a.q(this.f478b);
            } else if (str2.equals("BIZ")) {
                if (this.f477a.t() == null) {
                    this.f477a.a(new JSONObject());
                }
                try {
                    this.f477a.t().put("BIZ", this.f478b);
                } catch (Throwable th4) {
                    th4.printStackTrace();
                }
            } else if (str2.equals("flr")) {
                this.f477a.b(this.f478b);
            } else if (str2.equals("pid")) {
                this.f477a.a(this.f478b);
            } else if (str2.equals("apiTime")) {
                try {
                    if (!"".equals(this.f478b)) {
                        this.f477a.a(Long.parseLong(this.f478b));
                    }
                } catch (Throwable th5) {
                    th5.printStackTrace();
                    t.a(th5);
                    this.f477a.a(t.a());
                }
            } else if (str2.equals("coord")) {
                try {
                    this.f477a.d(this.f478b);
                } catch (Throwable th6) {
                    th6.printStackTrace();
                    t.a(th6);
                }
            } else if (str2.equals("mcell")) {
                try {
                    this.f477a.e(this.f478b);
                } catch (Throwable th7) {
                    th7.printStackTrace();
                    t.a(th7);
                }
            } else if (str2.equals("district")) {
                try {
                    this.f477a.c(this.f478b);
                } catch (Throwable th8) {
                    th8.printStackTrace();
                    t.a(th8);
                }
            }
            if (this.f477a.t() == null) {
                this.f477a.a(new JSONObject());
            }
            try {
                if (str2.equals("eab")) {
                    this.f477a.t().put(str2, this.f478b);
                } else if (str2.equals("ctl")) {
                    this.f477a.t().put(str2, this.f478b);
                } else if (str2.equals("suc")) {
                    this.f477a.t().put(str2, this.f478b);
                } else if (str2.equals("spa")) {
                    this.f477a.t().put(str2, this.f478b);
                }
            } catch (Throwable th9) {
                th9.printStackTrace();
            }
        }

        public void startElement(String str, String str2, String str3, Attributes attributes) {
            this.f478b = "";
        }
    }

    class b extends DefaultHandler {

        /* renamed from: a  reason: collision with root package name */
        public String f479a;

        /* renamed from: b  reason: collision with root package name */
        private boolean f480b;

        private b() {
            this.f479a = "";
            this.f480b = false;
        }

        public void characters(char[] cArr, int i, int i2) {
            if (this.f480b) {
                this.f479a = String.valueOf(cArr, i, i2);
            }
        }

        public void endElement(String str, String str2, String str3) {
            if (str2.equals("sres")) {
                this.f480b = false;
            }
        }

        public void startElement(String str, String str2, String str3, Attributes attributes) {
            if (str2.equals("sres")) {
                this.f480b = true;
            }
        }
    }

    protected m() {
    }

    /* access modifiers changed from: package-private */
    public String a(String str) {
        ByteArrayInputStream byteArrayInputStream;
        if (str == null || str.length() == 0) {
            return null;
        }
        try {
            byteArrayInputStream = new ByteArrayInputStream(str.getBytes(Md5Util.DEFAULT_CHARSET));
        } catch (UnsupportedEncodingException e) {
            byteArrayInputStream = null;
        }
        b bVar = new b();
        if (byteArrayInputStream != null) {
            try {
                SAXParserFactory.newInstance().newSAXParser().parse(byteArrayInputStream, bVar);
                byteArrayInputStream.close();
            } catch (SAXException e2) {
            } catch (Throwable th) {
                th.printStackTrace();
                t.a(th);
            }
        }
        return bVar.f479a;
    }

    /* access modifiers changed from: package-private */
    public c b(String str) {
        ByteArrayInputStream byteArrayInputStream;
        if (str == null || str.length() == 0 || str.contains("SuccessCode=\"0\"")) {
            return null;
        }
        try {
            byteArrayInputStream = new ByteArrayInputStream(str.getBytes(Md5Util.DEFAULT_CHARSET));
        } catch (UnsupportedEncodingException e) {
            byteArrayInputStream = null;
        }
        SAXParserFactory newInstance = SAXParserFactory.newInstance();
        a aVar = new a();
        if (byteArrayInputStream != null) {
            try {
                newInstance.newSAXParser().parse(byteArrayInputStream, aVar);
                byteArrayInputStream.close();
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        aVar.f477a.f(LocationManagerProxy.NETWORK_PROVIDER);
        if (aVar.f477a.h() == 0) {
            aVar.f477a.a(t.a());
        }
        return aVar.f477a;
    }
}
