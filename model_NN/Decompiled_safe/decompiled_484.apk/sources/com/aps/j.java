package com.aps;

public class j {

    /* renamed from: a  reason: collision with root package name */
    public double f474a = 0.0d;

    /* renamed from: b  reason: collision with root package name */
    public double f475b = 0.0d;
    public float c = 0.0f;
    int d = -1;
    private long e = -1;

    public long a() {
        return this.e;
    }

    public void a(long j) {
        if (j >= 0) {
            this.e = t.a() + j;
        } else {
            this.e = j;
        }
    }

    public String b() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f474a).append("#").append(this.f475b).append("#").append(this.c);
        return sb.toString();
    }
}
