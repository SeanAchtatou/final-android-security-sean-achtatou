package com.aps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.GpsStatus;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Looper;
import android.provider.Settings;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import com.amap.api.location.LocationManagerProxy;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TreeMap;

public final class ak {
    /* access modifiers changed from: private */
    public static int D = 10000;
    private static ak u = null;
    /* access modifiers changed from: private */
    public Timer A = null;
    private Thread B = null;
    /* access modifiers changed from: private */
    public Looper C = null;

    /* renamed from: a  reason: collision with root package name */
    private Context f420a = null;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public TelephonyManager f421b = null;
    private LocationManager c = null;
    /* access modifiers changed from: private */
    public WifiManager d = null;
    private SensorManager e = null;
    private String f = "";
    private String g = "";
    private String h = "";
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public int j = 0;
    /* access modifiers changed from: private */
    public boolean k = false;
    /* access modifiers changed from: private */
    public long l = -1;
    /* access modifiers changed from: private */
    public String m = "";
    private String n = "";
    /* access modifiers changed from: private */
    public int o = 0;
    /* access modifiers changed from: private */
    public int p = 0;
    private int q = 0;
    private String r = "";
    /* access modifiers changed from: private */
    public long s = 0;
    /* access modifiers changed from: private */
    public long t = 0;
    /* access modifiers changed from: private */
    public am v = null;
    /* access modifiers changed from: private */
    public an w = null;
    /* access modifiers changed from: private */
    public CellLocation x = null;
    private ao y = null;
    /* access modifiers changed from: private */
    public List z = new ArrayList();

    private ak(Context context) {
        if (context != null) {
            this.f420a = context;
            this.f = Build.MODEL;
            this.f421b = (TelephonyManager) context.getSystemService("phone");
            this.c = (LocationManager) context.getSystemService(LocationManagerProxy.KEY_LOCATION_CHANGED);
            this.d = (WifiManager) context.getSystemService("wifi");
            this.e = (SensorManager) context.getSystemService("sensor");
            if (this.f421b != null && this.d != null) {
                try {
                    this.g = this.f421b.getDeviceId();
                } catch (Exception e2) {
                }
                this.h = this.f421b.getSubscriberId();
                if (this.d.getConnectionInfo() != null) {
                    this.n = this.d.getConnectionInfo().getMacAddress();
                    if (this.n != null && this.n.length() > 0) {
                        this.n = this.n.replace(":", "");
                    }
                }
                String[] b2 = b(this.f421b);
                this.o = Integer.parseInt(b2[0]);
                this.p = Integer.parseInt(b2[1]);
                this.q = this.f421b.getNetworkType();
                this.r = context.getPackageName();
                this.i = this.f421b.getPhoneType() == 2;
            }
        }
    }

    private void A() {
        if (this.d != null) {
            try {
                if (bd.f447a) {
                    this.d.startScan();
                }
            } catch (Exception e2) {
            }
        }
    }

    private CellLocation B() {
        CellLocation cellLocation;
        if (this.f421b == null) {
            return null;
        }
        try {
            cellLocation = b((List) ah.a(this.f421b, "getAllCellInfo", new Object[0]));
        } catch (NoSuchMethodException e2) {
            cellLocation = null;
        } catch (Exception e3) {
            cellLocation = null;
        }
        return cellLocation;
    }

    private static int a(CellLocation cellLocation, Context context) {
        if (Settings.System.getInt(context.getContentResolver(), "airplane_mode_on", 0) == 1 || cellLocation == null) {
            return 9;
        }
        if (cellLocation instanceof GsmCellLocation) {
            return 1;
        }
        try {
            Class.forName("android.telephony.cdma.CdmaCellLocation");
            return 2;
        } catch (Exception e2) {
            return 9;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0045 A[EDGE_INSN: B:19:0x0045->B:18:0x0045 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static com.aps.ak a(android.content.Context r3) {
        /*
            com.aps.ak r0 = com.aps.ak.u
            if (r0 != 0) goto L_0x0042
            boolean r0 = c(r3)
            if (r0 == 0) goto L_0x0042
            java.lang.String r0 = "location"
            java.lang.Object r0 = r3.getSystemService(r0)
            android.location.LocationManager r0 = (android.location.LocationManager) r0
            if (r0 == 0) goto L_0x0045
            java.util.List r0 = r0.getAllProviders()
            java.util.Iterator r1 = r0.iterator()
        L_0x001c:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0045
            java.lang.Object r0 = r1.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r2 = "passive"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x0038
            java.lang.String r2 = "gps"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x001c
        L_0x0038:
            r0 = 1
        L_0x0039:
            if (r0 == 0) goto L_0x0042
            com.aps.ak r0 = new com.aps.ak
            r0.<init>(r3)
            com.aps.ak.u = r0
        L_0x0042:
            com.aps.ak r0 = com.aps.ak.u
            return r0
        L_0x0045:
            r0 = 0
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.aps.ak.a(android.content.Context):com.aps.ak");
    }

    private void a(BroadcastReceiver broadcastReceiver) {
        if (broadcastReceiver != null && this.f420a != null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.net.wifi.SCAN_RESULTS");
            this.f420a.registerReceiver(broadcastReceiver, intentFilter);
        }
    }

    static /* synthetic */ void a(ak akVar, GpsStatus.NmeaListener nmeaListener) {
        if (akVar.c != null && nmeaListener != null) {
            akVar.c.addNmeaListener(nmeaListener);
        }
    }

    static /* synthetic */ void a(ak akVar, PhoneStateListener phoneStateListener) {
        if (akVar.f421b != null) {
            akVar.f421b.listen(phoneStateListener, 273);
        }
    }

    private static void a(List list) {
        if (list != null && list.size() > 0) {
            HashMap hashMap = new HashMap();
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= list.size()) {
                    break;
                }
                ScanResult scanResult = (ScanResult) list.get(i3);
                if (scanResult.SSID == null) {
                    scanResult.SSID = "null";
                }
                hashMap.put(Integer.valueOf(scanResult.level), scanResult);
                i2 = i3 + 1;
            }
            TreeMap treeMap = new TreeMap(Collections.reverseOrder());
            treeMap.putAll(hashMap);
            list.clear();
            for (Integer num : treeMap.keySet()) {
                list.add(treeMap.get(num));
            }
            hashMap.clear();
            treeMap.clear();
        }
    }

    private boolean a(CellLocation cellLocation) {
        if (cellLocation == null) {
            return false;
        }
        switch (a(cellLocation, this.f420a)) {
            case 1:
                GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
                if (gsmCellLocation.getLac() == -1 || gsmCellLocation.getLac() == 0 || gsmCellLocation.getLac() > 65535 || gsmCellLocation.getCid() == -1 || gsmCellLocation.getCid() == 0 || gsmCellLocation.getCid() == 65535 || gsmCellLocation.getCid() >= 268435455) {
                    return false;
                }
            case 2:
                try {
                    if (ah.b(cellLocation, "getSystemId", new Object[0]) <= 0 || ah.b(cellLocation, "getNetworkId", new Object[0]) < 0 || ah.b(cellLocation, "getBaseStationId", new Object[0]) < 0) {
                        return false;
                    }
                } catch (Exception e2) {
                    break;
                }
        }
        return true;
    }

    private static boolean a(Object obj) {
        try {
            Method declaredMethod = WifiManager.class.getDeclaredMethod("isScanAlwaysAvailable", null);
            if (declaredMethod != null) {
                return ((Boolean) declaredMethod.invoke(obj, null)).booleanValue();
            }
        } catch (Exception e2) {
        }
        return false;
    }

    private static int b(Object obj) {
        try {
            Method declaredMethod = Sensor.class.getDeclaredMethod("getMinDelay", null);
            if (declaredMethod != null) {
                return ((Integer) declaredMethod.invoke(obj, null)).intValue();
            }
        } catch (Exception e2) {
        }
        return 0;
    }

    /* JADX INFO: additional move instructions added (3) to help type inference */
    /* JADX WARN: Type inference failed for: r6v6 */
    /* JADX WARN: Type inference failed for: r6v7 */
    /* JADX WARN: Type inference failed for: r6v8 */
    /* JADX WARN: Type inference failed for: r6v9 */
    /* JADX WARN: Type inference failed for: r6v10 */
    /* JADX WARN: Type inference failed for: r6v11 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.telephony.CellLocation b(java.util.List r12) {
        /*
            if (r12 == 0) goto L_0x0008
            boolean r0 = r12.isEmpty()
            if (r0 == 0) goto L_0x000a
        L_0x0008:
            r0 = 0
        L_0x0009:
            return r0
        L_0x000a:
            java.lang.ClassLoader r9 = java.lang.ClassLoader.getSystemClassLoader()
            r7 = 0
            r2 = 0
            r1 = 0
            r0 = 0
            r8 = r0
            r0 = r1
            r1 = r2
        L_0x0015:
            int r2 = r12.size()
            if (r8 >= r2) goto L_0x0124
            java.lang.Object r2 = r12.get(r8)
            if (r2 == 0) goto L_0x0121
            java.lang.String r3 = "android.telephony.CellInfoGsm"
            java.lang.Class r3 = r9.loadClass(r3)     // Catch:{ Exception -> 0x010e }
            java.lang.String r4 = "android.telephony.CellInfoWcdma"
            java.lang.Class r4 = r9.loadClass(r4)     // Catch:{ Exception -> 0x010e }
            java.lang.String r5 = "android.telephony.CellInfoLte"
            java.lang.Class r5 = r9.loadClass(r5)     // Catch:{ Exception -> 0x010e }
            java.lang.String r6 = "android.telephony.CellInfoCdma"
            java.lang.Class r10 = r9.loadClass(r6)     // Catch:{ Exception -> 0x010e }
            boolean r6 = r3.isInstance(r2)     // Catch:{ Exception -> 0x010e }
            if (r6 == 0) goto L_0x005c
            r6 = 1
        L_0x0040:
            if (r6 <= 0) goto L_0x010a
            r0 = 0
            r11 = 1
            if (r6 != r11) goto L_0x0076
            java.lang.Object r0 = r3.cast(r2)     // Catch:{ Exception -> 0x0112 }
        L_0x004a:
            java.lang.String r2 = "getCellIdentity"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0112 }
            java.lang.Object r2 = com.aps.ah.a(r0, r2, r3)     // Catch:{ Exception -> 0x0112 }
            if (r2 != 0) goto L_0x008e
            r0 = r6
            r2 = r7
        L_0x0057:
            int r3 = r8 + 1
            r8 = r3
            r7 = r2
            goto L_0x0015
        L_0x005c:
            boolean r6 = r4.isInstance(r2)     // Catch:{ Exception -> 0x010e }
            if (r6 == 0) goto L_0x0064
            r6 = 2
            goto L_0x0040
        L_0x0064:
            boolean r6 = r5.isInstance(r2)     // Catch:{ Exception -> 0x010e }
            if (r6 == 0) goto L_0x006c
            r6 = 3
            goto L_0x0040
        L_0x006c:
            boolean r0 = r10.isInstance(r2)     // Catch:{ Exception -> 0x010e }
            if (r0 == 0) goto L_0x0074
            r6 = 4
            goto L_0x0040
        L_0x0074:
            r6 = 0
            goto L_0x0040
        L_0x0076:
            r3 = 2
            if (r6 != r3) goto L_0x007e
            java.lang.Object r0 = r4.cast(r2)     // Catch:{ Exception -> 0x0112 }
            goto L_0x004a
        L_0x007e:
            r3 = 3
            if (r6 != r3) goto L_0x0086
            java.lang.Object r0 = r5.cast(r2)     // Catch:{ Exception -> 0x0112 }
            goto L_0x004a
        L_0x0086:
            r3 = 4
            if (r6 != r3) goto L_0x004a
            java.lang.Object r0 = r10.cast(r2)     // Catch:{ Exception -> 0x0112 }
            goto L_0x004a
        L_0x008e:
            r0 = 4
            if (r6 != r0) goto L_0x00cd
            android.telephony.cdma.CdmaCellLocation r0 = new android.telephony.cdma.CdmaCellLocation     // Catch:{ Exception -> 0x0112 }
            r0.<init>()     // Catch:{ Exception -> 0x0112 }
            java.lang.String r1 = "getSystemId"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0117 }
            int r4 = com.aps.ah.b(r2, r1, r3)     // Catch:{ Exception -> 0x0117 }
            java.lang.String r1 = "getNetworkId"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0117 }
            int r5 = com.aps.ah.b(r2, r1, r3)     // Catch:{ Exception -> 0x0117 }
            java.lang.String r1 = "getBasestationId"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0117 }
            int r1 = com.aps.ah.b(r2, r1, r3)     // Catch:{ Exception -> 0x0117 }
            java.lang.String r3 = "getLongitude"
            r10 = 0
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Exception -> 0x0117 }
            int r3 = com.aps.ah.b(r2, r3, r10)     // Catch:{ Exception -> 0x0117 }
            java.lang.String r10 = "getLatitude"
            r11 = 0
            java.lang.Object[] r11 = new java.lang.Object[r11]     // Catch:{ Exception -> 0x0117 }
            int r2 = com.aps.ah.b(r2, r10, r11)     // Catch:{ Exception -> 0x0117 }
            r0.setCellLocationData(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0117 }
            r1 = r7
        L_0x00c7:
            r2 = 4
            if (r6 == r2) goto L_0x0009
            r0 = r1
            goto L_0x0009
        L_0x00cd:
            r0 = 3
            if (r6 != r0) goto L_0x00ed
            java.lang.String r0 = "getTac"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0112 }
            int r0 = com.aps.ah.b(r2, r0, r3)     // Catch:{ Exception -> 0x0112 }
            java.lang.String r3 = "getCi"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0112 }
            int r3 = com.aps.ah.b(r2, r3, r4)     // Catch:{ Exception -> 0x0112 }
            android.telephony.gsm.GsmCellLocation r2 = new android.telephony.gsm.GsmCellLocation     // Catch:{ Exception -> 0x0112 }
            r2.<init>()     // Catch:{ Exception -> 0x0112 }
            r2.setLacAndCid(r0, r3)     // Catch:{ Exception -> 0x011d }
            r0 = r1
            r1 = r2
            goto L_0x00c7
        L_0x00ed:
            java.lang.String r0 = "getLac"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0112 }
            int r0 = com.aps.ah.b(r2, r0, r3)     // Catch:{ Exception -> 0x0112 }
            java.lang.String r3 = "getCid"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0112 }
            int r3 = com.aps.ah.b(r2, r3, r4)     // Catch:{ Exception -> 0x0112 }
            android.telephony.gsm.GsmCellLocation r2 = new android.telephony.gsm.GsmCellLocation     // Catch:{ Exception -> 0x0112 }
            r2.<init>()     // Catch:{ Exception -> 0x0112 }
            r2.setLacAndCid(r0, r3)     // Catch:{ Exception -> 0x011d }
            r0 = r1
            r1 = r2
            goto L_0x00c7
        L_0x010a:
            r0 = r6
            r2 = r7
            goto L_0x0057
        L_0x010e:
            r2 = move-exception
            r2 = r7
            goto L_0x0057
        L_0x0112:
            r0 = move-exception
            r0 = r6
            r2 = r7
            goto L_0x0057
        L_0x0117:
            r1 = move-exception
            r1 = r0
            r2 = r7
            r0 = r6
            goto L_0x0057
        L_0x011d:
            r0 = move-exception
            r0 = r6
            goto L_0x0057
        L_0x0121:
            r2 = r7
            goto L_0x0057
        L_0x0124:
            r6 = r0
            r0 = r1
            r1 = r7
            goto L_0x00c7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.aps.ak.b(java.util.List):android.telephony.CellLocation");
    }

    private void b(BroadcastReceiver broadcastReceiver) {
        if (broadcastReceiver != null && this.f420a != null) {
            try {
                this.f420a.unregisterReceiver(broadcastReceiver);
            } catch (Exception e2) {
            }
        }
    }

    protected static boolean b(Context context) {
        boolean z2;
        boolean z3;
        if (context == null) {
            return true;
        }
        if (!Settings.Secure.getString(context.getContentResolver(), "mock_location").equals("0")) {
            PackageManager packageManager = context.getPackageManager();
            List<ApplicationInfo> installedApplications = packageManager.getInstalledApplications(128);
            String packageName = context.getPackageName();
            z2 = false;
            for (ApplicationInfo next : installedApplications) {
                if (z2) {
                    break;
                }
                try {
                    String[] strArr = packageManager.getPackageInfo(next.packageName, 4096).requestedPermissions;
                    if (strArr != null) {
                        int length = strArr.length;
                        int i2 = 0;
                        while (true) {
                            if (i2 >= length) {
                                break;
                            } else if (!strArr[i2].equals("android.permission.ACCESS_MOCK_LOCATION")) {
                                i2++;
                            } else if (!next.packageName.equals(packageName)) {
                                z3 = true;
                            }
                        }
                    }
                } catch (Exception e2) {
                    z3 = z2;
                }
            }
        } else {
            z2 = false;
        }
        return z2;
        z2 = z3;
    }

    /* access modifiers changed from: private */
    public static String[] b(TelephonyManager telephonyManager) {
        int i2 = 0;
        String str = null;
        if (telephonyManager != null) {
            str = telephonyManager.getNetworkOperator();
        }
        String[] strArr = {"0", "0"};
        if (TextUtils.isDigitsOnly(str) && str.length() > 4) {
            strArr[0] = str.substring(0, 3);
            char[] charArray = str.substring(3).toCharArray();
            while (i2 < charArray.length && Character.isDigit(charArray[i2])) {
                i2++;
            }
            strArr[1] = str.substring(3, i2 + 3);
        }
        return strArr;
    }

    private static boolean c(Context context) {
        try {
            String[] strArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions;
            for (String a2 : bd.f448b) {
                if (!bd.a(strArr, a2)) {
                    return false;
                }
            }
            return true;
        } catch (PackageManager.NameNotFoundException e2) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public final String a(int i2) {
        List<Sensor> sensorList;
        new ArrayList();
        return (this.e == null || (sensorList = this.e.getSensorList(-1)) == null || sensorList.get(i2) == null || sensorList.get(i2).getName() == null || sensorList.get(i2).getName().length() <= 0) ? "null" : sensorList.get(i2).getName();
    }

    /* access modifiers changed from: protected */
    public final List a(float f2) {
        CellLocation cellLocation;
        ArrayList arrayList = new ArrayList();
        long currentTimeMillis = System.currentTimeMillis();
        if (Math.abs(f2) <= 1.0f) {
            f2 = 1.0f;
        }
        if (c() && (cellLocation = (CellLocation) j().get(1)) != null && (cellLocation instanceof GsmCellLocation)) {
            arrayList.add(Integer.valueOf(((GsmCellLocation) cellLocation).getLac()));
            arrayList.add(Integer.valueOf(((GsmCellLocation) cellLocation).getCid()));
            if (((double) (currentTimeMillis - ((Long) j().get(0)).longValue())) <= 50000.0d / ((double) f2)) {
                arrayList.add(1);
            } else {
                arrayList.add(0);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        b();
        if (this.C != null) {
            this.C.quit();
            this.C = null;
        }
        if (this.B != null) {
            this.B.interrupt();
            this.B = null;
        }
        this.B = new al(this, "");
        this.B.start();
    }

    /* access modifiers changed from: protected */
    public final double b(int i2) {
        List<Sensor> sensorList;
        new ArrayList();
        if (this.e == null || (sensorList = this.e.getSensorList(-1)) == null || sensorList.get(i2) == null) {
            return 0.0d;
        }
        return (double) sensorList.get(i2).getMaximumRange();
    }

    /* access modifiers changed from: protected */
    public final List b(float f2) {
        CellLocation cellLocation;
        ArrayList arrayList = new ArrayList();
        long currentTimeMillis = System.currentTimeMillis();
        if (Math.abs(f2) <= 1.0f) {
            f2 = 1.0f;
        }
        if (c() && (cellLocation = (CellLocation) j().get(1)) != null && (cellLocation instanceof CdmaCellLocation)) {
            CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) cellLocation;
            arrayList.add(Integer.valueOf(cdmaCellLocation.getSystemId()));
            arrayList.add(Integer.valueOf(cdmaCellLocation.getNetworkId()));
            arrayList.add(Integer.valueOf(cdmaCellLocation.getBaseStationId()));
            arrayList.add(Integer.valueOf(cdmaCellLocation.getBaseStationLongitude()));
            arrayList.add(Integer.valueOf(cdmaCellLocation.getBaseStationLatitude()));
            if (((double) (currentTimeMillis - ((Long) j().get(0)).longValue())) <= 50000.0d / ((double) f2)) {
                arrayList.add(1);
            } else {
                arrayList.add(0);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.v != null) {
            am amVar = this.v;
            if (this.f421b != null) {
                this.f421b.listen(amVar, 0);
            }
            this.v = null;
        }
        if (this.w != null) {
            an anVar = this.w;
            if (!(this.c == null || anVar == null)) {
                this.c.removeNmeaListener(anVar);
            }
            this.w = null;
        }
        if (this.A != null) {
            this.A.cancel();
            this.A = null;
        }
        if (this.C != null) {
            this.C.quit();
            this.C = null;
        }
        if (this.B != null) {
            this.B.interrupt();
            this.B = null;
        }
    }

    /* access modifiers changed from: protected */
    public final int c(int i2) {
        List<Sensor> sensorList;
        new ArrayList();
        if (this.e == null || (sensorList = this.e.getSensorList(-1)) == null || sensorList.get(i2) == null) {
            return 0;
        }
        return b(sensorList.get(i2));
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        CellLocation cellLocation = null;
        if (this.f421b != null && this.f421b.getSimState() == 5 && this.k) {
            return true;
        }
        if (this.f421b != null) {
            try {
                cellLocation = this.f421b.getCellLocation();
            } catch (Exception e2) {
            }
            if (cellLocation != null) {
                this.t = System.currentTimeMillis();
                this.x = cellLocation;
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final int d(int i2) {
        List<Sensor> sensorList;
        new ArrayList();
        if (this.e == null || (sensorList = this.e.getSensorList(-1)) == null || sensorList.get(i2) == null) {
            return 0;
        }
        return (int) (((double) sensorList.get(i2).getPower()) * 100.0d);
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return this.d != null && (this.d.isWifiEnabled() || a(this.d));
    }

    /* access modifiers changed from: protected */
    public final double e(int i2) {
        List<Sensor> sensorList;
        new ArrayList();
        if (this.e == null || (sensorList = this.e.getSensorList(-1)) == null || sensorList.get(i2) == null) {
            return 0.0d;
        }
        return (double) sensorList.get(i2).getResolution();
    }

    /* access modifiers changed from: protected */
    public final boolean e() {
        try {
            return this.c != null && this.c.isProviderEnabled(LocationManagerProxy.GPS_PROVIDER);
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public final byte f(int i2) {
        new ArrayList();
        if (this.e == null) {
            return Byte.MAX_VALUE;
        }
        List<Sensor> sensorList = this.e.getSensorList(-1);
        if (sensorList == null || sensorList.get(i2) == null || sensorList.get(i2).getType() > 127) {
            return Byte.MAX_VALUE;
        }
        return (byte) sensorList.get(i2).getType();
    }

    /* access modifiers changed from: protected */
    public final String f() {
        if (this.f == null) {
            this.f = Build.MODEL;
        }
        return this.f != null ? this.f : "";
    }

    /* access modifiers changed from: protected */
    public final String g() {
        if (this.g == null && this.f420a != null) {
            this.f421b = (TelephonyManager) this.f420a.getSystemService("phone");
            if (this.f421b != null) {
                try {
                    this.g = this.f421b.getDeviceId();
                } catch (Exception e2) {
                }
            }
        }
        return this.g != null ? this.g : "";
    }

    /* access modifiers changed from: protected */
    public final String g(int i2) {
        List<Sensor> sensorList;
        new ArrayList();
        return (this.e == null || (sensorList = this.e.getSensorList(-1)) == null || sensorList.get(i2) == null || sensorList.get(i2).getVendor() == null || sensorList.get(i2).getVendor().length() <= 0) ? "null" : sensorList.get(i2).getVendor();
    }

    /* access modifiers changed from: protected */
    public final byte h(int i2) {
        new ArrayList();
        if (this.e == null) {
            return Byte.MAX_VALUE;
        }
        List<Sensor> sensorList = this.e.getSensorList(-1);
        if (sensorList == null || sensorList.get(i2) == null || sensorList.get(i2).getType() > 127) {
            return Byte.MAX_VALUE;
        }
        return (byte) sensorList.get(i2).getVersion();
    }

    /* access modifiers changed from: protected */
    public final String h() {
        if (this.h == null && this.f420a != null) {
            this.f421b = (TelephonyManager) this.f420a.getSystemService("phone");
            if (this.f421b != null) {
                this.h = this.f421b.getSubscriberId();
            }
        }
        return this.h != null ? this.h : "";
    }

    /* access modifiers changed from: protected */
    public final boolean i() {
        return this.i;
    }

    /* access modifiers changed from: protected */
    public final List j() {
        CellLocation cellLocation;
        if (Settings.System.getInt(this.f420a.getContentResolver(), "airplane_mode_on", 0) == 1) {
            return new ArrayList();
        }
        if (!c()) {
            return new ArrayList();
        }
        ArrayList arrayList = new ArrayList();
        if (!a(this.x)) {
            cellLocation = B();
            if (a(cellLocation)) {
                this.t = System.currentTimeMillis();
                arrayList.add(Long.valueOf(this.t));
                arrayList.add(cellLocation);
                return arrayList;
            }
        }
        cellLocation = this.x;
        arrayList.add(Long.valueOf(this.t));
        arrayList.add(cellLocation);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final List k() {
        ArrayList arrayList = new ArrayList();
        if (!d()) {
            return new ArrayList();
        }
        ArrayList arrayList2 = new ArrayList();
        synchronized (this) {
            if (System.currentTimeMillis() - this.s < 3500) {
                arrayList2.add(Long.valueOf(this.s));
                for (int i2 = 0; i2 < this.z.size(); i2++) {
                    arrayList.add(this.z.get(i2));
                }
                arrayList2.add(arrayList);
            }
        }
        return arrayList2;
    }

    /* access modifiers changed from: protected */
    public final byte l() {
        if (c()) {
            return (byte) this.j;
        }
        return Byte.MIN_VALUE;
    }

    /* access modifiers changed from: protected */
    public final List m() {
        ArrayList arrayList = new ArrayList();
        if (this.f421b == null) {
            return arrayList;
        }
        if (!c()) {
            return arrayList;
        }
        if (this.f421b.getSimState() == 1) {
            return arrayList;
        }
        int i2 = 0;
        for (NeighboringCellInfo neighboringCellInfo : this.f421b.getNeighboringCellInfo()) {
            if (i2 > 15) {
                break;
            } else if (!(neighboringCellInfo.getLac() == 0 || neighboringCellInfo.getLac() == 65535 || neighboringCellInfo.getCid() == 65535 || neighboringCellInfo.getCid() == 268435455)) {
                arrayList.add(neighboringCellInfo);
                i2++;
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final List n() {
        long j2;
        String str;
        ArrayList arrayList = new ArrayList();
        if (e()) {
            j2 = this.l;
            str = this.m;
        } else {
            j2 = -1;
            str = "";
        }
        if (j2 <= 0) {
            j2 = System.currentTimeMillis() / 1000;
        }
        if (j2 > 2147483647L) {
            j2 /= 1000;
        }
        arrayList.add(Long.valueOf(j2));
        arrayList.add(str);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final long o() {
        long j2 = 0;
        long j3 = this.l;
        if (j3 > 0) {
            j2 = j3;
            int length = String.valueOf(j3).length();
            while (length != 13) {
                j2 = length > 13 ? j2 / 10 : j2 * 10;
                length = String.valueOf(j2).length();
            }
        }
        return j2;
    }

    /* access modifiers changed from: protected */
    public final String p() {
        if (this.n == null && this.f420a != null) {
            this.d = (WifiManager) this.f420a.getSystemService("wifi");
            if (!(this.d == null || this.d.getConnectionInfo() == null)) {
                this.n = this.d.getConnectionInfo().getMacAddress();
                if (this.n != null && this.n.length() > 0) {
                    this.n = this.n.replace(":", "");
                }
            }
        }
        return this.n != null ? this.n : "";
    }

    /* access modifiers changed from: protected */
    public final int q() {
        return this.o;
    }

    /* access modifiers changed from: protected */
    public final int r() {
        return this.p;
    }

    /* access modifiers changed from: protected */
    public final int s() {
        return this.q;
    }

    /* access modifiers changed from: protected */
    public final String t() {
        if (this.r == null && this.f420a != null) {
            this.r = this.f420a.getPackageName();
        }
        return this.r != null ? this.r : "";
    }

    /* access modifiers changed from: protected */
    public final List u() {
        ArrayList arrayList = new ArrayList();
        if (d()) {
            List k2 = k();
            List list = (List) k2.get(1);
            long longValue = ((Long) k2.get(0)).longValue();
            a(list);
            arrayList.add(Long.valueOf(longValue));
            if (list != null && list.size() > 0) {
                for (int i2 = 0; i2 < list.size(); i2++) {
                    ScanResult scanResult = (ScanResult) list.get(i2);
                    if (arrayList.size() - 1 >= 40) {
                        break;
                    }
                    if (scanResult != null) {
                        ArrayList arrayList2 = new ArrayList();
                        arrayList2.add(scanResult.BSSID.replace(":", ""));
                        arrayList2.add(Integer.valueOf(scanResult.level));
                        arrayList2.add(scanResult.SSID);
                        arrayList.add(arrayList2);
                    }
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void v() {
        synchronized (this) {
            this.z.clear();
        }
        if (this.y != null) {
            b((BroadcastReceiver) this.y);
            this.y = null;
        }
        if (this.A != null) {
            this.A.cancel();
            this.A = null;
        }
        this.A = new Timer();
        this.y = new ao(this, (byte) 0);
        a((BroadcastReceiver) this.y);
        A();
    }

    /* access modifiers changed from: protected */
    public final void w() {
        synchronized (this) {
            this.z.clear();
        }
        if (this.y != null) {
            b((BroadcastReceiver) this.y);
            this.y = null;
        }
        if (this.A != null) {
            this.A.cancel();
            this.A = null;
        }
    }

    /* access modifiers changed from: protected */
    public final byte x() {
        List<Sensor> sensorList;
        new ArrayList();
        if (this.e == null || (sensorList = this.e.getSensorList(-1)) == null) {
            return 0;
        }
        return (byte) sensorList.size();
    }

    /* access modifiers changed from: protected */
    public final Context y() {
        return this.f420a;
    }
}
