package com.aps;

import android.location.GpsStatus;
import android.os.Looper;
import android.telephony.PhoneStateListener;
import java.util.Timer;

final class al extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ak f422a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    al(ak akVar, String str) {
        super(str);
        this.f422a = akVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.aps.ak.a(com.aps.ak, android.telephony.PhoneStateListener):void
     arg types: [com.aps.ak, com.aps.am]
     candidates:
      com.aps.ak.a(android.telephony.CellLocation, android.content.Context):int
      com.aps.ak.a(com.aps.ak, int):int
      com.aps.ak.a(com.aps.ak, long):long
      com.aps.ak.a(com.aps.ak, android.os.Looper):android.os.Looper
      com.aps.ak.a(com.aps.ak, android.telephony.CellLocation):android.telephony.CellLocation
      com.aps.ak.a(com.aps.ak, com.aps.am):com.aps.am
      com.aps.ak.a(com.aps.ak, com.aps.an):com.aps.an
      com.aps.ak.a(com.aps.ak, java.lang.String):java.lang.String
      com.aps.ak.a(com.aps.ak, java.util.Timer):java.util.Timer
      com.aps.ak.a(com.aps.ak, android.location.GpsStatus$NmeaListener):void
      com.aps.ak.a(com.aps.ak, boolean):boolean
      com.aps.ak.a(com.aps.ak, android.telephony.PhoneStateListener):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.aps.ak.a(com.aps.ak, android.location.GpsStatus$NmeaListener):void
     arg types: [com.aps.ak, com.aps.an]
     candidates:
      com.aps.ak.a(android.telephony.CellLocation, android.content.Context):int
      com.aps.ak.a(com.aps.ak, int):int
      com.aps.ak.a(com.aps.ak, long):long
      com.aps.ak.a(com.aps.ak, android.os.Looper):android.os.Looper
      com.aps.ak.a(com.aps.ak, android.telephony.CellLocation):android.telephony.CellLocation
      com.aps.ak.a(com.aps.ak, com.aps.am):com.aps.am
      com.aps.ak.a(com.aps.ak, com.aps.an):com.aps.an
      com.aps.ak.a(com.aps.ak, java.lang.String):java.lang.String
      com.aps.ak.a(com.aps.ak, java.util.Timer):java.util.Timer
      com.aps.ak.a(com.aps.ak, android.telephony.PhoneStateListener):void
      com.aps.ak.a(com.aps.ak, boolean):boolean
      com.aps.ak.a(com.aps.ak, android.location.GpsStatus$NmeaListener):void */
    public final void run() {
        try {
            Looper.prepare();
            Looper unused = this.f422a.C = Looper.myLooper();
            Timer unused2 = this.f422a.A = new Timer();
            am unused3 = this.f422a.v = new am(this.f422a, (byte) 0);
            ak.a(this.f422a, (PhoneStateListener) this.f422a.v);
            an unused4 = this.f422a.w = new an(this.f422a, (byte) 0);
            try {
                ak.a(this.f422a, (GpsStatus.NmeaListener) this.f422a.w);
            } catch (Exception e) {
            }
            Looper.loop();
        } catch (Exception e2) {
        }
    }
}
