package com.aps;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

final class ac implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    protected byte f404a = 0;

    /* renamed from: b  reason: collision with root package name */
    protected ArrayList f405b = new ArrayList();
    private byte c = 3;

    ac() {
    }

    /* access modifiers changed from: protected */
    public final Boolean a(DataOutputStream dataOutputStream) {
        try {
            dataOutputStream.writeByte(this.c);
            dataOutputStream.writeByte(this.f404a);
            for (int i = 0; i < this.f405b.size(); i++) {
                ad adVar = (ad) this.f405b.get(i);
                dataOutputStream.writeByte(adVar.f406a);
                byte[] bArr = new byte[adVar.f406a];
                System.arraycopy(adVar.f407b, 0, bArr, 0, adVar.f406a < adVar.f407b.length ? adVar.f406a : adVar.f407b.length);
                dataOutputStream.write(bArr);
                dataOutputStream.writeDouble(adVar.c);
                dataOutputStream.writeInt(adVar.d);
                dataOutputStream.writeInt(adVar.e);
                dataOutputStream.writeDouble(adVar.f);
                dataOutputStream.writeByte(adVar.g);
                dataOutputStream.writeByte(adVar.h);
                byte[] bArr2 = new byte[adVar.h];
                System.arraycopy(adVar.i, 0, bArr2, 0, adVar.h < adVar.i.length ? adVar.h : adVar.i.length);
                dataOutputStream.write(bArr2);
                dataOutputStream.writeByte(adVar.j);
            }
            return true;
        } catch (IOException e) {
            return null;
        }
    }
}
