package com.aps;

import com.qihoo.dynamic.util.Md5Util;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private String f440a = "AES/CBC/PKCS5Padding";

    /* renamed from: b  reason: collision with root package name */
    private Cipher f441b = null;

    b() {
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec("#a@u!t*o(n)a&v^i".getBytes(Md5Util.DEFAULT_CHARSET), "AES");
            IvParameterSpec ivParameterSpec = new IvParameterSpec("_a+m-a=p?a>p<s%3".getBytes(Md5Util.DEFAULT_CHARSET));
            this.f441b = Cipher.getInstance(this.f440a);
            this.f441b.init(2, secretKeySpec, ivParameterSpec);
        } catch (Throwable th) {
            th.printStackTrace();
            t.a(th);
        }
    }

    public static String a(byte[] bArr) {
        if (bArr == null) {
            return "";
        }
        try {
            return com.amap.api.location.core.b.a(bArr);
        } catch (Throwable th) {
            th.printStackTrace();
            return "";
        }
    }

    private byte[] a(String str) {
        byte[] bArr = null;
        if (!(str == null || str.length() == 0 || str.length() % 2 != 0)) {
            try {
                bArr = new byte[(str.length() / 2)];
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < str.length(); i += 2) {
                    sb.delete(0, sb.length());
                    sb.append("0X");
                    sb.append(str.substring(i, i + 2));
                    bArr[i / 2] = (byte) Integer.decode(sb.toString()).intValue();
                }
            } catch (Throwable th) {
                th.printStackTrace();
                t.a(th);
            }
        }
        return bArr;
    }

    /* access modifiers changed from: package-private */
    public String a(String str, String str2) {
        if (str == null || str.length() == 0) {
            return null;
        }
        try {
            return new String(this.f441b.doFinal(a(str)), str2);
        } catch (Exception e) {
            t.a(e);
            return null;
        }
    }
}
