package com.aps;

import android.text.TextUtils;
import com.mediav.ads.sdk.adcore.Config;
import java.util.zip.CRC32;

public class o {
    public byte[] A = null;
    public String B = null;
    public String C = null;
    public String D = null;
    public String E = null;

    /* renamed from: a  reason: collision with root package name */
    public String f481a = Config.CHANNEL_ID;

    /* renamed from: b  reason: collision with root package name */
    public short f482b = 0;
    public String c = null;
    public String d = null;
    public String e = null;
    public String f = null;
    public String g = null;
    public String h = null;
    public String i = null;
    public String j = null;
    public String k = null;
    public String l = null;
    public String m = null;
    public String n = null;
    public String o = null;
    public String p = null;
    public String q = null;
    public String r = null;
    public String s = null;
    public String t = null;
    public String u = null;
    public String v = null;
    public String w = null;
    public String x = null;
    public String y = null;
    public String z = null;

    private String a(String str, int i2) {
        String[] split = this.w.split("\\*")[i2].split(",");
        if (str.equals("lac")) {
            return split[0];
        }
        if (str.equals("cellid")) {
            return split[1];
        }
        if (str.equals("signal")) {
            return split[2];
        }
        return null;
    }

    private byte[] a(String str) {
        String[] split = str.split(":");
        if (split == null || split.length != 6) {
            String[] strArr = new String[6];
            for (int i2 = 0; i2 < strArr.length; i2++) {
                strArr[i2] = "0";
            }
            split = strArr;
        }
        byte[] bArr = new byte[6];
        for (int i3 = 0; i3 < split.length; i3++) {
            if (split[i3].length() > 2) {
                split[i3] = split[i3].substring(0, 2);
            }
            bArr[i3] = (byte) Integer.parseInt(split[i3], 16);
        }
        return bArr;
    }

    private String b(String str) {
        if (!this.v.contains(str + ">")) {
            return "0";
        }
        int indexOf = this.v.indexOf(str + ">");
        return this.v.substring(indexOf + str.length() + 1, this.v.indexOf("</" + str));
    }

    private void b() {
        if (TextUtils.isEmpty(this.f481a)) {
            this.f481a = "";
        }
        if (TextUtils.isEmpty(this.c)) {
            this.c = "";
        }
        if (TextUtils.isEmpty(this.d)) {
            this.d = "";
        }
        if (TextUtils.isEmpty(this.e)) {
            this.e = "";
        }
        if (TextUtils.isEmpty(this.f)) {
            this.f = "";
        }
        if (TextUtils.isEmpty(this.g)) {
            this.g = "";
        }
        if (TextUtils.isEmpty(this.h)) {
            this.h = "";
        }
        if (TextUtils.isEmpty(this.i)) {
            this.i = "";
        }
        if (TextUtils.isEmpty(this.j)) {
            this.j = "0";
        } else if (!this.j.equals(Config.CHANNEL_ID) && !this.j.equals("2")) {
            this.j = "0";
        }
        if (TextUtils.isEmpty(this.D)) {
            this.D = "0";
        } else if (!this.D.equals("0") && !this.D.equals(Config.CHANNEL_ID)) {
            this.D = "0";
        }
        if (TextUtils.isEmpty(this.k)) {
            this.k = "";
        } else {
            this.k = String.valueOf(Double.valueOf(Double.parseDouble(this.k) * 1200000.0d).intValue());
        }
        if (TextUtils.isEmpty(this.l)) {
            this.l = "";
        } else {
            this.l = String.valueOf(Double.valueOf(Double.parseDouble(this.l) * 1000000.0d).intValue());
        }
        if (TextUtils.isEmpty(this.m)) {
            this.m = "";
        }
        if (TextUtils.isEmpty(this.n)) {
            this.n = "";
        }
        if (TextUtils.isEmpty(this.o)) {
            this.o = "";
        }
        if (TextUtils.isEmpty(this.p)) {
            this.p = "";
        }
        if (TextUtils.isEmpty(this.q)) {
            this.q = "";
        }
        if (TextUtils.isEmpty(this.r)) {
            this.r = "";
        }
        if (TextUtils.isEmpty(this.B)) {
            this.B = "";
        }
        if (TextUtils.isEmpty(this.C)) {
            this.C = "";
        }
        if (TextUtils.isEmpty(this.s)) {
            this.s = "";
        }
        if (TextUtils.isEmpty(this.t)) {
            this.t = "0";
        } else if (!this.t.equals(Config.CHANNEL_ID) && !this.t.equals("2")) {
            this.t = "0";
        }
        if (TextUtils.isEmpty(this.u)) {
            this.u = "0";
        } else if (!this.u.equals(Config.CHANNEL_ID) && !this.u.equals("2")) {
            this.u = "0";
        }
        if (TextUtils.isEmpty(this.v)) {
            this.v = "";
        }
        if (TextUtils.isEmpty(this.w)) {
            this.w = "";
        }
        if (TextUtils.isEmpty(this.x)) {
            this.x = "";
        }
        if (TextUtils.isEmpty(this.y)) {
            this.y = "";
        }
        if (TextUtils.isEmpty(this.E)) {
            this.E = "";
        }
        if (TextUtils.isEmpty(this.z)) {
            this.z = "";
        }
        if (this.A == null) {
            this.A = new byte[0];
        }
    }

    public byte[] a() {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int length;
        int i12;
        int i13;
        int i14;
        int i15;
        String[] split;
        int i16;
        int i17;
        int i18;
        b();
        int i19 = 3072;
        if (this.A != null) {
            i19 = 3072 + this.A.length + 1;
        }
        byte[] bArr = new byte[i19];
        bArr[0] = Byte.parseByte(this.f481a);
        byte[] b2 = q.b(this.f482b);
        System.arraycopy(b2, 0, bArr, 1, b2.length);
        int length2 = b2.length + 1;
        try {
            byte[] bytes = this.c.getBytes("GBK");
            bArr[length2] = (byte) bytes.length;
            length2++;
            System.arraycopy(bytes, 0, bArr, length2, bytes.length);
            i2 = length2 + bytes.length;
        } catch (Exception e2) {
            bArr[length2] = 0;
            i2 = length2 + 1;
        }
        try {
            byte[] bytes2 = this.d.getBytes("GBK");
            bArr[i2] = (byte) bytes2.length;
            i2++;
            System.arraycopy(bytes2, 0, bArr, i2, bytes2.length);
            i3 = i2 + bytes2.length;
        } catch (Exception e3) {
            bArr[i2] = 0;
            i3 = i2 + 1;
        }
        try {
            byte[] bytes3 = this.n.getBytes("GBK");
            bArr[i3] = (byte) bytes3.length;
            i3++;
            System.arraycopy(bytes3, 0, bArr, i3, bytes3.length);
            i4 = i3 + bytes3.length;
        } catch (Exception e4) {
            bArr[i3] = 0;
            i4 = i3 + 1;
        }
        try {
            byte[] bytes4 = this.e.getBytes("GBK");
            bArr[i4] = (byte) bytes4.length;
            i4++;
            System.arraycopy(bytes4, 0, bArr, i4, bytes4.length);
            i5 = i4 + bytes4.length;
        } catch (Exception e5) {
            bArr[i4] = 0;
            i5 = i4 + 1;
        }
        try {
            byte[] bytes5 = this.f.getBytes("GBK");
            bArr[i5] = (byte) bytes5.length;
            i5++;
            System.arraycopy(bytes5, 0, bArr, i5, bytes5.length);
            i6 = i5 + bytes5.length;
        } catch (Exception e6) {
            bArr[i5] = 0;
            i6 = i5 + 1;
        }
        try {
            byte[] bytes6 = this.g.getBytes("GBK");
            bArr[i6] = (byte) bytes6.length;
            i6++;
            System.arraycopy(bytes6, 0, bArr, i6, bytes6.length);
            i7 = i6 + bytes6.length;
        } catch (Exception e7) {
            bArr[i6] = 0;
            i7 = i6 + 1;
        }
        try {
            byte[] bytes7 = this.r.getBytes("GBK");
            bArr[i7] = (byte) bytes7.length;
            i7++;
            System.arraycopy(bytes7, 0, bArr, i7, bytes7.length);
            i8 = i7 + bytes7.length;
        } catch (Exception e8) {
            bArr[i7] = 0;
            i8 = i7 + 1;
        }
        try {
            byte[] bytes8 = this.h.getBytes("GBK");
            bArr[i8] = (byte) bytes8.length;
            i8++;
            System.arraycopy(bytes8, 0, bArr, i8, bytes8.length);
            i9 = i8 + bytes8.length;
        } catch (Exception e9) {
            bArr[i8] = 0;
            i9 = i8 + 1;
        }
        try {
            byte[] bytes9 = this.o.getBytes("GBK");
            bArr[i9] = (byte) bytes9.length;
            i9++;
            System.arraycopy(bytes9, 0, bArr, i9, bytes9.length);
            i10 = i9 + bytes9.length;
        } catch (Exception e10) {
            bArr[i9] = 0;
            i10 = i9 + 1;
        }
        try {
            byte[] bytes10 = this.p.getBytes("GBK");
            bArr[i10] = (byte) bytes10.length;
            i10++;
            System.arraycopy(bytes10, 0, bArr, i10, bytes10.length);
            i11 = i10 + bytes10.length;
        } catch (Exception e11) {
            bArr[i10] = 0;
            i11 = i10 + 1;
        }
        if (TextUtils.isEmpty(this.q)) {
            bArr[i11] = 0;
            length = i11 + 1;
        } else {
            byte[] a2 = a(this.q);
            bArr[i11] = (byte) a2.length;
            int i20 = i11 + 1;
            System.arraycopy(a2, 0, bArr, i20, a2.length);
            length = i20 + a2.length;
        }
        try {
            byte[] bytes11 = this.B.getBytes("GBK");
            bArr[length] = (byte) bytes11.length;
            length++;
            System.arraycopy(bytes11, 0, bArr, length, bytes11.length);
            i12 = length + bytes11.length;
        } catch (Exception e12) {
            bArr[length] = 0;
            i12 = length + 1;
        }
        try {
            byte[] bytes12 = this.C.getBytes("GBK");
            bArr[i12] = (byte) bytes12.length;
            i12++;
            System.arraycopy(bytes12, 0, bArr, i12, bytes12.length);
            i13 = i12 + bytes12.length;
        } catch (Exception e13) {
            bArr[i12] = 0;
            i13 = i12 + 1;
        }
        try {
            byte[] bytes13 = this.s.getBytes("GBK");
            bArr[i13] = (byte) bytes13.length;
            i13++;
            System.arraycopy(bytes13, 0, bArr, i13, bytes13.length);
            i14 = i13 + bytes13.length;
        } catch (Exception e14) {
            bArr[i13] = 0;
            i14 = i13 + 1;
        }
        bArr[i14] = Byte.parseByte(this.t);
        int i21 = i14 + 1;
        bArr[i21] = Byte.parseByte(this.j);
        int i22 = i21 + 1;
        if (this.j.equals(Config.CHANNEL_ID)) {
            bArr[i22] = Byte.parseByte(this.D);
            i22++;
        }
        if (this.j.equals(Config.CHANNEL_ID) || this.j.equals("2")) {
            byte[] a3 = q.a(Integer.parseInt(this.k));
            System.arraycopy(a3, 0, bArr, i22, a3.length);
            i22 += a3.length;
        }
        if (this.j.equals(Config.CHANNEL_ID) || this.j.equals("2")) {
            byte[] a4 = q.a(Integer.parseInt(this.l));
            System.arraycopy(a4, 0, bArr, i22, a4.length);
            i22 += a4.length;
        }
        if (this.j.equals(Config.CHANNEL_ID) || this.j.equals("2")) {
            byte[] b3 = q.b(this.m);
            System.arraycopy(b3, 0, bArr, i22, b3.length);
            i22 += b3.length;
        }
        bArr[i22] = Byte.parseByte(this.u);
        int i23 = i22 + 1;
        if (this.u.equals(Config.CHANNEL_ID)) {
            byte[] b4 = q.b(b("mcc"));
            System.arraycopy(b4, 0, bArr, i23, b4.length);
            int length3 = i23 + b4.length;
            byte[] b5 = q.b(b("mnc"));
            System.arraycopy(b5, 0, bArr, length3, b5.length);
            int length4 = length3 + b5.length;
            byte[] b6 = q.b(b("lac"));
            System.arraycopy(b6, 0, bArr, length4, b6.length);
            int length5 = length4 + b6.length;
            byte[] a5 = q.a(b("cellid"));
            System.arraycopy(a5, 0, bArr, length5, a5.length);
            int length6 = a5.length + length5;
            int parseInt = Integer.parseInt(b("signal"));
            if (parseInt > 127) {
                parseInt = 0;
            } else if (parseInt < -128) {
                parseInt = 0;
            }
            bArr[length6] = (byte) parseInt;
            int i24 = length6 + 1;
            if (this.w.length() == 0) {
                bArr[i24] = 0;
                i23 = i24 + 1;
            } else {
                int length7 = this.w.split("\\*").length;
                bArr[i24] = (byte) length7;
                i23 = i24 + 1;
                int i25 = 0;
                while (i25 < length7) {
                    byte[] b7 = q.b(a("lac", i25));
                    System.arraycopy(b7, 0, bArr, i23, b7.length);
                    int length8 = i23 + b7.length;
                    byte[] a6 = q.a(a("cellid", i25));
                    System.arraycopy(a6, 0, bArr, length8, a6.length);
                    int length9 = a6.length + length8;
                    int parseInt2 = Integer.parseInt(a("signal", i25));
                    if (parseInt2 > 127) {
                        parseInt2 = 0;
                    } else if (parseInt2 < -128) {
                        parseInt2 = 0;
                    }
                    bArr[length9] = (byte) parseInt2;
                    i25++;
                    i23 = length9 + 1;
                }
            }
        } else if (this.u.equals("2")) {
            byte[] b8 = q.b(b("mcc"));
            System.arraycopy(b8, 0, bArr, i23, b8.length);
            int length10 = i23 + b8.length;
            byte[] b9 = q.b(b("sid"));
            System.arraycopy(b9, 0, bArr, length10, b9.length);
            int length11 = length10 + b9.length;
            byte[] b10 = q.b(b("nid"));
            System.arraycopy(b10, 0, bArr, length11, b10.length);
            int length12 = length11 + b10.length;
            byte[] b11 = q.b(b("bid"));
            System.arraycopy(b11, 0, bArr, length12, b11.length);
            int length13 = length12 + b11.length;
            byte[] a7 = q.a(b("lon"));
            System.arraycopy(a7, 0, bArr, length13, a7.length);
            int length14 = length13 + a7.length;
            byte[] a8 = q.a(b("lat"));
            System.arraycopy(a8, 0, bArr, length14, a8.length);
            int length15 = a8.length + length14;
            int parseInt3 = Integer.parseInt(b("signal"));
            if (parseInt3 > 127) {
                parseInt3 = 0;
            } else if (parseInt3 < -128) {
                parseInt3 = 0;
            }
            bArr[length15] = (byte) parseInt3;
            int i26 = length15 + 1;
            bArr[i26] = 0;
            i23 = i26 + 1;
        }
        if (this.x.length() == 0) {
            bArr[i23] = 0;
            i15 = i23 + 1;
        } else {
            bArr[i23] = 1;
            int i27 = i23 + 1;
            try {
                split = this.x.split(",");
                byte[] a9 = a(split[0]);
                System.arraycopy(a9, 0, bArr, i27, a9.length);
                i27 += a9.length;
                byte[] bytes14 = split[2].getBytes("GBK");
                bArr[i27] = (byte) bytes14.length;
                i27++;
                System.arraycopy(bytes14, 0, bArr, i27, bytes14.length);
                i27 += bytes14.length;
            } catch (Exception e15) {
                bArr[i27] = 0;
                i27++;
            } catch (Throwable th) {
                byte[] a10 = a("00:00:00:00:00:00");
                System.arraycopy(a10, 0, bArr, i27, a10.length);
                int length16 = i27 + a10.length;
                bArr[length16] = 0;
                int i28 = length16 + 1;
                bArr[i28] = Byte.parseByte("0");
                i15 = i28 + 1;
            }
            int parseInt4 = Integer.parseInt(split[1]);
            if (parseInt4 > 127) {
                parseInt4 = 0;
            } else if (parseInt4 < -128) {
                parseInt4 = 0;
            }
            bArr[i27] = Byte.parseByte(String.valueOf(parseInt4));
            i15 = i27 + 1;
        }
        String[] split2 = this.y.split("\\*");
        if (TextUtils.isEmpty(this.y) || split2.length == 0) {
            bArr[i15] = 0;
            i16 = i15 + 1;
        } else {
            bArr[i15] = (byte) split2.length;
            i16 = i15 + 1;
            int i29 = 0;
            while (i29 < split2.length) {
                String[] split3 = split2[i29].split(",");
                byte[] a11 = a(split3[0]);
                System.arraycopy(a11, 0, bArr, i16, a11.length);
                int length17 = i16 + a11.length;
                try {
                    byte[] bytes15 = split3[2].getBytes("GBK");
                    bArr[length17] = (byte) bytes15.length;
                    length17++;
                    System.arraycopy(bytes15, 0, bArr, length17, bytes15.length);
                    i18 = length17 + bytes15.length;
                } catch (Exception e16) {
                    bArr[length17] = 0;
                    i18 = length17 + 1;
                }
                int parseInt5 = Integer.parseInt(split3[1]);
                if (parseInt5 > 127) {
                    parseInt5 = 0;
                } else if (parseInt5 < -128) {
                    parseInt5 = 0;
                }
                bArr[i18] = Byte.parseByte(String.valueOf(parseInt5));
                i29++;
                i16 = i18 + 1;
            }
            if (this.E != null && this.E.length() > 0) {
                byte[] b12 = q.b(Integer.parseInt(this.E));
                System.arraycopy(b12, 0, bArr, i16, b12.length);
                i16 += b12.length;
            }
        }
        try {
            byte[] bytes16 = this.z.getBytes("GBK");
            if (bytes16.length > 127) {
                bytes16 = null;
            }
            if (bytes16 == null) {
                bArr[i16] = 0;
                i17 = i16 + 1;
            } else {
                bArr[i16] = (byte) bytes16.length;
                i16++;
                System.arraycopy(bytes16, 0, bArr, i16, bytes16.length);
                i17 = i16 + bytes16.length;
            }
        } catch (Exception e17) {
            bArr[i16] = 0;
            i17 = i16 + 1;
        }
        int length18 = this.A != null ? this.A.length : 0;
        byte[] b13 = q.b(length18);
        System.arraycopy(b13, 0, bArr, i17, b13.length);
        int length19 = i17 + b13.length;
        if (length18 > 0) {
            System.arraycopy(this.A, 0, bArr, length19, this.A.length);
            length19 += this.A.length;
        }
        byte[] bArr2 = new byte[length19];
        System.arraycopy(bArr, 0, bArr2, 0, length19);
        CRC32 crc32 = new CRC32();
        crc32.update(bArr2);
        byte[] a12 = q.a(crc32.getValue());
        byte[] bArr3 = new byte[(a12.length + length19)];
        System.arraycopy(bArr2, 0, bArr3, 0, length19);
        System.arraycopy(a12, 0, bArr3, length19, a12.length);
        int length20 = length19 + a12.length;
        return bArr3;
    }
}
