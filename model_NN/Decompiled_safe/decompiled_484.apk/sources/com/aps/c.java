package com.aps;

import com.amap.api.location.core.AMapLocException;
import org.json.JSONException;
import org.json.JSONObject;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private String f454a = "";

    /* renamed from: b  reason: collision with root package name */
    private double f455b = 0.0d;
    private double c = 0.0d;
    private float d = 0.0f;
    private float e = 0.0f;
    private float f = 0.0f;
    private long g = 0;
    private AMapLocException h = new AMapLocException();
    private String i = "new";
    private String j = "";
    private String k = "";
    private String l = "";
    private String m = "";
    private String n = "";
    private String o = "";
    private String p = "";
    private String q = "";
    private String r = "";
    private String s = "";
    private String t = "";
    private String u = "";
    private String v = "";
    private String w = "";
    private String x = "";
    private JSONObject y = null;

    public c() {
    }

    public c(JSONObject jSONObject) {
        if (jSONObject != null) {
            try {
                this.f454a = jSONObject.getString("provider");
                this.f455b = jSONObject.getDouble("lon");
                this.c = jSONObject.getDouble("lat");
                this.d = (float) jSONObject.getLong("accuracy");
                this.e = (float) jSONObject.getLong("speed");
                this.f = (float) jSONObject.getLong("bearing");
                this.g = jSONObject.getLong("time");
                this.i = jSONObject.getString("type");
                this.j = jSONObject.getString("retype");
                this.k = jSONObject.getString("citycode");
                this.l = jSONObject.getString("desc");
                this.m = jSONObject.getString("adcode");
                this.n = jSONObject.getString("country");
                this.o = jSONObject.getString("province");
                this.p = jSONObject.getString("city");
                this.q = jSONObject.getString("road");
                this.r = jSONObject.getString("street");
                this.s = jSONObject.getString("poiname");
                this.u = jSONObject.getString("floor");
                this.t = jSONObject.getString("poiid");
                this.v = jSONObject.getString("coord");
                this.w = jSONObject.getString("mcell");
                this.x = jSONObject.getString("district");
            } catch (Throwable th) {
                th.printStackTrace();
                t.a(th);
            }
        }
    }

    public AMapLocException a() {
        return this.h;
    }

    public void a(double d2) {
        this.f455b = d2;
    }

    public void a(float f2) {
        this.d = f2;
    }

    public void a(long j2) {
        this.g = j2;
    }

    public void a(AMapLocException aMapLocException) {
        this.h = aMapLocException;
    }

    public void a(String str) {
        this.t = str;
    }

    public void a(JSONObject jSONObject) {
        this.y = jSONObject;
    }

    public String b() {
        return this.t;
    }

    public void b(double d2) {
        this.c = d2;
    }

    public void b(String str) {
        this.u = str;
    }

    public String c() {
        return this.u;
    }

    public void c(String str) {
        this.x = str;
    }

    public String d() {
        return this.x;
    }

    public void d(String str) {
        this.v = str;
    }

    public double e() {
        return this.f455b;
    }

    public void e(String str) {
        this.w = str;
    }

    public double f() {
        return this.c;
    }

    public void f(String str) {
        this.f454a = str;
    }

    public float g() {
        return this.d;
    }

    public void g(String str) {
        this.i = str;
    }

    public long h() {
        return this.g;
    }

    public void h(String str) {
        this.j = str;
    }

    public String i() {
        return this.i;
    }

    public void i(String str) {
        this.k = str;
    }

    public String j() {
        return this.j;
    }

    public void j(String str) {
        this.l = str;
    }

    public String k() {
        return this.k;
    }

    public void k(String str) {
        this.m = str;
    }

    public String l() {
        return this.l;
    }

    public void l(String str) {
        this.n = str;
    }

    public String m() {
        return this.m;
    }

    public void m(String str) {
        this.o = str;
    }

    public String n() {
        return this.n;
    }

    public void n(String str) {
        this.p = str;
    }

    public String o() {
        return this.o;
    }

    public void o(String str) {
        this.q = str;
    }

    public String p() {
        return this.p;
    }

    public void p(String str) {
        this.r = str;
    }

    public String q() {
        return this.q;
    }

    public void q(String str) {
        this.s = str;
    }

    public String r() {
        return this.r;
    }

    public String s() {
        return this.s;
    }

    public JSONObject t() {
        return this.y;
    }

    public String u() {
        JSONObject jSONObject;
        try {
            jSONObject = new JSONObject();
            jSONObject.put("provider", this.f454a);
            jSONObject.put("lon", this.f455b);
            jSONObject.put("lat", this.c);
            jSONObject.put("accuracy", (double) this.d);
            jSONObject.put("speed", (double) this.e);
            jSONObject.put("bearing", (double) this.f);
            jSONObject.put("time", this.g);
            jSONObject.put("type", this.i);
            jSONObject.put("retype", this.j);
            jSONObject.put("citycode", this.k);
            jSONObject.put("desc", this.l);
            jSONObject.put("adcode", this.m);
            jSONObject.put("country", this.n);
            jSONObject.put("province", this.o);
            jSONObject.put("city", this.p);
            jSONObject.put("road", this.q);
            jSONObject.put("street", this.r);
            jSONObject.put("poiname", this.s);
            jSONObject.put("poiid", this.t);
            jSONObject.put("floor", this.u);
            jSONObject.put("coord", this.v);
            jSONObject.put("mcell", this.w);
            jSONObject.put("district", this.x);
        } catch (JSONException e2) {
            t.a(e2);
            jSONObject = null;
        }
        if (jSONObject == null) {
            return null;
        }
        return jSONObject.toString();
    }
}
