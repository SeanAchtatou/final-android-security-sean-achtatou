package com.aps;

import java.util.List;

public final class w {

    /* renamed from: a  reason: collision with root package name */
    private boolean f496a = false;

    /* renamed from: b  reason: collision with root package name */
    private String f497b = "";
    private boolean c = false;
    private double d = 0.0d;
    private double e = 0.0d;

    protected w(List list, String str, String str2, String str3) {
        this.f497b = str3;
        d();
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void d() {
        /*
            r7 = this;
            r6 = 2
            r1 = 1
            r3 = 0
            java.lang.String r4 = r7.f497b
            if (r4 == 0) goto L_0x00ca
            int r0 = r4.length()
            r2 = 8
            if (r0 <= r2) goto L_0x00ca
            r0 = r1
            r2 = r3
        L_0x0011:
            int r5 = r4.length()
            int r5 = r5 + -3
            if (r0 >= r5) goto L_0x0021
            char r5 = r4.charAt(r0)
            r2 = r2 ^ r5
            int r0 = r0 + 1
            goto L_0x0011
        L_0x0021:
            java.lang.String r0 = java.lang.Integer.toHexString(r2)
            int r2 = r4.length()
            int r2 = r2 + -2
            int r5 = r4.length()
            java.lang.String r2 = r4.substring(r2, r5)
            boolean r0 = r0.equalsIgnoreCase(r2)
            if (r0 == 0) goto L_0x00ca
            r0 = r1
        L_0x003a:
            if (r0 == 0) goto L_0x00c5
            java.lang.String r0 = r7.f497b
            java.lang.String r2 = r7.f497b
            int r2 = r2.length()
            int r2 = r2 + -3
            java.lang.String r2 = r0.substring(r3, r2)
            r0 = r3
        L_0x004b:
            int r4 = r2.length()
            if (r3 >= r4) goto L_0x005e
            char r4 = r2.charAt(r3)
            r5 = 44
            if (r4 != r5) goto L_0x005b
            int r0 = r0 + 1
        L_0x005b:
            int r3 = r3 + 1
            goto L_0x004b
        L_0x005e:
            java.lang.String r3 = ","
            int r0 = r0 + 1
            java.lang.String[] r0 = r2.split(r3, r0)
            int r2 = r0.length
            r3 = 6
            if (r2 >= r3) goto L_0x006b
        L_0x006a:
            return
        L_0x006b:
            r2 = r0[r6]
            java.lang.String r3 = ""
            boolean r2 = r2.equals(r3)
            if (r2 != 0) goto L_0x00c5
            int r2 = r0.length
            int r2 = r2 + -3
            r2 = r0[r2]
            java.lang.String r3 = ""
            boolean r2 = r2.equals(r3)
            if (r2 != 0) goto L_0x00c5
            int r2 = r0.length
            int r2 = r2 + -2
            r2 = r0[r2]
            java.lang.String r3 = ""
            boolean r2 = r2.equals(r3)
            if (r2 != 0) goto L_0x00c5
            int r2 = r0.length
            int r2 = r2 + -1
            r2 = r0[r2]
            java.lang.String r3 = ""
            boolean r2 = r2.equals(r3)
            if (r2 != 0) goto L_0x00c5
            r2 = r0[r6]
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r2.intValue()
            int r2 = r0.length
            int r2 = r2 + -3
            r2 = r0[r2]
            java.lang.Double r2 = java.lang.Double.valueOf(r2)
            double r2 = r2.doubleValue()
            r7.d = r2
            int r2 = r0.length
            int r2 = r2 + -2
            r0 = r0[r2]
            java.lang.Double r0 = java.lang.Double.valueOf(r0)
            double r2 = r0.doubleValue()
            r7.e = r2
            r7.c = r1
        L_0x00c5:
            boolean r0 = r7.c
            r7.f496a = r0
            goto L_0x006a
        L_0x00ca:
            r0 = r3
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.aps.w.d():void");
    }

    /* access modifiers changed from: protected */
    public final boolean a() {
        return this.f496a;
    }

    /* access modifiers changed from: protected */
    public final double b() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public final double c() {
        return this.e;
    }
}
