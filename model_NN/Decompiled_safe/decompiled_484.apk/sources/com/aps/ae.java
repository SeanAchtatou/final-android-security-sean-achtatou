package com.aps;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

final class ae implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    protected byte f408a = 0;

    /* renamed from: b  reason: collision with root package name */
    protected ArrayList f409b = new ArrayList();
    private byte c = 8;

    ae() {
    }

    /* access modifiers changed from: protected */
    public final Boolean a(DataOutputStream dataOutputStream) {
        try {
            dataOutputStream.writeByte(this.c);
            dataOutputStream.writeByte(this.f408a);
            for (byte b2 = 0; b2 < this.f408a; b2++) {
                af afVar = (af) this.f409b.get(b2);
                dataOutputStream.write(afVar.f410a);
                dataOutputStream.writeShort(afVar.f411b);
                dataOutputStream.write(ah.a(afVar.c, afVar.c.length));
            }
            return true;
        } catch (IOException e) {
            return null;
        }
    }
}
