package com.aps;

import com.mediav.ads.sdk.adcore.Config;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public final class g implements Closeable {

    /* renamed from: a  reason: collision with root package name */
    static final Pattern f464a = Pattern.compile("[a-z0-9_-]{1,120}");
    /* access modifiers changed from: private */
    public static final OutputStream p = new i();

    /* renamed from: b  reason: collision with root package name */
    final ThreadPoolExecutor f465b = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue());
    /* access modifiers changed from: private */
    public final File c;
    private final File d;
    private final File e;
    private final File f;
    private final int g;
    private long h;
    /* access modifiers changed from: private */
    public final int i;
    private long j = 0;
    /* access modifiers changed from: private */
    public Writer k;
    private final LinkedHashMap<String, b> l = new LinkedHashMap<>(0, 0.75f, true);
    /* access modifiers changed from: private */
    public int m;
    private long n = 0;
    private final Callable<Void> o = new h(this);

    public final class a {
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final b f467b;
        /* access modifiers changed from: private */
        public final boolean[] c;
        /* access modifiers changed from: private */
        public boolean d;
        private boolean e;

        /* renamed from: com.aps.g$a$a  reason: collision with other inner class name */
        class C0002a extends FilterOutputStream {
            private C0002a(OutputStream outputStream) {
                super(outputStream);
            }

            /* synthetic */ C0002a(a aVar, OutputStream outputStream, h hVar) {
                this(outputStream);
            }

            public void close() {
                try {
                    this.out.close();
                } catch (IOException e) {
                    boolean unused = a.this.d = true;
                }
            }

            public void flush() {
                try {
                    this.out.flush();
                } catch (IOException e) {
                    boolean unused = a.this.d = true;
                }
            }

            public void write(int i) {
                try {
                    this.out.write(i);
                } catch (IOException e) {
                    boolean unused = a.this.d = true;
                }
            }

            public void write(byte[] bArr, int i, int i2) {
                try {
                    this.out.write(bArr, i, i2);
                } catch (IOException e) {
                    boolean unused = a.this.d = true;
                }
            }
        }

        private a(b bVar) {
            this.f467b = bVar;
            this.c = bVar.d ? null : new boolean[g.this.i];
        }

        /* synthetic */ a(g gVar, b bVar, h hVar) {
            this(bVar);
        }

        public OutputStream a(int i) {
            OutputStream b2;
            FileOutputStream fileOutputStream;
            if (i < 0 || i >= g.this.i) {
                throw new IllegalArgumentException("Expected index " + i + " to " + "be greater than 0 and less than the maximum value count " + "of " + g.this.i);
            }
            synchronized (g.this) {
                if (this.f467b.e != this) {
                    throw new IllegalStateException();
                }
                if (!this.f467b.d) {
                    this.c[i] = true;
                }
                File b3 = this.f467b.b(i);
                try {
                    fileOutputStream = new FileOutputStream(b3);
                } catch (FileNotFoundException e2) {
                    g.this.c.mkdirs();
                    try {
                        fileOutputStream = new FileOutputStream(b3);
                    } catch (FileNotFoundException e3) {
                        b2 = g.p;
                    }
                }
                b2 = new C0002a(this, fileOutputStream, null);
            }
            return b2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.aps.g.a(com.aps.g, com.aps.g$a, boolean):void
         arg types: [com.aps.g, com.aps.g$a, int]
         candidates:
          com.aps.g.a(java.io.File, java.io.File, boolean):void
          com.aps.g.a(com.aps.g, com.aps.g$a, boolean):void */
        public void a() {
            if (this.d) {
                g.this.a(this, false);
                g.this.c(this.f467b.f470b);
            } else {
                g.this.a(this, true);
            }
            this.e = true;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.aps.g.a(com.aps.g, com.aps.g$a, boolean):void
         arg types: [com.aps.g, com.aps.g$a, int]
         candidates:
          com.aps.g.a(java.io.File, java.io.File, boolean):void
          com.aps.g.a(com.aps.g, com.aps.g$a, boolean):void */
        public void b() {
            g.this.a(this, false);
        }
    }

    final class b {
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final String f470b;
        /* access modifiers changed from: private */
        public final long[] c;
        /* access modifiers changed from: private */
        public boolean d;
        /* access modifiers changed from: private */
        public a e;
        /* access modifiers changed from: private */
        public long f;

        private b(String str) {
            this.f470b = str;
            this.c = new long[g.this.i];
        }

        /* synthetic */ b(g gVar, String str, h hVar) {
            this(str);
        }

        /* access modifiers changed from: private */
        public void a(String[] strArr) {
            if (strArr.length != g.this.i) {
                throw b(strArr);
            }
            int i = 0;
            while (i < strArr.length) {
                try {
                    this.c[i] = Long.parseLong(strArr[i]);
                    i++;
                } catch (NumberFormatException e2) {
                    throw b(strArr);
                }
            }
        }

        private IOException b(String[] strArr) {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        public File a(int i) {
            return new File(g.this.c, this.f470b + "." + i);
        }

        public String a() {
            StringBuilder sb = new StringBuilder();
            for (long append : this.c) {
                sb.append(' ').append(append);
            }
            return sb.toString();
        }

        public File b(int i) {
            return new File(g.this.c, this.f470b + "." + i + ".tmp");
        }
    }

    public final class c implements Closeable {

        /* renamed from: b  reason: collision with root package name */
        private final String f472b;
        private final long c;
        private final InputStream[] d;
        private final long[] e;

        private c(String str, long j, InputStream[] inputStreamArr, long[] jArr) {
            this.f472b = str;
            this.c = j;
            this.d = inputStreamArr;
            this.e = jArr;
        }

        /* synthetic */ c(g gVar, String str, long j, InputStream[] inputStreamArr, long[] jArr, h hVar) {
            this(str, j, inputStreamArr, jArr);
        }

        public InputStream a(int i) {
            return this.d[i];
        }

        public void close() {
            for (InputStream a2 : this.d) {
                s.a(a2);
            }
        }
    }

    private g(File file, int i2, int i3, long j2) {
        this.c = file;
        this.g = i2;
        this.d = new File(file, "journal");
        this.e = new File(file, "journal.tmp");
        this.f = new File(file, "journal.bkp");
        this.i = i3;
        this.h = j2;
    }

    private synchronized a a(String str, long j2) {
        b bVar;
        a aVar;
        if (this.k == null) {
            aVar = null;
        } else {
            g();
            e(str);
            b bVar2 = this.l.get(str);
            if (j2 == -1 || (bVar2 != null && bVar2.f == j2)) {
                if (bVar2 == null) {
                    b bVar3 = new b(this, str, null);
                    this.l.put(str, bVar3);
                    bVar = bVar3;
                } else if (bVar2.e != null) {
                    aVar = null;
                } else {
                    bVar = bVar2;
                }
                aVar = new a(this, bVar, null);
                a unused = bVar.e = aVar;
                this.k.write("DIRTY " + str + 10);
                this.k.flush();
            } else {
                aVar = null;
            }
        }
        return aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.aps.g.a(java.io.File, java.io.File, boolean):void
     arg types: [java.io.File, java.io.File, int]
     candidates:
      com.aps.g.a(com.aps.g, com.aps.g$a, boolean):void
      com.aps.g.a(java.io.File, java.io.File, boolean):void */
    public static g a(File file, int i2, int i3, long j2) {
        if (j2 <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i3 <= 0) {
            throw new IllegalArgumentException("valueCount <= 0");
        } else {
            File file2 = new File(file, "journal.bkp");
            if (file2.exists()) {
                File file3 = new File(file, "journal");
                if (file3.exists()) {
                    file2.delete();
                } else {
                    a(file2, file3, false);
                }
            }
            g gVar = new g(file, i2, i3, j2);
            if (gVar.d.exists()) {
                try {
                    gVar.c();
                    gVar.d();
                    gVar.k = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(gVar.d, true), s.f491a));
                    return gVar;
                } catch (IOException e2) {
                    System.out.println("DiskLruCache " + file + " is corrupt: " + e2.getMessage() + ", removing");
                    gVar.a();
                }
            }
            file.mkdirs();
            g gVar2 = new g(file, i2, i3, j2);
            gVar2.e();
            return gVar2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.aps.g.b.a(com.aps.g$b, boolean):boolean
     arg types: [com.aps.g$b, int]
     candidates:
      com.aps.g.b.a(com.aps.g$b, long):long
      com.aps.g.b.a(com.aps.g$b, com.aps.g$a):com.aps.g$a
      com.aps.g.b.a(com.aps.g$b, java.lang.String[]):void
      com.aps.g.b.a(com.aps.g$b, boolean):boolean */
    /* access modifiers changed from: private */
    public synchronized void a(a aVar, boolean z) {
        synchronized (this) {
            b a2 = aVar.f467b;
            if (a2.e != aVar) {
                throw new IllegalStateException();
            }
            if (z) {
                if (!a2.d) {
                    int i2 = 0;
                    while (true) {
                        if (i2 < this.i) {
                            if (!aVar.c[i2]) {
                                aVar.b();
                                throw new IllegalStateException("Newly created entry didn't create value for index " + i2);
                            } else if (!a2.b(i2).exists()) {
                                aVar.b();
                                break;
                            } else {
                                i2++;
                            }
                        }
                    }
                }
            }
            for (int i3 = 0; i3 < this.i; i3++) {
                File b2 = a2.b(i3);
                if (!z) {
                    a(b2);
                } else if (b2.exists()) {
                    File a3 = a2.a(i3);
                    b2.renameTo(a3);
                    long j2 = a2.c[i3];
                    long length = a3.length();
                    a2.c[i3] = length;
                    this.j = (this.j - j2) + length;
                }
            }
            this.m++;
            a unused = a2.e = (a) null;
            if (a2.d || z) {
                boolean unused2 = a2.d = true;
                this.k.write("CLEAN " + a2.f470b + a2.a() + 10);
                if (z) {
                    long j3 = this.n;
                    this.n = 1 + j3;
                    long unused3 = a2.f = j3;
                }
            } else {
                this.l.remove(a2.f470b);
                this.k.write("REMOVE " + a2.f470b + 10);
            }
            this.k.flush();
            if (this.j > this.h || f()) {
                this.f465b.submit(this.o);
            }
        }
    }

    private static void a(File file) {
        if (file.exists() && !file.delete()) {
            throw new IOException();
        }
    }

    private static void a(File file, File file2, boolean z) {
        if (z) {
            a(file2);
        }
        if (!file.renameTo(file2)) {
            throw new IOException();
        }
    }

    private void c() {
        int i2;
        r rVar = new r(new FileInputStream(this.d), s.f491a);
        try {
            String a2 = rVar.a();
            String a3 = rVar.a();
            String a4 = rVar.a();
            String a5 = rVar.a();
            String a6 = rVar.a();
            if (!"libcore.io.DiskLruCache".equals(a2) || !Config.CHANNEL_ID.equals(a3) || !Integer.toString(this.g).equals(a4) || !Integer.toString(this.i).equals(a5) || !"".equals(a6)) {
                throw new IOException("unexpected journal header: [" + a2 + ", " + a3 + ", " + a5 + ", " + a6 + "]");
            }
            i2 = 0;
            while (true) {
                d(rVar.a());
                i2++;
            }
        } catch (EOFException e2) {
            this.m = i2 - this.l.size();
            s.a(rVar);
        } catch (Throwable th) {
            s.a(rVar);
            throw th;
        }
    }

    private void d() {
        a(this.e);
        Iterator<b> it = this.l.values().iterator();
        while (it.hasNext()) {
            b next = it.next();
            if (next.e == null) {
                for (int i2 = 0; i2 < this.i; i2++) {
                    this.j += next.c[i2];
                }
            } else {
                a unused = next.e = (a) null;
                for (int i3 = 0; i3 < this.i; i3++) {
                    a(next.a(i3));
                    a(next.b(i3));
                }
                it.remove();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.aps.g.b.a(com.aps.g$b, boolean):boolean
     arg types: [com.aps.g$b, int]
     candidates:
      com.aps.g.b.a(com.aps.g$b, long):long
      com.aps.g.b.a(com.aps.g$b, com.aps.g$a):com.aps.g$a
      com.aps.g.b.a(com.aps.g$b, java.lang.String[]):void
      com.aps.g.b.a(com.aps.g$b, boolean):boolean */
    private void d(String str) {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf == -1) {
            throw new IOException("unexpected journal line: " + str);
        }
        int i2 = indexOf + 1;
        int indexOf2 = str.indexOf(32, i2);
        if (indexOf2 == -1) {
            String substring = str.substring(i2);
            if (indexOf != "REMOVE".length() || !str.startsWith("REMOVE")) {
                str2 = substring;
            } else {
                this.l.remove(substring);
                return;
            }
        } else {
            str2 = str.substring(i2, indexOf2);
        }
        b bVar = this.l.get(str2);
        if (bVar == null) {
            bVar = new b(this, str2, null);
            this.l.put(str2, bVar);
        }
        if (indexOf2 != -1 && indexOf == "CLEAN".length() && str.startsWith("CLEAN")) {
            String[] split = str.substring(indexOf2 + 1).split(" ");
            boolean unused = bVar.d = true;
            a unused2 = bVar.e = (a) null;
            bVar.a(split);
        } else if (indexOf2 == -1 && indexOf == "DIRTY".length() && str.startsWith("DIRTY")) {
            a unused3 = bVar.e = new a(this, bVar, null);
        } else if (indexOf2 != -1 || indexOf != "READ".length() || !str.startsWith("READ")) {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.aps.g.a(java.io.File, java.io.File, boolean):void
     arg types: [java.io.File, java.io.File, int]
     candidates:
      com.aps.g.a(com.aps.g, com.aps.g$a, boolean):void
      com.aps.g.a(java.io.File, java.io.File, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* access modifiers changed from: private */
    public synchronized void e() {
        if (this.k != null) {
            this.k.close();
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.e), s.f491a));
        try {
            bufferedWriter.write("libcore.io.DiskLruCache");
            bufferedWriter.write("\n");
            bufferedWriter.write(Config.CHANNEL_ID);
            bufferedWriter.write("\n");
            bufferedWriter.write(Integer.toString(this.g));
            bufferedWriter.write("\n");
            bufferedWriter.write(Integer.toString(this.i));
            bufferedWriter.write("\n");
            bufferedWriter.write("\n");
            for (b next : this.l.values()) {
                if (next.e != null) {
                    bufferedWriter.write("DIRTY " + next.f470b + 10);
                } else {
                    bufferedWriter.write("CLEAN " + next.f470b + next.a() + 10);
                }
            }
            bufferedWriter.close();
            if (this.d.exists()) {
                a(this.d, this.f, true);
            }
            a(this.e, this.d, false);
            this.f.delete();
            this.k = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.d, true), s.f491a));
        } catch (Throwable th) {
            bufferedWriter.close();
            throw th;
        }
    }

    private void e(String str) {
        if (!f464a.matcher(str).matches()) {
            throw new IllegalArgumentException("keys must match regex [a-z0-9_-]{1,120}: \"" + str + "\"");
        }
    }

    /* access modifiers changed from: private */
    public boolean f() {
        return this.m >= 2000 && this.m >= this.l.size();
    }

    private void g() {
        if (this.k == null) {
            throw new IllegalStateException("cache is closed");
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        while (this.j > this.h) {
            c((String) this.l.entrySet().iterator().next().getKey());
        }
    }

    public synchronized c a(String str) {
        c cVar = null;
        synchronized (this) {
            if (this.k != null) {
                g();
                e(str);
                b bVar = this.l.get(str);
                if (bVar != null && bVar.d) {
                    InputStream[] inputStreamArr = new InputStream[this.i];
                    int i2 = 0;
                    while (i2 < this.i) {
                        try {
                            inputStreamArr[i2] = new FileInputStream(bVar.a(i2));
                            i2++;
                        } catch (FileNotFoundException e2) {
                            int i3 = 0;
                            while (i3 < this.i && inputStreamArr[i3] != null) {
                                s.a(inputStreamArr[i3]);
                                i3++;
                            }
                        }
                    }
                    this.m++;
                    this.k.append((CharSequence) ("READ " + str + 10));
                    if (f()) {
                        this.f465b.submit(this.o);
                    }
                    cVar = new c(this, str, bVar.f, inputStreamArr, bVar.c, null);
                }
            }
        }
        return cVar;
    }

    public void a() {
        close();
        s.a(this.c);
    }

    public a b(String str) {
        return a(str, -1);
    }

    public synchronized boolean c(String str) {
        boolean z;
        int i2 = 0;
        synchronized (this) {
            g();
            e(str);
            b bVar = this.l.get(str);
            if (bVar == null || bVar.e != null) {
                z = false;
            } else {
                while (i2 < this.i) {
                    File a2 = bVar.a(i2);
                    if (!a2.exists() || a2.delete()) {
                        this.j -= bVar.c[i2];
                        bVar.c[i2] = 0;
                        i2++;
                    } else {
                        throw new IOException("failed to delete " + a2);
                    }
                }
                this.m++;
                this.k.append((CharSequence) ("REMOVE " + str + 10));
                this.l.remove(str);
                if (f()) {
                    this.f465b.submit(this.o);
                }
                z = true;
            }
        }
        return z;
    }

    public synchronized void close() {
        if (this.k != null) {
            Iterator it = new ArrayList(this.l.values()).iterator();
            while (it.hasNext()) {
                b bVar = (b) it.next();
                if (bVar.e != null) {
                    bVar.e.b();
                }
            }
            h();
            this.k.close();
            this.k = null;
        }
    }
}
