package com.aps;

import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.os.Environment;
import android.os.Process;
import android.os.StatFs;
import android.telephony.NeighboringCellInfo;
import android.text.TextUtils;
import com.qihoo360.daily.activity.SplashActivity;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.zip.GZIPOutputStream;

public class ah {

    /* renamed from: a  reason: collision with root package name */
    private Context f414a;

    /* renamed from: b  reason: collision with root package name */
    private int f415b = 0;
    private int c = 0;
    private int d = 0;

    protected ah(Context context) {
        this.f414a = context;
        a(768);
    }

    private static int a(int i, int i2) {
        return i < i2 ? i : i2;
    }

    protected static aa a(Location location, ak akVar, int i, byte b2, long j, boolean z) {
        int i2;
        aa aaVar = new aa();
        if (i <= 0 || i > 3 || akVar == null) {
            return null;
        }
        boolean z2 = i == 1 || i == 3;
        boolean z3 = i == 2 || i == 3;
        byte[] bytes = akVar.p().getBytes();
        System.arraycopy(bytes, 0, aaVar.c, 0, a(bytes.length, aaVar.c.length));
        byte[] bytes2 = akVar.f().getBytes();
        System.arraycopy(bytes2, 0, aaVar.g, 0, a(bytes2.length, aaVar.g.length));
        byte[] bytes3 = akVar.g().getBytes();
        System.arraycopy(bytes3, 0, aaVar.f402a, 0, a(bytes3.length, aaVar.f402a.length));
        byte[] bytes4 = akVar.h().getBytes();
        System.arraycopy(bytes4, 0, aaVar.f403b, 0, a(bytes4.length, aaVar.f403b.length));
        aaVar.d = (short) akVar.q();
        aaVar.e = (short) akVar.r();
        aaVar.f = (byte) akVar.s();
        byte[] bytes5 = akVar.t().getBytes();
        System.arraycopy(bytes5, 0, aaVar.h, 0, a(bytes5.length, aaVar.h.length));
        long j2 = j / 1000;
        if (location != null && akVar.e()) {
            x xVar = new x();
            xVar.f499b = (int) j2;
            z zVar = new z();
            zVar.f502a = (int) (location.getLongitude() * 1000000.0d);
            zVar.f503b = (int) (location.getLatitude() * 1000000.0d);
            zVar.c = (int) location.getAltitude();
            zVar.d = (int) location.getAccuracy();
            zVar.e = (int) location.getSpeed();
            zVar.f = (short) ((int) location.getBearing());
            if (Build.MODEL.equals("sdk") || (ak.b(akVar.y()) && y.f501b)) {
                zVar.g = 1;
            } else {
                zVar.g = 0;
            }
            zVar.h = b2;
            zVar.i = System.currentTimeMillis();
            zVar.j = akVar.o();
            xVar.c = zVar;
            i2 = 1;
            aaVar.j.add(xVar);
        } else if (!z) {
            return null;
        } else {
            x xVar2 = new x();
            xVar2.f499b = (int) j2;
            ac acVar = new ac();
            acVar.f404a = akVar.x();
            for (int i3 = 0; i3 < acVar.f404a; i3++) {
                ad adVar = new ad();
                adVar.f406a = (byte) akVar.a(i3).length();
                System.arraycopy(akVar.a(i3).getBytes(), 0, adVar.f407b, 0, adVar.f406a);
                adVar.c = akVar.b(i3);
                adVar.d = akVar.c(i3);
                adVar.e = akVar.d(i3);
                adVar.f = akVar.e(i3);
                adVar.g = akVar.f(i3);
                adVar.h = (byte) akVar.g(i3).length();
                System.arraycopy(akVar.g(i3).getBytes(), 0, adVar.i, 0, adVar.h);
                adVar.j = akVar.h(i3);
                acVar.f405b.add(adVar);
            }
            xVar2.g = acVar;
            i2 = 1;
            aaVar.j.add(xVar2);
        }
        if (akVar.c() && !akVar.i() && z2 && !z) {
            x xVar3 = new x();
            xVar3.f499b = (int) j2;
            v vVar = new v();
            List a2 = akVar.a(location.getSpeed());
            if (a2 != null && a2.size() >= 3) {
                vVar.f494a = (short) ((Integer) a2.get(0)).intValue();
                vVar.f495b = ((Integer) a2.get(1)).intValue();
            }
            vVar.c = akVar.l();
            List m = akVar.m();
            vVar.d = (byte) m.size();
            int i4 = 0;
            while (true) {
                int i5 = i4;
                if (i5 >= m.size()) {
                    break;
                }
                aj ajVar = new aj();
                ajVar.f418a = (short) ((NeighboringCellInfo) m.get(i5)).getLac();
                ajVar.f419b = ((NeighboringCellInfo) m.get(i5)).getCid();
                ajVar.c = (byte) ((NeighboringCellInfo) m.get(i5)).getRssi();
                vVar.e.add(ajVar);
                i4 = i5 + 1;
            }
            xVar3.d = vVar;
            i2 = 2;
            aaVar.j.add(xVar3);
        }
        int i6 = i2;
        if (akVar.c() && akVar.i() && z2 && !z) {
            x xVar4 = new x();
            xVar4.f499b = (int) j2;
            ai aiVar = new ai();
            List b3 = akVar.b(location.getSpeed());
            if (b3 != null && b3.size() >= 6) {
                aiVar.f416a = ((Integer) b3.get(3)).intValue();
                aiVar.f417b = ((Integer) b3.get(4)).intValue();
                aiVar.c = (short) ((Integer) b3.get(0)).intValue();
                aiVar.d = (short) ((Integer) b3.get(1)).intValue();
                aiVar.e = ((Integer) b3.get(2)).intValue();
                aiVar.f = akVar.l();
            }
            xVar4.e = aiVar;
            i6++;
            aaVar.j.add(xVar4);
        }
        if (akVar.d() && z3 && !z) {
            x xVar5 = new x();
            ae aeVar = new ae();
            List u = akVar.u();
            xVar5.f499b = (int) (((Long) u.get(0)).longValue() / 1000);
            aeVar.f408a = (byte) (u.size() - 1);
            int i7 = 1;
            while (true) {
                int i8 = i7;
                if (i8 >= u.size()) {
                    break;
                }
                List list = (List) u.get(i8);
                if (list != null && list.size() >= 3) {
                    af afVar = new af();
                    byte[] bytes6 = ((String) list.get(0)).getBytes();
                    System.arraycopy(bytes6, 0, afVar.f410a, 0, a(bytes6.length, afVar.f410a.length));
                    afVar.f411b = (short) ((Integer) list.get(1)).intValue();
                    byte[] bytes7 = ((String) list.get(2)).getBytes();
                    System.arraycopy(bytes7, 0, afVar.c, 0, a(bytes7.length, afVar.c.length));
                    aeVar.f409b.add(afVar);
                }
                i7 = i8 + 1;
            }
            xVar5.f = aeVar;
            i6++;
            aaVar.j.add(xVar5);
        }
        aaVar.i = (short) i6;
        if (i6 >= 2 || z) {
            return aaVar;
        }
        return null;
    }

    protected static File a(Context context) {
        return new File(Environment.getExternalStorageDirectory().getPath() + ("/Android/data/" + context.getPackageName() + "/files/"));
    }

    public static Object a(Object obj, String str, Object... objArr) {
        Class<?> cls = obj.getClass();
        Class<Integer>[] clsArr = new Class[objArr.length];
        int length = objArr.length;
        for (int i = 0; i < length; i++) {
            clsArr[i] = objArr[i].getClass();
            if (clsArr[i] == Integer.class) {
                clsArr[i] = Integer.TYPE;
            }
        }
        Method declaredMethod = cls.getDeclaredMethod(str, clsArr);
        if (!declaredMethod.isAccessible()) {
            declaredMethod.setAccessible(true);
        }
        return declaredMethod.invoke(obj, objArr);
    }

    private static ArrayList a(File[] fileArr) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < fileArr.length; i++) {
            if (fileArr[i].isFile() && fileArr[i].getName().length() == 10 && TextUtils.isDigitsOnly(fileArr[i].getName())) {
                arrayList.add(fileArr[i]);
            }
        }
        return arrayList;
    }

    protected static byte[] a(BitSet bitSet) {
        byte[] bArr = new byte[(bitSet.size() / 8)];
        for (int i = 0; i < bitSet.size(); i++) {
            int i2 = i / 8;
            bArr[i2] = (byte) (((bitSet.get(i) ? 1 : 0) << (7 - (i % 8))) | bArr[i2]);
        }
        return bArr;
    }

    protected static byte[] a(byte[] bArr) {
        byte[] bArr2 = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gZIPOutputStream.write(bArr);
            gZIPOutputStream.finish();
            gZIPOutputStream.close();
            bArr2 = byteArrayOutputStream.toByteArray();
            byteArrayOutputStream.close();
            return bArr2;
        } catch (Exception e) {
            return bArr2;
        }
    }

    protected static byte[] a(byte[] bArr, int i) {
        if (bArr == null || bArr.length == 0) {
            return null;
        }
        int indexOf = new String(bArr).indexOf(0);
        if (indexOf <= 0) {
            i = 1;
        } else if (indexOf + 1 <= i) {
            i = indexOf + 1;
        }
        byte[] bArr2 = new byte[i];
        System.arraycopy(bArr, 0, bArr2, 0, i);
        bArr2[i - 1] = 0;
        return bArr2;
    }

    public static int b(Object obj, String str, Object... objArr) {
        Class<?> cls = obj.getClass();
        Class<Integer>[] clsArr = new Class[objArr.length];
        int length = objArr.length;
        for (int i = 0; i < length; i++) {
            clsArr[i] = objArr[i].getClass();
            if (clsArr[i] == Integer.class) {
                clsArr[i] = Integer.TYPE;
            }
        }
        Method declaredMethod = cls.getDeclaredMethod(str, clsArr);
        if (!declaredMethod.isAccessible()) {
            declaredMethod.setAccessible(true);
        }
        return ((Integer) declaredMethod.invoke(obj, objArr)).intValue();
    }

    /* JADX WARN: Type inference failed for: r2v3, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static java.util.BitSet b(byte[] r9) {
        /*
            r4 = 1
            r1 = 0
            java.util.BitSet r7 = new java.util.BitSet
            int r0 = r9.length
            int r0 = r0 << 3
            r7.<init>(r0)
            r0 = r1
            r2 = r1
        L_0x000c:
            int r3 = r9.length
            if (r0 >= r3) goto L_0x002b
            r3 = 7
            r5 = r3
        L_0x0011:
            if (r5 < 0) goto L_0x0028
            int r6 = r2 + 1
            byte r3 = r9[r0]
            int r8 = r4 << r5
            r3 = r3 & r8
            int r3 = r3 >> r5
            if (r3 != r4) goto L_0x0026
            r3 = r4
        L_0x001e:
            r7.set(r2, r3)
            int r2 = r5 + -1
            r5 = r2
            r2 = r6
            goto L_0x0011
        L_0x0026:
            r3 = r1
            goto L_0x001e
        L_0x0028:
            int r0 = r0 + 1
            goto L_0x000c
        L_0x002b:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.aps.ah.b(byte[]):java.util.BitSet");
    }

    private File c(long j) {
        boolean z;
        File file;
        boolean z2 = false;
        if (Process.myUid() == 1000) {
            return null;
        }
        try {
            z = "mounted".equals(Environment.getExternalStorageState());
        } catch (Exception e) {
            z = false;
        }
        if (!c() || z) {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            if (((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize()) <= ((long) (this.c / 2))) {
                return null;
            }
            File file2 = new File(a(this.f414a).getPath() + File.separator + "carrierdata");
            if (!file2.exists() || !file2.isDirectory()) {
                file2.mkdirs();
            }
            file = new File(file2.getPath() + File.separator + j);
            try {
                z2 = file.createNewFile();
            } catch (IOException e2) {
            }
        } else {
            file = null;
        }
        if (!z2) {
            return null;
        }
        return file;
    }

    protected static boolean c() {
        if (Build.VERSION.SDK_INT >= 9) {
            try {
                return ((Boolean) Environment.class.getMethod("isExternalStorageRemovable", null).invoke(null, null)).booleanValue();
            } catch (Exception e) {
            }
        }
        return true;
    }

    private File d() {
        boolean z;
        File file;
        File[] listFiles;
        if (Process.myUid() == 1000) {
            return null;
        }
        try {
            z = "mounted".equals(Environment.getExternalStorageState());
        } catch (Exception e) {
            z = false;
        }
        if (!c() || z) {
            File file2 = new File(a(this.f414a).getPath() + File.separator + "carrierdata");
            if (file2.exists() && file2.isDirectory() && (listFiles = file2.listFiles()) != null && listFiles.length > 0) {
                ArrayList a2 = a(listFiles);
                if (a2.size() == 1) {
                    if (((File) a2.get(0)).length() < ((long) this.d)) {
                        file = (File) a2.get(0);
                        return file;
                    }
                } else if (a2.size() >= 2) {
                    file = (File) a2.get(0);
                    File file3 = (File) a2.get(1);
                    if (file.getName().compareTo(file3.getName()) <= 0) {
                        file = file3;
                    }
                    return file;
                }
            }
        }
        file = null;
        return file;
    }

    private int e() {
        boolean z;
        File[] listFiles;
        if (Process.myUid() == 1000) {
            return 0;
        }
        try {
            z = "mounted".equals(Environment.getExternalStorageState());
        } catch (Exception e) {
            z = false;
        }
        if (c() && !z) {
            return 0;
        }
        File file = new File(a(this.f414a).getPath() + File.separator + "carrierdata");
        if (!file.exists() || !file.isDirectory() || (listFiles = file.listFiles()) == null || listFiles.length <= 0) {
            return 0;
        }
        ArrayList a2 = a(listFiles);
        return a2.size() == 1 ? ((File) a2.get(0)).length() <= 0 ? 10 : 1 : a2.size() >= 2 ? 2 : 0;
    }

    private File f() {
        boolean z;
        File file;
        File a2;
        File[] listFiles;
        if (Process.myUid() == 1000) {
            return null;
        }
        try {
            z = "mounted".equals(Environment.getExternalStorageState());
        } catch (Exception e) {
            z = false;
        }
        if ((!c() || z) && (a2 = a(this.f414a)) != null) {
            File file2 = new File(a2.getPath() + File.separator + "carrierdata");
            if (file2.exists() && file2.isDirectory() && (listFiles = file2.listFiles()) != null && listFiles.length > 0) {
                ArrayList a3 = a(listFiles);
                if (a3.size() >= 2) {
                    File file3 = (File) a3.get(0);
                    file = (File) a3.get(1);
                    if (file3.getName().compareTo(file.getName()) <= 0) {
                        file = file3;
                    }
                    return file;
                }
            }
        }
        file = null;
        return file;
    }

    /* access modifiers changed from: protected */
    public int a() {
        return this.f415b;
    }

    /* access modifiers changed from: protected */
    public synchronized File a(long j) {
        File d2;
        d2 = d();
        if (d2 == null) {
            d2 = c(j);
        }
        return d2;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        this.f415b = i;
        this.c = ((this.f415b << 3) * SplashActivity.SPLASH_TYPE_OP_TIME) + this.f415b + 4;
        if (this.f415b == 256 || this.f415b == 768) {
            this.d = this.c / 100;
        } else if (this.f415b == 8736) {
            this.d = this.c - 5000;
        }
    }

    /* access modifiers changed from: protected */
    public File b() {
        return f();
    }

    /* access modifiers changed from: protected */
    public synchronized boolean b(long j) {
        boolean z = false;
        synchronized (this) {
            int e = e();
            if (e != 0) {
                if (e == 1) {
                    if (c(j) != null) {
                        z = true;
                    }
                } else if (e == 2) {
                    z = true;
                }
            }
        }
        return z;
    }
}
