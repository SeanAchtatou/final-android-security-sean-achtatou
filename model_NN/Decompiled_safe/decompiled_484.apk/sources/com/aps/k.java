package com.aps;

import android.app.PendingIntent;
import android.content.Context;
import com.amap.api.location.AMapLocation;
import org.json.JSONObject;

public interface k {
    c a();

    void a(PendingIntent pendingIntent);

    void a(Context context);

    void a(AMapLocation aMapLocation);

    void a(j jVar, PendingIntent pendingIntent);

    void a(String str);

    void a(JSONObject jSONObject);

    void b();

    void b(PendingIntent pendingIntent);

    void b(j jVar, PendingIntent pendingIntent);
}
