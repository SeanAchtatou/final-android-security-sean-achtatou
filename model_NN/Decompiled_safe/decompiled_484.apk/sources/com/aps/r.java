package com.aps;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

class r implements Closeable {

    /* renamed from: a  reason: collision with root package name */
    private final InputStream f488a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final Charset f489b;
    private byte[] c;
    private int d;
    private int e;

    public r(InputStream inputStream, int i, Charset charset) {
        if (inputStream == null || charset == null) {
            throw new NullPointerException();
        } else if (i < 0) {
            throw new IllegalArgumentException("capacity <= 0");
        } else if (!charset.equals(s.f491a)) {
            throw new IllegalArgumentException("Unsupported encoding");
        } else {
            this.f488a = inputStream;
            this.f489b = charset;
            this.c = new byte[i];
        }
    }

    public r(InputStream inputStream, Charset charset) {
        this(inputStream, 8192, charset);
    }

    private void b() {
        int read = this.f488a.read(this.c, 0, this.c.length);
        if (read == -1) {
            throw new EOFException();
        }
        this.d = 0;
        this.e = read;
    }

    public String a() {
        int i;
        String byteArrayOutputStream;
        synchronized (this.f488a) {
            if (this.c == null) {
                throw new IOException("LineReader is closed");
            }
            if (this.d >= this.e) {
                b();
            }
            int i2 = this.d;
            while (true) {
                if (i2 == this.e) {
                    AnonymousClass1 r1 = new ByteArrayOutputStream((this.e - this.d) + 80) {
                        public String toString() {
                            try {
                                return new String(this.buf, 0, (this.count <= 0 || this.buf[this.count + -1] != 13) ? this.count : this.count - 1, r.this.f489b.name());
                            } catch (UnsupportedEncodingException e) {
                                throw new AssertionError(e);
                            }
                        }
                    };
                    loop1:
                    while (true) {
                        r1.write(this.c, this.d, this.e - this.d);
                        this.e = -1;
                        b();
                        i = this.d;
                        while (true) {
                            if (i != this.e) {
                                if (this.c[i] == 10) {
                                    break loop1;
                                }
                                i++;
                            }
                        }
                    }
                    if (i != this.d) {
                        r1.write(this.c, this.d, i - this.d);
                    }
                    this.d = i + 1;
                    byteArrayOutputStream = r1.toString();
                } else if (this.c[i2] == 10) {
                    byteArrayOutputStream = new String(this.c, this.d, ((i2 == this.d || this.c[i2 + -1] != 13) ? i2 : i2 - 1) - this.d, this.f489b.name());
                    this.d = i2 + 1;
                } else {
                    i2++;
                }
            }
        }
        return byteArrayOutputStream;
    }

    public void close() {
        synchronized (this.f488a) {
            if (this.c != null) {
                this.c = null;
                this.f488a.close();
            }
        }
    }
}
