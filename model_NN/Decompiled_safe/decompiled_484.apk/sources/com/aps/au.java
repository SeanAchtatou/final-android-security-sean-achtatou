package com.aps;

import android.location.GpsSatellite;
import android.location.GpsStatus;
import com.amap.api.location.LocationManagerProxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public final class au implements GpsStatus.Listener, GpsStatus.NmeaListener {

    /* renamed from: a  reason: collision with root package name */
    private long f430a = 0;

    /* renamed from: b  reason: collision with root package name */
    private long f431b = 0;
    private boolean c = false;
    private List d = new ArrayList();
    private String e = null;
    private String f = null;
    private String g = null;
    private /* synthetic */ y h;

    protected au(y yVar) {
        this.h = yVar;
    }

    public final void a(String str) {
        if (System.currentTimeMillis() - this.f431b > 400 && this.c && this.d.size() > 0) {
            try {
                w wVar = new w(this.d, this.e, null, this.g);
                if (wVar.a()) {
                    int unused = this.h.O = y.a(this.h, wVar, this.h.L);
                    if (this.h.O > 0) {
                        y.b(this.h, String.format(Locale.CHINA, "&nmea=%.1f|%.1f&g_tp=%d", Double.valueOf(wVar.c()), Double.valueOf(wVar.b()), Integer.valueOf(this.h.O)));
                    }
                } else {
                    int unused2 = this.h.O = 0;
                }
            } catch (Exception e2) {
                int unused3 = this.h.O = 0;
            }
            this.d.clear();
            this.g = null;
            this.f = null;
            this.e = null;
            this.c = false;
        }
        if (str.startsWith("$GPGGA")) {
            this.c = true;
            this.e = str.trim();
        } else if (str.startsWith("$GPGSV")) {
            this.d.add(str.trim());
        } else if (str.startsWith("$GPGSA")) {
            this.g = str.trim();
        }
        this.f431b = System.currentTimeMillis();
    }

    public final void onGpsStatusChanged(int i) {
        int i2 = 0;
        try {
            if (this.h.s != null) {
                switch (i) {
                    case 2:
                        int unused = this.h.N = 0;
                        return;
                    case 3:
                    default:
                        return;
                    case 4:
                        if (y.f500a || System.currentTimeMillis() - this.f430a >= 10000) {
                            if (this.h.J == null) {
                                GpsStatus unused2 = this.h.J = this.h.s.getGpsStatus(null);
                            } else {
                                this.h.s.getGpsStatus(this.h.J);
                            }
                            int unused3 = this.h.K = 0;
                            int unused4 = this.h.L = 0;
                            HashMap unused5 = this.h.M = new HashMap();
                            int i3 = 0;
                            int i4 = 0;
                            for (GpsSatellite next : this.h.J.getSatellites()) {
                                i3++;
                                if (next.usedInFix()) {
                                    i4++;
                                }
                                if (next.getSnr() > 0.0f) {
                                    i2++;
                                }
                                if (next.getSnr() >= ((float) y.X)) {
                                    y.j(this.h);
                                }
                            }
                            if (this.h.m == -1 || ((i4 >= 4 && this.h.m < 4) || (i4 < 4 && this.h.m >= 4))) {
                                int unused6 = this.h.m = i4;
                                if (i4 < 4) {
                                    if (this.h.t != null) {
                                        this.h.t.w();
                                    }
                                } else if (this.h.t != null) {
                                    this.h.t.v();
                                }
                            }
                            int unused7 = this.h.N = i2;
                            int unused8 = this.h.a(this.h.M);
                            if (y.f500a) {
                                return;
                            }
                            if ((i4 > 3 || i3 > 15) && this.h.s.getLastKnownLocation(LocationManagerProxy.GPS_PROVIDER) != null) {
                                this.f430a = System.currentTimeMillis();
                                return;
                            }
                            return;
                        }
                        return;
                }
            }
        } catch (Exception e2) {
        }
    }

    public final void onNmeaReceived(long j, String str) {
        try {
            if (y.f500a && str != null && !str.equals("") && str.length() >= 9 && str.length() <= 150) {
                this.h.F.sendMessage(this.h.F.obtainMessage(1, str));
            }
        } catch (Exception e2) {
        }
    }
}
