package com.aps;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import com.amap.api.location.AMapLocation;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.core.AMapLocException;
import com.amap.api.location.core.c;
import com.amap.api.location.core.d;
import com.mediav.ads.sdk.adcore.Config;
import com.qihoo.messenger.util.QDefine;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import org.json.JSONObject;

public class a implements k {
    private l A = l.a();
    /* access modifiers changed from: private */
    public int B = 0;
    private String C = "00:00:00:00:00:00";
    private y D = null;
    private StringBuilder E = new StringBuilder();
    private long F = 0;
    private long G = 0;
    /* access modifiers changed from: private */
    public CellLocation H = null;
    private boolean I = false;

    /* renamed from: a  reason: collision with root package name */
    TimerTask f396a;

    /* renamed from: b  reason: collision with root package name */
    Timer f397b;
    ag c;
    int d = 0;
    private Context e = null;
    /* access modifiers changed from: private */
    public int f = 9;
    private ConnectivityManager g = null;
    /* access modifiers changed from: private */
    public WifiManager h = null;
    private TelephonyManager i = null;
    /* access modifiers changed from: private */
    public List<e> j = new ArrayList();
    /* access modifiers changed from: private */
    public List<ScanResult> k = new ArrayList();
    private Map<PendingIntent, List<j>> l = new HashMap();
    private Map<PendingIntent, List<j>> m = new HashMap();
    private b n = new b();
    private PhoneStateListener o = null;
    /* access modifiers changed from: private */
    public int p = -113;
    private C0001a q = new C0001a();
    private WifiInfo r = null;
    private JSONObject s = null;
    private String t = null;
    private c u = null;
    private long v = 0;
    /* access modifiers changed from: private */
    public boolean w = false;
    /* access modifiers changed from: private */
    public long x = 0;
    /* access modifiers changed from: private */
    public long y = 0;
    /* access modifiers changed from: private */
    public long z = 0;

    /* renamed from: com.aps.a$a  reason: collision with other inner class name */
    class C0001a extends BroadcastReceiver {
        private C0001a() {
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.aps.a.a(boolean, int):int
         arg types: [int, int]
         candidates:
          com.aps.a.a(com.aps.a, long):long
          com.aps.a.a(com.aps.a, android.telephony.CellLocation):android.telephony.CellLocation
          com.aps.a.a(byte[], boolean):com.aps.c
          com.aps.a.a(com.aps.a, java.util.List):java.util.List
          com.aps.a.a(com.aps.a, int):void
          com.aps.a.a(com.aps.a, boolean):boolean
          com.aps.a.a(com.aps.j, android.app.PendingIntent):void
          com.aps.k.a(com.aps.j, android.app.PendingIntent):void
          com.aps.a.a(boolean, int):int */
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                try {
                    String action = intent.getAction();
                    if (action.equals("android.net.wifi.SCAN_RESULTS")) {
                        if (a.this.h != null) {
                            List unused = a.this.k = a.this.h.getScanResults();
                            long unused2 = a.this.z = t.a();
                            if (a.this.k == null) {
                                List unused3 = a.this.k = new ArrayList();
                            }
                        }
                    } else if (action.equals("android.net.wifi.WIFI_STATE_CHANGED")) {
                        if (a.this.h != null) {
                            int i = 4;
                            try {
                                i = a.this.h.getWifiState();
                            } catch (SecurityException e) {
                            }
                            switch (i) {
                                case 0:
                                    a.this.n();
                                    return;
                                case 1:
                                    a.this.n();
                                    return;
                                case 2:
                                case 3:
                                default:
                                    return;
                                case 4:
                                    a.this.n();
                                    return;
                            }
                            th.printStackTrace();
                        }
                    } else if (action.equals("android.intent.action.SCREEN_ON")) {
                        CellLocation.requestLocationUpdate();
                        a.this.o();
                        f.i = 10000;
                        f.j = 30000;
                    } else if (action.equals("android.intent.action.SCREEN_OFF")) {
                        if (a.this.B >= 5) {
                            f.i = 20000;
                            f.j = QDefine.WAP_ACCESS_PERIOD;
                        }
                    } else if (action.equals("android.intent.action.AIRPLANE_MODE")) {
                        boolean unused4 = a.this.w = t.a(context);
                    } else if (action.equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                        a.this.a(true, 2);
                    }
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        }
    }

    private c a(byte[] bArr, boolean z2) {
        if (this.e == null) {
            return null;
        }
        m mVar = new m();
        String a2 = this.A.a(bArr, this.e, this.s);
        try {
            d.a(a2);
        } catch (AMapLocException e2) {
            throw e2;
        } catch (Exception e3) {
        }
        String[] a3 = l.a(this.s);
        if (a2 != null && a2.indexOf("<saps>") != -1) {
            a2 = this.n.a(mVar.a(a2), "GBK");
        } else if (a3[0].equals("true")) {
            t.a("api return pure");
        } else {
            t.a("aps return pure");
        }
        c b2 = mVar.b(a2);
        if (!t.a(b2)) {
            throw new AMapLocException(AMapLocException.ERROR_UNKNOWN);
        }
        if (b2.t() != null) {
        }
        if (this.E == null || this.E.length() <= 0) {
            return b2;
        }
        this.t = this.E.toString();
        return b2;
    }

    private e a(NeighboringCellInfo neighboringCellInfo) {
        if (t.b() < 5) {
            return null;
        }
        try {
            e eVar = new e();
            String[] a2 = t.a(this.i);
            eVar.f460a = a2[0];
            eVar.f461b = a2[1];
            eVar.c = neighboringCellInfo.getLac();
            eVar.d = neighboringCellInfo.getCid();
            eVar.j = t.a(neighboringCellInfo.getRssi());
            return eVar;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    private String a(int i2, int i3, int i4) {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("e", i2);
        jSONObject.put("d", i3);
        jSONObject.put("u", i4);
        return jSONObject.toString();
    }

    private void a(CellLocation cellLocation) {
        CellLocation cellLocation2 = null;
        if (!(this.w || this.i == null || this.i == null)) {
            cellLocation2 = this.i.getCellLocation();
        }
        if (cellLocation2 != null) {
            cellLocation = cellLocation2;
        }
        if (cellLocation != null) {
            switch (t.a(cellLocation, this.e)) {
                case 1:
                    if (this.i != null) {
                        c(cellLocation);
                        return;
                    }
                    return;
                case 2:
                    d(cellLocation);
                    return;
                default:
                    return;
            }
        }
    }

    private void a(StringBuilder sb) {
        if (sb != null) {
            for (String str : new String[]{" phnum=\"\"", " nettype=\"\"", " nettype=\"UNKNOWN\"", " inftype=\"\"", "<macs><![CDATA[]]></macs>", "<nb></nb>", "<mmac><![CDATA[]]></mmac>", " gtype=\"0\"", " glong=\"0.0\"", " glat=\"0.0\"", " precision=\"0.0\"", " glong=\"0\"", " glat=\"0\"", " precision=\"0\"", "<smac>null</smac>", "<smac>00:00:00:00:00:00</smac>", "<imei>000000000000000</imei>", "<imsi>000000000000000</imsi>", "<mcc>000</mcc>", "<mcc>0</mcc>", "<lac>0</lac>", "<cellid>0</cellid>", "<key></key>"}) {
                while (sb.indexOf(str) != -1) {
                    int indexOf = sb.indexOf(str);
                    sb.delete(indexOf, str.length() + indexOf);
                }
            }
            while (sb.indexOf("*<") != -1) {
                sb.deleteCharAt(sb.indexOf("*<"));
            }
        }
    }

    private synchronized void a(List<ScanResult> list) {
        if (list != null) {
            if (list.size() >= 1) {
                HashMap hashMap = new HashMap();
                for (int i2 = 0; i2 < list.size(); i2++) {
                    ScanResult scanResult = list.get(i2);
                    if (list.size() <= 20 || a(scanResult.level)) {
                        if (scanResult.SSID != null) {
                            scanResult.SSID = scanResult.SSID.replace("*", ".");
                        } else {
                            scanResult.SSID = "null";
                        }
                        hashMap.put(Integer.valueOf((scanResult.level * 30) + i2), scanResult);
                    }
                }
                TreeMap treeMap = new TreeMap(Collections.reverseOrder());
                treeMap.putAll(hashMap);
                list.clear();
                for (Map.Entry value : treeMap.entrySet()) {
                    list.add(value.getValue());
                    if (list.size() > 29) {
                        break;
                    }
                }
                hashMap.clear();
                treeMap.clear();
            }
        }
    }

    private boolean a(int i2) {
        int i3 = 20;
        try {
            i3 = WifiManager.calculateSignalLevel(i2, 20);
        } catch (ArithmeticException e2) {
            t.a(e2);
        }
        return i3 >= 1;
    }

    private boolean a(long j2) {
        long a2 = t.a();
        if (a2 - j2 >= 300) {
            return false;
        }
        long j3 = 0;
        if (this.u != null) {
            j3 = a2 - this.u.h();
        }
        return j3 <= 10000;
    }

    private boolean a(ScanResult scanResult) {
        boolean z2 = false;
        if (scanResult != null) {
            try {
                if (!TextUtils.isEmpty(scanResult.BSSID) && !scanResult.BSSID.equals("00:00:00:00:00:00")) {
                    z2 = true;
                }
            } catch (Exception e2) {
                return true;
            }
        }
        return z2;
    }

    private boolean a(WifiInfo wifiInfo) {
        return (wifiInfo == null || wifiInfo.getBSSID() == null || wifiInfo.getSSID() == null || wifiInfo.getBSSID().equals("00:00:00:00:00:00") || TextUtils.isEmpty(wifiInfo.getSSID())) ? false : true;
    }

    private synchronized byte[] a(Object obj) {
        o oVar;
        String str;
        String str2;
        String str3;
        String sb;
        oVar = new o();
        this.E.delete(0, this.E.length());
        f.c = "";
        String str4 = "";
        String str5 = "";
        String a2 = y.a("version");
        StringBuilder sb2 = new StringBuilder();
        StringBuilder sb3 = new StringBuilder();
        StringBuilder sb4 = new StringBuilder();
        String str6 = this.f == 2 ? Config.CHANNEL_ID : "0";
        if (this.i != null) {
            if (f.f462a == null || "888888888888888".equals(f.f462a)) {
                try {
                    f.f462a = this.i.getDeviceId();
                    if (f.f462a == null) {
                        f.f462a = "888888888888888";
                    }
                } catch (SecurityException e2) {
                }
            }
            if (f.f463b == null || "888888888888888".equals(f.f463b)) {
                try {
                    f.f463b = this.i.getSubscriberId();
                    if (f.f463b == null) {
                        f.f463b = "888888888888888";
                    }
                } catch (SecurityException e3) {
                }
            }
        }
        NetworkInfo networkInfo = null;
        try {
            networkInfo = this.g.getActiveNetworkInfo();
        } catch (SecurityException e4) {
        }
        if (l.a(networkInfo) != -1) {
            str4 = l.a(this.i);
            if (!s() || !a(this.r)) {
                str5 = Config.CHANNEL_ID;
                if (!s()) {
                    n();
                    str = str4;
                    str2 = str5;
                }
                str = str4;
                str2 = str5;
            } else {
                str = str4;
                str2 = "2";
            }
        } else {
            this.r = null;
            str = str4;
            str2 = str5;
        }
        String str7 = l.a(this.s)[1];
        oVar.i = str6;
        oVar.j = "0";
        oVar.k = "0";
        oVar.l = "0";
        oVar.m = "0";
        oVar.c = f.d;
        oVar.d = f.e;
        oVar.n = str7;
        oVar.o = f.f462a;
        oVar.r = f.c;
        oVar.p = f.f463b;
        oVar.q = this.C;
        oVar.s = str;
        oVar.t = str2;
        oVar.f = c.e();
        oVar.g = "android" + c.d();
        oVar.h = c.g() + "," + c.h();
        oVar.B = "V1.3.1";
        oVar.C = a2;
        try {
            if (this.k != null && this.k.size() > 0) {
                oVar.E = (t.a() - this.z) + "";
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        this.E.append("<?xml version=\"1.0\" encoding=\"");
        this.E.append("GBK").append("\"?>");
        this.E.append("<Cell_Req ver=\"3.0\"><HDR version=\"3.0\" cdma=\"");
        this.E.append(str6);
        this.E.append("\" gtype=\"").append("0");
        this.E.append("\" glong=\"").append("0");
        this.E.append("\" glat=\"").append("0");
        this.E.append("\" precision=\"").append("0");
        this.E.append("\"><src>").append(f.d);
        this.E.append("</src><license>").append(f.e);
        this.E.append("</license><key>").append(str7);
        this.E.append("</key><clientid>").append(f.f);
        this.E.append("</clientid><imei>").append(f.f462a);
        this.E.append("</imei><imsi>").append(f.f463b);
        this.E.append("</imsi><smac>").append(this.C);
        this.E.append("</smac></HDR><DRR phnum=\"").append(f.c);
        this.E.append("\" nettype=\"").append(str);
        this.E.append("\" inftype=\"").append(str2).append("\">");
        if (this.j.size() > 0) {
            StringBuilder sb5 = new StringBuilder();
            switch (this.f) {
                case 1:
                    e eVar = this.j.get(0);
                    sb5.delete(0, sb5.length());
                    sb5.append("<mcc>").append(eVar.f460a).append("</mcc>");
                    sb5.append("<mnc>").append(eVar.f461b).append("</mnc>");
                    sb5.append("<lac>").append(eVar.c).append("</lac>");
                    sb5.append("<cellid>").append(eVar.d);
                    sb5.append("</cellid>");
                    sb5.append("<signal>").append(eVar.j);
                    sb5.append("</signal>");
                    String sb6 = sb5.toString();
                    for (int i2 = 0; i2 < this.j.size(); i2++) {
                        if (i2 != 0) {
                            e eVar2 = this.j.get(i2);
                            sb2.append(eVar2.c).append(",");
                            sb2.append(eVar2.d).append(",");
                            sb2.append(eVar2.j);
                            if (i2 != this.j.size() - 1) {
                                sb2.append("*");
                            }
                        }
                    }
                    sb = sb6;
                    break;
                case 2:
                    e eVar3 = this.j.get(0);
                    sb5.delete(0, sb5.length());
                    sb5.append("<mcc>").append(eVar3.f460a).append("</mcc>");
                    sb5.append("<sid>").append(eVar3.g).append("</sid>");
                    sb5.append("<nid>").append(eVar3.h).append("</nid>");
                    sb5.append("<bid>").append(eVar3.i).append("</bid>");
                    if (eVar3.f > 0 && eVar3.e > 0) {
                        sb5.append("<lon>").append(eVar3.f).append("</lon>");
                        sb5.append("<lat>").append(eVar3.e).append("</lat>");
                    }
                    sb5.append("<signal>").append(eVar3.j).append("</signal>");
                    sb = sb5.toString();
                    break;
                default:
                    sb = "";
                    break;
            }
            sb5.delete(0, sb5.length());
            str3 = sb;
        } else {
            str3 = "";
        }
        if (s()) {
            if (a(this.r)) {
                sb4.append(this.r.getBSSID()).append(",");
                sb4.append(this.r.getRssi()).append(",");
                sb4.append(this.r.getSSID().replace("*", "."));
            }
            for (int i3 = 0; i3 < this.k.size(); i3++) {
                ScanResult scanResult = this.k.get(i3);
                if (a(scanResult)) {
                    sb3.append(scanResult.BSSID).append(",");
                    sb3.append(scanResult.level).append(",");
                    sb3.append(i3).append("*");
                }
            }
        } else {
            n();
        }
        this.E.append(str3);
        this.E.append(String.format("<nb>%s</nb>", sb2));
        if (sb3.length() == 0) {
            this.E.append(String.format("<macs><![CDATA[%s]]></macs>", sb4));
        } else {
            sb3.deleteCharAt(sb3.length() - 1);
            this.E.append(String.format("<macs><![CDATA[%s]]></macs>", sb3));
        }
        this.E.append(String.format("<mmac><![CDATA[%s]]></mmac>", sb4));
        this.E.append("</DRR></Cell_Req>");
        a(this.E);
        StringBuilder sb7 = sb3.length() == 0 ? sb4 : sb3;
        oVar.v = str3;
        oVar.w = sb2.toString();
        oVar.x = sb4.toString();
        oVar.y = sb7.toString();
        oVar.u = String.valueOf(this.f);
        sb2.delete(0, sb2.length());
        sb7.delete(0, sb7.length());
        sb4.delete(0, sb4.length());
        return oVar.a();
    }

    private e b(CellLocation cellLocation) {
        GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
        e eVar = new e();
        String[] a2 = t.a(this.i);
        eVar.f460a = a2[0];
        eVar.f461b = a2[1];
        eVar.c = gsmCellLocation.getLac();
        eVar.d = gsmCellLocation.getCid();
        eVar.j = this.p;
        return eVar;
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        if (i2 == -113) {
            this.p = -113;
            return;
        }
        this.p = i2;
        switch (this.f) {
            case 1:
            case 2:
                if (this.j.size() > 0) {
                    this.j.get(0).j = this.p;
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void c(final int i2) {
        try {
            if (t.a() - this.F < 45000 || !e()) {
                return;
            }
            if (!e() || this.D.f() >= 20) {
                x();
                if (this.f396a == null) {
                    this.f396a = new TimerTask() {
                        public void run() {
                            try {
                                if (a.this.m()) {
                                    a.this.d(i2);
                                    if (!a.this.e()) {
                                        a.this.v();
                                        return;
                                    }
                                    return;
                                }
                                a.this.v();
                            } catch (Throwable th) {
                                th.printStackTrace();
                            }
                        }
                    };
                }
                if (this.f397b == null) {
                    this.f397b = new Timer(false);
                    this.f397b.schedule(this.f396a, 3000, 3000);
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    private void c(CellLocation cellLocation) {
        e a2;
        if (this.j != null && cellLocation != null && this.i != null) {
            this.j.clear();
            GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
            if (!(gsmCellLocation.getLac() == -1 ? false : (gsmCellLocation.getCid() == -1 || gsmCellLocation.getCid() == 65535 || gsmCellLocation.getCid() >= 268435455) ? false : gsmCellLocation.getLac() == 0 ? false : gsmCellLocation.getLac() > 65535 ? false : gsmCellLocation.getCid() != 0)) {
                this.f = 9;
                t.a("case 2,gsm illegal");
                return;
            }
            this.f = 1;
            List<NeighboringCellInfo> list = null;
            this.j.add(b(cellLocation));
            if (this.i != null) {
                list = this.i.getNeighboringCellInfo();
            }
            if (list != null) {
                for (NeighboringCellInfo neighboringCellInfo : list) {
                    if (neighboringCellInfo.getCid() != -1) {
                        if ((neighboringCellInfo.getLac() == -1 ? false : neighboringCellInfo.getLac() == 0 ? false : neighboringCellInfo.getLac() > 65535 ? false : neighboringCellInfo.getCid() == -1 ? false : neighboringCellInfo.getCid() == 0 ? false : neighboringCellInfo.getCid() == 65535 ? false : neighboringCellInfo.getCid() < 268435455) && (a2 = a(neighboringCellInfo)) != null) {
                            this.j.add(a2);
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void d(int i2) {
        int i3 = 70254591;
        if (e()) {
            try {
                w();
                switch (i2) {
                    case 1:
                        i3 = 674234367;
                        break;
                    case 2:
                        if (m()) {
                            i3 = 2083520511;
                            break;
                        } else {
                            i3 = 674234367;
                            break;
                        }
                }
                this.D.a((ag) null, a(1, i3, 1));
                this.c = this.D.d();
                if (this.c != null) {
                    String a2 = this.A.a(this.c.a(), this.e);
                    if (e()) {
                        if (TextUtils.isEmpty(a2) || !a2.equals("true")) {
                            this.d++;
                            this.D.a(this.c, a(1, i3, 0));
                        } else {
                            this.D.a(this.c, a(1, i3, 1));
                        }
                    }
                }
                x();
                if (e() && this.D.f() == 0) {
                    v();
                } else if (this.d >= 3) {
                    v();
                }
            } catch (Throwable th) {
                th.printStackTrace();
                t.a(th);
            }
        }
    }

    private void d(CellLocation cellLocation) {
        this.j.clear();
        if (t.b() >= 5) {
            try {
                CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) cellLocation;
                if (cdmaCellLocation.getSystemId() <= 0) {
                    this.f = 9;
                    t.a("cdma illegal");
                } else if (cdmaCellLocation.getNetworkId() < 0) {
                    this.f = 9;
                    t.a("cdma illegal");
                } else if (cdmaCellLocation.getBaseStationId() < 0) {
                    this.f = 9;
                    t.a("cdma illegal");
                } else {
                    this.f = 2;
                    String[] a2 = t.a(this.i);
                    e eVar = new e();
                    eVar.f460a = a2[0];
                    eVar.f461b = a2[1];
                    eVar.g = cdmaCellLocation.getSystemId();
                    eVar.h = cdmaCellLocation.getNetworkId();
                    eVar.i = cdmaCellLocation.getBaseStationId();
                    eVar.j = this.p;
                    eVar.e = cdmaCellLocation.getBaseStationLatitude();
                    eVar.f = cdmaCellLocation.getBaseStationLongitude();
                    this.j.add(eVar);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    private void f() {
        this.h = (WifiManager) t.b(this.e, "wifi");
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        intentFilter.addAction("android.net.wifi.SCAN_RESULTS");
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.AIRPLANE_MODE");
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        this.e.registerReceiver(this.q, intentFilter);
        o();
    }

    private void g() {
        this.g = (ConnectivityManager) t.b(this.e, "connectivity");
        CellLocation.requestLocationUpdate();
        this.y = t.a();
        this.i = (TelephonyManager) t.b(this.e, "phone");
        switch (this.i != null ? this.i.getPhoneType() : 9) {
            case 1:
                this.f = 1;
                break;
            case 2:
                this.f = 2;
                break;
            default:
                this.f = 9;
                break;
        }
        this.o = new PhoneStateListener() {
            public void onCellLocationChanged(CellLocation cellLocation) {
                if (cellLocation != null) {
                    try {
                        if (!a.this.p()) {
                            CellLocation unused = a.this.H = cellLocation;
                            long unused2 = a.this.y = t.a();
                            long unused3 = a.this.x = t.a();
                        }
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                }
            }

            public void onServiceStateChanged(ServiceState serviceState) {
                try {
                    switch (serviceState.getState()) {
                        case 1:
                            a.this.j.clear();
                            int unused = a.this.p = -113;
                            return;
                        default:
                            return;
                    }
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }

            public void onSignalStrengthChanged(int i) {
                int i2 = -113;
                try {
                    switch (a.this.f) {
                        case 1:
                            i2 = t.a(i);
                            break;
                        case 2:
                            i2 = t.a(i);
                            break;
                    }
                    a.this.b(i2);
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }

            public void onSignalStrengthsChanged(SignalStrength signalStrength) {
                int i = -113;
                try {
                    switch (a.this.f) {
                        case 1:
                            i = t.a(signalStrength.getGsmSignalStrength());
                            break;
                        case 2:
                            i = signalStrength.getCdmaDbm();
                            break;
                    }
                    a.this.b(i);
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        };
        int i2 = t.b() < 7 ? 2 : 256;
        if (i2 != 0) {
            try {
                if (this.i != null) {
                    this.i.listen(this.o, i2 | 16);
                }
            } catch (SecurityException e2) {
                t.a(e2);
            }
        } else if (this.i != null) {
            this.i.listen(this.o, 16);
        }
    }

    private String h() {
        u();
        if (s()) {
            this.r = this.h.getConnectionInfo();
        } else {
            n();
        }
        switch (this.f) {
            case 1:
                if (this.j.size() > 0) {
                    e eVar = this.j.get(0);
                    StringBuilder sb = new StringBuilder();
                    sb.append(eVar.f460a).append("#");
                    sb.append(eVar.f461b).append("#");
                    sb.append(eVar.c).append("#");
                    sb.append(eVar.d).append("#");
                    sb.append(LocationManagerProxy.NETWORK_PROVIDER).append("#");
                    sb.append(this.k.size() > 0 ? "cellwifi" : "cell");
                    return sb.toString();
                }
                break;
            case 2:
                if (this.j.size() > 0) {
                    e eVar2 = this.j.get(0);
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(eVar2.f460a).append("#");
                    sb2.append(eVar2.f461b).append("#");
                    sb2.append(eVar2.g).append("#");
                    sb2.append(eVar2.h).append("#");
                    sb2.append(eVar2.i).append("#");
                    sb2.append(LocationManagerProxy.NETWORK_PROVIDER).append("#");
                    sb2.append(this.k.size() > 0 ? "cellwifi" : "cell");
                    return sb2.toString();
                }
                break;
            case 9:
                String format = String.format("#%s#", LocationManagerProxy.NETWORK_PROVIDER);
                if ((this.k.size() == 1 && !a(this.r)) || this.k.size() == 0) {
                    return null;
                }
                if (this.k.size() != 1 || !a(this.r)) {
                    return format + "wifi";
                }
                ScanResult scanResult = this.k.get(0);
                return (scanResult == null || !this.r.getBSSID().equals(scanResult.BSSID)) ? format : null;
        }
        return "";
    }

    private StringBuilder i() {
        u();
        StringBuilder sb = new StringBuilder((int) QMediaPlayer.MEDIA_INFO_VIDEO_TRACK_LAGGING);
        switch (this.f) {
            case 1:
                for (int i2 = 0; i2 < this.j.size(); i2++) {
                    if (i2 != 0) {
                        e eVar = this.j.get(i2);
                        sb.append("#").append(eVar.f461b);
                        sb.append("|").append(eVar.c);
                        sb.append("|").append(eVar.d);
                    }
                }
                break;
        }
        if (this.C == null || this.C.equals("00:00:00:00:00:00")) {
            if (this.r != null) {
                this.C = this.r.getMacAddress();
                if (this.C == null) {
                    this.C = "00:00:00:00:00:00";
                }
            } else if (this.h != null) {
                this.r = this.h.getConnectionInfo();
                if (this.r != null) {
                    this.C = this.r.getMacAddress();
                    if (this.C == null) {
                        this.C = "00:00:00:00:00:00";
                    }
                    this.r = null;
                }
            }
        }
        if (s()) {
            String bssid = a(this.r) ? this.r.getBSSID() : "";
            boolean z2 = false;
            for (int i3 = 0; i3 < this.k.size(); i3++) {
                ScanResult scanResult = this.k.get(i3);
                if (a(scanResult)) {
                    String str = scanResult.BSSID;
                    String str2 = "nb";
                    if (bssid.equals(str)) {
                        str2 = "access";
                        z2 = true;
                    }
                    sb.append(String.format("#%s,%s", str, str2));
                }
            }
            if (!z2 && bssid.length() > 0) {
                sb.append("#").append(bssid);
                sb.append(",access");
            }
        } else {
            n();
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(0);
        }
        return sb;
    }

    private synchronized byte[] j() {
        if (k()) {
            CellLocation.requestLocationUpdate();
            this.y = t.a();
        }
        if (l()) {
            o();
        }
        return a((Object) null);
    }

    private boolean k() {
        return !this.w && this.y != 0 && t.a() - this.y >= f.j;
    }

    private boolean l() {
        return s() && this.z != 0 && t.a() - this.z >= f.i;
    }

    /* access modifiers changed from: private */
    public boolean m() {
        if (this.h == null || !s()) {
            return false;
        }
        NetworkInfo networkInfo = null;
        try {
            if (this.g != null) {
                networkInfo = this.g.getActiveNetworkInfo();
            }
            return l.a(networkInfo) != -1 && a(this.h.getConnectionInfo());
        } catch (SecurityException e2) {
            return false;
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void n() {
        this.k.clear();
        this.r = null;
    }

    /* access modifiers changed from: private */
    public void o() {
        if (s()) {
            try {
                this.h.startScan();
                this.z = t.a();
            } catch (SecurityException e2) {
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean p() {
        return this.x != 0 && t.a() - this.x < 2000;
    }

    private void q() {
        if (this.u != null && this.l.size() >= 1) {
            Iterator<Map.Entry<PendingIntent, List<j>>> it = this.l.entrySet().iterator();
            while (it != null && it.hasNext()) {
                Map.Entry next = it.next();
                PendingIntent pendingIntent = (PendingIntent) next.getKey();
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                for (j jVar : (List) next.getValue()) {
                    long a2 = jVar.a();
                    if (a2 == -1 || a2 >= t.a()) {
                        float a3 = t.a(new double[]{jVar.f475b, jVar.f474a, this.u.f(), this.u.e()});
                        if (a3 < jVar.c) {
                            bundle.putFloat("distance", a3);
                            bundle.putString("fence", jVar.b());
                            intent.putExtras(bundle);
                            try {
                                pendingIntent.send(this.e, 0, intent);
                            } catch (Throwable th) {
                                th.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    }

    private void r() {
        switch (this.f) {
            case 1:
                if (this.j.size() == 0) {
                    this.f = 9;
                    return;
                }
                return;
            case 2:
                if (this.j.size() == 0) {
                    this.f = 9;
                    return;
                }
                return;
            default:
                return;
        }
    }

    private boolean s() {
        boolean z2 = false;
        if (this.h == null) {
            return false;
        }
        try {
            z2 = this.h.isWifiEnabled();
        } catch (Exception e2) {
        }
        if (z2 || t.b() <= 17) {
            return z2;
        }
        try {
            return String.valueOf(n.a(this.h, "isScanAlwaysAvailable", new Object[0])).equals("true");
        } catch (Exception e3) {
            return z2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.aps.a.a(byte[], boolean):com.aps.c
     arg types: [byte[], int]
     candidates:
      com.aps.a.a(com.aps.a, long):long
      com.aps.a.a(com.aps.a, android.telephony.CellLocation):android.telephony.CellLocation
      com.aps.a.a(com.aps.a, java.util.List):java.util.List
      com.aps.a.a(com.aps.a, int):void
      com.aps.a.a(com.aps.a, boolean):boolean
      com.aps.a.a(boolean, int):int
      com.aps.a.a(com.aps.j, android.app.PendingIntent):void
      com.aps.k.a(com.aps.j, android.app.PendingIntent):void
      com.aps.a.a(byte[], boolean):com.aps.c */
    private c t() {
        byte[] j2 = j();
        if (this.E == null || !this.E.toString().equals(this.t) || this.u == null) {
            return a(j2, false);
        }
        this.v = t.a();
        return this.u;
    }

    private void u() {
        if (this.w) {
            this.f = 9;
            this.j.clear();
            return;
        }
        r();
    }

    /* access modifiers changed from: private */
    public void v() {
        if (this.f397b != null) {
            this.f397b.cancel();
            this.f397b = null;
        }
        if (this.f396a != null) {
            this.f396a.cancel();
            this.f396a = null;
        }
    }

    private void w() {
        if (e()) {
            try {
                this.D.a(768);
            } catch (Throwable th) {
                th.printStackTrace();
                t.a(th);
            }
        }
    }

    private void x() {
        if (e() && this.D.f() <= 0) {
            try {
                if (this.D.e()) {
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public int a(boolean z2, int i2) {
        if (!z2) {
            v();
        } else {
            c(i2);
        }
        if (e()) {
            return this.D.f();
        }
        return -1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:62:0x011b, code lost:
        if ((com.aps.t.a() - r0.h()) > 3600000) goto L_0x011d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.aps.c a() {
        /*
            r10 = this;
            r2 = 0
            r1 = 1
            android.content.Context r0 = r10.e
            if (r0 != 0) goto L_0x000e
            com.amap.api.location.core.AMapLocException r0 = new com.amap.api.location.core.AMapLocException
            java.lang.String r1 = "未知的错误"
            r0.<init>(r1)
            throw r0
        L_0x000e:
            java.lang.String r0 = com.aps.f.d
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x001e
            com.amap.api.location.core.AMapLocException r0 = new com.amap.api.location.core.AMapLocException
            java.lang.String r1 = "key鉴权失败"
            r0.<init>(r1)
            throw r0
        L_0x001e:
            java.lang.String r0 = com.aps.f.e
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x002e
            com.amap.api.location.core.AMapLocException r0 = new com.amap.api.location.core.AMapLocException
            java.lang.String r1 = "key鉴权失败"
            r0.<init>(r1)
            throw r0
        L_0x002e:
            org.json.JSONObject r0 = r10.s
            java.lang.String[] r0 = com.aps.l.a(r0)
            java.lang.String r3 = "false"
            r0 = r0[r2]
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x004d
            java.lang.String r0 = "AuthLocation"
            java.lang.String r1 = "key鉴权失败"
            android.util.Log.e(r0, r1)
            com.amap.api.location.core.AMapLocException r0 = new com.amap.api.location.core.AMapLocException
            java.lang.String r1 = "key鉴权失败"
            r0.<init>(r1)
            throw r0
        L_0x004d:
            boolean r0 = r10.k()
            if (r0 == 0) goto L_0x005c
            android.telephony.CellLocation.requestLocationUpdate()
            long r4 = com.aps.t.a()
            r10.y = r4
        L_0x005c:
            boolean r0 = r10.l()
            if (r0 == 0) goto L_0x0065
            r10.o()
        L_0x0065:
            int r0 = r10.B
            int r0 = r0 + 1
            r10.B = r0
            int r0 = r10.B
            if (r0 <= r1) goto L_0x0072
            r10.c()
        L_0x0072:
            int r0 = r10.B
            if (r0 != r1) goto L_0x0090
            long r4 = java.lang.System.currentTimeMillis()
            r10.G = r4
            android.content.Context r0 = r10.e
            boolean r0 = com.aps.t.a(r0)
            r10.w = r0
            android.net.wifi.WifiManager r0 = r10.h
            if (r0 == 0) goto L_0x0090
            android.net.wifi.WifiManager r0 = r10.h
            java.util.List r0 = r0.getScanResults()
            r10.k = r0
        L_0x0090:
            java.util.List<android.net.wifi.ScanResult> r0 = r10.k
            if (r0 != 0) goto L_0x009b
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r10.k = r0
        L_0x009b:
            int r0 = r10.B
            if (r0 != r1) goto L_0x00c3
            boolean r0 = r10.s()
            if (r0 == 0) goto L_0x00c3
            long r4 = r10.G
            long r6 = r10.F
            long r4 = r4 - r6
            r6 = 2000(0x7d0, double:9.88E-321)
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 >= 0) goto L_0x00c3
            r0 = 4
        L_0x00b1:
            if (r0 <= 0) goto L_0x00c3
            java.util.List<android.net.wifi.ScanResult> r3 = r10.k
            int r3 = r3.size()
            if (r3 != 0) goto L_0x00c3
            r4 = 500(0x1f4, double:2.47E-321)
            android.os.SystemClock.sleep(r4)
            int r0 = r0 + -1
            goto L_0x00b1
        L_0x00c3:
            long r4 = r10.v
            boolean r0 = r10.a(r4)
            if (r0 == 0) goto L_0x00d8
            com.aps.c r0 = r10.u
            if (r0 == 0) goto L_0x00d8
            long r0 = com.aps.t.a()
            r10.v = r0
            com.aps.c r0 = r10.u
        L_0x00d7:
            return r0
        L_0x00d8:
            android.telephony.CellLocation r0 = r10.H     // Catch:{ Throwable -> 0x00f4 }
            r10.a(r0)     // Catch:{ Throwable -> 0x00f4 }
        L_0x00dd:
            java.util.List<android.net.wifi.ScanResult> r0 = r10.k
            r10.a(r0)
            java.lang.String r3 = r10.h()
            boolean r0 = android.text.TextUtils.isEmpty(r3)
            if (r0 == 0) goto L_0x00f9
            com.amap.api.location.core.AMapLocException r0 = new com.amap.api.location.core.AMapLocException
            java.lang.String r1 = "获取基站/WiFi信息为空或失败"
            r0.<init>(r1)
            throw r0
        L_0x00f4:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00dd
        L_0x00f9:
            java.lang.StringBuilder r4 = r10.i()
            r0 = 0
            android.content.Context r5 = r10.e     // Catch:{ Throwable -> 0x014f }
            com.aps.d r5 = com.aps.d.a(r5)     // Catch:{ Throwable -> 0x014f }
            java.lang.String r6 = "mem"
            com.aps.c r0 = r5.a(r3, r4, r6)     // Catch:{ Throwable -> 0x014f }
        L_0x010a:
            if (r0 == 0) goto L_0x0151
            long r6 = r0.h()
            long r8 = com.aps.t.a()
            long r6 = r8 - r6
            r8 = 3600000(0x36ee80, double:1.7786363E-317)
            int r5 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r5 <= 0) goto L_0x0151
        L_0x011d:
            if (r0 == 0) goto L_0x0121
            if (r1 == 0) goto L_0x014c
        L_0x0121:
            com.aps.c r0 = r10.t()     // Catch:{ AMapLocException -> 0x0148 }
        L_0x0125:
            r10.u = r0
        L_0x0127:
            android.content.Context r0 = r10.e
            com.aps.d r0 = com.aps.d.a(r0)
            com.aps.c r1 = r10.u
            r0.a(r3, r1, r4)
            int r0 = r4.length()
            r4.delete(r2, r0)
            long r0 = com.aps.t.a()
            r10.v = r0
            r10.q()
            r10.d()
            com.aps.c r0 = r10.u
            goto L_0x00d7
        L_0x0148:
            r1 = move-exception
            if (r0 != 0) goto L_0x0125
            throw r1
        L_0x014c:
            r10.u = r0
            goto L_0x0127
        L_0x014f:
            r5 = move-exception
            goto L_0x010a
        L_0x0151:
            r1 = r2
            goto L_0x011d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.aps.a.a():com.aps.c");
    }

    public void a(PendingIntent pendingIntent) {
        if (pendingIntent != null) {
            this.l.remove(pendingIntent);
        }
    }

    public void a(Context context) {
        if (context != null && this.e == null) {
            this.e = context.getApplicationContext();
            t.a(this.e, "in debug mode, only for test");
            f();
            g();
            this.F = System.currentTimeMillis();
        }
    }

    public void a(AMapLocation aMapLocation) {
        if (aMapLocation != null && this.m.size() >= 1) {
            Iterator<Map.Entry<PendingIntent, List<j>>> it = this.m.entrySet().iterator();
            while (it != null && it.hasNext()) {
                Map.Entry next = it.next();
                PendingIntent pendingIntent = (PendingIntent) next.getKey();
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                for (j jVar : (List) next.getValue()) {
                    long a2 = jVar.a();
                    if (a2 == -1 || a2 >= t.a()) {
                        float a3 = t.a(new double[]{jVar.f475b, jVar.f474a, aMapLocation.getLatitude(), aMapLocation.getLongitude()});
                        if (a3 >= jVar.c) {
                            if (jVar.d != 0) {
                                jVar.d = 0;
                            }
                        }
                        if (a3 < jVar.c) {
                            if (jVar.d != 1) {
                                jVar.d = 1;
                            }
                        }
                        bundle.putFloat("distance", a3);
                        bundle.putString("fence", jVar.b());
                        bundle.putInt("status", jVar.d);
                        intent.putExtras(bundle);
                        try {
                            pendingIntent.send(this.e, 0, intent);
                        } catch (Throwable th) {
                            th.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    public void a(j jVar, PendingIntent pendingIntent) {
        if (pendingIntent != null && jVar != null) {
            long a2 = jVar.a();
            if (a2 != -1 && a2 < t.a()) {
                return;
            }
            if (this.l.get(pendingIntent) != null) {
                List list = this.l.get(pendingIntent);
                list.add(jVar);
                this.l.put(pendingIntent, list);
                return;
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(jVar);
            this.l.put(pendingIntent, arrayList);
        }
    }

    public void a(String str) {
        if (str != null && str.indexOf("##") != -1) {
            String[] split = str.split("##");
            if (split.length == 3) {
                f.a(split[0]);
                if (!f.e.equals(split[1])) {
                    d.a(this.e).a();
                }
                f.b(split[1]);
                f.c(split[2]);
            }
        }
    }

    public void a(JSONObject jSONObject) {
        this.s = jSONObject;
    }

    public void b() {
        try {
            if (this.D != null) {
                this.D.c();
                this.I = false;
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        try {
            if (this.e != null) {
                this.e.unregisterReceiver(this.q);
            }
        } catch (Throwable th2) {
            this.q = null;
            throw th2;
        }
        this.q = null;
        v();
        try {
            if (!(this.i == null || this.o == null)) {
                this.i.listen(this.o, 0);
            }
        } catch (Throwable th3) {
            th3.printStackTrace();
            t.a(th3);
        }
        if (this.e != null) {
            d.a(this.e).a();
            d.a(this.e).b();
        }
        f.a(false);
        this.v = 0;
        this.j.clear();
        this.l.clear();
        this.m.clear();
        this.p = -113;
        n();
        this.t = null;
        this.u = null;
        this.e = null;
        this.i = null;
    }

    public void b(PendingIntent pendingIntent) {
        if (pendingIntent != null) {
            this.m.remove(pendingIntent);
        }
    }

    public void b(j jVar, PendingIntent pendingIntent) {
        if (pendingIntent != null && jVar != null) {
            long a2 = jVar.a();
            if (a2 != -1 && a2 < t.a()) {
                return;
            }
            if (this.m.get(pendingIntent) != null) {
                List list = this.m.get(pendingIntent);
                list.add(jVar);
                this.m.put(pendingIntent, list);
                return;
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(jVar);
            this.m.put(pendingIntent, arrayList);
        }
    }

    public void c() {
        try {
            if (this.D == null) {
                this.D = y.a(this.e);
                this.D.a(256);
            }
            if (!this.I) {
                this.I = true;
                this.D.a();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void d() {
        if (this.u != null && this.m.size() >= 1) {
            Iterator<Map.Entry<PendingIntent, List<j>>> it = this.m.entrySet().iterator();
            while (it != null && it.hasNext()) {
                Map.Entry next = it.next();
                PendingIntent pendingIntent = (PendingIntent) next.getKey();
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                for (j jVar : (List) next.getValue()) {
                    long a2 = jVar.a();
                    if (a2 == -1 || a2 >= t.a()) {
                        float a3 = t.a(new double[]{jVar.f475b, jVar.f474a, this.u.f(), this.u.e()});
                        if (a3 >= jVar.c) {
                            if (jVar.d != 0) {
                                jVar.d = 0;
                            }
                        }
                        if (a3 < jVar.c) {
                            if (jVar.d != 1) {
                                jVar.d = 1;
                            }
                        }
                        bundle.putFloat("distance", a3);
                        bundle.putString("fence", jVar.b());
                        bundle.putInt("status", jVar.d);
                        intent.putExtras(bundle);
                        try {
                            pendingIntent.send(this.e, 0, intent);
                        } catch (Throwable th) {
                            th.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return this.D != null;
    }
}
