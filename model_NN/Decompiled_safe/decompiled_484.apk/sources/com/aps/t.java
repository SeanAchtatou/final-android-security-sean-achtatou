package com.aps;

import android.content.ContentResolver;
import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.widget.Toast;
import com.amap.api.location.core.c;
import java.io.ByteArrayOutputStream;
import java.util.zip.GZIPOutputStream;
import org.apache.http.params.HttpParams;

public class t {
    private t() {
    }

    static float a(double[] dArr) {
        if (dArr.length != 4) {
            return 0.0f;
        }
        float[] fArr = new float[1];
        Location.distanceBetween(dArr[0], dArr[1], dArr[2], dArr[3], fArr);
        return fArr[0];
    }

    static int a(int i) {
        return (i * 2) - 113;
    }

    static int a(CellLocation cellLocation, Context context) {
        if (a(context)) {
            a("air plane mode on");
            return 9;
        } else if (cellLocation instanceof GsmCellLocation) {
            return 1;
        } else {
            try {
                Class.forName("android.telephony.cdma.CdmaCellLocation");
                return 2;
            } catch (Throwable th) {
                th.printStackTrace();
                a(th);
                return 9;
            }
        }
    }

    static long a() {
        return System.currentTimeMillis();
    }

    static void a(Context context, String str) {
        boolean z;
        if (str == null) {
            str = "null";
        }
        if (c.j().indexOf("test") != -1) {
            z = true;
        } else if (f.d.indexOf("test") != -1) {
            z = true;
        } else {
            char[] cArr = null;
            if (c.j().length() > 0) {
                cArr = c.j().substring(7, 8).toCharArray();
            }
            z = cArr == null || !Character.isLetter(cArr[0]);
        }
        if (z && context != null) {
            Toast.makeText(context, str, 0).show();
            a(str);
        }
    }

    public static void a(Throwable th) {
    }

    static void a(HttpParams httpParams, int i) {
        httpParams.setIntParameter("http.connection.timeout", i);
        httpParams.setIntParameter("http.socket.timeout", i);
        httpParams.setLongParameter("http.conn-manager.timeout", (long) i);
    }

    public static void a(Object... objArr) {
    }

    static boolean a(Context context) {
        boolean z = true;
        if (context == null) {
            return false;
        }
        ContentResolver contentResolver = context.getContentResolver();
        if (b() < 17) {
            try {
                if (Settings.System.getInt(contentResolver, "airplane_mode_on", 0) != 1) {
                    z = false;
                }
                return z;
            } catch (Throwable th) {
                th.printStackTrace();
                a(th);
                return false;
            }
        } else {
            try {
                if (Settings.Global.getInt(contentResolver, "airplane_mode_on", 0) != 1) {
                    z = false;
                }
                return z;
            } catch (Throwable th2) {
                th2.printStackTrace();
                a(th2);
                return false;
            }
        }
    }

    static boolean a(c cVar) {
        if (cVar == null || cVar.j().equals("5") || cVar.j().equals("6")) {
            return false;
        }
        return (cVar.e() == 0.0d && cVar.f() == 0.0d && ((double) cVar.g()) == 0.0d) ? false : true;
    }

    public static boolean a(String str) {
        if (!TextUtils.isEmpty(str) && TextUtils.isDigitsOnly(str)) {
            return ",111,123,134,199,202,204,206,208,212,213,214,216,218,219,220,222,225,226,228,230,231,232,234,235,238,240,242,244,246,247,248,250,255,257,259,260,262,266,268,270,272,274,276,278,280,282,283,284,286,288,289,290,292,293,294,295,297,302,308,310,311,312,313,314,315,316,310,330,332,334,338,340,342,344,346,348,350,352,354,356,358,360,362,363,364,365,366,368,370,372,374,376,400,401,402,404,405,406,410,412,413,414,415,416,417,418,419,420,421,422,424,425,426,427,428,429,430,431,432,434,436,437,438,440,441,450,452,454,455,456,457,466,467,470,472,502,505,510,514,515,520,525,528,530,534,535,536,537,539,540,541,542,543,544,545,546,547,548,549,550,551,552,553,555,560,598,602,603,604,605,606,607,608,609,610,611,612,613,614,615,616,617,618,619,620,621,622,623,624,625,626,627,628,629,630,631,632,633,634,635,636,637,638,639,640,641,642,643,645,646,647,648,649,650,651,652,653,654,655,657,659,665,702,704,706,708,710,712,714,716,722,724,730,732,734,736,738,740,742,744,746,748,750,850,901,".contains("," + str + ",");
        }
        return false;
    }

    public static byte[] a(byte[] bArr) {
        Throwable th;
        byte[] bArr2;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gZIPOutputStream.write(bArr);
            gZIPOutputStream.close();
            bArr2 = byteArrayOutputStream.toByteArray();
            try {
                byteArrayOutputStream.close();
            } catch (Throwable th2) {
                th = th2;
                th.printStackTrace();
                return bArr2;
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            bArr2 = null;
            th = th4;
        }
        return bArr2;
    }

    public static String[] a(TelephonyManager telephonyManager) {
        boolean z = true;
        int i = 0;
        String[] strArr = {"0", "0"};
        String str = null;
        if (telephonyManager != null) {
            try {
                str = telephonyManager.getNetworkOperator();
            } catch (Exception e) {
            }
        }
        if (TextUtils.isEmpty(str)) {
            z = false;
        } else if (!TextUtils.isDigitsOnly(str)) {
            z = false;
        } else if (str.length() <= 4) {
            z = false;
        }
        if (z) {
            strArr[0] = str.substring(0, 3);
            char[] charArray = str.substring(3).toCharArray();
            int i2 = 0;
            while (i2 < charArray.length && Character.isDigit(charArray[i2])) {
                i2++;
            }
            strArr[1] = str.substring(3, i2 + 3);
        }
        try {
            i = Integer.parseInt(strArr[0]);
        } catch (Exception e2) {
        }
        if (i == 0) {
            strArr[0] = "0";
        }
        return strArr;
    }

    static int b() {
        try {
            return Build.VERSION.SDK_INT;
        } catch (Throwable th) {
            th.printStackTrace();
            a(th);
            return 0;
        }
    }

    static NetworkInfo b(Context context) {
        NetworkInfo networkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) b(context, "connectivity");
        if (connectivityManager != null) {
            try {
                networkInfo = connectivityManager.getActiveNetworkInfo();
            } catch (SecurityException e) {
                return null;
            }
        } else {
            networkInfo = null;
        }
        return networkInfo;
    }

    static Object b(Context context, String str) {
        if (context == null) {
            return null;
        }
        return context.getApplicationContext().getSystemService(str);
    }
}
