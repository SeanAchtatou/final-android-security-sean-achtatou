package com.aps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import com.amap.api.location.LocationManagerProxy;
import com.qihoo.messenger.util.QDefine;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class y {
    private static float P = 1.1f;
    private static float Q = 2.2f;
    private static float R = 2.3f;
    private static float S = 3.8f;
    private static int T = 3;
    private static int U = 10;
    private static int V = 2;
    private static int W = 7;
    /* access modifiers changed from: private */
    public static int X = 20;
    private static int Y = 70;
    private static int Z = 120;

    /* renamed from: a  reason: collision with root package name */
    protected static boolean f500a = false;

    /* renamed from: b  reason: collision with root package name */
    protected static boolean f501b = true;
    /* access modifiers changed from: private */
    public static int c = 10;
    /* access modifiers changed from: private */
    public static int d = 2;
    /* access modifiers changed from: private */
    public static int e = 10;
    /* access modifiers changed from: private */
    public static int f = 10;
    /* access modifiers changed from: private */
    public static int g = 50;
    /* access modifiers changed from: private */
    public static int h = 200;
    private static Object i = new Object();
    private static y j;
    private Thread A = null;
    /* access modifiers changed from: private */
    public Looper B = null;
    private av C = null;
    /* access modifiers changed from: private */
    public Location D = null;
    /* access modifiers changed from: private */
    public au E = null;
    /* access modifiers changed from: private */
    public Handler F = null;
    private aw G = new aw(this);
    /* access modifiers changed from: private */
    public LocationListener H = new aq(this);
    private BroadcastReceiver I = new ar(this);
    /* access modifiers changed from: private */
    public GpsStatus J = null;
    /* access modifiers changed from: private */
    public int K = 0;
    /* access modifiers changed from: private */
    public int L = 0;
    /* access modifiers changed from: private */
    public HashMap M = null;
    /* access modifiers changed from: private */
    public int N = 0;
    /* access modifiers changed from: private */
    public int O = 0;
    /* access modifiers changed from: private */
    public boolean k = false;
    private boolean l = false;
    /* access modifiers changed from: private */
    public int m = -1;
    private int n = 0;
    private int o = 0;
    /* access modifiers changed from: private */
    public int p = 10000;
    /* access modifiers changed from: private */
    public long q = 0;
    private Context r;
    /* access modifiers changed from: private */
    public LocationManager s;
    /* access modifiers changed from: private */
    public ak t;
    private ay u;
    private bf v;
    private ah w;
    private be x;
    /* access modifiers changed from: private */
    public ax y;
    private ab z;

    private y(Context context) {
        this.r = context;
        this.t = ak.a(context);
        this.z = new ab();
        this.u = new ay(this.t);
        this.w = new ah(context);
        this.v = new bf(this.w);
        this.x = new be(this.w);
        this.s = (LocationManager) this.r.getSystemService(LocationManagerProxy.KEY_LOCATION_CHANGED);
        this.y = ax.a(this.r);
        this.y.a(this.G);
        n();
        List<String> allProviders = this.s.getAllProviders();
        this.l = allProviders != null && allProviders.contains(LocationManagerProxy.GPS_PROVIDER) && allProviders.contains("passive");
        bg.a(context);
    }

    static /* synthetic */ int a(y yVar, w wVar, int i2) {
        if (yVar.N >= U) {
            return 1;
        }
        if (yVar.N <= T) {
            return 4;
        }
        double c2 = wVar.c();
        if (c2 <= ((double) P)) {
            return 1;
        }
        if (c2 >= ((double) Q)) {
            return 4;
        }
        double b2 = wVar.b();
        if (b2 <= ((double) R)) {
            return 1;
        }
        if (b2 >= ((double) S)) {
            return 4;
        }
        if (i2 >= W) {
            return 1;
        }
        if (i2 <= V) {
            return 4;
        }
        if (yVar.M != null) {
            return yVar.a(yVar.M);
        }
        return 3;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v34, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: double[]} */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int a(java.util.HashMap r13) {
        /*
            r12 = this;
            int r0 = r12.K
            r1 = 4
            if (r0 <= r1) goto L_0x013d
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            r0 = 0
            java.util.Set r1 = r13.entrySet()
            java.util.Iterator r2 = r1.iterator()
            r1 = r0
        L_0x0019:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0041
            java.lang.Object r0 = r2.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r0 = r0.getValue()
            java.util.List r0 = (java.util.List) r0
            if (r0 == 0) goto L_0x013f
            double[] r0 = r12.a(r0)
            if (r0 == 0) goto L_0x013f
            r3.add(r0)
            int r0 = r1 + 1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r4.add(r1)
        L_0x003f:
            r1 = r0
            goto L_0x0019
        L_0x0041:
            boolean r0 = r3.isEmpty()
            if (r0 != 0) goto L_0x013d
            r0 = 2
            double[] r5 = new double[r0]
            int r6 = r3.size()
            r0 = 0
            r2 = r0
        L_0x0050:
            if (r2 >= r6) goto L_0x0087
            java.lang.Object r0 = r3.get(r2)
            r1 = r0
            double[] r1 = (double[]) r1
            java.lang.Object r0 = r4.get(r2)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            r7 = 0
            r8 = r1[r7]
            double r10 = (double) r0
            double r8 = r8 * r10
            r1[r7] = r8
            r7 = 1
            r8 = r1[r7]
            double r10 = (double) r0
            double r8 = r8 * r10
            r1[r7] = r8
            r0 = 0
            r8 = r5[r0]
            r7 = 0
            r10 = r1[r7]
            double r8 = r8 + r10
            r5[r0] = r8
            r0 = 1
            r8 = r5[r0]
            r7 = 1
            r10 = r1[r7]
            double r8 = r8 + r10
            r5[r0] = r8
            int r0 = r2 + 1
            r2 = r0
            goto L_0x0050
        L_0x0087:
            r0 = 0
            r2 = r5[r0]
            double r8 = (double) r6
            double r2 = r2 / r8
            r5[r0] = r2
            r0 = 1
            r2 = r5[r0]
            double r6 = (double) r6
            double r2 = r2 / r6
            r5[r0] = r2
            r0 = 0
            r2 = r5[r0]
            r0 = 1
            r6 = r5[r0]
            r0 = 0
            int r0 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r0 != 0) goto L_0x0125
            r0 = 0
            int r0 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x0116
            r0 = 4636033603912859648(0x4056800000000000, double:90.0)
        L_0x00ac:
            r4 = 2
            double[] r4 = new double[r4]
            r8 = 0
            double r2 = r2 * r2
            double r6 = r6 * r6
            double r2 = r2 + r6
            double r2 = java.lang.Math.sqrt(r2)
            r4[r8] = r2
            r2 = 1
            r4[r2] = r0
            java.util.Locale r0 = java.util.Locale.CHINA
            java.lang.String r1 = "%d,%d,%d,%d"
            r2 = 4
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            r6 = 0
            r6 = r5[r6]
            r8 = 4636737291354636288(0x4059000000000000, double:100.0)
            double r6 = r6 * r8
            long r6 = java.lang.Math.round(r6)
            java.lang.Long r6 = java.lang.Long.valueOf(r6)
            r2[r3] = r6
            r3 = 1
            r6 = 1
            r6 = r5[r6]
            r8 = 4636737291354636288(0x4059000000000000, double:100.0)
            double r6 = r6 * r8
            long r6 = java.lang.Math.round(r6)
            java.lang.Long r5 = java.lang.Long.valueOf(r6)
            r2[r3] = r5
            r3 = 2
            r5 = 0
            r6 = r4[r5]
            r8 = 4636737291354636288(0x4059000000000000, double:100.0)
            double r6 = r6 * r8
            long r6 = java.lang.Math.round(r6)
            java.lang.Long r5 = java.lang.Long.valueOf(r6)
            r2[r3] = r5
            r3 = 3
            r5 = 1
            r6 = r4[r5]
            r8 = 4636737291354636288(0x4059000000000000, double:100.0)
            double r6 = r6 * r8
            long r6 = java.lang.Math.round(r6)
            java.lang.Long r5 = java.lang.Long.valueOf(r6)
            r2[r3] = r5
            java.lang.String.format(r0, r1, r2)
            r0 = 0
            r0 = r4[r0]
            int r2 = com.aps.y.Y
            double r2 = (double) r2
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 > 0) goto L_0x0131
            r0 = 1
        L_0x0115:
            return r0
        L_0x0116:
            r0 = 0
            int r0 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x0122
            r0 = 4643457506423603200(0x4070e00000000000, double:270.0)
            goto L_0x00ac
        L_0x0122:
            r0 = 0
            goto L_0x00ac
        L_0x0125:
            double r0 = r2 / r6
            double r0 = java.lang.Math.atan(r0)
            double r0 = java.lang.Math.toDegrees(r0)
            goto L_0x00ac
        L_0x0131:
            r0 = 0
            r0 = r4[r0]
            int r2 = com.aps.y.Z
            double r2 = (double) r2
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 < 0) goto L_0x013d
            r0 = 4
            goto L_0x0115
        L_0x013d:
            r0 = 3
            goto L_0x0115
        L_0x013f:
            r0 = r1
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.aps.y.a(java.util.HashMap):int");
    }

    public static y a(Context context) {
        if (j == null) {
            synchronized (i) {
                if (j == null) {
                    j = new y(context);
                }
            }
        }
        return j;
    }

    public static String a(String str) {
        if (str.equals("version")) {
            return "COL.14.1126r";
        }
        return null;
    }

    static /* synthetic */ void a(y yVar, Location location, int i2, long j2) {
        boolean z2;
        boolean z3;
        Location location2;
        aa aaVar;
        aa aaVar2;
        System.currentTimeMillis();
        boolean a2 = yVar.u.a(location);
        if (a2) {
            yVar.u.f437b.f439b = new Location(location);
        }
        boolean b2 = yVar.u.b(location);
        if (b2) {
            yVar.u.f436a.f446b = new Location(location);
        }
        int i3 = 0;
        if (i2 == 1) {
            z2 = true;
            z3 = true;
            location2 = yVar.D;
        } else if (i2 == 2) {
            z2 = false;
            z3 = true;
            location2 = yVar.D;
        } else {
            z2 = a2;
            z3 = b2;
            location2 = location;
        }
        if (z2) {
            i3 = 1;
            if (z3) {
                i3 = 3;
            }
        } else if (z3) {
            i3 = 2;
        }
        try {
            ab abVar = yVar.z;
            aaVar = ab.a(location2, yVar.t, i3, (byte) yVar.O, j2, false);
        } catch (Exception e2) {
            aaVar = null;
        }
        if (!(aaVar == null || yVar.t == null)) {
            List n2 = yVar.t.n();
            Long l2 = 0L;
            if (n2 != null && n2.size() > 0) {
                l2 = (Long) n2.get(0);
            }
            yVar.v.a(l2.longValue(), aaVar.a());
        }
        if (yVar.r != null && yVar.z != null) {
            SharedPreferences sharedPreferences = yVar.r.getSharedPreferences("app_pref", 0);
            if (!sharedPreferences.getString("get_sensor", "").equals("true")) {
                try {
                    ab abVar2 = yVar.z;
                    aaVar2 = ab.a(null, yVar.t, i3, (byte) yVar.O, j2, true);
                } catch (Exception e3) {
                    aaVar2 = null;
                }
                if (aaVar2 != null && yVar.t != null) {
                    List n3 = yVar.t.n();
                    Long l3 = 0L;
                    if (n3 != null && n3.size() > 0) {
                        l3 = (Long) n3.get(0);
                    }
                    yVar.v.a(l3.longValue(), aaVar2.a());
                    sharedPreferences.edit().putString("get_sensor", "true").commit();
                }
            }
        }
    }

    private double[] a(List list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        double[] dArr = new double[2];
        Iterator it = list.iterator();
        while (it.hasNext()) {
            GpsSatellite gpsSatellite = (GpsSatellite) it.next();
            if (gpsSatellite != null) {
                double elevation = (double) (90.0f - gpsSatellite.getElevation());
                double azimuth = (double) gpsSatellite.getAzimuth();
                double[] dArr2 = {Math.sin(Math.toRadians(azimuth)) * elevation, elevation * Math.cos(Math.toRadians(azimuth))};
                dArr[0] = dArr[0] + dArr2[0];
                dArr[1] = dArr[1] + dArr2[1];
            }
        }
        int size = list.size();
        dArr[0] = dArr[0] / ((double) size);
        dArr[1] = dArr[1] / ((double) size);
        return dArr;
    }

    static /* synthetic */ String b(y yVar, String str) {
        return str;
    }

    static /* synthetic */ int j(y yVar) {
        int i2 = yVar.L;
        yVar.L = i2 + 1;
        return i2;
    }

    /* access modifiers changed from: private */
    public void n() {
        this.n = this.y.b() * QDefine.ONE_SECOND;
        this.o = this.y.c();
        ay ayVar = this.u;
        int i2 = this.n;
        int i3 = this.o;
        ay.a();
    }

    public void a() {
        bd.f447a = true;
        if (this.l && this.t != null && !f500a) {
            IntentFilter intentFilter = new IntentFilter("android.location.GPS_ENABLED_CHANGE");
            intentFilter.addAction("android.location.GPS_FIX_CHANGE");
            f501b = true;
            this.r.registerReceiver(this.I, intentFilter);
            this.s.removeUpdates(this.H);
            if (this.B != null) {
                this.B.quit();
                this.B = null;
            }
            if (this.A != null) {
                this.A.interrupt();
                this.A = null;
            }
            this.A = new as(this, "");
            this.A.start();
            this.t.a();
            f500a = true;
        }
    }

    public void a(int i2) {
        if (i2 == 256 || i2 == 8736 || i2 == 768) {
            this.w.a(i2);
            return;
        }
        throw new RuntimeException("invalid Size! must be COLLECTOR_SMALL_SIZE or COLLECTOR_BIG_SIZE or COLLECTOR_MEDIUM_SIZE");
    }

    public void a(ag agVar, String str) {
        NetworkInfo activeNetworkInfo;
        boolean a2 = this.y.a(str);
        if (agVar != null) {
            byte[] a3 = agVar.a();
            if (a2 && a3 != null && (activeNetworkInfo = ((ConnectivityManager) this.r.getSystemService("connectivity")).getActiveNetworkInfo()) != null && activeNetworkInfo.isConnected()) {
                if (activeNetworkInfo.getType() == 1) {
                    this.y.a(a3.length + this.y.e());
                } else {
                    this.y.b(a3.length + this.y.f());
                }
            }
            agVar.a(a2);
            this.x.a(agVar);
        }
    }

    public void b() {
        bd.f447a = false;
        if (this.l && this.t != null && f500a) {
            if (this.I != null) {
                try {
                    this.r.unregisterReceiver(this.I);
                } catch (Exception e2) {
                }
            }
            if (this.t != null) {
                this.t.w();
            }
            this.s.removeGpsStatusListener(this.E);
            this.s.removeNmeaListener(this.E);
            this.E = null;
            this.s.removeUpdates(this.H);
            if (this.B != null) {
                this.B.quit();
                this.B = null;
            }
            if (this.A != null) {
                this.A.interrupt();
                this.A = null;
            }
            if (this.C != null) {
                this.k = false;
                this.C.interrupt();
                this.C = null;
            }
            this.t.b();
            f500a = false;
        }
    }

    public void c() {
        if (this.l) {
            b();
        }
    }

    public ag d() {
        if (this.x == null) {
            return null;
        }
        e();
        if (this.y.a()) {
            return this.x.a(this.y.d());
        }
        return null;
    }

    public boolean e() {
        List n2;
        if (this.t == null || (n2 = this.t.n()) == null || n2.size() <= 0) {
            return false;
        }
        return this.w.b(((Long) n2.get(0)).longValue());
    }

    public int f() {
        if (this.x != null) {
            return this.x.a();
        }
        return 0;
    }
}
