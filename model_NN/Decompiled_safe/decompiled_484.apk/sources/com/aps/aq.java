package com.aps;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import com.amap.api.location.LocationManagerProxy;
import java.text.SimpleDateFormat;

final class aq implements LocationListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ y f427a;

    aq(y yVar) {
        this.f427a = yVar;
    }

    private static boolean a(Location location) {
        return location != null && LocationManagerProxy.GPS_PROVIDER.equalsIgnoreCase(location.getProvider()) && location.getLatitude() > -90.0d && location.getLatitude() < 90.0d && location.getLongitude() > -180.0d && location.getLongitude() < 180.0d;
    }

    public final void onLocationChanged(Location location) {
        try {
            long time = location.getTime();
            long currentTimeMillis = System.currentTimeMillis();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            simpleDateFormat.format(Long.valueOf(time));
            simpleDateFormat.format(Long.valueOf(currentTimeMillis));
            if (time > 0) {
                currentTimeMillis = time;
            }
            if (location != null && a(location)) {
                if (location.getSpeed() > ((float) y.e)) {
                    ay.a(y.h);
                    ay.b(y.h * 10);
                } else if (location.getSpeed() > ((float) y.d)) {
                    ay.a(y.g);
                    ay.b(y.g * 10);
                } else {
                    ay.a(y.f);
                    ay.b(y.f * 10);
                }
                this.f427a.y.a();
                a(location);
                if (this.f427a.y.a() && a(location)) {
                    location.setTime(System.currentTimeMillis());
                    long unused = this.f427a.q = System.currentTimeMillis();
                    Location unused2 = this.f427a.D = location;
                    if (!this.f427a.k) {
                        y.a(this.f427a, location, 0, currentTimeMillis);
                    } else {
                        bg.a("collector");
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
