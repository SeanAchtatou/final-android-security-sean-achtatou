package com.aps;

import com.qihoo360.daily.activity.SplashActivity;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.BitSet;

public final class bf {

    /* renamed from: a  reason: collision with root package name */
    private RandomAccessFile f451a;

    /* renamed from: b  reason: collision with root package name */
    private ah f452b;
    private String c = "";
    private File d = null;

    protected bf(ah ahVar) {
        this.f452b = ahVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.BitSet.set(int, boolean):void}
     arg types: [int, int]
     candidates:
      ClspMth{java.util.BitSet.set(int, int):void}
      ClspMth{java.util.BitSet.set(int, boolean):void} */
    /* access modifiers changed from: protected */
    public final synchronized void a(long j, byte[] bArr) {
        int i = 0;
        synchronized (this) {
            this.d = this.f452b.a(j);
            if (this.d != null) {
                try {
                    this.f451a = new RandomAccessFile(this.d, "rw");
                    byte[] bArr2 = new byte[this.f452b.a()];
                    int readInt = this.f451a.read(bArr2) == -1 ? 0 : this.f451a.readInt();
                    BitSet b2 = ah.b(bArr2);
                    int a2 = this.f452b.a() + 4 + (readInt * SplashActivity.SPLASH_TYPE_OP_TIME);
                    if (readInt < 0 || readInt > (this.f452b.a() << 3)) {
                        this.f451a.close();
                        this.d.delete();
                        if (this.f451a != null) {
                            try {
                                this.f451a.close();
                            } catch (IOException e) {
                            }
                        }
                    } else {
                        this.f451a.seek((long) a2);
                        byte[] a3 = ah.a(bArr);
                        this.f451a.writeInt(a3.length);
                        this.f451a.writeLong(j);
                        this.f451a.write(a3);
                        b2.set(readInt, true);
                        this.f451a.seek(0);
                        this.f451a.write(ah.a(b2));
                        int i2 = readInt + 1;
                        if (i2 != (this.f452b.a() << 3)) {
                            i = i2;
                        }
                        this.f451a.writeInt(i);
                        if (!this.c.equalsIgnoreCase(this.d.getName())) {
                            this.c = this.d.getName();
                        }
                        this.d.length();
                        if (this.f451a != null) {
                            try {
                                this.f451a.close();
                            } catch (IOException e2) {
                            }
                        }
                        this.d = null;
                    }
                } catch (FileNotFoundException e3) {
                    if (this.f451a != null) {
                        try {
                            this.f451a.close();
                        } catch (IOException e4) {
                        }
                    }
                } catch (IOException e5) {
                    if (this.f451a != null) {
                        try {
                            this.f451a.close();
                        } catch (IOException e6) {
                        }
                    }
                } catch (Throwable th) {
                    if (this.f451a != null) {
                        try {
                            this.f451a.close();
                        } catch (IOException e7) {
                        }
                    }
                    throw th;
                }
            }
        }
        return;
    }
}
