package com.aps;

import android.net.NetworkInfo;
import android.net.Proxy;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.amap.api.location.core.c;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;

public class l {

    /* renamed from: a  reason: collision with root package name */
    private static l f476a = null;

    private l() {
    }

    public static int a(NetworkInfo networkInfo) {
        if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
            return networkInfo.getType();
        }
        return -1;
    }

    public static l a() {
        if (f476a == null) {
            f476a = new l();
        }
        return f476a;
    }

    public static String a(TelephonyManager telephonyManager) {
        int i = 0;
        if (telephonyManager != null) {
            i = telephonyManager.getNetworkType();
        }
        return f.l.get(i, "UNKNOWN");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.aps.t.a(org.apache.http.params.HttpParams, int):void
     arg types: [org.apache.http.params.BasicHttpParams, int]
     candidates:
      com.aps.t.a(android.telephony.CellLocation, android.content.Context):int
      com.aps.t.a(android.content.Context, java.lang.String):void
      com.aps.t.a(org.apache.http.params.HttpParams, int):void */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0131  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x013c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.apache.http.client.HttpClient a(android.content.Context r12, android.net.NetworkInfo r13) {
        /*
            r6 = -1
            r8 = 80
            r9 = 1
            r10 = 0
            r7 = 0
            org.apache.http.params.BasicHttpParams r11 = new org.apache.http.params.BasicHttpParams
            r11.<init>()
            int r0 = r13.getType()
            if (r0 != 0) goto L_0x0165
            java.lang.String r0 = "content://telephony/carriers/preferapn"
            android.net.Uri r1 = android.net.Uri.parse(r0)
            android.content.ContentResolver r0 = r12.getContentResolver()
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ SecurityException -> 0x00cf, Exception -> 0x0128, all -> 0x0138 }
            if (r2 == 0) goto L_0x015d
            boolean r0 = r2.moveToFirst()     // Catch:{ SecurityException -> 0x014b, Exception -> 0x0145 }
            if (r0 == 0) goto L_0x015d
            java.lang.String r0 = "apn"
            int r0 = r2.getColumnIndex(r0)     // Catch:{ SecurityException -> 0x014b, Exception -> 0x0145 }
            java.lang.String r0 = r2.getString(r0)     // Catch:{ SecurityException -> 0x014b, Exception -> 0x0145 }
            if (r0 == 0) goto L_0x004b
            java.util.Locale r1 = java.util.Locale.US     // Catch:{ SecurityException -> 0x014b, Exception -> 0x0145 }
            java.lang.String r0 = r0.toLowerCase(r1)     // Catch:{ SecurityException -> 0x014b, Exception -> 0x0145 }
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ SecurityException -> 0x014b, Exception -> 0x0145 }
            r3 = 0
            java.lang.String r4 = "nm|found apn:"
            r1[r3] = r4     // Catch:{ SecurityException -> 0x014b, Exception -> 0x0145 }
            r3 = 1
            r1[r3] = r0     // Catch:{ SecurityException -> 0x014b, Exception -> 0x0145 }
            com.aps.t.a(r1)     // Catch:{ SecurityException -> 0x014b, Exception -> 0x0145 }
        L_0x004b:
            if (r0 == 0) goto L_0x00ab
            java.lang.String r1 = "ctwap"
            boolean r1 = r0.contains(r1)     // Catch:{ SecurityException -> 0x014b, Exception -> 0x0145 }
            if (r1 == 0) goto L_0x00ab
            java.lang.String r0 = b()     // Catch:{ SecurityException -> 0x014b, Exception -> 0x0145 }
            boolean r1 = android.text.TextUtils.isEmpty(r0)     // Catch:{ SecurityException -> 0x014b, Exception -> 0x0145 }
            if (r1 != 0) goto L_0x0161
            java.lang.String r1 = "null"
            boolean r1 = r0.equals(r1)     // Catch:{ SecurityException -> 0x014b, Exception -> 0x0145 }
            if (r1 != 0) goto L_0x0161
            r1 = r9
        L_0x0068:
            if (r1 != 0) goto L_0x006c
            java.lang.String r0 = "10.0.0.200"
        L_0x006c:
            r1 = r0
            r0 = r8
        L_0x006e:
            if (r2 == 0) goto L_0x0073
            r2.close()
        L_0x0073:
            boolean r2 = a(r1, r0)
            if (r2 == 0) goto L_0x0085
            java.lang.String r2 = "http"
            org.apache.http.HttpHost r3 = new org.apache.http.HttpHost
            r3.<init>(r1, r0, r2)
            java.lang.String r0 = "http.route.default-proxy"
            r11.setParameter(r0, r3)
        L_0x0085:
            r0 = 30000(0x7530, float:4.2039E-41)
            com.aps.t.a(r11, r0)
            org.apache.http.params.HttpProtocolParams.setUseExpectContinue(r11, r10)
            org.apache.http.conn.scheme.SchemeRegistry r0 = new org.apache.http.conn.scheme.SchemeRegistry
            r0.<init>()
            org.apache.http.conn.scheme.PlainSocketFactory r1 = org.apache.http.conn.scheme.PlainSocketFactory.getSocketFactory()
            org.apache.http.conn.scheme.Scheme r2 = new org.apache.http.conn.scheme.Scheme
            java.lang.String r3 = "http"
            r2.<init>(r3, r1, r8)
            r0.register(r2)
            org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager r1 = new org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager
            r1.<init>(r11, r0)
            org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient
            r0.<init>(r1, r11)
            return r0
        L_0x00ab:
            if (r0 == 0) goto L_0x015d
            java.lang.String r1 = "wap"
            boolean r0 = r0.contains(r1)     // Catch:{ SecurityException -> 0x014b, Exception -> 0x0145 }
            if (r0 == 0) goto L_0x015d
            java.lang.String r0 = b()     // Catch:{ SecurityException -> 0x014b, Exception -> 0x0145 }
            boolean r1 = android.text.TextUtils.isEmpty(r0)     // Catch:{ SecurityException -> 0x014b, Exception -> 0x0145 }
            if (r1 != 0) goto L_0x0159
            java.lang.String r1 = "null"
            boolean r1 = r0.equals(r1)     // Catch:{ SecurityException -> 0x014b, Exception -> 0x0145 }
            if (r1 != 0) goto L_0x0159
            r1 = r9
        L_0x00c8:
            if (r1 != 0) goto L_0x00cc
            java.lang.String r0 = "10.0.0.172"
        L_0x00cc:
            r1 = r0
            r0 = r8
            goto L_0x006e
        L_0x00cf:
            r0 = move-exception
            r0 = r7
        L_0x00d1:
            java.lang.String r1 = r13.getExtraInfo()     // Catch:{ all -> 0x0142 }
            if (r1 == 0) goto L_0x0154
            java.lang.String r1 = r13.getExtraInfo()     // Catch:{ all -> 0x0142 }
            java.util.Locale r2 = java.util.Locale.US     // Catch:{ all -> 0x0142 }
            java.lang.String r2 = r1.toLowerCase(r2)     // Catch:{ all -> 0x0142 }
            java.lang.String r1 = b()     // Catch:{ all -> 0x0142 }
            java.lang.String r3 = "ctwap"
            int r3 = r2.indexOf(r3)     // Catch:{ all -> 0x0142 }
            if (r3 == r6) goto L_0x0109
            boolean r2 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x0142 }
            if (r2 != 0) goto L_0x0157
            java.lang.String r2 = "null"
            boolean r2 = r1.equals(r2)     // Catch:{ all -> 0x0142 }
            if (r2 != 0) goto L_0x0157
            r0 = r1
        L_0x00fc:
            if (r9 != 0) goto L_0x0100
            java.lang.String r0 = "10.0.0.200"
        L_0x0100:
            r1 = r0
            r0 = r8
        L_0x0102:
            if (r7 == 0) goto L_0x0073
            r7.close()
            goto L_0x0073
        L_0x0109:
            java.lang.String r3 = "wap"
            int r2 = r2.indexOf(r3)     // Catch:{ all -> 0x0142 }
            if (r2 == r6) goto L_0x0154
            boolean r2 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x0142 }
            if (r2 != 0) goto L_0x0152
            java.lang.String r2 = "null"
            boolean r2 = r1.equals(r2)     // Catch:{ all -> 0x0142 }
            if (r2 != 0) goto L_0x0152
            r0 = r1
            r1 = r9
        L_0x0121:
            if (r1 != 0) goto L_0x0125
            java.lang.String r0 = "10.0.0.200"
        L_0x0125:
            r1 = r0
            r0 = r8
            goto L_0x0102
        L_0x0128:
            r0 = move-exception
            r1 = r0
            r2 = r7
            r0 = r7
        L_0x012c:
            com.aps.t.a(r1)     // Catch:{ all -> 0x0140 }
            if (r2 == 0) goto L_0x0134
            r2.close()
        L_0x0134:
            r1 = r0
            r0 = r6
            goto L_0x0073
        L_0x0138:
            r0 = move-exception
            r2 = r7
        L_0x013a:
            if (r2 == 0) goto L_0x013f
            r2.close()
        L_0x013f:
            throw r0
        L_0x0140:
            r0 = move-exception
            goto L_0x013a
        L_0x0142:
            r0 = move-exception
            r2 = r7
            goto L_0x013a
        L_0x0145:
            r0 = move-exception
            r1 = r0
            r0 = r7
            goto L_0x012c
        L_0x0149:
            r1 = move-exception
            goto L_0x012c
        L_0x014b:
            r0 = move-exception
            r0 = r7
            r7 = r2
            goto L_0x00d1
        L_0x014f:
            r1 = move-exception
            r7 = r2
            goto L_0x00d1
        L_0x0152:
            r1 = r10
            goto L_0x0121
        L_0x0154:
            r1 = r0
            r0 = r6
            goto L_0x0102
        L_0x0157:
            r9 = r10
            goto L_0x00fc
        L_0x0159:
            r1 = r10
            r0 = r7
            goto L_0x00c8
        L_0x015d:
            r0 = r6
            r1 = r7
            goto L_0x006e
        L_0x0161:
            r1 = r10
            r0 = r7
            goto L_0x0068
        L_0x0165:
            r0 = r6
            r1 = r7
            goto L_0x0073
        */
        throw new UnsupportedOperationException("Method not decompiled: com.aps.l.a(android.content.Context, android.net.NetworkInfo):org.apache.http.client.HttpClient");
    }

    private static boolean a(String str, int i) {
        return (str == null || str.length() <= 0 || i == -1) ? false : true;
    }

    private static boolean a(HttpResponse httpResponse) {
        Header firstHeader = httpResponse.getFirstHeader("Content-Encoding");
        return firstHeader != null && firstHeader.getValue().equalsIgnoreCase("gzip");
    }

    public static String[] a(JSONObject jSONObject) {
        String[] strArr = {null, null, null, null, null};
        if (jSONObject == null || c.j().length() == 0) {
            strArr[0] = "false";
        } else {
            try {
                String string = jSONObject.getString("key");
                String string2 = jSONObject.getString("X-INFO");
                String string3 = jSONObject.getString("X-BIZ");
                String string4 = jSONObject.getString("User-Agent");
                if (!TextUtils.isEmpty(string) && !TextUtils.isEmpty(string4)) {
                    strArr[0] = "true";
                    strArr[1] = string;
                    strArr[2] = string2;
                    strArr[3] = string3;
                    strArr[4] = string4;
                }
            } catch (JSONException e) {
            }
            if (strArr[0] == null || !strArr[0].equals("true")) {
                strArr[0] = "true";
            }
        }
        return strArr;
    }

    private static String b() {
        String str;
        try {
            str = Proxy.getDefaultHost();
        } catch (Throwable th) {
            th.printStackTrace();
            str = null;
        }
        return str == null ? "null" : str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.aps.l.a(android.content.Context, android.net.NetworkInfo):org.apache.http.client.HttpClient
     arg types: [android.content.Context, org.apache.http.client.methods.HttpPost]
     candidates:
      com.aps.l.a(java.lang.String, int):boolean
      com.aps.l.a(byte[], android.content.Context):java.lang.String
      com.aps.l.a(android.content.Context, android.net.NetworkInfo):org.apache.http.client.HttpClient */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00ff  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0108 A[SYNTHETIC, Splitter:B:48:0x0108] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x010d A[SYNTHETIC, Splitter:B:51:0x010d] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0112 A[SYNTHETIC, Splitter:B:54:0x0112] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0117 A[SYNTHETIC, Splitter:B:57:0x0117] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String a(android.content.Context r15, java.lang.String r16, byte[] r17, java.lang.String r18) {
        /*
            r14 = this;
            boolean r1 = android.text.TextUtils.isEmpty(r16)
            if (r1 != 0) goto L_0x0008
            if (r17 != 0) goto L_0x000a
        L_0x0008:
            r1 = 0
        L_0x0009:
            return r1
        L_0x000a:
            android.net.NetworkInfo r7 = com.aps.t.b(r15)
            int r1 = a(r7)
            r2 = -1
            if (r1 != r2) goto L_0x0017
            r1 = 0
            goto L_0x0009
        L_0x0017:
            r6 = 0
            r5 = 0
            r4 = 0
            r3 = 0
            r2 = 0
            r1 = 0
            java.lang.StringBuffer r9 = new java.lang.StringBuffer
            r9.<init>()
            java.lang.String r8 = ""
            org.apache.http.client.HttpClient r7 = a(r15, r7)     // Catch:{ UnknownHostException -> 0x0288, SocketException -> 0x016f, SocketTimeoutException -> 0x0183, ConnectTimeoutException -> 0x018e, Throwable -> 0x0199, all -> 0x01d6 }
            org.apache.http.client.methods.HttpPost r6 = new org.apache.http.client.methods.HttpPost     // Catch:{ UnknownHostException -> 0x028b, SocketException -> 0x026b, SocketTimeoutException -> 0x024e, ConnectTimeoutException -> 0x0231, Throwable -> 0x0209, all -> 0x01e1 }
            r0 = r16
            r6.<init>(r0)     // Catch:{ UnknownHostException -> 0x028b, SocketException -> 0x026b, SocketTimeoutException -> 0x024e, ConnectTimeoutException -> 0x0231, Throwable -> 0x0209, all -> 0x01e1 }
            org.apache.http.entity.ByteArrayEntity r5 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            r0 = r17
            r5.<init>(r0)     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            java.lang.String r10 = "Content-Type"
            java.lang.String r11 = "application/x-www-form-urlencoded"
            r6.addHeader(r10, r11)     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            java.lang.String r10 = "User-Agent"
            java.lang.String r11 = "AMAP Location SDK Android 1.3.1"
            r6.addHeader(r10, r11)     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            java.lang.String r10 = "Accept-Encoding"
            java.lang.String r11 = "gzip"
            r6.addHeader(r10, r11)     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            java.lang.String r10 = "Connection"
            java.lang.String r11 = "Keep-Alive"
            r6.addHeader(r10, r11)     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            java.lang.String r10 = "X-INFO"
            r11 = 0
            com.amap.api.location.core.c r11 = com.amap.api.location.core.c.a(r11)     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            r0 = r18
            java.lang.String r11 = r11.a(r0)     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            r6.addHeader(r10, r11)     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            java.lang.String r10 = "ia"
            java.lang.String r11 = "1"
            r6.addHeader(r10, r11)     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            java.lang.String r10 = "key"
            java.lang.String r11 = com.amap.api.location.core.c.a()     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            r6.addHeader(r10, r11)     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            r10 = 0
            int r11 = r9.length()     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            r9.delete(r10, r11)     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            r6.setEntity(r5)     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            org.apache.http.HttpResponse r10 = r7.execute(r6)     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            org.apache.http.StatusLine r5 = r10.getStatusLine()     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            int r5 = r5.getStatusCode()     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            r11 = 200(0xc8, float:2.8E-43)
            if (r5 != r11) goto L_0x015e
            org.apache.http.HttpEntity r5 = r10.getEntity()     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            java.io.InputStream r5 = r5.getContent()     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            org.apache.http.HttpEntity r4 = r10.getEntity()     // Catch:{ UnknownHostException -> 0x028f, SocketException -> 0x0272, SocketTimeoutException -> 0x0255, ConnectTimeoutException -> 0x0238, Throwable -> 0x021a, all -> 0x01f4 }
            org.apache.http.Header r4 = r4.getContentType()     // Catch:{ UnknownHostException -> 0x028f, SocketException -> 0x0272, SocketTimeoutException -> 0x0255, ConnectTimeoutException -> 0x0238, Throwable -> 0x021a, all -> 0x01f4 }
            java.lang.String r8 = r4.getValue()     // Catch:{ UnknownHostException -> 0x028f, SocketException -> 0x0272, SocketTimeoutException -> 0x0255, ConnectTimeoutException -> 0x0238, Throwable -> 0x021a, all -> 0x01f4 }
            java.lang.String r4 = ""
            java.lang.String r11 = "charset="
            int r11 = r8.indexOf(r11)     // Catch:{ UnknownHostException -> 0x028f, SocketException -> 0x0272, SocketTimeoutException -> 0x0255, ConnectTimeoutException -> 0x0238, Throwable -> 0x021a, all -> 0x01f4 }
            r12 = -1
            if (r11 == r12) goto L_0x00b2
            int r4 = r11 + 8
            java.lang.String r4 = r8.substring(r4)     // Catch:{ UnknownHostException -> 0x028f, SocketException -> 0x0272, SocketTimeoutException -> 0x0255, ConnectTimeoutException -> 0x0238, Throwable -> 0x021a, all -> 0x01f4 }
        L_0x00b2:
            boolean r8 = android.text.TextUtils.isEmpty(r4)     // Catch:{ UnknownHostException -> 0x028f, SocketException -> 0x0272, SocketTimeoutException -> 0x0255, ConnectTimeoutException -> 0x0238, Throwable -> 0x021a, all -> 0x01f4 }
            if (r8 == 0) goto L_0x02a7
            java.lang.String r4 = "UTF-8"
            r8 = r4
        L_0x00bb:
            boolean r4 = a(r10)     // Catch:{ UnknownHostException -> 0x028f, SocketException -> 0x0272, SocketTimeoutException -> 0x0255, ConnectTimeoutException -> 0x0238, Throwable -> 0x021a, all -> 0x01f4 }
            if (r4 == 0) goto L_0x02a4
            java.util.zip.GZIPInputStream r4 = new java.util.zip.GZIPInputStream     // Catch:{ UnknownHostException -> 0x028f, SocketException -> 0x0272, SocketTimeoutException -> 0x0255, ConnectTimeoutException -> 0x0238, Throwable -> 0x021a, all -> 0x01f4 }
            r4.<init>(r5)     // Catch:{ UnknownHostException -> 0x028f, SocketException -> 0x0272, SocketTimeoutException -> 0x0255, ConnectTimeoutException -> 0x0238, Throwable -> 0x021a, all -> 0x01f4 }
        L_0x00c6:
            if (r4 == 0) goto L_0x011b
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ UnknownHostException -> 0x0121, SocketException -> 0x0276, SocketTimeoutException -> 0x0259, ConnectTimeoutException -> 0x023c, Throwable -> 0x0221, all -> 0x01fc }
            r3.<init>(r4, r8)     // Catch:{ UnknownHostException -> 0x0121, SocketException -> 0x0276, SocketTimeoutException -> 0x0259, ConnectTimeoutException -> 0x023c, Throwable -> 0x0221, all -> 0x01fc }
        L_0x00cd:
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ UnknownHostException -> 0x0295, SocketException -> 0x027b, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0241, Throwable -> 0x0228, all -> 0x0203 }
            r8 = 2048(0x800, float:2.87E-42)
            r2.<init>(r3, r8)     // Catch:{ UnknownHostException -> 0x0295, SocketException -> 0x027b, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0241, Throwable -> 0x0228, all -> 0x0203 }
            java.lang.String r1 = ""
        L_0x00d6:
            java.lang.String r1 = r2.readLine()     // Catch:{ UnknownHostException -> 0x00e0, SocketException -> 0x0281, SocketTimeoutException -> 0x0264, ConnectTimeoutException -> 0x0247, Throwable -> 0x022e }
            if (r1 == 0) goto L_0x0127
            r9.append(r1)     // Catch:{ UnknownHostException -> 0x00e0, SocketException -> 0x0281, SocketTimeoutException -> 0x0264, ConnectTimeoutException -> 0x0247, Throwable -> 0x022e }
            goto L_0x00d6
        L_0x00e0:
            r1 = move-exception
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r5
            r5 = r6
            r6 = r7
        L_0x00e7:
            com.amap.api.location.core.AMapLocException r7 = new com.amap.api.location.core.AMapLocException     // Catch:{ all -> 0x00ef }
            java.lang.String r8 = "未知主机 - UnKnowHostException"
            r7.<init>(r8)     // Catch:{ all -> 0x00ef }
            throw r7     // Catch:{ all -> 0x00ef }
        L_0x00ef:
            r7 = move-exception
            r13 = r7
            r7 = r6
            r6 = r5
            r5 = r4
            r4 = r3
            r3 = r2
            r2 = r1
            r1 = r13
        L_0x00f8:
            if (r6 == 0) goto L_0x00fd
            r6.abort()
        L_0x00fd:
            if (r7 == 0) goto L_0x0106
            org.apache.http.conn.ClientConnectionManager r6 = r7.getConnectionManager()
            r6.shutdown()
        L_0x0106:
            if (r4 == 0) goto L_0x010b
            r4.close()     // Catch:{ Throwable -> 0x01b0 }
        L_0x010b:
            if (r5 == 0) goto L_0x0110
            r5.close()     // Catch:{ Throwable -> 0x01b3 }
        L_0x0110:
            if (r3 == 0) goto L_0x0115
            r3.close()     // Catch:{ Throwable -> 0x01b9 }
        L_0x0115:
            if (r2 == 0) goto L_0x011a
            r2.close()     // Catch:{ Throwable -> 0x01bf }
        L_0x011a:
            throw r1
        L_0x011b:
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ UnknownHostException -> 0x0121, SocketException -> 0x0276, SocketTimeoutException -> 0x0259, ConnectTimeoutException -> 0x023c, Throwable -> 0x0221, all -> 0x01fc }
            r3.<init>(r5, r8)     // Catch:{ UnknownHostException -> 0x0121, SocketException -> 0x0276, SocketTimeoutException -> 0x0259, ConnectTimeoutException -> 0x023c, Throwable -> 0x0221, all -> 0x01fc }
            goto L_0x00cd
        L_0x0121:
            r3 = move-exception
            r3 = r4
            r4 = r5
            r5 = r6
            r6 = r7
            goto L_0x00e7
        L_0x0127:
            java.lang.String r1 = r9.toString()     // Catch:{ UnknownHostException -> 0x00e0, SocketException -> 0x0281, SocketTimeoutException -> 0x0264, ConnectTimeoutException -> 0x0247, Throwable -> 0x022e }
            r8 = 0
            int r10 = r9.length()     // Catch:{ UnknownHostException -> 0x00e0, SocketException -> 0x0281, SocketTimeoutException -> 0x0264, ConnectTimeoutException -> 0x0247, Throwable -> 0x022e }
            r9.delete(r8, r10)     // Catch:{ UnknownHostException -> 0x00e0, SocketException -> 0x0281, SocketTimeoutException -> 0x0264, ConnectTimeoutException -> 0x0247, Throwable -> 0x022e }
        L_0x0133:
            if (r6 == 0) goto L_0x0138
            r6.abort()
        L_0x0138:
            if (r7 == 0) goto L_0x0141
            org.apache.http.conn.ClientConnectionManager r6 = r7.getConnectionManager()
            r6.shutdown()
        L_0x0141:
            if (r4 == 0) goto L_0x0146
            r4.close()     // Catch:{ Throwable -> 0x01c5 }
        L_0x0146:
            if (r5 == 0) goto L_0x014b
            r5.close()     // Catch:{ Throwable -> 0x01c7 }
        L_0x014b:
            if (r3 == 0) goto L_0x0150
            r3.close()     // Catch:{ Throwable -> 0x01cc }
        L_0x0150:
            if (r2 == 0) goto L_0x0155
            r2.close()     // Catch:{ Throwable -> 0x01d1 }
        L_0x0155:
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 == 0) goto L_0x0009
            r1 = 0
            goto L_0x0009
        L_0x015e:
            r9 = 404(0x194, float:5.66E-43)
            if (r5 != r9) goto L_0x029d
            com.amap.api.location.core.AMapLocException r5 = new com.amap.api.location.core.AMapLocException     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            java.lang.String r8 = "服务器连接失败 - UnknownServiceException"
            r5.<init>(r8)     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
            throw r5     // Catch:{ UnknownHostException -> 0x016a, SocketException -> 0x026f, SocketTimeoutException -> 0x0252, ConnectTimeoutException -> 0x0235, Throwable -> 0x0212, all -> 0x01eb }
        L_0x016a:
            r5 = move-exception
            r5 = r6
            r6 = r7
            goto L_0x00e7
        L_0x016f:
            r7 = move-exception
            r7 = r6
            r6 = r5
        L_0x0172:
            com.amap.api.location.core.AMapLocException r5 = new com.amap.api.location.core.AMapLocException     // Catch:{ all -> 0x017a }
            java.lang.String r8 = "socket 连接异常 - SocketException"
            r5.<init>(r8)     // Catch:{ all -> 0x017a }
            throw r5     // Catch:{ all -> 0x017a }
        L_0x017a:
            r5 = move-exception
            r13 = r5
            r5 = r4
            r4 = r3
            r3 = r2
            r2 = r1
            r1 = r13
            goto L_0x00f8
        L_0x0183:
            r7 = move-exception
            r7 = r6
            r6 = r5
        L_0x0186:
            com.amap.api.location.core.AMapLocException r5 = new com.amap.api.location.core.AMapLocException     // Catch:{ all -> 0x017a }
            java.lang.String r8 = "socket 连接超时 - SocketTimeoutException"
            r5.<init>(r8)     // Catch:{ all -> 0x017a }
            throw r5     // Catch:{ all -> 0x017a }
        L_0x018e:
            r7 = move-exception
            r7 = r6
            r6 = r5
        L_0x0191:
            com.amap.api.location.core.AMapLocException r5 = new com.amap.api.location.core.AMapLocException     // Catch:{ all -> 0x017a }
            java.lang.String r8 = "http连接失败 - ConnectionException"
            r5.<init>(r8)     // Catch:{ all -> 0x017a }
            throw r5     // Catch:{ all -> 0x017a }
        L_0x0199:
            r7 = move-exception
            r13 = r7
            r7 = r6
            r6 = r5
            r5 = r4
            r4 = r3
            r3 = r2
            r2 = r1
            r1 = r13
        L_0x01a2:
            r1.printStackTrace()     // Catch:{ all -> 0x01ad }
            com.amap.api.location.core.AMapLocException r1 = new com.amap.api.location.core.AMapLocException     // Catch:{ all -> 0x01ad }
            java.lang.String r8 = "未知的错误"
            r1.<init>(r8)     // Catch:{ all -> 0x01ad }
            throw r1     // Catch:{ all -> 0x01ad }
        L_0x01ad:
            r1 = move-exception
            goto L_0x00f8
        L_0x01b0:
            r4 = move-exception
            goto L_0x010b
        L_0x01b3:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x0110
        L_0x01b9:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x0115
        L_0x01bf:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x011a
        L_0x01c5:
            r4 = move-exception
            goto L_0x0146
        L_0x01c7:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x014b
        L_0x01cc:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x0150
        L_0x01d1:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0155
        L_0x01d6:
            r7 = move-exception
            r13 = r7
            r7 = r6
            r6 = r5
            r5 = r4
            r4 = r3
            r3 = r2
            r2 = r1
            r1 = r13
            goto L_0x00f8
        L_0x01e1:
            r6 = move-exception
            r13 = r6
            r6 = r5
            r5 = r4
            r4 = r3
            r3 = r2
            r2 = r1
            r1 = r13
            goto L_0x00f8
        L_0x01eb:
            r5 = move-exception
            r13 = r5
            r5 = r4
            r4 = r3
            r3 = r2
            r2 = r1
            r1 = r13
            goto L_0x00f8
        L_0x01f4:
            r4 = move-exception
            r13 = r4
            r4 = r3
            r3 = r2
            r2 = r1
            r1 = r13
            goto L_0x00f8
        L_0x01fc:
            r3 = move-exception
            r13 = r3
            r3 = r2
            r2 = r1
            r1 = r13
            goto L_0x00f8
        L_0x0203:
            r2 = move-exception
            r13 = r2
            r2 = r1
            r1 = r13
            goto L_0x00f8
        L_0x0209:
            r6 = move-exception
            r13 = r6
            r6 = r5
            r5 = r4
            r4 = r3
            r3 = r2
            r2 = r1
            r1 = r13
            goto L_0x01a2
        L_0x0212:
            r5 = move-exception
            r13 = r5
            r5 = r4
            r4 = r3
            r3 = r2
            r2 = r1
            r1 = r13
            goto L_0x01a2
        L_0x021a:
            r4 = move-exception
            r13 = r4
            r4 = r3
            r3 = r2
            r2 = r1
            r1 = r13
            goto L_0x01a2
        L_0x0221:
            r3 = move-exception
            r13 = r3
            r3 = r2
            r2 = r1
            r1 = r13
            goto L_0x01a2
        L_0x0228:
            r2 = move-exception
            r13 = r2
            r2 = r1
            r1 = r13
            goto L_0x01a2
        L_0x022e:
            r1 = move-exception
            goto L_0x01a2
        L_0x0231:
            r6 = move-exception
            r6 = r5
            goto L_0x0191
        L_0x0235:
            r5 = move-exception
            goto L_0x0191
        L_0x0238:
            r4 = move-exception
            r4 = r5
            goto L_0x0191
        L_0x023c:
            r3 = move-exception
            r3 = r4
            r4 = r5
            goto L_0x0191
        L_0x0241:
            r2 = move-exception
            r2 = r3
            r3 = r4
            r4 = r5
            goto L_0x0191
        L_0x0247:
            r1 = move-exception
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r5
            goto L_0x0191
        L_0x024e:
            r6 = move-exception
            r6 = r5
            goto L_0x0186
        L_0x0252:
            r5 = move-exception
            goto L_0x0186
        L_0x0255:
            r4 = move-exception
            r4 = r5
            goto L_0x0186
        L_0x0259:
            r3 = move-exception
            r3 = r4
            r4 = r5
            goto L_0x0186
        L_0x025e:
            r2 = move-exception
            r2 = r3
            r3 = r4
            r4 = r5
            goto L_0x0186
        L_0x0264:
            r1 = move-exception
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r5
            goto L_0x0186
        L_0x026b:
            r6 = move-exception
            r6 = r5
            goto L_0x0172
        L_0x026f:
            r5 = move-exception
            goto L_0x0172
        L_0x0272:
            r4 = move-exception
            r4 = r5
            goto L_0x0172
        L_0x0276:
            r3 = move-exception
            r3 = r4
            r4 = r5
            goto L_0x0172
        L_0x027b:
            r2 = move-exception
            r2 = r3
            r3 = r4
            r4 = r5
            goto L_0x0172
        L_0x0281:
            r1 = move-exception
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r5
            goto L_0x0172
        L_0x0288:
            r7 = move-exception
            goto L_0x00e7
        L_0x028b:
            r6 = move-exception
            r6 = r7
            goto L_0x00e7
        L_0x028f:
            r4 = move-exception
            r4 = r5
            r5 = r6
            r6 = r7
            goto L_0x00e7
        L_0x0295:
            r2 = move-exception
            r2 = r3
            r3 = r4
            r4 = r5
            r5 = r6
            r6 = r7
            goto L_0x00e7
        L_0x029d:
            r5 = r4
            r4 = r3
            r3 = r2
            r2 = r1
            r1 = r8
            goto L_0x0133
        L_0x02a4:
            r4 = r3
            goto L_0x00c6
        L_0x02a7:
            r8 = r4
            goto L_0x00bb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.aps.l.a(android.content.Context, java.lang.String, byte[], java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:101:0x01bd  */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x01c3  */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x01c9  */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x0274  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x0277  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x027a  */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x027d  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x0280  */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x0283  */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x00f2 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x00f2 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x00f2 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:171:0x00f2 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00ee  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0137  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x013c  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x014a  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x015d  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0163  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x016d  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0173  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0179  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0185  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x018b  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0195  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x019b  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x01a1  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x01ad  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x01b3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String a(byte[] r17, android.content.Context r18) {
        /*
            r16 = this;
            java.lang.String r9 = ""
            android.net.NetworkInfo r12 = com.aps.t.b(r18)
            int r1 = a(r12)
            r2 = -1
            if (r1 != r2) goto L_0x000f
            r1 = 0
        L_0x000e:
            return r1
        L_0x000f:
            r8 = 0
            r7 = 0
            r6 = 0
            r4 = 0
            r5 = 0
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            r3.<init>()
            java.lang.StringBuffer r13 = new java.lang.StringBuffer
            r13.<init>()
            java.lang.String r1 = "http://cgicol.amap.com/collection/writedata?ver=v1.0_ali&"
            r13.append(r1)
            java.lang.String r1 = "zei="
            java.lang.StringBuffer r1 = r13.append(r1)
            java.lang.String r2 = com.aps.f.f462a
            r1.append(r2)
            java.lang.String r1 = "&zsi="
            java.lang.StringBuffer r1 = r13.append(r1)
            java.lang.String r2 = com.aps.f.f463b
            r1.append(r2)
            r2 = 0
            r1 = 0
            r11 = r1
            r1 = r2
            r2 = r3
            r3 = r9
        L_0x003f:
            r9 = 1
            if (r11 >= r9) goto L_0x0044
            if (r1 == 0) goto L_0x0056
        L_0x0044:
            r1 = 0
            int r2 = r13.length()
            r13.delete(r1, r2)
            java.lang.String r1 = ""
            boolean r1 = r3.equals(r1)
            if (r1 == 0) goto L_0x0153
            r1 = 0
            goto L_0x000e
        L_0x0056:
            r0 = r18
            org.apache.http.client.HttpClient r10 = a(r0, r12)     // Catch:{ UnknownHostException -> 0x0244, SocketException -> 0x0222, SocketTimeoutException -> 0x0200, ConnectTimeoutException -> 0x01e3, all -> 0x01ce }
            org.apache.http.client.methods.HttpPost r9 = new org.apache.http.client.methods.HttpPost     // Catch:{ UnknownHostException -> 0x024d, SocketException -> 0x022a, SocketTimeoutException -> 0x0208, ConnectTimeoutException -> 0x01ea, all -> 0x01d3 }
            java.lang.String r8 = r13.toString()     // Catch:{ UnknownHostException -> 0x024d, SocketException -> 0x022a, SocketTimeoutException -> 0x0208, ConnectTimeoutException -> 0x01ea, all -> 0x01d3 }
            r9.<init>(r8)     // Catch:{ UnknownHostException -> 0x024d, SocketException -> 0x022a, SocketTimeoutException -> 0x0208, ConnectTimeoutException -> 0x01ea, all -> 0x01d3 }
            r7 = 0
            int r8 = r2.length()     // Catch:{ UnknownHostException -> 0x0255, SocketException -> 0x0231, SocketTimeoutException -> 0x020f, ConnectTimeoutException -> 0x01f0, all -> 0x01d7 }
            r2.delete(r7, r8)     // Catch:{ UnknownHostException -> 0x0255, SocketException -> 0x0231, SocketTimeoutException -> 0x020f, ConnectTimeoutException -> 0x01f0, all -> 0x01d7 }
            java.lang.String r7 = "application/soap+xml;charset="
            r2.append(r7)     // Catch:{ UnknownHostException -> 0x0255, SocketException -> 0x0231, SocketTimeoutException -> 0x020f, ConnectTimeoutException -> 0x01f0, all -> 0x01d7 }
            java.lang.String r7 = "UTF-8"
            r2.append(r7)     // Catch:{ UnknownHostException -> 0x0255, SocketException -> 0x0231, SocketTimeoutException -> 0x020f, ConnectTimeoutException -> 0x01f0, all -> 0x01d7 }
            r7 = 0
            int r8 = r2.length()     // Catch:{ UnknownHostException -> 0x0255, SocketException -> 0x0231, SocketTimeoutException -> 0x020f, ConnectTimeoutException -> 0x01f0, all -> 0x01d7 }
            r2.delete(r7, r8)     // Catch:{ UnknownHostException -> 0x0255, SocketException -> 0x0231, SocketTimeoutException -> 0x020f, ConnectTimeoutException -> 0x01f0, all -> 0x01d7 }
            java.lang.String r7 = "gzipped"
            java.lang.String r8 = "1"
            r9.addHeader(r7, r8)     // Catch:{ UnknownHostException -> 0x0255, SocketException -> 0x0231, SocketTimeoutException -> 0x020f, ConnectTimeoutException -> 0x01f0, all -> 0x01d7 }
            byte[] r7 = com.aps.t.a(r17)     // Catch:{ UnknownHostException -> 0x0255, SocketException -> 0x0231, SocketTimeoutException -> 0x020f, ConnectTimeoutException -> 0x01f0, all -> 0x01d7 }
            org.apache.http.entity.ByteArrayEntity r8 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ UnknownHostException -> 0x0255, SocketException -> 0x0231, SocketTimeoutException -> 0x020f, ConnectTimeoutException -> 0x01f0, all -> 0x01d7 }
            r8.<init>(r7)     // Catch:{ UnknownHostException -> 0x0255, SocketException -> 0x0231, SocketTimeoutException -> 0x020f, ConnectTimeoutException -> 0x01f0, all -> 0x01d7 }
            java.lang.String r7 = "application/octet-stream"
            r8.setContentType(r7)     // Catch:{ UnknownHostException -> 0x0255, SocketException -> 0x0231, SocketTimeoutException -> 0x020f, ConnectTimeoutException -> 0x01f0, all -> 0x01d7 }
            r9.setEntity(r8)     // Catch:{ UnknownHostException -> 0x0255, SocketException -> 0x0231, SocketTimeoutException -> 0x020f, ConnectTimeoutException -> 0x01f0, all -> 0x01d7 }
            org.apache.http.HttpResponse r7 = r10.execute(r9)     // Catch:{ UnknownHostException -> 0x0255, SocketException -> 0x0231, SocketTimeoutException -> 0x020f, ConnectTimeoutException -> 0x01f0, all -> 0x01d7 }
            org.apache.http.StatusLine r8 = r7.getStatusLine()     // Catch:{ UnknownHostException -> 0x0255, SocketException -> 0x0231, SocketTimeoutException -> 0x020f, ConnectTimeoutException -> 0x01f0, all -> 0x01d7 }
            int r8 = r8.getStatusCode()     // Catch:{ UnknownHostException -> 0x0255, SocketException -> 0x0231, SocketTimeoutException -> 0x020f, ConnectTimeoutException -> 0x01f0, all -> 0x01d7 }
            r14 = 200(0xc8, float:2.8E-43)
            if (r8 != r14) goto L_0x0286
            org.apache.http.HttpEntity r7 = r7.getEntity()     // Catch:{ UnknownHostException -> 0x0255, SocketException -> 0x0231, SocketTimeoutException -> 0x020f, ConnectTimeoutException -> 0x01f0, all -> 0x01d7 }
            java.io.InputStream r7 = r7.getContent()     // Catch:{ UnknownHostException -> 0x0255, SocketException -> 0x0231, SocketTimeoutException -> 0x020f, ConnectTimeoutException -> 0x01f0, all -> 0x01d7 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ UnknownHostException -> 0x025d, SocketException -> 0x0237, SocketTimeoutException -> 0x0215, ConnectTimeoutException -> 0x01f5, all -> 0x01da }
            java.lang.String r8 = "UTF-8"
            r6.<init>(r7, r8)     // Catch:{ UnknownHostException -> 0x025d, SocketException -> 0x0237, SocketTimeoutException -> 0x0215, ConnectTimeoutException -> 0x01f5, all -> 0x01da }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ UnknownHostException -> 0x0265, SocketException -> 0x023d, SocketTimeoutException -> 0x021b, ConnectTimeoutException -> 0x01fa, all -> 0x01de }
            r8 = 2048(0x800, float:2.87E-42)
            r4.<init>(r6, r8)     // Catch:{ UnknownHostException -> 0x0265, SocketException -> 0x023d, SocketTimeoutException -> 0x021b, ConnectTimeoutException -> 0x01fa, all -> 0x01de }
            java.lang.String r5 = ""
        L_0x00bf:
            java.lang.String r5 = r4.readLine()     // Catch:{ UnknownHostException -> 0x00c9, SocketException -> 0x0156, SocketTimeoutException -> 0x017e, ConnectTimeoutException -> 0x01a6, all -> 0x0131 }
            if (r5 == 0) goto L_0x00fd
            r2.append(r5)     // Catch:{ UnknownHostException -> 0x00c9, SocketException -> 0x0156, SocketTimeoutException -> 0x017e, ConnectTimeoutException -> 0x01a6, all -> 0x0131 }
            goto L_0x00bf
        L_0x00c9:
            r5 = move-exception
            r5 = r7
            r8 = r3
            r3 = r4
            r7 = r10
            r4 = r6
            r6 = r9
        L_0x00d0:
            if (r6 == 0) goto L_0x00d6
            r6.abort()
            r6 = 0
        L_0x00d6:
            if (r7 == 0) goto L_0x00e0
            org.apache.http.conn.ClientConnectionManager r7 = r7.getConnectionManager()
            r7.shutdown()
            r7 = 0
        L_0x00e0:
            if (r5 == 0) goto L_0x00e6
            r5.close()
            r5 = 0
        L_0x00e6:
            if (r4 == 0) goto L_0x00ec
            r4.close()
            r4 = 0
        L_0x00ec:
            if (r3 == 0) goto L_0x00f2
            r3.close()
        L_0x00f1:
            r3 = 0
        L_0x00f2:
            int r9 = r11 + 1
            r11 = r9
            r15 = r3
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r5
            r5 = r15
            goto L_0x003f
        L_0x00fd:
            java.lang.String r3 = r2.toString()     // Catch:{ UnknownHostException -> 0x00c9, SocketException -> 0x0156, SocketTimeoutException -> 0x017e, ConnectTimeoutException -> 0x01a6, all -> 0x0131 }
            r5 = 0
            int r8 = r2.length()     // Catch:{ UnknownHostException -> 0x00c9, SocketException -> 0x0156, SocketTimeoutException -> 0x017e, ConnectTimeoutException -> 0x01a6, all -> 0x0131 }
            r2.delete(r5, r8)     // Catch:{ UnknownHostException -> 0x00c9, SocketException -> 0x0156, SocketTimeoutException -> 0x017e, ConnectTimeoutException -> 0x01a6, all -> 0x0131 }
            r2 = 0
            r1 = 1
            r5 = r7
            r8 = r3
            r3 = r4
            r4 = r6
        L_0x010f:
            if (r9 == 0) goto L_0x0271
            r9.abort()
            r6 = 0
        L_0x0115:
            if (r10 == 0) goto L_0x026e
            org.apache.http.conn.ClientConnectionManager r7 = r10.getConnectionManager()
            r7.shutdown()
            r7 = 0
        L_0x011f:
            if (r5 == 0) goto L_0x0125
            r5.close()
            r5 = 0
        L_0x0125:
            if (r4 == 0) goto L_0x012b
            r4.close()
            r4 = 0
        L_0x012b:
            if (r3 == 0) goto L_0x00f2
            r3.close()
            goto L_0x00f1
        L_0x0131:
            r1 = move-exception
            r5 = r4
            r4 = r6
            r6 = r7
        L_0x0135:
            if (r9 == 0) goto L_0x013a
            r9.abort()
        L_0x013a:
            if (r10 == 0) goto L_0x0143
            org.apache.http.conn.ClientConnectionManager r2 = r10.getConnectionManager()
            r2.shutdown()
        L_0x0143:
            if (r6 == 0) goto L_0x0148
            r6.close()
        L_0x0148:
            if (r4 == 0) goto L_0x014d
            r4.close()
        L_0x014d:
            if (r5 == 0) goto L_0x0152
            r5.close()
        L_0x0152:
            throw r1
        L_0x0153:
            r1 = r3
            goto L_0x000e
        L_0x0156:
            r5 = move-exception
            r5 = r7
            r8 = r3
            r3 = r4
            r4 = r6
        L_0x015b:
            if (r9 == 0) goto L_0x0283
            r9.abort()
            r6 = 0
        L_0x0161:
            if (r10 == 0) goto L_0x0280
            org.apache.http.conn.ClientConnectionManager r7 = r10.getConnectionManager()
            r7.shutdown()
            r7 = 0
        L_0x016b:
            if (r5 == 0) goto L_0x0171
            r5.close()
            r5 = 0
        L_0x0171:
            if (r4 == 0) goto L_0x0177
            r4.close()
            r4 = 0
        L_0x0177:
            if (r3 == 0) goto L_0x00f2
            r3.close()
            goto L_0x00f1
        L_0x017e:
            r5 = move-exception
            r5 = r7
            r8 = r3
            r3 = r4
            r4 = r6
        L_0x0183:
            if (r9 == 0) goto L_0x027d
            r9.abort()
            r6 = 0
        L_0x0189:
            if (r10 == 0) goto L_0x027a
            org.apache.http.conn.ClientConnectionManager r7 = r10.getConnectionManager()
            r7.shutdown()
            r7 = 0
        L_0x0193:
            if (r5 == 0) goto L_0x0199
            r5.close()
            r5 = 0
        L_0x0199:
            if (r4 == 0) goto L_0x019f
            r4.close()
            r4 = 0
        L_0x019f:
            if (r3 == 0) goto L_0x00f2
            r3.close()
            goto L_0x00f1
        L_0x01a6:
            r5 = move-exception
            r5 = r7
            r8 = r3
            r3 = r4
            r4 = r6
        L_0x01ab:
            if (r9 == 0) goto L_0x0277
            r9.abort()
            r6 = 0
        L_0x01b1:
            if (r10 == 0) goto L_0x0274
            org.apache.http.conn.ClientConnectionManager r7 = r10.getConnectionManager()
            r7.shutdown()
            r7 = 0
        L_0x01bb:
            if (r5 == 0) goto L_0x01c1
            r5.close()
            r5 = 0
        L_0x01c1:
            if (r4 == 0) goto L_0x01c7
            r4.close()
            r4 = 0
        L_0x01c7:
            if (r3 == 0) goto L_0x00f2
            r3.close()
            goto L_0x00f1
        L_0x01ce:
            r1 = move-exception
            r9 = r7
            r10 = r8
            goto L_0x0135
        L_0x01d3:
            r1 = move-exception
            r9 = r7
            goto L_0x0135
        L_0x01d7:
            r1 = move-exception
            goto L_0x0135
        L_0x01da:
            r1 = move-exception
            r6 = r7
            goto L_0x0135
        L_0x01de:
            r1 = move-exception
            r4 = r6
            r6 = r7
            goto L_0x0135
        L_0x01e3:
            r9 = move-exception
            r9 = r7
            r10 = r8
            r8 = r3
            r3 = r5
            r5 = r6
            goto L_0x01ab
        L_0x01ea:
            r8 = move-exception
            r9 = r7
            r8 = r3
            r3 = r5
            r5 = r6
            goto L_0x01ab
        L_0x01f0:
            r7 = move-exception
            r8 = r3
            r3 = r5
            r5 = r6
            goto L_0x01ab
        L_0x01f5:
            r6 = move-exception
            r8 = r3
            r3 = r5
            r5 = r7
            goto L_0x01ab
        L_0x01fa:
            r4 = move-exception
            r4 = r6
            r8 = r3
            r3 = r5
            r5 = r7
            goto L_0x01ab
        L_0x0200:
            r9 = move-exception
            r9 = r7
            r10 = r8
            r8 = r3
            r3 = r5
            r5 = r6
            goto L_0x0183
        L_0x0208:
            r8 = move-exception
            r9 = r7
            r8 = r3
            r3 = r5
            r5 = r6
            goto L_0x0183
        L_0x020f:
            r7 = move-exception
            r8 = r3
            r3 = r5
            r5 = r6
            goto L_0x0183
        L_0x0215:
            r6 = move-exception
            r8 = r3
            r3 = r5
            r5 = r7
            goto L_0x0183
        L_0x021b:
            r4 = move-exception
            r4 = r6
            r8 = r3
            r3 = r5
            r5 = r7
            goto L_0x0183
        L_0x0222:
            r9 = move-exception
            r9 = r7
            r10 = r8
            r8 = r3
            r3 = r5
            r5 = r6
            goto L_0x015b
        L_0x022a:
            r8 = move-exception
            r9 = r7
            r8 = r3
            r3 = r5
            r5 = r6
            goto L_0x015b
        L_0x0231:
            r7 = move-exception
            r8 = r3
            r3 = r5
            r5 = r6
            goto L_0x015b
        L_0x0237:
            r6 = move-exception
            r8 = r3
            r3 = r5
            r5 = r7
            goto L_0x015b
        L_0x023d:
            r4 = move-exception
            r4 = r6
            r8 = r3
            r3 = r5
            r5 = r7
            goto L_0x015b
        L_0x0244:
            r9 = move-exception
            r15 = r5
            r5 = r6
            r6 = r7
            r7 = r8
            r8 = r3
            r3 = r15
            goto L_0x00d0
        L_0x024d:
            r8 = move-exception
            r8 = r3
            r3 = r5
            r5 = r6
            r6 = r7
            r7 = r10
            goto L_0x00d0
        L_0x0255:
            r7 = move-exception
            r7 = r10
            r8 = r3
            r3 = r5
            r5 = r6
            r6 = r9
            goto L_0x00d0
        L_0x025d:
            r6 = move-exception
            r6 = r9
            r8 = r3
            r3 = r5
            r5 = r7
            r7 = r10
            goto L_0x00d0
        L_0x0265:
            r4 = move-exception
            r4 = r6
            r8 = r3
            r6 = r9
            r3 = r5
            r5 = r7
            r7 = r10
            goto L_0x00d0
        L_0x026e:
            r7 = r10
            goto L_0x011f
        L_0x0271:
            r6 = r9
            goto L_0x0115
        L_0x0274:
            r7 = r10
            goto L_0x01bb
        L_0x0277:
            r6 = r9
            goto L_0x01b1
        L_0x027a:
            r7 = r10
            goto L_0x0193
        L_0x027d:
            r6 = r9
            goto L_0x0189
        L_0x0280:
            r7 = r10
            goto L_0x016b
        L_0x0283:
            r6 = r9
            goto L_0x0161
        L_0x0286:
            r8 = r3
            r3 = r5
            r5 = r6
            goto L_0x010f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.aps.l.a(byte[], android.content.Context):java.lang.String");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:95:0x01f5 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:37:0x0145 */
    /* JADX WARN: Type inference failed for: r2v15, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r2v29, types: [java.lang.String] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0167  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x016c  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0175 A[SYNTHETIC, Splitter:B:53:0x0175] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x017a A[SYNTHETIC, Splitter:B:56:0x017a] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x017f  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0184  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String a(byte[] r20, android.content.Context r21, org.json.JSONObject r22) {
        /*
            r19 = this;
            android.net.NetworkInfo r12 = com.aps.t.b(r21)
            int r1 = a(r12)
            r2 = -1
            if (r1 != r2) goto L_0x0013
            com.amap.api.location.core.AMapLocException r1 = new com.amap.api.location.core.AMapLocException
            java.lang.String r2 = "http连接失败 - ConnectionException"
            r1.<init>(r2)
            throw r1
        L_0x0013:
            r8 = 0
            r7 = 0
            r6 = 0
            r4 = 0
            r5 = 0
            r2 = 0
            java.lang.StringBuffer r10 = new java.lang.StringBuffer
            r10.<init>()
            java.lang.String r3 = ""
            r9 = 0
            r1 = 0
            r11 = r1
            r1 = r3
            r3 = r5
            r5 = r6
            r6 = r8
        L_0x0027:
            r8 = 1
            if (r11 >= r8) goto L_0x002c
            if (r9 == 0) goto L_0x0034
        L_0x002c:
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 == 0) goto L_0x0033
            r1 = 0
        L_0x0033:
            return r1
        L_0x0034:
            r0 = r21
            org.apache.http.client.HttpClient r8 = a(r0, r12)     // Catch:{ UnknownHostException -> 0x0269, SocketException -> 0x0260, SocketTimeoutException -> 0x0257, ConnectTimeoutException -> 0x024e, Throwable -> 0x0245, all -> 0x023c }
            java.lang.String[] r13 = a(r22)     // Catch:{ UnknownHostException -> 0x0271, SocketException -> 0x0264, SocketTimeoutException -> 0x025b, ConnectTimeoutException -> 0x0252, Throwable -> 0x0249, all -> 0x0241 }
            org.apache.http.client.methods.HttpPost r6 = new org.apache.http.client.methods.HttpPost     // Catch:{ UnknownHostException -> 0x0271, SocketException -> 0x0264, SocketTimeoutException -> 0x025b, ConnectTimeoutException -> 0x0252, Throwable -> 0x0249, all -> 0x0241 }
            java.lang.String r14 = com.amap.api.location.core.c.j()     // Catch:{ UnknownHostException -> 0x0271, SocketException -> 0x0264, SocketTimeoutException -> 0x025b, ConnectTimeoutException -> 0x0252, Throwable -> 0x0249, all -> 0x0241 }
            r6.<init>(r14)     // Catch:{ UnknownHostException -> 0x0271, SocketException -> 0x0264, SocketTimeoutException -> 0x025b, ConnectTimeoutException -> 0x0252, Throwable -> 0x0249, all -> 0x0241 }
            java.lang.String r7 = "UTF-8"
            byte[] r14 = com.aps.t.a(r20)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            org.apache.http.entity.ByteArrayEntity r15 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r15.<init>(r14)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            java.lang.String r14 = "application/octet-stream"
            r15.setContentType(r14)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            java.lang.String r14 = "Accept-Encoding"
            java.lang.String r16 = "gzip"
            r0 = r16
            r6.addHeader(r14, r0)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            java.lang.String r14 = "gzipped"
            java.lang.String r16 = "1"
            r0 = r16
            r6.addHeader(r14, r0)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            java.lang.String r14 = "X-INFO"
            r16 = 2
            r16 = r13[r16]     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r0 = r16
            r6.addHeader(r14, r0)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            java.lang.String r14 = "X-BIZ"
            r16 = 3
            r16 = r13[r16]     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r0 = r16
            r6.addHeader(r14, r0)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            java.lang.String r14 = "KEY"
            r16 = 1
            r16 = r13[r16]     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r0 = r16
            r6.addHeader(r14, r0)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            java.lang.String r14 = "ec"
            java.lang.String r16 = "1"
            r0 = r16
            r6.addHeader(r14, r0)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            java.lang.String r14 = "enginever"
            java.lang.String r16 = "4.2"
            r0 = r16
            r6.addHeader(r14, r0)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r14 = 4
            r14 = r13[r14]     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            if (r14 == 0) goto L_0x00b5
            r14 = 4
            r14 = r13[r14]     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            int r14 = r14.length()     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            if (r14 <= 0) goto L_0x00b5
            java.lang.String r14 = "User-Agent"
            r16 = 4
            r16 = r13[r16]     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r0 = r16
            r6.addHeader(r14, r0)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
        L_0x00b5:
            java.lang.String r14 = com.amap.api.location.core.d.a()     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            java.lang.StringBuilder r16 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r16.<init>()     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            java.lang.String r17 = "key="
            java.lang.StringBuilder r16 = r16.append(r17)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r17 = 1
            r13 = r13[r17]     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r0 = r16
            java.lang.StringBuilder r13 = r0.append(r13)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            java.lang.String r13 = r13.toString()     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            java.lang.String r13 = com.amap.api.location.core.d.a(r14, r13)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            java.lang.String r16 = "ts"
            r0 = r16
            r6.addHeader(r0, r14)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            java.lang.String r14 = "scode"
            r6.addHeader(r14, r13)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r13 = 0
            int r14 = r10.length()     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r10.delete(r13, r14)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r6.setEntity(r15)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            org.apache.http.HttpResponse r13 = r8.execute(r6)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            org.apache.http.StatusLine r14 = r13.getStatusLine()     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            int r14 = r14.getStatusCode()     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r15 = 200(0xc8, float:2.8E-43)
            if (r14 != r15) goto L_0x01de
            org.apache.http.HttpEntity r1 = r13.getEntity()     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            java.io.InputStream r5 = r1.getContent()     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            org.apache.http.HttpEntity r1 = r13.getEntity()     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            org.apache.http.Header r1 = r1.getContentType()     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            java.lang.String r9 = r1.getValue()     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            java.lang.String r1 = ""
            java.lang.String r14 = "charset="
            int r14 = r9.indexOf(r14)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r15 = -1
            if (r14 == r15) goto L_0x0122
            int r1 = r14 + 8
            java.lang.String r1 = r9.substring(r1)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
        L_0x0122:
            boolean r9 = android.text.TextUtils.isEmpty(r1)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            if (r9 == 0) goto L_0x0286
        L_0x0128:
            boolean r1 = a(r13)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            if (r1 == 0) goto L_0x0134
            java.util.zip.GZIPInputStream r1 = new java.util.zip.GZIPInputStream     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r1.<init>(r5)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r4 = r1
        L_0x0134:
            if (r4 == 0) goto L_0x0188
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r1.<init>(r4, r7)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r3 = r1
        L_0x013c:
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r7 = 2048(0x800, float:2.87E-42)
            r1.<init>(r3, r7)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            java.lang.String r2 = ""
        L_0x0145:
            java.lang.String r2 = r1.readLine()     // Catch:{ UnknownHostException -> 0x014f, SocketException -> 0x01f3, SocketTimeoutException -> 0x0200, ConnectTimeoutException -> 0x020a, Throwable -> 0x0214, all -> 0x0234 }
            if (r2 == 0) goto L_0x018f
            r10.append(r2)     // Catch:{ UnknownHostException -> 0x014f, SocketException -> 0x01f3, SocketTimeoutException -> 0x0200, ConnectTimeoutException -> 0x020a, Throwable -> 0x0214, all -> 0x0234 }
            goto L_0x0145
        L_0x014f:
            r2 = move-exception
            r2 = r3
            r3 = r4
            r4 = r5
            r5 = r6
            r6 = r8
        L_0x0155:
            com.amap.api.location.core.AMapLocException r7 = new com.amap.api.location.core.AMapLocException     // Catch:{ all -> 0x015d }
            java.lang.String r8 = "未知主机 - UnKnowHostException"
            r7.<init>(r8)     // Catch:{ all -> 0x015d }
            throw r7     // Catch:{ all -> 0x015d }
        L_0x015d:
            r7 = move-exception
            r8 = r6
            r6 = r5
            r5 = r4
            r4 = r3
            r3 = r2
            r2 = r1
            r1 = r7
        L_0x0165:
            if (r6 == 0) goto L_0x016a
            r6.abort()
        L_0x016a:
            if (r8 == 0) goto L_0x0173
            org.apache.http.conn.ClientConnectionManager r6 = r8.getConnectionManager()
            r6.shutdown()
        L_0x0173:
            if (r4 == 0) goto L_0x0178
            r4.close()     // Catch:{ Throwable -> 0x021e }
        L_0x0178:
            if (r5 == 0) goto L_0x017d
            r5.close()     // Catch:{ Throwable -> 0x0224 }
        L_0x017d:
            if (r3 == 0) goto L_0x0182
            r3.close()
        L_0x0182:
            if (r2 == 0) goto L_0x0187
            r2.close()
        L_0x0187:
            throw r1
        L_0x0188:
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r1.<init>(r5, r7)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            r3 = r1
            goto L_0x013c
        L_0x018f:
            java.lang.String r7 = r10.toString()     // Catch:{ UnknownHostException -> 0x014f, SocketException -> 0x01f3, SocketTimeoutException -> 0x0200, ConnectTimeoutException -> 0x020a, Throwable -> 0x0214, all -> 0x0234 }
            r2 = 0
            int r9 = r10.length()     // Catch:{ UnknownHostException -> 0x014f, SocketException -> 0x01f3, SocketTimeoutException -> 0x0200, ConnectTimeoutException -> 0x020a, Throwable -> 0x0214, all -> 0x0234 }
            r10.delete(r2, r9)     // Catch:{ UnknownHostException -> 0x014f, SocketException -> 0x01f3, SocketTimeoutException -> 0x0200, ConnectTimeoutException -> 0x020a, Throwable -> 0x0214, all -> 0x0234 }
            r9 = 0
            r2 = 1
            r18 = r7
            r7 = r4
            r4 = r9
            r9 = r5
            r5 = r3
            r3 = r18
        L_0x01a5:
            if (r6 == 0) goto L_0x01ab
            r6.abort()
            r6 = 0
        L_0x01ab:
            if (r8 == 0) goto L_0x01b5
            org.apache.http.conn.ClientConnectionManager r8 = r8.getConnectionManager()
            r8.shutdown()
            r8 = 0
        L_0x01b5:
            if (r7 == 0) goto L_0x01bb
            r7.close()     // Catch:{ Throwable -> 0x022a }
        L_0x01ba:
            r7 = 0
        L_0x01bb:
            if (r9 == 0) goto L_0x01c1
            r9.close()     // Catch:{ Throwable -> 0x022f }
        L_0x01c0:
            r9 = 0
        L_0x01c1:
            if (r5 == 0) goto L_0x01c7
            r5.close()
            r5 = 0
        L_0x01c7:
            if (r1 == 0) goto L_0x01cd
            r1.close()
            r1 = 0
        L_0x01cd:
            int r10 = r11 + 1
            r11 = r10
            r10 = r4
            r4 = r7
            r7 = r6
            r6 = r8
            r18 = r5
            r5 = r9
            r9 = r2
            r2 = r1
            r1 = r3
            r3 = r18
            goto L_0x0027
        L_0x01de:
            r7 = 404(0x194, float:5.66E-43)
            if (r14 != r7) goto L_0x027a
            com.amap.api.location.core.AMapLocException r1 = new com.amap.api.location.core.AMapLocException     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            java.lang.String r7 = "服务器连接失败 - UnknownServiceException"
            r1.<init>(r7)     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
            throw r1     // Catch:{ UnknownHostException -> 0x01ea, SocketException -> 0x0267, SocketTimeoutException -> 0x025e, ConnectTimeoutException -> 0x0255, Throwable -> 0x024c }
        L_0x01ea:
            r1 = move-exception
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r5
            r5 = r6
            r6 = r8
            goto L_0x0155
        L_0x01f3:
            r2 = move-exception
            r2 = r1
        L_0x01f5:
            com.amap.api.location.core.AMapLocException r1 = new com.amap.api.location.core.AMapLocException     // Catch:{ all -> 0x01fd }
            java.lang.String r7 = "socket 连接异常 - SocketException"
            r1.<init>(r7)     // Catch:{ all -> 0x01fd }
            throw r1     // Catch:{ all -> 0x01fd }
        L_0x01fd:
            r1 = move-exception
            goto L_0x0165
        L_0x0200:
            r2 = move-exception
            r2 = r1
        L_0x0202:
            com.amap.api.location.core.AMapLocException r1 = new com.amap.api.location.core.AMapLocException     // Catch:{ all -> 0x01fd }
            java.lang.String r7 = "socket 连接超时 - SocketTimeoutException"
            r1.<init>(r7)     // Catch:{ all -> 0x01fd }
            throw r1     // Catch:{ all -> 0x01fd }
        L_0x020a:
            r2 = move-exception
            r2 = r1
        L_0x020c:
            com.amap.api.location.core.AMapLocException r1 = new com.amap.api.location.core.AMapLocException     // Catch:{ all -> 0x01fd }
            java.lang.String r7 = "http连接失败 - ConnectionException"
            r1.<init>(r7)     // Catch:{ all -> 0x01fd }
            throw r1     // Catch:{ all -> 0x01fd }
        L_0x0214:
            r2 = move-exception
            r2 = r1
        L_0x0216:
            com.amap.api.location.core.AMapLocException r1 = new com.amap.api.location.core.AMapLocException     // Catch:{ all -> 0x01fd }
            java.lang.String r7 = "http连接失败 - ConnectionException"
            r1.<init>(r7)     // Catch:{ all -> 0x01fd }
            throw r1     // Catch:{ all -> 0x01fd }
        L_0x021e:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x0178
        L_0x0224:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x017d
        L_0x022a:
            r7 = move-exception
            r7.printStackTrace()
            goto L_0x01ba
        L_0x022f:
            r9 = move-exception
            r9.printStackTrace()
            goto L_0x01c0
        L_0x0234:
            r2 = move-exception
            r18 = r2
            r2 = r1
            r1 = r18
            goto L_0x0165
        L_0x023c:
            r1 = move-exception
            r8 = r6
            r6 = r7
            goto L_0x0165
        L_0x0241:
            r1 = move-exception
            r6 = r7
            goto L_0x0165
        L_0x0245:
            r1 = move-exception
            r8 = r6
            r6 = r7
            goto L_0x0216
        L_0x0249:
            r1 = move-exception
            r6 = r7
            goto L_0x0216
        L_0x024c:
            r1 = move-exception
            goto L_0x0216
        L_0x024e:
            r1 = move-exception
            r8 = r6
            r6 = r7
            goto L_0x020c
        L_0x0252:
            r1 = move-exception
            r6 = r7
            goto L_0x020c
        L_0x0255:
            r1 = move-exception
            goto L_0x020c
        L_0x0257:
            r1 = move-exception
            r8 = r6
            r6 = r7
            goto L_0x0202
        L_0x025b:
            r1 = move-exception
            r6 = r7
            goto L_0x0202
        L_0x025e:
            r1 = move-exception
            goto L_0x0202
        L_0x0260:
            r1 = move-exception
            r8 = r6
            r6 = r7
            goto L_0x01f5
        L_0x0264:
            r1 = move-exception
            r6 = r7
            goto L_0x01f5
        L_0x0267:
            r1 = move-exception
            goto L_0x01f5
        L_0x0269:
            r1 = move-exception
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r5
            r5 = r7
            goto L_0x0155
        L_0x0271:
            r1 = move-exception
            r1 = r2
            r6 = r8
            r2 = r3
            r3 = r4
            r4 = r5
            r5 = r7
            goto L_0x0155
        L_0x027a:
            r7 = r4
            r4 = r10
            r18 = r1
            r1 = r2
            r2 = r9
            r9 = r5
            r5 = r3
            r3 = r18
            goto L_0x01a5
        L_0x0286:
            r7 = r1
            goto L_0x0128
        */
        throw new UnsupportedOperationException("Method not decompiled: com.aps.l.a(byte[], android.content.Context, org.json.JSONObject):java.lang.String");
    }
}
