package com.aps;

import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;

final class am extends PhoneStateListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ak f423a;

    private am(ak akVar) {
        this.f423a = akVar;
    }

    /* synthetic */ am(ak akVar, byte b2) {
        this(akVar);
    }

    public final void onCellLocationChanged(CellLocation cellLocation) {
        try {
            long unused = this.f423a.t = System.currentTimeMillis();
            CellLocation unused2 = this.f423a.x = cellLocation;
            super.onCellLocationChanged(cellLocation);
        } catch (Exception e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.aps.ak.a(com.aps.ak, boolean):boolean
     arg types: [com.aps.ak, int]
     candidates:
      com.aps.ak.a(android.telephony.CellLocation, android.content.Context):int
      com.aps.ak.a(com.aps.ak, int):int
      com.aps.ak.a(com.aps.ak, long):long
      com.aps.ak.a(com.aps.ak, android.os.Looper):android.os.Looper
      com.aps.ak.a(com.aps.ak, android.telephony.CellLocation):android.telephony.CellLocation
      com.aps.ak.a(com.aps.ak, com.aps.am):com.aps.am
      com.aps.ak.a(com.aps.ak, com.aps.an):com.aps.an
      com.aps.ak.a(com.aps.ak, java.lang.String):java.lang.String
      com.aps.ak.a(com.aps.ak, java.util.Timer):java.util.Timer
      com.aps.ak.a(com.aps.ak, android.location.GpsStatus$NmeaListener):void
      com.aps.ak.a(com.aps.ak, android.telephony.PhoneStateListener):void
      com.aps.ak.a(com.aps.ak, boolean):boolean */
    public final void onServiceStateChanged(ServiceState serviceState) {
        try {
            if (serviceState.getState() == 0) {
                boolean unused = this.f423a.k = true;
                String[] a2 = ak.b(this.f423a.f421b);
                int unused2 = this.f423a.o = Integer.parseInt(a2[0]);
                int unused3 = this.f423a.p = Integer.parseInt(a2[1]);
            } else {
                boolean unused4 = this.f423a.k = false;
            }
            super.onServiceStateChanged(serviceState);
        } catch (Exception e) {
        }
    }

    public final void onSignalStrengthsChanged(SignalStrength signalStrength) {
        try {
            if (this.f423a.i) {
                int unused = this.f423a.j = signalStrength.getCdmaDbm();
            } else {
                int unused2 = this.f423a.j = signalStrength.getGsmSignalStrength();
                if (this.f423a.j == 99) {
                    int unused3 = this.f423a.j = -1;
                } else {
                    int unused4 = this.f423a.j = (this.f423a.j * 2) - 113;
                }
            }
            super.onSignalStrengthsChanged(signalStrength);
        } catch (Exception e) {
        }
    }
}
