package com.f.a;

import b.a.Cif;

public class j extends m {

    /* renamed from: a  reason: collision with root package name */
    private long f856a = 10000;

    /* renamed from: b  reason: collision with root package name */
    private long f857b;
    private Cif c;

    public j(Cif ifVar, long j) {
        this.c = ifVar;
        this.f857b = j < this.f856a ? this.f856a : j;
    }

    public boolean a(boolean z) {
        return System.currentTimeMillis() - this.c.c >= this.f857b;
    }
}
