package com.f.a;

import android.content.Context;
import b.a.fl;

public class a {

    /* renamed from: a  reason: collision with root package name */
    public static String f843a = null;

    /* renamed from: b  reason: collision with root package name */
    public static String f844b = null;
    public static int c;
    public static String d = "";
    public static String e = "";
    public static b f;
    public static int g;
    public static String h;
    public static String i;
    public static boolean j = true;
    public static boolean k = true;
    public static boolean l = true;
    public static boolean m = true;
    public static long n = 30000;
    private static String o = null;
    private static String p = null;
    private static double[] q = null;
    private static int[] r;

    public static String a(Context context) {
        if (o == null) {
            o = fl.j(context);
        }
        return o;
    }

    public static void a(int i2, int i3) {
        if (r == null) {
            r = new int[2];
        }
        r[0] = i2;
        r[1] = i3;
    }

    public static double[] a() {
        return q;
    }

    public static String b(Context context) {
        if (p == null) {
            p = fl.n(context);
        }
        return p;
    }

    public static int[] c(Context context) {
        if (r == null) {
            r = v.a(context).a();
        }
        return r;
    }
}
