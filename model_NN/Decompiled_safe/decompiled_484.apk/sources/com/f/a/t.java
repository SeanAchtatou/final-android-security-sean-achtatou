package com.f.a;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class t {

    /* renamed from: a  reason: collision with root package name */
    private static ExecutorService f871a = Executors.newSingleThreadExecutor();

    /* renamed from: b  reason: collision with root package name */
    private static long f872b = 5;
    private static ExecutorService c = Executors.newSingleThreadExecutor();

    public static void a() {
        try {
            if (!f871a.isShutdown()) {
                f871a.shutdown();
            }
            if (!c.isShutdown()) {
                c.shutdown();
            }
            f871a.awaitTermination(f872b, TimeUnit.SECONDS);
            c.awaitTermination(f872b, TimeUnit.SECONDS);
        } catch (Exception e) {
        }
    }

    public static void a(Runnable runnable) {
        if (f871a.isShutdown()) {
            f871a = Executors.newSingleThreadExecutor();
        }
        f871a.execute(runnable);
    }

    public static void b(Runnable runnable) {
        if (c.isShutdown()) {
            c = Executors.newSingleThreadExecutor();
        }
        c.execute(runnable);
    }
}
