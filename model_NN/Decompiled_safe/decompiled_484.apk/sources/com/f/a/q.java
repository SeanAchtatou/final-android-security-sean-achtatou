package com.f.a;

import android.content.Context;
import b.a.c;
import b.a.fm;
import b.a.hs;
import b.a.hw;
import b.a.hx;
import b.a.ic;
import b.a.id;
import b.a.ie;
import b.a.ii;
import com.f.a.a.b;

public class q implements ic {

    /* renamed from: a  reason: collision with root package name */
    private final b f865a = new b();

    /* renamed from: b  reason: collision with root package name */
    private Context f866b = null;
    private p c;
    private hw d = new hw();
    private ii e = new ii();
    private ie f = new ie();
    private hx g;
    private hs h;
    private boolean i = false;

    q() {
        this.d.a(this);
    }

    private void e(Context context) {
        if (!this.i) {
            this.f866b = context.getApplicationContext();
            this.g = new hx(this.f866b);
            this.h = hs.a(this.f866b);
            this.i = true;
        }
    }

    /* access modifiers changed from: private */
    public void f(Context context) {
        this.f.c(context);
        if (this.c != null) {
            this.c.a();
        }
    }

    /* access modifiers changed from: private */
    public void g(Context context) {
        this.f.d(context);
        this.e.a(context);
        if (this.c != null) {
            this.c.b();
        }
        this.h.a();
    }

    /* access modifiers changed from: package-private */
    public void a(Context context) {
        if (context == null) {
            fm.b("MobclickAgent", "unexpected null context in onResume");
            return;
        }
        this.f865a.a(context);
        try {
            hs.a(context).a(this.f865a);
        } catch (Exception e2) {
        }
    }

    public void a(Context context, String str, String str2, long j, int i2) {
        try {
            if (!this.i) {
                e(context);
            }
            this.g.a(str, str2, j, i2);
        } catch (Exception e2) {
            fm.b("MobclickAgent", "", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        if (!a.j) {
            try {
                this.e.a(str);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void a(Throwable th) {
        try {
            this.e.a();
            if (this.f866b != null) {
                if (!(th == null || this.h == null)) {
                    this.h.b(new c(th));
                }
                g(this.f866b);
                id.a(this.f866b).edit().commit();
            }
            t.a();
        } catch (Exception e2) {
            fm.a("MobclickAgent", "Exception in onAppCrash", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Context context) {
        if (context == null) {
            fm.b("MobclickAgent", "unexpected null context in onResume");
            return;
        }
        if (a.j) {
            this.e.a(context.getClass().getName());
        }
        try {
            if (!this.i) {
                e(context);
            }
            t.a(new r(this, context));
        } catch (Exception e2) {
            fm.b("MobclickAgent", "Exception occurred in Mobclick.onResume(). ", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        if (!a.j) {
            try {
                this.e.b(str);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void c(Context context) {
        if (context == null) {
            fm.b("MobclickAgent", "unexpected null context in onPause");
            return;
        }
        if (a.j) {
            this.e.b(context.getClass().getName());
        }
        try {
            if (!this.i) {
                e(context);
            }
            t.a(new s(this, context));
        } catch (Exception e2) {
            fm.b("MobclickAgent", "Exception occurred in Mobclick.onRause(). ", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void d(Context context) {
        try {
            this.e.a();
            g(context);
            id.a(context).edit().commit();
            t.a();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
