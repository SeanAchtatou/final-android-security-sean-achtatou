package com.f.a.a;

import b.a.fm;
import b.a.fp;
import com.qihoo.messenger.util.QDefine;
import java.util.Locale;
import org.json.JSONObject;

public class e extends fp {

    /* renamed from: a  reason: collision with root package name */
    public JSONObject f850a = null;

    /* renamed from: b  reason: collision with root package name */
    boolean f851b = false;
    int c = -1;
    int d = -1;
    String e;
    private final String f = "config_update";
    private final String g = "report_policy";
    private final String h = "online_params";
    private final String i = "last_config_time";
    private final String j = "report_interval";

    public e(JSONObject jSONObject) {
        super(jSONObject);
        if (jSONObject != null) {
            a(jSONObject);
            a();
        }
    }

    private void a() {
        if (this.c < 0 || this.c > 6) {
            this.c = 1;
        }
    }

    private void a(JSONObject jSONObject) {
        try {
            if (jSONObject.has("config_update") && !jSONObject.getString("config_update").toLowerCase(Locale.US).equals("no")) {
                if (jSONObject.has("report_policy")) {
                    this.c = jSONObject.getInt("report_policy");
                    this.d = jSONObject.optInt("report_interval") * QDefine.ONE_SECOND;
                    this.e = jSONObject.optString("last_config_time");
                } else {
                    fm.d("MobclickAgent", " online config fetch no report policy");
                }
                this.f850a = jSONObject.optJSONObject("online_params");
                this.f851b = true;
            }
        } catch (Exception e2) {
            fm.d("MobclickAgent", "fail to parce online config response", e2);
        }
    }
}
