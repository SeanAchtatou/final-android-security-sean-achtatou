package com.f.a.a;

import android.content.Context;
import b.a.fm;
import b.a.fn;
import com.f.a.o;
import org.json.JSONObject;

public class d extends fn implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    Context f848a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ b f849b;

    public d(b bVar, Context context) {
        this.f849b = bVar;
        this.f848a = context.getApplicationContext();
    }

    private void b() {
        c cVar = new c(this.f849b, this.f849b.b(this.f848a));
        String[] strArr = o.f864b;
        e eVar = null;
        for (String a2 : strArr) {
            cVar.a(a2);
            eVar = (e) a(cVar, e.class);
            if (eVar != null) {
                break;
            }
        }
        if (eVar == null) {
            this.f849b.a((JSONObject) null);
        } else if (eVar.f851b) {
            if (this.f849b.e != null) {
                this.f849b.e.a(eVar.c, (long) eVar.d);
            }
            this.f849b.a(this.f848a, eVar);
            this.f849b.b(this.f848a, eVar);
            this.f849b.a(eVar.f850a);
        } else {
            this.f849b.a((JSONObject) null);
        }
    }

    public boolean a() {
        return false;
    }

    public void run() {
        try {
            b();
        } catch (Exception e) {
            this.f849b.a((JSONObject) null);
            fm.c("MobclickAgent", "reques update error", e);
        }
    }
}
