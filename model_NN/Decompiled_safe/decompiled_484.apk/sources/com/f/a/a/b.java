package com.f.a.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import b.a.fl;
import b.a.fm;
import b.a.fr;
import com.f.a.a;
import com.f.a.v;
import java.util.Iterator;
import org.json.JSONObject;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private final String f845a = "last_config_time";

    /* renamed from: b  reason: collision with root package name */
    private final String f846b = "report_policy";
    private final String c = "online_config";
    private a d = null;
    /* access modifiers changed from: private */
    public f e = null;
    private long f = 0;

    /* access modifiers changed from: private */
    public void a(Context context, e eVar) {
        SharedPreferences.Editor edit = v.a(context).g().edit();
        if (!TextUtils.isEmpty(eVar.e)) {
            edit.putString("umeng_last_config_time", eVar.e);
            edit.commit();
        }
        if (eVar.c != -1) {
            v.a(context).a(eVar.c, eVar.d);
        }
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject) {
        if (this.d != null) {
            this.d.a(jSONObject);
        }
    }

    /* access modifiers changed from: private */
    public JSONObject b(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            getClass();
            jSONObject.put("type", "online_config");
            jSONObject.put("appkey", a.a(context));
            jSONObject.put("version_code", fl.a(context));
            jSONObject.put("package", fl.o(context));
            jSONObject.put("sdk_version", "5.2.4");
            jSONObject.put("idmd5", fr.b(fl.c(context)));
            jSONObject.put("channel", a.b(context));
            jSONObject.put("report_policy", v.a(context).a()[0]);
            jSONObject.put("last_config_time", c(context));
            return jSONObject;
        } catch (Exception e2) {
            fm.b("MobclickAgent", "exception in onlineConfigInternal");
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void b(Context context, e eVar) {
        if (eVar.f850a != null && eVar.f850a.length() != 0) {
            SharedPreferences.Editor edit = v.a(context).g().edit();
            try {
                JSONObject jSONObject = eVar.f850a;
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    edit.putString(next, jSONObject.getString(next));
                }
                edit.commit();
                fm.a("MobclickAgent", "get online setting params: " + jSONObject);
            } catch (Exception e2) {
                fm.c("MobclickAgent", "save online config params", e2);
            }
        }
    }

    private String c(Context context) {
        return v.a(context).g().getString("umeng_last_config_time", "");
    }

    public void a(Context context) {
        if (context == null) {
            try {
                fm.b("MobclickAgent", "unexpected null context in updateOnlineConfig");
            } catch (Exception e2) {
                fm.b("MobclickAgent", "exception in updateOnlineConfig");
            }
        } else if (!fm.f137a || !fl.q(context)) {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - this.f > 3600000) {
                this.f = currentTimeMillis;
                new Thread(new d(this, context.getApplicationContext())).start();
            }
        } else {
            new Thread(new d(this, context.getApplicationContext())).start();
        }
    }

    public void a(f fVar) {
        this.e = fVar;
    }
}
