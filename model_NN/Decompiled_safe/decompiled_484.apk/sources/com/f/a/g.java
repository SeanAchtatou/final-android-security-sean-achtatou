package com.f.a;

import android.content.Context;
import android.text.TextUtils;
import b.a.fm;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private static final q f855a = new q();

    public static void a(Context context) {
        f855a.c(context);
    }

    public static void a(Context context, String str) {
        f855a.a(context, str, null, -1, 1);
    }

    public static void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            f855a.a(str);
        } else {
            fm.b("MobclickAgent", "pageName is null or empty");
        }
    }

    public static void a(boolean z) {
    }

    public static void b(Context context) {
        if (context == null) {
            fm.b("MobclickAgent", "unexpected null context in onResume");
        } else {
            f855a.b(context);
        }
    }

    public static void b(String str) {
        if (!TextUtils.isEmpty(str)) {
            f855a.b(str);
        } else {
            fm.b("MobclickAgent", "pageName is null or empty");
        }
    }

    public static void b(boolean z) {
        a.j = z;
    }

    public static void c(Context context) {
        f855a.a(context);
    }

    public static void d(Context context) {
        f855a.d(context);
    }
}
