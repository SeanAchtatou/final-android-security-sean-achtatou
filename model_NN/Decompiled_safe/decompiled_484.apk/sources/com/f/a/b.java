package com.f.a;

import b.a.ax;

public enum b {
    Male(1),
    Female(2),
    Unknown(0);
    
    public int d;

    private b(int i) {
        this.d = i;
    }

    public static ax a(b bVar) {
        switch (f.f854a[bVar.ordinal()]) {
            case 1:
                return ax.MALE;
            case 2:
                return ax.FEMALE;
            default:
                return ax.UNKNOWN;
        }
    }
}
