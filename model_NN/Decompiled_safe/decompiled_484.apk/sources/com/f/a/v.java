package com.f.a;

import android.content.Context;
import android.content.SharedPreferences;
import b.a.fl;
import b.a.fm;
import b.a.fr;
import java.io.File;
import java.io.FileInputStream;

public final class v {

    /* renamed from: a  reason: collision with root package name */
    private static v f873a = new v();

    /* renamed from: b  reason: collision with root package name */
    private static Context f874b;
    private static String c;
    private static long d = 1209600000;
    private static long e = 2097152;

    public static v a(Context context) {
        if (f874b == null) {
            f874b = context.getApplicationContext();
        }
        if (c == null) {
            c = context.getPackageName();
        }
        return f873a;
    }

    private static boolean a(File file) {
        return file.exists() && file.length() > e;
    }

    private String h() {
        return "mobclick_agent_header_" + c;
    }

    private String i() {
        return "mobclick_agent_cached_" + c + fl.a(f874b);
    }

    private String j() {
        return "mobclick_agent_sealed_" + c;
    }

    public void a(int i, int i2) {
        SharedPreferences.Editor edit = a(f874b).g().edit();
        edit.putInt("umeng_net_report_policy", i);
        edit.putLong("umeng_net_report_interval", (long) i2);
        edit.commit();
    }

    public void a(byte[] bArr) {
        try {
            fr.a(new File(f874b.getFilesDir(), i()), bArr);
        } catch (Exception e2) {
            fm.b("MobclickAgent", e2.getMessage());
        }
    }

    public int[] a() {
        SharedPreferences g = g();
        int[] iArr = new int[2];
        if (g.getInt("umeng_net_report_policy", -1) != -1) {
            iArr[0] = g.getInt("umeng_net_report_policy", 1);
            iArr[1] = (int) g.getLong("umeng_net_report_interval", 0);
        } else {
            iArr[0] = g.getInt("umeng_local_report_policy", 1);
            iArr[1] = (int) g.getLong("umeng_local_report_interval", 0);
        }
        return iArr;
    }

    public void b(byte[] bArr) {
        try {
            fr.a(new File(f874b.getFilesDir(), j()), bArr);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public byte[] b() {
        FileInputStream fileInputStream;
        Throwable th;
        byte[] bArr = null;
        String i = i();
        File file = new File(f874b.getFilesDir(), i);
        if (a(file)) {
            file.delete();
        } else if (file.exists()) {
            try {
                fileInputStream = f874b.openFileInput(i);
                try {
                    bArr = fr.b(fileInputStream);
                    fr.c(fileInputStream);
                } catch (Exception e2) {
                    e = e2;
                    try {
                        e.printStackTrace();
                        fr.c(fileInputStream);
                        return bArr;
                    } catch (Throwable th2) {
                        th = th2;
                        fr.c(fileInputStream);
                        throw th;
                    }
                }
            } catch (Exception e3) {
                e = e3;
                fileInputStream = null;
            } catch (Throwable th3) {
                fileInputStream = null;
                th = th3;
                fr.c(fileInputStream);
                throw th;
            }
        }
        return bArr;
    }

    public void c() {
        f874b.deleteFile(h());
        f874b.deleteFile(i());
    }

    public byte[] d() {
        FileInputStream fileInputStream;
        String j = j();
        File file = new File(f874b.getFilesDir(), j);
        try {
            if (!file.exists() || file.length() <= 0) {
                return null;
            }
            try {
                fileInputStream = f874b.openFileInput(j);
                try {
                    byte[] b2 = fr.b(fileInputStream);
                    fr.c(fileInputStream);
                    return b2;
                } catch (Exception e2) {
                    e = e2;
                }
            } catch (Exception e3) {
                e = e3;
                fileInputStream = null;
            } catch (Throwable th) {
                th = th;
                fileInputStream = null;
                fr.c(fileInputStream);
                throw th;
            }
            return null;
            try {
                e.printStackTrace();
                fr.c(fileInputStream);
                return null;
            } catch (Throwable th2) {
                th = th2;
                fr.c(fileInputStream);
                throw th;
            }
        } catch (Exception e4) {
            file.delete();
            e4.printStackTrace();
        }
    }

    public void e() {
        fm.a("--->", "delete envelope:" + f874b.deleteFile(j()));
    }

    public boolean f() {
        File file = new File(f874b.getFilesDir(), j());
        return file.exists() && file.length() > 0;
    }

    public SharedPreferences g() {
        return f874b.getSharedPreferences("mobclick_agent_online_setting_" + c, 0);
    }
}
