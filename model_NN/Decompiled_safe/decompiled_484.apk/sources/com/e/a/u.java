package com.e.a;

import com.e.a.a.q;
import com.e.a.a.v;
import com.qihoo.messenger.util.QDefine;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class u {

    /* renamed from: a  reason: collision with root package name */
    private static final u f760a;

    /* renamed from: b  reason: collision with root package name */
    private final int f761b;
    private final long c;
    private final LinkedList<t> d = new LinkedList<>();
    private Executor e = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), v.a("OkHttp ConnectionPool", true));
    private final Runnable f = new v(this);

    static {
        String property = System.getProperty("http.keepAlive");
        String property2 = System.getProperty("http.keepAliveDuration");
        String property3 = System.getProperty("http.maxConnections");
        long parseLong = property2 != null ? Long.parseLong(property2) : QDefine.WIFI_ACCESS_PERIOD;
        if (property != null && !Boolean.parseBoolean(property)) {
            f760a = new u(0, parseLong);
        } else if (property3 != null) {
            f760a = new u(Integer.parseInt(property3), parseLong);
        } else {
            f760a = new u(5, parseLong);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.v.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
     arg types: [java.lang.String, int]
     candidates:
      com.e.a.a.v.a(java.lang.String, int):int
      com.e.a.a.v.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
      com.e.a.a.v.a(java.io.Closeable, java.io.Closeable):void
      com.e.a.a.v.a(java.lang.Object, java.lang.Object):boolean
      com.e.a.a.v.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory */
    public u(int i, long j) {
        this.f761b = i;
        this.c = j * 1000 * 1000;
    }

    public static u a() {
        return f760a;
    }

    /* access modifiers changed from: private */
    public void c() {
        do {
        } while (b());
    }

    private void c(t tVar) {
        boolean isEmpty = this.d.isEmpty();
        this.d.addFirst(tVar);
        if (isEmpty) {
            this.e.execute(this.f);
        } else {
            notifyAll();
        }
    }

    public synchronized t a(a aVar) {
        t tVar;
        ListIterator<t> listIterator = this.d.listIterator(this.d.size());
        while (true) {
            if (!listIterator.hasPrevious()) {
                tVar = null;
                break;
            }
            tVar = listIterator.previous();
            if (tVar.c().a().equals(aVar) && tVar.e() && System.nanoTime() - tVar.i() < this.c) {
                listIterator.remove();
                if (tVar.k()) {
                    break;
                }
                try {
                    q.a().a(tVar.d());
                    break;
                } catch (SocketException e2) {
                    v.a(tVar.d());
                    q.a().a("Unable to tagSocket(): " + e2);
                }
            }
        }
        if (tVar != null) {
            if (tVar.k()) {
                this.d.addFirst(tVar);
            }
        }
        return tVar;
    }

    /* access modifiers changed from: package-private */
    public void a(t tVar) {
        if (tVar.k() || !tVar.a()) {
            return;
        }
        if (!tVar.e()) {
            v.a(tVar.d());
            return;
        }
        try {
            q.a().b(tVar.d());
            synchronized (this) {
                c(tVar);
                tVar.m();
                tVar.g();
            }
        } catch (SocketException e2) {
            q.a().a("Unable to untagSocket(): " + e2);
            v.a(tVar.d());
        }
    }

    /* access modifiers changed from: package-private */
    public void b(t tVar) {
        if (!tVar.k()) {
            throw new IllegalArgumentException();
        } else if (tVar.e()) {
            synchronized (this) {
                c(tVar);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00be, code lost:
        r4 = r7.size();
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00c4, code lost:
        if (r3 >= r4) goto L_0x00d7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00c6, code lost:
        com.e.a.a.v.a(((com.e.a.t) r7.get(r3)).d());
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00d7, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean b() {
        /*
            r18 = this;
            monitor-enter(r18)
            r0 = r18
            java.util.LinkedList<com.e.a.t> r2 = r0.d     // Catch:{ all -> 0x00b9 }
            boolean r2 = r2.isEmpty()     // Catch:{ all -> 0x00b9 }
            if (r2 == 0) goto L_0x000e
            r2 = 0
            monitor-exit(r18)     // Catch:{ all -> 0x00b9 }
        L_0x000d:
            return r2
        L_0x000e:
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ all -> 0x00b9 }
            r7.<init>()     // Catch:{ all -> 0x00b9 }
            r3 = 0
            long r8 = java.lang.System.nanoTime()     // Catch:{ all -> 0x00b9 }
            r0 = r18
            long r4 = r0.c     // Catch:{ all -> 0x00b9 }
            r0 = r18
            java.util.LinkedList<com.e.a.t> r2 = r0.d     // Catch:{ all -> 0x00b9 }
            r0 = r18
            java.util.LinkedList<com.e.a.t> r6 = r0.d     // Catch:{ all -> 0x00b9 }
            int r6 = r6.size()     // Catch:{ all -> 0x00b9 }
            java.util.ListIterator r10 = r2.listIterator(r6)     // Catch:{ all -> 0x00b9 }
        L_0x002c:
            boolean r2 = r10.hasPrevious()     // Catch:{ all -> 0x00b9 }
            if (r2 == 0) goto L_0x006d
            java.lang.Object r2 = r10.previous()     // Catch:{ all -> 0x00b9 }
            com.e.a.t r2 = (com.e.a.t) r2     // Catch:{ all -> 0x00b9 }
            long r12 = r2.i()     // Catch:{ all -> 0x00b9 }
            r0 = r18
            long r14 = r0.c     // Catch:{ all -> 0x00b9 }
            long r12 = r12 + r14
            long r12 = r12 - r8
            r14 = 0
            int r6 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r6 <= 0) goto L_0x004e
            boolean r6 = r2.e()     // Catch:{ all -> 0x00b9 }
            if (r6 != 0) goto L_0x005f
        L_0x004e:
            r10.remove()     // Catch:{ all -> 0x00b9 }
            r7.add(r2)     // Catch:{ all -> 0x00b9 }
            r16 = r4
            r4 = r3
            r2 = r16
        L_0x0059:
            r16 = r2
            r3 = r4
            r4 = r16
            goto L_0x002c
        L_0x005f:
            boolean r2 = r2.h()     // Catch:{ all -> 0x00b9 }
            if (r2 == 0) goto L_0x00dc
            int r6 = r3 + 1
            long r2 = java.lang.Math.min(r4, r12)     // Catch:{ all -> 0x00b9 }
            r4 = r6
            goto L_0x0059
        L_0x006d:
            r0 = r18
            java.util.LinkedList<com.e.a.t> r2 = r0.d     // Catch:{ all -> 0x00b9 }
            r0 = r18
            java.util.LinkedList<com.e.a.t> r6 = r0.d     // Catch:{ all -> 0x00b9 }
            int r6 = r6.size()     // Catch:{ all -> 0x00b9 }
            java.util.ListIterator r6 = r2.listIterator(r6)     // Catch:{ all -> 0x00b9 }
        L_0x007d:
            boolean r2 = r6.hasPrevious()     // Catch:{ all -> 0x00b9 }
            if (r2 == 0) goto L_0x009f
            r0 = r18
            int r2 = r0.f761b     // Catch:{ all -> 0x00b9 }
            if (r3 <= r2) goto L_0x009f
            java.lang.Object r2 = r6.previous()     // Catch:{ all -> 0x00b9 }
            com.e.a.t r2 = (com.e.a.t) r2     // Catch:{ all -> 0x00b9 }
            boolean r8 = r2.h()     // Catch:{ all -> 0x00b9 }
            if (r8 == 0) goto L_0x00da
            r7.add(r2)     // Catch:{ all -> 0x00b9 }
            r6.remove()     // Catch:{ all -> 0x00b9 }
            int r2 = r3 + -1
        L_0x009d:
            r3 = r2
            goto L_0x007d
        L_0x009f:
            boolean r2 = r7.isEmpty()     // Catch:{ all -> 0x00b9 }
            if (r2 == 0) goto L_0x00bd
            r2 = 1000000(0xf4240, double:4.940656E-318)
            long r2 = r4 / r2
            r8 = 1000000(0xf4240, double:4.940656E-318)
            long r8 = r8 * r2
            long r4 = r4 - r8
            int r4 = (int) r4     // Catch:{ InterruptedException -> 0x00bc }
            r0 = r18
            r0.wait(r2, r4)     // Catch:{ InterruptedException -> 0x00bc }
            r2 = 1
            monitor-exit(r18)     // Catch:{ all -> 0x00b9 }
            goto L_0x000d
        L_0x00b9:
            r2 = move-exception
            monitor-exit(r18)     // Catch:{ all -> 0x00b9 }
            throw r2
        L_0x00bc:
            r2 = move-exception
        L_0x00bd:
            monitor-exit(r18)     // Catch:{ all -> 0x00b9 }
            r2 = 0
            int r4 = r7.size()
            r3 = r2
        L_0x00c4:
            if (r3 >= r4) goto L_0x00d7
            java.lang.Object r2 = r7.get(r3)
            com.e.a.t r2 = (com.e.a.t) r2
            java.net.Socket r2 = r2.d()
            com.e.a.a.v.a(r2)
            int r2 = r3 + 1
            r3 = r2
            goto L_0x00c4
        L_0x00d7:
            r2 = 1
            goto L_0x000d
        L_0x00da:
            r2 = r3
            goto L_0x009d
        L_0x00dc:
            r16 = r4
            r4 = r3
            r2 = r16
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: com.e.a.u.b():boolean");
    }
}
