package com.e.a;

import com.e.a.a.v;
import java.net.Proxy;
import java.net.ProxySelector;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    final Proxy f556a;

    /* renamed from: b  reason: collision with root package name */
    final String f557b;
    final int c;
    final SocketFactory d;
    final SSLSocketFactory e;
    final HostnameVerifier f;
    final o g;
    final b h;
    final List<ao> i;
    final List<w> j;
    final ProxySelector k;

    public a(String str, int i2, SocketFactory socketFactory, SSLSocketFactory sSLSocketFactory, HostnameVerifier hostnameVerifier, o oVar, b bVar, Proxy proxy, List<ao> list, List<w> list2, ProxySelector proxySelector) {
        if (str == null) {
            throw new NullPointerException("uriHost == null");
        } else if (i2 <= 0) {
            throw new IllegalArgumentException("uriPort <= 0: " + i2);
        } else if (bVar == null) {
            throw new IllegalArgumentException("authenticator == null");
        } else if (list == null) {
            throw new IllegalArgumentException("protocols == null");
        } else if (proxySelector == null) {
            throw new IllegalArgumentException("proxySelector == null");
        } else {
            this.f556a = proxy;
            this.f557b = str;
            this.c = i2;
            this.d = socketFactory;
            this.e = sSLSocketFactory;
            this.f = hostnameVerifier;
            this.g = oVar;
            this.h = bVar;
            this.i = v.a(list);
            this.j = v.a(list2);
            this.k = proxySelector;
        }
    }

    public String a() {
        return this.f557b;
    }

    public int b() {
        return this.c;
    }

    public SocketFactory c() {
        return this.d;
    }

    public SSLSocketFactory d() {
        return this.e;
    }

    public HostnameVerifier e() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        return v.a(this.f556a, aVar.f556a) && this.f557b.equals(aVar.f557b) && this.c == aVar.c && v.a(this.e, aVar.e) && v.a(this.f, aVar.f) && v.a(this.g, aVar.g) && v.a(this.h, aVar.h) && v.a(this.i, aVar.i) && v.a(this.j, aVar.j) && v.a(this.k, aVar.k);
    }

    public b f() {
        return this.h;
    }

    public List<ao> g() {
        return this.i;
    }

    public List<w> h() {
        return this.j;
    }

    public int hashCode() {
        int i2 = 0;
        int hashCode = ((this.f != null ? this.f.hashCode() : 0) + (((this.e != null ? this.e.hashCode() : 0) + (((((((this.f556a != null ? this.f556a.hashCode() : 0) + 527) * 31) + this.f557b.hashCode()) * 31) + this.c) * 31)) * 31)) * 31;
        if (this.g != null) {
            i2 = this.g.hashCode();
        }
        return ((((((((hashCode + i2) * 31) + this.h.hashCode()) * 31) + this.i.hashCode()) * 31) + this.j.hashCode()) * 31) + this.k.hashCode();
    }

    public Proxy i() {
        return this.f556a;
    }

    public ProxySelector j() {
        return this.k;
    }

    public o k() {
        return this.g;
    }
}
