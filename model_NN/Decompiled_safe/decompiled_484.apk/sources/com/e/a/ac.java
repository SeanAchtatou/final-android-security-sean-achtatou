package com.e.a;

import com.e.a.a.v;
import java.security.cert.Certificate;
import java.util.Collections;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;

public final class ac {

    /* renamed from: a  reason: collision with root package name */
    private final String f702a;

    /* renamed from: b  reason: collision with root package name */
    private final List<Certificate> f703b;
    private final List<Certificate> c;

    private ac(String str, List<Certificate> list, List<Certificate> list2) {
        this.f702a = str;
        this.f703b = list;
        this.c = list2;
    }

    public static ac a(String str, List<Certificate> list, List<Certificate> list2) {
        if (str != null) {
            return new ac(str, v.a(list), v.a(list2));
        }
        throw new IllegalArgumentException("cipherSuite == null");
    }

    public static ac a(SSLSession sSLSession) {
        Certificate[] certificateArr;
        String cipherSuite = sSLSession.getCipherSuite();
        if (cipherSuite == null) {
            throw new IllegalStateException("cipherSuite == null");
        }
        try {
            certificateArr = sSLSession.getPeerCertificates();
        } catch (SSLPeerUnverifiedException e) {
            certificateArr = null;
        }
        List a2 = certificateArr != null ? v.a(certificateArr) : Collections.emptyList();
        Certificate[] localCertificates = sSLSession.getLocalCertificates();
        return new ac(cipherSuite, a2, localCertificates != null ? v.a(localCertificates) : Collections.emptyList());
    }

    public String a() {
        return this.f702a;
    }

    public List<Certificate> b() {
        return this.f703b;
    }

    public List<Certificate> c() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ac)) {
            return false;
        }
        ac acVar = (ac) obj;
        return this.f702a.equals(acVar.f702a) && this.f703b.equals(acVar.f703b) && this.c.equals(acVar.c);
    }

    public int hashCode() {
        return ((((this.f702a.hashCode() + 527) * 31) + this.f703b.hashCode()) * 31) + this.c.hashCode();
    }
}
