package com.e.a;

import com.e.a.a.a.a;
import com.e.a.a.d.b;
import com.e.a.a.k;
import com.e.a.a.l;
import com.e.a.a.n;
import com.e.a.a.u;
import com.e.a.a.v;
import java.net.CookieHandler;
import java.net.Proxy;
import java.net.ProxySelector;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

public class am implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private static final List<ao> f712a = v.a(ao.HTTP_2, ao.SPDY_3, ao.HTTP_1_1);

    /* renamed from: b  reason: collision with root package name */
    private static final List<w> f713b = v.a(w.f763a, w.f764b, w.c);
    private static SSLSocketFactory c;
    private int A;
    private final u d;
    private aa e;
    private Proxy f;
    private List<ao> g;
    private List<w> h;
    private final List<aj> i;
    private final List<aj> j;
    private ProxySelector k;
    private CookieHandler l;
    private l m;
    private c n;
    private SocketFactory o;
    private SSLSocketFactory p;
    private HostnameVerifier q;
    private o r;
    private b s;
    private u t;
    /* access modifiers changed from: private */
    public n u;
    private boolean v;
    private boolean w;
    private boolean x;
    private int y;
    private int z;

    static {
        k.f681b = new an();
    }

    public am() {
        this.i = new ArrayList();
        this.j = new ArrayList();
        this.v = true;
        this.w = true;
        this.x = true;
        this.d = new u();
        this.e = new aa();
    }

    private am(am amVar) {
        this.i = new ArrayList();
        this.j = new ArrayList();
        this.v = true;
        this.w = true;
        this.x = true;
        this.d = amVar.d;
        this.e = amVar.e;
        this.f = amVar.f;
        this.g = amVar.g;
        this.h = amVar.h;
        this.i.addAll(amVar.i);
        this.j.addAll(amVar.j);
        this.k = amVar.k;
        this.l = amVar.l;
        this.n = amVar.n;
        this.m = this.n != null ? this.n.f730a : amVar.m;
        this.o = amVar.o;
        this.p = amVar.p;
        this.q = amVar.q;
        this.r = amVar.r;
        this.s = amVar.s;
        this.t = amVar.t;
        this.u = amVar.u;
        this.v = amVar.v;
        this.w = amVar.w;
        this.x = amVar.x;
        this.y = amVar.y;
        this.z = amVar.z;
        this.A = amVar.A;
    }

    private synchronized SSLSocketFactory y() {
        if (c == null) {
            try {
                SSLContext instance = SSLContext.getInstance("TLS");
                instance.init(null, null, null);
                c = instance.getSocketFactory();
            } catch (GeneralSecurityException e2) {
                throw new AssertionError();
            }
        }
        return c;
    }

    public int a() {
        return this.y;
    }

    public am a(c cVar) {
        this.n = cVar;
        this.m = null;
        return this;
    }

    public m a(ap apVar) {
        return new m(this, apVar);
    }

    public void a(long j2, TimeUnit timeUnit) {
        if (j2 < 0) {
            throw new IllegalArgumentException("timeout < 0");
        } else if (timeUnit == null) {
            throw new IllegalArgumentException("unit == null");
        } else {
            long millis = timeUnit.toMillis(j2);
            if (millis > 2147483647L) {
                throw new IllegalArgumentException("Timeout too large.");
            } else if (millis != 0 || j2 <= 0) {
                this.y = (int) millis;
            } else {
                throw new IllegalArgumentException("Timeout too small.");
            }
        }
    }

    public void a(boolean z2) {
        this.x = z2;
    }

    public int b() {
        return this.z;
    }

    public void b(long j2, TimeUnit timeUnit) {
        if (j2 < 0) {
            throw new IllegalArgumentException("timeout < 0");
        } else if (timeUnit == null) {
            throw new IllegalArgumentException("unit == null");
        } else {
            long millis = timeUnit.toMillis(j2);
            if (millis > 2147483647L) {
                throw new IllegalArgumentException("Timeout too large.");
            } else if (millis != 0 || j2 <= 0) {
                this.z = (int) millis;
            } else {
                throw new IllegalArgumentException("Timeout too small.");
            }
        }
    }

    public int c() {
        return this.A;
    }

    public void c(long j2, TimeUnit timeUnit) {
        if (j2 < 0) {
            throw new IllegalArgumentException("timeout < 0");
        } else if (timeUnit == null) {
            throw new IllegalArgumentException("unit == null");
        } else {
            long millis = timeUnit.toMillis(j2);
            if (millis > 2147483647L) {
                throw new IllegalArgumentException("Timeout too large.");
            } else if (millis != 0 || j2 <= 0) {
                this.A = (int) millis;
            } else {
                throw new IllegalArgumentException("Timeout too small.");
            }
        }
    }

    public Proxy d() {
        return this.f;
    }

    public ProxySelector e() {
        return this.k;
    }

    public CookieHandler f() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public l g() {
        return this.m;
    }

    public SocketFactory h() {
        return this.o;
    }

    public SSLSocketFactory i() {
        return this.p;
    }

    public HostnameVerifier j() {
        return this.q;
    }

    public o k() {
        return this.r;
    }

    public b l() {
        return this.s;
    }

    public u m() {
        return this.t;
    }

    public boolean n() {
        return this.v;
    }

    public boolean o() {
        return this.w;
    }

    public boolean p() {
        return this.x;
    }

    /* access modifiers changed from: package-private */
    public u q() {
        return this.d;
    }

    public aa r() {
        return this.e;
    }

    public List<ao> s() {
        return this.g;
    }

    public List<w> t() {
        return this.h;
    }

    public List<aj> u() {
        return this.i;
    }

    public List<aj> v() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public am w() {
        am amVar = new am(this);
        if (amVar.k == null) {
            amVar.k = ProxySelector.getDefault();
        }
        if (amVar.l == null) {
            amVar.l = CookieHandler.getDefault();
        }
        if (amVar.o == null) {
            amVar.o = SocketFactory.getDefault();
        }
        if (amVar.p == null) {
            amVar.p = y();
        }
        if (amVar.q == null) {
            amVar.q = b.f670a;
        }
        if (amVar.r == null) {
            amVar.r = o.f751a;
        }
        if (amVar.s == null) {
            amVar.s = a.f560a;
        }
        if (amVar.t == null) {
            amVar.t = u.a();
        }
        if (amVar.g == null) {
            amVar.g = f712a;
        }
        if (amVar.h == null) {
            amVar.h = f713b;
        }
        if (amVar.u == null) {
            amVar.u = n.f683a;
        }
        return amVar;
    }

    /* renamed from: x */
    public am clone() {
        return new am(this);
    }
}
