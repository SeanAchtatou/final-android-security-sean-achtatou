package com.e.a;

import a.q;
import com.e.a.a.i;

class g extends ax {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final i f737a;

    /* renamed from: b  reason: collision with root package name */
    private final a.i f738b;
    private final String c;
    private final String d;

    public g(i iVar, String str, String str2) {
        this.f737a = iVar;
        this.c = str;
        this.d = str2;
        this.f738b = q.a(new h(this, iVar.a(1), iVar));
    }

    public al a() {
        if (this.c != null) {
            return al.a(this.c);
        }
        return null;
    }

    public long b() {
        try {
            if (this.d != null) {
                return Long.parseLong(this.d);
            }
            return -1;
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public a.i c() {
        return this.f738b;
    }
}
