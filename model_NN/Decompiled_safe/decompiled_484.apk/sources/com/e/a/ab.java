package com.e.a;

import a.f;

public final class ab {

    /* renamed from: a  reason: collision with root package name */
    private static final al f700a = al.a("application/x-www-form-urlencoded");

    /* renamed from: b  reason: collision with root package name */
    private final f f701b = new f();

    public ab a(String str, String str2) {
        if (this.f701b.b() > 0) {
            this.f701b.i(38);
        }
        ag.a(this.f701b, str, 0, str.length(), " \"'<>#&=", false, true);
        this.f701b.i(61);
        ag.a(this.f701b, str2, 0, str2.length(), " \"'<>#&=", false, true);
        return this;
    }

    public as a() {
        if (this.f701b.b() != 0) {
            return as.a(f700a, this.f701b.v());
        }
        throw new IllegalStateException("Form encoded body must have at least one part.");
    }
}
