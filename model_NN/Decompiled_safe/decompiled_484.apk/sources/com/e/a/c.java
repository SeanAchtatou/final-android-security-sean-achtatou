package com.e.a;

import a.i;
import com.e.a.a.a.u;
import com.e.a.a.a.w;
import com.e.a.a.b;
import com.e.a.a.b.a;
import com.e.a.a.f;
import com.e.a.a.l;
import com.e.a.a.v;
import java.io.File;
import java.io.IOException;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    final l f730a = new d(this);

    /* renamed from: b  reason: collision with root package name */
    private final b f731b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;

    public c(File file, long j) {
        this.f731b = b.a(a.f603a, file, 201105, 2, j);
    }

    /* access modifiers changed from: private */
    public com.e.a.a.a.b a(au auVar) {
        f fVar;
        String d2 = auVar.a().d();
        if (u.a(auVar.a().d())) {
            try {
                c(auVar.a());
                return null;
            } catch (IOException e2) {
                return null;
            }
        } else if (!d2.equals("GET") || w.b(auVar)) {
            return null;
        } else {
            i iVar = new i(auVar);
            try {
                f b2 = this.f731b.b(b(auVar.a()));
                if (b2 == null) {
                    return null;
                }
                try {
                    iVar.a(b2);
                    return new e(this, b2);
                } catch (IOException e3) {
                    fVar = b2;
                    a(fVar);
                    return null;
                }
            } catch (IOException e4) {
                fVar = null;
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a() {
        this.f++;
    }

    /* access modifiers changed from: private */
    public synchronized void a(com.e.a.a.a.c cVar) {
        this.g++;
        if (cVar.f575a != null) {
            this.e++;
        } else if (cVar.f576b != null) {
            this.f++;
        }
    }

    private void a(f fVar) {
        if (fVar != null) {
            try {
                fVar.b();
            } catch (IOException e2) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(au auVar, au auVar2) {
        i iVar = new i(auVar2);
        try {
            f a2 = ((g) auVar.h()).f737a.a();
            if (a2 != null) {
                iVar.a(a2);
                a2.a();
            }
        } catch (IOException e2) {
            a((f) null);
        }
    }

    /* access modifiers changed from: private */
    public static int b(i iVar) {
        try {
            long n = iVar.n();
            String r = iVar.r();
            if (n >= 0 && n <= 2147483647L && r.isEmpty()) {
                return (int) n;
            }
            throw new IOException("expected an int but was \"" + n + r + "\"");
        } catch (NumberFormatException e2) {
            throw new IOException(e2.getMessage());
        }
    }

    static /* synthetic */ int b(c cVar) {
        int i = cVar.c;
        cVar.c = i + 1;
        return i;
    }

    private static String b(ap apVar) {
        return v.b(apVar.c());
    }

    static /* synthetic */ int c(c cVar) {
        int i = cVar.d;
        cVar.d = i + 1;
        return i;
    }

    /* access modifiers changed from: private */
    public void c(ap apVar) {
        this.f731b.c(b(apVar));
    }

    /* access modifiers changed from: package-private */
    public au a(ap apVar) {
        try {
            com.e.a.a.i a2 = this.f731b.a(b(apVar));
            if (a2 == null) {
                return null;
            }
            try {
                i iVar = new i(a2.a(0));
                au a3 = iVar.a(apVar, a2);
                if (iVar.a(apVar, a3)) {
                    return a3;
                }
                v.a(a3.h());
                return null;
            } catch (IOException e2) {
                v.a(a2);
                return null;
            }
        } catch (IOException e3) {
            return null;
        }
    }
}
