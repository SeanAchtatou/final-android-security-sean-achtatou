package com.e.a.a.a;

import a.f;

class n extends i {
    final /* synthetic */ g d;
    private boolean e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private n(g gVar) {
        super(gVar);
        this.d = gVar;
    }

    public long a(f fVar, long j) {
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f582b) {
            throw new IllegalStateException("closed");
        } else if (this.e) {
            return -1;
        } else {
            long a2 = this.d.d.a(fVar, j);
            if (a2 != -1) {
                return a2;
            }
            this.e = true;
            a(false);
            return -1;
        }
    }

    public void close() {
        if (!this.f582b) {
            if (!this.e) {
                b();
            }
            this.f582b = true;
        }
    }
}
