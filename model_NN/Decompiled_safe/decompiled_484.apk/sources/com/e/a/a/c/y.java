package com.e.a.a.c;

import java.util.Arrays;

public final class y {

    /* renamed from: a  reason: collision with root package name */
    private int f663a;

    /* renamed from: b  reason: collision with root package name */
    private int f664b;
    private int c;
    private final int[] d = new int[10];

    /* access modifiers changed from: package-private */
    public y a(int i, int i2, int i3) {
        if (i < this.d.length) {
            int i4 = 1 << i;
            this.f663a |= i4;
            if ((i2 & 1) != 0) {
                this.f664b |= i4;
            } else {
                this.f664b &= i4 ^ -1;
            }
            if ((i2 & 2) != 0) {
                this.c = i4 | this.c;
            } else {
                this.c = (i4 ^ -1) & this.c;
            }
            this.d[i] = i3;
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.c = 0;
        this.f664b = 0;
        this.f663a = 0;
        Arrays.fill(this.d, 0);
    }

    /* access modifiers changed from: package-private */
    public void a(y yVar) {
        for (int i = 0; i < 10; i++) {
            if (yVar.a(i)) {
                a(i, yVar.c(i), yVar.b(i));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i) {
        return ((1 << i) & this.f663a) != 0;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return Integer.bitCount(this.f663a);
    }

    /* access modifiers changed from: package-private */
    public int b(int i) {
        return this.d[i];
    }

    /* access modifiers changed from: package-private */
    public int c() {
        if ((2 & this.f663a) != 0) {
            return this.d[1];
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public int c(int i) {
        int i2 = 0;
        if (g(i)) {
            i2 = 2;
        }
        return f(i) ? i2 | 1 : i2;
    }

    /* access modifiers changed from: package-private */
    public int d(int i) {
        return (32 & this.f663a) != 0 ? this.d[5] : i;
    }

    /* access modifiers changed from: package-private */
    public int e(int i) {
        return (128 & this.f663a) != 0 ? this.d[7] : i;
    }

    /* access modifiers changed from: package-private */
    public boolean f(int i) {
        return ((1 << i) & this.f664b) != 0;
    }

    /* access modifiers changed from: package-private */
    public boolean g(int i) {
        return ((1 << i) & this.c) != 0;
    }
}
