package com.e.a.a.c;

import a.ab;
import a.ac;
import a.f;
import a.i;
import android.support.v7.internal.widget.ActivityChooserView;
import java.util.logging.Level;

final class k implements ab {

    /* renamed from: a  reason: collision with root package name */
    int f643a;

    /* renamed from: b  reason: collision with root package name */
    byte f644b;
    int c;
    int d;
    short e;
    private final i f;

    public k(i iVar) {
        this.f = iVar;
    }

    private void b() {
        int i = this.c;
        int a2 = j.b(this.f);
        this.d = a2;
        this.f643a = a2;
        byte i2 = (byte) (this.f.i() & 255);
        this.f644b = (byte) (this.f.i() & 255);
        if (j.f641a.isLoggable(Level.FINE)) {
            j.f641a.fine(l.a(true, this.c, this.f643a, i2, this.f644b));
        }
        this.c = this.f.k() & ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        if (i2 != 9) {
            throw j.d("%s != TYPE_CONTINUATION", Byte.valueOf(i2));
        } else if (this.c != i) {
            throw j.d("TYPE_CONTINUATION streamId changed", new Object[0]);
        }
    }

    public long a(f fVar, long j) {
        while (this.d == 0) {
            this.f.g((long) this.e);
            this.e = 0;
            if ((this.f644b & 4) != 0) {
                return -1;
            }
            b();
        }
        long a2 = this.f.a(fVar, Math.min(j, (long) this.d));
        if (a2 == -1) {
            return -1;
        }
        this.d = (int) (((long) this.d) - a2);
        return a2;
    }

    public ac a() {
        return this.f.a();
    }

    public void close() {
    }
}
