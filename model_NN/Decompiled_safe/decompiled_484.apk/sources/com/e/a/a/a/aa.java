package com.e.a.a.a;

import com.e.a.ao;
import com.e.a.ap;
import java.net.Proxy;
import java.net.URL;

public final class aa {
    public static String a(ao aoVar) {
        return aoVar == ao.HTTP_1_0 ? "HTTP/1.0" : "HTTP/1.1";
    }

    static String a(ap apVar, Proxy.Type type, ao aoVar) {
        StringBuilder sb = new StringBuilder();
        sb.append(apVar.d());
        sb.append(' ');
        if (a(apVar, type)) {
            sb.append(apVar.a());
        } else {
            sb.append(a(apVar.a()));
        }
        sb.append(' ');
        sb.append(a(aoVar));
        return sb.toString();
    }

    public static String a(URL url) {
        String file = url.getFile();
        return file == null ? "/" : !file.startsWith("/") ? "/" + file : file;
    }

    private static boolean a(ap apVar, Proxy.Type type) {
        return !apVar.i() && type == Proxy.Type.HTTP;
    }
}
