package com.e.a.a.a;

import a.aa;
import a.j;
import a.q;
import com.e.a.a.c.ac;
import com.e.a.a.c.ao;
import com.e.a.a.c.e;
import com.e.a.a.v;
import com.e.a.ad;
import com.e.a.af;
import com.e.a.ap;
import com.e.a.au;
import com.e.a.aw;
import com.e.a.ax;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public final class ag implements ai {

    /* renamed from: a  reason: collision with root package name */
    private static final List<j> f571a = v.a(j.a("connection"), j.a("host"), j.a("keep-alive"), j.a("proxy-connection"), j.a("transfer-encoding"));

    /* renamed from: b  reason: collision with root package name */
    private static final List<j> f572b = v.a(j.a("connection"), j.a("host"), j.a("keep-alive"), j.a("proxy-connection"), j.a("te"), j.a("transfer-encoding"), j.a("encoding"), j.a("upgrade"));
    private final q c;
    private final ac d;
    private ao e;

    public ag(q qVar, ac acVar) {
        this.c = qVar;
        this.d = acVar;
    }

    public static aw a(List<e> list, com.e.a.ao aoVar) {
        String str = null;
        String str2 = "HTTP/1.1";
        af afVar = new af();
        afVar.c(w.d, aoVar.toString());
        int size = list.size();
        int i = 0;
        while (i < size) {
            j jVar = list.get(i).h;
            String a2 = list.get(i).i.a();
            String str3 = str2;
            int i2 = 0;
            while (i2 < a2.length()) {
                int indexOf = a2.indexOf(0, i2);
                if (indexOf == -1) {
                    indexOf = a2.length();
                }
                String substring = a2.substring(i2, indexOf);
                if (!jVar.equals(e.f632a)) {
                    if (jVar.equals(e.g)) {
                        str3 = substring;
                        substring = str;
                    } else {
                        if (!a(aoVar, jVar)) {
                            afVar.a(jVar.a(), substring);
                        }
                        substring = str;
                    }
                }
                str = substring;
                i2 = indexOf + 1;
            }
            i++;
            str2 = str3;
        }
        if (str == null) {
            throw new ProtocolException("Expected ':status' header not present");
        }
        ah a3 = ah.a(str2 + " " + str);
        return new aw().a(aoVar).a(a3.f574b).a(a3.c).a(afVar.a());
    }

    private static String a(String str, String str2) {
        return str + 0 + str2;
    }

    public static List<e> a(ap apVar, com.e.a.ao aoVar, String str) {
        ad e2 = apVar.e();
        ArrayList arrayList = new ArrayList(e2.a() + 10);
        arrayList.add(new e(e.f633b, apVar.d()));
        arrayList.add(new e(e.c, aa.a(apVar.a())));
        String a2 = q.a(apVar.a());
        if (com.e.a.ao.SPDY_3 == aoVar) {
            arrayList.add(new e(e.g, str));
            arrayList.add(new e(e.f, a2));
        } else if (com.e.a.ao.HTTP_2 == aoVar) {
            arrayList.add(new e(e.e, a2));
        } else {
            throw new AssertionError();
        }
        arrayList.add(new e(e.d, apVar.a().getProtocol()));
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        int a3 = e2.a();
        for (int i = 0; i < a3; i++) {
            j a4 = j.a(e2.a(i).toLowerCase(Locale.US));
            String b2 = e2.b(i);
            if (!a(aoVar, a4) && !a4.equals(e.f633b) && !a4.equals(e.c) && !a4.equals(e.d) && !a4.equals(e.e) && !a4.equals(e.f) && !a4.equals(e.g)) {
                if (linkedHashSet.add(a4)) {
                    arrayList.add(new e(a4, b2));
                } else {
                    int i2 = 0;
                    while (true) {
                        if (i2 >= arrayList.size()) {
                            break;
                        } else if (((e) arrayList.get(i2)).h.equals(a4)) {
                            arrayList.set(i2, new e(a4, a(((e) arrayList.get(i2)).i.a(), b2)));
                            break;
                        } else {
                            i2++;
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    private static boolean a(com.e.a.ao aoVar, j jVar) {
        if (aoVar == com.e.a.ao.SPDY_3) {
            return f571a.contains(jVar);
        }
        if (aoVar == com.e.a.ao.HTTP_2) {
            return f572b.contains(jVar);
        }
        throw new AssertionError(aoVar);
    }

    public aa a(ap apVar, long j) {
        return this.e.g();
    }

    public ax a(au auVar) {
        return new y(auVar.g(), q.a(this.e.f()));
    }

    public void a() {
        this.e.g().close();
    }

    public void a(ab abVar) {
        abVar.a(this.e.g());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.c.ac.a(java.util.List<com.e.a.a.c.e>, boolean, boolean):com.e.a.a.c.ao
     arg types: [java.util.List<com.e.a.a.c.e>, boolean, int]
     candidates:
      com.e.a.a.c.ac.a(int, java.util.List<com.e.a.a.c.e>, boolean):void
      com.e.a.a.c.ac.a(com.e.a.a.c.ac, int, com.e.a.a.c.a):void
      com.e.a.a.c.ac.a(com.e.a.a.c.ac, int, java.util.List):void
      com.e.a.a.c.ac.a(com.e.a.a.c.ac, com.e.a.a.c.a, com.e.a.a.c.a):void
      com.e.a.a.c.ac.a(java.util.List<com.e.a.a.c.e>, boolean, boolean):com.e.a.a.c.ao */
    public void a(ap apVar) {
        if (this.e == null) {
            this.c.b();
            this.e = this.d.a(a(apVar, this.d.a(), aa.a(this.c.f().l())), this.c.c(), true);
            this.e.e().a((long) this.c.f589a.b(), TimeUnit.MILLISECONDS);
        }
    }

    public aw b() {
        return a(this.e.d(), this.d.a());
    }

    public void c() {
    }

    public boolean d() {
        return true;
    }
}
