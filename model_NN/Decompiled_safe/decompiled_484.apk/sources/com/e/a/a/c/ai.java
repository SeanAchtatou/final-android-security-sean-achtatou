package com.e.a.a.c;

import a.f;
import com.e.a.a.m;
import java.io.IOException;

class ai extends m {

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ int f618b;
    final /* synthetic */ f c;
    final /* synthetic */ int d;
    final /* synthetic */ boolean e;
    final /* synthetic */ ac f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ai(ac acVar, String str, Object[] objArr, int i, f fVar, int i2, boolean z) {
        super(str, objArr);
        this.f = acVar;
        this.f618b = i;
        this.c = fVar;
        this.d = i2;
        this.e = z;
    }

    public void a() {
        try {
            boolean a2 = this.f.v.a(this.f618b, this.c, this.d, this.e);
            if (a2) {
                this.f.i.a(this.f618b, a.CANCEL);
            }
            if (a2 || this.e) {
                synchronized (this.f) {
                    this.f.y.remove(Integer.valueOf(this.f618b));
                }
            }
        } catch (IOException e2) {
        }
    }
}
