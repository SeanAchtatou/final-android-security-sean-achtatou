package com.e.a.a.a;

import a.aa;
import a.ac;
import a.f;
import com.e.a.a.v;
import java.net.ProtocolException;

public final class ab implements aa {

    /* renamed from: a  reason: collision with root package name */
    private boolean f561a;

    /* renamed from: b  reason: collision with root package name */
    private final int f562b;
    private final f c;

    public ab() {
        this(-1);
    }

    public ab(int i) {
        this.c = new f();
        this.f562b = i;
    }

    public ac a() {
        return ac.f1b;
    }

    public void a(aa aaVar) {
        f fVar = new f();
        this.c.a(fVar, 0, this.c.b());
        aaVar.a_(fVar, fVar.b());
    }

    public void a_(f fVar, long j) {
        if (this.f561a) {
            throw new IllegalStateException("closed");
        }
        v.a(fVar.b(), 0, j);
        if (this.f562b == -1 || this.c.b() <= ((long) this.f562b) - j) {
            this.c.a_(fVar, j);
            return;
        }
        throw new ProtocolException("exceeded content-length limit of " + this.f562b + " bytes");
    }

    public long b() {
        return this.c.b();
    }

    public void close() {
        if (!this.f561a) {
            this.f561a = true;
            if (this.c.b() < ((long) this.f562b)) {
                throw new ProtocolException("content-length promised " + this.f562b + " bytes, but received " + this.c.b());
            }
        }
    }

    public void flush() {
    }
}
