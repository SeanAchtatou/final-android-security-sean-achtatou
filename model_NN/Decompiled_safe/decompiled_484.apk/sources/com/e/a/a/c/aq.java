package com.e.a.a.c;

import a.aa;
import a.ac;
import a.f;

final class aq implements aa {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f627a = (!ao.class.desiredAssertionStatus());

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ao f628b;
    private final f c = new f();
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public boolean e;

    aq(ao aoVar) {
        this.f628b = aoVar;
    }

    private void a(boolean z) {
        long min;
        synchronized (this.f628b) {
            this.f628b.k.c();
            while (this.f628b.f626b <= 0 && !this.e && !this.d && this.f628b.l == null) {
                try {
                    this.f628b.k();
                } catch (Throwable th) {
                    this.f628b.k.b();
                    throw th;
                }
            }
            this.f628b.k.b();
            this.f628b.j();
            min = Math.min(this.f628b.f626b, this.c.b());
            this.f628b.f626b -= min;
        }
        this.f628b.k.c();
        try {
            this.f628b.f.a(this.f628b.e, z && min == this.c.b(), this.c, min);
        } finally {
            this.f628b.k.b();
        }
    }

    public ac a() {
        return this.f628b.k;
    }

    public void a_(f fVar, long j) {
        if (f627a || !Thread.holdsLock(this.f628b)) {
            this.c.a_(fVar, j);
            while (this.c.b() >= 16384) {
                a(false);
            }
            return;
        }
        throw new AssertionError();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.c.ac.a(int, boolean, a.f, long):void
     arg types: [int, int, ?[OBJECT, ARRAY], int]
     candidates:
      com.e.a.a.c.ac.a(int, java.util.List<com.e.a.a.c.e>, boolean, boolean):com.e.a.a.c.ao
      com.e.a.a.c.ac.a(int, a.i, int, boolean):void
      com.e.a.a.c.ac.a(com.e.a.a.c.ac, int, java.util.List, boolean):void
      com.e.a.a.c.ac.a(boolean, int, int, com.e.a.a.c.v):void
      com.e.a.a.c.ac.a(int, boolean, a.f, long):void */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0025, code lost:
        if (r6.f628b.c.e != false) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
        if (r6.c.b() <= 0) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0039, code lost:
        if (r6.c.b() <= 0) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003b, code lost:
        a(true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0042, code lost:
        r6.f628b.f.a(r6.f628b.e, true, (a.f) null, 0L);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0052, code lost:
        r1 = r6.f628b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0054, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r6.d = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0058, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0059, code lost:
        r6.f628b.f.d();
        r6.f628b.i();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void close() {
        /*
            r6 = this;
            r4 = 0
            r2 = 1
            boolean r0 = com.e.a.a.c.aq.f627a
            if (r0 != 0) goto L_0x0015
            com.e.a.a.c.ao r0 = r6.f628b
            boolean r0 = java.lang.Thread.holdsLock(r0)
            if (r0 == 0) goto L_0x0015
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        L_0x0015:
            com.e.a.a.c.ao r1 = r6.f628b
            monitor-enter(r1)
            boolean r0 = r6.d     // Catch:{ all -> 0x003f }
            if (r0 == 0) goto L_0x001e
            monitor-exit(r1)     // Catch:{ all -> 0x003f }
        L_0x001d:
            return
        L_0x001e:
            monitor-exit(r1)     // Catch:{ all -> 0x003f }
            com.e.a.a.c.ao r0 = r6.f628b
            com.e.a.a.c.aq r0 = r0.c
            boolean r0 = r0.e
            if (r0 != 0) goto L_0x0052
            a.f r0 = r6.c
            long r0 = r0.b()
            int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x0042
        L_0x0031:
            a.f r0 = r6.c
            long r0 = r0.b()
            int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x0052
            r6.a(r2)
            goto L_0x0031
        L_0x003f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x003f }
            throw r0
        L_0x0042:
            com.e.a.a.c.ao r0 = r6.f628b
            com.e.a.a.c.ac r0 = r0.f
            com.e.a.a.c.ao r1 = r6.f628b
            int r1 = r1.e
            r3 = 0
            r0.a(r1, r2, r3, r4)
        L_0x0052:
            com.e.a.a.c.ao r1 = r6.f628b
            monitor-enter(r1)
            r0 = 1
            r6.d = r0     // Catch:{ all -> 0x0068 }
            monitor-exit(r1)     // Catch:{ all -> 0x0068 }
            com.e.a.a.c.ao r0 = r6.f628b
            com.e.a.a.c.ac r0 = r0.f
            r0.d()
            com.e.a.a.c.ao r0 = r6.f628b
            r0.i()
            goto L_0x001d
        L_0x0068:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0068 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.e.a.a.c.aq.close():void");
    }

    public void flush() {
        if (f627a || !Thread.holdsLock(this.f628b)) {
            synchronized (this.f628b) {
                this.f628b.j();
            }
            while (this.c.b() > 0) {
                a(false);
                this.f628b.f.d();
            }
            return;
        }
        throw new AssertionError();
    }
}
