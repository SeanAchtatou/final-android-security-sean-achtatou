package com.e.a.a.c;

import a.ab;
import a.f;
import a.m;

class t extends m {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ s f658a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    t(s sVar, ab abVar) {
        super(abVar);
        this.f658a = sVar;
    }

    public long a(f fVar, long j) {
        if (this.f658a.f657b == 0) {
            return -1;
        }
        long a2 = super.a(fVar, Math.min(j, (long) this.f658a.f657b));
        if (a2 == -1) {
            return -1;
        }
        int unused = this.f658a.f657b = (int) (((long) this.f658a.f657b) - a2);
        return a2;
    }
}
