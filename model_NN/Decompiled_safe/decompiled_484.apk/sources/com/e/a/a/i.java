package com.e.a.a;

import a.ab;
import java.io.Closeable;

public final class i implements Closeable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f677a;

    /* renamed from: b  reason: collision with root package name */
    private final String f678b;
    private final long c;
    private final ab[] d;
    private final long[] e;

    private i(b bVar, String str, long j, ab[] abVarArr, long[] jArr) {
        this.f677a = bVar;
        this.f678b = str;
        this.c = j;
        this.d = abVarArr;
        this.e = jArr;
    }

    /* synthetic */ i(b bVar, String str, long j, ab[] abVarArr, long[] jArr, c cVar) {
        this(bVar, str, j, abVarArr, jArr);
    }

    public ab a(int i) {
        return this.d[i];
    }

    public f a() {
        return this.f677a.a(this.f678b, this.c);
    }

    public void close() {
        for (ab a2 : this.d) {
            v.a(a2);
        }
    }
}
