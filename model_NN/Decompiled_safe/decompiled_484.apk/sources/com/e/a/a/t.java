package com.e.a.a;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.List;

class t implements InvocationHandler {

    /* renamed from: a  reason: collision with root package name */
    private final List<String> f691a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public boolean f692b;
    /* access modifiers changed from: private */
    public String c;

    public t(List<String> list) {
        this.f691a = list;
    }

    public Object invoke(Object obj, Method method, Object[] objArr) {
        String name = method.getName();
        Class<?> returnType = method.getReturnType();
        if (objArr == null) {
            objArr = v.f695b;
        }
        if (name.equals("supports") && Boolean.TYPE == returnType) {
            return true;
        }
        if (name.equals("unsupported") && Void.TYPE == returnType) {
            this.f692b = true;
            return null;
        } else if (name.equals("protocols") && objArr.length == 0) {
            return this.f691a;
        } else {
            if ((name.equals("selectProtocol") || name.equals("select")) && String.class == returnType && objArr.length == 1 && (objArr[0] instanceof List)) {
                List list = (List) objArr[0];
                int size = list.size();
                for (int i = 0; i < size; i++) {
                    if (this.f691a.contains(list.get(i))) {
                        String str = (String) list.get(i);
                        this.c = str;
                        return str;
                    }
                }
                String str2 = this.f691a.get(0);
                this.c = str2;
                return str2;
            } else if ((!name.equals("protocolSelected") && !name.equals("selected")) || objArr.length != 1) {
                return method.invoke(this, objArr);
            } else {
                this.c = (String) objArr[0];
                return null;
            }
        }
    }
}
