package com.e.a.a.c;

final class p {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final p[] f653a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final int f654b;
    /* access modifiers changed from: private */
    public final int c;

    p() {
        this.f653a = new p[256];
        this.f654b = 0;
        this.c = 0;
    }

    p(int i, int i2) {
        this.f653a = null;
        this.f654b = i;
        int i3 = i2 & 7;
        this.c = i3 == 0 ? 8 : i3;
    }
}
