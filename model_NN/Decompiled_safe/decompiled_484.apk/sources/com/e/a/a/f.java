package com.e.a.a;

import a.aa;
import java.io.FileNotFoundException;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f672a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final h f673b;
    /* access modifiers changed from: private */
    public final boolean[] c;
    /* access modifiers changed from: private */
    public boolean d;
    private boolean e;

    private f(b bVar, h hVar) {
        this.f672a = bVar;
        this.f673b = hVar;
        this.c = hVar.f ? null : new boolean[bVar.j];
    }

    /* synthetic */ f(b bVar, h hVar, c cVar) {
        this(bVar, hVar);
    }

    public aa a(int i) {
        aa d2;
        synchronized (this.f672a) {
            if (this.f673b.g != this) {
                throw new IllegalStateException();
            }
            if (!this.f673b.f) {
                this.c[i] = true;
            }
            try {
                d2 = new g(this, this.f672a.c.b(this.f673b.e[i]));
            } catch (FileNotFoundException e2) {
                d2 = b.u;
            }
        }
        return d2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.b.a(com.e.a.a.b, com.e.a.a.f, boolean):void
     arg types: [com.e.a.a.b, com.e.a.a.f, int]
     candidates:
      com.e.a.a.b.a(com.e.a.a.b, java.lang.String, long):com.e.a.a.f
      com.e.a.a.b.a(com.e.a.a.b, com.e.a.a.f, boolean):void */
    public void a() {
        synchronized (this.f672a) {
            if (this.d) {
                this.f672a.a(this, false);
                boolean unused = this.f672a.a(this.f673b);
            } else {
                this.f672a.a(this, true);
            }
            this.e = true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.b.a(com.e.a.a.b, com.e.a.a.f, boolean):void
     arg types: [com.e.a.a.b, com.e.a.a.f, int]
     candidates:
      com.e.a.a.b.a(com.e.a.a.b, java.lang.String, long):com.e.a.a.f
      com.e.a.a.b.a(com.e.a.a.b, com.e.a.a.f, boolean):void */
    public void b() {
        synchronized (this.f672a) {
            this.f672a.a(this, false);
        }
    }
}
