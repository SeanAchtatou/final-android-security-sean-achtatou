package com.e.a.a.c;

import a.aa;
import a.ab;
import a.ac;
import a.i;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.List;

public final class ao {
    static final /* synthetic */ boolean d = (!ao.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    long f625a = 0;

    /* renamed from: b  reason: collision with root package name */
    long f626b;
    final aq c;
    /* access modifiers changed from: private */
    public final int e;
    /* access modifiers changed from: private */
    public final ac f;
    private final List<e> g;
    private List<e> h;
    private final ar i;
    /* access modifiers changed from: private */
    public final as j = new as(this);
    /* access modifiers changed from: private */
    public final as k = new as(this);
    /* access modifiers changed from: private */
    public a l = null;

    ao(int i2, ac acVar, boolean z, boolean z2, List<e> list) {
        if (acVar == null) {
            throw new NullPointerException("connection == null");
        } else if (list == null) {
            throw new NullPointerException("requestHeaders == null");
        } else {
            this.e = i2;
            this.f = acVar;
            this.f626b = (long) acVar.f.e(65536);
            this.i = new ar(this, (long) acVar.e.e(65536));
            this.c = new aq(this);
            boolean unused = this.i.g = z2;
            boolean unused2 = this.c.e = z;
            this.g = list;
        }
    }

    private boolean d(a aVar) {
        if (d || !Thread.holdsLock(this)) {
            synchronized (this) {
                if (this.l != null) {
                    return false;
                }
                if (this.i.g && this.c.e) {
                    return false;
                }
                this.l = aVar;
                notifyAll();
                this.f.b(this.e);
                return true;
            }
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: private */
    public void i() {
        boolean z;
        boolean b2;
        if (d || !Thread.holdsLock(this)) {
            synchronized (this) {
                z = !this.i.g && this.i.f && (this.c.e || this.c.d);
                b2 = b();
            }
            if (z) {
                a(a.CANCEL);
            } else if (!b2) {
                this.f.b(this.e);
            }
        } else {
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        if (this.c.d) {
            throw new IOException("stream closed");
        } else if (this.c.e) {
            throw new IOException("stream finished");
        } else if (this.l != null) {
            throw new IOException("stream was reset: " + this.l);
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        try {
            wait();
        } catch (InterruptedException e2) {
            throw new InterruptedIOException();
        }
    }

    public int a() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void a(long j2) {
        this.f626b += j2;
        if (j2 > 0) {
            notifyAll();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(i iVar, int i2) {
        if (d || !Thread.holdsLock(this)) {
            this.i.a(iVar, (long) i2);
            return;
        }
        throw new AssertionError();
    }

    public void a(a aVar) {
        if (d(aVar)) {
            this.f.b(this.e, aVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(List<e> list, f fVar) {
        if (d || !Thread.holdsLock(this)) {
            a aVar = null;
            boolean z = true;
            synchronized (this) {
                if (this.h == null) {
                    if (fVar.c()) {
                        aVar = a.PROTOCOL_ERROR;
                    } else {
                        this.h = list;
                        z = b();
                        notifyAll();
                    }
                } else if (fVar.d()) {
                    aVar = a.STREAM_IN_USE;
                } else {
                    ArrayList arrayList = new ArrayList();
                    arrayList.addAll(this.h);
                    arrayList.addAll(list);
                    this.h = arrayList;
                }
            }
            if (aVar != null) {
                b(aVar);
            } else if (!z) {
                this.f.b(this.e);
            }
        } else {
            throw new AssertionError();
        }
    }

    public void b(a aVar) {
        if (d(aVar)) {
            this.f.a(this.e, aVar);
        }
    }

    public synchronized boolean b() {
        boolean z = false;
        synchronized (this) {
            if (this.l == null) {
                if ((!this.i.g && !this.i.f) || ((!this.c.e && !this.c.d) || this.h == null)) {
                    z = true;
                }
            }
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public synchronized void c(a aVar) {
        if (this.l == null) {
            this.l = aVar;
            notifyAll();
        }
    }

    public boolean c() {
        return this.f.f612b == ((this.e & 1) == 1);
    }

    public synchronized List<e> d() {
        this.j.c();
        while (this.h == null && this.l == null) {
            try {
                k();
            } catch (Throwable th) {
                this.j.b();
                throw th;
            }
        }
        this.j.b();
        if (this.h != null) {
        } else {
            throw new IOException("stream was reset: " + this.l);
        }
        return this.h;
    }

    public ac e() {
        return this.j;
    }

    public ab f() {
        return this.i;
    }

    public aa g() {
        synchronized (this) {
            if (this.h == null && !c()) {
                throw new IllegalStateException("reply before requesting the sink");
            }
        }
        return this.c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.c.ar.a(com.e.a.a.c.ar, boolean):boolean
     arg types: [com.e.a.a.c.ar, int]
     candidates:
      com.e.a.a.c.ar.a(a.f, long):long
      com.e.a.a.c.ar.a(a.i, long):void
      a.ab.a(a.f, long):long
      com.e.a.a.c.ar.a(com.e.a.a.c.ar, boolean):boolean */
    /* access modifiers changed from: package-private */
    public void h() {
        boolean b2;
        if (d || !Thread.holdsLock(this)) {
            synchronized (this) {
                boolean unused = this.i.g = true;
                b2 = b();
                notifyAll();
            }
            if (!b2) {
                this.f.b(this.e);
                return;
            }
            return;
        }
        throw new AssertionError();
    }
}
