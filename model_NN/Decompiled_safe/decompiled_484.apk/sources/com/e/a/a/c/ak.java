package com.e.a.a.c;

import com.e.a.ao;
import java.net.Socket;

public class ak {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f620a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Socket f621b;
    /* access modifiers changed from: private */
    public q c = q.f655a;
    /* access modifiers changed from: private */
    public ao d = ao.SPDY_3;
    /* access modifiers changed from: private */
    public w e = w.f662a;
    /* access modifiers changed from: private */
    public boolean f;

    public ak(String str, boolean z, Socket socket) {
        this.f620a = str;
        this.f = z;
        this.f621b = socket;
    }

    public ac a() {
        return new ac(this, null);
    }

    public ak a(ao aoVar) {
        this.d = aoVar;
        return this;
    }
}
