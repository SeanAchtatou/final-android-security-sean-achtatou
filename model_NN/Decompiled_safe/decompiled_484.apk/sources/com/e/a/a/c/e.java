package com.e.a.a.c;

import a.j;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    public static final j f632a = j.a(":status");

    /* renamed from: b  reason: collision with root package name */
    public static final j f633b = j.a(":method");
    public static final j c = j.a(":path");
    public static final j d = j.a(":scheme");
    public static final j e = j.a(":authority");
    public static final j f = j.a(":host");
    public static final j g = j.a(":version");
    public final j h;
    public final j i;
    final int j;

    public e(j jVar, j jVar2) {
        this.h = jVar;
        this.i = jVar2;
        this.j = jVar.f() + 32 + jVar2.f();
    }

    public e(j jVar, String str) {
        this(jVar, j.a(str));
    }

    public e(String str, String str2) {
        this(j.a(str), j.a(str2));
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof e)) {
            return false;
        }
        e eVar = (e) obj;
        return this.h.equals(eVar.h) && this.i.equals(eVar.i);
    }

    public int hashCode() {
        return ((this.h.hashCode() + 527) * 31) + this.i.hashCode();
    }

    public String toString() {
        return String.format("%s: %s", this.h.a(), this.i.a());
    }
}
