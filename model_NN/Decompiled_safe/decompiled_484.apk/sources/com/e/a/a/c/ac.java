package com.e.a.a.c;

import a.f;
import a.i;
import a.q;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.ViewCompat;
import android.support.v7.internal.widget.ActivityChooserView;
import com.e.a.a.v;
import com.e.a.ao;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class ac implements Closeable {
    static final /* synthetic */ boolean k = (!ac.class.desiredAssertionStatus());
    /* access modifiers changed from: private */
    public static final ExecutorService l = new ThreadPoolExecutor(0, (int) ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED, 60, TimeUnit.SECONDS, new SynchronousQueue(), v.a("OkHttp SpdyConnection", true));

    /* renamed from: a  reason: collision with root package name */
    final ao f611a;

    /* renamed from: b  reason: collision with root package name */
    final boolean f612b;
    long c;
    long d;
    final y e;
    final y f;
    final at g;
    final Socket h;
    final d i;
    final al j;
    /* access modifiers changed from: private */
    public final q m;
    /* access modifiers changed from: private */
    public final Map<Integer, ao> n;
    /* access modifiers changed from: private */
    public final String o;
    /* access modifiers changed from: private */
    public int p;
    /* access modifiers changed from: private */
    public int q;
    /* access modifiers changed from: private */
    public boolean r;
    private long s;
    private final ExecutorService t;
    private Map<Integer, v> u;
    /* access modifiers changed from: private */
    public final w v;
    private int w;
    /* access modifiers changed from: private */
    public boolean x;
    /* access modifiers changed from: private */
    public final Set<Integer> y;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.v.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
     arg types: [java.lang.String, int]
     candidates:
      com.e.a.a.v.a(java.lang.String, int):int
      com.e.a.a.v.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
      com.e.a.a.v.a(java.io.Closeable, java.io.Closeable):void
      com.e.a.a.v.a(java.lang.Object, java.lang.Object):boolean
      com.e.a.a.v.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory */
    private ac(ak akVar) {
        int i2 = 2;
        this.n = new HashMap();
        this.s = System.nanoTime();
        this.c = 0;
        this.e = new y();
        this.f = new y();
        this.x = false;
        this.y = new LinkedHashSet();
        this.f611a = akVar.d;
        this.v = akVar.e;
        this.f612b = akVar.f;
        this.m = akVar.c;
        this.q = akVar.f ? 1 : 2;
        if (akVar.f && this.f611a == ao.HTTP_2) {
            this.q += 2;
        }
        this.w = akVar.f ? 1 : i2;
        if (akVar.f) {
            this.e.a(7, 0, ViewCompat.MEASURED_STATE_TOO_SMALL);
        }
        this.o = akVar.f620a;
        if (this.f611a == ao.HTTP_2) {
            this.g = new j();
            this.t = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), v.a(String.format("OkHttp %s Push Observer", this.o), true));
            this.f.a(7, 0, SupportMenu.USER_MASK);
            this.f.a(5, 0, 16384);
        } else if (this.f611a == ao.SPDY_3) {
            this.g = new z();
            this.t = null;
        } else {
            throw new AssertionError(this.f611a);
        }
        this.d = (long) this.f.e(65536);
        this.h = akVar.f621b;
        this.i = this.g.a(q.a(q.a(akVar.f621b)), this.f612b);
        this.j = new al(this, null);
        new Thread(this.j).start();
    }

    /* synthetic */ ac(ak akVar, ad adVar) {
        this(akVar);
    }

    private ao a(int i2, List<e> list, boolean z, boolean z2) {
        int i3;
        ao aoVar;
        boolean z3 = true;
        boolean z4 = !z;
        if (z2) {
            z3 = false;
        }
        synchronized (this.i) {
            synchronized (this) {
                if (this.r) {
                    throw new IOException("shutdown");
                }
                i3 = this.q;
                this.q += 2;
                aoVar = new ao(i3, this, z4, z3, list);
                if (aoVar.b()) {
                    this.n.put(Integer.valueOf(i3), aoVar);
                    a(false);
                }
            }
            if (i2 == 0) {
                this.i.a(z4, z3, i3, i2, list);
            } else if (this.f612b) {
                throw new IllegalArgumentException("client streams shouldn't have associated stream IDs");
            } else {
                this.i.a(i2, i3, list);
            }
        }
        if (!z) {
            this.i.b();
        }
        return aoVar;
    }

    /* access modifiers changed from: private */
    public void a(int i2, i iVar, int i3, boolean z) {
        f fVar = new f();
        iVar.a((long) i3);
        iVar.a(fVar, (long) i3);
        if (fVar.b() != ((long) i3)) {
            throw new IOException(fVar.b() + " != " + i3);
        }
        this.t.execute(new ai(this, "OkHttp %s Push Data[%s]", new Object[]{this.o, Integer.valueOf(i2)}, i2, fVar, i3, z));
    }

    /* access modifiers changed from: private */
    public void a(int i2, List<e> list) {
        synchronized (this) {
            if (this.y.contains(Integer.valueOf(i2))) {
                a(i2, a.PROTOCOL_ERROR);
                return;
            }
            this.y.add(Integer.valueOf(i2));
            this.t.execute(new ag(this, "OkHttp %s Push Request[%s]", new Object[]{this.o, Integer.valueOf(i2)}, i2, list));
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, List<e> list, boolean z) {
        this.t.execute(new ah(this, "OkHttp %s Push Headers[%s]", new Object[]{this.o, Integer.valueOf(i2)}, i2, list, z));
    }

    /* access modifiers changed from: private */
    public void a(a aVar, a aVar2) {
        IOException iOException;
        ao[] aoVarArr;
        v[] vVarArr;
        if (k || !Thread.holdsLock(this)) {
            try {
                a(aVar);
                iOException = null;
            } catch (IOException e2) {
                iOException = e2;
            }
            synchronized (this) {
                if (!this.n.isEmpty()) {
                    this.n.clear();
                    a(false);
                    aoVarArr = (ao[]) this.n.values().toArray(new ao[this.n.size()]);
                } else {
                    aoVarArr = null;
                }
                if (this.u != null) {
                    this.u = null;
                    vVarArr = (v[]) this.u.values().toArray(new v[this.u.size()]);
                } else {
                    vVarArr = null;
                }
            }
            if (aoVarArr != null) {
                IOException iOException2 = iOException;
                for (ao a2 : aoVarArr) {
                    try {
                        a2.a(aVar2);
                    } catch (IOException e3) {
                        if (iOException2 != null) {
                            iOException2 = e3;
                        }
                    }
                }
                iOException = iOException2;
            }
            if (vVarArr != null) {
                for (v c2 : vVarArr) {
                    c2.c();
                }
            }
            try {
                this.i.close();
                e = iOException;
            } catch (IOException e4) {
                e = e4;
                if (iOException != null) {
                    e = iOException;
                }
            }
            try {
                this.h.close();
            } catch (IOException e5) {
                e = e5;
            }
            if (e != null) {
                throw e;
            }
            return;
        }
        throw new AssertionError();
    }

    private synchronized void a(boolean z) {
        this.s = z ? System.nanoTime() : Long.MAX_VALUE;
    }

    /* access modifiers changed from: private */
    public void a(boolean z, int i2, int i3, v vVar) {
        l.execute(new af(this, "OkHttp %s ping %08x%08x", new Object[]{this.o, Integer.valueOf(i2), Integer.valueOf(i3)}, z, i2, i3, vVar));
    }

    /* access modifiers changed from: private */
    public void b(boolean z, int i2, int i3, v vVar) {
        synchronized (this.i) {
            if (vVar != null) {
                vVar.a();
            }
            this.i.a(z, i2, i3);
        }
    }

    /* access modifiers changed from: private */
    public synchronized v c(int i2) {
        return this.u != null ? this.u.remove(Integer.valueOf(i2)) : null;
    }

    /* access modifiers changed from: private */
    public void c(int i2, a aVar) {
        this.t.execute(new aj(this, "OkHttp %s Push Reset[%s]", new Object[]{this.o, Integer.valueOf(i2)}, i2, aVar));
    }

    /* access modifiers changed from: private */
    public boolean d(int i2) {
        return this.f611a == ao.HTTP_2 && i2 != 0 && (i2 & 1) == 0;
    }

    /* access modifiers changed from: package-private */
    public synchronized ao a(int i2) {
        return this.n.get(Integer.valueOf(i2));
    }

    public ao a(List<e> list, boolean z, boolean z2) {
        return a(0, list, z, z2);
    }

    public ao a() {
        return this.f611a;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, long j2) {
        l.execute(new ae(this, "OkHttp Window Update %s stream %d", new Object[]{this.o, Integer.valueOf(i2)}, i2, j2));
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, a aVar) {
        l.submit(new ad(this, "OkHttp %s stream %d", new Object[]{this.o, Integer.valueOf(i2)}, i2, aVar));
    }

    public void a(int i2, boolean z, f fVar, long j2) {
        int min;
        if (j2 == 0) {
            this.i.a(z, i2, fVar, 0);
            return;
        }
        while (j2 > 0) {
            synchronized (this) {
                while (this.d <= 0) {
                    try {
                        if (!this.n.containsKey(Integer.valueOf(i2))) {
                            throw new IOException("stream closed");
                        }
                        wait();
                    } catch (InterruptedException e2) {
                        throw new InterruptedIOException();
                    }
                }
                min = Math.min((int) Math.min(j2, this.d), this.i.c());
                this.d -= (long) min;
            }
            j2 -= (long) min;
            this.i.a(z && j2 == 0, i2, fVar, min);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(long j2) {
        this.d += j2;
        if (j2 > 0) {
            notifyAll();
        }
    }

    public void a(a aVar) {
        synchronized (this.i) {
            synchronized (this) {
                if (!this.r) {
                    this.r = true;
                    int i2 = this.p;
                    this.i.a(i2, aVar, v.f694a);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized ao b(int i2) {
        ao remove;
        remove = this.n.remove(Integer.valueOf(i2));
        if (remove != null && this.n.isEmpty()) {
            a(true);
        }
        notifyAll();
        return remove;
    }

    /* access modifiers changed from: package-private */
    public void b(int i2, a aVar) {
        this.i.a(i2, aVar);
    }

    public synchronized boolean b() {
        return this.s != Long.MAX_VALUE;
    }

    public synchronized long c() {
        return this.s;
    }

    public void close() {
        a(a.NO_ERROR, a.CANCEL);
    }

    public void d() {
        this.i.b();
    }

    public void e() {
        this.i.a();
        this.i.b(this.e);
        int e2 = this.e.e(65536);
        if (e2 != 65536) {
            this.i.a(0, (long) (e2 - 65536));
        }
    }
}
