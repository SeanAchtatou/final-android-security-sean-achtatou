package com.e.a.a;

import com.e.a.ay;
import java.util.LinkedHashSet;
import java.util.Set;

public final class u {

    /* renamed from: a  reason: collision with root package name */
    private final Set<ay> f693a = new LinkedHashSet();

    public synchronized void a(ay ayVar) {
        this.f693a.add(ayVar);
    }

    public synchronized void b(ay ayVar) {
        this.f693a.remove(ayVar);
    }

    public synchronized boolean c(ay ayVar) {
        return this.f693a.contains(ayVar);
    }
}
