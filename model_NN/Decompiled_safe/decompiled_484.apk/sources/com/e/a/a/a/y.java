package com.e.a.a.a;

import a.i;
import com.e.a.ad;
import com.e.a.al;
import com.e.a.ax;

public final class y extends ax {

    /* renamed from: a  reason: collision with root package name */
    private final ad f599a;

    /* renamed from: b  reason: collision with root package name */
    private final i f600b;

    public y(ad adVar, i iVar) {
        this.f599a = adVar;
        this.f600b = iVar;
    }

    public al a() {
        String a2 = this.f599a.a("Content-Type");
        if (a2 != null) {
            return al.a(a2);
        }
        return null;
    }

    public long b() {
        return w.a(this.f599a);
    }

    public i c() {
        return this.f600b;
    }
}
