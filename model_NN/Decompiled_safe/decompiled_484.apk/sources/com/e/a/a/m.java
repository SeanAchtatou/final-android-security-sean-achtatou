package com.e.a.a;

public abstract class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    protected final String f682a;

    public m(String str, Object... objArr) {
        this.f682a = String.format(str, objArr);
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    public final void run() {
        String name = Thread.currentThread().getName();
        Thread.currentThread().setName(this.f682a);
        try {
            a();
        } finally {
            Thread.currentThread().setName(name);
        }
    }
}
