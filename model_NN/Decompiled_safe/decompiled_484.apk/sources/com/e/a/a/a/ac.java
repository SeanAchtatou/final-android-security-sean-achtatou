package com.e.a.a.a;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public final class ac extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private static final Method f563a;

    /* renamed from: b  reason: collision with root package name */
    private IOException f564b;

    static {
        Method method;
        try {
            method = Throwable.class.getDeclaredMethod("addSuppressed", Throwable.class);
        } catch (Exception e) {
            method = null;
        }
        f563a = method;
    }

    public ac(IOException iOException) {
        super(iOException);
        this.f564b = iOException;
    }

    private void a(IOException iOException, IOException iOException2) {
        if (f563a != null) {
            try {
                f563a.invoke(iOException, iOException2);
            } catch (IllegalAccessException | InvocationTargetException e) {
            }
        }
    }

    public IOException a() {
        return this.f564b;
    }

    public void a(IOException iOException) {
        a(iOException, this.f564b);
        this.f564b = iOException;
    }
}
