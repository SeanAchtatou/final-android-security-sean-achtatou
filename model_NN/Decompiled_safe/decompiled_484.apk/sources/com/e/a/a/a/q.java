package com.e.a.a.a;

import a.aa;
import a.h;
import com.e.a.a;
import com.e.a.a.k;
import com.e.a.a.l;
import com.e.a.a.v;
import com.e.a.a.x;
import com.e.a.ad;
import com.e.a.af;
import com.e.a.am;
import com.e.a.ao;
import com.e.a.ap;
import com.e.a.ar;
import com.e.a.au;
import com.e.a.aw;
import com.e.a.ax;
import com.e.a.ay;
import com.e.a.o;
import com.e.a.t;
import com.e.a.u;
import com.mediav.ads.sdk.adcore.Config;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.CookieHandler;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.cert.CertificateException;
import java.util.Date;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;

public final class q {
    private static final ax d = new r();

    /* renamed from: a  reason: collision with root package name */
    final am f589a;

    /* renamed from: b  reason: collision with root package name */
    long f590b = -1;
    public final boolean c;
    /* access modifiers changed from: private */
    public t e;
    private a f;
    private ad g;
    private ay h;
    private final au i;
    /* access modifiers changed from: private */
    public ai j;
    private boolean k;
    private final ap l;
    /* access modifiers changed from: private */
    public ap m;
    private au n;
    private au o;
    private aa p;
    private h q;
    private final boolean r;
    private final boolean s;
    private b t;
    private c u;

    public q(am amVar, ap apVar, boolean z, boolean z2, boolean z3, t tVar, ad adVar, ab abVar, au auVar) {
        this.f589a = amVar;
        this.l = apVar;
        this.c = z;
        this.r = z2;
        this.s = z3;
        this.e = tVar;
        this.g = adVar;
        this.p = abVar;
        this.i = auVar;
        if (tVar != null) {
            k.f681b.b(tVar, this);
            this.h = tVar.c();
            return;
        }
        this.h = null;
    }

    private static a a(am amVar, ap apVar) {
        HostnameVerifier hostnameVerifier;
        SSLSocketFactory sSLSocketFactory;
        o oVar = null;
        String host = apVar.a().getHost();
        if (host == null || host.length() == 0) {
            throw new z(new UnknownHostException(apVar.a().toString()));
        }
        if (apVar.i()) {
            sSLSocketFactory = amVar.i();
            hostnameVerifier = amVar.j();
            oVar = amVar.k();
        } else {
            hostnameVerifier = null;
            sSLSocketFactory = null;
        }
        return new a(host, v.a(apVar.a()), amVar.h(), sSLSocketFactory, hostnameVerifier, oVar, amVar.l(), amVar.d(), amVar.s(), amVar.t(), amVar.e());
    }

    private static ad a(ad adVar, ad adVar2) {
        af afVar = new af();
        int a2 = adVar.a();
        for (int i2 = 0; i2 < a2; i2++) {
            String a3 = adVar.a(i2);
            String b2 = adVar.b(i2);
            if ((!"Warning".equalsIgnoreCase(a3) || !b2.startsWith(Config.CHANNEL_ID)) && (!w.a(a3) || adVar2.a(a3) == null)) {
                afVar.a(a3, b2);
            }
        }
        int a4 = adVar2.a();
        for (int i3 = 0; i3 < a4; i3++) {
            String a5 = adVar2.a(i3);
            if (!"Content-Length".equalsIgnoreCase(a5) && w.a(a5)) {
                afVar.a(a5, adVar2.b(i3));
            }
        }
        return afVar.a();
    }

    private ap a(ap apVar) {
        ar g2 = apVar.g();
        if (apVar.a("Host") == null) {
            g2.a("Host", a(apVar.a()));
        }
        if ((this.e == null || this.e.l() != ao.HTTP_1_0) && apVar.a("Connection") == null) {
            g2.a("Connection", "Keep-Alive");
        }
        if (apVar.a("Accept-Encoding") == null) {
            this.k = true;
            g2.a("Accept-Encoding", "gzip");
        }
        CookieHandler f2 = this.f589a.f();
        if (f2 != null) {
            w.a(g2, f2.get(apVar.b(), w.a(g2.a().e(), (String) null)));
        }
        if (apVar.a("User-Agent") == null) {
            g2.a("User-Agent", x.a());
        }
        return g2.a();
    }

    private au a(b bVar, au auVar) {
        aa b2;
        return (bVar == null || (b2 = bVar.b()) == null) ? auVar : auVar.i().a(new y(auVar.g(), a.q.a(new s(this, auVar.h().c(), bVar, a.q.a(b2))))).a();
    }

    public static String a(URL url) {
        return v.a(url) != v.a(url.getProtocol()) ? url.getHost() + ":" + url.getPort() : url.getHost();
    }

    private void a(ad adVar, IOException iOException) {
        if (k.f681b.b(this.e) <= 0) {
            adVar.a(this.e.c(), iOException);
        }
    }

    public static boolean a(au auVar) {
        if (auVar.a().d().equals("HEAD")) {
            return false;
        }
        int c2 = auVar.c();
        if ((c2 >= 100 && c2 < 200) || c2 == 204 || c2 == 304) {
            return w.a(auVar) != -1 || "chunked".equalsIgnoreCase(auVar.a("Transfer-Encoding"));
        }
        return true;
    }

    private static boolean a(au auVar, au auVar2) {
        Date b2;
        if (auVar2.c() == 304) {
            return true;
        }
        Date b3 = auVar.g().b("Last-Modified");
        return (b3 == null || (b2 = auVar2.g().b("Last-Modified")) == null || b2.getTime() >= b3.getTime()) ? false : true;
    }

    private boolean a(IOException iOException) {
        return this.f589a.p() && !(iOException instanceof ProtocolException) && !(iOException instanceof InterruptedIOException);
    }

    private static au b(au auVar) {
        return (auVar == null || auVar.h() == null) ? auVar : auVar.i().a((ax) null).a();
    }

    private boolean b(ac acVar) {
        if (!this.f589a.p()) {
            return false;
        }
        IOException a2 = acVar.a();
        if ((a2 instanceof ProtocolException) || (a2 instanceof InterruptedIOException)) {
            return false;
        }
        return (!(a2 instanceof SSLHandshakeException) || !(a2.getCause() instanceof CertificateException)) && !(a2 instanceof SSLPeerUnverifiedException);
    }

    private au c(au auVar) {
        if (!this.k || !"gzip".equalsIgnoreCase(this.o.a("Content-Encoding")) || auVar.h() == null) {
            return auVar;
        }
        a.o oVar = new a.o(auVar.h().c());
        ad a2 = auVar.g().b().b("Content-Encoding").b("Content-Length").a();
        return auVar.i().a(a2).a(new y(a2, a.q.a(oVar))).a();
    }

    private void l() {
        if (this.e != null) {
            throw new IllegalStateException();
        }
        if (this.g == null) {
            this.f = a(this.f589a, this.m);
            try {
                this.g = ad.a(this.f, this.m, this.f589a);
            } catch (IOException e2) {
                throw new z(e2);
            }
        }
        this.e = m();
        this.h = this.e.c();
    }

    private t m() {
        t n2 = n();
        k.f681b.a(this.f589a, n2, this, this.m);
        return n2;
    }

    private t n() {
        u m2 = this.f589a.m();
        while (true) {
            t a2 = m2.a(this.f);
            if (a2 == null) {
                try {
                    return new t(m2, this.g.b());
                } catch (IOException e2) {
                    throw new ac(e2);
                }
            } else if (this.m.d().equals("GET") || k.f681b.c(a2)) {
                return a2;
            } else {
                v.a(a2.d());
            }
        }
    }

    private void o() {
        l a2 = k.f681b.a(this.f589a);
        if (a2 != null) {
            if (c.a(this.o, this.m)) {
                this.t = a2.a(b(this.o));
            } else if (u.a(this.m.d())) {
                try {
                    a2.b(this.m);
                } catch (IOException e2) {
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public au p() {
        this.j.a();
        au a2 = this.j.b().a(this.m).a(this.e.j()).a(w.f598b, Long.toString(this.f590b)).a(w.c, Long.toString(System.currentTimeMillis())).a();
        if (!this.s) {
            a2 = a2.i().a(this.j.a(a2)).a();
        }
        k.f681b.a(this.e, a2.b());
        return a2;
    }

    public q a(ac acVar) {
        if (!(this.g == null || this.e == null)) {
            a(this.g, acVar.a());
        }
        if ((this.g == null && this.e == null) || ((this.g != null && !this.g.a()) || !b(acVar))) {
            return null;
        }
        return new q(this.f589a, this.l, this.c, this.r, this.s, i(), this.g, (ab) this.p, this.i);
    }

    public q a(IOException iOException, aa aaVar) {
        if (!(this.g == null || this.e == null)) {
            a(this.g, iOException);
        }
        boolean z = aaVar == null || (aaVar instanceof ab);
        if ((this.g == null && this.e == null) || ((this.g != null && !this.g.a()) || !a(iOException) || !z)) {
            return null;
        }
        return new q(this.f589a, this.l, this.c, this.r, this.s, i(), this.g, (ab) aaVar, this.i);
    }

    public void a() {
        if (this.u == null) {
            if (this.j != null) {
                throw new IllegalStateException();
            }
            ap a2 = a(this.l);
            l a3 = k.f681b.a(this.f589a);
            au a4 = a3 != null ? a3.a(a2) : null;
            this.u = new e(System.currentTimeMillis(), a2, a4).a();
            this.m = this.u.f575a;
            this.n = this.u.f576b;
            if (a3 != null) {
                a3.a(this.u);
            }
            if (a4 != null && this.n == null) {
                v.a(a4.h());
            }
            if (this.m != null) {
                if (this.e == null) {
                    l();
                }
                this.j = k.f681b.a(this.e, this);
                if (this.r && c() && this.p == null) {
                    long a5 = w.a(a2);
                    if (!this.c) {
                        this.j.a(this.m);
                        this.p = this.j.a(this.m, a5);
                    } else if (a5 > 2147483647L) {
                        throw new IllegalStateException("Use setFixedLengthStreamingMode() or setChunkedStreamingMode() for requests larger than 2 GiB.");
                    } else if (a5 != -1) {
                        this.j.a(this.m);
                        this.p = new ab((int) a5);
                    } else {
                        this.p = new ab();
                    }
                }
            } else {
                if (this.e != null) {
                    k.f681b.a(this.f589a.m(), this.e);
                    this.e = null;
                }
                if (this.n != null) {
                    this.o = this.n.i().a(this.l).c(b(this.i)).b(b(this.n)).a();
                } else {
                    this.o = new aw().a(this.l).c(b(this.i)).a(ao.HTTP_1_1).a(504).a("Unsatisfiable Request (only-if-cached)").a(d).a();
                }
                this.o = c(this.o);
            }
        }
    }

    public void a(ad adVar) {
        CookieHandler f2 = this.f589a.f();
        if (f2 != null) {
            f2.put(this.l.b(), w.a(adVar, (String) null));
        }
    }

    public void b() {
        if (this.f590b != -1) {
            throw new IllegalStateException();
        }
        this.f590b = System.currentTimeMillis();
    }

    public boolean b(URL url) {
        URL a2 = this.l.a();
        return a2.getHost().equals(url.getHost()) && v.a(a2) == v.a(url) && a2.getProtocol().equals(url.getProtocol());
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return u.c(this.l.d());
    }

    public ap d() {
        return this.l;
    }

    public au e() {
        if (this.o != null) {
            return this.o;
        }
        throw new IllegalStateException();
    }

    public t f() {
        return this.e;
    }

    public ay g() {
        return this.h;
    }

    public void h() {
        if (!(this.j == null || this.e == null)) {
            this.j.c();
        }
        this.e = null;
    }

    public t i() {
        if (this.q != null) {
            v.a(this.q);
        } else if (this.p != null) {
            v.a(this.p);
        }
        if (this.o == null) {
            if (this.e != null) {
                v.a(this.e.d());
            }
            this.e = null;
            return null;
        }
        v.a(this.o.h());
        if (this.j == null || this.e == null || this.j.d()) {
            if (this.e != null && !k.f681b.a(this.e)) {
                this.e = null;
            }
            t tVar = this.e;
            this.e = null;
            return tVar;
        }
        v.a(this.e.d());
        this.e = null;
        return null;
    }

    public void j() {
        au p2;
        if (this.o == null) {
            if (this.m == null && this.n == null) {
                throw new IllegalStateException("call sendRequest() first!");
            } else if (this.m != null) {
                if (this.s) {
                    this.j.a(this.m);
                    p2 = p();
                } else if (!this.r) {
                    p2 = new t(this, 0, this.m).a(this.m);
                } else {
                    if (this.q != null && this.q.c().b() > 0) {
                        this.q.e();
                    }
                    if (this.f590b == -1) {
                        if (w.a(this.m) == -1 && (this.p instanceof ab)) {
                            this.m = this.m.g().a("Content-Length", Long.toString(((ab) this.p).b())).a();
                        }
                        this.j.a(this.m);
                    }
                    if (this.p != null) {
                        if (this.q != null) {
                            this.q.close();
                        } else {
                            this.p.close();
                        }
                        if (this.p instanceof ab) {
                            this.j.a((ab) this.p);
                        }
                    }
                    p2 = p();
                }
                a(p2.g());
                if (this.n != null) {
                    if (a(this.n, p2)) {
                        this.o = this.n.i().a(this.l).c(b(this.i)).a(a(this.n.g(), p2.g())).b(b(this.n)).a(b(p2)).a();
                        p2.h().close();
                        h();
                        l a2 = k.f681b.a(this.f589a);
                        a2.a();
                        a2.a(this.n, b(this.o));
                        this.o = c(this.o);
                        return;
                    }
                    v.a(this.n.h());
                }
                this.o = p2.i().a(this.l).c(b(this.i)).b(b(this.n)).a(b(p2)).a();
                if (a(this.o)) {
                    o();
                    this.o = c(a(this.t, this.o));
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0070  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.e.a.ap k() {
        /*
            r4 = this;
            r1 = 0
            com.e.a.au r0 = r4.o
            if (r0 != 0) goto L_0x000b
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>()
            throw r0
        L_0x000b:
            com.e.a.ay r0 = r4.g()
            if (r0 == 0) goto L_0x0024
            com.e.a.ay r0 = r4.g()
            java.net.Proxy r0 = r0.b()
        L_0x0019:
            com.e.a.au r2 = r4.o
            int r2 = r2.c()
            switch(r2) {
                case 300: goto L_0x0066;
                case 301: goto L_0x0066;
                case 302: goto L_0x0066;
                case 303: goto L_0x0066;
                case 307: goto L_0x0048;
                case 308: goto L_0x0048;
                case 401: goto L_0x003b;
                case 407: goto L_0x002b;
                default: goto L_0x0022;
            }
        L_0x0022:
            r0 = r1
        L_0x0023:
            return r0
        L_0x0024:
            com.e.a.am r0 = r4.f589a
            java.net.Proxy r0 = r0.d()
            goto L_0x0019
        L_0x002b:
            java.net.Proxy$Type r1 = r0.type()
            java.net.Proxy$Type r2 = java.net.Proxy.Type.HTTP
            if (r1 == r2) goto L_0x003b
            java.net.ProtocolException r0 = new java.net.ProtocolException
            java.lang.String r1 = "Received HTTP_PROXY_AUTH (407) code while not using proxy"
            r0.<init>(r1)
            throw r0
        L_0x003b:
            com.e.a.am r1 = r4.f589a
            com.e.a.b r1 = r1.l()
            com.e.a.au r2 = r4.o
            com.e.a.ap r0 = com.e.a.a.a.w.a(r1, r2, r0)
            goto L_0x0023
        L_0x0048:
            com.e.a.ap r0 = r4.l
            java.lang.String r0 = r0.d()
            java.lang.String r2 = "GET"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0066
            com.e.a.ap r0 = r4.l
            java.lang.String r0 = r0.d()
            java.lang.String r2 = "HEAD"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0066
            r0 = r1
            goto L_0x0023
        L_0x0066:
            com.e.a.am r0 = r4.f589a
            boolean r0 = r0.o()
            if (r0 != 0) goto L_0x0070
            r0 = r1
            goto L_0x0023
        L_0x0070:
            com.e.a.au r0 = r4.o
            java.lang.String r2 = "Location"
            java.lang.String r0 = r0.a(r2)
            if (r0 != 0) goto L_0x007c
            r0 = r1
            goto L_0x0023
        L_0x007c:
            java.net.URL r2 = new java.net.URL
            com.e.a.ap r3 = r4.l
            java.net.URL r3 = r3.a()
            r2.<init>(r3, r0)
            java.lang.String r0 = r2.getProtocol()
            java.lang.String r3 = "https"
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x00a1
            java.lang.String r0 = r2.getProtocol()
            java.lang.String r3 = "http"
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x00a1
            r0 = r1
            goto L_0x0023
        L_0x00a1:
            java.lang.String r0 = r2.getProtocol()
            com.e.a.ap r3 = r4.l
            java.net.URL r3 = r3.a()
            java.lang.String r3 = r3.getProtocol()
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x00c0
            com.e.a.am r0 = r4.f589a
            boolean r0 = r0.n()
            if (r0 != 0) goto L_0x00c0
            r0 = r1
            goto L_0x0023
        L_0x00c0:
            com.e.a.ap r0 = r4.l
            com.e.a.ar r0 = r0.g()
            com.e.a.ap r3 = r4.l
            java.lang.String r3 = r3.d()
            boolean r3 = com.e.a.a.a.u.c(r3)
            if (r3 == 0) goto L_0x00e6
            java.lang.String r3 = "GET"
            r0.a(r3, r1)
            java.lang.String r1 = "Transfer-Encoding"
            r0.b(r1)
            java.lang.String r1 = "Content-Length"
            r0.b(r1)
            java.lang.String r1 = "Content-Type"
            r0.b(r1)
        L_0x00e6:
            boolean r1 = r4.b(r2)
            if (r1 != 0) goto L_0x00f1
            java.lang.String r1 = "Authorization"
            r0.b(r1)
        L_0x00f1:
            com.e.a.ar r0 = r0.a(r2)
            com.e.a.ap r0 = r0.a()
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.e.a.a.a.q.k():com.e.a.ap");
    }
}
