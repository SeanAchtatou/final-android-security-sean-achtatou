package com.e.a.a.c;

final class l {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f645a = {"DATA", "HEADERS", "PRIORITY", "RST_STREAM", "SETTINGS", "PUSH_PROMISE", "PING", "GOAWAY", "WINDOW_UPDATE", "CONTINUATION"};

    /* renamed from: b  reason: collision with root package name */
    private static final String[] f646b = new String[64];
    private static final String[] c = new String[256];

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    static {
        for (int i = 0; i < c.length; i++) {
            c[i] = String.format("%8s", Integer.toBinaryString(i)).replace(' ', '0');
        }
        f646b[0] = "";
        f646b[1] = "END_STREAM";
        int[] iArr = {1};
        f646b[8] = "PADDED";
        for (int i2 : iArr) {
            f646b[i2 | 8] = f646b[i2] + "|PADDED";
        }
        f646b[4] = "END_HEADERS";
        f646b[32] = "PRIORITY";
        f646b[36] = "END_HEADERS|PRIORITY";
        for (int i3 : new int[]{4, 32, 36}) {
            for (int i4 : iArr) {
                f646b[i4 | i3] = f646b[i4] + '|' + f646b[i3];
                f646b[i4 | i3 | 8] = f646b[i4] + '|' + f646b[i3] + "|PADDED";
            }
        }
        for (int i5 = 0; i5 < f646b.length; i5++) {
            if (f646b[i5] == null) {
                f646b[i5] = c[i5];
            }
        }
    }

    l() {
    }

    static String a(byte b2, byte b3) {
        if (b3 == 0) {
            return "";
        }
        switch (b2) {
            case 2:
            case 3:
            case 7:
            case 8:
                return c[b3];
            case 4:
            case 6:
                return b3 == 1 ? "ACK" : c[b3];
            case 5:
            default:
                String str = b3 < f646b.length ? f646b[b3] : c[b3];
                return (b2 != 5 || (b3 & 4) == 0) ? (b2 != 0 || (b3 & 32) == 0) ? str : str.replace("PRIORITY", "COMPRESSED") : str.replace("HEADERS", "PUSH_PROMISE");
        }
    }

    static String a(boolean z, int i, int i2, byte b2, byte b3) {
        String format = b2 < f645a.length ? f645a[b2] : String.format("0x%02x", Byte.valueOf(b2));
        String a2 = a(b2, b3);
        Object[] objArr = new Object[5];
        objArr[0] = z ? "<<" : ">>";
        objArr[1] = Integer.valueOf(i);
        objArr[2] = Integer.valueOf(i2);
        objArr[3] = format;
        objArr[4] = a2;
        return String.format("%s 0x%08x %5d %-13s %s", objArr);
    }
}
