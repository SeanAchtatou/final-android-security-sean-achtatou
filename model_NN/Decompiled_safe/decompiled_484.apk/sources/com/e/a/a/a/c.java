package com.e.a.a.a;

import com.e.a.ap;
import com.e.a.au;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    public final ap f575a;

    /* renamed from: b  reason: collision with root package name */
    public final au f576b;

    private c(ap apVar, au auVar) {
        this.f575a = apVar;
        this.f576b = auVar;
    }

    public static boolean a(au auVar, ap apVar) {
        switch (auVar.c()) {
            case 200:
            case 203:
            case 204:
            case 300:
            case 301:
            case 308:
            case 404:
            case 405:
            case 410:
            case 414:
            case 501:
                break;
            default:
                return false;
            case 302:
            case 307:
                if (auVar.a("Expires") == null && auVar.m().c() == -1 && !auVar.m().e() && !auVar.m().d()) {
                    return false;
                }
        }
        return !auVar.m().b() && !apVar.h().b();
    }
}
