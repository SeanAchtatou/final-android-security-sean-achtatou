package com.e.a.a.c;

import a.f;
import a.h;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;

final class n implements d {

    /* renamed from: a  reason: collision with root package name */
    private final h f649a;

    /* renamed from: b  reason: collision with root package name */
    private final boolean f650b;
    private final f c = new f();
    private final i d = new i(this.c);
    private int e = 16384;
    private boolean f;

    n(h hVar, boolean z) {
        this.f649a = hVar;
        this.f650b = z;
    }

    private void b(int i, long j) {
        while (j > 0) {
            int min = (int) Math.min((long) this.e, j);
            j -= (long) min;
            a(i, min, (byte) 9, j == 0 ? (byte) 4 : 0);
            this.f649a.a_(this.c, (long) min);
        }
    }

    public synchronized void a() {
        if (this.f) {
            throw new IOException("closed");
        } else if (this.f650b) {
            if (j.f641a.isLoggable(Level.FINE)) {
                j.f641a.fine(String.format(">> CONNECTION %s", j.f642b.d()));
            }
            this.f649a.c(j.f642b.g());
            this.f649a.flush();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i, byte b2, f fVar, int i2) {
        a(i, i2, (byte) 0, b2);
        if (i2 > 0) {
            this.f649a.a_(fVar, (long) i2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i, int i2, byte b2, byte b3) {
        if (j.f641a.isLoggable(Level.FINE)) {
            j.f641a.fine(l.a(false, i, i2, b2, b3));
        }
        if (i2 > this.e) {
            throw j.c("FRAME_SIZE_ERROR length > %d: %d", Integer.valueOf(this.e), Integer.valueOf(i2));
        } else if ((Integer.MIN_VALUE & i) != 0) {
            throw j.c("reserved bit set: %s", Integer.valueOf(i));
        } else {
            j.b(this.f649a, i2);
            this.f649a.i(b2 & 255);
            this.f649a.i(b3 & 255);
            this.f649a.g(Integer.MAX_VALUE & i);
        }
    }

    public synchronized void a(int i, int i2, List<e> list) {
        if (this.f) {
            throw new IOException("closed");
        } else if (this.c.b() != 0) {
            throw new IllegalStateException();
        } else {
            this.d.a(list);
            long b2 = this.c.b();
            int min = (int) Math.min((long) (this.e - 4), b2);
            a(i, min + 4, (byte) 5, b2 == ((long) min) ? (byte) 4 : 0);
            this.f649a.g(Integer.MAX_VALUE & i2);
            this.f649a.a_(this.c, (long) min);
            if (b2 > ((long) min)) {
                b(i, b2 - ((long) min));
            }
        }
    }

    public synchronized void a(int i, long j) {
        if (this.f) {
            throw new IOException("closed");
        } else if (j == 0 || j > 2147483647L) {
            throw j.c("windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: %s", Long.valueOf(j));
        } else {
            a(i, 4, (byte) 8, (byte) 0);
            this.f649a.g((int) j);
            this.f649a.flush();
        }
    }

    public synchronized void a(int i, a aVar) {
        if (this.f) {
            throw new IOException("closed");
        } else if (aVar.t == -1) {
            throw new IllegalArgumentException();
        } else {
            a(i, 4, (byte) 3, (byte) 0);
            this.f649a.g(aVar.s);
            this.f649a.flush();
        }
    }

    public synchronized void a(int i, a aVar, byte[] bArr) {
        if (this.f) {
            throw new IOException("closed");
        } else if (aVar.s == -1) {
            throw j.c("errorCode.httpCode == -1", new Object[0]);
        } else {
            a(0, bArr.length + 8, (byte) 7, (byte) 0);
            this.f649a.g(i);
            this.f649a.g(aVar.s);
            if (bArr.length > 0) {
                this.f649a.c(bArr);
            }
            this.f649a.flush();
        }
    }

    public synchronized void a(y yVar) {
        if (this.f) {
            throw new IOException("closed");
        }
        this.e = yVar.d(this.e);
        a(0, 0, (byte) 4, (byte) 1);
        this.f649a.flush();
    }

    public synchronized void a(boolean z, int i, int i2) {
        byte b2 = 0;
        synchronized (this) {
            if (this.f) {
                throw new IOException("closed");
            }
            if (z) {
                b2 = 1;
            }
            a(0, 8, (byte) 6, b2);
            this.f649a.g(i);
            this.f649a.g(i2);
            this.f649a.flush();
        }
    }

    public synchronized void a(boolean z, int i, f fVar, int i2) {
        if (this.f) {
            throw new IOException("closed");
        }
        byte b2 = 0;
        if (z) {
            b2 = (byte) 1;
        }
        a(i, b2, fVar, i2);
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z, int i, List<e> list) {
        if (this.f) {
            throw new IOException("closed");
        } else if (this.c.b() != 0) {
            throw new IllegalStateException();
        } else {
            this.d.a(list);
            long b2 = this.c.b();
            int min = (int) Math.min((long) this.e, b2);
            byte b3 = b2 == ((long) min) ? (byte) 4 : 0;
            if (z) {
                b3 = (byte) (b3 | 1);
            }
            a(i, min, (byte) 1, b3);
            this.f649a.a_(this.c, (long) min);
            if (b2 > ((long) min)) {
                b(i, b2 - ((long) min));
            }
        }
    }

    public synchronized void a(boolean z, boolean z2, int i, int i2, List<e> list) {
        if (z2) {
            throw new UnsupportedOperationException();
        } else if (this.f) {
            throw new IOException("closed");
        } else {
            a(z, i, list);
        }
    }

    public synchronized void b() {
        if (this.f) {
            throw new IOException("closed");
        }
        this.f649a.flush();
    }

    public synchronized void b(y yVar) {
        int i = 0;
        synchronized (this) {
            if (this.f) {
                throw new IOException("closed");
            }
            a(0, yVar.b() * 6, (byte) 4, (byte) 0);
            while (i < 10) {
                if (yVar.a(i)) {
                    this.f649a.h(i == 4 ? 3 : i == 7 ? 4 : i);
                    this.f649a.g(yVar.b(i));
                }
                i++;
            }
            this.f649a.flush();
        }
    }

    public int c() {
        return this.e;
    }

    public synchronized void close() {
        this.f = true;
        this.f649a.close();
    }
}
