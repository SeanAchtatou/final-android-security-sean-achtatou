package com.e.a.a.a;

import a.h;
import a.q;
import com.e.a.a;
import com.e.a.a.v;
import com.e.a.aj;
import com.e.a.ak;
import com.e.a.ap;
import com.e.a.au;
import java.net.ProtocolException;

class t implements ak {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f593a;

    /* renamed from: b  reason: collision with root package name */
    private final int f594b;
    private final ap c;
    private int d;

    t(q qVar, int i, ap apVar) {
        this.f593a = qVar;
        this.f594b = i;
        this.c = apVar;
    }

    public au a(ap apVar) {
        this.d++;
        if (this.f594b > 0) {
            aj ajVar = this.f593a.f589a.v().get(this.f594b - 1);
            a a2 = a().c().a();
            if (!apVar.a().getHost().equals(a2.a()) || v.a(apVar.a()) != a2.b()) {
                throw new IllegalStateException("network interceptor " + ajVar + " must retain the same host and port");
            } else if (this.d > 1) {
                throw new IllegalStateException("network interceptor " + ajVar + " must call proceed() exactly once");
            }
        }
        if (this.f594b < this.f593a.f589a.v().size()) {
            t tVar = new t(this.f593a, this.f594b + 1, apVar);
            aj ajVar2 = this.f593a.f589a.v().get(this.f594b);
            au a3 = ajVar2.a(tVar);
            if (tVar.d == 1) {
                return a3;
            }
            throw new IllegalStateException("network interceptor " + ajVar2 + " must call proceed() exactly once");
        }
        this.f593a.j.a(apVar);
        ap unused = this.f593a.m = apVar;
        if (this.f593a.c() && apVar.f() != null) {
            h a4 = q.a(this.f593a.j.a(apVar, apVar.f().b()));
            apVar.f().a(a4);
            a4.close();
        }
        au c2 = this.f593a.p();
        int c3 = c2.c();
        if ((c3 != 204 && c3 != 205) || c2.h().b() <= 0) {
            return c2;
        }
        throw new ProtocolException("HTTP " + c3 + " had non-zero Content-Length: " + c2.h().b());
    }

    public com.e.a.t a() {
        return this.f593a.e;
    }
}
