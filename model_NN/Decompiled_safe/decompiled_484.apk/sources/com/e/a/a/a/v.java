package com.e.a.a.a;

import a.aa;
import a.ab;
import a.q;
import com.e.a.ap;
import com.e.a.au;
import com.e.a.aw;
import com.e.a.ax;

public final class v implements ai {

    /* renamed from: a  reason: collision with root package name */
    private final q f595a;

    /* renamed from: b  reason: collision with root package name */
    private final g f596b;

    public v(q qVar, g gVar) {
        this.f595a = qVar;
        this.f596b = gVar;
    }

    private ab b(au auVar) {
        if (!q.a(auVar)) {
            return this.f596b.b(0);
        }
        if ("chunked".equalsIgnoreCase(auVar.a("Transfer-Encoding"))) {
            return this.f596b.a(this.f595a);
        }
        long a2 = w.a(auVar);
        return a2 != -1 ? this.f596b.b(a2) : this.f596b.i();
    }

    public aa a(ap apVar, long j) {
        if ("chunked".equalsIgnoreCase(apVar.a("Transfer-Encoding"))) {
            return this.f596b.h();
        }
        if (j != -1) {
            return this.f596b.a(j);
        }
        throw new IllegalStateException("Cannot stream a request body without chunked encoding or a known content length!");
    }

    public ax a(au auVar) {
        return new y(auVar.g(), q.a(b(auVar)));
    }

    public void a() {
        this.f596b.d();
    }

    public void a(ab abVar) {
        this.f596b.a(abVar);
    }

    public void a(ap apVar) {
        this.f595a.b();
        this.f596b.a(apVar.e(), aa.a(apVar, this.f595a.f().c().b().type(), this.f595a.f().l()));
    }

    public aw b() {
        return this.f596b.g();
    }

    public void c() {
        if (d()) {
            this.f596b.a();
        } else {
            this.f596b.b();
        }
    }

    public boolean d() {
        return !"close".equalsIgnoreCase(this.f595a.d().a("Connection")) && !"close".equalsIgnoreCase(this.f595a.e().a("Connection")) && !this.f596b.c();
    }
}
