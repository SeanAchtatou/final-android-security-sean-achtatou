package com.e.a.a.a;

import com.e.a.a.v;
import com.e.a.ap;
import com.e.a.au;
import com.e.a.b;
import com.e.a.r;
import com.e.a.z;
import java.net.Authenticator;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URL;
import java.util.List;

public final class a implements b {

    /* renamed from: a  reason: collision with root package name */
    public static final b f560a = new a();

    private InetAddress a(Proxy proxy, URL url) {
        return (proxy == null || proxy.type() == Proxy.Type.DIRECT) ? InetAddress.getByName(url.getHost()) : ((InetSocketAddress) proxy.address()).getAddress();
    }

    public ap a(Proxy proxy, au auVar) {
        PasswordAuthentication requestPasswordAuthentication;
        List<r> l = auVar.l();
        ap a2 = auVar.a();
        URL a3 = a2.a();
        int size = l.size();
        for (int i = 0; i < size; i++) {
            r rVar = l.get(i);
            if ("Basic".equalsIgnoreCase(rVar.a()) && (requestPasswordAuthentication = Authenticator.requestPasswordAuthentication(a3.getHost(), a(proxy, a3), v.a(a3), a3.getProtocol(), rVar.b(), rVar.a(), a3, Authenticator.RequestorType.SERVER)) != null) {
                return a2.g().a("Authorization", z.a(requestPasswordAuthentication.getUserName(), new String(requestPasswordAuthentication.getPassword()))).a();
            }
        }
        return null;
    }

    public ap b(Proxy proxy, au auVar) {
        List<r> l = auVar.l();
        ap a2 = auVar.a();
        URL a3 = a2.a();
        int size = l.size();
        for (int i = 0; i < size; i++) {
            r rVar = l.get(i);
            if ("Basic".equalsIgnoreCase(rVar.a())) {
                InetSocketAddress inetSocketAddress = (InetSocketAddress) proxy.address();
                PasswordAuthentication requestPasswordAuthentication = Authenticator.requestPasswordAuthentication(inetSocketAddress.getHostName(), a(proxy, a3), inetSocketAddress.getPort(), a3.getProtocol(), rVar.b(), rVar.a(), a3, Authenticator.RequestorType.PROXY);
                if (requestPasswordAuthentication != null) {
                    return a2.g().a("Proxy-Authorization", z.a(requestPasswordAuthentication.getUserName(), new String(requestPasswordAuthentication.getPassword()))).a();
                }
            }
        }
        return null;
    }
}
