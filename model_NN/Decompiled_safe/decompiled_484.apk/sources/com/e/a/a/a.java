package com.e.a.a;

import com.e.a.w;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.UnknownServiceException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLProtocolException;
import javax.net.ssl.SSLSocket;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final List<w> f558a;

    /* renamed from: b  reason: collision with root package name */
    private int f559b = 0;
    private boolean c;
    private boolean d;

    public a(List<w> list) {
        this.f558a = list;
    }

    private boolean b(SSLSocket sSLSocket) {
        int i = this.f559b;
        while (true) {
            int i2 = i;
            if (i2 >= this.f558a.size()) {
                return false;
            }
            if (this.f558a.get(i2).a(sSLSocket)) {
                return true;
            }
            i = i2 + 1;
        }
    }

    public w a(SSLSocket sSLSocket) {
        w wVar;
        int i = this.f559b;
        int size = this.f558a.size();
        int i2 = i;
        while (true) {
            if (i2 >= size) {
                wVar = null;
                break;
            }
            wVar = this.f558a.get(i2);
            if (wVar.a(sSLSocket)) {
                this.f559b = i2 + 1;
                break;
            }
            i2++;
        }
        if (wVar == null) {
            throw new UnknownServiceException("Unable to find acceptable protocols. isFallback=" + this.d + ", modes=" + this.f558a + ", supported protocols=" + Arrays.toString(sSLSocket.getEnabledProtocols()));
        }
        this.c = b(sSLSocket);
        k.f681b.a(wVar, sSLSocket, this.d);
        return wVar;
    }

    public boolean a(IOException iOException) {
        boolean z = true;
        this.d = true;
        if ((iOException instanceof ProtocolException) || (iOException instanceof InterruptedIOException)) {
            return false;
        }
        if (((iOException instanceof SSLHandshakeException) && (iOException.getCause() instanceof CertificateException)) || (iOException instanceof SSLPeerUnverifiedException)) {
            return false;
        }
        if ((!(iOException instanceof SSLHandshakeException) && !(iOException instanceof SSLProtocolException)) || !this.c) {
            z = false;
        }
        return z;
    }
}
