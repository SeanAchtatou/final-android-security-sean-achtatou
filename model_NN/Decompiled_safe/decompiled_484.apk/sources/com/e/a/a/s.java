package com.e.a.a;

import com.e.a.ao;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.net.ssl.SSLSocket;

class s extends q {

    /* renamed from: a  reason: collision with root package name */
    private final Method f689a;

    /* renamed from: b  reason: collision with root package name */
    private final Method f690b;
    private final Method c;
    private final Class<?> d;
    private final Class<?> e;

    public s(Method method, Method method2, Method method3, Class<?> cls, Class<?> cls2) {
        this.f689a = method;
        this.f690b = method2;
        this.c = method3;
        this.d = cls;
        this.e = cls2;
    }

    public void a(SSLSocket sSLSocket) {
        try {
            this.c.invoke(null, sSLSocket);
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new AssertionError();
        }
    }

    public void a(SSLSocket sSLSocket, String str, List<ao> list) {
        ArrayList arrayList = new ArrayList(list.size());
        int size = list.size();
        for (int i = 0; i < size; i++) {
            ao aoVar = list.get(i);
            if (aoVar != ao.HTTP_1_0) {
                arrayList.add(aoVar.toString());
            }
        }
        try {
            this.f689a.invoke(null, sSLSocket, Proxy.newProxyInstance(q.class.getClassLoader(), new Class[]{this.d, this.e}, new t(arrayList)));
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new AssertionError(e2);
        }
    }

    public String b(SSLSocket sSLSocket) {
        try {
            t tVar = (t) Proxy.getInvocationHandler(this.f690b.invoke(null, sSLSocket));
            if (tVar.f692b || tVar.c != null) {
                return tVar.f692b ? null : tVar.c;
            }
            k.f680a.log(Level.INFO, "ALPN callback dropped: SPDY and HTTP/2 are disabled. Is alpn-boot on the boot class path?");
            return null;
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new AssertionError();
        }
    }
}
