package com.e.a.a.a;

import com.e.a.ac;
import com.e.a.ao;
import com.e.a.ay;
import java.net.Socket;
import javax.net.ssl.SSLSocket;

public class af {

    /* renamed from: a  reason: collision with root package name */
    public final ay f569a;

    /* renamed from: b  reason: collision with root package name */
    public final Socket f570b;
    public final ao c;
    public final ac d;

    public af(ay ayVar, Socket socket) {
        this.f569a = ayVar;
        this.f570b = socket;
        this.c = null;
        this.d = null;
    }

    public af(ay ayVar, SSLSocket sSLSocket, ao aoVar, ac acVar) {
        this.f569a = ayVar;
        this.f570b = sSLSocket;
        this.c = aoVar;
        this.d = acVar;
    }
}
