package com.e.a.a.c;

import a.aa;
import a.f;
import a.h;
import a.j;
import a.k;
import a.q;
import android.support.v4.view.ViewCompat;
import android.support.v7.internal.widget.ActivityChooserView;
import com.e.a.a.v;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.zip.Deflater;

final class ab implements d {

    /* renamed from: a  reason: collision with root package name */
    private final h f609a;

    /* renamed from: b  reason: collision with root package name */
    private final f f610b = new f();
    private final h c;
    private final boolean d;
    private boolean e;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.k.<init>(a.aa, java.util.zip.Deflater):void
     arg types: [a.f, java.util.zip.Deflater]
     candidates:
      a.k.<init>(a.h, java.util.zip.Deflater):void
      a.k.<init>(a.aa, java.util.zip.Deflater):void */
    ab(h hVar, boolean z) {
        this.f609a = hVar;
        this.d = z;
        Deflater deflater = new Deflater();
        deflater.setDictionary(z.f665a);
        this.c = q.a(new k((aa) this.f610b, deflater));
    }

    private void a(List<e> list) {
        if (this.f610b.b() != 0) {
            throw new IllegalStateException();
        }
        this.c.g(list.size());
        int size = list.size();
        for (int i = 0; i < size; i++) {
            j jVar = list.get(i).h;
            this.c.g(jVar.f());
            this.c.b(jVar);
            j jVar2 = list.get(i).i;
            this.c.g(jVar2.f());
            this.c.b(jVar2);
        }
        this.c.flush();
    }

    public synchronized void a() {
    }

    /* access modifiers changed from: package-private */
    public void a(int i, int i2, f fVar, int i3) {
        if (this.e) {
            throw new IOException("closed");
        } else if (((long) i3) > 16777215) {
            throw new IllegalArgumentException("FRAME_TOO_LARGE max size is 16Mib: " + i3);
        } else {
            this.f609a.g(Integer.MAX_VALUE & i);
            this.f609a.g(((i2 & 255) << 24) | (16777215 & i3));
            if (i3 > 0) {
                this.f609a.a_(fVar, (long) i3);
            }
        }
    }

    public void a(int i, int i2, List<e> list) {
    }

    public synchronized void a(int i, long j) {
        if (this.e) {
            throw new IOException("closed");
        } else if (j == 0 || j > 2147483647L) {
            throw new IllegalArgumentException("windowSizeIncrement must be between 1 and 0x7fffffff: " + j);
        } else {
            this.f609a.g(-2147287031);
            this.f609a.g(8);
            this.f609a.g(i);
            this.f609a.g((int) j);
            this.f609a.flush();
        }
    }

    public synchronized void a(int i, a aVar) {
        if (this.e) {
            throw new IOException("closed");
        } else if (aVar.t == -1) {
            throw new IllegalArgumentException();
        } else {
            this.f609a.g(-2147287037);
            this.f609a.g(8);
            this.f609a.g(Integer.MAX_VALUE & i);
            this.f609a.g(aVar.t);
            this.f609a.flush();
        }
    }

    public synchronized void a(int i, a aVar, byte[] bArr) {
        if (this.e) {
            throw new IOException("closed");
        } else if (aVar.u == -1) {
            throw new IllegalArgumentException("errorCode.spdyGoAwayCode == -1");
        } else {
            this.f609a.g(-2147287033);
            this.f609a.g(8);
            this.f609a.g(i);
            this.f609a.g(aVar.u);
            this.f609a.flush();
        }
    }

    public void a(y yVar) {
    }

    public synchronized void a(boolean z, int i, int i2) {
        boolean z2 = true;
        synchronized (this) {
            if (this.e) {
                throw new IOException("closed");
            }
            if (this.d == ((i & 1) == 1)) {
                z2 = false;
            }
            if (z != z2) {
                throw new IllegalArgumentException("payload != reply");
            }
            this.f609a.g(-2147287034);
            this.f609a.g(4);
            this.f609a.g(i);
            this.f609a.flush();
        }
    }

    public synchronized void a(boolean z, int i, f fVar, int i2) {
        a(i, z ? 1 : 0, fVar, i2);
    }

    public synchronized void a(boolean z, boolean z2, int i, int i2, List<e> list) {
        int i3 = 0;
        synchronized (this) {
            if (this.e) {
                throw new IOException("closed");
            }
            a(list);
            int b2 = (int) (10 + this.f610b.b());
            int i4 = z ? 1 : 0;
            if (z2) {
                i3 = 2;
            }
            this.f609a.g(-2147287039);
            this.f609a.g((((i3 | i4) & 255) << 24) | (b2 & ViewCompat.MEASURED_SIZE_MASK));
            this.f609a.g(i & ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED);
            this.f609a.g(i2 & ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED);
            this.f609a.h(0);
            this.f609a.a(this.f610b);
            this.f609a.flush();
        }
    }

    public synchronized void b() {
        if (this.e) {
            throw new IOException("closed");
        }
        this.f609a.flush();
    }

    public synchronized void b(y yVar) {
        if (this.e) {
            throw new IOException("closed");
        }
        int b2 = yVar.b();
        this.f609a.g(-2147287036);
        this.f609a.g((((b2 * 8) + 4) & ViewCompat.MEASURED_SIZE_MASK) | 0);
        this.f609a.g(b2);
        for (int i = 0; i <= 10; i++) {
            if (yVar.a(i)) {
                this.f609a.g(((yVar.c(i) & 255) << 24) | (i & ViewCompat.MEASURED_SIZE_MASK));
                this.f609a.g(yVar.b(i));
            }
        }
        this.f609a.flush();
    }

    public int c() {
        return 16383;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.v.a(java.io.Closeable, java.io.Closeable):void
     arg types: [a.h, a.h]
     candidates:
      com.e.a.a.v.a(java.lang.String, int):int
      com.e.a.a.v.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
      com.e.a.a.v.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
      com.e.a.a.v.a(java.lang.Object, java.lang.Object):boolean
      com.e.a.a.v.a(java.io.Closeable, java.io.Closeable):void */
    public synchronized void close() {
        this.e = true;
        v.a((Closeable) this.f609a, (Closeable) this.c);
    }
}
