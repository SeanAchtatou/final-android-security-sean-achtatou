package com.e.a.a.a;

import com.e.a.a;
import com.e.a.a.k;
import com.e.a.a.n;
import com.e.a.a.u;
import com.e.a.a.v;
import com.e.a.am;
import com.e.a.ap;
import com.e.a.ay;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

public final class ad {

    /* renamed from: a  reason: collision with root package name */
    private final a f565a;

    /* renamed from: b  reason: collision with root package name */
    private final URI f566b;
    private final n c;
    private final am d;
    private final u e;
    private Proxy f;
    private InetSocketAddress g;
    private List<Proxy> h = Collections.emptyList();
    private int i;
    private List<InetSocketAddress> j = Collections.emptyList();
    private int k;
    private final List<ay> l = new ArrayList();

    private ad(a aVar, URI uri, am amVar) {
        this.f565a = aVar;
        this.f566b = uri;
        this.d = amVar;
        this.e = k.f681b.b(amVar);
        this.c = k.f681b.c(amVar);
        a(uri, aVar.i());
    }

    public static ad a(a aVar, ap apVar, am amVar) {
        return new ad(aVar, apVar.b(), amVar);
    }

    static String a(InetSocketAddress inetSocketAddress) {
        InetAddress address = inetSocketAddress.getAddress();
        return address == null ? inetSocketAddress.getHostName() : address.getHostAddress();
    }

    private void a(Proxy proxy) {
        int i2;
        String str;
        this.j = new ArrayList();
        if (proxy.type() == Proxy.Type.DIRECT || proxy.type() == Proxy.Type.SOCKS) {
            str = this.f565a.a();
            i2 = v.a(this.f566b);
        } else {
            SocketAddress address = proxy.address();
            if (!(address instanceof InetSocketAddress)) {
                throw new IllegalArgumentException("Proxy.address() is not an InetSocketAddress: " + address.getClass());
            }
            InetSocketAddress inetSocketAddress = (InetSocketAddress) address;
            String a2 = a(inetSocketAddress);
            int port = inetSocketAddress.getPort();
            str = a2;
            i2 = port;
        }
        if (i2 < 1 || i2 > 65535) {
            throw new SocketException("No route to " + str + ":" + i2 + "; port is out of range");
        }
        for (InetAddress inetSocketAddress2 : this.c.a(str)) {
            this.j.add(new InetSocketAddress(inetSocketAddress2, i2));
        }
        this.k = 0;
    }

    private void a(URI uri, Proxy proxy) {
        if (proxy != null) {
            this.h = Collections.singletonList(proxy);
        } else {
            this.h = new ArrayList();
            List<Proxy> select = this.d.e().select(uri);
            if (select != null) {
                this.h.addAll(select);
            }
            this.h.removeAll(Collections.singleton(Proxy.NO_PROXY));
            this.h.add(Proxy.NO_PROXY);
        }
        this.i = 0;
    }

    private boolean c() {
        return this.i < this.h.size();
    }

    private Proxy d() {
        if (!c()) {
            throw new SocketException("No route to " + this.f565a.a() + "; exhausted proxy configurations: " + this.h);
        }
        List<Proxy> list = this.h;
        int i2 = this.i;
        this.i = i2 + 1;
        Proxy proxy = list.get(i2);
        a(proxy);
        return proxy;
    }

    private boolean e() {
        return this.k < this.j.size();
    }

    private InetSocketAddress f() {
        if (!e()) {
            throw new SocketException("No route to " + this.f565a.a() + "; exhausted inet socket addresses: " + this.j);
        }
        List<InetSocketAddress> list = this.j;
        int i2 = this.k;
        this.k = i2 + 1;
        return list.get(i2);
    }

    private boolean g() {
        return !this.l.isEmpty();
    }

    private ay h() {
        return this.l.remove(0);
    }

    public void a(ay ayVar, IOException iOException) {
        if (!(ayVar.b().type() == Proxy.Type.DIRECT || this.f565a.j() == null)) {
            this.f565a.j().connectFailed(this.f566b, ayVar.b().address(), iOException);
        }
        this.e.a(ayVar);
    }

    public boolean a() {
        return e() || c() || g();
    }

    public ay b() {
        if (!e()) {
            if (c()) {
                this.f = d();
            } else if (g()) {
                return h();
            } else {
                throw new NoSuchElementException();
            }
        }
        this.g = f();
        ay ayVar = new ay(this.f565a, this.f, this.g);
        if (!this.e.c(ayVar)) {
            return ayVar;
        }
        this.l.add(ayVar);
        return b();
    }
}
