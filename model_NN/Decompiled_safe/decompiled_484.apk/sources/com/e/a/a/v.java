package com.e.a.a;

import a.ab;
import a.f;
import a.j;
import android.support.v4.media.session.PlaybackStateCompat;
import com.qihoo.dynamic.util.Md5Util;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.Socket;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public final class v {

    /* renamed from: a  reason: collision with root package name */
    public static final byte[] f694a = new byte[0];

    /* renamed from: b  reason: collision with root package name */
    public static final String[] f695b = new String[0];
    public static final Charset c = Charset.forName(Md5Util.DEFAULT_CHARSET);

    public static int a(String str) {
        if ("http".equals(str)) {
            return 80;
        }
        return "https".equals(str) ? 443 : -1;
    }

    private static int a(String str, int i) {
        return i != -1 ? i : a(str);
    }

    public static int a(URI uri) {
        return a(uri.getScheme(), uri.getPort());
    }

    public static int a(URL url) {
        return a(url.getProtocol(), url.getPort());
    }

    public static j a(j jVar) {
        try {
            return j.a(MessageDigest.getInstance("SHA-1").digest(jVar.g()));
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    public static <T> List<T> a(List list) {
        return Collections.unmodifiableList(new ArrayList(list));
    }

    public static <T> List<T> a(Object... objArr) {
        return Collections.unmodifiableList(Arrays.asList((Object[]) objArr.clone()));
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private static <T> java.util.List<T> a(T[] r9, T[] r10) {
        /*
            r1 = 0
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            int r4 = r9.length
            r2 = r1
        L_0x0008:
            if (r2 >= r4) goto L_0x0022
            r5 = r9[r2]
            int r6 = r10.length
            r0 = r1
        L_0x000e:
            if (r0 >= r6) goto L_0x001b
            r7 = r10[r0]
            boolean r8 = r5.equals(r7)
            if (r8 == 0) goto L_0x001f
            r3.add(r7)
        L_0x001b:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x0008
        L_0x001f:
            int r0 = r0 + 1
            goto L_0x000e
        L_0x0022:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.e.a.a.v.a(java.lang.Object[], java.lang.Object[]):java.util.List");
    }

    public static <K, V> Map<K, V> a(Map map) {
        return Collections.unmodifiableMap(new LinkedHashMap(map));
    }

    public static ThreadFactory a(String str, boolean z) {
        return new w(str, z);
    }

    public static void a(long j, long j2, long j3) {
        if ((j2 | j3) < 0 || j2 > j || j - j2 < j3) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e2) {
            }
        }
    }

    public static void a(Closeable closeable, Closeable closeable2) {
        Throwable th = null;
        try {
            closeable.close();
        } catch (Throwable th2) {
            th = th2;
        }
        try {
            closeable2.close();
        } catch (Throwable th3) {
            if (th == null) {
                th = th3;
            }
        }
        if (th != null) {
            if (th instanceof IOException) {
                throw ((IOException) th);
            } else if (th instanceof RuntimeException) {
                throw ((RuntimeException) th);
            } else if (th instanceof Error) {
                throw ((Error) th);
            } else {
                throw new AssertionError(th);
            }
        }
    }

    public static void a(Socket socket) {
        if (socket != null) {
            try {
                socket.close();
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e2) {
            }
        }
    }

    public static boolean a(ab abVar, int i, TimeUnit timeUnit) {
        try {
            return b(abVar, i, timeUnit);
        } catch (IOException e) {
            return false;
        }
    }

    public static boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.v.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
     arg types: [T[], T[]]
     candidates:
      com.e.a.a.v.a(java.lang.String, int):int
      com.e.a.a.v.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
      com.e.a.a.v.a(java.io.Closeable, java.io.Closeable):void
      com.e.a.a.v.a(java.lang.Object, java.lang.Object):boolean
      com.e.a.a.v.a(java.lang.Object[], java.lang.Object[]):java.util.List<T> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<T>, int]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException} */
    public static <T> T[] a(Class<T> cls, T[] tArr, T[] tArr2) {
        List a2 = a((Object[]) tArr, (Object[]) tArr2);
        return a2.toArray((Object[]) Array.newInstance((Class<?>) cls, a2.size()));
    }

    public static String b(String str) {
        try {
            return j.a(MessageDigest.getInstance(Md5Util.ALGORITHM).digest(str.getBytes(Md5Util.DEFAULT_CHARSET))).d();
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    public static boolean b(ab abVar, int i, TimeUnit timeUnit) {
        long nanoTime = System.nanoTime();
        long d = abVar.a().c_() ? abVar.a().d() - nanoTime : Long.MAX_VALUE;
        abVar.a().a(Math.min(d, timeUnit.toNanos((long) i)) + nanoTime);
        try {
            f fVar = new f();
            while (abVar.a(fVar, PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH) != -1) {
                fVar.t();
            }
            if (d == Long.MAX_VALUE) {
                abVar.a().f();
            } else {
                abVar.a().a(d + nanoTime);
            }
            return true;
        } catch (InterruptedIOException e) {
            if (d == Long.MAX_VALUE) {
                abVar.a().f();
            } else {
                abVar.a().a(d + nanoTime);
            }
            return false;
        } catch (Throwable th) {
            if (d == Long.MAX_VALUE) {
                abVar.a().f();
            } else {
                abVar.a().a(d + nanoTime);
            }
            throw th;
        }
    }
}
