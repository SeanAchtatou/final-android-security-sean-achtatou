package com.e.a.a.a;

import a.ab;
import a.ac;
import a.n;
import com.e.a.a.k;
import com.e.a.a.v;

abstract class i implements ab {

    /* renamed from: a  reason: collision with root package name */
    protected final n f581a;

    /* renamed from: b  reason: collision with root package name */
    protected boolean f582b;
    final /* synthetic */ g c;

    private i(g gVar) {
        this.c = gVar;
        this.f581a = new n(this.c.d.a());
    }

    public ac a() {
        return this.f581a;
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z) {
        if (this.c.f != 5) {
            throw new IllegalStateException("state: " + this.c.f);
        }
        this.c.a(this.f581a);
        int unused = this.c.f = 0;
        if (z && this.c.g == 1) {
            int unused2 = this.c.g = 0;
            k.f681b.a(this.c.f579a, this.c.f580b);
        } else if (this.c.g == 2) {
            int unused3 = this.c.f = 6;
            this.c.f580b.d().close();
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        v.a(this.c.f580b.d());
        int unused = this.c.f = 6;
    }
}
