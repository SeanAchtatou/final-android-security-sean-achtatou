package com.e.a.a.a;

import java.util.Comparator;

final class x implements Comparator<String> {
    x() {
    }

    /* renamed from: a */
    public int compare(String str, String str2) {
        if (str == str2) {
            return 0;
        }
        if (str == null) {
            return -1;
        }
        if (str2 == null) {
            return 1;
        }
        return String.CASE_INSENSITIVE_ORDER.compare(str, str2);
    }
}
