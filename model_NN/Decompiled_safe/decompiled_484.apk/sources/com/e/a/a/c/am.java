package com.e.a.a.c;

import com.e.a.a.k;
import com.e.a.a.m;
import java.io.IOException;
import java.util.logging.Level;

class am extends m {

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ao f623b;
    final /* synthetic */ al c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    am(al alVar, String str, Object[] objArr, ao aoVar) {
        super(str, objArr);
        this.c = alVar;
        this.f623b = aoVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public void a() {
        try {
            this.c.c.m.a(this.f623b);
        } catch (IOException e) {
            k.f680a.log(Level.INFO, "StreamHandler failure for " + this.c.c.o, (Throwable) e);
            try {
                this.f623b.a(a.PROTOCOL_ERROR);
            } catch (IOException e2) {
            }
        }
    }
}
