package com.e.a.a.a;

import a.f;
import com.e.a.a.v;
import com.e.a.af;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;

class k extends i {
    final /* synthetic */ g d;
    private long e = -1;
    private boolean f = true;
    private final q g;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    k(g gVar, q qVar) {
        super(gVar);
        this.d = gVar;
        this.g = qVar;
    }

    private void c() {
        if (this.e != -1) {
            this.d.d.r();
        }
        try {
            this.e = this.d.d.o();
            String trim = this.d.d.r().trim();
            if (this.e < 0 || (!trim.isEmpty() && !trim.startsWith(";"))) {
                throw new ProtocolException("expected chunk size and optional extensions but was \"" + this.e + trim + "\"");
            } else if (this.e == 0) {
                this.f = false;
                af afVar = new af();
                this.d.a(afVar);
                this.g.a(afVar.a());
                a(true);
            }
        } catch (NumberFormatException e2) {
            throw new ProtocolException(e2.getMessage());
        }
    }

    public long a(f fVar, long j) {
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f582b) {
            throw new IllegalStateException("closed");
        } else if (!this.f) {
            return -1;
        } else {
            if (this.e == 0 || this.e == -1) {
                c();
                if (!this.f) {
                    return -1;
                }
            }
            long a2 = this.d.d.a(fVar, Math.min(j, this.e));
            if (a2 == -1) {
                b();
                throw new IOException("unexpected end of stream");
            }
            this.e -= a2;
            return a2;
        }
    }

    public void close() {
        if (!this.f582b) {
            if (this.f && !v.a(this, 100, TimeUnit.MILLISECONDS)) {
                b();
            }
            this.f582b = true;
        }
    }
}
