package com.e.a.a;

import a.aa;
import java.io.IOException;

class d extends j {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f666a = (!b.class.desiredAssertionStatus());

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ b f667b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    d(b bVar, aa aaVar) {
        super(aaVar);
        this.f667b = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.b.a(com.e.a.a.b, boolean):boolean
     arg types: [com.e.a.a.b, int]
     candidates:
      com.e.a.a.b.a(com.e.a.a.b, int):int
      com.e.a.a.b.a(java.lang.String, long):com.e.a.a.f
      com.e.a.a.b.a(com.e.a.a.f, boolean):void
      com.e.a.a.b.a(com.e.a.a.b, com.e.a.a.h):boolean
      com.e.a.a.b.a(com.e.a.a.b, boolean):boolean */
    /* access modifiers changed from: protected */
    public void a(IOException iOException) {
        if (f666a || Thread.holdsLock(this.f667b)) {
            boolean unused = this.f667b.o = true;
            return;
        }
        throw new AssertionError();
    }
}
