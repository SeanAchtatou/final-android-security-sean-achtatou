package com.e.a.a.b;

import a.aa;
import a.ab;
import a.q;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

final class b implements a {
    b() {
    }

    public ab a(File file) {
        return q.a(file);
    }

    public void a(File file, File file2) {
        d(file2);
        if (!file.renameTo(file2)) {
            throw new IOException("failed to rename " + file + " to " + file2);
        }
    }

    public aa b(File file) {
        try {
            return q.b(file);
        } catch (FileNotFoundException e) {
            file.getParentFile().mkdirs();
            return q.b(file);
        }
    }

    public aa c(File file) {
        try {
            return q.c(file);
        } catch (FileNotFoundException e) {
            file.getParentFile().mkdirs();
            return q.c(file);
        }
    }

    public void d(File file) {
        if (!file.delete() && file.exists()) {
            throw new IOException("failed to delete " + file);
        }
    }

    public boolean e(File file) {
        return file.exists();
    }

    public long f(File file) {
        return file.length();
    }

    public void g(File file) {
        File[] listFiles = file.listFiles();
        if (listFiles == null) {
            throw new IOException("not a readable directory: " + file);
        }
        for (File file2 : listFiles) {
            if (file2.isDirectory()) {
                g(file2);
            }
            if (!file2.delete()) {
                throw new IOException("failed to delete " + file2);
            }
        }
    }
}
