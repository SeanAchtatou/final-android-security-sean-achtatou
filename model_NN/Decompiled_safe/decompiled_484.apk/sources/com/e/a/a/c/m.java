package com.e.a.a.c;

import a.i;
import a.j;
import android.support.v7.internal.widget.ActivityChooserView;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;

final class m implements b {

    /* renamed from: a  reason: collision with root package name */
    final h f647a;

    /* renamed from: b  reason: collision with root package name */
    private final i f648b;
    private final k c = new k(this.f648b);
    private final boolean d;

    m(i iVar, int i, boolean z) {
        this.f648b = iVar;
        this.d = z;
        this.f647a = new h(i, this.c);
    }

    private List<e> a(int i, short s, byte b2, int i2) {
        k kVar = this.c;
        this.c.d = i;
        kVar.f643a = i;
        this.c.e = s;
        this.c.f644b = b2;
        this.c.c = i2;
        this.f647a.a();
        return this.f647a.b();
    }

    private void a(c cVar, int i) {
        int k = this.f648b.k();
        cVar.a(i, k & ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED, (this.f648b.i() & 255) + 1, (Integer.MIN_VALUE & k) != 0);
    }

    private void a(c cVar, int i, byte b2, int i2) {
        if (i2 == 0) {
            throw j.d("PROTOCOL_ERROR: TYPE_HEADERS streamId == 0", new Object[0]);
        }
        boolean z = (b2 & 1) != 0;
        short i3 = (b2 & 8) != 0 ? (short) (this.f648b.i() & 255) : 0;
        if ((b2 & 32) != 0) {
            a(cVar, i2);
            i -= 5;
        }
        cVar.a(false, z, i2, -1, a(j.b(i, b2, i3), i3, b2, i2), f.HTTP_20_HEADERS);
    }

    private void b(c cVar, int i, byte b2, int i2) {
        boolean z = true;
        short s = 0;
        boolean z2 = (b2 & 1) != 0;
        if ((b2 & 32) == 0) {
            z = false;
        }
        if (z) {
            throw j.d("PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA", new Object[0]);
        }
        if ((b2 & 8) != 0) {
            s = (short) (this.f648b.i() & 255);
        }
        cVar.a(z2, i2, this.f648b, j.b(i, b2, s));
        this.f648b.g((long) s);
    }

    private void c(c cVar, int i, byte b2, int i2) {
        if (i != 5) {
            throw j.d("TYPE_PRIORITY length: %d != 5", Integer.valueOf(i));
        } else if (i2 == 0) {
            throw j.d("TYPE_PRIORITY streamId == 0", new Object[0]);
        } else {
            a(cVar, i2);
        }
    }

    private void d(c cVar, int i, byte b2, int i2) {
        if (i != 4) {
            throw j.d("TYPE_RST_STREAM length: %d != 4", Integer.valueOf(i));
        } else if (i2 == 0) {
            throw j.d("TYPE_RST_STREAM streamId == 0", new Object[0]);
        } else {
            int k = this.f648b.k();
            a b3 = a.b(k);
            if (b3 == null) {
                throw j.d("TYPE_RST_STREAM unexpected error code: %d", Integer.valueOf(k));
            } else {
                cVar.a(i2, b3);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.c.c.a(boolean, com.e.a.a.c.y):void
     arg types: [int, com.e.a.a.c.y]
     candidates:
      com.e.a.a.c.c.a(int, long):void
      com.e.a.a.c.c.a(int, com.e.a.a.c.a):void
      com.e.a.a.c.c.a(boolean, com.e.a.a.c.y):void */
    private void e(c cVar, int i, byte b2, int i2) {
        if (i2 != 0) {
            throw j.d("TYPE_SETTINGS streamId != 0", new Object[0]);
        } else if ((b2 & 1) != 0) {
            if (i != 0) {
                throw j.d("FRAME_SIZE_ERROR ack frame should be empty!", new Object[0]);
            }
            cVar.b();
        } else if (i % 6 != 0) {
            throw j.d("TYPE_SETTINGS length %% 6 != 0: %s", Integer.valueOf(i));
        } else {
            y yVar = new y();
            for (int i3 = 0; i3 < i; i3 += 6) {
                short j = this.f648b.j();
                int k = this.f648b.k();
                switch (j) {
                    case 1:
                    case 6:
                        break;
                    case 2:
                        if (!(k == 0 || k == 1)) {
                            throw j.d("PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1", new Object[0]);
                        }
                    case 3:
                        j = 4;
                        break;
                    case 4:
                        j = 7;
                        if (k >= 0) {
                            break;
                        } else {
                            throw j.d("PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1", new Object[0]);
                        }
                    case 5:
                        if (k >= 16384 && k <= 16777215) {
                            break;
                        } else {
                            throw j.d("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s", Integer.valueOf(k));
                        }
                    default:
                        throw j.d("PROTOCOL_ERROR invalid settings id: %s", Short.valueOf(j));
                }
                yVar.a(j, 0, k);
            }
            cVar.a(false, yVar);
            if (yVar.c() >= 0) {
                this.f647a.a(yVar.c());
            }
        }
    }

    private void f(c cVar, int i, byte b2, int i2) {
        short s = 0;
        if (i2 == 0) {
            throw j.d("PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0", new Object[0]);
        }
        if ((b2 & 8) != 0) {
            s = (short) (this.f648b.i() & 255);
        }
        cVar.a(i2, this.f648b.k() & ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED, a(j.b(i - 4, b2, s), s, b2, i2));
    }

    private void g(c cVar, int i, byte b2, int i2) {
        boolean z = true;
        if (i != 8) {
            throw j.d("TYPE_PING length != 8: %s", Integer.valueOf(i));
        } else if (i2 != 0) {
            throw j.d("TYPE_PING streamId != 0", new Object[0]);
        } else {
            int k = this.f648b.k();
            int k2 = this.f648b.k();
            if ((b2 & 1) == 0) {
                z = false;
            }
            cVar.a(z, k, k2);
        }
    }

    private void h(c cVar, int i, byte b2, int i2) {
        if (i < 8) {
            throw j.d("TYPE_GOAWAY length < 8: %s", Integer.valueOf(i));
        } else if (i2 != 0) {
            throw j.d("TYPE_GOAWAY streamId != 0", new Object[0]);
        } else {
            int k = this.f648b.k();
            int k2 = this.f648b.k();
            int i3 = i - 8;
            a b3 = a.b(k2);
            if (b3 == null) {
                throw j.d("TYPE_GOAWAY unexpected error code: %d", Integer.valueOf(k2));
            }
            j jVar = j.f14b;
            if (i3 > 0) {
                jVar = this.f648b.c((long) i3);
            }
            cVar.a(k, b3, jVar);
        }
    }

    private void i(c cVar, int i, byte b2, int i2) {
        if (i != 4) {
            throw j.d("TYPE_WINDOW_UPDATE length !=4: %s", Integer.valueOf(i));
        }
        long k = ((long) this.f648b.k()) & 2147483647L;
        if (k == 0) {
            throw j.d("windowSizeIncrement was 0", Long.valueOf(k));
        } else {
            cVar.a(i2, k);
        }
    }

    public void a() {
        if (!this.d) {
            j c2 = this.f648b.c((long) j.f642b.f());
            if (j.f641a.isLoggable(Level.FINE)) {
                j.f641a.fine(String.format("<< CONNECTION %s", c2.d()));
            }
            if (!j.f642b.equals(c2)) {
                throw j.d("Expected a connection header but was %s", c2.a());
            }
        }
    }

    public boolean a(c cVar) {
        try {
            this.f648b.a(9);
            int a2 = j.b(this.f648b);
            if (a2 < 0 || a2 > 16384) {
                throw j.d("FRAME_SIZE_ERROR: %s", Integer.valueOf(a2));
            }
            byte i = (byte) (this.f648b.i() & 255);
            byte i2 = (byte) (this.f648b.i() & 255);
            int k = this.f648b.k() & ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
            if (j.f641a.isLoggable(Level.FINE)) {
                j.f641a.fine(l.a(true, k, a2, i, i2));
            }
            switch (i) {
                case 0:
                    b(cVar, a2, i2, k);
                    return true;
                case 1:
                    a(cVar, a2, i2, k);
                    return true;
                case 2:
                    c(cVar, a2, i2, k);
                    return true;
                case 3:
                    d(cVar, a2, i2, k);
                    return true;
                case 4:
                    e(cVar, a2, i2, k);
                    return true;
                case 5:
                    f(cVar, a2, i2, k);
                    return true;
                case 6:
                    g(cVar, a2, i2, k);
                    return true;
                case 7:
                    h(cVar, a2, i2, k);
                    return true;
                case 8:
                    i(cVar, a2, i2, k);
                    return true;
                default:
                    this.f648b.g((long) a2);
                    return true;
            }
        } catch (IOException e) {
            return false;
        }
    }

    public void close() {
        this.f648b.close();
    }
}
