package com.e.a.a;

import com.e.a.a.a.ai;
import com.e.a.a.a.q;
import com.e.a.af;
import com.e.a.am;
import com.e.a.ao;
import com.e.a.ap;
import com.e.a.t;
import com.e.a.u;
import com.e.a.w;
import java.util.logging.Logger;
import javax.net.ssl.SSLSocket;

public abstract class k {

    /* renamed from: a  reason: collision with root package name */
    public static final Logger f680a = Logger.getLogger(am.class.getName());

    /* renamed from: b  reason: collision with root package name */
    public static k f681b;

    public abstract ai a(t tVar, q qVar);

    public abstract l a(am amVar);

    public abstract void a(af afVar, String str);

    public abstract void a(am amVar, t tVar, q qVar, ap apVar);

    public abstract void a(t tVar, ao aoVar);

    public abstract void a(u uVar, t tVar);

    public abstract void a(w wVar, SSLSocket sSLSocket, boolean z);

    public abstract boolean a(t tVar);

    public abstract int b(t tVar);

    public abstract u b(am amVar);

    public abstract void b(t tVar, q qVar);

    public abstract n c(am amVar);

    public abstract boolean c(t tVar);
}
