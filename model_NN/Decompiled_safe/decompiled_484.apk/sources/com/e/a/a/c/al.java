package com.e.a.a.c;

import a.i;
import a.j;
import a.q;
import com.e.a.a.m;
import com.e.a.a.v;
import com.e.a.ao;
import java.io.IOException;
import java.util.List;

class al extends m implements c {

    /* renamed from: b  reason: collision with root package name */
    b f622b;
    final /* synthetic */ ac c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private al(ac acVar) {
        super("OkHttp %s", acVar.o);
        this.c = acVar;
    }

    /* synthetic */ al(ac acVar, ad adVar) {
        this(acVar);
    }

    private void a(y yVar) {
        ac.l.execute(new an(this, "OkHttp %s ACK Settings", new Object[]{this.c.o}, yVar));
    }

    /* access modifiers changed from: protected */
    public void a() {
        a aVar;
        Throwable th;
        a aVar2 = a.INTERNAL_ERROR;
        a aVar3 = a.INTERNAL_ERROR;
        try {
            this.f622b = this.c.g.a(q.a(q.b(this.c.h)), this.c.f612b);
            if (!this.c.f612b) {
                this.f622b.a();
            }
            do {
            } while (this.f622b.a(this));
            try {
                this.c.a(a.NO_ERROR, a.CANCEL);
            } catch (IOException e) {
            }
            v.a(this.f622b);
        } catch (IOException e2) {
            aVar = a.PROTOCOL_ERROR;
            try {
                this.c.a(aVar, a.PROTOCOL_ERROR);
            } catch (IOException e3) {
            }
            v.a(this.f622b);
        } catch (Throwable th2) {
            th = th2;
            this.c.a(aVar, aVar3);
            v.a(this.f622b);
            throw th;
        }
    }

    public void a(int i, int i2, int i3, boolean z) {
    }

    public void a(int i, int i2, List<e> list) {
        this.c.a(i2, list);
    }

    public void a(int i, long j) {
        if (i == 0) {
            synchronized (this.c) {
                this.c.d += j;
                this.c.notifyAll();
            }
            return;
        }
        ao a2 = this.c.a(i);
        if (a2 != null) {
            synchronized (a2) {
                a2.a(j);
            }
        }
    }

    public void a(int i, a aVar) {
        if (this.c.d(i)) {
            this.c.c(i, aVar);
            return;
        }
        ao b2 = this.c.b(i);
        if (b2 != null) {
            b2.c(aVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.c.ac.b(com.e.a.a.c.ac, boolean):boolean
     arg types: [com.e.a.a.c.ac, int]
     candidates:
      com.e.a.a.c.ac.b(com.e.a.a.c.ac, int):int
      com.e.a.a.c.ac.b(int, com.e.a.a.c.a):void
      com.e.a.a.c.ac.b(com.e.a.a.c.ac, boolean):boolean */
    public void a(int i, a aVar, j jVar) {
        ao[] aoVarArr;
        if (jVar.f() > 0) {
        }
        synchronized (this.c) {
            aoVarArr = (ao[]) this.c.n.values().toArray(new ao[this.c.n.size()]);
            boolean unused = this.c.r = true;
        }
        for (ao aoVar : aoVarArr) {
            if (aoVar.a() > i && aoVar.c()) {
                aoVar.c(a.REFUSED_STREAM);
                this.c.b(aoVar.a());
            }
        }
    }

    public void a(boolean z, int i, int i2) {
        if (z) {
            v c2 = this.c.c(i);
            if (c2 != null) {
                c2.b();
                return;
            }
            return;
        }
        this.c.a(true, i, i2, null);
    }

    public void a(boolean z, int i, i iVar, int i2) {
        if (this.c.d(i)) {
            this.c.a(i, iVar, i2, z);
            return;
        }
        ao a2 = this.c.a(i);
        if (a2 == null) {
            this.c.a(i, a.INVALID_STREAM);
            iVar.g((long) i2);
            return;
        }
        a2.a(iVar, i2);
        if (z) {
            a2.h();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.c.ac.a(com.e.a.a.c.ac, boolean):boolean
     arg types: [com.e.a.a.c.ac, int]
     candidates:
      com.e.a.a.c.ac.a(int, java.util.List<com.e.a.a.c.e>):void
      com.e.a.a.c.ac.a(com.e.a.a.c.a, com.e.a.a.c.a):void
      com.e.a.a.c.ac.a(com.e.a.a.c.ac, int):boolean
      com.e.a.a.c.ac.a(int, long):void
      com.e.a.a.c.ac.a(int, com.e.a.a.c.a):void
      com.e.a.a.c.ac.a(com.e.a.a.c.ac, boolean):boolean */
    public void a(boolean z, y yVar) {
        ao[] aoVarArr;
        long j;
        synchronized (this.c) {
            int e = this.c.f.e(65536);
            if (z) {
                this.c.f.a();
            }
            this.c.f.a(yVar);
            if (this.c.a() == ao.HTTP_2) {
                a(yVar);
            }
            int e2 = this.c.f.e(65536);
            if (e2 == -1 || e2 == e) {
                aoVarArr = null;
                j = 0;
            } else {
                j = (long) (e2 - e);
                if (!this.c.x) {
                    this.c.a(j);
                    boolean unused = this.c.x = true;
                }
                aoVarArr = !this.c.n.isEmpty() ? (ao[]) this.c.n.values().toArray(new ao[this.c.n.size()]) : null;
            }
        }
        if (aoVarArr != null && j != 0) {
            for (ao aoVar : aoVarArr) {
                synchronized (aoVar) {
                    aoVar.a(j);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0092, code lost:
        if (r14.b() == false) goto L_0x00a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0094, code lost:
        r0.b(com.e.a.a.c.a.PROTOCOL_ERROR);
        r8.c.b(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a0, code lost:
        r0.a(r13, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a3, code lost:
        if (r10 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00a5, code lost:
        r0.h();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(boolean r9, boolean r10, int r11, int r12, java.util.List<com.e.a.a.c.e> r13, com.e.a.a.c.f r14) {
        /*
            r8 = this;
            com.e.a.a.c.ac r0 = r8.c
            boolean r0 = r0.d(r11)
            if (r0 == 0) goto L_0x000e
            com.e.a.a.c.ac r0 = r8.c
            r0.a(r11, r13, r10)
        L_0x000d:
            return
        L_0x000e:
            com.e.a.a.c.ac r6 = r8.c
            monitor-enter(r6)
            com.e.a.a.c.ac r0 = r8.c     // Catch:{ all -> 0x001b }
            boolean r0 = r0.r     // Catch:{ all -> 0x001b }
            if (r0 == 0) goto L_0x001e
            monitor-exit(r6)     // Catch:{ all -> 0x001b }
            goto L_0x000d
        L_0x001b:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x001b }
            throw r0
        L_0x001e:
            com.e.a.a.c.ac r0 = r8.c     // Catch:{ all -> 0x001b }
            com.e.a.a.c.ao r0 = r0.a(r11)     // Catch:{ all -> 0x001b }
            if (r0 != 0) goto L_0x008d
            boolean r0 = r14.a()     // Catch:{ all -> 0x001b }
            if (r0 == 0) goto L_0x0035
            com.e.a.a.c.ac r0 = r8.c     // Catch:{ all -> 0x001b }
            com.e.a.a.c.a r1 = com.e.a.a.c.a.INVALID_STREAM     // Catch:{ all -> 0x001b }
            r0.a(r11, r1)     // Catch:{ all -> 0x001b }
            monitor-exit(r6)     // Catch:{ all -> 0x001b }
            goto L_0x000d
        L_0x0035:
            com.e.a.a.c.ac r0 = r8.c     // Catch:{ all -> 0x001b }
            int r0 = r0.p     // Catch:{ all -> 0x001b }
            if (r11 > r0) goto L_0x003f
            monitor-exit(r6)     // Catch:{ all -> 0x001b }
            goto L_0x000d
        L_0x003f:
            int r0 = r11 % 2
            com.e.a.a.c.ac r1 = r8.c     // Catch:{ all -> 0x001b }
            int r1 = r1.q     // Catch:{ all -> 0x001b }
            int r1 = r1 % 2
            if (r0 != r1) goto L_0x004d
            monitor-exit(r6)     // Catch:{ all -> 0x001b }
            goto L_0x000d
        L_0x004d:
            com.e.a.a.c.ao r0 = new com.e.a.a.c.ao     // Catch:{ all -> 0x001b }
            com.e.a.a.c.ac r2 = r8.c     // Catch:{ all -> 0x001b }
            r1 = r11
            r3 = r9
            r4 = r10
            r5 = r13
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x001b }
            com.e.a.a.c.ac r1 = r8.c     // Catch:{ all -> 0x001b }
            int unused = r1.p = r11     // Catch:{ all -> 0x001b }
            com.e.a.a.c.ac r1 = r8.c     // Catch:{ all -> 0x001b }
            java.util.Map r1 = r1.n     // Catch:{ all -> 0x001b }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x001b }
            r1.put(r2, r0)     // Catch:{ all -> 0x001b }
            java.util.concurrent.ExecutorService r1 = com.e.a.a.c.ac.l     // Catch:{ all -> 0x001b }
            com.e.a.a.c.am r2 = new com.e.a.a.c.am     // Catch:{ all -> 0x001b }
            java.lang.String r3 = "OkHttp %s stream %d"
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x001b }
            r5 = 0
            com.e.a.a.c.ac r7 = r8.c     // Catch:{ all -> 0x001b }
            java.lang.String r7 = r7.o     // Catch:{ all -> 0x001b }
            r4[r5] = r7     // Catch:{ all -> 0x001b }
            r5 = 1
            java.lang.Integer r7 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x001b }
            r4[r5] = r7     // Catch:{ all -> 0x001b }
            r2.<init>(r8, r3, r4, r0)     // Catch:{ all -> 0x001b }
            r1.execute(r2)     // Catch:{ all -> 0x001b }
            monitor-exit(r6)     // Catch:{ all -> 0x001b }
            goto L_0x000d
        L_0x008d:
            monitor-exit(r6)     // Catch:{ all -> 0x001b }
            boolean r1 = r14.b()
            if (r1 == 0) goto L_0x00a0
            com.e.a.a.c.a r1 = com.e.a.a.c.a.PROTOCOL_ERROR
            r0.b(r1)
            com.e.a.a.c.ac r0 = r8.c
            r0.b(r11)
            goto L_0x000d
        L_0x00a0:
            r0.a(r13, r14)
            if (r10 == 0) goto L_0x000d
            r0.h()
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.e.a.a.c.al.a(boolean, boolean, int, int, java.util.List, com.e.a.a.c.f):void");
    }

    public void b() {
    }
}
