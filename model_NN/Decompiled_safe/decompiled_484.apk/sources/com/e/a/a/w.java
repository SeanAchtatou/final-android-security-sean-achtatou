package com.e.a.a;

import java.util.concurrent.ThreadFactory;

final class w implements ThreadFactory {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f696a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ boolean f697b;

    w(String str, boolean z) {
        this.f696a = str;
        this.f697b = z;
    }

    public Thread newThread(Runnable runnable) {
        Thread thread = new Thread(runnable, this.f696a);
        thread.setDaemon(this.f697b);
        return thread;
    }
}
