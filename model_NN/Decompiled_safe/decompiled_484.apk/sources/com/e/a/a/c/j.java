package com.e.a.a.c;

import a.h;
import a.i;
import java.io.IOException;
import java.util.logging.Logger;

public final class j implements at {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final Logger f641a = Logger.getLogger(l.class.getName());
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final a.j f642b = a.j.a("PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n");

    /* access modifiers changed from: private */
    public static int b(int i, byte b2, short s) {
        if ((b2 & 8) != 0) {
            i--;
        }
        if (s <= i) {
            return (short) (i - s);
        }
        throw d("PROTOCOL_ERROR padding %s > remaining length %s", Short.valueOf(s), Integer.valueOf(i));
    }

    /* access modifiers changed from: private */
    public static int b(i iVar) {
        return ((iVar.i() & 255) << 16) | ((iVar.i() & 255) << 8) | (iVar.i() & 255);
    }

    /* access modifiers changed from: private */
    public static void b(h hVar, int i) {
        hVar.i((i >>> 16) & 255);
        hVar.i((i >>> 8) & 255);
        hVar.i(i & 255);
    }

    /* access modifiers changed from: private */
    public static IllegalArgumentException c(String str, Object... objArr) {
        throw new IllegalArgumentException(String.format(str, objArr));
    }

    /* access modifiers changed from: private */
    public static IOException d(String str, Object... objArr) {
        throw new IOException(String.format(str, objArr));
    }

    public b a(i iVar, boolean z) {
        return new m(iVar, 4096, z);
    }

    public d a(h hVar, boolean z) {
        return new n(hVar, z);
    }
}
