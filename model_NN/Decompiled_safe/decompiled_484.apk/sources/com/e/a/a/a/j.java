package com.e.a.a.a;

import a.aa;
import a.ac;
import a.f;
import a.n;

final class j implements aa {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f583a;

    /* renamed from: b  reason: collision with root package name */
    private final n f584b;
    private boolean c;

    private j(g gVar) {
        this.f583a = gVar;
        this.f584b = new n(this.f583a.e.a());
    }

    public ac a() {
        return this.f584b;
    }

    public void a_(f fVar, long j) {
        if (this.c) {
            throw new IllegalStateException("closed");
        } else if (j != 0) {
            this.f583a.e.j(j);
            this.f583a.e.b("\r\n");
            this.f583a.e.a_(fVar, j);
            this.f583a.e.b("\r\n");
        }
    }

    public synchronized void close() {
        if (!this.c) {
            this.c = true;
            this.f583a.e.b("0\r\n\r\n");
            this.f583a.a(this.f584b);
            int unused = this.f583a.f = 3;
        }
    }

    public synchronized void flush() {
        if (!this.c) {
            this.f583a.e.flush();
        }
    }
}
