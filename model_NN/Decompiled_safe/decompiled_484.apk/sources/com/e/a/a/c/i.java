package com.e.a.a.c;

import a.f;
import a.j;
import android.support.v4.media.TransportMediator;
import java.util.List;

final class i {

    /* renamed from: a  reason: collision with root package name */
    private final f f640a;

    i(f fVar) {
        this.f640a = fVar;
    }

    /* access modifiers changed from: package-private */
    public void a(int i, int i2, int i3) {
        if (i < i2) {
            this.f640a.i(i3 | i);
            return;
        }
        this.f640a.i(i3 | i2);
        int i4 = i - i2;
        while (i4 >= 128) {
            this.f640a.i((i4 & TransportMediator.KEYCODE_MEDIA_PAUSE) | 128);
            i4 >>>= 7;
        }
        this.f640a.i(i4);
    }

    /* access modifiers changed from: package-private */
    public void a(j jVar) {
        a(jVar.f(), TransportMediator.KEYCODE_MEDIA_PAUSE, 0);
        this.f640a.b(jVar);
    }

    /* access modifiers changed from: package-private */
    public void a(List<e> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            j e = list.get(i).h.e();
            Integer num = (Integer) g.f637b.get(e);
            if (num != null) {
                a(num.intValue() + 1, 15, 0);
                a(list.get(i).i);
            } else {
                this.f640a.i(0);
                a(e);
                a(list.get(i).i);
            }
        }
    }
}
