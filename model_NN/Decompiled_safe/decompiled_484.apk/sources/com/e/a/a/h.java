package com.e.a.a;

import a.ab;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

final class h {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f675a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final String f676b;
    /* access modifiers changed from: private */
    public final long[] c;
    /* access modifiers changed from: private */
    public final File[] d;
    /* access modifiers changed from: private */
    public final File[] e;
    /* access modifiers changed from: private */
    public boolean f;
    /* access modifiers changed from: private */
    public f g;
    /* access modifiers changed from: private */
    public long h;

    private h(b bVar, String str) {
        this.f675a = bVar;
        this.f676b = str;
        this.c = new long[bVar.j];
        this.d = new File[bVar.j];
        this.e = new File[bVar.j];
        StringBuilder append = new StringBuilder(str).append('.');
        int length = append.length();
        for (int i = 0; i < bVar.j; i++) {
            append.append(i);
            this.d[i] = new File(bVar.d, append.toString());
            append.append(".tmp");
            this.e[i] = new File(bVar.d, append.toString());
            append.setLength(length);
        }
    }

    /* synthetic */ h(b bVar, String str, c cVar) {
        this(bVar, str);
    }

    /* access modifiers changed from: private */
    public void a(String[] strArr) {
        if (strArr.length != this.f675a.j) {
            throw b(strArr);
        }
        int i = 0;
        while (i < strArr.length) {
            try {
                this.c[i] = Long.parseLong(strArr[i]);
                i++;
            } catch (NumberFormatException e2) {
                throw b(strArr);
            }
        }
    }

    private IOException b(String[] strArr) {
        throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
    }

    /* access modifiers changed from: package-private */
    public i a() {
        int i = 0;
        if (!Thread.holdsLock(this.f675a)) {
            throw new AssertionError();
        }
        ab[] abVarArr = new ab[this.f675a.j];
        long[] jArr = (long[]) this.c.clone();
        int i2 = 0;
        while (i2 < this.f675a.j) {
            try {
                abVarArr[i2] = this.f675a.c.a(this.d[i2]);
                i2++;
            } catch (FileNotFoundException e2) {
                while (i < this.f675a.j && abVarArr[i] != null) {
                    v.a(abVarArr[i]);
                    i++;
                }
                return null;
            }
        }
        return new i(this.f675a, this.f676b, this.h, abVarArr, jArr, null);
    }

    /* access modifiers changed from: package-private */
    public void a(a.h hVar) {
        for (long k : this.c) {
            hVar.i(32).k(k);
        }
    }
}
