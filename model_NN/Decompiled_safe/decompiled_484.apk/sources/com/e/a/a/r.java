package com.e.a.a;

import com.e.a.ao;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.List;
import javax.net.ssl.SSLSocket;

class r extends q {

    /* renamed from: a  reason: collision with root package name */
    private final p<Socket> f687a;

    /* renamed from: b  reason: collision with root package name */
    private final p<Socket> f688b;
    private final Method c;
    private final Method d;
    private final p<Socket> e;
    private final p<Socket> f;

    public r(p<Socket> pVar, p<Socket> pVar2, Method method, Method method2, p<Socket> pVar3, p<Socket> pVar4) {
        this.f687a = pVar;
        this.f688b = pVar2;
        this.c = method;
        this.d = method2;
        this.e = pVar3;
        this.f = pVar4;
    }

    public void a(Socket socket) {
        if (this.c != null) {
            try {
                this.c.invoke(null, socket);
            } catch (IllegalAccessException e2) {
                throw new RuntimeException(e2);
            } catch (InvocationTargetException e3) {
                throw new RuntimeException(e3.getCause());
            }
        }
    }

    public void a(Socket socket, InetSocketAddress inetSocketAddress, int i) {
        try {
            socket.connect(inetSocketAddress, i);
        } catch (SecurityException e2) {
            IOException iOException = new IOException("Exception in connect");
            iOException.initCause(e2);
            throw iOException;
        }
    }

    public void a(SSLSocket sSLSocket, String str, List<ao> list) {
        if (str != null) {
            this.f687a.b(sSLSocket, true);
            this.f688b.b(sSLSocket, str);
        }
        if (this.f != null && this.f.a(sSLSocket)) {
            this.f.d(sSLSocket, a(list));
        }
    }

    public String b(SSLSocket sSLSocket) {
        if (this.e == null || !this.e.a(sSLSocket)) {
            return null;
        }
        byte[] bArr = (byte[]) this.e.d(sSLSocket, new Object[0]);
        return bArr != null ? new String(bArr, v.c) : null;
    }

    public void b(Socket socket) {
        if (this.d != null) {
            try {
                this.d.invoke(null, socket);
            } catch (IllegalAccessException e2) {
                throw new RuntimeException(e2);
            } catch (InvocationTargetException e3) {
                throw new RuntimeException(e3.getCause());
            }
        }
    }
}
