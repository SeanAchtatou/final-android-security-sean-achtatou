package com.e.a.a.a;

import a.ab;
import android.support.v7.internal.widget.ActivityChooserView;
import com.e.a.a.q;
import com.e.a.a.v;
import com.e.a.ap;
import com.e.a.ar;
import com.e.a.au;
import com.e.a.ay;
import com.e.a.t;
import com.e.a.u;
import java.io.IOException;
import java.net.Proxy;
import java.net.Socket;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class ae {

    /* renamed from: a  reason: collision with root package name */
    private final t f567a;

    /* renamed from: b  reason: collision with root package name */
    private final u f568b;

    public ae(t tVar, u uVar) {
        this.f567a = tVar;
        this.f568b = uVar;
    }

    private ap a(ap apVar) {
        String host = apVar.a().getHost();
        int a2 = v.a(apVar.a());
        ar a3 = new ar().a(new URL("https", host, a2, "/")).a("Host", a2 == v.a("https") ? host : host + ":" + a2).a("Proxy-Connection", "Keep-Alive");
        String a4 = apVar.a("User-Agent");
        if (a4 != null) {
            a3.a("User-Agent", a4);
        }
        String a5 = apVar.a("Proxy-Authorization");
        if (a5 != null) {
            a3.a("Proxy-Authorization", a5);
        }
        return a3.a();
    }

    private void a(int i, int i2, ap apVar, ay ayVar, Socket socket) {
        try {
            ap a2 = a(apVar);
            g gVar = new g(this.f568b, this.f567a, socket);
            gVar.a(i, i2);
            URL a3 = a2.a();
            String str = "CONNECT " + a3.getHost() + ":" + v.a(a3) + " HTTP/1.1";
            do {
                gVar.a(a2.e(), str);
                gVar.d();
                au a4 = gVar.g().a(a2).a();
                long a5 = w.a(a4);
                if (a5 == -1) {
                    a5 = 0;
                }
                ab b2 = gVar.b(a5);
                v.b(b2, ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED, TimeUnit.MILLISECONDS);
                b2.close();
                switch (a4.c()) {
                    case 200:
                        if (gVar.e() > 0) {
                            throw new IOException("TLS tunnel buffered too many bytes!");
                        }
                        return;
                    case 407:
                        a2 = w.a(ayVar.a().f(), a4, ayVar.b());
                        break;
                    default:
                        throw new IOException("Unexpected response code for CONNECT: " + a4.c());
                }
            } while (a2 != null);
            throw new IOException("Failed to authenticate with proxy");
        } catch (IOException e) {
            throw new ac(e);
        }
    }

    private Socket b(int i, int i2, ay ayVar) {
        q a2 = q.a();
        try {
            Proxy b2 = ayVar.b();
            Socket createSocket = (b2.type() == Proxy.Type.DIRECT || b2.type() == Proxy.Type.HTTP) ? ayVar.a().c().createSocket() : new Socket(b2);
            createSocket.setSoTimeout(i);
            a2.a(createSocket, ayVar.c(), i2);
            return createSocket;
        } catch (IOException e) {
            throw new ac(e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException}
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      ClspMth{javax.net.SocketFactory.createSocket(java.lang.String, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException, java.net.UnknownHostException}
      ClspMth{javax.net.SocketFactory.createSocket(java.net.InetAddress, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException}
      ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException} */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00f3  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x011a  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x011f A[LOOP:0: B:1:0x000d->B:43:0x011f, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00fa A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.e.a.a.a.af a(int r12, int r13, int r14, com.e.a.ap r15, com.e.a.ay r16, java.util.List<com.e.a.w> r17, boolean r18) {
        /*
            r11 = this;
            com.e.a.a r8 = r16.a()
            com.e.a.a.a r9 = new com.e.a.a.a
            r0 = r17
            r9.<init>(r0)
            r1 = 0
            r7 = r1
        L_0x000d:
            r0 = r16
            java.net.Socket r6 = r11.b(r13, r12, r0)
            boolean r1 = r16.d()
            if (r1 == 0) goto L_0x0022
            r1 = r11
            r2 = r13
            r3 = r14
            r4 = r15
            r5 = r16
            r1.a(r2, r3, r4, r5, r6)
        L_0x0022:
            r2 = 0
            javax.net.ssl.SSLSocketFactory r1 = r8.d()     // Catch:{ IOException -> 0x0122 }
            java.lang.String r3 = r8.a()     // Catch:{ IOException -> 0x0122 }
            int r4 = r8.b()     // Catch:{ IOException -> 0x0122 }
            r5 = 1
            java.net.Socket r1 = r1.createSocket(r6, r3, r4, r5)     // Catch:{ IOException -> 0x0122 }
            javax.net.ssl.SSLSocket r1 = (javax.net.ssl.SSLSocket) r1     // Catch:{ IOException -> 0x0122 }
            com.e.a.w r3 = r9.a(r1)     // Catch:{ IOException -> 0x00de }
            com.e.a.a.q r4 = com.e.a.a.q.a()     // Catch:{ IOException -> 0x00de }
            r2 = 0
            boolean r5 = r3.c()     // Catch:{ all -> 0x00fb }
            if (r5 == 0) goto L_0x0050
            java.lang.String r5 = r8.a()     // Catch:{ all -> 0x00fb }
            java.util.List r10 = r8.g()     // Catch:{ all -> 0x00fb }
            r4.a(r1, r5, r10)     // Catch:{ all -> 0x00fb }
        L_0x0050:
            r1.startHandshake()     // Catch:{ all -> 0x00fb }
            javax.net.ssl.SSLSession r5 = r1.getSession()     // Catch:{ all -> 0x00fb }
            com.e.a.ac r5 = com.e.a.ac.a(r5)     // Catch:{ all -> 0x00fb }
            boolean r3 = r3.c()     // Catch:{ all -> 0x00fb }
            if (r3 == 0) goto L_0x006b
            java.lang.String r3 = r4.b(r1)     // Catch:{ all -> 0x00fb }
            if (r3 == 0) goto L_0x006b
            com.e.a.ao r2 = com.e.a.ao.a(r3)     // Catch:{ all -> 0x00fb }
        L_0x006b:
            r4.a(r1)     // Catch:{ IOException -> 0x00de }
            javax.net.ssl.HostnameVerifier r3 = r8.e()     // Catch:{ IOException -> 0x00de }
            java.lang.String r4 = r8.a()     // Catch:{ IOException -> 0x00de }
            javax.net.ssl.SSLSession r10 = r1.getSession()     // Catch:{ IOException -> 0x00de }
            boolean r3 = r3.verify(r4, r10)     // Catch:{ IOException -> 0x00de }
            if (r3 != 0) goto L_0x0100
            javax.net.ssl.SSLSession r2 = r1.getSession()     // Catch:{ IOException -> 0x00de }
            java.security.cert.Certificate[] r2 = r2.getPeerCertificates()     // Catch:{ IOException -> 0x00de }
            r3 = 0
            r2 = r2[r3]     // Catch:{ IOException -> 0x00de }
            java.security.cert.X509Certificate r2 = (java.security.cert.X509Certificate) r2     // Catch:{ IOException -> 0x00de }
            javax.net.ssl.SSLPeerUnverifiedException r3 = new javax.net.ssl.SSLPeerUnverifiedException     // Catch:{ IOException -> 0x00de }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00de }
            r4.<init>()     // Catch:{ IOException -> 0x00de }
            java.lang.String r5 = "Hostname "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x00de }
            java.lang.String r5 = r8.a()     // Catch:{ IOException -> 0x00de }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x00de }
            java.lang.String r5 = " not verified:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x00de }
            java.lang.String r5 = "\n    certificate: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x00de }
            java.lang.String r5 = com.e.a.o.a(r2)     // Catch:{ IOException -> 0x00de }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x00de }
            java.lang.String r5 = "\n    DN: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x00de }
            java.security.Principal r5 = r2.getSubjectDN()     // Catch:{ IOException -> 0x00de }
            java.lang.String r5 = r5.getName()     // Catch:{ IOException -> 0x00de }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x00de }
            java.lang.String r5 = "\n    subjectAltNames: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x00de }
            java.util.List r2 = com.e.a.a.d.b.a(r2)     // Catch:{ IOException -> 0x00de }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ IOException -> 0x00de }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x00de }
            r3.<init>(r2)     // Catch:{ IOException -> 0x00de }
            throw r3     // Catch:{ IOException -> 0x00de }
        L_0x00de:
            r2 = move-exception
            r3 = r2
            r4 = r1
        L_0x00e1:
            if (r18 == 0) goto L_0x0117
            boolean r1 = r9.a(r3)
            if (r1 == 0) goto L_0x0117
            r1 = 1
            r2 = r1
        L_0x00eb:
            com.e.a.a.v.a(r4)
            com.e.a.a.v.a(r6)
            if (r7 != 0) goto L_0x011a
            com.e.a.a.a.ac r1 = new com.e.a.a.a.ac
            r1.<init>(r3)
        L_0x00f8:
            if (r2 != 0) goto L_0x011f
            throw r1
        L_0x00fb:
            r2 = move-exception
            r4.a(r1)     // Catch:{ IOException -> 0x00de }
            throw r2     // Catch:{ IOException -> 0x00de }
        L_0x0100:
            com.e.a.o r3 = r8.k()     // Catch:{ IOException -> 0x00de }
            java.lang.String r4 = r8.a()     // Catch:{ IOException -> 0x00de }
            java.util.List r10 = r5.b()     // Catch:{ IOException -> 0x00de }
            r3.a(r4, r10)     // Catch:{ IOException -> 0x00de }
            com.e.a.a.a.af r3 = new com.e.a.a.a.af     // Catch:{ IOException -> 0x00de }
            r0 = r16
            r3.<init>(r0, r1, r2, r5)     // Catch:{ IOException -> 0x00de }
            return r3
        L_0x0117:
            r1 = 0
            r2 = r1
            goto L_0x00eb
        L_0x011a:
            r7.a(r3)
            r1 = r7
            goto L_0x00f8
        L_0x011f:
            r7 = r1
            goto L_0x000d
        L_0x0122:
            r1 = move-exception
            r3 = r1
            r4 = r2
            goto L_0x00e1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.e.a.a.a.ae.a(int, int, int, com.e.a.ap, com.e.a.ay, java.util.List, boolean):com.e.a.a.a.af");
    }

    public af a(int i, int i2, ay ayVar) {
        return new af(ayVar, b(i2, i, ayVar));
    }
}
