package com.e.a.a;

import java.io.IOException;

class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f604a;

    c(b bVar) {
        this.f604a = bVar;
    }

    public void run() {
        boolean z = false;
        synchronized (this.f604a) {
            if (!this.f604a.p) {
                z = true;
            }
            if (!z && !this.f604a.q) {
                try {
                    this.f604a.k();
                    if (this.f604a.i()) {
                        this.f604a.h();
                        int unused = this.f604a.n = 0;
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
