package com.e.a.a.a;

import a.ab;
import a.ac;
import a.f;
import a.h;
import a.i;
import com.e.a.a.v;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

class s implements ab {

    /* renamed from: a  reason: collision with root package name */
    boolean f591a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ i f592b;
    final /* synthetic */ b c;
    final /* synthetic */ h d;
    final /* synthetic */ q e;

    s(q qVar, i iVar, b bVar, h hVar) {
        this.e = qVar;
        this.f592b = iVar;
        this.c = bVar;
        this.d = hVar;
    }

    public long a(f fVar, long j) {
        try {
            long a2 = this.f592b.a(fVar, j);
            if (a2 == -1) {
                if (!this.f591a) {
                    this.f591a = true;
                    this.d.close();
                }
                return -1;
            }
            fVar.a(this.d.c(), fVar.b() - a2, a2);
            this.d.w();
            return a2;
        } catch (IOException e2) {
            if (!this.f591a) {
                this.f591a = true;
                this.c.a();
            }
            throw e2;
        }
    }

    public ac a() {
        return this.f592b.a();
    }

    public void close() {
        if (!this.f591a && !v.a(this, 100, TimeUnit.MILLISECONDS)) {
            this.f591a = true;
            this.c.a();
        }
        this.f592b.close();
    }
}
