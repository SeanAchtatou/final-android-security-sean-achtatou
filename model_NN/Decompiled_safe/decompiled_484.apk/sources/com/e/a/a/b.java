package com.e.a.a;

import a.aa;
import a.h;
import a.i;
import a.q;
import com.e.a.a.b.a;
import com.mediav.ads.sdk.adcore.Config;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public final class b implements Closeable {

    /* renamed from: a  reason: collision with root package name */
    static final Pattern f601a = Pattern.compile("[a-z0-9_-]{1,120}");

    /* renamed from: b  reason: collision with root package name */
    static final /* synthetic */ boolean f602b = (!b.class.desiredAssertionStatus());
    /* access modifiers changed from: private */
    public static final aa u = new e();
    /* access modifiers changed from: private */
    public final a c;
    /* access modifiers changed from: private */
    public final File d;
    private final File e;
    private final File f;
    private final File g;
    private final int h;
    private long i;
    /* access modifiers changed from: private */
    public final int j;
    private long k = 0;
    private h l;
    private final LinkedHashMap<String, h> m = new LinkedHashMap<>(0, 0.75f, true);
    /* access modifiers changed from: private */
    public int n;
    /* access modifiers changed from: private */
    public boolean o;
    /* access modifiers changed from: private */
    public boolean p;
    /* access modifiers changed from: private */
    public boolean q;
    private long r = 0;
    private final Executor s;
    private final Runnable t = new c(this);

    b(a aVar, File file, int i2, int i3, long j2, Executor executor) {
        this.c = aVar;
        this.d = file;
        this.h = i2;
        this.e = new File(file, "journal");
        this.f = new File(file, "journal.tmp");
        this.g = new File(file, "journal.bkp");
        this.j = i3;
        this.i = j2;
        this.s = executor;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.v.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
     arg types: [java.lang.String, int]
     candidates:
      com.e.a.a.v.a(java.lang.String, int):int
      com.e.a.a.v.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
      com.e.a.a.v.a(java.io.Closeable, java.io.Closeable):void
      com.e.a.a.v.a(java.lang.Object, java.lang.Object):boolean
      com.e.a.a.v.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory */
    public static b a(a aVar, File file, int i2, int i3, long j2) {
        if (j2 <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i3 <= 0) {
            throw new IllegalArgumentException("valueCount <= 0");
        } else {
            return new b(aVar, file, i2, i3, j2, new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), v.a("OkHttp DiskLruCache", true)));
        }
    }

    /* access modifiers changed from: private */
    public synchronized f a(String str, long j2) {
        h hVar;
        f fVar;
        a();
        j();
        e(str);
        h hVar2 = this.m.get(str);
        if (j2 == -1 || (hVar2 != null && hVar2.h == j2)) {
            if (hVar2 != null) {
                if (hVar2.g != null) {
                    fVar = null;
                }
            }
            this.l.b("DIRTY").i(32).b(str).i(10);
            this.l.flush();
            if (this.o) {
                fVar = null;
            } else {
                if (hVar2 == null) {
                    h hVar3 = new h(this, str, null);
                    this.m.put(str, hVar3);
                    hVar = hVar3;
                } else {
                    hVar = hVar2;
                }
                fVar = new f(this, hVar, null);
                f unused = hVar.g = fVar;
            }
        } else {
            fVar = null;
        }
        return fVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.h.a(com.e.a.a.h, boolean):boolean
     arg types: [com.e.a.a.h, int]
     candidates:
      com.e.a.a.h.a(com.e.a.a.h, long):long
      com.e.a.a.h.a(com.e.a.a.h, com.e.a.a.f):com.e.a.a.f
      com.e.a.a.h.a(com.e.a.a.h, java.lang.String[]):void
      com.e.a.a.h.a(com.e.a.a.h, boolean):boolean */
    /* access modifiers changed from: private */
    public synchronized void a(f fVar, boolean z) {
        synchronized (this) {
            h a2 = fVar.f673b;
            if (a2.g != fVar) {
                throw new IllegalStateException();
            }
            if (z) {
                if (!a2.f) {
                    int i2 = 0;
                    while (true) {
                        if (i2 < this.j) {
                            if (!fVar.c[i2]) {
                                fVar.b();
                                throw new IllegalStateException("Newly created entry didn't create value for index " + i2);
                            } else if (!this.c.e(a2.e[i2])) {
                                fVar.b();
                                break;
                            } else {
                                i2++;
                            }
                        }
                    }
                }
            }
            for (int i3 = 0; i3 < this.j; i3++) {
                File file = a2.e[i3];
                if (!z) {
                    this.c.d(file);
                } else if (this.c.e(file)) {
                    File file2 = a2.d[i3];
                    this.c.a(file, file2);
                    long j2 = a2.c[i3];
                    long f2 = this.c.f(file2);
                    a2.c[i3] = f2;
                    this.k = (this.k - j2) + f2;
                }
            }
            this.n++;
            f unused = a2.g = (f) null;
            if (a2.f || z) {
                boolean unused2 = a2.f = true;
                this.l.b("CLEAN").i(32);
                this.l.b(a2.f676b);
                a2.a(this.l);
                this.l.i(10);
                if (z) {
                    long j3 = this.r;
                    this.r = 1 + j3;
                    long unused3 = a2.h = j3;
                }
            } else {
                this.m.remove(a2.f676b);
                this.l.b("REMOVE").i(32);
                this.l.b(a2.f676b);
                this.l.i(10);
            }
            this.l.flush();
            if (this.k > this.i || i()) {
                this.s.execute(this.t);
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean a(h hVar) {
        if (hVar.g != null) {
            boolean unused = hVar.g.d = true;
        }
        for (int i2 = 0; i2 < this.j; i2++) {
            this.c.d(hVar.d[i2]);
            this.k -= hVar.c[i2];
            hVar.c[i2] = 0;
        }
        this.n++;
        this.l.b("REMOVE").i(32).b(hVar.f676b).i(10);
        this.m.remove(hVar.f676b);
        if (i()) {
            this.s.execute(this.t);
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.h.a(com.e.a.a.h, boolean):boolean
     arg types: [com.e.a.a.h, int]
     candidates:
      com.e.a.a.h.a(com.e.a.a.h, long):long
      com.e.a.a.h.a(com.e.a.a.h, com.e.a.a.f):com.e.a.a.f
      com.e.a.a.h.a(com.e.a.a.h, java.lang.String[]):void
      com.e.a.a.h.a(com.e.a.a.h, boolean):boolean */
    private void d(String str) {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf == -1) {
            throw new IOException("unexpected journal line: " + str);
        }
        int i2 = indexOf + 1;
        int indexOf2 = str.indexOf(32, i2);
        if (indexOf2 == -1) {
            String substring = str.substring(i2);
            if (indexOf != "REMOVE".length() || !str.startsWith("REMOVE")) {
                str2 = substring;
            } else {
                this.m.remove(substring);
                return;
            }
        } else {
            str2 = str.substring(i2, indexOf2);
        }
        h hVar = this.m.get(str2);
        if (hVar == null) {
            hVar = new h(this, str2, null);
            this.m.put(str2, hVar);
        }
        if (indexOf2 != -1 && indexOf == "CLEAN".length() && str.startsWith("CLEAN")) {
            String[] split = str.substring(indexOf2 + 1).split(" ");
            boolean unused = hVar.f = true;
            f unused2 = hVar.g = (f) null;
            hVar.a(split);
        } else if (indexOf2 == -1 && indexOf == "DIRTY".length() && str.startsWith("DIRTY")) {
            f unused3 = hVar.g = new f(this, hVar, null);
        } else if (indexOf2 != -1 || indexOf != "READ".length() || !str.startsWith("READ")) {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    private void e() {
        int i2;
        i a2 = q.a(this.c.a(this.e));
        try {
            String r2 = a2.r();
            String r3 = a2.r();
            String r4 = a2.r();
            String r5 = a2.r();
            String r6 = a2.r();
            if (!"libcore.io.DiskLruCache".equals(r2) || !Config.CHANNEL_ID.equals(r3) || !Integer.toString(this.h).equals(r4) || !Integer.toString(this.j).equals(r5) || !"".equals(r6)) {
                throw new IOException("unexpected journal header: [" + r2 + ", " + r3 + ", " + r5 + ", " + r6 + "]");
            }
            i2 = 0;
            while (true) {
                d(a2.r());
                i2++;
            }
        } catch (EOFException e2) {
            this.n = i2 - this.m.size();
            if (!a2.f()) {
                h();
            } else {
                this.l = f();
            }
            v.a(a2);
        } catch (Throwable th) {
            v.a(a2);
            throw th;
        }
    }

    private void e(String str) {
        if (!f601a.matcher(str).matches()) {
            throw new IllegalArgumentException("keys must match regex [a-z0-9_-]{1,120}: \"" + str + "\"");
        }
    }

    private h f() {
        return q.a(new d(this, this.c.c(this.e)));
    }

    private void g() {
        this.c.d(this.f);
        Iterator<h> it = this.m.values().iterator();
        while (it.hasNext()) {
            h next = it.next();
            if (next.g == null) {
                for (int i2 = 0; i2 < this.j; i2++) {
                    this.k += next.c[i2];
                }
            } else {
                f unused = next.g = (f) null;
                for (int i3 = 0; i3 < this.j; i3++) {
                    this.c.d(next.d[i3]);
                    this.c.d(next.e[i3]);
                }
                it.remove();
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void h() {
        if (this.l != null) {
            this.l.close();
        }
        h a2 = q.a(this.c.b(this.f));
        try {
            a2.b("libcore.io.DiskLruCache").i(10);
            a2.b(Config.CHANNEL_ID).i(10);
            a2.k((long) this.h).i(10);
            a2.k((long) this.j).i(10);
            a2.i(10);
            for (h next : this.m.values()) {
                if (next.g != null) {
                    a2.b("DIRTY").i(32);
                    a2.b(next.f676b);
                    a2.i(10);
                } else {
                    a2.b("CLEAN").i(32);
                    a2.b(next.f676b);
                    next.a(a2);
                    a2.i(10);
                }
            }
            a2.close();
            if (this.c.e(this.e)) {
                this.c.a(this.e, this.g);
            }
            this.c.a(this.f, this.e);
            this.c.d(this.g);
            this.l = f();
            this.o = false;
        } catch (Throwable th) {
            a2.close();
            throw th;
        }
    }

    /* access modifiers changed from: private */
    public boolean i() {
        return this.n >= 2000 && this.n >= this.m.size();
    }

    private synchronized void j() {
        if (b()) {
            throw new IllegalStateException("cache is closed");
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        while (this.k > this.i) {
            a(this.m.values().iterator().next());
        }
    }

    public synchronized i a(String str) {
        i iVar;
        a();
        j();
        e(str);
        h hVar = this.m.get(str);
        if (hVar == null || !hVar.f) {
            iVar = null;
        } else {
            iVar = hVar.a();
            if (iVar == null) {
                iVar = null;
            } else {
                this.n++;
                this.l.b("READ").i(32).b(str).i(10);
                if (i()) {
                    this.s.execute(this.t);
                }
            }
        }
        return iVar;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (!f602b && !Thread.holdsLock(this)) {
            throw new AssertionError();
        } else if (!this.p) {
            if (this.c.e(this.g)) {
                if (this.c.e(this.e)) {
                    this.c.d(this.g);
                } else {
                    this.c.a(this.g, this.e);
                }
            }
            if (this.c.e(this.e)) {
                try {
                    e();
                    g();
                    this.p = true;
                    return;
                } catch (IOException e2) {
                    q.a().a("DiskLruCache " + this.d + " is corrupt: " + e2.getMessage() + ", removing");
                    c();
                    this.q = false;
                }
            }
            h();
            this.p = true;
        }
    }

    public f b(String str) {
        return a(str, -1);
    }

    public synchronized boolean b() {
        return this.q;
    }

    public void c() {
        close();
        this.c.g(this.d);
    }

    public synchronized boolean c(String str) {
        h hVar;
        a();
        j();
        e(str);
        hVar = this.m.get(str);
        return hVar == null ? false : a(hVar);
    }

    public synchronized void close() {
        if (!this.p || this.q) {
            this.q = true;
        } else {
            for (h hVar : (h[]) this.m.values().toArray(new h[this.m.size()])) {
                if (hVar.g != null) {
                    hVar.g.b();
                }
            }
            k();
            this.l.close();
            this.l = null;
            this.q = true;
        }
    }
}
