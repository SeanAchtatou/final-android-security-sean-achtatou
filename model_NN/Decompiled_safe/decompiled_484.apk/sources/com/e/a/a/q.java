package com.e.a.a;

import a.f;
import com.e.a.ao;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.List;
import javax.net.ssl.SSLSocket;

public class q {

    /* renamed from: a  reason: collision with root package name */
    private static final q f686a = c();

    public static q a() {
        return f686a;
    }

    static byte[] a(List<ao> list) {
        f fVar = new f();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            ao aoVar = list.get(i);
            if (aoVar != ao.HTTP_1_0) {
                fVar.i(aoVar.toString().length());
                fVar.b(aoVar.toString());
            }
        }
        return fVar.s();
    }

    private static q c() {
        p pVar;
        Method method;
        Method method2;
        p pVar2;
        Method method3;
        p pVar3;
        Method method4;
        Method method5;
        p pVar4;
        p pVar5;
        p pVar6;
        p pVar7;
        try {
            Class.forName("com.android.org.conscrypt.OpenSSLSocketImpl");
        } catch (ClassNotFoundException e) {
            Class.forName("org.apache.harmony.xnet.provider.jsse.OpenSSLSocketImpl");
        }
        try {
            p pVar8 = new p(null, "setUseSessionTickets", Boolean.TYPE);
            p pVar9 = new p(null, "setHostname", String.class);
            try {
                Class<?> cls = Class.forName("android.net.TrafficStats");
                method2 = cls.getMethod("tagSocket", Socket.class);
                try {
                    method5 = cls.getMethod("untagSocket", Socket.class);
                    method = method5;
                    pVar2 = pVar6;
                    p pVar10 = pVar5;
                    method3 = method2;
                    pVar3 = pVar10;
                } catch (ClassNotFoundException e2) {
                    method4 = method2;
                    method = null;
                    method2 = method4;
                    pVar = null;
                    pVar2 = null;
                    method3 = method2;
                    pVar3 = pVar;
                    return new r(pVar8, pVar9, method3, method, pVar3, pVar2);
                } catch (NoSuchMethodException e3) {
                    pVar = null;
                    method = null;
                    pVar2 = null;
                    method3 = method2;
                    pVar3 = pVar;
                    return new r(pVar8, pVar9, method3, method, pVar3, pVar2);
                }
                try {
                    Class.forName("android.net.Network");
                    pVar7 = new p(byte[].class, "getAlpnSelectedProtocol", new Class[0]);
                } catch (ClassNotFoundException e4) {
                    pVar4 = null;
                    pVar5 = pVar4;
                    pVar6 = null;
                    method = method5;
                    pVar2 = pVar6;
                    p pVar102 = pVar5;
                    method3 = method2;
                    pVar3 = pVar102;
                    return new r(pVar8, pVar9, method3, method, pVar3, pVar2);
                } catch (NoSuchMethodException e5) {
                    pVar = null;
                    method = method5;
                    pVar2 = null;
                    method3 = method2;
                    pVar3 = pVar;
                    return new r(pVar8, pVar9, method3, method, pVar3, pVar2);
                }
                try {
                    pVar6 = new p(null, "setAlpnProtocols", byte[].class);
                    pVar5 = pVar7;
                } catch (ClassNotFoundException e6) {
                    pVar4 = pVar7;
                    pVar5 = pVar4;
                    pVar6 = null;
                    method = method5;
                    pVar2 = pVar6;
                    p pVar1022 = pVar5;
                    method3 = method2;
                    pVar3 = pVar1022;
                    return new r(pVar8, pVar9, method3, method, pVar3, pVar2);
                } catch (NoSuchMethodException e7) {
                    pVar = pVar7;
                    method = method5;
                    pVar2 = null;
                    method3 = method2;
                    pVar3 = pVar;
                    return new r(pVar8, pVar9, method3, method, pVar3, pVar2);
                }
            } catch (ClassNotFoundException e8) {
                method4 = null;
                method = null;
                method2 = method4;
                pVar = null;
                pVar2 = null;
                method3 = method2;
                pVar3 = pVar;
                return new r(pVar8, pVar9, method3, method, pVar3, pVar2);
            } catch (NoSuchMethodException e9) {
                pVar = null;
                method = null;
                method2 = null;
                pVar2 = null;
                method3 = method2;
                pVar3 = pVar;
                return new r(pVar8, pVar9, method3, method, pVar3, pVar2);
            }
            return new r(pVar8, pVar9, method3, method, pVar3, pVar2);
        } catch (ClassNotFoundException e10) {
            try {
                Class<?> cls2 = Class.forName("org.eclipse.jetty.alpn.ALPN");
                Class<?> cls3 = Class.forName("org.eclipse.jetty.alpn.ALPN" + "$Provider");
                return new s(cls2.getMethod("put", SSLSocket.class, cls3), cls2.getMethod("get", SSLSocket.class), cls2.getMethod("remove", SSLSocket.class), Class.forName("org.eclipse.jetty.alpn.ALPN" + "$ClientProvider"), Class.forName("org.eclipse.jetty.alpn.ALPN" + "$ServerProvider"));
            } catch (ClassNotFoundException | NoSuchMethodException e11) {
                return new q();
            }
        }
    }

    public void a(String str) {
        System.out.println(str);
    }

    public void a(Socket socket) {
    }

    public void a(Socket socket, InetSocketAddress inetSocketAddress, int i) {
        socket.connect(inetSocketAddress, i);
    }

    public void a(SSLSocket sSLSocket) {
    }

    public void a(SSLSocket sSLSocket, String str, List<ao> list) {
    }

    public String b() {
        return "OkHttp";
    }

    public String b(SSLSocket sSLSocket) {
        return null;
    }

    public void b(Socket socket) {
    }
}
