package com.e.a.a.c;

import a.i;
import a.j;
import android.support.v4.view.ViewCompat;
import android.support.v7.internal.widget.ActivityChooserView;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.List;

final class aa implements b {

    /* renamed from: a  reason: collision with root package name */
    private final i f607a;

    /* renamed from: b  reason: collision with root package name */
    private final boolean f608b;
    private final s c = new s(this.f607a);

    aa(i iVar, boolean z) {
        this.f607a = iVar;
        this.f608b = z;
    }

    private static IOException a(String str, Object... objArr) {
        throw new IOException(String.format(str, objArr));
    }

    private void a(c cVar, int i, int i2) {
        boolean z = true;
        int k = this.f607a.k();
        int k2 = this.f607a.k();
        int i3 = k & ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        int i4 = k2 & ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        this.f607a.j();
        List<e> a2 = this.c.a(i2 - 10);
        boolean z2 = (i & 1) != 0;
        if ((i & 2) == 0) {
            z = false;
        }
        cVar.a(z, z2, i3, i4, a2, f.SPDY_SYN_STREAM);
    }

    private void b(c cVar, int i, int i2) {
        cVar.a(false, (i & 1) != 0, this.f607a.k() & ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED, -1, this.c.a(i2 - 4), f.SPDY_REPLY);
    }

    private void c(c cVar, int i, int i2) {
        if (i2 != 8) {
            throw a("TYPE_RST_STREAM length: %d != 8", Integer.valueOf(i2));
        }
        int k = this.f607a.k() & ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        int k2 = this.f607a.k();
        a a2 = a.a(k2);
        if (a2 == null) {
            throw a("TYPE_RST_STREAM unexpected error code: %d", Integer.valueOf(k2));
        } else {
            cVar.a(k, a2);
        }
    }

    private void d(c cVar, int i, int i2) {
        cVar.a(false, false, this.f607a.k() & ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED, -1, this.c.a(i2 - 4), f.SPDY_HEADERS);
    }

    private void e(c cVar, int i, int i2) {
        if (i2 != 8) {
            throw a("TYPE_WINDOW_UPDATE length: %d != 8", Integer.valueOf(i2));
        }
        int k = this.f607a.k();
        int k2 = this.f607a.k();
        int i3 = k & ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        long j = (long) (k2 & ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED);
        if (j == 0) {
            throw a("windowSizeIncrement was 0", Long.valueOf(j));
        } else {
            cVar.a(i3, j);
        }
    }

    private void f(c cVar, int i, int i2) {
        boolean z = true;
        if (i2 != 4) {
            throw a("TYPE_PING length: %d != 4", Integer.valueOf(i2));
        }
        int k = this.f607a.k();
        if (this.f608b != ((k & 1) == 1)) {
            z = false;
        }
        cVar.a(z, k, 0);
    }

    private void g(c cVar, int i, int i2) {
        if (i2 != 8) {
            throw a("TYPE_GOAWAY length: %d != 8", Integer.valueOf(i2));
        }
        int k = this.f607a.k() & ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        int k2 = this.f607a.k();
        a c2 = a.c(k2);
        if (c2 == null) {
            throw a("TYPE_GOAWAY unexpected error code: %d", Integer.valueOf(k2));
        } else {
            cVar.a(k, c2, j.f14b);
        }
    }

    private void h(c cVar, int i, int i2) {
        boolean z = true;
        int k = this.f607a.k();
        if (i2 != (k * 8) + 4) {
            throw a("TYPE_SETTINGS length: %d != 4 + 8 * %d", Integer.valueOf(i2), Integer.valueOf(k));
        }
        y yVar = new y();
        for (int i3 = 0; i3 < k; i3++) {
            int k2 = this.f607a.k();
            yVar.a(k2 & ViewCompat.MEASURED_SIZE_MASK, (-16777216 & k2) >>> 24, this.f607a.k());
        }
        if ((i & 1) == 0) {
            z = false;
        }
        cVar.a(z, yVar);
    }

    public void a() {
    }

    public boolean a(c cVar) {
        boolean z = false;
        try {
            int k = this.f607a.k();
            int k2 = this.f607a.k();
            boolean z2 = (Integer.MIN_VALUE & k) != 0;
            int i = (-16777216 & k2) >>> 24;
            int i2 = k2 & ViewCompat.MEASURED_SIZE_MASK;
            if (z2) {
                int i3 = (2147418112 & k) >>> 16;
                int i4 = 65535 & k;
                if (i3 != 3) {
                    throw new ProtocolException("version != 3: " + i3);
                }
                switch (i4) {
                    case 1:
                        a(cVar, i, i2);
                        return true;
                    case 2:
                        b(cVar, i, i2);
                        return true;
                    case 3:
                        c(cVar, i, i2);
                        return true;
                    case 4:
                        h(cVar, i, i2);
                        return true;
                    case 5:
                    default:
                        this.f607a.g((long) i2);
                        return true;
                    case 6:
                        f(cVar, i, i2);
                        return true;
                    case 7:
                        g(cVar, i, i2);
                        return true;
                    case 8:
                        d(cVar, i, i2);
                        return true;
                    case 9:
                        e(cVar, i, i2);
                        return true;
                }
            } else {
                int i5 = Integer.MAX_VALUE & k;
                if ((i & 1) != 0) {
                    z = true;
                }
                cVar.a(z, i5, this.f607a, i2);
                return true;
            }
        } catch (IOException e) {
            return false;
        }
    }

    public void close() {
        this.c.a();
    }
}
