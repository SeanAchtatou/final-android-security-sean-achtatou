package com.e.a.a.c;

import com.e.a.a.m;
import java.io.IOException;
import java.util.List;

class ah extends m {

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ int f617b;
    final /* synthetic */ List c;
    final /* synthetic */ boolean d;
    final /* synthetic */ ac e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ah(ac acVar, String str, Object[] objArr, int i, List list, boolean z) {
        super(str, objArr);
        this.e = acVar;
        this.f617b = i;
        this.c = list;
        this.d = z;
    }

    public void a() {
        boolean a2 = this.e.v.a(this.f617b, this.c, this.d);
        if (a2) {
            try {
                this.e.i.a(this.f617b, a.CANCEL);
            } catch (IOException e2) {
                return;
            }
        }
        if (a2 || this.d) {
            synchronized (this.e) {
                this.e.y.remove(Integer.valueOf(this.f617b));
            }
        }
    }
}
