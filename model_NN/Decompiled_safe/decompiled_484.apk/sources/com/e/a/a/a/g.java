package com.e.a.a.a;

import a.aa;
import a.ab;
import a.ac;
import a.h;
import a.i;
import a.n;
import a.q;
import com.e.a.a.k;
import com.e.a.ad;
import com.e.a.af;
import com.e.a.aw;
import com.e.a.t;
import com.e.a.u;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

public final class g {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final u f579a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final t f580b;
    private final Socket c;
    /* access modifiers changed from: private */
    public final i d;
    /* access modifiers changed from: private */
    public final h e;
    /* access modifiers changed from: private */
    public int f = 0;
    /* access modifiers changed from: private */
    public int g = 0;

    public g(u uVar, t tVar, Socket socket) {
        this.f579a = uVar;
        this.f580b = tVar;
        this.c = socket;
        this.d = q.a(q.b(socket));
        this.e = q.a(q.a(socket));
    }

    /* access modifiers changed from: private */
    public void a(n nVar) {
        ac a2 = nVar.a();
        nVar.a(ac.f1b);
        a2.f();
        a2.d_();
    }

    public aa a(long j) {
        if (this.f != 1) {
            throw new IllegalStateException("state: " + this.f);
        }
        this.f = 2;
        return new l(this, j);
    }

    public ab a(q qVar) {
        if (this.f != 4) {
            throw new IllegalStateException("state: " + this.f);
        }
        this.f = 5;
        return new k(this, qVar);
    }

    public void a() {
        this.g = 1;
        if (this.f == 0) {
            this.g = 0;
            k.f681b.a(this.f579a, this.f580b);
        }
    }

    public void a(int i, int i2) {
        if (i != 0) {
            this.d.a().a((long) i, TimeUnit.MILLISECONDS);
        }
        if (i2 != 0) {
            this.e.a().a((long) i2, TimeUnit.MILLISECONDS);
        }
    }

    public void a(ab abVar) {
        if (this.f != 1) {
            throw new IllegalStateException("state: " + this.f);
        }
        this.f = 3;
        abVar.a(this.e);
    }

    public void a(ad adVar, String str) {
        if (this.f != 0) {
            throw new IllegalStateException("state: " + this.f);
        }
        this.e.b(str).b("\r\n");
        int a2 = adVar.a();
        for (int i = 0; i < a2; i++) {
            this.e.b(adVar.a(i)).b(": ").b(adVar.b(i)).b("\r\n");
        }
        this.e.b("\r\n");
        this.f = 1;
    }

    public void a(af afVar) {
        while (true) {
            String r = this.d.r();
            if (r.length() != 0) {
                k.f681b.a(afVar, r);
            } else {
                return;
            }
        }
    }

    public ab b(long j) {
        if (this.f != 4) {
            throw new IllegalStateException("state: " + this.f);
        }
        this.f = 5;
        return new m(this, j);
    }

    public void b() {
        this.g = 2;
        if (this.f == 0) {
            this.f = 6;
            this.f580b.d().close();
        }
    }

    public boolean c() {
        return this.f == 6;
    }

    public void d() {
        this.e.flush();
    }

    public long e() {
        return this.d.c().b();
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean f() {
        /*
            r5 = this;
            r0 = 0
            r1 = 1
            java.net.Socket r2 = r5.c     // Catch:{ SocketTimeoutException -> 0x002a, IOException -> 0x002d }
            int r3 = r2.getSoTimeout()     // Catch:{ SocketTimeoutException -> 0x002a, IOException -> 0x002d }
            java.net.Socket r2 = r5.c     // Catch:{ all -> 0x0023 }
            r4 = 1
            r2.setSoTimeout(r4)     // Catch:{ all -> 0x0023 }
            a.i r2 = r5.d     // Catch:{ all -> 0x0023 }
            boolean r2 = r2.f()     // Catch:{ all -> 0x0023 }
            if (r2 == 0) goto L_0x001c
            java.net.Socket r2 = r5.c     // Catch:{ SocketTimeoutException -> 0x002a, IOException -> 0x002d }
            r2.setSoTimeout(r3)     // Catch:{ SocketTimeoutException -> 0x002a, IOException -> 0x002d }
        L_0x001b:
            return r0
        L_0x001c:
            java.net.Socket r2 = r5.c     // Catch:{ SocketTimeoutException -> 0x002a, IOException -> 0x002d }
            r2.setSoTimeout(r3)     // Catch:{ SocketTimeoutException -> 0x002a, IOException -> 0x002d }
            r0 = r1
            goto L_0x001b
        L_0x0023:
            r2 = move-exception
            java.net.Socket r4 = r5.c     // Catch:{ SocketTimeoutException -> 0x002a, IOException -> 0x002d }
            r4.setSoTimeout(r3)     // Catch:{ SocketTimeoutException -> 0x002a, IOException -> 0x002d }
            throw r2     // Catch:{ SocketTimeoutException -> 0x002a, IOException -> 0x002d }
        L_0x002a:
            r0 = move-exception
            r0 = r1
            goto L_0x001b
        L_0x002d:
            r1 = move-exception
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.e.a.a.a.g.f():boolean");
    }

    public aw g() {
        ah a2;
        aw a3;
        if (this.f == 1 || this.f == 3) {
            do {
                try {
                    a2 = ah.a(this.d.r());
                    a3 = new aw().a(a2.f573a).a(a2.f574b).a(a2.c);
                    af afVar = new af();
                    a(afVar);
                    afVar.a(w.d, a2.f573a.toString());
                    a3.a(afVar.a());
                } catch (EOFException e2) {
                    IOException iOException = new IOException("unexpected end of stream on " + this.f580b + " (recycle count=" + k.f681b.b(this.f580b) + ")");
                    iOException.initCause(e2);
                    throw iOException;
                }
            } while (a2.f574b == 100);
            this.f = 4;
            return a3;
        }
        throw new IllegalStateException("state: " + this.f);
    }

    public aa h() {
        if (this.f != 1) {
            throw new IllegalStateException("state: " + this.f);
        }
        this.f = 2;
        return new j(this);
    }

    public ab i() {
        if (this.f != 4) {
            throw new IllegalStateException("state: " + this.f);
        }
        this.f = 5;
        return new n(this);
    }
}
