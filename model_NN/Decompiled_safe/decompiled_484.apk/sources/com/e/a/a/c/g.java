package com.e.a.a.c;

import a.j;
import com.amap.api.location.LocationManagerProxy;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

final class g {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final e[] f636a = {new e(e.e, ""), new e(e.f633b, "GET"), new e(e.f633b, "POST"), new e(e.c, "/"), new e(e.c, "/index.html"), new e(e.d, "http"), new e(e.d, "https"), new e(e.f632a, "200"), new e(e.f632a, "204"), new e(e.f632a, "206"), new e(e.f632a, "304"), new e(e.f632a, "400"), new e(e.f632a, "404"), new e(e.f632a, "500"), new e("accept-charset", ""), new e("accept-encoding", "gzip, deflate"), new e("accept-language", ""), new e("accept-ranges", ""), new e("accept", ""), new e("access-control-allow-origin", ""), new e("age", ""), new e("allow", ""), new e("authorization", ""), new e("cache-control", ""), new e("content-disposition", ""), new e("content-encoding", ""), new e("content-language", ""), new e("content-length", ""), new e("content-location", ""), new e("content-range", ""), new e("content-type", ""), new e("cookie", ""), new e("date", ""), new e("etag", ""), new e("expect", ""), new e("expires", ""), new e("from", ""), new e("host", ""), new e("if-match", ""), new e("if-modified-since", ""), new e("if-none-match", ""), new e("if-range", ""), new e("if-unmodified-since", ""), new e("last-modified", ""), new e("link", ""), new e((String) LocationManagerProxy.KEY_LOCATION_CHANGED, ""), new e("max-forwards", ""), new e("proxy-authenticate", ""), new e("proxy-authorization", ""), new e("range", ""), new e("referer", ""), new e("refresh", ""), new e("retry-after", ""), new e("server", ""), new e("set-cookie", ""), new e("strict-transport-security", ""), new e("transfer-encoding", ""), new e("user-agent", ""), new e("vary", ""), new e("via", ""), new e("www-authenticate", "")};
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final Map<j, Integer> f637b = c();

    /* access modifiers changed from: private */
    public static j b(j jVar) {
        int i = 0;
        int f = jVar.f();
        while (i < f) {
            byte a2 = jVar.a(i);
            if (a2 < 65 || a2 > 90) {
                i++;
            } else {
                throw new IOException("PROTOCOL_ERROR response malformed: mixed case name: " + jVar.a());
            }
        }
        return jVar;
    }

    private static Map<j, Integer> c() {
        LinkedHashMap linkedHashMap = new LinkedHashMap(f636a.length);
        for (int i = 0; i < f636a.length; i++) {
            if (!linkedHashMap.containsKey(f636a[i].h)) {
                linkedHashMap.put(f636a[i].h, Integer.valueOf(i));
            }
        }
        return Collections.unmodifiableMap(linkedHashMap);
    }
}
