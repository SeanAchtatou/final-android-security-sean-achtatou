package com.e.a.a.a;

import com.e.a.ad;
import com.e.a.ap;
import com.e.a.ar;
import com.e.a.au;
import com.e.a.aw;
import com.e.a.j;
import com.qihoo.messenger.util.QDefine;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class e {

    /* renamed from: a  reason: collision with root package name */
    final long f577a;

    /* renamed from: b  reason: collision with root package name */
    final ap f578b;
    final au c;
    private Date d;
    private String e;
    private Date f;
    private String g;
    private Date h;
    private long i;
    private long j;
    private String k;
    private int l = -1;

    public e(long j2, ap apVar, au auVar) {
        this.f577a = j2;
        this.f578b = apVar;
        this.c = auVar;
        if (auVar != null) {
            ad g2 = auVar.g();
            int a2 = g2.a();
            for (int i2 = 0; i2 < a2; i2++) {
                String a3 = g2.a(i2);
                String b2 = g2.b(i2);
                if ("Date".equalsIgnoreCase(a3)) {
                    this.d = o.a(b2);
                    this.e = b2;
                } else if ("Expires".equalsIgnoreCase(a3)) {
                    this.h = o.a(b2);
                } else if ("Last-Modified".equalsIgnoreCase(a3)) {
                    this.f = o.a(b2);
                    this.g = b2;
                } else if ("ETag".equalsIgnoreCase(a3)) {
                    this.k = b2;
                } else if ("Age".equalsIgnoreCase(a3)) {
                    this.l = f.b(b2, -1);
                } else if (w.f598b.equalsIgnoreCase(a3)) {
                    this.i = Long.parseLong(b2);
                } else if (w.c.equalsIgnoreCase(a3)) {
                    this.j = Long.parseLong(b2);
                }
            }
        }
    }

    private static boolean a(ap apVar) {
        return (apVar.a("If-Modified-Since") == null && apVar.a("If-None-Match") == null) ? false : true;
    }

    private c b() {
        long j2 = 0;
        if (this.c == null) {
            return new c(this.f578b, null);
        }
        if (this.f578b.i() && this.c.f() == null) {
            return new c(this.f578b, null);
        }
        if (!c.a(this.c, this.f578b)) {
            return new c(this.f578b, null);
        }
        j h2 = this.f578b.h();
        if (h2.a() || a(this.f578b)) {
            return new c(this.f578b, null);
        }
        long d2 = d();
        long c2 = c();
        if (h2.c() != -1) {
            c2 = Math.min(c2, TimeUnit.SECONDS.toMillis((long) h2.c()));
        }
        long millis = h2.h() != -1 ? TimeUnit.SECONDS.toMillis((long) h2.h()) : 0;
        j m = this.c.m();
        if (!m.f() && h2.g() != -1) {
            j2 = TimeUnit.SECONDS.toMillis((long) h2.g());
        }
        if (m.a() || d2 + millis >= j2 + c2) {
            ar g2 = this.f578b.g();
            if (this.k != null) {
                g2.a("If-None-Match", this.k);
            } else if (this.f != null) {
                g2.a("If-Modified-Since", this.g);
            } else if (this.d != null) {
                g2.a("If-Modified-Since", this.e);
            }
            ap a2 = g2.a();
            return a(a2) ? new c(a2, this.c) : new c(a2, null);
        }
        aw i2 = this.c.i();
        if (millis + d2 >= c2) {
            i2.b("Warning", "110 HttpURLConnection \"Response is stale\"");
        }
        if (d2 > QDefine.ONE_DAY && e()) {
            i2.b("Warning", "113 HttpURLConnection \"Heuristic expiration\"");
        }
        return new c(null, i2.a());
    }

    private long c() {
        j m = this.c.m();
        if (m.c() != -1) {
            return TimeUnit.SECONDS.toMillis((long) m.c());
        }
        if (this.h != null) {
            long time = this.h.getTime() - (this.d != null ? this.d.getTime() : this.j);
            if (time <= 0) {
                time = 0;
            }
            return time;
        } else if (this.f == null || this.c.a().a().getQuery() != null) {
            return 0;
        } else {
            long time2 = (this.d != null ? this.d.getTime() : this.i) - this.f.getTime();
            if (time2 > 0) {
                return time2 / 10;
            }
            return 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    private long d() {
        long j2 = 0;
        if (this.d != null) {
            j2 = Math.max(0L, this.j - this.d.getTime());
        }
        if (this.l != -1) {
            j2 = Math.max(j2, TimeUnit.SECONDS.toMillis((long) this.l));
        }
        return j2 + (this.j - this.i) + (this.f577a - this.j);
    }

    private boolean e() {
        return this.c.m().c() == -1 && this.h == null;
    }

    public c a() {
        c b2 = b();
        return (b2.f575a == null || !this.f578b.h().i()) ? b2 : new c(null, null);
    }
}
