package com.e.a.a.c;

import java.util.zip.Inflater;

class u extends Inflater {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ s f659a;

    u(s sVar) {
        this.f659a = sVar;
    }

    public int inflate(byte[] bArr, int i, int i2) {
        int inflate = super.inflate(bArr, i, i2);
        if (inflate != 0 || !needsDictionary()) {
            return inflate;
        }
        setDictionary(z.f665a);
        return super.inflate(bArr, i, i2);
    }
}
