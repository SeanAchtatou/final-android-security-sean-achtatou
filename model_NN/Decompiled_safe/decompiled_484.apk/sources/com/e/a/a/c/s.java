package com.e.a.a.c;

import a.i;
import a.j;
import a.p;
import a.q;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class s {

    /* renamed from: a  reason: collision with root package name */
    private final p f656a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f657b;
    private final i c = q.a(this.f656a);

    public s(i iVar) {
        this.f656a = new p(new t(this, iVar), new u(this));
    }

    private j b() {
        return this.c.c((long) this.c.k());
    }

    private void c() {
        if (this.f657b > 0) {
            this.f656a.b();
            if (this.f657b != 0) {
                throw new IOException("compressedLimit > 0: " + this.f657b);
            }
        }
    }

    public List<e> a(int i) {
        this.f657b += i;
        int k = this.c.k();
        if (k < 0) {
            throw new IOException("numberOfPairs < 0: " + k);
        } else if (k > 1024) {
            throw new IOException("numberOfPairs > 1024: " + k);
        } else {
            ArrayList arrayList = new ArrayList(k);
            for (int i2 = 0; i2 < k; i2++) {
                j e = b().e();
                j b2 = b();
                if (e.f() == 0) {
                    throw new IOException("name.size == 0");
                }
                arrayList.add(new e(e, b2));
            }
            c();
            return arrayList;
        }
    }

    public void a() {
        this.c.close();
    }
}
