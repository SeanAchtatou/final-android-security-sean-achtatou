package com.e.a.a.a;

import a.aa;
import a.ac;
import a.f;
import a.n;
import com.e.a.a.v;
import java.net.ProtocolException;

final class l implements aa {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f585a;

    /* renamed from: b  reason: collision with root package name */
    private final n f586b;
    private boolean c;
    private long d;

    private l(g gVar, long j) {
        this.f585a = gVar;
        this.f586b = new n(this.f585a.e.a());
        this.d = j;
    }

    public ac a() {
        return this.f586b;
    }

    public void a_(f fVar, long j) {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        v.a(fVar.b(), 0, j);
        if (j > this.d) {
            throw new ProtocolException("expected " + this.d + " bytes but received " + j);
        }
        this.f585a.e.a_(fVar, j);
        this.d -= j;
    }

    public void close() {
        if (!this.c) {
            this.c = true;
            if (this.d > 0) {
                throw new ProtocolException("unexpected end of stream");
            }
            this.f585a.a(this.f586b);
            int unused = this.f585a.f = 3;
        }
    }

    public void flush() {
        if (!this.c) {
            this.f585a.e.flush();
        }
    }
}
