package com.e.a.a.c;

import java.util.concurrent.CountDownLatch;

public final class v {

    /* renamed from: a  reason: collision with root package name */
    private final CountDownLatch f660a = new CountDownLatch(1);

    /* renamed from: b  reason: collision with root package name */
    private long f661b = -1;
    private long c = -1;

    v() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.f661b != -1) {
            throw new IllegalStateException();
        }
        this.f661b = System.nanoTime();
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.c != -1 || this.f661b == -1) {
            throw new IllegalStateException();
        }
        this.c = System.nanoTime();
        this.f660a.countDown();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.c != -1 || this.f661b == -1) {
            throw new IllegalStateException();
        }
        this.c = this.f661b - 1;
        this.f660a.countDown();
    }
}
