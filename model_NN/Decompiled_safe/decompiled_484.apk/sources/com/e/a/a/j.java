package com.e.a.a;

import a.aa;
import a.f;
import a.l;
import java.io.IOException;

class j extends l {

    /* renamed from: a  reason: collision with root package name */
    private boolean f679a;

    public j(aa aaVar) {
        super(aaVar);
    }

    /* access modifiers changed from: protected */
    public void a(IOException iOException) {
    }

    public void a_(f fVar, long j) {
        if (this.f679a) {
            fVar.g(j);
            return;
        }
        try {
            super.a_(fVar, j);
        } catch (IOException e) {
            this.f679a = true;
            a(e);
        }
    }

    public void close() {
        if (!this.f679a) {
            try {
                super.close();
            } catch (IOException e) {
                this.f679a = true;
                a(e);
            }
        }
    }

    public void flush() {
        if (!this.f679a) {
            try {
                super.flush();
            } catch (IOException e) {
                this.f679a = true;
                a(e);
            }
        }
    }
}
