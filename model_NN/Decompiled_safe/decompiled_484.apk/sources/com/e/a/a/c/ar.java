package com.e.a.a.c;

import a.ab;
import a.ac;
import a.f;
import a.i;
import java.io.EOFException;
import java.io.IOException;

final class ar implements ab {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f629a = (!ao.class.desiredAssertionStatus());

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ao f630b;
    private final f c;
    private final f d;
    private final long e;
    /* access modifiers changed from: private */
    public boolean f;
    /* access modifiers changed from: private */
    public boolean g;

    private ar(ao aoVar, long j) {
        this.f630b = aoVar;
        this.c = new f();
        this.d = new f();
        this.e = j;
    }

    private void b() {
        this.f630b.j.c();
        while (this.d.b() == 0 && !this.g && !this.f && this.f630b.l == null) {
            try {
                this.f630b.k();
            } finally {
                this.f630b.j.b();
            }
        }
    }

    private void c() {
        if (this.f) {
            throw new IOException("stream closed");
        } else if (this.f630b.l != null) {
            throw new IOException("stream was reset: " + this.f630b.l);
        }
    }

    public long a(f fVar, long j) {
        long a2;
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        }
        synchronized (this.f630b) {
            b();
            c();
            if (this.d.b() == 0) {
                a2 = -1;
            } else {
                a2 = this.d.a(fVar, Math.min(j, this.d.b()));
                this.f630b.f625a += a2;
                if (this.f630b.f625a >= ((long) (this.f630b.f.e.e(65536) / 2))) {
                    this.f630b.f.a(this.f630b.e, this.f630b.f625a);
                    this.f630b.f625a = 0;
                }
                synchronized (this.f630b.f) {
                    this.f630b.f.c += a2;
                    if (this.f630b.f.c >= ((long) (this.f630b.f.e.e(65536) / 2))) {
                        this.f630b.f.a(0, this.f630b.f.c);
                        this.f630b.f.c = 0;
                    }
                }
            }
        }
        return a2;
    }

    public ac a() {
        return this.f630b.j;
    }

    /* access modifiers changed from: package-private */
    public void a(i iVar, long j) {
        boolean z;
        boolean z2;
        if (f629a || !Thread.holdsLock(this.f630b)) {
            while (j > 0) {
                synchronized (this.f630b) {
                    z = this.g;
                    z2 = this.d.b() + j > this.e;
                }
                if (z2) {
                    iVar.g(j);
                    this.f630b.b(a.FLOW_CONTROL_ERROR);
                    return;
                } else if (z) {
                    iVar.g(j);
                    return;
                } else {
                    long a2 = iVar.a(this.c, j);
                    if (a2 == -1) {
                        throw new EOFException();
                    }
                    j -= a2;
                    synchronized (this.f630b) {
                        boolean z3 = this.d.b() == 0;
                        this.d.a(this.c);
                        if (z3) {
                            this.f630b.notifyAll();
                        }
                    }
                }
            }
            return;
        }
        throw new AssertionError();
    }

    public void close() {
        synchronized (this.f630b) {
            this.f = true;
            this.d.t();
            this.f630b.notifyAll();
        }
        this.f630b.i();
    }
}
