package com.e.a.a.c;

import a.ab;
import a.i;
import a.j;
import a.q;
import android.support.v4.media.TransportMediator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

final class h {

    /* renamed from: a  reason: collision with root package name */
    e[] f638a = new e[8];

    /* renamed from: b  reason: collision with root package name */
    int f639b = (this.f638a.length - 1);
    int c = 0;
    int d = 0;
    private final List<e> e = new ArrayList();
    private final i f;
    private int g;
    private int h;

    h(int i, ab abVar) {
        this.g = i;
        this.h = i;
        this.f = q.a(abVar);
    }

    private void a(int i, e eVar) {
        this.e.add(eVar);
        int i2 = eVar.j;
        if (i != -1) {
            i2 -= this.f638a[d(i)].j;
        }
        if (i2 > this.h) {
            e();
            return;
        }
        int b2 = b((this.d + i2) - this.h);
        if (i == -1) {
            if (this.c + 1 > this.f638a.length) {
                e[] eVarArr = new e[(this.f638a.length * 2)];
                System.arraycopy(this.f638a, 0, eVarArr, this.f638a.length, this.f638a.length);
                this.f639b = this.f638a.length - 1;
                this.f638a = eVarArr;
            }
            int i3 = this.f639b;
            this.f639b = i3 - 1;
            this.f638a[i3] = eVar;
            this.c++;
        } else {
            this.f638a[b2 + d(i) + i] = eVar;
        }
        this.d = i2 + this.d;
    }

    private int b(int i) {
        int i2 = 0;
        if (i > 0) {
            int length = this.f638a.length;
            while (true) {
                length--;
                if (length < this.f639b || i <= 0) {
                    System.arraycopy(this.f638a, this.f639b + 1, this.f638a, this.f639b + 1 + i2, this.c);
                    this.f639b += i2;
                } else {
                    i -= this.f638a[length].j;
                    this.d -= this.f638a[length].j;
                    this.c--;
                    i2++;
                }
            }
            System.arraycopy(this.f638a, this.f639b + 1, this.f638a, this.f639b + 1 + i2, this.c);
            this.f639b += i2;
        }
        return i2;
    }

    private void c(int i) {
        if (h(i)) {
            this.e.add(g.f636a[i]);
            return;
        }
        int d2 = d(i - g.f636a.length);
        if (d2 < 0 || d2 > this.f638a.length - 1) {
            throw new IOException("Header index too large " + (i + 1));
        }
        this.e.add(this.f638a[d2]);
    }

    private int d(int i) {
        return this.f639b + 1 + i;
    }

    private void d() {
        if (this.h >= this.d) {
            return;
        }
        if (this.h == 0) {
            e();
        } else {
            b(this.d - this.h);
        }
    }

    private void e() {
        this.e.clear();
        Arrays.fill(this.f638a, (Object) null);
        this.f639b = this.f638a.length - 1;
        this.c = 0;
        this.d = 0;
    }

    private void e(int i) {
        this.e.add(new e(g(i), c()));
    }

    private void f() {
        this.e.add(new e(g.b(c()), c()));
    }

    private void f(int i) {
        a(-1, new e(g(i), c()));
    }

    private j g(int i) {
        return h(i) ? g.f636a[i].h : this.f638a[d(i - g.f636a.length)].h;
    }

    private void g() {
        a(-1, new e(g.b(c()), c()));
    }

    private int h() {
        return this.f.i() & 255;
    }

    private boolean h(int i) {
        return i >= 0 && i <= g.f636a.length + -1;
    }

    /* access modifiers changed from: package-private */
    public int a(int i, int i2) {
        int i3 = i & i2;
        if (i3 < i2) {
            return i3;
        }
        int i4 = 0;
        while (true) {
            int h2 = h();
            if ((h2 & 128) == 0) {
                return (h2 << i4) + i2;
            }
            i2 += (h2 & TransportMediator.KEYCODE_MEDIA_PAUSE) << i4;
            i4 += 7;
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        while (!this.f.f()) {
            byte i = this.f.i() & 255;
            if (i == 128) {
                throw new IOException("index == 0");
            } else if ((i & 128) == 128) {
                c(a(i, (int) TransportMediator.KEYCODE_MEDIA_PAUSE) - 1);
            } else if (i == 64) {
                g();
            } else if ((i & 64) == 64) {
                f(a(i, 63) - 1);
            } else if ((i & 32) == 32) {
                this.h = a(i, 31);
                if (this.h < 0 || this.h > this.g) {
                    throw new IOException("Invalid dynamic table size update " + this.h);
                }
                d();
            } else if (i == 16 || i == 0) {
                f();
            } else {
                e(a(i, 15) - 1);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.g = i;
        this.h = i;
        d();
    }

    public List<e> b() {
        ArrayList arrayList = new ArrayList(this.e);
        this.e.clear();
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public j c() {
        int h2 = h();
        boolean z = (h2 & 128) == 128;
        int a2 = a(h2, (int) TransportMediator.KEYCODE_MEDIA_PAUSE);
        return z ? j.a(o.a().a(this.f.f((long) a2))) : this.f.c((long) a2);
    }
}
