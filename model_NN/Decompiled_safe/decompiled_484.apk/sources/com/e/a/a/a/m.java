package com.e.a.a.a;

import a.f;
import com.e.a.a.v;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;

class m extends i {
    final /* synthetic */ g d;
    private long e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(g gVar, long j) {
        super(gVar);
        this.d = gVar;
        this.e = j;
        if (this.e == 0) {
            a(true);
        }
    }

    public long a(f fVar, long j) {
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f582b) {
            throw new IllegalStateException("closed");
        } else if (this.e == 0) {
            return -1;
        } else {
            long a2 = this.d.d.a(fVar, Math.min(this.e, j));
            if (a2 == -1) {
                b();
                throw new ProtocolException("unexpected end of stream");
            }
            this.e -= a2;
            if (this.e == 0) {
                a(true);
            }
            return a2;
        }
    }

    public void close() {
        if (!this.f582b) {
            if (this.e != 0 && !v.a(this, 100, TimeUnit.MILLISECONDS)) {
                b();
            }
            this.f582b = true;
        }
    }
}
