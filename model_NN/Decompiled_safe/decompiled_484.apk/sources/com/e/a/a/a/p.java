package com.e.a.a.a;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

final class p extends ThreadLocal<DateFormat> {
    p() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public DateFormat initialValue() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss 'GMT'", Locale.US);
        simpleDateFormat.setLenient(false);
        simpleDateFormat.setTimeZone(o.f587a);
        return simpleDateFormat;
    }
}
