package com.e.a.a.c;

import a.f;
import java.io.Closeable;
import java.util.List;

public interface d extends Closeable {
    void a();

    void a(int i, int i2, List<e> list);

    void a(int i, long j);

    void a(int i, a aVar);

    void a(int i, a aVar, byte[] bArr);

    void a(y yVar);

    void a(boolean z, int i, int i2);

    void a(boolean z, int i, f fVar, int i2);

    void a(boolean z, boolean z2, int i, int i2, List<e> list);

    void b();

    void b(y yVar);

    int c();
}
