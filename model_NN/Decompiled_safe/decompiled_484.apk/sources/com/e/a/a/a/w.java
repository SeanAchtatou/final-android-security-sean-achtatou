package com.e.a.a.a;

import com.e.a.a.q;
import com.e.a.a.v;
import com.e.a.ad;
import com.e.a.af;
import com.e.a.ap;
import com.e.a.ar;
import com.e.a.au;
import com.e.a.b;
import com.e.a.r;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public final class w {

    /* renamed from: a  reason: collision with root package name */
    static final String f597a = q.a().b();

    /* renamed from: b  reason: collision with root package name */
    public static final String f598b = (f597a + "-Sent-Millis");
    public static final String c = (f597a + "-Received-Millis");
    public static final String d = (f597a + "-Selected-Protocol");
    private static final Comparator<String> e = new x();

    public static long a(ad adVar) {
        return b(adVar.a("Content-Length"));
    }

    public static long a(ap apVar) {
        return a(apVar.e());
    }

    public static long a(au auVar) {
        return a(auVar.g());
    }

    public static ad a(ad adVar, ad adVar2) {
        Set<String> c2 = c(adVar2);
        if (c2.isEmpty()) {
            return new af().a();
        }
        af afVar = new af();
        int a2 = adVar.a();
        for (int i = 0; i < a2; i++) {
            String a3 = adVar.a(i);
            if (c2.contains(a3)) {
                afVar.a(a3, adVar.b(i));
            }
        }
        return afVar.a();
    }

    public static ap a(b bVar, au auVar, Proxy proxy) {
        return auVar.c() == 407 ? bVar.b(proxy, auVar) : bVar.a(proxy, auVar);
    }

    private static String a(List<String> list) {
        if (list.size() == 1) {
            return list.get(0);
        }
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sb.append("; ");
            }
            sb.append(list.get(i));
        }
        return sb.toString();
    }

    public static Map<String, List<String>> a(ad adVar, String str) {
        TreeMap treeMap = new TreeMap(e);
        int a2 = adVar.a();
        for (int i = 0; i < a2; i++) {
            String a3 = adVar.a(i);
            String b2 = adVar.b(i);
            ArrayList arrayList = new ArrayList();
            List list = (List) treeMap.get(a3);
            if (list != null) {
                arrayList.addAll(list);
            }
            arrayList.add(b2);
            treeMap.put(a3, Collections.unmodifiableList(arrayList));
        }
        if (str != null) {
            treeMap.put(null, Collections.unmodifiableList(Collections.singletonList(str)));
        }
        return Collections.unmodifiableMap(treeMap);
    }

    public static void a(ar arVar, Map<String, List<String>> map) {
        for (Map.Entry next : map.entrySet()) {
            String str = (String) next.getKey();
            if (("Cookie".equalsIgnoreCase(str) || "Cookie2".equalsIgnoreCase(str)) && !((List) next.getValue()).isEmpty()) {
                arVar.b(str, a((List) next.getValue()));
            }
        }
    }

    public static boolean a(au auVar, ad adVar, ap apVar) {
        for (String next : d(auVar)) {
            if (!v.a(adVar.c(next), apVar.b(next))) {
                return false;
            }
        }
        return true;
    }

    static boolean a(String str) {
        return !"Connection".equalsIgnoreCase(str) && !"Keep-Alive".equalsIgnoreCase(str) && !"Proxy-Authenticate".equalsIgnoreCase(str) && !"Proxy-Authorization".equalsIgnoreCase(str) && !"TE".equalsIgnoreCase(str) && !"Trailers".equalsIgnoreCase(str) && !"Transfer-Encoding".equalsIgnoreCase(str) && !"Upgrade".equalsIgnoreCase(str);
    }

    private static long b(String str) {
        if (str == null) {
            return -1;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException e2) {
            return -1;
        }
    }

    public static List<r> b(ad adVar, String str) {
        ArrayList arrayList = new ArrayList();
        int a2 = adVar.a();
        for (int i = 0; i < a2; i++) {
            if (str.equalsIgnoreCase(adVar.a(i))) {
                String b2 = adVar.b(i);
                int i2 = 0;
                while (i2 < b2.length()) {
                    int a3 = f.a(b2, i2, " ");
                    String trim = b2.substring(i2, a3).trim();
                    int a4 = f.a(b2, a3);
                    if (!b2.regionMatches(true, a4, "realm=\"", 0, "realm=\"".length())) {
                        break;
                    }
                    int length = "realm=\"".length() + a4;
                    int a5 = f.a(b2, length, "\"");
                    String substring = b2.substring(length, a5);
                    i2 = f.a(b2, f.a(b2, a5 + 1, ",") + 1);
                    arrayList.add(new r(trim, substring));
                }
            }
        }
        return arrayList;
    }

    public static boolean b(ad adVar) {
        return c(adVar).contains("*");
    }

    public static boolean b(au auVar) {
        return b(auVar.g());
    }

    public static ad c(au auVar) {
        return a(auVar.j().a().e(), auVar.g());
    }

    public static Set<String> c(ad adVar) {
        Set<String> emptySet = Collections.emptySet();
        int a2 = adVar.a();
        for (int i = 0; i < a2; i++) {
            if ("Vary".equalsIgnoreCase(adVar.a(i))) {
                String b2 = adVar.b(i);
                if (emptySet.isEmpty()) {
                    emptySet = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
                }
                for (String trim : b2.split(",")) {
                    emptySet.add(trim.trim());
                }
            }
        }
        return emptySet;
    }

    private static Set<String> d(au auVar) {
        return c(auVar.g());
    }
}
