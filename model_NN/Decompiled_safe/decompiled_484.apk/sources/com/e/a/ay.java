package com.e.a;

import java.net.InetSocketAddress;
import java.net.Proxy;

public final class ay {

    /* renamed from: a  reason: collision with root package name */
    final a f726a;

    /* renamed from: b  reason: collision with root package name */
    final Proxy f727b;
    final InetSocketAddress c;

    public ay(a aVar, Proxy proxy, InetSocketAddress inetSocketAddress) {
        if (aVar == null) {
            throw new NullPointerException("address == null");
        } else if (proxy == null) {
            throw new NullPointerException("proxy == null");
        } else if (inetSocketAddress == null) {
            throw new NullPointerException("inetSocketAddress == null");
        } else {
            this.f726a = aVar;
            this.f727b = proxy;
            this.c = inetSocketAddress;
        }
    }

    public a a() {
        return this.f726a;
    }

    public Proxy b() {
        return this.f727b;
    }

    public InetSocketAddress c() {
        return this.c;
    }

    public boolean d() {
        return this.f726a.e != null && this.f727b.type() == Proxy.Type.HTTP;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ay)) {
            return false;
        }
        ay ayVar = (ay) obj;
        return this.f726a.equals(ayVar.f726a) && this.f727b.equals(ayVar.f727b) && this.c.equals(ayVar.c);
    }

    public int hashCode() {
        return ((((this.f726a.hashCode() + 527) * 31) + this.f727b.hashCode()) * 31) + this.c.hashCode();
    }
}
