package com.e.a;

import com.e.a.a.a.u;
import java.net.URL;

public class ar {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ag f718a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public String f719b;
    /* access modifiers changed from: private */
    public af c;
    /* access modifiers changed from: private */
    public as d;
    /* access modifiers changed from: private */
    public Object e;

    public ar() {
        this.f719b = "GET";
        this.c = new af();
    }

    private ar(ap apVar) {
        this.f718a = apVar.f716a;
        this.f719b = apVar.f717b;
        this.d = apVar.d;
        this.e = apVar.e;
        this.c = apVar.c.b();
    }

    public ap a() {
        if (this.f718a != null) {
            return new ap(this);
        }
        throw new IllegalStateException("url == null");
    }

    public ar a(ad adVar) {
        this.c = adVar.b();
        return this;
    }

    public ar a(ag agVar) {
        if (agVar == null) {
            throw new IllegalArgumentException("url == null");
        }
        this.f718a = agVar;
        return this;
    }

    public ar a(as asVar) {
        return a("POST", asVar);
    }

    public ar a(j jVar) {
        String jVar2 = jVar.toString();
        return jVar2.isEmpty() ? b("Cache-Control") : a("Cache-Control", jVar2);
    }

    public ar a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("url == null");
        }
        if (str.regionMatches(true, 0, "ws:", 0, 3)) {
            str = "http:" + str.substring(3);
        } else if (str.regionMatches(true, 0, "wss:", 0, 4)) {
            str = "https:" + str.substring(4);
        }
        ag c2 = ag.c(str);
        if (c2 != null) {
            return a(c2);
        }
        throw new IllegalArgumentException("unexpected url: " + str);
    }

    public ar a(String str, as asVar) {
        if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("method == null || method.length() == 0");
        } else if (asVar != null && !u.c(str)) {
            throw new IllegalArgumentException("method " + str + " must not have a request body.");
        } else if (asVar != null || !u.b(str)) {
            this.f719b = str;
            this.d = asVar;
            return this;
        } else {
            throw new IllegalArgumentException("method " + str + " must have a request body.");
        }
    }

    public ar a(String str, String str2) {
        this.c.c(str, str2);
        return this;
    }

    public ar a(URL url) {
        if (url == null) {
            throw new IllegalArgumentException("url == null");
        }
        ag a2 = ag.a(url);
        if (a2 != null) {
            return a(a2);
        }
        throw new IllegalArgumentException("unexpected url: " + url);
    }

    public ar b(String str) {
        this.c.b(str);
        return this;
    }

    public ar b(String str, String str2) {
        this.c.a(str, str2);
        return this;
    }
}
