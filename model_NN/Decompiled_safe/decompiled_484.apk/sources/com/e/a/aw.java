package com.e.a;

public class aw {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ap f724a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public ao f725b;
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public ac e;
    /* access modifiers changed from: private */
    public af f;
    /* access modifiers changed from: private */
    public ax g;
    /* access modifiers changed from: private */
    public au h;
    /* access modifiers changed from: private */
    public au i;
    /* access modifiers changed from: private */
    public au j;

    public aw() {
        this.c = -1;
        this.f = new af();
    }

    private aw(au auVar) {
        this.c = -1;
        this.f724a = auVar.f722a;
        this.f725b = auVar.f723b;
        this.c = auVar.c;
        this.d = auVar.d;
        this.e = auVar.e;
        this.f = auVar.f.b();
        this.g = auVar.g;
        this.h = auVar.h;
        this.i = auVar.i;
        this.j = auVar.j;
    }

    private void a(String str, au auVar) {
        if (auVar.g != null) {
            throw new IllegalArgumentException(str + ".body != null");
        } else if (auVar.h != null) {
            throw new IllegalArgumentException(str + ".networkResponse != null");
        } else if (auVar.i != null) {
            throw new IllegalArgumentException(str + ".cacheResponse != null");
        } else if (auVar.j != null) {
            throw new IllegalArgumentException(str + ".priorResponse != null");
        }
    }

    private void d(au auVar) {
        if (auVar.g != null) {
            throw new IllegalArgumentException("priorResponse.body != null");
        }
    }

    public au a() {
        if (this.f724a == null) {
            throw new IllegalStateException("request == null");
        } else if (this.f725b == null) {
            throw new IllegalStateException("protocol == null");
        } else if (this.c >= 0) {
            return new au(this);
        } else {
            throw new IllegalStateException("code < 0: " + this.c);
        }
    }

    public aw a(int i2) {
        this.c = i2;
        return this;
    }

    public aw a(ac acVar) {
        this.e = acVar;
        return this;
    }

    public aw a(ad adVar) {
        this.f = adVar.b();
        return this;
    }

    public aw a(ao aoVar) {
        this.f725b = aoVar;
        return this;
    }

    public aw a(ap apVar) {
        this.f724a = apVar;
        return this;
    }

    public aw a(au auVar) {
        if (auVar != null) {
            a("networkResponse", auVar);
        }
        this.h = auVar;
        return this;
    }

    public aw a(ax axVar) {
        this.g = axVar;
        return this;
    }

    public aw a(String str) {
        this.d = str;
        return this;
    }

    public aw a(String str, String str2) {
        this.f.c(str, str2);
        return this;
    }

    public aw b(au auVar) {
        if (auVar != null) {
            a("cacheResponse", auVar);
        }
        this.i = auVar;
        return this;
    }

    public aw b(String str, String str2) {
        this.f.a(str, str2);
        return this;
    }

    public aw c(au auVar) {
        if (auVar != null) {
            d(auVar);
        }
        this.j = auVar;
        return this;
    }
}
