package com.e.a;

import a.aa;
import com.e.a.a.a.ac;
import com.e.a.a.a.q;
import com.e.a.a.a.z;
import java.io.IOException;
import java.net.ProtocolException;

public class m {

    /* renamed from: a  reason: collision with root package name */
    volatile boolean f747a;

    /* renamed from: b  reason: collision with root package name */
    ap f748b;
    q c;
    /* access modifiers changed from: private */
    public final am d;
    private boolean e;

    m(am amVar, ap apVar) {
        this.d = amVar.w();
        this.f748b = apVar;
    }

    private au a(boolean z) {
        return new n(this, 0, this.f748b, z).a(this.f748b);
    }

    public au a() {
        synchronized (this) {
            if (this.e) {
                throw new IllegalStateException("Already Executed");
            }
            this.e = true;
        }
        try {
            this.d.r().a(this);
            au a2 = a(false);
            if (a2 != null) {
                return a2;
            }
            throw new IOException("Canceled");
        } finally {
            this.d.r().b(this);
        }
    }

    /* access modifiers changed from: package-private */
    public au a(ap apVar, boolean z) {
        ap apVar2;
        as f = apVar.f();
        if (f != null) {
            ar g = apVar.g();
            al a2 = f.a();
            if (a2 != null) {
                g.a("Content-Type", a2.toString());
            }
            long b2 = f.b();
            if (b2 != -1) {
                g.a("Content-Length", Long.toString(b2));
                g.b("Transfer-Encoding");
            } else {
                g.a("Transfer-Encoding", "chunked");
                g.b("Content-Length");
            }
            apVar2 = g.a();
        } else {
            apVar2 = apVar;
        }
        this.c = new q(this.d, apVar2, false, false, z, null, null, null, null);
        int i = 0;
        while (!this.f747a) {
            try {
                this.c.a();
                this.c.j();
                au e2 = this.c.e();
                ap k = this.c.k();
                if (k == null) {
                    if (!z) {
                        this.c.h();
                    }
                    return e2;
                }
                int i2 = i + 1;
                if (i2 > 20) {
                    throw new ProtocolException("Too many follow-up requests: " + i2);
                }
                if (!this.c.b(k.a())) {
                    this.c.h();
                }
                this.c = new q(this.d, k, false, false, z, this.c.i(), null, null, e2);
                i = i2;
            } catch (z e3) {
                throw e3.getCause();
            } catch (ac e4) {
                q a3 = this.c.a(e4);
                if (a3 != null) {
                    this.c = a3;
                } else {
                    throw e4.a();
                }
            } catch (IOException e5) {
                q a4 = this.c.a(e5, (aa) null);
                if (a4 != null) {
                    this.c = a4;
                } else {
                    throw e5;
                }
            }
        }
        this.c.h();
        throw new IOException("Canceled");
    }
}
