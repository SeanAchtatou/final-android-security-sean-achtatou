package com.e.a;

import android.support.v7.internal.widget.ActivityChooserView;
import java.util.concurrent.TimeUnit;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    boolean f745a;

    /* renamed from: b  reason: collision with root package name */
    boolean f746b;
    int c = -1;
    int d = -1;
    int e = -1;
    boolean f;
    boolean g;

    public l a() {
        this.f745a = true;
        return this;
    }

    public l a(int i, TimeUnit timeUnit) {
        if (i < 0) {
            throw new IllegalArgumentException("maxStale < 0: " + i);
        }
        long seconds = timeUnit.toSeconds((long) i);
        this.d = seconds > 2147483647L ? ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED : (int) seconds;
        return this;
    }

    public l b() {
        this.f746b = true;
        return this;
    }

    public l c() {
        this.f = true;
        return this;
    }

    public j d() {
        return new j(this);
    }
}
