package com.e.a;

import a.aa;
import a.l;

class f extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f735a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ com.e.a.a.f f736b;
    final /* synthetic */ e c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    f(e eVar, aa aaVar, c cVar, com.e.a.a.f fVar) {
        super(aaVar);
        this.c = eVar;
        this.f735a = cVar;
        this.f736b = fVar;
    }

    public void close() {
        synchronized (this.c.f733a) {
            if (!this.c.d) {
                boolean unused = this.c.d = true;
                c.b(this.c.f733a);
                super.close();
                this.f736b.a();
            }
        }
    }
}
