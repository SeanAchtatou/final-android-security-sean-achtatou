package com.e.a;

import com.e.a.a.a.ae;
import com.e.a.a.a.af;
import com.e.a.a.a.ag;
import com.e.a.a.a.ai;
import com.e.a.a.a.g;
import com.e.a.a.a.q;
import com.e.a.a.a.v;
import com.e.a.a.c.ac;
import com.e.a.a.c.ak;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownServiceException;
import java.util.List;

public final class t {

    /* renamed from: a  reason: collision with root package name */
    private final u f758a;

    /* renamed from: b  reason: collision with root package name */
    private final ay f759b;
    private Socket c;
    private boolean d = false;
    private g e;
    private ac f;
    private ao g = ao.HTTP_1_1;
    private long h;
    private ac i;
    private int j;
    private Object k;

    public t(u uVar, ay ayVar) {
        this.f758a = uVar;
        this.f759b = ayVar;
    }

    /* access modifiers changed from: package-private */
    public ai a(q qVar) {
        return this.f != null ? new ag(qVar, this.f) : new v(qVar, this.e);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3) {
        if (!this.d) {
            throw new IllegalStateException("setTimeouts - not connected");
        } else if (this.e != null) {
            try {
                this.c.setSoTimeout(i2);
                this.e.a(i2, i3);
            } catch (IOException e2) {
                throw new com.e.a.a.a.ac(e2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, int i4, ap apVar, List<w> list, boolean z) {
        af a2;
        if (this.d) {
            throw new IllegalStateException("already connected");
        }
        ae aeVar = new ae(this, this.f758a);
        if (this.f759b.f726a.d() != null) {
            a2 = aeVar.a(i2, i3, i4, apVar, this.f759b, list, z);
        } else if (!list.contains(w.c)) {
            throw new com.e.a.a.a.ac(new UnknownServiceException("CLEARTEXT communication not supported: " + list));
        } else {
            a2 = aeVar.a(i2, i3, this.f759b);
        }
        this.c = a2.f570b;
        this.i = a2.d;
        this.g = a2.c == null ? ao.HTTP_1_1 : a2.c;
        try {
            if (this.g == ao.SPDY_3 || this.g == ao.HTTP_2) {
                this.c.setSoTimeout(0);
                this.f = new ak(this.f759b.f726a.f557b, true, this.c).a(this.g).a();
                this.f.e();
            } else {
                this.e = new g(this.f758a, this, this.c);
            }
            this.d = true;
        } catch (IOException e2) {
            throw new com.e.a.a.a.ac(e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(am amVar, Object obj, ap apVar) {
        a(obj);
        if (!b()) {
            ap apVar2 = apVar;
            a(amVar.a(), amVar.b(), amVar.c(), apVar2, this.f759b.f726a.h(), amVar.p());
            if (k()) {
                amVar.m().b(this);
            }
            amVar.q().b(c());
        }
        a(amVar.b(), amVar.c());
    }

    /* access modifiers changed from: package-private */
    public void a(ao aoVar) {
        if (aoVar == null) {
            throw new IllegalArgumentException("protocol == null");
        }
        this.g = aoVar;
    }

    /* access modifiers changed from: package-private */
    public void a(Object obj) {
        if (!k()) {
            synchronized (this.f758a) {
                if (this.k != null) {
                    throw new IllegalStateException("Connection already has an owner!");
                }
                this.k = obj;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        boolean z;
        synchronized (this.f758a) {
            if (this.k == null) {
                z = false;
            } else {
                this.k = null;
                z = true;
            }
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.d;
    }

    public ay c() {
        return this.f759b;
    }

    public Socket d() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return !this.c.isClosed() && !this.c.isInputShutdown() && !this.c.isOutputShutdown();
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        if (this.e != null) {
            return this.e.f();
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void g() {
        if (this.f != null) {
            throw new IllegalStateException("spdyConnection != null");
        }
        this.h = System.nanoTime();
    }

    /* access modifiers changed from: package-private */
    public boolean h() {
        return this.f == null || this.f.b();
    }

    /* access modifiers changed from: package-private */
    public long i() {
        return this.f == null ? this.h : this.f.c();
    }

    public ac j() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public boolean k() {
        return this.f != null;
    }

    public ao l() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public void m() {
        this.j++;
    }

    /* access modifiers changed from: package-private */
    public int n() {
        return this.j;
    }

    public String toString() {
        return "Connection{" + this.f759b.f726a.f557b + ":" + this.f759b.f726a.c + ", proxy=" + this.f759b.f727b + " hostAddress=" + this.f759b.c.getAddress().getHostAddress() + " cipherSuite=" + (this.i != null ? this.i.a() : "none") + " protocol=" + this.g + '}';
    }
}
