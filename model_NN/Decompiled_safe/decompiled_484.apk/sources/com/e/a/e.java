package com.e.a;

import a.aa;
import com.e.a.a.a.b;
import com.e.a.a.f;

final class e implements b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f733a;

    /* renamed from: b  reason: collision with root package name */
    private final f f734b;
    private aa c;
    /* access modifiers changed from: private */
    public boolean d;
    private aa e;

    public e(c cVar, f fVar) {
        this.f733a = cVar;
        this.f734b = fVar;
        this.c = fVar.a(1);
        this.e = new f(this, this.c, cVar, fVar);
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a() {
        /*
            r2 = this;
            com.e.a.c r1 = r2.f733a
            monitor-enter(r1)
            boolean r0 = r2.d     // Catch:{ all -> 0x001f }
            if (r0 == 0) goto L_0x0009
            monitor-exit(r1)     // Catch:{ all -> 0x001f }
        L_0x0008:
            return
        L_0x0009:
            r0 = 1
            r2.d = r0     // Catch:{ all -> 0x001f }
            com.e.a.c r0 = r2.f733a     // Catch:{ all -> 0x001f }
            com.e.a.c.c(r0)     // Catch:{ all -> 0x001f }
            monitor-exit(r1)     // Catch:{ all -> 0x001f }
            a.aa r0 = r2.c
            com.e.a.a.v.a(r0)
            com.e.a.a.f r0 = r2.f734b     // Catch:{ IOException -> 0x001d }
            r0.b()     // Catch:{ IOException -> 0x001d }
            goto L_0x0008
        L_0x001d:
            r0 = move-exception
            goto L_0x0008
        L_0x001f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.e.a.e.a():void");
    }

    public aa b() {
        return this.e;
    }
}
