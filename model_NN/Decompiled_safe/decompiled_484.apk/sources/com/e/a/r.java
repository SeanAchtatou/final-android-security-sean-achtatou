package com.e.a;

import com.e.a.a.v;

public final class r {

    /* renamed from: a  reason: collision with root package name */
    private final String f754a;

    /* renamed from: b  reason: collision with root package name */
    private final String f755b;

    public r(String str, String str2) {
        this.f754a = str;
        this.f755b = str2;
    }

    public String a() {
        return this.f754a;
    }

    public String b() {
        return this.f755b;
    }

    public boolean equals(Object obj) {
        return (obj instanceof r) && v.a(this.f754a, ((r) obj).f754a) && v.a(this.f755b, ((r) obj).f755b);
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.f755b != null ? this.f755b.hashCode() : 0) + 899) * 31;
        if (this.f754a != null) {
            i = this.f754a.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return this.f754a + " realm=\"" + this.f755b + "\"";
    }
}
