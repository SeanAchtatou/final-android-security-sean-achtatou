package com.e.a;

import com.e.a.a.a.ai;
import com.e.a.a.a.q;
import com.e.a.a.k;
import com.e.a.a.l;
import com.e.a.a.n;
import com.e.a.a.u;
import javax.net.ssl.SSLSocket;

final class an extends k {
    an() {
    }

    public ai a(t tVar, q qVar) {
        return tVar.a(qVar);
    }

    public l a(am amVar) {
        return amVar.g();
    }

    public void a(af afVar, String str) {
        afVar.a(str);
    }

    public void a(am amVar, t tVar, q qVar, ap apVar) {
        tVar.a(amVar, qVar, apVar);
    }

    public void a(t tVar, ao aoVar) {
        tVar.a(aoVar);
    }

    public void a(u uVar, t tVar) {
        uVar.a(tVar);
    }

    public void a(w wVar, SSLSocket sSLSocket, boolean z) {
        wVar.a(sSLSocket, z);
    }

    public boolean a(t tVar) {
        return tVar.a();
    }

    public int b(t tVar) {
        return tVar.n();
    }

    public u b(am amVar) {
        return amVar.q();
    }

    public void b(t tVar, q qVar) {
        tVar.a((Object) qVar);
    }

    public n c(am amVar) {
        return amVar.u;
    }

    public boolean c(t tVar) {
        return tVar.f();
    }
}
