package com.e.a;

import java.util.ArrayDeque;
import java.util.Deque;

public final class aa {

    /* renamed from: a  reason: collision with root package name */
    private int f698a = 64;

    /* renamed from: b  reason: collision with root package name */
    private int f699b = 5;
    private final Deque<Object> c = new ArrayDeque();
    private final Deque<Object> d = new ArrayDeque();
    private final Deque<m> e = new ArrayDeque();

    /* access modifiers changed from: package-private */
    public synchronized void a(m mVar) {
        this.e.add(mVar);
    }

    /* access modifiers changed from: package-private */
    public synchronized void b(m mVar) {
        if (!this.e.remove(mVar)) {
            throw new AssertionError("Call wasn't in-flight!");
        }
    }
}
