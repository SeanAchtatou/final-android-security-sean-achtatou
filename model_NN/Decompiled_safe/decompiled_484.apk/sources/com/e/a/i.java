package com.e.a;

import a.ab;
import a.f;
import a.h;
import a.j;
import a.q;
import com.e.a.a.a.ah;
import com.e.a.a.a.w;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class i {

    /* renamed from: a  reason: collision with root package name */
    private final String f741a;

    /* renamed from: b  reason: collision with root package name */
    private final ad f742b;
    private final String c;
    private final ao d;
    private final int e;
    private final String f;
    private final ad g;
    private final ac h;

    public i(ab abVar) {
        try {
            a.i a2 = q.a(abVar);
            this.f741a = a2.r();
            this.c = a2.r();
            af afVar = new af();
            int a3 = c.b(a2);
            for (int i = 0; i < a3; i++) {
                afVar.a(a2.r());
            }
            this.f742b = afVar.a();
            ah a4 = ah.a(a2.r());
            this.d = a4.f573a;
            this.e = a4.f574b;
            this.f = a4.c;
            af afVar2 = new af();
            int a5 = c.b(a2);
            for (int i2 = 0; i2 < a5; i2++) {
                afVar2.a(a2.r());
            }
            this.g = afVar2.a();
            if (a()) {
                String r = a2.r();
                if (r.length() > 0) {
                    throw new IOException("expected \"\" but was \"" + r + "\"");
                }
                this.h = ac.a(a2.r(), a(a2), a(a2));
            } else {
                this.h = null;
            }
        } finally {
            abVar.close();
        }
    }

    public i(au auVar) {
        this.f741a = auVar.a().c();
        this.f742b = w.c(auVar);
        this.c = auVar.a().d();
        this.d = auVar.b();
        this.e = auVar.c();
        this.f = auVar.e();
        this.g = auVar.g();
        this.h = auVar.f();
    }

    private List<Certificate> a(a.i iVar) {
        int a2 = c.b(iVar);
        if (a2 == -1) {
            return Collections.emptyList();
        }
        try {
            CertificateFactory instance = CertificateFactory.getInstance("X.509");
            ArrayList arrayList = new ArrayList(a2);
            for (int i = 0; i < a2; i++) {
                String r = iVar.r();
                f fVar = new f();
                fVar.b(j.b(r));
                arrayList.add(instance.generateCertificate(fVar.g()));
            }
            return arrayList;
        } catch (CertificateException e2) {
            throw new IOException(e2.getMessage());
        }
    }

    private void a(h hVar, List<Certificate> list) {
        try {
            hVar.k((long) list.size());
            hVar.i(10);
            int size = list.size();
            for (int i = 0; i < size; i++) {
                hVar.b(j.a(list.get(i).getEncoded()).b());
                hVar.i(10);
            }
        } catch (CertificateEncodingException e2) {
            throw new IOException(e2.getMessage());
        }
    }

    private boolean a() {
        return this.f741a.startsWith("https://");
    }

    public au a(ap apVar, com.e.a.a.i iVar) {
        String a2 = this.g.a("Content-Type");
        String a3 = this.g.a("Content-Length");
        return new aw().a(new ar().a(this.f741a).a(this.c, (as) null).a(this.f742b).a()).a(this.d).a(this.e).a(this.f).a(this.g).a(new g(iVar, a2, a3)).a(this.h).a();
    }

    public void a(com.e.a.a.f fVar) {
        h a2 = q.a(fVar.a(0));
        a2.b(this.f741a);
        a2.i(10);
        a2.b(this.c);
        a2.i(10);
        a2.k((long) this.f742b.a());
        a2.i(10);
        int a3 = this.f742b.a();
        for (int i = 0; i < a3; i++) {
            a2.b(this.f742b.a(i));
            a2.b(": ");
            a2.b(this.f742b.b(i));
            a2.i(10);
        }
        a2.b(new ah(this.d, this.e, this.f).toString());
        a2.i(10);
        a2.k((long) this.g.a());
        a2.i(10);
        int a4 = this.g.a();
        for (int i2 = 0; i2 < a4; i2++) {
            a2.b(this.g.a(i2));
            a2.b(": ");
            a2.b(this.g.b(i2));
            a2.i(10);
        }
        if (a()) {
            a2.i(10);
            a2.b(this.h.a());
            a2.i(10);
            a(a2, this.h.b());
            a(a2, this.h.c());
        }
        a2.close();
    }

    public boolean a(ap apVar, au auVar) {
        return this.f741a.equals(apVar.c()) && this.c.equals(apVar.d()) && w.a(auVar, this.f742b, apVar);
    }
}
