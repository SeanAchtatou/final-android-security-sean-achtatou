package com.e.a;

import com.e.a.a.a.w;
import java.util.Collections;
import java.util.List;

public final class au {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final ap f722a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final ao f723b;
    /* access modifiers changed from: private */
    public final int c;
    /* access modifiers changed from: private */
    public final String d;
    /* access modifiers changed from: private */
    public final ac e;
    /* access modifiers changed from: private */
    public final ad f;
    /* access modifiers changed from: private */
    public final ax g;
    /* access modifiers changed from: private */
    public au h;
    /* access modifiers changed from: private */
    public au i;
    /* access modifiers changed from: private */
    public final au j;
    private volatile j k;

    private au(aw awVar) {
        this.f722a = awVar.f724a;
        this.f723b = awVar.f725b;
        this.c = awVar.c;
        this.d = awVar.d;
        this.e = awVar.e;
        this.f = awVar.f.a();
        this.g = awVar.g;
        this.h = awVar.h;
        this.i = awVar.i;
        this.j = awVar.j;
    }

    public ap a() {
        return this.f722a;
    }

    public String a(String str) {
        return a(str, null);
    }

    public String a(String str, String str2) {
        String a2 = this.f.a(str);
        return a2 != null ? a2 : str2;
    }

    public ao b() {
        return this.f723b;
    }

    public int c() {
        return this.c;
    }

    public boolean d() {
        return this.c >= 200 && this.c < 300;
    }

    public String e() {
        return this.d;
    }

    public ac f() {
        return this.e;
    }

    public ad g() {
        return this.f;
    }

    public ax h() {
        return this.g;
    }

    public aw i() {
        return new aw(this);
    }

    public au j() {
        return this.h;
    }

    public au k() {
        return this.i;
    }

    public List<r> l() {
        String str;
        if (this.c == 401) {
            str = "WWW-Authenticate";
        } else if (this.c != 407) {
            return Collections.emptyList();
        } else {
            str = "Proxy-Authenticate";
        }
        return w.b(g(), str);
    }

    public j m() {
        j jVar = this.k;
        if (jVar != null) {
            return jVar;
        }
        j a2 = j.a(this.f);
        this.k = a2;
        return a2;
    }

    public String toString() {
        return "Response{protocol=" + this.f723b + ", code=" + this.c + ", message=" + this.d + ", url=" + this.f722a.c() + '}';
    }
}
