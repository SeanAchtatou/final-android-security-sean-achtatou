package com.e.a;

import a.j;
import com.e.a.a.v;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.net.ssl.SSLPeerUnverifiedException;

public final class o {

    /* renamed from: a  reason: collision with root package name */
    public static final o f751a = new q().a();

    /* renamed from: b  reason: collision with root package name */
    private final Map<String, Set<j>> f752b;

    private o(q qVar) {
        this.f752b = v.a(qVar.f753a);
    }

    private static j a(X509Certificate x509Certificate) {
        return v.a(j.a(x509Certificate.getPublicKey().getEncoded()));
    }

    public static String a(Certificate certificate) {
        if (certificate instanceof X509Certificate) {
            return "sha1/" + a((X509Certificate) certificate).b();
        }
        throw new IllegalArgumentException("Certificate pinning requires X509 certificates");
    }

    /* access modifiers changed from: package-private */
    public Set<j> a(String str) {
        Set<j> set = this.f752b.get(str);
        int indexOf = str.indexOf(46);
        Set<j> set2 = indexOf != str.lastIndexOf(46) ? this.f752b.get("*." + str.substring(indexOf + 1)) : null;
        if (set == null && set2 == null) {
            return null;
        }
        if (set == null || set2 == null) {
            return set == null ? set2 : set;
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        linkedHashSet.addAll(set);
        linkedHashSet.addAll(set2);
        return linkedHashSet;
    }

    public void a(String str, List<Certificate> list) {
        Set<j> a2 = a(str);
        if (a2 != null) {
            int size = list.size();
            int i = 0;
            while (i < size) {
                if (!a2.contains(a((X509Certificate) list.get(i)))) {
                    i++;
                } else {
                    return;
                }
            }
            StringBuilder append = new StringBuilder().append("Certificate pinning failure!").append("\n  Peer certificate chain:");
            int size2 = list.size();
            for (int i2 = 0; i2 < size2; i2++) {
                X509Certificate x509Certificate = (X509Certificate) list.get(i2);
                append.append("\n    ").append(a((Certificate) x509Certificate)).append(": ").append(x509Certificate.getSubjectDN().getName());
            }
            append.append("\n  Pinned certificates for ").append(str).append(":");
            for (j b2 : a2) {
                append.append("\n    sha1/").append(b2.b());
            }
            throw new SSLPeerUnverifiedException(append.toString());
        }
    }
}
