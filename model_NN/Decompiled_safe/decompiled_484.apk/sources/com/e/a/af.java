package com.e.a;

import java.util.ArrayList;
import java.util.List;

public final class af {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final List<String> f705a = new ArrayList(20);

    public ad a() {
        return new ad(this);
    }

    /* access modifiers changed from: package-private */
    public af a(String str) {
        int indexOf = str.indexOf(":", 1);
        return indexOf != -1 ? b(str.substring(0, indexOf), str.substring(indexOf + 1)) : str.startsWith(":") ? b("", str.substring(1)) : b("", str);
    }

    public af a(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("name == null");
        } else if (str2 == null) {
            throw new IllegalArgumentException("value == null");
        } else if (str.length() != 0 && str.indexOf(0) == -1 && str2.indexOf(0) == -1) {
            return b(str, str2);
        } else {
            throw new IllegalArgumentException("Unexpected header: " + str + ": " + str2);
        }
    }

    public af b(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f705a.size()) {
                return this;
            }
            if (str.equalsIgnoreCase(this.f705a.get(i2))) {
                this.f705a.remove(i2);
                this.f705a.remove(i2);
                i2 -= 2;
            }
            i = i2 + 2;
        }
    }

    /* access modifiers changed from: package-private */
    public af b(String str, String str2) {
        this.f705a.add(str);
        this.f705a.add(str2.trim());
        return this;
    }

    public af c(String str, String str2) {
        b(str);
        a(str, str2);
        return this;
    }
}
