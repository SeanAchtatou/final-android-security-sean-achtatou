package com.e.a;

import com.e.a.a.a.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public final class ad {

    /* renamed from: a  reason: collision with root package name */
    private final String[] f704a;

    private ad(af afVar) {
        this.f704a = (String[]) afVar.f705a.toArray(new String[afVar.f705a.size()]);
    }

    private static String a(String[] strArr, String str) {
        for (int length = strArr.length - 2; length >= 0; length -= 2) {
            if (str.equalsIgnoreCase(strArr[length])) {
                return strArr[length + 1];
            }
        }
        return null;
    }

    public int a() {
        return this.f704a.length / 2;
    }

    public String a(int i) {
        int i2 = i * 2;
        if (i2 < 0 || i2 >= this.f704a.length) {
            return null;
        }
        return this.f704a[i2];
    }

    public String a(String str) {
        return a(this.f704a, str);
    }

    public af b() {
        af afVar = new af();
        Collections.addAll(afVar.f705a, this.f704a);
        return afVar;
    }

    public String b(int i) {
        int i2 = (i * 2) + 1;
        if (i2 < 0 || i2 >= this.f704a.length) {
            return null;
        }
        return this.f704a[i2];
    }

    public Date b(String str) {
        String a2 = a(str);
        if (a2 != null) {
            return o.a(a2);
        }
        return null;
    }

    public List<String> c(String str) {
        int a2 = a();
        ArrayList arrayList = null;
        for (int i = 0; i < a2; i++) {
            if (str.equalsIgnoreCase(a(i))) {
                if (arrayList == null) {
                    arrayList = new ArrayList(2);
                }
                arrayList.add(b(i));
            }
        }
        return arrayList != null ? Collections.unmodifiableList(arrayList) : Collections.emptyList();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        int a2 = a();
        for (int i = 0; i < a2; i++) {
            sb.append(a(i)).append(": ").append(b(i)).append("\n");
        }
        return sb.toString();
    }
}
