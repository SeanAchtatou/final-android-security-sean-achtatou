package com.e.a;

import a.f;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class ag {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f706a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final String f707b;
    private final String c;
    private final String d;
    /* access modifiers changed from: private */
    public final String e;
    /* access modifiers changed from: private */
    public final int f;
    private final List<String> g;
    private final List<String> h;
    private final String i;
    private final String j;

    private ag(ai aiVar) {
        String str = null;
        this.f707b = aiVar.f708a;
        this.c = d(aiVar.f709b);
        this.d = d(aiVar.c);
        this.e = aiVar.d;
        this.f = aiVar.a();
        this.g = a(aiVar.f);
        this.h = aiVar.g != null ? a(aiVar.g) : null;
        this.i = aiVar.h != null ? d(aiVar.h) : str;
        this.j = aiVar.toString();
    }

    static int a(char c2) {
        if (c2 >= '0' && c2 <= '9') {
            return c2 - '0';
        }
        if (c2 >= 'a' && c2 <= 'f') {
            return (c2 - 'a') + 10;
        }
        if (c2 < 'A' || c2 > 'F') {
            return -1;
        }
        return (c2 - 'A') + 10;
    }

    public static int a(String str) {
        if (str.equals("http")) {
            return 80;
        }
        return str.equals("https") ? 443 : -1;
    }

    public static ag a(URL url) {
        return c(url.toString());
    }

    static String a(String str, int i2, int i3) {
        for (int i4 = i2; i4 < i3; i4++) {
            if (str.charAt(i4) == '%') {
                f fVar = new f();
                fVar.a(str, i2, i4);
                a(fVar, str, i4, i3);
                return fVar.q();
            }
        }
        return str.substring(i2, i3);
    }

    static String a(String str, int i2, int i3, String str2, boolean z, boolean z2) {
        int i4 = i2;
        while (i4 < i3) {
            int codePointAt = str.codePointAt(i4);
            if (codePointAt < 32 || codePointAt >= 127 || str2.indexOf(codePointAt) != -1 || ((codePointAt == 37 && !z) || (z2 && codePointAt == 43))) {
                f fVar = new f();
                fVar.a(str, i2, i4);
                a(fVar, str, i4, i3, str2, z, z2);
                return fVar.q();
            }
            i4 += Character.charCount(codePointAt);
        }
        return str.substring(i2, i3);
    }

    static String a(String str, String str2, boolean z, boolean z2) {
        return a(str, 0, str.length(), str2, z, z2);
    }

    private List<String> a(List<String> list) {
        ArrayList arrayList = new ArrayList(list.size());
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            String next = it.next();
            arrayList.add(next != null ? d(next) : null);
        }
        return Collections.unmodifiableList(arrayList);
    }

    static void a(f fVar, String str, int i2, int i3) {
        int i4 = i2;
        while (i4 < i3) {
            int codePointAt = str.codePointAt(i4);
            if (codePointAt == 37 && i4 + 2 < i3) {
                int a2 = a(str.charAt(i4 + 1));
                int a3 = a(str.charAt(i4 + 2));
                if (!(a2 == -1 || a3 == -1)) {
                    fVar.i((a2 << 4) + a3);
                    i4 += 2;
                    i4 += Character.charCount(codePointAt);
                }
            }
            fVar.a(codePointAt);
            i4 += Character.charCount(codePointAt);
        }
    }

    static void a(f fVar, String str, int i2, int i3, String str2, boolean z, boolean z2) {
        f fVar2 = null;
        while (i2 < i3) {
            int codePointAt = str.codePointAt(i2);
            if (!z || !(codePointAt == 9 || codePointAt == 10 || codePointAt == 12 || codePointAt == 13)) {
                if (z2 && codePointAt == 43) {
                    fVar.b(z ? "%20" : "%2B");
                } else if (codePointAt < 32 || codePointAt >= 127 || str2.indexOf(codePointAt) != -1 || (codePointAt == 37 && !z)) {
                    if (fVar2 == null) {
                        fVar2 = new f();
                    }
                    fVar2.a(codePointAt);
                    while (!fVar2.f()) {
                        byte i4 = fVar2.i() & 255;
                        fVar.i(37);
                        fVar.i((int) f706a[(i4 >> 4) & 15]);
                        fVar.i((int) f706a[i4 & 15]);
                    }
                } else {
                    fVar.a(codePointAt);
                }
            }
            i2 += Character.charCount(codePointAt);
        }
    }

    static void a(StringBuilder sb, List<String> list) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            sb.append('/');
            sb.append(list.get(i2));
        }
    }

    /* access modifiers changed from: private */
    public static int b(String str, int i2, int i3, String str2) {
        for (int i4 = i2; i4 < i3; i4++) {
            if (str2.indexOf(str.charAt(i4)) != -1) {
                return i4;
            }
        }
        return i3;
    }

    static List<String> b(String str) {
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        while (i2 <= str.length()) {
            int indexOf = str.indexOf(38, i2);
            if (indexOf == -1) {
                indexOf = str.length();
            }
            int indexOf2 = str.indexOf(61, i2);
            if (indexOf2 == -1 || indexOf2 > indexOf) {
                arrayList.add(str.substring(i2, indexOf));
                arrayList.add(null);
            } else {
                arrayList.add(str.substring(i2, indexOf2));
                arrayList.add(str.substring(indexOf2 + 1, indexOf));
            }
            i2 = indexOf + 1;
        }
        return arrayList;
    }

    static void b(StringBuilder sb, List<String> list) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2 += 2) {
            String str = list.get(i2);
            String str2 = list.get(i2 + 1);
            if (i2 > 0) {
                sb.append('&');
            }
            sb.append(str);
            if (str2 != null) {
                sb.append('=');
                sb.append(str2);
            }
        }
    }

    public static ag c(String str) {
        return new ai().a(null, str);
    }

    static String d(String str) {
        return a(str, 0, str.length());
    }

    public URL a() {
        try {
            return new URL(this.j);
        } catch (MalformedURLException e2) {
            throw new RuntimeException(e2);
        }
    }

    public URI b() {
        try {
            return new URI(this.j);
        } catch (URISyntaxException e2) {
            throw new IllegalStateException("not valid as a java.net.URI: " + this.j);
        }
    }

    public boolean c() {
        return this.f707b.equals("https");
    }

    public String d() {
        if (this.c.isEmpty()) {
            return "";
        }
        int length = this.f707b.length() + 3;
        return this.j.substring(length, b(this.j, length, this.j.length(), ":@"));
    }

    public String e() {
        if (this.d.isEmpty()) {
            return "";
        }
        int indexOf = this.j.indexOf(64);
        return this.j.substring(this.j.indexOf(58, this.f707b.length() + 3) + 1, indexOf);
    }

    public boolean equals(Object obj) {
        return (obj instanceof ag) && ((ag) obj).j.equals(this.j);
    }

    public List<String> f() {
        int indexOf = this.j.indexOf(47, this.f707b.length() + 3);
        int b2 = b(this.j, indexOf, this.j.length(), "?#");
        ArrayList arrayList = new ArrayList();
        while (indexOf < b2) {
            int i2 = indexOf + 1;
            indexOf = b(this.j, i2, b2, "/");
            arrayList.add(this.j.substring(i2, indexOf));
        }
        return arrayList;
    }

    public String g() {
        if (this.h == null) {
            return null;
        }
        int indexOf = this.j.indexOf(63) + 1;
        return this.j.substring(indexOf, b(this.j, indexOf + 1, this.j.length(), "#"));
    }

    public int hashCode() {
        return this.j.hashCode();
    }

    public String toString() {
        return this.j;
    }
}
