package com.e.a;

import java.net.IDN;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public final class ai {

    /* renamed from: a  reason: collision with root package name */
    String f708a;

    /* renamed from: b  reason: collision with root package name */
    String f709b = "";
    String c = "";
    String d;
    int e = -1;
    final List<String> f = new ArrayList();
    List<String> g;
    String h;

    public ai() {
        this.f.add("");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.ai.a(java.lang.String, int, int, boolean, boolean):void
     arg types: [java.lang.String, int, int, boolean, int]
     candidates:
      com.e.a.ai.a(java.lang.String, int, int, byte[], int):boolean
      com.e.a.ai.a(java.lang.String, int, int, boolean, boolean):void */
    private void a(String str, int i, int i2) {
        if (i != i2) {
            char charAt = str.charAt(i);
            if (charAt == '/' || charAt == '\\') {
                this.f.clear();
                this.f.add("");
                i++;
            } else {
                this.f.set(this.f.size() - 1, "");
            }
            int i3 = i;
            while (i3 < i2) {
                int a2 = ag.b(str, i3, i2, "/\\");
                boolean z = a2 < i2;
                a(str, i3, a2, z, true);
                if (z) {
                    a2++;
                }
                i3 = a2;
            }
        }
    }

    private void a(String str, int i, int i2, boolean z, boolean z2) {
        String a2 = ag.a(str, i, i2, " \"<>^`{}|/\\?#", z2, false);
        if (!b(a2)) {
            if (c(a2)) {
                c();
                return;
            }
            if (this.f.get(this.f.size() - 1).isEmpty()) {
                this.f.set(this.f.size() - 1, a2);
            } else {
                this.f.add(a2);
            }
            if (z) {
                this.f.add("");
            }
        }
    }

    private static boolean a(String str, int i, int i2, byte[] bArr, int i3) {
        int i4 = i;
        int i5 = i3;
        while (i4 < i2) {
            if (i5 == bArr.length) {
                return false;
            }
            if (i5 != i3) {
                if (str.charAt(i4) != '.') {
                    return false;
                }
                i4++;
            }
            int i6 = 0;
            int i7 = i4;
            while (i7 < i2) {
                char charAt = str.charAt(i7);
                if (charAt < '0' || charAt > '9') {
                    break;
                } else if (i6 == 0 && i4 != i7) {
                    return false;
                } else {
                    i6 = ((i6 * 10) + charAt) - 48;
                    if (i6 > 255) {
                        return false;
                    }
                    i7++;
                }
            }
            if (i7 - i4 == 0) {
                return false;
            }
            bArr[i5] = (byte) i6;
            i5++;
            i4 = i7;
        }
        return i5 == i3 + 4;
    }

    /* JADX WARNING: Removed duplicated region for block: B:2:0x0003  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int b(java.lang.String r3, int r4, int r5) {
        /*
            r2 = this;
            r0 = r4
        L_0x0001:
            if (r0 >= r5) goto L_0x000b
            char r1 = r3.charAt(r0)
            switch(r1) {
                case 9: goto L_0x000c;
                case 10: goto L_0x000c;
                case 12: goto L_0x000c;
                case 13: goto L_0x000c;
                case 32: goto L_0x000c;
                default: goto L_0x000a;
            }
        L_0x000a:
            r5 = r0
        L_0x000b:
            return r5
        L_0x000c:
            int r0 = r0 + 1
            goto L_0x0001
        */
        throw new UnsupportedOperationException("Method not decompiled: com.e.a.ai.b(java.lang.String, int, int):int");
    }

    private boolean b(String str) {
        return str.equals(".") || str.equalsIgnoreCase("%2e");
    }

    /* JADX WARNING: Removed duplicated region for block: B:2:0x0004  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int c(java.lang.String r3, int r4, int r5) {
        /*
            r2 = this;
            int r0 = r5 + -1
        L_0x0002:
            if (r0 < r4) goto L_0x000d
            char r1 = r3.charAt(r0)
            switch(r1) {
                case 9: goto L_0x000e;
                case 10: goto L_0x000e;
                case 12: goto L_0x000e;
                case 13: goto L_0x000e;
                case 32: goto L_0x000e;
                default: goto L_0x000b;
            }
        L_0x000b:
            int r4 = r0 + 1
        L_0x000d:
            return r4
        L_0x000e:
            int r0 = r0 + -1
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: com.e.a.ai.c(java.lang.String, int, int):int");
    }

    private void c() {
        if (!this.f.remove(this.f.size() - 1).isEmpty() || this.f.isEmpty()) {
            this.f.add("");
        } else {
            this.f.set(this.f.size() - 1, "");
        }
    }

    private boolean c(String str) {
        return str.equals("..") || str.equalsIgnoreCase("%2e.") || str.equalsIgnoreCase(".%2e") || str.equalsIgnoreCase("%2e%2e");
    }

    private static int d(String str, int i, int i2) {
        if (i2 - i < 2) {
            return -1;
        }
        char charAt = str.charAt(i);
        if ((charAt < 'a' || charAt > 'z') && (charAt < 'A' || charAt > 'Z')) {
            return -1;
        }
        int i3 = i + 1;
        while (i3 < i2) {
            char charAt2 = str.charAt(i3);
            if ((charAt2 >= 'a' && charAt2 <= 'z') || ((charAt2 >= 'A' && charAt2 <= 'Z') || charAt2 == '+' || charAt2 == '-' || charAt2 == '.')) {
                i3++;
            } else if (charAt2 == ':') {
                return i3;
            } else {
                return -1;
            }
        }
        return -1;
    }

    private static String d(String str) {
        try {
            String lowerCase = IDN.toASCII(str).toLowerCase(Locale.US);
            if (lowerCase.isEmpty()) {
                return null;
            }
            return lowerCase;
        } catch (IllegalArgumentException e2) {
            return null;
        }
    }

    private static int e(String str, int i, int i2) {
        int i3 = 0;
        while (i < i2) {
            char charAt = str.charAt(i);
            if (charAt != '\\' && charAt != '/') {
                break;
            }
            i3++;
            i++;
        }
        return i3;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private static int f(String str, int i, int i2) {
        int i3 = i;
        while (i3 < i2) {
            switch (str.charAt(i3)) {
                case ':':
                    return i3;
                case '[':
                    break;
                default:
                    i3++;
            }
            do {
                i3++;
                if (i3 < i2) {
                }
                i3++;
            } while (str.charAt(i3) != ']');
            i3++;
        }
        return i2;
    }

    private static String g(String str, int i, int i2) {
        String a2 = ag.a(str, i, i2);
        if (!a2.startsWith("[") || !a2.endsWith("]")) {
            String d2 = d(a2);
            if (d2 == null) {
                return null;
            }
            int length = d2.length();
            if (ag.b(d2, 0, length, "\u0000\t\n\r #%/:?@[\\]") == length) {
                return d2;
            }
            return null;
        }
        InetAddress h2 = h(a2, 1, a2.length() - 1);
        if (h2 != null) {
            return h2.getHostAddress();
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.net.InetAddress h(java.lang.String r12, int r13, int r14) {
        /*
            r11 = 1
            r7 = -1
            r3 = 0
            r5 = 0
            r0 = 16
            byte[] r8 = new byte[r0]
            r0 = r13
            r4 = r7
            r1 = r7
            r2 = r5
        L_0x000c:
            if (r0 >= r14) goto L_0x002b
            int r6 = r8.length
            if (r2 != r6) goto L_0x0013
            r0 = r3
        L_0x0012:
            return r0
        L_0x0013:
            int r6 = r0 + 2
            if (r6 > r14) goto L_0x0032
            java.lang.String r6 = "::"
            r9 = 2
            boolean r6 = r12.regionMatches(r0, r6, r5, r9)
            if (r6 == 0) goto L_0x0032
            if (r1 == r7) goto L_0x0024
            r0 = r3
            goto L_0x0012
        L_0x0024:
            int r0 = r0 + 2
            int r1 = r2 + 2
            if (r0 != r14) goto L_0x00a1
            r2 = r1
        L_0x002b:
            int r0 = r8.length
            if (r2 == r0) goto L_0x0094
            if (r1 != r7) goto L_0x0085
            r0 = r3
            goto L_0x0012
        L_0x0032:
            if (r2 == 0) goto L_0x003e
            java.lang.String r6 = ":"
            boolean r6 = r12.regionMatches(r0, r6, r5, r11)
            if (r6 == 0) goto L_0x0055
            int r0 = r0 + 1
        L_0x003e:
            r4 = r5
            r6 = r0
        L_0x0040:
            if (r6 >= r14) goto L_0x004c
            char r9 = r12.charAt(r6)
            int r9 = com.e.a.ag.a(r9)
            if (r9 != r7) goto L_0x006c
        L_0x004c:
            int r9 = r6 - r0
            if (r9 == 0) goto L_0x0053
            r10 = 4
            if (r9 <= r10) goto L_0x0072
        L_0x0053:
            r0 = r3
            goto L_0x0012
        L_0x0055:
            java.lang.String r6 = "."
            boolean r0 = r12.regionMatches(r0, r6, r5, r11)
            if (r0 == 0) goto L_0x006a
            int r0 = r2 + -2
            boolean r0 = a(r12, r4, r14, r8, r0)
            if (r0 != 0) goto L_0x0067
            r0 = r3
            goto L_0x0012
        L_0x0067:
            int r2 = r2 + 2
            goto L_0x002b
        L_0x006a:
            r0 = r3
            goto L_0x0012
        L_0x006c:
            int r4 = r4 << 4
            int r4 = r4 + r9
            int r6 = r6 + 1
            goto L_0x0040
        L_0x0072:
            int r9 = r2 + 1
            int r10 = r4 >>> 8
            r10 = r10 & 255(0xff, float:3.57E-43)
            byte r10 = (byte) r10
            r8[r2] = r10
            int r2 = r9 + 1
            r4 = r4 & 255(0xff, float:3.57E-43)
            byte r4 = (byte) r4
            r8[r9] = r4
            r4 = r0
            r0 = r6
            goto L_0x000c
        L_0x0085:
            int r0 = r8.length
            int r3 = r2 - r1
            int r0 = r0 - r3
            int r3 = r2 - r1
            java.lang.System.arraycopy(r8, r1, r8, r0, r3)
            int r0 = r8.length
            int r0 = r0 - r2
            int r0 = r0 + r1
            java.util.Arrays.fill(r8, r1, r0, r5)
        L_0x0094:
            java.net.InetAddress r0 = java.net.InetAddress.getByAddress(r8)     // Catch:{ UnknownHostException -> 0x009a }
            goto L_0x0012
        L_0x009a:
            r0 = move-exception
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        L_0x00a1:
            r2 = r1
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.e.a.ai.h(java.lang.String, int, int):java.net.InetAddress");
    }

    private static int i(String str, int i, int i2) {
        try {
            int parseInt = Integer.parseInt(ag.a(str, i, i2, "", false, false));
            if (parseInt <= 0 || parseInt > 65535) {
                return -1;
            }
            return parseInt;
        } catch (NumberFormatException e2) {
            return -1;
        }
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.e != -1 ? this.e : ag.a(this.f708a);
    }

    /* access modifiers changed from: package-private */
    public ag a(ag agVar, String str) {
        int i;
        int b2 = b(str, 0, str.length());
        int c2 = c(str, b2, str.length());
        if (d(str, b2, c2) != -1) {
            if (str.regionMatches(true, b2, "https:", 0, 6)) {
                this.f708a = "https";
                b2 += "https:".length();
            } else if (!str.regionMatches(true, b2, "http:", 0, 5)) {
                return null;
            } else {
                this.f708a = "http";
                b2 += "http:".length();
            }
        } else if (agVar == null) {
            return null;
        } else {
            this.f708a = agVar.f707b;
        }
        boolean z = false;
        boolean z2 = false;
        int e2 = e(str, b2, c2);
        if (e2 >= 2 || agVar == null || !agVar.f707b.equals(this.f708a)) {
            int i2 = b2 + e2;
            while (true) {
                boolean z3 = z2;
                boolean z4 = z;
                int i3 = i2;
                int a2 = ag.b(str, i3, c2, "@/\\?#");
                switch (a2 != c2 ? str.charAt(a2) : 65535) {
                    case 65535:
                    case '#':
                    case '/':
                    case '?':
                    case '\\':
                        int f2 = f(str, i3, a2);
                        if (f2 + 1 < a2) {
                            this.d = g(str, i3, f2);
                            this.e = i(str, f2 + 1, a2);
                            if (this.e == -1) {
                                return null;
                            }
                        } else {
                            this.d = g(str, i3, f2);
                            this.e = ag.a(this.f708a);
                        }
                        if (this.d != null) {
                            b2 = a2;
                            break;
                        } else {
                            return null;
                        }
                    case '@':
                        if (!z3) {
                            int a3 = ag.b(str, i3, a2, ":");
                            String a4 = ag.a(str, i3, a3, " \"':;<=>@[]^`{}|/\\?#", true, false);
                            if (z4) {
                                a4 = this.f709b + "%40" + a4;
                            }
                            this.f709b = a4;
                            if (a3 != a2) {
                                z3 = true;
                                this.c = ag.a(str, a3 + 1, a2, " \"':;<=>@[]\\^`{}|/\\?#", true, false);
                            }
                            z4 = true;
                        } else {
                            this.c += "%40" + ag.a(str, i3, a2, " \"':;<=>@[]\\^`{}|/\\?#", true, false);
                        }
                        i2 = a2 + 1;
                        z2 = z3;
                        break;
                    default:
                        z2 = z3;
                        i2 = i3;
                        break;
                }
                z = z4;
            }
        } else {
            this.f709b = agVar.d();
            this.c = agVar.e();
            this.d = agVar.e;
            this.e = agVar.f;
            this.f.clear();
            this.f.addAll(agVar.f());
            if (b2 == c2 || str.charAt(b2) == '#') {
                a(agVar.g());
            }
        }
        int a5 = ag.b(str, b2, c2, "?#");
        a(str, b2, a5);
        if (a5 >= c2 || str.charAt(a5) != '?') {
            i = a5;
        } else {
            i = ag.b(str, a5, c2, "#");
            this.g = ag.b(ag.a(str, a5 + 1, i, " \"'<>#", true, true));
        }
        if (i < c2 && str.charAt(i) == '#') {
            this.h = ag.a(str, i + 1, c2, "", true, false);
        }
        return b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.ag.a(java.lang.String, java.lang.String, boolean, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      com.e.a.ag.a(java.lang.String, int, int, java.lang.String):int
      com.e.a.ag.a(a.f, java.lang.String, int, int):void
      com.e.a.ag.a(java.lang.String, java.lang.String, boolean, boolean):java.lang.String */
    public ai a(String str) {
        this.g = str != null ? ag.b(ag.a(str, " \"'<>#", true, true)) : null;
        return this;
    }

    public ag b() {
        if (this.f708a == null) {
            throw new IllegalStateException("scheme == null");
        } else if (this.d != null) {
            return new ag(this);
        } else {
            throw new IllegalStateException("host == null");
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f708a);
        sb.append("://");
        if (!this.f709b.isEmpty() || !this.c.isEmpty()) {
            sb.append(this.f709b);
            if (!this.c.isEmpty()) {
                sb.append(':');
                sb.append(this.c);
            }
            sb.append('@');
        }
        if (this.d.indexOf(58) != -1) {
            sb.append('[');
            sb.append(this.d);
            sb.append(']');
        } else {
            sb.append(this.d);
        }
        int a2 = a();
        if (a2 != ag.a(this.f708a)) {
            sb.append(':');
            sb.append(a2);
        }
        ag.a(sb, this.f);
        if (this.g != null) {
            sb.append('?');
            ag.b(sb, this.g);
        }
        if (this.h != null) {
            sb.append('#');
            sb.append(this.h);
        }
        return sb.toString();
    }
}
