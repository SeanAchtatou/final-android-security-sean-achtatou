package com.e.a;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.List;

public final class ap {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final ag f716a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final String f717b;
    /* access modifiers changed from: private */
    public final ad c;
    /* access modifiers changed from: private */
    public final as d;
    /* access modifiers changed from: private */
    public final Object e;
    private volatile URL f;
    private volatile URI g;
    private volatile j h;

    private ap(ar arVar) {
        this.f716a = arVar.f718a;
        this.f717b = arVar.f719b;
        this.c = arVar.c.a();
        this.d = arVar.d;
        this.e = arVar.e != null ? arVar.e : this;
    }

    public String a(String str) {
        return this.c.a(str);
    }

    public URL a() {
        URL url = this.f;
        if (url != null) {
            return url;
        }
        URL a2 = this.f716a.a();
        this.f = a2;
        return a2;
    }

    public URI b() {
        try {
            URI uri = this.g;
            if (uri != null) {
                return uri;
            }
            URI b2 = this.f716a.b();
            this.g = b2;
            return b2;
        } catch (IllegalStateException e2) {
            throw new IOException(e2.getMessage());
        }
    }

    public List<String> b(String str) {
        return this.c.c(str);
    }

    public String c() {
        return this.f716a.toString();
    }

    public String d() {
        return this.f717b;
    }

    public ad e() {
        return this.c;
    }

    public as f() {
        return this.d;
    }

    public ar g() {
        return new ar(this);
    }

    public j h() {
        j jVar = this.h;
        if (jVar != null) {
            return jVar;
        }
        j a2 = j.a(this.c);
        this.h = a2;
        return a2;
    }

    public boolean i() {
        return this.f716a.c();
    }

    public String toString() {
        return "Request{method=" + this.f717b + ", url=" + this.f716a + ", tag=" + (this.e != this ? this.e : null) + '}';
    }
}
