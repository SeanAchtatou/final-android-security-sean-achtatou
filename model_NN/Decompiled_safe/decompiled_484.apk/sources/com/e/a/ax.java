package com.e.a;

import a.i;
import com.e.a.a.v;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

public abstract class ax implements Closeable {
    private Charset g() {
        al a2 = a();
        return a2 != null ? a2.a(v.c) : v.c;
    }

    public abstract al a();

    public abstract long b();

    public abstract i c();

    public void close() {
        c().close();
    }

    public final InputStream d() {
        return c().g();
    }

    /* JADX INFO: finally extract failed */
    public final byte[] e() {
        long b2 = b();
        if (b2 > 2147483647L) {
            throw new IOException("Cannot buffer entire body for content length: " + b2);
        }
        i c = c();
        try {
            byte[] s = c.s();
            v.a(c);
            if (b2 == -1 || b2 == ((long) s.length)) {
                return s;
            }
            throw new IOException("Content-Length and stream length disagree");
        } catch (Throwable th) {
            v.a(c);
            throw th;
        }
    }

    public final String f() {
        return new String(e(), g().name());
    }
}
