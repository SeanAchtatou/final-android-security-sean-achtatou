package com.e.a;

import com.e.a.a.v;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLSocket;

public final class w {

    /* renamed from: a  reason: collision with root package name */
    public static final w f763a = new y(true).a(f).a(az.TLS_1_2, az.TLS_1_1, az.TLS_1_0).a(true).a();

    /* renamed from: b  reason: collision with root package name */
    public static final w f764b = new y(f763a).a(az.TLS_1_0).a(true).a();
    public static final w c = new y(false).a();
    private static final s[] f = {s.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256, s.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256, s.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256, s.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA, s.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA, s.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA, s.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA, s.TLS_DHE_RSA_WITH_AES_128_CBC_SHA, s.TLS_DHE_DSS_WITH_AES_128_CBC_SHA, s.TLS_DHE_RSA_WITH_AES_256_CBC_SHA, s.TLS_RSA_WITH_AES_128_GCM_SHA256, s.TLS_RSA_WITH_AES_128_CBC_SHA, s.TLS_RSA_WITH_AES_256_CBC_SHA, s.TLS_RSA_WITH_3DES_EDE_CBC_SHA};
    final boolean d;
    final boolean e;
    /* access modifiers changed from: private */
    public final String[] g;
    /* access modifiers changed from: private */
    public final String[] h;

    private w(y yVar) {
        this.d = yVar.f765a;
        this.g = yVar.f766b;
        this.h = yVar.c;
        this.e = yVar.d;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.a.a.v.a(java.lang.Object, java.lang.Object):boolean
     arg types: [T, T]
     candidates:
      com.e.a.a.v.a(java.lang.String, int):int
      com.e.a.a.v.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
      com.e.a.a.v.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
      com.e.a.a.v.a(java.io.Closeable, java.io.Closeable):void
      com.e.a.a.v.a(java.lang.Object, java.lang.Object):boolean */
    private static <T> boolean a(T[] tArr, T t) {
        for (T t2 : tArr) {
            if (v.a((Object) t, (Object) t2)) {
                return true;
            }
        }
        return false;
    }

    private static boolean a(String[] strArr, String[] strArr2) {
        if (strArr == null || strArr2 == null || strArr.length == 0 || strArr2.length == 0) {
            return false;
        }
        for (String a2 : strArr) {
            if (a(strArr2, a2)) {
                return true;
            }
        }
        return false;
    }

    private w b(SSLSocket sSLSocket, boolean z) {
        String[] strArr;
        String[] strArr2 = null;
        if (this.g != null) {
            strArr2 = (String[]) v.a(String.class, this.g, sSLSocket.getEnabledCipherSuites());
        }
        if (!z || !Arrays.asList(sSLSocket.getSupportedCipherSuites()).contains("TLS_FALLBACK_SCSV")) {
            strArr = strArr2;
        } else {
            if (strArr2 == null) {
                strArr2 = sSLSocket.getEnabledCipherSuites();
            }
            strArr = new String[(strArr2.length + 1)];
            System.arraycopy(strArr2, 0, strArr, 0, strArr2.length);
            strArr[strArr.length - 1] = "TLS_FALLBACK_SCSV";
        }
        String[] enabledProtocols = sSLSocket.getEnabledProtocols();
        return new y(this).a(strArr).b((String[]) v.a(String.class, this.h, enabledProtocols)).a();
    }

    public List<s> a() {
        if (this.g == null) {
            return null;
        }
        s[] sVarArr = new s[this.g.length];
        for (int i = 0; i < this.g.length; i++) {
            sVarArr[i] = s.a(this.g[i]);
        }
        return v.a(sVarArr);
    }

    /* access modifiers changed from: package-private */
    public void a(SSLSocket sSLSocket, boolean z) {
        w b2 = b(sSLSocket, z);
        sSLSocket.setEnabledProtocols(b2.h);
        String[] strArr = b2.g;
        if (strArr != null) {
            sSLSocket.setEnabledCipherSuites(strArr);
        }
    }

    public boolean a(SSLSocket sSLSocket) {
        if (!this.d) {
            return false;
        }
        if (!a(this.h, sSLSocket.getEnabledProtocols())) {
            return false;
        }
        if (this.g == null) {
            return sSLSocket.getEnabledCipherSuites().length > 0;
        }
        return a(this.g, sSLSocket.getEnabledCipherSuites());
    }

    public List<az> b() {
        az[] azVarArr = new az[this.h.length];
        for (int i = 0; i < this.h.length; i++) {
            azVarArr[i] = az.a(this.h[i]);
        }
        return v.a(azVarArr);
    }

    public boolean c() {
        return this.e;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof w)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        w wVar = (w) obj;
        if (this.d == wVar.d) {
            return !this.d || (Arrays.equals(this.g, wVar.g) && Arrays.equals(this.h, wVar.h) && this.e == wVar.e);
        }
        return false;
    }

    public int hashCode() {
        if (!this.d) {
            return 17;
        }
        return (this.e ? 0 : 1) + ((((Arrays.hashCode(this.g) + 527) * 31) + Arrays.hashCode(this.h)) * 31);
    }

    public String toString() {
        if (!this.d) {
            return "ConnectionSpec()";
        }
        List<s> a2 = a();
        return "ConnectionSpec(cipherSuites=" + (a2 == null ? "[use default]" : a2.toString()) + ", tlsVersions=" + b() + ", supportsTlsExtensions=" + this.e + ")";
    }
}
