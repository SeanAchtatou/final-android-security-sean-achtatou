package com.e.a;

public final class y {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public boolean f765a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public String[] f766b;
    /* access modifiers changed from: private */
    public String[] c;
    /* access modifiers changed from: private */
    public boolean d;

    public y(w wVar) {
        this.f765a = wVar.d;
        this.f766b = wVar.g;
        this.c = wVar.h;
        this.d = wVar.e;
    }

    y(boolean z) {
        this.f765a = z;
    }

    public w a() {
        return new w(this);
    }

    public y a(boolean z) {
        if (!this.f765a) {
            throw new IllegalStateException("no TLS extensions for cleartext connections");
        }
        this.d = z;
        return this;
    }

    public y a(az... azVarArr) {
        if (!this.f765a) {
            throw new IllegalStateException("no TLS versions for cleartext connections");
        } else if (azVarArr.length == 0) {
            throw new IllegalArgumentException("At least one TlsVersion is required");
        } else {
            String[] strArr = new String[azVarArr.length];
            for (int i = 0; i < azVarArr.length; i++) {
                strArr[i] = azVarArr[i].e;
            }
            this.c = strArr;
            return this;
        }
    }

    public y a(s... sVarArr) {
        if (!this.f765a) {
            throw new IllegalStateException("no cipher suites for cleartext connections");
        }
        String[] strArr = new String[sVarArr.length];
        for (int i = 0; i < sVarArr.length; i++) {
            strArr[i] = sVarArr[i].aS;
        }
        this.f766b = strArr;
        return this;
    }

    public y a(String... strArr) {
        if (!this.f765a) {
            throw new IllegalStateException("no cipher suites for cleartext connections");
        }
        if (strArr == null) {
            this.f766b = null;
        } else {
            this.f766b = (String[]) strArr.clone();
        }
        return this;
    }

    public y b(String... strArr) {
        if (!this.f765a) {
            throw new IllegalStateException("no TLS versions for cleartext connections");
        }
        if (strArr == null) {
            this.c = null;
        } else {
            this.c = (String[]) strArr.clone();
        }
        return this;
    }
}
