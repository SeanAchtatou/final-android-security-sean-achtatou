package com.e.b;

import android.graphics.BitmapFactory;
import android.net.NetworkInfo;

public abstract class bc {
    static void a(int i, int i2, int i3, int i4, BitmapFactory.Options options, ay ayVar) {
        int i5 = 1;
        if (i4 > i2 || i3 > i) {
            if (i2 == 0) {
                i5 = (int) Math.floor((double) (((float) i3) / ((float) i)));
            } else if (i == 0) {
                i5 = (int) Math.floor((double) (((float) i4) / ((float) i2)));
            } else {
                int floor = (int) Math.floor((double) (((float) i4) / ((float) i2)));
                int floor2 = (int) Math.floor((double) (((float) i3) / ((float) i)));
                i5 = ayVar.k ? Math.max(floor, floor2) : Math.min(floor, floor2);
            }
        }
        options.inSampleSize = i5;
        options.inJustDecodeBounds = false;
    }

    static void a(int i, int i2, BitmapFactory.Options options, ay ayVar) {
        a(i, i2, options.outWidth, options.outHeight, options, ayVar);
    }

    static boolean a(BitmapFactory.Options options) {
        return options != null && options.inJustDecodeBounds;
    }

    static BitmapFactory.Options c(ay ayVar) {
        boolean d = ayVar.d();
        boolean z = ayVar.q != null;
        BitmapFactory.Options options = null;
        if (d || z) {
            options = new BitmapFactory.Options();
            options.inJustDecodeBounds = d;
            if (z) {
                options.inPreferredConfig = ayVar.q;
            }
        }
        return options;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return 0;
    }

    public abstract bd a(ay ayVar, int i);

    public abstract boolean a(ay ayVar);

    /* access modifiers changed from: package-private */
    public boolean a(boolean z, NetworkInfo networkInfo) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return false;
    }
}
