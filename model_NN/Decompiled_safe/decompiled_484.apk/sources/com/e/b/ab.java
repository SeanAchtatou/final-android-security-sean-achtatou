package com.e.b;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

class ab extends a<ImageView> {
    m m;

    ab(al alVar, ImageView imageView, ay ayVar, int i, int i2, int i3, Drawable drawable, String str, Object obj, m mVar, boolean z) {
        super(alVar, imageView, ayVar, i, i2, i3, drawable, str, obj, z);
        this.m = mVar;
    }

    public void a() {
        ImageView imageView = (ImageView) this.c.get();
        if (imageView != null) {
            if (this.g != 0) {
                imageView.setImageResource(this.g);
            } else if (this.h != null) {
                imageView.setImageDrawable(this.h);
            }
            if (this.m != null) {
                this.m.onError();
            }
        }
    }

    public void a(Bitmap bitmap, ar arVar) {
        if (bitmap == null) {
            throw new AssertionError(String.format("Attempted to complete action with no result!\n%s", this));
        }
        ImageView imageView = (ImageView) this.c.get();
        if (imageView != null) {
            Bitmap bitmap2 = bitmap;
            ar arVar2 = arVar;
            av.a(imageView, this.f767a.c, bitmap2, arVar2, this.d, this.f767a.k);
            if (this.m != null) {
                this.m.onSuccess();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        super.b();
        if (this.m != null) {
            this.m = null;
        }
    }
}
