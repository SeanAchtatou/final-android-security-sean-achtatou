package com.e.b;

import android.content.Context;
import android.net.Uri;
import com.e.a.am;
import com.e.a.ar;
import com.e.a.au;
import com.e.a.ax;
import com.e.a.c;
import com.e.a.j;
import com.e.a.l;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class ak implements w {

    /* renamed from: a  reason: collision with root package name */
    private final am f781a;

    public ak(Context context) {
        this(bn.b(context));
    }

    public ak(am amVar) {
        this.f781a = amVar;
    }

    public ak(File file) {
        this(file, bn.a(file));
    }

    public ak(File file, long j) {
        this(a());
        try {
            this.f781a.a(new c(file, j));
        } catch (IOException e) {
        }
    }

    private static am a() {
        am amVar = new am();
        amVar.a(15000, TimeUnit.MILLISECONDS);
        amVar.b(20000, TimeUnit.MILLISECONDS);
        amVar.c(20000, TimeUnit.MILLISECONDS);
        return amVar;
    }

    public x a(Uri uri, int i) {
        j jVar = null;
        if (i != 0) {
            if (ah.c(i)) {
                jVar = j.f744b;
            } else {
                l lVar = new l();
                if (!ah.a(i)) {
                    lVar.a();
                }
                if (!ah.b(i)) {
                    lVar.b();
                }
                jVar = lVar.d();
            }
        }
        ar a2 = new ar().a(uri.toString());
        if (jVar != null) {
            a2.a(jVar);
        }
        au a3 = this.f781a.a(a2.a()).a();
        int c = a3.c();
        if (c >= 300) {
            a3.h().close();
            throw new y(c + " " + a3.e(), i, c);
        }
        boolean z = a3.k() != null;
        ax h = a3.h();
        return new x(h.d(), z, h.b());
    }
}
