package com.e.b;

import android.support.v4.internal.view.SupportMenu;

public enum ar {
    MEMORY(-16711936),
    DISK(-16776961),
    NETWORK(SupportMenu.CATEGORY_MASK);
    
    final int d;

    private ar(int i) {
        this.d = i;
    }
}
