package com.e.b;

import android.graphics.Bitmap;
import android.net.Uri;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public final class ay {
    private static final long s = TimeUnit.SECONDS.toNanos(5);

    /* renamed from: a  reason: collision with root package name */
    int f798a;

    /* renamed from: b  reason: collision with root package name */
    long f799b;
    int c;
    public final Uri d;
    public final int e;
    public final String f;
    public final List<bj> g;
    public final int h;
    public final int i;
    public final boolean j;
    public final boolean k;
    public final boolean l;
    public final float m;
    public final float n;
    public final float o;
    public final boolean p;
    public final Bitmap.Config q;
    public final as r;

    private ay(Uri uri, int i2, String str, List<bj> list, int i3, int i4, boolean z, boolean z2, boolean z3, float f2, float f3, float f4, boolean z4, Bitmap.Config config, as asVar) {
        this.d = uri;
        this.e = i2;
        this.f = str;
        if (list == null) {
            this.g = null;
        } else {
            this.g = Collections.unmodifiableList(list);
        }
        this.h = i3;
        this.i = i4;
        this.j = z;
        this.k = z2;
        this.l = z3;
        this.m = f2;
        this.n = f3;
        this.o = f4;
        this.p = z4;
        this.q = config;
        this.r = asVar;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        long nanoTime = System.nanoTime() - this.f799b;
        return nanoTime > s ? b() + '+' + TimeUnit.NANOSECONDS.toSeconds(nanoTime) + 's' : b() + '+' + TimeUnit.NANOSECONDS.toMillis(nanoTime) + "ms";
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return "[R" + this.f798a + ']';
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.d != null ? String.valueOf(this.d.getPath()) : Integer.toHexString(this.e);
    }

    public boolean d() {
        return (this.h == 0 && this.i == 0) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return f() || g();
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return d() || this.m != 0.0f;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return this.g != null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Request{");
        if (this.e > 0) {
            sb.append(this.e);
        } else {
            sb.append(this.d);
        }
        if (this.g != null && !this.g.isEmpty()) {
            for (bj a2 : this.g) {
                sb.append(' ').append(a2.a());
            }
        }
        if (this.f != null) {
            sb.append(" stableKey(").append(this.f).append(')');
        }
        if (this.h > 0) {
            sb.append(" resize(").append(this.h).append(',').append(this.i).append(')');
        }
        if (this.j) {
            sb.append(" centerCrop");
        }
        if (this.k) {
            sb.append(" centerInside");
        }
        if (this.m != 0.0f) {
            sb.append(" rotation(").append(this.m);
            if (this.p) {
                sb.append(" @ ").append(this.n).append(',').append(this.o);
            }
            sb.append(')');
        }
        if (this.q != null) {
            sb.append(' ').append(this.q);
        }
        sb.append('}');
        return sb.toString();
    }
}
