package com.e.b;

import android.content.Context;
import java.io.InputStream;

class p extends bc {

    /* renamed from: a  reason: collision with root package name */
    final Context f830a;

    p(Context context) {
        this.f830a = context;
    }

    public bd a(ay ayVar, int i) {
        return new bd(b(ayVar), ar.DISK);
    }

    public boolean a(ay ayVar) {
        return "content".equals(ayVar.d.getScheme());
    }

    /* access modifiers changed from: package-private */
    public InputStream b(ay ayVar) {
        return this.f830a.getContentResolver().openInputStream(ayVar.d);
    }
}
