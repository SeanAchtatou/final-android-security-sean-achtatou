package com.e.b;

final class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bj f825a;

    i(bj bjVar) {
        this.f825a = bjVar;
    }

    public void run() {
        throw new IllegalStateException("Transformation " + this.f825a.a() + " returned input Bitmap but recycled it.");
    }
}
