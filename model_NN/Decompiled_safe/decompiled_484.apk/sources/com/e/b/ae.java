package com.e.b;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;

class ae extends p {

    /* renamed from: b  reason: collision with root package name */
    private static final String[] f772b = {"orientation"};

    ae(Context context) {
        super(context);
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0034  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static int a(android.content.ContentResolver r8, android.net.Uri r9) {
        /*
            r6 = 0
            r7 = 0
            java.lang.String[] r2 = com.e.b.ae.f772b     // Catch:{ RuntimeException -> 0x0027, all -> 0x0030 }
            r3 = 0
            r4 = 0
            r5 = 0
            r0 = r8
            r1 = r9
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ RuntimeException -> 0x0027, all -> 0x0030 }
            if (r1 == 0) goto L_0x0015
            boolean r0 = r1.moveToFirst()     // Catch:{ RuntimeException -> 0x003a, all -> 0x0038 }
            if (r0 != 0) goto L_0x001c
        L_0x0015:
            if (r1 == 0) goto L_0x001a
            r1.close()
        L_0x001a:
            r0 = r6
        L_0x001b:
            return r0
        L_0x001c:
            r0 = 0
            int r0 = r1.getInt(r0)     // Catch:{ RuntimeException -> 0x003a, all -> 0x0038 }
            if (r1 == 0) goto L_0x001b
            r1.close()
            goto L_0x001b
        L_0x0027:
            r0 = move-exception
            r0 = r7
        L_0x0029:
            if (r0 == 0) goto L_0x002e
            r0.close()
        L_0x002e:
            r0 = r6
            goto L_0x001b
        L_0x0030:
            r0 = move-exception
            r1 = r7
        L_0x0032:
            if (r1 == 0) goto L_0x0037
            r1.close()
        L_0x0037:
            throw r0
        L_0x0038:
            r0 = move-exception
            goto L_0x0032
        L_0x003a:
            r0 = move-exception
            r0 = r1
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.e.b.ae.a(android.content.ContentResolver, android.net.Uri):int");
    }

    static af a(int i, int i2) {
        return (i > af.MICRO.e || i2 > af.MICRO.f) ? (i > af.MINI.e || i2 > af.MINI.f) ? af.FULL : af.MINI : af.MICRO;
    }

    public bd a(ay ayVar, int i) {
        Bitmap thumbnail;
        ContentResolver contentResolver = this.f830a.getContentResolver();
        int a2 = a(contentResolver, ayVar.d);
        String type = contentResolver.getType(ayVar.d);
        boolean z = type != null && type.startsWith("video/");
        if (ayVar.d()) {
            af a3 = a(ayVar.h, ayVar.i);
            if (!z && a3 == af.FULL) {
                return new bd(null, b(ayVar), ar.DISK, a2);
            }
            long parseId = ContentUris.parseId(ayVar.d);
            BitmapFactory.Options c = c(ayVar);
            c.inJustDecodeBounds = true;
            a(ayVar.h, ayVar.i, a3.e, a3.f, c, ayVar);
            if (z) {
                thumbnail = MediaStore.Video.Thumbnails.getThumbnail(contentResolver, parseId, a3 == af.FULL ? 1 : a3.d, c);
            } else {
                thumbnail = MediaStore.Images.Thumbnails.getThumbnail(contentResolver, parseId, a3.d, c);
            }
            if (thumbnail != null) {
                return new bd(thumbnail, null, ar.DISK, a2);
            }
        }
        return new bd(null, b(ayVar), ar.DISK, a2);
    }

    public boolean a(ay ayVar) {
        Uri uri = ayVar.d;
        return "content".equals(uri.getScheme()) && "media".equals(uri.getAuthority());
    }
}
