package com.e.b;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class bk implements w {

    /* renamed from: a  reason: collision with root package name */
    static volatile Object f815a;

    /* renamed from: b  reason: collision with root package name */
    private static final Object f816b = new Object();
    private static final ThreadLocal<StringBuilder> c = new bl();
    private final Context d;

    public bk(Context context) {
        this.d = context.getApplicationContext();
    }

    private static void a(Context context) {
        if (f815a == null) {
            try {
                synchronized (f816b) {
                    if (f815a == null) {
                        f815a = bm.a(context);
                    }
                }
            } catch (IOException e) {
            }
        }
    }

    public x a(Uri uri, int i) {
        String sb;
        if (Build.VERSION.SDK_INT >= 14) {
            a(this.d);
        }
        HttpURLConnection a2 = a(uri);
        a2.setUseCaches(true);
        if (i != 0) {
            if (ah.c(i)) {
                sb = "only-if-cached,max-age=2147483647";
            } else {
                StringBuilder sb2 = c.get();
                sb2.setLength(0);
                if (!ah.a(i)) {
                    sb2.append("no-cache");
                }
                if (!ah.b(i)) {
                    if (sb2.length() > 0) {
                        sb2.append(',');
                    }
                    sb2.append("no-store");
                }
                sb = sb2.toString();
            }
            a2.setRequestProperty("Cache-Control", sb);
        }
        int responseCode = a2.getResponseCode();
        if (responseCode >= 300) {
            a2.disconnect();
            throw new y(responseCode + " " + a2.getResponseMessage(), i, responseCode);
        }
        long headerFieldInt = (long) a2.getHeaderFieldInt("Content-Length", -1);
        return new x(a2.getInputStream(), bn.a(a2.getHeaderField("X-Android-Response-Source")), headerFieldInt);
    }

    /* access modifiers changed from: protected */
    public HttpURLConnection a(Uri uri) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(uri.toString()).openConnection();
        httpURLConnection.setConnectTimeout(15000);
        httpURLConnection.setReadTimeout(20000);
        return httpURLConnection;
    }
}
