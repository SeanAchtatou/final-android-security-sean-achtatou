package com.e.b;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

class bg extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private final bf f810a;

    public bg(Looper looper, bf bfVar) {
        super(looper);
        this.f810a = bfVar;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                this.f810a.c();
                return;
            case 1:
                this.f810a.d();
                return;
            case 2:
                this.f810a.b((long) message.arg1);
                return;
            case 3:
                this.f810a.c((long) message.arg1);
                return;
            case 4:
                this.f810a.a((Long) message.obj);
                return;
            default:
                al.f782a.post(new bh(this, message));
                return;
        }
    }
}
