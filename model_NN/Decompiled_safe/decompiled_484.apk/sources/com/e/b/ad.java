package com.e.b;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

final class ad extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    private final InputStream f770a;

    /* renamed from: b  reason: collision with root package name */
    private long f771b;
    private long c;
    private long d;
    private long e;

    public ad(InputStream inputStream) {
        this(inputStream, 4096);
    }

    public ad(InputStream inputStream, int i) {
        this.e = -1;
        this.f770a = !inputStream.markSupported() ? new BufferedInputStream(inputStream, i) : inputStream;
    }

    private void a(long j, long j2) {
        while (j < j2) {
            long skip = this.f770a.skip(j2 - j);
            if (skip == 0) {
                if (read() != -1) {
                    skip = 1;
                } else {
                    return;
                }
            }
            j += skip;
        }
    }

    private void b(long j) {
        try {
            if (this.c >= this.f771b || this.f771b > this.d) {
                this.c = this.f771b;
                this.f770a.mark((int) (j - this.f771b));
            } else {
                this.f770a.reset();
                this.f770a.mark((int) (j - this.c));
                a(this.c, this.f771b);
            }
            this.d = j;
        } catch (IOException e2) {
            throw new IllegalStateException("Unable to mark: " + e2);
        }
    }

    public long a(int i) {
        long j = this.f771b + ((long) i);
        if (this.d < j) {
            b(j);
        }
        return this.f771b;
    }

    public void a(long j) {
        if (this.f771b > this.d || j < this.c) {
            throw new IOException("Cannot reset");
        }
        this.f770a.reset();
        a(this.c, j);
        this.f771b = j;
    }

    public int available() {
        return this.f770a.available();
    }

    public void close() {
        this.f770a.close();
    }

    public void mark(int i) {
        this.e = a(i);
    }

    public boolean markSupported() {
        return this.f770a.markSupported();
    }

    public int read() {
        int read = this.f770a.read();
        if (read != -1) {
            this.f771b++;
        }
        return read;
    }

    public int read(byte[] bArr) {
        int read = this.f770a.read(bArr);
        if (read != -1) {
            this.f771b += (long) read;
        }
        return read;
    }

    public int read(byte[] bArr, int i, int i2) {
        int read = this.f770a.read(bArr, i, i2);
        if (read != -1) {
            this.f771b += (long) read;
        }
        return read;
    }

    public void reset() {
        a(this.e);
    }

    public long skip(long j) {
        long skip = this.f770a.skip(j);
        this.f771b += skip;
        return skip;
    }
}
