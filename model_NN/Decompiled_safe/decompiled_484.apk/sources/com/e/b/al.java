package com.e.b;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.widget.ImageView;
import java.lang.ref.ReferenceQueue;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

public class al {

    /* renamed from: a  reason: collision with root package name */
    static final Handler f782a = new am(Looper.getMainLooper());

    /* renamed from: b  reason: collision with root package name */
    static volatile al f783b = null;
    final Context c;
    final r d;
    final k e;
    final bf f;
    final Map<Object, a> g;
    final Map<ImageView, q> h;
    final ReferenceQueue<Object> i;
    final Bitmap.Config j;
    boolean k;
    volatile boolean l;
    boolean m;
    private final aq n;
    private final at o;
    private final ao p;
    private final List<bc> q;

    al(Context context, r rVar, k kVar, aq aqVar, at atVar, List<bc> list, bf bfVar, Bitmap.Config config, boolean z, boolean z2) {
        this.c = context;
        this.d = rVar;
        this.e = kVar;
        this.n = aqVar;
        this.o = atVar;
        this.j = config;
        ArrayList arrayList = new ArrayList((list != null ? list.size() : 0) + 7);
        arrayList.add(new be(context));
        if (list != null) {
            arrayList.addAll(list);
        }
        arrayList.add(new n(context));
        arrayList.add(new ae(context));
        arrayList.add(new p(context));
        arrayList.add(new c(context));
        arrayList.add(new z(context));
        arrayList.add(new ai(rVar.d, bfVar));
        this.q = Collections.unmodifiableList(arrayList);
        this.f = bfVar;
        this.g = new WeakHashMap();
        this.h = new WeakHashMap();
        this.k = z;
        this.l = z2;
        this.i = new ReferenceQueue<>();
        this.p = new ao(this.i, f782a);
        this.p.start();
    }

    public static al a(Context context) {
        if (f783b == null) {
            synchronized (al.class) {
                if (f783b == null) {
                    f783b = new an(context).a();
                }
            }
        }
        return f783b;
    }

    private void a(Bitmap bitmap, ar arVar, a aVar) {
        if (!aVar.f()) {
            if (!aVar.g()) {
                this.g.remove(aVar.d());
            }
            if (bitmap == null) {
                aVar.a();
                if (this.l) {
                    bn.a("Main", "errored", aVar.f768b.a());
                }
            } else if (arVar == null) {
                throw new AssertionError("LoadedFrom cannot be null.");
            } else {
                aVar.a(bitmap, arVar);
                if (this.l) {
                    bn.a("Main", "completed", aVar.f768b.a(), "from " + arVar);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void d(Object obj) {
        q remove;
        bn.b();
        a remove2 = this.g.remove(obj);
        if (remove2 != null) {
            remove2.b();
            this.d.b(remove2);
        }
        if ((obj instanceof ImageView) && (remove = this.h.remove((ImageView) obj)) != null) {
            remove.a();
        }
    }

    /* access modifiers changed from: package-private */
    public ay a(ay ayVar) {
        ay a2 = this.o.a(ayVar);
        if (a2 != null) {
            return a2;
        }
        throw new IllegalStateException("Request transformer " + this.o.getClass().getCanonicalName() + " returned null for " + ayVar);
    }

    public bb a(Uri uri) {
        return new bb(this, uri, 0);
    }

    public bb a(String str) {
        if (str == null) {
            return new bb(this, null, 0);
        }
        if (str.trim().length() != 0) {
            return a(Uri.parse(str));
        }
        throw new IllegalArgumentException("Path must not be empty.");
    }

    /* access modifiers changed from: package-private */
    public List<bc> a() {
        return this.q;
    }

    public void a(ImageView imageView) {
        d(imageView);
    }

    /* access modifiers changed from: package-private */
    public void a(ImageView imageView, q qVar) {
        this.h.put(imageView, qVar);
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        Object d2 = aVar.d();
        if (!(d2 == null || this.g.get(d2) == aVar)) {
            d(d2);
            this.g.put(d2, aVar);
        }
        b(aVar);
    }

    /* access modifiers changed from: package-private */
    public void a(d dVar) {
        boolean z = true;
        a i2 = dVar.i();
        List<a> k2 = dVar.k();
        boolean z2 = k2 != null && !k2.isEmpty();
        if (i2 == null && !z2) {
            z = false;
        }
        if (z) {
            Uri uri = dVar.h().d;
            Exception l2 = dVar.l();
            Bitmap e2 = dVar.e();
            ar m2 = dVar.m();
            if (i2 != null) {
                a(e2, m2, i2);
            }
            if (z2) {
                int size = k2.size();
                for (int i3 = 0; i3 < size; i3++) {
                    a(e2, m2, k2.get(i3));
                }
            }
            if (this.n != null && l2 != null) {
                this.n.a(this, uri, l2);
            }
        }
    }

    public void a(Object obj) {
        bn.b();
        ArrayList arrayList = new ArrayList(this.g.values());
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            a aVar = (a) arrayList.get(i2);
            if (aVar.l().equals(obj)) {
                d(aVar.d());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Bitmap b(String str) {
        Bitmap a2 = this.e.a(str);
        if (a2 != null) {
            this.f.a();
        } else {
            this.f.b();
        }
        return a2;
    }

    /* access modifiers changed from: package-private */
    public void b(a aVar) {
        this.d.a(aVar);
    }

    public void b(Object obj) {
        this.d.a(obj);
    }

    /* access modifiers changed from: package-private */
    public void c(a aVar) {
        Bitmap bitmap = null;
        if (ag.a(aVar.e)) {
            bitmap = b(aVar.e());
        }
        if (bitmap != null) {
            a(bitmap, ar.MEMORY, aVar);
            if (this.l) {
                bn.a("Main", "completed", aVar.f768b.a(), "from " + ar.MEMORY);
                return;
            }
            return;
        }
        a(aVar);
        if (this.l) {
            bn.a("Main", "resumed", aVar.f768b.a());
        }
    }

    public void c(Object obj) {
        this.d.b(obj);
    }
}
