package com.e.b;

import android.graphics.Bitmap;
import android.net.Uri;
import java.util.ArrayList;
import java.util.List;

public final class ba {

    /* renamed from: a  reason: collision with root package name */
    private Uri f801a;

    /* renamed from: b  reason: collision with root package name */
    private int f802b;
    private String c;
    private int d;
    private int e;
    private boolean f;
    private boolean g;
    private boolean h;
    private float i;
    private float j;
    private float k;
    private boolean l;
    private List<bj> m;
    private Bitmap.Config n;
    private as o;

    ba(Uri uri, int i2, Bitmap.Config config) {
        this.f801a = uri;
        this.f802b = i2;
        this.n = config;
    }

    public ba a(int i2, int i3) {
        if (i2 < 0) {
            throw new IllegalArgumentException("Width must be positive number or 0.");
        } else if (i3 < 0) {
            throw new IllegalArgumentException("Height must be positive number or 0.");
        } else if (i3 == 0 && i2 == 0) {
            throw new IllegalArgumentException("At least one dimension has to be positive number.");
        } else {
            this.d = i2;
            this.e = i3;
            return this;
        }
    }

    public ba a(bj bjVar) {
        if (bjVar == null) {
            throw new IllegalArgumentException("Transformation must not be null.");
        } else if (bjVar.a() == null) {
            throw new IllegalArgumentException("Transformation key must not be null.");
        } else {
            if (this.m == null) {
                this.m = new ArrayList(2);
            }
            this.m.add(bjVar);
            return this;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return (this.f801a == null && this.f802b == 0) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return (this.d == 0 && this.e == 0) ? false : true;
    }

    public ba c() {
        if (this.g) {
            throw new IllegalStateException("Center crop can not be used after calling centerInside");
        }
        this.f = true;
        return this;
    }

    public ay d() {
        if (this.g && this.f) {
            throw new IllegalStateException("Center crop and center inside can not be used together.");
        } else if (this.f && this.d == 0 && this.e == 0) {
            throw new IllegalStateException("Center crop requires calling resize with positive width and height.");
        } else if (this.g && this.d == 0 && this.e == 0) {
            throw new IllegalStateException("Center inside requires calling resize with positive width and height.");
        } else {
            if (this.o == null) {
                this.o = as.NORMAL;
            }
            return new ay(this.f801a, this.f802b, this.c, this.m, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.n, this.o);
        }
    }
}
