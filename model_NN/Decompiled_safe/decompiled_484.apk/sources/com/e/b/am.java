package com.e.b;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.List;

final class am extends Handler {
    am(Looper looper) {
        super(looper);
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 3:
                a aVar = (a) message.obj;
                if (aVar.j().l) {
                    bn.a("Main", "canceled", aVar.f768b.a(), "target got garbage collected");
                }
                aVar.f767a.d(aVar.d());
                return;
            case 8:
                List list = (List) message.obj;
                int size = list.size();
                for (int i = 0; i < size; i++) {
                    d dVar = (d) list.get(i);
                    dVar.f821b.a(dVar);
                }
                return;
            case 13:
                List list2 = (List) message.obj;
                int size2 = list2.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    a aVar2 = (a) list2.get(i2);
                    aVar2.f767a.c(aVar2);
                }
                return;
            default:
                throw new AssertionError("Unknown handler message received: " + message.what);
        }
    }
}
