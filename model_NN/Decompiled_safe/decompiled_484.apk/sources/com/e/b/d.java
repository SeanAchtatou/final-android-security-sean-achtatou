package com.e.b;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.NetworkInfo;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

class d implements Runnable {
    private static final Object t = new Object();
    private static final ThreadLocal<StringBuilder> u = new e();
    private static final AtomicInteger v = new AtomicInteger();
    private static final bc w = new f();

    /* renamed from: a  reason: collision with root package name */
    final int f820a = v.incrementAndGet();

    /* renamed from: b  reason: collision with root package name */
    final al f821b;
    final r c;
    final k d;
    final bf e;
    final String f;
    final ay g;
    final int h;
    int i;
    final bc j;
    a k;
    List<a> l;
    Bitmap m;
    Future<?> n;
    ar o;
    Exception p;
    int q;
    int r;
    as s;

    d(al alVar, r rVar, k kVar, bf bfVar, a aVar, bc bcVar) {
        this.f821b = alVar;
        this.c = rVar;
        this.d = kVar;
        this.e = bfVar;
        this.k = aVar;
        this.f = aVar.e();
        this.g = aVar.c();
        this.s = aVar.k();
        this.h = aVar.h();
        this.i = aVar.i();
        this.j = bcVar;
        this.r = bcVar.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static android.graphics.Bitmap a(com.e.b.ay r16, android.graphics.Bitmap r17, int r18) {
        /*
            int r8 = r17.getWidth()
            int r5 = r17.getHeight()
            r0 = r16
            boolean r11 = r0.l
            r9 = 0
            r6 = 0
            android.graphics.Matrix r7 = new android.graphics.Matrix
            r7.<init>()
            boolean r2 = r16.f()
            if (r2 == 0) goto L_0x00da
            r0 = r16
            int r12 = r0.h
            r0 = r16
            int r13 = r0.i
            r0 = r16
            float r2 = r0.m
            r3 = 0
            int r3 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r3 == 0) goto L_0x003b
            r0 = r16
            boolean r3 = r0.p
            if (r3 == 0) goto L_0x0085
            r0 = r16
            float r3 = r0.n
            r0 = r16
            float r4 = r0.o
            r7.setRotate(r2, r3, r4)
        L_0x003b:
            r0 = r16
            boolean r2 = r0.j
            if (r2 == 0) goto L_0x009e
            float r2 = (float) r12
            float r3 = (float) r8
            float r3 = r2 / r3
            float r2 = (float) r13
            float r4 = (float) r5
            float r2 = r2 / r4
            int r4 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
            if (r4 <= 0) goto L_0x0089
            float r4 = (float) r5
            float r2 = r2 / r3
            float r2 = r2 * r4
            double r14 = (double) r2
            double r14 = java.lang.Math.ceil(r14)
            int r4 = (int) r14
            int r2 = r5 - r4
            int r6 = r2 / 2
            float r2 = (float) r13
            float r10 = (float) r4
            float r2 = r2 / r10
            r10 = r9
            r9 = r6
            r6 = r8
        L_0x005f:
            boolean r5 = a(r11, r8, r5, r12, r13)
            if (r5 == 0) goto L_0x0068
            r7.preScale(r3, r2)
        L_0x0068:
            r5 = r6
            r3 = r10
            r6 = r4
            r4 = r9
        L_0x006c:
            if (r18 == 0) goto L_0x0074
            r0 = r18
            float r2 = (float) r0
            r7.preRotate(r2)
        L_0x0074:
            r8 = 1
            r2 = r17
            android.graphics.Bitmap r2 = android.graphics.Bitmap.createBitmap(r2, r3, r4, r5, r6, r7, r8)
            r0 = r17
            if (r2 == r0) goto L_0x0084
            r17.recycle()
            r17 = r2
        L_0x0084:
            return r17
        L_0x0085:
            r7.setRotate(r2)
            goto L_0x003b
        L_0x0089:
            float r4 = (float) r8
            float r3 = r3 / r2
            float r3 = r3 * r4
            double r14 = (double) r3
            double r14 = java.lang.Math.ceil(r14)
            int r4 = (int) r14
            int r3 = r8 - r4
            int r9 = r3 / 2
            float r3 = (float) r12
            float r10 = (float) r4
            float r3 = r3 / r10
            r10 = r9
            r9 = r6
            r6 = r4
            r4 = r5
            goto L_0x005f
        L_0x009e:
            r0 = r16
            boolean r2 = r0.k
            if (r2 == 0) goto L_0x00be
            float r2 = (float) r12
            float r3 = (float) r8
            float r2 = r2 / r3
            float r3 = (float) r13
            float r4 = (float) r5
            float r3 = r3 / r4
            int r4 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r4 >= 0) goto L_0x00bc
        L_0x00ae:
            boolean r3 = a(r11, r8, r5, r12, r13)
            if (r3 == 0) goto L_0x00b7
            r7.preScale(r2, r2)
        L_0x00b7:
            r4 = r6
            r3 = r9
            r6 = r5
            r5 = r8
            goto L_0x006c
        L_0x00bc:
            r2 = r3
            goto L_0x00ae
        L_0x00be:
            if (r12 != 0) goto L_0x00c2
            if (r13 == 0) goto L_0x00da
        L_0x00c2:
            if (r12 != r8) goto L_0x00c6
            if (r13 == r5) goto L_0x00da
        L_0x00c6:
            if (r12 == 0) goto L_0x00df
            float r2 = (float) r12
            float r3 = (float) r8
            float r2 = r2 / r3
            r3 = r2
        L_0x00cc:
            if (r13 == 0) goto L_0x00e4
            float r2 = (float) r13
            float r4 = (float) r5
            float r2 = r2 / r4
        L_0x00d1:
            boolean r4 = a(r11, r8, r5, r12, r13)
            if (r4 == 0) goto L_0x00da
            r7.preScale(r3, r2)
        L_0x00da:
            r4 = r6
            r3 = r9
            r6 = r5
            r5 = r8
            goto L_0x006c
        L_0x00df:
            float r2 = (float) r13
            float r3 = (float) r5
            float r2 = r2 / r3
            r3 = r2
            goto L_0x00cc
        L_0x00e4:
            float r2 = (float) r12
            float r4 = (float) r8
            float r2 = r2 / r4
            goto L_0x00d1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.e.b.d.a(com.e.b.ay, android.graphics.Bitmap, int):android.graphics.Bitmap");
    }

    static Bitmap a(InputStream inputStream, ay ayVar) {
        ad adVar = new ad(inputStream);
        long a2 = adVar.a(65536);
        BitmapFactory.Options c2 = bc.c(ayVar);
        boolean a3 = bc.a(c2);
        boolean c3 = bn.c(adVar);
        adVar.a(a2);
        if (c3) {
            byte[] b2 = bn.b(adVar);
            if (a3) {
                BitmapFactory.decodeByteArray(b2, 0, b2.length, c2);
                bc.a(ayVar.h, ayVar.i, c2, ayVar);
            }
            return BitmapFactory.decodeByteArray(b2, 0, b2.length, c2);
        }
        if (a3) {
            BitmapFactory.decodeStream(adVar, null, c2);
            bc.a(ayVar.h, ayVar.i, c2, ayVar);
            adVar.a(a2);
        }
        Bitmap decodeStream = BitmapFactory.decodeStream(adVar, null, c2);
        if (decodeStream != null) {
            return decodeStream;
        }
        throw new IOException("Failed to decode stream.");
    }

    static Bitmap a(List<bj> list, Bitmap bitmap) {
        int size = list.size();
        int i2 = 0;
        Bitmap bitmap2 = bitmap;
        while (i2 < size) {
            bj bjVar = list.get(i2);
            try {
                Bitmap a2 = bjVar.a(bitmap2);
                if (a2 == null) {
                    StringBuilder append = new StringBuilder().append("Transformation ").append(bjVar.a()).append(" returned null after ").append(i2).append(" previous transformation(s).\n\nTransformation list:\n");
                    for (bj a3 : list) {
                        append.append(a3.a()).append(10);
                    }
                    al.f782a.post(new h(append));
                    return null;
                } else if (a2 == bitmap2 && bitmap2.isRecycled()) {
                    al.f782a.post(new i(bjVar));
                    return null;
                } else if (a2 == bitmap2 || bitmap2.isRecycled()) {
                    i2++;
                    bitmap2 = a2;
                } else {
                    al.f782a.post(new j(bjVar));
                    return null;
                }
            } catch (RuntimeException e2) {
                al.f782a.post(new g(bjVar, e2));
                return null;
            }
        }
        return bitmap2;
    }

    static d a(al alVar, r rVar, k kVar, bf bfVar, a aVar) {
        ay c2 = aVar.c();
        List<bc> a2 = alVar.a();
        int size = a2.size();
        for (int i2 = 0; i2 < size; i2++) {
            bc bcVar = a2.get(i2);
            if (bcVar.a(c2)) {
                return new d(alVar, rVar, kVar, bfVar, aVar, bcVar);
            }
        }
        return new d(alVar, rVar, kVar, bfVar, aVar, w);
    }

    static void a(ay ayVar) {
        String c2 = ayVar.c();
        StringBuilder sb = u.get();
        sb.ensureCapacity("Picasso-".length() + c2.length());
        sb.replace("Picasso-".length(), sb.length(), c2);
        Thread.currentThread().setName(sb.toString());
    }

    private static boolean a(boolean z, int i2, int i3, int i4, int i5) {
        return !z || i2 > i4 || i3 > i5;
    }

    private as o() {
        boolean z = true;
        int i2 = 0;
        as asVar = as.LOW;
        boolean z2 = this.l != null && !this.l.isEmpty();
        if (this.k == null && !z2) {
            z = false;
        }
        if (!z) {
            return asVar;
        }
        as k2 = this.k != null ? this.k.k() : asVar;
        if (!z2) {
            return k2;
        }
        int size = this.l.size();
        while (i2 < size) {
            as k3 = this.l.get(i2).k();
            if (k3.ordinal() <= k2.ordinal()) {
                k3 = k2;
            }
            i2++;
            k2 = k3;
        }
        return k2;
    }

    /* access modifiers changed from: package-private */
    public Bitmap a() {
        Bitmap bitmap = null;
        if (!ag.a(this.h) || (bitmap = this.d.a(this.f)) == null) {
            this.g.c = this.r == 0 ? ah.OFFLINE.d : this.i;
            bd a2 = this.j.a(this.g, this.i);
            if (a2 != null) {
                this.o = a2.c();
                this.q = a2.d();
                bitmap = a2.a();
                if (bitmap == null) {
                    InputStream b2 = a2.b();
                    try {
                        bitmap = a(b2, this.g);
                    } finally {
                        bn.a(b2);
                    }
                }
            }
            if (bitmap != null) {
                if (this.f821b.l) {
                    bn.a("Hunter", "decoded", this.g.a());
                }
                this.e.a(bitmap);
                if (this.g.e() || this.q != 0) {
                    synchronized (t) {
                        if (this.g.f() || this.q != 0) {
                            bitmap = a(this.g, bitmap, this.q);
                            if (this.f821b.l) {
                                bn.a("Hunter", "transformed", this.g.a());
                            }
                        }
                        if (this.g.g()) {
                            bitmap = a(this.g.g, bitmap);
                            if (this.f821b.l) {
                                bn.a("Hunter", "transformed", this.g.a(), "from custom transformations");
                            }
                        }
                    }
                    if (bitmap != null) {
                        this.e.b(bitmap);
                    }
                }
            }
        } else {
            this.e.a();
            this.o = ar.MEMORY;
            if (this.f821b.l) {
                bn.a("Hunter", "decoded", this.g.a(), "from cache");
            }
        }
        return bitmap;
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        boolean z = this.f821b.l;
        ay ayVar = aVar.f768b;
        if (this.k == null) {
            this.k = aVar;
            if (!z) {
                return;
            }
            if (this.l == null || this.l.isEmpty()) {
                bn.a("Hunter", "joined", ayVar.a(), "to empty hunter");
            } else {
                bn.a("Hunter", "joined", ayVar.a(), bn.a(this, "to "));
            }
        } else {
            if (this.l == null) {
                this.l = new ArrayList(3);
            }
            this.l.add(aVar);
            if (z) {
                bn.a("Hunter", "joined", ayVar.a(), bn.a(this, "to "));
            }
            as k2 = aVar.k();
            if (k2.ordinal() > this.s.ordinal()) {
                this.s = k2;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(boolean z, NetworkInfo networkInfo) {
        if (!(this.r > 0)) {
            return false;
        }
        this.r--;
        return this.j.a(z, networkInfo);
    }

    /* access modifiers changed from: package-private */
    public void b(a aVar) {
        boolean z = false;
        if (this.k == aVar) {
            this.k = null;
            z = true;
        } else if (this.l != null) {
            z = this.l.remove(aVar);
        }
        if (z && aVar.k() == this.s) {
            this.s = o();
        }
        if (this.f821b.l) {
            bn.a("Hunter", "removed", aVar.f768b.a(), bn.a(this, "from "));
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        if (this.k == null) {
            return (this.l == null || this.l.isEmpty()) && this.n != null && this.n.cancel(false);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.n != null && this.n.isCancelled();
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.j.b();
    }

    /* access modifiers changed from: package-private */
    public Bitmap e() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public String f() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public int g() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public ay h() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public a i() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public al j() {
        return this.f821b;
    }

    /* access modifiers changed from: package-private */
    public List<a> k() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public Exception l() {
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public ar m() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public as n() {
        return this.s;
    }

    public void run() {
        String str;
        try {
            a(this.g);
            if (this.f821b.l) {
                bn.a("Hunter", "executing", bn.a(this));
            }
            this.m = a();
            if (this.m == null) {
                this.c.c(this);
            } else {
                this.c.a(this);
            }
        } catch (y e2) {
            if (!e2.f841a || e2.f842b != 504) {
                this.p = e2;
            }
            this.c.c(this);
        } catch (aj e3) {
            this.p = e3;
            this.c.b(this);
        } catch (IOException e4) {
            this.p = e4;
            this.c.b(this);
        } catch (OutOfMemoryError e5) {
            StringWriter stringWriter = new StringWriter();
            this.e.e().a(new PrintWriter(stringWriter));
            this.p = new RuntimeException(stringWriter.toString(), e5);
            this.c.c(this);
        } catch (Exception e6) {
            this.p = e6;
            this.c.c(this);
        } finally {
            str = "Picasso-Idle";
            Thread.currentThread().setName(str);
        }
    }
}
