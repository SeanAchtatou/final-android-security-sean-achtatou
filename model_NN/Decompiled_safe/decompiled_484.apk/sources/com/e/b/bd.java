package com.e.b;

import android.graphics.Bitmap;
import java.io.InputStream;

public final class bd {

    /* renamed from: a  reason: collision with root package name */
    private final ar f805a;

    /* renamed from: b  reason: collision with root package name */
    private final Bitmap f806b;
    private final InputStream c;
    private final int d;

    public bd(Bitmap bitmap, ar arVar) {
        this((Bitmap) bn.a(bitmap, "bitmap == null"), null, arVar, 0);
    }

    bd(Bitmap bitmap, InputStream inputStream, ar arVar, int i) {
        boolean z = true;
        if (!((inputStream == null ? false : z) ^ (bitmap != null))) {
            throw new AssertionError();
        }
        this.f806b = bitmap;
        this.c = inputStream;
        this.f805a = (ar) bn.a(arVar, "loadedFrom == null");
        this.d = i;
    }

    public bd(InputStream inputStream, ar arVar) {
        this(null, (InputStream) bn.a(inputStream, "stream == null"), arVar, 0);
    }

    public Bitmap a() {
        return this.f806b;
    }

    public InputStream b() {
        return this.c;
    }

    public ar c() {
        return this.f805a;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return this.d;
    }
}
