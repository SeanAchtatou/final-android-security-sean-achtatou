package com.e.b;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.SystemClock;
import android.widget.ImageView;

final class av extends BitmapDrawable {
    private static final Paint e = new Paint();

    /* renamed from: a  reason: collision with root package name */
    Drawable f795a;

    /* renamed from: b  reason: collision with root package name */
    long f796b;
    boolean c;
    int d = 255;
    private final boolean f;
    private final float g;
    private final ar h;

    av(Context context, Bitmap bitmap, Drawable drawable, ar arVar, boolean z, boolean z2) {
        super(context.getResources(), bitmap);
        this.f = z2;
        this.g = context.getResources().getDisplayMetrics().density;
        this.h = arVar;
        if (arVar != ar.MEMORY && !z) {
            this.f795a = drawable;
            this.c = true;
            this.f796b = SystemClock.uptimeMillis();
        }
    }

    private static Path a(Point point, int i) {
        Point point2 = new Point(point.x + i, point.y);
        Point point3 = new Point(point.x, point.y + i);
        Path path = new Path();
        path.moveTo((float) point.x, (float) point.y);
        path.lineTo((float) point2.x, (float) point2.y);
        path.lineTo((float) point3.x, (float) point3.y);
        return path;
    }

    private void a(Canvas canvas) {
        e.setColor(-1);
        canvas.drawPath(a(new Point(0, 0), (int) (16.0f * this.g)), e);
        e.setColor(this.h.d);
        canvas.drawPath(a(new Point(0, 0), (int) (15.0f * this.g)), e);
    }

    static void a(ImageView imageView, Context context, Bitmap bitmap, ar arVar, boolean z, boolean z2) {
        Drawable drawable = imageView.getDrawable();
        if (drawable instanceof AnimationDrawable) {
            ((AnimationDrawable) drawable).stop();
        }
        imageView.setImageDrawable(new av(context, bitmap, drawable, arVar, z, z2));
    }

    static void a(ImageView imageView, Drawable drawable) {
        imageView.setImageDrawable(drawable);
        if (imageView.getDrawable() instanceof AnimationDrawable) {
            ((AnimationDrawable) imageView.getDrawable()).start();
        }
    }

    public void draw(Canvas canvas) {
        if (!this.c) {
            super.draw(canvas);
        } else {
            float uptimeMillis = ((float) (SystemClock.uptimeMillis() - this.f796b)) / 200.0f;
            if (uptimeMillis >= 1.0f) {
                this.c = false;
                this.f795a = null;
                super.draw(canvas);
            } else {
                if (this.f795a != null) {
                    this.f795a.draw(canvas);
                }
                super.setAlpha((int) (uptimeMillis * ((float) this.d)));
                super.draw(canvas);
                super.setAlpha(this.d);
                if (Build.VERSION.SDK_INT <= 10) {
                    invalidateSelf();
                }
            }
        }
        if (this.f) {
            a(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        if (this.f795a != null) {
            this.f795a.setBounds(rect);
        }
        super.onBoundsChange(rect);
    }

    public void setAlpha(int i) {
        this.d = i;
        if (this.f795a != null) {
            this.f795a.setAlpha(i);
        }
        super.setAlpha(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        if (this.f795a != null) {
            this.f795a.setColorFilter(colorFilter);
        }
        super.setColorFilter(colorFilter);
    }
}
