package com.e.b;

import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

class s extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private final r f835a;

    public s(Looper looper, r rVar) {
        super(looper);
        this.f835a = rVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.b.r.a(com.e.b.d, boolean):void
     arg types: [com.e.b.d, int]
     candidates:
      com.e.b.r.a(com.e.b.a, boolean):void
      com.e.b.r.a(com.e.b.d, boolean):void */
    public void handleMessage(Message message) {
        boolean z = true;
        switch (message.what) {
            case 1:
                this.f835a.c((a) message.obj);
                return;
            case 2:
                this.f835a.d((a) message.obj);
                return;
            case 3:
            case 8:
            default:
                al.f782a.post(new t(this, message));
                return;
            case 4:
                this.f835a.e((d) message.obj);
                return;
            case 5:
                this.f835a.d((d) message.obj);
                return;
            case 6:
                this.f835a.a((d) message.obj, false);
                return;
            case 7:
                this.f835a.a();
                return;
            case 9:
                this.f835a.b((NetworkInfo) message.obj);
                return;
            case 10:
                r rVar = this.f835a;
                if (message.arg1 != 1) {
                    z = false;
                }
                rVar.b(z);
                return;
            case 11:
                this.f835a.c(message.obj);
                return;
            case 12:
                this.f835a.d(message.obj);
                return;
        }
    }
}
