package com.e.b;

final class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bj f822a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ RuntimeException f823b;

    g(bj bjVar, RuntimeException runtimeException) {
        this.f822a = bjVar;
        this.f823b = runtimeException;
    }

    public void run() {
        throw new RuntimeException("Transformation " + this.f822a.a() + " crashed with exception.", this.f823b);
    }
}
