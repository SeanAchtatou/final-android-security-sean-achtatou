package com.e.b;

import android.content.ContentResolver;
import android.content.Context;
import android.content.UriMatcher;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import java.io.InputStream;

class n extends bc {

    /* renamed from: a  reason: collision with root package name */
    private static final UriMatcher f828a = new UriMatcher(-1);

    /* renamed from: b  reason: collision with root package name */
    private final Context f829b;

    static {
        f828a.addURI("com.android.contacts", "contacts/lookup/*/#", 1);
        f828a.addURI("com.android.contacts", "contacts/lookup/*", 1);
        f828a.addURI("com.android.contacts", "contacts/#/photo", 2);
        f828a.addURI("com.android.contacts", "contacts/#", 3);
        f828a.addURI("com.android.contacts", "display_photo/#", 4);
    }

    n(Context context) {
        this.f829b = context;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private InputStream b(ay ayVar) {
        ContentResolver contentResolver = this.f829b.getContentResolver();
        Uri uri = ayVar.d;
        switch (f828a.match(uri)) {
            case 1:
                uri = ContactsContract.Contacts.lookupContact(contentResolver, uri);
                if (uri == null) {
                    return null;
                }
                break;
            case 2:
            case 4:
                return contentResolver.openInputStream(uri);
            case 3:
                break;
            default:
                throw new IllegalStateException("Invalid uri: " + uri);
        }
        return Build.VERSION.SDK_INT < 14 ? ContactsContract.Contacts.openContactPhotoInputStream(contentResolver, uri) : o.a(contentResolver, uri);
    }

    public bd a(ay ayVar, int i) {
        InputStream b2 = b(ayVar);
        if (b2 != null) {
            return new bd(b2, ar.DISK);
        }
        return null;
    }

    public boolean a(ay ayVar) {
        Uri uri = ayVar.d;
        return "content".equals(uri.getScheme()) && ContactsContract.Contacts.CONTENT_URI.getHost().equals(uri.getHost()) && f828a.match(ayVar.d) != -1;
    }
}
