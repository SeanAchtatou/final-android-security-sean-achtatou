package com.e.b;

import android.graphics.Bitmap;
import java.io.InputStream;

public class x {

    /* renamed from: a  reason: collision with root package name */
    final InputStream f839a;

    /* renamed from: b  reason: collision with root package name */
    final Bitmap f840b;
    final boolean c;
    final long d;

    public x(InputStream inputStream, boolean z, long j) {
        if (inputStream == null) {
            throw new IllegalArgumentException("Stream may not be null.");
        }
        this.f839a = inputStream;
        this.f840b = null;
        this.c = z;
        this.d = j;
    }

    public InputStream a() {
        return this.f839a;
    }

    @Deprecated
    public Bitmap b() {
        return this.f840b;
    }

    public long c() {
        return this.d;
    }
}
