package com.e.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;

class v extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private final r f838a;

    v(r rVar) {
        this.f838a = rVar;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.AIRPLANE_MODE");
        if (this.f838a.o) {
            intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        }
        this.f838a.f834b.registerReceiver(this, intentFilter);
    }

    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            String action = intent.getAction();
            if ("android.intent.action.AIRPLANE_MODE".equals(action)) {
                if (intent.hasExtra("state")) {
                    this.f838a.a(intent.getBooleanExtra("state", false));
                }
            } else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
                this.f838a.a(((ConnectivityManager) bn.a(context, "connectivity")).getActiveNetworkInfo());
            }
        }
    }
}
