package com.e.b;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.HandlerThread;

class bf {

    /* renamed from: a  reason: collision with root package name */
    final HandlerThread f808a = new HandlerThread("Picasso-Stats", 10);

    /* renamed from: b  reason: collision with root package name */
    final k f809b;
    final Handler c;
    long d;
    long e;
    long f;
    long g;
    long h;
    long i;
    long j;
    long k;
    int l;
    int m;
    int n;

    bf(k kVar) {
        this.f809b = kVar;
        this.f808a.start();
        bn.a(this.f808a.getLooper());
        this.c = new bg(this.f808a.getLooper(), this);
    }

    private static long a(int i2, long j2) {
        return j2 / ((long) i2);
    }

    private void a(Bitmap bitmap, int i2) {
        this.c.sendMessage(this.c.obtainMessage(i2, bn.a(bitmap), 0));
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.c.sendEmptyMessage(0);
    }

    /* access modifiers changed from: package-private */
    public void a(long j2) {
        this.c.sendMessage(this.c.obtainMessage(4, Long.valueOf(j2)));
    }

    /* access modifiers changed from: package-private */
    public void a(Bitmap bitmap) {
        a(bitmap, 2);
    }

    /* access modifiers changed from: package-private */
    public void a(Long l2) {
        this.l++;
        this.f += l2.longValue();
        this.i = a(this.l, this.f);
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.c.sendEmptyMessage(1);
    }

    /* access modifiers changed from: package-private */
    public void b(long j2) {
        this.m++;
        this.g += j2;
        this.j = a(this.m, this.g);
    }

    /* access modifiers changed from: package-private */
    public void b(Bitmap bitmap) {
        a(bitmap, 3);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.d++;
    }

    /* access modifiers changed from: package-private */
    public void c(long j2) {
        this.n++;
        this.h += j2;
        this.k = a(this.m, this.h);
    }

    /* access modifiers changed from: package-private */
    public void d() {
        this.e++;
    }

    /* access modifiers changed from: package-private */
    public bi e() {
        return new bi(this.f809b.b(), this.f809b.a(), this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, System.currentTimeMillis());
    }
}
