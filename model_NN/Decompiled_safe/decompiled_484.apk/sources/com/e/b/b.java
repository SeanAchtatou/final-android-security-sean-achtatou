package com.e.b;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

class b<M> extends WeakReference<M> {

    /* renamed from: a  reason: collision with root package name */
    final a f800a;

    public b(a aVar, M m, ReferenceQueue<? super M> referenceQueue) {
        super(m, referenceQueue);
        this.f800a = aVar;
    }
}
