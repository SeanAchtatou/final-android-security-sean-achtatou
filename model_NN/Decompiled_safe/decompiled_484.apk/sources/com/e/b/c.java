package com.e.b;

import android.content.Context;
import android.content.res.AssetManager;
import android.net.Uri;

class c extends bc {

    /* renamed from: a  reason: collision with root package name */
    private static final int f818a = "file:///android_asset/".length();

    /* renamed from: b  reason: collision with root package name */
    private final AssetManager f819b;

    public c(Context context) {
        this.f819b = context.getAssets();
    }

    static String b(ay ayVar) {
        return ayVar.d.toString().substring(f818a);
    }

    public bd a(ay ayVar, int i) {
        return new bd(this.f819b.open(b(ayVar)), ar.DISK);
    }

    public boolean a(ay ayVar) {
        Uri uri = ayVar.d;
        return "file".equals(uri.getScheme()) && !uri.getPathSegments().isEmpty() && "android_asset".equals(uri.getPathSegments().get(0));
    }
}
