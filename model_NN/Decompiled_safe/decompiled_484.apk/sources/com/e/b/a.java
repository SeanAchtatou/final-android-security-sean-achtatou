package com.e.b;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import java.lang.ref.WeakReference;

abstract class a<T> {

    /* renamed from: a  reason: collision with root package name */
    final al f767a;

    /* renamed from: b  reason: collision with root package name */
    final ay f768b;
    final WeakReference<T> c;
    final boolean d;
    final int e;
    final int f;
    final int g;
    final Drawable h;
    final String i;
    final Object j;
    boolean k;
    boolean l;

    a(al alVar, T t, ay ayVar, int i2, int i3, int i4, Drawable drawable, String str, Object obj, boolean z) {
        this.f767a = alVar;
        this.f768b = ayVar;
        this.c = t == null ? null : new b(this, t, alVar.i);
        this.e = i2;
        this.f = i3;
        this.d = z;
        this.g = i4;
        this.h = drawable;
        this.i = str;
        this.j = obj == null ? this : obj;
    }

    /* access modifiers changed from: package-private */
    public abstract void a();

    /* access modifiers changed from: package-private */
    public abstract void a(Bitmap bitmap, ar arVar);

    /* access modifiers changed from: package-private */
    public void b() {
        this.l = true;
    }

    /* access modifiers changed from: package-private */
    public ay c() {
        return this.f768b;
    }

    /* access modifiers changed from: package-private */
    public T d() {
        if (this.c == null) {
            return null;
        }
        return this.c.get();
    }

    /* access modifiers changed from: package-private */
    public String e() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public int h() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public int i() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public al j() {
        return this.f767a;
    }

    /* access modifiers changed from: package-private */
    public as k() {
        return this.f768b.r;
    }

    /* access modifiers changed from: package-private */
    public Object l() {
        return this.j;
    }
}
