package com.e.b;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Looper;
import android.os.StatFs;
import android.provider.Settings;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

final class bn {

    /* renamed from: a  reason: collision with root package name */
    static final StringBuilder f817a = new StringBuilder();

    static int a(Resources resources, ay ayVar) {
        if (ayVar.e != 0 || ayVar.d == null) {
            return ayVar.e;
        }
        String authority = ayVar.d.getAuthority();
        if (authority == null) {
            throw new FileNotFoundException("No package provided: " + ayVar.d);
        }
        List<String> pathSegments = ayVar.d.getPathSegments();
        if (pathSegments == null || pathSegments.isEmpty()) {
            throw new FileNotFoundException("No path segments: " + ayVar.d);
        } else if (pathSegments.size() == 1) {
            try {
                return Integer.parseInt(pathSegments.get(0));
            } catch (NumberFormatException e) {
                throw new FileNotFoundException("Last path segment is not a resource ID: " + ayVar.d);
            }
        } else if (pathSegments.size() == 2) {
            return resources.getIdentifier(pathSegments.get(1), pathSegments.get(0), authority);
        } else {
            throw new FileNotFoundException("More than two path segments: " + ayVar.d);
        }
    }

    static int a(Bitmap bitmap) {
        int a2 = Build.VERSION.SDK_INT >= 12 ? bq.a(bitmap) : bitmap.getRowBytes() * bitmap.getHeight();
        if (a2 >= 0) {
            return a2;
        }
        throw new IllegalStateException("Negative size: " + bitmap);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    static long a(File file) {
        long j;
        try {
            StatFs statFs = new StatFs(file.getAbsolutePath());
            j = (((long) statFs.getBlockSize()) * ((long) statFs.getBlockCount())) / 50;
        } catch (IllegalArgumentException e) {
            j = 5242880;
        }
        return Math.max(Math.min(j, 52428800L), 5242880L);
    }

    static Resources a(Context context, ay ayVar) {
        if (ayVar.e != 0 || ayVar.d == null) {
            return context.getResources();
        }
        String authority = ayVar.d.getAuthority();
        if (authority == null) {
            throw new FileNotFoundException("No package provided: " + ayVar.d);
        }
        try {
            return context.getPackageManager().getResourcesForApplication(authority);
        } catch (PackageManager.NameNotFoundException e) {
            throw new FileNotFoundException("Unable to obtain resources for package: " + ayVar.d);
        }
    }

    static w a(Context context) {
        try {
            Class.forName("com.e.a.am");
            return br.a(context);
        } catch (ClassNotFoundException e) {
            return new bk(context);
        }
    }

    static <T> T a(Context context, String str) {
        return context.getSystemService(str);
    }

    static <T> T a(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    static String a(ay ayVar) {
        String a2 = a(ayVar, f817a);
        f817a.setLength(0);
        return a2;
    }

    static String a(ay ayVar, StringBuilder sb) {
        if (ayVar.f != null) {
            sb.ensureCapacity(ayVar.f.length() + 50);
            sb.append(ayVar.f);
        } else if (ayVar.d != null) {
            String uri = ayVar.d.toString();
            sb.ensureCapacity(uri.length() + 50);
            sb.append(uri);
        } else {
            sb.ensureCapacity(50);
            sb.append(ayVar.e);
        }
        sb.append(10);
        if (ayVar.m != 0.0f) {
            sb.append("rotation:").append(ayVar.m);
            if (ayVar.p) {
                sb.append('@').append(ayVar.n).append('x').append(ayVar.o);
            }
            sb.append(10);
        }
        if (ayVar.d()) {
            sb.append("resize:").append(ayVar.h).append('x').append(ayVar.i);
            sb.append(10);
        }
        if (ayVar.j) {
            sb.append("centerCrop").append(10);
        } else if (ayVar.k) {
            sb.append("centerInside").append(10);
        }
        if (ayVar.g != null) {
            int size = ayVar.g.size();
            for (int i = 0; i < size; i++) {
                sb.append(ayVar.g.get(i).a());
                sb.append(10);
            }
        }
        return sb.toString();
    }

    static String a(d dVar) {
        return a(dVar, "");
    }

    static String a(d dVar, String str) {
        StringBuilder sb = new StringBuilder(str);
        a i = dVar.i();
        if (i != null) {
            sb.append(i.f768b.a());
        }
        List<a> k = dVar.k();
        if (k != null) {
            int size = k.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (i2 > 0 || i != null) {
                    sb.append(", ");
                }
                sb.append(k.get(i2).f768b.a());
            }
        }
        return sb.toString();
    }

    static void a() {
        if (c()) {
            throw new IllegalStateException("Method call should not happen from the main thread.");
        }
    }

    static void a(Looper looper) {
        bo boVar = new bo(looper);
        boVar.sendMessageDelayed(boVar.obtainMessage(), 1000);
    }

    static void a(InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
            }
        }
    }

    static void a(String str, String str2, String str3) {
        a(str, str2, str3, "");
    }

    static void a(String str, String str2, String str3, String str4) {
        Log.d("Picasso", String.format("%1$-11s %2$-12s %3$s %4$s", str, str2, str3, str4));
    }

    static boolean a(String str) {
        boolean z = true;
        if (str == null) {
            return false;
        }
        String[] split = str.split(" ", 2);
        if ("CACHE".equals(split[0])) {
            return true;
        }
        if (split.length == 1) {
            return false;
        }
        try {
            if (!"CONDITIONAL_CACHE".equals(split[0]) || Integer.parseInt(split[1]) != 304) {
                z = false;
            }
            return z;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    static File b(Context context) {
        File file = new File(context.getApplicationContext().getCacheDir(), "picasso-cache");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    static void b() {
        if (!c()) {
            throw new IllegalStateException("Method call should happen from the main thread.");
        }
    }

    static boolean b(Context context, String str) {
        return context.checkCallingOrSelfPermission(str) == 0;
    }

    static byte[] b(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[4096];
        while (true) {
            int read = inputStream.read(bArr);
            if (-1 == read) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    static int c(Context context) {
        ActivityManager activityManager = (ActivityManager) a(context, "activity");
        return (((!((context.getApplicationInfo().flags & AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START) != 0) || Build.VERSION.SDK_INT < 11) ? activityManager.getMemoryClass() : bp.a(activityManager)) * AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START) / 7;
    }

    static boolean c() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    static boolean c(InputStream inputStream) {
        byte[] bArr = new byte[12];
        return inputStream.read(bArr, 0, 12) == 12 && "RIFF".equals(new String(bArr, 0, 4, "US-ASCII")) && "WEBP".equals(new String(bArr, 8, 4, "US-ASCII"));
    }

    static boolean d(Context context) {
        try {
            return Settings.System.getInt(context.getContentResolver(), "airplane_mode_on", 0) != 0;
        } catch (NullPointerException e) {
            return false;
        }
    }
}
