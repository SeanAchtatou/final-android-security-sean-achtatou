package com.e.b;

import android.content.Context;
import android.graphics.Bitmap;
import java.util.List;
import java.util.concurrent.ExecutorService;

public class an {

    /* renamed from: a  reason: collision with root package name */
    private final Context f784a;

    /* renamed from: b  reason: collision with root package name */
    private w f785b;
    private ExecutorService c;
    private k d;
    private aq e;
    private at f;
    private List<bc> g;
    private Bitmap.Config h;
    private boolean i;
    private boolean j;

    public an(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Context must not be null.");
        }
        this.f784a = context.getApplicationContext();
    }

    public al a() {
        Context context = this.f784a;
        if (this.f785b == null) {
            this.f785b = bn.a(context);
        }
        if (this.d == null) {
            this.d = new ac(context);
        }
        if (this.c == null) {
            this.c = new aw();
        }
        if (this.f == null) {
            this.f = at.f794a;
        }
        bf bfVar = new bf(this.d);
        return new al(context, new r(context, this.c, al.f782a, this.f785b, this.d, bfVar), this.d, this.e, this.f, this.g, bfVar, this.h, this.i, this.j);
    }
}
