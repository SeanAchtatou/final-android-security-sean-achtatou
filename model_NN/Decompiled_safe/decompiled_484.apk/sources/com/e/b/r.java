package com.e.b;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;

class r {

    /* renamed from: a  reason: collision with root package name */
    final u f833a = new u();

    /* renamed from: b  reason: collision with root package name */
    final Context f834b;
    final ExecutorService c;
    final w d;
    final Map<String, d> e;
    final Map<Object, a> f;
    final Map<Object, a> g;
    final Set<Object> h;
    final Handler i;
    final Handler j;
    final k k;
    final bf l;
    final List<d> m;
    final v n;
    final boolean o;
    boolean p;

    r(Context context, ExecutorService executorService, Handler handler, w wVar, k kVar, bf bfVar) {
        this.f833a.start();
        bn.a(this.f833a.getLooper());
        this.f834b = context;
        this.c = executorService;
        this.e = new LinkedHashMap();
        this.f = new WeakHashMap();
        this.g = new WeakHashMap();
        this.h = new HashSet();
        this.i = new s(this.f833a.getLooper(), this);
        this.d = wVar;
        this.j = handler;
        this.k = kVar;
        this.l = bfVar;
        this.m = new ArrayList(4);
        this.p = bn.d(this.f834b);
        this.o = bn.b(context, "android.permission.ACCESS_NETWORK_STATE");
        this.n = new v(this);
        this.n.a();
    }

    private void a(List<d> list) {
        if (list != null && !list.isEmpty() && list.get(0).j().l) {
            StringBuilder sb = new StringBuilder();
            for (d next : list) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(bn.a(next));
            }
            bn.a("Dispatcher", "delivered", sb.toString());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.b.r.a(com.e.b.a, boolean):void
     arg types: [com.e.b.a, int]
     candidates:
      com.e.b.r.a(com.e.b.d, boolean):void
      com.e.b.r.a(com.e.b.a, boolean):void */
    private void b() {
        if (!this.f.isEmpty()) {
            Iterator<a> it = this.f.values().iterator();
            while (it.hasNext()) {
                a next = it.next();
                it.remove();
                if (next.j().l) {
                    bn.a("Dispatcher", "replaying", next.c().a());
                }
                a(next, false);
            }
        }
    }

    private void e(a aVar) {
        Object d2 = aVar.d();
        if (d2 != null) {
            aVar.k = true;
            this.f.put(d2, aVar);
        }
    }

    private void f(d dVar) {
        a i2 = dVar.i();
        if (i2 != null) {
            e(i2);
        }
        List<a> k2 = dVar.k();
        if (k2 != null) {
            int size = k2.size();
            for (int i3 = 0; i3 < size; i3++) {
                e(k2.get(i3));
            }
        }
    }

    private void g(d dVar) {
        if (!dVar.c()) {
            this.m.add(dVar);
            if (!this.i.hasMessages(7)) {
                this.i.sendEmptyMessageDelayed(7, 200);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        ArrayList arrayList = new ArrayList(this.m);
        this.m.clear();
        this.j.sendMessage(this.j.obtainMessage(8, arrayList));
        a((List<d>) arrayList);
    }

    /* access modifiers changed from: package-private */
    public void a(NetworkInfo networkInfo) {
        this.i.sendMessage(this.i.obtainMessage(9, networkInfo));
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        this.i.sendMessage(this.i.obtainMessage(1, aVar));
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar, boolean z) {
        if (this.h.contains(aVar.l())) {
            this.g.put(aVar.d(), aVar);
            if (aVar.j().l) {
                bn.a("Dispatcher", "paused", aVar.f768b.a(), "because tag '" + aVar.l() + "' is paused");
                return;
            }
            return;
        }
        d dVar = this.e.get(aVar.e());
        if (dVar != null) {
            dVar.a(aVar);
        } else if (!this.c.isShutdown()) {
            d a2 = d.a(aVar.j(), this, this.k, this.l, aVar);
            a2.n = this.c.submit(a2);
            this.e.put(aVar.e(), a2);
            if (z) {
                this.f.remove(aVar.d());
            }
            if (aVar.j().l) {
                bn.a("Dispatcher", "enqueued", aVar.f768b.a());
            }
        } else if (aVar.j().l) {
            bn.a("Dispatcher", "ignored", aVar.f768b.a(), "because shut down");
        }
    }

    /* access modifiers changed from: package-private */
    public void a(d dVar) {
        this.i.sendMessage(this.i.obtainMessage(4, dVar));
    }

    /* access modifiers changed from: package-private */
    public void a(d dVar, boolean z) {
        if (dVar.j().l) {
            bn.a("Dispatcher", "batched", bn.a(dVar), "for error" + (z ? " (will replay)" : ""));
        }
        this.e.remove(dVar.f());
        g(dVar);
    }

    /* access modifiers changed from: package-private */
    public void a(Object obj) {
        this.i.sendMessage(this.i.obtainMessage(11, obj));
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.i.sendMessage(this.i.obtainMessage(10, z ? 1 : 0, 0));
    }

    /* access modifiers changed from: package-private */
    public void b(NetworkInfo networkInfo) {
        if (this.c instanceof aw) {
            ((aw) this.c).a(networkInfo);
        }
        if (networkInfo != null && networkInfo.isConnected()) {
            b();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(a aVar) {
        this.i.sendMessage(this.i.obtainMessage(2, aVar));
    }

    /* access modifiers changed from: package-private */
    public void b(d dVar) {
        this.i.sendMessageDelayed(this.i.obtainMessage(5, dVar), 500);
    }

    /* access modifiers changed from: package-private */
    public void b(Object obj) {
        this.i.sendMessage(this.i.obtainMessage(12, obj));
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z) {
        this.p = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.b.r.a(com.e.b.a, boolean):void
     arg types: [com.e.b.a, int]
     candidates:
      com.e.b.r.a(com.e.b.d, boolean):void
      com.e.b.r.a(com.e.b.a, boolean):void */
    /* access modifiers changed from: package-private */
    public void c(a aVar) {
        a(aVar, true);
    }

    /* access modifiers changed from: package-private */
    public void c(d dVar) {
        this.i.sendMessage(this.i.obtainMessage(6, dVar));
    }

    /* access modifiers changed from: package-private */
    public void c(Object obj) {
        if (this.h.add(obj)) {
            Iterator<d> it = this.e.values().iterator();
            while (it.hasNext()) {
                d next = it.next();
                boolean z = next.j().l;
                a i2 = next.i();
                List<a> k2 = next.k();
                boolean z2 = k2 != null && !k2.isEmpty();
                if (i2 != null || z2) {
                    if (i2 != null && i2.l().equals(obj)) {
                        next.b(i2);
                        this.g.put(i2.d(), i2);
                        if (z) {
                            bn.a("Dispatcher", "paused", i2.f768b.a(), "because tag '" + obj + "' was paused");
                        }
                    }
                    if (z2) {
                        for (int size = k2.size() - 1; size >= 0; size--) {
                            a aVar = k2.get(size);
                            if (aVar.l().equals(obj)) {
                                next.b(aVar);
                                this.g.put(aVar.d(), aVar);
                                if (z) {
                                    bn.a("Dispatcher", "paused", aVar.f768b.a(), "because tag '" + obj + "' was paused");
                                }
                            }
                        }
                    }
                    if (next.b()) {
                        it.remove();
                        if (z) {
                            bn.a("Dispatcher", "canceled", bn.a(next), "all actions paused");
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void d(a aVar) {
        String e2 = aVar.e();
        d dVar = this.e.get(e2);
        if (dVar != null) {
            dVar.b(aVar);
            if (dVar.b()) {
                this.e.remove(e2);
                if (aVar.j().l) {
                    bn.a("Dispatcher", "canceled", aVar.c().a());
                }
            }
        }
        if (this.h.contains(aVar.l())) {
            this.g.remove(aVar.d());
            if (aVar.j().l) {
                bn.a("Dispatcher", "canceled", aVar.c().a(), "because paused request got canceled");
            }
        }
        a remove = this.f.remove(aVar.d());
        if (remove != null && remove.j().l) {
            bn.a("Dispatcher", "canceled", remove.c().a(), "from replaying");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.e.b.r.a(com.e.b.d, boolean):void
     arg types: [com.e.b.d, int]
     candidates:
      com.e.b.r.a(com.e.b.a, boolean):void
      com.e.b.r.a(com.e.b.d, boolean):void */
    /* access modifiers changed from: package-private */
    public void d(d dVar) {
        boolean z = true;
        if (!dVar.c()) {
            if (this.c.isShutdown()) {
                a(dVar, false);
                return;
            }
            NetworkInfo activeNetworkInfo = this.o ? ((ConnectivityManager) bn.a(this.f834b, "connectivity")).getActiveNetworkInfo() : null;
            boolean z2 = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            boolean a2 = dVar.a(this.p, activeNetworkInfo);
            boolean d2 = dVar.d();
            if (!a2) {
                if (!this.o || !d2) {
                    z = false;
                }
                a(dVar, z);
                if (z) {
                    f(dVar);
                }
            } else if (!this.o || z2) {
                if (dVar.j().l) {
                    bn.a("Dispatcher", "retrying", bn.a(dVar));
                }
                if (dVar.l() instanceof aj) {
                    dVar.i |= ah.NO_CACHE.d;
                }
                dVar.n = this.c.submit(dVar);
            } else {
                a(dVar, d2);
                if (d2) {
                    f(dVar);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void d(Object obj) {
        if (this.h.remove(obj)) {
            ArrayList arrayList = null;
            Iterator<a> it = this.g.values().iterator();
            while (it.hasNext()) {
                a next = it.next();
                if (next.l().equals(obj)) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(next);
                    it.remove();
                }
            }
            if (arrayList != null) {
                this.j.sendMessage(this.j.obtainMessage(13, arrayList));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void e(d dVar) {
        if (ag.b(dVar.g())) {
            this.k.a(dVar.f(), dVar.e());
        }
        this.e.remove(dVar.f());
        g(dVar);
        if (dVar.j().l) {
            bn.a("Dispatcher", "batched", bn.a(dVar), "for completion");
        }
    }
}
