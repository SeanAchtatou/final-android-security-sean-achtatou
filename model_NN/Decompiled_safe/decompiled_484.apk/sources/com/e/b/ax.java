package com.e.b;

import java.util.concurrent.FutureTask;

final class ax extends FutureTask<d> implements Comparable<ax> {

    /* renamed from: a  reason: collision with root package name */
    private final d f797a;

    public ax(d dVar) {
        super(dVar, null);
        this.f797a = dVar;
    }

    /* renamed from: a */
    public int compareTo(ax axVar) {
        as n = this.f797a.n();
        as n2 = axVar.f797a.n();
        return n == n2 ? this.f797a.f820a - axVar.f797a.f820a : n2.ordinal() - n.ordinal();
    }
}
