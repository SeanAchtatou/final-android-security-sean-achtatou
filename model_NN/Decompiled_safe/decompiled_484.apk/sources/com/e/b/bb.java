package com.e.b;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;
import java.util.concurrent.atomic.AtomicInteger;

public class bb {

    /* renamed from: a  reason: collision with root package name */
    private static final AtomicInteger f803a = new AtomicInteger();

    /* renamed from: b  reason: collision with root package name */
    private final al f804b;
    private final ba c;
    private boolean d;
    private boolean e;
    private boolean f;
    private int g;
    private int h;
    private int i;
    private int j;
    private Drawable k;
    private Drawable l;
    private Object m;

    bb() {
        this.f = true;
        this.f804b = null;
        this.c = new ba(null, 0, null);
    }

    bb(al alVar, Uri uri, int i2) {
        this.f = true;
        if (alVar.m) {
            throw new IllegalStateException("Picasso instance already shut down. Cannot submit new requests.");
        }
        this.f804b = alVar;
        this.c = new ba(uri, i2, alVar.j);
    }

    private ay a(long j2) {
        int andIncrement = f803a.getAndIncrement();
        ay d2 = this.c.d();
        d2.f798a = andIncrement;
        d2.f799b = j2;
        boolean z = this.f804b.l;
        if (z) {
            bn.a("Main", "created", d2.b(), d2.toString());
        }
        ay a2 = this.f804b.a(d2);
        if (a2 != d2) {
            a2.f798a = andIncrement;
            a2.f799b = j2;
            if (z) {
                bn.a("Main", "changed", a2.a(), "into " + a2);
            }
        }
        return a2;
    }

    private Drawable d() {
        return this.g != 0 ? this.f804b.c.getResources().getDrawable(this.g) : this.k;
    }

    /* access modifiers changed from: package-private */
    public bb a() {
        this.e = false;
        return this;
    }

    public bb a(int i2) {
        if (!this.f) {
            throw new IllegalStateException("Already explicitly declared as no placeholder.");
        } else if (i2 == 0) {
            throw new IllegalArgumentException("Placeholder image resource invalid.");
        } else if (this.k != null) {
            throw new IllegalStateException("Placeholder image already set.");
        } else {
            this.g = i2;
            return this;
        }
    }

    public bb a(int i2, int i3) {
        this.c.a(i2, i3);
        return this;
    }

    public bb a(bj bjVar) {
        this.c.a(bjVar);
        return this;
    }

    public bb a(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("Tag invalid.");
        } else if (this.m != null) {
            throw new IllegalStateException("Tag already set.");
        } else {
            this.m = obj;
            return this;
        }
    }

    public void a(ImageView imageView) {
        a(imageView, (m) null);
    }

    public void a(ImageView imageView, m mVar) {
        Bitmap b2;
        long nanoTime = System.nanoTime();
        bn.b();
        if (imageView == null) {
            throw new IllegalArgumentException("Target must not be null.");
        } else if (!this.c.a()) {
            this.f804b.a(imageView);
            if (this.f) {
                av.a(imageView, d());
            }
        } else {
            if (this.e) {
                if (this.c.b()) {
                    throw new IllegalStateException("Fit cannot be used with resize.");
                }
                int width = imageView.getWidth();
                int height = imageView.getHeight();
                if (width == 0 || height == 0) {
                    if (this.f) {
                        av.a(imageView, d());
                    }
                    this.f804b.a(imageView, new q(this, imageView, mVar));
                    return;
                }
                this.c.a(width, height);
            }
            ay a2 = a(nanoTime);
            String a3 = bn.a(a2);
            if (!ag.a(this.i) || (b2 = this.f804b.b(a3)) == null) {
                if (this.f) {
                    av.a(imageView, d());
                }
                this.f804b.a((a) new ab(this.f804b, imageView, a2, this.i, this.j, this.h, this.l, a3, this.m, mVar, this.d));
                return;
            }
            this.f804b.a(imageView);
            av.a(imageView, this.f804b.c, b2, ar.MEMORY, this.d, this.f804b.k);
            if (this.f804b.l) {
                bn.a("Main", "completed", a2.b(), "from " + ar.MEMORY);
            }
            if (mVar != null) {
                mVar.onSuccess();
            }
        }
    }

    public bb b() {
        this.c.c();
        return this;
    }

    public Bitmap c() {
        long nanoTime = System.nanoTime();
        bn.a();
        if (this.e) {
            throw new IllegalStateException("Fit cannot be used with get.");
        } else if (!this.c.a()) {
            return null;
        } else {
            ay a2 = a(nanoTime);
            return d.a(this.f804b, this.f804b.d, this.f804b.e, this.f804b.f, new aa(this.f804b, a2, this.i, this.j, this.m, bn.a(a2, new StringBuilder()))).a();
        }
    }
}
