package com.e.b;

import android.view.ViewTreeObserver;
import android.widget.ImageView;
import java.lang.ref.WeakReference;

class q implements ViewTreeObserver.OnPreDrawListener {

    /* renamed from: a  reason: collision with root package name */
    final bb f831a;

    /* renamed from: b  reason: collision with root package name */
    final WeakReference<ImageView> f832b;
    m c;

    q(bb bbVar, ImageView imageView, m mVar) {
        this.f831a = bbVar;
        this.f832b = new WeakReference<>(imageView);
        this.c = mVar;
        imageView.getViewTreeObserver().addOnPreDrawListener(this);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.c = null;
        ImageView imageView = this.f832b.get();
        if (imageView != null) {
            ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this);
            }
        }
    }

    public boolean onPreDraw() {
        ImageView imageView = this.f832b.get();
        if (imageView != null) {
            ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                int width = imageView.getWidth();
                int height = imageView.getHeight();
                if (width > 0 && height > 0) {
                    viewTreeObserver.removeOnPreDrawListener(this);
                    this.f831a.a().a(width, height).a(imageView, this.c);
                }
            }
        }
        return true;
    }
}
