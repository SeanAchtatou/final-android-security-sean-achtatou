package com.e.b;

import android.graphics.Bitmap;
import android.net.NetworkInfo;
import java.io.InputStream;

class ai extends bc {

    /* renamed from: a  reason: collision with root package name */
    private final w f779a;

    /* renamed from: b  reason: collision with root package name */
    private final bf f780b;

    public ai(w wVar, bf bfVar) {
        this.f779a = wVar;
        this.f780b = bfVar;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return 2;
    }

    public bd a(ay ayVar, int i) {
        x a2 = this.f779a.a(ayVar.d, ayVar.c);
        if (a2 == null) {
            return null;
        }
        ar arVar = a2.c ? ar.DISK : ar.NETWORK;
        Bitmap b2 = a2.b();
        if (b2 != null) {
            return new bd(b2, arVar);
        }
        InputStream a3 = a2.a();
        if (a3 == null) {
            return null;
        }
        if (arVar == ar.DISK && a2.c() == 0) {
            bn.a(a3);
            throw new aj("Received response with 0 content-length header.");
        }
        if (arVar == ar.NETWORK && a2.c() > 0) {
            this.f780b.a(a2.c());
        }
        return new bd(a3, arVar);
    }

    public boolean a(ay ayVar) {
        String scheme = ayVar.d.getScheme();
        return "http".equals(scheme) || "https".equals(scheme);
    }

    /* access modifiers changed from: package-private */
    public boolean a(boolean z, NetworkInfo networkInfo) {
        return networkInfo == null || networkInfo.isConnected();
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return true;
    }
}
