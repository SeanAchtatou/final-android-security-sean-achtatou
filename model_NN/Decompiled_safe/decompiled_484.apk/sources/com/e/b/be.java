package com.e.b;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

class be extends bc {

    /* renamed from: a  reason: collision with root package name */
    private final Context f807a;

    be(Context context) {
        this.f807a = context;
    }

    private static Bitmap a(Resources resources, int i, ay ayVar) {
        BitmapFactory.Options c = c(ayVar);
        if (a(c)) {
            BitmapFactory.decodeResource(resources, i, c);
            a(ayVar.h, ayVar.i, c, ayVar);
        }
        return BitmapFactory.decodeResource(resources, i, c);
    }

    public bd a(ay ayVar, int i) {
        Resources a2 = bn.a(this.f807a, ayVar);
        return new bd(a(a2, bn.a(a2, ayVar), ayVar), ar.DISK);
    }

    public boolean a(ay ayVar) {
        if (ayVar.e != 0) {
            return true;
        }
        return "android.resource".equals(ayVar.d.getScheme());
    }
}
