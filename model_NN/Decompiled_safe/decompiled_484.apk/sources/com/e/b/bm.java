package com.e.b;

import android.content.Context;
import android.net.http.HttpResponseCache;
import java.io.File;

class bm {
    static Object a(Context context) {
        File b2 = bn.b(context);
        HttpResponseCache installed = HttpResponseCache.getInstalled();
        return installed == null ? HttpResponseCache.install(b2, bn.a(b2)) : installed;
    }
}
