package com.e.b;

import android.os.Handler;
import android.os.Message;
import android.os.Process;
import java.lang.ref.ReferenceQueue;

class ao extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private final ReferenceQueue<Object> f786a;

    /* renamed from: b  reason: collision with root package name */
    private final Handler f787b;

    ao(ReferenceQueue<Object> referenceQueue, Handler handler) {
        this.f786a = referenceQueue;
        this.f787b = handler;
        setDaemon(true);
        setName("Picasso-refQueue");
    }

    public void run() {
        Process.setThreadPriority(10);
        while (true) {
            try {
                b bVar = (b) this.f786a.remove(1000);
                Message obtainMessage = this.f787b.obtainMessage();
                if (bVar != null) {
                    obtainMessage.what = 3;
                    obtainMessage.obj = bVar.f800a;
                    this.f787b.sendMessage(obtainMessage);
                } else {
                    obtainMessage.recycle();
                }
            } catch (InterruptedException e) {
                return;
            } catch (Exception e2) {
                this.f787b.post(new ap(this, e2));
                return;
            }
        }
    }
}
