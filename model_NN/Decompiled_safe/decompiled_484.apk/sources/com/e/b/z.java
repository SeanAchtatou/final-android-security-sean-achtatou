package com.e.b;

import android.content.Context;
import android.media.ExifInterface;
import android.net.Uri;

class z extends p {
    z(Context context) {
        super(context);
    }

    static int a(Uri uri) {
        switch (new ExifInterface(uri.getPath()).getAttributeInt("Orientation", 1)) {
            case 3:
                return 180;
            case 4:
            case 5:
            case 7:
            default:
                return 0;
            case 6:
                return 90;
            case 8:
                return 270;
        }
    }

    public bd a(ay ayVar, int i) {
        return new bd(null, b(ayVar), ar.DISK, a(ayVar.d));
    }

    public boolean a(ay ayVar) {
        return "file".equals(ayVar.d.getScheme());
    }
}
