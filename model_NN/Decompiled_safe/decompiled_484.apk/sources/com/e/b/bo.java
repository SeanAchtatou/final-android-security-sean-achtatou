package com.e.b;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class bo extends Handler {
    bo(Looper looper) {
        super(looper);
    }

    public void handleMessage(Message message) {
        sendMessageDelayed(obtainMessage(), 1000);
    }
}
