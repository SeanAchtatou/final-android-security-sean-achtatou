package com.amap.api.location;

import com.amap.api.location.core.AMapLocException;

public class AMapLocalWeatherLive {

    /* renamed from: a  reason: collision with root package name */
    private String f353a;

    /* renamed from: b  reason: collision with root package name */
    private String f354b;
    private String c;
    private String d;
    private String e;
    private String f;
    private AMapLocException g;
    private String h;
    private String i;
    private String j;

    /* access modifiers changed from: package-private */
    public void a(AMapLocException aMapLocException) {
        this.g = aMapLocException;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.f353a = str;
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.f354b = str;
    }

    /* access modifiers changed from: package-private */
    public void c(String str) {
        this.c = str;
    }

    /* access modifiers changed from: package-private */
    public void d(String str) {
        this.d = str;
    }

    /* access modifiers changed from: package-private */
    public void e(String str) {
        this.e = str;
    }

    /* access modifiers changed from: package-private */
    public void f(String str) {
        this.f = str;
    }

    public AMapLocException getAMapException() {
        return this.g;
    }

    public String getCity() {
        return this.h;
    }

    public String getCityCode() {
        return this.j;
    }

    public String getHumidity() {
        return this.e;
    }

    public String getProvince() {
        return this.i;
    }

    public String getReportTime() {
        return this.f;
    }

    public String getTemperature() {
        return this.f354b;
    }

    public String getWeather() {
        return this.f353a;
    }

    public String getWindDir() {
        return this.c;
    }

    public String getWindPower() {
        return this.d;
    }

    public void setCity(String str) {
        this.h = str;
    }

    public void setCityCode(String str) {
        this.j = str;
    }

    public void setProvince(String str) {
        this.i = str;
    }
}
