package com.amap.api.location;

import android.content.Context;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Looper;
import com.amap.api.location.a;

public class d {

    /* renamed from: a  reason: collision with root package name */
    public LocationManager f389a = null;

    /* renamed from: b  reason: collision with root package name */
    LocationListener f390b = new e(this);
    /* access modifiers changed from: private */
    public a.C0000a c;
    /* access modifiers changed from: private */
    public a d;
    private Context e;

    d(Context context, LocationManager locationManager, a.C0000a aVar, a aVar2) {
        this.e = context;
        this.f389a = locationManager;
        this.d = aVar2;
        this.c = aVar;
    }

    /* access modifiers changed from: package-private */
    public void a() {
    }

    /* access modifiers changed from: package-private */
    public void a(long j, float f) {
        try {
            Looper mainLooper = this.e.getMainLooper();
            if (Looper.myLooper() == null) {
                Looper.prepare();
            }
            this.f389a.requestLocationUpdates(LocationManagerProxy.GPS_PROVIDER, j, f, this.f390b, mainLooper);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.f390b != null) {
            this.f389a.removeUpdates(this.f390b);
        }
    }
}
