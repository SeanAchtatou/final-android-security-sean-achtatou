package com.amap.api.location.core;

import android.content.Context;
import android.util.Log;
import com.aps.l;
import com.qihoo.dynamic.util.Md5Util;
import java.util.Arrays;
import org.json.JSONObject;

public class a {

    /* renamed from: a  reason: collision with root package name */
    static String f379a = "";

    /* renamed from: b  reason: collision with root package name */
    private static int f380b = -1;

    public static int a() {
        return f380b;
    }

    public static String a(String str) {
        String[] split = str.split("&");
        Arrays.sort(split);
        StringBuffer stringBuffer = new StringBuffer();
        for (String append : split) {
            stringBuffer.append(append);
            stringBuffer.append("&");
        }
        String stringBuffer2 = stringBuffer.toString();
        return stringBuffer2.length() > 1 ? (String) stringBuffer2.subSequence(0, stringBuffer2.length() - 1) : str;
    }

    public static synchronized boolean a(Context context) {
        boolean z;
        synchronized (a.class) {
            try {
                String a2 = l.a().a(context, b(), c(), "loc");
                if (a2 != null) {
                    z = c(a2);
                } else {
                    f380b = 0;
                    z = true;
                }
                if (f380b != 1) {
                    f380b = 0;
                }
            } catch (Throwable th) {
                if (f380b != 1) {
                    f380b = 0;
                }
                throw th;
            }
        }
        return z;
    }

    private static String b() {
        return "http://apiinit.amap.com/v3/log/init";
    }

    public static String b(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(str);
        String a2 = d.a();
        stringBuffer.append("&ts=" + a2);
        stringBuffer.append("&scode=" + d.a(a2, str));
        return stringBuffer.toString();
    }

    private static boolean c(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("status")) {
                int i = jSONObject.getInt("status");
                if (i == 1) {
                    f380b = 1;
                } else if (i == 0) {
                    f380b = 0;
                }
            }
            if (jSONObject.has("info")) {
                f379a = jSONObject.getString("info");
            }
            if (f380b == 0) {
                Log.i("AuthFailure", f379a);
            }
        } catch (Exception e) {
            e.printStackTrace();
            f380b = 0;
        }
        return f380b == 1;
    }

    private static byte[] c() {
        try {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("resType=json&encode=UTF-8&ec=1");
            return b(a(stringBuffer.toString())).toString().getBytes(Md5Util.DEFAULT_CHARSET);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
