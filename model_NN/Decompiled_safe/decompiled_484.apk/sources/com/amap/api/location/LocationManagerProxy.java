package com.amap.api.location;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import com.amap.api.location.core.d;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class LocationManagerProxy {
    public static final String GPS_PROVIDER = "gps";
    public static final String KEY_LOCATION_CHANGED = "location";
    public static final String KEY_PROVIDER_ENABLED = "providerEnabled";
    public static final String KEY_PROXIMITY_ENTERING = "entering";
    public static final String KEY_STATUS_CHANGED = "status";
    public static final String NETWORK_PROVIDER = "network";
    public static final int WEATHER_TYPE_FORECAST = 2;
    public static final int WEATHER_TYPE_LIVE = 1;

    /* renamed from: a  reason: collision with root package name */
    static Object f357a = new Object();
    private static LocationManagerProxy c = null;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public LocationManager f358b = null;
    private a d = null;
    /* access modifiers changed from: private */
    public Context e;
    private f f;
    private b g;
    /* access modifiers changed from: private */
    public ArrayList<PendingIntent> h = new ArrayList<>();
    private Hashtable<String, LocationProviderProxy> i = new Hashtable<>();
    /* access modifiers changed from: private */
    public Vector<g> j = new Vector<>();
    /* access modifiers changed from: private */
    public Vector<g> k = new Vector<>();
    /* access modifiers changed from: private */
    public a l = new a();

    class a implements AMapLocationListener {
        a() {
        }

        public void onLocationChanged(Location location) {
            if (location != null) {
                try {
                    AMapLocation aMapLocation = new AMapLocation(location);
                    int i = 0;
                    while (LocationManagerProxy.this.j != null && i < LocationManagerProxy.this.j.size()) {
                        g gVar = (g) LocationManagerProxy.this.j.get(i);
                        if (!(gVar == null || gVar.f395b == null)) {
                            gVar.f395b.onLocationChanged(aMapLocation);
                        }
                        if (!(gVar == null || gVar.f394a != -1 || LocationManagerProxy.this.k == null)) {
                            LocationManagerProxy.this.k.add(gVar);
                        }
                        i++;
                    }
                    if (LocationManagerProxy.this.k != null && LocationManagerProxy.this.k.size() > 0 && LocationManagerProxy.this.j != null) {
                        for (int i2 = 0; i2 < LocationManagerProxy.this.k.size(); i2++) {
                            LocationManagerProxy.this.j.remove(LocationManagerProxy.this.k.get(i2));
                        }
                        LocationManagerProxy.this.k.clear();
                        if (LocationManagerProxy.this.j.size() == 0 && LocationManagerProxy.this.f358b != null && LocationManagerProxy.this.l != null) {
                            LocationManagerProxy.this.f358b.removeUpdates(LocationManagerProxy.this.l);
                        }
                    }
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            } else {
                int i3 = 0;
                while (LocationManagerProxy.this.j != null && i3 < LocationManagerProxy.this.j.size()) {
                    g gVar2 = (g) LocationManagerProxy.this.j.get(i3);
                    if (!(gVar2 == null || gVar2.f394a != -1 || LocationManagerProxy.this.k == null)) {
                        LocationManagerProxy.this.k.add(gVar2);
                    }
                    i3++;
                }
                if (LocationManagerProxy.this.k != null && LocationManagerProxy.this.k.size() > 0 && LocationManagerProxy.this.j != null) {
                    for (int i4 = 0; i4 < LocationManagerProxy.this.k.size(); i4++) {
                        LocationManagerProxy.this.j.remove(LocationManagerProxy.this.k.get(i4));
                    }
                    LocationManagerProxy.this.k.clear();
                    if (LocationManagerProxy.this.j.size() == 0 && LocationManagerProxy.this.f358b != null && LocationManagerProxy.this.l != null) {
                        LocationManagerProxy.this.f358b.removeUpdates(LocationManagerProxy.this.l);
                    }
                }
            }
        }

        public void onLocationChanged(AMapLocation aMapLocation) {
        }

        public void onProviderDisabled(String str) {
        }

        public void onProviderEnabled(String str) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }
    }

    class b implements AMapLocationListener {
        b() {
        }

        public void onLocationChanged(Location location) {
            try {
                if (LocationManagerProxy.this.h != null && LocationManagerProxy.this.h.size() > 0) {
                    Iterator it = LocationManagerProxy.this.h.iterator();
                    while (it.hasNext()) {
                        PendingIntent pendingIntent = (PendingIntent) it.next();
                        Intent intent = new Intent();
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(LocationManagerProxy.KEY_LOCATION_CHANGED, location);
                        intent.putExtras(bundle);
                        pendingIntent.send(LocationManagerProxy.this.e, 0, intent);
                    }
                }
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }

        public void onLocationChanged(AMapLocation aMapLocation) {
            try {
                if (LocationManagerProxy.this.h != null && LocationManagerProxy.this.h.size() > 0) {
                    Iterator it = LocationManagerProxy.this.h.iterator();
                    while (it.hasNext()) {
                        PendingIntent pendingIntent = (PendingIntent) it.next();
                        Intent intent = new Intent();
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(LocationManagerProxy.KEY_LOCATION_CHANGED, aMapLocation);
                        intent.putExtras(bundle);
                        pendingIntent.send(LocationManagerProxy.this.e, 0, intent);
                    }
                }
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }

        public void onProviderDisabled(String str) {
        }

        public void onProviderEnabled(String str) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }
    }

    private LocationManagerProxy(Activity activity) {
        a(activity.getApplicationContext());
    }

    private LocationManagerProxy(Context context) {
        a(context);
    }

    private static void a() {
        c = null;
    }

    private void a(Context context) {
        try {
            this.e = context;
            this.f358b = (LocationManager) context.getSystemService(KEY_LOCATION_CHANGED);
            this.d = a.a(context.getApplicationContext(), this.f358b);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    private synchronized void a(String str, long j2, float f2, AMapLocationListener aMapLocationListener, boolean z) {
        try {
            if (this.d == null) {
                this.d = a.a(this.e.getApplicationContext(), this.f358b);
            }
            String str2 = str == null ? LocationProviderProxy.AMapNetwork : str;
            if (LocationProviderProxy.AMapNetwork.equals(str2)) {
                if (this.d != null) {
                    this.d.a(j2, f2, aMapLocationListener, LocationProviderProxy.AMapNetwork, z);
                }
            } else if (!GPS_PROVIDER.equals(str2)) {
                Looper mainLooper = this.e.getMainLooper();
                if (Looper.myLooper() == null) {
                    Looper.prepare();
                }
                this.j.add(new g(j2, f2, aMapLocationListener, str2, false));
                this.f358b.requestLocationUpdates(str2, j2, f2, this.l, mainLooper);
            } else if (this.d != null) {
                this.d.a(j2, f2, aMapLocationListener, GPS_PROVIDER, z);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return;
    }

    public static LocationManagerProxy getInstance(Activity activity) {
        try {
            synchronized (f357a) {
                if (c == null) {
                    c = new LocationManagerProxy(activity);
                }
            }
            return c;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public static synchronized LocationManagerProxy getInstance(Context context) {
        LocationManagerProxy locationManagerProxy;
        synchronized (LocationManagerProxy.class) {
            try {
                if (c == null) {
                    c = new LocationManagerProxy(context);
                }
                locationManagerProxy = c;
            } catch (Throwable th) {
                th.printStackTrace();
                locationManagerProxy = null;
            }
        }
        return locationManagerProxy;
    }

    public static String getVersion() {
        return "V1.3.1";
    }

    public void addGeoFenceAlert(double d2, double d3, float f2, long j2, PendingIntent pendingIntent) {
        try {
            if (this.d != null) {
                this.d.b(d2, d3, f2, j2, pendingIntent);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public boolean addGpsStatusListener(GpsStatus.Listener listener) {
        try {
            if (this.f358b != null) {
                return this.f358b.addGpsStatusListener(listener);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return false;
    }

    public void addProximityAlert(double d2, double d3, float f2, long j2, PendingIntent pendingIntent) {
        try {
            if (this.d.f) {
                this.f358b.addProximityAlert(d2, d3, f2, j2, pendingIntent);
            }
            this.d.a(d2, d3, f2, j2, pendingIntent);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void addTestProvider(String str, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, int i2, int i3) {
        try {
            if (this.f358b != null) {
                this.f358b.addTestProvider(str, z, z2, z3, z4, z5, z6, z7, i2, i3);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void clearTestProviderEnabled(String str) {
        try {
            if (this.f358b != null) {
                this.f358b.clearTestProviderEnabled(str);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void clearTestProviderLocation(String str) {
        try {
            if (this.f358b != null) {
                this.f358b.clearTestProviderLocation(str);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void clearTestProviderStatus(String str) {
        try {
            if (this.f358b != null) {
                this.f358b.clearTestProviderStatus(str);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    @Deprecated
    public void destory() {
        try {
            destroy();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void destroy() {
        try {
            synchronized (f357a) {
                a.c();
                if (this.i != null) {
                    this.i.clear();
                }
                if (this.j != null) {
                    this.j.clear();
                }
                if (this.f358b != null) {
                    if (this.l != null) {
                        this.f358b.removeUpdates(this.l);
                    }
                    if (this.h != null) {
                        for (int i2 = 0; i2 < this.h.size(); i2++) {
                            PendingIntent pendingIntent = this.h.get(i2);
                            if (pendingIntent != null) {
                                this.f358b.removeUpdates(pendingIntent);
                            }
                        }
                    }
                }
                if (this.h != null) {
                    this.h.clear();
                }
                this.d = null;
                a();
                this.l = null;
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public List<String> getAllProviders() {
        try {
            List<String> allProviders = this.f358b.getAllProviders();
            if (allProviders == null) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(LocationProviderProxy.AMapNetwork);
                arrayList.addAll(this.f358b.getAllProviders());
                return arrayList;
            } else if (allProviders.contains(LocationProviderProxy.AMapNetwork)) {
                return allProviders;
            } else {
                allProviders.add(LocationProviderProxy.AMapNetwork);
                return allProviders;
            }
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public String getBestProvider(Criteria criteria, boolean z) {
        String str = LocationProviderProxy.AMapNetwork;
        if (criteria == null) {
            return str;
        }
        try {
            if (!getProvider(LocationProviderProxy.AMapNetwork).meetsCriteria(criteria)) {
                str = this.f358b.getBestProvider(criteria, z);
            }
            return (!z || d.a(this.e)) ? str : this.f358b.getBestProvider(criteria, z);
        } catch (Throwable th) {
            th.printStackTrace();
            return GPS_PROVIDER;
        }
    }

    public GpsStatus getGpsStatus(GpsStatus gpsStatus) {
        try {
            if (this.f358b != null) {
                return this.f358b.getGpsStatus(gpsStatus);
            }
            return null;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public AMapLocation getLastKnownLocation(String str) {
        Location lastKnownLocation;
        try {
            if (this.d == null) {
                return null;
            }
            if (LocationProviderProxy.AMapNetwork.equals(str)) {
                return this.d.a();
            }
            if (this.f358b == null || (lastKnownLocation = this.f358b.getLastKnownLocation(str)) == null) {
                return null;
            }
            return new AMapLocation(lastKnownLocation);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public LocationProviderProxy getProvider(String str) {
        if (str == null) {
            try {
                throw new IllegalArgumentException("name不能为空！");
            } catch (Throwable th) {
                th.printStackTrace();
                return null;
            }
        } else if (this.i.containsKey(str)) {
            return this.i.get(str);
        } else {
            LocationProviderProxy a2 = LocationProviderProxy.a(this.f358b, str);
            this.i.put(str, a2);
            return a2;
        }
    }

    public List<String> getProviders(Criteria criteria, boolean z) {
        try {
            List<String> providers = this.f358b.getProviders(criteria, z);
            if (providers == null || providers.size() == 0) {
                providers = new ArrayList<>();
            }
            if (!LocationProviderProxy.AMapNetwork.equals(getBestProvider(criteria, z))) {
                return providers;
            }
            providers.add(LocationProviderProxy.AMapNetwork);
            return providers;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public List<String> getProviders(boolean z) {
        try {
            List<String> providers = this.f358b.getProviders(z);
            if (!isProviderEnabled(LocationProviderProxy.AMapNetwork)) {
                return providers;
            }
            if (providers == null || providers.size() == 0) {
                providers = new ArrayList<>();
            }
            providers.add(LocationProviderProxy.AMapNetwork);
            return providers;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public boolean isProviderEnabled(String str) {
        try {
            return LocationProviderProxy.AMapNetwork.equals(str) ? d.a(this.e) : this.f358b.isProviderEnabled(str);
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }

    public void removeGeoFenceAlert(PendingIntent pendingIntent) {
        try {
            if (this.d != null) {
                this.d.b(pendingIntent);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void removeGpsStatusListener(GpsStatus.Listener listener) {
        try {
            if (this.f358b != null) {
                this.f358b.removeGpsStatusListener(listener);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void removeProximityAlert(PendingIntent pendingIntent) {
        try {
            if (!(this.d == null || !this.d.f || this.f358b == null)) {
                this.f358b.removeProximityAlert(pendingIntent);
            }
            if (this.d != null) {
                this.d.a(pendingIntent);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void removeUpdates(PendingIntent pendingIntent) {
        try {
            if (this.f != null) {
                this.h.remove(pendingIntent);
                if (this.h.size() == 0) {
                    this.f.a();
                }
            }
            this.f = null;
            this.f358b.removeUpdates(pendingIntent);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public synchronized void removeUpdates(AMapLocationListener aMapLocationListener) {
        int i2;
        if (aMapLocationListener != null) {
            try {
                if (this.d != null) {
                    this.d.a(aMapLocationListener);
                }
                this.f358b.removeUpdates(aMapLocationListener);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        if (this.j != null && this.j.size() > 0) {
            int size = this.j.size();
            int i3 = 0;
            while (i3 < size) {
                g gVar = this.j.get(i3);
                if (aMapLocationListener.equals(gVar.f395b)) {
                    this.j.remove(gVar);
                    size--;
                    i2 = i3 - 1;
                } else {
                    i2 = i3;
                }
                size = size;
                i3 = i2 + 1;
            }
            if (this.j.size() == 0 && this.l != null) {
                this.f358b.removeUpdates(this.l);
            }
        }
        return;
    }

    public synchronized void requestLocationData(String str, long j2, float f2, AMapLocationListener aMapLocationListener) {
        a(str, j2, f2, aMapLocationListener, true);
    }

    public void requestLocationUpdates(String str, long j2, float f2, PendingIntent pendingIntent) {
        try {
            if (LocationProviderProxy.AMapNetwork.equals(str)) {
                if (this.f == null) {
                    this.f = new f(this);
                }
                if (this.g == null) {
                    this.g = new b();
                }
                this.f.a(this.g, j2, f2, str);
                this.h.add(pendingIntent);
                return;
            }
            this.h.add(pendingIntent);
            this.f358b.requestLocationUpdates(str, j2, f2, pendingIntent);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    @Deprecated
    public synchronized void requestLocationUpdates(String str, long j2, float f2, AMapLocationListener aMapLocationListener) {
        a(str, j2, f2, aMapLocationListener, false);
    }

    public void requestWeatherUpdates(int i2, AMapLocalWeatherListener aMapLocalWeatherListener) {
        try {
            this.d.a(i2, aMapLocalWeatherListener);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void setGpsEnable(boolean z) {
        try {
            if (this.d != null) {
                this.d.a(z);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void setTestProviderEnabled(String str, boolean z) {
        try {
            if (this.f358b != null) {
                this.f358b.setTestProviderEnabled(str, z);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void setTestProviderLocation(String str, Location location) {
        try {
            if (this.f358b != null) {
                this.f358b.setTestProviderLocation(str, location);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void setTestProviderStatus(String str, int i2, Bundle bundle, long j2) {
        try {
            if (this.f358b != null) {
                this.f358b.setTestProviderStatus(str, i2, bundle, j2);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
