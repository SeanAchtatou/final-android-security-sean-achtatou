package com.amap.api.location;

public class g {

    /* renamed from: a  reason: collision with root package name */
    long f394a;

    /* renamed from: b  reason: collision with root package name */
    public AMapLocationListener f395b;
    Boolean c;

    public g(long j, float f, AMapLocationListener aMapLocationListener, String str, boolean z) {
        this.f394a = j;
        this.f395b = aMapLocationListener;
        this.c = Boolean.valueOf(z);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        g gVar = (g) obj;
        return this.f395b == null ? gVar.f395b == null : this.f395b.equals(gVar.f395b);
    }

    public int hashCode() {
        return (this.f395b == null ? 0 : this.f395b.hashCode()) + 31;
    }
}
