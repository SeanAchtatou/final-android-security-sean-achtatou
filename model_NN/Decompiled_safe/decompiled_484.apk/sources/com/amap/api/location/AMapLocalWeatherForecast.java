package com.amap.api.location;

import com.amap.api.location.core.AMapLocException;
import java.util.List;

public class AMapLocalWeatherForecast {

    /* renamed from: a  reason: collision with root package name */
    private String f351a;

    /* renamed from: b  reason: collision with root package name */
    private List<AMapLocalDayWeatherForecast> f352b;
    private AMapLocException c;

    /* access modifiers changed from: package-private */
    public void a(AMapLocException aMapLocException) {
        this.c = aMapLocException;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.f351a = str;
    }

    /* access modifiers changed from: package-private */
    public void a(List<AMapLocalDayWeatherForecast> list) {
        this.f352b = list;
    }

    public AMapLocException getAMapException() {
        return this.c;
    }

    public String getReportTime() {
        return this.f351a;
    }

    public List<AMapLocalDayWeatherForecast> getWeatherForecast() {
        return this.f352b;
    }
}
