package com.amap.api.location;

import android.app.PendingIntent;
import android.content.Context;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.amap.api.location.core.d;
import com.aps.j;
import java.util.Iterator;
import java.util.Vector;

public class a {
    static a h = null;

    /* renamed from: a  reason: collision with root package name */
    d f363a = null;

    /* renamed from: b  reason: collision with root package name */
    c f364b = null;
    boolean c = false;
    long d;
    boolean e = true;
    boolean f = true;
    b g;
    /* access modifiers changed from: private */
    public Context i;
    /* access modifiers changed from: private */
    public Vector<g> j = null;
    private C0000a k = null;
    /* access modifiers changed from: private */
    public Vector<g> l = new Vector<>();
    /* access modifiers changed from: private */
    public AMapLocation m;
    /* access modifiers changed from: private */
    public AMapLocation n;
    private volatile Thread o;
    private long p = 2000;
    private float q = 10.0f;

    /* renamed from: com.amap.api.location.a$a  reason: collision with other inner class name */
    class C0000a extends Handler {
        public C0000a() {
        }

        public C0000a(Looper looper) {
            super(looper);
            Looper.prepare();
        }

        public void handleMessage(Message message) {
            if (message != null) {
                try {
                    if (message.what == 100 && a.this.j != null) {
                        AMapLocation unused = a.this.m = (AMapLocation) message.obj;
                        if (!(a.this.m == null || a.this.m.getAdCode() == null || a.this.m.getAdCode().length() <= 0)) {
                            AMapLocation unused2 = a.this.n = a.this.m;
                        }
                        Iterator it = a.this.j.iterator();
                        while (it.hasNext()) {
                            g gVar = (g) it.next();
                            if (gVar.f395b != null) {
                                AMapLocation aMapLocation = (AMapLocation) message.obj;
                                if (gVar.c.booleanValue() || aMapLocation.getAMapException().getErrorCode() == 0) {
                                    gVar.f395b.onLocationChanged(aMapLocation);
                                    if (gVar.c.booleanValue() && gVar.f394a == -1 && a.this.l != null) {
                                        a.this.l.add(gVar);
                                    }
                                }
                            }
                        }
                        if (a.this.l != null && a.this.l.size() > 0) {
                            for (int i = 0; i < a.this.l.size(); i++) {
                                a.this.a(((g) a.this.l.get(i)).f395b);
                            }
                            a.this.l.clear();
                        }
                        if (a.this.m != null) {
                            d.a(a.this.i, a.this.m);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (Throwable th) {
                }
            }
        }
    }

    private a(Context context, LocationManager locationManager) {
        this.i = context;
        e();
        if (Looper.myLooper() == null) {
            this.k = new C0000a(context.getMainLooper());
        } else {
            this.k = new C0000a();
        }
        this.f363a = new d(context, locationManager, this.k, this);
        this.f364b = new c(context, this.k, this);
        b(false);
        this.e = true;
        this.f = true;
        this.g = new b(this, context);
    }

    public static synchronized a a(Context context, LocationManager locationManager) {
        a aVar;
        synchronized (a.class) {
            if (h == null) {
                h = new a(context, locationManager);
            }
            aVar = h;
        }
        return aVar;
    }

    static synchronized void c() {
        synchronized (a.class) {
            if (h != null) {
                h.d();
            }
            h = null;
        }
    }

    private void c(boolean z) {
        this.e = z;
    }

    private void d(boolean z) {
        this.f = z;
    }

    private void e() {
        this.j = new Vector<>();
    }

    /* access modifiers changed from: package-private */
    public AMapLocation a() {
        return this.m != null ? this.m : d.b(this.i);
    }

    /* access modifiers changed from: package-private */
    public void a(double d2, double d3, float f2, long j2, PendingIntent pendingIntent) {
        j jVar = new j();
        jVar.f475b = d2;
        jVar.f474a = d3;
        jVar.c = f2;
        jVar.a(j2);
        this.f364b.a(jVar, pendingIntent);
    }

    /* access modifiers changed from: package-private */
    public void a(final int i2, final AMapLocalWeatherListener aMapLocalWeatherListener) {
        try {
            new Thread() {
                public void run() {
                    a.this.g.a(i2, aMapLocalWeatherListener, a.this.n);
                }
            }.start();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(long j2, float f2, AMapLocationListener aMapLocationListener, String str, boolean z) {
        this.p = j2;
        this.q = f2;
        if (aMapLocationListener != null) {
            this.j.add(new g(j2, f2, aMapLocationListener, str, z));
            if (LocationManagerProxy.GPS_PROVIDER.equals(str)) {
                this.f363a.a(j2, f2);
            } else if (LocationProviderProxy.AMapNetwork.equals(str)) {
                if (this.f) {
                    this.f363a.a(j2, f2);
                }
                this.f364b.a(j2);
                c(true);
                if (this.o == null) {
                    this.f364b.b(true);
                    this.o = new Thread(this.f364b);
                    this.o.start();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(PendingIntent pendingIntent) {
        this.f364b.a(pendingIntent);
    }

    /* access modifiers changed from: package-private */
    public void a(AMapLocationListener aMapLocationListener) {
        int i2;
        int i3 = 0;
        int size = this.j != null ? this.j.size() : 0;
        while (i3 < size) {
            g gVar = this.j.get(i3);
            if (gVar == null) {
                this.j.remove(i3);
                size--;
                i2 = i3 - 1;
            } else if (gVar.f395b == null || aMapLocationListener.equals(gVar.f395b)) {
                this.j.remove(gVar);
                size--;
                i2 = i3 - 1;
            } else {
                i2 = i3;
            }
            size = size;
            i3 = i2 + 1;
        }
        if (this.j == null || this.j.size() == 0) {
            b(false);
            c(false);
            b();
            if (this.f363a != null) {
                this.f363a.b();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        d(z);
        if (this.j != null && this.j.size() > 0) {
            if (z) {
                this.f363a.b();
                this.f363a.a(this.p, this.q);
                return;
            }
            this.f363a.b();
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.f364b != null) {
            this.f364b.b(false);
        }
        if (this.o != null) {
            this.o.interrupt();
            this.o = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void b(double d2, double d3, float f2, long j2, PendingIntent pendingIntent) {
        j jVar = new j();
        jVar.f475b = d2;
        jVar.f474a = d3;
        jVar.c = f2;
        jVar.a(j2);
        this.f364b.b(jVar, pendingIntent);
    }

    /* access modifiers changed from: package-private */
    public void b(PendingIntent pendingIntent) {
        this.f364b.b(pendingIntent);
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z) {
        this.c = z;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (this.f363a != null) {
            this.f363a.b();
            this.f363a.a();
            this.f363a = null;
        }
        if (this.f364b != null) {
            this.f364b.b();
        }
        if (this.j != null) {
            this.j.clear();
        }
        b(false);
    }
}
