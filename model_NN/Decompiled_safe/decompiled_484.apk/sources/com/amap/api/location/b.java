package com.amap.api.location;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.amap.api.location.core.AMapLocException;
import com.amap.api.location.core.c;
import com.amap.api.location.core.d;
import com.aps.l;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class b implements AMapLocationListener {

    /* renamed from: a  reason: collision with root package name */
    a f368a = null;

    /* renamed from: b  reason: collision with root package name */
    AMapLocalWeatherListener f369b;
    a c;
    private Context d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public AMapLocalWeatherListener f;

    class a extends Handler {

        /* renamed from: a  reason: collision with root package name */
        private WeakReference<b> f370a;

        a(b bVar, Looper looper) {
            super(looper);
            try {
                this.f370a = new WeakReference<>(bVar);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }

        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void handleMessage(android.os.Message r4) {
            /*
                r3 = this;
                super.handleMessage(r4)     // Catch:{ Throwable -> 0x001f }
                java.lang.ref.WeakReference<com.amap.api.location.b> r0 = r3.f370a     // Catch:{ Throwable -> 0x001f }
                java.lang.Object r0 = r0.get()     // Catch:{ Throwable -> 0x001f }
                com.amap.api.location.b r0 = (com.amap.api.location.b) r0     // Catch:{ Throwable -> 0x001f }
                int r1 = r4.what     // Catch:{ Throwable -> 0x001f }
                switch(r1) {
                    case 1: goto L_0x0011;
                    case 2: goto L_0x0024;
                    case 3: goto L_0x0032;
                    default: goto L_0x0010;
                }     // Catch:{ Throwable -> 0x001f }
            L_0x0010:
                return
            L_0x0011:
                com.amap.api.location.AMapLocalWeatherListener r1 = r0.f369b     // Catch:{ Throwable -> 0x001f }
                if (r1 == 0) goto L_0x0010
                com.amap.api.location.AMapLocalWeatherListener r1 = r0.f369b     // Catch:{ Throwable -> 0x001f }
                java.lang.Object r0 = r4.obj     // Catch:{ Throwable -> 0x001f }
                com.amap.api.location.AMapLocalWeatherLive r0 = (com.amap.api.location.AMapLocalWeatherLive) r0     // Catch:{ Throwable -> 0x001f }
                r1.onWeatherLiveSearched(r0)     // Catch:{ Throwable -> 0x001f }
                goto L_0x0010
            L_0x001f:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0010
            L_0x0024:
                com.amap.api.location.AMapLocalWeatherListener r1 = r0.f369b     // Catch:{ Throwable -> 0x001f }
                if (r1 == 0) goto L_0x0010
                com.amap.api.location.AMapLocalWeatherListener r1 = r0.f369b     // Catch:{ Throwable -> 0x001f }
                java.lang.Object r0 = r4.obj     // Catch:{ Throwable -> 0x001f }
                com.amap.api.location.AMapLocalWeatherForecast r0 = (com.amap.api.location.AMapLocalWeatherForecast) r0     // Catch:{ Throwable -> 0x001f }
                r1.onWeatherForecaseSearched(r0)     // Catch:{ Throwable -> 0x001f }
                goto L_0x0010
            L_0x0032:
                java.lang.Object r1 = r4.obj     // Catch:{ Throwable -> 0x003f }
                com.amap.api.location.AMapLocation r1 = (com.amap.api.location.AMapLocation) r1     // Catch:{ Throwable -> 0x003f }
                com.amap.api.location.b$a$1 r2 = new com.amap.api.location.b$a$1     // Catch:{ Throwable -> 0x003f }
                r2.<init>(r0, r1)     // Catch:{ Throwable -> 0x003f }
                r2.start()     // Catch:{ Throwable -> 0x003f }
                goto L_0x0010
            L_0x003f:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ Throwable -> 0x001f }
                goto L_0x0010
            */
            throw new UnsupportedOperationException("Method not decompiled: com.amap.api.location.b.a.handleMessage(android.os.Message):void");
        }
    }

    public b(a aVar, Context context) {
        this.d = context;
        this.c = aVar;
        this.f368a = new a(this, context.getMainLooper());
    }

    private AMapLocalWeatherLive a(String str, AMapLocation aMapLocation) {
        AMapLocalWeatherLive aMapLocalWeatherLive = new AMapLocalWeatherLive();
        try {
            d.a(str);
        } catch (AMapLocException e2) {
            aMapLocalWeatherLive.a(e2);
        }
        try {
            JSONArray jSONArray = new JSONObject(str).getJSONArray("lives");
            if (jSONArray != null && jSONArray.length() > 0) {
                JSONObject jSONObject = (JSONObject) jSONArray.get(0);
                String a2 = a(jSONObject, "weather");
                String a3 = a(jSONObject, "temperature");
                String a4 = a(jSONObject, "winddirection");
                String a5 = a(jSONObject, "windpower");
                String a6 = a(jSONObject, "humidity");
                String a7 = a(jSONObject, "reporttime");
                aMapLocalWeatherLive.a(a2);
                aMapLocalWeatherLive.f(a7);
                aMapLocalWeatherLive.e(a6);
                aMapLocalWeatherLive.b(a3);
                aMapLocalWeatherLive.c(a4);
                aMapLocalWeatherLive.d(a5);
                aMapLocalWeatherLive.setCity(aMapLocation.getCity());
                aMapLocalWeatherLive.setCityCode(aMapLocation.getCityCode());
                aMapLocalWeatherLive.setProvince(aMapLocation.getProvince());
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        return aMapLocalWeatherLive;
    }

    private String a() {
        return "http://restapi.amap.com/v3/weather/weatherInfo?";
    }

    private byte[] a(AMapLocation aMapLocation, String str) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("output=json&ec=1").append("&extensions=" + str).append("&city=").append(aMapLocation.getAdCode());
        stringBuffer.append("&key=" + c.a());
        return com.amap.api.location.core.a.b(com.amap.api.location.core.a.a(stringBuffer.toString())).getBytes("utf-8");
    }

    private AMapLocalWeatherForecast b(String str, AMapLocation aMapLocation) {
        AMapLocalWeatherForecast aMapLocalWeatherForecast = new AMapLocalWeatherForecast();
        try {
            d.a(str);
        } catch (AMapLocException e2) {
            aMapLocalWeatherForecast.a(e2);
            e2.printStackTrace();
        }
        JSONArray jSONArray = new JSONObject(str).getJSONArray("forecasts");
        if (jSONArray != null && jSONArray.length() > 0) {
            JSONObject jSONObject = (JSONObject) jSONArray.get(0);
            aMapLocalWeatherForecast.a(a(jSONObject, "reporttime"));
            JSONArray jSONArray2 = jSONObject.getJSONArray("casts");
            if (jSONArray2 != null && jSONArray2.length() > 0) {
                ArrayList arrayList = new ArrayList();
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= jSONArray2.length()) {
                        break;
                    }
                    AMapLocalDayWeatherForecast aMapLocalDayWeatherForecast = new AMapLocalDayWeatherForecast();
                    JSONObject jSONObject2 = (JSONObject) jSONArray2.get(i2);
                    String a2 = a(jSONObject2, "date");
                    String a3 = a(jSONObject2, "week");
                    String a4 = a(jSONObject2, "dayweather");
                    String a5 = a(jSONObject2, "nightweather");
                    String a6 = a(jSONObject2, "daytemp");
                    String a7 = a(jSONObject2, "nighttemp");
                    String a8 = a(jSONObject2, "daywind");
                    String a9 = a(jSONObject2, "nightwind");
                    String a10 = a(jSONObject2, "daypower");
                    String a11 = a(jSONObject2, "nightpower");
                    aMapLocalDayWeatherForecast.a(a2);
                    aMapLocalDayWeatherForecast.b(a3);
                    aMapLocalDayWeatherForecast.c(a4);
                    aMapLocalDayWeatherForecast.d(a5);
                    aMapLocalDayWeatherForecast.e(a6);
                    aMapLocalDayWeatherForecast.f(a7);
                    aMapLocalDayWeatherForecast.g(a8);
                    aMapLocalDayWeatherForecast.h(a9);
                    aMapLocalDayWeatherForecast.i(a10);
                    aMapLocalDayWeatherForecast.j(a11);
                    aMapLocalDayWeatherForecast.setCity(aMapLocation.getCity());
                    aMapLocalDayWeatherForecast.setCityCode(aMapLocation.getCityCode());
                    aMapLocalDayWeatherForecast.setProvince(aMapLocation.getProvince());
                    arrayList.add(aMapLocalDayWeatherForecast);
                    i = i2 + 1;
                }
                aMapLocalWeatherForecast.a(arrayList);
            }
        }
        return aMapLocalWeatherForecast;
    }

    /* access modifiers changed from: protected */
    public String a(JSONObject jSONObject, String str) {
        return (jSONObject != null && jSONObject.has(str) && !jSONObject.getString(str).equals("[]")) ? jSONObject.optString(str) : "";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.api.location.a.a(long, float, com.amap.api.location.AMapLocationListener, java.lang.String, boolean):void
     arg types: [int, int, com.amap.api.location.b, java.lang.String, int]
     candidates:
      com.amap.api.location.a.a(double, double, float, long, android.app.PendingIntent):void
      com.amap.api.location.a.a(long, float, com.amap.api.location.AMapLocationListener, java.lang.String, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(int i, AMapLocalWeatherListener aMapLocalWeatherListener, AMapLocation aMapLocation) {
        try {
            this.e = i;
            this.f = aMapLocalWeatherListener;
            if (aMapLocation == null) {
                this.c.a(-1L, 10.0f, (AMapLocationListener) this, LocationProviderProxy.AMapNetwork, true);
                return;
            }
            if (i == 1) {
                a(aMapLocation, "base", aMapLocalWeatherListener);
            }
            if (i == 2) {
                a(aMapLocation, "all", aMapLocalWeatherListener);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(AMapLocation aMapLocation, String str, AMapLocalWeatherListener aMapLocalWeatherListener) {
        AMapLocException aMapLocException;
        AMapLocalWeatherForecast aMapLocalWeatherForecast;
        AMapLocalWeatherLive aMapLocalWeatherLive;
        this.f369b = aMapLocalWeatherListener;
        if (aMapLocation != null) {
            byte[] a2 = a(aMapLocation, str);
            String a3 = a();
            AMapLocException e2 = new AMapLocException();
            String str2 = null;
            try {
                str2 = l.a().a(this.d, a3, a2, "sea");
            } catch (AMapLocException e3) {
                e2 = e3;
            }
            if ("base".equals(str)) {
                if (str2 != null) {
                    aMapLocException = e2;
                    aMapLocalWeatherLive = a(str2, aMapLocation);
                } else {
                    aMapLocalWeatherLive = new AMapLocalWeatherLive();
                    aMapLocException = new AMapLocException(AMapLocException.ERROR_CONNECTION);
                }
                aMapLocalWeatherLive.a(aMapLocException);
                aMapLocalWeatherLive.setCity(aMapLocation.getCity());
                aMapLocalWeatherLive.setCityCode(aMapLocation.getCityCode());
                aMapLocalWeatherLive.setProvince(aMapLocation.getProvince());
                Message obtain = Message.obtain();
                obtain.what = 1;
                obtain.obj = aMapLocalWeatherLive;
                this.f368a.sendMessage(obtain);
            } else {
                aMapLocException = e2;
            }
            if ("all".equals(str)) {
                if (str2 != null) {
                    aMapLocalWeatherForecast = b(str2, aMapLocation);
                } else {
                    aMapLocalWeatherForecast = new AMapLocalWeatherForecast();
                    aMapLocException = new AMapLocException(AMapLocException.ERROR_CONNECTION);
                }
                aMapLocalWeatherForecast.a(aMapLocException);
                Message obtain2 = Message.obtain();
                obtain2.what = 2;
                obtain2.obj = aMapLocalWeatherForecast;
                this.f368a.sendMessage(obtain2);
            }
        }
    }

    public void onLocationChanged(Location location) {
    }

    public void onLocationChanged(AMapLocation aMapLocation) {
        if (aMapLocation != null) {
            try {
                if (aMapLocation.getAMapException() != null && aMapLocation.getAMapException().getErrorCode() == 0 && aMapLocation.getAdCode() != null && aMapLocation.getAdCode().length() > 0) {
                    this.c.a(this);
                    Message obtain = Message.obtain();
                    obtain.what = 3;
                    obtain.obj = aMapLocation;
                    this.f368a.sendMessage(obtain);
                    return;
                }
            } catch (Throwable th) {
                th.printStackTrace();
                return;
            }
        }
        this.c.a(this);
        Message obtain2 = Message.obtain();
        obtain2.what = this.e;
        AMapLocException aMapLocException = new AMapLocException(AMapLocException.ERROR_FAILURE_LOCATION);
        if (1 == this.e) {
            AMapLocalWeatherLive aMapLocalWeatherLive = new AMapLocalWeatherLive();
            aMapLocalWeatherLive.a(aMapLocException);
            obtain2.obj = aMapLocalWeatherLive;
            this.f368a.sendMessage(obtain2);
        }
        if (2 == this.e) {
            AMapLocalWeatherForecast aMapLocalWeatherForecast = new AMapLocalWeatherForecast();
            aMapLocalWeatherForecast.a(aMapLocException);
            obtain2.obj = aMapLocalWeatherForecast;
            this.f368a.sendMessage(obtain2);
        }
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
