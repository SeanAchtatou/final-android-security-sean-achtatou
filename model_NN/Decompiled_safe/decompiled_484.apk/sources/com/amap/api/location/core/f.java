package com.amap.api.location.core;

import android.os.Parcel;
import android.os.Parcelable;

final class f implements Parcelable.Creator<GeoPoint> {
    f() {
    }

    /* renamed from: a */
    public GeoPoint createFromParcel(Parcel parcel) {
        return new GeoPoint(parcel, (f) null);
    }

    /* renamed from: a */
    public GeoPoint[] newArray(int i) {
        return new GeoPoint[i];
    }
}
