package com.amap.api.location;

import android.location.Location;
import android.os.Bundle;

public class f implements AMapLocationListener {

    /* renamed from: a  reason: collision with root package name */
    private LocationManagerProxy f392a;

    /* renamed from: b  reason: collision with root package name */
    private AMapLocationListener f393b = null;

    public f(LocationManagerProxy locationManagerProxy) {
        this.f392a = locationManagerProxy;
    }

    public void a() {
        if (this.f392a != null) {
            this.f392a.removeUpdates(this);
        }
        this.f393b = null;
    }

    public boolean a(AMapLocationListener aMapLocationListener, long j, float f, String str) {
        this.f393b = aMapLocationListener;
        if (!LocationProviderProxy.AMapNetwork.equals(str)) {
            return false;
        }
        this.f392a.requestLocationUpdates(str, j, f, this);
        return true;
    }

    public void onLocationChanged(Location location) {
        if (this.f393b != null) {
            this.f393b.onLocationChanged(location);
        }
    }

    public void onLocationChanged(AMapLocation aMapLocation) {
        if (this.f393b != null) {
            this.f393b.onLocationChanged(aMapLocation);
        }
    }

    public void onProviderDisabled(String str) {
        if (this.f393b != null) {
            this.f393b.onProviderDisabled(str);
        }
    }

    public void onProviderEnabled(String str) {
        if (this.f393b != null) {
            this.f393b.onProviderEnabled(str);
        }
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
        if (this.f393b != null) {
            this.f393b.onStatusChanged(str, i, bundle);
        }
    }
}
