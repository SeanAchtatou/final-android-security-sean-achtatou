package com.amap.api.location;

import android.location.Criteria;
import android.location.LocationManager;
import android.location.LocationProvider;

public class LocationProviderProxy {
    public static final String AMapNetwork = "lbs";
    public static final int AVAILABLE = 2;
    public static final int OUT_OF_SERVICE = 0;
    public static final int TEMPORARILY_UNAVAILABLE = 1;

    /* renamed from: a  reason: collision with root package name */
    private LocationManager f361a;

    /* renamed from: b  reason: collision with root package name */
    private String f362b;

    protected LocationProviderProxy(LocationManager locationManager, String str) {
        this.f361a = locationManager;
        this.f362b = str;
    }

    private LocationProvider a() {
        try {
            if (this.f361a != null) {
                return this.f361a.getProvider(this.f362b);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return null;
    }

    static LocationProviderProxy a(LocationManager locationManager, String str) {
        return new LocationProviderProxy(locationManager, str);
    }

    public int getAccuracy() {
        if (AMapNetwork != 0) {
            try {
                if (AMapNetwork.equals(this.f362b)) {
                    return 2;
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        if (a() != null) {
            return a().getAccuracy();
        }
        return -1;
    }

    public String getName() {
        if (AMapNetwork != 0) {
            try {
                if (AMapNetwork.equals(this.f362b)) {
                    return AMapNetwork;
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        if (a() != null) {
            return a().getName();
        }
        return "null";
    }

    public int getPowerRequirement() {
        if (AMapNetwork != 0) {
            try {
                if (AMapNetwork.equals(this.f362b)) {
                    return 2;
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        if (a() != null) {
            return a().getPowerRequirement();
        }
        return -1;
    }

    public boolean hasMonetaryCost() {
        if (AMapNetwork != 0) {
            try {
                if (AMapNetwork.equals(this.f362b)) {
                    return false;
                }
            } catch (Throwable th) {
                th.printStackTrace();
                return false;
            }
        }
        if (a() != null) {
            return a().hasMonetaryCost();
        }
        return false;
    }

    public boolean meetsCriteria(Criteria criteria) {
        if (AMapNetwork != 0) {
            try {
                if (AMapNetwork.equals(this.f362b)) {
                    if (criteria == null) {
                        return true;
                    }
                    return !criteria.isAltitudeRequired() && !criteria.isBearingRequired() && !criteria.isSpeedRequired() && criteria.getAccuracy() != 1;
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        if (a() != null) {
            return a().meetsCriteria(criteria);
        }
        return false;
    }

    public boolean requiresCell() {
        if (AMapNetwork != 0) {
            try {
                if (AMapNetwork.equals(this.f362b)) {
                    return true;
                }
            } catch (Throwable th) {
                th.printStackTrace();
                return true;
            }
        }
        if (a() != null) {
            return a().requiresCell();
        }
        return true;
    }

    public boolean requiresNetwork() {
        if (AMapNetwork != 0) {
            try {
                if (AMapNetwork.equals(this.f362b)) {
                    return true;
                }
            } catch (Throwable th) {
                th.printStackTrace();
                return true;
            }
        }
        if (a() != null) {
            return a().requiresNetwork();
        }
        return true;
    }

    public boolean requiresSatellite() {
        if (AMapNetwork != 0) {
            try {
                if (AMapNetwork.equals(this.f362b)) {
                    return false;
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        if (a() != null) {
            return a().requiresNetwork();
        }
        return true;
    }

    public boolean supportsAltitude() {
        if (AMapNetwork != 0) {
            try {
                if (AMapNetwork.equals(this.f362b)) {
                    return false;
                }
            } catch (Throwable th) {
                th.printStackTrace();
                return false;
            }
        }
        if (a() != null) {
            return a().supportsAltitude();
        }
        return false;
    }

    public boolean supportsBearing() {
        if (AMapNetwork != 0) {
            try {
                if (AMapNetwork.equals(this.f362b)) {
                    return false;
                }
            } catch (Throwable th) {
                th.printStackTrace();
                return false;
            }
        }
        if (a() != null) {
            return a().supportsBearing();
        }
        return false;
    }

    public boolean supportsSpeed() {
        if (AMapNetwork != 0) {
            try {
                if (AMapNetwork.equals(this.f362b)) {
                    return false;
                }
            } catch (Throwable th) {
                th.printStackTrace();
                return false;
            }
        }
        if (a() != null) {
            return a().supportsSpeed();
        }
        return false;
    }
}
