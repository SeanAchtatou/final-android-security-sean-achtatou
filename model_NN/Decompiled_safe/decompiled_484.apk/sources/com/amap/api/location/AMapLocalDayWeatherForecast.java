package com.amap.api.location;

public class AMapLocalDayWeatherForecast {

    /* renamed from: a  reason: collision with root package name */
    private String f349a;

    /* renamed from: b  reason: collision with root package name */
    private String f350b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    private String m;

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.f349a = str;
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.f350b = str;
    }

    /* access modifiers changed from: package-private */
    public void c(String str) {
        this.c = str;
    }

    /* access modifiers changed from: package-private */
    public void d(String str) {
        this.d = str;
    }

    /* access modifiers changed from: package-private */
    public void e(String str) {
        this.e = str;
    }

    /* access modifiers changed from: package-private */
    public void f(String str) {
        this.f = str;
    }

    /* access modifiers changed from: package-private */
    public void g(String str) {
        this.g = str;
    }

    public String getCity() {
        return this.k;
    }

    public String getCityCode() {
        return this.m;
    }

    public String getDate() {
        return this.f349a;
    }

    public String getDayTemp() {
        return this.e;
    }

    public String getDayWeather() {
        return this.c;
    }

    public String getDayWindDir() {
        return this.g;
    }

    public String getDayWindPower() {
        return this.i;
    }

    public String getNightTemp() {
        return this.f;
    }

    public String getNightWeather() {
        return this.d;
    }

    public String getNightWindDir() {
        return this.h;
    }

    public String getNightWindPower() {
        return this.j;
    }

    public String getProvince() {
        return this.l;
    }

    public String getWeek() {
        return this.f350b;
    }

    /* access modifiers changed from: package-private */
    public void h(String str) {
        this.h = str;
    }

    /* access modifiers changed from: package-private */
    public void i(String str) {
        this.i = str;
    }

    /* access modifiers changed from: package-private */
    public void j(String str) {
        this.j = str;
    }

    public void setCity(String str) {
        this.k = str;
    }

    public void setCityCode(String str) {
        this.m = str;
    }

    public void setProvince(String str) {
        this.l = str;
    }
}
