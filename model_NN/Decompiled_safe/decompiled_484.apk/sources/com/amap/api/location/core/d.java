package com.amap.api.location.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import com.amap.api.location.AMapLocation;
import com.amap.api.location.LocationProviderProxy;
import com.mediav.ads.sdk.adcore.Config;
import java.lang.reflect.Method;
import org.json.JSONException;
import org.json.JSONObject;

public class d {

    /* renamed from: a  reason: collision with root package name */
    public static String f385a = "";

    /* renamed from: b  reason: collision with root package name */
    public static String f386b = "";
    static int c = 2048;
    static String d = null;
    private static SharedPreferences e = null;
    private static SharedPreferences.Editor f = null;
    private static Method g;

    public static String a() {
        try {
            String valueOf = String.valueOf(System.currentTimeMillis());
            int length = valueOf.length();
            return valueOf.substring(0, length - 2) + Config.CHANNEL_ID + valueOf.substring(length - 1);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public static String a(String str, String str2) {
        try {
            if (d == null || d.length() == 0) {
                d = c.a((Context) null).i();
            }
            return g.a(d + ":" + str.substring(0, str.length() - 3) + ":" + str2);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public static void a(Context context, AMapLocation aMapLocation) {
        try {
            if (e == null) {
                e = context.getSharedPreferences("last_know_location", 0);
            }
            if (f == null) {
                f = e.edit();
            }
            f.putString("last_know_lat", String.valueOf(aMapLocation.getLatitude()));
            f.putString("last_know_lng", String.valueOf(aMapLocation.getLongitude()));
            f.putString("province", aMapLocation.getProvince());
            f.putString("city", aMapLocation.getCity());
            f.putString("district", aMapLocation.getDistrict());
            f.putString("cityCode", aMapLocation.getCityCode());
            f.putString("adCode", aMapLocation.getAdCode());
            f.putFloat("accuracy", aMapLocation.getAccuracy());
            f.putLong("time", aMapLocation.getTime());
            a(f);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    private static void a(SharedPreferences.Editor editor) {
        if (editor != null) {
            if (Build.VERSION.SDK_INT >= 9) {
                try {
                    if (g == null) {
                        g = SharedPreferences.Editor.class.getDeclaredMethod("apply", new Class[0]);
                    }
                    g.invoke(editor, new Object[0]);
                } catch (Throwable th) {
                    th.printStackTrace();
                    editor.commit();
                }
            } else {
                editor.commit();
            }
        }
    }

    public static void a(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("status") && jSONObject.has("info")) {
                String string = jSONObject.getString("status");
                String string2 = jSONObject.getString("info");
                if (!string.equals(Config.CHANNEL_ID) && string.equals("0")) {
                    if (string2.equals("INVALID_USER_KEY") || string2.equals("INSUFFICIENT_PRIVILEGES") || string2.equals("USERKEY_PLAT_NOMATCH") || string2.equals("INVALID_USER_SCODE")) {
                        throw new AMapLocException(AMapLocException.ERROR_FAILURE_AUTH);
                    } else if (string2.equals("SERVICE_NOT_EXIST") || string2.equals("SERVICE_RESPONSE_ERROR") || string2.equals("OVER_QUOTA") || string2.equals("UNKNOWN_ERROR")) {
                        throw new AMapLocException(AMapLocException.ERROR_UNKNOWN);
                    } else if (string2.equals("INVALID_PARAMS")) {
                        throw new AMapLocException(AMapLocException.ERROR_INVALID_PARAMETER);
                    }
                }
            }
        } catch (JSONException e2) {
        }
    }

    public static boolean a(Context context) {
        if (context == null) {
            return false;
        }
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null) {
                return false;
            }
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo == null) {
                return false;
            }
            NetworkInfo.State state = activeNetworkInfo.getState();
            return (state == null || state == NetworkInfo.State.DISCONNECTED || state == NetworkInfo.State.DISCONNECTING) ? false : true;
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }

    public static AMapLocation b(Context context) {
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences("last_know_location", 0);
            AMapLocation aMapLocation = new AMapLocation("");
            aMapLocation.setProvider(LocationProviderProxy.AMapNetwork);
            double parseDouble = Double.parseDouble(sharedPreferences.getString("last_know_lat", "0.0"));
            double parseDouble2 = Double.parseDouble(sharedPreferences.getString("last_know_lng", "0.0"));
            aMapLocation.setLatitude(parseDouble);
            aMapLocation.setLongitude(parseDouble2);
            aMapLocation.setProvince(sharedPreferences.getString("province", ""));
            aMapLocation.setCity(sharedPreferences.getString("city", ""));
            aMapLocation.setDistrict(sharedPreferences.getString("district", ""));
            aMapLocation.setCityCode(sharedPreferences.getString("cityCode", ""));
            aMapLocation.setAdCode(sharedPreferences.getString("adCode", ""));
            aMapLocation.setAccuracy(sharedPreferences.getFloat("accuracy", 0.0f));
            aMapLocation.setTime(sharedPreferences.getLong("time", 0));
            return aMapLocation;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }
}
