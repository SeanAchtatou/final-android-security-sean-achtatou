package com.amap.api.location;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import com.amap.api.location.a;
import com.amap.api.location.core.AMapLocException;
import com.aps.j;
import com.aps.k;

public class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    k f373a = null;

    /* renamed from: b  reason: collision with root package name */
    volatile boolean f374b = false;
    boolean c = true;
    private volatile boolean d = false;
    private Context e;
    private long f = 2000;
    private a.C0000a g;
    private a h;
    private boolean i = false;

    c(Context context, a.C0000a aVar, a aVar2) {
        this.h = aVar2;
        b(false);
        this.e = context;
        this.f373a = new com.aps.a();
        this.g = aVar;
    }

    private AMapLocation a(com.aps.c cVar) {
        AMapLocation aMapLocation = new AMapLocation("");
        aMapLocation.setProvider(LocationProviderProxy.AMapNetwork);
        aMapLocation.setLatitude(cVar.f());
        aMapLocation.setLongitude(cVar.e());
        aMapLocation.setAccuracy(cVar.g());
        aMapLocation.setTime(cVar.h());
        aMapLocation.setPoiId(cVar.b());
        aMapLocation.setFloor(cVar.c());
        aMapLocation.setCountry(cVar.n());
        aMapLocation.setRoad(cVar.q());
        aMapLocation.setPoiName(cVar.s());
        aMapLocation.setAMapException(cVar.a());
        Bundle bundle = new Bundle();
        bundle.putString("citycode", cVar.k());
        bundle.putString("desc", cVar.l());
        bundle.putString("adcode", cVar.m());
        aMapLocation.setExtras(bundle);
        String k = cVar.k();
        String l = cVar.l();
        String m = cVar.m();
        aMapLocation.setCityCode(k);
        aMapLocation.setAdCode(m);
        if (m == null || m.trim().length() <= 0) {
            aMapLocation.b(l);
        } else {
            aMapLocation.b(l.replace(" ", ""));
        }
        aMapLocation.setCity(cVar.p());
        aMapLocation.setDistrict(cVar.d());
        aMapLocation.a(cVar.r());
        aMapLocation.setProvince(cVar.o());
        return aMapLocation;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x009d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x009e, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00a7, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00a8, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x009d A[ExcHandler: JSONException (r0v1 'e' org.json.JSONException A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void d() {
        /*
            r5 = this;
            android.content.Context r0 = r5.e     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            com.amap.api.location.core.c.a(r0)     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            com.aps.k r0 = r5.f373a     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            if (r0 == 0) goto L_0x0010
            com.aps.k r0 = r5.f373a     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            android.content.Context r1 = r5.e     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            r0.a(r1)     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
        L_0x0010:
            com.aps.k r0 = r5.f373a     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            if (r0 == 0) goto L_0x0040
            com.aps.k r0 = r5.f373a     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            r1.<init>()     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            java.lang.String r2 = "api_serverSDK_130905##S128DF1572465B890OE3F7A13167KLEI##"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            android.content.Context r2 = r5.e     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            java.lang.String r2 = com.amap.api.location.core.c.b(r2)     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            java.lang.String r2 = ","
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            java.lang.String r2 = com.amap.api.location.core.c.b()     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            java.lang.String r1 = r1.toString()     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            r0.a(r1)     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
        L_0x0040:
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            r1.<init>()     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            java.lang.String r0 = "key"
            android.content.Context r2 = r5.e     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            java.lang.String r2 = com.amap.api.location.core.c.b(r2)     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            r1.put(r0, r2)     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            java.lang.String r0 = "X-INFO"
            android.content.Context r2 = r5.e     // Catch:{ Throwable -> 0x0098, JSONException -> 0x009d }
            com.amap.api.location.core.c r2 = com.amap.api.location.core.c.a(r2)     // Catch:{ Throwable -> 0x0098, JSONException -> 0x009d }
            java.lang.String r3 = "loc"
            java.lang.String r2 = r2.a(r3)     // Catch:{ Throwable -> 0x0098, JSONException -> 0x009d }
            r1.put(r0, r2)     // Catch:{ Throwable -> 0x0098, JSONException -> 0x009d }
        L_0x0061:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            r2.<init>()     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            android.content.Context r0 = r5.e     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            com.amap.api.location.core.c r0 = com.amap.api.location.core.c.a(r0)     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            java.lang.String r0 = r0.c()     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            java.lang.String r3 = "ex"
            java.lang.String r4 = "UTF-8"
            byte[] r0 = r0.getBytes(r4)     // Catch:{ UnsupportedEncodingException -> 0x00a2 }
            java.lang.String r0 = com.aps.b.a(r0)     // Catch:{ UnsupportedEncodingException -> 0x00a2 }
            r2.put(r3, r0)     // Catch:{ UnsupportedEncodingException -> 0x00a2 }
        L_0x007f:
            java.lang.String r0 = "X-BIZ"
            r1.put(r0, r2)     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            java.lang.String r0 = "User-Agent"
            java.lang.String r2 = "AMAP Location SDK Android 1.3.1"
            r1.put(r0, r2)     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            com.aps.k r0 = r5.f373a     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            if (r0 == 0) goto L_0x0094
            com.aps.k r0 = r5.f373a     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            r0.a(r1)     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
        L_0x0094:
            r0 = 1
            r5.i = r0
            return
        L_0x0098:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            goto L_0x0061
        L_0x009d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0094
        L_0x00a2:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ JSONException -> 0x009d, Throwable -> 0x00a7 }
            goto L_0x007f
        L_0x00a7:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0094
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.api.location.c.d():void");
    }

    private com.aps.c e() {
        com.aps.c f2 = f();
        if (f2 != null) {
            return f2;
        }
        com.aps.c cVar = new com.aps.c();
        cVar.a(new AMapLocException(AMapLocException.ERROR_UNKNOWN));
        this.c = false;
        return cVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0017, code lost:
        r1 = r0;
        r0 = new com.aps.c();
        r0.a(r1);
        r4.c = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0023, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0024, code lost:
        r3 = r0;
        r0 = null;
        r1 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0016 A[ExcHandler: AMapLocException (r0v3 'e' com.amap.api.location.core.AMapLocException A[CUSTOM_DECLARE]), Splitter:B:1:0x0002] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.aps.c f() {
        /*
            r4 = this;
            r2 = 0
            r1 = 0
            com.aps.k r0 = r4.f373a     // Catch:{ AMapLocException -> 0x0016, Throwable -> 0x0023 }
            if (r0 == 0) goto L_0x002f
            com.aps.k r0 = r4.f373a     // Catch:{ AMapLocException -> 0x0016, Throwable -> 0x0023 }
            com.aps.c r0 = r0.a()     // Catch:{ AMapLocException -> 0x0016, Throwable -> 0x0023 }
        L_0x000c:
            if (r0 != 0) goto L_0x0012
            r1 = 0
            r4.c = r1     // Catch:{ AMapLocException -> 0x0016, Throwable -> 0x002d }
        L_0x0011:
            return r0
        L_0x0012:
            r1 = 1
            r4.c = r1     // Catch:{ AMapLocException -> 0x0016, Throwable -> 0x002d }
            goto L_0x0011
        L_0x0016:
            r0 = move-exception
            r1 = r0
            com.aps.c r0 = new com.aps.c
            r0.<init>()
            r0.a(r1)
            r4.c = r2
            goto L_0x0011
        L_0x0023:
            r0 = move-exception
            r3 = r0
            r0 = r1
            r1 = r3
        L_0x0027:
            r4.c = r2
            r1.printStackTrace()
            goto L_0x0011
        L_0x002d:
            r1 = move-exception
            goto L_0x0027
        L_0x002f:
            r0 = r1
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.api.location.c.f():com.aps.c");
    }

    private boolean g() {
        if (System.currentTimeMillis() - this.h.d <= 5 * this.f) {
            return false;
        }
        this.h.c = false;
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(long j) {
        if (j > this.f) {
            this.f = j;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(PendingIntent pendingIntent) {
        this.f373a.a(pendingIntent);
    }

    /* access modifiers changed from: package-private */
    public void a(j jVar, PendingIntent pendingIntent) {
        this.f373a.a(jVar, pendingIntent);
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(boolean z) {
        this.f374b = z;
    }

    public synchronized boolean a() {
        return this.f374b;
    }

    /* access modifiers changed from: package-private */
    public synchronized void b() {
        a(true);
        if (!this.d) {
            c();
        }
        if (this.h != null) {
            this.h.b();
        }
        this.i = false;
    }

    /* access modifiers changed from: package-private */
    public void b(PendingIntent pendingIntent) {
        this.f373a.b(pendingIntent);
    }

    /* access modifiers changed from: package-private */
    public void b(j jVar, PendingIntent pendingIntent) {
        this.f373a.b(jVar, pendingIntent);
    }

    /* access modifiers changed from: package-private */
    public synchronized void b(boolean z) {
        this.d = z;
    }

    /* access modifiers changed from: package-private */
    public synchronized void c() {
        if (this.f373a != null) {
            this.f373a.b();
        }
        this.f373a = null;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r5 = this;
            r4 = -1
            android.os.Looper.prepare()     // Catch:{ Throwable -> 0x011a }
            boolean r0 = r5.a()     // Catch:{ Throwable -> 0x011a }
            if (r0 == 0) goto L_0x000e
            r5.c()     // Catch:{ Throwable -> 0x011a }
        L_0x000d:
            return
        L_0x000e:
            boolean r0 = r5.i     // Catch:{ Throwable -> 0x011a }
            if (r0 != 0) goto L_0x0019
            boolean r0 = r5.d     // Catch:{ Throwable -> 0x011a }
            if (r0 == 0) goto L_0x0019
            r5.d()     // Catch:{ Throwable -> 0x011a }
        L_0x0019:
            boolean r0 = r5.d     // Catch:{ Throwable -> 0x011a }
            if (r0 == 0) goto L_0x0082
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ Throwable -> 0x011a }
            boolean r0 = r0.isInterrupted()     // Catch:{ Throwable -> 0x011a }
            if (r0 != 0) goto L_0x0082
            boolean r0 = r5.a()     // Catch:{ Throwable -> 0x011a }
            if (r0 != 0) goto L_0x0082
            r1 = 0
            com.amap.api.location.a r0 = r5.h     // Catch:{ Throwable -> 0x00d6 }
            boolean r0 = r0.c     // Catch:{ Throwable -> 0x00d6 }
            if (r0 == 0) goto L_0x003a
            boolean r0 = r5.g()     // Catch:{ Throwable -> 0x00d6 }
            if (r0 == 0) goto L_0x0040
        L_0x003a:
            com.amap.api.location.a r0 = r5.h     // Catch:{ Throwable -> 0x00d6 }
            boolean r0 = r0.e     // Catch:{ Throwable -> 0x00d6 }
            if (r0 != 0) goto L_0x0092
        L_0x0040:
            r0 = 1
            r5.c = r0     // Catch:{ Throwable -> 0x0161 }
            long r2 = r5.f     // Catch:{ Throwable -> 0x0161 }
            java.lang.Thread.sleep(r2)     // Catch:{ Throwable -> 0x0161 }
            if (r1 == 0) goto L_0x006c
            com.amap.api.location.a r0 = r5.h     // Catch:{ Throwable -> 0x011a }
            boolean r0 = r0.e     // Catch:{ Throwable -> 0x011a }
            if (r0 == 0) goto L_0x006c
            com.amap.api.location.a r0 = r5.h     // Catch:{ Throwable -> 0x011a }
            boolean r0 = r0.c     // Catch:{ Throwable -> 0x011a }
            if (r0 == 0) goto L_0x005c
            boolean r0 = r5.g()     // Catch:{ Throwable -> 0x011a }
            if (r0 == 0) goto L_0x006c
        L_0x005c:
            android.os.Message r0 = new android.os.Message     // Catch:{ Throwable -> 0x011a }
            r0.<init>()     // Catch:{ Throwable -> 0x011a }
            r0.obj = r1     // Catch:{ Throwable -> 0x011a }
            r1 = 100
            r0.what = r1     // Catch:{ Throwable -> 0x011a }
            com.amap.api.location.a$a r1 = r5.g     // Catch:{ Throwable -> 0x011a }
            r1.sendMessage(r0)     // Catch:{ Throwable -> 0x011a }
        L_0x006c:
            int r0 = com.amap.api.location.core.a.a()     // Catch:{ Throwable -> 0x011a }
            if (r0 != r4) goto L_0x0077
            android.content.Context r0 = r5.e     // Catch:{ Throwable -> 0x011a }
            com.amap.api.location.core.a.a(r0)     // Catch:{ Throwable -> 0x011a }
        L_0x0077:
            boolean r0 = r5.c     // Catch:{ Throwable -> 0x0081 }
            if (r0 == 0) goto L_0x01a3
            long r0 = r5.f     // Catch:{ Throwable -> 0x0081 }
            java.lang.Thread.sleep(r0)     // Catch:{ Throwable -> 0x0081 }
            goto L_0x0019
        L_0x0081:
            r0 = move-exception
        L_0x0082:
            boolean r0 = r5.a()     // Catch:{ Throwable -> 0x008c }
            if (r0 == 0) goto L_0x000d
            r5.c()     // Catch:{ Throwable -> 0x008c }
            goto L_0x000d
        L_0x008c:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x000d
        L_0x0092:
            com.aps.c r0 = r5.e()     // Catch:{ Throwable -> 0x00d6 }
            if (r0 == 0) goto L_0x01b1
            com.amap.api.location.AMapLocation r0 = r5.a(r0)     // Catch:{ Throwable -> 0x00d6 }
        L_0x009c:
            if (r0 == 0) goto L_0x00c0
            com.amap.api.location.a r1 = r5.h     // Catch:{ Throwable -> 0x011a }
            boolean r1 = r1.e     // Catch:{ Throwable -> 0x011a }
            if (r1 == 0) goto L_0x00c0
            com.amap.api.location.a r1 = r5.h     // Catch:{ Throwable -> 0x011a }
            boolean r1 = r1.c     // Catch:{ Throwable -> 0x011a }
            if (r1 == 0) goto L_0x00b0
            boolean r1 = r5.g()     // Catch:{ Throwable -> 0x011a }
            if (r1 == 0) goto L_0x00c0
        L_0x00b0:
            android.os.Message r1 = new android.os.Message     // Catch:{ Throwable -> 0x011a }
            r1.<init>()     // Catch:{ Throwable -> 0x011a }
            r1.obj = r0     // Catch:{ Throwable -> 0x011a }
            r0 = 100
            r1.what = r0     // Catch:{ Throwable -> 0x011a }
            com.amap.api.location.a$a r0 = r5.g     // Catch:{ Throwable -> 0x011a }
            r0.sendMessage(r1)     // Catch:{ Throwable -> 0x011a }
        L_0x00c0:
            int r0 = com.amap.api.location.core.a.a()     // Catch:{ Throwable -> 0x011a }
            if (r0 != r4) goto L_0x00cb
            android.content.Context r0 = r5.e     // Catch:{ Throwable -> 0x011a }
            com.amap.api.location.core.a.a(r0)     // Catch:{ Throwable -> 0x011a }
        L_0x00cb:
            boolean r0 = r5.c     // Catch:{ Throwable -> 0x0081 }
            if (r0 == 0) goto L_0x01aa
            long r0 = r5.f     // Catch:{ Throwable -> 0x0081 }
            java.lang.Thread.sleep(r0)     // Catch:{ Throwable -> 0x0081 }
            goto L_0x0019
        L_0x00d6:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0120 }
            if (r1 == 0) goto L_0x00fe
            com.amap.api.location.a r0 = r5.h     // Catch:{ Throwable -> 0x011a }
            boolean r0 = r0.e     // Catch:{ Throwable -> 0x011a }
            if (r0 == 0) goto L_0x00fe
            com.amap.api.location.a r0 = r5.h     // Catch:{ Throwable -> 0x011a }
            boolean r0 = r0.c     // Catch:{ Throwable -> 0x011a }
            if (r0 == 0) goto L_0x00ee
            boolean r0 = r5.g()     // Catch:{ Throwable -> 0x011a }
            if (r0 == 0) goto L_0x00fe
        L_0x00ee:
            android.os.Message r0 = new android.os.Message     // Catch:{ Throwable -> 0x011a }
            r0.<init>()     // Catch:{ Throwable -> 0x011a }
            r0.obj = r1     // Catch:{ Throwable -> 0x011a }
            r1 = 100
            r0.what = r1     // Catch:{ Throwable -> 0x011a }
            com.amap.api.location.a$a r1 = r5.g     // Catch:{ Throwable -> 0x011a }
            r1.sendMessage(r0)     // Catch:{ Throwable -> 0x011a }
        L_0x00fe:
            int r0 = com.amap.api.location.core.a.a()     // Catch:{ Throwable -> 0x011a }
            if (r0 != r4) goto L_0x0109
            android.content.Context r0 = r5.e     // Catch:{ Throwable -> 0x011a }
            com.amap.api.location.core.a.a(r0)     // Catch:{ Throwable -> 0x011a }
        L_0x0109:
            boolean r0 = r5.c     // Catch:{ Throwable -> 0x0081 }
            if (r0 == 0) goto L_0x015a
            long r0 = r5.f     // Catch:{ Throwable -> 0x0081 }
            java.lang.Thread.sleep(r0)     // Catch:{ Throwable -> 0x0081 }
            goto L_0x0019
        L_0x0114:
            r2 = 30000(0x7530, double:1.4822E-319)
            java.lang.Thread.sleep(r2)     // Catch:{ Throwable -> 0x0081 }
        L_0x0119:
            throw r0     // Catch:{ Throwable -> 0x011a }
        L_0x011a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0082
        L_0x0120:
            r0 = move-exception
            if (r1 == 0) goto L_0x0145
            com.amap.api.location.a r2 = r5.h     // Catch:{ Throwable -> 0x011a }
            boolean r2 = r2.e     // Catch:{ Throwable -> 0x011a }
            if (r2 == 0) goto L_0x0145
            com.amap.api.location.a r2 = r5.h     // Catch:{ Throwable -> 0x011a }
            boolean r2 = r2.c     // Catch:{ Throwable -> 0x011a }
            if (r2 == 0) goto L_0x0135
            boolean r2 = r5.g()     // Catch:{ Throwable -> 0x011a }
            if (r2 == 0) goto L_0x0145
        L_0x0135:
            android.os.Message r2 = new android.os.Message     // Catch:{ Throwable -> 0x011a }
            r2.<init>()     // Catch:{ Throwable -> 0x011a }
            r2.obj = r1     // Catch:{ Throwable -> 0x011a }
            r1 = 100
            r2.what = r1     // Catch:{ Throwable -> 0x011a }
            com.amap.api.location.a$a r1 = r5.g     // Catch:{ Throwable -> 0x011a }
            r1.sendMessage(r2)     // Catch:{ Throwable -> 0x011a }
        L_0x0145:
            int r1 = com.amap.api.location.core.a.a()     // Catch:{ Throwable -> 0x011a }
            if (r1 != r4) goto L_0x0150
            android.content.Context r1 = r5.e     // Catch:{ Throwable -> 0x011a }
            com.amap.api.location.core.a.a(r1)     // Catch:{ Throwable -> 0x011a }
        L_0x0150:
            boolean r1 = r5.c     // Catch:{ Throwable -> 0x0081 }
            if (r1 == 0) goto L_0x0114
            long r2 = r5.f     // Catch:{ Throwable -> 0x0081 }
            java.lang.Thread.sleep(r2)     // Catch:{ Throwable -> 0x0081 }
            goto L_0x0119
        L_0x015a:
            r0 = 30000(0x7530, double:1.4822E-319)
            java.lang.Thread.sleep(r0)     // Catch:{ Throwable -> 0x0081 }
            goto L_0x0019
        L_0x0161:
            r0 = move-exception
            if (r1 == 0) goto L_0x0186
            com.amap.api.location.a r0 = r5.h     // Catch:{ Throwable -> 0x011a }
            boolean r0 = r0.e     // Catch:{ Throwable -> 0x011a }
            if (r0 == 0) goto L_0x0186
            com.amap.api.location.a r0 = r5.h     // Catch:{ Throwable -> 0x011a }
            boolean r0 = r0.c     // Catch:{ Throwable -> 0x011a }
            if (r0 == 0) goto L_0x0176
            boolean r0 = r5.g()     // Catch:{ Throwable -> 0x011a }
            if (r0 == 0) goto L_0x0186
        L_0x0176:
            android.os.Message r0 = new android.os.Message     // Catch:{ Throwable -> 0x011a }
            r0.<init>()     // Catch:{ Throwable -> 0x011a }
            r0.obj = r1     // Catch:{ Throwable -> 0x011a }
            r1 = 100
            r0.what = r1     // Catch:{ Throwable -> 0x011a }
            com.amap.api.location.a$a r1 = r5.g     // Catch:{ Throwable -> 0x011a }
            r1.sendMessage(r0)     // Catch:{ Throwable -> 0x011a }
        L_0x0186:
            int r0 = com.amap.api.location.core.a.a()     // Catch:{ Throwable -> 0x011a }
            if (r0 != r4) goto L_0x0191
            android.content.Context r0 = r5.e     // Catch:{ Throwable -> 0x011a }
            com.amap.api.location.core.a.a(r0)     // Catch:{ Throwable -> 0x011a }
        L_0x0191:
            boolean r0 = r5.c     // Catch:{ Throwable -> 0x0081 }
            if (r0 == 0) goto L_0x019c
            long r0 = r5.f     // Catch:{ Throwable -> 0x0081 }
            java.lang.Thread.sleep(r0)     // Catch:{ Throwable -> 0x0081 }
            goto L_0x0082
        L_0x019c:
            r0 = 30000(0x7530, double:1.4822E-319)
            java.lang.Thread.sleep(r0)     // Catch:{ Throwable -> 0x0081 }
            goto L_0x0082
        L_0x01a3:
            r0 = 30000(0x7530, double:1.4822E-319)
            java.lang.Thread.sleep(r0)     // Catch:{ Throwable -> 0x0081 }
            goto L_0x0019
        L_0x01aa:
            r0 = 30000(0x7530, double:1.4822E-319)
            java.lang.Thread.sleep(r0)     // Catch:{ Throwable -> 0x0081 }
            goto L_0x0019
        L_0x01b1:
            r0 = r1
            goto L_0x009c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.api.location.c.run():void");
    }
}
