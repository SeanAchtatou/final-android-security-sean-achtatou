package com.amap.api.location.core;

import android.os.Parcel;
import android.os.Parcelable;

public class GeoPoint implements Parcelable {
    public static final Parcelable.Creator<GeoPoint> CREATOR = new f();

    /* renamed from: a  reason: collision with root package name */
    private long f377a;

    /* renamed from: b  reason: collision with root package name */
    private long f378b;
    private double c;
    private double d;

    public GeoPoint() {
        this.f377a = Long.MIN_VALUE;
        this.f378b = Long.MIN_VALUE;
        this.c = Double.MIN_VALUE;
        this.d = Double.MIN_VALUE;
        this.f377a = 0;
        this.f378b = 0;
    }

    public GeoPoint(int i, int i2) {
        this.f377a = Long.MIN_VALUE;
        this.f378b = Long.MIN_VALUE;
        this.c = Double.MIN_VALUE;
        this.d = Double.MIN_VALUE;
        this.f377a = (long) i;
        this.f378b = (long) i2;
    }

    public GeoPoint(long j, long j2) {
        this.f377a = Long.MIN_VALUE;
        this.f378b = Long.MIN_VALUE;
        this.c = Double.MIN_VALUE;
        this.d = Double.MIN_VALUE;
        this.f377a = j;
        this.f378b = j2;
    }

    private GeoPoint(Parcel parcel) {
        this.f377a = Long.MIN_VALUE;
        this.f378b = Long.MIN_VALUE;
        this.c = Double.MIN_VALUE;
        this.d = Double.MIN_VALUE;
        this.f377a = parcel.readLong();
        this.f378b = parcel.readLong();
    }

    /* synthetic */ GeoPoint(Parcel parcel, f fVar) {
        this(parcel);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        GeoPoint geoPoint = (GeoPoint) obj;
        return this.c == geoPoint.c && this.d == geoPoint.d && this.f377a == geoPoint.f377a && this.f378b == geoPoint.f378b;
    }

    public int getLatitudeE6() {
        return (int) this.f377a;
    }

    public int getLongitudeE6() {
        return (int) this.f378b;
    }

    public int hashCode() {
        return (int) ((this.d * 7.0d) + (this.c * 11.0d));
    }

    public String toString() {
        return "" + this.f377a + "," + this.f378b;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.f377a);
        parcel.writeLong(this.f378b);
    }
}
