package com.amap.api.location;

import android.location.LocationListener;
import android.os.Bundle;

class e implements LocationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f391a;

    e(d dVar) {
        this.f391a = dVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x0169 A[Catch:{ Exception -> 0x00fa, all -> 0x0155, Throwable -> 0x00ee }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLocationChanged(android.location.Location r7) {
        /*
            r6 = this;
            com.amap.api.location.d r0 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r0 = r0.d     // Catch:{ Throwable -> 0x00ee }
            r1 = 1
            r0.b(r1)     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.d r0 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r0 = r0.d     // Catch:{ Throwable -> 0x00ee }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x00ee }
            r0.d = r2     // Catch:{ Throwable -> 0x00ee }
            r2 = 0
            if (r7 != 0) goto L_0x006e
            android.os.Message r0 = new android.os.Message     // Catch:{ Throwable -> 0x00ee }
            r0.<init>()     // Catch:{ Throwable -> 0x00ee }
            r0.obj = r2     // Catch:{ Throwable -> 0x00ee }
            r1 = 100
            r0.what = r1     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.d r1 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a$a r1 = r1.c     // Catch:{ Throwable -> 0x00ee }
            if (r1 == 0) goto L_0x0035
            com.amap.api.location.d r1 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a$a r1 = r1.c     // Catch:{ Throwable -> 0x00ee }
            r1.sendMessage(r0)     // Catch:{ Throwable -> 0x00ee }
        L_0x0035:
            com.amap.api.location.d r0 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r0 = r0.d     // Catch:{ Throwable -> 0x00ee }
            r1 = 1
            r0.c = r1     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.d r0 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r0 = r0.d     // Catch:{ Throwable -> 0x00ee }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x00ee }
            r0.d = r4     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.d r0 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r0 = r0.d     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.c r0 = r0.f364b     // Catch:{ Throwable -> 0x00ee }
            if (r0 == 0) goto L_0x006d
            com.amap.api.location.d r0 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r0 = r0.d     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.c r0 = r0.f364b     // Catch:{ Throwable -> 0x00ee }
            com.aps.k r0 = r0.f373a     // Catch:{ Throwable -> 0x00ee }
            if (r0 == 0) goto L_0x006d
            com.amap.api.location.d r0 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r0 = r0.d     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.c r0 = r0.f364b     // Catch:{ Throwable -> 0x00ee }
            com.aps.k r0 = r0.f373a     // Catch:{ Throwable -> 0x00ee }
            r0.a(r2)     // Catch:{ Throwable -> 0x00ee }
        L_0x006d:
            return
        L_0x006e:
            double r0 = r7.getLatitude()     // Catch:{ Exception -> 0x00fa, all -> 0x0155 }
            double r4 = r7.getLongitude()     // Catch:{ Exception -> 0x00fa, all -> 0x0155 }
            boolean r0 = com.amap.api.location.core.c.a(r0, r4)     // Catch:{ Exception -> 0x00fa, all -> 0x0155 }
            if (r0 == 0) goto L_0x00f4
            double r0 = r7.getLongitude()     // Catch:{ Exception -> 0x00fa, all -> 0x0155 }
            double r4 = r7.getLatitude()     // Catch:{ Exception -> 0x00fa, all -> 0x0155 }
            double[] r0 = com.aps.u.a(r0, r4)     // Catch:{ Exception -> 0x00fa, all -> 0x0155 }
            com.amap.api.location.AMapLocation r1 = new com.amap.api.location.AMapLocation     // Catch:{ Exception -> 0x00fa, all -> 0x0155 }
            r1.<init>(r7)     // Catch:{ Exception -> 0x00fa, all -> 0x0155 }
            r2 = 1
            r2 = r0[r2]     // Catch:{ Exception -> 0x01ae }
            r1.setLatitude(r2)     // Catch:{ Exception -> 0x01ae }
            r2 = 0
            r2 = r0[r2]     // Catch:{ Exception -> 0x01ae }
            r1.setLongitude(r2)     // Catch:{ Exception -> 0x01ae }
        L_0x0099:
            android.os.Message r0 = new android.os.Message     // Catch:{ Throwable -> 0x00ee }
            r0.<init>()     // Catch:{ Throwable -> 0x00ee }
            r0.obj = r1     // Catch:{ Throwable -> 0x00ee }
            r2 = 100
            r0.what = r2     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.d r2 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a$a r2 = r2.c     // Catch:{ Throwable -> 0x00ee }
            if (r2 == 0) goto L_0x00b5
            com.amap.api.location.d r2 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a$a r2 = r2.c     // Catch:{ Throwable -> 0x00ee }
            r2.sendMessage(r0)     // Catch:{ Throwable -> 0x00ee }
        L_0x00b5:
            com.amap.api.location.d r0 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r0 = r0.d     // Catch:{ Throwable -> 0x00ee }
            r2 = 1
            r0.c = r2     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.d r0 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r0 = r0.d     // Catch:{ Throwable -> 0x00ee }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x00ee }
            r0.d = r2     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.d r0 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r0 = r0.d     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.c r0 = r0.f364b     // Catch:{ Throwable -> 0x00ee }
            if (r0 == 0) goto L_0x006d
            com.amap.api.location.d r0 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r0 = r0.d     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.c r0 = r0.f364b     // Catch:{ Throwable -> 0x00ee }
            com.aps.k r0 = r0.f373a     // Catch:{ Throwable -> 0x00ee }
            if (r0 == 0) goto L_0x006d
            com.amap.api.location.d r0 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r0 = r0.d     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.c r0 = r0.f364b     // Catch:{ Throwable -> 0x00ee }
            com.aps.k r0 = r0.f373a     // Catch:{ Throwable -> 0x00ee }
            r0.a(r1)     // Catch:{ Throwable -> 0x00ee }
            goto L_0x006d
        L_0x00ee:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x006d
        L_0x00f4:
            com.amap.api.location.AMapLocation r1 = new com.amap.api.location.AMapLocation     // Catch:{ Exception -> 0x00fa, all -> 0x0155 }
            r1.<init>(r7)     // Catch:{ Exception -> 0x00fa, all -> 0x0155 }
            goto L_0x0099
        L_0x00fa:
            r0 = move-exception
            r1 = r2
        L_0x00fc:
            r0.printStackTrace()     // Catch:{ all -> 0x01ab }
            android.os.Message r0 = new android.os.Message     // Catch:{ Throwable -> 0x00ee }
            r0.<init>()     // Catch:{ Throwable -> 0x00ee }
            r0.obj = r1     // Catch:{ Throwable -> 0x00ee }
            r2 = 100
            r0.what = r2     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.d r2 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a$a r2 = r2.c     // Catch:{ Throwable -> 0x00ee }
            if (r2 == 0) goto L_0x011b
            com.amap.api.location.d r2 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a$a r2 = r2.c     // Catch:{ Throwable -> 0x00ee }
            r2.sendMessage(r0)     // Catch:{ Throwable -> 0x00ee }
        L_0x011b:
            com.amap.api.location.d r0 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r0 = r0.d     // Catch:{ Throwable -> 0x00ee }
            r2 = 1
            r0.c = r2     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.d r0 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r0 = r0.d     // Catch:{ Throwable -> 0x00ee }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x00ee }
            r0.d = r2     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.d r0 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r0 = r0.d     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.c r0 = r0.f364b     // Catch:{ Throwable -> 0x00ee }
            if (r0 == 0) goto L_0x006d
            com.amap.api.location.d r0 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r0 = r0.d     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.c r0 = r0.f364b     // Catch:{ Throwable -> 0x00ee }
            com.aps.k r0 = r0.f373a     // Catch:{ Throwable -> 0x00ee }
            if (r0 == 0) goto L_0x006d
            com.amap.api.location.d r0 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r0 = r0.d     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.c r0 = r0.f364b     // Catch:{ Throwable -> 0x00ee }
            com.aps.k r0 = r0.f373a     // Catch:{ Throwable -> 0x00ee }
            r0.a(r1)     // Catch:{ Throwable -> 0x00ee }
            goto L_0x006d
        L_0x0155:
            r0 = move-exception
        L_0x0156:
            android.os.Message r1 = new android.os.Message     // Catch:{ Throwable -> 0x00ee }
            r1.<init>()     // Catch:{ Throwable -> 0x00ee }
            r1.obj = r2     // Catch:{ Throwable -> 0x00ee }
            r3 = 100
            r1.what = r3     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.d r3 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a$a r3 = r3.c     // Catch:{ Throwable -> 0x00ee }
            if (r3 == 0) goto L_0x0172
            com.amap.api.location.d r3 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a$a r3 = r3.c     // Catch:{ Throwable -> 0x00ee }
            r3.sendMessage(r1)     // Catch:{ Throwable -> 0x00ee }
        L_0x0172:
            com.amap.api.location.d r1 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r1 = r1.d     // Catch:{ Throwable -> 0x00ee }
            r3 = 1
            r1.c = r3     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.d r1 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r1 = r1.d     // Catch:{ Throwable -> 0x00ee }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x00ee }
            r1.d = r4     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.d r1 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r1 = r1.d     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.c r1 = r1.f364b     // Catch:{ Throwable -> 0x00ee }
            if (r1 == 0) goto L_0x01aa
            com.amap.api.location.d r1 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r1 = r1.d     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.c r1 = r1.f364b     // Catch:{ Throwable -> 0x00ee }
            com.aps.k r1 = r1.f373a     // Catch:{ Throwable -> 0x00ee }
            if (r1 == 0) goto L_0x01aa
            com.amap.api.location.d r1 = r6.f391a     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.a r1 = r1.d     // Catch:{ Throwable -> 0x00ee }
            com.amap.api.location.c r1 = r1.f364b     // Catch:{ Throwable -> 0x00ee }
            com.aps.k r1 = r1.f373a     // Catch:{ Throwable -> 0x00ee }
            r1.a(r2)     // Catch:{ Throwable -> 0x00ee }
        L_0x01aa:
            throw r0     // Catch:{ Throwable -> 0x00ee }
        L_0x01ab:
            r0 = move-exception
            r2 = r1
            goto L_0x0156
        L_0x01ae:
            r0 = move-exception
            goto L_0x00fc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.api.location.e.onLocationChanged(android.location.Location):void");
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
