package com.mediav.ads.sdk.interfaces;

public interface ILandingPageListener {
    void onPageClose();

    void onPageLoadFailed();

    void onPageLoadFinished();
}
