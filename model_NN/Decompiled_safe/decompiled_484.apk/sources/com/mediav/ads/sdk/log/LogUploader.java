package com.mediav.ads.sdk.log;

import android.content.Context;
import android.content.SharedPreferences;
import com.mediav.ads.sdk.adcore.Config;
import com.qihoo.dynamic.util.Md5Util;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class LogUploader {
    private static final int DAILY_MAX_LIMIT = 100;
    private static final String ERROR_LOG_KEY = "mvadsdkerrordaycheck";
    private static int logid = 0;

    private static boolean checkLimit(Context context) {
        String str = "count" + new SimpleDateFormat("yyyyMMdd").format(Long.valueOf(System.currentTimeMillis()));
        int i = context.getSharedPreferences(ERROR_LOG_KEY, 0).getInt(str, -1);
        if (i >= 0) {
            return i < 100;
        }
        SharedPreferences.Editor edit = context.getSharedPreferences(ERROR_LOG_KEY, 0).edit();
        edit.clear();
        edit.putInt(str, 0);
        edit.commit();
        return true;
    }

    private static void incLogCount(Context context) {
        String str = "count" + new SimpleDateFormat("yyyyMMdd").format(Long.valueOf(System.currentTimeMillis()));
        context.getSharedPreferences(ERROR_LOG_KEY, 0).edit().putInt(str, context.getSharedPreferences(ERROR_LOG_KEY, 0).getInt(str, 0) + 1).commit();
    }

    public static boolean postData(String str, HashMap<String, String> hashMap) {
        HttpPost httpPost = new HttpPost(str);
        ArrayList arrayList = new ArrayList();
        if (hashMap != null) {
            for (Map.Entry next : hashMap.entrySet()) {
                arrayList.add(new BasicNameValuePair((String) next.getKey(), (String) next.getValue()));
            }
        }
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList, Md5Util.DEFAULT_CHARSET));
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            defaultHttpClient.getParams().setParameter("http.connection.timeout", 20000);
            defaultHttpClient.getParams().setParameter("http.socket.timeout", 20000);
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            if (execute.getStatusLine().getStatusCode() == 200) {
                return true;
            }
            MVLog.d("POST异常:Code=" + execute.getStatusLine().getStatusCode());
            return false;
        } catch (Exception e) {
            MVLog.d("POST异常:" + e.getMessage());
        }
    }

    public static synchronized void postLog(HashMap<String, String> hashMap, Context context, boolean z) {
        synchronized (LogUploader.class) {
            if (z) {
                logid++;
                hashMap.put("elogid", logid + "");
            }
            incLogCount(context);
            if (!checkLimit(context)) {
                MVLog.d("上传LOG数已超过上限，取消上传");
            } else {
                MVLog.d("上传LOG");
                if (Utils.isNetEnable() && !postData(Config.ERROR_LOG_URL, hashMap) && z) {
                    LogFileManager.saveLog(hashMap);
                }
            }
        }
    }
}
