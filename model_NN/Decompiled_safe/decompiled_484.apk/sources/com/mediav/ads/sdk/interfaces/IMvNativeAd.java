package com.mediav.ads.sdk.interfaces;

import org.json.JSONObject;

public interface IMvNativeAd {
    JSONObject getContent();

    void onAdClicked();

    void onAdShowed();
}
