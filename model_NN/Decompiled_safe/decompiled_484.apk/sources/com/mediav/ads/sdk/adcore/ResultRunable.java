package com.mediav.ads.sdk.adcore;

import android.os.Message;
import com.mediav.ads.sdk.adcore.HttpRequester;
import com.qihoo.video.httpservice.NetWorkError;
import com.qihoo360.daily.activity.SendCmtActivity;
import java.util.HashMap;

class ResultRunable implements Runnable {
    private static final String UNKOWN_ERROR = "unkown error";
    private Message msg = null;

    public ResultRunable(Message message) {
        this.msg = message;
    }

    public void run() {
        try {
            HashMap hashMap = (HashMap) this.msg.obj;
            HttpRequester.Listener listener = (HttpRequester.Listener) hashMap.get("callback");
            if (this.msg.what == 0) {
                listener.onGetDataSucceed((byte[]) hashMap.get(SendCmtActivity.TAG_DATA));
            } else if (this.msg.what == 1) {
                listener.onGetDataFailed((String) hashMap.get(NetWorkError.ErrorCode));
            } else if (this.msg.what == 2) {
                listener.onGetDataFailed((String) hashMap.get(NetWorkError.ErrorCode));
            } else {
                listener.onGetDataFailed(UNKOWN_ERROR);
            }
        } catch (Exception e) {
            ((HttpRequester.Listener) ((HashMap) this.msg.obj).get("callback")).onGetDataFailed("HttpRequester get data error:" + e.getMessage());
        }
    }
}
