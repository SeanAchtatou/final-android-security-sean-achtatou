package com.mediav.ads.sdk.adcore;

import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup;
import com.mediav.ads.sdk.interfaces.IBridge;
import com.mediav.ads.sdk.interfaces.IMvAdEventListener;
import com.mediav.ads.sdk.interfaces.IMvBannerAd;
import com.mediav.ads.sdk.interfaces.IMvFloatbannerAd;
import com.mediav.ads.sdk.interfaces.IMvInterstitialAd;
import com.mediav.ads.sdk.interfaces.IMvLandingPageView;
import com.mediav.ads.sdk.interfaces.IMvNativeAdListener;
import com.mediav.ads.sdk.interfaces.IMvNativeAdLoader;
import com.mediav.ads.sdk.interfaces.IMvVideoAdListener;
import com.mediav.ads.sdk.interfaces.IMvVideoAdLoader;
import com.mediav.ads.sdk.log.MVLog;

public class Mvad {
    private static IMvFloatbannerAd floatbannerAd = null;
    private static IMvInterstitialAd interstitialAd = null;
    private static IBridge mvad = null;

    public enum FLOAT_BANNER_SIZE {
        SIZE_DEFAULT,
        SIZE_MATCH_PARENT,
        SIZE_640X100,
        SIZE_936x120,
        SIZE_728x90
    }

    public enum FLOAT_LOCATION {
        TOP,
        BOTTOM
    }

    public static void activityDestroy(Activity activity) {
        if (mvad != null) {
            mvad.activityDestroy(activity);
            floatbannerAd = null;
            interstitialAd = null;
        }
    }

    public static IMvFloatbannerAd closeFloatbannerAd(Activity activity) {
        if (floatbannerAd == null) {
            return null;
        }
        floatbannerAd.closeAds();
        return null;
    }

    public static IMvInterstitialAd closeInterstitial(Activity activity) {
        if (interstitialAd == null) {
            return null;
        }
        interstitialAd.closeAds();
        return null;
    }

    public static IMvNativeAdLoader initNativeAdLoader(Activity activity, String str, IMvNativeAdListener iMvNativeAdListener, Boolean bool) {
        if (mvad == null) {
            mvad = UpdateBridge.getBridge(activity.getApplicationContext());
            if (mvad == null) {
                return null;
            }
        }
        return (IMvNativeAdLoader) mvad.getNativeAdLoader(activity, str, iMvNativeAdListener, bool);
    }

    public static IMvVideoAdLoader initVideoAdLoader(Context context, String str, IMvVideoAdListener iMvVideoAdListener, Boolean bool) {
        if (mvad == null) {
            mvad = UpdateBridge.getBridge(context);
            if (mvad == null) {
                return null;
            }
        }
        return (IMvVideoAdLoader) mvad.getVideoAdLoader(context, str, iMvVideoAdListener, bool);
    }

    public static void setLandingPageView(Context context, IMvLandingPageView iMvLandingPageView) {
        if (mvad == null) {
            mvad = UpdateBridge.getBridge(context);
            if (mvad == null) {
                return;
            }
        }
        mvad.setLandingPageView(iMvLandingPageView);
    }

    public static void setLogSwitch(Context context, boolean z) {
        if (mvad == null) {
            mvad = UpdateBridge.getBridge(context);
            if (mvad == null) {
                return;
            }
        }
        MVLog.logcatSwitch = z;
        mvad.setLogSwitch(z);
    }

    public static IMvBannerAd showBanner(ViewGroup viewGroup, Activity activity, String str, Boolean bool) {
        if (mvad == null) {
            mvad = UpdateBridge.getBridge(activity);
            if (mvad == null) {
                return null;
            }
        }
        try {
            return (IMvBannerAd) mvad.getBanner(viewGroup, activity, str, bool);
        } catch (Exception e) {
            MVLog.e("初始化插屏失败:" + e.getMessage());
            return null;
        }
    }

    public static IMvFloatbannerAd showFloatbannerAd(Activity activity, String str, Boolean bool, FLOAT_BANNER_SIZE float_banner_size, FLOAT_LOCATION float_location) {
        if (mvad == null) {
            mvad = UpdateBridge.getBridge(activity);
            if (mvad == null) {
                return null;
            }
        }
        try {
            if (floatbannerAd == null) {
                floatbannerAd = (IMvFloatbannerAd) mvad.getFloatingBanner(activity, str, bool, Integer.valueOf(float_banner_size.ordinal()), Integer.valueOf(float_location.ordinal()));
            } else {
                mvad.getFloatingBanner(activity, str, bool, Integer.valueOf(float_banner_size.ordinal()), Integer.valueOf(float_location.ordinal()));
            }
            return floatbannerAd;
        } catch (Exception e) {
            MVLog.e("初始化插屏失败:" + e.getMessage());
            return null;
        }
    }

    public static IMvInterstitialAd showInterstitial(Activity activity, String str, Boolean bool) {
        if (mvad == null) {
            mvad = UpdateBridge.getBridge(activity);
            if (mvad == null) {
                return null;
            }
        }
        try {
            if (interstitialAd == null) {
                interstitialAd = (IMvInterstitialAd) mvad.getInterstitial(activity, str, bool);
            } else {
                interstitialAd.showAds(activity);
            }
            return interstitialAd;
        } catch (Exception e) {
            MVLog.e("初始化插屏失败:" + e.getMessage());
            return null;
        }
    }

    public static void showSplashAd(ViewGroup viewGroup, Activity activity, String str, IMvAdEventListener iMvAdEventListener, Boolean bool, Boolean bool2) {
        if (mvad == null) {
            mvad = UpdateBridge.getBridge(activity);
            if (mvad == null) {
                return;
            }
        }
        try {
            mvad.getSplashAd(viewGroup, activity, str, iMvAdEventListener, bool, bool2);
        } catch (Exception e) {
            MVLog.e("初始化开屏失败:" + e.getMessage());
        }
    }
}
