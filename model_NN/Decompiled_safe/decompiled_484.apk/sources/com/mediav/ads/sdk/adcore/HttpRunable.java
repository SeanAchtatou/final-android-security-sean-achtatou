package com.mediav.ads.sdk.adcore;

import android.content.Context;
import android.os.Handler;
import com.mediav.ads.sdk.adcore.HttpRequester;
import com.qihoo.messenger.util.QDefine;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

class HttpRunable implements Runnable {
    private final int NET_TIMEOUT = QDefine.ONE_SECOND;
    private Context context = null;
    private Handler handler = null;
    private Boolean isUsecache = true;
    private HttpRequester.Listener listener = null;
    private String urlString = null;

    public HttpRunable(String str, Handler handler2, HttpRequester.Listener listener2, Context context2, Boolean bool) {
        this.urlString = str;
        this.handler = handler2;
        this.listener = listener2;
        this.context = context2;
        this.isUsecache = bool;
    }

    private byte[] getBytes(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr, 0, 1024);
            if (read == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
            byteArrayOutputStream.flush();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x009f A[Catch:{ Exception -> 0x00d3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00fc  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x010b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r9 = this;
            r2 = 0
            r8 = 1
            r7 = 0
            android.os.Message r3 = new android.os.Message
            r3.<init>()
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            java.lang.String r0 = "callback"
            com.mediav.ads.sdk.adcore.HttpRequester$Listener r1 = r9.listener
            r4.put(r0, r1)
            android.content.Context r0 = r9.context     // Catch:{ Exception -> 0x0046 }
            com.mediav.ads.sdk.adcore.HttpCacher r1 = com.mediav.ads.sdk.adcore.HttpCacher.get(r0)     // Catch:{ Exception -> 0x0046 }
            java.lang.String r0 = r9.urlString     // Catch:{ Exception -> 0x0122 }
            byte[] r2 = r1.getAsBinary(r0)     // Catch:{ Exception -> 0x0122 }
        L_0x0020:
            if (r2 == 0) goto L_0x0063
            java.lang.Boolean r0 = r9.isUsecache
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0063
            java.lang.Boolean r0 = com.mediav.ads.sdk.adcore.HttpRequester.isOpenLog
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0037
            java.lang.String r0 = "异步:缓存命中"
            com.mediav.ads.sdk.log.MVLog.d(r0)
        L_0x0037:
            r3.what = r7
            java.lang.String r0 = "data"
            r4.put(r0, r2)
            r3.obj = r4
        L_0x0040:
            android.os.Handler r0 = r9.handler
            r0.dispatchMessage(r3)
            return
        L_0x0046:
            r0 = move-exception
            r1 = r2
        L_0x0048:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Cache error"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r5.append(r0)
            java.lang.String r0 = r0.toString()
            com.mediav.ads.sdk.log.MVLog.e(r0)
            goto L_0x0020
        L_0x0063:
            java.lang.Boolean r0 = r9.isUsecache
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x00fc
            java.lang.Boolean r0 = com.mediav.ads.sdk.adcore.HttpRequester.isOpenLog
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0078
            java.lang.String r0 = "异步:缓存未命中"
            com.mediav.ads.sdk.log.MVLog.d(r0)
        L_0x0078:
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r2 = r9.urlString     // Catch:{ Exception -> 0x00d3 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x00d3 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x00d3 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00d3 }
            r2 = 1000(0x3e8, float:1.401E-42)
            r0.setConnectTimeout(r2)     // Catch:{ Exception -> 0x00d3 }
            r2 = 0
            r0.setUseCaches(r2)     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r2 = "GET"
            r0.setRequestMethod(r2)     // Catch:{ Exception -> 0x00d3 }
            r2 = 1
            r0.setDoInput(r2)     // Catch:{ Exception -> 0x00d3 }
            int r2 = r0.getResponseCode()     // Catch:{ Exception -> 0x00d3 }
            r5 = 200(0xc8, float:2.8E-43)
            if (r2 != r5) goto L_0x010b
            java.io.InputStream r2 = r0.getInputStream()     // Catch:{ Exception -> 0x00d3 }
            java.io.BufferedInputStream r5 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x00d3 }
            r5.<init>(r2)     // Catch:{ Exception -> 0x00d3 }
            byte[] r6 = r9.getBytes(r2)     // Catch:{ Exception -> 0x00d3 }
            r5.close()     // Catch:{ Exception -> 0x00d3 }
            r2.close()     // Catch:{ Exception -> 0x00d3 }
            r0.disconnect()     // Catch:{ Exception -> 0x00d3 }
            r0 = 0
            r3.what = r0     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r0 = "data"
            r4.put(r0, r6)     // Catch:{ Exception -> 0x00d3 }
            r3.obj = r4     // Catch:{ Exception -> 0x00d3 }
            java.lang.Boolean r0 = r9.isUsecache     // Catch:{ Exception -> 0x00d3 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x00d3 }
            if (r0 == 0) goto L_0x0040
            if (r1 == 0) goto L_0x0040
            java.lang.String r0 = r9.urlString     // Catch:{ Exception -> 0x00d3 }
            r2 = 86400(0x15180, float:1.21072E-40)
            r1.put(r0, r6, r2)     // Catch:{ Exception -> 0x00d3 }
            goto L_0x0040
        L_0x00d3:
            r0 = move-exception
            r1 = 2
            r3.what = r1
            java.lang.String r1 = "error"
            java.lang.String r2 = r0.getMessage()
            r4.put(r1, r2)
            r3.obj = r4
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "HttpRunable Run Error,url: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r9.urlString
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.mediav.ads.sdk.log.MVLog.e(r8, r1, r0)
            goto L_0x0040
        L_0x00fc:
            java.lang.Boolean r0 = com.mediav.ads.sdk.adcore.HttpRequester.isOpenLog
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0078
            java.lang.String r0 = "异步:不使用缓存"
            com.mediav.ads.sdk.log.MVLog.d(r0)
            goto L_0x0078
        L_0x010b:
            r1 = 1
            r3.what = r1     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r1 = "error"
            int r2 = r0.getResponseCode()     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x00d3 }
            r4.put(r1, r2)     // Catch:{ Exception -> 0x00d3 }
            r3.obj = r4     // Catch:{ Exception -> 0x00d3 }
            r0.disconnect()     // Catch:{ Exception -> 0x00d3 }
            goto L_0x0040
        L_0x0122:
            r0 = move-exception
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mediav.ads.sdk.adcore.HttpRunable.run():void");
    }
}
