package com.mediav.ads.sdk.adcore;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import com.mediav.ads.sdk.interfaces.ActivityBridge;

public class MvActivity extends Activity {
    public static ActivityBridge activityBridge;

    public MvActivity() {
        if (activityBridge != null) {
            activityBridge.onInit(this);
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return activityBridge != null ? activityBridge.dispatchKeyEvent(keyEvent) : super.dispatchKeyEvent(keyEvent);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (activityBridge != null) {
            activityBridge.onConfigurationChanged(configuration);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (activityBridge != null) {
            activityBridge.onCreate(bundle);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (activityBridge != null) {
            activityBridge.onDestroy();
            activityBridge = null;
        }
    }

    public void onLowMemory() {
        super.onLowMemory();
        if (activityBridge != null) {
            activityBridge.onLowMemory();
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (activityBridge != null) {
            activityBridge.onNewIntent(intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (activityBridge != null) {
            activityBridge.onPause();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        if (activityBridge != null) {
            activityBridge.onRestart();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        if (activityBridge != null) {
            activityBridge.onRestoreInstanceState(bundle);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (activityBridge != null) {
            activityBridge.onResume();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (activityBridge != null) {
            activityBridge.onSaveInstanceState(bundle);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (activityBridge != null) {
            activityBridge.onStart();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (activityBridge != null) {
            activityBridge.onStop();
        }
    }

    public void onTrimMemory(int i) {
        super.onTrimMemory(i);
        if (activityBridge != null) {
            activityBridge.onTrimMemory(i);
        }
    }
}
