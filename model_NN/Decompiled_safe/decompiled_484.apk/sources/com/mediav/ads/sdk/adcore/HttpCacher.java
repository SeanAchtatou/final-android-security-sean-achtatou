package com.mediav.ads.sdk.adcore;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Process;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import org.json.JSONArray;
import org.json.JSONObject;

public class HttpCacher {
    private static final int MAX_COUNT = Integer.MAX_VALUE;
    private static final int MAX_SIZE = 50000000;
    public static final int TIME_DAY = 86400;
    public static final int TIME_HOUR = 3600;
    private static Map<String, HttpCacher> mInstanceMap = new HashMap();
    /* access modifiers changed from: private */
    public ACacheManager mCache;

    public class ACacheManager {
        /* access modifiers changed from: private */
        public final AtomicInteger cacheCount;
        protected File cacheDir;
        /* access modifiers changed from: private */
        public final AtomicLong cacheSize;
        private final int countLimit;
        /* access modifiers changed from: private */
        public final Map<File, Long> lastUsageDates;
        private final long sizeLimit;

        private ACacheManager(File file, long j, int i) {
            this.lastUsageDates = Collections.synchronizedMap(new HashMap());
            this.cacheDir = file;
            this.sizeLimit = j;
            this.countLimit = i;
            this.cacheSize = new AtomicLong();
            this.cacheCount = new AtomicInteger();
            calculateCacheSizeAndCacheCount();
        }

        private void calculateCacheSizeAndCacheCount() {
            new Thread(new Runnable() {
                public void run() {
                    File[] listFiles = ACacheManager.this.cacheDir.listFiles();
                    if (listFiles != null) {
                        int i = 0;
                        int i2 = 0;
                        for (File file : listFiles) {
                            i2 = (int) (((long) i2) + ACacheManager.this.calculateSize(file));
                            i++;
                            ACacheManager.this.lastUsageDates.put(file, Long.valueOf(file.lastModified()));
                        }
                        ACacheManager.this.cacheSize.set((long) i2);
                        ACacheManager.this.cacheCount.set(i);
                    }
                }
            }).start();
        }

        /* access modifiers changed from: private */
        public long calculateSize(File file) {
            return file.length();
        }

        /* access modifiers changed from: private */
        public void clear() {
            this.lastUsageDates.clear();
            this.cacheSize.set(0);
            File[] listFiles = this.cacheDir.listFiles();
            if (listFiles != null) {
                for (File delete : listFiles) {
                    delete.delete();
                }
            }
        }

        /* access modifiers changed from: private */
        public File get(String str) {
            File newFile = newFile(str);
            Long valueOf = Long.valueOf(System.currentTimeMillis());
            newFile.setLastModified(valueOf.longValue());
            this.lastUsageDates.put(newFile, valueOf);
            return newFile;
        }

        /* access modifiers changed from: private */
        public File newFile(String str) {
            return new File(this.cacheDir, str.hashCode() + "");
        }

        /* access modifiers changed from: private */
        public void put(File file) {
            int i = this.cacheCount.get();
            while (i + 1 > this.countLimit) {
                this.cacheSize.addAndGet(-removeNext());
                i = this.cacheCount.addAndGet(-1);
            }
            this.cacheCount.addAndGet(1);
            long calculateSize = calculateSize(file);
            long j = this.cacheSize.get();
            while (j + calculateSize > this.sizeLimit) {
                j = this.cacheSize.addAndGet(-removeNext());
            }
            this.cacheSize.addAndGet(calculateSize);
            Long valueOf = Long.valueOf(System.currentTimeMillis());
            file.setLastModified(valueOf.longValue());
            this.lastUsageDates.put(file, valueOf);
        }

        /* access modifiers changed from: private */
        public boolean remove(String str) {
            return get(str).delete();
        }

        private long removeNext() {
            File file;
            Long l;
            File file2 = null;
            if (this.lastUsageDates.isEmpty()) {
                return 0;
            }
            Set<Map.Entry<File, Long>> entrySet = this.lastUsageDates.entrySet();
            synchronized (this.lastUsageDates) {
                Long l2 = null;
                for (Map.Entry next : entrySet) {
                    if (file2 == null) {
                        file = (File) next.getKey();
                        l = (Long) next.getValue();
                    } else {
                        Long l3 = (Long) next.getValue();
                        if (l3.longValue() < l2.longValue()) {
                            File file3 = (File) next.getKey();
                            l = l3;
                            file = file3;
                        } else {
                            file = file2;
                            l = l2;
                        }
                    }
                    file2 = file;
                    l2 = l;
                }
            }
            if (file2 == null) {
                return 0;
            }
            long calculateSize = calculateSize(file2);
            if (!file2.delete()) {
                return calculateSize;
            }
            this.lastUsageDates.remove(file2);
            return calculateSize;
        }
    }

    class Utils {
        private static final char mSeparator = ' ';

        private Utils() {
        }

        /* access modifiers changed from: private */
        public static byte[] Bitmap2Bytes(Bitmap bitmap) {
            if (bitmap == null) {
                return null;
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        }

        /* access modifiers changed from: private */
        public static Bitmap Bytes2Bimap(byte[] bArr) {
            if (bArr.length == 0) {
                return null;
            }
            return BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
        }

        /* access modifiers changed from: private */
        public static Drawable bitmap2Drawable(Bitmap bitmap) {
            if (bitmap == null) {
                return null;
            }
            new BitmapDrawable(bitmap).setTargetDensity(bitmap.getDensity());
            return new BitmapDrawable(bitmap);
        }

        /* access modifiers changed from: private */
        public static String clearDateInfo(String str) {
            return (str == null || !hasDateInfo(str.getBytes())) ? str : str.substring(str.indexOf(32) + 1, str.length());
        }

        /* access modifiers changed from: private */
        public static byte[] clearDateInfo(byte[] bArr) {
            return hasDateInfo(bArr) ? copyOfRange(bArr, indexOf(bArr, mSeparator) + 1, bArr.length) : bArr;
        }

        private static byte[] copyOfRange(byte[] bArr, int i, int i2) {
            int i3 = i2 - i;
            if (i3 < 0) {
                throw new IllegalArgumentException(i + " > " + i2);
            }
            byte[] bArr2 = new byte[i3];
            System.arraycopy(bArr, i, bArr2, 0, Math.min(bArr.length - i, i3));
            return bArr2;
        }

        private static String createDateInfo(int i) {
            String str = System.currentTimeMillis() + "";
            while (str.length() < 13) {
                str = "0" + str;
            }
            return str + "-" + i + ((char) mSeparator);
        }

        /* access modifiers changed from: private */
        public static Bitmap drawable2Bitmap(Drawable drawable) {
            if (drawable == null) {
                return null;
            }
            int intrinsicWidth = drawable.getIntrinsicWidth();
            int intrinsicHeight = drawable.getIntrinsicHeight();
            Bitmap createBitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, drawable.getOpacity() != -1 ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
            Canvas canvas = new Canvas(createBitmap);
            drawable.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
            drawable.draw(canvas);
            return createBitmap;
        }

        private static String[] getDateInfoFromDate(byte[] bArr) {
            if (!hasDateInfo(bArr)) {
                return null;
            }
            return new String[]{new String(copyOfRange(bArr, 0, 13)), new String(copyOfRange(bArr, 14, indexOf(bArr, mSeparator)))};
        }

        private static boolean hasDateInfo(byte[] bArr) {
            return bArr != null && bArr.length > 15 && bArr[13] == 45 && indexOf(bArr, mSeparator) > 14;
        }

        private static int indexOf(byte[] bArr, char c) {
            for (int i = 0; i < bArr.length; i++) {
                if (bArr[i] == c) {
                    return i;
                }
            }
            return -1;
        }

        /* access modifiers changed from: private */
        public static boolean isDue(String str) {
            return isDue(str.getBytes());
        }

        /* access modifiers changed from: private */
        public static boolean isDue(byte[] bArr) {
            String[] dateInfoFromDate = getDateInfoFromDate(bArr);
            if (dateInfoFromDate != null && dateInfoFromDate.length == 2) {
                String str = dateInfoFromDate[0];
                while (str.startsWith("0")) {
                    str = str.substring(1, str.length());
                }
                if (System.currentTimeMillis() > Long.valueOf(str).longValue() + (Long.valueOf(dateInfoFromDate[1]).longValue() * 1000)) {
                    return true;
                }
            }
            return false;
        }

        /* access modifiers changed from: private */
        public static byte[] newByteArrayWithDateInfo(int i, byte[] bArr) {
            byte[] bytes = createDateInfo(i).getBytes();
            byte[] bArr2 = new byte[(bytes.length + bArr.length)];
            System.arraycopy(bytes, 0, bArr2, 0, bytes.length);
            System.arraycopy(bArr, 0, bArr2, bytes.length, bArr.length);
            return bArr2;
        }

        /* access modifiers changed from: private */
        public static String newStringWithDateInfo(int i, String str) {
            return createDateInfo(i) + str;
        }
    }

    class xFileOutputStream extends FileOutputStream {
        File file;

        public xFileOutputStream(File file2) {
            super(file2);
            this.file = file2;
        }

        public void close() {
            super.close();
            HttpCacher.this.mCache.put(this.file);
        }
    }

    private HttpCacher(File file, long j, int i) {
        if (file.exists() || file.mkdirs()) {
            this.mCache = new ACacheManager(file, j, i);
            return;
        }
        throw new RuntimeException("can't make dirs in " + file.getAbsolutePath());
    }

    public static HttpCacher get(Context context) {
        return get(context, "ACache");
    }

    public static HttpCacher get(Context context, long j, int i) {
        return get(new File(context.getCacheDir(), "ACache"), j, i);
    }

    public static HttpCacher get(Context context, String str) {
        return get(new File(context.getCacheDir(), str), 50000000, Integer.MAX_VALUE);
    }

    public static HttpCacher get(File file) {
        return get(file, 50000000, Integer.MAX_VALUE);
    }

    public static HttpCacher get(File file, long j, int i) {
        HttpCacher httpCacher = mInstanceMap.get(file.getAbsoluteFile() + myPid());
        if (httpCacher != null) {
            return httpCacher;
        }
        HttpCacher httpCacher2 = new HttpCacher(file, j, i);
        mInstanceMap.put(file.getAbsolutePath() + myPid(), httpCacher2);
        return httpCacher2;
    }

    private static String myPid() {
        return "_" + Process.myPid();
    }

    public void clear() {
        this.mCache.clear();
    }

    public File file(String str) {
        File access$100 = this.mCache.newFile(str);
        if (access$100.exists()) {
            return access$100;
        }
        return null;
    }

    public InputStream get(String str) {
        File access$400 = this.mCache.get(str);
        if (!access$400.exists()) {
            return null;
        }
        return new FileInputStream(access$400);
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x0062 A[SYNTHETIC, Splitter:B:39:0x0062] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] getAsBinary(java.lang.String r7) {
        /*
            r6 = this;
            r0 = 0
            r1 = 0
            com.mediav.ads.sdk.adcore.HttpCacher$ACacheManager r2 = r6.mCache     // Catch:{ Exception -> 0x004d, all -> 0x005d }
            java.io.File r3 = r2.get(r7)     // Catch:{ Exception -> 0x004d, all -> 0x005d }
            boolean r2 = r3.exists()     // Catch:{ Exception -> 0x004d, all -> 0x005d }
            if (r2 != 0) goto L_0x0019
            if (r0 == 0) goto L_0x0013
            r1.close()     // Catch:{ IOException -> 0x0014 }
        L_0x0013:
            return r0
        L_0x0014:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0013
        L_0x0019:
            java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x004d, all -> 0x005d }
            java.lang.String r1 = "r"
            r2.<init>(r3, r1)     // Catch:{ Exception -> 0x004d, all -> 0x005d }
            long r4 = r2.length()     // Catch:{ Exception -> 0x006d }
            int r1 = (int) r4     // Catch:{ Exception -> 0x006d }
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x006d }
            r2.read(r1)     // Catch:{ Exception -> 0x006d }
            boolean r3 = com.mediav.ads.sdk.adcore.HttpCacher.Utils.isDue(r1)     // Catch:{ Exception -> 0x006d }
            if (r3 != 0) goto L_0x003f
            byte[] r0 = com.mediav.ads.sdk.adcore.HttpCacher.Utils.clearDateInfo(r1)     // Catch:{ Exception -> 0x006d }
            if (r2 == 0) goto L_0x0013
            r2.close()     // Catch:{ IOException -> 0x003a }
            goto L_0x0013
        L_0x003a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0013
        L_0x003f:
            if (r2 == 0) goto L_0x0044
            r2.close()     // Catch:{ IOException -> 0x0048 }
        L_0x0044:
            r6.remove(r7)
            goto L_0x0013
        L_0x0048:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0044
        L_0x004d:
            r1 = move-exception
            r2 = r0
        L_0x004f:
            r1.printStackTrace()     // Catch:{ all -> 0x006b }
            if (r2 == 0) goto L_0x0013
            r2.close()     // Catch:{ IOException -> 0x0058 }
            goto L_0x0013
        L_0x0058:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0013
        L_0x005d:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0060:
            if (r2 == 0) goto L_0x0065
            r2.close()     // Catch:{ IOException -> 0x0066 }
        L_0x0065:
            throw r0
        L_0x0066:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0065
        L_0x006b:
            r0 = move-exception
            goto L_0x0060
        L_0x006d:
            r1 = move-exception
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mediav.ads.sdk.adcore.HttpCacher.getAsBinary(java.lang.String):byte[]");
    }

    public Bitmap getAsBitmap(String str) {
        if (getAsBinary(str) == null) {
            return null;
        }
        return Utils.Bytes2Bimap(getAsBinary(str));
    }

    public Drawable getAsDrawable(String str) {
        if (getAsBinary(str) == null) {
            return null;
        }
        return Utils.bitmap2Drawable(Utils.Bytes2Bimap(getAsBinary(str)));
    }

    public JSONArray getAsJSONArray(String str) {
        try {
            return new JSONArray(getAsString(str));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject getAsJSONObject(String str) {
        try {
            return new JSONObject(getAsString(str));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0032 A[SYNTHETIC, Splitter:B:24:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0037 A[SYNTHETIC, Splitter:B:27:0x0037] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x004b A[SYNTHETIC, Splitter:B:36:0x004b] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0050 A[SYNTHETIC, Splitter:B:39:0x0050] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object getAsObject(java.lang.String r5) {
        /*
            r4 = this;
            r0 = 0
            byte[] r1 = r4.getAsBinary(r5)
            if (r1 == 0) goto L_0x001f
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x002a, all -> 0x0045 }
            r3.<init>(r1)     // Catch:{ Exception -> 0x002a, all -> 0x0045 }
            java.io.ObjectInputStream r2 = new java.io.ObjectInputStream     // Catch:{ Exception -> 0x0064, all -> 0x005e }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0064, all -> 0x005e }
            java.lang.Object r0 = r2.readObject()     // Catch:{ Exception -> 0x0067 }
            if (r3 == 0) goto L_0x001a
            r3.close()     // Catch:{ IOException -> 0x0020 }
        L_0x001a:
            if (r2 == 0) goto L_0x001f
            r2.close()     // Catch:{ IOException -> 0x0025 }
        L_0x001f:
            return r0
        L_0x0020:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001a
        L_0x0025:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001f
        L_0x002a:
            r1 = move-exception
            r2 = r0
            r3 = r0
        L_0x002d:
            r1.printStackTrace()     // Catch:{ all -> 0x0062 }
            if (r3 == 0) goto L_0x0035
            r3.close()     // Catch:{ IOException -> 0x0040 }
        L_0x0035:
            if (r2 == 0) goto L_0x001f
            r2.close()     // Catch:{ IOException -> 0x003b }
            goto L_0x001f
        L_0x003b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001f
        L_0x0040:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0035
        L_0x0045:
            r1 = move-exception
            r2 = r0
            r3 = r0
            r0 = r1
        L_0x0049:
            if (r3 == 0) goto L_0x004e
            r3.close()     // Catch:{ IOException -> 0x0054 }
        L_0x004e:
            if (r2 == 0) goto L_0x0053
            r2.close()     // Catch:{ IOException -> 0x0059 }
        L_0x0053:
            throw r0
        L_0x0054:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004e
        L_0x0059:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0053
        L_0x005e:
            r1 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x0049
        L_0x0062:
            r0 = move-exception
            goto L_0x0049
        L_0x0064:
            r1 = move-exception
            r2 = r0
            goto L_0x002d
        L_0x0067:
            r1 = move-exception
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mediav.ads.sdk.adcore.HttpCacher.getAsObject(java.lang.String):java.lang.Object");
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x005c A[SYNTHETIC, Splitter:B:29:0x005c] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x006a A[SYNTHETIC, Splitter:B:36:0x006a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getAsString(java.lang.String r6) {
        /*
            r5 = this;
            r0 = 0
            com.mediav.ads.sdk.adcore.HttpCacher$ACacheManager r1 = r5.mCache
            java.io.File r1 = r1.get(r6)
            boolean r2 = r1.exists()
            if (r2 != 0) goto L_0x000e
        L_0x000d:
            return r0
        L_0x000e:
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0055, all -> 0x0065 }
            java.io.FileReader r3 = new java.io.FileReader     // Catch:{ Exception -> 0x0055, all -> 0x0065 }
            r3.<init>(r1)     // Catch:{ Exception -> 0x0055, all -> 0x0065 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0055, all -> 0x0065 }
            java.lang.String r1 = ""
        L_0x001a:
            java.lang.String r3 = r2.readLine()     // Catch:{ Exception -> 0x0075 }
            if (r3 == 0) goto L_0x0032
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0075 }
            r4.<init>()     // Catch:{ Exception -> 0x0075 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ Exception -> 0x0075 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0075 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0075 }
            goto L_0x001a
        L_0x0032:
            boolean r3 = com.mediav.ads.sdk.adcore.HttpCacher.Utils.isDue(r1)     // Catch:{ Exception -> 0x0075 }
            if (r3 != 0) goto L_0x0047
            java.lang.String r0 = com.mediav.ads.sdk.adcore.HttpCacher.Utils.clearDateInfo(r1)     // Catch:{ Exception -> 0x0075 }
            if (r2 == 0) goto L_0x000d
            r2.close()     // Catch:{ IOException -> 0x0042 }
            goto L_0x000d
        L_0x0042:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000d
        L_0x0047:
            if (r2 == 0) goto L_0x004c
            r2.close()     // Catch:{ IOException -> 0x0050 }
        L_0x004c:
            r5.remove(r6)
            goto L_0x000d
        L_0x0050:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004c
        L_0x0055:
            r1 = move-exception
            r2 = r0
        L_0x0057:
            r1.printStackTrace()     // Catch:{ all -> 0x0073 }
            if (r2 == 0) goto L_0x000d
            r2.close()     // Catch:{ IOException -> 0x0060 }
            goto L_0x000d
        L_0x0060:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000d
        L_0x0065:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0068:
            if (r2 == 0) goto L_0x006d
            r2.close()     // Catch:{ IOException -> 0x006e }
        L_0x006d:
            throw r0
        L_0x006e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006d
        L_0x0073:
            r0 = move-exception
            goto L_0x0068
        L_0x0075:
            r1 = move-exception
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mediav.ads.sdk.adcore.HttpCacher.getAsString(java.lang.String):java.lang.String");
    }

    public OutputStream put(String str) {
        return new xFileOutputStream(this.mCache.newFile(str));
    }

    public void put(String str, Bitmap bitmap) {
        put(str, Utils.Bitmap2Bytes(bitmap));
    }

    public void put(String str, Bitmap bitmap, int i) {
        put(str, Utils.Bitmap2Bytes(bitmap), i);
    }

    public void put(String str, Drawable drawable) {
        put(str, Utils.drawable2Bitmap(drawable));
    }

    public void put(String str, Drawable drawable, int i) {
        put(str, Utils.drawable2Bitmap(drawable), i);
    }

    public void put(String str, Serializable serializable) {
        put(str, serializable, -1);
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0042 A[SYNTHETIC, Splitter:B:25:0x0042] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void put(java.lang.String r4, java.io.Serializable r5, int r6) {
        /*
            r3 = this;
            r2 = 0
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0051, all -> 0x003e }
            r0.<init>()     // Catch:{ Exception -> 0x0051, all -> 0x003e }
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x0051, all -> 0x003e }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0051, all -> 0x003e }
            r1.writeObject(r5)     // Catch:{ Exception -> 0x0022 }
            byte[] r0 = r0.toByteArray()     // Catch:{ Exception -> 0x0022 }
            r2 = -1
            if (r6 == r2) goto L_0x001e
            r3.put(r4, r0, r6)     // Catch:{ Exception -> 0x0022 }
        L_0x0018:
            if (r1 == 0) goto L_0x001d
            r1.close()     // Catch:{ IOException -> 0x0035 }
        L_0x001d:
            return
        L_0x001e:
            r3.put(r4, r0)     // Catch:{ Exception -> 0x0022 }
            goto L_0x0018
        L_0x0022:
            r0 = move-exception
        L_0x0023:
            r0.printStackTrace()     // Catch:{ all -> 0x004f }
            if (r1 == 0) goto L_0x001d
            r1.close()     // Catch:{ IOException -> 0x002c }
            goto L_0x001d
        L_0x002c:
            r0 = move-exception
            java.lang.String r0 = r0.getMessage()
            com.mediav.ads.sdk.log.MVLog.e(r0)
            goto L_0x001d
        L_0x0035:
            r0 = move-exception
            java.lang.String r0 = r0.getMessage()
            com.mediav.ads.sdk.log.MVLog.e(r0)
            goto L_0x001d
        L_0x003e:
            r0 = move-exception
            r1 = r2
        L_0x0040:
            if (r1 == 0) goto L_0x0045
            r1.close()     // Catch:{ IOException -> 0x0046 }
        L_0x0045:
            throw r0
        L_0x0046:
            r1 = move-exception
            java.lang.String r1 = r1.getMessage()
            com.mediav.ads.sdk.log.MVLog.e(r1)
            goto L_0x0045
        L_0x004f:
            r0 = move-exception
            goto L_0x0040
        L_0x0051:
            r0 = move-exception
            r1 = r2
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mediav.ads.sdk.adcore.HttpCacher.put(java.lang.String, java.io.Serializable, int):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0030 A[SYNTHETIC, Splitter:B:16:0x0030] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0044 A[SYNTHETIC, Splitter:B:23:0x0044] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void put(java.lang.String r6, java.lang.String r7) {
        /*
            r5 = this;
            com.mediav.ads.sdk.adcore.HttpCacher$ACacheManager r0 = r5.mCache
            java.io.File r3 = r0.newFile(r6)
            r2 = 0
            java.io.BufferedWriter r1 = new java.io.BufferedWriter     // Catch:{ IOException -> 0x0029, all -> 0x0041 }
            java.io.FileWriter r0 = new java.io.FileWriter     // Catch:{ IOException -> 0x0029, all -> 0x0041 }
            r0.<init>(r3)     // Catch:{ IOException -> 0x0029, all -> 0x0041 }
            r4 = 1024(0x400, float:1.435E-42)
            r1.<init>(r0, r4)     // Catch:{ IOException -> 0x0029, all -> 0x0041 }
            r1.write(r7)     // Catch:{ IOException -> 0x0058 }
            if (r1 == 0) goto L_0x001e
            r1.flush()     // Catch:{ IOException -> 0x0024 }
            r1.close()     // Catch:{ IOException -> 0x0024 }
        L_0x001e:
            com.mediav.ads.sdk.adcore.HttpCacher$ACacheManager r0 = r5.mCache
            r0.put(r3)
        L_0x0023:
            return
        L_0x0024:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x001e
        L_0x0029:
            r0 = move-exception
            r1 = r2
        L_0x002b:
            r0.printStackTrace()     // Catch:{ all -> 0x0055 }
            if (r1 == 0) goto L_0x0036
            r1.flush()     // Catch:{ IOException -> 0x003c }
            r1.close()     // Catch:{ IOException -> 0x003c }
        L_0x0036:
            com.mediav.ads.sdk.adcore.HttpCacher$ACacheManager r0 = r5.mCache
            r0.put(r3)
            goto L_0x0023
        L_0x003c:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0036
        L_0x0041:
            r0 = move-exception
        L_0x0042:
            if (r2 == 0) goto L_0x004a
            r2.flush()     // Catch:{ IOException -> 0x0050 }
            r2.close()     // Catch:{ IOException -> 0x0050 }
        L_0x004a:
            com.mediav.ads.sdk.adcore.HttpCacher$ACacheManager r1 = r5.mCache
            r1.put(r3)
            throw r0
        L_0x0050:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004a
        L_0x0055:
            r0 = move-exception
            r2 = r1
            goto L_0x0042
        L_0x0058:
            r0 = move-exception
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mediav.ads.sdk.adcore.HttpCacher.put(java.lang.String, java.lang.String):void");
    }

    public void put(String str, String str2, int i) {
        put(str, Utils.newStringWithDateInfo(i, str2));
    }

    public void put(String str, JSONArray jSONArray) {
        put(str, jSONArray.toString());
    }

    public void put(String str, JSONArray jSONArray, int i) {
        put(str, jSONArray.toString(), i);
    }

    public void put(String str, JSONObject jSONObject) {
        put(str, jSONObject.toString());
    }

    public void put(String str, JSONObject jSONObject, int i) {
        put(str, jSONObject.toString(), i);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0029 A[SYNTHETIC, Splitter:B:16:0x0029] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x003e A[SYNTHETIC, Splitter:B:24:0x003e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void put(java.lang.String r5, byte[] r6) {
        /*
            r4 = this;
            com.mediav.ads.sdk.adcore.HttpCacher$ACacheManager r0 = r4.mCache
            java.io.File r3 = r0.newFile(r5)
            r2 = 0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0022, all -> 0x003a }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0022, all -> 0x003a }
            r1.write(r6)     // Catch:{ Exception -> 0x0051 }
            if (r1 == 0) goto L_0x0017
            r1.flush()     // Catch:{ IOException -> 0x001d }
            r1.close()     // Catch:{ IOException -> 0x001d }
        L_0x0017:
            com.mediav.ads.sdk.adcore.HttpCacher$ACacheManager r0 = r4.mCache
            r0.put(r3)
        L_0x001c:
            return
        L_0x001d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0017
        L_0x0022:
            r0 = move-exception
            r1 = r2
        L_0x0024:
            r0.printStackTrace()     // Catch:{ all -> 0x004f }
            if (r1 == 0) goto L_0x002f
            r1.flush()     // Catch:{ IOException -> 0x0035 }
            r1.close()     // Catch:{ IOException -> 0x0035 }
        L_0x002f:
            com.mediav.ads.sdk.adcore.HttpCacher$ACacheManager r0 = r4.mCache
            r0.put(r3)
            goto L_0x001c
        L_0x0035:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002f
        L_0x003a:
            r0 = move-exception
            r1 = r2
        L_0x003c:
            if (r1 == 0) goto L_0x0044
            r1.flush()     // Catch:{ IOException -> 0x004a }
            r1.close()     // Catch:{ IOException -> 0x004a }
        L_0x0044:
            com.mediav.ads.sdk.adcore.HttpCacher$ACacheManager r1 = r4.mCache
            r1.put(r3)
            throw r0
        L_0x004a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0044
        L_0x004f:
            r0 = move-exception
            goto L_0x003c
        L_0x0051:
            r0 = move-exception
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mediav.ads.sdk.adcore.HttpCacher.put(java.lang.String, byte[]):void");
    }

    public void put(String str, byte[] bArr, int i) {
        put(str, Utils.newByteArrayWithDateInfo(i, bArr));
    }

    public boolean remove(String str) {
        return this.mCache.remove(str);
    }
}
