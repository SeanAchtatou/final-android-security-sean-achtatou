package com.mediav.ads.sdk.interfaces;

public interface IMvLandingPageListener {
    void onPageClose();

    void onPageLoadFailed();

    void onPageLoadFinished();
}
