package com.qihoo.dynamic.patch;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.Signature;
import android.text.TextUtils;
import com.qihoo.dynamic.util.GlobalFlag;
import com.qihoo.dynamic.util.IO;
import com.qihoo.dynamic.util.Md5Util;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DynamicPatchManager {
    public static final String PLUGIN_SIGN_MD5 = "3C82988056BBD600F3CA86F3179F32C2";
    private static DynamicPatchManager instance;
    private Context context;
    private Map<String, PatchInfo> patchs = new HashMap();

    private DynamicPatchManager() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x0082  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.qihoo.dynamic.patch.PatchInfo applyPatch(android.content.Context r7, java.lang.String r8, java.lang.String r9, java.lang.String r10) {
        /*
            r6 = this;
            r0 = 0
            java.util.Map<java.lang.String, com.qihoo.dynamic.patch.PatchInfo> r1 = r6.patchs
            boolean r1 = r1.containsKey(r8)
            if (r1 != 0) goto L_0x0036
            java.io.File r1 = new java.io.File
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.io.File r3 = r7.getFilesDir()
            java.lang.String r3 = r3.getAbsolutePath()
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r2.<init>(r3)
            java.lang.String r3 = "/dex/"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r8)
            java.lang.String r3 = "/"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            r6.deleteDir(r1)
        L_0x0036:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.io.File r2 = r7.getFilesDir()
            java.lang.String r2 = r2.getAbsolutePath()
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r1.<init>(r2)
            java.lang.String r2 = "/dex/"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r2 = "/"
            java.lang.StringBuilder r1 = r1.append(r2)
            long r2 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r1.toString()
            java.io.File r1 = new java.io.File
            r1.<init>(r2)
            r1.mkdirs()
            boolean r1 = r6.verifySign(r7, r9)     // Catch:{ Exception -> 0x00c3 }
            if (r1 == 0) goto L_0x00c7
            dalvik.system.DexClassLoader r1 = new dalvik.system.DexClassLoader     // Catch:{ Exception -> 0x00c3 }
            java.lang.String r3 = ""
            java.lang.Class r4 = r6.getClass()     // Catch:{ Exception -> 0x00c3 }
            java.lang.ClassLoader r4 = r4.getClassLoader()     // Catch:{ Exception -> 0x00c3 }
            r1.<init>(r9, r2, r3, r4)     // Catch:{ Exception -> 0x00c3 }
        L_0x0080:
            if (r1 == 0) goto L_0x00c2
            com.qihoo.dynamic.patch.PatchInfo r0 = new com.qihoo.dynamic.patch.PatchInfo
            r0.<init>()
            r0.setName(r8)
            r0.setPath(r9)
            r0.setClassLoader(r1)
            if (r10 == 0) goto L_0x00bd
            java.lang.Class r1 = r1.loadClass(r10)     // Catch:{ Throwable -> 0x00c9 }
            r0.setMainClass(r1)     // Catch:{ Throwable -> 0x00c9 }
            java.lang.String r2 = "main"
            r3 = 2
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x00c9 }
            r4 = 0
            java.lang.Class<android.content.Context> r5 = android.content.Context.class
            r3[r4] = r5     // Catch:{ Throwable -> 0x00c9 }
            r4 = 1
            java.lang.Class<java.util.Map> r5 = java.util.Map.class
            r3[r4] = r5     // Catch:{ Throwable -> 0x00c9 }
            java.lang.reflect.Method r2 = r1.getMethod(r2, r3)     // Catch:{ Throwable -> 0x00c9 }
            java.util.HashMap r3 = new java.util.HashMap     // Catch:{ Throwable -> 0x00c9 }
            r3.<init>()     // Catch:{ Throwable -> 0x00c9 }
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x00c9 }
            r5 = 0
            r4[r5] = r7     // Catch:{ Throwable -> 0x00c9 }
            r5 = 1
            r4[r5] = r3     // Catch:{ Throwable -> 0x00c9 }
            r2.invoke(r1, r4)     // Catch:{ Throwable -> 0x00c9 }
        L_0x00bd:
            java.util.Map<java.lang.String, com.qihoo.dynamic.patch.PatchInfo> r1 = r6.patchs
            r1.put(r8, r0)
        L_0x00c2:
            return r0
        L_0x00c3:
            r1 = move-exception
            r1.printStackTrace()
        L_0x00c7:
            r1 = r0
            goto L_0x0080
        L_0x00c9:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00bd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo.dynamic.patch.DynamicPatchManager.applyPatch(android.content.Context, java.lang.String, java.lang.String, java.lang.String):com.qihoo.dynamic.patch.PatchInfo");
    }

    private void deleteDir(File file) {
        File[] listFiles;
        if (file != null) {
            if (file.isDirectory() && (listFiles = file.listFiles()) != null) {
                for (File deleteDir : listFiles) {
                    deleteDir(deleteDir);
                }
            }
            file.delete();
        }
    }

    public static DynamicPatchManager getInstance() {
        if (instance == null) {
            instance = new DynamicPatchManager();
        }
        return instance;
    }

    private String getSign(Signature[] signatureArr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            for (Signature byteArray : signatureArr) {
                byteArrayOutputStream.write(byteArray.toByteArray());
            }
            String md5str = Md5Util.md5str(byteArrayOutputStream.toByteArray());
            try {
                byteArrayOutputStream.close();
                return md5str;
            } catch (IOException e) {
                e.printStackTrace();
                return md5str;
            }
        } catch (IOException e2) {
            e2.printStackTrace();
            try {
                byteArrayOutputStream.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            return null;
        } catch (Throwable th) {
            try {
                byteArrayOutputStream.close();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
            throw th;
        }
    }

    private boolean verifySign(Context context2, String str) {
        PackageInfo packageArchiveInfo = context2.getPackageManager().getPackageArchiveInfo(str, 64);
        if (packageArchiveInfo == null || packageArchiveInfo.signatures == null) {
            return false;
        }
        return getSign(packageArchiveInfo.signatures).equals(PLUGIN_SIGN_MD5);
    }

    public PatchInfo getOrInstallPatch(Context context2, String str, String str2, String str3, String str4, String str5) {
        if (!TextUtils.isEmpty(str4) && !GlobalFlag.isChecked(context2, str4)) {
            GlobalFlag.check(context2, str4);
            try {
                if (TextUtils.isEmpty(str2)) {
                    return null;
                }
                File file = new File(str2);
                if (!TextUtils.isEmpty(str5) && !file.exists()) {
                    IO.copy(context2.getAssets().open(str5), str2);
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return this.patchs.containsKey(str) ? this.patchs.get(str) : applyPatch(context2, str, str2, str3);
    }

    public void reset() {
        if (this.patchs == null) {
            this.patchs = new HashMap();
        }
        this.patchs.clear();
    }
}
