package com.qihoo.dynamic.patch;

public class PatchInfo {
    private ClassLoader classLoader;
    private Class mainClass;
    private String name;
    private String path;

    public ClassLoader getClassLoader() {
        return this.classLoader;
    }

    public Class getMainClass() {
        return this.mainClass;
    }

    public String getName() {
        return this.name;
    }

    public String getPath() {
        return this.path;
    }

    public void setClassLoader(ClassLoader classLoader2) {
        this.classLoader = classLoader2;
    }

    public void setMainClass(Class cls) {
        this.mainClass = cls;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setPath(String str) {
        this.path = str;
    }
}
