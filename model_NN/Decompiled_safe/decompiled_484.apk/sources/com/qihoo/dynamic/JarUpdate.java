package com.qihoo.dynamic;

import android.content.Context;
import android.text.TextUtils;
import com.qihoo.dynamic.util.GlobalFlag;
import com.qihoo.dynamic.util.IO;
import java.io.File;
import java.io.FileInputStream;

public class JarUpdate {
    private static Class<?>[][] updateFunction = {new Class[]{Context.class, Integer.class}};

    public static boolean onJarUpdate(Context context) {
        return onJarUpdate(context, JarLoader.getJarPath(context));
    }

    public static boolean onJarUpdate(final Context context, String str) {
        if (TextUtils.isEmpty(str) || !str.endsWith(".jar") || !new File(str).exists()) {
            return false;
        }
        String jarPath = JarLoader.getJarPath(context);
        if (!str.equals(jarPath)) {
            try {
                IO.copy(new FileInputStream(new File(str)), jarPath);
            } catch (Throwable th) {
                return false;
            }
        }
        try {
            JarLoader.reset(context);
            EmulatorCheck.reset(context);
            GlobalFlag.setUpdate(context, false);
            new Thread(new Runnable() {
                public void run() {
                    EmulatorCheck.isEmulator(context);
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public static int testJarUpdate(Context context, int i) {
        try {
            return ((Integer) JarLoader.getInstance(context).execFunction("testUpdateLib", updateFunction[0], context, Integer.valueOf(i))).intValue();
        } catch (Throwable th) {
            th.printStackTrace();
            return -1;
        }
    }
}
