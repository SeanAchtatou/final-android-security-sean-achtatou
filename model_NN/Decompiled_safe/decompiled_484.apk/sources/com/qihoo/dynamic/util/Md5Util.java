package com.qihoo.dynamic.util;

import android.text.TextUtils;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Md5Util {
    public static final String ALGORITHM = "MD5";
    public static final String DEFAULT_CHARSET = "UTF-8";
    private static final char[] HEX = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private static final char[] HEX_LOWER_CASE = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0038 A[SYNTHETIC, Splitter:B:24:0x0038] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String md5(java.io.File r7) {
        /*
            r0 = 0
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r1]
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0042, all -> 0x0032 }
            r1.<init>(r7)     // Catch:{ Exception -> 0x0042, all -> 0x0032 }
            java.lang.String r3 = "MD5"
            java.security.MessageDigest r3 = java.security.MessageDigest.getInstance(r3)     // Catch:{ Exception -> 0x0029, all -> 0x0040 }
        L_0x0010:
            int r4 = r1.read(r2)     // Catch:{ Exception -> 0x0029, all -> 0x0040 }
            if (r4 > 0) goto L_0x0024
            byte[] r2 = r3.digest()     // Catch:{ Exception -> 0x0029, all -> 0x0040 }
            java.lang.String r0 = toHex(r2)     // Catch:{ Exception -> 0x0029, all -> 0x0040 }
            if (r1 == 0) goto L_0x0023
            r1.close()     // Catch:{ IOException -> 0x003c }
        L_0x0023:
            return r0
        L_0x0024:
            r5 = 0
            r3.update(r2, r5, r4)     // Catch:{ Exception -> 0x0029, all -> 0x0040 }
            goto L_0x0010
        L_0x0029:
            r2 = move-exception
        L_0x002a:
            if (r1 == 0) goto L_0x0023
            r1.close()     // Catch:{ IOException -> 0x0030 }
            goto L_0x0023
        L_0x0030:
            r1 = move-exception
            goto L_0x0023
        L_0x0032:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0036:
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ IOException -> 0x003e }
        L_0x003b:
            throw r0
        L_0x003c:
            r1 = move-exception
            goto L_0x0023
        L_0x003e:
            r1 = move-exception
            goto L_0x003b
        L_0x0040:
            r0 = move-exception
            goto L_0x0036
        L_0x0042:
            r1 = move-exception
            r1 = r0
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo.dynamic.util.Md5Util.md5(java.io.File):java.lang.String");
    }

    public static String md5(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance(ALGORITHM);
            instance.update(str.getBytes(DEFAULT_CHARSET));
            return toHex(instance.digest());
        } catch (UnsupportedEncodingException | Exception | NoSuchAlgorithmException e) {
            return str;
        }
    }

    public static String md5(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance(ALGORITHM);
            instance.update(bArr);
            return toHex(instance.digest());
        } catch (NoSuchAlgorithmException e) {
            return "";
        }
    }

    public static String md5LowerCase(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        try {
            MessageDigest instance = MessageDigest.getInstance(ALGORITHM);
            instance.update(str.getBytes(DEFAULT_CHARSET));
            return toLowerCaseHex(instance.digest());
        } catch (UnsupportedEncodingException | Exception | NoSuchAlgorithmException e) {
            return str;
        }
    }

    public static String md5str(byte[] bArr) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance(ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        messageDigest.update(bArr, 0, bArr.length);
        byte[] digest = messageDigest.digest();
        String str = new String();
        for (byte b2 : digest) {
            byte b3 = b2 & 255;
            if (b3 <= 15) {
                str = String.valueOf(str) + "0";
            }
            str = String.valueOf(str) + Integer.toHexString(b3);
        }
        return str.toUpperCase();
    }

    private static String toHex(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (byte b2 : bArr) {
            sb.append(HEX[(b2 & 240) >> 4]);
            sb.append(HEX[b2 & 15]);
        }
        return sb.toString();
    }

    private static String toLowerCaseHex(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (byte b2 : bArr) {
            sb.append(HEX_LOWER_CASE[(b2 & 240) >> 4]);
            sb.append(HEX_LOWER_CASE[b2 & 15]);
        }
        return sb.toString();
    }
}
