package com.qihoo.dynamic.util;

import android.content.Context;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;

public class IO {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public static void appendString(File file, String str) {
        FileOutputStream fileOutputStream = new FileOutputStream(file, true);
        fileOutputStream.write(str.getBytes());
        fileOutputStream.close();
    }

    public static void appendString(OutputStream outputStream, String str) {
        outputStream.write(str.getBytes());
    }

    public static void appendString(String str, String str2) {
        appendString(new File(str), str2);
    }

    public static synchronized boolean copy(Context context, InputStream inputStream, String str) {
        boolean z = false;
        synchronized (IO.class) {
            FileOutputStream fileOutputStream = null;
            try {
                fileOutputStream = context.openFileOutput(str, 0);
                byte[] bArr = new byte[8192];
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    fileOutputStream.write(bArr, 0, read);
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (Exception e) {
                    }
                }
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (Exception e2) {
                    }
                }
                z = true;
            } catch (Exception e3) {
                e3.printStackTrace();
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (Exception e4) {
                    }
                }
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (Exception e5) {
                    }
                }
            } catch (Throwable th) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (Exception e6) {
                    }
                }
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (Exception e7) {
                    }
                }
                throw th;
            }
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0021 A[SYNTHETIC, Splitter:B:18:0x0021] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0033 A[SYNTHETIC, Splitter:B:30:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x003f A[SYNTHETIC, Splitter:B:37:0x003f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean copy(android.content.Context r5, java.lang.String r6, java.lang.String r7) {
        /*
            r0 = 0
            java.lang.Class<com.qihoo.dynamic.util.IO> r4 = com.qihoo.dynamic.util.IO.class
            monitor-enter(r4)
            r3 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x001a, IOException -> 0x002d }
            java.io.File r1 = new java.io.File     // Catch:{ FileNotFoundException -> 0x001a, IOException -> 0x002d }
            r1.<init>(r6)     // Catch:{ FileNotFoundException -> 0x001a, IOException -> 0x002d }
            r2.<init>(r1)     // Catch:{ FileNotFoundException -> 0x001a, IOException -> 0x002d }
            copy(r5, r2, r7)     // Catch:{ FileNotFoundException -> 0x0053, IOException -> 0x0050 }
            if (r2 == 0) goto L_0x0017
            r2.close()     // Catch:{ IOException -> 0x0048 }
        L_0x0017:
            r0 = 1
        L_0x0018:
            monitor-exit(r4)
            return r0
        L_0x001a:
            r1 = move-exception
            r2 = r3
        L_0x001c:
            r1.printStackTrace()     // Catch:{ all -> 0x004d }
            if (r2 == 0) goto L_0x0018
            r2.close()     // Catch:{ IOException -> 0x0025 }
            goto L_0x0018
        L_0x0025:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x002a }
            goto L_0x0018
        L_0x002a:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x002d:
            r1 = move-exception
        L_0x002e:
            r1.printStackTrace()     // Catch:{ all -> 0x003c }
            if (r3 == 0) goto L_0x0018
            r3.close()     // Catch:{ IOException -> 0x0037 }
            goto L_0x0018
        L_0x0037:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x002a }
            goto L_0x0018
        L_0x003c:
            r0 = move-exception
        L_0x003d:
            if (r3 == 0) goto L_0x0042
            r3.close()     // Catch:{ IOException -> 0x0043 }
        L_0x0042:
            throw r0     // Catch:{ all -> 0x002a }
        L_0x0043:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x002a }
            goto L_0x0042
        L_0x0048:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x002a }
            goto L_0x0017
        L_0x004d:
            r0 = move-exception
            r3 = r2
            goto L_0x003d
        L_0x0050:
            r1 = move-exception
            r3 = r2
            goto L_0x002e
        L_0x0053:
            r1 = move-exception
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo.dynamic.util.IO.copy(android.content.Context, java.lang.String, java.lang.String):boolean");
    }

    public static boolean copy(InputStream inputStream, String str) {
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = new FileOutputStream(new File(str));
            try {
                byte[] bArr = new byte[10240];
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    fileOutputStream.write(bArr, 0, read);
                }
                if (fileOutputStream == null) {
                    return true;
                }
                try {
                    fileOutputStream.close();
                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    return true;
                }
            } catch (Throwable th) {
                th = th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileOutputStream = null;
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            throw th;
        }
    }

    public static void delete(File file) {
        file.delete();
    }

    public static void delete(String str) {
        new File(str).delete();
    }

    public static void mv(String str, String str2) {
        try {
            Runtime.getRuntime().exec(String.format("mv %s %s", str, str2));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static byte[] readBytes(InputStream inputStream) {
        byte[] bArr = new byte[1024];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = inputStream.read(bArr);
            if (read <= 0) {
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                byteArrayOutputStream.close();
                return byteArray;
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    public static byte[] readBytes(String str) {
        FileInputStream fileInputStream = new FileInputStream(str);
        byte[] readBytes = readBytes(fileInputStream);
        fileInputStream.close();
        return readBytes;
    }

    public static String readString(File file) {
        FileInputStream fileInputStream = new FileInputStream(file);
        String readString = readString(fileInputStream);
        fileInputStream.close();
        return readString;
    }

    public static String readString(InputStream inputStream) {
        return new String(readBytes(inputStream), Md5Util.DEFAULT_CHARSET);
    }

    public static String readString(String str) {
        return readString(new File(str));
    }

    public static void serialize(Serializable serializable, String str) {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(str));
        try {
            objectOutputStream.writeObject(serializable);
            try {
                objectOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e2) {
            e2.printStackTrace();
            throw e2;
        } catch (Throwable th) {
            try {
                objectOutputStream.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x002d A[SYNTHETIC, Splitter:B:21:0x002d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object unserialize(java.lang.String r4) {
        /*
            r0 = 0
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch:{ Exception -> 0x001a, all -> 0x0027 }
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x001a, all -> 0x0027 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x001a, all -> 0x0027 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x001a, all -> 0x0027 }
            java.lang.Object r0 = r1.readObject()     // Catch:{ Exception -> 0x0038, all -> 0x0036 }
            if (r1 == 0) goto L_0x0014
            r1.close()     // Catch:{ IOException -> 0x0015 }
        L_0x0014:
            return r0
        L_0x0015:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0014
        L_0x001a:
            r1 = move-exception
            r1 = r0
        L_0x001c:
            if (r1 == 0) goto L_0x0014
            r1.close()     // Catch:{ IOException -> 0x0022 }
            goto L_0x0014
        L_0x0022:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0014
        L_0x0027:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
        L_0x002b:
            if (r1 == 0) goto L_0x0030
            r1.close()     // Catch:{ IOException -> 0x0031 }
        L_0x0030:
            throw r0
        L_0x0031:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0030
        L_0x0036:
            r0 = move-exception
            goto L_0x002b
        L_0x0038:
            r2 = move-exception
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo.dynamic.util.IO.unserialize(java.lang.String):java.lang.Object");
    }

    public static void writeString(File file, String str) {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(str.getBytes());
        fileOutputStream.close();
    }

    public static void writeString(OutputStream outputStream, String str) {
        outputStream.write(str.getBytes());
    }

    public static void writeString(String str, String str2) {
        writeString(new File(str), str2);
    }

    public static void writeUTF8String(File file, String str) {
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file), Md5Util.DEFAULT_CHARSET);
        outputStreamWriter.write(str);
        outputStreamWriter.close();
    }

    public static void writeUTF8String(String str, String str2) {
        writeUTF8String(new File(str), str2);
    }
}
