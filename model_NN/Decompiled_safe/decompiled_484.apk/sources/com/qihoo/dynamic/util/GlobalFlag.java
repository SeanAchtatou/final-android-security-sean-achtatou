package com.qihoo.dynamic.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;

public class GlobalFlag {
    private static final String PREFS_NAME = "GlobalFlag";
    private static final String SUFFIX = "__public_util_global_flag";

    public static final void check(Context context, String str) {
        SharedPreferences.Editor edit = context.getApplicationContext().getSharedPreferences(PREFS_NAME, 0).edit();
        edit.putBoolean(String.valueOf(str) + "_" + getVersionCode(context) + "_" + SUFFIX, true);
        edit.commit();
    }

    private static String getVersion(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    private static int getVersionCode(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static final boolean isChecked(Context context, String str) {
        return context.getApplicationContext().getSharedPreferences(PREFS_NAME, 0).getBoolean(String.valueOf(str) + "_" + getVersionCode(context) + "_" + SUFFIX, false);
    }

    public static final boolean isUpdate(Context context) {
        return context.getApplicationContext().getSharedPreferences(PREFS_NAME, 0).getBoolean("LibUpdateFlag", false);
    }

    public static final void setUpdate(Context context, boolean z) {
        SharedPreferences.Editor edit = context.getApplicationContext().getSharedPreferences(PREFS_NAME, 0).edit();
        edit.putBoolean("LibUpdateFlag", z);
        edit.commit();
    }

    public static final void uncheck(Context context, String str) {
        SharedPreferences.Editor edit = context.getApplicationContext().getSharedPreferences(PREFS_NAME, 0).edit();
        edit.putBoolean(String.valueOf(str) + "_" + getVersionCode(context) + "_" + SUFFIX, false);
        edit.commit();
    }
}
