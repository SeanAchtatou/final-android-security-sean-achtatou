package com.qihoo.dynamic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.qihoo.iface.RequestEvent;
import java.util.HashMap;
import java.util.Map;

public class DotCount {
    public static String APP_NAME = "mso";
    public static final int VXERR_CANCELED = -7;
    public static final int VXERR_DEC_RESPONSE = -3;
    public static final int VXERR_ENC_REQUEST = -2;
    public static final int VXERR_INVAL_ARGS = -1;
    public static final int VXERR_NETQUERY = -8;
    public static final int VXERR_NO_SYMKEY = -5;
    public static final int VXERR_OK = 0;
    public static final int VXERR_OUTOFMEMORY = -4;
    public static final int VXERR_SYMKEY_EXPIRED = -6;
    public static final int VXERR_UNKNOW_ERR = -111;
    public static final int VXERR_VERIFY_FAIL = -9;
    private static Class<?>[][] dotFunction = {new Class[]{Context.class, String.class, String.class, Map.class}, new Class[]{Context.class, String.class, String.class, Map.class, Integer.class}, new Class[]{Context.class, String.class, String.class, Map.class, RequestEvent.class}};

    private static void dumpErrorMsg(Context context, String str, int i) {
        String str2;
        switch (i) {
            case VXERR_UNKNOW_ERR /*-111*/:
                str2 = "打点失败:未知错误! url=" + str;
                break;
            case VXERR_VERIFY_FAIL /*-9*/:
                str2 = "打点失败:反向验证失败! url=" + str;
                break;
            case VXERR_NETQUERY /*-8*/:
                str2 = "打点失败:网络查询失败! url=" + str;
                break;
            case VXERR_CANCELED /*-7*/:
                str2 = "打点失败:用户取消! url=" + str;
                break;
            case VXERR_SYMKEY_EXPIRED /*-6*/:
                str2 = "打点失败:对称密钥过期，需要重新用V6协商! url=" + str;
                break;
            case VXERR_NO_SYMKEY /*-5*/:
                str2 = "打点失败:没有对称密钥，需要先执行V6协商! url=" + str;
                break;
            case VXERR_OUTOFMEMORY /*-4*/:
                str2 = "打点失败:内存溢出! url=" + str;
                break;
            case -3:
                str2 = "打点失败:解析返回报文出错! url=" + str;
                break;
            case -2:
                str2 = "打点失败:加密上行包出错! url=" + str;
                break;
            case -1:
                str2 = "打点失败:无效参数! url=" + str;
                break;
            case 0:
                str2 = "打点成功! url=" + str;
                break;
            default:
                str2 = "打点失败:未知异常! code=" + i + ";url=" + str;
                break;
        }
        Log.d("EC", str2);
    }

    public static void initBroadCast(Context context, BroadcastReceiver broadcastReceiver) {
        JarLoader.getInstance(context).initBroadCast(context, broadcastReceiver);
    }

    public static int sendCount(Context context, String str) {
        return sendCount(context, str, null, null, 0);
    }

    public static int sendCount(Context context, String str, int i) {
        return sendCount(context, str, null, null, i);
    }

    public static int sendCount(Context context, String str, RequestEvent requestEvent) {
        return sendCount(context, str, null, requestEvent, 0);
    }

    private static int sendCount(Context context, String str, HashMap<String, String> hashMap) {
        return sendCount(context, str, hashMap, null, 0);
    }

    private static int sendCount(Context context, String str, HashMap<String, String> hashMap, int i) {
        return sendCount(context, str, hashMap, null, i);
    }

    private static int sendCount(Context context, String str, Map<String, String> map, RequestEvent requestEvent, int i) {
        try {
            JarLoader instance = JarLoader.getInstance(context);
            if (requestEvent != null) {
                Class<?>[] clsArr = dotFunction[2];
                clsArr[clsArr.length - 1] = instance.getIFaceCls("getIFace");
                Integer num = (Integer) instance.execFunction("clickFunction", clsArr, context, APP_NAME, str, map, requestEvent);
                dumpErrorMsg(context, str, num.intValue());
                return num.intValue();
            } else if (i > 0) {
                Integer num2 = (Integer) instance.execFunction("clickFunction", dotFunction[1], context, APP_NAME, str, map, Integer.valueOf(i));
                dumpErrorMsg(context, str, num2.intValue());
                return num2.intValue();
            } else {
                Integer num3 = (Integer) instance.execFunction("clickFunction", dotFunction[0], context, APP_NAME, str, map);
                dumpErrorMsg(context, str, num3.intValue());
                return num3.intValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return VXERR_UNKNOW_ERR;
        } catch (Error e2) {
            e2.printStackTrace();
            return VXERR_UNKNOW_ERR;
        }
    }

    public static void setAppName(String str) {
        if (!TextUtils.isEmpty(str)) {
            APP_NAME = str;
        }
    }

    public static void uninitBroadCast(Context context, BroadcastReceiver broadcastReceiver) {
        JarLoader.getInstance(context).uninitBroadCast(context, broadcastReceiver);
    }
}
