package com.qihoo.dynamic;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import com.qihoo.dynamic.util.IO;
import com.qihoo.dynamic.util.Md5Util;
import com.qihoo.iface.ECResult;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class EmulatorCheck {
    private static ECResult g_isEmulator = null;

    public class EmulatorInfo {
        public String name;
        public int ruleId;
    }

    public static String booleantoString(boolean z) {
        return z ? "true" : "false";
    }

    private static int checkEmulatorDev() {
        return 0;
    }

    private static int checkEmulatorFile() {
        if (new File(decodeString("L3N5c3RlbS9saWIvbGliY19tYWxsb2NfZGVidWdfcWVtdS5zbw==")).exists()) {
            return 12;
        }
        return new File(decodeString("L3N5c3RlbS9saWIvbGliY19tYWxsb2NfZGVidWdfcWVtdS5zby1hcm0=")).exists() ? 13 : 0;
    }

    private static String decodeString(String str) {
        if (str == null || str.equals("")) {
            return null;
        }
        try {
            return new String(Base64.decode(str.getBytes(Md5Util.DEFAULT_CHARSET), 0));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String encodeString(String str) {
        if (str == null || str.equals("")) {
            return null;
        }
        try {
            return Base64.encodeToString(str.getBytes(Md5Util.DEFAULT_CHARSET), 0);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static boolean findClass(String str) {
        try {
            return Class.forName(str) != null;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    public static synchronized int isEmulator(Context context) {
        int isEmulator;
        synchronized (EmulatorCheck.class) {
            isEmulator = isEmulator(context, null);
        }
        return isEmulator;
    }

    public static synchronized int isEmulator(Context context, ECResult eCResult) {
        int isEmulatorByLocal;
        synchronized (EmulatorCheck.class) {
            try {
                if (g_isEmulator != null) {
                    if (eCResult != null) {
                        eCResult.Copy(g_isEmulator);
                    }
                    isEmulatorByLocal = g_isEmulator.isEmulator;
                } else {
                    ECResult eCResult2 = new ECResult((String) JarLoader.getInstance(context).execFunction("ec", context));
                    if (eCResult != null) {
                        eCResult.Copy(eCResult2);
                    }
                    g_isEmulator = eCResult2;
                    isEmulatorByLocal = eCResult2.isEmulator;
                }
            } catch (Throwable th) {
                th.printStackTrace();
                isEmulatorByLocal = isEmulatorByLocal(context, eCResult);
            }
        }
        return isEmulatorByLocal;
    }

    @SuppressLint({"DefaultLocale"})
    private static int isEmulatorByLocal(Context context, ECResult eCResult) {
        String str = null;
        if (g_isEmulator != null) {
            if (eCResult != null) {
                eCResult.Copy(g_isEmulator);
            }
            return g_isEmulator.isEmulator;
        }
        g_isEmulator = new ECResult();
        if (new File(decodeString("L2Rldi92Ym94Z3Vlc3Q=")).exists() || new File(decodeString("L2Rldi92Ym94dXNlcg==")).exists()) {
            if (g_isEmulator.emulatorIdx == 0) {
                g_isEmulator.emulatorIdx = 1;
            }
            ECResult eCResult2 = g_isEmulator;
            eCResult2.matchList = String.valueOf(eCResult2.matchList) + ",1";
        }
        if (new File(decodeString("L3N5c3RlbS9saWIvdmJveGd1ZXN0Lmtv")).exists() || new File(decodeString("L3N5c3RlbS9saWIvdmJveHNmLmtv")).exists() || new File(decodeString("L3N5c3RlbS9saWIvdmJveHZpZGVvLmtv")).exists()) {
            if (g_isEmulator.emulatorIdx == 0) {
                g_isEmulator.emulatorIdx = 2;
            }
            ECResult eCResult3 = g_isEmulator;
            eCResult3.matchList = String.valueOf(eCResult3.matchList) + ",2";
        }
        if (findClass(decodeString("Y29tLmJsdWVzdGFja3Mub3MuSUJzdEZvbGRlclNlcnZpY2U=")) || findClass(decodeString("Y29tLmJsdWVzdGFja3Mub3MuSUJzdE9yaWVudGF0aW9uU2VydmljZQ==")) || findClass(decodeString("Y29tLmJsdWVzdGFja3Mudmlldy5Cc3RXaW5kb3dPcmllbnRhdGlvbkxpc3RlbmVy"))) {
            if (g_isEmulator.emulatorIdx == 0) {
                g_isEmulator.emulatorIdx = 3;
            }
            ECResult eCResult4 = g_isEmulator;
            eCResult4.matchList = String.valueOf(eCResult4.matchList) + ",3";
        }
        if (findClass(decodeString("Y29tLmdlbnltb3Rpb24uZ2VueWQuR2VueWRTZXJ2aWNl")) || findClass(decodeString("Y29tLmdlbnltb3Rpb24uZ2VueWQuSUdlbnlkU2VydmljZQ=="))) {
            if (g_isEmulator.emulatorIdx == 0) {
                g_isEmulator.emulatorIdx = 4;
            }
            ECResult eCResult5 = g_isEmulator;
            eCResult5.matchList = String.valueOf(eCResult5.matchList) + ",4";
        }
        try {
            str = IO.readString(decodeString("L3N5c3RlbS9idWlsZC5wcm9w"));
        } catch (IOException e) {
        }
        if (str != null) {
            String lowerCase = str.toLowerCase();
            if (lowerCase.indexOf(decodeString("Ymx1ZXN0YWNrcw==")) >= 0) {
                if (g_isEmulator.emulatorIdx == 0) {
                    g_isEmulator.emulatorIdx = 5;
                }
                ECResult eCResult6 = g_isEmulator;
                eCResult6.matchList = String.valueOf(eCResult6.matchList) + ",5";
            }
            if (lowerCase.indexOf(decodeString("dmJveDg2")) >= 0) {
                if (g_isEmulator.emulatorIdx == 0) {
                    g_isEmulator.emulatorIdx = 6;
                }
                ECResult eCResult7 = g_isEmulator;
                eCResult7.matchList = String.valueOf(eCResult7.matchList) + ",6";
            }
            if (lowerCase.indexOf(decodeString("Z2VueW1vdGlvbg==")) >= 0) {
                if (g_isEmulator.emulatorIdx == 0) {
                    g_isEmulator.emulatorIdx = 7;
                }
                ECResult eCResult8 = g_isEmulator;
                eCResult8.matchList = String.valueOf(eCResult8.matchList) + ",7";
            }
        }
        try {
            String readString = IO.readString(decodeString("L3N5c3RlbS9jcHVpbmZv"));
            if (readString != null && readString.indexOf(decodeString("R29sZGZpc2g=")) >= 0) {
                if (g_isEmulator.emulatorIdx == 0) {
                    g_isEmulator.emulatorIdx = 8;
                }
                ECResult eCResult9 = g_isEmulator;
                eCResult9.matchList = String.valueOf(eCResult9.matchList) + ",8";
            }
        } catch (IOException e2) {
        }
        try {
            String readString2 = IO.readString(decodeString("L3Byb2MvY3B1aW5mbw=="));
            if (readString2 != null) {
                if (readString2.indexOf(decodeString("Q1BVIGltcGxlbWVudGVy")) < 0) {
                    if (g_isEmulator.emulatorIdx == 0) {
                        g_isEmulator.emulatorIdx = 9;
                    }
                    ECResult eCResult10 = g_isEmulator;
                    eCResult10.matchList = String.valueOf(eCResult10.matchList) + ",9";
                }
                if (readString2.indexOf(decodeString("R29sZGZpc2g=")) >= 0) {
                    if (g_isEmulator.emulatorIdx == 0) {
                        g_isEmulator.emulatorIdx = 10;
                    }
                    ECResult eCResult11 = g_isEmulator;
                    eCResult11.matchList = String.valueOf(eCResult11.matchList) + ",10";
                }
            }
        } catch (IOException e3) {
        }
        if (new File(decodeString("L2RhdGEvYmx1ZXN0YWNrcy5wcm9w")).exists()) {
            if (g_isEmulator.emulatorIdx == 0) {
                g_isEmulator.emulatorIdx = 11;
            }
            ECResult eCResult12 = g_isEmulator;
            eCResult12.matchList = String.valueOf(eCResult12.matchList) + ",11";
        }
        int checkEmulatorFile = checkEmulatorFile();
        if (checkEmulatorFile > 0) {
            if (g_isEmulator.emulatorIdx == 0) {
                g_isEmulator.emulatorIdx = checkEmulatorFile;
            }
            ECResult eCResult13 = g_isEmulator;
            eCResult13.matchList = String.valueOf(eCResult13.matchList) + "," + checkEmulatorFile;
        }
        int checkEmulatorDev = checkEmulatorDev();
        if (checkEmulatorDev > 0) {
            if (g_isEmulator.emulatorIdx == 0) {
                g_isEmulator.emulatorIdx = checkEmulatorDev;
            }
            ECResult eCResult14 = g_isEmulator;
            eCResult14.matchList = String.valueOf(eCResult14.matchList) + "," + checkEmulatorDev;
        }
        if (!TextUtils.isEmpty(g_isEmulator.matchList)) {
            g_isEmulator.matchList = g_isEmulator.matchList.replaceFirst(",", "");
        }
        if (g_isEmulator.emulatorIdx == 0) {
            g_isEmulator.isEmulator = 0;
        } else {
            g_isEmulator.isEmulator = 1;
        }
        return g_isEmulator.isEmulator;
    }

    public static void reset(Context context) {
        g_isEmulator = null;
    }
}
