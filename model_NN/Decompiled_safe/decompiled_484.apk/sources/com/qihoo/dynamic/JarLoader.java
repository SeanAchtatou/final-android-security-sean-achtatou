package com.qihoo.dynamic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Build;
import com.qihoo.dynamic.patch.DynamicPatchManager;
import com.qihoo.dynamic.patch.PatchInfo;
import com.qihoo.dynamic.util.GlobalFlag;
import java.io.File;

public class JarLoader {
    private static final String CALLBACK_ACTION = "VX_COUNT_EVENT";
    private static final String DEFAULT_JAR = "patchs/global.jar";
    private static final String JARTAG = "__CHECK_EMULATOR_BY_PATCH";
    private static final String JAR_DIR = "patchs";
    private static final String JAR_NAME = "global";
    private static final String JAR_PACKAGE = "com.qihoo.patch.GlobalPatch";
    private static JarLoader mInstance = null;
    private PatchInfo mPi;

    private JarLoader(Context context) {
        this.mPi = initJar(context);
    }

    public static JarLoader getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new JarLoader(context);
        }
        return mInstance;
    }

    public static String getJarName() {
        return DEFAULT_JAR;
    }

    public static String getJarPath(Context context) {
        String str = String.valueOf(context.getFilesDir().getAbsolutePath()) + File.separator + JAR_DIR;
        new File(str).mkdirs();
        return String.valueOf(str) + "/" + JAR_NAME + ".jar";
    }

    private PatchInfo initJar(Context context) {
        try {
            if (Build.VERSION.SDK_INT >= 14) {
                return DynamicPatchManager.getInstance().getOrInstallPatch(context, JAR_NAME, getJarPath(context), JAR_PACKAGE, JARTAG, DEFAULT_JAR);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isJarExist(Context context) {
        String str = String.valueOf(context.getFilesDir().getAbsolutePath()) + File.separator + JAR_DIR;
        new File(str).mkdirs();
        return new File(String.valueOf(str) + "/" + JAR_NAME + ".jar").exists();
    }

    public static void reset(Context context) {
        mInstance = null;
        GlobalFlag.uncheck(context, JARTAG);
        DynamicPatchManager.getInstance().reset();
    }

    public <T> T execFunction(String str, Context context) {
        Class mainClass;
        if (this.mPi == null || (mainClass = this.mPi.getMainClass()) == null) {
            return null;
        }
        try {
            return mainClass.getMethod(str, Context.class).invoke(mainClass, context);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public <T> T execFunction(String str, Class<?>[] clsArr, Object... objArr) {
        Class mainClass;
        if (this.mPi == null || clsArr == null || objArr == null || clsArr.length != objArr.length || (mainClass = this.mPi.getMainClass()) == null) {
            return null;
        }
        try {
            return mainClass.getMethod(str, clsArr).invoke(mainClass, objArr);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public Class<?> getIFaceCls(String str) {
        if (this.mPi == null) {
            return null;
        }
        Class mainClass = this.mPi.getMainClass();
        if (mainClass != null) {
            try {
                return (Class) mainClass.getMethod(str, new Class[0]).invoke(mainClass, new Object[0]);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return null;
    }

    public void initBroadCast(Context context, BroadcastReceiver broadcastReceiver) {
        if (context != null && broadcastReceiver != null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(CALLBACK_ACTION);
            context.registerReceiver(broadcastReceiver, intentFilter);
        }
    }

    public void uninitBroadCast(Context context, BroadcastReceiver broadcastReceiver) {
        if (context != null && broadcastReceiver != null) {
            context.unregisterReceiver(broadcastReceiver);
        }
    }
}
