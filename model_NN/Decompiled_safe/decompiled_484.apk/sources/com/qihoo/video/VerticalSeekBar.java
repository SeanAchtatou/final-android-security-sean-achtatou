package com.qihoo.video;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.AbsSeekBar;
import com.qihoo.mediaplayer.R;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class VerticalSeekBar extends AbsSeekBar {
    private OnSeekBarChangeListener mOnSeekBarChangeListener;
    private boolean mTouchable;

    public interface OnSeekBarChangeListener {
        void onProgressChanged(VerticalSeekBar verticalSeekBar, int i, boolean z);

        void onStartTrackingTouch(VerticalSeekBar verticalSeekBar);

        void onStopTrackingTouch(VerticalSeekBar verticalSeekBar);
    }

    public VerticalSeekBar(Context context) {
        this(context, null);
    }

    public VerticalSeekBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842875);
    }

    public VerticalSeekBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mTouchable = true;
        setThumbOffset(context.getResources().getDimensionPixelSize(R.dimen.volume_thumb_offset));
    }

    private void setThumbPos(int i, Drawable drawable, float f, int i2) {
        int i3;
        if (drawable != null) {
            try {
                int paddingTop = getPaddingTop();
                int height = (getHeight() - paddingTop) - getPaddingBottom();
                int intrinsicWidth = drawable.getIntrinsicWidth();
                int intrinsicHeight = drawable.getIntrinsicHeight();
                int thumbOffset = (int) (((float) ((height - intrinsicWidth) + (getThumbOffset() * 2))) * f);
                if (i2 == Integer.MIN_VALUE) {
                    Rect bounds = drawable.getBounds();
                    i2 = bounds.top;
                    i3 = bounds.bottom;
                } else {
                    i3 = i2 + intrinsicHeight;
                }
                drawable.setBounds(thumbOffset, i2, intrinsicWidth + thumbOffset, i3);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        canvas.rotate(-90.0f);
        canvas.translate((float) (-getHeight()), 0.0f);
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public synchronized void onMeasure(int i, int i2) {
        super.onMeasure(i2, i);
        setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
    }

    /* access modifiers changed from: package-private */
    public void onProgressRefresh(float f, boolean z) {
        Drawable drawable;
        try {
            Field declaredField = getClass().getSuperclass().getDeclaredField("mThumb");
            declaredField.setAccessible(true);
            drawable = (Drawable) declaredField.get(this);
        } catch (Exception e) {
            e.printStackTrace();
            drawable = null;
        }
        setThumbPos(getWidth(), drawable, f, Integer.MIN_VALUE);
        invalidate();
        if (this.mOnSeekBarChangeListener != null) {
            this.mOnSeekBarChangeListener.onProgressChanged(this, getProgress(), z);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i2, i, i3, i4);
    }

    /* access modifiers changed from: package-private */
    public void onStartTrackingTouch() {
        if (this.mOnSeekBarChangeListener != null) {
            this.mOnSeekBarChangeListener.onStartTrackingTouch(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void onStopTrackingTouch() {
        if (this.mOnSeekBarChangeListener != null) {
            this.mOnSeekBarChangeListener.onStopTrackingTouch(this);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z;
        if (!this.mTouchable) {
            return false;
        }
        try {
            Field declaredField = getClass().getSuperclass().getDeclaredField("mIsUserSeekable");
            declaredField.setAccessible(true);
            z = declaredField.getBoolean(this);
        } catch (Exception e) {
            e.printStackTrace();
            z = true;
        }
        if (!z || !isEnabled()) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                setPressed(true);
                onStartTrackingTouch();
                trackTouchEvent(motionEvent);
                break;
            case 1:
                trackTouchEvent(motionEvent);
                onStopTrackingTouch();
                setPressed(false);
                invalidate();
                break;
            case 2:
                trackTouchEvent(motionEvent);
                try {
                    Method declaredMethod = getClass().getSuperclass().getDeclaredMethod("attemptClaimDrag", new Class[0]);
                    declaredMethod.setAccessible(true);
                    declaredMethod.invoke(this, new Object[0]);
                    break;
                } catch (Exception e2) {
                    e2.printStackTrace();
                    break;
                }
            case 3:
                onStopTrackingTouch();
                setPressed(false);
                invalidate();
                break;
        }
        return true;
    }

    public void setOnSeekBarChangeListener(OnSeekBarChangeListener onSeekBarChangeListener) {
        this.mOnSeekBarChangeListener = onSeekBarChangeListener;
    }

    public void setTouchable(boolean z) {
        this.mTouchable = z;
    }

    /* access modifiers changed from: protected */
    public void trackTouchEvent(MotionEvent motionEvent) {
        float paddingLeft;
        float f = 0.0f;
        int height = getHeight();
        int paddingLeft2 = (height - getPaddingLeft()) - getPaddingRight();
        int y = (int) (((float) height) - motionEvent.getY());
        if (y < getPaddingLeft()) {
            paddingLeft = 0.0f;
        } else if (y > height - getPaddingRight()) {
            paddingLeft = 1.0f;
        } else {
            paddingLeft = ((float) (y - getPaddingLeft())) / ((float) paddingLeft2);
            try {
                Field declaredField = getClass().getSuperclass().getDeclaredField("mTouchProgressOffset");
                declaredField.setAccessible(true);
                f = declaredField.getFloat(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        float max = f + (paddingLeft * ((float) getMax()));
        try {
            Method declaredMethod = getClass().getSuperclass().getSuperclass().getDeclaredMethod("setProgress", Integer.TYPE, Boolean.TYPE);
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(this, Integer.valueOf((int) max), true);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
