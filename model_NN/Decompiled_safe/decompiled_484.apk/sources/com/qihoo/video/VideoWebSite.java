package com.qihoo.video;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class VideoWebSite implements Serializable {
    private static final long serialVersionUID = 1;
    private int mSelectedIndex = 0;
    private String moreSourceStr;
    private String moreSourceUrl;
    private ArrayList<WebsiteInfo> websites = null;

    public VideoWebSite(WebsiteInfo websiteInfo) {
        if (websiteInfo != null) {
            this.websites = new ArrayList<>();
            this.websites.add(websiteInfo);
            setSelectedIndex(0);
        }
    }

    public VideoWebSite(ArrayList<WebsiteInfo> arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            this.websites = arrayList;
        }
    }

    public int getCount() {
        if (this.websites == null) {
            return 0;
        }
        return this.websites.size();
    }

    public String getDefaultLink(int i) {
        if (this.websites == null || i >= this.websites.size()) {
            return null;
        }
        return this.websites.get(i).getDefaultPlaylink();
    }

    public String getDefaultXstm(int i) {
        if (this.websites == null || i >= this.websites.size()) {
            return null;
        }
        return this.websites.get(i).getXstm();
    }

    public ArrayList<WebsiteInfo> getGroupWebsite(int i) {
        if (this.websites == null) {
            return null;
        }
        ArrayList<WebsiteInfo> arrayList = new ArrayList<>();
        Iterator<WebsiteInfo> it = this.websites.iterator();
        while (it.hasNext()) {
            WebsiteInfo next = it.next();
            if (next.getGroupId() == i) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public String getMoreSourceStr() {
        return this.moreSourceStr;
    }

    public String getMoreSourceUrl() {
        return this.moreSourceUrl;
    }

    public int getSelectedIndex() {
        return this.mSelectedIndex;
    }

    public WebsiteInfo getSelectedWebsiteInfo() {
        if (this.websites == null || this.websites.size() <= this.mSelectedIndex) {
            return null;
        }
        return this.websites.get(this.mSelectedIndex);
    }

    public String getVideoSource(int i) {
        if (this.websites == null || i >= this.websites.size()) {
            return null;
        }
        return this.websites.get(i).getWebsiteName();
    }

    public String getWebSiteFromIndex(int i) {
        if (this.websites == null || i >= this.websites.size()) {
            return null;
        }
        return this.websites.get(i).getWebsiteKey();
    }

    public WebsiteInfo getWebSiteInfoByIndex(int i) {
        if (this.websites == null || i >= this.websites.size() || i <= -1) {
            return null;
        }
        return this.websites.get(i);
    }

    public ArrayList<WebsiteInfo> getWebsites() {
        if (this.websites == null || this.websites.size() <= 0) {
            return null;
        }
        return this.websites;
    }

    public void setMoreSourceStr(String str) {
        this.moreSourceStr = str;
    }

    public void setMoreSourceUrl(String str) {
        this.moreSourceUrl = str;
    }

    public void setSelectedIndex(int i) {
        if (this.websites != null && i < this.websites.size()) {
            this.mSelectedIndex = i;
            Iterator<WebsiteInfo> it = this.websites.iterator();
            while (it.hasNext()) {
                WebsiteInfo next = it.next();
                if (next.getStatus() == WebsiteStatus.STATUS_SELECTED) {
                    next.setStatus(WebsiteStatus.STATUS_NOMAL);
                }
            }
            this.websites.get(i).setStatus(WebsiteStatus.STATUS_SELECTED);
        }
    }

    public void setSelectedIndex(String str, String str2) {
        int i;
        if (this.websites != null) {
            int i2 = 0;
            Iterator<WebsiteInfo> it = this.websites.iterator();
            while (true) {
                i = i2;
                if (it.hasNext()) {
                    WebsiteInfo next = it.next();
                    if (next.getWebsiteKey().equalsIgnoreCase(str) && next.getQualityKey().equalsIgnoreCase(str2)) {
                        break;
                    }
                    i2 = i + 1;
                } else {
                    break;
                }
            }
            setSelectedIndex(i);
        }
    }

    public void setSelectedWebsite(WebsiteInfo websiteInfo) {
        if (this.websites != null && websiteInfo != null) {
            int i = 0;
            Iterator<WebsiteInfo> it = this.websites.iterator();
            while (true) {
                int i2 = i;
                if (it.hasNext()) {
                    WebsiteInfo next = it.next();
                    if (next.getStatus() == WebsiteStatus.STATUS_SELECTED) {
                        next.setStatus(WebsiteStatus.STATUS_NOMAL);
                    } else if (next.getQualityKey().equalsIgnoreCase(websiteInfo.getQualityKey()) && next.getWebsiteKey().equalsIgnoreCase(websiteInfo.getWebsiteKey())) {
                        next.setStatus(WebsiteStatus.STATUS_SELECTED);
                        this.mSelectedIndex = i2;
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public void setSelectedWebsite(String str, String str2) {
        if (this.websites != null) {
            int i = 0;
            Iterator<WebsiteInfo> it = this.websites.iterator();
            while (true) {
                int i2 = i;
                if (it.hasNext()) {
                    WebsiteInfo next = it.next();
                    if (!next.getQualityKey().equalsIgnoreCase(str2) || !next.getWebsiteKey().equalsIgnoreCase(str)) {
                        i = i2 + 1;
                    } else {
                        setSelectedIndex(i2);
                        return;
                    }
                } else {
                    return;
                }
            }
        }
    }
}
