package com.qihoo.video;

import org.json.JSONArray;
import org.json.JSONObject;

public class VideoInfos {
    public int errCode = 0;
    public VideoInfo[] videoInfos = null;

    public VideoInfos(JSONArray jSONArray, int i) {
        this.errCode = i;
        if (jSONArray != null) {
            int length = jSONArray.length();
            this.videoInfos = new VideoInfo[length];
            for (int i2 = 0; i2 < length; i2++) {
                JSONObject optJSONObject = jSONArray.optJSONObject(i2);
                if (optJSONObject != null) {
                    this.videoInfos[i2] = new VideoInfo(optJSONObject);
                }
            }
        }
    }
}
