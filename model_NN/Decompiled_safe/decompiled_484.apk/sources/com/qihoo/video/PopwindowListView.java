package com.qihoo.video;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.AdapterView;
import android.widget.ListView;
import com.qihoo.mediaplayer.R;

public class PopwindowListView extends ListView {
    private float THREASHOLD;
    private AdapterView.OnItemClickListener mClickListener;
    private float x0;
    private float y0;

    public PopwindowListView(Context context) {
        super(context);
        init(context);
    }

    public PopwindowListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    public PopwindowListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init(context);
    }

    private void init(Context context) {
        this.THREASHOLD = context.getResources().getDimension(R.dimen.common_5dp);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (this.mClickListener == null) {
            return super.dispatchTouchEvent(motionEvent);
        }
        switch (motionEvent.getAction()) {
            case 0:
                this.x0 = motionEvent.getX();
                this.y0 = motionEvent.getY();
                break;
            case 1:
                if (Math.abs(motionEvent.getX() - this.x0) + Math.abs(motionEvent.getY() - this.y0) < this.THREASHOLD) {
                    int pointToPosition = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY());
                    if (pointToPosition < 0) {
                        this.mClickListener.onItemClick(this, null, -1, -1);
                        break;
                    } else {
                        this.mClickListener.onItemClick(this, getChildAt(pointToPosition - getFirstVisiblePosition()), pointToPosition, getAdapter().getItemId(pointToPosition));
                        break;
                    }
                }
                break;
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        super.setOnItemClickListener(onItemClickListener);
        this.mClickListener = onItemClickListener;
    }
}
