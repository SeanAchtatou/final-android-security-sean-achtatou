package com.qihoo.video;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.qihoo.mediaplayer.R;
import com.qihoo.qplayer.JsonVideoSource;
import com.qihoo.qplayer.LocalVideoSource;
import com.qihoo.qplayer.QihooMediaPlayer;
import com.qihoo.qplayer.util.NetWorkState;
import com.qihoo.qplayer.util.XstmUrlBuilder;
import com.qihoo.qplayer.view.QihooVideoView;
import com.qihoo.video.PlayerViewUtils;
import com.qihoo.video.VideoRequestUtils;
import com.qihoo.video.httpservice.AsyncRequest;
import com.qihoo360.daily.activity.SearchActivity;
import java.io.File;
import java.io.FilenameFilter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class QihooPlayerActivity extends Activity implements IPlayerViewListener, VideoRequestUtils.OnVideoRequestListener {
    public static final String ACTION_PLAYER_EXIT = "com.qihoo.video.playexit";
    public static final int CANCEL = 3;
    public static final int CREAT_MASK = 2;
    public static final String HOME_BUTTON_CLICK = "com.qihoo.video.save_history";
    public static final String PLAY_INFO = "playInfo";
    public static final String PLAY_STATE = "play_state";
    public static final String PLAY_URL = "play_url";
    public static final int PREPARE_TIMEOUT = 4;
    public static final String SEND_MSG = "com.qihoo.video.exitplayer";
    public static final int STATE_FAIL = 2;
    public static final int STATE_SUCCESS = 1;
    private final String TAG = getClass().getName();
    private IntentFilter filter;
    /* access modifiers changed from: private */
    public boolean is3GChangePlayPos = false;
    /* access modifiers changed from: private */
    public boolean isPlayComplete = false;
    private String jsonPlayData;
    /* access modifiers changed from: private */
    public AlertDialog m3GDialog;
    /* access modifiers changed from: private */
    public int mCurrentPosition;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 2:
                    new MaskHelper().createOnce(QihooPlayerActivity.this, R.layout.guide_player, MaskHelper.KEY_PLAYER_GUIDE);
                    return;
                case 3:
                    QihooPlayerActivity.this.cancel();
                    return;
                case 4:
                    QihooPlayerActivity.this.reportPlayError(3);
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public PlayerViewUtils.NetConnectStatus mLastNetStatus;
    /* access modifiers changed from: private */
    public WebsiteInfo mLastWebSiteInfo;
    private int mPlayState = 1;
    /* access modifiers changed from: private */
    public PlayerView mPlayerControlView = null;
    private ViewGroup mPlayerLayout = null;
    /* access modifiers changed from: private */
    public QihooVideoView mVideoView;
    boolean mWillExit = false;
    private VideoRequestUtils.VideoRequestParams param;
    /* access modifiers changed from: private */
    public PlayerInfo playInfo = null;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            PlayerViewUtils.NetConnectStatus netConnectStatusFromIntent;
            String action = intent.getAction();
            if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
                if (QihooPlayerActivity.this.isNotLocalFile() && (netConnectStatusFromIntent = PlayerViewUtils.getNetConnectStatusFromIntent(intent)) != QihooPlayerActivity.this.mLastNetStatus) {
                    QihooPlayerActivity.this.mLastNetStatus = netConnectStatusFromIntent;
                    if (netConnectStatusFromIntent == PlayerViewUtils.NetConnectStatus.CONNECTION_3G) {
                        QihooPlayerActivity.this.playVideoShow3GDialog(new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (QihooPlayerActivity.this.mVideoView != null) {
                                    Log.i("jy", "3G Receiver and recover");
                                    QihooPlayerActivity.this.mVideoView.recoverStop();
                                    QihooPlayerActivity.this.mVideoView.set3G(false);
                                }
                            }
                        });
                    } else if (netConnectStatusFromIntent == PlayerViewUtils.NetConnectStatus.DISCONNECTION && !NetWorkState.isWifi(QihooPlayerActivity.this)) {
                        Toast.makeText(QihooPlayerActivity.this, R.string.player_network_error, 0).show();
                    } else if (netConnectStatusFromIntent == PlayerViewUtils.NetConnectStatus.CONNECTION_WIFI && QihooPlayerActivity.this.mVideoView.getVisibility() == 0) {
                        QihooPlayerActivity.this.mVideoView.recover();
                    }
                }
            } else if (QihooPlayerActivity.SEND_MSG.equals(action)) {
                QihooPlayerActivity.this.playInfo.setGoHomePage(false);
                QihooPlayerActivity.this.finishPlayer();
            } else if (!"android.intent.action.USER_PRESENT".equals(action)) {
                action.equals(QihooPlayerActivity.HOME_BUTTON_CLICK);
            } else if (QihooPlayerActivity.this.isTopActivity() && QihooPlayerActivity.this.mVideoView != null) {
                QihooPlayerActivity.this.mVideoView.setVisibility(0);
            }
        }
    };
    /* access modifiers changed from: private */
    public int slownessCount;
    private long startPrepareTime;

    /* access modifiers changed from: private */
    public void cancel() {
        this.mPlayState = 2;
        finishPlayer();
    }

    /* access modifiers changed from: private */
    public void cancelDelay(long j) {
        this.mHandler.sendEmptyMessageDelayed(3, j);
    }

    /* access modifiers changed from: private */
    public void changeWebsiteInfoFail() {
        String string;
        if (this.playInfo == null || this.playInfo.getVideoWebSite() == null) {
            Toast.makeText(this, R.string.local_source_not_play, 1).show();
            cancelDelay(2000);
            return;
        }
        WebsiteInfo selectedWebsiteInfo = this.playInfo.getVideoWebSite().getSelectedWebsiteInfo();
        selectedWebsiteInfo.setStatus(WebsiteStatus.STATUS_FAILED);
        this.mPlayerControlView.changePlayError();
        if (NetWorkState.isNetworkAvailable(this)) {
            string = getString(R.string.video_select_erro, new Object[]{selectedWebsiteInfo.getWebsiteNameAndQuality()});
        } else {
            string = getString(R.string.network_unKnow);
        }
        Toast.makeText(this, string, 1).show();
        if (this.mLastWebSiteInfo != null && this.mLastWebSiteInfo.getStatus() != WebsiteStatus.STATUS_FAILED) {
            this.playInfo.getVideoWebSite().setSelectedWebsite(this.mLastWebSiteInfo);
            doVideoStreamParsing();
        }
    }

    private void createQihooVideoView() {
        if (this.mVideoView != null) {
            destroyQihooVideoView();
        }
        this.mVideoView = new QihooVideoView(this);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        layoutParams.gravity = 17;
        this.mPlayerLayout.addView(this.mVideoView, 0, layoutParams);
        setVideoView();
    }

    private void destroyQihooVideoView() {
        this.mPlayerLayout.removeView(this.mVideoView);
        this.mVideoView.release();
        this.mVideoView = null;
    }

    /* access modifiers changed from: private */
    public void doVideoStreamParsing() {
        this.param.parseToParams(this.playInfo);
        VideoRequestUtils.INSTANCE.RequestData(this.param, this, null);
    }

    /* access modifiers changed from: private */
    public int getPlayIndexWithNoLost(int i) {
        WebsiteInfo websiteInfo = null;
        VideoWebSite videoWebSite = this.playInfo.getVideoWebSite();
        if (videoWebSite != null) {
            websiteInfo = videoWebSite.getSelectedWebsiteInfo();
        }
        if (websiteInfo == null || websiteInfo.getLost() == null) {
            return i;
        }
        for (int i2 : websiteInfo.getLost()) {
            if (i2 == i + 1) {
                return getPlayIndexWithNoLost(i + 1);
            }
        }
        return i;
    }

    private void init() {
        this.mPlayerLayout = (ViewGroup) findViewById(R.id.player_layout);
        this.mPlayerControlView = (PlayerView) findViewById(R.id.player_controller_view);
        this.mPlayerControlView.setPlayViewListener(this);
        this.param = new VideoRequestUtils.VideoRequestParams();
        createQihooVideoView();
        this.filter = new IntentFilter();
        this.filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        this.filter.addAction("android.intent.action.SCREEN_ON");
        this.filter.addAction("android.intent.action.SCREEN_OFF");
        this.filter.addAction("android.intent.action.USER_PRESENT");
        this.filter.addAction(SEND_MSG);
        this.filter.addAction(HOME_BUTTON_CLICK);
        registerReceiver(this.receiver, this.filter);
        this.mHandler.sendEmptyMessageDelayed(2, 200);
    }

    private boolean isControllablePlay() {
        return !TextUtils.isEmpty(this.jsonPlayData) || this.playInfo.getIsLocalFile() || !TextUtils.isEmpty(this.playInfo.getPlayUrl()) || !TextUtils.isEmpty(this.playInfo.getJsonPlayData());
    }

    private boolean isScreenLocked() {
        return ((KeyguardManager) getSystemService("keyguard")).inKeyguardRestrictedInputMode();
    }

    /* access modifiers changed from: private */
    public boolean isTopActivity() {
        try {
            List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) getSystemService("activity")).getRunningTasks(1);
            if (runningTasks != null && runningTasks.size() > 0 && getPackageName().equals(runningTasks.get(0).topActivity.getPackageName())) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void openUriLocalVideo(Uri uri) {
        String queryParameter = uri.getQueryParameter("localfile");
        ArrayList arrayList = new ArrayList();
        arrayList.add(queryParameter);
        this.mVideoView.setDataSource(new LocalVideoSource(arrayList));
    }

    private void openUriUrlVideo(Uri uri) {
        String queryParameter = uri.getQueryParameter("playurl");
        this.playInfo.setPlayUrl(queryParameter);
        ArrayList arrayList = new ArrayList();
        arrayList.add(queryParameter);
        LocalVideoSource localVideoSource = new LocalVideoSource(arrayList);
        try {
            JSONObject jSONObject = new JSONObject(uri.getQueryParameter("head"));
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                localVideoSource.getHeader().put(next, jSONObject.optString(next));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.mVideoView.setDataSource(localVideoSource);
    }

    private void openUriVideo(Uri uri) {
        String queryParameter = uri.getQueryParameter(SearchActivity.TAG_URL);
        String queryParameter2 = uri.getQueryParameter("website");
        long j = 0;
        try {
            j = Long.valueOf(uri.getQueryParameter("startTime")).longValue();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String queryParameter3 = uri.getQueryParameter("reserve");
        if (TextUtils.isEmpty(queryParameter) || TextUtils.isEmpty(queryParameter2)) {
            finish();
            Toast.makeText(this, R.string.page_failure, 1).show();
            return;
        }
        if (queryParameter != null) {
            queryParameter = URLEncodeing.uRLEncode(Html.fromHtml(queryParameter).toString());
        }
        this.playInfo.setCatlog((byte) 0);
        String str = "http://xstm.v.360.cn/movie/" + queryParameter2 + "?url=" + queryParameter;
        if (!TextUtils.isEmpty(queryParameter3)) {
            str = String.valueOf(str) + queryParameter3;
        }
        this.playInfo.setXstmUrl(str);
        this.playInfo.setPlayTimeStamp(j);
        if (NetWorkState.is3G(this)) {
            show3GTips(new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    QihooPlayerActivity.this.doVideoStreamParsing();
                }
            });
        } else {
            doVideoStreamParsing();
        }
    }

    /* access modifiers changed from: private */
    public void playVideoShow3GDialog(DialogInterface.OnClickListener onClickListener) {
        if (this.m3GDialog != null && this.m3GDialog.isShowing()) {
            return;
        }
        if (this.playInfo.getIsShow3GAlart()) {
            this.playInfo.setHasShown3GAlartBefore(false);
            return;
        }
        if (this.mVideoView != null) {
            this.mVideoView.backup();
            Log.i("jy", "3G Receiver and stop");
            this.mVideoView.stop();
            this.mVideoView.set3G(true);
        }
        show3GTips(onClickListener);
    }

    private void playXstm(PlayerInfo playerInfo) {
        int i;
        String str = "";
        if (playerInfo.getCatlog() == 0 || playerInfo.getVideoWebSite() == null) {
            i = 0;
        } else {
            i = playerInfo.getVideoWebSite().getSelectedWebsiteInfo().getPlayerSDK();
            str = playerInfo.getVideoWebSite().getSelectedWebsiteInfo().getQualityKey();
        }
        StreamUrlRequest streamUrlRequest = new StreamUrlRequest(this, null, null);
        streamUrlRequest.setOnRecivedDataListener(new AsyncRequest.OnRecivedDataListener() {
            public void OnRecivedData(AsyncRequest asyncRequest, Object obj) {
                if (obj == null || !(obj instanceof XstmInfo) || ((XstmInfo) obj).errorCode != 0) {
                    QihooPlayerActivity.this.onRequestFailed(VideoRequestUtils.RequestError.TIMEOUT);
                } else {
                    QihooPlayerActivity.this.onRequestSucess((XstmInfo) obj);
                }
            }
        });
        streamUrlRequest.executeOnPoolExecutor(playerInfo.getXstmUrl(), str, "play", Integer.valueOf(i));
    }

    /* access modifiers changed from: private */
    public void preparedSuccess() {
        byte catlog;
        int i = 0;
        int playTimeStamp = (int) this.playInfo.getPlayTimeStamp();
        int duration = this.mVideoView.getDuration();
        if (duration <= 0 || playTimeStamp < duration || !((catlog = this.playInfo.getCatlog()) == 1 || catlog == 3 || (this.playInfo.getVideoWebSite() != null && this.playInfo.getVideoWebSite().getSelectedWebsiteInfo() != null && ((long) (this.playInfo.getPlayCount() + 1)) == this.playInfo.getVideoWebSite().getSelectedWebsiteInfo().getUpdatedInfo()))) {
            i = playTimeStamp;
        }
        if (i > 0) {
            this.mVideoView.seekTo(i);
        }
    }

    /* access modifiers changed from: private */
    public void reportPlayError(int i) {
        int i2 = 0;
        if (this.playInfo != null && !this.playInfo.getIsLocalFile()) {
            String str = "";
            if (!(this.playInfo.getVideoWebSite() == null || this.playInfo.getVideoWebSite().getSelectedWebsiteInfo() == null)) {
                str = this.playInfo.getVideoWebSite().getSelectedWebsiteInfo().getWebsiteKey();
            }
            if (TextUtils.isEmpty(str)) {
                str = this.playInfo.getFromPageStr();
            }
            ReportPlayErrorRequest reportPlayErrorRequest = new ReportPlayErrorRequest(this, null, null);
            Object[] objArr = new Object[7];
            objArr[0] = Integer.valueOf(i);
            objArr[1] = Integer.valueOf(this.playInfo.getCatlog());
            objArr[2] = str;
            objArr[3] = this.playInfo.getVideoId() == null ? "" : this.playInfo.getVideoId();
            objArr[4] = this.playInfo.getRefUrl();
            objArr[5] = Integer.valueOf(i == 4 ? this.mCurrentPosition : 0);
            if (i == 4) {
                i2 = this.slownessCount;
            }
            objArr[6] = Integer.valueOf(i2);
            reportPlayErrorRequest.execute(objArr);
        }
    }

    private void requestData() {
        Bundle extras;
        Serializable serializable;
        Uri data = getIntent().getData();
        if (data != null) {
            this.playInfo = new PlayerInfo();
            this.playInfo.setVideoTitle(data.getQueryParameter("title"));
            if (!TextUtils.isEmpty(data.getQueryParameter(SearchActivity.TAG_URL))) {
                openUriVideo(data);
            } else if (!TextUtils.isEmpty(data.getQueryParameter("localfile"))) {
                openUriLocalVideo(data);
                this.playInfo.setIsLocalFile(true);
                this.mPlayerControlView.setPlayerData(this.playInfo);
            } else if (!TextUtils.isEmpty(data.getQueryParameter("playurl"))) {
                openUriUrlVideo(data);
                this.mPlayerControlView.setPlayerData(this.playInfo);
            } else {
                Toast.makeText(this, R.string.param_error, 0).show();
                finishPlayer();
            }
        } else if (getIntent() == null || (extras = getIntent().getExtras()) == null || (serializable = extras.getSerializable(PLAY_INFO)) == null || !(serializable instanceof PlayerInfo)) {
            finishPlayer();
        } else {
            this.playInfo = (PlayerInfo) serializable;
            ArrayList<String> localMeidaFile = this.playInfo.getLocalMeidaFile();
            if (this.playInfo.getIsLocalFile()) {
                if (localMeidaFile == null || localMeidaFile.size() <= 0) {
                    Toast.makeText(this, R.string.invalid_play_file, 0).show();
                    finishPlayer();
                } else {
                    this.is3GChangePlayPos = true;
                    this.mVideoView.setDataSource(new LocalVideoSource(localMeidaFile));
                }
            } else if (!TextUtils.isEmpty(this.playInfo.getPlayUrl())) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(this.playInfo.getPlayUrl());
                this.mVideoView.setDataSource(new LocalVideoSource(arrayList));
            } else if (!TextUtils.isEmpty(this.playInfo.getJsonPlayData())) {
                try {
                    this.mVideoView.setDataSource(new JsonVideoSource(this.playInfo.getJsonPlayData()));
                } catch (Exception e) {
                    Toast.makeText(this, R.string.local_source_not_play, 0).show();
                    finishPlayer();
                }
            } else if (!TextUtils.isEmpty(this.playInfo.getXstmUrl())) {
                playXstm(this.playInfo);
            } else {
                doVideoStreamParsing();
            }
            this.mPlayerControlView.setPlayerData(this.playInfo);
        }
    }

    private void sendPlayerStateBroadcast() {
        Intent intent = new Intent();
        intent.setAction(ACTION_PLAYER_EXIT);
        intent.putExtra(PLAY_STATE, this.mPlayState);
        sendBroadcast(intent);
    }

    private void setVideoTitle(String str) {
        if (str != null) {
            try {
                String optString = new JSONObject(str).optString("title");
                if (optString != null) {
                    String spanned = Html.fromHtml(optString).toString();
                    if (spanned.length() > 0) {
                        this.playInfo.setVideoTitle(spanned);
                        this.mPlayerControlView.setPlayerData(this.playInfo);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setVideoView() {
        this.mVideoView.setMediaController(this.mPlayerControlView);
        this.mPlayerControlView.showPrepareView();
        this.mVideoView.setOnCompletetionListener(new QihooMediaPlayer.OnCompletionListener() {
            public void onCompletion(QihooMediaPlayer qihooMediaPlayer) {
                byte catlog = QihooPlayerActivity.this.playInfo.getCatlog();
                int playCount = QihooPlayerActivity.this.playInfo.getPlayCount() + 1;
                VideoWebSite videoWebSite = QihooPlayerActivity.this.playInfo.getVideoWebSite();
                if (videoWebSite != null && !QihooPlayerActivity.this.playInfo.getIsLocalFile()) {
                    WebsiteInfo selectedWebsiteInfo = videoWebSite.getSelectedWebsiteInfo();
                    if ((catlog == 2 || catlog == 4) && ((long) playCount) < selectedWebsiteInfo.getUpdatedInfo()) {
                        int access$8 = QihooPlayerActivity.this.getPlayIndexWithNoLost(QihooPlayerActivity.this.playInfo.getPlayCount() + 1);
                        QihooPlayerActivity.this.mPlayerControlView.showPrepareView();
                        QihooPlayerActivity.this.playVideo(access$8, true);
                        return;
                    }
                }
                QihooPlayerActivity.this.isPlayComplete = true;
                QihooPlayerActivity.this.finish();
            }
        });
        this.mVideoView.setOnPreparedListener(new QihooMediaPlayer.OnPreparedListener() {
            public void onPrepared(QihooMediaPlayer qihooMediaPlayer) {
                Log.i("jy", "mpa onPrepared");
                QihooPlayerActivity.this.mHandler.removeMessages(4);
                QihooPlayerActivity.this.preparedSuccess();
                QihooPlayerActivity.this.mLastWebSiteInfo = null;
                if (!(QihooPlayerActivity.this.playInfo == null || QihooPlayerActivity.this.playInfo.getVideoWebSite() == null || QihooPlayerActivity.this.playInfo.getVideoWebSite().getSelectedWebsiteInfo() == null)) {
                    QihooPlayerActivity.this.playInfo.getVideoWebSite().getSelectedWebsiteInfo().setStatus(WebsiteStatus.STATUS_SELECTED);
                }
                if (QihooPlayerActivity.this.playInfo.getIsLocalFile()) {
                    QihooPlayerActivity.this.mPlayerControlView.hidePrepareView();
                }
            }
        });
        this.mVideoView.setOnErrorListener(new QihooMediaPlayer.OnErrorListener() {
            public boolean onError(QihooMediaPlayer qihooMediaPlayer, int i, int i2) {
                Log.i("wlc", "onError|what:" + i);
                if (QihooPlayerActivity.this.playInfo != null) {
                    Log.i("jy", "MoboAct.onError: " + QihooPlayerActivity.this.playInfo.getPlayUrl());
                }
                switch (i) {
                    case 0:
                        if (!NetWorkState.isNetworkAvailable(QihooPlayerActivity.this)) {
                            Toast.makeText(QihooPlayerActivity.this, R.string.network_unKnow, 1).show();
                            QihooPlayerActivity.this.cancelDelay(2000);
                        } else {
                            QihooPlayerActivity.this.changeWebsiteInfoFail();
                        }
                        QihooPlayerActivity.this.reportPlayError(2);
                        break;
                    case 1:
                        QihooPlayerActivity.this.cancel();
                        break;
                    case 3:
                        if ((QihooPlayerActivity.this.m3GDialog != null && QihooPlayerActivity.this.m3GDialog.isShowing()) || NetWorkState.isNetworkAvailable(QihooPlayerActivity.this)) {
                            if (NetWorkState.isNetworkAvailable(QihooPlayerActivity.this)) {
                                QihooPlayerActivity.this.reportPlayError(2);
                                break;
                            }
                        } else {
                            QihooPlayerActivity.this.mPlayerControlView.showNetworkErrorView();
                            QihooPlayerActivity.this.mVideoView.backup();
                            break;
                        }
                        break;
                }
                return true;
            }
        });
        this.mVideoView.setOnPositionChangeListener(new QihooMediaPlayer.OnPositionChangeListener() {
            public void onPlayPositionChanged(QihooMediaPlayer qihooMediaPlayer, int i) {
                QihooPlayerActivity.this.mCurrentPosition = i;
            }
        });
        this.mVideoView.setOnBufferListener(new QihooMediaPlayer.OnBufferingUpdateListener() {
            public void onBufferingUpdate(QihooMediaPlayer qihooMediaPlayer, int i) {
                if (i == 100 && !QihooPlayerActivity.this.playInfo.getIsLocalFile() && QihooPlayerActivity.this.mCurrentPosition > 0) {
                    QihooPlayerActivity qihooPlayerActivity = QihooPlayerActivity.this;
                    qihooPlayerActivity.slownessCount = qihooPlayerActivity.slownessCount + 1;
                }
            }
        });
        this.mVideoView.setOnSeekCompleteListener(new QihooMediaPlayer.OnSeekCompleteListener() {
            public void onSeekComplete(QihooMediaPlayer qihooMediaPlayer) {
                QihooPlayerActivity qihooPlayerActivity = QihooPlayerActivity.this;
                qihooPlayerActivity.slownessCount = qihooPlayerActivity.slownessCount - 1;
            }
        });
    }

    private void show3GTips(final DialogInterface.OnClickListener onClickListener) {
        this.m3GDialog = new AlertDialog.Builder(this).setTitle(R.string.net_tips).setMessage(R.string.wifi_invaild).setPositiveButton(R.string.pause, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                QihooPlayerActivity.this.finishPlayer();
                dialogInterface.dismiss();
            }
        }).setNegativeButton(R.string.continueplay, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (onClickListener != null) {
                    onClickListener.onClick(dialogInterface, i);
                }
                dialogInterface.dismiss();
            }
        }).setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
                QihooPlayerActivity.this.finishPlayer();
            }
        }).show();
        this.m3GDialog.setCanceledOnTouchOutside(false);
    }

    public void changeQualityVideo(WebsiteInfo websiteInfo, WebsiteInfo websiteInfo2) {
        Log.i("jy", "mpa changeQualityVideo: " + this.mVideoView.getCurrentPosition());
        this.playInfo.setPlayTimeStamp((long) this.mCurrentPosition);
        this.mLastWebSiteInfo = websiteInfo;
        if (this.slownessCount > 0 && this.mCurrentPosition > 0) {
            reportPlayError(4);
            this.slownessCount = 0;
        }
        if (websiteInfo2 != null) {
            this.playInfo.getVideoWebSite().setSelectedWebsite(websiteInfo2);
            this.playInfo.setXstmUrl(websiteInfo2.getXstm());
            doVideoStreamParsing();
        }
    }

    public void finish() {
        Intent intent = new Intent();
        intent.putExtra(PLAY_URL, XstmUrlBuilder.parse(this.playInfo.getXstmUrl()));
        setResult(this.mPlayState, intent);
        this.mVideoView.release();
        super.finish();
    }

    public void finishPlayer() {
        try {
            finish();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            super.finish();
        }
    }

    public void fullScreenPlay() {
    }

    public ArrayList<String> getLocalFiles(String str) {
        ArrayList<String> arrayList = new ArrayList<>();
        File file = new File(str);
        if (file.isDirectory()) {
            arrayList.addAll(Arrays.asList(file.list(new FilenameFilter() {
                public boolean accept(File file, String str) {
                    return !".".equals(str) && !"..".equals(str) && !"finish.txt".equals(str);
                }
            })));
        } else {
            arrayList.add(str);
        }
        return arrayList;
    }

    public boolean isNotLocalFile() {
        return this.playInfo != null && !this.playInfo.getIsLocalFile();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().addFlags(128);
        if (Build.VERSION.SDK_INT < 9) {
            setRequestedOrientation(0);
        }
        setContentView(R.layout.activity_player);
        init();
        setVideoView();
        requestData();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mVideoView.stop();
        this.mVideoView.release();
        unregisterReceiver(this.receiver);
        if (this.mPlayerControlView != null) {
            this.mPlayerControlView.onDestroy();
        }
        if (this.m3GDialog != null) {
            this.m3GDialog.dismiss();
        }
        if (this.slownessCount > 0 && this.mCurrentPosition > 0) {
            reportPlayError(4);
            this.slownessCount = 0;
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            if (keyEvent.getRepeatCount() != 0) {
                return super.onKeyDown(i, keyEvent);
            }
            if (!this.mWillExit) {
                this.mWillExit = true;
                Toast.makeText(this, R.string.press_again_will_exit_player, 0).show();
                this.mHandler.postDelayed(new Runnable() {
                    public void run() {
                        QihooPlayerActivity.this.mWillExit = false;
                    }
                }, 3000);
                return true;
            }
            finishPlayer();
        }
        return super.onKeyDown(i, keyEvent);
    }

    public void onRequestFailed(VideoRequestUtils.RequestError requestError) {
        Toast.makeText(this, R.string.player_network_error, 0).show();
        cancelDelay(2000);
        reportPlayError(1);
    }

    public void onRequestSucess(XstmInfo xstmInfo) {
        this.jsonPlayData = xstmInfo.xstm;
        this.playInfo.setRefUrl(xstmInfo.refUrl);
        setVideoTitle(this.jsonPlayData);
        if (!TextUtils.isEmpty(this.jsonPlayData)) {
            this.startPrepareTime = System.currentTimeMillis();
            this.mHandler.removeMessages(4);
            this.mHandler.sendEmptyMessageDelayed(4, 10000);
            this.mVideoView.reset();
            try {
                this.mVideoView.setDataSource(new JsonVideoSource(this.jsonPlayData));
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, R.string.local_source_not_play, 0).show();
                finishPlayer();
            }
        } else {
            changeWebsiteInfoFail();
            reportPlayError(1);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (isControllablePlay() && !isScreenLocked()) {
            this.mVideoView.setVisibility(0);
        }
        if (this.mPlayerControlView != null) {
            this.mPlayerControlView.startControlLight();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (isControllablePlay() && this.mVideoView != null) {
            this.mVideoView.pause();
            this.mVideoView.setVisibility(8);
        }
        if (this.mPlayerControlView != null) {
            this.mPlayerControlView.stopControlLight();
        }
    }

    public void playVideo(int i) {
        playVideo(i, false);
    }

    public void playVideo(final int i, final boolean z) {
        this.playInfo.setPlayCount(i - 1);
        this.mCurrentPosition = 0;
        this.playInfo.setPlayTimeStamp(0);
        this.playInfo.setPlayCount(i);
        if (!this.is3GChangePlayPos || !NetWorkState.is3G(this)) {
            this.playInfo.setIsLocalFile(false);
            doVideoStreamParsing();
            return;
        }
        playVideoShow3GDialog(new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                QihooPlayerActivity.this.is3GChangePlayPos = false;
                QihooPlayerActivity.this.playVideo(i, z);
            }
        });
    }
}
