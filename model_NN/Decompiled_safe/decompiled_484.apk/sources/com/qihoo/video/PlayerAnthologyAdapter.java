package com.qihoo.video;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.qihoo.mediaplayer.R;

public class PlayerAnthologyAdapter extends BaseAdapter {
    private int anthologyCount = 0;
    private Context mContext = null;
    private String playCurrentIndex = null;
    private int playIndex = -1;
    private WebsiteInfo webSiteInfo = null;

    public PlayerAnthologyAdapter(Context context) {
        this.mContext = context;
        this.playCurrentIndex = this.mContext.getResources().getString(R.string.playCurrentIndexString);
    }

    public int getCount() {
        return this.anthologyCount;
    }

    public Object getItem(int i) {
        return null;
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView textView = view != null ? (TextView) view : (TextView) LayoutInflater.from(this.mContext).inflate(R.layout.anthology_layout, (ViewGroup) null);
        textView.setClickable(false);
        textView.setBackgroundResource(R.drawable.antholoy_background_selector);
        if (this.playIndex == i) {
            ColorStateList colorStateList = this.mContext.getResources().getColorStateList(R.drawable.text_hightlighted_color_selector);
            if (colorStateList != null) {
                textView.setTextColor(colorStateList);
            }
        } else {
            ColorStateList colorStateList2 = this.mContext.getResources().getColorStateList(R.color.text_color_pressed);
            if (colorStateList2 != null) {
                textView.setTextColor(colorStateList2);
            }
        }
        if (!(this.webSiteInfo == null || this.webSiteInfo.getLost() == null || this.webSiteInfo.getLost().length <= 0)) {
            for (int i2 : this.webSiteInfo.getLost()) {
                if (i2 == i + 1) {
                    textView.setClickable(true);
                    textView.setTextColor(this.mContext.getResources().getColor(R.color.source_checked_color));
                }
            }
        }
        if (this.playCurrentIndex != null) {
            int i3 = i + 1;
            if (this.playCurrentIndex.indexOf("$") >= 0) {
                textView.setText(this.playCurrentIndex.replaceAll("\\$", String.valueOf(i3)));
            }
        }
        return textView;
    }

    public void setPlayIndex(int i) {
        this.playIndex = i;
        notifyDataSetChanged();
    }

    public void setWebsiteInfo(WebsiteInfo websiteInfo) {
        if (websiteInfo != null) {
            this.webSiteInfo = websiteInfo;
            this.anthologyCount = (int) this.webSiteInfo.getUpdatedInfo();
        }
    }
}
