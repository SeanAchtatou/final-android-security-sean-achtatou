package com.qihoo.video;

import android.util.Log;
import com.qihoo.dynamic.util.Md5Util;
import com.qihoo.http.base.HttpUtils;
import com.qihoo.qplayer.QHPlayerSDK;
import com.qihoo.qplayer.util.HttpRequest;
import com.qihoo.video.plugins.XstmConverter;
import com.qihoo.video.plugins.XstmHackLoader;
import java.net.URLEncoder;
import java.util.Locale;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class Requests {
    public static void reportPlayError(Integer num, Integer num2, String str, String str2, String str3, Integer num3, Integer num4) {
        if (str3 != null) {
            try {
                str3 = URLEncoder.encode(str3, Md5Util.DEFAULT_CHARSET);
            } catch (Exception e) {
            }
        }
        String str4 = "http://xstm.v.360.cn/report.html?e=" + num + "&type=" + num2 + "&id=" + str2 + "&f=" + str + "&url=" + str3 + "&rnd=" + System.currentTimeMillis() + "&pt=" + num3 + "&ec=" + num4;
        Log.e("reportPlayError", "reportPlayError " + str4);
        try {
            Log.e("reportPlayError", "reportPlayError ====== " + EntityUtils.toString(HttpUtils.getThreadSafeClient().execute(new HttpGet(str4)).getEntity(), Md5Util.DEFAULT_CHARSET));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static XstmInfo requestXstm(String str, String str2, String str3) {
        Log.e("lcy", "requestXstm============================================");
        Log.e("jy", "Request.requestXstm() " + str);
        XstmInfo requestXstmRaw = requestXstmRaw(str, str2, str3);
        if (requestXstmRaw == null) {
            Log.e("lcy", "info == null");
        } else if (requestXstmRaw.errorCode != 0) {
            Log.e("lcy", "info.errorCode != NetWorkError.NetWorkNoError");
        } else {
            Log.d("lcy", "requestXstm requestXstmRaw xstm = " + requestXstmRaw.xstm);
            try {
                XstmConverter.XstmHackParam fromString = XstmConverter.XstmHackParam.fromString(requestXstmRaw.xstm);
                if (fromString == null) {
                    Log.d("lcy", "param == null 不需要破解");
                } else {
                    fromString.request.quality = XstmConverter.getQuality(str2);
                    XstmConverter.XstmResponse hackXstm = XstmHackLoader.getInstance(QHPlayerSDK.getInstance().getContext()).hackXstm(QHPlayerSDK.getInstance().getContext(), fromString.packageversion, fromString.packageurl, fromString.request.toString());
                    if (hackXstm == null || !hackXstm.result.equals("NO_ERROR")) {
                        Log.d("lcy", "破解失败");
                    } else {
                        Log.d("lcy", "破解成功 xstm = " + hackXstm.xstm);
                        requestXstmRaw.xstm = hackXstm.xstm;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
                requestXstmRaw.errorCode = 2;
            }
        }
        return requestXstmRaw;
    }

    public static XstmInfo requestXstmRaw(String str, String str2, String str3) {
        XstmInfo xstmInfo = new XstmInfo();
        int i = 0;
        if (str == null) {
            return null;
        }
        Log.i("jy", "Requests.requestXstmRaw() " + str);
        String str4 = String.valueOf(str) + HttpRequest.Maker.getMakerTails(str) + "&quality=" + str2 + "&act=play" + "&" + RequestSource.KEY + "=" + RequestSource.PLAYER_SDK + "&ver=1.0" + "&ch=" + QHPlayerSDK.getInstance().getPackageName();
        Log.d("requestXstm", "破解请求urlString = " + str4);
        String executeGetResult = HttpRequest.executeGetResult(str4);
        if (executeGetResult != null && executeGetResult.toLowerCase(Locale.CHINA).contains("<html>")) {
            i = 1;
        }
        xstmInfo.errorCode = i;
        try {
            JSONObject jSONObject = new JSONObject(executeGetResult);
            xstmInfo.xstm = executeGetResult;
            xstmInfo.refUrl = jSONObject.optString("ref");
            JSONObject optJSONObject = jSONObject.optJSONObject("currqua");
            xstmInfo.currentQuality = String.valueOf(optJSONObject.keys().next());
            xstmInfo.quality = optJSONObject.optString(xstmInfo.currentQuality);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return xstmInfo;
    }
}
