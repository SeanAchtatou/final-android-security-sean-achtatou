package com.qihoo.video;

public class CatalogModel {
    public static final int CATALOG_CARTOON = 4;
    public static final int CATALOG_MOVIE = 1;
    public static final int CATALOG_SHORT = 0;
    public static final int CATALOG_TV = 2;
    public static final int CATALOG_VARIETY = 3;
}
