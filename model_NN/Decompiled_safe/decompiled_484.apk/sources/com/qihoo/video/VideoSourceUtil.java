package com.qihoo.video;

import com.qihoo.mediaplayer.R;
import java.util.HashMap;
import java.util.Locale;

public class VideoSourceUtil {
    private static HashMap<String, Integer> iconMap = new HashMap<>();

    public static int getVideoSourceResouceId(String str) {
        if (iconMap.isEmpty()) {
            iconMap.put("cntv", Integer.valueOf(R.drawable.cntv));
            iconMap.put("sohu", Integer.valueOf(R.drawable.sohu));
            iconMap.put("baofen", Integer.valueOf(R.drawable.baofen));
            iconMap.put("funshion", Integer.valueOf(R.drawable.funshion));
            iconMap.put("fengxing", Integer.valueOf(R.drawable.funshion));
            iconMap.put("qiyi", Integer.valueOf(R.drawable.iqiyi));
            iconMap.put("iqiyi", Integer.valueOf(R.drawable.iqiyi));
            iconMap.put("letv", Integer.valueOf(R.drawable.letv));
            iconMap.put("leshi", Integer.valueOf(R.drawable.letv));
            iconMap.put("m1905", Integer.valueOf(R.drawable.m1905));
            iconMap.put("pptv", Integer.valueOf(R.drawable.pptv));
            iconMap.put("tencent", Integer.valueOf(R.drawable.tencent));
            iconMap.put("tudou", Integer.valueOf(R.drawable.tudou));
            iconMap.put("xunlei", Integer.valueOf(R.drawable.xunlei));
            iconMap.put("taomi", Integer.valueOf(R.drawable.taomi));
            iconMap.put("kumi", Integer.valueOf(R.drawable.kumi));
            iconMap.put("pps", Integer.valueOf(R.drawable.pps));
            iconMap.put("youku", Integer.valueOf(R.drawable.youku));
            iconMap.put("56com", Integer.valueOf(R.drawable.icon_56));
        }
        if (str == null) {
            return -1;
        }
        String lowerCase = str.toLowerCase(Locale.getDefault());
        return iconMap.containsKey(lowerCase) ? iconMap.get(lowerCase).intValue() : R.drawable.other;
    }
}
