package com.qihoo.video;

import android.content.Context;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.qihoo.qplayer.view.IVideoViewController;

public abstract class BasePlayerControllView extends FrameLayout implements IVideoViewController {
    protected Context mContext;

    public BasePlayerControllView(Context context) {
        super(context);
        init(context);
    }

    public BasePlayerControllView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    public BasePlayerControllView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init(context);
    }

    private void init(Context context) {
        this.mContext = context;
        setFocusable(true);
        setFocusableInTouchMode(true);
        setDescendantFocusability(AccessibilityEventCompat.TYPE_GESTURE_DETECTION_START);
        requestFocus();
    }
}
