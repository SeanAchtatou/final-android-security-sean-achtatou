package com.qihoo.video;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

public class MaskHelper {
    private static final int INVALID_ID = -1;
    public static final String KEY_IMAGEBROWER_GUIDE = "KEY_IMAGEBROWER_GUIDE";
    public static final String KEY_MAIN_GUIDE = "KEY_MAIN_GUIDE";
    public static final String KEY_PLAYER_GUIDE = "KEY_PLAYER_GUIDE";
    private static final String SF_NAME = "mask_helper";
    private View mView;
    private WindowManager mWindowManager;
    private WindowManager.LayoutParams mWindowParams;

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean checkOnce(android.content.Context r9, java.lang.String r10) {
        /*
            r8 = this;
            r4 = 1
            r2 = 0
            java.lang.String r0 = "mask_helper"
            android.content.SharedPreferences r5 = r9.getSharedPreferences(r0, r2)
            r0 = 0
            java.lang.String r0 = r5.getString(r10, r0)
            if (r0 != 0) goto L_0x003a
            r3 = r4
        L_0x0010:
            if (r3 == 0) goto L_0x0055
            java.util.Map r0 = r5.getAll()
            java.util.Set r0 = r0.keySet()
            java.util.Iterator r6 = r0.iterator()
            r1 = r2
        L_0x001f:
            boolean r0 = r6.hasNext()
            if (r0 != 0) goto L_0x003c
            if (r1 != r4) goto L_0x004e
            java.lang.String r0 = "KEY_MAIN_GUIDE"
            boolean r0 = r0.equals(r10)
            if (r0 == 0) goto L_0x0055
            java.lang.String r0 = "KEY_MAIN_GUIDE"
            r8.commit(r0, r5)
        L_0x0034:
            if (r2 == 0) goto L_0x0039
            r8.commit(r10, r5)
        L_0x0039:
            return r2
        L_0x003a:
            r3 = r2
            goto L_0x0010
        L_0x003c:
            java.lang.Object r0 = r6.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r7 = "KEY"
            boolean r0 = r0.startsWith(r7)
            if (r0 != 0) goto L_0x001f
            int r0 = r1 + 1
            r1 = r0
            goto L_0x001f
        L_0x004e:
            r0 = 2
            if (r1 != r0) goto L_0x0055
            r8.commit(r10, r5)
            goto L_0x0034
        L_0x0055:
            r2 = r3
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo.video.MaskHelper.checkOnce(android.content.Context, java.lang.String):boolean");
    }

    private void commit(String str, SharedPreferences sharedPreferences) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(str, str);
        edit.commit();
    }

    public static void reset(Context context, int i) {
        SharedPreferences.Editor edit = context.getSharedPreferences(SF_NAME, 0).edit();
        edit.putInt(String.valueOf(-1), i);
        edit.commit();
    }

    public void create(Context context, int i) {
        create(context, LayoutInflater.from(context).inflate(i, (ViewGroup) null));
    }

    public void create(Context context, View view) {
        this.mView = view;
        this.mWindowParams = new WindowManager.LayoutParams();
        this.mWindowParams.gravity = 51;
        this.mWindowParams.x = 0;
        this.mWindowParams.y = 0;
        this.mWindowParams.height = -1;
        this.mWindowParams.width = -1;
        this.mWindowParams.format = -3;
        this.mWindowParams.windowAnimations = 0;
        this.mWindowManager = (WindowManager) context.getSystemService("window");
        this.mWindowManager.addView(this.mView, this.mWindowParams);
        view.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MaskHelper.this.destroy();
            }
        });
    }

    public void createOnce(Context context, int i, String str) {
        if (checkOnce(context, str)) {
            create(context, i);
        }
    }

    public void createOnce(Context context, View view, int i, String str) {
        if (checkOnce(context, str)) {
            create(context, view);
        }
    }

    public void destroy() {
        try {
            this.mWindowManager.removeView(this.mView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hide() {
        if (this.mView.getVisibility() == 0) {
            this.mView.setVisibility(4);
        }
    }

    public void show() {
        if (this.mView.getVisibility() != 0) {
            this.mView.setVisibility(0);
        }
    }
}
