package com.qihoo.video;

public class PlayError {
    public static final int ERROR_PLAYER = 2;
    public static final int ERROR_SLOW = 4;
    public static final int ERROR_STEAM = 1;
    public static final int ERROR_TIMEOUT = 3;
}
