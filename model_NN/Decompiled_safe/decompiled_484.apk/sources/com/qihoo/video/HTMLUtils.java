package com.qihoo.video;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTMLUtils {
    public static final HashMap<String, String> character = new HashMap<>();

    static {
        character.put("&quot;", "\"");
        character.put("&apos;", "'");
        character.put("&amp;", "&");
        character.put("&lt;", "<");
        character.put("&gt;", ">");
        character.put("&nbsp;", " ");
        character.put("&iexcl;", "¡");
        character.put("&cent;", "¢");
        character.put("&pound;", "£");
        character.put("&curren;", "¤");
        character.put("&yen;", "¥");
        character.put("&brvbar;", "¦");
        character.put("&sect;", "§");
        character.put("&uml;", "¨");
        character.put("&copy;", "©");
        character.put("&ordf;", "ª");
        character.put("&laquo;", "«");
        character.put("&not;", "¬");
        character.put("&shy;", String.valueOf(Character.toChars(173)));
        character.put("&reg;", "®");
        character.put("&macr;", "¯");
        character.put("&deg;", "°");
        character.put("&plusmn;", "±");
        character.put("&sup2;", "²");
        character.put("&sup3;", "³");
        character.put("&acute;", "´");
        character.put("&micro;", "µ");
        character.put("&para;", "¶");
        character.put("&middot;", "·");
        character.put("&cedil;", "¸");
        character.put("&sup1;", "¹");
        character.put("&ordm;", "º");
        character.put("&raquo;", "»");
        character.put("&frac14;", "¼");
        character.put("&frac12;", "½");
        character.put("&frac34;", "¾");
        character.put("&iquest;", "¿");
        character.put("&times;", "×");
        character.put("&divide;", "÷");
    }

    public static String unentityify(String str) {
        String valueOf;
        Matcher matcher = Pattern.compile("&[a-z]+;|&#([0-9]+);").matcher(str);
        StringBuffer stringBuffer = new StringBuffer();
        while (matcher.find()) {
            String group = matcher.group();
            String group2 = matcher.group(1);
            if (group2 == null) {
                valueOf = character.get(group);
                if (valueOf == null) {
                    valueOf = group;
                }
            } else {
                valueOf = String.valueOf(Character.toChars(Integer.parseInt(group2)));
            }
            matcher.appendReplacement(stringBuffer, valueOf);
        }
        matcher.appendTail(stringBuffer);
        return stringBuffer.toString();
    }
}
