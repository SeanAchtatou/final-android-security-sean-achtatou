package com.qihoo.video;

public enum WebsiteStatus {
    STATUS_NOMAL,
    STATUS_SELECTED,
    STATUS_LOADING,
    STATUS_FAILED
}
