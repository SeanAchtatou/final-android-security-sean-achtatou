package com.qihoo.video;

import android.text.Html;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;

public class EpisodeSerial {
    public byte catalog;
    public String episode;
    public EpisodeZyInfo[] episodeList = null;
    public int errorCode;
    public String from;
    public String title;
    public String vid = null;
    public String xstm = null;
    public String xstmUrl = null;

    public EpisodeSerial(JSONObject jSONObject, int i) {
        this.errorCode = i;
        if (jSONObject != null) {
            this.catalog = (byte) jSONObject.optInt("cat");
            this.title = jSONObject.optString("title");
            this.title = HTMLUtils.unentityify(this.title);
            if (this.title != null) {
                this.title = Html.fromHtml(this.title).toString();
            }
            this.from = jSONObject.optString("from");
            JSONArray optJSONArray = jSONObject.optJSONArray("allepisode");
            if (optJSONArray == null) {
                this.episode = jSONObject.optString("episode");
                this.xstm = jSONObject.optString("xstm");
                return;
            }
            int length = optJSONArray.length();
            this.episodeList = new EpisodeZyInfo[length];
            for (int i2 = 0; i2 < length; i2++) {
                JSONObject optJSONObject = optJSONArray.optJSONObject(i2);
                if (i2 == 0) {
                    this.xstm = optJSONObject.optString("xstm");
                }
                if (optJSONObject != null) {
                    this.episodeList[i2] = new EpisodeZyInfo(optJSONObject);
                }
            }
        }
    }

    public String toString() {
        return "EpisodeSerial [catalog=" + ((int) this.catalog) + ", title=" + this.title + ", from=" + this.from + ", xstm=" + this.xstm + ", episode=" + this.episode + ", episodeList=" + Arrays.toString(this.episodeList) + ", xstmUrl=" + this.xstmUrl + ", errorCode=" + this.errorCode + "]";
    }
}
