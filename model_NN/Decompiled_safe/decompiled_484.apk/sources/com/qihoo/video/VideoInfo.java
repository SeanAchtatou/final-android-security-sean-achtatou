package com.qihoo.video;

import org.json.JSONArray;
import org.json.JSONObject;

public class VideoInfo {
    public String[] actor = null;
    public String[] area = null;
    public byte cat = 0;
    public String[] director = null;
    public String epiname = null;
    public boolean finish = false;
    public String imageUrl = null;
    public String[] moviecat = null;
    public float score = 0.0f;
    public String title = null;
    public String[] type = null;
    public int upinfo = 0;
    public String videoId = null;
    public String word = null;
    public String year = null;

    public VideoInfo(JSONObject jSONObject) {
        if (jSONObject != null) {
            this.cat = (byte) jSONObject.optInt("cat");
            this.videoId = jSONObject.optString("id");
            this.title = jSONObject.optString("title");
            this.title = HTMLUtils.unentityify(this.title);
            this.imageUrl = jSONObject.optString("cover");
            this.year = jSONObject.optString("year");
            this.score = (float) jSONObject.optDouble("score");
            this.upinfo = jSONObject.optInt("upinfo");
            this.finish = jSONObject.optBoolean("finish");
            this.epiname = jSONObject.optString("epiname");
            this.word = jSONObject.optString("word");
            this.area = arrayByJSONArray(jSONObject, "area");
            this.actor = arrayByJSONArray(jSONObject, "actor");
            this.director = arrayByJSONArray(jSONObject, "director");
            this.type = arrayByJSONArray(jSONObject, "type");
            this.moviecat = arrayByJSONArray(jSONObject, "moviecat");
        }
    }

    private String[] arrayByJSONArray(JSONObject jSONObject, String str) {
        try {
            JSONArray jSONArray = jSONObject.getJSONArray(str);
            String[] strArr = new String[jSONArray.length()];
            for (int i = 0; i < jSONArray.length(); i++) {
                strArr[i] = jSONArray.getString(i);
            }
            return strArr;
        } catch (Exception e) {
            return null;
        }
    }
}
