package com.qihoo.video.httpservice;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Build;
import java.util.concurrent.RejectedExecutionException;

public abstract class ParallelAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {
    @SuppressLint({"NewApi"})
    public final void executeOnPoolExecutor(Params... paramsArr) {
        if (Build.VERSION.SDK_INT >= 11) {
            try {
                super.executeOnExecutor(THREAD_POOL_EXECUTOR, paramsArr);
            } catch (RejectedExecutionException e) {
                e.printStackTrace();
            }
        } else {
            try {
                super.execute(paramsArr);
            } catch (RejectedExecutionException e2) {
                e2.printStackTrace();
            }
        }
    }
}
