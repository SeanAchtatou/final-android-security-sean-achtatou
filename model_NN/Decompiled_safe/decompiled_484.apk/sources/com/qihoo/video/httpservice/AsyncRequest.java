package com.qihoo.video.httpservice;

import android.app.Activity;
import com.qihoo.qplayer.util.MainThreadHandlerUtil;

public class AsyncRequest extends ParallelAsyncTask<Object, Object, Object> {
    protected OnRecivedDataListener mOnReceivedDataListener = null;
    protected AsyncRequest request = null;

    public interface OnRecivedDataListener {
        void OnRecivedData(AsyncRequest asyncRequest, Object obj);
    }

    public AsyncRequest(Activity activity, String str, String str2) {
    }

    /* access modifiers changed from: protected */
    public Object doInBackground(Object... objArr) {
        return null;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(final Object obj) {
        this.request = this;
        MainThreadHandlerUtil.getMainThreadHandler().post(new Runnable() {
            public void run() {
                if (AsyncRequest.this.mOnReceivedDataListener != null) {
                    AsyncRequest.this.mOnReceivedDataListener.OnRecivedData(AsyncRequest.this.request, obj);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
    }

    public void setOnRecivedDataListener(OnRecivedDataListener onRecivedDataListener) {
        this.mOnReceivedDataListener = onRecivedDataListener;
    }
}
