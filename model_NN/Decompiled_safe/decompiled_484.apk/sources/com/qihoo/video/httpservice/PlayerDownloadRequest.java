package com.qihoo.video.httpservice;

import com.qihoo360.daily.activity.SearchActivity;
import org.json.JSONObject;

public class PlayerDownloadRequest extends BaseHttpRequest {

    public class PlayerDownloadParam {
        public String so_type;
        public String so_ver;
    }

    public PlayerDownloadRequest() {
        super(null, null, null, "player");
    }

    /* access modifiers changed from: protected */
    public Object doInBackground(Object... objArr) {
        if (objArr == null || !(objArr[0] instanceof PlayerDownloadParam)) {
            return null;
        }
        PlayerDownloadParam playerDownloadParam = (PlayerDownloadParam) objArr[0];
        appendGetParameter("method", "player.download");
        appendGetParameter("so_ver", playerDownloadParam.so_ver);
        appendGetParameter("so_type", playerDownloadParam.so_type);
        JSONObject requestGetDataJsonObject = requestGetDataJsonObject();
        if (requestGetDataJsonObject == null) {
            return null;
        }
        return requestGetDataJsonObject.optString(SearchActivity.TAG_URL);
    }
}
