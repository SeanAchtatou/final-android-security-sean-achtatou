package com.qihoo.video.httpservice;

public class NetWorkError {
    public static final String ErrorCode = "error";
    public static final int NetWorkNoError = 0;
    public static final int NetWorkTimeOut = 1;
    public static final int NetWorkUnKnow = 2;
}
