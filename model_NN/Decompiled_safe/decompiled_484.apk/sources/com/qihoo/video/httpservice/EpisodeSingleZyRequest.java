package com.qihoo.video.httpservice;

import android.app.Activity;
import android.text.TextUtils;
import com.qihoo.video.EpisodeSerial;
import com.qihoo.video.RequestSource;
import com.qihoo.video.Requests;
import com.qihoo.video.VideoRequestUtils;
import com.qihoo.video.XstmInfo;

public class EpisodeSingleZyRequest extends BaseHttpRequest {
    public EpisodeSingleZyRequest(Activity activity, String str, String str2) {
        super(activity, str, str2, "episode");
    }

    /* access modifiers changed from: protected */
    public Object doInBackground(Object... objArr) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7 = "play";
        Integer.valueOf(0);
        String str8 = objArr.length >= 8 ? objArr[7] : RequestSource.PLAYER_SDK;
        if (objArr.length >= 7) {
            String str9 = (String) objArr[0];
            str3 = (String) objArr[1];
            String str10 = (String) objArr[2];
            String str11 = (String) objArr[3];
            str2 = (String) objArr[4];
            str7 = (String) objArr[5];
            Integer num = (Integer) objArr[6];
            String str12 = objArr.length >= 8 ? objArr[7] : RequestSource.PLAYER_SDK;
            String str13 = objArr.length >= 9 ? objArr[8] : null;
            str6 = str11;
            str5 = str10;
            str4 = str9;
            str = null;
        } else if (objArr.length != 1 || !(objArr[0] instanceof VideoRequestUtils.VideoRequestParams)) {
            str = null;
            str2 = null;
            str3 = null;
            str4 = null;
            str5 = null;
            str6 = null;
        } else {
            VideoRequestUtils.VideoRequestParams videoRequestParams = (VideoRequestUtils.VideoRequestParams) objArr[0];
            String videoId = videoRequestParams.getVideoId();
            String b2 = Byte.toString(videoRequestParams.getCatalog());
            String index = videoRequestParams.getIndex();
            String siteKey = videoRequestParams.getSiteKey();
            str2 = videoRequestParams.getQualityKey();
            String xstmUrl = videoRequestParams.getXstmUrl();
            Integer.valueOf(videoRequestParams.getPlayerSDK());
            videoRequestParams.getRequestSource();
            videoRequestParams.getZsParams();
            str = xstmUrl;
            str3 = b2;
            str4 = videoId;
            str5 = index;
            str6 = siteKey;
        }
        appendGetParameter("id", str4);
        appendGetParameter("cat", str3);
        appendGetParameter("index", str5);
        appendGetParameter("site", str6);
        appendGetParameter("quality", str2);
        appendGetParameter("method", "episode.zongyisingle");
        EpisodeSerial episodeSerial = new EpisodeSerial(requestGetDataJsonObject(), 0);
        if (isCancelled()) {
            return null;
        }
        if (episodeSerial != null) {
            str = episodeSerial.xstm;
        }
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        XstmInfo requestXstm = Requests.requestXstm(str, str2, str7);
        if (!isCancelled()) {
            return requestXstm;
        }
        return null;
    }
}
