package com.qihoo.video.httpservice;

import android.app.Activity;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.qihoo.dynamic.util.Md5Util;
import com.qihoo.http.base.HttpUtils;
import com.qihoo.qplayer.QHPlayerSDK;
import com.qihoo.video.GzipUtils;
import com.qihoo360.daily.activity.SendCmtActivity;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

public abstract class BaseHttpRequest extends AsyncRequest {
    protected Uri.Builder mBuilder = new Uri.Builder();
    protected int mNetConnectionStatus = 0;
    private List<NameValuePair> mPostParamList;
    private String method = "";

    public BaseHttpRequest(Activity activity, String str, String str2) {
        super(activity, str, str2);
    }

    public BaseHttpRequest(Activity activity, String str, String str2, String str3) {
        super(activity, str, str2);
        this.method = str3;
    }

    private void appendCommonParameter() {
        this.mBuilder.appendQueryParameter("token", QHPlayerSDK.getInstance().getToken());
        this.mBuilder.appendQueryParameter("ch", QHPlayerSDK.getInstance().getPackageName());
    }

    private JSONObject executeDataJSONObject(JSONObject jSONObject) {
        JSONObject optJSONObject;
        if (jSONObject == null || (optJSONObject = jSONObject.optJSONObject(SendCmtActivity.TAG_DATA)) == null) {
            return null;
        }
        return optJSONObject.optJSONObject(SendCmtActivity.TAG_DATA);
    }

    private JSONArray executeDataJsonArray(JSONObject jSONObject) {
        JSONObject optJSONObject;
        if (jSONObject == null || (optJSONObject = jSONObject.optJSONObject(SendCmtActivity.TAG_DATA)) == null) {
            return null;
        }
        return optJSONObject.optJSONArray(SendCmtActivity.TAG_DATA);
    }

    private JSONObject executeJsonObject(String str) {
        Log.d("Response", "Response: " + str);
        if (this.mNetConnectionStatus == 0) {
            try {
                return parseJSONObject(str);
            } catch (Exception e) {
                e.printStackTrace();
                e.getLocalizedMessage();
                e.toString();
                this.mNetConnectionStatus = 2;
            }
        }
        return null;
    }

    private String executePostResult(String str) {
        HttpResponse httpResponse;
        Log.e("HttpRequest", "requestPost url: " + str);
        try {
            HttpPost httpPost = new HttpPost(str);
            httpPost.addHeader("Accept-Encoding", "gzip");
            httpPost.setEntity(new UrlEncodedFormEntity(this.mPostParamList, Md5Util.DEFAULT_CHARSET));
            httpResponse = HttpUtils.getThreadSafeClient().execute(httpPost);
            try {
                return GzipUtils.getJsonStringFromGZIP(httpResponse);
            } catch (Exception e) {
                e = e;
            }
        } catch (Exception e2) {
            e = e2;
            httpResponse = null;
        }
        e.printStackTrace();
        if (httpResponse == null) {
            this.mNetConnectionStatus = 1;
            return null;
        }
        this.mNetConnectionStatus = 2;
        return null;
    }

    /* access modifiers changed from: protected */
    public void appendGetParameter(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            this.mBuilder.appendQueryParameter(str, str2);
        }
    }

    /* access modifiers changed from: protected */
    public void appendGetParameter(Map<String, String> map) {
        if (map != null) {
            for (Map.Entry next : map.entrySet()) {
                this.mBuilder.appendQueryParameter((String) next.getKey(), (String) next.getValue());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void appendPostParameter(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            if (this.mPostParamList == null) {
                this.mPostParamList = new ArrayList();
            }
            this.mPostParamList.add(new BasicNameValuePair(str, str2));
        }
    }

    /* access modifiers changed from: package-private */
    public void buildFromUrl(String str) {
        this.mBuilder = Uri.parse(str).buildUpon();
    }

    /* access modifiers changed from: protected */
    public void buildUrl(String str, String str2, String str3) {
        this.mBuilder.scheme(str);
        this.mBuilder.authority(str2);
        this.mBuilder.path(str3);
    }

    /* access modifiers changed from: protected */
    public abstract Object doInBackground(Object... objArr);

    /* access modifiers changed from: protected */
    public String executeGetResult(String str) {
        HttpResponse httpResponse;
        Log.e("HttpRequest", "requestGet url: " + str);
        try {
            HttpGet httpGet = new HttpGet(str);
            httpGet.addHeader("Accept-Encoding", "gzip");
            httpResponse = HttpUtils.getThreadSafeClient().execute(httpGet);
            try {
                return GzipUtils.getJsonStringFromGZIP(httpResponse);
            } catch (Exception e) {
                e = e;
            }
        } catch (Exception e2) {
            e = e2;
            httpResponse = null;
        }
        e.printStackTrace();
        if (httpResponse == null) {
            this.mNetConnectionStatus = 1;
            return null;
        }
        this.mNetConnectionStatus = 2;
        return null;
    }

    /* access modifiers changed from: protected */
    public String executePostResult(String str, byte[] bArr) {
        HttpResponse httpResponse;
        Log.e("SharedVideo", "requestPost url: " + str);
        try {
            HttpPost httpPost = new HttpPost(str);
            httpPost.addHeader("Accept-Encoding", "gzip");
            httpPost.setEntity(new ByteArrayEntity(bArr));
            httpResponse = HttpUtils.getThreadSafeClient().execute(httpPost);
            try {
                return GzipUtils.getJsonStringFromGZIP(httpResponse);
            } catch (Exception e) {
                e = e;
            }
        } catch (Exception e2) {
            e = e2;
            httpResponse = null;
        }
        e.printStackTrace();
        if (httpResponse == null) {
            this.mNetConnectionStatus = 1;
            return null;
        }
        this.mNetConnectionStatus = 2;
        return null;
    }

    /* access modifiers changed from: protected */
    public String get360VideoUrl() {
        buildUrl("http", "android.api.360kan.com", "/" + this.method + "/");
        appendCommonParameter();
        return this.mBuilder.build().toString();
    }

    public int getNetConnectionStatus() {
        return this.mNetConnectionStatus;
    }

    /* access modifiers changed from: protected */
    public JSONObject parseJSONObject(String str) {
        if (str != null && str.length() >= 32) {
            String substring = str.substring(0, 32);
            String substring2 = str.substring(32);
            if (substring.equals(HttpUtils.MD5Encode(substring2))) {
                return new JSONObject(substring2);
            }
        }
        return null;
    }

    public JSONArray requestGetDataJsonArray() {
        return executeDataJsonArray(requestGetJsonObject());
    }

    public JSONArray requestGetDataJsonArray(String str) {
        return executeDataJsonArray(requestGetJsonObject(str));
    }

    public JSONObject requestGetDataJsonObject() {
        return executeDataJSONObject(requestGetJsonObject());
    }

    public JSONObject requestGetDataJsonObject(String str) {
        return executeDataJSONObject(requestGetJsonObject(str));
    }

    public JSONObject requestGetJsonObject() {
        return executeJsonObject(requestGetResult());
    }

    public JSONObject requestGetJsonObject(String str) {
        return executeJsonObject(requestGetResult(str));
    }

    public String requestGetResult() {
        return executeGetResult(get360VideoUrl());
    }

    public String requestGetResult(String str) {
        return executeGetResult(str);
    }

    public String requestGetResult(String str, String str2, String str3) {
        buildUrl(str, str2, str3);
        appendCommonParameter();
        return executeGetResult(this.mBuilder.build().toString());
    }

    public JSONArray requestPostDataJsonArray() {
        return executeDataJsonArray(requestPostJsonObject());
    }

    public JSONObject requestPostDataJsonObject() {
        return executeDataJSONObject(requestPostJsonObject());
    }

    public JSONObject requestPostJsonObject() {
        return executeJsonObject(requestPostResult());
    }

    public String requestPostResult() {
        return executePostResult(get360VideoUrl());
    }

    public String requestPostResult(String str, String str2, String str3) {
        buildUrl(str, str2, str3);
        appendCommonParameter();
        return executePostResult(this.mBuilder.build().toString());
    }

    /* access modifiers changed from: package-private */
    public void setRequestPathName(String str) {
        this.method = str;
    }
}
