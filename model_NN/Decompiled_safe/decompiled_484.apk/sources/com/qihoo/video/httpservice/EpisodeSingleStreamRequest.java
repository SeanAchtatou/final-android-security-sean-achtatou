package com.qihoo.video.httpservice;

import android.app.Activity;
import com.qihoo.video.EpisodeSerial;
import com.qihoo.video.RequestSource;
import com.qihoo.video.Requests;
import com.qihoo.video.VideoRequestUtils;
import com.qihoo.video.XstmInfo;

public class EpisodeSingleStreamRequest extends BaseHttpRequest {
    public EpisodeSingleStreamRequest(Activity activity, String str, String str2) {
        super(activity, str, str2, "episode");
    }

    /* access modifiers changed from: protected */
    public XstmInfo doInBackground(Object... objArr) {
        String str;
        String str2;
        int i;
        String str3;
        String str4;
        XstmInfo requestXstm;
        String str5 = "play";
        if (objArr.length >= 5) {
            str4 = (String) objArr[0];
            str2 = (String) objArr[1];
            int intValue = ((Integer) objArr[2]).intValue();
            String str6 = (String) objArr[3];
            str = (String) objArr[4];
            str5 = objArr.length >= 6 ? objArr[5] : null;
            (objArr.length >= 7 ? objArr[6] : 0).intValue();
            String str7 = objArr.length >= 8 ? objArr[7] : RequestSource.PLAYER_SDK;
            String str8 = objArr.length >= 9 ? objArr[8] : null;
            str3 = str6;
            i = intValue;
        } else if (objArr.length != 1 || !(objArr[0] instanceof VideoRequestUtils.VideoRequestParams)) {
            str = null;
            str2 = null;
            i = -1;
            str3 = null;
            str4 = null;
        } else {
            VideoRequestUtils.VideoRequestParams videoRequestParams = (VideoRequestUtils.VideoRequestParams) objArr[0];
            String videoId = videoRequestParams.getVideoId();
            str2 = Byte.toString(videoRequestParams.getCatalog());
            int intValue2 = Integer.valueOf(videoRequestParams.getIndex()).intValue();
            String siteKey = videoRequestParams.getSiteKey();
            str = videoRequestParams.getQualityKey();
            videoRequestParams.getPlayerSDK();
            videoRequestParams.getRequestSource();
            videoRequestParams.getZsParams();
            str4 = videoId;
            i = intValue2;
            str3 = siteKey;
        }
        appendGetParameter("id", str4);
        appendGetParameter("cat", str2);
        appendGetParameter("index", Integer.toString(i));
        appendGetParameter("site", str3);
        appendGetParameter("quality", str);
        appendGetParameter("method", "episode.single");
        EpisodeSerial episodeSerial = new EpisodeSerial(requestGetDataJsonObject(), 0);
        if (isCancelled() || episodeSerial == null || episodeSerial.xstm == null || (requestXstm = Requests.requestXstm(episodeSerial.xstm, str, str5)) == null || isCancelled()) {
            return null;
        }
        return requestXstm;
    }
}
