package com.qihoo.video;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AbsSeekBar;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.qihoo.mediaplayer.R;
import com.qihoo.qplayer.QihooMediaPlayer;
import com.qihoo.qplayer.view.IMediaPlayerController;
import com.qihoo.video.GestureDetectorController;
import com.qihoo.video.VerticalSeekBar;
import java.util.ArrayList;
import java.util.Iterator;

public class PlayerView extends BasePlayerControllView implements View.OnClickListener, View.OnTouchListener, AbsListView.OnScrollListener, AdapterView.OnItemClickListener, CompoundButton.OnCheckedChangeListener, SeekBar.OnSeekBarChangeListener, QihooMediaPlayer.OnSeekCompleteListener, GestureDetectorController.IGestureListener, VerticalSeekBar.OnSeekBarChangeListener {
    public static final int BUFFER_END = -3;
    public static final int DELAY_HIDE_CONTROL = 1;
    private static final int DELAY_HIDE_TIME = 5000;
    protected static int DELAY_HIDE_TOAST_TIME = DELAY_HIDE_TIME;
    private static final int PLAY_NEXT_INTERVAL = 8000;
    private static final int PROGRESS_INIT = -1;
    public static final int TOAST_QUAITY_ANIMATION = 3;
    public static final int VOLUME_TIMES = 10;
    private int SLIDE_DISTANCE = 600000;
    private GestureDetectorController gestureController;
    /* access modifiers changed from: private */
    public boolean isDraw;
    protected boolean isLoading = false;
    private boolean isMove;
    private boolean isPlayEndToast;
    private boolean isSeeking;
    protected boolean isSendQuaityAnimation;
    protected boolean isVideoBuffer;
    protected int lastBuffer;
    protected CheckBox mAnthologyCheckBox;
    private ListView mAnthologyListView;
    /* access modifiers changed from: private */
    public AudioManager mAudioManager = null;
    private ImageView mBackImage;
    protected View mBottomView;
    protected AnimationDrawable mBufferAnim;
    protected ImageView mBufferImage;
    private TextView mBufferTextView;
    protected View mBufferView;
    private ArrayList<CheckBox> mCheckBoxList = new ArrayList<>();
    protected TextView mCurrentPlayTimeTextView = null;
    protected TextView mDragProgressTextView;
    private TextView mDragText;
    private boolean mFromUser;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    PlayerView.this.hide();
                    return;
                case 2:
                default:
                    return;
                case 3:
                    PlayerView.this.toastSelectQuality();
                    return;
            }
        }
    };
    private boolean mIsAutoBrightness = false;
    protected CheckBox mLightCheckBox;
    private VerticalSeekBar mLightSeekBar;
    protected TextView mLiveSourceView;
    private ImageView mPauseImage;
    private int mPlayDuration;
    private IMediaPlayerController mPlayer;
    protected CheckBox mPlayerCheckBox;
    private PlayerInfo mPlayerInfo = null;
    protected IPlayerViewListener mPlayerViewListener;
    private int mProgress = 0;
    protected SeekBar mProgressSeekBar;
    protected CheckBox mQualityCheckBox;
    private PopupWindow mQualityPopupWindow;
    private int mScrollProgress = -1;
    private TextView mSourceTextView;
    private TextView mTitleTextView;
    protected View mTopView;
    private CheckBox mVolumeCheckBox;
    /* access modifiers changed from: private */
    public VerticalSeekBar mVolumeSeekBar;
    private ListView qualityListView;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ("android.intent.action.HEADSET_PLUG".equals(intent.getAction())) {
                int intExtra = intent.getIntExtra("state", -1);
                if ((intExtra == 0 || intExtra == 1) && PlayerView.this.mAudioManager != null) {
                    PlayerView.this.mHandler.postDelayed(new Runnable() {
                        public void run() {
                            if (PlayerView.this.mVolumeSeekBar != null) {
                                PlayerView.this.mVolumeSeekBar.setProgress(PlayerView.this.mAudioManager.getStreamVolume(3) * 10);
                            }
                        }
                    }, 50);
                }
            }
        }
    };
    private int systemLightness = -1;

    public PlayerView(Context context) {
        super(context);
        init(context);
    }

    public PlayerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    public PlayerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init(context);
    }

    private void chageDragText(int i, AbsSeekBar absSeekBar) {
        this.mDragText.setText(PlayerViewUtils.changeDoubleToPercent(((double) i) / ((double) absSeekBar.getMax())));
    }

    private void changeBufferViewText(int i) {
        if (i == 0) {
            this.mBufferTextView.setText(this.mContext.getString(R.string.video_loading));
        } else {
            this.mBufferTextView.setText(String.valueOf(this.mContext.getString(R.string.already_download)) + "  " + this.lastBuffer + "%");
        }
    }

    private void checkBufferView(int i) {
        if ((this.mPlayerInfo != null && this.mPlayerInfo.getIsLocalFile()) || !this.isVideoBuffer) {
            return;
        }
        if (i == 0) {
            this.mBufferView.setVisibility(8);
            return;
        }
        this.mBufferView.setVisibility(0);
        this.mBufferAnim.start();
    }

    private void clickQualityCheckBox(CompoundButton compoundButton, boolean z) {
        if (!z) {
            dismissQualityPopup();
        } else if (!isShowCheckBox(compoundButton)) {
            mutexCheckBox((CheckBox) compoundButton);
            setQualityCheckBoxAnimation(false, null);
            showQualityPopWindow(compoundButton);
        }
    }

    private PopupWindow createQualityPopupWindow() {
        if (this.qualityListView == null) {
            return null;
        }
        if (this.mQualityPopupWindow == null) {
            this.mQualityPopupWindow = new PopupWindow(this.qualityListView, this.mContext.getResources().getDimensionPixelSize(R.dimen.player_select_source_popupwindow), (getHeight() - this.mTopView.getHeight()) - this.mBottomView.getHeight());
            this.mQualityPopupWindow.setOutsideTouchable(true);
        }
        return this.mQualityPopupWindow;
    }

    private void dismissQualityPopup() {
        if (this.mQualityPopupWindow != null) {
            try {
                this.mQualityPopupWindow.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private int getMoveProgress(AbsSeekBar absSeekBar, float f) {
        int height = (int) ((f / ((float) absSeekBar.getHeight())) * ((float) absSeekBar.getMax()));
        if (Math.abs(height) < 1) {
            return -1;
        }
        int max = Math.max(0, Math.min(absSeekBar.getMax(), height + absSeekBar.getProgress()));
        absSeekBar.setProgress(max);
        chageDragText(max, absSeekBar);
        return max;
    }

    private void hideBufferView() {
        this.mBufferView.setVisibility(8);
        this.mBufferAnim.stop();
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.player_layout, this);
        initViews();
        if (this.mContext instanceof Activity) {
            ((Activity) this.mContext).getWindow().addFlags(512);
        }
        setOnTouchListener(this);
        this.gestureController = new GestureDetectorController(this.mContext);
        this.gestureController.setGestureListener(this);
    }

    private void initAntholotyView(WebsiteInfo websiteInfo) {
        PlayerAnthologyAdapter playerAnthologyAdapter = new PlayerAnthologyAdapter(this.mContext);
        this.mAnthologyListView.setAdapter((ListAdapter) playerAnthologyAdapter);
        this.mAnthologyListView.setOnItemClickListener(this);
        this.mAnthologyListView.setOnScrollListener(this);
        this.mAnthologyListView.setSelection(this.mPlayerInfo.getPlayCount());
        playerAnthologyAdapter.setPlayIndex(this.mPlayerInfo.getPlayCount());
        playerAnthologyAdapter.setWebsiteInfo(websiteInfo);
        playerAnthologyAdapter.notifyDataSetChanged();
    }

    private void initControlLight() {
        this.systemLightness = LightnessControl.getSystemLightness(this.mContext);
        this.mIsAutoBrightness = LightnessControl.isAutoBrightness(this.mContext);
        if (this.mIsAutoBrightness) {
            LightnessControl.stopAutoBrightness(this.mContext);
        }
    }

    private void initData() {
        this.isSendQuaityAnimation = false;
        this.isPlayEndToast = false;
        this.isSeeking = false;
        setPlayProgressView(0);
        this.mPlayerCheckBox.setOnCheckedChangeListener(null);
        this.mPlayerCheckBox.setChecked(true);
        this.mPauseImage.setVisibility(8);
        this.mPlayerCheckBox.setOnCheckedChangeListener(this);
        this.mPlayDuration = 0;
        this.isVideoBuffer = false;
    }

    private void initViews() {
        this.mTopView = findViewById(R.id.player_title_control);
        this.mBottomView = findViewById(R.id.player_bottom_control);
        this.mBufferView = findViewById(R.id.loadingPage);
        this.mTitleTextView = (TextView) findViewById(R.id.playervideotitle);
        this.mTitleTextView.setOnClickListener(this);
        this.mTitleTextView.setOnTouchListener(this);
        this.mBackImage = (ImageView) findViewById(R.id.playerBackImage);
        this.mBackImage.setOnTouchListener(this);
        this.mBackImage.setOnClickListener(this);
        this.mLiveSourceView = (TextView) findViewById(R.id.live_source_view);
        this.mLiveSourceView.setOnClickListener(this);
        this.mLiveSourceView.setOnTouchListener(this);
        this.mAnthologyListView = (ListView) findViewById(R.id.anthology_list);
        this.mLightSeekBar = (VerticalSeekBar) findViewById(R.id.playerLightSeekBar);
        this.mVolumeSeekBar = (VerticalSeekBar) findViewById(R.id.player_Volume_seekBar);
        this.mAnthologyCheckBox = (CheckBox) findViewById(R.id.playerbuttonAnthology);
        this.mQualityCheckBox = (CheckBox) findViewById(R.id.selectedQuality);
        this.mLightCheckBox = (CheckBox) findViewById(R.id.playerlightbutton);
        this.mLightCheckBox.setOnCheckedChangeListener(this);
        this.mPlayerCheckBox = (CheckBox) findViewById(R.id.playerplaybutton);
        this.mVolumeCheckBox = (CheckBox) findViewById(R.id.playervolumebutton);
        this.mVolumeCheckBox.setOnCheckedChangeListener(this);
        this.mProgressSeekBar = (SeekBar) findViewById(R.id.playerplayprogressseekbar);
        this.mDragProgressTextView = (TextView) findViewById(R.id.dragProgressTextView);
        this.mDragText = (TextView) findViewById(R.id.dragVolumeText);
        this.mPauseImage = (ImageView) findViewById(R.id.pauseImage);
        this.mBufferImage = (ImageView) findViewById(R.id.playLoadingImage);
        this.mBufferAnim = (AnimationDrawable) this.mBufferImage.getDrawable();
        this.mBufferTextView = (TextView) findViewById(R.id.bufferTextView);
        this.mSourceTextView = (TextView) findViewById(R.id.sourceTextView);
        this.mLightSeekBar.setProgress(LightnessControl.getLightness(this.mContext));
        this.mPlayerCheckBox.setOnCheckedChangeListener(this);
        this.mVolumeSeekBar.setOnSeekBarChangeListener(this);
        this.mLightSeekBar.setOnSeekBarChangeListener(this);
        this.mProgressSeekBar.setOnSeekBarChangeListener(this);
        this.mPauseImage.setOnClickListener(this);
        this.mQualityCheckBox.setOnCheckedChangeListener(this);
        this.mAudioManager = (AudioManager) this.mContext.getSystemService("audio");
        PlayerViewUtils.requestAudioFocus(this.mAudioManager, this.mContext);
        this.mVolumeSeekBar.setMax(this.mAudioManager.getStreamMaxVolume(3) * 10);
        this.mVolumeSeekBar.setProgress(this.mAudioManager.getStreamVolume(3) * 10);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.HEADSET_PLUG");
        this.mContext.registerReceiver(this.receiver, intentFilter);
        this.mCurrentPlayTimeTextView = (TextView) findViewById(R.id.playercurrentplaytime);
        getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                PlayerView.this.isDraw = true;
                PlayerView.this.getViewTreeObserver().removeOnPreDrawListener(this);
                PlayerView.this.onViewPreDraw();
                return true;
            }
        });
    }

    private boolean isShowCheckBox(CompoundButton compoundButton) {
        if (isShow()) {
            return false;
        }
        compoundButton.setChecked(false);
        return true;
    }

    private void isShowWindowTitle(boolean z) {
        if (this.mContext instanceof Activity) {
            if (z) {
                ((Activity) this.mContext).getWindow().clearFlags(1024);
            } else {
                ((Activity) this.mContext).getWindow().addFlags(1024);
            }
        }
    }

    private void playLastToast(int i, int i2) {
        if (this.mPlayerInfo != null && this.mPlayerInfo.getVideoWebSite() != null && !this.mPlayerInfo.getIsLocalFile()) {
            if (!this.isPlayEndToast && i > 0 && i2 > 0 && i > i2 - 8000) {
                byte catlog = this.mPlayerInfo.getCatlog();
                WebsiteInfo selectedWebsiteInfo = this.mPlayerInfo.getVideoWebSite().getSelectedWebsiteInfo();
                if (selectedWebsiteInfo != null && ((catlog == 2 || catlog == 4) && ((long) (this.mPlayerInfo.getPlayCount() + 1)) < selectedWebsiteInfo.getUpdatedInfo())) {
                    Toast.makeText(this.mContext, this.mContext.getString(R.string.play_next_tips), 0).show();
                }
            }
            this.isPlayEndToast = true;
        }
    }

    private boolean pointInControlView(float f, float f2) {
        return PlayerViewUtils.isPointInsideView(f, f2, this.mTopView) || PlayerViewUtils.isPointInsideView(f, f2, this.mBottomView) || (this.mAnthologyListView.getVisibility() == 0 && PlayerViewUtils.isPointInsideView(f, f2, this.mAnthologyListView)) || (this.mVolumeSeekBar.getVisibility() == 0 && PlayerViewUtils.isPointInsideView(f, f2, this.mVolumeSeekBar));
    }

    private void pressTitle(boolean z) {
        this.mTitleTextView.setPressed(z);
        this.mBackImage.setPressed(z);
        this.mLiveSourceView.setPressed(z);
    }

    private void refreshVolumeImage(int i) {
        if (i <= 0) {
            this.mVolumeCheckBox.setButtonDrawable(R.drawable.player_mute_sound_selector);
            PlayerViewUtils.setTextTopDrawables(this.mDragText, R.drawable.mute, this.mContext);
            return;
        }
        this.mVolumeCheckBox.setButtonDrawable(R.drawable.player_sound_selector);
        PlayerViewUtils.setTextTopDrawables(this.mDragText, R.drawable.nonmute, this.mContext);
    }

    private void setDragImageVisibility(int i) {
        this.mDragText.setVisibility(i);
        checkBufferView(i);
    }

    private void setSourceTextView(String str) {
        String string = this.mContext.getString(R.string.video_source);
        this.mSourceTextView.setText(TextUtils.isEmpty(str) ? String.valueOf(string) + this.mContext.getString(R.string.other_source) : String.valueOf(string) + str);
    }

    private void setViewEnabled(boolean z) {
        this.mPlayerCheckBox.setEnabled(z);
    }

    private void showBufferView() {
        if (this.mPlayerInfo != null && this.mPlayerInfo.getIsLocalFile()) {
            return;
        }
        if (this.mDragText.getVisibility() == 0) {
            this.mBufferView.setVisibility(8);
        } else if ((!(this.mBufferImage.getDrawable() instanceof AnimationDrawable) || this.mBufferView.getVisibility() != 0) && this.mPauseImage.getVisibility() != 0) {
            this.mBufferView.setVisibility(0);
            this.mBufferImage.setImageDrawable(this.mBufferAnim);
            this.mBufferAnim.start();
        }
    }

    private void showQualityPopWindow(View view) {
        if (!(this.mContext instanceof Activity) || !((Activity) this.mContext).isFinishing()) {
            if (this.mQualityPopupWindow == null) {
                this.mQualityPopupWindow = createQualityPopupWindow();
            }
            try {
                ListView listView = (ListView) this.mQualityPopupWindow.getContentView();
                this.mQualityPopupWindow.showAsDropDown(this.mTopView, getWidth() - this.mQualityPopupWindow.getWidth(), 0);
                ((VideoSourceAdapter) listView.getAdapter()).notifyDataSetChanged();
                listView.setSelection(getSelectSourceIndex());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startDelayHide() {
        this.mHandler.sendEmptyMessageDelayed(1, 5000);
    }

    private void stopDelayHide() {
        this.mHandler.removeMessages(1);
    }

    /* access modifiers changed from: private */
    public void toastSelectQuality() {
        toastSelectQuality(R.string.select_play_source_to_nomal);
    }

    private void updateProgressTextView(int i) {
        this.mDragProgressTextView.setText(String.valueOf(DateUtil.formatVideoDuration(i)) + "/" + DateUtil.formatVideoDuration(this.mProgressSeekBar.getMax()));
    }

    public void changePlayError() {
        show();
        if (this.mAnthologyCheckBox != null) {
            this.mAnthologyCheckBox.setEnabled(true);
        }
        this.mQualityCheckBox.setEnabled(true);
        setQualityText();
        this.mBufferTextView.setText(R.string.qvod_play_error);
        showBufferView();
        if (this.mQualityPopupWindow != null && this.mQualityPopupWindow.isShowing()) {
            ((BaseAdapter) ((ListView) this.mQualityPopupWindow.getContentView()).getAdapter()).notifyDataSetChanged();
        }
        stopDelayHide();
    }

    /* access modifiers changed from: protected */
    public void changePlaySource() {
        if (this.mPlayer != null) {
            this.mPlayer.reset();
        }
        showPrepareView();
    }

    /* access modifiers changed from: protected */
    public VideoSourceAdapter createQualityAdapter() {
        return new VideoSourceAdapter(this.mContext, true);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent);
    }

    /* access modifiers changed from: protected */
    public int getSelectSourceIndex() {
        if (this.mPlayerInfo == null) {
            return 0;
        }
        return this.mPlayerInfo.getVideoWebSite().getSelectedIndex();
    }

    public void hide() {
        isShowWindowTitle(false);
        this.mTopView.setVisibility(4);
        this.mBottomView.setVisibility(4);
        setQualityCheckBoxAnimation(false, null);
        setListChecked(false);
        stopDelayHide();
    }

    public void hidePrepareView() {
        Log.e("PlayerView", "hidePrepareView() isLoading: " + this.isLoading);
        if (this.isLoading) {
            setViewEnabled(true);
            this.isLoading = false;
            setBackgroundColor(this.mContext.getResources().getColor(17170445));
            hideBufferView();
            this.mSourceTextView.setText("");
            this.mProgressSeekBar.setEnabled(true);
            show();
        }
    }

    /* access modifiers changed from: protected */
    public void initQualityView(Object[] objArr) {
        if (this.mQualityPopupWindow != null) {
            VideoSourceAdapter videoSourceAdapter = (VideoSourceAdapter) ((ListView) this.mQualityPopupWindow.getContentView()).getAdapter();
            videoSourceAdapter.reset();
            videoSourceAdapter.addItems(objArr);
            return;
        }
        this.qualityListView = new PopwindowListView(this.mContext);
        this.qualityListView.setCacheColorHint(0);
        this.qualityListView.setDivider(new ColorDrawable(this.mContext.getResources().getColor(R.color.translucent_black_color)));
        this.qualityListView.setDividerHeight(this.mContext.getResources().getDimensionPixelSize(R.dimen.common_1dp));
        this.qualityListView.setFadingEdgeLength(0);
        this.qualityListView.setOnScrollListener(this);
        this.qualityListView.setOnItemClickListener(this);
        VideoSourceAdapter createQualityAdapter = createQualityAdapter();
        createQualityAdapter.addItems(objArr);
        this.qualityListView.setAdapter((ListAdapter) createQualityAdapter);
    }

    /* access modifiers changed from: protected */
    public boolean isShow() {
        return this.mTopView.getVisibility() == 0 && this.mBottomView.getVisibility() == 0;
    }

    public boolean isShowQuaityAnimation() {
        return !this.isSendQuaityAnimation && this.mPlayer.getCurrentPosition() > 0 && this.mPlayerInfo != null && this.mPlayerInfo.getVideoWebSite() != null && this.mPlayerInfo.getVideoWebSite().getCount() > 1;
    }

    /* access modifiers changed from: protected */
    public void mutexCheckBox(CheckBox checkBox) {
        Iterator<CheckBox> it = this.mCheckBoxList.iterator();
        boolean z = false;
        while (it.hasNext()) {
            CheckBox next = it.next();
            if (next == checkBox) {
                z = true;
            } else {
                next.setChecked(false);
            }
        }
        if (!z) {
            this.mCheckBoxList.add(checkBox);
        }
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        int id = compoundButton.getId();
        if (id == R.id.playerplaybutton) {
            if (z) {
                this.mPlayer.start();
                this.mPauseImage.setVisibility(8);
            } else {
                this.mPlayer.controllablePause();
                this.mPauseImage.setVisibility(0);
            }
            checkBufferView(this.mPauseImage.getVisibility());
        } else if (id == R.id.playervolumebutton) {
            if (!z) {
                this.mVolumeSeekBar.setVisibility(4);
            } else if (!isShowCheckBox(compoundButton)) {
                mutexCheckBox((CheckBox) compoundButton);
                this.mVolumeSeekBar.setVisibility(0);
            } else {
                return;
            }
        } else if (id == R.id.playerlightbutton) {
            if (!z) {
                this.mLightSeekBar.setVisibility(4);
            } else if (!isShowCheckBox(compoundButton)) {
                mutexCheckBox((CheckBox) compoundButton);
                this.mLightSeekBar.setVisibility(0);
            } else {
                return;
            }
        } else if (id == R.id.selectedQuality) {
            clickQualityCheckBox(compoundButton, z);
        } else if (id == R.id.playerbuttonAnthology) {
            if (!z) {
                this.mAnthologyListView.setVisibility(8);
            } else if (!isShowCheckBox(compoundButton)) {
                mutexCheckBox((CheckBox) compoundButton);
                this.mAnthologyListView.setVisibility(0);
                this.mAnthologyListView.setSelection(this.mPlayerInfo.getPlayCount());
            } else {
                return;
            }
        }
        stopDelayHide();
        startDelayHide();
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.playervideotitle || id == R.id.playerBackImage || id == R.id.live_source_view) {
            if (this.mPlayerViewListener != null && isShow()) {
                this.mPlayerViewListener.finishPlayer();
            }
        } else if (id == R.id.pauseImage) {
            this.mPlayerCheckBox.setChecked(true);
        }
    }

    public void onDestroy() {
        this.mContext.unregisterReceiver(this.receiver);
        PlayerViewUtils.abandonAudioFocus(this.mAudioManager, this.mContext);
        if (this.mQualityPopupWindow != null && (this.mContext instanceof Activity) && !((Activity) this.mContext).isFinishing()) {
            this.mQualityPopupWindow.dismiss();
            this.mQualityPopupWindow = null;
        }
    }

    public void onError(int i) {
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        if (adapterView.getId() == R.id.anthology_list && adapterView.getVisibility() == 0) {
            if (i != this.mPlayerInfo.getPlayCount()) {
                ((PlayerAnthologyAdapter) adapterView.getAdapter()).setPlayIndex(i);
                ((PlayerAnthologyAdapter) adapterView.getAdapter()).notifyDataSetChanged();
                changePlaySource();
                if (this.mPlayerViewListener != null) {
                    this.mPlayerViewListener.playVideo(i);
                }
            }
        } else if (view == null) {
            hide();
        } else {
            if (this.mPlayerViewListener != null && adapterView.getVisibility() == 0) {
                selectQuality(adapterView, i);
            }
            this.mQualityCheckBox.setChecked(false);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 24:
                int progress = this.mVolumeSeekBar.getProgress() + (this.mVolumeSeekBar.getMax() / 10);
                VerticalSeekBar verticalSeekBar = this.mVolumeSeekBar;
                if (progress > this.mVolumeSeekBar.getMax()) {
                    progress = this.mVolumeSeekBar.getMax();
                }
                verticalSeekBar.setProgress(progress);
                break;
            case 25:
                int progress2 = this.mVolumeSeekBar.getProgress() - (this.mVolumeSeekBar.getMax() / 10);
                VerticalSeekBar verticalSeekBar2 = this.mVolumeSeekBar;
                if (progress2 < 0) {
                    progress2 = 0;
                }
                verticalSeekBar2.setProgress(progress2);
                break;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        this.mFromUser = z;
        if (z) {
            this.mProgress = i;
            this.mCurrentPlayTimeTextView.setText(DateUtil.formatVideoDuration(i));
        }
        playLastToast(i, seekBar.getMax());
    }

    public void onProgressChanged(VerticalSeekBar verticalSeekBar, int i, boolean z) {
        if (R.id.player_Volume_seekBar == verticalSeekBar.getId()) {
            if (this.mAudioManager != null) {
                int i2 = i / 10;
                if (i2 == 0 && i > 0) {
                    i2 = 1;
                }
                this.mAudioManager.setStreamVolume(3, i2, 0);
                refreshVolumeImage(i);
            }
        } else if (R.id.playerLightSeekBar == verticalSeekBar.getId()) {
            if (LightnessControl.getVideoLightness(this.mContext) == -1) {
                initControlLight();
            }
            LightnessControl.setAndSaveLightness(this.mContext, i);
        }
        stopDelayHide();
        startDelayHide();
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
    }

    public void onScrollBegin(GestureDetectorController.ScrollViewType scrollViewType) {
        this.isMove = true;
        if (isShow()) {
            hide();
        }
        if (scrollViewType == GestureDetectorController.ScrollViewType.VERTICAL_LEFT) {
            PlayerViewUtils.setTextTopDrawables(this.mDragText, R.drawable.light, this.mContext);
            setDragImageVisibility(0);
            chageDragText(this.mLightSeekBar.getProgress(), this.mLightSeekBar);
        } else if (scrollViewType == GestureDetectorController.ScrollViewType.VERTICAL_RIGHT) {
            refreshVolumeImage(this.mVolumeSeekBar.getProgress());
            setDragImageVisibility(0);
            chageDragText(this.mVolumeSeekBar.getProgress(), this.mVolumeSeekBar);
        } else if (scrollViewType == GestureDetectorController.ScrollViewType.HORIZONTAL) {
            this.mDragProgressTextView.setVisibility(0);
            this.mScrollProgress = -1;
        }
    }

    public void onScrollHorizontal(float f, float f2) {
        int max = Math.max(0, Math.min(this.mProgressSeekBar.getMax(), ((int) ((f2 / ((float) getWidth())) * ((float) (this.SLIDE_DISTANCE / 2)))) + this.mProgressSeekBar.getProgress()));
        this.mScrollProgress = max;
        updateProgressTextView(max);
    }

    public void onScrollLeft(float f, float f2) {
        getMoveProgress(this.mLightSeekBar, f);
    }

    public void onScrollRight(float f, float f2) {
        getMoveProgress(this.mVolumeSeekBar, f);
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        if (i == 0) {
            startDelayHide();
            return;
        }
        stopDelayHide();
        startDelayHide();
    }

    public void onSeekComplete(QihooMediaPlayer qihooMediaPlayer) {
        this.isSeeking = false;
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
        stopDelayHide();
    }

    public void onStartTrackingTouch(VerticalSeekBar verticalSeekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
        updateProgress(this.mProgress, this.mFromUser);
        startDelayHide();
    }

    public void onStopTrackingTouch(VerticalSeekBar verticalSeekBar) {
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        int id = view.getId();
        if (id == R.id.playerBackImage || id == R.id.playervideotitle || id == R.id.live_source_view) {
            pressTitle(true);
            if (motionEvent.getAction() != 1) {
                return true;
            }
            onClick(view);
            return true;
        }
        switch (motionEvent.getAction()) {
            case 1:
                if (!this.isMove) {
                    if (isShow()) {
                        if (!pointInControlView(motionEvent.getX(), motionEvent.getY())) {
                            hide();
                            break;
                        }
                    } else {
                        show();
                        break;
                    }
                } else {
                    this.isMove = false;
                    setDragImageVisibility(8);
                    this.mDragProgressTextView.setVisibility(8);
                    if (this.mScrollProgress > -1) {
                        setPlayProgressView(this.mScrollProgress);
                        updateProgress(this.mScrollProgress, true);
                        this.mScrollProgress = -1;
                        break;
                    }
                }
                break;
        }
        if (!this.mPlayerCheckBox.isChecked() || this.isLoading) {
            return true;
        }
        return this.gestureController.onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public void onViewPreDraw() {
        showBufferView();
    }

    public void pause() {
        if (this.mPlayerCheckBox != null) {
            this.mPlayerCheckBox.setChecked(false);
        }
    }

    /* access modifiers changed from: protected */
    public void selectQuality(AdapterView<?> adapterView, int i) {
        WebsiteInfo selectedWebsiteInfo = this.mPlayerInfo.getVideoWebSite().getSelectedWebsiteInfo();
        WebsiteInfo websiteInfo = (WebsiteInfo) adapterView.getAdapter().getItem(i);
        if (selectedWebsiteInfo == websiteInfo && websiteInfo.getStatus() != WebsiteStatus.STATUS_FAILED) {
            return;
        }
        if (((long) this.mPlayerInfo.getPlayCount()) <= websiteInfo.getUpdatedInfo()) {
            this.mQualityCheckBox.setText(websiteInfo.getWebsiteNameAndQuality());
            if (websiteInfo.getStatus() == WebsiteStatus.STATUS_FAILED) {
                websiteInfo.setStatus(WebsiteStatus.STATUS_LOADING);
            }
            changePlaySource();
            this.mPlayerViewListener.changeQualityVideo(selectedWebsiteInfo, websiteInfo);
            return;
        }
        Toast.makeText(this.mContext, "“" + websiteInfo.getWebsiteNameAndQuality() + "”" + this.mContext.getString(R.string.the_source_no_episode), 0).show();
    }

    public void setDuration(int i) {
        this.mPlayDuration = i;
        this.mProgressSeekBar.setMax(i);
        ((TextView) findViewById(R.id.playertotalplaytime)).setText(DateUtil.formatVideoDuration(i));
    }

    /* access modifiers changed from: protected */
    public void setListChecked(boolean z) {
        Iterator<CheckBox> it = this.mCheckBoxList.iterator();
        while (it.hasNext()) {
            it.next().setChecked(z);
        }
    }

    public void setMediaPlayer(IMediaPlayerController iMediaPlayerController) {
        this.mPlayer = iMediaPlayerController;
        this.mPlayer.setOnSeekCompleteListener(this);
    }

    /* access modifiers changed from: protected */
    public void setPlayProgressView(int i) {
        this.mProgressSeekBar.setProgress(i);
        this.mCurrentPlayTimeTextView.setText(DateUtil.formatVideoDuration(i));
    }

    public void setPlayViewListener(IPlayerViewListener iPlayerViewListener) {
        this.mPlayerViewListener = iPlayerViewListener;
    }

    public void setPlayerData(Object obj) {
        if (obj != null && (obj instanceof PlayerInfo)) {
            PlayerInfo playerInfo = (PlayerInfo) obj;
            this.mPlayerInfo = playerInfo;
            setSourceTextView(playerInfo.getPlaySource());
            VideoWebSite videoWebSite = playerInfo.getVideoWebSite();
            if (videoWebSite == null || playerInfo.getIsLocalFile()) {
                this.mQualityCheckBox.setVisibility(8);
            } else {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(videoWebSite.getWebsites());
                if (this.mPlayerInfo.getPlayCount() > 0 && videoWebSite.getWebsites() != null && videoWebSite.getWebsites().size() > 0) {
                    for (int size = arrayList.size(); size > 0; size--) {
                        WebsiteInfo websiteInfo = (WebsiteInfo) arrayList.get(size - 1);
                        if (websiteInfo.getUpdatedInfo() <= ((long) this.mPlayerInfo.getPlayCount())) {
                            arrayList.remove(websiteInfo);
                        }
                    }
                }
                initQualityView(arrayList.toArray());
                this.mQualityCheckBox.setText(videoWebSite.getSelectedWebsiteInfo().getWebsiteNameAndQuality());
                this.mQualityCheckBox.setOnCheckedChangeListener(this);
                this.mQualityCheckBox.setVisibility(0);
            }
            if ((playerInfo.getCatlog() == 2 || playerInfo.getCatlog() == 4) && videoWebSite != null) {
                initAntholotyView(videoWebSite.getSelectedWebsiteInfo());
                this.mAnthologyCheckBox.setOnCheckedChangeListener(this);
                this.mAnthologyCheckBox.setVisibility(0);
            } else {
                this.mAnthologyCheckBox.setVisibility(8);
            }
            setTitle(playerInfo.getVideoTitle());
        }
    }

    /* access modifiers changed from: protected */
    public void setQualityCheckBoxAnimation(boolean z, Animation animation) {
        if (this.mQualityCheckBox.getAnimation() != animation) {
            int paddingBottom = this.mQualityCheckBox.getPaddingBottom();
            int paddingTop = this.mQualityCheckBox.getPaddingTop();
            int paddingRight = this.mQualityCheckBox.getPaddingRight();
            int paddingLeft = this.mQualityCheckBox.getPaddingLeft();
            if (z) {
                this.mQualityCheckBox.setBackgroundResource(R.drawable.player_panel_btton_flash);
                this.mQualityCheckBox.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
                this.mQualityCheckBox.startAnimation(animation);
                return;
            }
            this.mQualityCheckBox.clearAnimation();
            this.mQualityCheckBox.setBackgroundResource(R.drawable.player_panel_btton_selector);
            this.mQualityCheckBox.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        }
    }

    /* access modifiers changed from: protected */
    public void setQualityText() {
        this.mQualityCheckBox.setChecked(true);
        this.mQualityCheckBox.setText(this.mPlayerInfo.getVideoWebSite().getSelectedWebsiteInfo().getWebsiteNameAndQuality());
    }

    public void setTitle(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.mTitleTextView.setText(PlayerViewUtils.getNewString(Html.fromHtml(str).toString()));
        }
    }

    public void show() {
        pressTitle(false);
        isShowWindowTitle(true);
        this.mTopView.setVisibility(0);
        this.mBottomView.setVisibility(0);
        startDelayHide();
    }

    public void showNetworkErrorView() {
        this.isVideoBuffer = true;
        this.mBufferAnim.stop();
        this.mBufferImage.setImageResource(R.drawable.play_error_icon);
        this.mBufferTextView.setText(this.mContext.getResources().getString(R.string.network_unKnow));
        this.mBufferView.setVisibility(this.mPauseImage.getVisibility() == 0 ? 8 : 0);
        this.mHandler.removeMessages(3);
    }

    public void showPrepareView() {
        setViewEnabled(false);
        this.mHandler.removeMessages(3);
        this.mProgressSeekBar.setEnabled(false);
        initData();
        this.isLoading = true;
        if (this.isDraw) {
            this.mBufferView.setVisibility(0);
            this.mBufferImage.setImageDrawable(this.mBufferAnim);
            this.mBufferAnim.start();
        }
        setBackgroundColor(this.mContext.getResources().getColor(R.color.player_background_color));
        changeBufferViewText(0);
        if (this.mPlayerInfo != null) {
            setSourceTextView(this.mPlayerInfo.getPlaySource());
        }
    }

    public void start() {
        if (this.mPlayerCheckBox != null) {
            hide();
            if (this.mPlayerCheckBox.isChecked()) {
                this.mPlayer.start();
            } else {
                this.mPlayerCheckBox.setChecked(true);
            }
        }
    }

    public void startControlLight() {
        int videoLightness = LightnessControl.getVideoLightness(this.mContext);
        if (videoLightness != -1) {
            initControlLight();
            LightnessControl.setLightness(this.mContext, videoLightness);
        }
    }

    public void stopControlLight() {
        if (this.mIsAutoBrightness) {
            LightnessControl.startAutoBrightness(this.mContext);
        }
        if (this.systemLightness > -1) {
            LightnessControl.setSystemLisghtness(this.mContext, this.systemLightness);
        }
    }

    /* access modifiers changed from: protected */
    public void toastSelectQuality(int i) {
        this.isSendQuaityAnimation = true;
        Animation loadAnimation = AnimationUtils.loadAnimation(this.mContext, R.anim.animation_alpha);
        loadAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                PlayerView.this.setQualityCheckBoxAnimation(false, null);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        stopDelayHide();
        show();
        setQualityCheckBoxAnimation(true, loadAnimation);
        if (!(this.mContext instanceof Activity) || !((Activity) this.mContext).isFinishing()) {
            Toast.makeText(this.mContext, this.mContext.getResources().getString(i), 0).show();
        }
    }

    public void updateBuffer(int i) {
        this.mSourceTextView.setText("");
        if (i >= 100) {
            this.isVideoBuffer = false;
            hideBufferView();
            if (this.mHandler.hasMessages(3)) {
                this.mHandler.removeMessages(3);
            }
            this.lastBuffer = -3;
        } else if (i == 0) {
            this.lastBuffer = 0;
            this.isVideoBuffer = true;
            this.mBufferTextView.setText(R.string.video_loading);
            changeBufferViewText(0);
            showBufferView();
            if (isShowQuaityAnimation() && !this.mHandler.hasMessages(3)) {
                this.mHandler.sendEmptyMessageDelayed(3, (long) DELAY_HIDE_TOAST_TIME);
            }
        } else {
            this.isVideoBuffer = true;
            showBufferView();
            if (this.lastBuffer < i) {
                this.lastBuffer = i;
            }
            changeBufferViewText(i);
            if (isShowQuaityAnimation() && !this.mHandler.hasMessages(3)) {
                this.mHandler.sendEmptyMessageDelayed(3, (long) DELAY_HIDE_TOAST_TIME);
            }
        }
    }

    public void updatePlayProgress(int i) {
        if (!this.isSeeking && i > 0 && this.mProgressSeekBar != null && this.mPlayDuration > 0) {
            setPlayProgressView(i);
        }
    }

    /* access modifiers changed from: protected */
    public void updateProgress(int i, boolean z) {
        if (z) {
            this.isSeeking = true;
            this.mPlayer.seekTo(i);
        }
        stopDelayHide();
        startDelayHide();
        this.mProgress = 0;
    }
}
