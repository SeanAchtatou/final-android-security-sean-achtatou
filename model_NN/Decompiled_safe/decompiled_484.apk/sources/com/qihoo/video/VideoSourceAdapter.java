package com.qihoo.video;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.qihoo.mediaplayer.R;

public class VideoSourceAdapter extends ListViewAdapter {
    protected boolean mIsPlay = false;

    class ViewHolder {
        ProgressBar loadingProgressBar;
        ImageView sourceImage;
        TextView sourceTextView;
        ImageView statusImage;

        ViewHolder() {
        }
    }

    public VideoSourceAdapter(Context context, boolean z) {
        super(context);
        this.mIsPlay = z;
    }

    /* access modifiers changed from: protected */
    public void OnScrapHeap(View view) {
    }

    public int getCount() {
        if (this.infos == null) {
            return 0;
        }
        return this.infos.size();
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        View inflate;
        if (view == null) {
            if (this.mIsPlay) {
                View inflate2 = LayoutInflater.from(this.mContext).inflate(R.layout.select_source_for_player_item_layout, (ViewGroup) null);
                inflate2.setBackgroundResource(R.drawable.antholoy_background_selector);
                inflate = inflate2;
            } else {
                inflate = LayoutInflater.from(this.mContext).inflate(R.layout.select_source_item_layout, (ViewGroup) null);
            }
            ViewHolder viewHolder2 = new ViewHolder();
            viewHolder2.sourceImage = (ImageView) inflate.findViewById(R.id.sourceImage);
            viewHolder2.sourceTextView = (TextView) inflate.findViewById(R.id.sourceTextView);
            viewHolder2.statusImage = (ImageView) inflate.findViewById(R.id.statusImage);
            viewHolder2.loadingProgressBar = (ProgressBar) inflate.findViewById(R.id.loadingProgressBar);
            viewHolder2.sourceTextView.setClickable(false);
            inflate.setTag(viewHolder2);
            view = inflate;
            viewHolder = viewHolder2;
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        WebsiteInfo websiteInfo = (WebsiteInfo) getItem(i);
        if (websiteInfo != null) {
            viewHolder.loadingProgressBar.setVisibility(8);
            viewHolder.sourceTextView.setText(websiteInfo.getWebsiteNameAndQuality());
            Log.d("meng", "getStatus" + websiteInfo.getStatus());
            if (websiteInfo.getStatus() == WebsiteStatus.STATUS_FAILED) {
                viewHolder.statusImage.setVisibility(0);
                viewHolder.statusImage.setBackgroundResource(websiteInfo.getDrawableWithWebsiteStatus());
                viewHolder.sourceTextView.setTextColor(this.mContext.getResources().getColor(R.color.red));
            } else if (websiteInfo.getStatus() == WebsiteStatus.STATUS_LOADING) {
                viewHolder.statusImage.setVisibility(8);
                viewHolder.loadingProgressBar.setVisibility(0);
                viewHolder.sourceTextView.setTextColor((int) ViewCompat.MEASURED_STATE_MASK);
            } else if (websiteInfo.getStatus() == WebsiteStatus.STATUS_SELECTED) {
                viewHolder.statusImage.setVisibility(0);
                viewHolder.statusImage.setBackgroundResource(websiteInfo.getDrawableWithWebsiteStatus());
                viewHolder.sourceTextView.setTextColor(this.mContext.getResources().getColor(R.color.green));
            } else {
                Resources resources = this.mContext.getResources();
                ColorStateList colorStateList = resources.getColorStateList(R.color.item_title_color);
                if (this.mIsPlay) {
                    ColorStateList colorStateList2 = resources.getColorStateList(R.color.text_color_pressed);
                    if (colorStateList2 != null) {
                        viewHolder.sourceTextView.setTextColor(colorStateList2);
                    } else {
                        viewHolder.sourceTextView.setTextColor(colorStateList);
                    }
                } else {
                    viewHolder.sourceTextView.setTextColor(colorStateList);
                }
                viewHolder.statusImage.setVisibility(0);
                viewHolder.statusImage.setBackgroundResource(websiteInfo.getDrawableWithWebsiteStatus());
            }
            int icon = websiteInfo.getIcon();
            if (icon > 0) {
                viewHolder.sourceImage.setImageResource(icon);
            }
        }
        return view;
    }
}
