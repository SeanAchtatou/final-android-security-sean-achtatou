package com.qihoo.video;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import java.text.NumberFormat;

public class PlayerViewUtils {

    public enum NetConnectStatus {
        DISCONNECTION,
        CONNECTING,
        CONNECTION_WIFI,
        CONNECTION_3G
    }

    public static void abandonAudioFocus(AudioManager audioManager, Context context) {
        if (audioManager != null) {
            audioManager.abandonAudioFocus(null);
        } else {
            ((AudioManager) context.getSystemService("audio")).abandonAudioFocus(null);
        }
    }

    public static String changeDoubleToPercent(double d) {
        NumberFormat percentInstance = NumberFormat.getPercentInstance();
        percentInstance.setMaximumFractionDigits(0);
        return percentInstance.format(d);
    }

    public static NetConnectStatus getNetConnectStatus(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return NetConnectStatus.DISCONNECTION;
        }
        boolean isConnected = activeNetworkInfo.isConnected();
        boolean z = !activeNetworkInfo.isConnectedOrConnecting();
        Log.i("jy", "Mobo: isDisconnected: " + z + ", isConnected:" + isConnected + ", type: " + activeNetworkInfo.getType());
        return z ? NetConnectStatus.DISCONNECTION : (z || isConnected) ? activeNetworkInfo.getType() == 0 ? NetConnectStatus.CONNECTION_3G : NetConnectStatus.CONNECTION_WIFI : NetConnectStatus.CONNECTING;
    }

    public static NetConnectStatus getNetConnectStatusFromIntent(Intent intent) {
        if (intent == null) {
            return NetConnectStatus.DISCONNECTION;
        }
        NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra("networkInfo");
        if (networkInfo == null) {
            return NetConnectStatus.DISCONNECTION;
        }
        boolean isConnected = networkInfo.isConnected();
        boolean z = !networkInfo.isConnectedOrConnecting();
        Log.i("jy", "Mobo: isDisconnected: " + z + ", isConnected:" + isConnected + ", type: " + networkInfo.getType());
        return z ? NetConnectStatus.DISCONNECTION : (z || isConnected) ? (networkInfo.getType() == 0 || networkInfo.getType() == 5) ? NetConnectStatus.CONNECTION_3G : NetConnectStatus.CONNECTION_WIFI : NetConnectStatus.CONNECTING;
    }

    public static String getNewString(String str) {
        int lastIndexOf = str.lastIndexOf(".");
        return lastIndexOf > 0 ? str.substring(0, lastIndexOf) : str;
    }

    public static boolean isPointInsideView(float f, float f2, View view) {
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        int i = iArr[0];
        int i2 = iArr[1];
        return f > ((float) i) && f < ((float) (i + view.getWidth())) && f2 > ((float) i2) && f2 < ((float) (i2 + view.getHeight()));
    }

    public static void requestAudioFocus(AudioManager audioManager, Context context) {
        if (audioManager != null) {
            audioManager.requestAudioFocus(null, 3, 1);
        } else {
            ((AudioManager) context.getSystemService("audio")).requestAudioFocus(null, 3, 1);
        }
    }

    public static void setTextTopDrawables(TextView textView, int i, Context context) {
        Drawable drawable = context.getResources().getDrawable(i);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        textView.setCompoundDrawables(null, drawable, null, null);
    }
}
