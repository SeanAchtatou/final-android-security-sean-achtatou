package com.qihoo.video;

import android.app.Activity;
import com.qihoo.video.httpservice.AsyncRequest;

public class ReportPlayErrorRequest extends AsyncRequest {
    public ReportPlayErrorRequest(Activity activity, String str, String str2) {
        super(activity, str, str2);
    }

    /* access modifiers changed from: protected */
    public VideoInfos doInBackground(Object... objArr) {
        Integer valueOf = Integer.valueOf(objArr.length >= 6 ? ((Integer) objArr[5]).intValue() : 0);
        Integer num = objArr.length >= 7 ? objArr[6] : 0;
        if (objArr.length >= 5) {
            Integer num2 = (Integer) objArr[0];
            Integer num3 = (Integer) objArr[1];
            Requests.reportPlayError(num2, Integer.valueOf(num3.intValue() == 0 ? 5 : num3.intValue()), (String) objArr[2], (String) objArr[3], (String) objArr[4], valueOf, num);
            if (isCancelled()) {
            }
        }
        return null;
    }
}
