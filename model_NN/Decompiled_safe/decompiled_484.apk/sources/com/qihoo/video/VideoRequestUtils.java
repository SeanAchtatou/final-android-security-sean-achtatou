package com.qihoo.video;

import android.app.Activity;
import android.content.Context;
import com.qihoo.mediaplayer.R;
import com.qihoo.video.httpservice.AsyncRequest;
import com.qihoo.video.httpservice.EpisodeSingleStreamRequest;
import com.qihoo.video.httpservice.EpisodeSingleZyRequest;
import java.lang.ref.SoftReference;

public enum VideoRequestUtils {
    INSTANCE;
    
    private boolean isCancel;
    AsyncRequest.OnRecivedDataListener mRecievedDataListener = new AsyncRequest.OnRecivedDataListener() {
        public void OnRecivedData(AsyncRequest asyncRequest, Object obj) {
            OnVideoRequestListener onVideoRequestListener;
            if (!(VideoRequestUtils.access$2(VideoRequestUtils.this) == null || VideoRequestUtils.access$3(VideoRequestUtils.this) || (onVideoRequestListener = (OnVideoRequestListener) VideoRequestUtils.access$2(VideoRequestUtils.this).get()) == null)) {
                if (obj == null || !(obj instanceof XstmInfo) || ((XstmInfo) obj).errorCode != 0) {
                    onVideoRequestListener.onRequestFailed(RequestError.TIMEOUT);
                } else {
                    onVideoRequestListener.onRequestSucess((XstmInfo) obj);
                }
            }
            VideoRequestUtils.access$4(VideoRequestUtils.this, null);
        }
    };
    private AsyncRequest mRequest = null;
    private SoftReference<OnVideoRequestListener> mSoftRequestListener;

    public interface OnVideoRequestListener {
        void onRequestFailed(RequestError requestError);

        void onRequestSucess(XstmInfo xstmInfo);
    }

    public enum RequestError {
        TIMEOUT,
        NONETWORK
    }

    public class VideoRequestParams {
        private String action = null;
        byte catalog = -1;
        String index = null;
        int playerSDK = 0;
        String qualityKey = null;
        private String requestSource = null;
        String siteKey = null;
        String videoId = null;
        String xstmUrl = null;
        private String zsParams = null;

        public VideoRequestParams() {
        }

        public VideoRequestParams(String str, String str2) {
            this.xstmUrl = str;
            this.qualityKey = str2;
        }

        public String getAction() {
            return this.action;
        }

        public byte getCatalog() {
            return this.catalog;
        }

        public String getIndex() {
            return this.index;
        }

        public int getPlayerSDK() {
            return this.playerSDK;
        }

        public String getQualityKey() {
            return this.qualityKey;
        }

        public String getRequestSource() {
            return this.requestSource;
        }

        public String getSiteKey() {
            return this.siteKey;
        }

        public String getVideoId() {
            return this.videoId;
        }

        public String getXstmUrl() {
            return this.xstmUrl;
        }

        public String getZsParams() {
            return this.zsParams;
        }

        public void parseToParams(PlayerInfo playerInfo) {
            this.videoId = playerInfo.getVideoId();
            this.catalog = playerInfo.getCatlog();
            this.index = this.catalog == 3 ? playerInfo.getVarietyIndex() : String.valueOf(playerInfo.getPlayCount());
            if (!(this.catalog == 0 || playerInfo.getVideoWebSite() == null)) {
                this.siteKey = playerInfo.getVideoWebSite().getSelectedWebsiteInfo().getWebsiteKey();
                this.qualityKey = playerInfo.getVideoWebSite().getSelectedWebsiteInfo().getQualityKey();
                this.playerSDK = playerInfo.getVideoWebSite().getSelectedWebsiteInfo().getPlayerSDK();
            }
            if (playerInfo.getVideoWebSite() == null || playerInfo.getVideoWebSite().getSelectedWebsiteInfo() == null) {
                this.xstmUrl = playerInfo.getXstmUrl();
            } else {
                this.xstmUrl = playerInfo.getVideoWebSite().getSelectedWebsiteInfo().getXstm();
            }
            this.zsParams = playerInfo.getZsParams();
            this.requestSource = playerInfo.getRequestSource();
        }

        public void setAction(String str) {
            this.action = str;
        }

        public void setCatalog(byte b2) {
            this.catalog = b2;
        }

        public void setIndex(String str) {
            this.index = str;
        }

        public void setQualityKey(String str) {
            this.qualityKey = str;
        }

        public void setRequestSource(String str) {
            this.requestSource = str;
        }

        public void setSiteKey(String str) {
            this.siteKey = str;
        }

        public void setVideoId(String str) {
            this.videoId = str;
        }

        public void setXstmUrl(String str) {
            this.xstmUrl = str;
        }

        public void setZsParams(String str) {
            this.zsParams = str;
        }
    }

    public void RequestData(VideoRequestParams videoRequestParams, OnVideoRequestListener onVideoRequestListener, Context context) {
        String str;
        String str2;
        if (onVideoRequestListener != null) {
            this.mSoftRequestListener = new SoftReference<>(onVideoRequestListener);
            if (context != null) {
                str2 = context.getResources().getString(R.string.play_tips);
                str = context.getResources().getString(R.string.wait_for_request);
            } else {
                str = null;
                str2 = null;
            }
            if (this.mRequest != null) {
                this.mRequest.cancel(true);
                this.mRequest = null;
            }
            switch (videoRequestParams.catalog) {
                case 0:
                case 1:
                    this.mRequest = new StreamUrlRequest((Activity) context, str2, str);
                    break;
                case 2:
                case 4:
                    this.mRequest = new EpisodeSingleStreamRequest((Activity) context, str2, str);
                    break;
                case 3:
                    this.mRequest = new EpisodeSingleZyRequest((Activity) context, str2, str);
                    break;
            }
            this.mRequest.setOnRecivedDataListener(this.mRecievedDataListener);
            this.mRequest.executeOnPoolExecutor(videoRequestParams);
        }
    }
}
