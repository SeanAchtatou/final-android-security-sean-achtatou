package com.qihoo.video;

import java.io.Serializable;
import java.util.ArrayList;

public class PlayerInfo implements Serializable {
    public static final int FROM_DETAIL_PAGE = 0;
    public static final int FROM_HTML_PAGE = 1;
    private static final long serialVersionUID = -684149940411377957L;
    private byte catlog = 0;
    private String coverUrl = null;
    private int fromPage = 0;
    private VideoWebSite info = null;
    private boolean isGoHomePage = false;
    private boolean isLocalfile = false;
    private boolean isShow3GAlart = false;
    private String jsonPlayData;
    private ArrayList<String> localMeidaFile = null;
    private int playCount = 0;
    private String playUrl = null;
    private int playUrlType = 0;
    private long playtimeStamp = 0;
    private String[] qvodUrls = null;
    private String refUrl = null;
    private String requestSource = null;
    private String varietyIndex = null;
    private String videoId = null;
    private String videoTitle = null;
    private String xstmUrl = null;
    private String zsParams = null;

    public byte getCatlog() {
        return this.catlog;
    }

    public String getCoverUrl() {
        return this.coverUrl;
    }

    public int getFromPage() {
        return this.fromPage;
    }

    public String getFromPageStr() {
        return this.fromPage == 1 ? "html5" : this.fromPage == 0 ? "qvod" : "";
    }

    public boolean getIsLocalFile() {
        return this.isLocalfile;
    }

    public boolean getIsShow3GAlart() {
        return this.isShow3GAlart;
    }

    public String getJsonPlayData() {
        return this.jsonPlayData;
    }

    public ArrayList<String> getLocalMeidaFile() {
        return this.localMeidaFile;
    }

    public int getPlayCount() {
        return this.playCount;
    }

    public String getPlaySource() {
        return this.info == null ? this.refUrl : this.info.getSelectedWebsiteInfo().getDefaultPlaylink();
    }

    public long getPlayTimeStamp() {
        return this.playtimeStamp;
    }

    public String getPlayUrl() {
        return this.playUrl;
    }

    public int getPlayUrlType() {
        return this.playUrlType;
    }

    public String getQvodUrls(int i) {
        if (this.qvodUrls == null || i >= this.qvodUrls.length) {
            return null;
        }
        return this.qvodUrls[i];
    }

    public String getRefUrl() {
        return this.refUrl;
    }

    public String getRequestSource() {
        return this.requestSource;
    }

    public String getVarietyIndex() {
        return this.varietyIndex;
    }

    public String getVideoId() {
        return this.videoId;
    }

    public String getVideoTitle() {
        return this.videoTitle;
    }

    public VideoWebSite getVideoWebSite() {
        return this.info;
    }

    public String getXstmUrl() {
        return this.xstmUrl;
    }

    public String getZsParams() {
        return this.zsParams;
    }

    public boolean isGoHomePage() {
        return this.isGoHomePage;
    }

    public void setCatlog(byte b2) {
        this.catlog = b2;
    }

    public void setCatlog(int i) {
        this.catlog = (byte) i;
    }

    public void setCoverUrl(String str) {
        this.coverUrl = str;
    }

    public void setFromPage(int i) {
        this.fromPage = i;
    }

    public void setGoHomePage(boolean z) {
        this.isGoHomePage = z;
    }

    public void setHasShown3GAlartBefore(boolean z) {
        this.isShow3GAlart = z;
    }

    public void setIsLocalFile(boolean z) {
        this.isLocalfile = z;
    }

    public void setJsonDataSource(String str) {
        this.jsonPlayData = str;
    }

    public void setLocalMeidaFile(ArrayList<String> arrayList) {
        this.localMeidaFile = arrayList;
    }

    public void setPlayCount(int i) {
        this.playCount = i;
    }

    public void setPlayTimeStamp(long j) {
        this.playtimeStamp = j;
    }

    public void setPlayUrl(String str) {
        this.playUrl = str;
    }

    public void setPlayUrlType(int i) {
        this.playUrlType = i;
    }

    public void setQvodUrls(String[] strArr) {
        this.qvodUrls = strArr;
    }

    public void setRefUrl(String str) {
        this.refUrl = str;
    }

    public void setRequestSource(String str) {
        this.requestSource = str;
    }

    public void setVarietyIndex(String str) {
        this.varietyIndex = str;
    }

    public void setVideoId(String str) {
        this.videoId = str;
    }

    public void setVideoTitle(String str) {
        this.videoTitle = str;
    }

    public void setVideoWebSite(VideoWebSite videoWebSite) {
        this.info = videoWebSite;
    }

    public void setXstmUrl(String str) {
        this.xstmUrl = str;
    }

    public void setZsParams(String str) {
        this.zsParams = str;
    }
}
