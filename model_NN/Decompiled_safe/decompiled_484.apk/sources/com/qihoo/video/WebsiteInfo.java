package com.qihoo.video;

import com.qihoo.mediaplayer.R;
import java.io.Serializable;

public class WebsiteInfo implements Serializable {
    public static final String QUALITY_HIGHT = "high";
    public static final String QUALITY_NORMAL = "normal";
    public static final int ThirdSDK = 1;
    private static final long serialVersionUID = 8666213577797958191L;
    private String defaultPlaylink;
    private int groupId;
    private int icon = -1;
    private int[] lost;
    private String mQualityKey = null;
    private int mQualityLevel = 0;
    private String mQualityName = null;
    private int playerSDK = 0;
    private String pubdate = null;
    private WebsiteStatus status = WebsiteStatus.STATUS_NOMAL;
    private long updatedInfo;
    private String websiteKey;
    private String websiteName;
    private String websiteNameAndQuality;
    private String xstm = null;

    public static boolean isLetvSource(String str) {
        return "leshi".equalsIgnoreCase(str);
    }

    public static boolean isThirdSDKPlay(int i) {
        return i == 1;
    }

    public String getDefaultPlaylink() {
        return this.defaultPlaylink;
    }

    public int getDrawableWithWebsiteStatus() {
        if (this.status == WebsiteStatus.STATUS_SELECTED) {
            return R.drawable.selected;
        }
        if (this.status == WebsiteStatus.STATUS_LOADING) {
            return R.drawable.loading;
        }
        if (this.status == WebsiteStatus.STATUS_FAILED) {
            return R.drawable.refresh_nomal;
        }
        return 0;
    }

    public int getGroupId() {
        return this.groupId;
    }

    public int getIcon() {
        return this.icon;
    }

    public int[] getLost() {
        return this.lost;
    }

    public int getPlayerSDK() {
        return this.playerSDK;
    }

    public String getPubdate() {
        return this.pubdate;
    }

    public String getQualityKey() {
        return this.mQualityKey;
    }

    public int getQualityLevel() {
        return this.mQualityLevel;
    }

    public String getQualityName() {
        return this.mQualityName;
    }

    public WebsiteStatus getStatus() {
        return this.status;
    }

    public long getUpdatedInfo() {
        return this.updatedInfo;
    }

    public String getWebsiteKey() {
        return this.websiteKey;
    }

    public String getWebsiteName() {
        return this.websiteName;
    }

    public String getWebsiteNameAndQuality() {
        return String.valueOf(this.websiteName) + "-" + this.mQualityName;
    }

    public String getXstm() {
        return this.xstm;
    }

    public void setDefaultPlaylink(String str) {
        this.defaultPlaylink = str;
    }

    public void setGroupId(int i) {
        this.groupId = i;
    }

    public void setIcon(int i) {
        this.icon = i;
    }

    public void setLost(int[] iArr) {
        this.lost = iArr;
    }

    public void setPlayerSDK(int i) {
        this.playerSDK = i;
    }

    public void setPubdate(String str) {
        this.pubdate = str;
    }

    public void setQualityKey(String str) {
        this.mQualityKey = str;
        if (QUALITY_NORMAL.equals(str)) {
            this.mQualityLevel = 0;
        } else if (QUALITY_HIGHT.equals(str)) {
            this.mQualityLevel = 1;
        } else if ("super".equals(str)) {
            this.mQualityLevel = 2;
        } else if ("super2".equals(str)) {
            this.mQualityLevel = 3;
        } else if ("real".equals(str)) {
            this.mQualityLevel = 4;
        }
    }

    public void setQualityName(String str) {
        this.mQualityName = str;
    }

    public void setStatus(WebsiteStatus websiteStatus) {
        setWebsiteNameAndQuality(this.websiteNameAndQuality);
        this.status = websiteStatus;
    }

    public void setUpdatedInfo(long j) {
        this.updatedInfo = j;
    }

    public void setWebsiteKey(String str) {
        this.websiteKey = str;
        setIcon(VideoSourceUtil.getVideoSourceResouceId(str));
    }

    public void setWebsiteName(String str) {
        this.websiteName = str;
    }

    public void setWebsiteNameAndQuality(String str) {
        this.websiteNameAndQuality = str;
    }

    public void setXstm(String str) {
        this.xstm = str;
    }

    public String toString() {
        return "WebsiteInfo [websiteName=" + this.websiteName + ", mQualityKey=" + this.mQualityKey + ", mQualityName=" + this.mQualityName + ", mQualityLevel=" + this.mQualityLevel + "]";
    }
}
