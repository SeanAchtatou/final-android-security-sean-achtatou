package com.qihoo.video;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import com.qihoo.http.base.HttpUtils;
import java.util.HashMap;

public class AppInfo {
    public static HashMap<String, String> getAppInfo(Context context) {
        HashMap<String, String> hashMap = new HashMap<>();
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            hashMap.put("id", Build.MODEL);
            hashMap.put("rom", Build.VERSION.RELEASE);
            hashMap.put("appName", context.getPackageName());
            hashMap.put("versionName", packageInfo.versionName);
            hashMap.put("versionCode", Integer.toString(packageInfo.versionCode));
        } catch (Exception e) {
        }
        return hashMap;
    }

    public static String getDeviceId(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }

    private static String getDeviceSerial() {
        try {
            Class<?> cls = Class.forName("android.os.SystemProperties");
            return (String) cls.getMethod("get", String.class).invoke(cls, "ro.serialno");
        } catch (Exception e) {
            return "";
        }
    }

    public static String getTokenM2(Context context) {
        String deviceId = getDeviceId(context);
        String string = Settings.System.getString(context.getContentResolver(), "android_id");
        return HttpUtils.MD5Encode(String.valueOf(deviceId) + string + getDeviceSerial());
    }

    public static boolean isDevicePlay(Context context) {
        return !Build.MODEL.equals("ZTE-T U880") || !Build.MANUFACTURER.equals("ZTE");
    }
}
