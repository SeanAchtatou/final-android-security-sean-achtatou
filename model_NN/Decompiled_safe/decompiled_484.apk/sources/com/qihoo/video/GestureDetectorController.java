package com.qihoo.video;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;

public class GestureDetectorController implements GestureDetector.OnGestureListener {
    private ScrollViewType currentType = ScrollViewType.NOTHING;
    private GestureDetector mGestureDetector = null;
    private IGestureListener mListener;
    private int mWidth;

    public interface IGestureListener {
        void onScrollBegin(ScrollViewType scrollViewType);

        void onScrollHorizontal(float f, float f2);

        void onScrollLeft(float f, float f2);

        void onScrollRight(float f, float f2);
    }

    public enum ScrollViewType {
        NOTHING,
        VERTICAL_RIGHT,
        VERTICAL_LEFT,
        HORIZONTAL
    }

    public GestureDetectorController(Context context) {
        this.mGestureDetector = new GestureDetector(context, this);
        this.mWidth = context.getResources().getDisplayMetrics().widthPixels;
    }

    public boolean onDown(MotionEvent motionEvent) {
        this.currentType = ScrollViewType.NOTHING;
        return true;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return true;
    }

    public void onLongPress(MotionEvent motionEvent) {
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        if (this.mListener == null || motionEvent == null || motionEvent2 == null) {
            return false;
        }
        if (this.currentType == ScrollViewType.NOTHING) {
            if (Math.abs(f2) > Math.abs(f)) {
                int i = this.mWidth / 3;
                this.currentType = motionEvent.getX() < ((float) i) ? ScrollViewType.VERTICAL_LEFT : motionEvent.getX() > ((float) (i * 2)) ? ScrollViewType.VERTICAL_RIGHT : ScrollViewType.NOTHING;
            } else {
                this.currentType = ScrollViewType.HORIZONTAL;
            }
            this.mListener.onScrollBegin(this.currentType);
            return false;
        } else if (this.currentType == ScrollViewType.VERTICAL_RIGHT) {
            this.mListener.onScrollRight(f2, motionEvent2.getY() - motionEvent.getY());
            return false;
        } else if (this.currentType == ScrollViewType.VERTICAL_LEFT) {
            this.mListener.onScrollLeft(f2, motionEvent2.getY() - motionEvent.getY());
            return false;
        } else if (this.currentType != ScrollViewType.HORIZONTAL) {
            return false;
        } else {
            this.mListener.onScrollHorizontal(f, motionEvent2.getX() - motionEvent.getX());
            return false;
        }
    }

    public void onShowPress(MotionEvent motionEvent) {
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.mGestureDetector.onTouchEvent(motionEvent);
    }

    public void setGestureListener(IGestureListener iGestureListener) {
        this.mListener = iGestureListener;
    }
}
