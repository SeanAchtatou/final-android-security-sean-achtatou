package com.qihoo.video;

import com.qihoo360.daily.activity.SearchActivity;
import org.json.JSONObject;

public class EpisodeZyInfo {
    public String coverUrl;
    public String desc;
    public String pubdate;
    public String title;
    public String url;
    public String xstm;

    public EpisodeZyInfo(JSONObject jSONObject) {
        this.coverUrl = jSONObject.optString("cover");
        this.title = jSONObject.optString("name");
        this.url = jSONObject.optString(SearchActivity.TAG_URL);
        this.desc = jSONObject.optString("desc");
        this.xstm = jSONObject.optString("xstm");
        this.pubdate = jSONObject.optString("pubdate");
    }

    public String getDescription() {
        return this.desc;
    }

    public String getImageUrl() {
        return this.coverUrl;
    }

    public String getTitle() {
        return this.title;
    }
}
