package com.qihoo.video.plugins;

import android.util.Log;
import com.qihoo.video.WebsiteInfo;
import com.qihoo360.daily.activity.SearchActivity;
import com.qihoo360.daily.model.ChannelType;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class XstmConverter {
    public static final int QUALITY_HIGH = 1;
    public static final int QUALITY_NORMAL = 0;
    public static final int QUALITY_REAL = 4;
    public static final int QUALITY_SUPER = 2;
    public static final int QUALITY_SUPER2 = 3;

    public class XstmHackParam {
        public String packageurl;
        public int packageversion;
        public XstmRequest request;

        public static XstmHackParam fromString(String str) {
            XstmHackParam xstmHackParam = new XstmHackParam();
            if (str == null) {
                return null;
            }
            xstmHackParam.request = new XstmRequest();
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("AppParser");
            if (optString == null || optString.trim().length() == 0) {
                return null;
            }
            JSONObject jSONObject2 = new JSONObject(optString);
            boolean optBoolean = jSONObject2.optBoolean(ChannelType.TYPE_CHANNEL_LOCAL);
            xstmHackParam.packageurl = jSONObject2.optString("packageurl");
            xstmHackParam.packageversion = jSONObject2.optInt("packageversion");
            Log.d("lcy", "local = " + optBoolean + ", packageurl = " + xstmHackParam.packageurl + ", packageversion = " + xstmHackParam.packageversion);
            if (!optBoolean) {
                return null;
            }
            xstmHackParam.request.url = jSONObject.optString("ref");
            xstmHackParam.request.website = jSONObject.optString("site");
            if (xstmHackParam.request.website.equals("iqiyi")) {
                xstmHackParam.request.website = "qiyi";
            }
            Log.d("lcy", "url = " + xstmHackParam.request.url + ", website = " + xstmHackParam.request.website);
            JSONObject jSONObject3 = new JSONObject(jSONObject.optString("header"));
            Iterator<String> keys = jSONObject3.keys();
            ArrayList arrayList = new ArrayList();
            while (keys.hasNext()) {
                String valueOf = String.valueOf(keys.next());
                arrayList.add(valueOf);
                Log.d("lcy", "key = " + valueOf);
            }
            XstmRequest.Header[] headerArr = new XstmRequest.Header[arrayList.size()];
            Log.d("lcy", "key size = " + arrayList.size());
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= headerArr.length) {
                    xstmHackParam.request.headers = headerArr;
                    Log.d("lcy", "request.quality = " + xstmHackParam.request.quality);
                    return xstmHackParam;
                }
                XstmRequest.Header header = new XstmRequest.Header();
                header.key = (String) arrayList.get(i2);
                header.value = jSONObject3.optString((String) arrayList.get(i2));
                Log.d("lcy", "value = " + header.value);
                headerArr[i2] = header;
                i = i2 + 1;
            }
        }
    }

    public class XstmRequest {
        public Header[] headers;
        public int quality;
        public String url;
        public String website;

        public class Header {
            public String key;
            public String value;
        }

        public static XstmRequest fromString(String str) {
            XstmRequest xstmRequest = new XstmRequest();
            try {
                JSONObject jSONObject = new JSONObject(str);
                xstmRequest.website = jSONObject.getString("website");
                xstmRequest.url = jSONObject.getString(SearchActivity.TAG_URL);
                xstmRequest.quality = jSONObject.getInt("quality");
                JSONArray jSONArray = jSONObject.getJSONArray("header");
                if (jSONArray != null && jSONArray.length() != 0) {
                    xstmRequest.headers = new Header[jSONArray.length()];
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 >= jSONArray.length()) {
                            break;
                        }
                        xstmRequest.headers[i2] = new Header();
                        JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                        Iterator<String> keys = jSONObject2.keys();
                        if (keys.hasNext()) {
                            String next = keys.next();
                            String string = jSONObject2.getString(next);
                            xstmRequest.headers[i2] = new Header();
                            xstmRequest.headers[i2].key = next;
                            xstmRequest.headers[i2].value = string;
                        }
                        i = i2 + 1;
                    }
                }
                return xstmRequest;
            } catch (JSONException e) {
                return null;
            }
        }

        public String toString() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("website", this.website);
                jSONObject.put(SearchActivity.TAG_URL, this.url);
                jSONObject.put("quality", this.quality);
                if (this.headers != null) {
                    JSONArray jSONArray = new JSONArray();
                    for (int i = 0; i < this.headers.length; i++) {
                        JSONObject jSONObject2 = new JSONObject();
                        jSONObject2.put(this.headers[i].key, this.headers[i].value);
                        jSONArray.put(jSONObject2);
                    }
                    jSONObject.put("header", jSONArray);
                }
                return jSONObject.toString();
            } catch (JSONException e) {
                return null;
            }
        }
    }

    public class XstmResponse {
        public String result;
        public String xstm;

        public static XstmResponse fromString(String str) {
            XstmResponse xstmResponse = new XstmResponse();
            try {
                JSONObject jSONObject = new JSONObject(str);
                xstmResponse.result = jSONObject.getString("result");
                xstmResponse.xstm = jSONObject.getString("xstm");
                return xstmResponse;
            } catch (JSONException e) {
                return null;
            }
        }

        public String toString() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("result", this.result);
                jSONObject.put("xstm", this.xstm);
                return jSONObject.toString();
            } catch (JSONException e) {
                return null;
            }
        }
    }

    public static int getQuality(String str) {
        if (WebsiteInfo.QUALITY_NORMAL.equals(str)) {
            return 0;
        }
        if (WebsiteInfo.QUALITY_HIGHT.equals(str)) {
            return 1;
        }
        if ("super".equals(str)) {
            return 2;
        }
        if ("super2".equals(str)) {
            return 3;
        }
        return "real".equals(str) ? 4 : 0;
    }
}
