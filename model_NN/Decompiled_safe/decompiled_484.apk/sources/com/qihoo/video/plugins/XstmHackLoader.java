package com.qihoo.video.plugins;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Environment;
import android.util.Log;
import com.qihoo.video.plugins.XstmConverter;
import dalvik.system.DexClassLoader;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;

public class XstmHackLoader {
    private static final String HACK_CALSS_PACKAGE_NAME = "com.qihoo.video.spider.XstmHackHelper";
    private static String LOCAL_DIR_PATH = null;
    private static final String METHOD_XstmHack = "XstmHack";
    private static final String METHOD_getAppVersion = "getAppVersion";
    private static final String METHOD_verifyContext = "verifyContext";
    private static final int NET_CONNECT_TIMTOUT = 30000;
    private static final String PLUGIN_DEX_NAME = (String.valueOf(String.valueOf(Math.abs("hackplugin".hashCode()))) + ".dex");
    private static String PLUGIN_DEX_PATH = null;
    private static String PLUGIN_DIR_PATH = null;
    private static final String PLUGIN_FOLDER_NAME = String.valueOf(Math.abs("plugins".hashCode()));
    private static final String PLUGIN_NAME = (String.valueOf(String.valueOf(Math.abs("hackplugin".hashCode()))) + ".jar");
    private static String PLUGIN_PATH = null;
    private static final int READ_BUFFER_SIZE = 10240;
    private static final String SDCARD_DIR_PAHT = (String.valueOf(Environment.getExternalStorageDirectory().getPath()) + File.separator);
    private static final String TAG = "XstmHackLoader";
    private static volatile XstmHackLoader instance;
    Class<?>[] StringParam = {String.class};
    Class<?>[] contextParam = {Context.class};
    private int curVersion = -1;
    private Method getAppVersion;
    private Object hackInstance = null;
    private volatile boolean useable = false;
    private Method verifyContext;
    private Method xstmHack;

    private XstmHackLoader(Context context) {
        if (context == null) {
            this.useable = false;
            Log.e(TAG, "XstmHackLoader context == null");
            return;
        }
        LOCAL_DIR_PATH = String.valueOf(context.getFilesDir().getAbsolutePath()) + File.separator;
        PLUGIN_DIR_PATH = String.valueOf(LOCAL_DIR_PATH) + PLUGIN_FOLDER_NAME + File.separator;
        PLUGIN_PATH = String.valueOf(PLUGIN_DIR_PATH) + PLUGIN_NAME;
        PLUGIN_DEX_PATH = String.valueOf(PLUGIN_DIR_PATH) + PLUGIN_DEX_NAME;
        loadLocalPlugin(context);
        if (!this.useable) {
            clearAll();
        }
        Log.d(TAG, "XstmHackLoader create ok ! useable = " + this.useable);
    }

    private void checkUpdate(Context context, int i, String str) {
        Log.d(TAG, "checkUpdate useable = " + this.useable + ", curVersion = " + this.curVersion + ", serverVersion = " + i);
        if (this.useable) {
            if (this.curVersion < i) {
                Log.d(TAG, "curVersion < appParser.packageversion " + this.curVersion);
                clearAll();
                if (getFileFromServer(str)) {
                    loadLocalPlugin(context);
                } else {
                    clearAll();
                }
            }
        } else if (getFileFromServer(str)) {
            loadLocalPlugin(context);
        } else {
            clearAll();
        }
    }

    private void clearAll() {
        Log.d(TAG, "clearAll()");
        this.hackInstance = null;
        this.curVersion = -1;
        this.useable = false;
        File file = new File(PLUGIN_PATH);
        if (file.exists()) {
            Log.d(TAG, "file.exists() PLUGIN_PATH = " + PLUGIN_PATH);
            if (file.delete()) {
                Log.d(TAG, "delete jar ok");
            } else {
                Log.e(TAG, "delete jar fail");
            }
        }
        File file2 = new File(PLUGIN_DEX_PATH);
        if (file2.exists()) {
            Log.d(TAG, "file.exists() PLUGIN_DEX_PATH = " + PLUGIN_DEX_PATH);
            if (file2.delete()) {
                Log.d(TAG, "delete dex ok");
            } else {
                Log.e(TAG, "delete dex fail");
            }
        }
    }

    private boolean getFileFromServer(String str) {
        Log.d(TAG, "getFileFromServer begin");
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setConnectTimeout(NET_CONNECT_TIMTOUT);
            InputStream inputStream = httpURLConnection.getInputStream();
            File file = new File(PLUGIN_PATH);
            if (!file.exists() || file.delete()) {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
                byte[] bArr = new byte[READ_BUFFER_SIZE];
                while (true) {
                    int read = bufferedInputStream.read(bArr);
                    if (read == -1) {
                        fileOutputStream.close();
                        bufferedInputStream.close();
                        inputStream.close();
                        Log.d(TAG, "getFileFromServer end ok");
                        return true;
                    }
                    fileOutputStream.write(bArr, 0, read);
                }
            } else {
                Log.d(TAG, "getFileFromServer !file.delete()");
                return false;
            }
        } catch (Exception e) {
            Log.e(TAG, "getFileFromServer end e = " + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    private static XstmConverter.XstmRequest.Header[] getHeaders(String str) {
        XstmConverter.XstmRequest.Header[] headerArr = new XstmConverter.XstmRequest.Header[1];
        if (str.equals("")) {
            headerArr[0] = new XstmConverter.XstmRequest.Header();
            headerArr[0].key = "User-Agent";
            headerArr[0].value = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)";
        } else if (str.equals("")) {
            headerArr[0] = new XstmConverter.XstmRequest.Header();
            headerArr[0].key = "User-Agent";
            headerArr[0].value = "Mozilla/5.0 (Linux; U; Android 4.0.2; en-us; Galaxy Nexus Build/ICL53F) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";
        }
        return headerArr;
    }

    public static XstmHackLoader getInstance(Context context) {
        if (instance == null) {
            synchronized (XstmHackLoader.class) {
                if (instance == null) {
                    instance = new XstmHackLoader(context);
                }
            }
        }
        return instance;
    }

    @SuppressLint({"NewApi"})
    private void loadLocalPlugin(Context context) {
        File file = new File(PLUGIN_DIR_PATH);
        if (!file.exists() && !file.mkdirs()) {
            Log.e(TAG, "PLUGIN_DIR_PATH file.mkdirs() false");
        }
        if (!new File(PLUGIN_PATH).exists()) {
            Log.e(TAG, "loadLocalPlugin !file.exists() DEX_PATH = " + PLUGIN_PATH);
            this.useable = false;
            return;
        }
        Log.d(TAG, "PLUGIN_PATH = " + PLUGIN_PATH);
        Log.d(TAG, "PLUGIN_DIR_PATH = " + PLUGIN_DIR_PATH);
        Log.d(TAG, "PLUGIN_DEX_PATH = " + PLUGIN_DEX_PATH);
        try {
            Class<?> cls = Class.forName(HACK_CALSS_PACKAGE_NAME, true, new DexClassLoader(PLUGIN_PATH, PLUGIN_DIR_PATH, null, context.getClassLoader()));
            Log.d(TAG, "loadLocalPlugin hackClass ok");
            Constructor<?> constructor = cls.getConstructor(new Class[0]);
            Log.d(TAG, "loadLocalPlugin constructor ok");
            this.hackInstance = constructor.newInstance(new Object[0]);
            Log.d(TAG, "loadLocalPlugin hackInstance ok");
            this.verifyContext = cls.getMethod(METHOD_verifyContext, this.contextParam);
            Log.d(TAG, "loadLocalPlugin getMethod verifyContext ok");
            Boolean bool = (Boolean) this.verifyContext.invoke(this.hackInstance, context);
            Log.d(TAG, "loadLocalPlugin getMethod verifyContext result = " + bool.booleanValue());
            this.useable = bool.booleanValue();
            if (this.useable) {
                this.getAppVersion = cls.getMethod(METHOD_getAppVersion, new Class[0]);
                Log.d(TAG, "loadLocalPlugin getMethod getAppVersion ok");
                this.xstmHack = cls.getMethod(METHOD_XstmHack, this.StringParam);
                Log.d(TAG, "loadLocalPlugin getMethod XstmHack ok");
                Integer num = (Integer) this.getAppVersion.invoke(this.hackInstance, new Object[0]);
                Log.d(TAG, "loadLocalPlugin getCurrentVersion version = " + num);
                this.curVersion = num.intValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "loadLocalPlugin e = " + e.getMessage());
            this.useable = false;
        }
    }

    public synchronized boolean getUseable() {
        return this.useable;
    }

    public synchronized XstmConverter.XstmResponse hackXstm(Context context, int i, String str, String str2) {
        String str3;
        XstmConverter.XstmResponse fromString;
        checkUpdate(context, i, str);
        if (!this.useable) {
            Log.d(TAG, "hackXstm useable = " + this.useable);
            fromString = null;
        } else {
            try {
                str3 = (String) this.xstmHack.invoke(this.hackInstance, str2);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                str3 = null;
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
                str3 = null;
            } catch (InvocationTargetException e3) {
                e3.printStackTrace();
                str3 = null;
            }
            Log.d(TAG, "hackXstm response = " + str3);
            fromString = XstmConverter.XstmResponse.fromString(str3);
            if (fromString == null) {
                fromString = null;
            } else {
                Log.d(TAG, "hackXstm xstmResponse.result = " + fromString.result + ", xstmResponse.xstm = " + fromString.xstm);
            }
        }
        return fromString;
    }

    public void setPluginDirPath(Context context, String str) {
        if (PLUGIN_DIR_PATH.equals(str)) {
            Log.d(TAG, "setPluginDirPath PLUGIN_DIR_PATH.equals(path) = " + str);
            return;
        }
        clearAll();
        PLUGIN_DIR_PATH = str;
        PLUGIN_PATH = String.valueOf(PLUGIN_DIR_PATH) + PLUGIN_NAME;
        PLUGIN_DEX_PATH = String.valueOf(PLUGIN_DIR_PATH) + PLUGIN_DEX_NAME;
        loadLocalPlugin(context);
        if (!this.useable) {
            clearAll();
        }
        Log.d(TAG, "setPluginDirPath end useable = " + this.useable);
    }
}
