package com.qihoo.video;

import android.util.Log;
import com.mediav.ads.sdk.adcore.HttpCacher;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtil {
    public static final String DATE_FULL_STR = "yyyy-MM-dd HH:mm";
    public static final String FORMAT_DEFAULT = "yyyy-MM-dd";
    private static final long TEN_MINUTES = 600000;
    private static long TIME_DIFFERENCE = 0;

    public static long dateToUnixTimestamp(String str) {
        return dateToUnixTimestamp(str, DATE_FULL_STR);
    }

    public static long dateToUnixTimestamp(String str, String str2) {
        try {
            return new SimpleDateFormat(str2, Locale.CHINA).parse(str).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String formaShortVideoDuration3(int i) {
        int i2 = i / 60;
        if (i2 < 60) {
            return String.valueOf(unitFormat(i2)) + ":" + unitFormat(i % 60);
        }
        int i3 = i2 / 60;
        if (i3 > 99) {
            return "99:59:59";
        }
        int i4 = i2 % 60;
        return String.valueOf(unitFormat(i3)) + ":" + unitFormat(i4) + ":" + unitFormat((i - (i3 * HttpCacher.TIME_HOUR)) - (i4 * 60));
    }

    public static String formatShortVideoDuration(int i) {
        return new SimpleDateFormat("mm : ss", Locale.CHINA).format(new Date((long) i));
    }

    public static String formatShortVideoDuration2(int i) {
        return new SimpleDateFormat("hh : mm : ss", Locale.CHINA).format(new Date((long) i));
    }

    public static String formatVideoDuration(int i) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("H:mm:ss", Locale.CHINA);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));
        return simpleDateFormat.format(new Date((long) i));
    }

    public static String formaterTime(long j) {
        Date date = new Date();
        Date date2 = new Date(j);
        return date.getYear() == date2.getYear() ? new SimpleDateFormat("MM月dd日", Locale.CHINA).format(date2) : new SimpleDateFormat("yyyy年MM月dd日", Locale.CHINA).format(date2);
    }

    public static String getCurrentDate() {
        return getCurrentDate("MM-dd-yyyy");
    }

    public static String getCurrentDate(String str) {
        return new SimpleDateFormat(str, Locale.CHINA).format(new Date());
    }

    public static String getCurrentDateInServer(String str) {
        return new SimpleDateFormat(str, Locale.CHINA).format(new Date(getCurrentTimestampInServer()));
    }

    public static String getCurrentDateToSecond() {
        return new SimpleDateFormat("MM-dd-yyyy HH:mm:ss", Locale.CHINA).format(new Date());
    }

    public static long getCurrentTime() {
        return System.currentTimeMillis();
    }

    public static long getCurrentTimestamp() {
        long time = new Date().getTime();
        System.currentTimeMillis();
        return time;
    }

    public static long getCurrentTimestampInServer() {
        return System.currentTimeMillis() - TIME_DIFFERENCE;
    }

    private static int getDayOfWeek(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        return instance.get(7);
    }

    public static long getFutureTime(long j) {
        return new Date().getTime() + (24 * j * 60 * 60 * 1000);
    }

    public static long getProofreadTimestamp(long j) {
        return TIME_DIFFERENCE + j;
    }

    public static String getStrDayOfWeek(String str) {
        return parseDayOfWeek(getDayOfWeek(parseDate(str)));
    }

    public static boolean isSameDay(long j, long j2) {
        Date date = new Date(j);
        Date date2 = new Date(j2);
        return date.getYear() == date2.getYear() && date.getMonth() == date2.getMonth() && date.getDate() == date2.getDate();
    }

    private static Date parseDate(String str) {
        return parseDate(str, null);
    }

    private static Date parseDate(String str, String str2) {
        if (str2 == null) {
            str2 = FORMAT_DEFAULT;
        }
        try {
            return new SimpleDateFormat(str2, Locale.CHINA).parse(str);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String parseDayOfWeek(int i) {
        switch (i) {
            case 1:
                return "周日";
            case 2:
                return "周一";
            case 3:
                return "周二";
            case 4:
                return "周三";
            case 5:
                return "周四";
            case 6:
                return "周五";
            case 7:
                return "周六";
            default:
                return "";
        }
    }

    public static void setTimeDifference(long j) {
        TIME_DIFFERENCE = System.currentTimeMillis() - j;
        if (TIME_DIFFERENCE > 0 && TIME_DIFFERENCE <= TEN_MINUTES) {
            TIME_DIFFERENCE = 0;
        }
        Log.d("time", "setTimeDifference TIME_DIFFERENCE: " + TIME_DIFFERENCE);
    }

    private static String unitFormat(int i) {
        return (i < 0 || i >= 10) ? new StringBuilder().append(i).toString() : "0" + Integer.toString(i);
    }

    public static String unixTimestampToDate(long j) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FULL_STR, Locale.CHINA);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        return simpleDateFormat.format(new Date(j));
    }
}
