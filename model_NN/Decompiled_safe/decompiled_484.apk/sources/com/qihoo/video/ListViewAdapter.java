package com.qihoo.video;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import java.util.ArrayList;
import java.util.List;

public abstract class ListViewAdapter extends BaseAdapter implements AbsListView.RecyclerListener {
    protected List<Object> infos = null;
    protected Context mContext = null;
    protected AbsListView mListView = null;

    public ListViewAdapter(Context context) {
        this.mContext = context;
        this.infos = new ArrayList();
    }

    /* access modifiers changed from: protected */
    public abstract void OnScrapHeap(View view);

    public void addItem(Object obj) {
        if (obj != null) {
            this.infos.add(0, obj);
        }
        if (this.infos.size() > 0) {
            notifyDataSetChanged();
        }
    }

    public void addItems(Object[] objArr) {
        if (objArr != null) {
            for (Object add : objArr) {
                this.infos.add(add);
            }
            if (objArr.length > 0) {
                notifyDataSetChanged();
            }
        }
    }

    public void addItemsAtFirst(List<?> list) {
        if (list != null) {
            for (Object add : list) {
                this.infos.add(0, add);
            }
            if (list.size() > 0) {
                notifyDataSetChanged();
            }
        }
    }

    public int getCount() {
        if (this.infos == null) {
            return 0;
        }
        return this.infos.size();
    }

    public Object getItem(int i) {
        if (i >= getCount() || i < 0) {
            return null;
        }
        return this.infos.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        return null;
    }

    public void onMovedToScrapHeap(View view) {
        OnScrapHeap(view);
    }

    public void reset() {
        if (this.infos != null) {
            this.infos.clear();
        }
    }

    public void setListView(AbsListView absListView) {
        if (absListView != null) {
            this.mListView = absListView;
            absListView.setRecyclerListener(this);
        }
    }
}
