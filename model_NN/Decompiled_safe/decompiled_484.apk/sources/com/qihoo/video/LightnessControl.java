package com.qihoo.video;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.WindowManager;
import android.widget.Toast;

public class LightnessControl {
    public static final int LIGHT_SYSTEM_TAG = -1;
    public static final String SHARED_PREFERENCES_LIGHT = "shared_preferences_light";

    public static int getLightness(Context context) {
        int videoLightness = getVideoLightness(context);
        return videoLightness == -1 ? getSystemLightness(context) : videoLightness;
    }

    public static int getSystemLightness(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "screen_brightness", -1);
    }

    public static int getVideoLightness(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(SHARED_PREFERENCES_LIGHT, -1);
    }

    public static boolean isAutoBrightness(Context context) {
        try {
            return Settings.System.getInt(context.getContentResolver(), "screen_brightness_mode") == 1;
        } catch (Exception e) {
            Toast.makeText(context, "无法获取亮度", 0).show();
            return false;
        }
    }

    private static void saveLightness(Context context, int i) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(context).edit();
        edit.putInt(SHARED_PREFERENCES_LIGHT, i);
        edit.commit();
    }

    public static void setAndSaveLightness(Context context, int i) {
        setLightness(context, i);
        saveLightness(context, i);
    }

    public static void setLightness(Context context, int i) {
        try {
            if (Build.VERSION.SDK_INT >= 19) {
                setSystemLisghtness(context, i);
            } else if (context instanceof Activity) {
                WindowManager.LayoutParams attributes = ((Activity) context).getWindow().getAttributes();
                if (i <= 20) {
                    i = 20;
                }
                attributes.screenBrightness = ((float) i) / 255.0f;
                ((Activity) context).getWindow().setAttributes(attributes);
            }
        } catch (Exception e) {
            Toast.makeText(context, "无法改变亮度", 0).show();
        }
    }

    public static void setSystemLisghtness(Context context, int i) {
        Settings.System.putInt(context.getContentResolver(), "screen_brightness", i);
    }

    public static void startAutoBrightness(Context context) {
        Settings.System.putInt(context.getContentResolver(), "screen_brightness_mode", 1);
    }

    public static void stopAutoBrightness(Context context) {
        Settings.System.putInt(context.getContentResolver(), "screen_brightness_mode", 0);
    }
}
