package com.qihoo.video;

public interface IPlayerViewListener {
    void changeQualityVideo(WebsiteInfo websiteInfo, WebsiteInfo websiteInfo2);

    void finishPlayer();

    void fullScreenPlay();

    void playVideo(int i);
}
