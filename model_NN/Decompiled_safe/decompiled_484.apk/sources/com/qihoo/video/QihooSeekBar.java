package com.qihoo.video;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.SeekBar;

public class QihooSeekBar extends SeekBar {
    public QihooSeekBar(Context context) {
        super(context);
    }

    public QihooSeekBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public QihooSeekBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    private void trackTouchEvent(MotionEvent motionEvent) {
        int width = getWidth();
        int x = (int) motionEvent.getX();
        setProgress((int) ((x < 0 ? 0.0f : x > width ? 1.0f : ((float) x) / ((float) width)) * ((float) getMax())));
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!isEnabled()) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                trackTouchEvent(motionEvent);
                break;
        }
        return super.onTouchEvent(motionEvent);
    }
}
