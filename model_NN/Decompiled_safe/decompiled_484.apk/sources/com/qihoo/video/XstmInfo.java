package com.qihoo.video;

public class XstmInfo {
    public String currentQuality = null;
    public int errorCode = 0;
    public String quality = null;
    public String refUrl = null;
    public String vid = null;
    public String xstm = null;

    public String toString() {
        return "XstmInfo [errorCode=" + this.errorCode + ", xstm=" + this.xstm + ", refUrl=" + this.refUrl + ", quality=" + this.quality + ", currentQuality=" + this.currentQuality + ", vid=" + this.vid + "]";
    }
}
