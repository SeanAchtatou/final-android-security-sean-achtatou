package com.qihoo.video;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import com.qihoo.video.VideoRequestUtils;
import com.qihoo.video.httpservice.AsyncRequest;

public class StreamUrlRequest extends AsyncRequest {
    private Object obj = null;
    private int playerSDK = 0;

    public StreamUrlRequest(Activity activity, String str, String str2) {
        super(activity, str, str2);
    }

    /* access modifiers changed from: protected */
    public Object doInBackground(Object... objArr) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5 = RequestSource.PLAYER_SDK;
        if (objArr.length >= 2) {
            str4 = (String) objArr[0];
            str3 = (String) objArr[1];
            str = objArr.length >= 3 ? objArr[2] : "play";
            try {
                this.playerSDK = (objArr.length >= 4 ? objArr[3] == null ? 0 : objArr[3] : 0).intValue();
            } catch (Exception e) {
                this.playerSDK = 0;
            }
            str5 = objArr.length >= 5 ? objArr[4] : RequestSource.PLAYER_SDK;
            str2 = objArr.length >= 6 ? objArr[5] : null;
        } else if (objArr.length != 1 || !(objArr[0] instanceof VideoRequestUtils.VideoRequestParams)) {
            str = null;
            str2 = null;
            str3 = null;
            str4 = null;
        } else {
            VideoRequestUtils.VideoRequestParams videoRequestParams = (VideoRequestUtils.VideoRequestParams) objArr[0];
            String xstmUrl = videoRequestParams.getXstmUrl();
            str3 = videoRequestParams.getQualityKey();
            this.playerSDK = videoRequestParams.getPlayerSDK();
            str = videoRequestParams.getAction();
            str5 = videoRequestParams.getRequestSource();
            str2 = videoRequestParams.getZsParams();
            str4 = xstmUrl;
        }
        if (TextUtils.isEmpty(str3)) {
            str3 = WebsiteInfo.QUALITY_NORMAL;
        }
        if (str4 == null) {
            return null;
        }
        Log.i("jy", str4);
        Log.i("jy", "StreamUrlRequest " + str3 + " act = " + str);
        Log.d("meng", "StreamUrlRequest " + str5 + " zsParams = " + str2);
        XstmInfo requestXstm = Requests.requestXstm(str4, str3, str);
        if (isCancelled()) {
            return null;
        }
        return requestXstm;
    }

    public Object getObject() {
        return this.obj;
    }

    public int getPlayerSDK() {
        return this.playerSDK;
    }

    public void setObject(Object obj2) {
        this.obj = obj2;
    }
}
