package com.qihoo.video;

import com.qihoo.dynamic.util.Md5Util;
import java.net.URLEncoder;

public class URLEncodeing {
    public static String uRLEncode(String str) {
        try {
            return URLEncoder.encode(str, Md5Util.DEFAULT_CHARSET).replaceAll("\\+", "%20");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
