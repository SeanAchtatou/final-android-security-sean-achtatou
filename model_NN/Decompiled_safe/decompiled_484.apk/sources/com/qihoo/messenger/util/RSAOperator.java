package com.qihoo.messenger.util;

import java.security.KeyFactory;
import java.security.Signature;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;

public class RSAOperator {
    private static final String TAG = "RSAOperator";
    private static final byte[] X509_ENVELOP = {48, -127, -97, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 1, 5, 0, 3, -127, -115, 0};
    private RSAPublicKey mPublicKey;

    protected RSAOperator(RSAPublicKey rSAPublicKey) {
        this.mPublicKey = rSAPublicKey;
    }

    public RSAOperator(byte[] bArr) {
        int i = 0;
        if (bArr.length == 140) {
            byte[] bArr2 = new byte[162];
            int i2 = 0;
            while (i2 < X509_ENVELOP.length) {
                bArr2[i2] = X509_ENVELOP[i2];
                i2++;
            }
            while (i2 < 162) {
                bArr2[i2] = bArr[i];
                i2++;
                i++;
            }
            bArr = bArr2;
        } else if (bArr.length != 162) {
            throw new Exception("Invalid Buffer Length: " + bArr.length + " of Public Key");
        }
        this.mPublicKey = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(bArr));
    }

    public byte[] encrypt(byte[] bArr) {
        RSAPublicKey rSAPublicKey = this.mPublicKey;
        Cipher instance = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        instance.init(1, rSAPublicKey);
        return instance.doFinal(bArr);
    }

    public byte[] getEncodedPublicKey() {
        return this.mPublicKey.getEncoded();
    }

    public RSAPublicKey getPublicKey() {
        return this.mPublicKey;
    }

    public boolean verify(byte[] bArr, byte[] bArr2) {
        Signature instance = Signature.getInstance("SHA1withRSA");
        instance.initVerify(this.mPublicKey);
        instance.update(bArr);
        return instance.verify(bArr2);
    }
}
