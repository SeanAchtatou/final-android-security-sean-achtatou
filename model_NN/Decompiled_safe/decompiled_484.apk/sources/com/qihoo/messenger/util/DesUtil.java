package com.qihoo.messenger.util;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class DesUtil {
    private static final String ALGORITHM_DES = "DES/CBC/PKCS5Padding";
    private static final int BUFFER_LENGTH = 1024;
    public static final String KEY = "zshTtp^1";
    private static final String TAG = "DesUtil";

    public static String decryptDES(InputStream inputStream, int i) {
        return decryptDES(inputStream, i, KEY.getBytes());
    }

    public static String decryptDES(InputStream inputStream, int i, byte[] bArr) {
        if (i < 1024) {
            i = 1024;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(i);
        byte[] bArr2 = new byte[1024];
        try {
            IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr);
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "DES");
            Cipher instance = Cipher.getInstance(ALGORITHM_DES);
            CipherInputStream cipherInputStream = new CipherInputStream(inputStream, instance);
            instance.init(2, secretKeySpec, ivParameterSpec);
            while (true) {
                int read = cipherInputStream.read(bArr2);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr2, 0, read);
                } else {
                    byteArrayOutputStream.flush();
                    byteArrayOutputStream.close();
                    cipherInputStream.close();
                    return byteArrayOutputStream.toString();
                }
            }
        } catch (Exception e) {
            return null;
        }
    }

    public static byte[] decryptDES(byte[] bArr) {
        return decryptDES(bArr, KEY.getBytes());
    }

    public static byte[] decryptDES(byte[] bArr, byte[] bArr2) {
        if (bArr == null) {
            return null;
        }
        try {
            if (bArr.length <= 0) {
                return null;
            }
            IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr2);
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr2, "DES");
            Cipher instance = Cipher.getInstance(ALGORITHM_DES);
            instance.init(2, secretKeySpec, ivParameterSpec);
            return instance.doFinal(bArr);
        } catch (Exception e) {
            return null;
        }
    }

    public static void decryptFile(String str, File file, File file2) {
        if (file == null || !file.exists() || !file.isFile()) {
            throw new IOException();
        }
        FileInputStream fileInputStream = new FileInputStream(file);
        DataInputStream dataInputStream = new DataInputStream(fileInputStream);
        FileOutputStream fileOutputStream = new FileOutputStream(file2);
        DataOutputStream dataOutputStream = new DataOutputStream(fileOutputStream);
        byte[] bArr = new byte[1024];
        try {
            long readLong = dataInputStream.readLong();
            if (file.length() >= 8 + readLong) {
                byte[] bArr2 = new byte[((int) readLong)];
                dataInputStream.read(bArr2, 0, bArr2.length);
                dataOutputStream.write(decryptDES(bArr2, str.getBytes()));
                while (true) {
                    int read = dataInputStream.read(bArr, 0, bArr.length);
                    if (read == -1) {
                        break;
                    }
                    dataOutputStream.write(bArr, 0, read);
                }
            } else {
                int read2 = dataInputStream.read(bArr, 0, bArr.length);
                byte[] bArr3 = new byte[read2];
                System.arraycopy(bArr, 0, bArr3, 0, read2);
                dataOutputStream.write(decryptDES(bArr3, str.getBytes()));
            }
            dataInputStream.close();
            fileInputStream.close();
            dataOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            throw new IOException();
        } catch (Throwable th) {
            dataInputStream.close();
            fileInputStream.close();
            dataOutputStream.close();
            fileOutputStream.close();
            throw th;
        }
    }

    public static byte[] encryptDES(byte[] bArr) {
        return encryptDES(bArr, KEY.getBytes());
    }

    public static byte[] encryptDES(byte[] bArr, byte[] bArr2) {
        if (bArr == null) {
            return null;
        }
        try {
            if (bArr.length <= 0) {
                return null;
            }
            IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr2);
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr2, "DES");
            Cipher instance = Cipher.getInstance(ALGORITHM_DES);
            instance.init(1, secretKeySpec, ivParameterSpec);
            return instance.doFinal(bArr);
        } catch (Exception e) {
            return null;
        }
    }

    public static void encryptFile(String str, File file, File file2) {
        if (file == null || !file.exists() || !file.isFile()) {
            throw new IOException();
        }
        FileInputStream fileInputStream = new FileInputStream(file);
        DataInputStream dataInputStream = new DataInputStream(fileInputStream);
        FileOutputStream fileOutputStream = new FileOutputStream(file2);
        DataOutputStream dataOutputStream = new DataOutputStream(fileOutputStream);
        byte[] bArr = new byte[1024];
        try {
            if (file.length() >= PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID) {
                dataInputStream.read(bArr, 0, bArr.length);
                byte[] encryptDES = encryptDES(bArr, str.getBytes());
                dataOutputStream.writeLong((long) encryptDES.length);
                dataOutputStream.write(encryptDES);
                while (true) {
                    int read = dataInputStream.read(bArr, 0, bArr.length);
                    if (read == -1) {
                        break;
                    }
                    dataOutputStream.write(bArr, 0, read);
                }
            } else {
                int read2 = dataInputStream.read(bArr, 0, bArr.length);
                byte[] bArr2 = new byte[read2];
                System.arraycopy(bArr, 0, bArr2, 0, read2);
                byte[] encryptDES2 = encryptDES(bArr2, str.getBytes());
                dataOutputStream.writeLong((long) encryptDES2.length);
                dataOutputStream.write(encryptDES2);
            }
            dataInputStream.close();
            fileInputStream.close();
            dataOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            throw new IOException();
        } catch (Throwable th) {
            dataInputStream.close();
            fileInputStream.close();
            dataOutputStream.close();
            fileOutputStream.close();
            throw th;
        }
    }
}
