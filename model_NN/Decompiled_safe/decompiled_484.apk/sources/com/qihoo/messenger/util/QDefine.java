package com.qihoo.messenger.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import com.qihoo.dynamic.util.Md5Util;
import com.qihoo360.daily.activity.SendCmtActivity;
import java.io.File;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class QDefine {
    public static final String APP_START_TYPE = "app_start";
    public static final String DOWN_FINISH_EVENT = "down_finish";
    public static final String DOWN_INSTALL_EVENT = "down_install";
    public static final String DOWN_START_EVENT = "down_start";
    private static char[] ENCODE_KEY = null;
    public static final String GET_STATUS_TYPE = "get_status";
    public static final String LOCAL_ACTION = "com.qihoo.iot.psdk.action.local";
    public static final long NET_ACCESS_PERIOD = 180000;
    public static final String NOTIFY_ACTION = "com.qihoo.iot.psdk.action.notify";
    public static final String NOTIFY_CLICK_EVENT = "notify_click";
    public static final long ONE_DAY = 86400000;
    public static final int ONE_HOUR = 3600000;
    public static final int ONE_MINUTE = 60000;
    public static final int ONE_SECOND = 1000;
    public static final String SET_ALIAS_TYPE = "set_alias";
    public static final String SET_TAGS_TYPE = "set_tags";
    private static final String TAG = "QDefine";
    public static final long WAP_ACCESS_PERIOD = 60000;
    public static final long WIFI_ACCESS_PERIOD = 300000;
    private static String encodeKey = "";
    private static String registerId = "";

    public static String MD5(String str) {
        String str2;
        if (!TextUtils.isEmpty(str)) {
            char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
            try {
                byte[] bytes = str.getBytes();
                MessageDigest instance = MessageDigest.getInstance(Md5Util.ALGORITHM);
                instance.update(bytes);
                char[] cArr2 = new char[(r5 * 2)];
                int i = 0;
                for (byte b2 : instance.digest()) {
                    int i2 = i + 1;
                    cArr2[i] = cArr[(b2 >>> 4) & 15];
                    i = i2 + 1;
                    cArr2[i2] = cArr[b2 & 15];
                }
                str2 = new String(cArr2);
            } catch (Exception e) {
                QLOG.error(TAG, e);
            }
            QLOG.debug(TAG, "md5  input: " + str);
            QLOG.debug(TAG, "md5 output: " + str2);
            return str2;
        }
        str2 = "";
        QLOG.debug(TAG, "md5  input: " + str);
        QLOG.debug(TAG, "md5 output: " + str2);
        return str2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0030 A[SYNTHETIC, Splitter:B:20:0x0030] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0035 A[Catch:{ Exception -> 0x0039 }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0045 A[SYNTHETIC, Splitter:B:29:0x0045] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x004a A[Catch:{ Exception -> 0x004e }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void appendToFile(java.lang.String r4, java.lang.String r5) {
        /*
            r2 = 0
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0027, all -> 0x0040 }
            r0 = 1
            r3.<init>(r5, r0)     // Catch:{ Exception -> 0x0027, all -> 0x0040 }
            java.io.OutputStreamWriter r1 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x005d, all -> 0x0055 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x005d, all -> 0x0055 }
            r1.write(r4)     // Catch:{ Exception -> 0x0061, all -> 0x0058 }
            r1.close()     // Catch:{ Exception -> 0x0061, all -> 0x0058 }
            r3.close()     // Catch:{ Exception -> 0x0061, all -> 0x0058 }
            if (r1 == 0) goto L_0x001a
            r1.close()     // Catch:{ Exception -> 0x0020 }
        L_0x001a:
            if (r3 == 0) goto L_0x001f
            r3.close()     // Catch:{ Exception -> 0x0020 }
        L_0x001f:
            return
        L_0x0020:
            r0 = move-exception
            java.lang.String r1 = "QDefine"
            com.qihoo.messenger.util.QLOG.error(r1, r0)
            goto L_0x001f
        L_0x0027:
            r0 = move-exception
            r1 = r2
        L_0x0029:
            java.lang.String r3 = "QDefine"
            com.qihoo.messenger.util.QLOG.error(r3, r0)     // Catch:{ all -> 0x005a }
            if (r1 == 0) goto L_0x0033
            r1.close()     // Catch:{ Exception -> 0x0039 }
        L_0x0033:
            if (r2 == 0) goto L_0x001f
            r2.close()     // Catch:{ Exception -> 0x0039 }
            goto L_0x001f
        L_0x0039:
            r0 = move-exception
            java.lang.String r1 = "QDefine"
            com.qihoo.messenger.util.QLOG.error(r1, r0)
            goto L_0x001f
        L_0x0040:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x0043:
            if (r1 == 0) goto L_0x0048
            r1.close()     // Catch:{ Exception -> 0x004e }
        L_0x0048:
            if (r3 == 0) goto L_0x004d
            r3.close()     // Catch:{ Exception -> 0x004e }
        L_0x004d:
            throw r0
        L_0x004e:
            r1 = move-exception
            java.lang.String r2 = "QDefine"
            com.qihoo.messenger.util.QLOG.error(r2, r1)
            goto L_0x004d
        L_0x0055:
            r0 = move-exception
            r1 = r2
            goto L_0x0043
        L_0x0058:
            r0 = move-exception
            goto L_0x0043
        L_0x005a:
            r0 = move-exception
            r3 = r2
            goto L_0x0043
        L_0x005d:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0029
        L_0x0061:
            r0 = move-exception
            r2 = r3
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo.messenger.util.QDefine.appendToFile(java.lang.String, java.lang.String):void");
    }

    public static void broadcastToLocal(Context context, String str, String str2) {
        Intent intent = new Intent();
        intent.setAction(LOCAL_ACTION);
        intent.setPackage(context.getPackageName());
        intent.putExtra("targetPack", context.getPackageName());
        intent.putExtra("type", str);
        intent.putExtra(SendCmtActivity.TAG_DATA, str2);
        QLOG.debug(TAG, LOCAL_ACTION + str);
        context.sendBroadcast(intent);
    }

    public static void broadcastToLocal(Context context, String str, String str2, String str3, int i) {
        Intent intent = new Intent();
        intent.setAction(LOCAL_ACTION);
        intent.setPackage(context.getPackageName());
        intent.putExtra("targetPack", context.getPackageName());
        intent.putExtra("type", str);
        intent.putExtra(SendCmtActivity.TAG_DATA, str2);
        intent.putExtra("appPackName", str3);
        intent.putExtra("appVersionCode", i);
        context.sendBroadcast(intent);
    }

    public static String decode(String str) {
        if (ENCODE_KEY == null) {
            ENCODE_KEY = new char[encodeKey.length()];
            for (int i = 0; i < encodeKey.length(); i++) {
                ENCODE_KEY[i] = encodeKey.charAt(i);
            }
        }
        byte[] decode = Base64.decode(str, 11);
        for (int i2 = 0; i2 < decode.length; i2++) {
            decode[i2] = (byte) (decode[i2] ^ ENCODE_KEY[i2 % ENCODE_KEY.length]);
        }
        return new String(decode);
    }

    public static String encode(String str) {
        try {
            if (ENCODE_KEY == null) {
                ENCODE_KEY = new char[encodeKey.length()];
                for (int i = 0; i < encodeKey.length(); i++) {
                    ENCODE_KEY[i] = encodeKey.charAt(i);
                }
            }
            byte[] bytes = str.getBytes();
            for (int i2 = 0; i2 < bytes.length; i2++) {
                bytes[i2] = (byte) (bytes[i2] ^ ENCODE_KEY[i2 % ENCODE_KEY.length]);
            }
            return Base64.encodeToString(bytes, 11);
        } catch (Exception e) {
            QLOG.error(TAG, e);
            return "";
        }
    }

    private static String generateKey() {
        int nextInt = new Random().nextInt();
        if (nextInt < 0) {
            nextInt = -nextInt;
        }
        String str = "00000" + nextInt;
        return str.length() > 6 ? str.substring(str.length() - 6) : str;
    }

    public static String getAn(Context context) {
        DisplayMetrics displayMetrics = context.getApplicationContext().getResources().getDisplayMetrics();
        int i = displayMetrics.widthPixels;
        return "" + i + "x" + displayMetrics.heightPixels;
    }

    public static String getAndroidId(Context context) {
        try {
            return Settings.Secure.getString(context.getContentResolver(), "android_id");
        } catch (Exception e) {
            QLOG.error(TAG, e);
            return "";
        }
    }

    public static String getAppCacheDir() {
        Error e;
        String str;
        Exception e2;
        try {
            str = Environment.getExternalStorageDirectory().getPath() + "/appcache/";
            try {
                File file = new File(str);
                if (!file.exists()) {
                    file.mkdirs();
                }
            } catch (Exception e3) {
                e2 = e3;
                QLOG.error(TAG, e2);
                return str;
            } catch (Error e4) {
                e = e4;
                QLOG.error(TAG, e);
                return str;
            }
        } catch (Exception e5) {
            Exception exc = e5;
            str = "";
            e2 = exc;
        } catch (Error e6) {
            Error error = e6;
            str = "";
            e = error;
            QLOG.error(TAG, e);
            return str;
        }
        return str;
    }

    public static String getAppName(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            return (String) packageManager.getApplicationLabel(packageManager.getApplicationInfo(context.getPackageName(), 0));
        } catch (Exception e) {
            QLOG.error(TAG, e);
            return "";
        } catch (Error e2) {
            QLOG.error(TAG, e2);
            return "";
        }
    }

    public static String getCurrentTime() {
        Date date = new Date();
        int month = date.getMonth() + 1;
        int date2 = date.getDate();
        int hours = date.getHours();
        int minutes = date.getMinutes();
        int seconds = date.getSeconds();
        String str = "" + (date.getYear() + 1900);
        String str2 = month < 10 ? str + "0" + month : str + month;
        String str3 = date2 < 10 ? str2 + "0" + date2 : str2 + date2;
        String str4 = hours < 10 ? str3 + "0" + hours : str3 + hours;
        String str5 = minutes < 10 ? str4 + "0" + minutes : str4 + minutes;
        return seconds < 10 ? str5 + "0" + seconds : str5 + seconds;
    }

    public static String getEncodeKey(Context context) {
        try {
            encodeKey = getMetaData(context, "QHOPENSDK_APPID");
        } catch (Exception e) {
            QLOG.error(TAG, e);
        } catch (Error e2) {
            QLOG.error(TAG, e2);
        }
        return encodeKey;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x006b, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x006c, code lost:
        com.qihoo.messenger.util.QLOG.error(com.qihoo.messenger.util.QDefine.TAG, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0080, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0081, code lost:
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0089, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x008a, code lost:
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        return r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0067 A[SYNTHETIC, Splitter:B:29:0x0067] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0075 A[SYNTHETIC, Splitter:B:35:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0080 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:13:0x003a] */
    /* JADX WARNING: Removed duplicated region for block: B:49:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getFileContent(java.lang.String r8) {
        /*
            r2 = 0
            java.lang.String r1 = ""
            r0 = 0
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x005c }
            r4.<init>(r8)     // Catch:{ Exception -> 0x005c }
            boolean r3 = r4.exists()     // Catch:{ Exception -> 0x005c }
            if (r3 != 0) goto L_0x0035
            java.lang.String r3 = "QDefine"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x005c }
            r4.<init>()     // Catch:{ Exception -> 0x005c }
            java.lang.StringBuilder r4 = r4.append(r8)     // Catch:{ Exception -> 0x005c }
            java.lang.String r5 = " does not exist!"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x005c }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x005c }
            com.qihoo.messenger.util.QLOG.debug(r3, r4)     // Catch:{ Exception -> 0x005c }
            if (r2 == 0) goto L_0x002c
            r0.close()     // Catch:{ Exception -> 0x002e }
        L_0x002c:
            r0 = r1
        L_0x002d:
            return r0
        L_0x002e:
            r0 = move-exception
            java.lang.String r2 = "QDefine"
            com.qihoo.messenger.util.QLOG.error(r2, r0)
            goto L_0x002c
        L_0x0035:
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x005c }
            r3.<init>(r4)     // Catch:{ Exception -> 0x005c }
            long r4 = r4.length()     // Catch:{ Exception -> 0x0083, all -> 0x0080 }
            int r0 = (int) r4     // Catch:{ Exception -> 0x0083, all -> 0x0080 }
            byte[] r4 = new byte[r0]     // Catch:{ Exception -> 0x0083, all -> 0x0080 }
            r3.read(r4)     // Catch:{ Exception -> 0x0083, all -> 0x0080 }
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x0083, all -> 0x0080 }
            r5 = 0
            int r6 = r4.length     // Catch:{ Exception -> 0x0083, all -> 0x0080 }
            r0.<init>(r4, r5, r6)     // Catch:{ Exception -> 0x0083, all -> 0x0080 }
            r3.close()     // Catch:{ Exception -> 0x0089, all -> 0x0080 }
            r1 = 0
            if (r2 == 0) goto L_0x002d
            r1.close()     // Catch:{ Exception -> 0x0055 }
            goto L_0x002d
        L_0x0055:
            r1 = move-exception
            java.lang.String r2 = "QDefine"
            com.qihoo.messenger.util.QLOG.error(r2, r1)
            goto L_0x002d
        L_0x005c:
            r0 = move-exception
            r7 = r0
            r0 = r1
            r1 = r7
        L_0x0060:
            java.lang.String r3 = "QDefine"
            com.qihoo.messenger.util.QLOG.error(r3, r1)     // Catch:{ all -> 0x0072 }
            if (r2 == 0) goto L_0x002d
            r2.close()     // Catch:{ Exception -> 0x006b }
            goto L_0x002d
        L_0x006b:
            r1 = move-exception
            java.lang.String r2 = "QDefine"
            com.qihoo.messenger.util.QLOG.error(r2, r1)
            goto L_0x002d
        L_0x0072:
            r0 = move-exception
        L_0x0073:
            if (r2 == 0) goto L_0x0078
            r2.close()     // Catch:{ Exception -> 0x0079 }
        L_0x0078:
            throw r0
        L_0x0079:
            r1 = move-exception
            java.lang.String r2 = "QDefine"
            com.qihoo.messenger.util.QLOG.error(r2, r1)
            goto L_0x0078
        L_0x0080:
            r0 = move-exception
            r2 = r3
            goto L_0x0073
        L_0x0083:
            r0 = move-exception
            r2 = r3
            r7 = r0
            r0 = r1
            r1 = r7
            goto L_0x0060
        L_0x0089:
            r1 = move-exception
            r2 = r3
            goto L_0x0060
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo.messenger.util.QDefine.getFileContent(java.lang.String):java.lang.String");
    }

    public static String getImei(Context context) {
        String deviceId;
        return (context == null || (deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId()) == null) ? "" : deviceId;
    }

    public static String getMetaData(Context context, String str) {
        String str2 = "";
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                try {
                    str2 = "" + applicationInfo.metaData.get(str).toString();
                } catch (Exception e) {
                    QLOG.error(TAG, e);
                }
            }
        } catch (Exception e2) {
            QLOG.error(TAG, e2);
        }
        QLOG.debug(TAG, "meta " + str + ": " + str2);
        return str2;
    }

    public static String getModel() {
        String str = Build.MODEL;
        if (str == null) {
            str = "";
        }
        return str.replace(" ", "").replace(",", "");
    }

    public static String getNetworkType(Context context) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isAvailable()) {
                return activeNetworkInfo.getExtraInfo();
            }
        } catch (Exception e) {
            QLOG.debug(TAG, "ex: " + e.toString());
        }
        return "";
    }

    public static String getOSVersion() {
        try {
            String str = Build.VERSION.RELEASE;
            return str == null ? "" : str;
        } catch (Exception e) {
            Exception exc = e;
            String str2 = "";
            QLOG.error(TAG, exc);
            return str2;
        }
    }

    public static String getPackName(Context context) {
        return context != null ? context.getPackageName() : "";
    }

    public static String getRandId() {
        int nextInt = new Random().nextInt();
        if (nextInt <= 0 || nextInt > 1000) {
            nextInt = 1000;
        }
        long currentTimeMillis = System.currentTimeMillis() * ((long) nextInt);
        long nextLong = new Random().nextLong();
        if (nextLong < 0) {
            nextLong = -nextLong;
        }
        String str = "000000" + Long.toHexString(nextLong);
        if (str.length() > 7) {
            str = str.substring(str.length() - 7);
        }
        String str2 = Long.toHexString(currentTimeMillis) + str;
        if (str2.length() > 18) {
            str2 = str2.substring(str2.length() - 18);
        }
        return str2.toUpperCase();
    }

    public static String getRegisterId() {
        return registerId;
    }

    public static String getRegisterId(Context context, String str, String str2) {
        try {
            if (TextUtils.isEmpty(registerId)) {
                registerId = registerSdId(context, null);
                if (TextUtils.isEmpty(registerId)) {
                    registerId = QPreference.getPreference(context, "registerId", "");
                    if (TextUtils.isEmpty(registerId)) {
                        String imei = getImei(context);
                        String oSVersion = getOSVersion();
                        String model = getModel();
                        String sn = getSN(context);
                        String androidId = getAndroidId(context);
                        String metaData = getMetaData(context, "QHOPENSDK_APPKEY");
                        String metaData2 = getMetaData(context, "QHOPENSDK_CHANNEL");
                        QLOG.debug(TAG, "*****************************");
                        QLOG.debug(TAG, "packName: " + str2);
                        QLOG.debug(TAG, "imei: " + imei);
                        QLOG.debug(TAG, "osVersion: " + oSVersion);
                        QLOG.debug(TAG, "model: " + model);
                        QLOG.debug(TAG, "sn: " + sn);
                        QLOG.debug(TAG, "androidId: " + androidId);
                        QLOG.debug(TAG, "appKey: " + metaData);
                        QLOG.debug(TAG, "channel: " + metaData2);
                        QLOG.debug(TAG, "appId: " + str);
                        registerId = MD5(str2 + imei + oSVersion + model + sn + androidId + metaData + str + getRandId());
                        QLOG.debug(TAG, "registerId: " + registerId);
                        QPreference.setPreference(context, "registerId", registerId);
                    }
                    registerSdId(context, registerId);
                } else {
                    QPreference.setPreference(context, "registerId", registerId);
                }
            }
        } catch (Exception e) {
            QLOG.error(TAG, e);
        } catch (Error e2) {
            QLOG.error(TAG, e2);
        }
        return registerId;
    }

    public static String getSN(Context context) {
        String simSerialNumber;
        return (context == null || (simSerialNumber = ((TelephonyManager) context.getSystemService("phone")).getSimSerialNumber()) == null) ? "" : simSerialNumber;
    }

    public static String getSSID(Context context) {
        try {
            String ssid = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getSSID();
            return ssid == null ? "" : ssid;
        } catch (Exception e) {
            Exception exc = e;
            String str = "";
            QLOG.debug(TAG, "ex: " + exc.toString());
            return str;
        }
    }

    public static String getTime() {
        try {
            return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
        } catch (Exception e) {
            QLOG.error(TAG, e);
            return "";
        } catch (Error e2) {
            QLOG.error(TAG, e2);
            return "";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x0015  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getVersion(android.content.Context r4) {
        /*
            java.lang.String r1 = ""
            if (r4 == 0) goto L_0x002e
            android.content.pm.PackageManager r0 = r4.getPackageManager()     // Catch:{ Exception -> 0x0028 }
            java.lang.String r2 = r4.getPackageName()     // Catch:{ Exception -> 0x0028 }
            r3 = 0
            android.content.pm.PackageInfo r0 = r0.getPackageInfo(r2, r3)     // Catch:{ Exception -> 0x0028 }
            java.lang.String r0 = r0.versionName     // Catch:{ Exception -> 0x0028 }
        L_0x0013:
            if (r0 != 0) goto L_0x0017
            java.lang.String r0 = ""
        L_0x0017:
            java.lang.String r1 = " "
            java.lang.String r2 = ""
            java.lang.String r0 = r0.replaceAll(r1, r2)
            java.lang.String r1 = ","
            java.lang.String r2 = ""
            java.lang.String r0 = r0.replace(r1, r2)
            return r0
        L_0x0028:
            r0 = move-exception
            java.lang.String r2 = "QDefine"
            com.qihoo.messenger.util.QLOG.error(r2, r0)
        L_0x002e:
            r0 = r1
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo.messenger.util.QDefine.getVersion(android.content.Context):java.lang.String");
    }

    public static boolean isWiFi(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && "WIFI".equalsIgnoreCase(activeNetworkInfo.getTypeName());
    }

    public static boolean networkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null || connectivityManager.getActiveNetworkInfo() == null) {
            return false;
        }
        return connectivityManager.getActiveNetworkInfo().isAvailable();
    }

    public static String registerSdId(Context context, String str) {
        try {
            if ("mounted".equals(Environment.getExternalStorageState())) {
                String str2 = Environment.getExternalStorageDirectory().getPath() + "/data/com/" + QUtil.BRANCH_ID + "/regId/";
                File file = new File(str2);
                if (!file.exists()) {
                    file.mkdirs();
                }
                String str3 = str2 + context.getPackageName();
                if (str != null) {
                    QUtil.saveFile(str3, str);
                    return "";
                }
                String fileContent = QUtil.getFileContent(str3);
                QLOG.debug(TAG, "fileName: " + str3 + ", regId: " + fileContent);
                return fileContent;
            }
            QLOG.debug(TAG, "SD card is oos");
            return "";
        } catch (Exception e) {
            QLOG.error(TAG, e);
            return "";
        } catch (Error e2) {
            QLOG.error(TAG, e2);
            return "";
        }
    }
}
