package com.qihoo.messenger.util;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class FormatUtil {
    public static byte[] int2Stream(int i) {
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.order(ByteOrder.BIG_ENDIAN);
        allocate.putInt(i);
        allocate.flip();
        return allocate.array();
    }

    public static byte[] long2Stream(long j) {
        ByteBuffer allocate = ByteBuffer.allocate(8);
        allocate.order(ByteOrder.BIG_ENDIAN);
        allocate.putLong(j);
        allocate.flip();
        return allocate.array();
    }

    public static byte[] short2Stream(short s) {
        ByteBuffer allocate = ByteBuffer.allocate(2);
        allocate.order(ByteOrder.BIG_ENDIAN);
        allocate.putShort(s);
        allocate.flip();
        return allocate.array();
    }

    public static int stream2Int(byte[] bArr, int i) {
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.order(ByteOrder.BIG_ENDIAN);
        allocate.put(bArr[i]);
        allocate.put(bArr[i + 1]);
        allocate.put(bArr[i + 2]);
        allocate.put(bArr[i + 3]);
        return allocate.getInt(0);
    }

    public static long stream2Long(byte[] bArr, int i) {
        ByteBuffer allocate = ByteBuffer.allocate(8);
        allocate.order(ByteOrder.BIG_ENDIAN);
        allocate.put(bArr[i]);
        allocate.put(bArr[i + 1]);
        allocate.put(bArr[i + 2]);
        allocate.put(bArr[i + 3]);
        allocate.put(bArr[i + 4]);
        allocate.put(bArr[i + 5]);
        allocate.put(bArr[i + 6]);
        allocate.put(bArr[i + 7]);
        return allocate.getLong(0);
    }

    public static short stream2Short(byte[] bArr, int i) {
        ByteBuffer allocate = ByteBuffer.allocate(2);
        allocate.order(ByteOrder.BIG_ENDIAN);
        allocate.put(bArr[i]);
        allocate.put(bArr[i + 1]);
        return allocate.getShort(0);
    }
}
