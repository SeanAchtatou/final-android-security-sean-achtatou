package com.qihoo.messenger.util;

import android.content.Context;
import android.text.TextUtils;
import org.json.JSONObject;

public class QHeader {
    private static String TAG = "QHeader";
    public static final String os = "Android";
    private String alias = "";
    private String appId = "";
    private String appKey = "";
    private String appName = "";
    private String board = "";
    private String brand = "";
    private String channel = "";
    private String country = "";
    private String cpu = "";
    private String imei = "";
    private String language = "";
    private String lastVersion = "";
    private String m1 = "";
    private String m2 = "";
    private String mTag = "";
    private String mac = "";
    private String manufacturer = "";
    private String model = "";
    private String netType = "";
    private String operator = "";
    private String osVersion = "";
    private String packname = "";
    private String registerId = "";
    private String rid = "";
    private String sTag = "";
    private String screen = "";
    private String tags = "";
    private String tid = "";
    private long ttimes = 0;
    private String versionCode = "";
    private String versionName = "";

    protected QHeader(Context context, String str) {
        this.appName = QUtil.getApplicationName(context);
        this.appKey = QUtil.getMetaData(context, "QHOPENSDK_APPKEY");
        this.appId = QUtil.getMetaData(context, "QHOPENSDK_APPID");
        this.channel = QUtil.getMetaData(context, "QHOPENSDK_CHANNEL");
        this.packname = QDefine.getPackName(context);
        this.versionName = QUtil.getVersionName(context);
        this.versionCode = QUtil.getVersionCode(context);
        this.netType = QUtil.getNetType(context);
        this.language = QUtil.getLanguage();
        this.country = QUtil.getCountry();
        this.model = QUtil.getDeviceModel();
        this.imei = QUtil.getImei(context);
        this.operator = QUtil.getOperator(context);
        this.screen = QUtil.getScreen(context);
        this.manufacturer = QUtil.getDeviceManufacturer();
        this.osVersion = QUtil.getDeviceOSVersion();
        this.cpu = QUtil.getCpuName();
        this.lastVersion = QData.getPreferenceString(context, "lastVersion", "");
        this.rid = QUtil.refreshRid(context);
        this.tid = QUtil.getTransferId();
        this.ttimes = QUtil.getTtimes(context, this.rid, this.channel, this.packname);
        this.board = QUtil.getDeviceBoard();
        this.brand = QUtil.getDeviceBrand();
        this.mac = QUtil.getMac(context);
        this.mTag = QSession.PRODUCT;
        this.sTag = QSession.PRODUCT;
        this.registerId = str;
        this.m1 = QUtil.getAndroidImeiMd5(context);
        this.m2 = QUtil.getAndroidDeviceMd5(context);
    }

    /* access modifiers changed from: protected */
    public JSONObject convertJson() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("appName", this.appName);
            jSONObject.put("appKey", this.appKey);
            jSONObject.put("appId", this.appId);
            jSONObject.put("channel", this.channel);
            jSONObject.put("packname", this.packname);
            jSONObject.put("versionName", this.versionName);
            jSONObject.put("versionCode", this.versionCode);
            if (!TextUtils.isEmpty(this.lastVersion)) {
                jSONObject.put("lastVersion", this.lastVersion);
            }
            jSONObject.put("netType", this.netType);
            jSONObject.put("language", this.language);
            jSONObject.put("country", this.country);
            jSONObject.put("model", this.model);
            jSONObject.put("imei", this.imei);
            jSONObject.put("operator", this.operator);
            jSONObject.put("screen", this.screen);
            jSONObject.put("manufacturer", this.manufacturer);
            jSONObject.put("osVersion", this.osVersion);
            jSONObject.put("sdkVersion", QSession.sdkVersion);
            jSONObject.put("os", os);
            jSONObject.put("cpu", this.cpu);
            jSONObject.put("rid", this.rid);
            jSONObject.put("tid", this.tid);
            jSONObject.put("ttimes", this.ttimes);
            if (!TextUtils.isEmpty(this.mac)) {
                jSONObject.put("mac", this.mac);
            }
            jSONObject.put("board", this.board);
            jSONObject.put("brand", this.brand);
            jSONObject.put("mTag", this.mTag);
            jSONObject.put("sTag", this.sTag);
            jSONObject.put("m1", this.m1);
            jSONObject.put("m2", this.m2);
            jSONObject.put("registerId", this.registerId);
            if (!TextUtils.isEmpty(this.alias)) {
                jSONObject.put("alias", this.alias);
            }
            if (!TextUtils.isEmpty(this.tags)) {
                jSONObject.put("tags", this.tags);
            }
        } catch (Exception e) {
            QLOG.error(TAG, e);
        } catch (Error e2) {
            QLOG.error(TAG, e2);
        }
        return jSONObject;
    }

    /* access modifiers changed from: protected */
    public void print() {
        QLOG.debug(TAG, "sdkVersion: 1.0.0");
        QLOG.debug(TAG, "os: Android");
        QLOG.debug(TAG, "appName: " + this.appName);
        QLOG.debug(TAG, "appKey: " + this.appKey);
        QLOG.debug(TAG, "appId: " + this.appId);
        QLOG.debug(TAG, "channel: " + this.channel);
        QLOG.debug(TAG, "packname: " + this.packname);
        QLOG.debug(TAG, "versionName: " + this.versionName);
        QLOG.debug(TAG, "lastVersion: " + this.lastVersion);
        QLOG.debug(TAG, "versionCode: " + this.versionCode);
        QLOG.debug(TAG, "netType: " + this.netType);
        QLOG.debug(TAG, "mac: " + this.mac);
        QLOG.debug(TAG, "language: " + this.language);
        QLOG.debug(TAG, "country: " + this.country);
        QLOG.debug(TAG, "model: " + this.model);
        QLOG.debug(TAG, "board: " + this.board);
        QLOG.debug(TAG, "imei: " + this.imei);
        QLOG.debug(TAG, "operator: " + this.operator);
        QLOG.debug(TAG, "screen: " + this.screen);
        QLOG.debug(TAG, "manufacturer: " + this.manufacturer);
        QLOG.debug(TAG, "osVersion: " + this.osVersion);
        QLOG.debug(TAG, "cpu: " + this.cpu);
        QLOG.debug(TAG, "rid: " + this.rid);
        QLOG.debug(TAG, "tid: " + this.tid);
        QLOG.debug(TAG, "mTag: " + this.mTag);
    }
}
