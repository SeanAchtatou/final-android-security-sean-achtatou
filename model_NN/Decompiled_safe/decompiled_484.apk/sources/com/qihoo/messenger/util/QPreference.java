package com.qihoo.messenger.util;

import android.content.Context;

public class QPreference {
    private static final String PERFERENCES_NAME = "qhpush_session_info";
    private static String TAG = "QPreference";

    public static String getPreference(Context context, String str, String str2) {
        return context.getSharedPreferences(PERFERENCES_NAME, 0).getString(str, str2);
    }

    public static void setPreference(Context context, String str, String str2) {
        context.getSharedPreferences(PERFERENCES_NAME, 0).edit().putString(str, str2).commit();
    }
}
