package com.qihoo.messenger.util;

import java.util.Random;

public final class HttpClient {
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0061  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String get(java.lang.String r6) {
        /*
            r1 = 0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x006a }
            r0.<init>(r6)     // Catch:{ Exception -> 0x006a }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x006a }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x006a }
            r1 = 1
            r0.setDoOutput(r1)     // Catch:{ Exception -> 0x0044, all -> 0x0065 }
            java.lang.String r1 = "GET"
            r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x0044, all -> 0x0065 }
            java.io.OutputStream r1 = r0.getOutputStream()     // Catch:{ Exception -> 0x0044, all -> 0x0065 }
            r1.flush()     // Catch:{ Exception -> 0x0044, all -> 0x0065 }
            r1.close()     // Catch:{ Exception -> 0x0044, all -> 0x0065 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0044, all -> 0x0065 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0044, all -> 0x0065 }
            java.io.InputStream r4 = r0.getInputStream()     // Catch:{ Exception -> 0x0044, all -> 0x0065 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0044, all -> 0x0065 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0044, all -> 0x0065 }
            java.lang.String r3 = ""
        L_0x0034:
            java.lang.String r3 = r1.readLine()     // Catch:{ Exception -> 0x0044, all -> 0x0065 }
            if (r3 == 0) goto L_0x0055
            java.lang.StringBuilder r3 = r2.append(r3)     // Catch:{ Exception -> 0x0044, all -> 0x0065 }
            java.lang.String r4 = "\n"
            r3.append(r4)     // Catch:{ Exception -> 0x0044, all -> 0x0065 }
            goto L_0x0034
        L_0x0044:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0048:
            r0.printStackTrace()     // Catch:{ all -> 0x005e }
            if (r1 == 0) goto L_0x0050
            r1.disconnect()
        L_0x0050:
            java.lang.String r0 = r2.toString()
            return r0
        L_0x0055:
            r1.close()     // Catch:{ Exception -> 0x0044, all -> 0x0065 }
            if (r0 == 0) goto L_0x0050
            r0.disconnect()
            goto L_0x0050
        L_0x005e:
            r0 = move-exception
        L_0x005f:
            if (r1 == 0) goto L_0x0064
            r1.disconnect()
        L_0x0064:
            throw r0
        L_0x0065:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x005f
        L_0x006a:
            r0 = move-exception
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo.messenger.util.HttpClient.get(java.lang.String):java.lang.String");
    }

    public static void main(String[] strArr) {
        String[] split = get("http://md.openapi.360.cn/list/get").split("\n");
        System.out.println(split[new Random().nextInt(split.length)]);
    }
}
