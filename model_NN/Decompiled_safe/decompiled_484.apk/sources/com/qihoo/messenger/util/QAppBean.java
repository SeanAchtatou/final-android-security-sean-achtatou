package com.qihoo.messenger.util;

import org.json.JSONObject;

public class QAppBean {
    private static final String TAG = "QAppBean";
    public int apkSize = 0;
    public String downloadUrl = "";
    public String logoUrl = "";
    public String msgId = "";
    public String pname = "";
    public String softId = "";
    public String softName = "";
    public int versionCode = 0;
    public String versionName = "";

    public QAppBean() {
    }

    public QAppBean(String str) {
        parser(str);
    }

    private void parser(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (!jSONObject.isNull("si")) {
                this.softId = jSONObject.getString("si");
            }
            if (!jSONObject.isNull("sn")) {
                this.softName = jSONObject.getString("sn");
            }
            if (!jSONObject.isNull("lu")) {
                this.logoUrl = jSONObject.getString("lu");
            }
            if (!jSONObject.isNull("pn")) {
                this.pname = jSONObject.getString("pn");
            }
            if (!jSONObject.isNull("vc")) {
                this.versionCode = jSONObject.getInt("vc");
            }
            if (!jSONObject.isNull("vn")) {
                this.versionName = jSONObject.getString("vn");
            }
            if (!jSONObject.isNull("as")) {
                this.apkSize = jSONObject.getInt("as");
            }
            if (!jSONObject.isNull("du")) {
                this.downloadUrl = jSONObject.getString("du");
            }
        } catch (Exception e) {
            QLOG.error(TAG, e);
        } catch (Error e2) {
            QLOG.error(TAG, e2);
        }
    }

    public void print() {
        QLOG.debug(TAG, "softId: " + this.softId);
        QLOG.debug(TAG, "softName: " + this.softName);
        QLOG.debug(TAG, "logoUrl: " + this.logoUrl);
        QLOG.debug(TAG, "pname: " + this.pname);
        QLOG.debug(TAG, "versionCode: " + this.versionCode);
        QLOG.debug(TAG, "versionName: " + this.versionName);
        QLOG.debug(TAG, "apkSize: " + this.apkSize);
        QLOG.debug(TAG, "downloadUrl: " + this.downloadUrl);
    }
}
