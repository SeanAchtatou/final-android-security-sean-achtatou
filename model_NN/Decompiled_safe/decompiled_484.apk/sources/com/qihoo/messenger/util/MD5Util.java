package com.qihoo.messenger.util;

public class MD5Util {
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v9, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v13, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v14, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String encode(java.lang.String r5) {
        /*
            java.lang.String r0 = "MD5"
            java.security.MessageDigest r0 = java.security.MessageDigest.getInstance(r0)     // Catch:{ Exception -> 0x003c }
            byte[] r1 = r5.getBytes()     // Catch:{ Exception -> 0x003c }
            r0.update(r1)     // Catch:{ Exception -> 0x003c }
            byte[] r2 = r0.digest()     // Catch:{ Exception -> 0x003c }
            java.lang.StringBuffer r3 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x003c }
            java.lang.String r0 = ""
            r3.<init>(r0)     // Catch:{ Exception -> 0x003c }
            r0 = 0
            r1 = r0
        L_0x001a:
            int r0 = r2.length     // Catch:{ Exception -> 0x003c }
            if (r1 >= r0) goto L_0x0037
            byte r0 = r2[r1]     // Catch:{ Exception -> 0x003c }
            if (r0 >= 0) goto L_0x0023
            int r0 = r0 + 256
        L_0x0023:
            r4 = 16
            if (r0 >= r4) goto L_0x002c
            java.lang.String r4 = "0"
            r3.append(r4)     // Catch:{ Exception -> 0x003c }
        L_0x002c:
            java.lang.String r0 = java.lang.Integer.toHexString(r0)     // Catch:{ Exception -> 0x003c }
            r3.append(r0)     // Catch:{ Exception -> 0x003c }
            int r0 = r1 + 1
            r1 = r0
            goto L_0x001a
        L_0x0037:
            java.lang.String r0 = r3.toString()     // Catch:{ Exception -> 0x003c }
        L_0x003b:
            return r0
        L_0x003c:
            r0 = move-exception
            r0 = 0
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo.messenger.util.MD5Util.encode(java.lang.String):java.lang.String");
    }
}
