package com.qihoo.messenger.util;

import android.content.Context;
import android.widget.Toast;
import java.util.HashMap;
import java.util.Vector;

public class QSession {
    public static final String CONF_DOMAIN = "md.openapi.360.cn";
    public static final String PRODUCT = "iot";
    /* access modifiers changed from: private */
    public static String TAG = "QSession";
    public static boolean bDebug = true;
    public static Context ctx = null;
    public static Vector<QAppBean> downAppVector = new Vector<>();
    protected static final String mTag = "iot";
    protected static final String sTag = "iot";
    public static final String sdkVersion = "1.0.0";

    public static void init(Context context) {
        try {
            if (ctx == null) {
                ctx = context;
                if (bDebug) {
                    Toast.makeText(ctx, "您使用的是Debug版的360推送SDK v1.0.0", 1).show();
                }
                QUtil.checkPermission(ctx);
                new Thread() {
                    public void run() {
                        boolean z;
                        boolean z2 = true;
                        QLOG.debug(QSession.TAG, "Startup the QSession.");
                        while (QSession.ctx != null) {
                            if (z2) {
                                try {
                                    sleep(30000);
                                    z = false;
                                } catch (Exception e) {
                                    e = e;
                                    z2 = false;
                                    QLOG.error(QSession.TAG, e);
                                } catch (Error e2) {
                                    e = e2;
                                    z2 = false;
                                    QLOG.error(QSession.TAG, e);
                                }
                            } else {
                                try {
                                    sleep(1800000);
                                    z = z2;
                                } catch (Exception e3) {
                                    e = e3;
                                    QLOG.error(QSession.TAG, e);
                                } catch (Error e4) {
                                    e = e4;
                                    QLOG.error(QSession.TAG, e);
                                }
                            }
                            int i = 0;
                            while (true) {
                                try {
                                    if (i >= QSession.downAppVector.size()) {
                                        break;
                                    }
                                    QAppBean qAppBean = QSession.downAppVector.get(i);
                                    if (qAppBean.versionCode == QUtil.getVersionCode(QSession.ctx, qAppBean.pname)) {
                                        new HashMap().put("msgId", qAppBean.msgId);
                                        QSession.downAppVector.remove(i);
                                        break;
                                    }
                                    i++;
                                } catch (Exception e5) {
                                    Exception exc = e5;
                                    z2 = z;
                                    e = exc;
                                    QLOG.error(QSession.TAG, e);
                                } catch (Error e6) {
                                    Error error = e6;
                                    z2 = z;
                                    e = error;
                                    QLOG.error(QSession.TAG, e);
                                }
                            }
                            z2 = z;
                        }
                        QLOG.warn(QSession.TAG, "Warning, the QSession is stoppped!");
                    }
                }.start();
            }
        } catch (Exception e) {
            QLOG.error(TAG, e);
        } catch (Error e2) {
            QLOG.error(TAG, e2);
        }
    }
}
