package com.qihoo.messenger.util;

import android.content.Context;
import android.content.SharedPreferences;
import org.json.JSONObject;

public class QData {
    private static final String PERFERENCES_NAME = "qhopen_game_session_info";
    private static String TAG = "QData";

    public static String getChannel(Context context, String str) {
        return context.getSharedPreferences(PERFERENCES_NAME, 0).getString("channel", str);
    }

    public static long getPreferenceLong(Context context, String str, long j) {
        return context.getSharedPreferences(PERFERENCES_NAME, 0).getLong(str, j);
    }

    public static String getPreferenceString(Context context, String str, String str2) {
        return context.getSharedPreferences(PERFERENCES_NAME, 0).getString(str, str2);
    }

    public static String getRegisterLog(Context context, String str) {
        String str2 = "";
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("header", new QHeader(context, str).convertJson());
            str2 = jSONObject.toString();
            return QUtil.encode(str2);
        } catch (Exception e) {
            Exception exc = e;
            String str3 = str2;
            QLOG.error(TAG, exc);
            return str3;
        } catch (Error e2) {
            Error error = e2;
            String str4 = str2;
            QLOG.error(TAG, error);
            return str4;
        }
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:31:0x009a=Splitter:B:31:0x009a, B:24:0x008d=Splitter:B:24:0x008d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized java.lang.String mergeLogData(android.content.Context r8, java.lang.String r9, java.lang.String r10) {
        /*
            r6 = 0
            java.lang.Class<com.qihoo.messenger.util.QData> r2 = com.qihoo.messenger.util.QData.class
            monitor-enter(r2)
            java.lang.String r1 = ""
            boolean r0 = android.text.TextUtils.isEmpty(r10)     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            if (r0 == 0) goto L_0x000e
            java.lang.String r10 = com.qihoo.messenger.util.QData.TAG     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
        L_0x000e:
            java.lang.String r0 = com.qihoo.messenger.util.QUtil.getLogFile(r8)     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            java.lang.String r0 = com.qihoo.messenger.util.QUtil.getFileContent(r0)     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            com.qihoo.messenger.util.QUtil.decode(r0)     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            r0.<init>()     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            r3 = 0
            r4 = 0
            if (r6 == 0) goto L_0x0031
            int r5 = r4.length()     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            if (r5 <= 0) goto L_0x0031
            if (r6 == 0) goto L_0x0031
            java.lang.String r5 = com.qihoo.messenger.util.QUtil.generateSession(r8)     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            r3.put(r5, r4)     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
        L_0x0031:
            if (r6 == 0) goto L_0x003e
            int r4 = r3.length()     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            if (r4 <= 0) goto L_0x003e
            java.lang.String r4 = "event"
            r0.put(r4, r3)     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
        L_0x003e:
            com.qihoo.messenger.util.QHeader r3 = new com.qihoo.messenger.util.QHeader     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            r3.<init>(r8, r9)     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            org.json.JSONObject r3 = r3.convertJson()     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            java.lang.String r4 = "header"
            r0.put(r4, r3)     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            java.lang.String r1 = r0.toString()     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            java.lang.String r3 = com.qihoo.messenger.util.QUtil.getLogFile(r8)     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            r0.<init>()     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            java.lang.String r4 = "logFile: "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            com.qihoo.messenger.util.QLOG.debug(r10, r0)     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            r0.<init>()     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            java.lang.String r4 = "logData: "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            com.qihoo.messenger.util.QLOG.debug(r10, r0)     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            java.lang.String r0 = com.qihoo.messenger.util.QUtil.encode(r1)     // Catch:{ Exception -> 0x0089, Error -> 0x0096 }
            com.qihoo.messenger.util.QUtil.saveFile(r3, r0)     // Catch:{ Exception -> 0x00a2, Error -> 0x00a0 }
        L_0x0087:
            monitor-exit(r2)
            return r0
        L_0x0089:
            r0 = move-exception
            r7 = r0
            r0 = r1
            r1 = r7
        L_0x008d:
            java.lang.String r3 = com.qihoo.messenger.util.QData.TAG     // Catch:{ all -> 0x0093 }
            com.qihoo.messenger.util.QLOG.error(r3, r1)     // Catch:{ all -> 0x0093 }
            goto L_0x0087
        L_0x0093:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x0096:
            r0 = move-exception
            r7 = r0
            r0 = r1
            r1 = r7
        L_0x009a:
            java.lang.String r3 = com.qihoo.messenger.util.QData.TAG     // Catch:{ all -> 0x0093 }
            com.qihoo.messenger.util.QLOG.error(r3, r1)     // Catch:{ all -> 0x0093 }
            goto L_0x0087
        L_0x00a0:
            r1 = move-exception
            goto L_0x009a
        L_0x00a2:
            r1 = move-exception
            goto L_0x008d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo.messenger.util.QData.mergeLogData(android.content.Context, java.lang.String, java.lang.String):java.lang.String");
    }

    public static void setChannel(Context context, String str) {
        context.getSharedPreferences(PERFERENCES_NAME, 0).edit().putString("channel", str).commit();
    }

    public static void setPreference(Context context, String str, long j) {
        context.getSharedPreferences(PERFERENCES_NAME, 0).edit().putLong(str, j).commit();
    }

    public static void setPreference(Context context, String str, String str2) {
        context.getSharedPreferences(PERFERENCES_NAME, 0).edit().putString(str, str2).commit();
    }

    public static void setTodaySession(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PERFERENCES_NAME, 0);
        String preferenceString = getPreferenceString(context, "day", "");
        String currentDay = QUtil.getCurrentDay();
        if (currentDay.equals(preferenceString)) {
            sharedPreferences.edit().putLong("todaySession", sharedPreferences.getLong("todaySession", 0) + 1).commit();
            return;
        }
        sharedPreferences.edit().putString("day", currentDay).commit();
        sharedPreferences.edit().putLong("todaySession", 1).commit();
    }

    public static void setTotalSession(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PERFERENCES_NAME, 0);
        sharedPreferences.edit().putLong("totalSession", sharedPreferences.getLong("totalSession", 0) + 1).commit();
    }
}
