package com.qihoo.messenger.util;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.Settings;
import android.support.v4.media.session.PlaybackStateCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.widget.Toast;
import com.qihoo.dynamic.util.Md5Util;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.security.MessageDigest;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

public class QUtil {
    private static String ANDROID_DEVICE_MD5 = null;
    private static String ANDROID_ID = null;
    private static String ANDROID_IMEI_MD5 = null;
    public static final String BRANCH_ID = "qhpush";
    private static final String GLOBAL_RID_PATH = "/data/com/qihoo/stat/";
    private static final char[] SECRET = {'d', 'r', 'e', 'a', 'm', '3', '6', '0', '@', 'c', 'h', 'i', 'n', 'a'};
    private static String SN = null;
    private static String TAG = "QUtil";
    private static ActivityManager am = null;
    private static boolean bWifiAccess = false;
    private static ComponentName cn = null;
    private static final String[] pList = {"READ_PHONE_STATE", "INTERNET", "ACCESS_NETWORK_STATE", "GET_TASKS", "WRITE_EXTERNAL_STORAGE"};
    private static String pn = null;
    private static List<ActivityManager.RunningAppProcessInfo> rap = null;

    protected static String MD5(String str) {
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        try {
            byte[] bytes = str.getBytes();
            MessageDigest instance = MessageDigest.getInstance(Md5Util.ALGORITHM);
            instance.update(bytes);
            char[] cArr2 = new char[(r4 * 2)];
            int i = 0;
            for (byte b2 : instance.digest()) {
                int i2 = i + 1;
                cArr2[i] = cArr[(b2 >>> 4) & 15];
                i = i2 + 1;
                cArr2[i2] = cArr[b2 & 15];
            }
            return new String(cArr2);
        } catch (Exception e) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0033 A[SYNTHETIC, Splitter:B:18:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0042 A[SYNTHETIC, Splitter:B:25:0x0042] */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static void appendFile(java.lang.String r5, java.lang.String r6) {
        /*
            r2 = 0
            boolean r0 = android.text.TextUtils.isEmpty(r6)
            if (r0 == 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x002a, all -> 0x003e }
            r0 = 1
            r1.<init>(r5, r0)     // Catch:{ Exception -> 0x002a, all -> 0x003e }
            java.lang.String r0 = "UTF-8"
            byte[] r0 = r6.getBytes(r0)     // Catch:{ Exception -> 0x004f }
            r3 = 0
            int r4 = r0.length     // Catch:{ Exception -> 0x004f }
            r1.write(r0, r3, r4)     // Catch:{ Exception -> 0x004f }
            r1.close()     // Catch:{ Exception -> 0x004f }
            r0 = 0
            if (r2 == 0) goto L_0x0007
            r0.close()     // Catch:{ Exception -> 0x0023 }
            goto L_0x0007
        L_0x0023:
            r0 = move-exception
            java.lang.String r1 = com.qihoo.messenger.util.QUtil.TAG
            com.qihoo.messenger.util.QLOG.error(r1, r0)
            goto L_0x0007
        L_0x002a:
            r0 = move-exception
            r1 = r2
        L_0x002c:
            java.lang.String r2 = com.qihoo.messenger.util.QUtil.TAG     // Catch:{ all -> 0x004d }
            com.qihoo.messenger.util.QLOG.error(r2, r0)     // Catch:{ all -> 0x004d }
            if (r1 == 0) goto L_0x0007
            r1.close()     // Catch:{ Exception -> 0x0037 }
            goto L_0x0007
        L_0x0037:
            r0 = move-exception
            java.lang.String r1 = com.qihoo.messenger.util.QUtil.TAG
            com.qihoo.messenger.util.QLOG.error(r1, r0)
            goto L_0x0007
        L_0x003e:
            r0 = move-exception
            r1 = r2
        L_0x0040:
            if (r1 == 0) goto L_0x0045
            r1.close()     // Catch:{ Exception -> 0x0046 }
        L_0x0045:
            throw r0
        L_0x0046:
            r1 = move-exception
            java.lang.String r2 = com.qihoo.messenger.util.QUtil.TAG
            com.qihoo.messenger.util.QLOG.error(r2, r1)
            goto L_0x0045
        L_0x004d:
            r0 = move-exception
            goto L_0x0040
        L_0x004f:
            r0 = move-exception
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo.messenger.util.QUtil.appendFile(java.lang.String, java.lang.String):void");
    }

    protected static void checkPermission(Context context) {
        boolean z;
        try {
            String[] strArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions;
            int i = 0;
            while (true) {
                if (i >= pList.length) {
                    break;
                }
                if (strArr != null) {
                    int i2 = 0;
                    while (true) {
                        if (i2 >= strArr.length) {
                            break;
                        } else if (strArr[i2].replace("android.permission.", "").equals(pList[i])) {
                            z = true;
                            break;
                        } else {
                            i2++;
                        }
                    }
                }
                z = false;
                if (!z) {
                    Toast.makeText(context, "您的程序没有声明android.permission." + pList[i] + "权限", 1).show();
                    break;
                }
                i++;
            }
            if (strArr != null) {
                for (String replace : strArr) {
                    if (replace.replace("android.permission.", "").equals("ACCESS_WIFI_STATE")) {
                        bWifiAccess = true;
                        return;
                    }
                }
            }
        } catch (Exception e) {
            QLOG.error(TAG, e);
        }
    }

    protected static String decode(String str) {
        byte[] decode = Base64.decode(str, 11);
        for (int i = 0; i < decode.length; i++) {
            decode[i] = (byte) (decode[i] ^ SECRET[i % SECRET.length]);
        }
        return new String(decode);
    }

    protected static String encode(String str) {
        try {
            byte[] bytes = str.getBytes();
            for (int i = 0; i < bytes.length; i++) {
                bytes[i] = (byte) (bytes[i] ^ SECRET[i % SECRET.length]);
            }
            return Base64.encodeToString(bytes, 11);
        } catch (Exception e) {
            QLOG.error(TAG, e);
            return "";
        }
    }

    protected static String generateSession(Context context) {
        int nextInt = new Random().nextInt();
        if (nextInt < 0) {
            nextInt = -nextInt;
        }
        String str = "000" + nextInt;
        if (str.length() > 4) {
            str = str.substring(str.length() - 4);
        }
        long currentTimeMillis = System.currentTimeMillis();
        String lowerCase = MD5(getMetaData(context, "QHOPENSDK_APPKEY") + currentTimeMillis + getImei(context) + str).toLowerCase();
        QLOG.debug(TAG, "Generate session: " + lowerCase + ", createTime: " + (currentTimeMillis / 1000));
        return lowerCase;
    }

    public static String getAndroidDeviceMd5(Context context) {
        try {
            if (ANDROID_DEVICE_MD5 == null) {
                ANDROID_DEVICE_MD5 = MD5(getImei(context) + getSerialNumber() + getAndroidId(context.getContentResolver())).toLowerCase();
            }
        } catch (Exception e) {
            QLOG.error(TAG, e);
        }
        return ANDROID_DEVICE_MD5;
    }

    private static String getAndroidId(ContentResolver contentResolver) {
        if (ANDROID_ID == null) {
            ANDROID_ID = Settings.System.getString(contentResolver, "android_id");
        }
        return ANDROID_ID;
    }

    public static String getAndroidImeiMd5(Context context) {
        try {
            if (ANDROID_IMEI_MD5 == null && context != null) {
                ANDROID_IMEI_MD5 = MD5(getImei(context)).toLowerCase();
            }
        } catch (Exception e) {
            QLOG.error(TAG, e);
        }
        return ANDROID_IMEI_MD5;
    }

    protected static int getAppStatus(Context context) {
        try {
            if (am == null) {
                am = (ActivityManager) context.getSystemService("activity");
            }
            cn = am.getRunningTasks(1).get(0).topActivity;
            if (pn == null) {
                pn = context.getPackageName();
            }
            if (cn.getPackageName().equals(pn)) {
                return 100;
            }
        } catch (Exception e) {
            QLOG.error(TAG, e);
        }
        return -1;
    }

    public static String getApplicationName(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            return (String) packageManager.getApplicationLabel(packageManager.getApplicationInfo(context.getPackageName(), 0));
        } catch (Exception e) {
            QLOG.error(TAG, e);
            return "";
        } catch (Error e2) {
            QLOG.error(TAG, e2);
            return "";
        }
    }

    protected static String getContextActivityName(Context context) {
        try {
            String obj = context.toString();
            return obj.substring(obj.lastIndexOf(".") + 1, obj.indexOf("@"));
        } catch (Exception e) {
            QLOG.error(TAG, e);
            return "";
        }
    }

    protected static String getCountry() {
        return Locale.getDefault().getCountry();
    }

    protected static String getCpuName() {
        Error e;
        String str;
        Exception e2;
        try {
            FileReader fileReader = new FileReader("/proc/cpuinfo");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            str = bufferedReader.readLine().split(":\\s+", 2)[1];
            try {
                bufferedReader.close();
                fileReader.close();
            } catch (Exception e3) {
                e2 = e3;
                QLOG.error(TAG, e2);
                return str;
            } catch (Error e4) {
                e = e4;
                QLOG.error(TAG, e);
                return str;
            }
        } catch (Exception e5) {
            Exception exc = e5;
            str = "";
            e2 = exc;
            QLOG.error(TAG, e2);
            return str;
        } catch (Error e6) {
            Error error = e6;
            str = "";
            e = error;
            QLOG.error(TAG, e);
            return str;
        }
        return str;
    }

    protected static String getCurrentActivity(Context context) {
        try {
            return ((ActivityManager) context.getSystemService("activity")).getRunningTasks(1).get(0).topActivity.getShortClassName().replace(".", "");
        } catch (Exception e) {
            QLOG.error(TAG, e);
            return "";
        }
    }

    protected static String getCurrentDay() {
        Date date = new Date();
        int month = date.getMonth() + 1;
        int date2 = date.getDate();
        String str = "" + (date.getYear() + 1900);
        String str2 = month < 10 ? str + "0" + month : str + month;
        return date2 < 10 ? str2 + "0" + date2 : str2 + date2;
    }

    private static String getCurrentTime() {
        Date date = new Date();
        int month = date.getMonth() + 1;
        int date2 = date.getDate();
        int hours = date.getHours();
        int minutes = date.getMinutes();
        int seconds = date.getSeconds();
        String str = "" + (date.getYear() + 1900);
        String str2 = month < 10 ? str + "0" + month : str + month;
        String str3 = date2 < 10 ? str2 + "0" + date2 : str2 + date2;
        String str4 = hours < 10 ? str3 + "0" + hours : str3 + hours;
        String str5 = minutes < 10 ? str4 + "0" + minutes : str4 + minutes;
        return seconds < 10 ? str5 + "0" + seconds : str5 + seconds;
    }

    protected static String getDeviceBoard() {
        try {
            String str = Build.BOARD;
            return str == null ? "" : str;
        } catch (Exception e) {
            Exception exc = e;
            String str2 = "";
            QLOG.error(TAG, exc);
            return str2;
        }
    }

    protected static String getDeviceBrand() {
        try {
            String str = Build.BRAND;
            return str == null ? "" : str;
        } catch (Exception e) {
            Exception exc = e;
            String str2 = "";
            QLOG.error(TAG, exc);
            return str2;
        }
    }

    protected static String getDeviceManufacturer() {
        try {
            String str = Build.MANUFACTURER;
            return str == null ? "" : str;
        } catch (Exception e) {
            Exception exc = e;
            String str2 = "";
            QLOG.error(TAG, exc);
            return str2;
        }
    }

    protected static String getDeviceModel() {
        try {
            String str = Build.MODEL;
            return str == null ? "" : str;
        } catch (Exception e) {
            Exception exc = e;
            String str2 = "";
            QLOG.error(TAG, exc);
            return str2;
        }
    }

    protected static String getDeviceOSVersion() {
        try {
            String str = Build.VERSION.RELEASE;
            return str == null ? "" : str;
        } catch (Exception e) {
            Exception exc = e;
            String str2 = "";
            QLOG.error(TAG, exc);
            return str2;
        }
    }

    protected static String getExceptionFile(Context context) {
        File filesDir = context.getFilesDir();
        if (!filesDir.exists()) {
            filesDir.mkdirs();
        }
        return filesDir.getPath() + "/" + BRANCH_ID + "_game_stat_ex.log";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0063, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0064, code lost:
        com.qihoo.messenger.util.QLOG.error(com.qihoo.messenger.util.QUtil.TAG, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0071, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0072, code lost:
        com.qihoo.messenger.util.QLOG.error(com.qihoo.messenger.util.QUtil.TAG, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0078, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0079, code lost:
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0081, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0082, code lost:
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        return r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x005f A[SYNTHETIC, Splitter:B:31:0x005f] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x006d A[SYNTHETIC, Splitter:B:37:0x006d] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0078 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:12:0x0022] */
    /* JADX WARNING: Removed duplicated region for block: B:52:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getFileContent(java.lang.String r11) {
        /*
            r2 = 0
            java.lang.String r1 = ""
            r0 = 0
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x0054 }
            r4.<init>(r11)     // Catch:{ Exception -> 0x0054 }
            boolean r3 = r4.exists()     // Catch:{ Exception -> 0x0054 }
            if (r3 != 0) goto L_0x001d
            if (r2 == 0) goto L_0x0014
            r0.close()     // Catch:{ Exception -> 0x0016 }
        L_0x0014:
            r0 = r1
        L_0x0015:
            return r0
        L_0x0016:
            r0 = move-exception
            java.lang.String r2 = com.qihoo.messenger.util.QUtil.TAG
            com.qihoo.messenger.util.QLOG.error(r2, r0)
            goto L_0x0014
        L_0x001d:
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0054 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0054 }
            long r6 = r4.length()     // Catch:{ Exception -> 0x007b, all -> 0x0078 }
            int r0 = (int) r6     // Catch:{ Exception -> 0x007b, all -> 0x0078 }
            byte[] r5 = new byte[r0]     // Catch:{ Exception -> 0x007b, all -> 0x0078 }
            r3.read(r5)     // Catch:{ Exception -> 0x007b, all -> 0x0078 }
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x007b, all -> 0x0078 }
            r6 = 0
            int r7 = r5.length     // Catch:{ Exception -> 0x007b, all -> 0x0078 }
            java.lang.String r8 = "UTF-8"
            r0.<init>(r5, r6, r7, r8)     // Catch:{ Exception -> 0x007b, all -> 0x0078 }
            r3.close()     // Catch:{ Exception -> 0x0081, all -> 0x0078 }
            r1 = 0
            long r6 = r4.length()     // Catch:{ Exception -> 0x0084 }
            r8 = 20971520(0x1400000, double:1.03613076E-316)
            int r3 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r3 <= 0) goto L_0x0047
            r4.delete()     // Catch:{ Exception -> 0x0084 }
        L_0x0047:
            if (r2 == 0) goto L_0x0015
            r1.close()     // Catch:{ Exception -> 0x004d }
            goto L_0x0015
        L_0x004d:
            r1 = move-exception
            java.lang.String r2 = com.qihoo.messenger.util.QUtil.TAG
            com.qihoo.messenger.util.QLOG.error(r2, r1)
            goto L_0x0015
        L_0x0054:
            r0 = move-exception
            r10 = r0
            r0 = r1
            r1 = r10
        L_0x0058:
            java.lang.String r3 = com.qihoo.messenger.util.QUtil.TAG     // Catch:{ all -> 0x006a }
            com.qihoo.messenger.util.QLOG.error(r3, r1)     // Catch:{ all -> 0x006a }
            if (r2 == 0) goto L_0x0015
            r2.close()     // Catch:{ Exception -> 0x0063 }
            goto L_0x0015
        L_0x0063:
            r1 = move-exception
            java.lang.String r2 = com.qihoo.messenger.util.QUtil.TAG
            com.qihoo.messenger.util.QLOG.error(r2, r1)
            goto L_0x0015
        L_0x006a:
            r0 = move-exception
        L_0x006b:
            if (r2 == 0) goto L_0x0070
            r2.close()     // Catch:{ Exception -> 0x0071 }
        L_0x0070:
            throw r0
        L_0x0071:
            r1 = move-exception
            java.lang.String r2 = com.qihoo.messenger.util.QUtil.TAG
            com.qihoo.messenger.util.QLOG.error(r2, r1)
            goto L_0x0070
        L_0x0078:
            r0 = move-exception
            r2 = r3
            goto L_0x006b
        L_0x007b:
            r0 = move-exception
            r2 = r3
            r10 = r0
            r0 = r1
            r1 = r10
            goto L_0x0058
        L_0x0081:
            r1 = move-exception
            r2 = r3
            goto L_0x0058
        L_0x0084:
            r1 = move-exception
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo.messenger.util.QUtil.getFileContent(java.lang.String):java.lang.String");
    }

    protected static String getImei(Context context) {
        try {
            String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
            return deviceId == null ? "" : deviceId;
        } catch (Exception e) {
            Exception exc = e;
            String str = "";
            QLOG.error(TAG, exc);
            return str;
        }
    }

    protected static String getLanguage() {
        return Locale.getDefault().getLanguage();
    }

    protected static String getLogFile(Context context) {
        File filesDir = context.getFilesDir();
        if (!filesDir.exists()) {
            filesDir.mkdirs();
        }
        return filesDir.getPath() + "/" + BRANCH_ID + "_game_stat_log.json";
    }

    protected static String getMac(Context context) {
        String str;
        try {
            if (bWifiAccess) {
                WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
                WifiInfo connectionInfo = wifiManager == null ? null : wifiManager.getConnectionInfo();
                if (connectionInfo != null) {
                    str = connectionInfo.getMacAddress();
                    return str;
                }
            }
            str = "";
            return str;
        } catch (Exception e) {
            QLOG.error(TAG, e);
            return "";
        } catch (Error e2) {
            QLOG.error(TAG, e2);
            return "";
        }
    }

    public static String getMetaData(Context context, String str) {
        String str2 = "";
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                try {
                    str2 = "" + applicationInfo.metaData.get(str).toString();
                } catch (Exception e) {
                    QLOG.error(TAG, e);
                }
            }
        } catch (Exception e2) {
            QLOG.error(TAG, e2);
        }
        return "QHOPENSDK_CHANNEL".equals(str) ? QData.getChannel(context, str2) : str2;
    }

    public static boolean getNetAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null || connectivityManager.getActiveNetworkInfo() == null) {
            return false;
        }
        return connectivityManager.getActiveNetworkInfo().isAvailable();
    }

    protected static String getNetType(Context context) {
        Error e;
        String str;
        Exception e2;
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null) {
                return "";
            }
            str = activeNetworkInfo.getTypeName().toLowerCase();
            try {
                return str.equals("wifi") ? str : activeNetworkInfo.getExtraInfo().toLowerCase() + "-" + activeNetworkInfo.getSubtypeName().toLowerCase();
            } catch (Exception e3) {
                e2 = e3;
                QLOG.error(TAG, e2);
                return str;
            } catch (Error e4) {
                e = e4;
                QLOG.error(TAG, e);
                return str;
            }
        } catch (Exception e5) {
            Exception exc = e5;
            str = "";
            e2 = exc;
            QLOG.error(TAG, e2);
            return str;
        } catch (Error e6) {
            Error error = e6;
            str = "";
            e = error;
            QLOG.error(TAG, e);
            return str;
        }
    }

    protected static String getOperator(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getNetworkOperatorName();
        } catch (Exception e) {
            QLOG.error(TAG, e);
            return "";
        }
    }

    protected static String getRandId() {
        int nextInt = new Random().nextInt();
        if (nextInt <= 0 || nextInt > 1000) {
            nextInt = 1000;
        }
        long currentTimeMillis = System.currentTimeMillis() * ((long) nextInt);
        long nextLong = new Random().nextLong();
        if (nextLong < 0) {
            nextLong = -nextLong;
        }
        String str = "000000" + Long.toHexString(nextLong);
        if (str.length() > 7) {
            str = str.substring(str.length() - 7);
        }
        String str2 = Long.toHexString(currentTimeMillis) + str;
        if (str2.length() > 18) {
            str2 = str2.substring(str2.length() - 18);
        }
        return str2.toUpperCase();
    }

    protected static String getScreen(Context context) {
        DisplayMetrics displayMetrics = context.getApplicationContext().getResources().getDisplayMetrics();
        return "" + displayMetrics.widthPixels + "x" + displayMetrics.heightPixels;
    }

    public static long getSeconds() {
        return System.currentTimeMillis() / 1000;
    }

    private static String getSerialNumber() {
        if (SN == null) {
            try {
                Class<?> cls = Class.forName("android.os.SystemProperties");
                SN = (String) cls.getMethod("get", String.class).invoke(cls, "ro.serialno");
            } catch (Exception e) {
                QLOG.error(TAG, e);
                SN = "";
            }
        }
        return SN;
    }

    protected static String getTransferId() {
        int nextInt = new Random().nextInt();
        if (nextInt < 0) {
            nextInt = -nextInt;
        }
        String str = "0000000" + nextInt;
        if (str.length() > 8) {
            str = str.substring(str.length() - 8);
        }
        String str2 = "" + System.currentTimeMillis();
        return getCurrentTime() + (str2.substring(str2.length() - 3) + str);
    }

    protected static long getTtimes(Context context, String str, String str2, String str3) {
        int i;
        long preferenceLong = QData.getPreferenceLong(context, "ttimes", 1);
        int i2 = 1;
        try {
            if ("mounted".equals(Environment.getExternalStorageState())) {
                String fileContent = getFileContent(Environment.getExternalStorageDirectory().getPath() + "/data/com/" + BRANCH_ID + "/stat/" + str + "_" + str2 + "_" + str3);
                if (!TextUtils.isEmpty(fileContent)) {
                    i2 = Integer.parseInt(fileContent);
                }
            }
            i = i2;
        } catch (Exception e) {
            QLOG.error(TAG, e);
            i = 1;
        } catch (Error e2) {
            QLOG.error(TAG, e2);
            i = 1;
        }
        if (((long) i) <= preferenceLong) {
            return preferenceLong;
        }
        long j = (long) i;
        QData.setPreference(context, "ttimes", (long) i);
        return j;
    }

    public static int getVersionCode(Context context, String str) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(str, 0);
            if (packageInfo != null) {
                return packageInfo.versionCode;
            }
            return -1;
        } catch (Exception e) {
            QLOG.error(TAG, e);
            return -1;
        } catch (Error e2) {
            QLOG.error(TAG, e2);
            return -1;
        }
    }

    protected static String getVersionCode(Context context) {
        try {
            return "" + context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (Exception e) {
            QLOG.error(TAG, e);
            return "";
        } catch (Error e2) {
            QLOG.error(TAG, e2);
            return "";
        }
    }

    protected static String getVersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            QLOG.error(TAG, e);
            return "";
        } catch (Error e2) {
            QLOG.error(TAG, e2);
            return "";
        }
    }

    protected static String getZone() {
        return "" + TimeZone.getDefault();
    }

    private static String readSDRid() {
        String str = "";
        try {
            if ("mounted".equals(Environment.getExternalStorageState())) {
                String str2 = Environment.getExternalStorageDirectory().getPath() + GLOBAL_RID_PATH;
                File file = new File(str2);
                if (!file.exists()) {
                    file.mkdirs();
                }
                str = getFileContent(str2 + "randId");
                File file2 = new File(Environment.getExternalStorageDirectory().getPath() + "/data/com/" + BRANCH_ID + "/stat/");
                if (!file2.exists()) {
                    file2.mkdirs();
                }
            }
        } catch (Exception e) {
            QLOG.error(TAG, e);
        } catch (Error e2) {
            QLOG.error(TAG, e2);
        }
        return str;
    }

    public static long readSDStorage() {
        try {
            if ("mounted".equals(Environment.getExternalStorageState())) {
                File externalStorageDirectory = Environment.getExternalStorageDirectory();
                QLOG.debug(TAG, "The sdcard dir: " + externalStorageDirectory.getPath());
                StatFs statFs = new StatFs(externalStorageDirectory.getPath());
                long availableBlocks = ((((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize())) / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID) / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID;
                QLOG.debug(TAG, "Free sdcard size:" + availableBlocks + "MB");
                return availableBlocks;
            }
            QLOG.warn(TAG, "The sdcard does not exist!");
            return 0;
        } catch (Exception e) {
            QLOG.error(TAG, e);
            return 0;
        } catch (Error e2) {
            QLOG.error(TAG, e2);
            return 0;
        }
    }

    protected static String refreshRid(Context context) {
        Error e;
        String str;
        Exception e2;
        try {
            str = readSDRid();
            try {
                if (TextUtils.isEmpty(str)) {
                    String preferenceString = QData.getPreferenceString(context, "randId", "");
                    if (TextUtils.isEmpty(preferenceString)) {
                        preferenceString = getRandId();
                        QData.setPreference(context, "randId", preferenceString);
                    }
                    str = preferenceString;
                    writeSDRid(str);
                }
            } catch (Exception e3) {
                e2 = e3;
                QLOG.error(TAG, e2);
                return str;
            } catch (Error e4) {
                e = e4;
                QLOG.error(TAG, e);
                return str;
            }
        } catch (Exception e5) {
            Exception exc = e5;
            str = "";
            e2 = exc;
        } catch (Error e6) {
            Error error = e6;
            str = "";
            e = error;
            QLOG.error(TAG, e);
            return str;
        }
        return str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0032 A[SYNTHETIC, Splitter:B:18:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0041 A[SYNTHETIC, Splitter:B:25:0x0041] */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void saveFile(java.lang.String r5, java.lang.String r6) {
        /*
            r2 = 0
            boolean r0 = android.text.TextUtils.isEmpty(r6)
            if (r0 == 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0029, all -> 0x003d }
            r1.<init>(r5)     // Catch:{ Exception -> 0x0029, all -> 0x003d }
            java.lang.String r0 = "UTF-8"
            byte[] r0 = r6.getBytes(r0)     // Catch:{ Exception -> 0x004e }
            r3 = 0
            int r4 = r0.length     // Catch:{ Exception -> 0x004e }
            r1.write(r0, r3, r4)     // Catch:{ Exception -> 0x004e }
            r1.close()     // Catch:{ Exception -> 0x004e }
            r0 = 0
            if (r2 == 0) goto L_0x0007
            r0.close()     // Catch:{ Exception -> 0x0022 }
            goto L_0x0007
        L_0x0022:
            r0 = move-exception
            java.lang.String r1 = com.qihoo.messenger.util.QUtil.TAG
            com.qihoo.messenger.util.QLOG.error(r1, r0)
            goto L_0x0007
        L_0x0029:
            r0 = move-exception
            r1 = r2
        L_0x002b:
            java.lang.String r2 = com.qihoo.messenger.util.QUtil.TAG     // Catch:{ all -> 0x004c }
            com.qihoo.messenger.util.QLOG.error(r2, r0)     // Catch:{ all -> 0x004c }
            if (r1 == 0) goto L_0x0007
            r1.close()     // Catch:{ Exception -> 0x0036 }
            goto L_0x0007
        L_0x0036:
            r0 = move-exception
            java.lang.String r1 = com.qihoo.messenger.util.QUtil.TAG
            com.qihoo.messenger.util.QLOG.error(r1, r0)
            goto L_0x0007
        L_0x003d:
            r0 = move-exception
            r1 = r2
        L_0x003f:
            if (r1 == 0) goto L_0x0044
            r1.close()     // Catch:{ Exception -> 0x0045 }
        L_0x0044:
            throw r0
        L_0x0045:
            r1 = move-exception
            java.lang.String r2 = com.qihoo.messenger.util.QUtil.TAG
            com.qihoo.messenger.util.QLOG.error(r2, r1)
            goto L_0x0044
        L_0x004c:
            r0 = move-exception
            goto L_0x003f
        L_0x004e:
            r0 = move-exception
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo.messenger.util.QUtil.saveFile(java.lang.String, java.lang.String):void");
    }

    protected static void saveTtimes(Context context) {
        try {
            long preferenceLong = QData.getPreferenceLong(context, "ttimes", 1) + 1;
            QData.setPreference(context, "ttimes", preferenceLong);
            QData.setPreference(context, "lastVersion", getVersionName(context));
            if ("mounted".equals(Environment.getExternalStorageState())) {
                saveFile(Environment.getExternalStorageDirectory().getPath() + "/data/com/" + BRANCH_ID + "/stat/" + refreshRid(context) + "_" + getMetaData(context, "QHOPENSDK_CHANNEL") + "_" + QDefine.getPackName(context), "" + preferenceLong);
            }
        } catch (Exception e) {
            QLOG.error(TAG, e);
        } catch (Error e2) {
            QLOG.error(TAG, e2);
        }
    }

    private static void writeSDRid(String str) {
        try {
            if ("mounted".equals(Environment.getExternalStorageState())) {
                String str2 = Environment.getExternalStorageDirectory().getPath() + GLOBAL_RID_PATH + "randId";
                if (TextUtils.isEmpty(getFileContent(str2))) {
                    saveFile(str2, str);
                }
            }
        } catch (Exception e) {
            QLOG.error(TAG, e);
        } catch (Error e2) {
            QLOG.error(TAG, e2);
        }
    }
}
