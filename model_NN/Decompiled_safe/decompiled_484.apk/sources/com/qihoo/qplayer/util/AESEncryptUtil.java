package com.qihoo.qplayer.util;

import android.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESEncryptUtil {
    private static final String TAG = "Encrypt";
    private static byte[] map;

    static {
        byte[] bArr = new byte[32];
        bArr[0] = 5;
        bArr[1] = 19;
        bArr[2] = 12;
        bArr[3] = 6;
        bArr[4] = 14;
        bArr[5] = 2;
        bArr[6] = 3;
        bArr[8] = 18;
        bArr[9] = 10;
        bArr[10] = 29;
        bArr[11] = 11;
        bArr[12] = 15;
        bArr[13] = 9;
        bArr[14] = 22;
        bArr[15] = 28;
        bArr[16] = 27;
        bArr[17] = 21;
        bArr[18] = 4;
        bArr[19] = 7;
        bArr[20] = 30;
        bArr[21] = 24;
        bArr[22] = 23;
        bArr[23] = 1;
        bArr[24] = 25;
        bArr[25] = 16;
        bArr[26] = 8;
        bArr[27] = 17;
        bArr[28] = 31;
        bArr[29] = 13;
        bArr[30] = 20;
        bArr[31] = 26;
        map = bArr;
    }

    public static String decrypt(String str, String str2) {
        if (!MyLog._VERIFY) {
            return "360 Video App";
        }
        byte[] bArr = null;
        try {
            String[] splitString = splitString(str2);
            SecretKeySpec secretKeySpec = new SecretKeySpec(splitString[0].getBytes(), "AES");
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(2, secretKeySpec, new IvParameterSpec(splitString[1].getBytes()));
            bArr = instance.doFinal(Base64.decode(str, 0));
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return new String(bArr);
    }

    public static String[] splitString(String str) {
        if (str.length() != 32) {
            return null;
        }
        byte[] bArr = new byte[32];
        byte[] bytes = str.getBytes();
        for (int i = 0; i < 32; i++) {
            bArr[i] = bytes[map[i]];
        }
        return new String[]{str.substring(5, 21), String.valueOf(str.substring(0, 5)) + str.substring(21, 32)};
    }
}
