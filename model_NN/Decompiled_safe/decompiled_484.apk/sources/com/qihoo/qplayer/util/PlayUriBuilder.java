package com.qihoo.qplayer.util;

import android.net.Uri;

public class PlayUriBuilder {
    public static Uri build(String str) {
        return build(str, "other");
    }

    public static Uri build(String str, String str2) {
        return Uri.parse("qhvideo://vapp.360.cn/playvideo?url=" + str + "&website=" + str2 + "&startfrom=inside");
    }
}
