package com.qihoo.qplayer.util;

public class ZipAntUtils {
    private static final int BUFF_SIZE = 1048576;

    /* JADX WARNING: Removed duplicated region for block: B:39:0x0097 A[SYNTHETIC, Splitter:B:39:0x0097] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x009c A[Catch:{ IOException -> 0x00a6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00a1 A[Catch:{ IOException -> 0x00a6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00b1 A[SYNTHETIC, Splitter:B:51:0x00b1] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00b6 A[Catch:{ IOException -> 0x00bf }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00bb A[Catch:{ IOException -> 0x00bf }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.ArrayList<java.io.File> unZipFile(java.lang.String r10, java.lang.String r11) {
        /*
            r2 = 0
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.zip.ZipFile r6 = new java.util.zip.ZipFile     // Catch:{ Exception -> 0x00d8, all -> 0x00ab }
            r6.<init>(r10)     // Catch:{ Exception -> 0x00d8, all -> 0x00ab }
            java.util.Enumeration r7 = r6.entries()     // Catch:{ Exception -> 0x00dd, all -> 0x00ca }
            r3 = r2
            r4 = r2
        L_0x0011:
            boolean r0 = r7.hasMoreElements()     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
            if (r0 != 0) goto L_0x0028
            if (r6 == 0) goto L_0x001c
            r6.close()     // Catch:{ IOException -> 0x00c4 }
        L_0x001c:
            if (r3 == 0) goto L_0x0021
            r3.close()     // Catch:{ IOException -> 0x00c4 }
        L_0x0021:
            if (r4 == 0) goto L_0x0026
            r4.close()     // Catch:{ IOException -> 0x00c4 }
        L_0x0026:
            r0 = r1
        L_0x0027:
            return r0
        L_0x0028:
            java.lang.Object r0 = r7.nextElement()     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
            java.util.zip.ZipEntry r0 = (java.util.zip.ZipEntry) r0     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
            boolean r5 = r0.isDirectory()     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
            if (r5 != 0) goto L_0x0011
            java.io.InputStream r3 = r6.getInputStream(r0)     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
            java.io.File r8 = new java.io.File     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
            java.lang.String r9 = java.lang.String.valueOf(r11)     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
            r5.<init>(r9)     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
            java.lang.String r9 = java.io.File.separator     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
            java.lang.StringBuilder r5 = r5.append(r9)     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
            r8.<init>(r0)     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
            boolean r0 = r8.exists()     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
            if (r0 != 0) goto L_0x006e
            java.io.File r0 = r8.getParentFile()     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
            boolean r5 = r0.exists()     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
            if (r5 != 0) goto L_0x006b
            r0.mkdirs()     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
        L_0x006b:
            r8.createNewFile()     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
        L_0x006e:
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
            r5.<init>(r8)     // Catch:{ Exception -> 0x00e2, all -> 0x00d1 }
            r0 = 1048576(0x100000, float:1.469368E-39)
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x008e, all -> 0x00ce }
        L_0x0077:
            int r4 = r3.read(r0)     // Catch:{ Exception -> 0x008e, all -> 0x00ce }
            r9 = -1
            if (r4 != r9) goto L_0x0089
            r5.close()     // Catch:{ Exception -> 0x008e, all -> 0x00ce }
            r3.close()     // Catch:{ Exception -> 0x008e, all -> 0x00ce }
            r1.add(r8)     // Catch:{ Exception -> 0x008e, all -> 0x00ce }
            r4 = r5
            goto L_0x0011
        L_0x0089:
            r9 = 0
            r5.write(r0, r9, r4)     // Catch:{ Exception -> 0x008e, all -> 0x00ce }
            goto L_0x0077
        L_0x008e:
            r0 = move-exception
            r1 = r3
            r4 = r6
            r3 = r5
        L_0x0092:
            r0.printStackTrace()     // Catch:{ all -> 0x00d3 }
            if (r4 == 0) goto L_0x009a
            r4.close()     // Catch:{ IOException -> 0x00a6 }
        L_0x009a:
            if (r1 == 0) goto L_0x009f
            r1.close()     // Catch:{ IOException -> 0x00a6 }
        L_0x009f:
            if (r3 == 0) goto L_0x00a4
            r3.close()     // Catch:{ IOException -> 0x00a6 }
        L_0x00a4:
            r0 = r2
            goto L_0x0027
        L_0x00a6:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a4
        L_0x00ab:
            r0 = move-exception
            r3 = r2
            r4 = r2
            r6 = r2
        L_0x00af:
            if (r6 == 0) goto L_0x00b4
            r6.close()     // Catch:{ IOException -> 0x00bf }
        L_0x00b4:
            if (r3 == 0) goto L_0x00b9
            r3.close()     // Catch:{ IOException -> 0x00bf }
        L_0x00b9:
            if (r4 == 0) goto L_0x00be
            r4.close()     // Catch:{ IOException -> 0x00bf }
        L_0x00be:
            throw r0
        L_0x00bf:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00be
        L_0x00c4:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0026
        L_0x00ca:
            r0 = move-exception
            r3 = r2
            r4 = r2
            goto L_0x00af
        L_0x00ce:
            r0 = move-exception
            r4 = r5
            goto L_0x00af
        L_0x00d1:
            r0 = move-exception
            goto L_0x00af
        L_0x00d3:
            r0 = move-exception
            r6 = r4
            r4 = r3
            r3 = r1
            goto L_0x00af
        L_0x00d8:
            r0 = move-exception
            r1 = r2
            r3 = r2
            r4 = r2
            goto L_0x0092
        L_0x00dd:
            r0 = move-exception
            r1 = r2
            r3 = r2
            r4 = r6
            goto L_0x0092
        L_0x00e2:
            r0 = move-exception
            r1 = r3
            r3 = r4
            r4 = r6
            goto L_0x0092
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo.qplayer.util.ZipAntUtils.unZipFile(java.lang.String, java.lang.String):java.util.ArrayList");
    }
}
