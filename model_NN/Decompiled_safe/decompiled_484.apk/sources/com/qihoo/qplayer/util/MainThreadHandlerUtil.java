package com.qihoo.qplayer.util;

import android.os.Handler;
import android.os.Looper;

public class MainThreadHandlerUtil {
    private static Handler mHandler = null;

    public static Handler getMainThreadHandler() {
        if (mHandler == null) {
            mHandler = new Handler(Looper.getMainLooper());
        }
        return mHandler;
    }
}
