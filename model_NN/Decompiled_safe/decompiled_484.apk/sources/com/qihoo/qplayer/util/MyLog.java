package com.qihoo.qplayer.util;

import android.util.Log;
import com.qihoo.qplayer.QHPlayerSDK;

public class MyLog {
    public static final boolean _LOG = QHPlayerSDK._DEBUG;
    public static final boolean _VERIFY = (!QHPlayerSDK._DEBUG);

    public static int d(String str, String str2) {
        if (_LOG) {
            return Log.d(str, str2);
        }
        return 0;
    }

    public static int e(String str, String str2) {
        if (_LOG) {
            return Log.e(str, str2);
        }
        return 0;
    }

    public static int i(String str, String str2) {
        if (_LOG) {
            return Log.i(str, str2);
        }
        return 0;
    }

    public static int v(String str, String str2) {
        if (_LOG) {
            return Log.v(str, str2);
        }
        return 0;
    }

    public static int w(String str, String str2) {
        if (_LOG) {
            return Log.w(str, str2);
        }
        return 0;
    }
}
