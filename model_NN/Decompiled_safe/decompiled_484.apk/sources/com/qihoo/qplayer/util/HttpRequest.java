package com.qihoo.qplayer.util;

import android.net.Uri;
import android.util.Log;
import com.qihoo.dynamic.util.Md5Util;
import com.qihoo.http.base.HttpUtils;
import com.qihoo.video.GzipUtils;
import com.qihoo360.daily.activity.SearchActivity;
import java.net.URLEncoder;
import java.security.MessageDigest;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

public class HttpRequest {

    public class Maker {
        private static byte[] bytes = {53, 102, 100, 97, 49, 52, 56, 102, 55, 48, 57, 55, 98, 98, 56, 51, 52, 54, 57, 100, 52, 100, 52, 98, 101, 98, 50, 99, 50, 56, 49, 51};
        private static String hashCode = new String(bytes);

        public static String MD5Encode(String str) {
            return MD5Encode(str.getBytes());
        }

        public static String MD5Encode(byte[] bArr) {
            StringBuffer stringBuffer = new StringBuffer();
            try {
                MessageDigest instance = MessageDigest.getInstance(Md5Util.ALGORITHM);
                instance.update(bArr);
                byte[] digest = instance.digest();
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= digest.length) {
                        break;
                    }
                    String hexString = Integer.toHexString(digest[i2] & 255);
                    if (hexString.length() < 2) {
                        hexString = "0" + hexString;
                    }
                    stringBuffer.append(hexString);
                    i = i2 + 1;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return stringBuffer.toString();
        }

        private static int getFirstByteIndex(String str) {
            if (str != null) {
                byte[] bytes2 = str.getBytes();
                if (bytes2[0] >= 48 && bytes2[0] <= 57) {
                    return bytes2[0] - 48;
                }
                if (bytes2[0] >= 97 && bytes2[0] <= 122) {
                    return (bytes2[0] - 97) + 10;
                }
                if (bytes2[0] >= 65 && bytes2[0] <= 90) {
                    return (bytes2[0] - 65) + 10;
                }
            }
            return -1;
        }

        public static String getMakerTails(String str) {
            Log.i("jy", "maker url: " + str);
            Uri parse = Uri.parse(str);
            StringBuilder sb = new StringBuilder();
            sb.append(parse.getScheme());
            sb.append("://");
            sb.append(parse.getHost());
            sb.append(parse.getPath());
            sb.append("?");
            String queryParameter = parse.getQueryParameter("ismini");
            if (queryParameter != null) {
                sb.append("ismini=");
                sb.append(queryParameter);
                sb.append("&");
            }
            String queryParameter2 = parse.getQueryParameter(SearchActivity.TAG_URL);
            if (queryParameter2 != null) {
                sb.append("url=");
                sb.append(URLEncoder.encode(queryParameter2));
            }
            String sb2 = sb.toString();
            Log.i("jy", "maker: " + sb2);
            String MD5Encode = MD5Encode(Long.valueOf(System.currentTimeMillis()).toString());
            String str2 = String.valueOf(String.valueOf("&r=") + MD5Encode) + "&p=";
            int firstByteIndex = getFirstByteIndex(MD5Encode);
            String str3 = "";
            if (firstByteIndex >= 0 && firstByteIndex < 16) {
                str3 = MD5Encode.substring(firstByteIndex, firstByteIndex + 16);
            }
            return String.valueOf(str2) + MD5Encode(String.valueOf(hashCode) + sb2 + str3);
        }
    }

    public static String executeGetResult(String str) {
        HttpResponse httpResponse;
        Log.e("http", "url = " + str);
        try {
            HttpGet httpGet = new HttpGet(str);
            httpGet.addHeader("Accept-Encoding", "gzip");
            httpResponse = HttpUtils.getThreadSafeClient().execute(httpGet);
            try {
                return GzipUtils.getJsonStringFromGZIP(httpResponse);
            } catch (Exception e) {
                e = e;
            }
        } catch (Exception e2) {
            e = e2;
            httpResponse = null;
        }
        e.printStackTrace();
        if (httpResponse == null) {
            return null;
        }
        Log.e("wcbd", "response: " + httpResponse.getStatusLine().getStatusCode());
        return null;
    }

    public static String requestXstm(String str) {
        return executeGetResult(String.valueOf(str) + Maker.getMakerTails(str));
    }
}
