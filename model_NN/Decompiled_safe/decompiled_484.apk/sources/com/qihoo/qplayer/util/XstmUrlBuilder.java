package com.qihoo.qplayer.util;

import android.net.Uri;
import com.qihoo360.daily.activity.SearchActivity;

public class XstmUrlBuilder {
    public static String build(String str) {
        return build(str, "other");
    }

    public static String build(String str, String str2) {
        return "http://xstm.v.360.cn/movie/" + str2 + "?url=" + str;
    }

    public static String parse(String str) {
        Uri parse;
        if (str == null || (parse = Uri.parse(str)) == null) {
            return null;
        }
        return parse.getQueryParameter(SearchActivity.TAG_URL);
    }
}
