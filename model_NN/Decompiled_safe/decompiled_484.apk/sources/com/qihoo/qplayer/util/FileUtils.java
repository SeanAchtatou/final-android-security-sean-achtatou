package com.qihoo.qplayer.util;

import android.support.v4.media.session.PlaybackStateCompat;
import android.text.TextUtils;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.text.DecimalFormat;

public class FileUtils {
    private static FileUtils instance;

    public static void RecursionDeleteFile(File file) {
        if (file.isFile()) {
            file.delete();
        } else if (file.isDirectory()) {
            File[] listFiles = file.listFiles();
            if (listFiles == null || listFiles.length == 0) {
                file.delete();
                return;
            }
            for (File RecursionDeleteFile : listFiles) {
                RecursionDeleteFile(RecursionDeleteFile);
            }
            file.delete();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x003c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void copyFile(java.io.File r5, java.io.File r6) {
        /*
            r2 = 0
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ all -> 0x0040 }
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ all -> 0x0040 }
            r0.<init>(r5)     // Catch:{ all -> 0x0040 }
            r3.<init>(r0)     // Catch:{ all -> 0x0040 }
            java.io.BufferedOutputStream r1 = new java.io.BufferedOutputStream     // Catch:{ all -> 0x0043 }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ all -> 0x0043 }
            r0.<init>(r6)     // Catch:{ all -> 0x0043 }
            r1.<init>(r0)     // Catch:{ all -> 0x0043 }
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r0]     // Catch:{ all -> 0x0033 }
        L_0x0019:
            int r2 = r3.read(r0)     // Catch:{ all -> 0x0033 }
            r4 = -1
            if (r2 != r4) goto L_0x002e
            r1.flush()     // Catch:{ all -> 0x0033 }
            if (r3 == 0) goto L_0x0028
            r3.close()
        L_0x0028:
            if (r1 == 0) goto L_0x002d
            r1.close()
        L_0x002d:
            return
        L_0x002e:
            r4 = 0
            r1.write(r0, r4, r2)     // Catch:{ all -> 0x0033 }
            goto L_0x0019
        L_0x0033:
            r0 = move-exception
            r2 = r3
        L_0x0035:
            if (r2 == 0) goto L_0x003a
            r2.close()
        L_0x003a:
            if (r1 == 0) goto L_0x003f
            r1.close()
        L_0x003f:
            throw r0
        L_0x0040:
            r0 = move-exception
            r1 = r2
            goto L_0x0035
        L_0x0043:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo.qplayer.util.FileUtils.copyFile(java.io.File, java.io.File):void");
    }

    public static boolean deleteDirectory(String str) {
        if (!str.endsWith(File.separator)) {
            str = String.valueOf(str) + File.separator;
        }
        File file = new File(str);
        if (!file.exists() || !file.isDirectory()) {
            return false;
        }
        File[] listFiles = file.listFiles();
        boolean z = true;
        for (int i = 0; i < listFiles.length; i++) {
            if (listFiles[i].isFile()) {
                z = deleteFile(listFiles[i].getAbsolutePath());
                if (!z) {
                    break;
                }
            } else {
                z = deleteDirectory(listFiles[i].getAbsolutePath());
                if (!z) {
                    break;
                }
            }
        }
        return z && file.delete();
    }

    public static boolean deleteFile(String str) {
        if (str == null) {
            return false;
        }
        File file = new File(str);
        if (!file.exists()) {
            return false;
        }
        Log.d("FileUtils", "path: " + str);
        return file.delete();
    }

    public static boolean deleteFolder(String str) {
        try {
            File file = new File(str);
            if (!file.exists()) {
                return false;
            }
            return file.isFile() ? deleteFile(str) : deleteDirectory(str);
        } catch (Exception e) {
            return false;
        }
    }

    public static File getFileByPath(String str) {
        if (str == null) {
            return null;
        }
        File file = new File(str);
        if (file == null || !file.exists()) {
            return null;
        }
        return file;
    }

    public static FileUtils getInstance() {
        if (instance == null) {
            instance = new FileUtils();
        }
        return instance;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo.qplayer.util.FileUtils.deleteFolderFiles(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.qihoo.qplayer.util.FileUtils.deleteFolderFiles(java.lang.String, int):boolean
      com.qihoo.qplayer.util.FileUtils.deleteFolderFiles(java.lang.String, boolean):boolean */
    public boolean deleteFolderFiles(String str, int i) {
        if (!TextUtils.isEmpty(str)) {
            File file = new File(str);
            if (file.isDirectory()) {
                File[] listFiles = file.listFiles();
                if (i >= listFiles.length) {
                    i = listFiles.length;
                }
                for (int i2 = 0; i2 < i; i2++) {
                    deleteFolderFiles(listFiles[i2].getAbsolutePath(), true);
                }
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo.qplayer.util.FileUtils.deleteFolderFiles(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.qihoo.qplayer.util.FileUtils.deleteFolderFiles(java.lang.String, int):boolean
      com.qihoo.qplayer.util.FileUtils.deleteFolderFiles(java.lang.String, boolean):boolean */
    public boolean deleteFolderFiles(String str, boolean z) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        File file = new File(str);
        if (file.isDirectory()) {
            File[] listFiles = file.listFiles();
            for (File absolutePath : listFiles) {
                deleteFolderFiles(absolutePath.getAbsolutePath(), true);
            }
        }
        if (!z) {
            return false;
        }
        if (!file.isDirectory()) {
            return file.delete();
        }
        if (file.listFiles().length == 0) {
            return file.delete();
        }
        return false;
    }

    public String formatFileSize(long j) {
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        return j < PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID ? String.valueOf(decimalFormat.format((double) j)) + "B" : j < 1048576 ? String.valueOf(decimalFormat.format(((double) j) / 1024.0d)) + "K" : j < 1073741824 ? String.valueOf(decimalFormat.format(((double) j) / 1048576.0d)) + "M" : String.valueOf(decimalFormat.format(((double) j) / 1.073741824E9d)) + "G";
    }

    public long getDirectoryCount(File file) {
        File[] listFiles = file.listFiles();
        long length = (long) listFiles.length;
        for (int i = 0; i < listFiles.length; i++) {
            if (listFiles[i].isDirectory()) {
                length = (length + getDirectoryCount(listFiles[i])) - 1;
            }
        }
        return length;
    }

    public long getFileSizes(File file) {
        if (file.exists()) {
            FileInputStream fileInputStream = new FileInputStream(file);
            long available = (long) fileInputStream.available();
            fileInputStream.close();
            return available;
        }
        file.createNewFile();
        return 0;
    }

    public long getFolderSize(File file) {
        long j = 0;
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (int i = 0; i < listFiles.length; i++) {
                j += listFiles[i].isDirectory() ? getFolderSize(listFiles[i]) : listFiles[i].length();
            }
        }
        return j;
    }

    public long getFolderSize(String str) {
        if (str != null) {
            return getFolderSize(new File(str));
        }
        return 0;
    }
}
