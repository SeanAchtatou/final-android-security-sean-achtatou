package com.qihoo.qplayer;

import java.io.IOException;

public class CpuUtils {
    public static CpuInfo mCpuInfo = null;

    public class CpuInfo {
        public float bogoMIPS;
        public boolean hasArmV6;
        public boolean hasArmV7;
        public boolean hasFpu;
        public boolean hasMips;
        public boolean hasNeon;
        public boolean hasX86;
        public int processors;
    }

    public static synchronized CpuInfo getCpuInfo() {
        CpuInfo cpuInfo;
        synchronized (CpuUtils.class) {
            if (mCpuInfo == null) {
                try {
                    mCpuInfo = loadCpuInfo();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            cpuInfo = mCpuInfo;
        }
        return cpuInfo;
    }

    public static boolean isSupport() {
        if (mCpuInfo == null) {
            mCpuInfo = getCpuInfo();
        }
        return mCpuInfo.hasArmV6;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x004f A[Catch:{ IOException -> 0x00c6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0061 A[Catch:{ IOException -> 0x00cb }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0087 A[SYNTHETIC, Splitter:B:44:0x0087] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00e0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.qihoo.qplayer.CpuUtils.CpuInfo loadCpuInfo() {
        /*
            r8 = -1082130432(0xffffffffbf800000, float:-1.0)
            r1 = 0
            r10 = 1
            java.io.FileReader r11 = new java.io.FileReader     // Catch:{ IOException -> 0x00b4 }
            java.lang.String r0 = "/proc/cpuinfo"
            r11.<init>(r0)     // Catch:{ IOException -> 0x00b4 }
            java.io.BufferedReader r12 = new java.io.BufferedReader     // Catch:{ IOException -> 0x00b4 }
            r12.<init>(r11)     // Catch:{ IOException -> 0x00b4 }
            r0 = r1
            r2 = r1
            r3 = r1
            r4 = r1
            r5 = r1
            r6 = r1
            r7 = r1
            r1 = r8
        L_0x0018:
            java.lang.String r13 = r12.readLine()     // Catch:{ IOException -> 0x00c4 }
            if (r13 != 0) goto L_0x0037
            r11.close()     // Catch:{ IOException -> 0x00c4 }
        L_0x0021:
            com.qihoo.qplayer.CpuUtils$CpuInfo r8 = new com.qihoo.qplayer.CpuUtils$CpuInfo
            r8.<init>()
            r8.hasArmV6 = r5
            r8.hasArmV7 = r4
            r8.hasFpu = r6
            r8.hasMips = r3
            r8.hasNeon = r7
            r8.hasX86 = r2
            r8.bogoMIPS = r1
            r8.processors = r0
            return r8
        L_0x0037:
            if (r4 != 0) goto L_0x00e3
            java.lang.String r9 = "ARMv7"
            boolean r9 = r13.contains(r9)     // Catch:{ IOException -> 0x00c4 }
            if (r9 == 0) goto L_0x00e3
            r5 = r10
            r9 = r10
        L_0x0043:
            if (r5 != 0) goto L_0x0050
            if (r9 != 0) goto L_0x0050
            java.lang.String r4 = "ARMv6"
            boolean r4 = r13.contains(r4)     // Catch:{ IOException -> 0x00c6 }
            if (r4 == 0) goto L_0x0050
            r9 = r10
        L_0x0050:
            java.lang.String r4 = "clflush size"
            boolean r4 = r13.contains(r4)     // Catch:{ IOException -> 0x00c6 }
            if (r4 == 0) goto L_0x00e0
            r4 = r10
        L_0x0059:
            java.lang.String r2 = "microsecond timers"
            boolean r2 = r13.contains(r2)     // Catch:{ IOException -> 0x00cb }
            if (r2 == 0) goto L_0x0062
            r3 = r10
        L_0x0062:
            if (r7 != 0) goto L_0x006d
            java.lang.String r2 = "neon"
            boolean r2 = r13.contains(r2)     // Catch:{ IOException -> 0x00cb }
            if (r2 == 0) goto L_0x006d
            r7 = r10
        L_0x006d:
            if (r6 != 0) goto L_0x0078
            java.lang.String r2 = "vfp"
            boolean r2 = r13.contains(r2)     // Catch:{ IOException -> 0x00cb }
            if (r2 == 0) goto L_0x0078
            r6 = r10
        L_0x0078:
            java.lang.String r2 = "processor"
            boolean r2 = r13.startsWith(r2)     // Catch:{ IOException -> 0x00cb }
            if (r2 == 0) goto L_0x00de
            int r2 = r0 + 1
        L_0x0082:
            r0 = 0
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x00d8
            java.util.Locale r0 = java.util.Locale.ENGLISH     // Catch:{ IOException -> 0x00d1 }
            java.lang.String r0 = r13.toLowerCase(r0)     // Catch:{ IOException -> 0x00d1 }
            java.lang.String r14 = "bogomips"
            boolean r0 = r0.contains(r14)     // Catch:{ IOException -> 0x00d1 }
            if (r0 == 0) goto L_0x00d8
            java.lang.String r0 = ":"
            java.lang.String[] r0 = r13.split(r0)     // Catch:{ IOException -> 0x00d1 }
            r13 = 1
            r0 = r0[r13]     // Catch:{ NumberFormatException -> 0x00ac }
            java.lang.String r0 = r0.trim()     // Catch:{ NumberFormatException -> 0x00ac }
            float r1 = java.lang.Float.parseFloat(r0)     // Catch:{ NumberFormatException -> 0x00ac }
            r0 = r2
            r2 = r4
            r4 = r5
            r5 = r9
            goto L_0x0018
        L_0x00ac:
            r0 = move-exception
            r0 = r2
            r1 = r8
            r2 = r4
            r4 = r5
            r5 = r9
            goto L_0x0018
        L_0x00b4:
            r0 = move-exception
            r2 = r1
            r3 = r1
            r4 = r1
            r5 = r1
            r6 = r1
            r7 = r1
            r15 = r0
            r0 = r1
            r1 = r8
            r8 = r15
        L_0x00bf:
            r8.printStackTrace()
            goto L_0x0021
        L_0x00c4:
            r8 = move-exception
            goto L_0x00bf
        L_0x00c6:
            r4 = move-exception
            r8 = r4
            r4 = r5
            r5 = r9
            goto L_0x00bf
        L_0x00cb:
            r2 = move-exception
            r8 = r2
            r2 = r4
            r4 = r5
            r5 = r9
            goto L_0x00bf
        L_0x00d1:
            r0 = move-exception
            r8 = r0
            r0 = r2
            r2 = r4
            r4 = r5
            r5 = r9
            goto L_0x00bf
        L_0x00d8:
            r0 = r2
            r2 = r4
            r4 = r5
            r5 = r9
            goto L_0x0018
        L_0x00de:
            r2 = r0
            goto L_0x0082
        L_0x00e0:
            r4 = r2
            goto L_0x0059
        L_0x00e3:
            r9 = r5
            r5 = r4
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo.qplayer.CpuUtils.loadCpuInfo():com.qihoo.qplayer.CpuUtils$CpuInfo");
    }
}
