package com.qihoo.qplayer;

import android.content.Context;
import android.util.Log;
import com.qihoo.qplayer.CpuUtils;
import java.io.File;

public class LoadLibraryUtils {
    private static final String FILE_SUFFIX = ".so";
    public static final String LIB_DIRETORY_PREFFIX = "videolib_";
    private static final String TAG = "LoadLibraryUtils";
    private static String libPath;
    private static String libSuffix;
    private static String videoLibPath;

    static String getLibNameSuffix() {
        if (libSuffix == null) {
            CpuUtils.CpuInfo cpuInfo = CpuUtils.getCpuInfo();
            if (cpuInfo.hasArmV7) {
                if (cpuInfo.hasNeon) {
                    libSuffix = "-v7a-neon";
                } else {
                    libSuffix = "-v7a";
                }
            } else if (cpuInfo.hasArmV6) {
                libSuffix = "";
            }
        }
        return libSuffix;
    }

    public static String getLibPath(Context context) {
        return String.valueOf(context.getFilesDir().getParent()) + "/lib/";
    }

    public static String getVideoLibPath(Context context) {
        if (videoLibPath == null) {
            Log.e(TAG, "playerSoVersion: " + QHPlayerSDK.SO_VERSION);
            videoLibPath = String.valueOf(context.getDir(LIB_DIRETORY_PREFFIX + QHPlayerSDK.SO_VERSION, 0).toString()) + "/";
        }
        return videoLibPath;
    }

    public static boolean isLibraryExist(Context context) {
        if (isLibraryExist(getLibPath(context))) {
            libPath = getLibPath(context);
            return true;
        } else if (isLibraryExist(getVideoLibPath(context))) {
            libPath = getVideoLibPath(context);
            return true;
        } else {
            libPath = getVideoLibPath(context);
            return false;
        }
    }

    public static boolean isLibraryExist(String str) {
        File file;
        File file2;
        String str2 = String.valueOf(getLibNameSuffix()) + FILE_SUFFIX;
        File file3 = new File(String.valueOf(str) + "libffmpeg-armeabi" + str2);
        return file3 != null && file3.exists() && (file = new File(new StringBuilder(String.valueOf(str)).append("libqutil-armeabi").append(str2).toString())) != null && file.exists() && (file2 = new File(new StringBuilder(String.valueOf(str)).append("libqplayer-armeabi").append(str2).toString())) != null && file2.exists();
    }

    static void load(String str) {
        System.loadLibrary(String.valueOf(str) + getLibNameSuffix());
    }

    public static boolean loadCommonLibrary() {
        if (libPath == null || !CpuUtils.isSupport()) {
            return false;
        }
        try {
            loadPath(String.valueOf(libPath) + "libffmpeg-armeabi");
            loadPath(String.valueOf(libPath) + "libqutil-armeabi");
            loadPath(String.valueOf(libPath) + "libqplayer-armeabi");
            QUtils.initLibFfmpeg();
            return true;
        } catch (UnsatisfiedLinkError e) {
            e.printStackTrace();
            return false;
        }
    }

    static void loadPath(String str) {
        System.load(String.valueOf(str) + getLibNameSuffix() + FILE_SUFFIX);
    }
}
