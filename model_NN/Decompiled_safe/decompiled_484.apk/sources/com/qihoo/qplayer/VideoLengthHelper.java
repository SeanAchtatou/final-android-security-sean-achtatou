package com.qihoo.qplayer;

import com.qihoo.qplayer.util.MyLog;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class VideoLengthHelper {
    private boolean isRunning = false;
    /* access modifiers changed from: private */
    public OnDurationObtainedListener mListener;
    /* access modifiers changed from: private */
    public int runningCount = 0;
    /* access modifiers changed from: private */
    public ExecutorService threadPool;
    /* access modifiers changed from: private */
    public int totalDuration = 0;

    public interface OnDurationObtainedListener {
        void onDurationObtained(int i);

        void onItemDurationObtained(int i, int i2);
    }

    public VideoLengthHelper(int i) {
        this.threadPool = Executors.newFixedThreadPool(i);
    }

    public static int getDurationSync(String str) {
        return getItemVideoDuration(QMediaPlayer.getVideoInfo(str, ""));
    }

    /* access modifiers changed from: private */
    public static int getItemVideoDuration(String str) {
        String[] split;
        if (str == null || (split = str.split("\n")) == null) {
            return 0;
        }
        return Integer.parseInt(split[0]);
    }

    public void setOnDurationObtainedListener(OnDurationObtainedListener onDurationObtainedListener) {
        this.mListener = onDurationObtainedListener;
    }

    public void shutdownNow() {
        MyLog.i("jy", "gm length helper is shut down");
        this.threadPool.shutdownNow();
    }

    public void start(List<String> list, final String str) {
        MyLog.i("jy", "gm length helper start");
        if (list != null && list.size() != 0) {
            if (this.isRunning) {
                throw new IllegalStateException("VideoLengthHelper should not use second time");
            }
            this.isRunning = true;
            int size = list.size();
            for (final int i = 0; i < size; i++) {
                final String str2 = list.get(i);
                if (str2 != null) {
                    this.threadPool.submit(new Runnable() {
                        public void run() {
                            MyLog.e("dddddddddd jy", "getVideoInfo ********");
                            String videoInfo = QMediaPlayer.getVideoInfo(str2, str == null ? "" : str);
                            MyLog.e("dddddddddd jy", "getVideoInfo ******  === " + VideoLengthHelper.getItemVideoDuration(videoInfo));
                            if (videoInfo != null) {
                                int access$0 = VideoLengthHelper.getItemVideoDuration(videoInfo);
                                VideoLengthHelper.this.mListener.onItemDurationObtained(access$0, i);
                                VideoLengthHelper videoLengthHelper = VideoLengthHelper.this;
                                videoLengthHelper.totalDuration = access$0 + videoLengthHelper.totalDuration;
                            }
                            if (Thread.currentThread().isInterrupted()) {
                                MyLog.i("jy", "gm length helper isInterrupted");
                                return;
                            }
                            MyLog.i("jy", "gm length helper videoLength get total: " + VideoLengthHelper.this.totalDuration);
                            VideoLengthHelper videoLengthHelper2 = VideoLengthHelper.this;
                            videoLengthHelper2.runningCount = videoLengthHelper2.runningCount - 1;
                            if (VideoLengthHelper.this.runningCount == 0) {
                                MyLog.i("jy", "gm length helper all returned: " + VideoLengthHelper.this.totalDuration);
                                if (VideoLengthHelper.this.mListener != null) {
                                    VideoLengthHelper.this.mListener.onDurationObtained(VideoLengthHelper.this.totalDuration);
                                }
                                VideoLengthHelper.this.threadPool.shutdown();
                            }
                        }
                    });
                    this.runningCount++;
                }
            }
        }
    }
}
