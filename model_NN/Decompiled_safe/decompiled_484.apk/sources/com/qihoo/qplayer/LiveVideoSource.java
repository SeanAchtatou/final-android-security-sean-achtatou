package com.qihoo.qplayer;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LiveVideoSource implements IVideoSource {
    private Map<String, String> mHeaders;
    private List<Integer> mLengthList;
    private List<String> mUrlList;

    public LiveVideoSource(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.mUrlList = new ArrayList();
            this.mUrlList.add(str);
        } else {
            this.mUrlList = new ArrayList(0);
        }
        this.mLengthList = new ArrayList(0);
        this.mHeaders = new HashMap();
    }

    public Map<String, String> getHeader() {
        return this.mHeaders;
    }

    public List<String> getUrls() {
        return this.mUrlList;
    }

    public List<Integer> getVideoLength() {
        return this.mLengthList;
    }

    public void updateVideoLength(List<Integer> list) {
    }
}
