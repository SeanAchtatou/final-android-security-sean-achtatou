package com.qihoo.qplayer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.view.Surface;
import android.view.SurfaceHolder;
import com.qihoo.qplayer.CpuUtils;
import com.qihoo.qplayer.util.MyLog;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

class QMediaPlayer {
    private static final int BYTE = 524288;
    public static final int ERROR_REQUEST = 0;
    public static final int ERROR_STREAM = 1;
    public static final int ERROR_SURFACE = 2;
    private static final int MEDIA_AUDIO_FLUSH = 1003;
    private static final int MEDIA_AUDIO_OPEN = 1004;
    private static final int MEDIA_AUDIO_PAUSE = 1000;
    private static final int MEDIA_AUDIO_PLAY = 1002;
    private static final int MEDIA_AUDIO_STOP = 1001;
    private static final int MEDIA_BUFFERING_UPDATE = 3;
    private static final int MEDIA_BUFFING_BEGIN = 1005;
    private static final int MEDIA_BUFFING_END = 1006;
    private static final int MEDIA_ERROR = 100;
    public static final int MEDIA_ERROR_NETWORK = 101;
    private static final int MEDIA_INFO = 200;
    public static final int MEDIA_INFO_BAD_INTERLEAVING = 800;
    public static final int MEDIA_INFO_BUFFERING_END = 702;
    public static final int MEDIA_INFO_BUFFERING_START = 701;
    public static final int MEDIA_INFO_METADATA_UPDATE = 802;
    public static final int MEDIA_INFO_NOT_SEEKABLE = 801;
    public static final int MEDIA_INFO_STARTED_AS_NEXT = 2;
    public static final int MEDIA_INFO_TIMED_TEXT_ERROR = 900;
    public static final int MEDIA_INFO_UNKNOWN = 1;
    public static final int MEDIA_INFO_VIDEO_RENDERING_START = 3;
    public static final int MEDIA_INFO_VIDEO_TRACK_LAGGING = 700;
    private static final int MEDIA_NOP = 0;
    private static final int MEDIA_PLAYBACK_COMPLETE = 2;
    private static final int MEDIA_PREPARED = 1;
    private static final int MEDIA_SEEK_COMPLETE = 4;
    private static final int MEDIA_SET_VIDEO_SIZE = 5;
    private static final int MEDIA_STREAM_END = 1007;
    private static final int MEDIA_STREAM_ERROR = 1009;
    private static final int MEDIA_SURFACE_ERROR = 1010;
    private static final int MEDIA_TIMED_TEXT = 99;
    private static final int MEDIA_VIDEO_PIC_NOT_LOCKED = 3001;
    private static final int MEDIA_VIDEO_PLAYBACK_SLOW = 3002;
    private static final int REQUEST_TIMEOUT = 5000;
    private static final int SO_TIMEOUT = 5000;
    private static final String TAG = "gkm MediaPlayer";
    static int mSurfaceImageFormat = 0;
    private static boolean nativeLibReady = false;
    private MediaPlayer.OnCompletionListener OnCompletionListener;
    private final int SDK_VERSION = Build.VERSION.SDK_INT;
    private boolean isFastPlayMode = false;
    private AudioTrack mAudio = null;
    private EventHandler mEventHandler;
    /* access modifiers changed from: private */
    public int mNativeContext;
    /* access modifiers changed from: private */
    public QMediaPlayer mNextMediaPlayer;
    /* access modifiers changed from: private */
    public OnBufferingUpdateListener mOnBufferingUpdateListener;
    /* access modifiers changed from: private */
    public OnCompletionListener mOnCompletionListener;
    /* access modifiers changed from: private */
    public OnErrorListener mOnErrorListener;
    /* access modifiers changed from: private */
    public OnInfoListener mOnInfoListener;
    /* access modifiers changed from: private */
    public OnPreparedListener mOnPreparedListener;
    /* access modifiers changed from: private */
    public OnSeekCompleteListener mOnSeekCompleteListener;
    /* access modifiers changed from: private */
    public OnStreamEndListener mOnStreamEndListener;
    /* access modifiers changed from: private */
    public OnVideoSizeChangedListener mOnVideoSizeChangedListener;
    private boolean mScreenOnWhilePlaying;
    /* access modifiers changed from: private */
    public States mStates = States.Idle;
    private boolean mStayAwake;
    /* access modifiers changed from: private */
    public SurfaceHolder mSurfaceHolder;
    /* access modifiers changed from: private */
    public int mVideoHeight = 0;
    /* access modifiers changed from: private */
    public int mVideoWidth = 0;
    private PowerManager.WakeLock mWakeLock = null;

    @SuppressLint({"HandlerLeak"})
    class EventHandler extends Handler {
        private QMediaPlayer mMediaPlayer;

        public EventHandler(QMediaPlayer qMediaPlayer, Looper looper) {
            super(looper);
            this.mMediaPlayer = qMediaPlayer;
        }

        public void handleMessage(Message message) {
            MyLog.i("jy", "gkm (" + QMediaPlayer.this.hashCode() + ") handleMessage: " + QMediaPlayer.this.codeToMsg(message.what));
            if (this.mMediaPlayer.mNativeContext == 0) {
                MyLog.w(QMediaPlayer.TAG, "mediaplayer went away with unhandled events");
                return;
            }
            switch (message.what) {
                case 0:
                case 99:
                case QMediaPlayer.MEDIA_VIDEO_PLAYBACK_SLOW /*3002*/:
                    return;
                case 1:
                    QMediaPlayer.this.mStates = States.Prepared;
                    MyLog.e("dddddddddd jy", "handleMessage ****");
                    if (QMediaPlayer.this.mOnPreparedListener != null) {
                        QMediaPlayer.this.mOnPreparedListener.onPrepared(this.mMediaPlayer);
                        return;
                    }
                    return;
                case 2:
                    MyLog.i(QMediaPlayer.TAG, "MEDIA_PLAYBACK_COMPLETE (" + message.arg1 + "," + message.arg2 + ")");
                    this.mMediaPlayer.mStates = States.PlaybackCompleted;
                    if (QMediaPlayer.this.mNextMediaPlayer != null) {
                        QMediaPlayer.this.mNextMediaPlayer.start();
                        MyLog.i(QMediaPlayer.TAG, "MEDIA_PLAYBACK_COMPLETE Next Play(" + message.arg1 + "," + message.arg2 + ")");
                    }
                    if (QMediaPlayer.this.mOnCompletionListener != null) {
                        QMediaPlayer.this.mOnCompletionListener.onCompletion(this.mMediaPlayer);
                    }
                    MyLog.i(QMediaPlayer.TAG, "MEDIA_PLAYBACK_COMPLETE (" + message.arg1 + "," + message.arg2 + ")");
                    QMediaPlayer.this.stayAwake(false);
                    return;
                case 3:
                    if (QMediaPlayer.this.mOnBufferingUpdateListener != null) {
                        QMediaPlayer.this.mOnBufferingUpdateListener.onBufferingUpdate(this.mMediaPlayer, message.arg1, message.arg2);
                        return;
                    } else {
                        MyLog.i("jy", "gkm mOnBufferingUpdateListener is null: " + QMediaPlayer.this.hashCode());
                        return;
                    }
                case 4:
                    MyLog.e("jy", "gkm (" + QMediaPlayer.this.hashCode() + ") onSeekComplete");
                    if (QMediaPlayer.this.mOnSeekCompleteListener != null) {
                        QMediaPlayer.this.mOnSeekCompleteListener.onSeekComplete(this.mMediaPlayer);
                        return;
                    } else {
                        MyLog.i("jy", "gkm mOnSeekCompleteListener is null: " + QMediaPlayer.this.hashCode());
                        return;
                    }
                case 5:
                    QMediaPlayer.this.mVideoWidth = message.arg1;
                    QMediaPlayer.this.mVideoHeight = message.arg2;
                    MyLog.i("jy", "videoSizeChanged: " + QMediaPlayer.this.mVideoWidth + ", " + QMediaPlayer.this.mVideoHeight);
                    if (!((QMediaPlayer.this.mVideoWidth == message.arg1 && QMediaPlayer.this.mVideoHeight == message.arg2) || QMediaPlayer.this.mSurfaceHolder == null)) {
                        MyLog.v(QMediaPlayer.TAG, "resize video frame:" + message.arg1 + " " + message.arg2);
                        QMediaPlayer.this.mSurfaceHolder.setFixedSize(QMediaPlayer.this.mVideoWidth, QMediaPlayer.this.mVideoHeight);
                        QMediaPlayer.setVideoSurface(QMediaPlayer.this.mSurfaceHolder);
                    }
                    if (QMediaPlayer.this.mOnVideoSizeChangedListener != null) {
                        QMediaPlayer.this.mOnVideoSizeChangedListener.onVideoSizeChanged(this.mMediaPlayer, message.arg1, message.arg2);
                        return;
                    }
                    return;
                case 100:
                    MyLog.e("jy", "gkm (" + QMediaPlayer.this.hashCode() + ") MEDIA_ERROR (" + message.arg1 + "," + message.arg2 + ") ");
                    if (QMediaPlayer.this.mStates != States.Stopped) {
                        QMediaPlayer.this.stop();
                    }
                    QMediaPlayer.this.mStates = States.Error;
                    if (QMediaPlayer.this.mOnErrorListener != null) {
                        QMediaPlayer.this.mOnErrorListener.onError(this.mMediaPlayer, 0, message.arg2, message.obj);
                    }
                    QMediaPlayer.this.stayAwake(false);
                    return;
                case QMediaPlayer.MEDIA_INFO /*200*/:
                    if (message.arg1 != 700) {
                        MyLog.i(QMediaPlayer.TAG, "Info (" + message.arg1 + "," + message.arg2 + ")");
                    }
                    if (QMediaPlayer.this.mOnInfoListener != null) {
                        QMediaPlayer.this.mOnInfoListener.onInfo(this.mMediaPlayer, message.arg1, message.arg2);
                        return;
                    }
                    return;
                case 1000:
                    QMediaPlayer.this.audioPause();
                    return;
                case QMediaPlayer.MEDIA_AUDIO_STOP /*1001*/:
                    QMediaPlayer.this.audioStop();
                    return;
                case QMediaPlayer.MEDIA_AUDIO_PLAY /*1002*/:
                    QMediaPlayer.this.audioPlay();
                    return;
                case QMediaPlayer.MEDIA_AUDIO_FLUSH /*1003*/:
                    QMediaPlayer.this.audioFlush();
                    return;
                case QMediaPlayer.MEDIA_AUDIO_OPEN /*1004*/:
                    try {
                        MyLog.v(QMediaPlayer.TAG, "JNI send_msg_out call");
                        AudioTrack unused = QMediaPlayer.this.newAudioTrack(message.arg1, message.arg2);
                        return;
                    } catch (Exception e) {
                        MyLog.v(QMediaPlayer.TAG, "parse init faild" + e.toString());
                        return;
                    }
                case QMediaPlayer.MEDIA_BUFFING_BEGIN /*1005*/:
                    MyLog.e(QMediaPlayer.TAG, "MEDIA_BUFFING_BEGIN message type " + message.what);
                    MyLog.i("当前的状态为:", new StringBuilder().append(QMediaPlayer.this.mStates).toString());
                    if (QMediaPlayer.this.mOnBufferingUpdateListener != null) {
                        QMediaPlayer.this.mOnBufferingUpdateListener.onBufferingUpdate(this.mMediaPlayer, 0, message.arg2);
                        return;
                    } else {
                        MyLog.i("jy", "gkm mOnBufferingUpdateListener is null: " + QMediaPlayer.this.hashCode());
                        return;
                    }
                case QMediaPlayer.MEDIA_BUFFING_END /*1006*/:
                    MyLog.e(QMediaPlayer.TAG, "MEDIA_BUFFING_END message type " + message.what);
                    try {
                        if (QMediaPlayer.this.mStates != States.Paused) {
                            this.mMediaPlayer.start();
                        }
                    } catch (Exception e2) {
                    }
                    if (QMediaPlayer.this.mOnBufferingUpdateListener != null) {
                        QMediaPlayer.this.mOnBufferingUpdateListener.onBufferingUpdate(this.mMediaPlayer, 100, message.arg2);
                        return;
                    } else {
                        MyLog.i("jy", "gkm mOnBufferingUpdateListener is null: " + QMediaPlayer.this.hashCode());
                        return;
                    }
                case QMediaPlayer.MEDIA_STREAM_END /*1007*/:
                    MyLog.e("jy", "gkm (" + QMediaPlayer.this.hashCode() + ") MEDIA_STREAM_END");
                    if (QMediaPlayer.this.mOnStreamEndListener != null) {
                        QMediaPlayer.this.mOnStreamEndListener.onStreamEnd(this.mMediaPlayer);
                        return;
                    }
                    return;
                case QMediaPlayer.MEDIA_STREAM_ERROR /*1009*/:
                    QMediaPlayer.this.stop();
                    QMediaPlayer.this.mStates = States.Error;
                    if (QMediaPlayer.this.mOnErrorListener != null) {
                        QMediaPlayer.this.mOnErrorListener.onError(this.mMediaPlayer, 1, message.arg2, message.obj);
                        return;
                    }
                    return;
                case QMediaPlayer.MEDIA_SURFACE_ERROR /*1010*/:
                    QMediaPlayer.this.stop();
                    QMediaPlayer.this.mStates = States.Error;
                    if (QMediaPlayer.this.mOnErrorListener != null) {
                        QMediaPlayer.this.mOnErrorListener.onError(this.mMediaPlayer, 2, message.arg2, message.obj);
                        return;
                    }
                    return;
                case QMediaPlayer.MEDIA_VIDEO_PIC_NOT_LOCKED /*3001*/:
                    QMediaPlayer.this.mSurfaceHolder.setFormat(4);
                    QMediaPlayer.this.mSurfaceHolder.setSizeFromLayout();
                    return;
                default:
                    MyLog.e(QMediaPlayer.TAG, "Unknown message type " + message.what);
                    return;
            }
        }
    }

    public interface OnBufferingUpdateListener {
        void onBufferingUpdate(QMediaPlayer qMediaPlayer, int i, int i2);
    }

    public interface OnCompletionListener {
        void onCompletion(QMediaPlayer qMediaPlayer);
    }

    public interface OnErrorListener {
        boolean onError(QMediaPlayer qMediaPlayer, int i, int i2, Object obj);
    }

    public interface OnInfoListener {
        boolean onInfo(QMediaPlayer qMediaPlayer, int i, int i2);
    }

    public interface OnPreparedListener {
        void onPrepared(QMediaPlayer qMediaPlayer);
    }

    public interface OnSeekCompleteListener {
        void onSeekComplete(QMediaPlayer qMediaPlayer);
    }

    public interface OnStreamEndListener {
        void onStreamEnd(QMediaPlayer qMediaPlayer);
    }

    public interface OnVideoSizeChangedListener {
        void onVideoSizeChanged(QMediaPlayer qMediaPlayer, int i, int i2);
    }

    enum States {
        Idle,
        Initialized,
        Preparing,
        Prepared,
        Started,
        Paused,
        Stopped,
        PlaybackCompleted,
        Error,
        End
    }

    public QMediaPlayer() {
        MyLog.i("jy", "gkm new player: " + hashCode());
        Looper myLooper = Looper.myLooper();
        if (myLooper != null) {
            this.mEventHandler = new EventHandler(this, myLooper);
        } else {
            Looper mainLooper = Looper.getMainLooper();
            if (mainLooper != null) {
                this.mEventHandler = new EventHandler(this, mainLooper);
            } else {
                this.mEventHandler = null;
            }
        }
        native_setup(new WeakReference(this));
    }

    private native void _pause();

    private native void _release();

    private native void _reset();

    private native void _setDataSource(String str, String[] strArr, String[] strArr2);

    private static native void _setVideoSurface(Surface surface);

    private native void _start();

    private native void _stop();

    /* access modifiers changed from: private */
    public void audioFlush() {
        MyLog.i("jy", "gkm (" + hashCode() + ") audioFlush!");
        if (this.mAudio != null) {
            this.mAudio.flush();
        }
    }

    /* access modifiers changed from: private */
    public void audioPause() {
        MyLog.i("jy", "gkm (" + hashCode() + ") audioPause");
        if (this.mAudio != null) {
            this.mAudio.pause();
        }
    }

    /* access modifiers changed from: private */
    public void audioPlay() {
        MyLog.i("jy", "gkm (" + hashCode() + ") audioPlay!");
        if (this.mAudio != null) {
            this.mAudio.play();
        } else {
            MyLog.d(TAG, "Error,audioPlay not found mAudio");
        }
    }

    /* access modifiers changed from: private */
    public void audioStop() {
        MyLog.i("jy", "gkm (" + hashCode() + ") audioStop");
        if (this.mAudio != null) {
            this.mAudio.stop();
        }
        MyLog.i(TAG, "audioStop!");
    }

    private native void audioTrackReady(AudioTrack audioTrack);

    public static native void cleanSurface();

    /* access modifiers changed from: private */
    public String codeToMsg(int i) {
        switch (i) {
            case 0:
                return "MEDIA_NOP";
            case 1:
                return "MEDIA_PREPARED";
            case 2:
                return "MEDIA_PLAYBACK_COMPLETE";
            case 3:
                return "MEDIA_BUFFERING_UPDATE";
            case 4:
                return "MEDIA_SEEK_COMPLETE";
            case 5:
                return "MEDIA_SET_VIDEO_SIZE";
            case 99:
                return "MEDIA_TIMED_TEXT";
            case 100:
                return "MEDIA_ERROR";
            case 101:
                return "MEDIA_ERROR_NETWORK";
            case MEDIA_INFO /*200*/:
                return "MEDIA_INFO";
            case 1000:
                return "MEDIA_AUDIO_PAUSE";
            case MEDIA_AUDIO_STOP /*1001*/:
                return "MEDIA_AUDIO_STOP";
            case MEDIA_AUDIO_PLAY /*1002*/:
                return "MEDIA_AUDIO_PLAY";
            case MEDIA_AUDIO_FLUSH /*1003*/:
                return "MEDIA_AUDIO_FLUSH";
            case MEDIA_AUDIO_OPEN /*1004*/:
                return "MEDIA_AUDIO_OPEN";
            case MEDIA_BUFFING_BEGIN /*1005*/:
                return "MEDIA_BUFFING_BEGIN";
            case MEDIA_BUFFING_END /*1006*/:
                return "MEDIA_BUFFING_END";
            case MEDIA_STREAM_END /*1007*/:
                return "MEDIA_STREAM_END";
            case MEDIA_STREAM_ERROR /*1009*/:
                return "MEDIA_STREAM_ERROR";
            case MEDIA_VIDEO_PIC_NOT_LOCKED /*3001*/:
                return "MEDIA_VIDEO_PIC_NOT_LOCKED";
            case MEDIA_VIDEO_PLAYBACK_SLOW /*3002*/:
                return "MEDIA_VIDEO_PLAYBACK_SLOW";
            default:
                return "unknow";
        }
    }

    public static void detachDisplay() {
        MyLog.e("CBL", "detachDisplay.........");
        detachSurface();
    }

    private static native void detachSurface();

    public static void enableGLRender(boolean z) {
        native_setrender(z);
    }

    /* access modifiers changed from: private */
    public static InputStream getFromUrl(String str) {
        try {
            HttpResponse execute = getHttpClient().execute(new HttpGet(str));
            if (execute.getStatusLine().getStatusCode() == MEDIA_INFO) {
                return execute.getEntity().getContent();
            }
            return null;
        } catch (Exception e) {
            throw new Exception("连接不到网络");
        }
    }

    private static HttpClient getHttpClient() {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 5000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 5000);
        return new DefaultHttpClient(basicHttpParams);
    }

    @SuppressLint({"NewApi"})
    public static String getNativeLibraryDir(Context context) {
        if (context == null) {
            return null;
        }
        return Build.VERSION.SDK_INT > 8 ? context.getApplicationInfo().nativeLibraryDir : String.valueOf(context.getApplicationInfo().dataDir) + "/lib";
    }

    public static int getSurfaceImageFormat() {
        MyLog.v(TAG, "get mSurfaceImageFormat" + mSurfaceImageFormat);
        return mSurfaceImageFormat;
    }

    public static native String getVideoInfo(String str, String str2);

    public static void init() {
        CpuUtils.CpuInfo cpuInfo = CpuUtils.getCpuInfo();
        String str = null;
        if (cpuInfo.hasArmV7) {
            str = cpuInfo.hasNeon ? "-v7a-neon" : "-v7a";
        } else if (cpuInfo.hasArmV6) {
            str = "";
        }
        native_init(String.valueOf(QHPlayerSDK.getInstance().getFilePath()) + "/", "ffmpeg-armeabi" + str, Build.VERSION.SDK_INT);
        enableGLRender(false);
    }

    private static boolean isFroyoOrLater() {
        return Build.VERSION.SDK_INT >= 8;
    }

    private static boolean isGingerbreadOrLater() {
        return Build.VERSION.SDK_INT >= 9;
    }

    private static boolean isHoneycombOrLater() {
        return Build.VERSION.SDK_INT >= 11;
    }

    private static boolean isICSOrLater() {
        return Build.VERSION.SDK_INT >= 14;
    }

    private static boolean isJellyBeanOrLater() {
        return Build.VERSION.SDK_INT >= 16;
    }

    public static boolean loadLibraryFromUrl(final Context context, final String str) {
        final String substring = str.substring(str.lastIndexOf("/") + 1);
        new Thread(new Runnable() {
            public void run() {
                try {
                    InputStream access$24 = QMediaPlayer.getFromUrl(str);
                    if (access$24 != null) {
                        OutputStream access$25 = QMediaPlayer.openFile(context, substring);
                        if (access$25 != null) {
                            byte[] bArr = new byte[524288];
                            while (true) {
                                int read = access$24.read(bArr);
                                if (read == -1) {
                                    break;
                                }
                                access$25.write(bArr, 0, read);
                            }
                        }
                        if (access$25 != null) {
                            access$25.close();
                        }
                    }
                    if (access$24 != null) {
                        access$24.close();
                    }
                    boolean unused = QMediaPlayer.saveFilePath(context, substring);
                } catch (Exception e) {
                }
            }
        }).start();
        return false;
    }

    private native int nativeGetCurrentPosition();

    private native void nativeSetFastPlayMode(int i);

    private native void nativeSetParameter(String str, String str2);

    private final native void native_finalize();

    private static int native_init(String str, String str2, int i) {
        return native_init(str, "", str2, i);
    }

    private static final native int native_init(String str, String str2, String str3, int i);

    private static native void native_setrender(boolean z);

    private final native void native_setup(Object obj);

    /* access modifiers changed from: private */
    public AudioTrack newAudioTrack(int i, int i2) {
        int i3;
        MyLog.i("jy", "gkm (" + hashCode() + ") newAudioTrack");
        int i4 = 0;
        try {
            if (this.mAudio != null) {
                this.mAudio.stop();
                this.mAudio.release();
                this.mAudio = null;
            }
            i3 = i2 == 1 ? 4 : 12;
            try {
                i4 = AudioTrack.getMinBufferSize(i, i3, 2);
                this.mAudio = new AudioTrack(3, i, i3, 2, i4, 1);
            } catch (Exception e) {
                e = e;
                e.printStackTrace();
                this.mAudio = new AudioTrack(3, 44100, i3, 2, i4, 1);
                audioTrackReady(this.mAudio);
                return this.mAudio;
            }
        } catch (Exception e2) {
            e = e2;
            i3 = i2;
            e.printStackTrace();
            this.mAudio = new AudioTrack(3, 44100, i3, 2, i4, 1);
            audioTrackReady(this.mAudio);
            return this.mAudio;
        }
        audioTrackReady(this.mAudio);
        return this.mAudio;
    }

    /* access modifiers changed from: private */
    public static OutputStream openFile(Context context, String str) {
        try {
            return context.openFileOutput(str, 32768);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void postEventFromNative(Object obj, int i, int i2, int i3, Object obj2) {
        QMediaPlayer qMediaPlayer = (QMediaPlayer) ((WeakReference) obj).get();
        if (qMediaPlayer != null && qMediaPlayer.mEventHandler != null) {
            qMediaPlayer.mEventHandler.sendMessage(qMediaPlayer.mEventHandler.obtainMessage(i, i2, i3, obj2));
        }
    }

    private native int prepareAsync(int i);

    /* access modifiers changed from: private */
    public static boolean saveFilePath(Context context, String str) {
        SharedPreferences.Editor edit = context.getSharedPreferences("ffmpeg_path", 0).edit();
        edit.putString("fileName", str);
        return edit.commit();
    }

    private void setDataSource(String str, String[] strArr, String[] strArr2) {
        if (this.mStates == States.Idle) {
            _setDataSource(str, strArr, strArr2);
            this.mStates = States.Initialized;
            return;
        }
        throw new IllegalStateException("setDataSource error State: " + this.mStates);
    }

    public static void setSurfaceImageFormat(int i) {
        MyLog.v(TAG, "set mSurfaceImageFormat" + i);
        mSurfaceImageFormat = i;
    }

    public static void setVideoSurface(SurfaceHolder surfaceHolder) {
        MyLog.e("cbl", "setVideoSurface sh == null " + (surfaceHolder == null));
        if (surfaceHolder == null) {
            _setVideoSurface(null);
            return;
        }
        _setVideoSurface(surfaceHolder.getSurface());
        surfaceReady();
    }

    /* access modifiers changed from: private */
    @SuppressLint({"Wakelock"})
    public void stayAwake(boolean z) {
        if (this.mWakeLock != null) {
            if (z && !this.mWakeLock.isHeld()) {
                this.mWakeLock.acquire();
            } else if (!z && this.mWakeLock.isHeld()) {
                this.mWakeLock.release();
            }
        }
        this.mStayAwake = z;
        updateSurfaceScreenOn();
    }

    private static native int surfaceReady();

    private void updateSurfaceScreenOn() {
        if (this.mSurfaceHolder != null) {
            this.mSurfaceHolder.setKeepScreenOn(this.mScreenOnWhilePlaying && this.mStayAwake);
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        MyLog.i("jy", "gkm (" + hashCode() + ") finalize");
        if (this.mWakeLock != null) {
            this.mWakeLock.release();
        }
    }

    public int getCurrentPosition() {
        if (this.mStates == States.Paused || this.mStates == States.Started) {
            return nativeGetCurrentPosition();
        }
        return 0;
    }

    public native int getDuration();

    public boolean getFastPlayMode() {
        return this.isFastPlayMode;
    }

    public String getParameterString(String str) {
        return str;
    }

    public native int getVideoHeight();

    public native int getVideoWidth();

    public boolean isHardwareDecodeing() {
        return true;
    }

    public native boolean isPlaying();

    public void pause() {
        MyLog.i("jy", "gkm (" + hashCode() + ") time pause()");
        if (this.mStates == States.Started || this.mStates == States.Paused || this.mStates == States.PlaybackCompleted) {
            stayAwake(false);
            _pause();
            audioPause();
            this.mStates = States.Paused;
            return;
        }
        throw new IllegalStateException("pause " + this.mStates);
    }

    public void prepareAsync() {
        MyLog.i("jy", "gkm (" + hashCode() + ") prepareAsync");
        if (this.mStates == States.Initialized || this.mStates == States.Stopped) {
            this.mVideoWidth = 0;
            this.mVideoHeight = 0;
            if (prepareAsync(0) == 0) {
                this.mEventHandler.sendEmptyMessage(100);
                this.mStates = States.Error;
                return;
            }
            this.mStates = States.Preparing;
            return;
        }
        throw new IllegalStateException("prepareAsync " + this.mStates);
    }

    public void release() {
        MyLog.i("jy", "gkm (" + hashCode() + ") release");
        stayAwake(false);
        this.mStates = States.End;
        updateSurfaceScreenOn();
        this.mOnPreparedListener = null;
        this.mOnBufferingUpdateListener = null;
        this.mOnCompletionListener = null;
        this.mOnSeekCompleteListener = null;
        this.mOnErrorListener = null;
        this.mOnInfoListener = null;
        this.mOnVideoSizeChangedListener = null;
        if (this.mAudio != null) {
            MyLog.i("jy", "trying to release audio track " + this);
            this.mAudio.release();
            this.mAudio = null;
            MyLog.i("jy", "audio track release " + this);
        }
        _release();
        native_finalize();
        MyLog.i("jy", "gkm release " + this);
    }

    public void reset() {
        MyLog.i("jy", "gkm (" + hashCode() + ") time reset");
        stayAwake(false);
        _reset();
        this.mEventHandler.removeCallbacksAndMessages(null);
    }

    public native void seekTo(int i);

    public void setDataSource(Context context, Uri uri) {
        MyLog.i("jy", "gkm (" + hashCode() + ") setDataSource: " + uri);
        setDataSource(context, uri, (Map<String, String>) null);
    }

    public void setDataSource(Context context, Uri uri, Map<String, String> map) {
        MyLog.i("jy", "gkm (" + hashCode() + ") setDataSource: " + uri);
        String scheme = uri.getScheme();
        if (scheme == null || scheme.equals("file")) {
            setDataSource(uri.getPath());
            return;
        }
        MyLog.d(TAG, "Couldn't open file on client side, trying server side");
        setDataSource(uri.toString(), map);
    }

    public void setDataSource(String str) {
        MyLog.i("jy", "gkm (" + hashCode() + ") setDataSource: " + str);
        setDataSource(str, (String[]) null, (String[]) null);
    }

    public void setDataSource(String str, Map<String, String> map) {
        String[] strArr;
        String[] strArr2 = null;
        MyLog.i("jy", "gkm (" + hashCode() + ") setDataSource: " + str);
        if (map != null) {
            String[] strArr3 = new String[map.size()];
            String[] strArr4 = new String[map.size()];
            int i = 0;
            Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry next = it.next();
                strArr3[i2] = (String) next.getKey();
                strArr4[i2] = (String) next.getValue();
                i = i2 + 1;
            }
            strArr2 = strArr4;
            strArr = strArr3;
        } else {
            strArr = null;
        }
        setDataSource(str, strArr, strArr2);
    }

    public void setDisplay(SurfaceHolder surfaceHolder) {
        MyLog.i("jy", "gkm (" + hashCode() + ") setDisplay " + surfaceHolder);
        if (this.mSurfaceHolder == null) {
            this.mSurfaceHolder = surfaceHolder;
            setVideoSurface(surfaceHolder);
            updateSurfaceScreenOn();
        }
    }

    public void setFastPlayMode(boolean z) {
        this.isFastPlayMode = z;
        if (this.isFastPlayMode) {
            nativeSetFastPlayMode(1);
        } else {
            nativeSetFastPlayMode(0);
        }
    }

    public void setNextMediaPlayer(QMediaPlayer qMediaPlayer) {
        this.mNextMediaPlayer = qMediaPlayer;
    }

    public void setOnBufferingUpdateListener(OnBufferingUpdateListener onBufferingUpdateListener) {
        this.mOnBufferingUpdateListener = onBufferingUpdateListener;
    }

    public void setOnCompletionListener(OnCompletionListener onCompletionListener) {
        this.mOnCompletionListener = onCompletionListener;
    }

    public void setOnErrorListener(OnErrorListener onErrorListener) {
        this.mOnErrorListener = onErrorListener;
    }

    public void setOnInfoListener(OnInfoListener onInfoListener) {
        this.mOnInfoListener = onInfoListener;
    }

    public void setOnPreparedListener(OnPreparedListener onPreparedListener) {
        this.mOnPreparedListener = onPreparedListener;
    }

    public void setOnSeekCompleteListener(OnSeekCompleteListener onSeekCompleteListener) {
        this.mOnSeekCompleteListener = onSeekCompleteListener;
    }

    public void setOnStreamEndListener(OnStreamEndListener onStreamEndListener) {
        this.mOnStreamEndListener = onStreamEndListener;
    }

    public void setOnVideoSizeChangedListener(OnVideoSizeChangedListener onVideoSizeChangedListener) {
        this.mOnVideoSizeChangedListener = onVideoSizeChangedListener;
    }

    public void setParameter(String str, String str2) {
        nativeSetParameter(str, str2);
    }

    public void setScreenOnWhilePlaying(boolean z) {
        if (this.mScreenOnWhilePlaying != z) {
            if (z && this.mSurfaceHolder == null) {
                MyLog.w(TAG, "setScreenOnWhilePlaying(true) is ineffective without a SurfaceHolder");
            }
            this.mScreenOnWhilePlaying = z;
            updateSurfaceScreenOn();
        }
    }

    @SuppressLint({"Wakelock"})
    public void setWakeMode(Context context, int i) {
        boolean z;
        boolean z2;
        if (this.mWakeLock != null) {
            if (this.mWakeLock.isHeld()) {
                z2 = true;
                this.mWakeLock.release();
            } else {
                z2 = false;
            }
            this.mWakeLock = null;
            z = z2;
        } else {
            z = false;
        }
        this.mWakeLock = ((PowerManager) context.getSystemService("power")).newWakeLock(536870912 | i, QMediaPlayer.class.getName());
        this.mWakeLock.setReferenceCounted(false);
        if (z) {
            this.mWakeLock.acquire();
        }
    }

    public void start() {
        MyLog.i("jy", "gkm (" + hashCode() + ") start()" + this.mStates);
        if (this.mStates == States.Prepared || this.mStates == States.Started || this.mStates == States.Paused || this.mStates == States.PlaybackCompleted) {
            stayAwake(true);
            if (this.mStates != States.Started) {
                MyLog.i("jy", "gk native_start");
                _start();
                audioPlay();
            }
            this.mStates = States.Started;
            return;
        }
        throw new IllegalStateException("start " + this.mStates);
    }

    public void start(int i) {
        MyLog.i("jy", "gkm (" + hashCode() + ") start(" + i + ")" + this.mStates);
        if (this.mStates == States.Prepared || this.mStates == States.Started || this.mStates == States.Paused || this.mStates == States.PlaybackCompleted) {
            stayAwake(true);
            if (this.mStates != States.Started) {
                _start();
                audioPlay();
                seekTo(i);
            }
            this.mStates = States.Started;
            return;
        }
        throw new IllegalStateException("start " + this.mStates);
    }

    public void stop() {
        MyLog.i("jy", "gkm (" + hashCode() + ") stop() " + this.mStates);
        if (this.mStates == States.Preparing || this.mStates == States.Prepared || this.mStates == States.Started || this.mStates == States.Paused || this.mStates == States.PlaybackCompleted) {
            MyLog.i("jy", "gkm (" + hashCode() + ") stop inner");
            stayAwake(false);
            _stop();
            audioStop();
            this.mStates = States.Stopped;
            return;
        }
        throw new IllegalStateException("stop " + this.mStates);
    }

    public String toString() {
        return "GKMediaPlayer [mSurfaceHolder=" + this.mSurfaceHolder + ", mStates=" + this.mStates + ", mVideoWidth=" + this.mVideoWidth + ", mVideoHeight=" + this.mVideoHeight + ", mNextMediaPlayer=" + this.mNextMediaPlayer + "]";
    }
}
