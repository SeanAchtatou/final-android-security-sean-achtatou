package com.qihoo.qplayer;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.SurfaceHolder;
import com.qihoo.messenger.util.QDefine;
import com.qihoo.qplayer.IMediaPlayer;
import com.qihoo.qplayer.VideoLengthHelper;
import com.qihoo.qplayer.util.HttpRequest;
import com.qihoo.qplayer.util.MyLog;
import com.qihoo.qplayer.util.NetWorkState;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QihooMediaPlayer implements IMediaPlayer.Callback {
    public static final int ERROR_DOWNLOAD_SO = 4;
    public static final int ERROR_REQUEST_FAILED = 0;
    public static final int ERROR_STATE = 1;
    public static final int ERROR_STREAM = 3;
    private static final int INTERVAL = 500;
    private static final String KEY_PARAM_USERAGENT = "User-Agent";
    private static final int MAX_ERROR_COUNT = 5;
    private static final String XSTM_HOST = "xstm.v.360.cn";
    public static Map<String, String> abi_urlMap = new HashMap();
    private final String XSTMURL_PARAMER = "?url=";
    private final String XSTMURL_PREFIX = "http://xstm.v.360.cn/movie/";
    private int errorCount = 0;
    /* access modifiers changed from: private */
    public boolean mCanPrepare = false;
    private Context mContext;
    private int mCurrentMsec;
    private int mCurrentSeekMsec;
    protected int mDownloadPercent;
    protected int mDownloadSpeed;
    private long mDownloadTime;
    /* access modifiers changed from: private */
    public int mFragmentCount = 0;
    /* access modifiers changed from: private */
    public RefreshHandler mHandler;
    private Map<String, String> mHeaders = new HashMap();
    private boolean mIsBufferring = false;
    private boolean mIsLocalFile = false;
    private boolean mIsSeeking = false;
    /* access modifiers changed from: private */
    public int[] mLengthArray = new int[0];
    protected IMediaPlayer mMediaPlayer;
    private VideoLengthHelper mOldHelper;
    private OnBufferingUpdateListener mOnBufferingUpdateListener;
    private OnCompletionListener mOnCompletionListener;
    OnErrorListener mOnErrorListener;
    /* access modifiers changed from: private */
    public OnGetTimeListener mOnGetTimeListener;
    private OnPositionChangeListener mOnPositionChangeListener;
    private OnPreparedListener mOnPreparedListener;
    private OnSeekCompleteListener mOnSeekCompleteListener;
    private OnVideoSizeChangedListener mOnVideoSizeChangedListener;
    private PlayerDoctor mPlayerDoctor = new PlayerDoctor();
    private int mPosition = 0;
    private int mSeekMsec;
    /* access modifiers changed from: private */
    public String mSource = null;
    protected States mStates;
    protected SurfaceHolder mSurfaceHolder;
    private IMediaPlayer mTempMediaPlayer;
    private List<String> mUrlList = new ArrayList(0);
    /* access modifiers changed from: private */
    public IVideoSource mVideoSource = null;
    private WakeLockHelper mWakeLockHelper;

    public interface OnBufferingUpdateListener {
        void onBufferingUpdate(QihooMediaPlayer qihooMediaPlayer, int i);
    }

    public interface OnCompletionListener {
        void onCompletion(QihooMediaPlayer qihooMediaPlayer);
    }

    public interface OnErrorListener {
        boolean onError(QihooMediaPlayer qihooMediaPlayer, int i, int i2);
    }

    public interface OnGetTimeListener {
        void onGetTime(int i);
    }

    public interface OnPositionChangeListener {
        void onPlayPositionChanged(QihooMediaPlayer qihooMediaPlayer, int i);
    }

    public interface OnPreparedListener {
        void onPrepared(QihooMediaPlayer qihooMediaPlayer);
    }

    public interface OnSeekCompleteListener {
        void onSeekComplete(QihooMediaPlayer qihooMediaPlayer);
    }

    public interface OnVideoSizeChangedListener {
        void onVideoSizeChanged(QihooMediaPlayer qihooMediaPlayer, int i, int i2);
    }

    class RefreshHandler extends Handler {
        public static final int MSG_CHECK = 2;
        public static final int MSG_GET_TIMED = 1;
        public static final int MSG_REFRESH = 0;
        private SoftReference<QihooMediaPlayer> ref;

        public RefreshHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            QihooMediaPlayer qihooMediaPlayer = this.ref.get();
            if (qihooMediaPlayer != null) {
                switch (message.what) {
                    case 0:
                        qihooMediaPlayer.refresh();
                        return;
                    case 1:
                        if (qihooMediaPlayer.mCanPrepare) {
                            MyLog.i("jy", "mx gettime time doPrepared");
                            qihooMediaPlayer.doPrepared();
                        } else {
                            MyLog.i("jy", "mx gettime time pass");
                            qihooMediaPlayer.mCanPrepare = true;
                        }
                        MyLog.i("jy", "mx MSG_GET_TIMED");
                        qihooMediaPlayer.mOnGetTimeListener.onGetTime(qihooMediaPlayer.getDuration());
                        return;
                    case 2:
                    default:
                        return;
                }
            }
        }

        public void setEnv(QihooMediaPlayer qihooMediaPlayer) {
            this.ref = new SoftReference<>(qihooMediaPlayer);
        }
    }

    public enum States {
        Idle,
        Initialized,
        Preparing,
        Prepared,
        Started,
        Paused,
        Stopped,
        PlaybackCompleted,
        Error,
        End
    }

    public QihooMediaPlayer(Context context) {
        MyLog.i("jy", "mx MediaPlayerX created");
        this.mContext = context;
        this.mWakeLockHelper = new WakeLockHelper();
        this.mHandler = new RefreshHandler(Looper.getMainLooper());
        this.mHandler.setEnv(this);
        this.mHandler.sendEmptyMessage(0);
        this.mCurrentMsec = 0;
        reset();
    }

    private void checkSource() {
        if (this.mVideoSource != null) {
            setData();
            obtainVideoDuration();
            return;
        }
        Uri parse = Uri.parse(this.mSource);
        if (this.mSource.startsWith("/") || (parse != null && parse.getScheme().equalsIgnoreCase("file"))) {
            this.mIsLocalFile = true;
            this.mVideoSource = new LocalVideoSource(this.mSource);
            setData();
            obtainVideoDuration();
            return;
        }
        this.mIsLocalFile = false;
        String str = null;
        if (parse != null) {
            str = parse.getHost();
        }
        if (XSTM_HOST.equalsIgnoreCase(str)) {
            new Thread(new Runnable() {
                public void run() {
                    final String requestXstm = HttpRequest.requestXstm(QihooMediaPlayer.this.mSource);
                    if (QihooMediaPlayer.this.mHandler != null) {
                        QihooMediaPlayer.this.mHandler.post(new Runnable() {
                            public void run() {
                                try {
                                    QihooMediaPlayer.this.mVideoSource = new JsonVideoSource(requestXstm);
                                    QihooMediaPlayer.this.setData();
                                    QihooMediaPlayer.this.obtainVideoDuration();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    if (QihooMediaPlayer.this.mOnErrorListener != null) {
                                        QihooMediaPlayer.this.mOnErrorListener.onError(QihooMediaPlayer.this, 0, 0);
                                    }
                                }
                            }
                        });
                    }
                }
            }).start();
        }
    }

    public static void detachDisplay() {
        QMediaPlayer.detachDisplay();
    }

    /* access modifiers changed from: private */
    public void doPrepared() {
        this.mStates = States.Prepared;
        if (this.mOnPreparedListener != null) {
            this.mOnPreparedListener.onPrepared(this);
        }
    }

    private void errorMainPlayer(IMediaPlayer iMediaPlayer, int i, int i2, Object obj) {
        MyLog.i("jy", toString());
        switch (i) {
            case 0:
                this.mStates = States.Error;
                if (NetWorkState.isNetworkAvailable(this.mContext)) {
                    if (this.mOnErrorListener != null) {
                        this.mOnErrorListener.onError(this, 0, 0);
                        return;
                    }
                    return;
                } else if (this.mOnErrorListener != null) {
                    this.mOnErrorListener.onError(this, 3, 0);
                    return;
                } else {
                    return;
                }
            case 1:
                if (NetWorkState.isNetworkAvailable(this.mContext)) {
                    MyLog.e("jy", "mx onError 1009 network available reseek");
                    reloadCurrentSection();
                    return;
                }
                MyLog.e("jy", "mx onError [main] 1009 network unavailable abort");
                this.mStates = States.Error;
                if (this.mOnErrorListener != null) {
                    this.mOnErrorListener.onError(this, 3, 0);
                    return;
                }
                return;
            case 2:
                MyLog.e("jy", "mx ERROR_SURFACE");
                QMediaPlayer.enableGLRender(false);
                reloadCurrentSection();
                return;
            default:
                return;
        }
    }

    private void errorPreparingPlayer(IMediaPlayer iMediaPlayer, int i, int i2, Object obj) {
        MyLog.i("jy", "mx onError preparingPlayer " + i);
        if (!NetWorkState.isNetworkAvailable(this.mContext)) {
            MyLog.e("jy", "mx onError [prepare] 1009 network unavailable abort");
        } else if (this.errorCount < 5) {
            this.errorCount++;
            release(this.mTempMediaPlayer);
            this.mTempMediaPlayer = getPreparedPlayer(this.mPosition + 1);
        } else {
            this.errorCount = 0;
            MyLog.e("jy", "mx onError prepare too much");
            stop();
            this.mStates = States.Error;
            if (this.mOnErrorListener != null) {
                this.mOnErrorListener.onError(this, 0, 0);
            }
        }
    }

    private int getMsecFromPosition(int i) {
        if (i < 0 || this.mFragmentCount <= 0) {
            return 0;
        }
        return i >= this.mFragmentCount ? this.mLengthArray[this.mFragmentCount - 1] : this.mLengthArray[i];
    }

    private int getPositionFromMsec(int i) {
        if (this.mLengthArray == null || this.mFragmentCount == 0 || this.mLengthArray[0] == 0) {
            return 0;
        }
        for (int i2 = 0; i2 < this.mFragmentCount; i2++) {
            if (this.mLengthArray[i2] > i) {
                return i2;
            }
        }
        return this.mLengthArray.length - 1;
    }

    private IMediaPlayer getPreparedPlayer(int i) {
        if (i < this.mFragmentCount) {
            return getPreparedPlayer(this.mUrlList.get(i));
        }
        throw new IllegalArgumentException("position is over size");
    }

    private IMediaPlayer getPreparedPlayer(String str) {
        IMediaPlayer newMediaPlayer = newMediaPlayer();
        try {
            newMediaPlayer.setDataSource(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (this.mHeaders != null) {
            for (Map.Entry next : this.mHeaders.entrySet()) {
                MyLog.i("jy", "mx header: " + ((String) next.getKey()) + ": " + ((String) next.getValue()));
                newMediaPlayer.setParameter((String) next.getKey(), (String) next.getValue());
            }
        }
        newMediaPlayer.setCallback(this);
        try {
            newMediaPlayer.prepareAsyncLocal();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return newMediaPlayer;
    }

    /* access modifiers changed from: private */
    public void obtainVideoDuration() {
        boolean z = false;
        this.mCanPrepare = false;
        if (this.mFragmentCount > 1) {
            int i = 0;
            while (true) {
                if (i >= this.mFragmentCount - 1) {
                    break;
                } else if (this.mLengthArray[i] == this.mLengthArray[i + 1]) {
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
            if (getDuration() <= 0 || z) {
                obtainVideoDurationFromNative();
            } else {
                this.mHandler.sendEmptyMessage(1);
            }
        }
    }

    private void obtainVideoDurationFromNative() {
        if (this.mOldHelper != null) {
            this.mOldHelper.shutdownNow();
        }
        if (this.mFragmentCount != 0) {
            this.mOldHelper = new VideoLengthHelper(this.mFragmentCount);
            this.mOldHelper.setOnDurationObtainedListener(new VideoLengthHelper.OnDurationObtainedListener() {
                public void onDurationObtained(int i) {
                    for (int i2 = 1; i2 < QihooMediaPlayer.this.mFragmentCount; i2++) {
                        int[] access$10 = QihooMediaPlayer.this.mLengthArray;
                        access$10[i2] = access$10[i2] + QihooMediaPlayer.this.mLengthArray[i2 - 1];
                    }
                    QihooMediaPlayer.this.mHandler.sendEmptyMessage(1);
                }

                public void onItemDurationObtained(int i, int i2) {
                    QihooMediaPlayer.this.mLengthArray[i2] = i;
                }
            });
            this.mOldHelper.start(this.mUrlList, this.mHeaders.get(KEY_PARAM_USERAGENT));
        }
    }

    private void release(IMediaPlayer iMediaPlayer) {
        if (iMediaPlayer != null) {
            MyLog.i("jy", "mx release player " + iMediaPlayer.hashCode());
            iMediaPlayer.setCallback(null);
            try {
                iMediaPlayer.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
            iMediaPlayer.detachSurface();
            iMediaPlayer.release();
        }
    }

    private void releaseWithClean() {
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.setCallback(null);
            try {
                this.mMediaPlayer.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
            QMediaPlayer.cleanSurface();
            this.mMediaPlayer.release();
        }
    }

    private void reloadCurrentSection() {
        MyLog.i("jy", "mx reload current section " + toString());
        int positionFromMsec = getPositionFromMsec(this.mCurrentMsec);
        MyLog.i("jy", "PosArray: " + Arrays.toString(this.mLengthArray));
        this.mCurrentSeekMsec = this.mCurrentMsec - getMsecFromPosition(positionFromMsec - 1);
        MyLog.i("jy", "mx seekTo others");
        this.mPosition = positionFromMsec;
        release(this.mMediaPlayer);
        release(this.mTempMediaPlayer);
        this.mMediaPlayer = getPreparedPlayer(this.mPosition);
        this.mTempMediaPlayer = null;
        System.gc();
        if (this.mOnBufferingUpdateListener != null && !this.mIsLocalFile) {
            this.mOnBufferingUpdateListener.onBufferingUpdate(this, 0);
        }
    }

    /* access modifiers changed from: private */
    public void setData() {
        this.mUrlList = this.mVideoSource.getUrls();
        this.mHeaders = this.mVideoSource.getHeader();
        MyLog.i("jy", "mx setDataSource() " + this.mUrlList);
        this.mFragmentCount = this.mUrlList.size();
        this.mLengthArray = new int[this.mFragmentCount];
        List<Integer> videoLength = this.mVideoSource.getVideoLength();
        if (videoLength.size() > 0) {
            this.mLengthArray[0] = videoLength.get(0).intValue();
            int i = 1;
            while (true) {
                int i2 = i;
                if (i2 >= this.mFragmentCount) {
                    break;
                }
                this.mLengthArray[i2] = videoLength.get(i2).intValue() + this.mLengthArray[i2 - 1];
                i = i2 + 1;
            }
        }
        this.mMediaPlayer = getPreparedPlayer(0);
    }

    public static void setVideoSurface(SurfaceHolder surfaceHolder) {
        QMediaPlayer.setVideoSurface(surfaceHolder);
    }

    private void startPlay() {
        if (this.mMediaPlayer != null) {
            if (this.mSurfaceHolder != null) {
                this.mMediaPlayer.setDisplay(this.mSurfaceHolder);
            }
            this.mMediaPlayer.start();
        }
    }

    private void updateSingleVideoLength(IMediaPlayer iMediaPlayer) {
        this.mLengthArray = new int[1];
        if (iMediaPlayer != null) {
            this.mLengthArray[0] = iMediaPlayer.getDuration();
            ArrayList arrayList = new ArrayList(1);
            arrayList.add(Integer.valueOf(this.mLengthArray[0]));
            this.mVideoSource.updateVideoLength(arrayList);
        }
        MyLog.i("jy", "mx 视频数为: 1 -> " + this.mLengthArray[0]);
        this.mHandler.sendEmptyMessage(1);
    }

    public void detachSurface() {
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.detachSurface();
        }
    }

    /* access modifiers changed from: package-private */
    public Context getContext() {
        return this.mContext;
    }

    public int getCurrentPosition() {
        if (this.mIsSeeking || this.mIsBufferring) {
            return this.mSeekMsec;
        }
        if (this.mMediaPlayer == null || this.mMediaPlayer.getCurrentPosition() == 0) {
            return this.mCurrentMsec;
        }
        this.mCurrentMsec = getMsecFromPosition(this.mPosition - 1) + this.mMediaPlayer.getCurrentPosition();
        return this.mCurrentMsec;
    }

    public int getDuration() {
        if (this.mFragmentCount <= 0) {
            return 0;
        }
        MyLog.i("jy", "getDuration(): " + this.mLengthArray[this.mFragmentCount - 1]);
        return this.mLengthArray[this.mFragmentCount - 1];
    }

    public States getState() {
        return this.mStates;
    }

    public int getVideoHeight() {
        if (this.mMediaPlayer != null) {
            return this.mMediaPlayer.getVideoHeight();
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public IVideoSource getVideoSource() {
        return this.mVideoSource;
    }

    public int getVideoWidth() {
        if (this.mMediaPlayer != null) {
            return this.mMediaPlayer.getVideoWidth();
        }
        return 0;
    }

    public boolean isControllable() {
        return this.mStates == States.Prepared || this.mStates == States.Started || this.mStates == States.Paused || this.mStates == States.Stopped || this.mStates == States.PlaybackCompleted;
    }

    public boolean isIdle() {
        return this.mStates == States.Idle;
    }

    public boolean isPlaying() {
        return this.mStates == States.Started;
    }

    /* access modifiers changed from: protected */
    public IMediaPlayer newMediaPlayer() {
        return new AppMediaPlayer();
    }

    public void onBufferingUpdate(IMediaPlayer iMediaPlayer, int i, int i2) {
        MyLog.i("jy", "mx onBufferingUpdate: " + iMediaPlayer.hashCode() + ", " + i + "%");
        if (this.mHandler != null && this.mStates != States.Error) {
            if (this.mDownloadTime == 0) {
                this.mDownloadTime = System.currentTimeMillis();
            } else if (i != this.mDownloadPercent) {
                long currentTimeMillis = System.currentTimeMillis();
                int i3 = (int) (currentTimeMillis - this.mDownloadTime);
                int i4 = ((i - this.mDownloadPercent) * i2) / 100;
                if (i3 != 0) {
                    this.mDownloadSpeed = (i4 * QDefine.ONE_SECOND) / i3;
                }
                this.mDownloadPercent = i;
                this.mDownloadTime = currentTimeMillis;
            }
            if (i == 100) {
                this.mPlayerDoctor.endBuffer();
                this.mDownloadSpeed = 0;
                this.mDownloadPercent = 0;
                this.mDownloadTime = 0;
                this.mIsBufferring = false;
            } else {
                if (i == 0) {
                    this.mPlayerDoctor.beginBuffer((long) getCurrentPosition());
                }
                this.mIsBufferring = true;
            }
            if (this.mOnBufferingUpdateListener != null && !this.mIsLocalFile) {
                this.mOnBufferingUpdateListener.onBufferingUpdate(this, i);
            }
        }
    }

    public void onCompletion(IMediaPlayer iMediaPlayer) {
        if (this.mHandler != null) {
            MyLog.i("jy", "mx onCompletion " + toString());
            if (this.mPosition != this.mFragmentCount - 1) {
                release(this.mMediaPlayer);
                this.mPosition++;
                this.mMediaPlayer = this.mTempMediaPlayer;
                this.mTempMediaPlayer = null;
            } else if (this.mStates == States.Started) {
                this.mStates = States.PlaybackCompleted;
                this.mCurrentMsec = getMsecFromPosition(this.mFragmentCount - 1);
                release(this.mMediaPlayer);
                this.mMediaPlayer = null;
                if (this.mOnCompletionListener != null) {
                    this.mOnCompletionListener.onCompletion(this);
                }
                this.mPlayerDoctor.end();
                MyLog.e("jy", this.mPlayerDoctor.toString());
            }
        }
    }

    public boolean onError(IMediaPlayer iMediaPlayer, int i, int i2, Object obj) {
        MyLog.i("jy", "mx onError " + toString());
        this.mIsSeeking = false;
        this.mIsBufferring = false;
        if (this.mHandler != null) {
            MyLog.i("jy", "mx ERROR " + i);
            if (this.mMediaPlayer == iMediaPlayer) {
                errorMainPlayer(iMediaPlayer, i, i2, obj);
            } else if (this.mTempMediaPlayer == iMediaPlayer) {
                errorPreparingPlayer(iMediaPlayer, i, i2, obj);
            } else {
                this.mStates = States.Error;
                MyLog.e("jy", "mx onError abandon player error");
                if (this.mOnErrorListener != null) {
                    this.mOnErrorListener.onError(this, 0, 0);
                }
            }
        }
        return true;
    }

    public boolean onInfo(IMediaPlayer iMediaPlayer, int i, int i2) {
        return false;
    }

    public void onPrepared(IMediaPlayer iMediaPlayer) {
        if (this.mHandler != null) {
            MyLog.i("jy", "mx onPrepared: " + toString());
            if (this.mStates == States.Preparing) {
                if (this.mFragmentCount == 1) {
                    updateSingleVideoLength(iMediaPlayer);
                }
                if (this.mCanPrepare) {
                    MyLog.i("jy", "mx gettime onPrepared doPrepare");
                    doPrepared();
                    return;
                }
                this.mCanPrepare = true;
                MyLog.i("jy", "mx gettime onPrepared pass");
            } else if (iMediaPlayer.equals(this.mMediaPlayer) && this.mMediaPlayer != null) {
                MyLog.i("jy", "mx mMediaPlayer ready");
                try {
                    startPlay();
                    if (this.mStates == States.Paused) {
                        this.mMediaPlayer.pause();
                    }
                    if (this.mCurrentSeekMsec < this.mMediaPlayer.getDuration()) {
                        this.mMediaPlayer.seekTo(this.mCurrentSeekMsec);
                        MyLog.i("jy", "mx prepared seekTo = " + this.mCurrentSeekMsec);
                        this.mIsSeeking = true;
                    }
                    if (this.mMediaPlayer.isLoading() && this.mOnBufferingUpdateListener != null && !this.mIsLocalFile) {
                        this.mOnBufferingUpdateListener.onBufferingUpdate(this, 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                MyLog.i("jy", "mx onPrepared current ready " + this.mPosition);
            } else if (!iMediaPlayer.equals(this.mTempMediaPlayer) || this.mMediaPlayer == null) {
                MyLog.i("jy", toString());
                new IllegalStateException("no player found").printStackTrace();
            } else {
                this.errorCount = 0;
                this.mMediaPlayer.setNextMediaPlayer(this.mTempMediaPlayer);
            }
        }
    }

    public void onSeekComplete(IMediaPlayer iMediaPlayer) {
        if (this.mHandler != null) {
            MyLog.i("jy", "mx onSeekComplete: " + toString());
            this.mIsSeeking = false;
            this.mCurrentMsec = this.mSeekMsec;
            if (this.mStates == States.Started) {
                startPlay();
            } else if (this.mStates == States.Paused && this.mMediaPlayer != null) {
                this.mMediaPlayer.pause();
                this.mOnBufferingUpdateListener.onBufferingUpdate(this, 100);
            }
            if (!this.mIsLocalFile) {
                this.mIsBufferring = true;
            }
            this.mOnSeekCompleteListener.onSeekComplete(this);
        }
    }

    public void onStreamEnd(IMediaPlayer iMediaPlayer) {
        if (this.mHandler != null) {
            MyLog.i("jy", "mx onStreamEnd " + toString());
            if (this.mPosition < this.mFragmentCount - 1) {
                this.mTempMediaPlayer = getPreparedPlayer(this.mPosition + 1);
                System.gc();
            }
        }
    }

    public void onVideoSizeChanged(IMediaPlayer iMediaPlayer, int i, int i2) {
        if (this.mOnVideoSizeChangedListener != null) {
            this.mOnVideoSizeChangedListener.onVideoSizeChanged(this, i, i2);
        }
    }

    public void pause() {
        MyLog.i("jy", "mx pause() " + toString());
        if ((this.mStates == States.Started || this.mStates == States.Paused) && this.mMediaPlayer != null) {
            this.mStates = States.Paused;
            this.mWakeLockHelper.stayAwake(false, this.mSurfaceHolder);
            try {
                if (!this.mIsSeeking) {
                    this.mMediaPlayer.pause();
                }
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        } else if (this.mStates == States.Preparing || this.mStates == States.Idle || this.mStates == States.Initialized) {
            MyLog.e("jy", "mx error state: pause->" + this.mStates);
            new IllegalStateException("MX PAUSE_ERROR_STATE").printStackTrace();
            if (this.mOnErrorListener != null) {
                this.mOnErrorListener.onError(this, 1, 0);
            }
        } else {
            MyLog.i("jy", toString());
            throw new IllegalStateException(this.mStates.toString());
        }
    }

    public void prepareAsync() {
        MyLog.i("jy", "mx prepareAsync " + toString());
        if (this.mStates == States.Initialized || this.mStates == States.Stopped) {
            this.mStates = States.Preparing;
            release(this.mMediaPlayer);
            release(this.mTempMediaPlayer);
            this.mTempMediaPlayer = null;
            System.gc();
            checkSource();
            return;
        }
        MyLog.i("jy", toString());
        throw new IllegalStateException(this.mStates.toString());
    }

    public void recover() {
        if (this.mSource != null) {
            setDataSource(this.mSource);
        } else if (this.mVideoSource != null) {
            setDataSource(this.mVideoSource);
        }
    }

    /* access modifiers changed from: protected */
    public void refresh() {
        int currentPosition = getCurrentPosition();
        if (!this.mIsSeeking && !this.mIsBufferring && this.mOnPositionChangeListener != null && this.mStates == States.Started) {
            this.mOnPositionChangeListener.onPlayPositionChanged(this, currentPosition);
        }
        this.mHandler.sendEmptyMessageDelayed(0, 500);
    }

    public void release() {
        MyLog.i("jy", "mx release() " + toString());
        this.mStates = States.End;
        if (this.mHandler != null) {
            this.mWakeLockHelper.stayAwake(false, this.mSurfaceHolder);
            this.mWakeLockHelper = null;
            release(this.mMediaPlayer);
            release(this.mTempMediaPlayer);
            this.mMediaPlayer = null;
            this.mTempMediaPlayer = null;
            this.mHandler.removeMessages(0);
            this.mHandler = null;
            System.gc();
        }
    }

    public void reset() {
        MyLog.i("jy", "mx reset() " + toString());
        this.mStates = States.Idle;
        if (this.mWakeLockHelper == null) {
            this.mWakeLockHelper = new WakeLockHelper();
        }
        this.mWakeLockHelper.stayAwake(false, this.mSurfaceHolder);
        releaseWithClean();
        this.mMediaPlayer = null;
        release(this.mTempMediaPlayer);
        this.mTempMediaPlayer = null;
        this.mPosition = 0;
        this.mSeekMsec = 0;
        this.mCurrentSeekMsec = 0;
        this.mCurrentMsec = 0;
        this.mFragmentCount = 0;
        this.mLengthArray = new int[0];
        this.mIsSeeking = false;
        this.mIsBufferring = false;
        this.mIsLocalFile = false;
    }

    public void seekTo(int i) {
        MyLog.i("jy", "mx seekTo " + toString());
        if (this.mStates == States.Idle || this.mStates == States.Initialized || this.mStates == States.Preparing || this.mStates == States.Stopped || this.mStates == States.End || this.mStates == States.Error) {
            if (this.mOnSeekCompleteListener != null) {
                this.mIsSeeking = false;
                this.mOnSeekCompleteListener.onSeekComplete(this);
            }
        } else if (this.mIsSeeking) {
            MyLog.i("jy", "mx seeking and cancel new seek");
        } else {
            int duration = getDuration();
            if (duration <= 0 || i < duration) {
                this.mIsSeeking = true;
                if (!this.mIsLocalFile) {
                    this.mOnBufferingUpdateListener.onBufferingUpdate(this, 0);
                }
                this.mSeekMsec = i;
                this.mCurrentMsec = i;
                if (this.mFragmentCount != 1 || this.mMediaPlayer == null) {
                    int positionFromMsec = getPositionFromMsec(i);
                    MyLog.i("jy", "PosArray: " + Arrays.toString(this.mLengthArray));
                    this.mCurrentSeekMsec = i - getMsecFromPosition(positionFromMsec - 1);
                    MyLog.i("jy", "mx seekTo pos: " + this.mPosition + " -> " + positionFromMsec + ", " + this.mCurrentMsec + " -> " + this.mCurrentSeekMsec);
                    if (positionFromMsec != this.mPosition || this.mMediaPlayer == null) {
                        MyLog.i("jy", "mx seekTo others");
                        this.mPosition = positionFromMsec;
                        release(this.mMediaPlayer);
                        release(this.mTempMediaPlayer);
                        this.mMediaPlayer = getPreparedPlayer(this.mPosition);
                        this.mTempMediaPlayer = null;
                        System.gc();
                        return;
                    }
                    MyLog.i("jy", "mx seekTo this");
                    this.mMediaPlayer.seekTo(this.mCurrentSeekMsec);
                    return;
                }
                startPlay();
                if (this.mStates == States.Paused) {
                    this.mMediaPlayer.pause();
                }
                this.mMediaPlayer.seekTo(i);
                return;
            }
            this.mCurrentMsec = duration;
            release(this.mMediaPlayer);
            release(this.mTempMediaPlayer);
            this.mMediaPlayer = null;
            this.mTempMediaPlayer = null;
            if (this.mOnSeekCompleteListener != null) {
                MyLog.i("jy", "mx seek onSeekComplete");
                this.mIsSeeking = false;
                this.mOnSeekCompleteListener.onSeekComplete(this);
            }
            this.mStates = States.PlaybackCompleted;
            if (this.mOnCompletionListener != null) {
                MyLog.i("jy", "mx seek onCompletion");
                this.mOnCompletionListener.onCompletion(this);
            }
        }
    }

    public void setDataSource(IVideoSource iVideoSource) {
        if (iVideoSource != null) {
            MyLog.i("jy", "mx setDataSource() " + toString());
            this.mVideoSource = iVideoSource;
            release(this.mMediaPlayer);
            release(this.mTempMediaPlayer);
            this.mMediaPlayer = null;
            this.mTempMediaPlayer = null;
            if (this.mStates == States.Idle) {
                this.mStates = States.Initialized;
            } else {
                MyLog.i("jy", toString());
                throw new IllegalStateException(this.mStates.toString());
            }
        }
    }

    public void setDataSource(String str) {
        this.mSource = str;
        release(this.mMediaPlayer);
        release(this.mTempMediaPlayer);
        this.mMediaPlayer = null;
        this.mTempMediaPlayer = null;
        if (this.mStates == States.Idle) {
            this.mStates = States.Initialized;
        } else {
            MyLog.i("jy", toString());
            throw new IllegalStateException(this.mStates.toString());
        }
    }

    public void setDataSource(String str, String str2) {
        setDataSource("http://xstm.v.360.cn/movie/" + str + "?url=" + str2);
    }

    public void setDisplay(SurfaceHolder surfaceHolder) {
        if (this.mSurfaceHolder != surfaceHolder) {
            this.mSurfaceHolder = surfaceHolder;
            if (this.mMediaPlayer != null && this.mSurfaceHolder != null) {
                this.mMediaPlayer.setDisplay(surfaceHolder);
            }
        }
    }

    public void setOnBufferingUpdateListener(OnBufferingUpdateListener onBufferingUpdateListener) {
        this.mOnBufferingUpdateListener = onBufferingUpdateListener;
    }

    public void setOnCompletionListener(OnCompletionListener onCompletionListener) {
        this.mOnCompletionListener = onCompletionListener;
    }

    public void setOnErrorListener(OnErrorListener onErrorListener) {
        this.mOnErrorListener = onErrorListener;
    }

    public void setOnGetTimeListener(OnGetTimeListener onGetTimeListener) {
        this.mOnGetTimeListener = onGetTimeListener;
    }

    public void setOnPositionChangeListener(OnPositionChangeListener onPositionChangeListener) {
        this.mOnPositionChangeListener = onPositionChangeListener;
    }

    public void setOnPreparedListener(OnPreparedListener onPreparedListener) {
        this.mOnPreparedListener = onPreparedListener;
    }

    public void setOnSeekCompleteListener(OnSeekCompleteListener onSeekCompleteListener) {
        this.mOnSeekCompleteListener = onSeekCompleteListener;
    }

    public void setOnVideoSizeChangedListener(OnVideoSizeChangedListener onVideoSizeChangedListener) {
        this.mOnVideoSizeChangedListener = onVideoSizeChangedListener;
    }

    public void setSurfaceImageFormat(int i) {
        QMediaPlayer.setSurfaceImageFormat(i);
    }

    public void setWakeMode(Context context, int i) {
        this.mWakeLockHelper.setWakeMode(context, i);
    }

    public void start() {
        MyLog.i("jy", "mx start() " + toString());
        if (this.mStates == States.Prepared || this.mStates == States.Started || this.mStates == States.Paused) {
            this.mWakeLockHelper.stayAwake(true, this.mSurfaceHolder);
            this.mStates = States.Started;
            if (!this.mIsSeeking) {
                startPlay();
                this.mPlayerDoctor.start();
            }
        } else if (this.mStates == States.PlaybackCompleted) {
            this.mCurrentMsec = 0;
            reloadCurrentSection();
        } else {
            MyLog.i("jy", toString());
            throw new IllegalStateException(this.mStates.toString());
        }
    }

    public void stop() {
        MyLog.i("jy", "mx stop() " + toString());
        if (this.mStates == States.Preparing || this.mStates == States.Prepared || this.mStates == States.Started || this.mStates == States.Paused || this.mStates == States.PlaybackCompleted) {
            this.mWakeLockHelper.stayAwake(false, this.mSurfaceHolder);
            if (this.mStates != States.Stopped && this.mMediaPlayer != null) {
                this.mMediaPlayer.stop();
                this.mStates = States.Stopped;
                MyLog.i("mStates", "stop()-----" + this.mStates);
            }
        } else if (this.mStates != States.Stopped && this.mStates != States.Idle) {
            MyLog.e("jy", "mx error state: stop->" + this.mStates);
            if (this.mOnErrorListener != null) {
                this.mOnErrorListener.onError(this, 1, 0);
            }
        }
    }

    public String toString() {
        return "MediaPlayerX [mMediaPlayer=" + this.mMediaPlayer + ", mTempMediaPlayer=" + this.mTempMediaPlayer + ", mStates=" + this.mStates + ", mUrlList=" + this.mUrlList + ", mLengthArray=" + Arrays.toString(this.mLengthArray) + ", mFragmentCount=" + this.mFragmentCount + ", mPosition=" + this.mPosition + ", mSeekMsec=" + this.mSeekMsec + ", mCurrentSeekMsec=" + this.mCurrentSeekMsec + ", mCurrentMsec=" + this.mCurrentMsec + ", mDownloadTime=" + this.mDownloadTime + ", mDownloadPercent=" + this.mDownloadPercent + ", mDownloadSpeed=" + this.mDownloadSpeed + ", mIsSeeking=" + this.mIsSeeking + ", mIsBufferring=" + this.mIsBufferring + ", mIsLocalFile=" + this.mIsLocalFile + ", mCanPrepare=" + this.mCanPrepare + "]";
    }
}
