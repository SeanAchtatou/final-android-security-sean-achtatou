package com.qihoo.qplayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class PlayerDoctor {
    private List<BufferAction> mBufferActionList = new ArrayList();
    private BufferAction mBufferingAction;
    private long mDuration;
    private long mEndTime;
    private int[] mFragLengthArray = new int[0];
    private List<String> mFragList;
    private long mPlayTime;
    private long mStartTime;
    private String mTitle;
    private String mXstm;

    class BufferAction {
        long endTime;
        long position;
        long startTime;

        BufferAction() {
        }

        public String toString() {
            return "BufferAction [startTime=" + this.startTime + ", endTime=" + this.endTime + ", position=" + this.position + "]";
        }
    }

    public void beginBuffer(long j) {
        this.mBufferingAction = new BufferAction();
        this.mBufferingAction.position = j;
        this.mBufferingAction.startTime = System.currentTimeMillis();
    }

    public void end() {
        this.mEndTime = System.currentTimeMillis();
        this.mPlayTime = this.mEndTime - this.mStartTime;
    }

    public void endBuffer() {
        if (this.mBufferingAction != null) {
            this.mBufferingAction.endTime = System.currentTimeMillis();
            this.mBufferActionList.add(this.mBufferingAction);
            return;
        }
        this.mBufferingAction = new BufferAction();
        BufferAction bufferAction = this.mBufferingAction;
        BufferAction bufferAction2 = this.mBufferingAction;
        long currentTimeMillis = System.currentTimeMillis();
        bufferAction2.endTime = currentTimeMillis;
        bufferAction.startTime = currentTimeMillis;
        this.mBufferActionList.add(this.mBufferingAction);
    }

    public void setFragLength(int[] iArr) {
        this.mFragLengthArray = iArr;
    }

    public void setParams(long j, String str, String str2, List<String> list) {
        this.mDuration = j;
        this.mTitle = str;
        this.mXstm = str2;
        this.mFragList = list;
    }

    public void start() {
        this.mStartTime = System.currentTimeMillis();
    }

    public String toString() {
        return "PlayerDoctor [mTitle=" + this.mTitle + ", mXstm=" + this.mXstm + ", mFragList=" + this.mFragList + ", mFragLengthArray=" + Arrays.toString(this.mFragLengthArray) + ", mDuration=" + this.mDuration + ", mPlayTime=" + this.mPlayTime + ", mStartTime=" + this.mStartTime + ", mEndTime=" + this.mEndTime + ", mBufferActionList=" + this.mBufferActionList + "]";
    }
}
