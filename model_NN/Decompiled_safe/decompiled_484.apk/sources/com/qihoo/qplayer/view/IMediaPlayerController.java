package com.qihoo.qplayer.view;

import com.qihoo.qplayer.QihooMediaPlayer;

public interface IMediaPlayerController {
    void controllablePause();

    int getCurrentPosition();

    int getDuration();

    void reset();

    void seekTo(int i);

    void setOnSeekCompleteListener(QihooMediaPlayer.OnSeekCompleteListener onSeekCompleteListener);

    void start();

    void stop();
}
