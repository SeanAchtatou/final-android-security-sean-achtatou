package com.qihoo.qplayer.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.qihoo.qplayer.IVideoSource;
import com.qihoo.qplayer.QihooMediaPlayer;
import com.qihoo.qplayer.SoMediaPlayer;
import com.qihoo.qplayer.util.MyLog;
import java.util.ArrayList;
import java.util.Iterator;

public class QihooVideoView extends SurfaceView implements IMediaPlayerController {
    private static final String TAG = "QihooVideoView";
    /* access modifiers changed from: private */
    public QihooMediaPlayer.OnBufferingUpdateListener bufferListener;
    /* access modifiers changed from: private */
    public boolean is3G = false;
    /* access modifiers changed from: private */
    public int mBackupPosition;
    private QihooMediaPlayer.OnBufferingUpdateListener mBufferListener = new QihooMediaPlayer.OnBufferingUpdateListener() {
        public void onBufferingUpdate(QihooMediaPlayer qihooMediaPlayer, int i) {
            if (QihooVideoView.this.mPlayerControllerView != null) {
                QihooVideoView.this.mPlayerControllerView.updateBuffer(i);
            }
            if (QihooVideoView.this.bufferListener != null) {
                QihooVideoView.this.bufferListener.onBufferingUpdate(qihooMediaPlayer, i);
            }
        }
    };
    private SurfaceHolder.Callback mCallback = new SurfaceHolder.Callback() {
        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        }

        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            if (QihooVideoView.this.mMediaPlayer != null) {
                QihooMediaPlayer.setVideoSurface(surfaceHolder);
                if (QihooVideoView.this.mMediaPlayer.getState() == QihooMediaPlayer.States.Paused && !QihooVideoView.this.is3G) {
                    QihooVideoView.this.onStart();
                } else if (QihooVideoView.this.mMediaPlayer.getState() == QihooMediaPlayer.States.Error) {
                    QihooVideoView.this.recover();
                }
            }
        }

        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            QihooMediaPlayer.detachDisplay();
        }
    };
    private QihooMediaPlayer.OnCompletionListener mCompletionListener = new QihooMediaPlayer.OnCompletionListener() {
        public void onCompletion(QihooMediaPlayer qihooMediaPlayer) {
            if (QihooVideoView.this.mOnCompletionListener != null) {
                QihooVideoView.this.mOnCompletionListener.onCompletion(qihooMediaPlayer);
            }
        }
    };
    /* access modifiers changed from: private */
    public int mCurrentPosition = 0;
    private QihooMediaPlayer.OnErrorListener mErrorListener = new QihooMediaPlayer.OnErrorListener() {
        public boolean onError(QihooMediaPlayer qihooMediaPlayer, int i, int i2) {
            if (QihooVideoView.this.mOnErrorListener == null) {
                return true;
            }
            QihooVideoView.this.mOnErrorListener.onError(qihooMediaPlayer, i, i2);
            return true;
        }
    };
    private QihooMediaPlayer.OnGetTimeListener mGetTimeListener = new QihooMediaPlayer.OnGetTimeListener() {
        public void onGetTime(int i) {
            if (QihooVideoView.this.mPlayerControllerView != null) {
                QihooVideoView.this.mPlayerControllerView.setDuration(i);
            }
        }
    };
    private int mHeight;
    private int mInitHeight;
    private int mInitWidth;
    /* access modifiers changed from: private */
    public boolean mIsSeeking = false;
    /* access modifiers changed from: private */
    public QihooMediaPlayer mMediaPlayer;
    /* access modifiers changed from: private */
    public QihooMediaPlayer.OnCompletionListener mOnCompletionListener;
    /* access modifiers changed from: private */
    public QihooMediaPlayer.OnErrorListener mOnErrorListener;
    /* access modifiers changed from: private */
    public QihooMediaPlayer.OnPositionChangeListener mOnPositionChangeListener;
    /* access modifiers changed from: private */
    public QihooMediaPlayer.OnPreparedListener mOnPreparedListener;
    /* access modifiers changed from: private */
    public ArrayList<QihooMediaPlayer.OnSeekCompleteListener> mOnSeekCompleteListenerList;
    /* access modifiers changed from: private */
    public IVideoViewController mPlayerControllerView;
    private QihooMediaPlayer.OnPositionChangeListener mPosChangeListener = new QihooMediaPlayer.OnPositionChangeListener() {
        public void onPlayPositionChanged(QihooMediaPlayer qihooMediaPlayer, int i) {
            if (QihooVideoView.this.mPlayerControllerView != null && !QihooVideoView.this.mIsSeeking) {
                QihooVideoView.this.mPlayerControllerView.updatePlayProgress(i);
            }
            if (QihooVideoView.this.mOnPositionChangeListener != null) {
                QihooVideoView.this.mOnPositionChangeListener.onPlayPositionChanged(qihooMediaPlayer, i);
                QihooVideoView.this.mCurrentPosition = i;
            }
        }
    };
    private QihooMediaPlayer.OnPreparedListener mPreparedListener = new QihooMediaPlayer.OnPreparedListener() {
        public void onPrepared(QihooMediaPlayer qihooMediaPlayer) {
            MyLog.i(QihooVideoView.TAG, "qvv start");
            MyLog.i("wlc", "onPrepared");
            MyLog.i(QihooVideoView.TAG, "qvv getHolder(): " + QihooVideoView.this.getHolder() + ";mp.getVideoWidth():" + qihooMediaPlayer.getVideoWidth() + ";mp.getVideoHeight():" + qihooMediaPlayer.getVideoHeight());
            QihooVideoView.this.setVideoSize(qihooMediaPlayer.getVideoWidth(), qihooMediaPlayer.getVideoHeight());
            QihooVideoView.this.onStart();
            if (QihooVideoView.this.mPlayerControllerView != null) {
                QihooVideoView.this.mPlayerControllerView.hidePrepareView();
            }
            try {
                if (QihooVideoView.this.mBackupPosition > 0) {
                    QihooVideoView.this.mMediaPlayer.seekTo(QihooVideoView.this.mBackupPosition);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            MyLog.i(QihooVideoView.TAG, "onPrepared mPlayerControllerView: " + QihooVideoView.this.mPlayerControllerView);
            if (QihooVideoView.this.mOnPreparedListener != null) {
                QihooVideoView.this.mOnPreparedListener.onPrepared(qihooMediaPlayer);
            }
            if (QihooVideoView.this.getVisibility() != 0) {
                QihooVideoView.this.pause();
            }
        }
    };
    private QihooMediaPlayer.OnSeekCompleteListener mSeekCompleteListener = new QihooMediaPlayer.OnSeekCompleteListener() {
        public void onSeekComplete(QihooMediaPlayer qihooMediaPlayer) {
            try {
                if (QihooVideoView.this.mOnSeekCompleteListenerList != null && QihooVideoView.this.mOnSeekCompleteListenerList.size() > 0) {
                    Iterator it = QihooVideoView.this.mOnSeekCompleteListenerList.iterator();
                    while (it.hasNext()) {
                        ((QihooMediaPlayer.OnSeekCompleteListener) it.next()).onSeekComplete(qihooMediaPlayer);
                    }
                }
                if (QihooVideoView.this.mSeekProgress == 0) {
                    QihooVideoView.this.mIsSeeking = false;
                    return;
                }
                int access$9 = QihooVideoView.this.mSeekProgress;
                QihooVideoView.this.mSeekProgress = 0;
                try {
                    QihooVideoView.this.mMediaPlayer.seekTo(access$9);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    };
    /* access modifiers changed from: private */
    public int mSeekProgress;
    private int mVideoHeight;
    private QihooMediaPlayer.OnVideoSizeChangedListener mVideoSizeChangedListener = new QihooMediaPlayer.OnVideoSizeChangedListener() {
        public void onVideoSizeChanged(QihooMediaPlayer qihooMediaPlayer, int i, int i2) {
            QihooVideoView.this.setVideoSize(i, i2);
        }
    };
    private IVideoSource mVideoSource;
    private int mVideoWidth;
    private int mWidth;

    public QihooVideoView(Context context) {
        super(context);
        initVideoView(context);
        initVideoWidAndHeight(context.getResources().getDisplayMetrics().widthPixels, context.getResources().getDisplayMetrics().heightPixels);
    }

    public QihooVideoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initVideoView(context);
    }

    public QihooVideoView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initVideoView(context);
    }

    private void initVideoView(Context context) {
        getHolder().setFormat(4);
        getHolder().addCallback(this.mCallback);
        openVideo();
        setFocusable(true);
        setFocusableInTouchMode(true);
    }

    private void measureVideo(int i, int i2) {
        this.mWidth = this.mInitWidth;
        this.mHeight = this.mInitHeight;
        if (i > 0 && i2 > 0) {
            float f = ((float) i) / ((float) i2);
            if (((float) this.mWidth) / ((float) this.mHeight) > f) {
                this.mWidth = (int) (f * ((float) this.mHeight));
            } else {
                this.mHeight = (int) (((float) this.mWidth) / f);
            }
            MyLog.d(TAG, "measureVideo mWidth: " + this.mWidth + ", mHeight: " + this.mHeight + ", videoWidth: " + i + ", videoHeight: " + i2);
        }
    }

    private void prepareAsync() {
        if (this.mMediaPlayer != null) {
            try {
                this.mMediaPlayer.prepareAsync();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public void setVideoSize(int i, int i2) {
        if (i != 0 && i2 != 0) {
            measureVideo(i, i2);
            this.mVideoWidth = i;
            this.mVideoHeight = i2;
            getHolder().setFixedSize(i, i2);
        }
    }

    public void backup() {
        this.mBackupPosition = this.mCurrentPosition;
    }

    public void controllablePause() {
        if (this.mMediaPlayer.getState() == QihooMediaPlayer.States.Started) {
            this.mMediaPlayer.pause();
        }
    }

    public int getCurrentPosition() {
        MyLog.i("jy", "qvv currentPosition: " + this.mMediaPlayer.getCurrentPosition());
        return this.mMediaPlayer.getCurrentPosition();
    }

    public IVideoSource getDataSource() {
        return this.mVideoSource;
    }

    public int getDuration() {
        return this.mMediaPlayer.getDuration();
    }

    public QihooMediaPlayer.States getStates() {
        return this.mMediaPlayer.getState();
    }

    public void initVideoWidAndHeight(int i, int i2) {
        MyLog.d(TAG, "initVideoWidAndHeight() width: " + i + ", height: " + i2 + ", mWidth: " + this.mWidth + ", mHeight: " + this.mHeight);
        this.mInitWidth = Math.max(i, i2);
        this.mInitHeight = Math.min(i, i2);
    }

    public boolean isIdle() {
        return this.mMediaPlayer.isIdle();
    }

    public boolean isPlaying() {
        return this.mMediaPlayer.isPlaying();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        MyLog.d(TAG, "onMeasure() mWidth: " + this.mWidth + ", mHeight: " + this.mHeight);
        setMeasuredDimension(this.mWidth, this.mHeight);
    }

    public void onPause() {
        if (this.mPlayerControllerView != null) {
            this.mPlayerControllerView.pause();
        }
    }

    public void onStart() {
        if (this.mPlayerControllerView != null) {
            this.mPlayerControllerView.start();
        }
    }

    /* access modifiers changed from: package-private */
    public void openVideo() {
        this.mMediaPlayer = new SoMediaPlayer(getContext());
        this.mMediaPlayer.setWakeMode(getContext(), 26);
        this.mMediaPlayer.setSurfaceImageFormat(4);
        this.mMediaPlayer.setOnSeekCompleteListener(this.mSeekCompleteListener);
        this.mMediaPlayer.setOnPositionChangeListener(this.mPosChangeListener);
        this.mMediaPlayer.setOnErrorListener(this.mErrorListener);
        this.mMediaPlayer.setOnBufferingUpdateListener(this.mBufferListener);
        this.mMediaPlayer.setOnGetTimeListener(this.mGetTimeListener);
        this.mMediaPlayer.setOnCompletionListener(this.mCompletionListener);
        this.mMediaPlayer.setOnPreparedListener(this.mPreparedListener);
        this.mMediaPlayer.setOnVideoSizeChangedListener(this.mVideoSizeChangedListener);
    }

    public void pause() {
        try {
            this.mMediaPlayer.pause();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void recover() {
        if (this.mMediaPlayer != null && this.mMediaPlayer.getState() == QihooMediaPlayer.States.Error) {
            this.mIsSeeking = false;
            this.mMediaPlayer.reset();
            this.mMediaPlayer.recover();
            prepareAsync();
            if (this.mPlayerControllerView != null) {
                this.mPlayerControllerView.updateBuffer(0);
            }
        }
    }

    public void recoverStop() {
        MyLog.i("jy", "qvv recoverStop in: " + this.mMediaPlayer);
        if (this.mMediaPlayer == null) {
            return;
        }
        if (this.mMediaPlayer.getState() == QihooMediaPlayer.States.Stopped || this.mMediaPlayer.getState() == QihooMediaPlayer.States.Error) {
            this.mIsSeeking = false;
            this.mMediaPlayer.reset();
            this.mMediaPlayer.setDataSource(this.mVideoSource);
            prepareAsync();
            if (this.mPlayerControllerView != null) {
                this.mPlayerControllerView.updateBuffer(0);
            }
        }
    }

    public void release() {
        this.mMediaPlayer.release();
    }

    public void reset() {
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.reset();
        }
    }

    public void resetVideoWidAndHeight(int i, int i2) {
        MyLog.d(TAG, "resetVideoWidAndHeight() width: " + i + ", height: " + i2 + " , mVideoWidth: " + this.mVideoWidth + ", mVideoHeight: " + this.mVideoHeight);
        this.mInitWidth = Math.max(i, i2);
        this.mInitHeight = Math.min(i, i2);
        measureVideo(this.mVideoWidth, this.mVideoHeight);
    }

    public void seekTo(int i) {
        new IllegalStateException("seekTo: " + i).printStackTrace();
        try {
            if (!this.mIsSeeking) {
                this.mIsSeeking = true;
                this.mMediaPlayer.seekTo(i);
                return;
            }
            this.mSeekProgress = i;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void set3G(boolean z) {
        this.is3G = z;
    }

    public void setDataSource(IVideoSource iVideoSource) {
        if (this.mMediaPlayer != null) {
            this.mIsSeeking = false;
            this.mBackupPosition = -1;
            this.mMediaPlayer.reset();
            this.mVideoSource = iVideoSource;
            this.mMediaPlayer.setDataSource(this.mVideoSource);
            prepareAsync();
        }
    }

    public void setDataSource(String str) {
        if (this.mMediaPlayer != null) {
            this.mIsSeeking = false;
            this.mBackupPosition = 0;
            this.mMediaPlayer.reset();
            this.mMediaPlayer.setDataSource(str);
            prepareAsync();
        }
    }

    public void setDataSource(String str, String str2) {
        if (this.mMediaPlayer != null) {
            this.mIsSeeking = false;
            this.mBackupPosition = 0;
            this.mMediaPlayer.reset();
            this.mMediaPlayer.setDataSource(str, str2);
            prepareAsync();
        }
    }

    public void setMediaController(IVideoViewController iVideoViewController) {
        if (this.mPlayerControllerView != null) {
            this.mPlayerControllerView.hide();
        }
        this.mPlayerControllerView = iVideoViewController;
        this.mPlayerControllerView.setMediaPlayer(this);
    }

    public void setOnBufferListener(QihooMediaPlayer.OnBufferingUpdateListener onBufferingUpdateListener) {
        this.bufferListener = onBufferingUpdateListener;
    }

    public void setOnCompletetionListener(QihooMediaPlayer.OnCompletionListener onCompletionListener) {
        this.mOnCompletionListener = onCompletionListener;
    }

    public void setOnErrorListener(QihooMediaPlayer.OnErrorListener onErrorListener) {
        this.mOnErrorListener = onErrorListener;
    }

    public void setOnPositionChangeListener(QihooMediaPlayer.OnPositionChangeListener onPositionChangeListener) {
        this.mOnPositionChangeListener = onPositionChangeListener;
    }

    public void setOnPreparedListener(QihooMediaPlayer.OnPreparedListener onPreparedListener) {
        this.mOnPreparedListener = onPreparedListener;
    }

    public void setOnSeekCompleteListener(QihooMediaPlayer.OnSeekCompleteListener onSeekCompleteListener) {
        if (this.mOnSeekCompleteListenerList == null) {
            this.mOnSeekCompleteListenerList = new ArrayList<>();
        }
        this.mOnSeekCompleteListenerList.add(onSeekCompleteListener);
    }

    public void start() {
        try {
            this.mMediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.stop();
        }
    }
}
