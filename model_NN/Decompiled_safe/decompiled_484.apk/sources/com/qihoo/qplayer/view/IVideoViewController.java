package com.qihoo.qplayer.view;

public interface IVideoViewController {
    void hide();

    void hidePrepareView();

    void onDestroy();

    void onError(int i);

    void pause();

    void setDuration(int i);

    void setMediaPlayer(IMediaPlayerController iMediaPlayerController);

    void setPlayerData(Object obj);

    void show();

    void showPrepareView();

    void start();

    void updateBuffer(int i);

    void updatePlayProgress(int i);
}
