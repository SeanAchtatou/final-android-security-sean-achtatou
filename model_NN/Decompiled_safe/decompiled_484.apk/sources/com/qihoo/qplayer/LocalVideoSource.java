package com.qihoo.qplayer;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocalVideoSource implements IVideoSource {
    public static FilenameFilter sFilter = new FilenameFilter() {
        public boolean accept(File file, String str) {
            return !".".equals(str) && !"..".equals(str) && !"finish.txt".equals(str);
        }
    };
    private Map<String, String> mHeaders;
    private List<Integer> mLengthList;
    private List<String> mUrlList;

    public LocalVideoSource(String str) {
        this.mUrlList = new ArrayList();
        this.mUrlList.add(str);
        this.mLengthList = new ArrayList(0);
        this.mHeaders = new HashMap();
    }

    public LocalVideoSource(List<String> list) {
        this.mUrlList = list;
        this.mLengthList = new ArrayList(0);
        this.mHeaders = new HashMap();
    }

    public static ArrayList<String> getVideoList(String str) {
        ArrayList<String> arrayList = new ArrayList<>();
        File file = new File(str);
        if (!file.exists() || !file.isDirectory()) {
            arrayList.add(str);
        } else {
            String[] list = file.list(sFilter);
            int length = list.length;
            for (int i = 0; i < length; i++) {
                arrayList.add(String.valueOf(str) + File.separator + list[i]);
            }
            Collections.sort(arrayList);
        }
        return arrayList;
    }

    public Map<String, String> getHeader() {
        return this.mHeaders;
    }

    public List<String> getUrls() {
        return this.mUrlList;
    }

    public List<Integer> getVideoLength() {
        return this.mLengthList;
    }

    public void updateVideoLength(List<Integer> list) {
        if (list != null) {
            this.mLengthList.clear();
            this.mLengthList.addAll(list);
        }
    }
}
