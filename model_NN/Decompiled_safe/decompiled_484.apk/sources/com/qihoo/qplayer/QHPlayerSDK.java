package com.qihoo.qplayer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import com.mediav.ads.sdk.adcore.Config;
import com.qihoo.http.base.HttpUtils;
import com.qihoo.video.AppInfo;
import com.qihoo.video.PlayerInfo;
import com.qihoo.video.QihooPlayerActivity;
import com.qihoo.video.StreamUrlRequest;
import com.qihoo.video.XstmInfo;
import com.qihoo.video.httpservice.AsyncRequest;
import java.util.HashMap;

public class QHPlayerSDK {
    public static final String SO_VERSION = "2.0.2";
    public static boolean _DEBUG = false;
    private static volatile QHPlayerSDK instance;
    private Context mApplicationContext;
    private String mFilePath = "";
    private String mPackageName;
    private String mPrivateKey = "";
    private String mSignature = "";
    private String mToken;
    private String mVersionName = "";

    public interface Html5CallBack {
        boolean isClosed();

        void notifyRequest(String str, boolean z);
    }

    private QHPlayerSDK() {
    }

    public static QHPlayerSDK getInstance() {
        if (instance == null) {
            synchronized (QHPlayerSDK.class) {
                if (instance == null) {
                    instance = new QHPlayerSDK();
                }
            }
        }
        return instance;
    }

    private String getSignMd5(Context context) {
        PackageInfo packageInfo;
        if (context == null) {
            return null;
        }
        try {
            packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 64);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            packageInfo = null;
        }
        if (packageInfo != null) {
            return HttpUtils.MD5Encode(packageInfo.signatures[0].toByteArray());
        }
        return null;
    }

    public Context getContext() {
        return this.mApplicationContext;
    }

    public String getFilePath() {
        return this.mFilePath;
    }

    public String getPackageName() {
        return this.mPackageName;
    }

    public String getPrivateKey() {
        return this.mPrivateKey;
    }

    public String getSignature() {
        return this.mSignature;
    }

    public String getToken() {
        return this.mToken;
    }

    public String getVersionName() {
        return this.mVersionName;
    }

    public void init(Context context, String str) {
        this.mPrivateKey = str;
        this.mFilePath = context.getFilesDir().toString();
        HashMap<String, String> appInfo = AppInfo.getAppInfo(context);
        this.mVersionName = appInfo.get("versionName");
        this.mPackageName = appInfo.get("appName");
        if (TextUtils.isEmpty(this.mVersionName)) {
            this.mVersionName = Config.CHANNEL_ID;
        }
        this.mSignature = getSignMd5(context).toLowerCase();
        this.mApplicationContext = context.getApplicationContext();
        this.mToken = AppInfo.getTokenM2(context);
    }

    /* access modifiers changed from: package-private */
    public boolean is360VideoInstalled(Context context) {
        try {
            context.getPackageManager().getPackageInfo("com.qihoo.video", 1);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void play(PlayerInfo playerInfo, Context context) {
        Intent intent = new Intent(context, QihooPlayerActivity.class);
        intent.putExtra(QihooPlayerActivity.PLAY_INFO, playerInfo);
        context.startActivity(intent);
    }

    public void preCrackVideo(Activity activity, String str, Html5CallBack html5CallBack) {
        preCrackVideo(activity, str, "other", html5CallBack);
    }

    public void preCrackVideo(Activity activity, final String str, String str2, final Html5CallBack html5CallBack) {
        StreamUrlRequest streamUrlRequest = new StreamUrlRequest(activity, null, null);
        streamUrlRequest.setOnRecivedDataListener(new AsyncRequest.OnRecivedDataListener() {
            public void OnRecivedData(AsyncRequest asyncRequest, Object obj) {
                if ((asyncRequest instanceof StreamUrlRequest) && obj != null && (obj instanceof XstmInfo)) {
                    XstmInfo xstmInfo = (XstmInfo) obj;
                    if (TextUtils.isEmpty(xstmInfo.xstm) || xstmInfo.errorCode != 0) {
                        if (html5CallBack != null && !html5CallBack.isClosed()) {
                            html5CallBack.notifyRequest(str, false);
                        }
                    } else if (html5CallBack != null && !html5CallBack.isClosed()) {
                        html5CallBack.notifyRequest(str, true);
                    }
                } else if (html5CallBack != null && !html5CallBack.isClosed()) {
                    html5CallBack.notifyRequest(str, false);
                }
            }
        });
        streamUrlRequest.executeOnPoolExecutor("http://xstm.v.360.cn/movie/" + str2 + "?url=" + str, null, null);
    }
}
