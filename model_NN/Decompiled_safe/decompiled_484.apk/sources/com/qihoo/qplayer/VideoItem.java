package com.qihoo.qplayer;

import android.annotation.SuppressLint;
import com.qihoo360.daily.activity.SearchActivity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint({"NewApi"})
class VideoItem {
    private int duration;
    private Map<String, String> header = new HashMap();
    private boolean isM3U8 = false;
    private List<VideoPathItem> items;
    private String title;

    VideoItem() {
    }

    public static VideoItem create(String str) {
        VideoItem videoItem = new VideoItem();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject != null) {
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    if ("title".equals(next)) {
                        videoItem.setTitle(jSONObject.getString(next));
                    } else if ("duration".equals(next)) {
                        videoItem.setDuration((int) (jSONObject.getDouble(next) * 1000.0d));
                    } else if ("header".equals(next)) {
                        JSONObject jSONObject2 = jSONObject.getJSONObject(next);
                        Iterator<String> keys2 = jSONObject2.keys();
                        while (keys2.hasNext()) {
                            String next2 = keys2.next();
                            videoItem.header.put(next2, jSONObject2.optString(next2));
                        }
                    } else if ("videos".equals(next)) {
                        ArrayList arrayList = new ArrayList();
                        JSONArray jSONArray = jSONObject.getJSONArray(next);
                        int length = jSONArray.length();
                        for (int i = 0; i < length; i++) {
                            VideoPathItem videoPathItem = new VideoPathItem();
                            JSONObject jSONObject3 = jSONArray.getJSONObject(i);
                            Iterator<String> keys3 = jSONObject3.keys();
                            while (keys3.hasNext()) {
                                String next3 = keys3.next();
                                if ("duration".equals(next3)) {
                                    videoPathItem.setItemDuration((int) (jSONObject3.getDouble(next3) * 1000.0d));
                                } else if (SearchActivity.TAG_URL.equals(next3)) {
                                    videoPathItem.setItemPath(jSONObject3.getString(next3));
                                }
                            }
                            arrayList.add(videoPathItem);
                        }
                        videoItem.setItems(arrayList);
                    } else if ("isM3U8".equals(next)) {
                        videoItem.isM3U8 = jSONObject.optBoolean(next);
                    }
                }
            }
            return videoItem;
        } catch (JSONException e) {
            throw new Exception("Json解析时出现异常");
        }
    }

    public int getDuration() {
        return this.duration;
    }

    public Map<String, String> getHeader() {
        return this.header;
    }

    public List<VideoPathItem> getItems() {
        return this.items;
    }

    public String getTitle() {
        return this.title;
    }

    public boolean isM3U8() {
        return this.isM3U8;
    }

    public void setDuration(int i) {
        this.duration = i;
    }

    public void setItems(List<VideoPathItem> list) {
        this.items = list;
    }

    public void setTitle(String str) {
        this.title = str;
    }
}
