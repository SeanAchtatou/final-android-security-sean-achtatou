package com.qihoo.qplayer;

import android.content.Context;
import android.util.Log;
import com.qihoo.download.base.AbsDownloadTask;
import com.qihoo.download.impl.so.SoDownloadManager;
import com.qihoo.qplayer.QihooMediaPlayer;

public class SoMediaPlayer extends QihooMediaPlayer {
    private static final String TAG = "SoMediaPlayer";

    public SoMediaPlayer(Context context) {
        super(context);
    }

    private void downloadSo(Context context) {
        SoDownloadManager.getInstance().setListener(new SoDownloadManager.IDowloadSoListener() {
            public void onDownloadFailed(AbsDownloadTask absDownloadTask) {
                SoMediaPlayer.this.mStates = QihooMediaPlayer.States.Error;
                if (SoMediaPlayer.this.mOnErrorListener != null) {
                    SoMediaPlayer.this.mOnErrorListener.onError(SoMediaPlayer.this, 1, 0);
                }
            }

            public void onDownloadSizeChanged(AbsDownloadTask absDownloadTask) {
            }

            public void onDownloadSucess(AbsDownloadTask absDownloadTask) {
                try {
                    SoMediaPlayer.this.loadLib();
                } catch (Exception e) {
                    e.printStackTrace();
                    SoMediaPlayer.this.mStates = QihooMediaPlayer.States.Error;
                    if (SoMediaPlayer.this.mOnErrorListener != null) {
                        SoMediaPlayer.this.mOnErrorListener.onError(SoMediaPlayer.this, 1, 0);
                    }
                }
            }
        });
        SoDownloadManager.getInstance().downloadSo(context);
    }

    /* access modifiers changed from: private */
    public void loadLib() {
        QUtils.init();
        super.prepareAsync();
    }

    public void prepareAsync() {
        Log.e(TAG, "prepareAsyn");
        if (SoDownloadManager.getInstance().needDownloadSo(getContext())) {
            Log.e(TAG, "needDownloadSo: true");
            downloadSo(getContext());
            return;
        }
        Log.e(TAG, "needDownloadSo: false");
        loadLib();
    }
}
