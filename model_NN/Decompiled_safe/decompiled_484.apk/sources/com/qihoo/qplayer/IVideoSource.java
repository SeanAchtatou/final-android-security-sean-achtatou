package com.qihoo.qplayer;

import java.util.List;
import java.util.Map;

public interface IVideoSource {
    Map<String, String> getHeader();

    List<String> getUrls();

    List<Integer> getVideoLength();

    void updateVideoLength(List<Integer> list);
}
