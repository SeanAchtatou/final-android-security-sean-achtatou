package com.qihoo.qplayer;

import android.view.SurfaceHolder;
import com.qihoo.qplayer.IMediaPlayer;
import com.qihoo.qplayer.QMediaPlayer;
import com.qihoo.qplayer.util.MyLog;
import java.io.File;

class AppMediaPlayer extends QMediaPlayer implements IMediaPlayer, QMediaPlayer.OnBufferingUpdateListener, QMediaPlayer.OnCompletionListener, QMediaPlayer.OnErrorListener, QMediaPlayer.OnPreparedListener, QMediaPlayer.OnSeekCompleteListener, QMediaPlayer.OnStreamEndListener, QMediaPlayer.OnVideoSizeChangedListener {
    private static int ID = 0;
    private static int INSTANCE_COUNT = 0;
    private int id = 0;
    private IMediaPlayer.Callback mCallback;
    private boolean mIsLoading = false;
    private boolean mIsLocalFile = false;

    public AppMediaPlayer() {
        setOnPreparedListener(this);
        setOnErrorListener(this);
        setOnCompletionListener(this);
        setOnBufferingUpdateListener(this);
        setOnSeekCompleteListener(this);
        setOnStreamEndListener(this);
        setOnVideoSizeChangedListener(this);
        INSTANCE_COUNT++;
        int i = ID;
        ID = i + 1;
        this.id = i;
        MyLog.i("jy", "AMP (" + this.id + ") newPlayer created, count = " + INSTANCE_COUNT);
    }

    public void detachSurface() {
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        INSTANCE_COUNT--;
        MyLog.i("jy", "AMP (" + this.id + ") newPlayer finalized, count = " + INSTANCE_COUNT);
    }

    public boolean isLoading() {
        return this.mIsLoading;
    }

    public boolean isPlaying() {
        return false;
    }

    public IMediaPlayer newPlayer() {
        return new AppMediaPlayer();
    }

    public void onBufferingUpdate(QMediaPlayer qMediaPlayer, int i, int i2) {
        MyLog.i("jy", "AMP (" + this.id + ") onBufferingUpdate:" + i);
        if (i == 100) {
            this.mIsLoading = false;
        }
        if (this.mCallback != null) {
            this.mCallback.onBufferingUpdate((IMediaPlayer) qMediaPlayer, i, i2);
        }
    }

    public void onCompletion(QMediaPlayer qMediaPlayer) {
        MyLog.i("jy", "AMP (" + this.id + ") onCompletion");
        if (this.mCallback != null) {
            this.mCallback.onCompletion((IMediaPlayer) qMediaPlayer);
        }
    }

    public boolean onError(QMediaPlayer qMediaPlayer, int i, int i2, Object obj) {
        MyLog.i("jy", "AMP (" + this.id + ") onError");
        if (this.mCallback != null) {
            return this.mCallback.onError((IMediaPlayer) qMediaPlayer, i, i2, obj);
        }
        return false;
    }

    public void onPrepared(QMediaPlayer qMediaPlayer) {
        MyLog.i("jy", "AMP (" + this.id + ") onPrepared");
        if (this.mCallback != null) {
            this.mCallback.onPrepared((IMediaPlayer) qMediaPlayer);
        }
    }

    public void onSeekComplete(QMediaPlayer qMediaPlayer) {
        MyLog.i("jy", "AMP (" + this.id + ") onSeekComplete");
        if (this.mIsLocalFile) {
            this.mIsLoading = false;
        }
        if (this.mCallback != null) {
            this.mCallback.onSeekComplete((IMediaPlayer) qMediaPlayer);
        }
    }

    public void onStreamEnd(QMediaPlayer qMediaPlayer) {
        MyLog.i("jy", "AMP (" + this.id + ") onCompletion");
        if (this.mCallback != null) {
            this.mCallback.onStreamEnd((IMediaPlayer) qMediaPlayer);
        }
    }

    public void onVideoSizeChanged(QMediaPlayer qMediaPlayer, int i, int i2) {
        MyLog.i("jy", "AMP (" + this.id + ") onVideoSizeChanged");
        if (this.mCallback != null) {
            this.mCallback.onVideoSizeChanged((IMediaPlayer) qMediaPlayer, i, i2);
        }
    }

    public void prepareAsyncLocal() {
        MyLog.i("jy", "AMP (" + this.id + ") preparing");
        prepareAsync();
    }

    public void release() {
        MyLog.i("jy", "AMP (" + this.id + ") release");
        super.release();
    }

    public void seekTo(int i) {
        MyLog.i("jy", "AMP (" + this.id + ") seekTo: " + i);
        if (getDuration() >= i) {
            this.mIsLoading = true;
            super.seekTo(i);
            return;
        }
        new IllegalStateException("seekTo " + i + " > " + getDuration()).printStackTrace();
        this.mCallback.onError(this, 1, 0, null);
    }

    public void setCallback(IMediaPlayer.Callback callback) {
        MyLog.i("jy", "AMP (" + this.id + ") setCallback " + this.id + ", callback is null: " + (callback == null));
        this.mCallback = callback;
    }

    public void setDataSource(String str) {
        MyLog.i("jy", "AMP (" + this.id + ") setDataSource: " + str);
        this.mIsLocalFile = str.startsWith(File.separator);
        super.setDataSource(str);
    }

    public void setDisplay(SurfaceHolder surfaceHolder) {
    }

    public void setNextMediaPlayer(IMediaPlayer iMediaPlayer) {
        MyLog.i("jy", "AMP (" + this.id + ") setNextMediaPlayer");
        setNextMediaPlayer((QMediaPlayer) iMediaPlayer);
    }

    public void stop() {
        MyLog.i("jy", "AMP (" + this.id + ") stop");
        super.stop();
    }
}
