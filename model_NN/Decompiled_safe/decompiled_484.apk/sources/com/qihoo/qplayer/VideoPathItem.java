package com.qihoo.qplayer;

class VideoPathItem {
    private int itemDuration;
    private String itemPath;

    VideoPathItem() {
    }

    public int getItemDuration() {
        return this.itemDuration;
    }

    public String getItemPath() {
        return this.itemPath;
    }

    public void setItemDuration(int i) {
        this.itemDuration = i;
    }

    public void setItemPath(String str) {
        this.itemPath = str;
    }
}
