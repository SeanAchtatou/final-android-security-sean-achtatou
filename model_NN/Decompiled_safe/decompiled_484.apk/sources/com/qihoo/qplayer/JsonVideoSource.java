package com.qihoo.qplayer;

import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonVideoSource implements IVideoSource {
    private Map<String, String> mHeaders;
    private List<Integer> mLengthList;
    private List<String> mUrlList;

    public JsonVideoSource(String str) {
        Log.i("jy", str);
        VideoItem create = VideoItem.create(str);
        if (create == null || create.getItems().size() <= 0) {
            this.mUrlList = new ArrayList(0);
            this.mLengthList = new ArrayList(0);
            this.mHeaders = new HashMap();
            return;
        }
        List<VideoPathItem> items = create.getItems();
        int size = items.size();
        this.mUrlList = new ArrayList(size);
        this.mLengthList = new ArrayList(size);
        this.mHeaders = create.getHeader();
        for (int i = 0; i < size; i++) {
            this.mUrlList.add(items.get(i).getItemPath());
            if (!create.isM3U8()) {
                this.mLengthList.add(Integer.valueOf(items.get(i).getItemDuration()));
            }
        }
    }

    public Map<String, String> getHeader() {
        return this.mHeaders;
    }

    public List<String> getUrls() {
        return this.mUrlList;
    }

    public List<Integer> getVideoLength() {
        return this.mLengthList;
    }

    public void updateVideoLength(List<Integer> list) {
        if (list != null) {
            this.mLengthList.clear();
            this.mLengthList.addAll(list);
        }
    }
}
