package com.qihoo.qplayer;

import android.view.SurfaceHolder;

public interface IMediaPlayer {

    public interface Callback {
        void onBufferingUpdate(IMediaPlayer iMediaPlayer, int i, int i2);

        void onCompletion(IMediaPlayer iMediaPlayer);

        boolean onError(IMediaPlayer iMediaPlayer, int i, int i2, Object obj);

        boolean onInfo(IMediaPlayer iMediaPlayer, int i, int i2);

        void onPrepared(IMediaPlayer iMediaPlayer);

        void onSeekComplete(IMediaPlayer iMediaPlayer);

        void onStreamEnd(IMediaPlayer iMediaPlayer);

        void onVideoSizeChanged(IMediaPlayer iMediaPlayer, int i, int i2);
    }

    void detachSurface();

    int getCurrentPosition();

    int getDuration();

    boolean getFastPlayMode();

    int getVideoHeight();

    int getVideoWidth();

    boolean isLoading();

    boolean isPlaying();

    IMediaPlayer newPlayer();

    void pause();

    void prepareAsyncLocal();

    void release();

    void reset();

    void seekTo(int i);

    void setCallback(Callback callback);

    void setDataSource(String str);

    void setDisplay(SurfaceHolder surfaceHolder);

    void setFastPlayMode(boolean z);

    void setNextMediaPlayer(IMediaPlayer iMediaPlayer);

    void setParameter(String str, String str2);

    void start();

    void stop();
}
