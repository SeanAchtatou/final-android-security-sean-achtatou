package com.qihoo.miop;

import android.text.TextUtils;
import com.qihoo.messenger.util.Base64;
import com.qihoo.messenger.util.DesUtil;
import com.qihoo.messenger.util.HttpClient;
import com.qihoo.messenger.util.MD5Util;
import com.qihoo.messenger.util.QDefine;
import com.qihoo.messenger.util.RSAUtil;
import com.qihoo.miop.message.Message;
import com.qihoo.miop.message.MessageInputStream;
import com.qihoo.miop.message.MessageOutputStream;
import com.qihoo360.daily.h.ad;
import com.qihoo360.daily.h.u;
import java.net.Socket;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Pattern;

public class MiopClient {
    private static final Pattern IP_ADDRESS = Pattern.compile("((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[0-9]))");
    private final String DISPATCHER_HOST = "http://md.openapi.360.cn";
    private final String IPLIST_URI = "/list/get";
    private final String ROOM_HOST = "message.openapi.360.cn";
    private MessageInputStream in;
    private String[] ipList;
    private int keepaliveTimeout;
    private String mProduct = "newsreader_mt";
    private String mVersion = "5";
    private short miopVersion;
    private MessageOutputStream out;
    private Socket socket;
    private long timeStamp;
    private final String userId;

    public MiopClient(String str, int i) {
        this.userId = str;
        this.keepaliveTimeout = i;
    }

    private String getRandomIp() {
        return this.ipList[new Random().nextInt(this.ipList.length)];
    }

    private String getRoomIpFromDispatcher() {
        StringBuilder sb = new StringBuilder();
        getClass();
        StringBuilder append = sb.append("http://md.openapi.360.cn");
        getClass();
        String str = HttpClient.get(append.append("/list/get").append("?product=").append(this.mProduct).append("&version=").append(this.mVersion).append("&user=").append(this.userId).toString());
        u.a("ips :" + str);
        this.ipList = str.split("\n");
        return getRandomIp();
    }

    private static boolean isIPv4Numeric(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return IP_ADDRESS.matcher(str).matches();
    }

    public void MiopBind() {
        Message message = new Message(this.miopVersion);
        String num = Integer.toString(this.keepaliveTimeout);
        this.timeStamp = System.currentTimeMillis();
        String l = Long.toString(this.timeStamp);
        message.addProperty("u", this.userId);
        message.addProperty("ts", l);
        message.addProperty("t", num);
        message.addProperty("op", String.valueOf(2));
        this.out.Write(message);
    }

    public void close() {
        this.socket.close();
    }

    public void connect() {
        String str;
        String str2;
        String roomIpFromDispatcher = getRoomIpFromDispatcher();
        u.a("ip address:" + roomIpFromDispatcher);
        String str3 = null;
        int i = 0;
        boolean z = false;
        while (true) {
            if (!z) {
                try {
                    String[] split = roomIpFromDispatcher.split(":");
                    if (split.length == 2) {
                        str = split[0];
                        str2 = split[1];
                    } else {
                        str2 = "80";
                        str = roomIpFromDispatcher;
                    }
                    if (!isIPv4Numeric(str)) {
                        throw new Exception("Host should be IP format, but not");
                    }
                } catch (Exception e) {
                    Exception exc = e;
                    String str4 = str;
                    Exception exc2 = exc;
                    if (z) {
                        throw exc2;
                    }
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                    if (i < 5) {
                        roomIpFromDispatcher = getRandomIp();
                    } else if (i == 5) {
                        roomIpFromDispatcher = getRoomIpFromDispatcher();
                    } else {
                        getClass();
                        str4 = "message.openapi.360.cn";
                        z = true;
                    }
                    i++;
                    str3 = str4;
                }
            } else {
                str2 = "80";
            }
            System.out.println(str);
            this.socket = new Socket(str, Integer.parseInt(str2));
            this.in = new MessageInputStream(this.socket.getInputStream());
            this.out = new MessageOutputStream(this.socket.getOutputStream());
            Message message = new Message(this.miopVersion);
            String num = Integer.toString(this.keepaliveTimeout);
            this.timeStamp = System.currentTimeMillis();
            String l = Long.toString(this.timeStamp);
            ad.a("miopVersion: " + ((int) this.miopVersion));
            switch (this.miopVersion) {
                case 2:
                case 4:
                    String uuid = UUID.randomUUID().toString();
                    String substring = UUID.randomUUID().toString().substring(0, 8);
                    String replace = new String(Base64.encode(RSAUtil.encrypt(substring.getBytes()), 0)).replace("\n", "");
                    String replace2 = new String(Base64.encode(DesUtil.encryptDES(("user=" + this.userId + "&sign=" + MD5Util.encode("n" + uuid + "t" + num + "ts" + l)).getBytes(), substring.getBytes()), 0)).replace("\n", "");
                    message.addProperty("t", num);
                    message.addProperty("n", uuid);
                    message.addProperty("ts", l);
                    message.addProperty("s", replace2);
                    message.addProperty("r", replace);
                    break;
                case 3:
                    message.addProperty("t", num);
                    message.addProperty("u", this.userId);
                    message.addProperty("ts", l);
                    break;
                case 5:
                    message.addProperty("u", this.userId);
                    message.addProperty("ts", l);
                    message.addProperty("t", num);
                    message.addProperty("op", String.valueOf(2));
                    break;
            }
            this.out.Write(message);
            return;
        }
    }

    public Message receiveMessage(int i) {
        this.socket.setSoTimeout(i * QDefine.ONE_SECOND);
        return this.in.Read();
    }

    public void sendAckMessage(String str) {
        synchronized (this) {
            Message message = new Message(this.miopVersion);
            message.addProperty("ack", str);
            message.addProperty("op", String.valueOf(4));
            System.out.println("send message ack " + str);
            this.out.Write(message);
        }
    }

    public void sendChangeMessage(int i) {
        Message message = new Message(this.miopVersion);
        message.addProperty("t", Integer.toString(i));
        this.out.Write(message);
    }

    public void sendPindMessage() {
        Message message = new Message(this.miopVersion);
        message.addProperty("op", String.valueOf(0));
        this.out.Write(message);
    }

    public void setMiopProduct(String str) {
        this.mProduct = str;
    }

    public void setMiopVersion(short s) {
        this.miopVersion = s;
    }
}
