package com.qihoo.miop;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.qihoo.messenger.util.QDefine;
import com.qihoo.miop.message.MessageData;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.u;
import com.qihoo360.daily.receiver.TPayloadReceiver;
import java.util.List;

public class PushService extends Service implements QHPushCallback {
    /* access modifiers changed from: private */
    public boolean isConnecting;
    private QHPushClient pushClient;

    /* access modifiers changed from: private */
    public int connect() {
        u.a("PushService start connect");
        try {
            return this.pushClient.asynStartLoop(this);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    private static String getUserId(Context context) {
        String metaData = QDefine.getMetaData(context, "QHOPENSDK_APPID");
        String a2 = a.a(context);
        u.a("tokenID:" + a2);
        return a2 + "@" + metaData;
    }

    private void startPush() {
        if (this.pushClient == null) {
            this.pushClient = new QHPushClient(getUserId(getApplicationContext()), 300, 5);
        }
        if (!this.pushClient.isWorking().booleanValue() && !this.isConnecting) {
            this.isConnecting = true;
            u.a("PushService startPush Thread");
            new Thread() {
                public void run() {
                    super.run();
                    int i = -1;
                    while (i == -1) {
                        i = PushService.this.connect();
                        if (i == -1) {
                            try {
                                Thread.sleep(30000);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            u.a("PushService connect failed reconnect");
                        } else {
                            u.a("PushService connect success");
                        }
                    }
                    boolean unused = PushService.this.isConnecting = false;
                }
            }.start();
        }
    }

    public void connectionLost(Throwable th) {
    }

    public void messageArrived(String str, List<MessageData> list) {
        for (MessageData body : list) {
            String body2 = body.getBody();
            Intent intent = new Intent(this, TPayloadReceiver.class);
            intent.putExtra("payload", body2);
            intent.setPackage(getPackageName());
            sendBroadcast(intent);
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        if (intent != null) {
            u.a("PushService onStartCommand action:" + intent.getAction());
        }
        startPush();
        return 1;
    }
}
