package com.qihoo.miop;

import com.qihoo.miop.message.MessageData;
import java.util.List;

public interface QHPushCallback {
    void connectionLost(Throwable th);

    void messageArrived(String str, List<MessageData> list);
}
