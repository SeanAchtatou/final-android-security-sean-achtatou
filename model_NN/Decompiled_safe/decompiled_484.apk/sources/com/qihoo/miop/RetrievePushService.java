package com.qihoo.miop;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.a.a.j;
import com.a.a.w;
import com.a.a.y;
import com.mediav.ads.sdk.adcore.Config;
import com.qihoo.dynamic.util.Md5Util;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.DailyDetailActivity;
import com.qihoo360.daily.activity.SendCmtActivity;
import com.qihoo360.daily.f.a;
import com.qihoo360.daily.h.ad;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.h.u;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.PushInfo;
import com.qihoo360.daily.model.PushMeta;
import java.io.StringReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.Header;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

public class RetrievePushService extends Service {
    public static final int PUSH_CLICK_TYPE_CMT = 2;
    public static final int PUSH_CLICK_TYPE_SHARE = 1;
    public static final String PUSH_META = "com.qihoo.miop.PUSH_META";
    public static final String RETRIEVE_PUSH_SERVICE = "com.qihoo.miop.Retrieve_Push_Service";
    private static int nId;
    private static String pushId;
    private static List<String> urls = new ArrayList(5);

    private static Notification buildNotification(PushInfo pushInfo, Context context, int i) {
        int hashCode = pushInfo.getUrl().hashCode();
        ad.a("push requestCode: " + hashCode);
        PendingIntent activity = PendingIntent.getActivity(context, hashCode, getIntentByVt(context, pushInfo), 134217728);
        String title = pushInfo.getTitle();
        String content = pushInfo.getContent();
        NotificationCompat.BigTextStyle bigContentTitle = new NotificationCompat.BigTextStyle().bigText(content).setBigContentTitle(title);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentTitle(title).setContentText(content).setTicker(title).setWhen(System.currentTimeMillis()).setTicker(pushInfo.getTitle()).setSmallIcon(R.drawable.notification_icon).setContentIntent(activity).setAutoCancel(true).setStyle(bigContentTitle);
        if (Config.CHANNEL_ID.equals(pushInfo.getEntrance())) {
            Intent intentByVt = getIntentByVt(context, pushInfo);
            intentByVt.putExtra("push_click_type", 1);
            intentByVt.putExtra("notificationId", i);
            PendingIntent activity2 = PendingIntent.getActivity(context, hashCode + 1, intentByVt, 134217728);
            Intent intentByVt2 = getIntentByVt(context, pushInfo);
            intentByVt2.putExtra("push_click_type", 2);
            intentByVt2.putExtra("notificationId", i);
            builder.addAction(R.drawable.ic_push_share, context.getString(R.string.notification_share), activity2).addAction(R.drawable.ic_push_comment, context.getString(R.string.notification_cmt), PendingIntent.getActivity(context, hashCode + 2, intentByVt2, 134217728));
        }
        int i2 = -1;
        if (!a.e(context)) {
            i2 = -2;
        }
        if (!a.f(context)) {
            i2 &= -3;
        }
        builder.setDefaults(i2);
        return builder.build();
    }

    private static Intent getIntentByVt(Context context, PushInfo pushInfo) {
        Intent intent = new Intent(context, DailyDetailActivity.class);
        pushInfo.setPaper(1);
        intent.putExtra("Info", pushInfo);
        intent.addFlags(67108864);
        intent.addFlags(65536);
        if (!TextUtils.isEmpty(pushId)) {
            intent.putExtra("PushId", pushId);
        }
        intent.putExtra("from", ChannelType.TYPE_PUSH);
        return intent;
    }

    private static int getNotificationId() {
        nId++;
        return nId % 3;
    }

    private static boolean isMsgRepeated(String str) {
        if (TextUtils.isEmpty(str) || urls.contains(str)) {
            ad.a("push isMsgRepeated");
            u.a("push isMsgRepeated");
            return true;
        }
        urls.add(str);
        int size = urls.size();
        ad.a("urls len: " + size);
        if (size <= 5) {
            return false;
        }
        urls.remove(0);
        return false;
    }

    public static void receiveMessage(String str, Context context) {
        PushInfo pushInfo;
        try {
            pushInfo = (PushInfo) new j().a(str, PushInfo.class);
        } catch (Exception e) {
            e.printStackTrace();
            pushInfo = null;
        }
        if (pushInfo != null && !isMsgRepeated(pushInfo.getUrl()) && a.d(context)) {
            int notificationId = getNotificationId();
            ad.a("notify notificationId: " + notificationId);
            ((NotificationManager) context.getSystemService("notification")).notify(notificationId, buildNotification(pushInfo, context, notificationId));
            b.b(context, "News_push_show");
        }
    }

    /* access modifiers changed from: protected */
    public void handleIntent(Intent intent) {
        u.a("onHandleIntent");
        if (intent != null && RETRIEVE_PUSH_SERVICE.equals(intent.getAction())) {
            PushMeta pushMeta = (PushMeta) intent.getParcelableExtra(PUSH_META);
            try {
                String pushUrl = pushMeta.getPushUrl();
                if (!TextUtils.isEmpty(pushUrl)) {
                    u.a("onHandleIntent msgid=" + pushMeta.getMsgID());
                    pushId = Uri.parse(pushUrl).getQueryParameter("pushId");
                    u.a("onHandleIntent pushId=" + pushId);
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(new BasicNameValuePair("msgID", pushMeta.getMsgID()));
                    arrayList.add(new BasicNameValuePair("token", com.qihoo360.daily.h.a.a(this)));
                    arrayList.add(new BasicNameValuePair("_cv", com.qihoo360.daily.h.a.b(this)));
                    com.qihoo360.daily.d.a.a(arrayList);
                    u.a("onHandleIntent addCommonParams");
                    String format = URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET);
                    u.a("onHandleIntent format Params");
                    StringBuilder sb = new StringBuilder();
                    sb.append(pushUrl).append("&").append(format);
                    URI create = URI.create(sb.toString());
                    u.a("fetch push uri: " + create.toString());
                    String a2 = com.qihoo360.daily.d.b.a(create, new Header[0]);
                    u.a("push getPushData: " + a2);
                    w k = new y().a(new com.a.a.d.a(new StringReader(a2))).k();
                    if (k.a("status").e() == 0) {
                        receiveMessage(k.a(SendCmtActivity.TAG_DATA).toString(), this);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        u.a("onCreate");
    }

    public int onStartCommand(final Intent intent, int i, int i2) {
        u.a("onStartCommand");
        if (intent != null && RETRIEVE_PUSH_SERVICE.equals(intent.getAction())) {
            PushMeta pushMeta = (PushMeta) intent.getParcelableExtra(PUSH_META);
            if (pushMeta != null) {
                u.a("onStartCommand msgid=" + pushMeta.getMsgID());
            }
            new Thread() {
                public void run() {
                    RetrievePushService.this.handleIntent(intent);
                }
            }.start();
        }
        return super.onStartCommand(intent, i, i2);
    }
}
