package com.qihoo.miop.message;

import java.io.OutputStream;

public class MessageOutputStream {
    private OutputStream out;

    public MessageOutputStream(OutputStream outputStream) {
        this.out = outputStream;
    }

    public void Write(Message message) {
        this.out.write(message.toByte());
    }
}
