package com.qihoo.miop.message;

import com.qihoo.dynamic.util.Md5Util;
import com.qihoo.messenger.util.FormatUtil;
import java.io.DataInputStream;
import java.io.InputStream;

public class MessageInputStream {
    private DataInputStream in;

    public MessageInputStream(InputStream inputStream) {
        this.in = new DataInputStream(inputStream);
    }

    public Message Read() {
        byte[] bArr = new byte[4];
        this.in.readFully(bArr);
        short stream2Short = FormatUtil.stream2Short(bArr, 0);
        short stream2Short2 = FormatUtil.stream2Short(bArr, 2);
        if (stream2Short2 == 1 || stream2Short2 == 0 || stream2Short2 == 7) {
            Message message = new Message(stream2Short);
            message.addProperty("op", String.valueOf((int) stream2Short2));
            return message;
        }
        byte[] bArr2 = new byte[2];
        this.in.readFully(bArr2);
        int stream2Short3 = FormatUtil.stream2Short(bArr2, 0);
        Message message2 = new Message(stream2Short);
        message2.addProperty("op", String.valueOf((int) stream2Short2));
        if (stream2Short3 > 0) {
            byte[] bArr3 = new byte[stream2Short3];
            this.in.readFully(bArr3);
            String[] split = new String(bArr3, Md5Util.DEFAULT_CHARSET).split("\n");
            for (String split2 : split) {
                String[] split3 = split2.split(":");
                message2.addProperty(split3[0], split3[1]);
            }
        }
        if (stream2Short2 == 4 || stream2Short2 == 6) {
            return message2;
        }
        byte[] bArr4 = new byte[4];
        this.in.readFully(bArr4);
        int stream2Int = FormatUtil.stream2Int(bArr4, 0);
        if (stream2Int > 0) {
            byte[] bArr5 = new byte[stream2Int];
            this.in.readFully(bArr5);
            int i = 0;
            while (i < stream2Int) {
                long stream2Long = FormatUtil.stream2Long(bArr5, i);
                int i2 = i + 8;
                int stream2Int2 = FormatUtil.stream2Int(bArr5, i2);
                int i3 = i2 + 4;
                int stream2Int3 = FormatUtil.stream2Int(bArr5, i3);
                int i4 = i3 + 4;
                byte[] bArr6 = new byte[stream2Int3];
                if (stream2Int3 > 0) {
                    for (int i5 = 0; i5 < stream2Int3; i5++) {
                        bArr6[i5] = bArr5[i5 + i4];
                    }
                }
                i = i4 + stream2Int3;
                message2.addMessageData(new MessageData(stream2Long, (long) stream2Int2, bArr6));
            }
        }
        return message2;
    }
}
