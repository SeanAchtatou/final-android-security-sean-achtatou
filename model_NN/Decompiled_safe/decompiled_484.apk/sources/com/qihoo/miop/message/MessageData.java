package com.qihoo.miop.message;

import com.qihoo.messenger.util.FormatUtil;
import java.util.Arrays;

public class MessageData {
    private long appId;
    private byte[] body;
    private long messageId;

    public MessageData(long j, long j2, byte[] bArr) {
        this.messageId = j;
        this.appId = j2;
        this.body = bArr;
    }

    public long getAppId() {
        return this.appId;
    }

    public String getBody() {
        return new String(this.body);
    }

    public byte[] getBodyBytes() {
        return this.body;
    }

    public int getBodyLength() {
        return this.body.length;
    }

    public long getMessageId() {
        return this.messageId;
    }

    public byte[] toBytes() {
        byte[] long2Stream = FormatUtil.long2Stream(this.messageId);
        byte[] int2Stream = FormatUtil.int2Stream(this.body.length);
        byte[] bArr = new byte[(this.body.length + 12)];
        for (int i = 0; i < long2Stream.length; i++) {
            bArr[(8 - long2Stream.length) + i] = long2Stream[i];
        }
        int i2 = 8;
        int i3 = 0;
        while (i3 < 4) {
            bArr[i2 + i3] = int2Stream[i3];
            i3++;
            i2++;
        }
        for (int i4 = 0; i4 < this.body.length; i4++) {
            bArr[i2 + i4] = this.body[i4];
            i2++;
        }
        return bArr;
    }

    public String toString() {
        return "MessageData{messageId=" + this.messageId + ", appId=" + this.appId + ", body=" + Arrays.toString(this.body) + '}';
    }
}
