package com.qihoo.miop.message;

import com.qihoo.messenger.util.FormatUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Message {
    public static final short MIOP5 = 5;
    public static final short NEED_ACK_AND_ENCRYPT = 4;
    public static final short ONLY_NEED_ACK = 3;
    public static final short ONLY_NEED_ENCRYPT = 2;
    public static final short OpcodeBind = 2;
    public static final short OpcodeBindAck = 6;
    public static final short OpcodeMsg = 3;
    public static final short OpcodeMsgAck = 4;
    public static final short OpcodePing = 0;
    public static final short OpcodePong = 1;
    public static final short OpcodeUnbind = 5;
    public static final short OpcodeUnbindAck = 7;
    private List<MessageData> messageDatas;
    private Map<String, String> properties;
    private short version;

    public Message() {
        this.version = 3;
        this.properties = new HashMap();
        this.messageDatas = new ArrayList();
    }

    public Message(short s) {
        this.version = s;
        this.properties = new HashMap();
        this.messageDatas = new ArrayList();
    }

    private byte[] messageData2Byte() {
        int i = 0;
        for (int i2 = 0; i2 < this.messageDatas.size(); i2++) {
            i += this.messageDatas.get(i2).toBytes().length;
        }
        byte[] bArr = new byte[i];
        int i3 = 0;
        int i4 = 0;
        while (i3 < this.messageDatas.size()) {
            int i5 = i4;
            for (byte b2 : this.messageDatas.get(i3).toBytes()) {
                bArr[i5] = b2;
                i5++;
            }
            i3++;
            i4 = i5;
        }
        return bArr;
    }

    private byte[] properties2Byte() {
        String str = "";
        for (String next : this.properties.keySet()) {
            if (str != "") {
                str = str + "\n";
            }
            str = str + next + ":" + this.properties.get(next);
        }
        return str == "" ? new byte[0] : str.getBytes();
    }

    /* access modifiers changed from: protected */
    public void addMessageData(MessageData messageData) {
        this.messageDatas.add(messageData);
    }

    public void addProperty(String str, String str2) {
        this.properties.put(str, str2);
    }

    public List<MessageData> getMessageDatas() {
        return this.messageDatas;
    }

    public String getPropery(String str) {
        return this.properties.get(str);
    }

    public short getVersion() {
        return this.version;
    }

    public boolean isExistPropery(String str) {
        return this.properties.containsKey(str);
    }

    public byte[] toByte() {
        int i = 0;
        short shortValue = Short.valueOf(getPropery("op")).shortValue();
        this.properties.remove("op");
        if (shortValue == 0) {
            byte[] bArr = new byte[4];
            byte[] short2Stream = FormatUtil.short2Stream(this.version);
            for (int i2 = 0; i2 < 2; i2++) {
                bArr[0 + i2] = short2Stream[i2];
            }
            byte[] short2Stream2 = FormatUtil.short2Stream(shortValue);
            while (i < 2) {
                bArr[i + 2] = short2Stream2[i];
                i++;
            }
            return bArr;
        }
        byte[] properties2Byte = properties2Byte();
        byte[] messageData2Byte = messageData2Byte();
        byte[] bArr2 = new byte[(messageData2Byte.length == 0 ? properties2Byte.length + 6 + messageData2Byte.length : properties2Byte.length + 10 + messageData2Byte.length)];
        byte[] short2Stream3 = FormatUtil.short2Stream(this.version);
        for (int i3 = 0; i3 < 2; i3++) {
            bArr2[0 + i3] = short2Stream3[i3];
        }
        byte[] short2Stream4 = FormatUtil.short2Stream(shortValue);
        for (int i4 = 0; i4 < 2; i4++) {
            bArr2[i4 + 2] = short2Stream4[i4];
        }
        byte[] short2Stream5 = FormatUtil.short2Stream((short) properties2Byte.length);
        for (int i5 = 0; i5 < 2; i5++) {
            bArr2[i5 + 4] = short2Stream5[i5];
        }
        for (int i6 = 0; i6 < properties2Byte.length; i6++) {
            bArr2[i6 + 6] = properties2Byte[i6];
        }
        int length = properties2Byte.length + 6;
        if (messageData2Byte.length != 0) {
            byte[] int2Stream = FormatUtil.int2Stream(messageData2Byte.length);
            for (int i7 = 0; i7 < 4; i7++) {
                bArr2[length + i7] = int2Stream[i7];
            }
            int i8 = length + 4;
            while (i < messageData2Byte.length) {
                bArr2[i8 + i] = messageData2Byte[i];
                i++;
            }
            int length2 = i8 + messageData2Byte.length;
        }
        return bArr2;
    }
}
