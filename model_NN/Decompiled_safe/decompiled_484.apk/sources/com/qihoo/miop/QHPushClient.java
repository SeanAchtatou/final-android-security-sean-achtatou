package com.qihoo.miop;

import com.qihoo.messenger.util.QDefine;
import com.qihoo360.daily.h.ad;
import com.qihoo360.daily.h.u;
import java.io.IOException;

public class QHPushClient {
    /* access modifiers changed from: private */
    public long lastPingTime;
    /* access modifiers changed from: private */
    public Boolean mIsConnect;
    /* access modifiers changed from: private */
    public Boolean mIsWorking;
    /* access modifiers changed from: private */
    public MiopClient mMiopClient;
    /* access modifiers changed from: private */
    public String mUserId;

    public QHPushClient(String str, int i) {
        this.mUserId = str;
        this.mIsWorking = false;
        this.mIsConnect = false;
        this.mMiopClient = new MiopClient(this.mUserId, i);
    }

    public QHPushClient(String str, int i, int i2) {
        this(str, i);
        this.mMiopClient.setMiopVersion((short) i2);
        String str2 = str.split("@")[1];
        this.mMiopClient.setMiopProduct("newsreader_mt");
    }

    public int asynStartLoop(final QHPushCallback qHPushCallback) {
        ad.a("QHPushClient asynStartLoop");
        if (connect() == -1) {
            ad.a("QHPushClient connect fail");
            return -1;
        }
        new Thread(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:24:0x0124  */
            /* JADX WARNING: Removed duplicated region for block: B:30:0x0175  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r6 = this;
                    r1 = 0
                    r0 = r1
                L_0x0002:
                    com.qihoo.miop.QHPushClient r2 = com.qihoo.miop.QHPushClient.this
                    java.lang.Boolean r2 = r2.mIsWorking
                    boolean r2 = r2.booleanValue()
                    if (r2 == 0) goto L_0x01cf
                    com.qihoo.miop.QHPushClient r2 = com.qihoo.miop.QHPushClient.this     // Catch:{ InterruptedIOException -> 0x0104, IOException -> 0x0193 }
                    java.lang.Boolean r2 = r2.mIsConnect     // Catch:{ InterruptedIOException -> 0x0104, IOException -> 0x0193 }
                    boolean r2 = r2.booleanValue()     // Catch:{ InterruptedIOException -> 0x0104, IOException -> 0x0193 }
                    if (r2 == 0) goto L_0x0002
                    if (r0 == 0) goto L_0x0067
                    r2 = 30
                L_0x001e:
                    com.qihoo.miop.QHPushClient r3 = com.qihoo.miop.QHPushClient.this     // Catch:{ InterruptedIOException -> 0x0104, IOException -> 0x0193 }
                    com.qihoo.miop.MiopClient r3 = r3.mMiopClient     // Catch:{ InterruptedIOException -> 0x0104, IOException -> 0x0193 }
                    com.qihoo.miop.message.Message r2 = r3.receiveMessage(r2)     // Catch:{ InterruptedIOException -> 0x0104, IOException -> 0x0193 }
                    short r3 = r2.getVersion()     // Catch:{ InterruptedIOException -> 0x0104, IOException -> 0x0193 }
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ InterruptedIOException -> 0x0104, IOException -> 0x0193 }
                    r4.<init>()     // Catch:{ InterruptedIOException -> 0x0104, IOException -> 0x0193 }
                    java.lang.String r5 = "QHPushClient recv a  message    ver: "
                    java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ InterruptedIOException -> 0x0104, IOException -> 0x0193 }
                    java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ InterruptedIOException -> 0x0104, IOException -> 0x0193 }
                    java.lang.String r3 = r3.toString()     // Catch:{ InterruptedIOException -> 0x0104, IOException -> 0x0193 }
                    com.qihoo360.daily.h.u.a(r3)     // Catch:{ InterruptedIOException -> 0x0104, IOException -> 0x0193 }
                    java.lang.String r0 = "op"
                    java.lang.String r0 = r2.getPropery(r0)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    switch(r0) {
                        case 1: goto L_0x006a;
                        case 2: goto L_0x004f;
                        case 3: goto L_0x0082;
                        case 4: goto L_0x00b9;
                        case 5: goto L_0x004f;
                        case 6: goto L_0x00d2;
                        case 7: goto L_0x00eb;
                        default: goto L_0x004f;
                    }     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                L_0x004f:
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    r2.<init>()     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.lang.String r3 = "QHPushClient not support the    opcode: "
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.lang.String r0 = r0.toString()     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    com.qihoo360.daily.h.u.a(r0)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    r0 = r1
                    goto L_0x0002
                L_0x0067:
                    r2 = 200(0xc8, float:2.8E-43)
                    goto L_0x001e
                L_0x006a:
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    r2.<init>()     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.lang.String r3 = "QHPushClient recv a pong message    opCode: "
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.lang.String r0 = r0.toString()     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    com.qihoo360.daily.h.u.a(r0)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    r0 = r1
                    goto L_0x0002
                L_0x0082:
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    r3.<init>()     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.lang.String r4 = "QHPushClient recv a message   opCode: "
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.lang.String r0 = r0.toString()     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    com.qihoo360.daily.h.u.a(r0)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.util.List r0 = r2.getMessageDatas()     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    com.qihoo.miop.QHPushCallback r3 = r3     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    com.qihoo.miop.QHPushClient r4 = com.qihoo.miop.QHPushClient.this     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.lang.String r4 = r4.mUserId     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    r3.messageArrived(r4, r0)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.lang.String r0 = "ack"
                    java.lang.String r0 = r2.getPropery(r0)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    com.qihoo.miop.QHPushClient r2 = com.qihoo.miop.QHPushClient.this     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    com.qihoo.miop.MiopClient r2 = r2.mMiopClient     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    r2.sendAckMessage(r0)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    r0 = r1
                    goto L_0x0002
                L_0x00b9:
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    r2.<init>()     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.lang.String r3 = "QHPushClient recv a ack message   opCode: "
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.lang.String r0 = r0.toString()     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    com.qihoo360.daily.h.u.a(r0)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    r0 = r1
                    goto L_0x0002
                L_0x00d2:
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    r2.<init>()     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.lang.String r3 = "QHPushClient recv a connected message    opCode: "
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.lang.String r0 = r0.toString()     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    com.qihoo360.daily.h.u.a(r0)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    r0 = r1
                    goto L_0x0002
                L_0x00eb:
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    r2.<init>()     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.lang.String r3 = "QHPushClient recv a connected message    opCode: "
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    java.lang.String r0 = r0.toString()     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    com.qihoo360.daily.h.u.a(r0)     // Catch:{ InterruptedIOException -> 0x01d4, IOException -> 0x01d0 }
                    r0 = r1
                    goto L_0x0002
                L_0x0104:
                    r2 = move-exception
                L_0x0105:
                    r2.printStackTrace()
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    java.lang.String r4 = "QHPushClient InterruptedIOException: "
                    java.lang.StringBuilder r3 = r3.append(r4)
                    java.lang.String r4 = r2.toString()
                    java.lang.StringBuilder r3 = r3.append(r4)
                    java.lang.String r3 = r3.toString()
                    com.qihoo360.daily.h.u.a(r3)
                    if (r0 != 0) goto L_0x0175
                    java.lang.String r2 = "QHPushClient sendPindMessage"
                    com.qihoo360.daily.h.u.a(r2)     // Catch:{ IOException -> 0x013e }
                    com.qihoo.miop.QHPushClient r2 = com.qihoo.miop.QHPushClient.this     // Catch:{ IOException -> 0x013e }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x013e }
                    long unused = r2.lastPingTime = r4     // Catch:{ IOException -> 0x013e }
                    com.qihoo.miop.QHPushClient r2 = com.qihoo.miop.QHPushClient.this     // Catch:{ IOException -> 0x013e }
                    com.qihoo.miop.MiopClient r2 = r2.mMiopClient     // Catch:{ IOException -> 0x013e }
                    r2.sendPindMessage()     // Catch:{ IOException -> 0x013e }
                    r0 = 1
                    goto L_0x0002
                L_0x013e:
                    r2 = move-exception
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    java.lang.String r4 = "QHPushClient connectionLost sendPindMessage:"
                    java.lang.StringBuilder r3 = r3.append(r4)
                    java.lang.String r4 = r2.toString()
                    java.lang.StringBuilder r3 = r3.append(r4)
                    java.lang.String r3 = r3.toString()
                    com.qihoo360.daily.h.u.a(r3)
                    r2.printStackTrace()
                    com.qihoo.miop.QHPushCallback r3 = r3
                    r3.connectionLost(r2)
                    com.qihoo.miop.QHPushClient r2 = com.qihoo.miop.QHPushClient.this
                    java.lang.Boolean r3 = java.lang.Boolean.valueOf(r1)
                    java.lang.Boolean unused = r2.mIsWorking = r3
                    com.qihoo.miop.QHPushClient r2 = com.qihoo.miop.QHPushClient.this
                    java.lang.Boolean r3 = java.lang.Boolean.valueOf(r1)
                    java.lang.Boolean unused = r2.mIsConnect = r3
                    goto L_0x0002
                L_0x0175:
                    java.lang.String r3 = "QHPushClient connectionLost"
                    com.qihoo360.daily.h.u.a(r3)
                    com.qihoo.miop.QHPushCallback r3 = r3
                    r3.connectionLost(r2)
                    com.qihoo.miop.QHPushClient r2 = com.qihoo.miop.QHPushClient.this
                    java.lang.Boolean r3 = java.lang.Boolean.valueOf(r1)
                    java.lang.Boolean unused = r2.mIsWorking = r3
                    com.qihoo.miop.QHPushClient r2 = com.qihoo.miop.QHPushClient.this
                    java.lang.Boolean r3 = java.lang.Boolean.valueOf(r1)
                    java.lang.Boolean unused = r2.mIsConnect = r3
                    goto L_0x0002
                L_0x0193:
                    r2 = move-exception
                L_0x0194:
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    java.lang.String r4 = "QHPushClient InterruptedIOException: "
                    java.lang.StringBuilder r3 = r3.append(r4)
                    java.lang.String r4 = r2.toString()
                    java.lang.StringBuilder r3 = r3.append(r4)
                    java.lang.String r3 = r3.toString()
                    com.qihoo360.daily.h.u.a(r3)
                    r2.printStackTrace()
                    com.qihoo.miop.QHPushCallback r3 = r3
                    r3.connectionLost(r2)
                    java.lang.String r2 = "QHPushClient connectionLost"
                    com.qihoo360.daily.h.u.a(r2)
                    com.qihoo.miop.QHPushClient r2 = com.qihoo.miop.QHPushClient.this
                    java.lang.Boolean r3 = java.lang.Boolean.valueOf(r1)
                    java.lang.Boolean unused = r2.mIsWorking = r3
                    com.qihoo.miop.QHPushClient r2 = com.qihoo.miop.QHPushClient.this
                    java.lang.Boolean r3 = java.lang.Boolean.valueOf(r1)
                    java.lang.Boolean unused = r2.mIsConnect = r3
                    goto L_0x0002
                L_0x01cf:
                    return
                L_0x01d0:
                    r0 = move-exception
                    r2 = r0
                    r0 = r1
                    goto L_0x0194
                L_0x01d4:
                    r0 = move-exception
                    r2 = r0
                    r0 = r1
                    goto L_0x0105
                */
                throw new UnsupportedOperationException("Method not decompiled: com.qihoo.miop.QHPushClient.AnonymousClass1.run():void");
            }
        }).start();
        return 0;
    }

    public int connect() {
        u.a("QHPushClient connecting");
        this.mIsWorking = true;
        this.mIsConnect = true;
        try {
            this.lastPingTime = System.currentTimeMillis();
            this.mMiopClient.connect();
            u.a("QHPushClient connect ok");
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            u.a("QHPushClient connect Exception: " + e.toString());
            this.mIsWorking = false;
            this.mIsConnect = false;
            return -1;
        }
    }

    public boolean isBlock() {
        long currentTimeMillis = System.currentTimeMillis() - this.lastPingTime;
        u.a("QHPushClient dTime: " + currentTimeMillis);
        return currentTimeMillis > QDefine.WIFI_ACCESS_PERIOD;
    }

    public Boolean isWorking() {
        return this.mIsWorking;
    }

    public void stopPushService() {
        this.mIsWorking = false;
        this.mIsConnect = false;
        try {
            this.mMiopClient.close();
        } catch (IOException e) {
            System.out.println("close success");
        }
        this.mMiopClient = null;
    }
}
