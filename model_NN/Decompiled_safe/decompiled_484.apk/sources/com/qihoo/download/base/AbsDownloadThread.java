package com.qihoo.download.base;

import java.util.List;
import java.util.Map;

public abstract class AbsDownloadThread extends Thread {
    protected static final int NO_ERROR = -1;
    protected long mDownloadSize;
    protected String mDownloadUrl;
    protected int mErrorCode;
    protected long mFileOffset;
    protected String mSavePath;
    protected IDownloadThreadListener mThreadListener;

    public interface IDownloadThreadListener {
        void onDownloadSizeChanged(AbsDownloadThread absDownloadThread, long j);

        void onResponseReturned(AbsDownloadThread absDownloadThread, long j, Map<String, List<String>> map);

        void onThreadFail(AbsDownloadThread absDownloadThread, int i);

        void onThreadSucess(AbsDownloadThread absDownloadThread);
    }

    public AbsDownloadThread(String str, String str2) {
        this.mDownloadUrl = str;
        this.mSavePath = str2;
    }

    public long getDownloadedPosition() {
        return this.mFileOffset;
    }

    /* access modifiers changed from: protected */
    public void onDownloadError() {
        if (this.mThreadListener != null) {
            this.mThreadListener.onThreadFail(this, this.mErrorCode);
        }
    }

    /* access modifiers changed from: protected */
    public void onDownloadSuccess() {
        if (this.mThreadListener != null) {
            this.mThreadListener.onThreadSucess(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onLengthReturned(long j, Map<String, List<String>> map) {
        if (this.mThreadListener != null) {
            this.mThreadListener.onResponseReturned(this, j, map);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(long j) {
        if (this.mThreadListener != null) {
            this.mThreadListener.onDownloadSizeChanged(this, j);
        }
    }

    public void run() {
        super.run();
        startDownload();
    }

    public void setDownloadThreadListener(IDownloadThreadListener iDownloadThreadListener) {
        this.mThreadListener = iDownloadThreadListener;
    }

    public void setStartDownloadPostion(long j) {
        this.mFileOffset = j;
    }

    public abstract void startDownload();

    public abstract void stopDownload();
}
