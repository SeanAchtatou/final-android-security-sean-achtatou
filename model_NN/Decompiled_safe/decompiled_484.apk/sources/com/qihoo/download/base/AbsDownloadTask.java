package com.qihoo.download.base;

import com.qihoo.download.base.AbsDownloadThread;
import java.util.List;
import java.util.Map;

public abstract class AbsDownloadTask implements AbsDownloadThread.IDownloadThreadListener {
    protected boolean mAllow3GDownload = false;
    protected int mDownloadErrorCode = 6;
    protected IDownloadTaskListener mDownloadListener;
    protected long mDownloadSize;
    protected int mDownloadSpeed;
    protected int mDownloadStatus = 10;
    protected String mDownloadUrl;
    protected String mSavePath;
    protected long mTotalSize;

    public void deleteDownload() {
        this.mDownloadStatus = 70;
    }

    public int getDownloadErrorCode() {
        return this.mDownloadErrorCode;
    }

    public long getDownloadSize() {
        return this.mDownloadSize;
    }

    public int getDownloadSpeed() {
        return this.mDownloadSpeed;
    }

    public int getDownloadStatus() {
        return this.mDownloadStatus;
    }

    public String getSavePath() {
        return this.mSavePath;
    }

    public long getTotalSize() {
        return this.mTotalSize;
    }

    public boolean isDownloadFailed() {
        return this.mDownloadStatus == 50;
    }

    public boolean isDownloadTerminal() {
        return this.mDownloadStatus == 80;
    }

    public boolean isDownloadWaitting() {
        return this.mDownloadStatus == 10;
    }

    public boolean isDownloaded() {
        return this.mDownloadStatus == 60;
    }

    public boolean isDownloading() {
        return this.mDownloadStatus == 20;
    }

    /* access modifiers changed from: protected */
    public void notifyTaskSizeChanged() {
        if (this.mDownloadListener != null) {
            this.mDownloadListener.onTaskDownloadSizeChanged(this);
        }
    }

    /* access modifiers changed from: protected */
    public void notifyTaskSpeedChaned() {
        if (this.mDownloadListener != null) {
            this.mDownloadListener.onTaskSpeedSizeChanged(this);
        }
    }

    /* access modifiers changed from: protected */
    public void notifyTaskStatusChanged() {
        if (this.mDownloadListener != null) {
            this.mDownloadListener.onTaskStatusChanged(this);
        }
    }

    public void onDownloadSizeChanged(AbsDownloadThread absDownloadThread, long j) {
        notifyTaskSizeChanged();
    }

    public abstract void onResponseReturned(AbsDownloadThread absDownloadThread, long j, Map<String, List<String>> map);

    public void onThreadFail(AbsDownloadThread absDownloadThread, int i) {
        notifyTaskStatusChanged();
    }

    public void onThreadSucess(AbsDownloadThread absDownloadThread) {
        notifyTaskStatusChanged();
    }

    public void setAllow3GDownload(boolean z) {
        this.mAllow3GDownload = z;
    }

    /* access modifiers changed from: protected */
    public void setDownloadStatus(int i) {
        this.mDownloadStatus = i;
    }

    public void setTaskListener(IDownloadTaskListener iDownloadTaskListener) {
        this.mDownloadListener = iDownloadTaskListener;
    }

    public void startDownload() {
        this.mDownloadStatus = 20;
    }

    public void stopDownload() {
        this.mDownloadStatus = 30;
    }

    public void waittingDownload() {
        this.mDownloadStatus = 10;
    }
}
