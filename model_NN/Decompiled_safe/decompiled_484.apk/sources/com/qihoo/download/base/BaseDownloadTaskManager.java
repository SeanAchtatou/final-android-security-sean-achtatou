package com.qihoo.download.base;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class BaseDownloadTaskManager extends AbsDownloadTaskManager {
    private static final int MAX_DOWNLOAD_COUNT = 1;
    private static final int MSG_FOR_SIZE_CHANGED = 2;
    private static final int MSG_FOR_SPEED_CHANGED = 3;
    private static final int MSG_FOR_STATUS_CHANGED = 1;
    private static final long NOTIFT_DELAY = 500;
    private static final String TAG = "BaseDownloadTaskManager";
    protected ArrayList<AbsDownloadTask> mDownloadedTaskList;
    protected ArrayList<AbsDownloadTask> mDownloadingTaskList;
    protected Handler mHandler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            AbsDownloadTask absDownloadTask = (AbsDownloadTask) message.obj;
            switch (message.what) {
                case 1:
                    BaseDownloadTaskManager.this.executeStatusChanged(absDownloadTask);
                    return;
                case 2:
                    BaseDownloadTaskManager.this.mHandler.removeMessages(2);
                    BaseDownloadTaskManager.this.notifyDownloadSizeChanged(absDownloadTask);
                    return;
                case 3:
                    BaseDownloadTaskManager.this.mHandler.removeMessages(3);
                    BaseDownloadTaskManager.this.notifySpeedSizeChanged(absDownloadTask);
                    return;
                default:
                    return;
            }
        }
    };
    private long mLastNofifyTime;

    public BaseDownloadTaskManager(Context context) {
        this.mDownloadCount = 1;
        this.mDownloadingTaskList = new ArrayList<>();
        this.mDownloadedTaskList = new ArrayList<>();
    }

    private void deleleDownloadTask(ArrayList<AbsDownloadTask> arrayList) {
        synchronized (arrayList) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                super.deleteTask(arrayList.get(i));
            }
            arrayList.clear();
        }
    }

    private int getWorkingTaskCount() {
        int i;
        int i2 = 0;
        if (this.mDownloadingTaskList != null) {
            synchronized (this.mDownloadingTaskList) {
                Iterator<AbsDownloadTask> it = this.mDownloadingTaskList.iterator();
                i = 0;
                while (it.hasNext()) {
                    if (it.next().isDownloading()) {
                        i++;
                    }
                }
            }
            i2 = i;
        }
        Log.e(TAG, "downloading task count: " + i2);
        return i2;
    }

    private void removeTaskFromList(ArrayList<AbsDownloadTask> arrayList, AbsDownloadTask absDownloadTask) {
        if (arrayList != null && arrayList.size() > 0) {
            synchronized (arrayList) {
                if (arrayList.size() > 0) {
                    arrayList.remove(absDownloadTask);
                }
            }
        }
    }

    private void sendMessage(int i, Object obj) {
        sendMessage(i, obj, 0);
    }

    private void sendMessage(int i, Object obj, long j) {
        Message obtainMessage = this.mHandler.obtainMessage(i);
        obtainMessage.obj = obj;
        if (j > 0) {
            this.mHandler.sendMessageDelayed(obtainMessage, j);
        } else {
            this.mHandler.sendMessage(obtainMessage);
        }
    }

    /* access modifiers changed from: protected */
    public void addTask(AbsDownloadTask absDownloadTask) {
        if (absDownloadTask == null) {
            return;
        }
        if (absDownloadTask.isDownloaded()) {
            synchronized (this.mDownloadedTaskList) {
                this.mDownloadedTaskList.add(absDownloadTask);
            }
            return;
        }
        absDownloadTask.setTaskListener(this);
        synchronized (this.mDownloadingTaskList) {
            this.mDownloadingTaskList.add(absDownloadTask);
        }
    }

    /* access modifiers changed from: protected */
    public void deleteAllDownloadedTask() {
        deleleDownloadTask(this.mDownloadedTaskList);
    }

    /* access modifiers changed from: protected */
    public void deleteAllDownloadingTask() {
        deleleDownloadTask(this.mDownloadingTaskList);
    }

    public void deleteTask(AbsDownloadTask absDownloadTask) {
        if (isExistTask(absDownloadTask)) {
            super.deleteTask(absDownloadTask);
            if (absDownloadTask.isDownloaded()) {
                removeTaskFromList(this.mDownloadedTaskList, absDownloadTask);
                notifyStatusChanged(absDownloadTask);
                return;
            }
            removeTaskFromList(this.mDownloadingTaskList, absDownloadTask);
            notifyStatusChanged(absDownloadTask);
            startSequence();
        }
    }

    /* access modifiers changed from: protected */
    public void executeStatusChanged(AbsDownloadTask absDownloadTask) {
        switch (absDownloadTask.getDownloadStatus()) {
            case 50:
                onTaskError(absDownloadTask);
                return;
            case 60:
                onTaskFinished(absDownloadTask);
                return;
            default:
                notifyStatusChanged(absDownloadTask);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public AbsDownloadTask findTask(Object obj) {
        AbsDownloadTask absDownloadTask = null;
        if (obj != null) {
            synchronized (this.mDownloadingTaskList) {
                absDownloadTask = findTask(obj, this.mDownloadingTaskList);
            }
            if (absDownloadTask == null) {
                synchronized (this.mDownloadedTaskList) {
                    absDownloadTask = findTask(obj, this.mDownloadedTaskList);
                }
            }
        }
        return absDownloadTask;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.qihoo.download.base.AbsDownloadTask findTask(java.lang.Object r3, java.util.ArrayList<com.qihoo.download.base.AbsDownloadTask> r4) {
        /*
            r2 = this;
            monitor-enter(r4)
            int r0 = r2.findTaskIndex(r3, r4)     // Catch:{ all -> 0x0018 }
            if (r0 < 0) goto L_0x0015
            int r1 = r4.size()     // Catch:{ all -> 0x0018 }
            if (r0 >= r1) goto L_0x0015
            java.lang.Object r0 = r4.get(r0)     // Catch:{ all -> 0x0018 }
            com.qihoo.download.base.AbsDownloadTask r0 = (com.qihoo.download.base.AbsDownloadTask) r0     // Catch:{ all -> 0x0018 }
            monitor-exit(r4)     // Catch:{ all -> 0x0018 }
        L_0x0014:
            return r0
        L_0x0015:
            monitor-exit(r4)     // Catch:{ all -> 0x0018 }
            r0 = 0
            goto L_0x0014
        L_0x0018:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0018 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo.download.base.BaseDownloadTaskManager.findTask(java.lang.Object, java.util.ArrayList):com.qihoo.download.base.AbsDownloadTask");
    }

    public int findTaskIndex(Object obj, ArrayList<AbsDownloadTask> arrayList) {
        if (obj != null) {
            synchronized (arrayList) {
                int size = arrayList.size();
                for (int i = 0; i < size; i++) {
                    if (arrayList.get(i).equals(obj)) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }

    public int getDownloadedTaskCount() {
        if (this.mDownloadedTaskList == null) {
            return 0;
        }
        return this.mDownloadedTaskList.size();
    }

    public List<AbsDownloadTask> getDownloadedTaskList() {
        return this.mDownloadedTaskList;
    }

    public int getDownloadingTaskCount() {
        if (this.mDownloadingTaskList == null) {
            return 0;
        }
        return this.mDownloadingTaskList.size();
    }

    public List<AbsDownloadTask> getDownloadingTaskList() {
        return this.mDownloadingTaskList;
    }

    /* access modifiers changed from: protected */
    public boolean isExistTask(Object obj) {
        return (obj == null || findTask(obj) == null) ? false : true;
    }

    /* access modifiers changed from: protected */
    public AbsDownloadTask nextWaittingTask() {
        synchronized (this.mDownloadingTaskList) {
            Iterator<AbsDownloadTask> it = this.mDownloadingTaskList.iterator();
            while (it.hasNext()) {
                AbsDownloadTask next = it.next();
                if (next.isDownloadWaitting()) {
                    return next;
                }
            }
            return null;
        }
    }

    public void onTaskDownloadSizeChanged(AbsDownloadTask absDownloadTask) {
        if (System.currentTimeMillis() - this.mLastNofifyTime >= NOTIFT_DELAY) {
            sendMessage(2, absDownloadTask, 0);
            this.mLastNofifyTime = System.currentTimeMillis();
        }
    }

    public void onTaskError(AbsDownloadTask absDownloadTask) {
        Log.d(TAG, "onTaskError error: " + absDownloadTask.mDownloadErrorCode);
        notifyStatusChanged(absDownloadTask);
        startSequence();
    }

    public void onTaskFinished(AbsDownloadTask absDownloadTask) {
        removeTaskFromList(this.mDownloadingTaskList, absDownloadTask);
        this.mDownloadedTaskList.add(absDownloadTask);
        notifyStatusChanged(absDownloadTask);
        startSequence();
    }

    public void onTaskSpeedSizeChanged(AbsDownloadTask absDownloadTask) {
        sendMessage(3, absDownloadTask);
    }

    public void onTaskStatusChanged(AbsDownloadTask absDownloadTask) {
        sendMessage(1, absDownloadTask);
    }

    /* access modifiers changed from: protected */
    public void startAllTask() {
        synchronized (this.mDownloadingTaskList) {
            int size = this.mDownloadingTaskList.size();
            for (int i = 0; i < size; i++) {
                startTask(this.mDownloadingTaskList.get(i));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void startSequence() {
        AbsDownloadTask nextWaittingTask;
        if (getWorkingTaskCount() < this.mDownloadCount && (nextWaittingTask = nextWaittingTask()) != null) {
            super.startTask(nextWaittingTask);
            notifyStatusChanged(nextWaittingTask);
        }
    }

    public void startTask(AbsDownloadTask absDownloadTask) {
        if (isExistTask(absDownloadTask) && !absDownloadTask.isDownloading()) {
            if (absDownloadTask.getDownloadStatus() != 10) {
                absDownloadTask.waittingDownload();
                notifyStatusChanged(absDownloadTask);
            }
            startSequence();
        }
    }

    /* access modifiers changed from: protected */
    public void stopAllTask() {
        synchronized (this.mDownloadingTaskList) {
            int size = this.mDownloadingTaskList.size();
            for (int i = 0; i < size; i++) {
                super.stopTask(this.mDownloadingTaskList.get(i));
                notifyStatusChanged(this.mDownloadingTaskList.get(i));
            }
        }
    }

    public void stopTask(AbsDownloadTask absDownloadTask) {
        if (isExistTask(absDownloadTask)) {
            super.stopTask(absDownloadTask);
            notifyStatusChanged(absDownloadTask);
            startSequence();
        }
    }
}
