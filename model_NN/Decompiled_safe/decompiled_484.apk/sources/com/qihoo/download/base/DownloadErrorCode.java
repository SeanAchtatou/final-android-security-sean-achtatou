package com.qihoo.download.base;

public class DownloadErrorCode {
    public static final int ERROR_FOR_FILE_DEELTED = 1;
    public static final int ERROR_FOR_MERGE_NOSPACE = 7;
    public static final int ERROR_FOR_MERGE_OTHER = 8;
    public static final int ERROR_FOR_NETWORK = 2;
    public static final int ERROR_FOR_NO_SUPPORT_3G = 10;
    public static final int ERROR_FOR_OTHER = 6;
    public static final int ERROR_FOR_SD_NOSPACE = 5;
    public static final int ERROR_FOR_SD_UNMOUNTED = 4;
    public static final int ERROR_FOR_URL_ERROR = 11;
    public static final int YUNPAN_ACCOUNT_ERROR = 9;
}
