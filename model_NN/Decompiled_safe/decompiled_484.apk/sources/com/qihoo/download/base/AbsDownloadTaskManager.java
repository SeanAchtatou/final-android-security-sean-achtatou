package com.qihoo.download.base;

import java.util.ArrayList;
import java.util.List;

public abstract class AbsDownloadTaskManager implements IDownloadTaskListener {
    protected int mDownloadCount;
    protected List<IDownloadManagerListener> mManagerListenerList = new ArrayList();

    public interface IDownloadManagerListener {
        void onDownloadSizeChanged(AbsDownloadTask absDownloadTask);

        void onDownloadStatusChanged(AbsDownloadTask absDownloadTask);

        void onSpeedSizeChanged(AbsDownloadTask absDownloadTask);
    }

    public void addDownloadManagerListener(IDownloadManagerListener iDownloadManagerListener) {
        if (this.mManagerListenerList != null) {
            synchronized (this.mManagerListenerList) {
                if (!this.mManagerListenerList.contains(iDownloadManagerListener)) {
                    this.mManagerListenerList.add(iDownloadManagerListener);
                }
            }
        }
    }

    public abstract AbsDownloadTask createTask();

    public abstract AbsDownloadTask createTask(Object obj);

    /* access modifiers changed from: protected */
    public abstract void deleteAllDownloadedTask();

    /* access modifiers changed from: protected */
    public abstract void deleteAllDownloadingTask();

    public void deleteDownloadManagerListener(IDownloadManagerListener iDownloadManagerListener) {
        if (this.mManagerListenerList != null) {
            synchronized (this.mManagerListenerList) {
                int indexOf = this.mManagerListenerList.indexOf(iDownloadManagerListener);
                if (indexOf > -1) {
                    this.mManagerListenerList.remove(indexOf);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void deleteTask(AbsDownloadTask absDownloadTask) {
        if (absDownloadTask != null && !absDownloadTask.isDownloadWaitting()) {
            absDownloadTask.deleteDownload();
        }
    }

    /* access modifiers changed from: protected */
    public abstract int getDownloadedTaskCount();

    /* access modifiers changed from: protected */
    public abstract int getDownloadingTaskCount();

    /* access modifiers changed from: protected */
    public void notifyDownloadSizeChanged(AbsDownloadTask absDownloadTask) {
        if (this.mManagerListenerList != null) {
            synchronized (this.mManagerListenerList) {
                for (IDownloadManagerListener onDownloadSizeChanged : this.mManagerListenerList) {
                    onDownloadSizeChanged.onDownloadSizeChanged(absDownloadTask);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void notifySpeedSizeChanged(AbsDownloadTask absDownloadTask) {
        if (this.mManagerListenerList != null) {
            synchronized (this.mManagerListenerList) {
                for (IDownloadManagerListener onSpeedSizeChanged : this.mManagerListenerList) {
                    onSpeedSizeChanged.onSpeedSizeChanged(absDownloadTask);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void notifyStatusChanged(AbsDownloadTask absDownloadTask) {
        if (this.mManagerListenerList != null) {
            synchronized (this.mManagerListenerList) {
                for (IDownloadManagerListener onDownloadStatusChanged : this.mManagerListenerList) {
                    onDownloadStatusChanged.onDownloadStatusChanged(absDownloadTask);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract void startAllTask();

    /* access modifiers changed from: protected */
    public void startTask(AbsDownloadTask absDownloadTask) {
        if (absDownloadTask != null) {
            absDownloadTask.startDownload();
        }
    }

    /* access modifiers changed from: protected */
    public abstract void stopAllTask();

    /* access modifiers changed from: protected */
    public void stopTask(AbsDownloadTask absDownloadTask) {
        if (absDownloadTask != null) {
            absDownloadTask.stopDownload();
        }
    }
}
