package com.qihoo.download.base;

public interface IDownloadTaskListener {
    void onTaskDownloadSizeChanged(AbsDownloadTask absDownloadTask);

    void onTaskSpeedSizeChanged(AbsDownloadTask absDownloadTask);

    void onTaskStatusChanged(AbsDownloadTask absDownloadTask);
}
