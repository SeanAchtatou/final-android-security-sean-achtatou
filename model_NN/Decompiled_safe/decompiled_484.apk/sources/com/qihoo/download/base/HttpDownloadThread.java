package com.qihoo.download.base;

import android.os.Build;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class HttpDownloadThread extends AbsDownloadThread {
    private static final int CONNECT_TIME_OUT = 30000;
    private static final int DEFAULT_RETRY_TIMES = 0;
    private static final int READ_SIZE = 10240;
    private static final int READ_TIME_OUT = 60000;
    private static final String TAG = "HttpDownloadThread";
    private FileOutputStream mFos;
    private HashMap<String, String> mHeader;
    /* access modifiers changed from: private */
    public HttpURLConnection mHttpConn;
    private volatile boolean mIsStop;
    private int mMaxRetryTimes;
    private int mRetryedTime;

    class ErrorType {
        private static final String ENET = "java.net";
        private static final String ENETUNREACH = "ENETUNREACH";
        private static final String ENOENT = "ENOENT";
        private static final String ENOSPC = "ENOSPC";
        private static final String EUNEXPECTED_END_OF_STREAM = "unexpected end of stream";

        ErrorType() {
        }
    }

    public HttpDownloadThread(String str, String str2) {
        this(str, str2, 0);
    }

    public HttpDownloadThread(String str, String str2, int i) {
        super(str, str2);
        this.mHttpConn = null;
        this.mIsStop = false;
        this.mFos = null;
        this.mMaxRetryTimes = 0;
        this.mMaxRetryTimes = i;
        Log.d(TAG, "HttpDownloadThread create: " + this + ", downloadUrl: " + str + ", savePath: " + str2 + ", maxRetryTimes: " + i);
    }

    private int changeError(String str) {
        if ((str.contains("java.net") || str.contains("unexpected end of stream")) && !str.contains("ENETUNREACH")) {
            return 2;
        }
        if (str.contains("ENOSPC")) {
            return 5;
        }
        return str.contains("ENOENT") ? 4 : 6;
    }

    private void initConnection() {
        try {
            Log.e(TAG, "initConnection download url: " + this.mDownloadUrl);
            this.mHttpConn = (HttpURLConnection) new URL(this.mDownloadUrl).openConnection();
            if (this.mHttpConn != null) {
                this.mHttpConn.setRequestMethod("GET");
                this.mHttpConn.setDoInput(true);
                this.mHttpConn.setConnectTimeout(CONNECT_TIME_OUT);
                this.mHttpConn.setReadTimeout(60000);
                HttpURLConnection.setFollowRedirects(true);
                if (this.mHeader != null) {
                    for (Map.Entry next : this.mHeader.entrySet()) {
                        Log.e(TAG, String.valueOf((String) next.getKey()) + ": " + ((String) next.getValue()));
                        this.mHttpConn.addRequestProperty((String) next.getKey(), (String) next.getValue());
                    }
                }
                if (this.mFileOffset > 0) {
                    this.mHttpConn.setRequestProperty("Range", "bytes=" + this.mFileOffset + "-");
                    Log.e(TAG, "setStartDownloadPostion range: " + this.mFileOffset);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void retryAgain() {
        Log.e(TAG, "retryAgain() mIsStop: " + this.mIsStop + ", mRetryedTime: " + this.mRetryedTime);
        if (this.mRetryedTime < this.mMaxRetryTimes) {
            this.mRetryedTime++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (!this.mIsStop) {
                File file = new File(this.mSavePath);
                if (file != null && file.exists()) {
                    setStartDownloadPostion(file.length());
                }
                startDownload();
                return;
            }
            return;
        }
        this.mErrorCode = 6;
        onDownloadError();
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
    /* access modifiers changed from: protected */
    public boolean downloadFile() {
        boolean z = true;
        long j = this.mFileOffset;
        InputStream inputStream = null;
        try {
            inputStream = this.mHttpConn.getInputStream();
            this.mFos = new FileOutputStream(this.mSavePath, true);
            if (!this.mIsStop) {
                byte[] bArr = new byte[READ_SIZE];
                long j2 = j;
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    j2 += (long) read;
                    this.mFos.write(bArr, 0, read);
                    onSizeChanged(j2);
                }
                Log.i(TAG, "thread: downFileSize " + j2 + ", thread: " + this);
                if (this.mFos != null) {
                    this.mFos.flush();
                }
            } else {
                z = false;
            }
            try {
                if (this.mFos != null) {
                    this.mFos.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
                if (this.mHttpConn != null) {
                    this.mHttpConn.disconnect();
                }
            } catch (IOException e) {
                Log.e(TAG, "download thread close output error!");
            }
            return z;
        } catch (Throwable th) {
            try {
                if (this.mFos != null) {
                    this.mFos.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
                if (this.mHttpConn != null) {
                    this.mHttpConn.disconnect();
                }
            } catch (IOException e2) {
                Log.e(TAG, "download thread close output error!");
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void onReceiveResponse(HttpURLConnection httpURLConnection) {
        long j;
        long j2 = 0;
        try {
            Log.e(TAG, "Return header: " + httpURLConnection.getHeaderFields().toString());
            int responseCode = httpURLConnection.getResponseCode();
            Log.e(TAG, "responseCode: " + responseCode + ", mDownloadUrl: " + this.mDownloadUrl);
            if (responseCode == 200 || responseCode == 206) {
                j = (long) httpURLConnection.getContentLength();
                Log.e(TAG, "content type: " + httpURLConnection.getContentType());
                if (responseCode == 200) {
                    File file = new File(this.mSavePath);
                    if (file.exists()) {
                        file.delete();
                        this.mFileOffset = 0;
                    }
                }
            } else {
                j = 0;
            }
            if (j > 0) {
                j2 = this.mFileOffset + j;
            }
            Log.d(TAG, "onLengthReturned mFileOffset: " + this.mFileOffset + ", fileLength: " + j2);
            onLengthReturned(j2, httpURLConnection.getHeaderFields());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(long j) {
        super.onSizeChanged(j);
    }

    public void setDownloadRequestHeader(HashMap<String, String> hashMap) {
        this.mHeader = hashMap;
    }

    public void startDownload() {
        boolean downloadFile;
        Log.d(TAG, "startDownload, mIsStop: " + this.mIsStop);
        initConnection();
        if (this.mHttpConn == null || this.mSavePath == null) {
            this.mErrorCode = 6;
            onDownloadError();
            return;
        }
        try {
            this.mHttpConn.connect();
            int responseCode = this.mHttpConn.getResponseCode();
            if (this.mHttpConn.getHeaderFields() == null) {
                this.mErrorCode = 2;
            } else if (responseCode == 200 || responseCode == 206) {
                String contentType = this.mHttpConn.getContentType();
                if (contentType == null || !contentType.contains("text")) {
                    onReceiveResponse(this.mHttpConn);
                    downloadFile = downloadFile();
                } else {
                    downloadFile = false;
                }
                if (!downloadFile) {
                    this.mErrorCode = 6;
                } else {
                    this.mErrorCode = -1;
                }
            } else if (responseCode == 502) {
                this.mErrorCode = 2;
            } else if (!(responseCode == 301 || responseCode == 302)) {
                this.mErrorCode = 2;
            }
            Log.e(TAG, "startDownload over need retry??? mIsStop: " + this.mIsStop);
            if (this.mIsStop) {
                Log.e(TAG, "user stop downlaodFile, mIsStop: " + this.mIsStop);
            } else if (this.mErrorCode == -1) {
                onDownloadSuccess();
            } else if (this.mErrorCode == 2) {
                retryAgain();
            } else {
                this.mErrorCode = 6;
                onDownloadError();
            }
        } catch (Exception e) {
            Log.e(TAG, "startDownload Exception mIsStop: " + this.mIsStop);
            e.printStackTrace();
            if (!this.mIsStop) {
                this.mErrorCode = changeError(String.valueOf(e.getClass().toString()) + ":" + e.getMessage() + "  ");
            }
            Log.e(TAG, "startDownload over need retry??? mIsStop: " + this.mIsStop);
            if (this.mIsStop) {
                Log.e(TAG, "user stop downlaodFile, mIsStop: " + this.mIsStop);
            } else if (this.mErrorCode == -1) {
                onDownloadSuccess();
            } else if (this.mErrorCode == 2) {
                retryAgain();
            } else {
                this.mErrorCode = 6;
                onDownloadError();
            }
        } catch (Throwable th) {
            Log.e(TAG, "startDownload over need retry??? mIsStop: " + this.mIsStop);
            if (this.mIsStop) {
                Log.e(TAG, "user stop downlaodFile, mIsStop: " + this.mIsStop);
            } else if (this.mErrorCode == -1) {
                onDownloadSuccess();
            } else if (this.mErrorCode == 2) {
                retryAgain();
            } else {
                this.mErrorCode = 6;
                onDownloadError();
            }
            throw th;
        }
    }

    public void stopDownload() {
        this.mIsStop = true;
        Log.e(TAG, "stopDownload mIsStop: " + this.mIsStop + ", thead: " + this + ",mHttpConn: " + this.mHttpConn);
        if (Build.VERSION.SDK_INT > 18) {
            try {
                if (this.mFos != null) {
                    this.mFos.flush();
                    this.mFos.close();
                    this.mFos = null;
                }
            } catch (IOException e) {
                Log.e(TAG, "file os close fail");
                e.printStackTrace();
            }
            new Thread(new Runnable() {
                public void run() {
                    try {
                        if (HttpDownloadThread.this.mHttpConn != null) {
                            HttpDownloadThread.this.mHttpConn.disconnect();
                            HttpDownloadThread.this.mHttpConn = null;
                        }
                    } catch (Exception e) {
                        HttpDownloadThread.this.mHttpConn = null;
                    }
                }
            }).start();
            return;
        }
        try {
            if (this.mHttpConn != null) {
                long currentTimeMillis = System.currentTimeMillis();
                this.mHttpConn.disconnect();
                long currentTimeMillis2 = System.currentTimeMillis();
                this.mHttpConn = null;
                Log.i(TAG, "thread: downloadStop, time use: " + (currentTimeMillis2 - currentTimeMillis) + ", thread: " + this);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
