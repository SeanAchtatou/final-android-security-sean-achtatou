package com.qihoo.download.base;

public class TaskDownloadStatus {
    public static final int STATUS_FOR_CANCELLED = 40;
    public static final int STATUS_FOR_CRAETED = 1;
    public static final int STATUS_FOR_DELETED = 70;
    public static final int STATUS_FOR_DOWNLOADING = 20;
    public static final int STATUS_FOR_FAILED = 50;
    public static final int STATUS_FOR_STOPPED = 30;
    public static final int STATUS_FOR_SUCCESSED = 60;
    public static final int STATUS_FOR_TERMINAL = 80;
    public static final int STATUS_FOR_WAITTING = 10;
}
