package com.qihoo.download.base;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.Random;

public class CalcDownloadSpeed {
    private static final int CLEAN_SPEED_DELAY = 3000;
    private static final int DELAY = 500;
    private final int MSG_CLEAN_SPEED = 1;
    protected Handler mHandler = new Handler(Looper.myLooper()) {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (message.what == 1) {
                CalcDownloadSpeed.this.cleanSpeed();
            }
        }
    };
    private long mLastDownloadSize;
    private long mLastTime;
    private int mSpeed;
    protected int[] mSpeedArray = new int[5];
    private int mSpeedIndex = 0;
    private ISpeedChangedListener mSpeedListener;

    public interface ISpeedChangedListener {
        void onSpeedChaned(int i);
    }

    /* access modifiers changed from: private */
    public void cleanSpeed() {
        this.mSpeedArray = new int[5];
        this.mSpeedIndex = 0;
        this.mSpeed = 0;
        if (this.mSpeedListener != null) {
            this.mSpeedListener.onSpeedChaned(this.mSpeed);
        }
    }

    private int getAverage(int[] iArr) {
        int i = 0;
        for (int i2 : iArr) {
            i += i2;
        }
        return i / iArr.length;
    }

    private void sendCleanSpeedMessage() {
        if (this.mHandler != null) {
            this.mHandler.removeMessages(1);
            this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(1), 3000);
        }
    }

    private void updateSpeed(int i, boolean z) {
        int i2 = this.mSpeedIndex;
        this.mSpeedIndex = i2 + 1;
        int length = i2 % this.mSpeedArray.length;
        if (z && i > 0) {
            i += (i / 4) - new Random().nextInt(i / 2);
        }
        this.mSpeedArray[length] = i;
    }

    public int calculation(long j) {
        if (System.currentTimeMillis() - this.mLastTime > 500) {
            if (this.mLastDownloadSize == 0) {
                this.mLastDownloadSize = j;
            }
            updateSpeed((int) (((j - this.mLastDownloadSize) * 1000) / 500), true);
            this.mSpeed = Math.max(0, getAverage(this.mSpeedArray));
            this.mLastTime = System.currentTimeMillis();
            this.mLastDownloadSize = j;
            sendCleanSpeedMessage();
        }
        return this.mSpeed;
    }

    public void setSpeedListener(ISpeedChangedListener iSpeedChangedListener) {
        this.mSpeedListener = iSpeedChangedListener;
    }
}
