package com.qihoo.download.base;

import android.text.TextUtils;
import android.util.Log;
import com.qihoo.download.base.CalcDownloadSpeed;
import com.qihoo.qplayer.util.FileUtils;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BaseDownloadTask extends AbsDownloadTask implements CalcDownloadSpeed.ISpeedChangedListener {
    private static final String TAG = "BaseDownloadTask";
    private static final String TEMP_FILE_SUFFIX = ".tmp";
    private final int MAX_RETRY_TIMES = 6;
    private CalcDownloadSpeed mCalSpeed = new CalcDownloadSpeed();
    protected AbsDownloadThread mDownloadThread;
    protected int mDownloadUrlCount;
    private List<String> mDownloadUrls;
    protected int mDownloadingIndex;
    protected long mFinishedFileSize;
    protected HashMap<String, String> mHeader;
    protected int mMaxRetryTimes = 6;
    private long mResponseTotalSize;
    private String mTempFilePath;

    public BaseDownloadTask() {
        this.mCalSpeed.setSpeedListener(this);
    }

    private void downloadNextFile() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.mDownloadUrlCount) {
                notifyTaskSizeChanged();
                this.mDownloadThread = null;
                renameFilePath();
                downloadFinish();
                return;
            }
            if (i2 == 0) {
                this.mFinishedFileSize = 0;
            }
            String filePath = getFilePath(i2);
            File file = new File(filePath);
            if (file.exists()) {
                this.mFinishedFileSize += file.length();
                i = i2 + 1;
            } else if (i2 <= this.mDownloadingIndex) {
                this.mDownloadErrorCode = 1;
                this.mDownloadStatus = 50;
                notifyTaskStatusChanged();
                return;
            } else {
                Log.d(TAG, "downloadNextFile i: " + i2 + ", mDownloadUrlCount: " + this.mDownloadUrlCount);
                downloadFile(this.mDownloadUrls.get(i2), String.valueOf(filePath) + TEMP_FILE_SUFFIX);
                this.mDownloadingIndex = i2;
                return;
            }
        }
    }

    private void renameFileTo() {
        if (this.mTempFilePath != null && this.mTempFilePath.endsWith(TEMP_FILE_SUFFIX)) {
            renameFileTo(this.mTempFilePath, this.mTempFilePath.substring(0, this.mTempFilePath.lastIndexOf(TEMP_FILE_SUFFIX)));
        }
    }

    public void addDownloadUrl(List<String> list) {
        this.mDownloadUrls = list;
    }

    /* access modifiers changed from: protected */
    public void calculateTotalSize(long j) {
        Log.d(TAG, "calculateTotalSize before contentLength: " + j + ", mFinishedFileSize: " + this.mFinishedFileSize + ", mTotalSize: " + this.mTotalSize);
        if (this.mDownloadUrlCount != 1 || j <= 0) {
            if (this.mTotalSize <= 0) {
                this.mTotalSize = ((long) this.mDownloadUrlCount) * j;
            }
            long j2 = this.mFinishedFileSize + j;
            if (j2 > this.mTotalSize) {
                this.mTotalSize = j2;
            }
        } else {
            this.mTotalSize = j;
        }
        Log.e(TAG, "calculateTotalSize end contentLength: " + j + ", mFinishedFileSize: " + this.mFinishedFileSize + ", mTotalSize: " + this.mTotalSize);
    }

    /* access modifiers changed from: protected */
    public void calculationSpeed() {
        if (this.mCalSpeed != null) {
            this.mDownloadSpeed = this.mCalSpeed.calculation(this.mDownloadSize);
        }
    }

    /* access modifiers changed from: protected */
    public AbsDownloadThread createDownloadThread() {
        HttpDownloadThread httpDownloadThread = new HttpDownloadThread(this.mDownloadUrl, this.mTempFilePath, this.mMaxRetryTimes);
        httpDownloadThread.setDownloadThreadListener(this);
        File file = new File(this.mTempFilePath);
        if (file != null && file.exists()) {
            long length = file.length();
            Log.e(TAG, "downloadFile: file.length()" + length);
            if (length != 0) {
                httpDownloadThread.setStartDownloadPostion(length);
            }
        }
        if (this.mHeader != null) {
            httpDownloadThread.setDownloadRequestHeader(this.mHeader);
        }
        return httpDownloadThread;
    }

    /* access modifiers changed from: protected */
    public abstract String createSavePath();

    /* access modifiers changed from: protected */
    public void deleteAllFiles() {
        Log.d(TAG, "deleteAllFiles mSavePath: " + this.mSavePath + ", mDownloadUrlCount: " + this.mDownloadUrlCount);
        if (TextUtils.isEmpty(this.mSavePath)) {
            this.mSavePath = createSavePath();
        }
        if (isDownloaded() || this.mDownloadUrlCount == 0) {
            Log.d(TAG, "file downloaded, delete filePath: " + this.mSavePath);
            FileUtils.deleteFolder(this.mSavePath);
            return;
        }
        Log.d(TAG, "file downloading, delete file mDownloadUrlCount " + this.mDownloadUrlCount);
        for (int i = 0; i < this.mDownloadUrlCount; i++) {
            String filePath = getFilePath(i);
            FileUtils.deleteFile(String.valueOf(filePath) + TEMP_FILE_SUFFIX);
            FileUtils.deleteFile(filePath);
        }
    }

    public void deleteDownload() {
        if (this.mDownloadThread != null) {
            this.mDownloadThread.stopDownload();
            this.mDownloadThread = null;
        }
        deleteAllFiles();
        super.deleteDownload();
    }

    /* access modifiers changed from: protected */
    public void downloadFile() {
        this.mSavePath = createSavePath();
        if (TextUtils.isEmpty(this.mSavePath) || this.mDownloadUrls == null || this.mDownloadUrls.size() <= 0) {
            super.onThreadFail(null, 6);
            return;
        }
        if (this.mDownloadThread != null) {
            this.mDownloadThread.stopDownload();
        }
        this.mDownloadingIndex = -1;
        this.mDownloadUrlCount = this.mDownloadUrls.size();
        downloadNextFile();
    }

    /* access modifiers changed from: protected */
    public void downloadFile(String str, String str2) {
        this.mDownloadUrl = str;
        this.mTempFilePath = str2;
        File file = new File(this.mTempFilePath);
        if (!file.exists()) {
            file.getParentFile().mkdirs();
        }
        Log.d(TAG, "downloadFile downloadUrl: " + str + ", savePath: " + str2 + ",mFinishedFileSize : " + this.mFinishedFileSize);
        this.mDownloadThread = createDownloadThread();
        if (this.mDownloadThread != null) {
            this.mDownloadThread.start();
        }
    }

    /* access modifiers changed from: protected */
    public void downloadFinish() {
        this.mDownloadStatus = 60;
        notifyTaskStatusChanged();
    }

    /* access modifiers changed from: protected */
    public abstract void downloadPrepared();

    /* access modifiers changed from: protected */
    public String getFilePath(int i) {
        if (TextUtils.isEmpty(this.mSavePath)) {
            this.mSavePath = createSavePath();
        }
        StringBuilder sb = new StringBuilder(this.mSavePath);
        if (i >= 0) {
            sb.append("_").append(i);
        }
        return sb.toString();
    }

    public int getMaxRetryTimes() {
        return this.mMaxRetryTimes;
    }

    public void onDownloadSizeChanged(AbsDownloadThread absDownloadThread, long j) {
        this.mDownloadSize = this.mFinishedFileSize + j;
        calculationSpeed();
        notifyTaskSizeChanged();
    }

    public void onResponseReturned(AbsDownloadThread absDownloadThread, long j, Map<String, List<String>> map) {
        String[] split;
        this.mResponseTotalSize = 0;
        if (map != null && map.size() > 0) {
            List list = map.get("Content-Range");
            String str = (list == null || list.size() <= 0) ? null : (String) list.get(0);
            if (!(str == null || (split = str.split("/")) == null || split.length <= 1)) {
                long parseLong = Long.parseLong(split[1]);
                if (parseLong > j) {
                    this.mResponseTotalSize = parseLong;
                    Log.e(TAG, "onResponseReturned totalRange: " + this.mResponseTotalSize + ", contentLength: " + j);
                }
            }
        }
        calculateTotalSize(j);
    }

    public void onSpeedChaned(int i) {
        Log.d(TAG, "onSpeedChaned speed: " + i);
        this.mDownloadSpeed = i;
        notifyTaskSpeedChaned();
    }

    public void onThreadFail(AbsDownloadThread absDownloadThread, int i) {
        Log.e(TAG, "onThreadFail downloadThread: " + absDownloadThread + ",errorCode: " + i);
        this.mDownloadErrorCode = i;
        if (this.mDownloadErrorCode == 2) {
            this.mDownloadStatus = 80;
        } else {
            this.mDownloadStatus = 50;
        }
        notifyTaskStatusChanged();
    }

    public void onThreadSucess(AbsDownloadThread absDownloadThread) {
        File file = new File(this.mTempFilePath);
        if (file == null || this.mResponseTotalSize <= file.length()) {
            renameFileTo();
            downloadNextFile();
            return;
        }
        Log.e(TAG, "onThreadSucess mResponseTotalSize " + this.mResponseTotalSize + ", file.length(): " + file.length());
        downloadFile(this.mDownloadUrls.get(this.mDownloadingIndex), this.mTempFilePath);
    }

    /* access modifiers changed from: protected */
    public void renameFilePath() {
        if (this.mDownloadUrlCount == 1) {
            renameFileTo(getFilePath(0), this.mSavePath);
        }
    }

    /* access modifiers changed from: protected */
    public boolean renameFileTo(String str, String str2) {
        boolean z = false;
        File file = new File(str);
        if (file.exists()) {
            z = file.renameTo(new File(str2));
        }
        Log.d(TAG, "renameFileTo src: " + str + ", dst: " + str2 + ", result: " + z);
        return z;
    }

    public void setMaxRetryTimes(int i) {
        this.mMaxRetryTimes = i;
    }

    public void startDownload() {
        super.startDownload();
        downloadPrepared();
    }

    public void stopDownload() {
        Log.d(TAG, "stopDownload() mDownloadThread: " + this.mDownloadThread);
        if (this.mDownloadThread != null) {
            this.mDownloadThread.stopDownload();
            this.mDownloadThread = null;
        }
        this.mDownloadSpeed = 0;
        super.stopDownload();
    }
}
