package com.qihoo.download.impl.so;

import android.content.Context;
import android.util.Log;
import com.qihoo.download.base.AbsDownloadTask;
import com.qihoo.download.base.BaseDownloadTaskManager;
import com.qihoo.download.base.IDownloadTaskListener;
import com.qihoo.qplayer.LoadLibraryUtils;
import com.qihoo.qplayer.util.FileUtils;
import com.qihoo.qplayer.util.MainThreadHandlerUtil;
import com.qihoo.qplayer.util.ZipAntUtils;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

public class SoDownloadManager extends BaseDownloadTaskManager {
    private static final String TAG = "SoDownloadManager";
    private static volatile SoDownloadManager mInstance;
    /* access modifiers changed from: private */
    public IDowloadSoListener mListener;
    private int mRetryTimes = -1;
    /* access modifiers changed from: private */
    public SoDownloadTask mTask;

    public interface IDowloadSoListener {
        void onDownloadFailed(AbsDownloadTask absDownloadTask);

        void onDownloadSizeChanged(AbsDownloadTask absDownloadTask);

        void onDownloadSucess(AbsDownloadTask absDownloadTask);
    }

    public SoDownloadManager(Context context) {
        super(context);
    }

    public static SoDownloadManager getInstance() {
        if (mInstance == null) {
            synchronized (SoDownloadManager.class) {
                if (mInstance == null) {
                    mInstance = new SoDownloadManager(null);
                }
            }
        }
        return mInstance;
    }

    public void cancel() {
        this.mListener = null;
    }

    public AbsDownloadTask createTask() {
        return null;
    }

    public AbsDownloadTask createTask(Object obj) {
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo.qplayer.util.FileUtils.deleteFolderFiles(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.qihoo.qplayer.util.FileUtils.deleteFolderFiles(java.lang.String, int):boolean
      com.qihoo.qplayer.util.FileUtils.deleteFolderFiles(java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean deleteOldVersion(Context context) {
        String[] list = new File(context.getCacheDir().getParent()).list(new FilenameFilter() {
            public boolean accept(File file, String str) {
                return str.startsWith(LoadLibraryUtils.LIB_DIRETORY_PREFFIX);
            }
        });
        int length = list.length;
        int i = 0;
        while (i < length) {
            try {
                FileUtils.getInstance().deleteFolderFiles(list[i], true);
                i++;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    public void downloadSo(final Context context) {
        if (this.mTask == null) {
            deleteOldVersion(context);
            this.mTask = new SoDownloadTask(context);
            if (this.mRetryTimes >= 0) {
                this.mTask.setMaxRetryTimes(this.mRetryTimes);
            }
            this.mTask.setTaskListener(new IDownloadTaskListener() {
                public void onTaskDownloadSizeChanged(final AbsDownloadTask absDownloadTask) {
                    MainThreadHandlerUtil.getMainThreadHandler().post(new Runnable() {
                        public void run() {
                            if (SoDownloadManager.this.mListener != null) {
                                SoDownloadManager.this.mListener.onDownloadSizeChanged(absDownloadTask);
                            }
                        }
                    });
                }

                public void onTaskSpeedSizeChanged(AbsDownloadTask absDownloadTask) {
                }

                public void onTaskStatusChanged(final AbsDownloadTask absDownloadTask) {
                    switch (absDownloadTask.getDownloadStatus()) {
                        case 50:
                            MainThreadHandlerUtil.getMainThreadHandler().post(new Runnable() {
                                public void run() {
                                    Log.e(SoDownloadManager.TAG, "downloadfailed");
                                    if (SoDownloadManager.this.mListener != null) {
                                        SoDownloadManager.this.mListener.onDownloadFailed(absDownloadTask);
                                    }
                                    SoDownloadManager.this.mTask = null;
                                }
                            });
                            return;
                        case 60:
                            ZipAntUtils.unZipFile(absDownloadTask.getSavePath(), LoadLibraryUtils.getVideoLibPath(context));
                            FileUtils.deleteFile(absDownloadTask.getSavePath());
                            MainThreadHandlerUtil.getMainThreadHandler().post(new Runnable() {
                                public void run() {
                                    if (SoDownloadManager.this.mListener != null) {
                                        SoDownloadManager.this.mListener.onDownloadSucess(absDownloadTask);
                                    }
                                    SoDownloadManager.this.mTask = null;
                                }
                            });
                            return;
                        default:
                            return;
                    }
                }
            });
            this.mTask.startDownload();
        }
    }

    public boolean needDownloadSo(Context context) {
        return !LoadLibraryUtils.isLibraryExist(context);
    }

    public void setListener(IDowloadSoListener iDowloadSoListener) {
        this.mListener = iDowloadSoListener;
    }

    public void setRetryTimes(int i) {
        this.mRetryTimes = i;
    }
}
