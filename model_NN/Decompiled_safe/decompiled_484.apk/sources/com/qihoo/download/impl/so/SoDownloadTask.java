package com.qihoo.download.impl.so;

import android.content.Context;
import com.qihoo.download.base.BaseDownloadTask;
import com.qihoo.qplayer.CpuUtils;
import com.qihoo.qplayer.QHPlayerSDK;
import com.qihoo.video.httpservice.AsyncRequest;
import com.qihoo.video.httpservice.PlayerDownloadRequest;
import java.util.ArrayList;

class SoDownloadTask extends BaseDownloadTask implements AsyncRequest.OnRecivedDataListener {
    private String savePath;
    private String type;
    private String ver;

    public SoDownloadTask(Context context) {
        String str;
        CpuUtils.CpuInfo cpuInfo = CpuUtils.getCpuInfo();
        if (!cpuInfo.hasArmV7) {
            str = "armeabi.zip";
            this.type = "arm-v6";
        } else if (cpuInfo.hasNeon) {
            str = "armeabi-v7a-neon.zip";
            this.type = "arm-v7-neon";
        } else {
            str = "armeabi-v7a.zip";
            this.type = "arm-v7";
        }
        this.savePath = String.valueOf(context.getFilesDir().getPath()) + "/" + str;
        this.ver = QHPlayerSDK.SO_VERSION;
    }

    public void OnRecivedData(AsyncRequest asyncRequest, Object obj) {
        if (obj == null || !(obj instanceof String)) {
            this.mDownloadStatus = 50;
            notifyTaskStatusChanged();
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add((String) obj);
        addDownloadUrl(arrayList);
        downloadFile();
    }

    /* access modifiers changed from: protected */
    public String createSavePath() {
        return this.savePath;
    }

    /* access modifiers changed from: protected */
    public void downloadPrepared() {
        PlayerDownloadRequest.PlayerDownloadParam playerDownloadParam = new PlayerDownloadRequest.PlayerDownloadParam();
        playerDownloadParam.so_ver = this.ver;
        playerDownloadParam.so_type = this.type;
        PlayerDownloadRequest playerDownloadRequest = new PlayerDownloadRequest();
        playerDownloadRequest.setOnRecivedDataListener(this);
        playerDownloadRequest.executeOnPoolExecutor(playerDownloadParam);
    }
}
