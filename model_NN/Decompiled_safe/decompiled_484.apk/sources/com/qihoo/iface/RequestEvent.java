package com.qihoo.iface;

public interface RequestEvent {
    void onErrorResponse(String str);

    void onResponse(String str);
}
