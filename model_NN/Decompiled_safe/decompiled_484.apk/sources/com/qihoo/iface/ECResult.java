package com.qihoo.iface;

import android.text.TextUtils;

public class ECResult {
    public int emulatorIdx;
    public int isEmulator;
    public String matchList;

    public ECResult() {
        this.isEmulator = 1;
        this.emulatorIdx = 0;
        this.matchList = "";
    }

    public ECResult(String str) {
        this.isEmulator = 1;
        this.emulatorIdx = 0;
        this.matchList = "";
        if (!TextUtils.isEmpty(str)) {
            String[] split = str.split(";");
            if (split.length > 0) {
                try {
                    this.isEmulator = Integer.parseInt(split[0]);
                } catch (Exception e) {
                }
            }
            try {
                if (split.length > 1) {
                    try {
                        this.emulatorIdx = Integer.parseInt(split[1]);
                    } catch (Exception e2) {
                    }
                }
                if (split.length > 2) {
                    this.matchList = split[2];
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    public void Copy(ECResult eCResult) {
        this.isEmulator = eCResult.isEmulator;
        this.emulatorIdx = eCResult.emulatorIdx;
        this.matchList = eCResult.matchList;
    }

    public String toString() {
        return String.valueOf(this.isEmulator) + ";" + this.emulatorIdx + ";" + this.matchList;
    }
}
