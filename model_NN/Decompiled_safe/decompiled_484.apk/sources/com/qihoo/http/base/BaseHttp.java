package com.qihoo.http.base;

import android.net.Uri;
import android.text.TextUtils;
import com.qihoo.dynamic.util.Md5Util;
import com.qihoo.qplayer.util.MyLog;
import com.qihoo.video.GzipUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

public class BaseHttp {
    protected Uri.Builder mBuilder;
    private List<NameValuePair> mPostParamList;

    public void appendGetParameter(String str, String str2) {
        if (!TextUtils.isEmpty(str) && this.mBuilder.build().getQueryParameter(str) == null) {
            this.mBuilder.appendQueryParameter(str, str2);
        }
    }

    public void appendGetParameter(Map<String, String> map) {
        if (map != null) {
            for (Map.Entry next : map.entrySet()) {
                if (this.mBuilder.build().getQueryParameter((String) next.getKey()) == null) {
                    this.mBuilder.appendQueryParameter((String) next.getKey(), (String) next.getValue());
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void appendPostParameter(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            if (this.mPostParamList == null) {
                this.mPostParamList = new ArrayList();
            }
            this.mPostParamList.add(new BasicNameValuePair(str, str2));
        }
    }

    public void build(String str, String str2, String str3) {
        this.mBuilder.scheme(str);
        this.mBuilder.authority(str2);
        this.mBuilder.path(str3);
    }

    public void buildFromUrl(String str) {
        this.mBuilder = Uri.parse(str).buildUpon();
    }

    /* access modifiers changed from: package-private */
    public Uri buildUri() {
        return this.mBuilder.build();
    }

    /* access modifiers changed from: protected */
    public String executeGetResult(String str) {
        MyLog.e("HttpRequest", "requestGet url: " + str);
        try {
            HttpGet httpGet = new HttpGet(str);
            httpGet.addHeader("Accept-Encoding", "gzip");
            return GzipUtils.getJsonStringFromGZIP(HttpUtils.getThreadSafeClient().execute(httpGet));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public String executePostResult(String str) {
        MyLog.e("HttpRequest", "requestPost url: " + str);
        try {
            HttpPost httpPost = new HttpPost(str);
            httpPost.addHeader("Accept-Encoding", "gzip");
            httpPost.setEntity(new UrlEncodedFormEntity(this.mPostParamList, Md5Util.DEFAULT_CHARSET));
            return GzipUtils.getJsonStringFromGZIP(HttpUtils.getThreadSafeClient().execute(httpPost));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
