package com.qihoo.http.base;

import com.qihoo.qplayer.util.MyLog;
import java.io.BufferedInputStream;
import java.io.FilterInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;
import org.apache.http.HttpResponse;

class GzipUtils {
    GzipUtils() {
    }

    public static String getJsonStringFromGZIP(HttpResponse httpResponse) {
        Exception e;
        String str;
        try {
            InputStream content = httpResponse.getEntity().getContent();
            str = getJsonStringFromGZIPInputStream(content);
            try {
                content.close();
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return str;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            str = null;
            e = exc;
        }
        return str;
    }

    public static String getJsonStringFromGZIPInputStream(InputStream inputStream) {
        Exception e;
        String str;
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            bufferedInputStream.mark(2);
            byte[] bArr = new byte[2];
            int read = bufferedInputStream.read(bArr);
            bufferedInputStream.reset();
            FilterInputStream gZIPInputStream = (read == -1 || getShort(bArr) != 8075) ? bufferedInputStream : new GZIPInputStream(bufferedInputStream);
            InputStreamReader inputStreamReader = new InputStreamReader(gZIPInputStream, "utf-8");
            char[] cArr = new char[100];
            StringBuffer stringBuffer = new StringBuffer();
            while (true) {
                int read2 = inputStreamReader.read(cArr);
                if (read2 <= 0) {
                    break;
                }
                stringBuffer.append(cArr, 0, read2);
            }
            str = stringBuffer.toString();
            try {
                bufferedInputStream.close();
                inputStreamReader.close();
                gZIPInputStream.close();
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                MyLog.d("HttpTask", "getJsonStringFromGZIP net output : " + str);
                return str;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            str = null;
            e = exc;
        }
        MyLog.d("HttpTask", "getJsonStringFromGZIP net output : " + str);
        return str;
    }

    private static int getShort(byte[] bArr) {
        return (bArr[0] << 8) | (bArr[1] & 255);
    }
}
