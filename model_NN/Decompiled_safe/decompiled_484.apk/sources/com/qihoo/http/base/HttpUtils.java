package com.qihoo.http.base;

import android.net.http.AndroidHttpClient;
import android.os.Build;
import android.util.Log;
import com.qihoo.dynamic.util.Md5Util;
import com.qihoo.qplayer.QHPlayerSDK;
import com.qihoo.qplayer.util.AESEncryptUtil;
import com.qihoo.video.URLEncodeing;
import java.net.URLEncoder;
import java.security.MessageDigest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

public class HttpUtils {
    private static final int httpRequestTimeOut = 50000;
    private static AndroidHttpClient mHttpClient = null;
    private static final String suffix = " Android/OSVer QIHU";

    public static String MD5Encode(String str) {
        return MD5Encode(str.getBytes());
    }

    public static String MD5Encode(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            MessageDigest instance = MessageDigest.getInstance(Md5Util.ALGORITHM);
            instance.update(bArr);
            byte[] digest = instance.digest();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= digest.length) {
                    break;
                }
                String hexString = Integer.toHexString(digest[i2] & 255);
                if (hexString.length() < 2) {
                    hexString = "0" + hexString;
                }
                stringBuffer.append(hexString);
                i = i2 + 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringBuffer.toString();
    }

    public static String URLEncode(String str) {
        try {
            return URLEncoder.encode(str, Md5Util.DEFAULT_CHARSET).replaceAll("\\+", "%20");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static DefaultHttpClient getThreadSafeClient() {
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        ClientConnectionManager connectionManager = defaultHttpClient.getConnectionManager();
        defaultHttpClient.getParams().setParameter("http.connection.timeout", Integer.valueOf((int) httpRequestTimeOut));
        defaultHttpClient.getParams().setParameter("http.socket.timeout", Integer.valueOf((int) httpRequestTimeOut));
        String str = Build.VERSION.RELEASE;
        if (str != null) {
            str = URLEncodeing.uRLEncode(str);
        }
        String replace = (String.valueOf(AESEncryptUtil.decrypt(QHPlayerSDK.getInstance().getPrivateKey(), QHPlayerSDK.getInstance().getSignature())) + "/" + QHPlayerSDK.getInstance().getVersionName() + suffix).replace("OSVer", str);
        Log.e("Http Request", "Http Request userAgent: " + replace);
        HttpProtocolParams.setUserAgent(defaultHttpClient.getParams(), replace);
        HttpParams params = defaultHttpClient.getParams();
        return new DefaultHttpClient(new ThreadSafeClientConnManager(params, connectionManager.getSchemeRegistry()), params);
    }
}
