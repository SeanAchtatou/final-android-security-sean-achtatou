package com.qihoo360.daily.b;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.qihoo360.daily.model.Info;
import java.lang.reflect.Field;

public class b extends SQLiteOpenHelper {
    public b(Context context) {
        super(context, "discovery.db", (SQLiteDatabase.CursorFactory) null, 11);
    }

    private void a(SQLiteDatabase sQLiteDatabase, Class cls) {
        String simpleName = cls.getSimpleName();
        Field[] declaredFields = cls.getDeclaredFields();
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE ");
        sb.append(simpleName);
        sb.append(" (");
        sb.append(" _id integer primary key autoincrement ");
        if (declaredFields != null) {
            int length = declaredFields.length;
            for (int i = 0; i < length; i++) {
                sb.append(" , '" + declaredFields[i].getName() + "'" + " text ");
            }
        }
        sb.append(")");
        sQLiteDatabase.execSQL(sb.toString());
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        a(sQLiteDatabase, Info.class);
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        switch (i) {
            case 1:
                sQLiteDatabase.execSQL("Alter table 'Info' add 'ext_item' text");
            case 2:
                sQLiteDatabase.execSQL("Alter table 'Info' add 'timeRange' text");
            case 3:
                sQLiteDatabase.execSQL("Alter table 'Info' add 'slogan' text");
                sQLiteDatabase.execSQL("Alter table 'Info' add 'subhead' text");
                sQLiteDatabase.execSQL("Alter table 'Info' add 'newtags' text");
                sQLiteDatabase.execSQL("Alter table 'Info' add 'listorder' text");
                sQLiteDatabase.execSQL("Alter table 'Info' add 'wiki' text");
                sQLiteDatabase.execSQL("Alter table 'Info' add 'song' text");
                sQLiteDatabase.execSQL("Alter table 'Info' add 'video' text");
                sQLiteDatabase.execSQL("Alter table 'Info' add 'photo' text");
                sQLiteDatabase.execSQL("Alter table 'Info' add 'cmt_hot' text");
            case 4:
                sQLiteDatabase.execSQL("Alter table 'Info' add 'newtrans' text");
            case 5:
                sQLiteDatabase.execSQL("Alter table 'Info' add 'page_template' text");
            case 6:
                sQLiteDatabase.execSQL("Alter table 'Info' add 'view' text");
            case 7:
                sQLiteDatabase.execSQL("Alter table 'Info' add 'in_gallery' text");
                sQLiteDatabase.execSQL("Alter table 'Info' add 'in_weibo' text");
                sQLiteDatabase.execSQL("Alter table 'Info' add 'in_wiki' text");
                sQLiteDatabase.execSQL("Alter table 'Info' add 'in_video' text");
                sQLiteDatabase.execSQL("Alter table 'Info' add 'in_music' text");
                sQLiteDatabase.execSQL("Alter table 'Info' add 'in_newsfeature' text");
                sQLiteDatabase.execSQL("Alter table 'Info' add 'in_furtherreading' text");
            case 8:
                sQLiteDatabase.execSQL("Alter table 'Info' add 'media_id' text");
            case 9:
                sQLiteDatabase.execSQL("Alter table 'Info' add 'subtitle' text");
                sQLiteDatabase.execSQL("Alter table 'Info' add 'sonum' text");
                break;
            case 10:
                break;
            default:
                return;
        }
        sQLiteDatabase.execSQL("Alter table 'Info' add 'editor_id' text");
        sQLiteDatabase.execSQL("Alter table 'Info' add 'editor_name' text");
        sQLiteDatabase.execSQL("Alter table 'Info' add 'editor_pic' text");
        sQLiteDatabase.execSQL("Alter table 'Info' add 'editor_desc' text");
        sQLiteDatabase.execSQL("Alter table 'Info' add 'hotlabel' text");
        sQLiteDatabase.execSQL("Alter table 'Info' add 'conhotwords' text");
        sQLiteDatabase.execSQL("Alter table 'Info' add 'isSubNews' text");
        sQLiteDatabase.execSQL("Alter table 'Info' add 'isCardTail' text");
        sQLiteDatabase.execSQL("Alter table 'Info' add 'hot_search' text");
        sQLiteDatabase.execSQL("Alter table 'Info' add 'newstag' text");
        sQLiteDatabase.execSQL("Alter table 'Info' add 'conhotwordstime' text");
        sQLiteDatabase.execSQL("Alter table 'Info' add 'extnews' text");
        sQLiteDatabase.execSQL("Alter table 'Info' add 'android_channeltarget' text");
        sQLiteDatabase.execSQL("Alter table 'Info' add 'android_channeltargetname' text");
    }
}
