package com.qihoo360.daily.b;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.text.TextUtils;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.SendCmtActivity;
import com.qihoo360.daily.h.q;
import com.qihoo360.daily.model.City;
import com.qihoo360.daily.wxapi.WXConfig;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class a extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private Context f931a;

    /* renamed from: b  reason: collision with root package name */
    private String f932b = (Environment.getDataDirectory().getPath() + File.separator + SendCmtActivity.TAG_DATA + File.separator + this.f931a.getPackageName() + File.separator + "databases" + File.separator);
    private SQLiteDatabase c;

    public a(Context context) {
        super(context, "cities.db", (SQLiteDatabase.CursorFactory) null, 1);
        this.f931a = context;
        try {
            SQLiteDatabase c2 = c();
            if (c2 != null) {
                c2.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private SQLiteDatabase c() {
        String str = this.f932b + "cities.db";
        File file = new File(str);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        if (file.exists()) {
            return null;
        }
        try {
            q.a(this.f931a.getResources().openRawResource(R.raw.cities), new FileOutputStream(file));
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        SQLiteDatabase openOrCreateDatabase = SQLiteDatabase.openOrCreateDatabase(str, (SQLiteDatabase.CursorFactory) null);
        openOrCreateDatabase.setVersion(1);
        return openOrCreateDatabase;
    }

    public City a(City city) {
        city.setCode(c(city.getCity()));
        city.setCitySpell(b(city.getCity()));
        city.setProvinceSpell(b(city.getProvince()));
        return city;
    }

    public List<City> a(String str) {
        ArrayList arrayList;
        Exception e;
        String str2 = "%" + str + "%";
        String str3 = str + "%";
        String str4 = "%" + str + "%";
        if (this.c == null || !this.c.isOpen()) {
            a();
        }
        if (this.c == null || !this.c.isOpen()) {
            return null;
        }
        try {
            Cursor rawQuery = this.c.rawQuery("select code,name,province,pinyin from city where name like ? or province like ? or shortpinyin like ? or fullpinyin like ? order by case when name like ? then 1 when name like ? then 2 when province like ?  then 3 when province like ?  then 4 when shortpinyin like ? then 5 when shortpinyin like ? then 6 when fullpinyin like ? then 7 when fullpinyin like ? then 8 else 9 end", new String[]{str2, str2, str2, str2, str3, str4, str3, str4, str3, str4, str3, str4});
            arrayList = new ArrayList();
            while (rawQuery.moveToNext()) {
                try {
                    City city = new City();
                    city.setCode(rawQuery.getString(0));
                    city.setCity(rawQuery.getString(1));
                    city.setProvince(rawQuery.getString(2));
                    city.setCitySpell(rawQuery.getString(3));
                    arrayList.add(city);
                } catch (Exception e2) {
                    e = e2;
                }
            }
            rawQuery.close();
            this.c.close();
            return arrayList;
        } catch (Exception e3) {
            Exception exc = e3;
            arrayList = null;
            e = exc;
            e.printStackTrace();
            return arrayList;
        }
    }

    public void a() {
        try {
            this.c = SQLiteDatabase.openDatabase(this.f932b + "cities.db", null, 16);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String b(String str) {
        String str2 = null;
        if (!TextUtils.isEmpty(str)) {
            if (this.c == null || !this.c.isOpen()) {
                a();
            }
            if (this.c != null && this.c.isOpen()) {
                try {
                    Cursor rawQuery = this.c.rawQuery("select code from cityspell where name like ?", new String[]{str + "%"});
                    if (rawQuery != null && rawQuery.moveToNext()) {
                        str2 = rawQuery.getString(rawQuery.getColumnIndex(WXConfig.WX_CODE));
                    }
                    if (rawQuery != null) {
                        rawQuery.close();
                    }
                    this.c.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return str2;
    }

    public List<City> b() {
        ArrayList arrayList = new ArrayList();
        if (this.c == null || !this.c.isOpen()) {
            a();
        }
        if (this.c == null || !this.c.isOpen()) {
            return null;
        }
        try {
            Cursor rawQuery = this.c.rawQuery("select code,name,province,pinyin from city order by pinyin", null);
            while (rawQuery != null && rawQuery.moveToNext()) {
                City city = new City();
                city.setCode(rawQuery.getString(0));
                city.setCity(rawQuery.getString(1));
                city.setProvince(rawQuery.getString(2));
                city.setCitySpell(rawQuery.getString(3));
                arrayList.add(city);
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            this.c.close();
            return arrayList;
        } catch (Exception e) {
            e.printStackTrace();
            return arrayList;
        }
    }

    public String c(String str) {
        String str2 = null;
        if (!TextUtils.isEmpty(str)) {
            if (this.c == null || !this.c.isOpen()) {
                a();
            }
            if (this.c != null && this.c.isOpen()) {
                try {
                    Cursor rawQuery = this.c.rawQuery("select code from city where name like ?", new String[]{str + "%"});
                    if (rawQuery != null && rawQuery.moveToNext()) {
                        str2 = rawQuery.getString(rawQuery.getColumnIndex(WXConfig.WX_CODE));
                    }
                    if (rawQuery != null) {
                        rawQuery.close();
                    }
                    this.c.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return str2;
    }

    public synchronized void close() {
        try {
            if (this.c != null && this.c.isOpen()) {
                this.c.close();
            }
            super.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }
}
