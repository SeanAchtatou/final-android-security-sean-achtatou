package com.qihoo360.daily.b;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.h.ae;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class d<T> {

    /* renamed from: b  reason: collision with root package name */
    private static final Pattern f935b = Pattern.compile("\\s*\\d+\\s*(,\\s*\\d+\\s*)?");

    /* renamed from: a  reason: collision with root package name */
    private Context f936a;

    public d(Context context) {
        this.f936a = context;
    }

    private String a(boolean z, String str, String[] strArr, String str2, String str3, String str4, String str5, boolean z2, String str6) {
        if (TextUtils.isEmpty(str3) && !TextUtils.isEmpty(str4)) {
            throw new IllegalArgumentException("HAVING clauses are only permitted when using a groupBy clause");
        } else if (TextUtils.isEmpty(str6) || f935b.matcher(str6).matches()) {
            StringBuilder sb = new StringBuilder(120);
            sb.append("SELECT ");
            if (z) {
                sb.append("DISTINCT ");
            }
            if (strArr == null || strArr.length == 0) {
                sb.append("* ");
            } else {
                a(sb, strArr);
            }
            sb.append("FROM ");
            sb.append(str);
            a(sb, " WHERE ", str2);
            if (!TextUtils.isEmpty(str3)) {
                a(sb, " GROUP BY ", str3);
            }
            if (!TextUtils.isEmpty(str4)) {
                a(sb, " HAVING ", str4);
            }
            if (!TextUtils.isEmpty(str5)) {
                a(sb, " ORDER BY ", str5);
            }
            if (z2) {
                sb.append(" desc ");
            }
            a(sb, " LIMIT ", str6);
            return sb.toString();
        } else {
            throw new IllegalArgumentException("invalid LIMIT clauses:" + str6);
        }
    }

    private void a(Object obj, ContentValues contentValues, Field field) {
        field.setAccessible(true);
        String valueOf = String.valueOf(field.get(obj));
        Class<?> type = field.getType();
        String name = field.getName();
        if (((ae) field.getAnnotation(ae.class)) == null) {
            if (String.class.equals(type)) {
                contentValues.put(name, "null".equals(valueOf) ? "" : valueOf);
            } else if (Integer.class.equals(type)) {
                contentValues.put(name, valueOf);
            } else if (Integer.TYPE.equals(type)) {
                contentValues.put(name, valueOf);
            } else if (Long.class.equals(type)) {
                contentValues.put(name, valueOf);
            } else if (Long.TYPE.equals(type)) {
                contentValues.put(name, valueOf);
            } else if (Double.class.equals(type)) {
                contentValues.put(name, valueOf);
            } else if (Double.TYPE.equals(type)) {
                contentValues.put(name, valueOf);
            } else if (Float.class.equals(type)) {
                contentValues.put(name, valueOf);
            } else if (Float.TYPE.equals(type)) {
                contentValues.put(name, valueOf);
            } else if (Short.class.equals(type)) {
                contentValues.put(name, valueOf);
            } else if (Short.TYPE.equals(type)) {
                contentValues.put(name, valueOf);
            } else if (Boolean.class.equals(type)) {
                contentValues.put(name, valueOf);
            } else if (Boolean.TYPE.equals(type)) {
                contentValues.put(name, valueOf);
            } else {
                if (field.getName().equals("extnews")) {
                    List list = (List) field.get(obj);
                    contentValues.put(name, list == null ? "" : Application.getGson().a(list));
                }
                if (field.getName().equals("hot_search")) {
                    List list2 = (List) field.get(obj);
                    contentValues.put(name, list2 == null ? "" : Application.getGson().a(list2));
                }
            }
        }
    }

    private void a(T t, SQLiteDatabase sQLiteDatabase) {
        try {
            Field[] declaredFields = t.getClass().getDeclaredFields();
            if (sQLiteDatabase != null && sQLiteDatabase.isOpen() && declaredFields != null) {
                ContentValues contentValues = new ContentValues();
                for (Field a2 : declaredFields) {
                    try {
                        a(t, contentValues, a2);
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e2) {
                        e2.printStackTrace();
                    }
                }
                sQLiteDatabase.insert(t.getClass().getSimpleName(), null, contentValues);
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    private void a(Object obj, Field field, String str) {
        if (str != null) {
            field.setAccessible(true);
            Class<?> type = field.getType();
            if (String.class.equals(type)) {
                field.set(obj, str);
            } else if (Integer.class.equals(type)) {
                field.set(obj, Integer.valueOf(str));
            } else if (Integer.TYPE.equals(type)) {
                field.set(obj, Integer.valueOf(Integer.parseInt(str)));
            } else if (Long.class.equals(type)) {
                field.set(obj, Long.valueOf(str));
            } else if (Long.TYPE.equals(type)) {
                field.set(obj, Long.valueOf(Long.parseLong(str)));
            } else if (Float.class.equals(type)) {
                field.set(obj, Float.valueOf(str));
            } else if (Float.TYPE.equals(type)) {
                field.set(obj, Float.valueOf(Float.parseFloat(str)));
            } else if (Double.class.equals(type)) {
                field.set(obj, Double.valueOf(str));
            } else if (Double.TYPE.equals(type)) {
                field.set(obj, Double.valueOf(Double.parseDouble(str)));
            } else if (List.class.equals(type) && field.getName().equals("extnews")) {
                field.set(obj, (List) Application.getGson().a(str, new e(this).getType()));
            }
        }
    }

    private void a(StringBuilder sb, String str, String str2) {
        if (!TextUtils.isEmpty(str2)) {
            sb.append(str);
            sb.append(str2);
        }
    }

    private void a(StringBuilder sb, String[] strArr) {
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            String str = strArr[i];
            if (str != null) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append(str);
            }
        }
        sb.append(' ');
    }

    public List<T> a(Class<T> cls, String str, String str2, int i, int i2, String str3) {
        return a(cls, new String[]{str}, new String[]{str2}, i, i2, str3);
    }

    public List<T> a(Class<T> cls, String str, String str2, int i, int i2, String str3, long j, long j2) {
        StringBuilder sb = new StringBuilder();
        if (!(str == null || str2 == null)) {
            sb.append(str + " = ? ");
            if (j > 0) {
                sb.append(" and " + str3 + "<" + j);
            }
            if (j2 > 0) {
                sb.append(" and " + str3 + ">" + j2);
            }
        }
        return a(cls, sb.toString(), new String[]{str2}, i, i2, str3);
    }

    public List<T> a(Class<T> cls, String str, String[] strArr, int i, int i2, String str2) {
        T t;
        ArrayList arrayList = new ArrayList();
        try {
            c a2 = c.a(this.f936a);
            SQLiteDatabase a3 = a2.a();
            if (a3 != null && a3.isOpen()) {
                Cursor rawQuery = a3.rawQuery(a(true, cls.getSimpleName(), null, str, null, null, str2, true, i + "," + i2), strArr);
                Field[] declaredFields = cls.getDeclaredFields();
                if (!(rawQuery == null || declaredFields == null)) {
                    while (rawQuery.moveToNext()) {
                        try {
                            t = cls.newInstance();
                        } catch (InstantiationException e) {
                            e.printStackTrace();
                            t = null;
                        } catch (IllegalAccessException e2) {
                            e2.printStackTrace();
                            t = null;
                        }
                        if (t != null) {
                            for (Field field : declaredFields) {
                                int columnIndex = rawQuery.getColumnIndex(field.getName());
                                if (columnIndex != -1) {
                                    try {
                                        a(t, field, rawQuery.getString(columnIndex));
                                    } catch (Exception e3) {
                                        e3.printStackTrace();
                                    }
                                }
                            }
                            arrayList.add(t);
                        }
                    }
                }
                if (rawQuery != null && !rawQuery.isClosed()) {
                    rawQuery.close();
                }
            }
            a2.b();
        } catch (Exception e4) {
            e4.printStackTrace();
        }
        return arrayList;
    }

    public List<T> a(Class<T> cls, String[] strArr, String[] strArr2, int i, int i2, String str) {
        StringBuilder sb = new StringBuilder();
        if (strArr != null) {
            int length = strArr.length;
            for (int i3 = 0; i3 < length; i3++) {
                String str2 = strArr[i3];
                if (i3 != 0) {
                    sb.append(" and ");
                }
                sb.append(str2 + " = ? ");
            }
        }
        return a(cls, sb.toString(), strArr2, i, i2, str);
    }

    public void a(Class cls) {
        try {
            c a2 = c.a(this.f936a);
            SQLiteDatabase a3 = a2.a();
            if (a3 != null && a3.isOpen()) {
                a3.execSQL("delete from " + cls.getSimpleName() + " where pdate <= datetime('now', '-604800 seconds', 'localtime')");
            }
            a2.b();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void a(T t, String[] strArr, String[] strArr2) {
        if (t != null) {
            try {
                c a2 = c.a(this.f936a);
                SQLiteDatabase a3 = a2.a();
                if (a3 != null && a3.isOpen()) {
                    Field[] declaredFields = t.getClass().getDeclaredFields();
                    ContentValues contentValues = new ContentValues();
                    for (Field a4 : declaredFields) {
                        try {
                            a(t, contentValues, a4);
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e2) {
                            e2.printStackTrace();
                        }
                    }
                    StringBuilder sb = new StringBuilder();
                    if (strArr != null) {
                        int length = strArr.length;
                        for (int i = 0; i < length; i++) {
                            String str = strArr[i];
                            if (i != 0) {
                                sb.append(" and ");
                            }
                            sb.append(str + " = ? ");
                        }
                    }
                    a3.update(t.getClass().getSimpleName(), contentValues, sb.toString(), strArr2);
                }
                a2.b();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: com.qihoo360.daily.b.c} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: com.qihoo360.daily.b.c} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: com.qihoo360.daily.b.c} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: com.qihoo360.daily.b.c} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v5, resolved type: com.qihoo360.daily.b.c} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v6, resolved type: com.qihoo360.daily.b.c} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.util.List r6) {
        /*
            r5 = this;
            r2 = 0
            if (r6 == 0) goto L_0x0009
            int r0 = r6.size()
            if (r0 > 0) goto L_0x000a
        L_0x0009:
            return
        L_0x000a:
            android.content.Context r0 = r5.f936a     // Catch:{ Exception -> 0x0038, all -> 0x0046 }
            com.qihoo360.daily.b.c r1 = com.qihoo360.daily.b.c.a(r0)     // Catch:{ Exception -> 0x0038, all -> 0x0046 }
            android.database.sqlite.SQLiteDatabase r2 = r1.a()     // Catch:{ Exception -> 0x0053 }
            int r3 = r6.size()     // Catch:{ Exception -> 0x0053 }
            r2.beginTransaction()     // Catch:{ Exception -> 0x0053 }
            r0 = 0
        L_0x001c:
            if (r0 >= r3) goto L_0x002c
            int r4 = r3 - r0
            int r4 = r4 + -1
            java.lang.Object r4 = r6.get(r4)     // Catch:{ Exception -> 0x0053 }
            r5.a(r4, r2)     // Catch:{ Exception -> 0x0053 }
            int r0 = r0 + 1
            goto L_0x001c
        L_0x002c:
            r2.setTransactionSuccessful()     // Catch:{ Exception -> 0x0053 }
            if (r2 == 0) goto L_0x0034
            r2.endTransaction()
        L_0x0034:
            r1.b()
            goto L_0x0009
        L_0x0038:
            r0 = move-exception
            r1 = r2
        L_0x003a:
            r0.printStackTrace()     // Catch:{ all -> 0x0051 }
            if (r2 == 0) goto L_0x0042
            r2.endTransaction()
        L_0x0042:
            r1.b()
            goto L_0x0009
        L_0x0046:
            r0 = move-exception
            r1 = r2
        L_0x0048:
            if (r2 == 0) goto L_0x004d
            r2.endTransaction()
        L_0x004d:
            r1.b()
            throw r0
        L_0x0051:
            r0 = move-exception
            goto L_0x0048
        L_0x0053:
            r0 = move-exception
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.b.d.a(java.util.List):void");
    }
}
