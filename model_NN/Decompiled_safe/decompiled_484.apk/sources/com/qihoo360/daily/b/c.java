package com.qihoo360.daily.b;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import java.util.concurrent.atomic.AtomicInteger;

public class c {

    /* renamed from: b  reason: collision with root package name */
    private static c f933b;

    /* renamed from: a  reason: collision with root package name */
    private AtomicInteger f934a = new AtomicInteger();
    private SQLiteDatabase c;
    private Context d;

    public c(Context context) {
        this.d = context;
    }

    public static synchronized c a(Context context) {
        c cVar;
        synchronized (c.class) {
            if (f933b == null) {
                f933b = new c(context);
            }
            cVar = f933b;
        }
        return cVar;
    }

    public synchronized SQLiteDatabase a() {
        if ((this.c == null || !this.c.isOpen()) && this.f934a.incrementAndGet() == 1) {
            try {
                this.c = new b(this.d).getWritableDatabase();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return this.c;
    }

    public synchronized void b() {
        if (this.c != null && this.c.isOpen() && this.f934a.decrementAndGet() == 0) {
            this.c.close();
        }
    }
}
