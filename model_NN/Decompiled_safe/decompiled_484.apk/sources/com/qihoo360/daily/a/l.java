package com.qihoo360.daily.a;

import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.PopupWindow;
import com.qihoo360.daily.R;
import com.qihoo360.daily.h.ad;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.widget.CmtItemLayout;

class l implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ p f911a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ k f912b;

    l(k kVar, p pVar) {
        this.f912b = kVar;
        this.f911a = pVar;
    }

    public void onClick(View view) {
        ad.a("setOnClickListener:");
        String charSequence = this.f911a.e.getText().toString();
        View inflate = View.inflate(this.f912b.f909a, R.layout.cmt_pop_view, null);
        PopupWindow popupWindow = new PopupWindow(inflate, -2, -2);
        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        inflate.setOnClickListener(new m(this, popupWindow, charSequence));
        n nVar = new n(this, popupWindow, charSequence);
        float width = (float) (view.getWidth() / 2);
        float lastY = ((CmtItemLayout) this.f911a.itemView).getLastY();
        int height = view.getHeight();
        int a2 = b.a(this.f912b.f909a, 48.0f);
        View findViewById = inflate.findViewById(R.id.replay);
        if (b.i(this.f911a.f.getUid())) {
            findViewById.setVisibility(8);
            a2 /= 2;
        }
        findViewById.setOnClickListener(nVar);
        inflate.findViewById(R.id.copy).setOnClickListener(nVar);
        popupWindow.showAsDropDown(view, (int) (width - ((float) a2)), (int) ((lastY - ((float) height)) - ((float) b.a(this.f912b.f909a, 96.0f))));
    }
}
