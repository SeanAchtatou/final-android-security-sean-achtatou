package com.qihoo360.daily.a;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import com.qihoo.dynamic.DotCount;
import com.qihoo360.daily.activity.PushListActivity;
import com.qihoo360.daily.e.ah;
import com.qihoo360.daily.e.aj;
import com.qihoo360.daily.e.ar;
import com.qihoo360.daily.e.bb;
import com.qihoo360.daily.e.bm;
import com.qihoo360.daily.e.ci;
import com.qihoo360.daily.e.r;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.Info;
import java.util.List;

public class ad extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public af f882a;

    /* renamed from: b  reason: collision with root package name */
    private PushListActivity f883b;
    private View c;
    private List<Info> d;

    public ad(PushListActivity pushListActivity, List<Info> list) {
        this.d = list;
        this.f883b = pushListActivity;
    }

    private Info i() {
        Info info = new Info();
        info.setV_t(-1);
        return info;
    }

    public List<Info> a() {
        return this.d;
    }

    public void a(View view) {
        this.c = view;
    }

    public void a(af afVar) {
        this.f882a = afVar;
    }

    public void a(Info info) {
        int i;
        if (info != null && this.d != null && !TextUtils.isEmpty(info.getNid())) {
            int size = this.d.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    i = -1;
                    break;
                } else if (info.getNid().equals(this.d.get(i2).getNid())) {
                    i = i2;
                    break;
                } else {
                    i2++;
                }
            }
            if (i != -1) {
                this.d.set(i, info);
            }
        }
    }

    public void b() {
        if (!f()) {
            this.d.add(i());
        }
    }

    public int c() {
        if (this.d == null || this.d.isEmpty()) {
            return 0;
        }
        return f() ? this.d.size() - 1 : this.d.size();
    }

    public void d() {
        if (f() && this.d != null && this.d.size() > 10) {
            this.d.get(this.d.size() - 1).setV_t(-1);
            notifyDataSetChanged();
        }
    }

    public void e() {
        if (f()) {
            this.d.get(this.d.size() - 1).setV_t(-4);
        }
    }

    public boolean f() {
        if (this.d == null || this.d.size() <= 0) {
            return false;
        }
        int v_t = this.d.get(this.d.size() - 1).getV_t();
        return v_t == -1 || v_t == -2 || v_t == -4;
    }

    public void g() {
        if (f() && this.d != null && this.d.size() > 0) {
            this.d.get(this.d.size() - 1).setV_t(-2);
        }
    }

    public int getItemCount() {
        if (this.d == null) {
            return 0;
        }
        return this.d.size();
    }

    public int getItemViewType(int i) {
        if (this.d == null || i >= this.d.size()) {
            return 0;
        }
        return this.d.get(i).getV_t();
    }

    public String h() {
        if (this.d == null || this.d.isEmpty()) {
            return null;
        }
        Info info = this.d.get(this.d.size() - 1);
        if (f()) {
            if (this.d.size() >= 2) {
                Info info2 = this.d.get(this.d.size() - 2);
                if (info2.getV_t() != 10) {
                    return info2.getC_pos();
                }
                if (this.d.size() >= 3) {
                    return this.d.get(this.d.size() - 3).getC_pos();
                }
            }
        } else if (info.getV_t() != 10) {
            return info.getC_pos();
        } else {
            if (this.d.size() >= 2) {
                return this.d.get(this.d.size() - 2).getC_pos();
            }
        }
        return "";
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        switch (viewHolder.getItemViewType()) {
            case DotCount.VXERR_OUTOFMEMORY:
                viewHolder.itemView.setOnClickListener(new ae(this));
                return;
            case -3:
            case 0:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            default:
                bb.a(viewHolder);
                return;
            case -2:
                ar.a();
                return;
            case -1:
                if (this.f882a != null) {
                    this.f882a.onLoadMore(h());
                }
                ah.a(viewHolder);
                return;
            case 1:
                ci.a(this.f883b, viewHolder, i, this.d.get(i), ChannelType.TYPE_PUSH_LIST, 6, 2);
                return;
            case 2:
                bm.a(this.f883b, viewHolder, i, this.d.get(i), ChannelType.TYPE_PUSH_LIST, 6, 2);
                return;
            case 3:
                r.a(this.f883b, viewHolder, i, this.d.get(i), ChannelType.TYPE_PUSH_LIST, 6, 2);
                return;
            case 11:
                bm.a(this.f883b, viewHolder, i, this.d.get(i), ChannelType.TYPE_PUSH_LIST, 6, 2);
                return;
        }
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        switch (i) {
            case DotCount.VXERR_OUTOFMEMORY:
                return aj.a(viewGroup.getContext());
            case -3:
            case 0:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            default:
                return bb.a(viewGroup.getContext());
            case -2:
                return ar.a(this.f883b);
            case -1:
                return ah.a(this.f883b);
            case 1:
                return ci.a(viewGroup.getContext());
            case 2:
                return bm.a(viewGroup.getContext());
            case 3:
                return r.b(viewGroup.getContext());
            case 11:
                return bm.a(this.f883b);
        }
    }
}
