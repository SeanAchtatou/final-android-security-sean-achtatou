package com.qihoo360.daily.a;

import android.view.View;
import android.widget.PopupWindow;
import com.qihoo360.daily.R;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.h.b;

class g implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopupWindow f901a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f902b;
    final /* synthetic */ e c;

    g(e eVar, PopupWindow popupWindow, String str) {
        this.c = eVar;
        this.f901a = popupWindow;
        this.f902b = str;
    }

    public void onClick(View view) {
        this.f901a.dismiss();
        switch (view.getId()) {
            case R.id.replay:
                if (this.c.c.c != null) {
                    try {
                        this.c.c.c.onReplay(this.c.c.f895b.subList(0, this.c.f897a.e + 1), this.c.f897a.d);
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                } else {
                    return;
                }
            case R.id.copy:
                b.c(this.c.c.f894a, this.f902b);
                ay.a(this.c.c.f894a).a((int) R.string.copy_ok);
                return;
            default:
                return;
        }
    }
}
