package com.qihoo360.daily.a;

import android.view.View;
import android.widget.PopupWindow;
import com.qihoo360.daily.R;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.h.b;

class n implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopupWindow f915a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f916b;
    final /* synthetic */ l c;

    n(l lVar, PopupWindow popupWindow, String str) {
        this.c = lVar;
        this.f915a = popupWindow;
        this.f916b = str;
    }

    public void onClick(View view) {
        this.f915a.dismiss();
        switch (view.getId()) {
            case R.id.replay:
                if (this.c.f912b.j != null) {
                    this.c.f912b.j.onReplay(this.c.f911a.f.getChain_comments(), this.c.f911a.f);
                    return;
                }
                return;
            case R.id.copy:
                b.c(this.c.f912b.f909a, this.f916b);
                ay.a(this.c.f912b.f909a).a((int) R.string.copy_ok);
                return;
            default:
                return;
        }
    }
}
