package com.qihoo360.daily.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.h.af;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.NewComment;
import com.qihoo360.daily.widget.CmtItemLayout;
import com.qihoo360.daily.widget.LinearLayoutForRecyclerView;
import java.util.ArrayList;
import java.util.List;

public class k extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f909a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public List<NewComment> f910b;
    private List<NewComment> c;
    /* access modifiers changed from: private */
    public q d;
    /* access modifiers changed from: private */
    public Animation e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public boolean g;
    private boolean h;
    private boolean i = true;
    /* access modifiers changed from: private */
    public r j;
    /* access modifiers changed from: private */
    public String k;
    private View.OnClickListener l = new o(this);

    public k(Context context, String str, List<NewComment> list, r rVar) {
        this.f909a = context;
        this.k = str;
        this.f910b = list;
        this.j = rVar;
        this.e = AnimationUtils.loadAnimation(this.f909a, R.anim.digg_click_scale);
    }

    private boolean c() {
        return this.h;
    }

    private boolean d() {
        return this.c != null && this.c.size() > 0;
    }

    public int a() {
        if (this.f910b != null) {
            return this.f910b.size();
        }
        return 0;
    }

    public void a(q qVar) {
        this.d = qVar;
    }

    public void a(NewComment newComment) {
        if (this.f910b == null) {
            this.f910b = new ArrayList();
        }
        this.f910b.add(0, newComment);
        notifyDataSetChanged();
    }

    public void a(List<NewComment> list) {
        this.c = list;
    }

    public void a(boolean z) {
        this.h = z;
    }

    public void b(List<NewComment> list) {
        this.f910b = list;
        this.f = 0;
        notifyDataSetChanged();
    }

    public void b(boolean z) {
        this.i = z;
    }

    public boolean b() {
        return this.i;
    }

    public void c(List<NewComment> list) {
        if (this.f910b == null) {
            this.f910b = list;
        } else if (list != null) {
            this.f910b.addAll(list);
        }
        notifyDataSetChanged();
    }

    public void c(boolean z) {
        this.g = z;
        this.f = 0;
        notifyDataSetChanged();
    }

    public int getItemCount() {
        if (this.f910b == null || this.f910b.size() <= 0) {
            return 0;
        }
        boolean c2 = c();
        int size = this.f910b.size();
        if (b()) {
            size++;
        }
        return !c2 ? size + 1 : size;
    }

    public int getItemViewType(int i2) {
        int itemViewType = super.getItemViewType(i2);
        if (b() && i2 == 0) {
            itemViewType = 3;
        }
        return (this.h || i2 != getItemCount() + -1) ? itemViewType : this.g ? 2 : 1;
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
        int size;
        if (i2 < getItemCount() && viewHolder != null) {
            p pVar = (p) viewHolder;
            int itemViewType = getItemViewType(i2);
            if (itemViewType == 2) {
                return;
            }
            if (itemViewType == 3) {
                View view = pVar.itemView;
                if (d()) {
                    view.findViewById(R.id.hot_cmt_layout).setVisibility(0);
                    k kVar = new k(this.f909a, this.k, this.c, this.j);
                    kVar.b(false);
                    kVar.a(true);
                    ((LinearLayoutForRecyclerView) view.findViewById(R.id.linearlayout_recyclerview)).setAdapter(kVar);
                    return;
                }
                view.findViewById(R.id.hot_cmt_layout).setVisibility(8);
            } else if (itemViewType != 1) {
                if (b()) {
                    i2--;
                }
                if (i2 >= 0 && i2 < this.f910b.size()) {
                    NewComment newComment = this.f910b.get(i2);
                    pVar.f = newComment;
                    af.b(this.f909a, pVar.f918a, newComment.getUavatar(), R.dimen.comment_avatar_size, R.dimen.comment_avatar_size, true, R.drawable.comment_avata);
                    pVar.f919b.setText(newComment.getUname());
                    pVar.c.setText(newComment.getSupport());
                    pVar.c.setTag(newComment);
                    if (newComment.getDiggok() == 1) {
                        pVar.c.setCompoundDrawablesWithIntrinsicBounds(0, 0, (int) R.drawable.comment_plus_done, 0);
                        pVar.c.setTextColor(this.f909a.getResources().getColor(R.color.praiseed_color));
                    } else {
                        pVar.c.setCompoundDrawablesWithIntrinsicBounds(0, 0, (int) R.drawable.comment_plus, 0);
                        pVar.c.setTextColor(this.f909a.getResources().getColor(R.color.gray_text));
                    }
                    pVar.c.setOnClickListener(this.l);
                    pVar.d.setText(b.c(newComment.getCreate_time()));
                    pVar.e.setText(newComment.getComment());
                    l lVar = new l(this, pVar);
                    List<NewComment> chain_comments = newComment.getChain_comments();
                    if (chain_comments == null || chain_comments.size() <= 0) {
                        pVar.g.setVisibility(8);
                        pVar.itemView.setOnClickListener(lVar);
                        pVar.e.setClickable(false);
                        return;
                    }
                    pVar.g.setAdapter(new c(this.f909a, newComment.getChain_comments(), this.j, pVar.g));
                    pVar.g.setVisibility(0);
                    pVar.itemView.setClickable(false);
                    pVar.e.setOnClickListener(lVar);
                }
            } else if (this.d != null && this.f != (size = this.f910b.size())) {
                this.f = size;
                this.d.onLoadMore(this.f910b.size() + "");
            }
        }
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        switch (i2) {
            case 1:
                return new p(View.inflate(this.f909a, R.layout.row_loadmore, null));
            case 2:
                View inflate = View.inflate(this.f909a, R.layout.row_loadmore_failed, null);
                inflate.setOnClickListener(this.l);
                return new p(inflate);
            case 3:
                return new p(View.inflate(this.f909a, R.layout.hot_cmt_header, null));
            default:
                CmtItemLayout cmtItemLayout = (CmtItemLayout) View.inflate(this.f909a, R.layout.row_comment, null);
                p pVar = new p(cmtItemLayout);
                pVar.f918a = (ImageView) cmtItemLayout.findViewById(R.id.comment_avata);
                pVar.f919b = (TextView) cmtItemLayout.findViewById(R.id.comment_account);
                pVar.d = (TextView) cmtItemLayout.findViewById(R.id.comment_time);
                pVar.c = (TextView) cmtItemLayout.findViewById(R.id.comment_plus_num);
                pVar.e = (TextView) cmtItemLayout.findViewById(R.id.comment_content);
                pVar.g = (LinearLayoutForRecyclerView) cmtItemLayout.findViewById(R.id.linearlayout_recyclerview);
                return pVar;
        }
    }
}
