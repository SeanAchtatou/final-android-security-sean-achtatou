package com.qihoo360.daily.a;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.SparseIntArray;
import android.view.ViewGroup;
import com.qihoo.dynamic.DotCount;
import com.qihoo360.daily.e.ae;
import com.qihoo360.daily.e.ah;
import com.qihoo360.daily.e.aj;
import com.qihoo360.daily.e.an;
import com.qihoo360.daily.e.bb;
import com.qihoo360.daily.e.bd;
import com.qihoo360.daily.e.bg;
import com.qihoo360.daily.e.bj;
import com.qihoo360.daily.e.bz;
import com.qihoo360.daily.e.cc;
import com.qihoo360.daily.e.cf;
import com.qihoo360.daily.e.h;
import com.qihoo360.daily.e.k;
import com.qihoo360.daily.e.v;
import com.qihoo360.daily.fragment.DailyFragment;
import com.qihoo360.daily.model.Info;
import java.util.List;

public class s extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /* renamed from: a  reason: collision with root package name */
    private List<Info> f920a;

    /* renamed from: b  reason: collision with root package name */
    private Activity f921b;
    private DailyFragment c;
    private SparseIntArray d = new SparseIntArray();

    public s(Activity activity) {
        this.f921b = activity;
    }

    private Info i() {
        Info info = new Info();
        info.setV_t(-1);
        return info;
    }

    public int a(Info info) {
        int i;
        if (info == null || this.f920a == null || TextUtils.isEmpty(info.getNid())) {
            return -1;
        }
        int size = this.f920a.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                i = -1;
                break;
            } else if (info.getNid().equals(this.f920a.get(i2).getNid())) {
                i = i2;
                break;
            } else {
                i2++;
            }
        }
        if (i == -1) {
            return i;
        }
        this.f920a.set(i, info);
        return i;
    }

    public List<Info> a() {
        return this.f920a;
    }

    public void a(DailyFragment dailyFragment) {
        this.c = dailyFragment;
    }

    public void a(List<Info> list) {
        this.d.clear();
        this.f920a = list;
        if (list.size() <= 2) {
            if (!f()) {
                this.f920a.add(i());
            }
            c();
        } else if (!f()) {
            this.f920a.add(i());
        } else {
            e();
        }
    }

    public String b() {
        if (this.f920a == null || this.f920a.isEmpty()) {
            return null;
        }
        Info info = this.f920a.get(this.f920a.size() - 1);
        if (f()) {
            if (this.f920a.size() >= 2) {
                Info info2 = this.f920a.get(this.f920a.size() - 2);
                if (info2.getV_t() != 10) {
                    return info2.getC_pos();
                }
                if (this.f920a.size() >= 3) {
                    return this.f920a.get(this.f920a.size() - 3).getC_pos();
                }
            }
        } else if (info.getV_t() != 10) {
            return info.getC_pos();
        } else {
            if (this.f920a.size() >= 2) {
                return this.f920a.get(this.f920a.size() - 2).getC_pos();
            }
        }
        return null;
    }

    public void b(List<Info> list) {
        if (list != null && !list.isEmpty()) {
            if (this.f920a != null) {
                if (f()) {
                    this.f920a.remove(this.f920a.size() - 1);
                }
                this.f920a.addAll(list);
            } else {
                this.f920a = list;
            }
            this.f920a.add(i());
        }
    }

    public void c() {
        if (f()) {
            this.f920a.get(this.f920a.size() - 1).setV_t(-2);
        }
    }

    public void d() {
        if (f()) {
            this.f920a.get(this.f920a.size() - 1).setV_t(-4);
        }
    }

    public void e() {
        if (f()) {
            this.f920a.get(this.f920a.size() - 1).setV_t(-1);
        }
    }

    public boolean f() {
        if (this.f920a == null || this.f920a.size() <= 0) {
            return false;
        }
        int v_t = this.f920a.get(this.f920a.size() - 1).getV_t();
        return v_t == -1 || v_t == -2 || v_t == -4;
    }

    public boolean g() {
        if (this.f920a == null || this.f920a.size() <= 0) {
            return false;
        }
        return this.f920a.get(this.f920a.size() + -1).getV_t() == -4;
    }

    public int getItemCount() {
        if (this.f920a != null) {
            return this.f920a.size();
        }
        return 0;
    }

    public int getItemViewType(int i) {
        return ((this.f920a == null || this.f920a.size() <= i || i < 0) ? new Info() : this.f920a.get(i)).getV_t();
    }

    public boolean h() {
        if (this.f920a == null || this.f920a.size() <= 0) {
            return false;
        }
        return this.f920a.get(this.f920a.size() + -1).getV_t() == -2;
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder != null) {
            Info info = null;
            if (this.f920a != null && this.f920a.size() > i) {
                info = this.f920a.get(i);
            }
            switch (viewHolder.getItemViewType()) {
                case DotCount.VXERR_CANCELED:
                    k.a(this.f921b, this.c, viewHolder, i, info, "daily", 1);
                    return;
                case DotCount.VXERR_SYMKEY_EXPIRED:
                    h.a(this.f921b, this.c, viewHolder, i, info, "daily", 1);
                    return;
                case DotCount.VXERR_NO_SYMKEY:
                    bg.a(this.c, viewHolder, info);
                    return;
                case DotCount.VXERR_OUTOFMEMORY:
                    aj.a(viewHolder, this.c);
                    return;
                case -3:
                case 0:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case 18:
                case 19:
                default:
                    bb.a(viewHolder);
                    return;
                case -2:
                    an.a(this.c, viewHolder);
                    return;
                case -1:
                    ah.a(viewHolder);
                    return;
                case 1:
                    bz.a(this.f921b, this.c, viewHolder, i, info, "daily", 1);
                    return;
                case 2:
                    cc.a(this.f921b, this.c, viewHolder, i, info, "daily", 1);
                    return;
                case 3:
                    cf.a(this.f921b, this.c, viewHolder, i, info, "daily", 1);
                    return;
                case 16:
                    ae.a(this.f921b, this.c, viewHolder, i, info, "daily", 1);
                    return;
                case 17:
                case 22:
                    bj.a(this.f921b, this.c, viewHolder, i, info, "daily", 1);
                    return;
                case 20:
                    bd.a(this.f921b, this.c, viewHolder, i, info, "daily", 1);
                    return;
                case 21:
                    v.a(this.f921b, this.c, viewHolder, i, info, "daily", 1);
                    return;
            }
        }
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        switch (i) {
            case DotCount.VXERR_CANCELED:
                return k.a(viewGroup.getContext());
            case DotCount.VXERR_SYMKEY_EXPIRED:
                return h.a(viewGroup.getContext());
            case DotCount.VXERR_NO_SYMKEY:
                this.c.fackNewsCount++;
                return bg.a(viewGroup.getContext());
            case DotCount.VXERR_OUTOFMEMORY:
                return aj.a(viewGroup.getContext());
            case -3:
            case 0:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 18:
            case 19:
            default:
                return bb.a(this.f921b);
            case -2:
                return an.a(this.f921b);
            case -1:
                return ah.a(this.f921b);
            case 1:
                return bz.a(this.f921b);
            case 2:
                return cc.a(this.f921b);
            case 3:
                return cf.a(this.f921b);
            case 16:
                return ae.a(viewGroup.getContext());
            case 17:
            case 22:
                return bj.a(viewGroup.getContext());
            case 20:
                this.c.fackNewsCount++;
                return bd.a(viewGroup.getContext());
            case 21:
                return v.a(viewGroup.getContext());
        }
    }
}
