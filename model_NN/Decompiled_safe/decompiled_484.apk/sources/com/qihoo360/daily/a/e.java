package com.qihoo360.daily.a;

import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.PopupWindow;
import com.qihoo360.daily.R;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.widget.CmtItemLayout;

class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f897a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ CmtItemLayout f898b;
    final /* synthetic */ c c;

    e(c cVar, h hVar, CmtItemLayout cmtItemLayout) {
        this.c = cVar;
        this.f897a = hVar;
        this.f898b = cmtItemLayout;
    }

    public void onClick(View view) {
        String charSequence = this.f897a.c.getText().toString();
        View inflate = View.inflate(this.c.f894a, R.layout.cmt_pop_view, null);
        PopupWindow popupWindow = new PopupWindow(inflate, -2, -2);
        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        inflate.setOnClickListener(new f(this, popupWindow, charSequence));
        g gVar = new g(this, popupWindow, charSequence);
        float width = (float) (view.getWidth() / 2);
        float lastY = this.f898b.getLastY();
        int height = view.getHeight();
        int a2 = b.a(this.c.f894a, 48.0f);
        View findViewById = inflate.findViewById(R.id.replay);
        if (b.i(this.f897a.d.getUid())) {
            findViewById.setVisibility(8);
            a2 /= 2;
        }
        findViewById.setOnClickListener(gVar);
        inflate.findViewById(R.id.copy).setOnClickListener(gVar);
        popupWindow.showAsDropDown(view, (int) (width - ((float) a2)), (int) ((lastY - ((float) height)) - ((float) b.a(this.c.f894a, 96.0f))));
    }
}
