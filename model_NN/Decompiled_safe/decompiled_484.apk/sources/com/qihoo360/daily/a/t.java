package com.qihoo360.daily.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import com.qihoo.dynamic.DotCount;
import com.qihoo360.daily.activity.FavouriteActivity;
import com.qihoo360.daily.e.ab;
import com.qihoo360.daily.e.ah;
import com.qihoo360.daily.e.aj;
import com.qihoo360.daily.e.an;
import com.qihoo360.daily.e.bm;
import com.qihoo360.daily.e.ci;
import com.qihoo360.daily.e.e;
import com.qihoo360.daily.e.n;
import com.qihoo360.daily.e.r;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.g.h;
import com.qihoo360.daily.g.k;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.FavouriteInfo;
import com.qihoo360.daily.widget.BadgeView;
import java.util.List;

public class t extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /* renamed from: a  reason: collision with root package name */
    private boolean f922a = false;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public FavouriteActivity f923b;
    private View c;
    private List<FavouriteInfo> d;
    private x e;

    public t(FavouriteActivity favouriteActivity, List<FavouriteInfo> list) {
        this.d = list;
        this.f923b = favouriteActivity;
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        if (this.d != null && this.d.size() > i) {
            b(true);
            new h(k.DEL, this.d.get(i).getNid()).a(new v(this, i), new Object[0]);
        }
    }

    /* access modifiers changed from: private */
    public void b(int i) {
        if (this.d != null && this.d.size() > i) {
            this.d.remove(i);
            if (this.d.isEmpty()) {
                this.f922a = false;
            }
            i();
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        if (this.c != null) {
            this.c.setVisibility(z ? 0 : 8);
        }
    }

    private void i() {
        notifyDataSetChanged();
        if (this.e != null) {
            this.e.onDataSetChanged();
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        this.d = null;
        this.f922a = false;
        d.g(this.f923b);
        a(false);
        if (this.e != null) {
            this.e.onDataSetChanged();
        }
    }

    private FavouriteInfo k() {
        FavouriteInfo favouriteInfo = new FavouriteInfo();
        favouriteInfo.setV_t(-1);
        return favouriteInfo;
    }

    public void a(View view) {
        this.c = view;
    }

    public void a(x xVar) {
        this.e = xVar;
    }

    public void a(FavouriteInfo favouriteInfo) {
        int i;
        if (favouriteInfo != null && this.d != null && !TextUtils.isEmpty(favouriteInfo.getNid())) {
            int size = this.d.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    i = -1;
                    break;
                } else if (favouriteInfo.getNid().equals(this.d.get(i2).getNid())) {
                    i = i2;
                    break;
                } else {
                    i2++;
                }
            }
            if (i != -1) {
                this.d.set(i, favouriteInfo);
            }
        }
    }

    public void a(String str) {
        int i;
        if (this.d != null) {
            int i2 = 0;
            while (true) {
                i = i2;
                if (i >= this.d.size()) {
                    i = -1;
                    break;
                } else if (str.equals(this.d.get(i).getNid())) {
                    break;
                } else {
                    i2 = i + 1;
                }
            }
            if (i >= 0) {
                b(i);
            }
        }
    }

    public void a(List<FavouriteInfo> list) {
        if (this.d == null) {
            this.d = list;
        } else {
            this.d.addAll(c(), list);
        }
        if (this.d != null && this.d.size() >= 10 && !g()) {
            this.d.add(k());
        }
        i();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.a(android.content.Context, boolean):void
     arg types: [com.qihoo360.daily.activity.FavouriteActivity, boolean]
     candidates:
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String):java.lang.String
      com.qihoo360.daily.f.d.a(android.content.Context, int):void
      com.qihoo360.daily.f.d.a(android.content.Context, long):void
      com.qihoo360.daily.f.d.a(android.content.Context, com.qihoo360.daily.model.City):void
      com.qihoo360.daily.f.d.a(android.content.Context, com.qihoo360.daily.model.QdData):void
      com.qihoo360.daily.f.d.a(android.content.Context, com.qihoo360.daily.model.UpdateInfo):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.util.List<com.qihoo360.daily.model.FavouriteInfo>):void
      com.qihoo360.daily.f.d.a(android.content.Context, boolean):void */
    public void a(boolean z) {
        this.f922a = z;
        d.a((Context) this.f923b, z);
        notifyDataSetChanged();
    }

    public boolean a() {
        return this.f922a;
    }

    public List<FavouriteInfo> b() {
        return this.d;
    }

    public int c() {
        if (this.d == null || this.d.isEmpty()) {
            return 0;
        }
        return g() ? this.d.size() - 1 : this.d.size();
    }

    public void d() {
        StringBuffer stringBuffer = new StringBuffer();
        for (FavouriteInfo nid : this.d) {
            stringBuffer.append(nid.getNid()).append(",");
        }
        String substring = stringBuffer.substring(0, stringBuffer.length() == 0 ? 0 : stringBuffer.length());
        b(true);
        new h(k.DEL, substring).a(new w(this), new Object[0]);
    }

    public String e() {
        return getItemCount() == 0 ? "0" : this.d.get(c() - 1).getPos();
    }

    public void f() {
        if (g() && this.d != null && this.d.size() > 10) {
            this.d.get(this.d.size() - 1).setV_t(-1);
            notifyDataSetChanged();
        }
    }

    public boolean g() {
        if (this.d == null || this.d.size() <= 0) {
            return false;
        }
        int v_t = this.d.get(this.d.size() - 1).getV_t();
        return v_t == -1 || v_t == -2 || v_t == -4;
    }

    public int getItemCount() {
        if (this.d == null) {
            return 0;
        }
        return this.d.size();
    }

    public int getItemViewType(int i) {
        if (this.d == null || i >= this.d.size()) {
            return 0;
        }
        return this.d.get(i).getV_t();
    }

    public void h() {
        if (g() && this.d != null && this.d.size() > 10) {
            this.d.get(this.d.size() - 1).setV_t(-2);
            notifyDataSetChanged();
        }
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        boolean z = true;
        switch (viewHolder.getItemViewType()) {
            case DotCount.VXERR_OUTOFMEMORY:
                aj.a(viewHolder, this.f923b, this.f922a);
                break;
            case -3:
            case 0:
            case 5:
            case 6:
            case 9:
            case 10:
            default:
                ci.a(this.f923b, viewHolder, i, this.d.get(i), ChannelType.TYPE_FAVOR, 6, 1);
                break;
            case -2:
                an.a(this.f923b, viewHolder, this.f922a);
                return;
            case -1:
                ah.a(viewHolder, this.f922a);
                return;
            case 1:
                ci.a(this.f923b, viewHolder, i, this.d.get(i), ChannelType.TYPE_FAVOR, 6, 1);
                break;
            case 2:
                bm.a(this.f923b, viewHolder, i, this.d.get(i), ChannelType.TYPE_FAVOR, 6, 1);
                break;
            case 3:
                r.a(this.f923b, viewHolder, i, this.d.get(i), ChannelType.TYPE_FAVOR, 6, 1);
                break;
            case 4:
                n.a(this.f923b, viewHolder, i, this.d.get(i), ChannelType.TYPE_FAVOR, 6);
                break;
            case 7:
                ab.a(this.f923b, viewHolder, i, this.d.get(i), ChannelType.TYPE_FAVOR, 6);
                break;
            case 8:
                e.a(this.f923b, viewHolder, i, this.d.get(i), ChannelType.TYPE_FAVOR, 6);
                break;
            case 11:
                bm.a(this.f923b, viewHolder, i, this.d.get(i), ChannelType.TYPE_FAVOR, 6, 1);
                break;
        }
        BadgeView badgeView = (BadgeView) viewHolder.itemView.getTag();
        View view = viewHolder.itemView;
        if (this.f922a) {
            z = false;
        }
        view.setEnabled(z);
        if (this.f922a) {
            badgeView.setOnClickListener(new u(this, i));
            badgeView.show();
            return;
        }
        badgeView.hide();
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder a2;
        switch (i) {
            case DotCount.VXERR_OUTOFMEMORY:
                a2 = aj.a(viewGroup.getContext());
                break;
            case -3:
            case 0:
            case 5:
            case 6:
            case 9:
            case 10:
            default:
                a2 = ci.a(viewGroup.getContext());
                break;
            case -2:
                a2 = an.a(this.f923b);
                break;
            case -1:
                a2 = ah.a(this.f923b);
                break;
            case 1:
                a2 = ci.a(viewGroup.getContext());
                break;
            case 2:
                a2 = bm.a(viewGroup.getContext());
                break;
            case 3:
                a2 = r.b(viewGroup.getContext());
                break;
            case 4:
                a2 = n.a(viewGroup.getContext());
                break;
            case 7:
                a2 = ab.a(viewGroup.getContext());
                break;
            case 8:
                a2 = e.a(viewGroup.getContext());
                break;
            case 11:
                a2 = bm.a(this.f923b);
                break;
        }
        if (a2 != null) {
            a2.itemView.setTag(new BadgeView(a2.itemView.getContext(), a2.itemView));
        }
        return a2;
    }
}
