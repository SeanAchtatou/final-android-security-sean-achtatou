package com.qihoo360.daily.a;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;
import com.qihoo360.daily.activity.SearchActivity;
import com.qihoo360.daily.fragment.ImgRecFragment;
import com.qihoo360.daily.fragment.TouchImageFragment2;
import com.qihoo360.daily.model.Info;
import java.util.List;

public class ab extends FragmentStatePagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private List<String> f878a;

    /* renamed from: b  reason: collision with root package name */
    private List<String> f879b;
    private View.OnClickListener c;
    private View.OnLongClickListener d;
    private Info e;
    private boolean f;
    private ImgRecFragment g;

    public ab(FragmentManager fragmentManager, Info info, List<String> list, List<String> list2, boolean z, View.OnClickListener onClickListener, View.OnLongClickListener onLongClickListener) {
        super(fragmentManager);
        this.e = info;
        this.f879b = list;
        this.f878a = list2;
        this.c = onClickListener;
        this.d = onLongClickListener;
        this.f = z;
    }

    private void a(Fragment fragment) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("Info", this.e);
        fragment.setArguments(bundle);
    }

    public ImgRecFragment a() {
        if (this.g == null) {
            this.g = new ImgRecFragment();
            a(this.g);
        }
        return this.g;
    }

    public int getCount() {
        int i = 0;
        if (this.f878a != null) {
            i = this.f878a.size();
        }
        return this.f ? i + 1 : i;
    }

    public Fragment getItem(int i) {
        int size = this.f878a == null ? 0 : this.f878a.size();
        if (!this.f || i != size) {
            TouchImageFragment2 touchImageFragment2 = new TouchImageFragment2();
            touchImageFragment2.setOnClickListener(this.c);
            touchImageFragment2.setOnLongClickListener(this.d);
            touchImageFragment2.setImageUrls(this.f879b);
            Bundle bundle = new Bundle();
            if (this.f878a != null && size > i) {
                bundle.putString(SearchActivity.TAG_URL, this.f878a.get(i));
                if (i == 0) {
                    bundle.putBoolean("save2disk", true);
                }
            }
            touchImageFragment2.setArguments(bundle);
            return touchImageFragment2;
        }
        if (this.g == null || this.g.isAdded()) {
            this.g = new ImgRecFragment();
            a(this.g);
        }
        return this.g;
    }
}
