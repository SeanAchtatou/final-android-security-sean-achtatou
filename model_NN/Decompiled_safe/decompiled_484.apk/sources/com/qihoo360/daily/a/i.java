package com.qihoo360.daily.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.model.City;
import java.util.Arrays;
import java.util.List;

public class i extends ArrayAdapter<City> implements SectionIndexer {

    /* renamed from: a  reason: collision with root package name */
    private String f905a = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /* renamed from: b  reason: collision with root package name */
    private List<City> f906b;
    private int[] c;
    private boolean d;
    private int e;
    private int f;

    public i(Context context, List<City> list, int i) {
        super(context, (int) R.layout.item_city, (int) R.id.tv_city_name, list);
        int a2;
        this.f906b = list;
        this.e = i;
        this.c = new int[this.f905a.length()];
        Arrays.fill(this.c, -100);
        this.c[0] = -1;
        this.c[1] = 0;
        char c2 = 'a';
        for (int i2 = 0; i2 < list.size(); i2++) {
            char charAt = list.get(i2).getCitySpell().charAt(0);
            if (!(c2 == charAt || (a2 = a(charAt)) == -100)) {
                this.c[a2] = i2;
                c2 = charAt;
            }
        }
    }

    private int a(char c2) {
        for (int i = 0; i < this.f905a.length(); i++) {
            if (c2 == Character.toLowerCase(this.f905a.charAt(i))) {
                return i;
            }
        }
        return -100;
    }

    public void a(boolean z) {
        this.d = z;
    }

    public int getPositionForSection(int i) {
        int i2 = this.c[i];
        if (i2 == -1) {
            return 0;
        }
        if (i2 == -100 && this.f > 0) {
            return this.f;
        }
        int i3 = i2 + this.e;
        this.f = i3;
        return i3;
    }

    public int getSectionForPosition(int i) {
        return 0;
    }

    public Object[] getSections() {
        String[] strArr = new String[this.f905a.length()];
        for (int i = 0; i < this.f905a.length(); i++) {
            strArr[i] = String.valueOf(this.f905a.charAt(i));
        }
        return strArr;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        j jVar;
        City city = (City) getItem(i);
        if (view == null) {
            view = View.inflate(viewGroup.getContext(), R.layout.item_city, null);
            j jVar2 = new j(this);
            jVar2.f908b = (TextView) view.findViewById(R.id.tv_city_name);
            jVar2.f907a = (TextView) view.findViewById(R.id.tv_section);
            view.setTag(jVar2);
            jVar = jVar2;
        } else {
            jVar = (j) view.getTag();
        }
        jVar.f908b.setText(city.getName());
        jVar.f907a.setVisibility(8);
        if (!this.d) {
            int[] iArr = this.c;
            int length = iArr.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    break;
                } else if (i == iArr[i2]) {
                    jVar.f907a.setVisibility(0);
                    jVar.f907a.setText(String.valueOf(Character.toUpperCase(city.getCitySpell().charAt(0))));
                    break;
                } else {
                    i2++;
                }
            }
        }
        return view;
    }
}
