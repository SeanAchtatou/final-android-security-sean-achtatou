package com.qihoo360.daily.a;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.ViewGroup;
import com.qihoo.dynamic.DotCount;
import com.qihoo360.daily.e.ab;
import com.qihoo360.daily.e.ae;
import com.qihoo360.daily.e.ah;
import com.qihoo360.daily.e.aj;
import com.qihoo360.daily.e.an;
import com.qihoo360.daily.e.at;
import com.qihoo360.daily.e.bb;
import com.qihoo360.daily.e.bd;
import com.qihoo360.daily.e.bg;
import com.qihoo360.daily.e.bj;
import com.qihoo360.daily.e.bm;
import com.qihoo360.daily.e.bz;
import com.qihoo360.daily.e.cc;
import com.qihoo360.daily.e.cf;
import com.qihoo360.daily.e.ci;
import com.qihoo360.daily.e.e;
import com.qihoo360.daily.e.h;
import com.qihoo360.daily.e.k;
import com.qihoo360.daily.e.n;
import com.qihoo360.daily.e.r;
import com.qihoo360.daily.e.v;
import com.qihoo360.daily.e.y;
import com.qihoo360.daily.fragment.BaseListFragment;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.Info;
import java.util.ArrayList;
import java.util.List;

public class ac extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /* renamed from: a  reason: collision with root package name */
    public List<RecyclerView.ViewHolder> f880a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    private Activity f881b;
    private int c;
    private List<Info> d;
    private BaseListFragment e;
    private String f;

    public ac(Activity activity, List<Info> list, String str, int i) {
        this.f881b = activity;
        this.d = list;
        this.f = str;
        this.c = i;
    }

    private Info j() {
        Info info = new Info();
        info.setV_t(-1);
        return info;
    }

    public List<Info> a() {
        return this.d;
    }

    public void a(BaseListFragment baseListFragment) {
        this.e = baseListFragment;
    }

    public void a(Info info) {
        int i;
        if (info != null && this.d != null && !TextUtils.isEmpty(info.getNid())) {
            int size = this.d.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    i = -1;
                    break;
                } else if (info.getNid().equals(this.d.get(i2).getNid())) {
                    i = i2;
                    break;
                } else {
                    i2++;
                }
            }
            if (i != -1) {
                this.d.set(i, info);
            }
        }
    }

    public void a(List<Info> list) {
        this.d = list;
    }

    public void a(List<Info> list, boolean z, boolean z2) {
        if (list != null && list.size() > 0) {
            if (this.d == null || this.d.size() == 0) {
                this.d = list;
                if (this.d.size() <= 2) {
                    this.d.add(j());
                    b();
                    return;
                }
                this.d.add(j());
            } else if (!z) {
                if (!z2) {
                    this.d.addAll(0, list);
                } else {
                    this.d.clear();
                    this.d.addAll(list);
                }
                if (!f()) {
                    this.d.add(j());
                } else {
                    d();
                }
            } else {
                if (f()) {
                    this.d.remove(this.d.size() - 1);
                }
                this.d.addAll(this.d.size(), list);
                this.d.add(j());
            }
        }
    }

    public void b() {
        if (f()) {
            this.d.get(this.d.size() - 1).setV_t(-2);
        }
    }

    public void c() {
        if (f()) {
            this.d.get(this.d.size() - 1).setV_t(-4);
        }
    }

    public void d() {
        if (f()) {
            this.d.get(this.d.size() - 1).setV_t(-1);
        }
    }

    public Info e() {
        if (this.d == null || this.d.size() <= 0) {
            return null;
        }
        if (!f()) {
            return this.d.get(this.d.size() - 1);
        }
        if (this.d.size() > 1) {
            return this.d.get(this.d.size() - 2);
        }
        return null;
    }

    public boolean f() {
        if (this.d == null || this.d.size() <= 0) {
            return false;
        }
        int v_t = this.d.get(this.d.size() - 1).getV_t();
        return v_t == -1 || v_t == -2 || v_t == -4;
    }

    public boolean g() {
        if (this.d == null || this.d.size() <= 0) {
            return false;
        }
        return this.d.get(this.d.size() + -1).getV_t() == -2;
    }

    public int getItemCount() {
        if (this.d != null) {
            return this.d.size();
        }
        return 0;
    }

    public int getItemViewType(int i) {
        return this.d.get(i).getV_t();
    }

    public boolean h() {
        if (this.d == null || this.d.size() <= 0) {
            return false;
        }
        return this.d.get(this.d.size() + -1).getV_t() == -4;
    }

    public boolean i() {
        return this.d == null || ((!f() || this.d.size() <= 1) && this.d.size() <= 0);
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        int itemViewType = viewHolder.getItemViewType();
        Info info = this.d.get(i);
        switch (itemViewType) {
            case -11:
                cf.a(this.f881b, this.e, viewHolder, i, info, this.f, this.c);
                return;
            case -10:
                cc.a(this.f881b, this.e, viewHolder, i, info, this.f, this.c);
                return;
            case DotCount.VXERR_VERIFY_FAIL:
                bz.a(this.f881b, this.e, viewHolder, i, info, this.f, this.c);
                return;
            case DotCount.VXERR_NETQUERY:
            case -3:
            case 0:
            case 5:
            case 6:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            default:
                bb.a(viewHolder);
                return;
            case DotCount.VXERR_CANCELED:
                k.a(this.f881b, this.e, viewHolder, i, info, this.f, this.c);
                return;
            case DotCount.VXERR_SYMKEY_EXPIRED:
                h.a(this.f881b, this.e, viewHolder, i, info, this.f, this.c);
                return;
            case DotCount.VXERR_NO_SYMKEY:
                bg.a(this.e, viewHolder, info);
                return;
            case DotCount.VXERR_OUTOFMEMORY:
                aj.a(viewHolder, this.e);
                return;
            case -2:
                an.a(this.e, viewHolder);
                return;
            case -1:
                ah.a(viewHolder);
                return;
            case 1:
                ci.a(this.f881b, viewHolder, i, info, this.f, this.c, 2);
                return;
            case 2:
                bm.a(this.f881b, viewHolder, i, info, this.f, this.c, 2);
                return;
            case 3:
                r.a(this.f881b, viewHolder, i, info, this.f, this.c, 2);
                return;
            case 4:
                this.f880a.add(viewHolder);
                n.a(this.f881b, viewHolder, i, info, this.f, this.c);
                return;
            case 7:
                if (!ChannelType.TYPE_CHANNEL_JOKE.equals(this.f)) {
                    ab.a(this.f881b, viewHolder, i, info, this.f, this.c);
                    return;
                }
                ab.a(this.f881b, viewHolder, i, this.d, this.f, this.c);
                return;
            case 8:
                e.a(this.f881b, viewHolder, i, info, this.f, this.c);
                return;
            case 16:
                ae.a(this.f881b, this.e, viewHolder, i, info, this.f, this.c);
                return;
            case 17:
            case 22:
                bj.a(this.f881b, this.e, viewHolder, i, info, this.f, this.c);
                return;
            case 18:
                y.a(this.f881b, viewHolder, this.f, info);
                return;
            case 19:
                at.a(this.f881b, viewHolder, this.e);
                return;
            case 20:
                bd.a(this.f881b, this.e, viewHolder, i, info, this.f, this.c);
                return;
            case 21:
                v.a(this.f881b, this.e, viewHolder, i, info, this.f, this.c);
                return;
        }
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        switch (i) {
            case -11:
                return cf.a(this.f881b);
            case -10:
                return cc.a(this.f881b);
            case DotCount.VXERR_VERIFY_FAIL:
                return bz.a(this.f881b);
            case DotCount.VXERR_NETQUERY:
            case -3:
            case 0:
            case 5:
            case 6:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            default:
                return bb.a(this.f881b);
            case DotCount.VXERR_CANCELED:
                return k.a(viewGroup.getContext());
            case DotCount.VXERR_SYMKEY_EXPIRED:
                return h.a(viewGroup.getContext());
            case DotCount.VXERR_NO_SYMKEY:
                return bg.a(viewGroup.getContext());
            case DotCount.VXERR_OUTOFMEMORY:
                return aj.a(viewGroup.getContext());
            case -2:
                return an.a(viewGroup.getContext());
            case -1:
                return ah.a(viewGroup.getContext());
            case 1:
                return ci.a(viewGroup.getContext());
            case 2:
                return bm.a(viewGroup.getContext());
            case 3:
                return r.a(viewGroup.getContext());
            case 4:
                return n.a(viewGroup.getContext());
            case 7:
                return ab.a(viewGroup.getContext());
            case 8:
                return e.a(viewGroup.getContext());
            case 16:
                return ae.a(viewGroup.getContext());
            case 17:
            case 22:
                return bj.a(viewGroup.getContext());
            case 18:
                return y.a(viewGroup.getContext());
            case 19:
                return at.a(viewGroup.getContext());
            case 20:
                return bd.a(viewGroup.getContext());
            case 21:
                return v.a(viewGroup.getContext());
        }
    }

    public void onViewDetachedFromWindow(RecyclerView.ViewHolder viewHolder) {
        super.onViewDetachedFromWindow(viewHolder);
        int itemViewType = viewHolder.getItemViewType();
        viewHolder.getPosition();
        switch (itemViewType) {
            case 4:
                n.a(viewHolder);
                return;
            default:
                return;
        }
    }
}
