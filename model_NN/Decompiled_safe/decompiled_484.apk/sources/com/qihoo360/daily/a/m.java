package com.qihoo360.daily.a;

import android.view.View;
import android.widget.PopupWindow;
import com.qihoo360.daily.R;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.h.b;

class m implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopupWindow f913a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f914b;
    final /* synthetic */ l c;

    m(l lVar, PopupWindow popupWindow, String str) {
        this.c = lVar;
        this.f913a = popupWindow;
        this.f914b = str;
    }

    public void onClick(View view) {
        this.f913a.dismiss();
        b.c(this.c.f912b.f909a, this.f914b);
        ay.a(this.c.f912b.f909a).a((int) R.string.copy_ok);
    }
}
