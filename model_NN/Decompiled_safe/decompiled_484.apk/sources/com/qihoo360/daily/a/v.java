package com.qihoo360.daily.a;

import com.qihoo360.daily.R;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.model.Result;

class v extends c<Void, Result<Object>> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f926a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ t f927b;

    v(t tVar, int i) {
        this.f927b = tVar;
        this.f926a = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.a.t.a(com.qihoo360.daily.a.t, boolean):void
     arg types: [com.qihoo360.daily.a.t, int]
     candidates:
      com.qihoo360.daily.a.t.a(com.qihoo360.daily.a.t, int):void
      com.qihoo360.daily.a.t.a(com.qihoo360.daily.a.t, boolean):void */
    /* renamed from: a */
    public void onNetRequest(int i, Result<Object> result) {
        this.f927b.b(false);
        if (result != null) {
            int status = result.getStatus();
            if (status == 0) {
                this.f927b.b(this.f926a);
                ay.a(this.f927b.f923b).a((int) R.string.favor_del_ok);
            } else if (status == 103) {
                ay.a(this.f927b.f923b).a((int) R.string.login_dialog_title_relogin);
            } else {
                ay.a(this.f927b.f923b).a(result.getMsg());
            }
        } else {
            ay.a(this.f927b.f923b).a((int) R.string.net_error);
        }
    }
}
