package com.qihoo360.daily.a;

import com.qihoo360.daily.R;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.model.Result;

class w extends c<Void, Result<Object>> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ t f928a;

    w(t tVar) {
        this.f928a = tVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.a.t.a(com.qihoo360.daily.a.t, boolean):void
     arg types: [com.qihoo360.daily.a.t, int]
     candidates:
      com.qihoo360.daily.a.t.a(com.qihoo360.daily.a.t, int):void
      com.qihoo360.daily.a.t.a(com.qihoo360.daily.a.t, boolean):void */
    /* renamed from: a */
    public void onNetRequest(int i, Result<Object> result) {
        this.f928a.b(false);
        if (result != null) {
            int status = result.getStatus();
            if (status == 0) {
                this.f928a.j();
                ay.a(this.f928a.f923b).a((int) R.string.favor_del_ok);
            } else if (status == 103) {
                ay.a(this.f928a.f923b).a((int) R.string.login_dialog_title_relogin);
            } else {
                ay.a(this.f928a.f923b).a(result.getMsg());
            }
        } else {
            ay.a(this.f928a.f923b).a((int) R.string.net_error);
        }
    }
}
