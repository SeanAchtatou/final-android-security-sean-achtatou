package com.qihoo360.daily.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.qihoo360.daily.e.ay;
import com.qihoo360.daily.e.bb;
import com.qihoo360.daily.e.bp;
import com.qihoo360.daily.e.br;
import com.qihoo360.daily.e.bt;
import com.qihoo360.daily.e.bw;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.SpecialTopicLanmu;
import java.util.List;

public class ak extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /* renamed from: a  reason: collision with root package name */
    private Context f890a;

    /* renamed from: b  reason: collision with root package name */
    private List<Info> f891b;

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0045 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ak(android.content.Context r5, com.qihoo360.daily.model.SpecialTopic r6) {
        /*
            r4 = this;
            r4.<init>()
            r4.f890a = r5
            if (r6 != 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r4.f891b = r0
            java.util.List<com.qihoo360.daily.model.Info> r0 = r4.f891b
            r0.add(r6)
            java.util.List r2 = r6.getTopicdata()
            if (r2 == 0) goto L_0x0007
            r0 = 0
            r1 = r0
        L_0x001c:
            int r0 = r2.size()
            if (r1 >= r0) goto L_0x005a
            java.lang.Object r0 = r2.get(r1)
            com.qihoo360.daily.model.SpecialTopicLanmu r0 = (com.qihoo360.daily.model.SpecialTopicLanmu) r0
            if (r1 != 0) goto L_0x0049
            java.util.List r3 = r0.getNewsList()
            if (r3 == 0) goto L_0x003a
            java.util.List r3 = r0.getNewsList()
            int r3 = r3.size()
            if (r3 != 0) goto L_0x004e
        L_0x003a:
            java.util.List r0 = r0.getNewsList()
            if (r0 == 0) goto L_0x0045
            java.util.List<com.qihoo360.daily.model.Info> r3 = r4.f891b
            r3.addAll(r0)
        L_0x0045:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x001c
        L_0x0049:
            java.util.List<com.qihoo360.daily.model.Info> r3 = r4.f891b
            r3.add(r0)
        L_0x004e:
            java.util.List r0 = r0.getNewsList()
            if (r0 == 0) goto L_0x0045
            java.util.List<com.qihoo360.daily.model.Info> r3 = r4.f891b
            r3.addAll(r0)
            goto L_0x0045
        L_0x005a:
            com.qihoo360.daily.model.Info r0 = new com.qihoo360.daily.model.Info
            r0.<init>()
            r1 = -8
            r0.setV_t(r1)
            java.lang.String r1 = r6.getKeywords()
            r0.setKeywords(r1)
            java.util.List<com.qihoo360.daily.model.Info> r1 = r4.f891b
            r1.add(r0)
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.a.ak.<init>(android.content.Context, com.qihoo360.daily.model.SpecialTopic):void");
    }

    public int getItemCount() {
        if (this.f891b != null) {
            return this.f891b.size();
        }
        return 0;
    }

    public int getItemViewType(int i) {
        if (i == 0) {
            return 1;
        }
        Info info = this.f891b.get(i);
        int v_t = info.getV_t();
        if (v_t == -8) {
            return 5;
        }
        if (info instanceof SpecialTopicLanmu) {
            return 2;
        }
        return v_t == 3 ? 3 : 4;
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        Info info = this.f891b.get(i);
        switch (viewHolder.getItemViewType()) {
            case 1:
                bp.a(this.f890a, viewHolder, info);
                return;
            case 2:
                br.a(this.f890a, viewHolder, info);
                return;
            case 3:
                bw.a(this.f890a, viewHolder, info);
                return;
            case 4:
                bt.a(this.f890a, viewHolder, info);
                return;
            case 5:
                ay.a(this.f890a, viewHolder, info);
                return;
            default:
                return;
        }
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        switch (i) {
            case 1:
                return bp.a(this.f890a);
            case 2:
                return br.a(this.f890a);
            case 3:
                return bw.a(this.f890a);
            case 4:
                return bt.a(this.f890a);
            case 5:
                return ay.a(this.f890a);
            default:
                return bb.a(this.f890a);
        }
    }
}
