package com.qihoo360.daily.a;

import android.view.View;
import android.widget.PopupWindow;
import com.qihoo360.daily.R;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.h.b;

class f implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopupWindow f899a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f900b;
    final /* synthetic */ e c;

    f(e eVar, PopupWindow popupWindow, String str) {
        this.c = eVar;
        this.f899a = popupWindow;
        this.f900b = str;
    }

    public void onClick(View view) {
        this.f899a.dismiss();
        b.c(this.c.c.f894a, this.f900b);
        ay.a(this.c.c.f894a).a((int) R.string.copy_ok);
    }
}
