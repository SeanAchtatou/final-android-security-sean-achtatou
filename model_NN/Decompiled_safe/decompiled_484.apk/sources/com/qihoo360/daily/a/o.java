package com.qihoo360.daily.a;

import android.view.View;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.g.f;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.model.NewComment;

class o implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ k f917a;

    o(k kVar) {
        this.f917a = kVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.a.k.a(com.qihoo360.daily.a.k, boolean):boolean
     arg types: [com.qihoo360.daily.a.k, int]
     candidates:
      com.qihoo360.daily.a.k.a(com.qihoo360.daily.a.k, int):int
      com.qihoo360.daily.a.k.a(com.qihoo360.daily.a.k, boolean):boolean */
    public void onClick(View view) {
        int size;
        switch (view.getId()) {
            case R.id.row_loadmore_failed:
                if (this.f917a.d != null && this.f917a.f != (size = this.f917a.f910b.size())) {
                    int unused = this.f917a.f = size;
                    this.f917a.d.onLoadMore(this.f917a.f910b.size() + "");
                    boolean unused2 = this.f917a.g = false;
                    this.f917a.notifyDataSetChanged();
                    return;
                }
                return;
            default:
                NewComment newComment = (NewComment) view.getTag();
                TextView textView = (TextView) view;
                if (newComment.getDiggok() != 1) {
                    if (this.f917a.j != null) {
                        this.f917a.j.onDiggClick();
                    }
                    newComment.setSupport(String.valueOf(Integer.valueOf(newComment.getSupport()).intValue() + 1));
                    newComment.setDiggok(1);
                    textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, (int) R.drawable.comment_plus_done, 0);
                    textView.setTextColor(this.f917a.f909a.getResources().getColor(R.color.praiseed_color));
                    textView.setText(newComment.getSupport());
                    new f(this.f917a.f909a, newComment.getComment_id(), this.f917a.k).a(null, new Object[0]);
                } else {
                    ay.a(this.f917a.f909a).a((int) R.string.comment_digged);
                }
                textView.startAnimation(this.f917a.e);
                return;
        }
    }
}
