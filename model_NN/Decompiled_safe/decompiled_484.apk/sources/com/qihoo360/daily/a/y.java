package com.qihoo360.daily.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.model.HotNews;
import java.util.List;

public class y extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f929a;

    /* renamed from: b  reason: collision with root package name */
    private List<HotNews> f930b;

    public y(Context context, List<HotNews> list) {
        this.f929a = context;
        this.f930b = list;
    }

    public int getCount() {
        if (this.f930b == null) {
            return 0;
        }
        return this.f930b.size();
    }

    public Object getItem(int i) {
        if (this.f930b == null || this.f930b.size() <= i) {
            return null;
        }
        return this.f930b.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        aa aaVar;
        if (view == null || view.getTag() == null) {
            aa aaVar2 = new aa();
            view = LayoutInflater.from(this.f929a).inflate((int) R.layout.item_hotnews, (ViewGroup) null);
            aaVar2.f877a = (TextView) view.findViewById(R.id.hotnews_title);
            view.setTag(aaVar2);
            aaVar = aaVar2;
        } else {
            aaVar = (aa) view.getTag();
        }
        aaVar.f877a.setText(this.f930b.get(i).getTitle());
        return view;
    }
}
