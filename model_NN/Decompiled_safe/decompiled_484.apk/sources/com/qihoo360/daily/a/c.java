package com.qihoo360.daily.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.model.NewComment;
import com.qihoo360.daily.widget.CmtItemLayout;
import com.qihoo360.daily.widget.LinearLayoutForRecyclerView;
import java.util.List;

public class c extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f894a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public List<NewComment> f895b;
    /* access modifiers changed from: private */
    public r c;
    /* access modifiers changed from: private */
    public boolean d = true;
    /* access modifiers changed from: private */
    public LinearLayoutForRecyclerView e;

    public c(Context context, List<NewComment> list, r rVar, LinearLayoutForRecyclerView linearLayoutForRecyclerView) {
        this.f894a = context;
        this.f895b = list;
        this.c = rVar;
        this.e = linearLayoutForRecyclerView;
        if ((list != null ? list.size() : 0) > 5) {
            this.d = false;
        }
    }

    public int getItemCount() {
        if (!this.d) {
            return 4;
        }
        if (this.f895b == null || this.f895b.size() <= 0) {
            return 0;
        }
        return this.f895b.size();
    }

    public int getItemViewType(int i) {
        return (!this.d && i == getItemCount() + -2) ? 2 : 1;
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        switch (getItemViewType(i)) {
            case 2:
                return;
            default:
                h hVar = (h) viewHolder;
                if (i >= 0 && i < this.f895b.size()) {
                    if (!this.d && i == 3) {
                        i = this.f895b.size() - 1;
                    }
                    NewComment newComment = this.f895b.get(i);
                    hVar.d = newComment;
                    hVar.f903a.setText(newComment.getUname());
                    hVar.f904b.setText((i + 1) + "");
                    hVar.c.setText(newComment.getComment());
                    hVar.e = i;
                    return;
                }
                return;
        }
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        switch (i) {
            case 2:
                View inflate = View.inflate(this.f894a, R.layout.row_chain_load_more, null);
                inflate.setOnClickListener(new d(this));
                return new h(inflate);
            default:
                CmtItemLayout cmtItemLayout = (CmtItemLayout) View.inflate(this.f894a, R.layout.row_chain_comment, null);
                h hVar = new h(cmtItemLayout);
                hVar.f903a = (TextView) cmtItemLayout.findViewById(R.id.comment_account);
                hVar.c = (TextView) cmtItemLayout.findViewById(R.id.comment_content);
                hVar.f904b = (TextView) cmtItemLayout.findViewById(R.id.floor_num);
                cmtItemLayout.setOnClickListener(new e(this, hVar, cmtItemLayout));
                return hVar;
        }
    }
}
