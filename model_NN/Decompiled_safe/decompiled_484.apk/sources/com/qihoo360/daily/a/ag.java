package com.qihoo360.daily.a;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.model.Suggest;

public class ag extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Suggest f885a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public ai f886b;
    private View.OnClickListener c = new ah(this);

    public ag(Suggest suggest) {
        this.f885a = suggest;
    }

    public void a(ai aiVar) {
        this.f886b = aiVar;
    }

    public void a(Suggest suggest) {
        this.f885a = suggest;
        notifyDataSetChanged();
    }

    public int getCount() {
        if (this.f885a == null) {
            return 0;
        }
        return this.f885a.getS().length;
    }

    public Object getItem(int i) {
        if (this.f885a == null) {
            return null;
        }
        return this.f885a.getS()[i];
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        aj ajVar;
        if (view == null || view.getTag() == null) {
            aj ajVar2 = new aj(this, null);
            view = View.inflate(viewGroup.getContext(), R.layout.row_search_suggest, null);
            ajVar2.f888a = (TextView) view.findViewById(R.id.suggest_text);
            ajVar2.f889b = view.findViewById(R.id.suggest_goto);
            view.setTag(ajVar2);
            ajVar = ajVar2;
        } else {
            ajVar = (aj) view.getTag();
        }
        ajVar.f888a.setText(this.f885a.getS()[i]);
        ajVar.f889b.setTag(this.f885a.getS()[i]);
        ajVar.f889b.setOnClickListener(this.c);
        return view;
    }
}
