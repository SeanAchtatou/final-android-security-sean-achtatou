package com.qihoo360.daily.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.model.Channel;
import java.util.ArrayList;
import java.util.List;

public class a extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f875a;

    /* renamed from: b  reason: collision with root package name */
    private List<Channel> f876b = new ArrayList();

    public a(Context context, List<Channel> list) {
        this.f875a = context;
        this.f876b = list;
    }

    public List<Channel> a() {
        return this.f876b;
    }

    public int getCount() {
        if (this.f876b != null) {
            return this.f876b.size();
        }
        return 0;
    }

    public Object getItem(int i) {
        return this.f876b.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        b bVar;
        if (view == null) {
            view = View.inflate(viewGroup.getContext(), R.layout.action_bar_item, null);
            bVar = new b(this);
            view.setTag(bVar);
            TextView unused = bVar.f893b = (TextView) view.findViewById(R.id.title);
            ImageView unused2 = bVar.c = (ImageView) view.findViewById(R.id.iv_badge);
        } else {
            bVar = (b) view.getTag();
        }
        Channel channel = this.f876b.get(i);
        if (channel != null) {
            bVar.f893b.setText(channel.getTitle());
            bVar.c.setVisibility(4);
        }
        return view;
    }
}
