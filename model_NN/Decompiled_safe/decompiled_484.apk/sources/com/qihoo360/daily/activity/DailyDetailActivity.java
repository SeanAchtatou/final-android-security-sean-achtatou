package com.qihoo360.daily.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import com.mediav.ads.sdk.adcore.Config;
import com.qihoo.messenger.util.QDefine;
import com.qihoo360.daily.R;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.f.a;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.g.ar;
import com.qihoo360.daily.g.av;
import com.qihoo360.daily.g.ba;
import com.qihoo360.daily.g.bh;
import com.qihoo360.daily.g.f;
import com.qihoo360.daily.g.s;
import com.qihoo360.daily.h.ab;
import com.qihoo360.daily.h.ad;
import com.qihoo360.daily.h.ai;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.h.bj;
import com.qihoo360.daily.h.i;
import com.qihoo360.daily.h.x;
import com.qihoo360.daily.h.y;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.NewComment;
import com.qihoo360.daily.model.NewCommentResponse;
import com.qihoo360.daily.model.NewsContent;
import com.qihoo360.daily.model.NewsDetail;
import com.qihoo360.daily.model.NewsRelated;
import com.qihoo360.daily.model.Related;
import com.qihoo360.daily.model.ResponseBean;
import com.qihoo360.daily.model.Result;
import com.qihoo360.daily.model.User;
import com.qihoo360.daily.widget.DetailVideoView;
import com.qihoo360.daily.widget.FindWebView;
import com.qihoo360.daily.widget.GestureView;
import com.qihoo360.daily.widget.TrafficDialog;
import com.qihoo360.daily.widget.webview.JavascriptInterface;
import com.qihoo360.daily.widget.webview.WebChromeClientDaily;
import com.qihoo360.daily.widget.webview.WebViewClientDaily;
import com.qihoo360.daily.wxapi.WXUser;
import java.io.File;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DailyDetailActivity extends BaseDetailActivity implements View.OnClickListener, ab, FindWebView.OnScrollChangedListener, GestureView.GestureListener, JavascriptInterface, WebViewClientDaily.WebViewClientListener {
    private static final int CMTS_REQUESTCODE = 10;
    private static final int SEND_CMT_REQUESTCODE = 9;
    private static final String TAG_CONTENT = "content";
    private final int MSG_SEND_CMT = 4;
    private final int MSG_START_VIDEO = 0;
    private final int MSG_STOP_VIDEO = 3;
    private float alpha = 1.0f;
    private float alphaLen;
    private String base_url = "file:///android_asset/index.html";
    /* access modifiers changed from: private */
    public boolean cmtAble = true;
    /* access modifiers changed from: private */
    public c<Void, Result<NewCommentResponse>> cmtOnNetRequestListener = new c<Void, Result<NewCommentResponse>>() {
        public void onNetRequest(int i, Result<NewCommentResponse> result) {
            if (result != null) {
                int status = result.getStatus();
                if (status == 0) {
                    NewCommentResponse data = result.getData();
                    if (!(data == null || DailyDetailActivity.this.mWebView == null)) {
                        DailyDetailActivity.this.mWebView.loadUrl("javascript:$ContentRender.renderComments(" + Application.getGson().a(data) + ");");
                        int unused = DailyDetailActivity.this.total = data.getComment_num();
                        DailyDetailActivity.this.mInfo.setCmt_cnt(DailyDetailActivity.this.total + "");
                        DailyDetailActivity.this.setCmtNum(DailyDetailActivity.this.total);
                        Intent intent = new Intent();
                        intent.putExtra("Info", DailyDetailActivity.this.mInfo);
                        intent.putExtra("position", DailyDetailActivity.this.position);
                        DailyDetailActivity.this.setResult(-1, intent);
                    }
                } else if (status == 110) {
                    boolean unused2 = DailyDetailActivity.this.cmtAble = false;
                } else {
                    ay.a(DailyDetailActivity.this.getApplicationContext()).a(result.getMsg());
                }
            }
            if (!(DailyDetailActivity.this.newsRelated == null || DailyDetailActivity.this.mWebView == null)) {
                ArrayList<Related> editorNewsList = DailyDetailActivity.this.newsRelated.getEditorNewsList();
                if (editorNewsList != null && editorNewsList.size() > 0) {
                    String a2 = Application.getGson().a(editorNewsList);
                    ad.a("renderEditorArticleList:" + a2);
                    DailyDetailActivity.this.mWebView.loadUrl("javascript:$ContentRender.renderEditorArticleList(" + a2 + ");");
                }
                ArrayList<Related> related = DailyDetailActivity.this.newsRelated.getRelated();
                if (related != null && related.size() > 0) {
                    String a3 = Application.getGson().a(DailyDetailActivity.this.newsRelated);
                    ad.a("renderRecommendArtices:" + a3);
                    DailyDetailActivity.this.mWebView.loadUrl("javascript:$ContentRender.renderRecommendArtices(" + a3 + ");");
                }
            }
            if (DailyDetailActivity.this.mWebView != null) {
                DailyDetailActivity.this.mWebView.loadUrl("javascript:$ContentRender.lazyLoadImage();");
                DailyDetailActivity.this.mWebView.loadUrl("javascript:$ContentRender.done()");
                DailyDetailActivity.this.mWebView.getSettings().setBlockNetworkImage(false);
            }
        }

        public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
            onNetRequest(i, (Result<NewCommentResponse>) ((Result) obj));
        }
    };
    private FrameLayout container;
    /* access modifiers changed from: private */
    public String content;
    private c<ResponseBean<NewsDetail>, ResponseBean<NewsDetail>> detailOnNetRequestListener = new c<ResponseBean<NewsDetail>, ResponseBean<NewsDetail>>() {
        public void onNetRequest(int i, ResponseBean<NewsDetail> responseBean) {
        }

        public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
            onNetRequest(i, (ResponseBean<NewsDetail>) ((ResponseBean) obj));
        }

        public void onProgressUpdate(int i, ResponseBean<NewsDetail> responseBean) {
            DailyDetailActivity.this.loading_layout.setVisibility(8);
            if (responseBean != null) {
                int errno = responseBean.getErrno();
                if (errno == 0) {
                    NewsDetail unused = DailyDetailActivity.this.mNewsDetail = responseBean.getData();
                    if (!(DailyDetailActivity.this.mNewsDetail == null || DailyDetailActivity.this.mWebView == null)) {
                        DailyDetailActivity.this.mNewsDetail.setSource(DailyDetailActivity.this.mInfo.getSrc());
                        DailyDetailActivity.this.mNewsDetail.setNewtags(DailyDetailActivity.this.mInfo.getNewtags());
                        String shorturl = DailyDetailActivity.this.mNewsDetail.getShorturl();
                        DailyDetailActivity.this.mNewsDetail.setShorturl(DailyDetailActivity.this.info_url);
                        DailyDetailActivity.this.mNewsDetail.setShorturl(shorturl);
                        String wapurl = DailyDetailActivity.this.mNewsDetail.getWapurl();
                        ArrayList<NewsContent> content = DailyDetailActivity.this.mNewsDetail.getContent();
                        if (TextUtils.isEmpty(wapurl) || (content != null && !content.isEmpty())) {
                            String json = responseBean.getJson();
                            boolean z = !"2".equals(DailyDetailActivity.this.mInfo.getN_t());
                            DailyDetailActivity.this.mWebView.loadUrl("javascript:window.$debug=false");
                            DailyDetailActivity.this.mWebView.loadUrl("javascript:$ContentRender.renderArticle(" + json + "," + z + ");");
                            DailyDetailActivity.this.mWebView.loadUrl("javascript:$ContentRender.renderShareButtons({})");
                            if (TextUtils.isEmpty(DailyDetailActivity.this.mFirstImageUrl)) {
                                DailyDetailActivity.this.mWebViewClient.setNewsDetail(DailyDetailActivity.this.mNewsDetail);
                            }
                            new com.qihoo360.daily.g.ay(DailyDetailActivity.this.getApplicationContext(), DailyDetailActivity.this.nid, DailyDetailActivity.this.info_url, DailyDetailActivity.this.v_t, 1, DailyDetailActivity.this.pager, null).a(DailyDetailActivity.this.relatedOnNetRequestListener, new Void[0]);
                        } else {
                            Intent intent = new Intent(DailyDetailActivity.this.getApplicationContext(), SearchDetailActivity.class);
                            intent.putExtra(SearchActivity.TAG_URL, wapurl);
                            DailyDetailActivity.this.startActivity(intent);
                            DailyDetailActivity.this.finish();
                            return;
                        }
                    }
                } else {
                    ay.a(DailyDetailActivity.this.getApplicationContext()).a(DailyDetailActivity.this.getString(R.string.error_code, new Object[]{Integer.valueOf(errno)}));
                    DailyDetailActivity.this.error_layout.setVisibility(0);
                }
                if (ChannelType.TYPE_PUSH.equals(DailyDetailActivity.this.from)) {
                    switch (DailyDetailActivity.this.getIntent().getIntExtra("push_click_type", 0)) {
                        case 1:
                            String unused2 = DailyDetailActivity.this.mFirstImageUrl = DailyDetailActivity.this.mWebViewClient.getFirstImgUrl();
                            new ai(DailyDetailActivity.this, DailyDetailActivity.this.mInfo, DailyDetailActivity.this.mNewsDetail, DailyDetailActivity.this.mFirstImageUrl, DailyDetailActivity.this.clickType, DailyDetailActivity.this.from).a();
                            break;
                        case 2:
                            if (!a.h(DailyDetailActivity.this.getApplicationContext())) {
                                DailyDetailActivity.this.login();
                                break;
                            } else {
                                DailyDetailActivity.this.startActivityForResult(new Intent(DailyDetailActivity.this.getApplicationContext(), SendCmtActivity.class).putExtra("Info", DailyDetailActivity.this.mInfo).putExtra("from", DailyDetailActivity.this.from).putExtra(DailyDetailActivity.TAG_CONTENT, DailyDetailActivity.this.content), 9);
                                break;
                            }
                    }
                    DailyDetailActivity.this.clickType = "0";
                    return;
                }
                return;
            }
            ay.a(DailyDetailActivity.this.getApplicationContext()).a((int) R.string.net_error);
            DailyDetailActivity.this.error_layout.setVisibility(0);
        }

        public /* bridge */ /* synthetic */ void onProgressUpdate(int i, Object obj) {
            onProgressUpdate(i, (ResponseBean<NewsDetail>) ((ResponseBean) obj));
        }
    };
    /* access modifiers changed from: private */
    public DetailVideoView detailVideoView;
    /* access modifiers changed from: private */
    public View error_layout;
    /* access modifiers changed from: private */
    public String from;
    /* access modifiers changed from: private */
    public GestureView gestureView;
    /* access modifiers changed from: private */
    public String info_url;
    /* access modifiers changed from: private */
    public boolean isCollected;
    private boolean isMusicStart;
    /* access modifiers changed from: private */
    public View loading_layout;
    private String m;
    /* access modifiers changed from: private */
    public String mFirstImageUrl;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    Bundle data = message.getData();
                    String str = (String) data.get(SearchActivity.TAG_URL);
                    String str2 = (String) data.get("title");
                    if (DailyDetailActivity.this.detailVideoView != null) {
                        DailyDetailActivity.this.detailVideoView.release();
                    }
                    DetailVideoView unused = DailyDetailActivity.this.detailVideoView = new DetailVideoView(DailyDetailActivity.this);
                    DailyDetailActivity.this.detailVideoView.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            DailyDetailActivity.this.detailVideoView.click();
                        }
                    });
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -2);
                    layoutParams.gravity = 48;
                    DailyDetailActivity.this.detailVideoView.init(str2, str, "");
                    DailyDetailActivity.this.gestureView.addView(DailyDetailActivity.this.detailVideoView, layoutParams);
                    DailyDetailActivity.this.detailVideoView.startVideo();
                    b.b(DailyDetailActivity.this, "Detail_video_play");
                    com.qihoo360.daily.music.c.a().a(DailyDetailActivity.this);
                    if (DailyDetailActivity.this.mWebView != null) {
                        DailyDetailActivity.this.mWebView.loadUrl("javascript:$ContentRender.stopMusic()");
                        return;
                    }
                    return;
                case 1:
                case 2:
                default:
                    return;
                case 3:
                    if (DailyDetailActivity.this.detailVideoView != null) {
                        DailyDetailActivity.this.detailVideoView.release();
                        return;
                    }
                    return;
                case 4:
                    if (!DailyDetailActivity.this.cmtAble) {
                        ay.a(DailyDetailActivity.this.getApplicationContext()).a((int) R.string.cmt_close);
                        return;
                    } else if (a.h(DailyDetailActivity.this.getApplicationContext())) {
                        DailyDetailActivity.this.startActivityForResult(new Intent(DailyDetailActivity.this.getApplicationContext(), SendCmtActivity.class).putExtra("Info", DailyDetailActivity.this.mInfo).putExtra("from", DailyDetailActivity.this.from).putExtra(DailyDetailActivity.TAG_CONTENT, DailyDetailActivity.this.content), 9);
                        return;
                    } else {
                        DailyDetailActivity.this.login(new x() {
                            public void onClickLogin() {
                                if (DailyDetailActivity.this.detailVideoView != null) {
                                    DailyDetailActivity.this.detailVideoView.showPlacePause();
                                }
                            }
                        });
                        return;
                    }
            }
        }
    };
    /* access modifiers changed from: private */
    public Info mInfo;
    /* access modifiers changed from: private */
    public NewsDetail mNewsDetail;
    /* access modifiers changed from: private */
    public Toolbar mToolbar;
    /* access modifiers changed from: private */
    public FindWebView mWebView;
    /* access modifiers changed from: private */
    public WebViewClientDaily mWebViewClient;
    private BroadcastReceiver musicReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            ad.a("musicReceiver action:" + action);
            if (("com.qihoo.completion".equals(action) || "com.qihoo.exception".equals(action) || "com.qihoo.stop".equals(action)) && DailyDetailActivity.this.mWebView != null) {
                DailyDetailActivity.this.mWebView.loadUrl("javascript:$ContentRender.stopMusic()");
            }
        }
    };
    /* access modifiers changed from: private */
    public NewsRelated newsRelated;
    private String newtrans;
    /* access modifiers changed from: private */
    public String nid;
    /* access modifiers changed from: private */
    public int pager;
    /* access modifiers changed from: private */
    public int position;
    private String pushId;
    /* access modifiers changed from: private */
    public c<Void, Result<NewsRelated>> relatedOnNetRequestListener = new c<Void, Result<NewsRelated>>() {
        public void onNetRequest(int i, Result<NewsRelated> result) {
            new ar(DailyDetailActivity.this.getApplicationContext(), DailyDetailActivity.this.info_url, "0").a(DailyDetailActivity.this.cmtOnNetRequestListener, new Void[0]);
            if (result == null) {
                return;
            }
            if (result.getStatus() == 0) {
                NewsRelated unused = DailyDetailActivity.this.newsRelated = result.getData();
                if (DailyDetailActivity.this.newsRelated != null && DailyDetailActivity.this.mWebView != null) {
                    if (Config.CHANNEL_ID.equals(DailyDetailActivity.this.newsRelated.getIsCollected())) {
                        boolean unused2 = DailyDetailActivity.this.isCollected = true;
                    } else {
                        boolean unused3 = DailyDetailActivity.this.isCollected = false;
                    }
                    DailyDetailActivity.this.mWebView.loadUrl("javascript:$ContentRender.renderExtend(" + result.getJson() + ");");
                    return;
                }
                return;
            }
            ay.a(DailyDetailActivity.this.getApplicationContext()).a(result.getMsg());
        }

        public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
            onNetRequest(i, (Result<NewsRelated>) ((Result) obj));
        }
    };
    /* access modifiers changed from: private */
    public int total;
    /* access modifiers changed from: private */
    public int v_t;

    /* renamed from: com.qihoo360.daily.activity.DailyDetailActivity$10  reason: invalid class name */
    /* synthetic */ class AnonymousClass10 {
        static final /* synthetic */ int[] $SwitchMap$android$webkit$WebSettings$TextSize = new int[WebSettings.TextSize.values().length];

        static {
            try {
                $SwitchMap$android$webkit$WebSettings$TextSize[WebSettings.TextSize.SMALLER.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$android$webkit$WebSettings$TextSize[WebSettings.TextSize.NORMAL.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$android$webkit$WebSettings$TextSize[WebSettings.TextSize.LARGER.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$android$webkit$WebSettings$TextSize[WebSettings.TextSize.LARGEST.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            $SwitchMap$com$qihoo360$daily$sp$Settings$FontSize = new int[com.qihoo360.daily.f.b.values().length];
            try {
                $SwitchMap$com$qihoo360$daily$sp$Settings$FontSize[com.qihoo360.daily.f.b.SMALL.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$qihoo360$daily$sp$Settings$FontSize[com.qihoo360.daily.f.b.MEDIUM.ordinal()] = 2;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$qihoo360$daily$sp$Settings$FontSize[com.qihoo360.daily.f.b.LARGE.ordinal()] = 3;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$qihoo360$daily$sp$Settings$FontSize[com.qihoo360.daily.f.b.X_LARGE.ordinal()] = 4;
            } catch (NoSuchFieldError e8) {
            }
        }
    }

    private Bitmap getCmtNumBm(Bitmap bitmap, String str) {
        if (bitmap == null) {
            return null;
        }
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.draw_text_size);
        Paint paint = new Paint();
        paint.setTextSize((float) dimensionPixelSize);
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        float measureText = paint.measureText(str);
        float f = fontMetrics.bottom - fontMetrics.top;
        return Bitmap.createScaledBitmap(bitmap, (int) Math.max(measureText + ((float) getResources().getDimensionPixelSize(R.dimen.margin_xsmall)), (float) bitmap.getWidth()), (int) (f + ((float) getResources().getDimensionPixelSize(R.dimen.margin_xxsmall))), true);
    }

    private void initToolbar() {
        this.mToolbar = (Toolbar) findViewById(R.id.toolbar);
        this.mToolbar.inflateMenu(R.menu.menu_news_detail);
        this.mToolbar.setNavigationIcon((int) R.drawable.ic_actionbar_back);
        this.mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(View view) {
                List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) DailyDetailActivity.this.getSystemService("activity")).getRunningTasks(10);
                if (runningTasks != null && runningTasks.size() > 0 && runningTasks.get(0).numActivities == 1) {
                    Intent intent = new Intent(DailyDetailActivity.this, IndexActivity.class);
                    intent.putExtra("fromDetail", true);
                    DailyDetailActivity.this.startActivity(intent);
                }
                b.b(DailyDetailActivity.this, "Bottombar_bottom_back");
                DailyDetailActivity.this.finish();
            }
        });
        this.mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_cmt:
                        if (DailyDetailActivity.this.cmtAble) {
                            Intent intent = new Intent(DailyDetailActivity.this.getApplicationContext(), CommentActivity.class);
                            intent.putExtra("Info", DailyDetailActivity.this.mInfo);
                            intent.putExtra("clickType", DailyDetailActivity.this.clickType);
                            intent.putExtra("from", DailyDetailActivity.this.from);
                            DailyDetailActivity.this.startActivityForResult(intent, 10);
                        } else {
                            ay.a(DailyDetailActivity.this.getApplicationContext()).a((int) R.string.cmt_close);
                        }
                        b.b(DailyDetailActivity.this, "Bottombar_bottom_comment");
                        break;
                    case R.id.action_share:
                        String unused = DailyDetailActivity.this.mFirstImageUrl = DailyDetailActivity.this.mWebViewClient.getFirstImgUrl();
                        new ai(DailyDetailActivity.this, DailyDetailActivity.this.mInfo, DailyDetailActivity.this.mNewsDetail, DailyDetailActivity.this.mFirstImageUrl, DailyDetailActivity.this.clickType, DailyDetailActivity.this.from).a();
                        b.b(DailyDetailActivity.this, "Bottombar_bottom_share");
                        break;
                    case R.id.action_more:
                        new y(DailyDetailActivity.this, DailyDetailActivity.this, DailyDetailActivity.this.nid, DailyDetailActivity.this.isCollected, true).a(DailyDetailActivity.this.mToolbar);
                        b.b(DailyDetailActivity.this, "Bottombar_bottom_menu");
                        break;
                }
                return true;
            }
        });
    }

    private void initViews() {
        this.gestureView = (GestureView) findViewById(R.id.gesture_view);
        this.gestureView.setGestureListener(this);
        findViewById(R.id.go_cmt).setOnClickListener(this);
        this.error_layout = findViewById(R.id.error_layout);
        this.error_layout.setOnClickListener(this);
        this.loading_layout = findViewById(R.id.loading_layout);
        this.loading_layout.setVisibility(0);
    }

    @SuppressLint({"JavascriptInterface", "SetJavaScriptEnabled"})
    @TargetApi(11)
    private void initWebView() {
        this.container = (FrameLayout) findViewById(R.id.container);
        this.mWebView = new FindWebView(this);
        this.mWebView.setOnScrollChangedListener(this);
        this.container.addView(this.mWebView);
        this.mWebView.setWebChromeClient(new WebChromeClientDaily());
        this.mWebViewClient = new WebViewClientDaily(this);
        this.mWebView.setWebViewClient(this.mWebViewClient);
        this.mWebView.addJavascriptInterface(this, "$_news");
        this.mWebView.addJavascriptInterface(this, "$_cmt");
        this.mWebView.addJavascriptInterface(this, "$_related");
        this.mWebView.addJavascriptInterface(this, "$_baike");
        this.mWebView.addJavascriptInterface(this, "$_music");
        this.mWebView.addJavascriptInterface(this, "$_photo");
        this.mWebView.addJavascriptInterface(this, "$_video");
        this.mWebView.addJavascriptInterface(this, "$_timeline");
        if (!bj.a()) {
            this.mWebView.setVerticalScrollBarEnabled(false);
        }
        WebSettings settings = this.mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setBuiltInZoomControls(true);
        if (bj.a()) {
            settings.setDisplayZoomControls(false);
        }
        settings.setSupportZoom(false);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setDomStorageEnabled(true);
        settings.setBlockNetworkImage(true);
        try {
            if (bj.a()) {
                this.mWebView.removeJavascriptInterface("searchBoxJavaBridge_");
                this.mWebView.removeJavascriptInterface("accessibility");
                this.mWebView.removeJavascriptInterface("accessibilityTraversal");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void setCmtNum(int i) {
        MenuItem findItem = this.mToolbar.getMenu().findItem(R.id.action_cmt);
        com.qihoo360.daily.c.b bVar = null;
        if (i > 0) {
            String a2 = b.a(i);
            bVar = new com.qihoo360.daily.c.b(getResources(), getCmtNumBm(BitmapFactory.decodeResource(getResources(), R.drawable.bg_action_comment_num), a2), a2);
            bVar.setBounds(0, 0, bVar.getIntrinsicWidth(), bVar.getIntrinsicHeight());
        }
        findItem.setIcon(new com.qihoo360.daily.c.a(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.ic_actionbar_comment), bVar));
    }

    private void setToolBarEnabled(boolean z) {
        if (z) {
            this.mToolbar.setNavigationIcon((int) R.drawable.ic_actionbar_back);
        } else {
            this.mToolbar.setNavigationIcon((Drawable) null);
        }
        Menu menu = this.mToolbar.getMenu();
        int size = menu.size();
        for (int i = 0; i < size; i++) {
            menu.getItem(i).setEnabled(z);
        }
    }

    /* access modifiers changed from: private */
    public void startVideo(String str, String str2) {
        Message obtainMessage = this.mHandler.obtainMessage();
        obtainMessage.what = 0;
        Bundle bundle = new Bundle();
        bundle.putString(SearchActivity.TAG_URL, str);
        bundle.putString("title", str2);
        obtainMessage.setData(bundle);
        obtainMessage.sendToTarget();
    }

    @android.webkit.JavascriptInterface
    public void OnClickBaike(String str) {
        Intent intent = new Intent(getApplicationContext(), SearchDetailActivity.class);
        intent.putExtra(SearchActivity.TAG_URL, str);
        startActivity(intent);
    }

    @android.webkit.JavascriptInterface
    public void OnClickBury() {
        ad.a("OnClickBury");
    }

    @android.webkit.JavascriptInterface
    public void OnClickCmt() {
    }

    @android.webkit.JavascriptInterface
    public void OnClickCmtLike(String str) {
        ad.a("OnClickCmtLike:" + str);
        new f(getApplicationContext(), str, this.info_url).a(null, new Object[0]);
        if (this.mInfo != null) {
            new av(Application.getInstance(), this.info_url, this.from, this.nid, this.mInfo.getA(), "b88f54f7f545e6b5", this.mInfo.getTitle(), "2", this.clickType).a(null, new Void[0]);
        }
    }

    @android.webkit.JavascriptInterface
    public void OnClickCmtMore() {
        ad.a("OnClickCmtMore");
        Intent intent = new Intent(getApplicationContext(), CommentActivity.class);
        intent.putExtra("Info", this.mInfo);
        intent.putExtra("clickType", this.clickType);
        intent.putExtra("from", this.from);
        startActivityForResult(intent, 10);
    }

    @android.webkit.JavascriptInterface
    public void OnClickDigg() {
        ad.a("OnClickDigg");
    }

    @android.webkit.JavascriptInterface
    public void OnClickEditorArticle(String str) {
        ArrayList<Related> editorNewsList;
        ad.a("OnClickEditorArticle:" + str);
        if (this.newsRelated != null && str != null && (editorNewsList = this.newsRelated.getEditorNewsList()) != null && editorNewsList.size() > 0) {
            Iterator<Related> it = editorNewsList.iterator();
            while (it.hasNext()) {
                Related next = it.next();
                if (str.equals(next.getUrl())) {
                    Intent intent = new Intent(getApplicationContext(), DailyDetailActivity.class);
                    intent.putExtra("Info", next);
                    intent.putExtra("from", ChannelType.TYPE_XIAOBIAN_RELATED);
                    startActivity(intent);
                }
            }
        }
    }

    @android.webkit.JavascriptInterface
    public void OnClickImg(String str, int i, int i2, int i3, int i4) {
        ad.a("OnClickImg: " + str);
        if (this.mNewsDetail != null) {
            ArrayList<NewsContent> content2 = this.mNewsDetail.getContent();
            ArrayList arrayList = new ArrayList();
            int size = content2.size();
            for (int i5 = 0; i5 < size; i5++) {
                NewsContent newsContent = content2.get(i5);
                String type = newsContent.getType();
                String value = newsContent.getValue();
                if ("img".equals(type)) {
                    arrayList.add(value);
                }
            }
            int i6 = 0;
            while (true) {
                if (i6 >= arrayList.size()) {
                    i6 = 0;
                    break;
                } else if (str.equals(arrayList.get(i6))) {
                    break;
                } else {
                    i6++;
                }
            }
            Intent intent = new Intent(this, ImageDetailActivity.class);
            intent.putExtra(ImageDetailActivity.IMAGE_URLS, arrayList);
            intent.putExtra("index", i6);
            startActivity(intent);
        }
    }

    @android.webkit.JavascriptInterface
    public void OnClickPhoto(String str, int i, int i2, int i3, int i4, String str2) {
        int i5;
        int i6 = 0;
        ad.a("OnClickPhoto: " + str);
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return;
        }
        if (!str2.startsWith("{") || !str2.endsWith("}")) {
            ArrayList arrayList = new ArrayList();
            if (!TextUtils.isEmpty(str2)) {
                String[] split = str2.split("\\|");
                int length = split.length;
                i5 = 0;
                while (i6 < length) {
                    String str3 = split[i6];
                    arrayList.add(str3);
                    if (str.equals(str3)) {
                        i5 = i6;
                    }
                    i6++;
                }
            } else {
                i5 = 0;
            }
            Intent intent = new Intent(this, ImageDetailActivity.class);
            intent.putExtra(ImageDetailActivity.IMAGE_URLS, arrayList);
            intent.putExtra("index", i5);
            startActivity(intent);
            return;
        }
        try {
            Info info = (Info) Application.getGson().a(str2, new com.a.a.c.a<Info>() {
            }.getType());
            String imgurl = info.getImgurl();
            if (imgurl != null && !TextUtils.isEmpty(imgurl)) {
                String[] split2 = imgurl.split("\\|");
                int length2 = split2.length;
                int i7 = 0;
                while (true) {
                    if (i7 >= length2) {
                        break;
                    } else if (str.equals(split2[i7])) {
                        i6 = i7;
                        break;
                    } else {
                        i7++;
                    }
                }
            }
            Intent intent2 = new Intent(getApplicationContext(), ImagesActivity.class);
            intent2.putExtra("Info", info);
            intent2.putExtra("p", i6);
            intent2.putExtra("from", ChannelType.TYPE_RELATED);
            startActivity(intent2);
            b.b(this, "Detail_gallery_bounce");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @android.webkit.JavascriptInterface
    public void OnClickRelated(String str) {
        ArrayList<Related> related;
        ad.a("OnClickRelated:" + str);
        if (this.newsRelated != null && str != null && (related = this.newsRelated.getRelated()) != null && related.size() > 0) {
            Iterator<Related> it = related.iterator();
            while (it.hasNext()) {
                Related next = it.next();
                if (str.equals(next.getUrl())) {
                    Intent intent = new Intent(getApplicationContext(), NewsDetailActivity.class);
                    intent.putExtra("Info", next);
                    intent.putExtra("from", ChannelType.TYPE_RELATED);
                    startActivity(intent);
                }
            }
        }
    }

    @android.webkit.JavascriptInterface
    public void OnClickShare(String str) {
        this.mFirstImageUrl = this.mWebViewClient.getFirstImgUrl();
        ai aiVar = new ai(this, this.mInfo, this.mNewsDetail, this.mFirstImageUrl, this.clickType, this.from);
        ad.a("onClickShare:" + str);
        if ("circle".equals(str)) {
            aiVar.d();
        } else if ("weixin".equals(str)) {
            aiVar.c();
        } else if ("weibo".equals(str)) {
            aiVar.b();
        }
    }

    @android.webkit.JavascriptInterface
    public void OnClickText() {
    }

    @android.webkit.JavascriptInterface
    public void OnClickVideo(final String str) {
        boolean z = true;
        if (!i.b()) {
            ay.a(this).a((int) R.string.cpu_no_support);
        }
        if (!TextUtils.isEmpty(str)) {
            try {
                str = URLDecoder.decode(str, "utf8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!i.b()) {
                ay.a(this).a((int) R.string.cpu_no_support);
            } else if (b.a((Context) this)) {
                if (d.l(this)) {
                    if (System.currentTimeMillis() - d.c(this, 0) <= QDefine.ONE_DAY) {
                        z = false;
                    }
                }
                if (z) {
                    TrafficDialog trafficDialog = new TrafficDialog(this, d.l(this));
                    trafficDialog.showDialog();
                    trafficDialog.setOnDialogViewClickListener(new TrafficDialog.OnDialogViewClickListener() {
                        private boolean isChecked = d.l(DailyDetailActivity.this);

                        public void onChooseClick(boolean z) {
                            this.isChecked = z;
                        }

                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.qihoo360.daily.f.d.c(android.content.Context, boolean):void
                         arg types: [com.qihoo360.daily.activity.DailyDetailActivity, int]
                         candidates:
                          com.qihoo360.daily.f.d.c(android.content.Context, long):long
                          com.qihoo360.daily.f.d.c(android.content.Context, java.lang.String):long
                          com.qihoo360.daily.f.d.c(android.content.Context, boolean):void */
                        public void onLeftButtonClick() {
                            d.c((Context) DailyDetailActivity.this, false);
                        }

                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.qihoo360.daily.f.d.c(android.content.Context, boolean):void
                         arg types: [com.qihoo360.daily.activity.DailyDetailActivity, boolean]
                         candidates:
                          com.qihoo360.daily.f.d.c(android.content.Context, long):long
                          com.qihoo360.daily.f.d.c(android.content.Context, java.lang.String):long
                          com.qihoo360.daily.f.d.c(android.content.Context, boolean):void */
                        public void onRightButtonClick() {
                            d.c((Context) DailyDetailActivity.this, this.isChecked);
                            DailyDetailActivity.this.startVideo(str, "");
                            if (this.isChecked) {
                                d.b(DailyDetailActivity.this, System.currentTimeMillis());
                            }
                        }
                    });
                    return;
                }
                startVideo(str, "");
            } else {
                startVideo(str, "");
            }
        }
    }

    @android.webkit.JavascriptInterface
    public void OnFlirtWithEditor(String str) {
        ad.a("OnFlirtWithEditor arg: " + str);
        new ba(getApplicationContext(), this.info_url, str).a(null, new Object[0]);
    }

    @android.webkit.JavascriptInterface
    public void OnSearchTag(String str) {
        ad.a("OnSearchTag:" + str);
        Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
        intent.putExtra(SearchActivity.TAG_SEARCH, str);
        startActivity(intent);
    }

    public void OnWeiboLoginCancel() {
    }

    public void OnWeiboLoginError(User user) {
    }

    public void OnWeiboLoginException(com.sina.weibo.sdk.c.c cVar) {
    }

    public void OnWeiboLoginSuccess(User user) {
        if (a.h(getApplicationContext())) {
            startActivityForResult(new Intent(getApplicationContext(), SendCmtActivity.class).putExtra("Info", this.mInfo).putExtra(TAG_CONTENT, this.content), 9);
        }
    }

    public void OnWeixinLoginCancel() {
    }

    public void OnWeixinLoginError(WXUser wXUser) {
    }

    public void OnWeixinLoginException(int i) {
    }

    public void OnWeixinLoginSuccess(WXUser wXUser) {
        if (a.h(getApplicationContext())) {
            startActivityForResult(new Intent(getApplicationContext(), SendCmtActivity.class).putExtra("Info", this.mInfo).putExtra(TAG_CONTENT, this.content), 9);
        }
    }

    @android.webkit.JavascriptInterface
    public void comment() {
        if (this.mHandler != null) {
            this.mHandler.sendEmptyMessage(4);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        NewComment newComment;
        super.onActivityResult(i, i2, intent);
        if (!(i2 != -1 || intent == null || (newComment = (NewComment) intent.getParcelableExtra(SendCmtActivity.TAG_DATA)) == null)) {
            this.mWebView.loadUrl("javascript:$ContentRender.prependComment(" + Application.getGson().a(newComment) + ");");
            this.total++;
            setCmtNum(this.total);
            this.mInfo.setCmt_cnt(this.total + "");
            Intent intent2 = new Intent();
            intent2.putExtra("Info", this.mInfo);
            intent2.putExtra("position", this.position);
            setResult(-1, intent2);
        }
        if (i == 9 && intent != null) {
            this.content = intent.getStringExtra(TAG_CONTENT);
        }
        if (i == 10 && intent != null) {
            this.total = intent.getIntExtra("cmt_cnt", this.total);
            setCmtNum(this.total);
            this.mInfo.setCmt_cnt(this.total + "");
            Intent intent3 = new Intent();
            intent3.putExtra("Info", this.mInfo);
            intent3.putExtra("position", this.position);
            setResult(-1, intent3);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onBackPressed() {
        List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) getSystemService("activity")).getRunningTasks(10);
        if (runningTasks != null && runningTasks.size() > 0 && runningTasks.get(0).numActivities == 1) {
            Intent intent = new Intent(this, IndexActivity.class);
            intent.putExtra("fromDetail", true);
            startActivity(intent);
        }
        super.onBackPressed();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.error_layout:
                this.error_layout.setVisibility(8);
                this.loading_layout.setVisibility(0);
                if (this.mWebView != null) {
                    this.mWebView.loadUrl(this.base_url);
                    return;
                }
                return;
            case R.id.go_cmt:
                if (a.h(getApplicationContext())) {
                    startActivityForResult(new Intent(getApplicationContext(), SendCmtActivity.class).putExtra("Info", this.mInfo).putExtra("from", this.from).putExtra(TAG_CONTENT, this.content), 9);
                    return;
                } else {
                    login();
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_detail);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.qihoo.completion");
        intentFilter.addAction("com.qihoo.exception");
        intentFilter.addAction("com.qihoo.stop");
        registerReceiver(this.musicReceiver, intentFilter);
        Intent intent = getIntent();
        this.mInfo = (Info) intent.getParcelableExtra("Info");
        this.pushId = intent.getStringExtra("PushId");
        this.position = intent.getIntExtra("position", -1);
        this.from = intent.getStringExtra("from");
        if (ChannelType.TYPE_PUSH.equals(this.from)) {
            this.clickType = Config.CHANNEL_ID;
        }
        if (ChannelType.TYPE_PUSH.equals(this.from)) {
            b.b(this, "News_push_onClick");
            NotificationManager notificationManager = (NotificationManager) getSystemService("notification");
            int intExtra = intent.getIntExtra("notificationId", -1);
            if (-1 != intExtra) {
                ad.a("cancel notificationId: " + intExtra);
                notificationManager.cancel(intExtra);
            }
        }
        if (this.mInfo != null) {
            String page_template = this.mInfo.getPage_template();
            if (TextUtils.isEmpty(page_template)) {
                page_template = "default_template";
            }
            File file = new File(bh.a(getApplicationContext(), page_template), "index.html");
            if (file.exists()) {
                this.base_url = Uri.fromFile(file).toString();
            } else {
                new bh(getApplicationContext()).a((c) null, new Void[0]);
            }
            this.info_url = this.mInfo.getUrl();
            this.m = this.mInfo.getM();
            this.v_t = this.mInfo.getV_t();
            this.nid = this.mInfo.getNid();
            this.pager = this.mInfo.getPaper();
            this.newtrans = this.mInfo.getNewtrans();
            this.mInfo.getA();
            if (Config.CHANNEL_ID.equals(this.mInfo.getNocmt())) {
                this.cmtAble = false;
            }
        }
        initViews();
        initWebView();
        initToolbar();
        this.mWebView.loadUrl(this.base_url);
    }

    public void onDestroy() {
        super.onDestroy();
        com.qihoo360.daily.music.c.a().b(getApplicationContext());
        unregisterReceiver(this.musicReceiver);
        if (this.mWebView != null) {
            this.container.removeAllViews();
            this.mWebView.stopLoading();
            this.mWebView.clearCache(false);
            this.mWebView.destroyDrawingCache();
            this.mWebView.setWebChromeClient(null);
            this.mWebView.setWebViewClient(null);
            this.mWebView.removeAllViews();
            this.mWebView.destroy();
            this.mWebView = null;
        }
    }

    public void onFavorChange(boolean z) {
        this.isCollected = z;
    }

    public void onLeftGesture() {
        if (this.cmtAble) {
            Intent intent = new Intent(getApplicationContext(), CommentActivity.class);
            intent.putExtra("Info", this.mInfo);
            intent.putExtra("clickType", this.clickType);
            intent.putExtra("from", this.from);
            startActivityForResult(intent, 10);
        }
    }

    public void onPageFinished(WebView webView, String str) {
        float height = (((float) this.mToolbar.getHeight()) / com.qihoo360.daily.h.a.a((Activity) this)) - 1.0f;
        this.mWebView.loadUrl("javascript:window.$netType=" + (b.b(Application.getInstance()) ? 1 : 0));
        this.mWebView.loadUrl("javascript:document.body.style.marginTop=\"" + height + "px\"; void 0");
        String str2 = null;
        if (this.mInfo != null) {
            str2 = this.mInfo.getPdate();
        }
        new s(getApplicationContext(), this.info_url, this.m, this.nid, this.newtrans, false, str2, this.pushId).a(this.detailOnNetRequestListener, new Void[0]);
        switch (a.b(this)) {
            case SMALL:
                onTextSizeChange(WebSettings.TextSize.SMALLER);
                return;
            case MEDIUM:
                onTextSizeChange(WebSettings.TextSize.NORMAL);
                return;
            case LARGE:
                onTextSizeChange(WebSettings.TextSize.LARGER);
                return;
            case X_LARGE:
                onTextSizeChange(WebSettings.TextSize.LARGEST);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.mWebView.pauseTimers();
        super.onPause();
        if (this.mInfo != null) {
            new av(this, this.info_url, this.from, this.nid, this.mInfo.getA(), "b88f54f7f545e6b4", this.mInfo.getTitle(), null, null).a(null, new Void[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.mWebView.resumeTimers();
        super.onResume();
        if (this.mInfo != null) {
            new av(this, this.info_url, this.from, this.nid, this.mInfo.getA(), "b88f54f7f545e6b3", this.mInfo.getTitle(), null, null).a(null, new Void[0]);
        }
    }

    public void onRightGesture() {
        finish();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public void onScroll(int i, int i2, int i3, int i4) {
        float f = (float) (i2 - i4);
        if (this.alphaLen == 0.0f) {
            this.alphaLen = (float) (getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material) * 2);
        }
        if (f < 0.0f) {
            this.alpha = ((-f) / this.alphaLen) + this.alpha;
        } else if (i2 > this.mToolbar.getHeight()) {
            this.alpha -= f / this.alphaLen;
        }
        this.alpha = Math.max(this.alpha, 0.0f);
        this.alpha = Math.min(this.alpha, 1.0f);
        if (((double) this.alpha) > 0.5d) {
            showActionbarShadow();
            if (!this.mToolbar.isEnabled()) {
                setToolBarEnabled(true);
                this.mToolbar.setEnabled(true);
            }
        } else {
            hideActionbarShadow();
            if (this.mToolbar.isEnabled()) {
                setToolBarEnabled(false);
                this.mToolbar.setEnabled(false);
            }
        }
        ViewCompat.setAlpha(this.mToolbar, this.alpha);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        com.qihoo360.daily.music.c.a().a(getApplicationContext());
        if (this.mWebView != null) {
            this.mWebView.loadUrl("javascript:$ContentRender.stopMusic()");
        }
        if (this.detailVideoView != null) {
            this.detailVideoView.release();
        }
        super.onStop();
    }

    public void onTextSizeChange(WebSettings.TextSize textSize) {
        if (this.mWebView != null) {
            switch (AnonymousClass10.$SwitchMap$android$webkit$WebSettings$TextSize[textSize.ordinal()]) {
                case 1:
                    this.mWebView.loadUrl("javascript:adjustFontSize('small');");
                    return;
                case 2:
                    this.mWebView.loadUrl("javascript:adjustFontSize('normal');");
                    return;
                case 3:
                    this.mWebView.loadUrl("javascript:adjustFontSize('big');");
                    return;
                case 4:
                    this.mWebView.loadUrl("javascript:adjustFontSize('extraBig');");
                    return;
                default:
                    return;
            }
        }
    }

    @android.webkit.JavascriptInterface
    public void play(String str) {
        if (this.mHandler != null) {
            this.mHandler.sendEmptyMessage(3);
        }
        com.qihoo360.daily.music.c.a().a(getApplicationContext(), str);
        this.isMusicStart = true;
        b.b(this, "Detail_music_play");
    }

    @android.webkit.JavascriptInterface
    public void setData(String str, String str2) {
        if ("headimg".equals(str) && !TextUtils.isEmpty(str2)) {
            this.mFirstImageUrl = str2;
            this.mWebViewClient.setTopImageUrl(this.mFirstImageUrl);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.h.b.a(android.app.Activity, java.lang.String):boolean
     arg types: [com.qihoo360.daily.activity.DailyDetailActivity, java.lang.String]
     candidates:
      com.qihoo360.daily.h.b.a(android.content.Context, float):int
      com.qihoo360.daily.h.b.a(java.lang.String, java.text.SimpleDateFormat):long
      com.qihoo360.daily.h.b.a(java.lang.String, java.lang.String):java.lang.String
      com.qihoo360.daily.h.b.a(android.content.Context, java.lang.String):java.util.List<com.qihoo360.daily.model.Info>
      com.qihoo360.daily.h.b.a(long, long):boolean
      com.qihoo360.daily.h.b.a(android.app.Activity, java.lang.String):boolean */
    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        try {
            String decode = URLDecoder.decode(str, "utf8");
            ad.a(decode);
            if (TextUtils.isEmpty(decode) || decode.startsWith("qkw:") || b.a((Activity) this, decode)) {
                return true;
            }
            Intent intent = new Intent(getApplicationContext(), SearchDetailActivity.class);
            if (this.mNewsDetail != null && decode.equals(this.mNewsDetail.getShorturl())) {
                decode = this.mInfo.getUrl();
            }
            intent.putExtra(SearchActivity.TAG_URL, decode);
            startActivity(intent);
            if (decode.startsWith("http://baike.so.com/") || decode.startsWith("http://baike.haosou.com/")) {
                ad.a("百科跳出打点");
                b.b(this, "Detail_wiki_bounce");
                return true;
            } else if (decode.startsWith("http://weibo.com/")) {
                ad.a("微博跳出打点");
                b.b(this, "Detail_weibo_bounce");
                return true;
            } else {
                ad.a("APP外链");
                b.b(this, "Detail_outer_link_bounce");
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }

    @android.webkit.JavascriptInterface
    public void stop() {
        this.isMusicStart = false;
        com.qihoo360.daily.music.c.a().a(getApplicationContext());
    }
}
