package com.qihoo360.daily.activity;

import android.app.ActivityManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.f.a.g;
import com.qihoo360.daily.R;
import com.qihoo360.daily.c.m;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.g.ab;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.QdData;
import com.qihoo360.daily.model.Splash;
import java.lang.ref.WeakReference;
import java.util.List;

public class SplashActivity extends BaseNoFragmentActivity implements View.OnClickListener {
    public static final String NEWS_SUGGEST = "news_sugggest";
    public static final int SPLASH_TYPE_AD = 1;
    public static final int SPLASH_TYPE_AD_TIME = 4000;
    public static final int SPLASH_TYPE_AD_WITH_LINK = -1;
    public static final int SPLASH_TYPE_DEFAULT = 0;
    public static final long SPLASH_TYPE_DEFAULT_TIME = 1500;
    public static final int SPLASH_TYPE_OP = 2;
    public static final int SPLASH_TYPE_OP_TIME = 1500;
    private Handler mHandler;
    private ImageView mIvSplash;
    private String mJumpUrl;
    private String mNewsSuggest;
    private long mSplashTime = SPLASH_TYPE_DEFAULT_TIME;
    private int mSplashType = 0;
    private TextView mTvSkip;
    private String mzClickUrl;
    private String mzShowUrl;

    class StaticHandler extends Handler {
        final WeakReference<SplashActivity> outer;

        StaticHandler(WeakReference<SplashActivity> weakReference) {
            this.outer = weakReference;
        }

        public void handleMessage(Message message) {
            try {
                SplashActivity splashActivity = this.outer.get();
                if (splashActivity != null) {
                    splashActivity.go2IndexActivity();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public void go2IndexActivity() {
        getWindow().setFlags(2048, 2048);
        Intent intent = new Intent(this, IndexActivity.class);
        intent.putExtra(NEWS_SUGGEST, this.mNewsSuggest);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.alpha_in_splash, R.anim.alpha_out_splash);
    }

    private void initViews() {
        List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) getSystemService("activity")).getRunningTasks(10);
        if (runningTasks == null || runningTasks.size() <= 0 || runningTasks.get(0).numRunning <= 1) {
            this.mHandler = new StaticHandler(new WeakReference(this));
            this.mTvSkip = (TextView) findViewById(R.id.tv_skip);
            this.mIvSplash = (ImageView) findViewById(R.id.iv_splash_kandian);
            new ab(Application.getInstance()).a((c) null, new Void[0]);
            QdData r = d.r(Application.getInstance());
            if (r != null) {
                if (r.getNews_suggest() != null) {
                    this.mNewsSuggest = r.getNews_suggest().getName();
                }
                Splash qd = r.getQd();
                if (qd != null) {
                    this.mzClickUrl = qd.getMzclick_url();
                    this.mzShowUrl = qd.getMzshow_url();
                }
                if (qd == null || qd.getQd_type() == 0) {
                    this.mIvSplash.setImageResource(R.drawable.splash_default);
                } else {
                    if (qd.getQd_type() == 1) {
                        this.mJumpUrl = qd.getUrl();
                        this.mSplashTime = 4000;
                        if (TextUtils.isEmpty(this.mJumpUrl)) {
                            this.mSplashType = 1;
                        } else {
                            this.mSplashType = -1;
                        }
                    } else {
                        this.mSplashTime = SPLASH_TYPE_DEFAULT_TIME;
                        this.mSplashType = 2;
                    }
                    Bitmap a2 = m.a(qd.getImgurl(), 0);
                    if (a2 != null) {
                        this.mIvSplash.setImageBitmap(a2);
                        if (qd.getQd_type() == 1) {
                            this.mTvSkip.setVisibility(0);
                            this.mTvSkip.setOnClickListener(this);
                            this.mIvSplash.setOnClickListener(this);
                        }
                    } else {
                        this.mIvSplash.setImageResource(R.drawable.splash_default);
                        this.mSplashTime = SPLASH_TYPE_DEFAULT_TIME;
                        this.mSplashType = 0;
                    }
                }
            } else {
                this.mIvSplash.setImageResource(R.drawable.splash_default);
            }
            showLogStat();
            if (this.mHandler != null) {
                this.mHandler.sendEmptyMessageDelayed(0, this.mSplashTime);
                return;
            }
            return;
        }
        finish();
    }

    private void showLogStat() {
        switch (this.mSplashType) {
            case -1:
                b.b(Application.getInstance(), "kandian_to_splash_show4");
                if (!TextUtils.isEmpty(this.mzClickUrl)) {
                    b.d(this, this.mzClickUrl);
                }
                if (!TextUtils.isEmpty(this.mzShowUrl)) {
                    b.d(this, this.mzShowUrl);
                    return;
                }
                return;
            case 0:
                b.b(Application.getInstance(), "kandian_to_splash_show1");
                return;
            case 1:
                b.b(Application.getInstance(), "kandian_to_splash_show3");
                if (!TextUtils.isEmpty(this.mzClickUrl)) {
                    b.d(this, this.mzClickUrl);
                }
                if (!TextUtils.isEmpty(this.mzShowUrl)) {
                    b.d(this, this.mzShowUrl);
                    return;
                }
                return;
            case 2:
                b.b(Application.getInstance(), "kandian_to_splash_show2");
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public boolean hasActionbar() {
        return false;
    }

    public void onBackPressed() {
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_splash_kandian:
                if (this.mSplashType == -1) {
                    if (this.mHandler != null) {
                        this.mHandler.removeCallbacksAndMessages(null);
                        this.mHandler = null;
                    }
                    b.b(Application.getInstance(), "kandian_to_splash_onclick");
                    if (!TextUtils.isEmpty(this.mzClickUrl)) {
                        b.d(this, this.mzClickUrl);
                    }
                    getWindow().setFlags(2048, 2048);
                    Intent intent = new Intent(this, SearchDetailActivity.class);
                    intent.putExtra(SearchActivity.TAG_URL, this.mJumpUrl);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.alpha_in_splash, R.anim.alpha_out_splash);
                    return;
                }
                return;
            case R.id.tv_skip:
                if (this.mHandler != null) {
                    this.mHandler.removeCallbacksAndMessages(null);
                    this.mHandler = null;
                }
                b.b(Application.getInstance(), "kandian_to_splash_cancel");
                go2IndexActivity();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        sethasStatusBar(false);
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_splash);
        g.c(Application.getInstance());
        g.a(false);
        g.b(false);
        initViews();
        if (a.g(this)) {
            b.e(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.mHandler != null) {
            this.mHandler.removeCallbacksAndMessages(null);
            this.mHandler = null;
        }
    }
}
