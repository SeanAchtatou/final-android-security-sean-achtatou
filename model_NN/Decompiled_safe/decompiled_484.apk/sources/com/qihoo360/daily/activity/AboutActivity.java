package com.qihoo360.daily.activity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.f.c;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.widget.MaterialCheckBox;

public class AboutActivity extends BaseNoFragmentActivity implements View.OnClickListener {
    private MaterialCheckBox mJoinSwitch = null;

    private void initActionBar() {
        configToolbar(0, R.string.settings_about_title, R.drawable.ic_actionbar_back).setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AboutActivity.this.onBackPressed();
            }
        });
    }

    private void initViews() {
        initActionBar();
        ((TextView) findViewById(R.id.about_version_name)).setText(getString(R.string.about_version_name, new Object[]{a.b((Context) this)}));
        ViewGroup viewGroup = (ViewGroup) findViewById(R.id.about_item_declare);
        ((TextView) viewGroup.findViewById(R.id.settings_item_title)).setText((int) R.string.settings_about_declare);
        viewGroup.setOnClickListener(this);
        ViewGroup viewGroup2 = (ViewGroup) findViewById(R.id.about_item_permit);
        ((TextView) viewGroup2.findViewById(R.id.settings_item_title)).setText((int) R.string.settings_about_permit);
        viewGroup2.setOnClickListener(this);
        ViewGroup viewGroup3 = (ViewGroup) findViewById(R.id.about_item_improve);
        ((TextView) viewGroup3.findViewById(R.id.settings_item_title)).setText((int) R.string.settings_about_improve);
        viewGroup3.setOnClickListener(this);
        ViewGroup viewGroup4 = (ViewGroup) findViewById(R.id.about_item_improve_join);
        ((TextView) viewGroup4.findViewById(R.id.settings_item_title)).setText((int) R.string.settings_about_improve_join);
        viewGroup4.setOnClickListener(this);
        this.mJoinSwitch = (MaterialCheckBox) viewGroup4.findViewById(R.id.settings_checkbox);
        this.mJoinSwitch.setChecked(com.qihoo360.daily.f.a.g(this), true);
        this.mJoinSwitch.setOncheckListener(new MaterialCheckBox.OnCheckListener() {
            public void onCheck(boolean z, boolean z2) {
                com.qihoo360.daily.f.a.a(AboutActivity.this, c.IMPROVE_JOIN, z);
            }
        });
    }

    private void openUrl(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(str));
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void toggleSetting(c cVar, MaterialCheckBox materialCheckBox) {
        materialCheckBox.setChecked(!this.mJoinSwitch.isChecked());
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.about_item_declare:
                openUrl("http://sh.qihoo.com/newsreader/kandian_privacy.html");
                return;
            case R.id.about_item_permit:
                openUrl("http://sh.qihoo.com/newsreader/kandian_license.html");
                return;
            case R.id.about_item_improve:
                openUrl("http://sh.qihoo.com/newsreader/kandian_experience.html");
                return;
            case R.id.about_item_improve_join:
                toggleSetting(c.IMPROVE_JOIN, this.mJoinSwitch);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_about);
        initViews();
    }
}
