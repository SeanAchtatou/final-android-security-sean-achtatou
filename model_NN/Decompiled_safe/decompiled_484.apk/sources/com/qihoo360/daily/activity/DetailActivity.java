package com.qihoo360.daily.activity;

import com.qihoo360.daily.model.Info;

public abstract class DetailActivity extends BaseDetailActivity {
    public abstract Info getInfo();

    public abstract int getPosition();
}
