package com.qihoo360.daily.activity;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.Time;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.f.a.g;
import com.qihoo360.daily.R;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.ad;
import com.qihoo360.daily.h.bg;
import com.qihoo360.daily.h.bh;
import com.qihoo360.daily.model.UpdateInfo;
import java.util.TimeZone;

public class BaseActivity extends AppCompatActivity implements Toolbar.OnMenuItemClickListener, bh {
    private static final String D_TIME_KEY = "D_TIME";
    private static final String SAVE_TIME_KEY = "SAVE_TIME";
    private static final long TIME_LIMIT = 30000;
    private boolean hasStatusBar = true;
    private long lastDTime;
    private long resumeTimeMillis;
    private ImageView shadow;
    private int statusBarColorId = R.color.colorPrimaryDark;

    private void checkUpgrade() {
        String str = "";
        if (this instanceof IndexActivity) {
            str = "index";
        }
        Time time = new Time(TimeZone.getDefault().getID());
        time.setToNow();
        int i = time.hour + (time.yearDay * 100);
        ad.a(str + " currHour: " + i);
        int j = d.j(getApplicationContext(), str);
        ad.a(str + " checkUpdateHour: " + j);
        if (j != i) {
            d.c(getApplicationContext(), str, i);
            bg bgVar = new bg(this);
            bgVar.a(this);
            bgVar.a();
        }
    }

    private boolean isUploadRunTime() {
        return Math.abs(System.currentTimeMillis() - d.c(getApplicationContext(), SAVE_TIME_KEY)) >= TIME_LIMIT;
    }

    @TargetApi(19)
    private void setTranslucentStatus(boolean z) {
        Window window = getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        if (z) {
            attributes.flags |= 67108864;
        } else {
            attributes.flags &= -67108865;
        }
        window.setAttributes(attributes);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, long):void
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.qihoo360.daily.f.d.a(android.content.Context, long, java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, int):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, boolean):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String[], java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, long):void */
    private void uploadRunTime() {
        d.a(getApplicationContext(), D_TIME_KEY, 0L);
        d.a(getApplicationContext(), SAVE_TIME_KEY, 0L);
        ad.a("uploadRunTime lastDTime: " + this.lastDTime);
        if (this.lastDTime / 1000 > 0) {
            new com.qihoo360.daily.g.bg(getApplicationContext(), this.lastDTime / 1000).a(null, new Void[0]);
        }
        this.resumeTimeMillis = System.currentTimeMillis();
        this.lastDTime = 0;
    }

    public void checkOver(UpdateInfo updateInfo) {
    }

    public Toolbar configToolbar(int i, int i2, int i3) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (i != 0) {
            toolbar.inflateMenu(i);
        }
        if (i2 != 0) {
            toolbar.setTitle(i2);
        }
        if (i3 != 0) {
            toolbar.setNavigationIcon(i3);
        }
        toolbar.setOnMenuItemClickListener(this);
        return toolbar;
    }

    /* access modifiers changed from: protected */
    public void exitToUploadRunTime() {
        long currentTimeMillis = System.currentTimeMillis();
        this.lastDTime = (currentTimeMillis - this.resumeTimeMillis) + this.lastDTime;
        ad.a("onPause lastDTime: " + this.lastDTime);
        d.a(getApplicationContext(), D_TIME_KEY, this.lastDTime);
        d.a(getApplicationContext(), SAVE_TIME_KEY, currentTimeMillis);
        uploadRunTime();
    }

    /* access modifiers changed from: protected */
    public boolean hasActionbar() {
        return true;
    }

    public void hideActionbarShadow() {
        if (this.shadow != null) {
            this.shadow.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStatusBarColor(this.statusBarColorId);
        if (hasActionbar()) {
            ImageView imageView = new ImageView(this);
            imageView.setBackgroundResource(R.drawable.bg_shadow);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -2);
            layoutParams.topMargin = getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_height_material) + a.l(this);
            imageView.setLayoutParams(layoutParams);
            ((ViewGroup) getWindow().getDecorView()).addView(imageView);
            this.shadow = imageView;
        }
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        long currentTimeMillis = System.currentTimeMillis();
        this.lastDTime = (currentTimeMillis - this.resumeTimeMillis) + this.lastDTime;
        ad.a("onPause lastDTime: " + this.lastDTime);
        d.a(getApplicationContext(), D_TIME_KEY, this.lastDTime);
        d.a(getApplicationContext(), SAVE_TIME_KEY, currentTimeMillis);
        g.a(this);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        setStatusBarColor(this.statusBarColorId);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        checkUpgrade();
        this.resumeTimeMillis = System.currentTimeMillis();
        this.lastDTime = d.c(getApplicationContext(), D_TIME_KEY);
        if (isUploadRunTime()) {
            uploadRunTime();
        }
        g.b(this);
    }

    /* access modifiers changed from: protected */
    public void setStatusBarColor(int i) {
        if (Build.VERSION.SDK_INT == 19 && this.hasStatusBar) {
            this.statusBarColorId = i;
            setTranslucentStatus(true);
            com.d.a.a aVar = new com.d.a.a(this);
            aVar.a(true);
            aVar.a(i);
        }
    }

    public void sethasStatusBar(boolean z) {
        this.hasStatusBar = z;
    }

    public void showActionbarShadow() {
        if (this.shadow != null) {
            this.shadow.setVisibility(0);
        }
    }
}
