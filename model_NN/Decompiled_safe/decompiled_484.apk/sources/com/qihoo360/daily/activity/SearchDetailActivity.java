package com.qihoo360.daily.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.DownloadListener;
import android.webkit.SslErrorHandler;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebIconDatabase;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import com.mediav.ads.sdk.adcore.Config;
import com.qihoo360.daily.R;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.g.av;
import com.qihoo360.daily.g.h;
import com.qihoo360.daily.g.k;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.ad;
import com.qihoo360.daily.h.ai;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.h.bj;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.NewsRelated;
import com.qihoo360.daily.model.Result;
import com.qihoo360.daily.widget.FindWebView;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SuppressLint({"DefaultLocale"})
public class SearchDetailActivity extends BaseNoFragmentActivity implements FindWebView.OnScrollChangedListener {
    private static final String BLANK_URL = "about:blank";
    private float alpha = 1.0f;
    private float alphaLen;
    private String from;
    /* access modifiers changed from: private */
    public boolean isCollected;
    /* access modifiers changed from: private */
    public Info mInfo;
    /* access modifiers changed from: private */
    public long mLastLoadTime;
    /* access modifiers changed from: private */
    public String mLastUrl;
    /* access modifiers changed from: private */
    public boolean mLoadError;
    /* access modifiers changed from: private */
    public Toolbar mToolbar;
    /* access modifiers changed from: private */
    public boolean mUrlLoading;
    private ViewGroup mViewGroup;
    /* access modifiers changed from: private */
    public ProgressBar mWebLoadingProgressBar;
    /* access modifiers changed from: private */
    public View mWebNetError;
    /* access modifiers changed from: private */
    public FindWebView mWebView;
    /* access modifiers changed from: private */
    public Set<String> redirectUrls = new HashSet();
    private c<Void, Result<NewsRelated>> relatedOnNetRequestListener = new c<Void, Result<NewsRelated>>() {
        public void onNetRequest(int i, Result<NewsRelated> result) {
            if (result == null) {
                return;
            }
            if (result.getStatus() == 0) {
                NewsRelated data = result.getData();
                if (data != null) {
                    if (Config.CHANNEL_ID.equals(data.getIsCollected())) {
                        boolean unused = SearchDetailActivity.this.isCollected = true;
                    } else {
                        boolean unused2 = SearchDetailActivity.this.isCollected = false;
                    }
                    SearchDetailActivity.this.updateFavorBtn();
                    return;
                }
                return;
            }
            ay.a(SearchDetailActivity.this.getApplicationContext()).a(result.getMsg());
        }

        public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
            onNetRequest(i, (Result<NewsRelated>) ((Result) obj));
        }
    };

    class MyWebChromeClient extends WebChromeClient {
        private MyWebChromeClient() {
        }

        public void onProgressChanged(WebView webView, int i) {
            super.onProgressChanged(webView, i);
            SearchDetailActivity.this.mWebLoadingProgressBar.setProgress(i);
            if (i >= 100 && SearchDetailActivity.this.mWebLoadingProgressBar.getVisibility() == 0) {
                SearchDetailActivity.this.mWebLoadingProgressBar.setVisibility(8);
            }
        }
    }

    class MyWebViewClient extends WebViewClient {
        private MyWebViewClient() {
        }

        public void onLoadResource(WebView webView, String str) {
            super.onLoadResource(webView, str);
        }

        public void onPageFinished(WebView webView, String str) {
            SearchDetailActivity.this.mWebView.loadUrl("javascript:document.body.style.marginTop=\"" + ((((float) SearchDetailActivity.this.mToolbar.getHeight()) / a.a((Activity) SearchDetailActivity.this)) - 1.0f) + "px\"; void 0");
            Application.getInstance().onPageLoadFinished(str);
            ad.a("onPageFinished: " + str);
            super.onPageFinished(webView, str);
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            super.onPageStarted(webView, str, bitmap);
            ad.a("onPageStarted: " + str);
            if (SearchDetailActivity.this.mWebNetError.getVisibility() == 0 && !SearchDetailActivity.this.mLoadError) {
                SearchDetailActivity.this.mWebNetError.setVisibility(8);
            }
        }

        @SuppressLint({"DefaultLocale"})
        public void onReceivedError(WebView webView, int i, String str, String str2) {
            ad.a("onReceivedError:" + str2);
            Application.getInstance().onPageLoadFailed(str2);
            if (str2.toLowerCase().startsWith("http") || str2.toLowerCase().startsWith("https") || str2.toLowerCase().startsWith("file")) {
                boolean unused = SearchDetailActivity.this.mLoadError = true;
                super.onReceivedError(webView, i, str, str2);
                webView.stopLoading();
                webView.clearView();
                webView.loadUrl(SearchDetailActivity.BLANK_URL);
                if (SearchDetailActivity.this.mWebNetError.getVisibility() == 8) {
                    SearchDetailActivity.this.mWebNetError.setVisibility(0);
                }
            }
        }

        public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
            sslErrorHandler.proceed();
        }

        @SuppressLint({"DefaultLocale"})
        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            ad.a("shouldOverrideUrlLoading mLastUrl:" + SearchDetailActivity.this.mLastUrl);
            ad.a("shouldOverrideUrlLoading:" + str);
            if (str.toLowerCase().startsWith("http") || str.toLowerCase().startsWith("https") || str.toLowerCase().startsWith("file")) {
                long currentTimeMillis = System.currentTimeMillis();
                long access$900 = currentTimeMillis - SearchDetailActivity.this.mLastLoadTime;
                if (access$900 < 2000 && !SearchDetailActivity.this.redirectUrls.contains(SearchDetailActivity.this.mLastUrl)) {
                    SearchDetailActivity.this.redirectUrls.add(SearchDetailActivity.this.mLastUrl);
                    ad.a("shouldOverrideUrlLoading redirectUrls add :" + SearchDetailActivity.this.mLastUrl);
                }
                ad.a("shouldOverrideUrlLoading dTime:" + access$900);
                String unused = SearchDetailActivity.this.mLastUrl = str;
                long unused2 = SearchDetailActivity.this.mLastLoadTime = currentTimeMillis;
                SearchDetailActivity.this.mWebView.loadUrl(str);
                return true;
            }
            try {
                SearchDetailActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return true;
            }
        }
    }

    private int getBackStep(WebBackForwardList webBackForwardList) {
        if (webBackForwardList == null || webBackForwardList.getSize() == 0) {
            return 0;
        }
        int i = -1;
        int currentIndex = webBackForwardList.getCurrentIndex() - 1;
        while (true) {
            if (currentIndex < 0) {
                break;
            }
            String url = webBackForwardList.getItemAtIndex(currentIndex).getUrl();
            if (!this.redirectUrls.contains(url)) {
                ad.a("redirectUrls not contains :" + url);
                break;
            }
            ad.a("redirectUrls.contains :" + url);
            currentIndex--;
            i--;
        }
        ad.a("getBackStep:" + i);
        return i;
    }

    private void initToolbar() {
        int i = R.menu.menu_webpage;
        if (this.mInfo != null) {
            i = R.menu.menu_webpage_outlink;
        }
        this.mToolbar = configToolbar(i, 0, R.drawable.ic_actionbar_back);
        this.mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SearchDetailActivity.this.finish();
            }
        });
    }

    private void onNewNetAction() {
        this.mLoadError = false;
        if (this.mWebNetError.getVisibility() == 0) {
            this.mWebNetError.setVisibility(8);
        }
    }

    private void setToolBarEnabled(boolean z) {
        if (z) {
            this.mToolbar.setNavigationIcon((int) R.drawable.ic_actionbar_back);
        } else {
            this.mToolbar.setNavigationIcon((Drawable) null);
        }
        Menu menu = this.mToolbar.getMenu();
        int size = menu.size();
        for (int i = 0; i < size; i++) {
            menu.getItem(i).setEnabled(z);
        }
    }

    /* access modifiers changed from: private */
    public void updateFavorBtn() {
        MenuItem findItem = this.mToolbar.getMenu().findItem(R.id.action_favor);
        if (findItem != null) {
            if (this.isCollected) {
                findItem.setIcon((int) R.drawable.ic_actionbar_yishoucang);
            } else {
                findItem.setIcon((int) R.drawable.ic_actionbar_shoucang);
            }
        }
    }

    public void finish() {
        List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) getSystemService("activity")).getRunningTasks(10);
        if (runningTasks != null && runningTasks.size() > 0) {
            if (runningTasks.get(0).numRunning <= 1) {
                startActivity(new Intent(this, IndexActivity.class));
            }
            super.finish();
        }
    }

    public void onBackPressed() {
        if (this.mWebNetError.getVisibility() == 0) {
            this.mWebNetError.setVisibility(8);
        }
        WebBackForwardList copyBackForwardList = this.mWebView.copyBackForwardList();
        if (copyBackForwardList == null || copyBackForwardList.getSize() == 0) {
            finish();
            return;
        }
        int backStep = getBackStep(copyBackForwardList);
        int currentIndex = copyBackForwardList.getCurrentIndex();
        ad.a("currentIndex:" + currentIndex);
        ad.a("step:" + backStep);
        if ((-backStep) > currentIndex) {
            finish();
        } else {
            this.mWebView.goBackOrForward(backStep);
        }
    }

    /* access modifiers changed from: protected */
    @TargetApi(11)
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_search_detail);
        initToolbar();
        this.mViewGroup = (ViewGroup) findViewById(R.id.search_detail_container);
        this.mWebNetError = findViewById(R.id.web_net_error);
        this.mWebView = new FindWebView(this);
        this.mWebView.setOnScrollChangedListener(this);
        this.mWebLoadingProgressBar = (ProgressBar) findViewById(R.id.web_load_progress);
        this.mViewGroup.addView(this.mWebView, 0, new ViewGroup.LayoutParams(-1, -1));
        this.mWebView.setWebViewClient(new MyWebViewClient());
        this.mWebView.setWebChromeClient(new MyWebChromeClient());
        try {
            if (bj.a()) {
                this.mWebView.removeJavascriptInterface("searchBoxJavaBridge_");
                this.mWebView.removeJavascriptInterface("accessibility");
                this.mWebView.removeJavascriptInterface("accessibilityTraversal");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String stringExtra = getIntent().getStringExtra(SearchActivity.TAG_URL);
        this.mLastUrl = stringExtra;
        this.mLastLoadTime = System.currentTimeMillis();
        this.mWebView.loadUrl(stringExtra);
        this.mInfo = (Info) getIntent().getParcelableExtra("Info");
        if (this.mInfo != null) {
            int v_t = this.mInfo.getV_t();
            new com.qihoo360.daily.g.ay(getApplicationContext(), this.mInfo.getNid(), stringExtra, v_t, 1, this.mInfo.getPaper(), null).a(this.relatedOnNetRequestListener, new Void[0]);
            this.from = getIntent().getStringExtra("from");
        }
        WebSettings settings = this.mWebView.getSettings();
        settings.setUserAgentString(settings.getUserAgentString() + " " + getPackageName());
        ad.a("ua: " + settings.getUserAgentString());
        settings.setJavaScriptEnabled(true);
        settings.setBuiltInZoomControls(true);
        if (bj.a()) {
            settings.setDisplayZoomControls(false);
        }
        settings.setSupportZoom(false);
        if (bj.a()) {
            this.mWebView.setLayerType(1, null);
        }
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setDomStorageEnabled(true);
        try {
            WebIconDatabase.getInstance().open(getDir("icons", 0).getPath());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        this.mWebView.setDownloadListener(new DownloadListener() {
            @TargetApi(11)
            public void onDownloadStart(String str, String str2, String str3, String str4, long j) {
                String str5;
                if (bj.a()) {
                    DownloadManager downloadManager = (DownloadManager) SearchDetailActivity.this.getSystemService("download");
                    try {
                        str = URLDecoder.decode(str, "utf-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    try {
                        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(str));
                        request.setNotificationVisibility(1);
                        request.setMimeType(str4);
                        request.allowScanningByMediaScanner();
                        try {
                            str5 = str.substring(str.lastIndexOf("."));
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            str5 = "";
                        }
                        String str6 = b.b() + "download/";
                        new File(str6).mkdirs();
                        request.setDestinationUri(Uri.fromFile(new File(str6 + (b.b(str) + str5))));
                        downloadManager.enqueue(request);
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                }
            }
        });
    }

    public void onDestroy() {
        Application.getInstance().onPageClose(getIntent().getStringExtra(SearchActivity.TAG_URL));
        super.onDestroy();
        if (this.mWebView != null) {
            try {
                WebIconDatabase.getInstance().close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.mViewGroup.removeAllViews();
            this.mWebView.stopLoading();
            this.mWebView.clearCache(false);
            this.mWebView.destroyDrawingCache();
            this.mWebView.setWebChromeClient(null);
            this.mWebView.setWebViewClient(null);
            this.mWebView.removeAllViews();
            this.mWebView.destroy();
            this.mWebView = null;
        }
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        k kVar;
        boolean z;
        switch (menuItem.getItemId()) {
            case R.id.action_share:
                if (this.mWebView != null) {
                    new ai(this, this.mWebView.getTitle(), this.mWebView.getTitle(), this.mWebView.getUrl(), this.mWebView.getFavicon(), "0", this.from).a();
                    break;
                }
                break;
            case R.id.action_refresh:
                if (!this.mUrlLoading) {
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            boolean unused = SearchDetailActivity.this.mUrlLoading = false;
                        }
                    }, 200);
                    this.mUrlLoading = true;
                    onNewNetAction();
                    if (this.mWebView != null) {
                        this.mWebLoadingProgressBar.setVisibility(0);
                        this.mWebView.reload();
                        break;
                    }
                }
                break;
            case R.id.action_favor:
                if (this.mInfo != null) {
                    k kVar2 = k.ADD;
                    if (this.isCollected) {
                        kVar = k.DEL;
                        z = true;
                    } else {
                        kVar = kVar2;
                        z = false;
                    }
                    new h(kVar, this.mInfo.getNid()).a(new c<Void, Result<Object>>() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.qihoo360.daily.f.d.c(android.content.Context, java.lang.String, boolean):void
                         arg types: [com.qihoo360.daily.activity.SearchDetailActivity, java.lang.String, boolean]
                         candidates:
                          com.qihoo360.daily.f.d.c(android.content.Context, java.lang.String, java.lang.String):java.lang.String
                          com.qihoo360.daily.f.d.c(android.content.Context, java.lang.String, int):void
                          com.qihoo360.daily.f.d.c(android.content.Context, java.lang.String, long):void
                          com.qihoo360.daily.f.d.c(android.content.Context, java.lang.String, boolean):void */
                        public void onNetRequest(int i, Result<Object> result) {
                            int i2;
                            boolean z = true;
                            if (result != null) {
                                int status = result.getStatus();
                                if (status == 0) {
                                    switch (i) {
                                        case 0:
                                            boolean unused = SearchDetailActivity.this.isCollected = true;
                                            i2 = R.string.favor_ok;
                                            break;
                                        default:
                                            boolean unused2 = SearchDetailActivity.this.isCollected = false;
                                            i2 = R.string.favor_cancel;
                                            break;
                                    }
                                    SearchDetailActivity.this.updateFavorBtn();
                                    SearchDetailActivity searchDetailActivity = SearchDetailActivity.this;
                                    String nid = SearchDetailActivity.this.mInfo.getNid();
                                    if (SearchDetailActivity.this.isCollected) {
                                        z = false;
                                    }
                                    d.c((Context) searchDetailActivity, nid, z);
                                    ay.a(SearchDetailActivity.this).a(i2);
                                } else if (status == 103) {
                                    ay.a(SearchDetailActivity.this).a((int) R.string.login_dialog_title_relogin);
                                } else {
                                    ay.a(SearchDetailActivity.this).a(result.getMsg());
                                }
                            } else {
                                ay.a(SearchDetailActivity.this).a((int) R.string.net_error);
                            }
                        }

                        public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
                            onNetRequest(i, (Result<Object>) ((Result) obj));
                        }
                    }, z, new Object[0]);
                    break;
                }
                break;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.mWebView.pauseTimers();
        super.onPause();
        if (this.mInfo != null) {
            new av(this, this.mInfo.getUrl(), this.from, this.mInfo.getNid(), this.mInfo.getA(), "b88f54f7f545e6b4", this.mInfo.getTitle(), null, null).a(null, new Void[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mWebView.resumeTimers();
        if (this.mInfo != null) {
            new av(this, this.mInfo.getUrl(), this.from, this.mInfo.getNid(), this.mInfo.getA(), "b88f54f7f545e6b3", this.mInfo.getTitle(), null, null).a(null, new Void[0]);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public void onScroll(int i, int i2, int i3, int i4) {
        float f = (float) (i2 - i4);
        if (this.alphaLen == 0.0f) {
            this.alphaLen = (float) (getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material) * 2);
        }
        if (f < 0.0f) {
            this.alpha = ((-f) / this.alphaLen) + this.alpha;
        } else {
            if (i2 > this.mToolbar.getHeight()) {
                this.alpha -= f / this.alphaLen;
            }
            if (this.mWebLoadingProgressBar.getVisibility() == 0) {
                this.mWebLoadingProgressBar.setVisibility(8);
            }
        }
        this.alpha = Math.max(this.alpha, 0.0f);
        this.alpha = Math.min(this.alpha, 1.0f);
        if (((double) this.alpha) > 0.5d) {
            showActionbarShadow();
            if (!this.mToolbar.isEnabled()) {
                setToolBarEnabled(true);
                this.mToolbar.setEnabled(true);
            }
        } else {
            hideActionbarShadow();
            if (this.mToolbar.isEnabled()) {
                setToolBarEnabled(false);
                this.mToolbar.setEnabled(false);
            }
        }
        ViewCompat.setAlpha(this.mToolbar, this.alpha);
    }
}
