package com.qihoo360.daily.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import com.e.b.al;
import com.qihoo360.daily.R;
import com.qihoo360.daily.a.k;
import com.qihoo360.daily.a.q;
import com.qihoo360.daily.a.r;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.f.a;
import com.qihoo360.daily.g.ar;
import com.qihoo360.daily.g.av;
import com.qihoo360.daily.h.ad;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.NewComment;
import com.qihoo360.daily.model.NewCommentResponse;
import com.qihoo360.daily.model.Result;
import com.qihoo360.daily.model.User;
import com.qihoo360.daily.widget.RecyclerViewDivider.CmtDividerItemDecoration;
import com.qihoo360.daily.wxapi.WXUser;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class CommentActivity extends BaseDetailActivity implements r {
    private static final int REQ_CODE_EDIT = 1;
    private static final String TAG_CONTENT = "content";
    private String clickType;
    /* access modifiers changed from: private */
    public NewCommentResponse commentResponse;
    /* access modifiers changed from: private */
    public String content;
    /* access modifiers changed from: private */
    public View error_layout;
    /* access modifiers changed from: private */
    public String from;
    /* access modifiers changed from: private */
    public Intent intent;
    /* access modifiers changed from: private */
    public View loading_layout;
    /* access modifiers changed from: private */
    public k mCommentAdapter;
    /* access modifiers changed from: private */
    public RecyclerView mCommentListView;
    /* access modifiers changed from: private */
    public Info mInfo;
    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.toolbar:
                    CommentActivity.this.mPullToRefreshView.setRefreshing(true);
                    CommentActivity.this.mPullToRefreshView.postDelayed(new Runnable() {
                        public void run() {
                            try {
                                Field declaredField = SwipeRefreshLayout.class.getDeclaredField("mProgress");
                                declaredField.setAccessible(true);
                                Object obj = declaredField.get(CommentActivity.this.mPullToRefreshView);
                                declaredField.getType().getDeclaredMethod("setProgressRotation", Float.TYPE).invoke(obj, Float.valueOf(1.0f));
                                declaredField.getType().getDeclaredMethod("setArrowScale", Float.TYPE).invoke(obj, Float.valueOf(1.0f));
                                declaredField.getType().getDeclaredMethod("setStartEndTrim", Float.TYPE, Float.TYPE).invoke(obj, Float.valueOf(0.75f), Float.valueOf(0.75f));
                                declaredField.getType().getDeclaredMethod("showArrow", Boolean.TYPE).invoke(obj, true);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 500);
                    CommentActivity.this.mCommentListView.scrollToPosition(0);
                    new Handler() {
                        public void handleMessage(Message message) {
                            new ar(CommentActivity.this.getApplicationContext(), CommentActivity.this.mInfo.getUrl(), "0").a(CommentActivity.this.onNetCommentRequestListener, new Void[0]);
                        }
                    }.sendEmptyMessageDelayed(0, SplashActivity.SPLASH_TYPE_DEFAULT_TIME);
                    return;
                case R.id.error_layout:
                    if (CommentActivity.this.mInfo != null) {
                        CommentActivity.this.error_layout.setVisibility(8);
                        CommentActivity.this.loading_layout.setVisibility(0);
                        new ar(CommentActivity.this.getApplicationContext(), CommentActivity.this.mInfo.getUrl(), "0").a(CommentActivity.this.onNetCommentRequestListener, new Void[0]);
                        return;
                    }
                    return;
                case R.id.go_cmt:
                    Intent unused = CommentActivity.this.intent = new Intent(CommentActivity.this.getApplicationContext(), SendCmtActivity.class).putExtra("Info", CommentActivity.this.mInfo).putExtra("from", CommentActivity.this.from).putExtra(CommentActivity.TAG_CONTENT, CommentActivity.this.content);
                    if (a.h(CommentActivity.this.getApplicationContext())) {
                        CommentActivity.this.startActivityForResult(CommentActivity.this.intent, 1);
                        Intent unused2 = CommentActivity.this.intent = null;
                        return;
                    }
                    CommentActivity.this.login();
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public SwipeRefreshLayout mPullToRefreshView;
    /* access modifiers changed from: private */
    public View no_cmt_layout;
    /* access modifiers changed from: private */
    public c<Void, Result<NewCommentResponse>> onNetCommentRequestListener = new c<Void, Result<NewCommentResponse>>() {
        public void onNetRequest(int i, Result<NewCommentResponse> result) {
            int i2;
            if (CommentActivity.this.loading_layout.getVisibility() == 0) {
                CommentActivity.this.loading_layout.setVisibility(8);
            }
            CommentActivity.this.mPullToRefreshView.setRefreshing(false);
            if (result != null) {
                if (result.getStatus() == 0) {
                    NewCommentResponse unused = CommentActivity.this.commentResponse = result.getData();
                    if (CommentActivity.this.commentResponse != null) {
                        List<NewComment> hot_comments = CommentActivity.this.commentResponse.getHot_comments();
                        if (hot_comments != null && hot_comments.size() > 0) {
                            CommentActivity.this.mCommentAdapter.a(hot_comments);
                        }
                        List<NewComment> comments = CommentActivity.this.commentResponse.getComments();
                        int unused2 = CommentActivity.this.total = CommentActivity.this.commentResponse.getComment_num();
                        int a2 = CommentActivity.this.mCommentAdapter != null ? CommentActivity.this.mCommentAdapter.a() : 0;
                        if (comments != null) {
                            i2 = comments.size();
                            a2 += comments.size();
                        } else {
                            i2 = 0;
                        }
                        if (CommentActivity.this.total > a2 && i2 > 0) {
                            CommentActivity.this.mCommentAdapter.a(false);
                        } else if (CommentActivity.this.mCommentAdapter != null) {
                            CommentActivity.this.mCommentAdapter.a(true);
                        }
                        if (CommentActivity.this.mCommentAdapter != null) {
                            CommentActivity.this.mCommentAdapter.b(comments);
                        }
                        if (CommentActivity.this.total == 0) {
                            CommentActivity.this.no_cmt_layout.setVisibility(0);
                        } else {
                            CommentActivity.this.no_cmt_layout.setVisibility(8);
                        }
                        Intent intent = new Intent();
                        intent.putExtra("cmts", CommentActivity.this.commentResponse);
                        intent.putExtra("cmt_cnt", CommentActivity.this.total);
                        CommentActivity.this.setResult(-1, intent);
                        return;
                    }
                    return;
                }
                ay.a(CommentActivity.this.getApplicationContext()).a(result.getMsg());
            } else if (CommentActivity.this.mCommentAdapter.getItemCount() == 0) {
                CommentActivity.this.error_layout.setVisibility(0);
            } else {
                CommentActivity.this.mCommentAdapter.c(true);
            }
        }

        public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
            onNetRequest(i, (Result<NewCommentResponse>) ((Result) obj));
        }
    };
    /* access modifiers changed from: private */
    public c<Void, Result<NewCommentResponse>> onNetMoreCommentRequestListener = new c<Void, Result<NewCommentResponse>>() {
        public void onNetRequest(int i, Result<NewCommentResponse> result) {
            if (result == null) {
                CommentActivity.this.mCommentAdapter.c(true);
            } else if (result.getStatus() == 0) {
                NewCommentResponse unused = CommentActivity.this.commentResponse = result.getData();
                if (CommentActivity.this.commentResponse != null) {
                    List<NewComment> comments = CommentActivity.this.commentResponse.getComments();
                    int a2 = CommentActivity.this.mCommentAdapter.a() + comments.size();
                    ad.a("onNetRequest size:" + a2);
                    if (CommentActivity.this.total <= a2 || comments.size() <= 0) {
                        CommentActivity.this.mCommentAdapter.a(true);
                    } else {
                        CommentActivity.this.mCommentAdapter.a(false);
                    }
                    CommentActivity.this.mCommentAdapter.c(comments);
                }
            } else {
                ay.a(CommentActivity.this.getApplicationContext()).a(result.getMsg());
            }
        }

        public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
            onNetRequest(i, (Result<NewCommentResponse>) ((Result) obj));
        }
    };
    /* access modifiers changed from: private */
    public int total;

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle((int) R.string.comment_action_bar_others);
        toolbar.setNavigationIcon((int) R.drawable.ic_actionbar_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                CommentActivity.this.onBackPressed();
            }
        });
        toolbar.setOnClickListener(this.mOnClickListener);
    }

    public void OnWeiboLoginCancel() {
    }

    public void OnWeiboLoginError(User user) {
    }

    public void OnWeiboLoginException(com.sina.weibo.sdk.c.c cVar) {
    }

    public void OnWeiboLoginSuccess(User user) {
        if (a.h(getApplicationContext())) {
            startActivityForResult(this.intent, 1);
            this.intent = null;
        }
    }

    public void OnWeixinLoginCancel() {
    }

    public void OnWeixinLoginError(WXUser wXUser) {
    }

    public void OnWeixinLoginException(int i) {
    }

    public void OnWeixinLoginSuccess(WXUser wXUser) {
        if (a.h(getApplicationContext())) {
            startActivityForResult(this.intent, 1);
            this.intent = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent2) {
        super.onActivityResult(i, i2, intent2);
        if (1 == i && -1 == i2 && intent2 != null) {
            this.mCommentAdapter.a((NewComment) intent2.getParcelableExtra(SendCmtActivity.TAG_DATA));
            this.mCommentListView.scrollToPosition(this.mCommentAdapter.b() ? 1 : 0);
            this.total++;
            this.commentResponse.setComment_num(this.total);
            if (this.total == 0) {
                this.no_cmt_layout.setVisibility(0);
            } else {
                this.no_cmt_layout.setVisibility(8);
            }
            Intent intent3 = new Intent();
            intent3.putExtra("cmts", this.commentResponse);
            intent3.putExtra("cmt_cnt", this.total);
            setResult(-1, intent3);
        }
        if (i == 1 && intent2 != null) {
            this.content = intent2.getStringExtra(TAG_CONTENT);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_comment);
        Intent intent2 = getIntent();
        this.mInfo = (Info) intent2.getParcelableExtra("Info");
        this.clickType = intent2.getStringExtra("clickType");
        this.from = intent2.getStringExtra("from");
        if (this.mInfo != null) {
            this.loading_layout = findViewById(R.id.loading_layout);
            this.error_layout = findViewById(R.id.error_layout);
            this.error_layout.setOnClickListener(this.mOnClickListener);
            this.no_cmt_layout = findViewById(R.id.no_cmt_layout);
            this.mPullToRefreshView = (SwipeRefreshLayout) findViewById(R.id.pull_refresh_view);
            this.mPullToRefreshView.setColorSchemeResources(R.color.colorPrimary);
            this.mPullToRefreshView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                public void onRefresh() {
                    new ar(CommentActivity.this.getApplicationContext(), CommentActivity.this.mInfo.getUrl(), "0").a(CommentActivity.this.onNetCommentRequestListener, new Void[0]);
                }
            });
            this.mCommentListView = (RecyclerView) findViewById(R.id.recyclerView);
            this.mCommentListView.setLayoutManager(new LinearLayoutManager(this) {
                public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                    return new RecyclerView.LayoutParams(-1, -2);
                }
            });
            this.mCommentListView.addItemDecoration(new CmtDividerItemDecoration(this, 1));
            this.mCommentAdapter = new k(this, this.mInfo.getUrl(), null, this);
            this.mCommentAdapter.a(new q() {
                public void onLoadMore(String str) {
                    new ar(CommentActivity.this.getApplicationContext(), CommentActivity.this.mInfo.getUrl(), str).a(CommentActivity.this.onNetMoreCommentRequestListener, new Void[0]);
                }
            });
            this.mCommentListView.setAdapter(this.mCommentAdapter);
            findViewById(R.id.go_cmt).setOnClickListener(this.mOnClickListener);
            this.loading_layout.setVisibility(0);
            new ar(getApplicationContext(), this.mInfo.getUrl(), "0").a(this.onNetCommentRequestListener, new Void[0]);
            initToolbar();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        al.a((Context) Application.getInstance()).a((Object) this);
    }

    public void onDiggClick() {
        if (this.mInfo != null) {
            new av(Application.getInstance(), this.mInfo.getUrl(), this.from, this.mInfo.getNid(), this.mInfo.getA(), "b88f54f7f545e6b5", this.mInfo.getTitle(), "2", this.clickType).a(null, new Void[0]);
        }
    }

    public void onReplay(List<NewComment> list, NewComment newComment) {
        this.intent = new Intent(getApplicationContext(), SendCmtActivity.class).putExtra("Info", this.mInfo).putExtra("from", this.from).putExtra("replay_comment", newComment).putParcelableArrayListExtra("chain_comments", list != null ? new ArrayList(list) : null).putExtra(TAG_CONTENT, this.content);
        if (a.h(getApplicationContext())) {
            startActivityForResult(this.intent, 1);
            this.intent = null;
            return;
        }
        login();
    }
}
