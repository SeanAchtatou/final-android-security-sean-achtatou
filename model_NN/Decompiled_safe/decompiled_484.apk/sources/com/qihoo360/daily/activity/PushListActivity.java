package com.qihoo360.daily.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import com.qihoo360.daily.R;
import com.qihoo360.daily.a.ad;
import com.qihoo360.daily.a.af;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.g.u;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.Result;
import com.qihoo360.daily.widget.PullRecyclerView;
import com.qihoo360.daily.widget.RecyclerViewDivider.DividerItemDecoration;
import java.util.ArrayList;
import java.util.List;

public class PushListActivity extends BaseNoFragmentActivity implements af {
    /* access modifiers changed from: private */
    public View error_layout;
    /* access modifiers changed from: private */
    public ad mAdapter;
    /* access modifiers changed from: private */
    public View mEmpty;
    /* access modifiers changed from: private */
    public List<Info> mList;
    /* access modifiers changed from: private */
    public View mLoadingLayout;
    /* access modifiers changed from: private */
    public SwipeRefreshLayout mPullToRefreshView;

    /* access modifiers changed from: private */
    public void loadData(final String str) {
        new u(this).a(new c<Void, Result<ArrayList<Info>>>() {
            public void onNetRequest(int i, Result<ArrayList<Info>> result) {
                PushListActivity.this.mPullToRefreshView.setRefreshing(false);
                if (PushListActivity.this.mLoadingLayout.getVisibility() == 0) {
                    PushListActivity.this.mLoadingLayout.setVisibility(8);
                }
                if (result == null || result.getStatus() != 0) {
                    if (result != null) {
                        ay.a(PushListActivity.this).a(result.getMsg());
                    }
                    if (PushListActivity.this.mList.size() == 0) {
                        PushListActivity.this.error_layout.setVisibility(0);
                        return;
                    }
                    PushListActivity.this.mAdapter.e();
                    PushListActivity.this.mAdapter.notifyDataSetChanged();
                    return;
                }
                PushListActivity.this.error_layout.setVisibility(8);
                if (result.getData() == null || result.getData().isEmpty()) {
                    PushListActivity.this.mAdapter.g();
                    PushListActivity.this.mAdapter.notifyDataSetChanged();
                } else {
                    if (TextUtils.isEmpty(str)) {
                        PushListActivity.this.mList.clear();
                        d.b(PushListActivity.this.getBaseContext(), result.getData());
                    }
                    if (PushListActivity.this.mAdapter.f()) {
                        PushListActivity.this.mList.remove(PushListActivity.this.mAdapter.c());
                    }
                    PushListActivity.this.mList.addAll(result.getData());
                    PushListActivity.this.mAdapter.b();
                    PushListActivity.this.mAdapter.notifyDataSetChanged();
                    PushListActivity.this.mAdapter.d();
                }
                PushListActivity.this.mEmpty.setVisibility((PushListActivity.this.mAdapter.a() == null || PushListActivity.this.mAdapter.a().size() == 0) ? 0 : 8);
            }

            public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
                onNetRequest(i, (Result<ArrayList<Info>>) ((Result) obj));
            }
        }, str);
    }

    private List<Info> loadLocalData() {
        return d.e(getBaseContext());
    }

    private void loadMore(String str) {
        loadData(str);
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        Info info;
        if (1 == i && intent != null && (info = (Info) intent.getParcelableExtra("Info")) != null && this.mAdapter != null) {
            this.mAdapter.a(info);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_pushlist);
        this.mEmpty = findViewById(R.id.favor_emtpy);
        this.error_layout = findViewById(R.id.error_layout);
        this.error_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                PushListActivity.this.mLoadingLayout.setVisibility(0);
                PushListActivity.this.error_layout.setVisibility(8);
                PushListActivity.this.loadData(null);
            }
        });
        configToolbar(0, R.string.push_title, R.drawable.ic_actionbar_back).setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                PushListActivity.this.onBackPressed();
            }
        });
        this.mLoadingLayout = findViewById(R.id.loading_layout);
        View findViewById = findViewById(R.id.favour_delete);
        this.mPullToRefreshView = (SwipeRefreshLayout) findViewById(R.id.pull_refresh_view);
        this.mPullToRefreshView.setColorSchemeResources(R.color.colorPrimary);
        this.mPullToRefreshView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            public void onRefresh() {
                PushListActivity.this.loadData(null);
            }
        });
        PullRecyclerView pullRecyclerView = (PullRecyclerView) findViewById(R.id.recyclerView);
        pullRecyclerView.setLayoutManager(new LinearLayoutManager(this) {
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(-1, -2);
            }
        });
        this.mList = new ArrayList();
        this.mAdapter = new ad(this, this.mList);
        this.mAdapter.a(this);
        this.mAdapter.a(findViewById);
        pullRecyclerView.setAdapter(this.mAdapter);
        pullRecyclerView.addItemDecoration(new DividerItemDecoration(this, 1));
        this.mLoadingLayout.setVisibility(0);
        List<Info> loadLocalData = loadLocalData();
        if (loadLocalData != null) {
            this.mList.addAll(loadLocalData);
            this.mAdapter.b();
        }
        this.mAdapter.notifyDataSetChanged();
        loadData(null);
    }

    public void onLoadMore(String str) {
        loadMore(str);
    }
}
