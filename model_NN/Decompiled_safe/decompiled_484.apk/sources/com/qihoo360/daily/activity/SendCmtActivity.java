package com.qihoo360.daily.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.g.at;
import com.qihoo360.daily.g.av;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.NewComment;
import com.qihoo360.daily.model.Result;
import com.qihoo360.daily.widget.SoftInputRelativeLayout;
import java.util.ArrayList;

public class SendCmtActivity extends Activity implements TextWatcher, View.OnClickListener, SoftInputRelativeLayout.OnSoftInputWindowChangeListener {
    public static final String TAG_DATA = "data";
    /* access modifiers changed from: private */
    public ArrayList<NewComment> chain_comments;
    /* access modifiers changed from: private */
    public NewComment comment;
    private EditText content_et;
    private String mFrom;
    private Info mInfo;
    private c<Void, Result<NewComment>> onNetRequestListener = new c<Void, Result<NewComment>>() {
        public void onNetRequest(int i, Result<NewComment> result) {
            if (result != null) {
                int status = result.getStatus();
                if (status == 0) {
                    Intent intent = new Intent();
                    NewComment data = result.getData();
                    if (SendCmtActivity.this.chain_comments == null) {
                        ArrayList unused = SendCmtActivity.this.chain_comments = new ArrayList();
                    }
                    int size = SendCmtActivity.this.chain_comments.size();
                    if (SendCmtActivity.this.comment != null) {
                        if (size > 0) {
                            if (!SendCmtActivity.this.comment.getComment_id().equals(((NewComment) SendCmtActivity.this.chain_comments.get(SendCmtActivity.this.chain_comments.size() - 1)).getComment_id())) {
                                SendCmtActivity.this.chain_comments.add(SendCmtActivity.this.comment);
                            }
                        } else {
                            SendCmtActivity.this.chain_comments.add(SendCmtActivity.this.comment);
                        }
                    }
                    data.setChain_comments(SendCmtActivity.this.chain_comments);
                    data.setSupport("0");
                    data.setCreate_time(b.c());
                    intent.putExtra(SendCmtActivity.TAG_DATA, data);
                    SendCmtActivity.this.setResult(-1, intent);
                    SendCmtActivity.this.finish();
                    ay.a(SendCmtActivity.this.getApplicationContext()).a((int) R.string.send_ok);
                } else if (status == 109) {
                    ay.a(SendCmtActivity.this.getApplicationContext()).a((int) R.string.cmd_too_frequent);
                } else if (status == 103) {
                    SendCmtActivity.this.setResult(2);
                    SendCmtActivity.this.finish();
                } else {
                    ay.a(SendCmtActivity.this.getApplicationContext()).a(result.getMsg());
                }
            } else {
                ay.a(SendCmtActivity.this.getApplicationContext()).a((int) R.string.net_error);
            }
        }

        public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
            onNetRequest(i, (Result<NewComment>) ((Result) obj));
        }
    };
    private View send_btn;
    private TextView text_num;
    private String title;
    private String url;

    public void afterTextChanged(Editable editable) {
        if (editable != null) {
            int length = editable.length();
            if (length == 0) {
                this.send_btn.setEnabled(false);
            } else {
                this.send_btn.setEnabled(true);
            }
            this.text_num.setText(getString(R.string.text_num, new Object[]{Integer.valueOf(length)}));
            if (length <= 140) {
                this.text_num.setTextColor(getResources().getColor(R.color.black_text));
            } else {
                this.text_num.setTextColor(getResources().getColor(R.color.exceed_num_text));
            }
        }
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }

    public void onBackPressed() {
        String obj = this.content_et.getText().toString();
        Intent intent = new Intent();
        intent.putExtra("content", obj);
        setResult(0, intent);
        super.onBackPressed();
    }

    public void onChange(boolean z) {
        if (z) {
            String obj = this.content_et.getText().toString();
            Intent intent = new Intent();
            intent.putExtra("content", obj);
            setResult(0, intent);
            finish();
        }
    }

    public void onClick(View view) {
        String obj = this.content_et.getText().toString();
        switch (view.getId()) {
            case R.id.soft_input_v:
                Intent intent = new Intent();
                intent.putExtra("content", obj);
                setResult(0, intent);
                finish();
                return;
            case R.id.content_et:
            case R.id.text_num:
            default:
                return;
            case R.id.send_btn:
                if (TextUtils.isEmpty(obj.trim())) {
                    ay.a(getApplicationContext()).a((int) R.string.no_content);
                    return;
                } else if (obj.length() > 140) {
                    ay.a(getApplicationContext()).a((int) R.string.content_too_large);
                    return;
                } else {
                    new at(this, this.comment != null ? this.comment.getComment_id() : null, this.url, obj, this.title).a(this.onNetRequestListener, new Void[0]);
                    if (this.mInfo != null) {
                        new av(Application.getInstance(), this.mInfo.getUrl(), this.mFrom, this.mInfo.getNid(), this.mInfo.getA(), "b88f54f7f545e6b5", this.mInfo.getTitle(), "0", "0").a(null, new Void[0]);
                        return;
                    }
                    return;
                }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_send_cmt);
        Intent intent = getIntent();
        this.comment = (NewComment) intent.getParcelableExtra("replay_comment");
        this.chain_comments = intent.getParcelableArrayListExtra("chain_comments");
        this.mInfo = (Info) intent.getParcelableExtra("Info");
        if (this.mInfo != null) {
            this.url = this.mInfo.getUrl();
            this.title = this.mInfo.getTitle();
        }
        this.mFrom = intent.getStringExtra("from");
        this.text_num = (TextView) findViewById(R.id.text_num);
        this.text_num.setText(getString(R.string.text_num, new Object[]{0}));
        this.content_et = (EditText) findViewById(R.id.content_et);
        this.content_et.addTextChangedListener(this);
        if (this.comment != null) {
            this.content_et.setHint("回复：" + this.comment.getUname());
            if (b.i(this.comment.getUid())) {
                ay.a(getApplicationContext()).a((int) R.string.can_not_reply_yourself);
                finish();
                return;
            }
        }
        this.send_btn = findViewById(R.id.send_btn);
        this.send_btn.setOnClickListener(this);
        SoftInputRelativeLayout softInputRelativeLayout = (SoftInputRelativeLayout) findViewById(R.id.soft_input_v);
        softInputRelativeLayout.setOnSoftInputWindowChangeListener(this);
        softInputRelativeLayout.setOnClickListener(this);
        String stringExtra = intent.getStringExtra("content");
        if (!TextUtils.isEmpty(stringExtra)) {
            this.content_et.setText(stringExtra);
            this.content_et.setSelection(stringExtra.length());
        }
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
