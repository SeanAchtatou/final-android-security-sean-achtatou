package com.qihoo360.daily.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Process;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import com.qihoo360.daily.R;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.g.bc;

public class CrashReportActivity extends Activity implements View.OnClickListener {
    private String mCrashInfo = null;

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibtn_close:
                finish();
                Process.killProcess(Process.myPid());
                return;
            case R.id.ibtn_send:
                if (!TextUtils.isEmpty(this.mCrashInfo)) {
                    new bc(this, this.mCrashInfo).a(null, new Void[0]);
                }
                finish();
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, boolean):void
     arg types: [com.qihoo360.daily.activity.CrashReportActivity, java.lang.String, int]
     candidates:
      com.qihoo360.daily.f.d.a(android.content.Context, long, java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, int):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, long):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String[], java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_crash);
        this.mCrashInfo = getIntent().getStringExtra("crashInfo");
        findViewById(R.id.ibtn_send).setOnClickListener(this);
        findViewById(R.id.ibtn_close).setOnClickListener(this);
        d.a((Context) this, "login_status", false);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || keyEvent.getAction() != 0) {
            return super.onKeyDown(i, keyEvent);
        }
        finish();
        Process.killProcess(Process.myPid());
        return true;
    }
}
