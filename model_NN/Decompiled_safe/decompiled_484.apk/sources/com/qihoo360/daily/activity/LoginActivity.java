package com.qihoo360.daily.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import com.qihoo360.daily.R;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.i.b;
import com.qihoo360.daily.i.e;
import com.qihoo360.daily.model.User;
import com.qihoo360.daily.wxapi.WXHelper;
import com.sina.weibo.sdk.c.c;

public class LoginActivity extends Activity implements View.OnClickListener {
    public static final String KEY_USER_INFO = "user_info";
    public static final int REQUEST_CODE_LOGIN = 0;
    private static final int SLEEP_TIME = 1000;
    private e mGetWeiboInfoListener = new e() {
        public void OnWeiboLoginCancel() {
            LoginActivity.this.forceExit();
        }

        public void OnWeiboLoginError(User user) {
            LoginActivity.this.forceExit();
        }

        public void OnWeiboLoginException(c cVar) {
            LoginActivity.this.forceExit();
        }

        public void OnWeiboLoginSuccess(User user) {
            new Handler(LoginActivity.this.getMainLooper()).postDelayed(new Runnable() {
                public void run() {
                    LoginActivity.this.forceExit();
                }
            }, 100);
        }
    };

    /* access modifiers changed from: private */
    public void forceExit() {
        finish();
    }

    private void postExit() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                LoginActivity.this.forceExit();
            }
        }, 1000);
    }

    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        b.INSTANCE.a(i, i2, intent);
        super.onActivityResult(i, i2, intent);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_login_weixin:
                if (com.qihoo360.daily.h.b.d(getApplicationContext())) {
                    WXHelper.INSTANCE.loginWeixin(null);
                } else {
                    ay.a(getApplicationContext()).a((int) R.string.net_error);
                }
                postExit();
                return;
            case R.id.rl_login_btn:
                if (!com.qihoo360.daily.h.b.d(getApplicationContext())) {
                    ay.a(getApplicationContext()).a((int) R.string.net_error);
                    postExit();
                    return;
                } else if (!b.INSTANCE.b(this, this.mGetWeiboInfoListener)) {
                    postExit();
                    return;
                } else {
                    return;
                }
            case R.id.btn_cancel:
                forceExit();
                return;
            case R.id.view_login_top:
                forceExit();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_login);
        findViewById(R.id.rl_login_weixin).setOnClickListener(this);
        findViewById(R.id.rl_login_btn).setOnClickListener(this);
        findViewById(R.id.btn_cancel).setOnClickListener(this);
        findViewById(R.id.view_login_top).setOnClickListener(this);
    }
}
