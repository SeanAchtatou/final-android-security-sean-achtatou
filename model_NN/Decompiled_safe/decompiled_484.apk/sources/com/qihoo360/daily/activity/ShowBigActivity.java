package com.qihoo360.daily.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RelativeLayout;
import com.qihoo360.daily.R;
import com.qihoo360.daily.h.al;
import com.qihoo360.daily.h.as;

public class ShowBigActivity extends BaseNoFragmentActivity {
    private String bigPic;
    /* access modifiers changed from: private */
    public Intent intent;
    private RelativeLayout lay;
    private String smallPic;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_showbig);
        this.lay = (RelativeLayout) findViewById(R.id.lay);
        this.intent = getIntent();
        int intExtra = getIntent().getIntExtra("show_big_startx", 0);
        int intExtra2 = getIntent().getIntExtra("show_big_starty", 0);
        int intExtra3 = getIntent().getIntExtra("show_big_width", 0);
        int intExtra4 = getIntent().getIntExtra("show_big_height", 0);
        this.smallPic = getIntent().getStringExtra("show_big_small_pic");
        this.bigPic = getIntent().getStringExtra("show_big_big_pic");
        new al(this, intExtra, intExtra2, intExtra3, intExtra4, this.lay, this.smallPic, this.bigPic, new as() {
            public void onShowBigEnd() {
                ShowBigActivity.this.startActivity(ShowBigActivity.this.intent);
                ShowBigActivity.this.finish();
            }

            public void onShowSmallEnd() {
            }
        }).a();
    }
}
