package com.qihoo360.daily.activity;

import android.app.Activity;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;
import com.qihoo360.daily.R;
import com.qihoo360.daily.a.i;
import com.qihoo360.daily.b.a;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.model.City;
import com.qihoo360.daily.widget.IndexableListView;
import java.util.List;

public class CitylistActivity extends BaseActivity {
    private AMapLocationListener aMapLocationListener = new AMapLocationListener() {
        public void onLocationChanged(Location location) {
        }

        public void onLocationChanged(final AMapLocation aMapLocation) {
            if (aMapLocation != null && aMapLocation.getAMapException() != null && aMapLocation.getAMapException().getErrorCode() == 0) {
                final a aVar = new a(CitylistActivity.this.getApplicationContext());
                new Thread(new Runnable() {
                    public void run() {
                        City city = new City();
                        city.setCity(aMapLocation.getCity());
                        city.setCode(aMapLocation.getCityCode());
                        city.setProvince(aMapLocation.getProvince());
                        city.setAuto(true);
                        city.setDistrict(aMapLocation.getDistrict());
                        final City a2 = aVar.a(city);
                        if (a2 == null || TextUtils.isEmpty(a2.getCitySpell()) || TextUtils.isEmpty(a2.getCode())) {
                            a2 = d.a();
                        }
                        a2.getCity();
                        d.q(CitylistActivity.this.getApplicationContext());
                        CitylistActivity.this.getWindow().getDecorView().post(new Runnable() {
                            public void run() {
                                CitylistActivity.this.mHeaderView.setTag(a2);
                                ((TextView) CitylistActivity.this.mHeaderView.findViewById(R.id.tv_city_name)).setText(a2.getCity());
                            }
                        });
                    }
                }).start();
            }
        }

        public void onProviderDisabled(String str) {
        }

        public void onProviderEnabled(String str) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }
    };
    /* access modifiers changed from: private */
    public boolean isSearchMode;
    /* access modifiers changed from: private */
    public a mCityDBHelper;
    /* access modifiers changed from: private */
    public i mCityListAdapter;
    /* access modifiers changed from: private */
    public View mHeaderView;
    /* access modifiers changed from: private */
    public List<City> mList;
    /* access modifiers changed from: private */
    public IndexableListView mListView;
    private LocationManagerProxy mLocationManagerProxy;

    class QuaryTask extends AsyncTask<String, Void, List<City>> {
        QuaryTask() {
        }

        /* access modifiers changed from: protected */
        public List<City> doInBackground(String... strArr) {
            String str = strArr[0];
            if (str.length() > 0) {
                List<City> a2 = CitylistActivity.this.mCityDBHelper.a(str);
                List unused = CitylistActivity.this.mList = a2;
                return a2;
            }
            List<City> b2 = CitylistActivity.this.mCityDBHelper.b();
            List unused2 = CitylistActivity.this.mList = b2;
            return b2;
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((List<City>) ((List) obj));
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<City> list) {
            super.onPostExecute((Object) list);
            CitylistActivity.this.mListView.setAdapter((ListAdapter) null);
            if (CitylistActivity.this.isSearchMode) {
                CitylistActivity.this.mListView.removeHeaderView(CitylistActivity.this.mHeaderView);
                CitylistActivity.this.mListView.hideIndexScroller();
            } else {
                CitylistActivity.this.mListView.addHeaderView(CitylistActivity.this.mHeaderView);
                CitylistActivity.this.mListView.showIndexScroller();
            }
            i unused = CitylistActivity.this.mCityListAdapter = new i(CitylistActivity.this.getApplicationContext(), CitylistActivity.this.mList, CitylistActivity.this.mListView.getHeaderViewsCount());
            CitylistActivity.this.mCityListAdapter.a(CitylistActivity.this.isSearchMode);
            CitylistActivity.this.mListView.setAdapter((ListAdapter) CitylistActivity.this.mCityListAdapter);
        }
    }

    private void hideSoftInputMethod() {
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mListView.getWindowToken(), 0);
    }

    private void showSoftInputMethod() {
        ((InputMethodManager) getSystemService("input_method")).toggleSoftInput(2, 1);
    }

    private void startLocation() {
        this.mLocationManagerProxy = LocationManagerProxy.getInstance((Activity) this);
        this.mLocationManagerProxy.setGpsEnable(false);
        this.mLocationManagerProxy.requestLocationData(LocationProviderProxy.AMapNetwork, -1, 500.0f, this.aMapLocationListener);
    }

    private void stopLocation() {
        if (this.mLocationManagerProxy != null) {
            this.mLocationManagerProxy.removeUpdates(this.aMapLocationListener);
            this.mLocationManagerProxy.destroy();
        }
        this.mLocationManagerProxy = null;
    }

    public void initToolBar() {
        configToolbar(0, R.string.select_city, R.drawable.ic_actionbar_back).setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                CitylistActivity.this.onBackPressed();
            }
        });
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mCityDBHelper = new a(this);
        setContentView((int) R.layout.activity_citylist);
        this.mListView = (IndexableListView) findViewById(R.id.lv_citylist);
        this.mHeaderView = View.inflate(this, R.layout.item_city, null);
        ((TextView) this.mHeaderView.findViewById(R.id.tv_section)).setText(getString(R.string.located_city));
        ((TextView) this.mHeaderView.findViewById(R.id.tv_city_name)).setText(d.q(this).getCity());
        this.mListView.addHeaderView(this.mHeaderView, null, false);
        this.mListView.setFastScrollEnabled(true);
        this.mList = this.mCityDBHelper.b();
        this.mCityListAdapter = new i(this, this.mList, this.mListView.getHeaderViewsCount());
        this.mListView.setAdapter((ListAdapter) this.mCityListAdapter);
        this.mHeaderView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (view.getTag() != null) {
                    City city = (City) view.getTag();
                    city.setProvinceSpell(CitylistActivity.this.mCityDBHelper.b(city.getProvince()));
                    d.a(CitylistActivity.this.mHeaderView.getContext(), city);
                }
                CitylistActivity.this.setResult(-1);
                CitylistActivity.this.finish();
            }
        });
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                City city = (City) CitylistActivity.this.mList.get(i - CitylistActivity.this.mListView.getHeaderViewsCount());
                city.setProvinceSpell(CitylistActivity.this.mCityDBHelper.b(city.getProvince()));
                d.a(view.getContext(), city);
                CitylistActivity.this.setResult(-1);
                CitylistActivity.this.finish();
            }
        });
        SearchView searchView = (SearchView) findViewById(R.id.sv_search);
        searchView.setSubmitButtonEnabled(false);
        searchView.setIconified(false);
        searchView.setQueryHint(getString(R.string.hint_search_city));
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            public boolean onClose() {
                return true;
            }
        });
        EditText editText = (EditText) searchView.findViewById(R.id.search_src_text);
        editText.setTextColor((int) ViewCompat.MEASURED_STATE_MASK);
        editText.setTextSize(14.0f);
        editText.requestFocus();
        editText.setHintTextColor(Color.parseColor("#8f8f8f"));
        showSoftInputMethod();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String str) {
                boolean unused = CitylistActivity.this.isSearchMode = str.length() != 0;
                new QuaryTask().execute(str.toString());
                return true;
            }

            public boolean onQueryTextSubmit(String str) {
                return true;
            }
        });
        startLocation();
        initToolBar();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        stopLocation();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        hideSoftInputMethod();
    }
}
