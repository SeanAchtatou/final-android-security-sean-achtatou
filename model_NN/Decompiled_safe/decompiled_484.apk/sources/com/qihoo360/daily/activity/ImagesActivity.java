package com.qihoo360.daily.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings;
import android.widget.TextView;
import com.mediav.ads.sdk.adcore.Config;
import com.qihoo360.daily.R;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.f.a;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.fragment.ImgRecFragment;
import com.qihoo360.daily.g.ar;
import com.qihoo360.daily.g.av;
import com.qihoo360.daily.g.h;
import com.qihoo360.daily.g.k;
import com.qihoo360.daily.g.s;
import com.qihoo360.daily.h.ab;
import com.qihoo360.daily.h.ag;
import com.qihoo360.daily.h.ai;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.h.y;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.NewComment;
import com.qihoo360.daily.model.NewCommentResponse;
import com.qihoo360.daily.model.NewsContent;
import com.qihoo360.daily.model.NewsDetail;
import com.qihoo360.daily.model.NewsRelated;
import com.qihoo360.daily.model.ResponseBean;
import com.qihoo360.daily.model.Result;
import com.qihoo360.daily.model.User;
import com.qihoo360.daily.wxapi.WXUser;
import java.util.ArrayList;
import java.util.Arrays;

public class ImagesActivity extends DetailActivity implements ViewPager.OnPageChangeListener, View.OnClickListener, View.OnLongClickListener, ab {
    private static final int CMTS_REQUESTCODE = 10;
    public static final String HAS_MORE_IMG_REC = "hasMoreImgRec";
    private static final int SEND_CMT_REQUESTCODE = 9;
    private View bottom_bar;
    /* access modifiers changed from: private */
    public boolean cmtAble = true;
    /* access modifiers changed from: private */
    public c<Void, Result<NewCommentResponse>> cmtOnNetRequestListener = new c<Void, Result<NewCommentResponse>>() {
        public void onNetRequest(int i, Result<NewCommentResponse> result) {
            if (result != null) {
                int status = result.getStatus();
                if (status == 0) {
                    NewCommentResponse data = result.getData();
                    if (data != null) {
                        int unused = ImagesActivity.this.mCmtTotal = data.getComment_num();
                        ImagesActivity.this.mInfo.setCmt_cnt(ImagesActivity.this.mCmtTotal + "");
                        ImagesActivity.this.setCmtNum(ImagesActivity.this.mCmtTotal);
                        Intent intent = new Intent();
                        intent.putExtra("Info", ImagesActivity.this.mInfo);
                        intent.putExtra("position", ImagesActivity.this.position);
                        ImagesActivity.this.setResult(-1, intent);
                    }
                } else if (status == 110) {
                    boolean unused2 = ImagesActivity.this.cmtAble = false;
                } else {
                    ay.a(ImagesActivity.this.getApplicationContext()).a(result.getMsg());
                }
            }
        }

        public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
            onNetRequest(i, (Result<NewCommentResponse>) ((Result) obj));
        }
    };
    private String content;
    private TextView count_tv;
    private c<ResponseBean<NewsDetail>, ResponseBean<NewsDetail>> detailOnNetRequestListener = new c<ResponseBean<NewsDetail>, ResponseBean<NewsDetail>>() {
        public void onNetRequest(int i, ResponseBean<NewsDetail> responseBean) {
        }

        public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
            onNetRequest(i, (ResponseBean<NewsDetail>) ((ResponseBean) obj));
        }

        public void onProgressUpdate(int i, ResponseBean<NewsDetail> responseBean) {
            if (responseBean != null) {
                int errno = responseBean.getErrno();
                if (errno == 0) {
                    ImagesActivity.this.setData(responseBean);
                    new com.qihoo360.daily.g.ay(ImagesActivity.this.getApplicationContext(), ImagesActivity.this.nid, ImagesActivity.this.info_url, 3, 0, null).a(ImagesActivity.this.relatedOnNetRequestListener, new Void[0]);
                    new ar(ImagesActivity.this.getApplicationContext(), ImagesActivity.this.info_url, "0").a(ImagesActivity.this.cmtOnNetRequestListener, new Void[0]);
                } else {
                    ay.a(ImagesActivity.this.getApplicationContext()).a(ImagesActivity.this.getString(R.string.error_code, new Object[]{Integer.valueOf(errno)}));
                    ImagesActivity.this.error_layout.setVisibility(0);
                }
                ImagesActivity.this.loading_layout.setVisibility(8);
                return;
            }
            ImagesActivity.this.disableDelayLoading();
            ImagesActivity.this.error_layout.setVisibility(0);
            ay.a(ImagesActivity.this.getApplicationContext()).a((int) R.string.net_error);
        }

        public /* bridge */ /* synthetic */ void onProgressUpdate(int i, Object obj) {
            onProgressUpdate(i, (ResponseBean<NewsDetail>) ((ResponseBean) obj));
        }
    };
    /* access modifiers changed from: private */
    public View error_layout;
    private String from;
    private int image_count;
    private ViewPager imgs_vp;
    /* access modifiers changed from: private */
    public String info_url;
    /* access modifiers changed from: private */
    public boolean isCollected;
    private int lastItemPosition;
    /* access modifiers changed from: private */
    public View loading_layout;
    private String m;
    /* access modifiers changed from: private */
    public com.qihoo360.daily.a.ab mAdapter;
    /* access modifiers changed from: private */
    public int mCmtTotal = 0;
    private boolean mHasMoreImgRec = true;
    /* access modifiers changed from: private */
    public Info mInfo;
    private int mLastSelectedPosition = 0;
    private Toolbar mToolbar;
    private TextView mTvCmtNum;
    private NewsDetail newsDetail;
    private String newtrans;
    /* access modifiers changed from: private */
    public String nid;
    private String nt;
    private TextView num_tv;
    private int p;
    private String[] pics;
    /* access modifiers changed from: private */
    public int position;
    /* access modifiers changed from: private */
    public c<Void, Result<NewsRelated>> relatedOnNetRequestListener = new c<Void, Result<NewsRelated>>() {
        public void onNetRequest(int i, Result<NewsRelated> result) {
            ImgRecFragment a2;
            if (result == null) {
                return;
            }
            if (result.getStatus() == 0) {
                NewsRelated data = result.getData();
                if (data != null) {
                    if (Config.CHANNEL_ID.equals(data.getIsCollected())) {
                        boolean unused = ImagesActivity.this.isCollected = true;
                    } else {
                        boolean unused2 = ImagesActivity.this.isCollected = false;
                    }
                }
                if (ImagesActivity.this.mAdapter != null && (a2 = ImagesActivity.this.mAdapter.a()) != null && a2.getOnNetRequestListener() != null) {
                    a2.getOnNetRequestListener().onNetRequest(i, result);
                    return;
                }
                return;
            }
            ay.a(ImagesActivity.this.getApplicationContext()).a(result.getMsg());
        }

        public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
            onNetRequest(i, (Result<NewsRelated>) ((Result) obj));
        }
    };
    private TextView summary_tv;
    private SparseArray<String> summarys = new SparseArray<>();
    private View title_layout;
    private TextView title_tv;
    private ArrayList<String> urls = new ArrayList<>();

    private void collectNews() {
        k kVar;
        boolean z;
        k kVar2 = k.ADD;
        if (this.isCollected) {
            kVar = k.DEL;
            z = true;
        } else {
            kVar = kVar2;
            z = false;
        }
        new h(kVar, this.nid).a(new c<Void, Result<Object>>() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.qihoo360.daily.f.d.c(android.content.Context, java.lang.String, boolean):void
             arg types: [com.qihoo360.daily.activity.ImagesActivity, java.lang.String, boolean]
             candidates:
              com.qihoo360.daily.f.d.c(android.content.Context, java.lang.String, java.lang.String):java.lang.String
              com.qihoo360.daily.f.d.c(android.content.Context, java.lang.String, int):void
              com.qihoo360.daily.f.d.c(android.content.Context, java.lang.String, long):void
              com.qihoo360.daily.f.d.c(android.content.Context, java.lang.String, boolean):void */
            public void onNetRequest(int i, Result<Object> result) {
                int i2;
                boolean z = true;
                if (result != null) {
                    int status = result.getStatus();
                    if (status == 0) {
                        switch (i) {
                            case 0:
                                boolean unused = ImagesActivity.this.isCollected = true;
                                i2 = R.string.favor_ok;
                                break;
                            default:
                                boolean unused2 = ImagesActivity.this.isCollected = false;
                                i2 = R.string.favor_cancel;
                                break;
                        }
                        ImagesActivity imagesActivity = ImagesActivity.this;
                        String access$400 = ImagesActivity.this.nid;
                        if (ImagesActivity.this.isCollected) {
                            z = false;
                        }
                        d.c((Context) imagesActivity, access$400, z);
                        ay.a(ImagesActivity.this).a(i2);
                    } else if (status == 103) {
                        ay.a(ImagesActivity.this).a((int) R.string.login_dialog_title_relogin);
                    } else {
                        ay.a(ImagesActivity.this).a(result.getMsg());
                    }
                } else {
                    ay.a(ImagesActivity.this).a((int) R.string.net_error);
                }
            }

            public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
                onNetRequest(i, (Result<Object>) ((Result) obj));
            }
        }, z, new Object[0]);
    }

    /* access modifiers changed from: private */
    public void disableDelayLoading() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                ImagesActivity.this.loading_layout.setVisibility(8);
            }
        }, 600);
    }

    /* access modifiers changed from: private */
    public void goComment() {
        if (this.cmtAble) {
            Intent intent = new Intent(getApplicationContext(), CommentActivity.class);
            intent.putExtra("Info", this.mInfo);
            intent.putExtra("clickType", this.clickType);
            intent.putExtra("from", this.from);
            startActivityForResult(intent, 10);
        } else {
            ay.a(getApplicationContext()).a((int) R.string.cmt_close);
        }
        b.b(this, "Bottombar_bottom_comment");
    }

    private void initData() {
        new s(getApplicationContext(), this.info_url, this.m, this.nid, this.newtrans, true, this.mInfo != null ? this.mInfo.getPdate() : null, null).a(this.detailOnNetRequestListener, new Void[0]);
    }

    private void initToolbar() {
        this.mToolbar = configToolbar(R.menu.menu_images, 0, R.drawable.ic_imgs_actionbar_back);
        View actionView = MenuItemCompat.getActionView(this.mToolbar.getMenu().findItem(R.id.go_comment));
        if (actionView != null) {
            actionView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    ImagesActivity.this.goComment();
                }
            });
            this.mTvCmtNum = (TextView) actionView.findViewById(R.id.tv_imgs_cmt_num);
            this.mTvCmtNum.setText("0");
        }
        this.mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ImagesActivity.this.onBackPressed();
            }
        });
    }

    private void initViews() {
        this.imgs_vp = (ViewPager) findViewById(R.id.imgs_vp);
        this.imgs_vp.setPageMargin(getResources().getDimensionPixelSize(R.dimen.margin_small));
        this.imgs_vp.setOnPageChangeListener(this);
        findViewById(R.id.go_cmt).setOnClickListener(this);
        this.title_tv = (TextView) findViewById(R.id.title_tv);
        this.summary_tv = (TextView) findViewById(R.id.summary_tv);
        this.summary_tv.setMovementMethod(new ScrollingMovementMethod());
        this.num_tv = (TextView) findViewById(R.id.num_tv);
        this.count_tv = (TextView) findViewById(R.id.count_tv);
        this.bottom_bar = findViewById(R.id.bottom_bar);
        this.title_layout = findViewById(R.id.title_layout);
        this.error_layout = findViewById(R.id.error_layout);
        this.error_layout.setOnClickListener(this);
        this.loading_layout = findViewById(R.id.loading_layout);
    }

    private void onClickMenu(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.go_comment:
                if (!this.cmtAble) {
                    ay.a(getApplicationContext()).a((int) R.string.cmt_close);
                    return;
                } else if (a.h(getApplicationContext())) {
                    startActivityForResult(new Intent(getApplicationContext(), SendCmtActivity.class).putExtra("Info", this.mInfo).putExtra("content", this.content), 9);
                    return;
                } else {
                    login();
                    return;
                }
            case R.id.go_share:
                new ai(this, this.mInfo, this.newsDetail, this.clickType, this.from).a();
                return;
            case R.id.go_more:
                new y(this, this, this.nid, this.isCollected, false).a(this.mToolbar);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void setCmtNum(int i) {
        if (i != 0 || this.mTvCmtNum == null) {
            if (this.mTvCmtNum != null) {
                this.mTvCmtNum.setVisibility(0);
            }
            String a2 = b.a(i);
            if (this.mTvCmtNum != null) {
                this.mTvCmtNum.setText(a2);
                return;
            }
            return;
        }
        this.mTvCmtNum.setVisibility(4);
    }

    /* access modifiers changed from: private */
    public void setData(ResponseBean<NewsDetail> responseBean) {
        int i;
        if (responseBean != null) {
            this.newsDetail = responseBean.getData();
            if (this.newsDetail != null) {
                this.newsDetail.setSource(this.mInfo.getSrc());
                ArrayList<NewsContent> content2 = this.newsDetail.getContent();
                String wapurl = this.newsDetail.getWapurl();
                if (TextUtils.isEmpty(wapurl) || (content2 != null && !content2.isEmpty())) {
                    this.title_tv.setText(this.newsDetail.getTitle());
                    if (content2 == null || content2.size() == 0) {
                        this.error_layout.setVisibility(0);
                        b.a(this.info_url, "", "detail");
                        return;
                    }
                    this.urls.clear();
                    int size = content2.size();
                    int i2 = 0;
                    while (i2 < size) {
                        NewsContent newsContent = content2.get(i2);
                        String type = newsContent.getType();
                        String value = newsContent.getValue();
                        if ("img".equals(type)) {
                            this.urls.add(value);
                            if (i2 + 1 < size) {
                                NewsContent newsContent2 = content2.get(i2 + 1);
                                String type2 = newsContent2.getType();
                                String value2 = newsContent2.getValue();
                                String subtype = newsContent2.getSubtype();
                                if ("txt".equals(type2) && "img_title".equals(subtype)) {
                                    this.summarys.put(this.urls.size() - 1, value2);
                                    i = i2 + 1;
                                    i2 = i + 1;
                                }
                            }
                        }
                        i = i2;
                        i2 = i + 1;
                    }
                    this.image_count = this.urls.size();
                    if (this.image_count == 0) {
                        this.error_layout.setVisibility(0);
                        b.a(this.info_url, "", "detail");
                        return;
                    }
                    this.num_tv.setText(Config.CHANNEL_ID);
                    this.count_tv.setText(getString(R.string.img_count, new Object[]{Integer.valueOf(this.image_count)}));
                    this.summary_tv.setText(this.summarys.get(0));
                    this.summary_tv.scrollTo(0, 0);
                    this.mAdapter = new com.qihoo360.daily.a.ab(getSupportFragmentManager(), this.mInfo, this.pics == null ? null : Arrays.asList(this.pics), this.urls, this.mHasMoreImgRec, this, this);
                    this.imgs_vp.setAdapter(this.mAdapter);
                    this.imgs_vp.setCurrentItem(this.p);
                    return;
                }
                Intent intent = new Intent(getApplicationContext(), SearchDetailActivity.class);
                intent.putExtra(SearchActivity.TAG_URL, wapurl);
                startActivity(intent);
                finish();
            }
        }
    }

    public void OnWeiboLoginCancel() {
    }

    public void OnWeiboLoginError(User user) {
    }

    public void OnWeiboLoginException(com.sina.weibo.sdk.c.c cVar) {
    }

    public void OnWeiboLoginSuccess(User user) {
        if (a.h(getApplicationContext())) {
            startActivityForResult(new Intent(getApplicationContext(), SendCmtActivity.class).putExtra("Info", this.mInfo).putExtra("content", this.content), 9);
        }
    }

    public void OnWeixinLoginCancel() {
    }

    public void OnWeixinLoginError(WXUser wXUser) {
    }

    public void OnWeixinLoginException(int i) {
    }

    public void OnWeixinLoginSuccess(WXUser wXUser) {
        if (a.h(getApplicationContext())) {
            startActivityForResult(new Intent(getApplicationContext(), SendCmtActivity.class).putExtra("Info", this.mInfo).putExtra("content", this.content), 9);
        }
    }

    public Info getInfo() {
        return this.mInfo;
    }

    public int getPosition() {
        return this.position;
    }

    /* access modifiers changed from: protected */
    public boolean hasActionbar() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (!(i2 != -1 || intent == null || ((NewComment) intent.getParcelableExtra(SendCmtActivity.TAG_DATA)) == null)) {
            this.mCmtTotal++;
            this.mInfo.setCmt_cnt(this.mCmtTotal + "");
            setCmtNum(this.mCmtTotal);
            Intent intent2 = new Intent();
            intent2.putExtra("Info", this.mInfo);
            intent2.putExtra("position", this.position);
            setResult(-1, intent2);
        }
        if (i == 9 && intent != null) {
            this.content = intent.getStringExtra("content");
        }
        if (i == 10 && intent != null) {
            this.mCmtTotal = intent.getIntExtra("cmt_cnt", this.mCmtTotal);
            setCmtNum(this.mCmtTotal);
            this.mInfo.setCmt_cnt(this.mCmtTotal + "");
            Intent intent3 = new Intent();
            intent3.putExtra("Info", this.mInfo);
            intent3.putExtra("position", this.position);
            setResult(-1, intent3);
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.error_layout:
                this.error_layout.setVisibility(8);
                this.loading_layout.setVisibility(0);
                String str = null;
                if (this.mInfo != null) {
                    str = this.mInfo.getPdate();
                }
                new s(getApplicationContext(), this.info_url, this.m, this.nid, this.newtrans, true, str, null).a(this.detailOnNetRequestListener, new Void[0]);
                return;
            case R.id.go_cmt:
                if (!this.cmtAble) {
                    ay.a(getApplicationContext()).a((int) R.string.cmt_close);
                    return;
                } else if (a.h(getApplicationContext())) {
                    startActivityForResult(new Intent(getApplicationContext(), SendCmtActivity.class).putExtra("Info", this.mInfo).putExtra("from", this.from).putExtra("content", this.content), 9);
                    return;
                } else {
                    login();
                    return;
                }
            case R.id.touch_iv:
            default:
                return;
            case R.id.tv_go_search:
                startActivity(new Intent(this, SearchActivity.class));
                b.b(this, "Bottom_menu_search");
                return;
            case R.id.tv_collect:
                collectNews();
                b.b(this, "Bottom_menu_bookmark");
                return;
            case R.id.go_share:
                new ai(this, this.mInfo, this.newsDetail, this.clickType, this.from).a();
                return;
            case R.id.go_more:
                new y(this, this, this.nid, this.isCollected, false).a();
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_imgs);
        setStatusBarColor(R.color.black);
        Intent intent = getIntent();
        this.mInfo = (Info) intent.getParcelableExtra("Info");
        this.position = intent.getIntExtra("position", -1);
        this.p = intent.getIntExtra("p", 0);
        this.pics = intent.getStringArrayExtra("pics");
        this.mHasMoreImgRec = intent.getBooleanExtra(HAS_MORE_IMG_REC, true);
        if (this.mInfo != null) {
            this.info_url = this.mInfo.getUrl();
            this.m = this.mInfo.getM();
            this.nid = this.mInfo.getNid();
            this.newtrans = this.mInfo.getNewtrans();
            if (Config.CHANNEL_ID.equals(this.mInfo.getNocmt())) {
                this.cmtAble = false;
            }
            this.nt = this.mInfo.getA();
        }
        initViews();
        initData();
        this.from = getIntent().getStringExtra("from");
        initToolbar();
    }

    public void onFavorChange(boolean z) {
        this.isCollected = z;
    }

    public boolean onLongClick(View view) {
        int currentItem = this.imgs_vp.getCurrentItem();
        if (this.urls == null || this.urls.size() <= currentItem || currentItem < 0) {
            return true;
        }
        new ag(this, this.urls.get(currentItem)).a();
        return true;
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        onClickMenu(menuItem);
        return super.onMenuItemClick(menuItem);
    }

    public void onPageScrollStateChanged(int i) {
    }

    public void onPageScrolled(int i, float f, int i2) {
    }

    public void onPageSelected(int i) {
        if (i < this.image_count) {
            this.lastItemPosition = i;
            this.mToolbar.getMenu().setGroupVisible(R.id.menu_group_images, true);
            this.mToolbar.setTitle("");
            if (this.mLastSelectedPosition >= this.image_count && this.bottom_bar.getVisibility() != 0) {
                this.bottom_bar.setVisibility(0);
                this.bottom_bar.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_bottom_in));
            }
        }
        if (i == this.image_count) {
            this.mToolbar.getMenu().setGroupVisible(R.id.menu_group_images, false);
            this.mToolbar.setTitle((int) R.string.imgs_title);
            if (this.bottom_bar.getVisibility() == 0) {
                this.bottom_bar.setVisibility(8);
            }
        }
        int i2 = i + 1;
        int visibility = this.title_layout.getVisibility();
        if (i2 <= this.image_count) {
            if (visibility == 8) {
                this.title_layout.setVisibility(0);
            }
            this.num_tv.setText(i2 + "");
            this.count_tv.setText(getString(R.string.img_count, new Object[]{Integer.valueOf(this.image_count)}));
            this.summary_tv.setText(this.summarys.get(i));
            this.summary_tv.scrollTo(0, 0);
        } else if (visibility == 0) {
            this.title_layout.setVisibility(8);
        }
        this.mLastSelectedPosition = i;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.mInfo != null) {
            new av(this, this.mInfo.getUrl(), this.from, this.mInfo.getNid(), this.mInfo.getA(), "b88f54f7f545e6b4", this.mInfo.getTitle(), null, null).a(null, new Void[0]);
        }
    }

    public void onPhotoViewClicked() {
        if (8 == this.bottom_bar.getVisibility()) {
            this.bottom_bar.setVisibility(0);
            this.bottom_bar.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_bottom_in));
            return;
        }
        this.bottom_bar.setVisibility(8);
        this.bottom_bar.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_bottom_out));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.mInfo != null) {
            new av(this, this.mInfo.getUrl(), this.from, this.mInfo.getNid(), this.mInfo.getA(), "b88f54f7f545e6b3", this.mInfo.getTitle(), null, null).a(null, new Void[0]);
        }
    }

    public void onTextSizeChange(WebSettings.TextSize textSize) {
    }
}
