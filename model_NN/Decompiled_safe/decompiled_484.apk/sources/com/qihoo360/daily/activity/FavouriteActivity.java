package com.qihoo360.daily.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import com.qihoo360.daily.R;
import com.qihoo360.daily.a.t;
import com.qihoo360.daily.a.x;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.g.o;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.model.FavouriteInfo;
import com.qihoo360.daily.model.Result;
import com.qihoo360.daily.widget.DiscoveryDialog;
import com.qihoo360.daily.widget.PullRecyclerView;
import com.qihoo360.daily.widget.RecyclerViewDivider.DividerItemDecoration;
import java.util.ArrayList;
import java.util.List;

public class FavouriteActivity extends BaseNoFragmentActivity {
    private boolean editMode = false;
    /* access modifiers changed from: private */
    public View error_layout;
    /* access modifiers changed from: private */
    public boolean isRequesting;
    /* access modifiers changed from: private */
    public t mAdapter;
    private View mDelete;
    private View mEmpty;
    /* access modifiers changed from: private */
    public boolean mEnd;
    /* access modifiers changed from: private */
    public View mLoadingLayout;
    private MenuItem mMenuItemEdit;
    /* access modifiers changed from: private */
    public PullRecyclerView mRecyclerView;

    private String getPage() {
        return this.mAdapter == null ? "0" : this.mAdapter.e();
    }

    /* access modifiers changed from: private */
    public void loadData() {
        this.isRequesting = true;
        new o(this).a(new c<Void, Result<ArrayList<FavouriteInfo>>>() {
            public void onNetRequest(int i, Result<ArrayList<FavouriteInfo>> result) {
                boolean unused = FavouriteActivity.this.isRequesting = false;
                if (FavouriteActivity.this.mLoadingLayout.getVisibility() == 0) {
                    FavouriteActivity.this.mLoadingLayout.setVisibility(8);
                }
                if (result != null && result.getStatus() == 0) {
                    FavouriteActivity.this.error_layout.setVisibility(8);
                    if (result.getData() == null || result.getData().isEmpty()) {
                        boolean unused2 = FavouriteActivity.this.mEnd = true;
                        FavouriteActivity.this.mAdapter.h();
                        return;
                    }
                    List<FavouriteInfo> b2 = FavouriteActivity.this.mAdapter.b();
                    if (b2 == null) {
                        b2 = result.getData();
                    } else if (FavouriteActivity.this.mAdapter.g()) {
                        b2.remove(FavouriteActivity.this.mAdapter.c());
                    }
                    d.a(FavouriteActivity.this.getBaseContext(), b2);
                    FavouriteActivity.this.mAdapter.a(result.getData());
                } else if (result != null) {
                    ay.a(FavouriteActivity.this).a(result.getMsg());
                } else {
                    List access$1000 = FavouriteActivity.this.loadLocalData();
                    if (access$1000 == null || access$1000.isEmpty()) {
                        FavouriteActivity.this.error_layout.setVisibility(0);
                    } else {
                        FavouriteActivity.this.mAdapter.a(access$1000);
                    }
                    boolean unused3 = FavouriteActivity.this.mEnd = true;
                    FavouriteActivity.this.mAdapter.h();
                }
            }

            public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
                onNetRequest(i, (Result<ArrayList<FavouriteInfo>>) ((Result) obj));
            }
        }, getPage());
    }

    /* access modifiers changed from: private */
    public List<FavouriteInfo> loadLocalData() {
        return d.d(getBaseContext());
    }

    /* access modifiers changed from: private */
    public void onConfirmCleanAll() {
        final DiscoveryDialog discoveryDialog = new DiscoveryDialog(this);
        discoveryDialog.setContentText((int) R.string.settings_favor_clear_all_confirm_msg);
        discoveryDialog.setOnDialogViewClickListener(new DiscoveryDialog.OnDialogViewClickListener() {
            public void onLeftButtonClick() {
                discoveryDialog.disMissDialog();
            }

            public void onRightButtonClick() {
                FavouriteActivity.this.mAdapter.d();
                discoveryDialog.disMissDialog();
            }
        });
        discoveryDialog.showDialog();
    }

    /* access modifiers changed from: private */
    public void onLoadMoreVisible() {
        if (!this.isRequesting && !this.mEnd) {
            loadMore();
        }
    }

    /* access modifiers changed from: private */
    public void switchView() {
        this.editMode = this.mAdapter.a();
        if (this.mAdapter.getItemCount() > 0) {
            this.mMenuItemEdit.setVisible(true);
            this.mMenuItemEdit.setIcon(this.editMode ? R.drawable.ic_actionbar_done : R.drawable.ic_actionbar_edit);
            this.mEmpty.setVisibility(8);
            return;
        }
        this.mDelete.setVisibility(8);
        this.mMenuItemEdit.setVisible(false);
        this.mEmpty.setVisibility(0);
    }

    public void backToTop() {
        if (this.mRecyclerView != null) {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    FavouriteActivity.this.mRecyclerView.scrollToPosition(0);
                }
            }, 100);
            this.mRecyclerView.smoothScrollToPosition(0);
        }
    }

    public void loadMore() {
        this.mAdapter.f();
        loadData();
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        FavouriteInfo favouriteInfo;
        if (1 == i && intent != null && (favouriteInfo = (FavouriteInfo) intent.getParcelableExtra("Info")) != null && this.mAdapter != null) {
            this.mAdapter.a(favouriteInfo);
        }
    }

    public void onBackPressed() {
        if (this.mAdapter.a()) {
            this.editMode = false;
            this.mAdapter.a(this.editMode);
            this.mDelete.setVisibility(8);
            switchView();
            return;
        }
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_favourite);
        this.mEmpty = findViewById(R.id.favor_emtpy);
        this.error_layout = findViewById(R.id.error_layout);
        this.error_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                FavouriteActivity.this.mLoadingLayout.setVisibility(0);
                FavouriteActivity.this.error_layout.setVisibility(8);
                FavouriteActivity.this.loadData();
            }
        });
        this.mDelete = findViewById(R.id.action_bar_delete);
        this.mDelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                FavouriteActivity.this.onConfirmCleanAll();
            }
        });
        Toolbar configToolbar = configToolbar(R.menu.menu_favor, R.string.favourite_title, R.drawable.ic_actionbar_back);
        configToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                FavouriteActivity.this.onBackPressed();
            }
        });
        this.mMenuItemEdit = configToolbar.getMenu().findItem(R.id.menu_item_favor_edit);
        this.mLoadingLayout = findViewById(R.id.loading_layout);
        View findViewById = findViewById(R.id.favour_delete);
        this.mRecyclerView = (PullRecyclerView) findViewById(R.id.recyclerView);
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(this) {
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(-1, -2);
            }
        });
        this.mAdapter = new t(this, null);
        this.mAdapter.a(findViewById);
        this.mAdapter.a(new x() {
            public void onDataSetChanged() {
                FavouriteActivity.this.switchView();
            }
        });
        switchView();
        this.mRecyclerView.setAdapter(this.mAdapter);
        this.mRecyclerView.addItemDecoration(new DividerItemDecoration(this, 1));
        this.mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                switch (i) {
                    case 0:
                        if (FavouriteActivity.this.mAdapter.getItemCount() > 0 && FavouriteActivity.this.mRecyclerView.getChildPosition(FavouriteActivity.this.mRecyclerView.getChildAt(FavouriteActivity.this.mRecyclerView.getChildCount() + -1)) >= FavouriteActivity.this.mAdapter.getItemCount() + -1) {
                            FavouriteActivity.this.onLoadMoreVisible();
                            return;
                        }
                        return;
                    case 1:
                    default:
                        return;
                }
            }

            public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            }
        });
        this.mLoadingLayout.setVisibility(0);
        loadData();
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        int i = 0;
        switch (menuItem.getItemId()) {
            case R.id.menu_item_favor_edit:
                this.editMode = !this.editMode;
                this.mAdapter.a(this.editMode);
                View view = this.mDelete;
                if (!this.editMode) {
                    i = 8;
                }
                view.setVisibility(i);
                menuItem.setIcon(this.editMode ? R.drawable.ic_actionbar_done : R.drawable.ic_actionbar_edit);
                break;
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String):java.lang.String
      com.qihoo360.daily.f.d.a(android.content.Context, int):void
      com.qihoo360.daily.f.d.a(android.content.Context, long):void
      com.qihoo360.daily.f.d.a(android.content.Context, com.qihoo360.daily.model.City):void
      com.qihoo360.daily.f.d.a(android.content.Context, com.qihoo360.daily.model.QdData):void
      com.qihoo360.daily.f.d.a(android.content.Context, com.qihoo360.daily.model.UpdateInfo):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.util.List<com.qihoo360.daily.model.FavouriteInfo>):void
      com.qihoo360.daily.f.d.a(android.content.Context, boolean):void */
    /* access modifiers changed from: protected */
    public void onPause() {
        d.a(getBaseContext(), false);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        String n = d.n(getBaseContext());
        if (!TextUtils.isEmpty(n)) {
            this.mAdapter.a(n);
        }
    }
}
