package com.qihoo360.daily.activity;

import android.content.Intent;
import android.os.Bundle;
import com.f.a.g;
import com.qihoo360.daily.h.v;
import com.qihoo360.daily.h.x;
import com.qihoo360.daily.i.b;
import com.qihoo360.daily.i.e;
import com.qihoo360.daily.widget.DialogView;
import com.qihoo360.daily.wxapi.WXHelper;

public abstract class BaseDetailActivity extends BaseActivity implements e, WXHelper.OnGetWeixinInfoCb {
    public static final int RESULT_TOKEN_TIME_OUT = 2;
    protected String clickType = "0";
    private DialogView mLoginDialog;

    private void login(boolean z) {
        v vVar = new v(this, this, this, z);
        this.mLoginDialog = vVar.b();
        vVar.a();
    }

    private void login(boolean z, x xVar) {
        v vVar = new v(this, this, this, z);
        vVar.a(xVar);
        this.mLoginDialog = vVar.b();
        vVar.a();
    }

    public void login() {
        login(false);
    }

    /* access modifiers changed from: protected */
    public void login(x xVar) {
        login(false, xVar);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (this.mLoginDialog != null) {
            this.mLoginDialog.disMissDialog();
        }
        b.INSTANCE.a(i, i2, intent);
        super.onActivityResult(i, i2, intent);
        if (2 == i2) {
            b.INSTANCE.a(this);
            WXHelper.INSTANCE.logoutWeixin(this);
            login(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!(this instanceof ImagesActivity)) {
            g.b(getClass().getSimpleName());
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!(this instanceof ImagesActivity)) {
            g.a(getClass().getSimpleName());
        }
    }
}
