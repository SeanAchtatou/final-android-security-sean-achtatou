package com.qihoo360.daily.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.qihoo.dynamic.DotCount;
import com.qihoo360.daily.R;
import com.qihoo360.daily.h.ay;
import com.sina.weibo.sdk.api.a.c;
import com.sina.weibo.sdk.api.a.f;
import com.sina.weibo.sdk.api.a.g;
import com.sina.weibo.sdk.api.a.p;

public class WeiboEntryActivity extends Activity implements f {
    private g weiboAPI = null;

    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.weiboAPI = p.a(this, "432085026", false);
        try {
            this.weiboAPI.a(getIntent(), this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.weiboAPI.a(intent, this);
    }

    public void onResponse(c cVar) {
        int i = 0;
        switch (cVar.f1182b) {
            case DotCount.VXERR_OUTOFMEMORY:
                i = R.string.share_deny;
                break;
            case -2:
                i = R.string.share_cancel;
                break;
            case 0:
                i = R.string.share_ok;
                break;
        }
        if (i != 0) {
            ay.a(getApplicationContext()).a(i);
        }
        finish();
    }
}
