package com.qihoo360.daily.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Process;
import android.text.TextUtils;
import com.a.a.j;
import com.mediav.ads.sdk.adcore.Mvad;
import com.mediav.ads.sdk.interfaces.IMvLandingPageListener;
import com.mediav.ads.sdk.interfaces.IMvLandingPageView;
import com.qihoo.qplayer.QHPlayerSDK;
import com.qihoo360.daily.f.a;
import com.qihoo360.daily.h.ad;
import com.qihoo360.daily.h.az;
import com.qihoo360.daily.h.k;
import com.qihoo360.daily.receiver.WakeUpPushReceiver;

public class Application extends android.app.Application {
    public static boolean DEBUG_SWITCHER = false;
    public static final String START_PUSH = "START_PUSH";
    private static j mGson = null;
    private static Application minstance = null;
    /* access modifiers changed from: private */
    public String ad_url;
    public k crashHandler = null;
    /* access modifiers changed from: private */
    public IMvLandingPageListener iMvLandingPageListener;

    public Application() {
        minstance = this;
    }

    public static j getGson() {
        if (mGson == null) {
            mGson = new j();
        }
        return mGson;
    }

    public static synchronized Application getInstance() {
        Application application;
        synchronized (Application.class) {
            application = minstance;
        }
        return application;
    }

    private void startPush() {
        Intent intent = new Intent(this, WakeUpPushReceiver.class);
        intent.setAction(START_PUSH);
        sendBroadcast(intent);
    }

    public static String toJson(Object obj) {
        j gson = getGson();
        if (gson == null || obj == null) {
            return null;
        }
        try {
            return gson.a(obj);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void clearAdData() {
        this.iMvLandingPageListener = null;
        this.ad_url = null;
    }

    public void onCreate() {
        super.onCreate();
        this.crashHandler = k.a();
        this.crashHandler.a(getApplicationContext());
        QHPlayerSDK.getInstance().init(this, "i0NJWYijyJZ6yUO0Itsaww==");
        QHPlayerSDK._DEBUG = false;
        a.i(this);
        mGson = new j();
        startPush();
        ad.a("my pid:" + Process.myPid());
        az.a(this);
        Mvad.setLandingPageView(getApplicationContext(), new IMvLandingPageView() {
            public void open(Context context, String str, IMvLandingPageListener iMvLandingPageListener) {
                IMvLandingPageListener unused = Application.this.iMvLandingPageListener = iMvLandingPageListener;
                String unused2 = Application.this.ad_url = str;
                ad.a("open ad url:" + str);
                Intent intent = new Intent(Application.this.getApplicationContext(), SearchDetailActivity.class);
                intent.putExtra(SearchActivity.TAG_URL, str);
                intent.setFlags(268435456);
                Application.this.startActivity(intent);
            }
        });
    }

    public void onPageClose(String str) {
        if (this.iMvLandingPageListener != null && !TextUtils.isEmpty(str) && str.equals(this.ad_url)) {
            this.iMvLandingPageListener.onPageClose();
        }
    }

    public void onPageLoadFailed(String str) {
        if (this.iMvLandingPageListener != null && !TextUtils.isEmpty(str) && str.equals(this.ad_url)) {
            this.iMvLandingPageListener.onPageLoadFailed();
        }
    }

    public void onPageLoadFinished(String str) {
        if (this.iMvLandingPageListener != null && !TextUtils.isEmpty(str) && str.equals(this.ad_url)) {
            this.iMvLandingPageListener.onPageLoadFinished();
        }
    }
}
