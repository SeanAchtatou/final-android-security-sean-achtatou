package com.qihoo360.daily.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.qihoo360.daily.h.ba;
import com.qihoo360.daily.model.UpdateInfo;

public class NotificationActivity extends Activity {
    public static final String EXTRA_DOWNLOAD = "EXTRA_DOWNLOAD";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        if (intent != null) {
            try {
                if (intent.getParcelableExtra(EXTRA_DOWNLOAD) != null) {
                    new ba(this, (UpdateInfo) intent.getParcelableExtra(EXTRA_DOWNLOAD)).a(true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }
}
