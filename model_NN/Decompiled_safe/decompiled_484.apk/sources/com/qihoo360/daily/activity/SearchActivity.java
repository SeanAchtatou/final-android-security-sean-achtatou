package com.qihoo360.daily.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.qihoo360.daily.R;
import com.qihoo360.daily.a.ag;
import com.qihoo360.daily.a.ai;
import com.qihoo360.daily.a.y;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.g.q;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.h.bj;
import com.qihoo360.daily.model.HotNews;
import com.qihoo360.daily.model.Suggest;
import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends BaseNoFragmentActivity {
    private static final int HOT_NEWS_SIZE = 6;
    private static final String SEARCH_API = "http://m.haosou.com/s?src=discovery&mso_app=1&offset=0&q=%s";
    public static final String TAG_SEARCH = "tag_search";
    public static final String TAG_URL = "url";
    /* access modifiers changed from: private */
    public Context mContext;
    private View mHotArea;
    /* access modifiers changed from: private */
    public GridView mHots;
    private c<Void, List<HotNews>> mKeywordsOnNetRequestListener = new c<Void, List<HotNews>>() {
        public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
            onNetRequest(i, (List<HotNews>) ((List) obj));
        }

        public void onNetRequest(int i, List<HotNews> list) {
            if (list != null && list.size() > 0) {
                d.f(SearchActivity.this, Application.getGson().a(list));
                SearchActivity.this.refreshHotNews();
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean mLoadError;
    private AdapterView.OnItemClickListener mOnItemClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            String str;
            if (adapterView instanceof GridView) {
                str = ((HotNews) SearchActivity.this.mHots.getAdapter().getItem(i)).getKeyword();
                b.b(SearchActivity.this.mContext, "Search_news_hot_onClick");
            } else {
                str = (String) SearchActivity.this.mSuggest.getAdapter().getItem(i);
            }
            SearchActivity.this.showWebViewResult(str, null);
        }
    };
    /* access modifiers changed from: private */
    public SearchView mSearchView;
    /* access modifiers changed from: private */
    public ListView mSuggest;
    /* access modifiers changed from: private */
    public c<Void, Suggest> mSuggestNetRequestListener = new c<Void, Suggest>() {
        public void onNetRequest(int i, Suggest suggest) {
            String charSequence = SearchActivity.this.mSearchView.getQuery().toString();
            if (!TextUtils.isEmpty(charSequence) && suggest != null) {
                String[] s = suggest.getS();
                String q = suggest.getQ();
                if (s != null && s.length > 0 && q != null && q.equals(charSequence)) {
                    ((ag) SearchActivity.this.mSuggest.getAdapter()).a(suggest);
                    SearchActivity.this.switchContent(Mode.SUGGEST);
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public ProgressBar mWebLoadingProgressBar;
    /* access modifiers changed from: private */
    public View mWebNetError;
    private WebView mWebView;

    enum Mode {
        HOTNEWS,
        SUGGEST,
        RESULT
    }

    class MyWebChromeClient extends WebChromeClient {
        private MyWebChromeClient() {
        }

        public void onProgressChanged(WebView webView, int i) {
            super.onProgressChanged(webView, i);
            SearchActivity.this.mWebLoadingProgressBar.setProgress(i);
            if (i >= 100 && SearchActivity.this.mWebLoadingProgressBar.getVisibility() == 0) {
                SearchActivity.this.mWebLoadingProgressBar.setVisibility(8);
            } else if (SearchActivity.this.mWebLoadingProgressBar.getVisibility() == 8) {
                SearchActivity.this.mWebLoadingProgressBar.setVisibility(0);
            }
        }
    }

    class MyWebViewClient extends WebViewClient {
        private MyWebViewClient() {
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            a.b((Activity) SearchActivity.this);
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            super.onPageStarted(webView, str, bitmap);
            if (SearchActivity.this.mWebNetError.getVisibility() == 0 && !SearchActivity.this.mLoadError) {
                SearchActivity.this.mWebNetError.setVisibility(8);
            }
            a.b((Activity) SearchActivity.this);
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            super.onReceivedError(webView, i, str, str2);
            boolean unused = SearchActivity.this.mLoadError = true;
            webView.stopLoading();
            webView.clearView();
            webView.loadUrl("about:blank");
            if (SearchActivity.this.mWebNetError.getVisibility() == 8) {
                SearchActivity.this.mWebNetError.setVisibility(0);
            }
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            Intent intent = new Intent(SearchActivity.this, SearchDetailActivity.class);
            intent.putExtra(SearchActivity.TAG_URL, str);
            SearchActivity.this.startActivity(intent);
            return true;
        }
    }

    private List<HotNews> getShowingHotNews(List<HotNews> list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(6);
        for (int i = 0; i < 6; i++) {
            int size = list.size();
            if (size > 0) {
                arrayList.add(list.remove((int) (Math.random() * ((double) size))));
            }
        }
        return arrayList;
    }

    @TargetApi(11)
    private void init() {
        Toolbar configToolbar = configToolbar(R.menu.menu_search, 0, R.drawable.ic_actionbar_back);
        configToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                a.b((Activity) SearchActivity.this);
                SearchActivity.this.finish();
            }
        });
        this.mSearchView = (SearchView) MenuItemCompat.getActionView(configToolbar.getMenu().findItem(R.id.menu_item_action_search));
        this.mSearchView.setIconified(false);
        this.mSearchView.clearFocus();
        this.mSearchView.setQueryHint("搜索相关内容");
        final ImageView imageView = (ImageView) this.mSearchView.findViewById(R.id.search_close_btn);
        this.mSearchView.findViewById(R.id.search_plate).setBackgroundResource(R.drawable.search_bottom_border);
        this.mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            public boolean onClose() {
                SearchActivity.this.mSearchView.setTag(null);
                return true;
            }
        });
        this.mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            /* JADX WARNING: Code restructure failed: missing block: B:8:0x002c, code lost:
                if (android.text.TextUtils.isEmpty(r6) == false) goto L_0x002e;
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public boolean onQueryTextChange(java.lang.String r6) {
                /*
                    r5 = this;
                    r4 = 1
                    r3 = 0
                    android.widget.ImageView r0 = r0
                    if (r0 == 0) goto L_0x0012
                    boolean r0 = android.text.TextUtils.isEmpty(r6)
                    if (r0 == 0) goto L_0x002f
                    android.widget.ImageView r0 = r0
                    r1 = 4
                    r0.setVisibility(r1)
                L_0x0012:
                    com.qihoo360.daily.activity.SearchActivity r0 = com.qihoo360.daily.activity.SearchActivity.this
                    android.support.v7.widget.SearchView r0 = r0.mSearchView
                    java.lang.Object r0 = r0.getTag()
                    if (r0 == 0) goto L_0x0035
                    com.qihoo360.daily.activity.SearchActivity r0 = com.qihoo360.daily.activity.SearchActivity.this
                    android.support.v7.widget.SearchView r0 = r0.mSearchView
                    r1 = 0
                    r0.setTag(r1)
                    boolean r0 = android.text.TextUtils.isEmpty(r6)
                    if (r0 != 0) goto L_0x0035
                L_0x002e:
                    return r4
                L_0x002f:
                    android.widget.ImageView r0 = r0
                    r0.setVisibility(r3)
                    goto L_0x0012
                L_0x0035:
                    boolean r0 = android.text.TextUtils.isEmpty(r6)
                    if (r0 == 0) goto L_0x0043
                    com.qihoo360.daily.activity.SearchActivity r0 = com.qihoo360.daily.activity.SearchActivity.this
                    com.qihoo360.daily.activity.SearchActivity$Mode r1 = com.qihoo360.daily.activity.SearchActivity.Mode.HOTNEWS
                    r0.switchContent(r1)
                    goto L_0x002e
                L_0x0043:
                    com.qihoo360.daily.g.ad r0 = new com.qihoo360.daily.g.ad
                    com.qihoo360.daily.activity.SearchActivity r1 = com.qihoo360.daily.activity.SearchActivity.this
                    java.lang.String r2 = r6.toString()
                    r0.<init>(r1, r2)
                    com.qihoo360.daily.activity.SearchActivity r1 = com.qihoo360.daily.activity.SearchActivity.this
                    com.qihoo360.daily.d.c r1 = r1.mSuggestNetRequestListener
                    java.lang.Void[] r2 = new java.lang.Void[r3]
                    r0.a(r1, r3, r2)
                    goto L_0x002e
                */
                throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.activity.SearchActivity.AnonymousClass3.onQueryTextChange(java.lang.String):boolean");
            }

            public boolean onQueryTextSubmit(String str) {
                String charSequence = SearchActivity.this.mSearchView.getQuery().toString();
                if (TextUtils.isEmpty(charSequence)) {
                    return false;
                }
                a.b((Activity) SearchActivity.this);
                SearchActivity.this.showWebViewResult(charSequence, null);
                return true;
            }
        });
        this.mWebView = (WebView) findViewById(R.id.search_web_view);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.setWebViewClient(new MyWebViewClient());
        this.mWebView.setWebChromeClient(new MyWebChromeClient());
        try {
            if (bj.a()) {
                this.mWebView.removeJavascriptInterface("searchBoxJavaBridge_");
                this.mWebView.removeJavascriptInterface("accessibility");
                this.mWebView.removeJavascriptInterface("accessibilityTraversal");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (Build.VERSION.SDK_INT < 16) {
            this.mWebView.setBackgroundColor(0);
        } else {
            this.mWebView.setBackgroundColor(Color.argb(1, 0, 0, 0));
        }
        this.mHots = (GridView) findViewById(R.id.search_hot);
        this.mHotArea = findViewById(R.id.layout_search_hot);
        this.mSuggest = (ListView) findViewById(R.id.search_suggest);
        this.mHots.setOnItemClickListener(this.mOnItemClickListener);
        this.mSuggest.setOnItemClickListener(this.mOnItemClickListener);
        this.mWebLoadingProgressBar = (ProgressBar) findViewById(R.id.web_load_progress);
        this.mWebNetError = findViewById(R.id.web_net_error);
        ag agVar = new ag(null);
        agVar.a(new ai() {
            public void onGoto(String str) {
                if (!TextUtils.isEmpty(str)) {
                    SearchActivity.this.mSearchView.setQuery(str, false);
                }
            }
        });
        this.mSuggest.setAdapter((ListAdapter) agVar);
        this.mSearchView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                boolean unused = SearchActivity.this.mLoadError = false;
            }
        });
        refreshHotNews();
        switchContent(Mode.HOTNEWS);
        Intent intent = getIntent();
        if (intent == null || TextUtils.isEmpty(intent.getStringExtra(TAG_SEARCH))) {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    SearchActivity.this.showSoftInputMethod(SearchActivity.this.mSearchView);
                }
            }, 300);
            if (imageView != null) {
                imageView.setVisibility(4);
                return;
            }
            return;
        }
        showWebViewResult(intent.getStringExtra(TAG_SEARCH), intent.getStringExtra(TAG_URL));
    }

    /* access modifiers changed from: private */
    public void refreshHotNews() {
        List list;
        try {
            list = (List) Application.getGson().a(d.c(this), new com.a.a.c.a<List<HotNews>>() {
            }.getType());
        } catch (Exception e) {
            e.printStackTrace();
            list = null;
        }
        List<HotNews> showingHotNews = getShowingHotNews(list);
        if (showingHotNews != null) {
            this.mHots.setAdapter((ListAdapter) new y(this, showingHotNews));
        } else {
            new q(this).a(this.mKeywordsOnNetRequestListener, 0, new Void[0]);
        }
    }

    private void setSearchContentSilence(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.mSearchView.setTag(true);
            this.mSearchView.setQuery(str, false);
        }
    }

    private void setWebViewVisibility(int i) {
        if (this.mWebView != null) {
            this.mWebView.setVisibility(i);
        }
    }

    /* access modifiers changed from: private */
    public void showSoftInputMethod(View view) {
        ((InputMethodManager) getSystemService("input_method")).toggleSoftInput(2, 1);
    }

    /* access modifiers changed from: private */
    public void showWebViewResult(String str, String str2) {
        switchContent(Mode.RESULT);
        setSearchContentSilence(str);
        if (TextUtils.isEmpty(str2)) {
            str2 = String.format(SEARCH_API, str);
        }
        this.mWebView.loadUrl(str2);
    }

    /* access modifiers changed from: private */
    public void switchContent(Mode mode) {
        this.mWebLoadingProgressBar.setVisibility(8);
        if (this.mWebNetError.getVisibility() == 0) {
            this.mWebNetError.setVisibility(8);
        }
        switch (mode) {
            case HOTNEWS:
                this.mHotArea.setVisibility(0);
                this.mSuggest.setVisibility(8);
                setWebViewVisibility(8);
                return;
            case SUGGEST:
                this.mHotArea.setVisibility(8);
                this.mSuggest.setVisibility(0);
                setWebViewVisibility(8);
                return;
            case RESULT:
                this.mHotArea.setVisibility(8);
                this.mSuggest.setVisibility(8);
                setWebViewVisibility(0);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mContext = this;
        setContentView((int) R.layout.activity_search);
        init();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.mWebView != null) {
            this.mWebView.stopLoading();
            this.mWebView.clearCache(true);
            this.mWebView.destroyDrawingCache();
            this.mWebView.setWebChromeClient(null);
            this.mWebView.setWebViewClient(null);
            this.mWebView.removeAllViews();
            this.mWebView.destroy();
            this.mWebView = null;
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    @TargetApi(11)
    public void onPause() {
        this.mWebView.pauseTimers();
        if (bj.a()) {
            this.mWebView.onPause();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    @TargetApi(11)
    public void onResume() {
        super.onResume();
        if (bj.a()) {
            this.mWebView.onResume();
        }
        this.mWebView.resumeTimers();
    }
}
