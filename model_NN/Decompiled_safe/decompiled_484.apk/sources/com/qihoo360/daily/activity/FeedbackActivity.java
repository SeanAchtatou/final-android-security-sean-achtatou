package com.qihoo360.daily.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import com.qihoo360.daily.R;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.g.bd;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.model.Result;

public class FeedbackActivity extends BaseNoFragmentActivity {
    /* access modifiers changed from: private */
    public EditText mContact;
    /* access modifiers changed from: private */
    public EditText mContent;
    /* access modifiers changed from: private */
    public c<Void, Result> onNetRequestListener = new c<Void, Result>() {
        public void onNetRequest(int i, Result result) {
            if (result != null) {
                FeedbackActivity.this.mContent.setText((CharSequence) null);
                FeedbackActivity.this.mContact.setText((CharSequence) null);
                FeedbackActivity.this.finish();
                ay.a(FeedbackActivity.this.getApplicationContext()).a((int) R.string.settings_feedback_succ);
                return;
            }
            ay.a(FeedbackActivity.this.getApplicationContext()).a((int) R.string.net_error);
        }
    };

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("意见反馈");
        toolbar.setNavigationIcon((int) R.drawable.ic_actionbar_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                FeedbackActivity.this.onBackPressed();
            }
        });
        toolbar.inflateMenu(R.menu.menu_feedback);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.menu_item_action_send:
                        String trim = FeedbackActivity.this.mContent.getText().toString().trim();
                        String trim2 = FeedbackActivity.this.mContact.getText().toString().trim();
                        if (TextUtils.isEmpty(trim)) {
                            ay.a(FeedbackActivity.this.getApplicationContext()).a((int) R.string.settings_feedback_input_empty);
                            FeedbackActivity.this.mContent.requestFocus();
                            return true;
                        }
                        new bd(FeedbackActivity.this, trim, trim2).a(FeedbackActivity.this.onNetRequestListener, new Void[0]);
                        return false;
                    default:
                        return false;
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_feedback);
        this.mContent = (EditText) findViewById(R.id.feedback_content);
        this.mContact = (EditText) findViewById(R.id.feedback_contact);
        initToolbar();
    }
}
