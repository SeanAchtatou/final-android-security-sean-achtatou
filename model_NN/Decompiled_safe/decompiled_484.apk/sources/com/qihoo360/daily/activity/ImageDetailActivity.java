package com.qihoo360.daily.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.fragment.TouchGifFragment;
import com.qihoo360.daily.fragment.TouchImageFragment2;
import com.qihoo360.daily.h.ag;
import java.util.ArrayList;

public class ImageDetailActivity extends BaseActivity implements View.OnLongClickListener {
    public static final String DEFAULT_INDEX = "index";
    public static final String IMAGE_URLS = "urls";
    private int index;
    private ViewPagerAdapter mAdapter;
    private FragmentManager mFm;
    /* access modifiers changed from: private */
    public int mLastSelectedPosition;
    private ViewPager mPager;
    /* access modifiers changed from: private */
    public TextView mTvIndex;
    /* access modifiers changed from: private */
    public ArrayList<String> urls;

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        public ViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public int getCount() {
            return ImageDetailActivity.this.urls.size();
        }

        public Fragment getItem(int i) {
            String str = (String) ImageDetailActivity.this.urls.get(i);
            if (str == null || !str.contains(".gif")) {
                TouchImageFragment2 touchImageFragment2 = new TouchImageFragment2();
                touchImageFragment2.setOnLongClickListener(ImageDetailActivity.this);
                Bundle bundle = new Bundle();
                touchImageFragment2.setArguments(bundle);
                bundle.putString(SearchActivity.TAG_URL, str);
                return touchImageFragment2;
            }
            TouchGifFragment touchGifFragment = new TouchGifFragment();
            Bundle bundle2 = new Bundle();
            touchGifFragment.setArguments(bundle2);
            bundle2.putString(SearchActivity.TAG_URL, str);
            return touchGifFragment;
        }

        public Object instantiateItem(ViewGroup viewGroup, int i) {
            return super.instantiateItem(viewGroup, i);
        }
    }

    public void finish() {
        super.finish();
        overridePendingTransition(0, R.anim.activity_zoom_out);
    }

    /* access modifiers changed from: protected */
    public boolean hasActionbar() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_image_detail);
        setStatusBarColor(R.color.black);
        this.mPager = (ViewPager) findViewById(R.id.pager);
        this.mTvIndex = (TextView) findViewById(R.id.index);
        if (getIntent() != null) {
            this.index = getIntent().getIntExtra("index", 0);
            this.urls = getIntent().getStringArrayListExtra(IMAGE_URLS);
            this.mLastSelectedPosition = this.index;
        }
        this.mTvIndex.setText((this.index + 1) + "/" + this.urls.size());
        this.mFm = getSupportFragmentManager();
        this.mAdapter = new ViewPagerAdapter(this.mFm);
        this.mPager.setAdapter(this.mAdapter);
        this.mPager.setCurrentItem(this.index, false);
        this.mPager.setPageMargin(getResources().getDimensionPixelSize(R.dimen.margin_small));
        this.mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            public void onPageSelected(final int i) {
                ImageDetailActivity.this.mTvIndex.setText((i + 1) + "/" + ImageDetailActivity.this.urls.size());
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        int unused = ImageDetailActivity.this.mLastSelectedPosition = i;
                    }
                }, 100);
            }
        });
        overridePendingTransition(R.anim.activity_zoom_in, 0);
    }

    public boolean onLongClick(View view) {
        int currentItem = this.mPager.getCurrentItem();
        if (this.urls == null || this.urls.size() <= currentItem || currentItem < 0) {
            return true;
        }
        new ag(this, this.urls.get(currentItem)).a();
        return true;
    }
}
