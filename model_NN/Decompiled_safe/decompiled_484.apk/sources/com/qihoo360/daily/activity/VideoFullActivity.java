package com.qihoo360.daily.activity;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.qihoo.qplayer.QihooMediaPlayer;
import com.qihoo.qplayer.view.QihooVideoView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.h.bk;
import com.qihoo360.daily.h.i;
import com.qihoo360.daily.widget.VerticalSeekBar;
import java.lang.ref.WeakReference;

public class VideoFullActivity extends Activity {
    private static final int MSG_HIDE_CONTROLLER = 1;
    private static final int MSG_TOAST_ERROR = 2;
    /* access modifiers changed from: private */
    public static ImageButton ibPlay;
    /* access modifiers changed from: private */
    public static QihooVideoView videoView;
    /* access modifiers changed from: private */
    public boolean adjustVolume = false;
    private final float alphaController = 0.3f;
    private final int controllerBottomMarginDp = 18;
    private FrameLayout decorView;
    private int defaultBright = 0;
    /* access modifiers changed from: private */
    public ControllerHandler handler = new ControllerHandler(new WeakReference(this));
    /* access modifiers changed from: private */
    public ImageButton ibBrightness;
    /* access modifiers changed from: private */
    public ImageButton ibVolume;
    private boolean isBrightnessAuto = false;
    /* access modifiers changed from: private */
    public boolean isPrepared = false;
    private ProgressBar pbBuffer;
    /* access modifiers changed from: private */
    public int position;
    /* access modifiers changed from: private */
    public RelativeLayout rlBuffer;
    private RelativeLayout rlController;
    private RelativeLayout rlControllerBottom;
    /* access modifiers changed from: private */
    public RelativeLayout rlControllerTop;
    private RelativeLayout rlVideoBottom;
    /* access modifiers changed from: private */
    public SeekBar sbProgress;
    private String title;
    /* access modifiers changed from: private */
    public TextView tvAllProgress;
    /* access modifiers changed from: private */
    public TextView tvBuffer;
    /* access modifiers changed from: private */
    public TextView tvCurrentProgress;
    private String url;
    /* access modifiers changed from: private */
    public VerticalSeekBar vsbBrightness;
    /* access modifiers changed from: private */
    public VerticalSeekBar vsbVolume;

    class ControllerHandler extends Handler {
        private WeakReference<VideoFullActivity> reference;

        public ControllerHandler(WeakReference<VideoFullActivity> weakReference) {
            this.reference = weakReference;
        }

        public void handleMessage(Message message) {
            VideoFullActivity videoFullActivity = this.reference.get();
            switch (message.what) {
                case 1:
                    if (videoFullActivity != null) {
                        videoFullActivity.hideBrightness();
                        videoFullActivity.hideVolume();
                        videoFullActivity.hideController();
                        return;
                    }
                    return;
                case 2:
                    if (videoFullActivity != null) {
                        ay.a(videoFullActivity).a((int) R.string.can_not_play);
                        videoFullActivity.hideBuffer();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public int getCurrentVolume() {
        return ((AudioManager) getSystemService("audio")).getStreamVolume(3);
    }

    private void getData() {
        Intent intent = getIntent();
        this.position = intent.getIntExtra("position", 0);
        this.url = intent.getStringExtra(SearchActivity.TAG_URL);
        this.url = bk.a(this.url);
        this.title = intent.getStringExtra("title");
    }

    /* access modifiers changed from: private */
    public int getMaxVolume() {
        return ((AudioManager) getSystemService("audio")).getStreamMaxVolume(3);
    }

    /* access modifiers changed from: private */
    public int getScreenBrightness() {
        try {
            return Settings.System.getInt(getContentResolver(), "screen_brightness");
        } catch (Exception e) {
            e.printStackTrace();
            return 255;
        }
    }

    private int getScreenMode() {
        try {
            return Settings.System.getInt(getContentResolver(), "screen_brightness_mode");
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /* access modifiers changed from: private */
    public void hideBrightness() {
        if (this.vsbBrightness.isShown()) {
            this.vsbBrightness.setVisibility(8);
            this.ibBrightness.setBackgroundResource(R.drawable.ib_brightness_full_video);
        }
    }

    /* access modifiers changed from: private */
    public void hideBuffer() {
        if (this.rlBuffer != null) {
            this.rlBuffer.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void hideController() {
        this.rlControllerTop.setVisibility(8);
        this.rlControllerBottom.setVisibility(8);
        this.rlControllerTop.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_top_out));
        this.rlControllerBottom.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_bottom_out));
    }

    /* access modifiers changed from: private */
    public void hideControllerMessage() {
        if (this.handler != null) {
            this.handler.removeCallbacksAndMessages(null);
            this.handler.sendEmptyMessageDelayed(1, 5000);
        }
    }

    /* access modifiers changed from: private */
    public void hideVolume() {
        if (this.vsbVolume.isShown()) {
            this.vsbVolume.setVisibility(8);
            initIbVolumeBack();
        }
    }

    private void initIbVolumeBack() {
        if (getCurrentVolume() == 0) {
            this.ibVolume.setBackgroundResource(R.drawable.ib_volume_silence_video);
        } else {
            this.ibVolume.setBackgroundResource(R.drawable.ib_volume_default_video);
        }
    }

    private void initScreenBrightness() {
        if (getScreenMode() == 1) {
            setScreenMode(0);
            this.isBrightnessAuto = true;
            return;
        }
        this.isBrightnessAuto = false;
        this.defaultBright = getScreenBrightness();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.FrameLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void initView() {
        initScreenBrightness();
        this.decorView = (FrameLayout) getWindow().getDecorView();
        videoView = new QihooVideoView(this);
        videoView.setKeepScreenOn(true);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        layoutParams.gravity = 17;
        this.decorView.addView(videoView, layoutParams);
        this.rlController = (RelativeLayout) LayoutInflater.from(this).inflate((int) R.layout.video_full, (ViewGroup) this.decorView, false);
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-1, -1);
        layoutParams2.bottomMargin = bk.a(this, 18.0f);
        this.decorView.addView(this.rlController, layoutParams2);
        this.rlControllerTop = (RelativeLayout) findViewById(R.id.rl_controller_top);
        this.rlControllerBottom = (RelativeLayout) findViewById(R.id.rl_controller_bottom);
        this.rlVideoBottom = (RelativeLayout) findViewById(R.id.rl_video_bottom);
        ((TextView) findViewById(R.id.tv_title)).setText(this.title);
        ((ImageButton) findViewById(R.id.ib_return)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                VideoFullActivity.this.finish();
            }
        });
        ibPlay = (ImageButton) findViewById(R.id.ib_play_full);
        this.tvCurrentProgress = (TextView) findViewById(R.id.tv_current_progress_full);
        this.tvAllProgress = (TextView) findViewById(R.id.tv_total_progress_full);
        this.sbProgress = (SeekBar) findViewById(R.id.sb_progress_full);
        this.sbProgress.setEnabled(false);
        this.ibBrightness = (ImageButton) findViewById(R.id.ib_brightness_full);
        this.ibVolume = (ImageButton) findViewById(R.id.ib_volume_full);
        initIbVolumeBack();
        this.vsbBrightness = (VerticalSeekBar) findViewById(R.id.vsb_brightness_full);
        this.vsbVolume = (VerticalSeekBar) findViewById(R.id.vsb_volume_full);
        this.vsbBrightness.setMax(100);
        this.vsbVolume.setMax(100);
        this.ibBrightness.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (VideoFullActivity.this.vsbBrightness.isShown()) {
                    VideoFullActivity.this.vsbBrightness.setVisibility(8);
                    VideoFullActivity.this.ibBrightness.setBackgroundResource(R.drawable.ib_brightness_full_video);
                } else {
                    VideoFullActivity.this.vsbBrightness.setVisibility(0);
                    VideoFullActivity.this.vsbBrightness.setProgress((int) (((((float) VideoFullActivity.this.getScreenBrightness()) * 1.0f) / 255.0f) * ((float) VideoFullActivity.this.vsbBrightness.getMax())));
                    VideoFullActivity.this.hideVolume();
                    VideoFullActivity.this.ibBrightness.setBackgroundResource(R.drawable.ib_brightness_selected_video);
                }
                VideoFullActivity.this.hideControllerMessage();
            }
        });
        this.ibVolume.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (VideoFullActivity.this.vsbVolume.isShown()) {
                    VideoFullActivity.this.vsbVolume.setVisibility(8);
                    if (VideoFullActivity.this.getCurrentVolume() == 0) {
                        VideoFullActivity.this.ibVolume.setBackgroundResource(R.drawable.ib_volume_silence_video);
                    } else {
                        VideoFullActivity.this.ibVolume.setBackgroundResource(R.drawable.ib_volume_default_video);
                    }
                } else {
                    VideoFullActivity.this.vsbVolume.setVisibility(0);
                    if (VideoFullActivity.this.getCurrentVolume() == 0) {
                        VideoFullActivity.this.ibVolume.setBackgroundResource(R.drawable.ib_volume_silence_selected_video);
                    } else {
                        VideoFullActivity.this.ibVolume.setBackgroundResource(R.drawable.ib_volume_selected_video);
                    }
                    VideoFullActivity.this.vsbVolume.setProgress((int) (((((float) VideoFullActivity.this.getCurrentVolume()) * 1.0f) / ((float) VideoFullActivity.this.getMaxVolume())) * ((float) VideoFullActivity.this.vsbVolume.getMax())));
                    VideoFullActivity.this.hideBrightness();
                }
                VideoFullActivity.this.hideControllerMessage();
            }
        });
        this.vsbBrightness.setOnSeekBarChangeListener(new VerticalSeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(VerticalSeekBar verticalSeekBar, int i, boolean z) {
                if (z) {
                    VideoFullActivity.this.saveScreenBrightness((int) (((((float) i) * 1.0f) / ((float) verticalSeekBar.getMax())) * 255.0f));
                }
            }

            public void onStartTrackingTouch(VerticalSeekBar verticalSeekBar) {
            }

            public void onStopTrackingTouch(VerticalSeekBar verticalSeekBar) {
                VideoFullActivity.this.hideControllerMessage();
            }
        });
        this.vsbVolume.setOnSeekBarChangeListener(new VerticalSeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(VerticalSeekBar verticalSeekBar, int i, boolean z) {
                if (z || VideoFullActivity.this.adjustVolume) {
                    VideoFullActivity.this.setVolume((int) (((((float) i) * 1.0f) / ((float) verticalSeekBar.getMax())) * ((float) VideoFullActivity.this.getMaxVolume())));
                    boolean unused = VideoFullActivity.this.adjustVolume = false;
                }
                if (!VideoFullActivity.this.vsbVolume.isShown()) {
                    return;
                }
                if (i == 0) {
                    VideoFullActivity.this.ibVolume.setBackgroundResource(R.drawable.ib_volume_silence_selected_video);
                } else {
                    VideoFullActivity.this.ibVolume.setBackgroundResource(R.drawable.ib_volume_selected_video);
                }
            }

            public void onStartTrackingTouch(VerticalSeekBar verticalSeekBar) {
            }

            public void onStopTrackingTouch(VerticalSeekBar verticalSeekBar) {
                VideoFullActivity.this.hideControllerMessage();
            }
        });
        findViewById(R.id.rl_video_bottom).getBackground().setAlpha(76);
        this.rlControllerTop.getBackground().setAlpha(76);
        FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(-2, -2);
        layoutParams3.gravity = 17;
        this.rlBuffer = (RelativeLayout) LayoutInflater.from(this).inflate((int) R.layout.buffer_video, (ViewGroup) this.decorView, false);
        this.pbBuffer = (ProgressBar) this.rlBuffer.findViewById(R.id.pb_buffer);
        this.pbBuffer.setIndeterminateDrawable(getResources().getDrawable(R.drawable.loading_video_progress));
        this.tvBuffer = (TextView) this.rlBuffer.findViewById(R.id.tv_buffer);
        this.tvBuffer.setText(String.format(getResources().getString(R.string.already_loading), 0));
        this.tvBuffer.setTextSize((float) b.a(this, 5.0f));
        new FrameLayout.LayoutParams(-2, -2).gravity = 17;
        this.decorView.addView(this.rlBuffer, layoutParams3);
        this.rlController.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (VideoFullActivity.this.rlControllerTop.isShown()) {
                    VideoFullActivity.this.hideBrightness();
                    VideoFullActivity.this.hideVolume();
                    VideoFullActivity.this.hideController();
                    VideoFullActivity.this.handler.removeCallbacksAndMessages(null);
                    return;
                }
                VideoFullActivity.this.showController();
                VideoFullActivity.this.hideControllerMessage();
            }
        });
        this.rlVideoBottom.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                VideoFullActivity.this.hideControllerMessage();
            }
        });
        this.rlControllerTop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                VideoFullActivity.this.hideControllerMessage();
            }
        });
        this.decorView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (VideoFullActivity.this.rlControllerTop.isShown()) {
                    VideoFullActivity.this.hideBrightness();
                    VideoFullActivity.this.hideVolume();
                    VideoFullActivity.this.hideController();
                    VideoFullActivity.this.handler.removeCallbacksAndMessages(null);
                    return;
                }
                VideoFullActivity.this.showController();
                VideoFullActivity.this.hideControllerMessage();
            }
        });
        videoView.setOnPreparedListener(new QihooMediaPlayer.OnPreparedListener() {
            public void onPrepared(QihooMediaPlayer qihooMediaPlayer) {
                VideoFullActivity.this.sbProgress.setEnabled(true);
                boolean unused = VideoFullActivity.this.isPrepared = true;
                VideoFullActivity.this.tvAllProgress.setText(bk.a((long) qihooMediaPlayer.getDuration()));
                VideoFullActivity.this.tvCurrentProgress.setText(bk.a((long) qihooMediaPlayer.getCurrentPosition()));
                if (!b.d(VideoFullActivity.this) || VideoFullActivity.videoView == null) {
                    ay.a(VideoFullActivity.this).a((int) R.string.net_error);
                    VideoFullActivity.this.release();
                    return;
                }
                VideoFullActivity.videoView.start();
                VideoFullActivity.videoView.seekTo(VideoFullActivity.this.position);
            }
        });
        videoView.setOnPositionChangeListener(new QihooMediaPlayer.OnPositionChangeListener() {
            public void onPlayPositionChanged(QihooMediaPlayer qihooMediaPlayer, int i) {
                VideoFullActivity.this.sbProgress.setProgress((int) ((((float) i) / ((float) qihooMediaPlayer.getDuration())) * 100.0f));
                VideoFullActivity.this.tvCurrentProgress.setText(bk.a((long) i));
            }
        });
        videoView.setOnBufferListener(new QihooMediaPlayer.OnBufferingUpdateListener() {
            public void onBufferingUpdate(QihooMediaPlayer qihooMediaPlayer, int i) {
                if (VideoFullActivity.this.tvBuffer != null) {
                    VideoFullActivity.this.tvBuffer.setText(String.format(VideoFullActivity.this.getResources().getString(R.string.already_loading), Integer.valueOf(i)));
                }
                if (i >= 0 && i < 100) {
                    boolean unused = VideoFullActivity.this.isPrepared = false;
                    if (!VideoFullActivity.this.rlBuffer.isShown()) {
                        VideoFullActivity.this.rlBuffer.setVisibility(0);
                    }
                } else if (i == 100) {
                    boolean unused2 = VideoFullActivity.this.isPrepared = true;
                    if (VideoFullActivity.this.rlBuffer.isShown()) {
                        VideoFullActivity.this.rlBuffer.setVisibility(8);
                    }
                }
            }
        });
        videoView.setOnSeekCompleteListener(new QihooMediaPlayer.OnSeekCompleteListener() {
            public void onSeekComplete(QihooMediaPlayer qihooMediaPlayer) {
                VideoFullActivity.this.tvCurrentProgress.setText(bk.a((long) qihooMediaPlayer.getCurrentPosition()));
                if (VideoFullActivity.this.rlBuffer.isShown()) {
                    VideoFullActivity.this.rlBuffer.setVisibility(8);
                }
                VideoFullActivity.ibPlay.setImageResource(R.drawable.ib_pause_full_video);
                qihooMediaPlayer.start();
            }
        });
        videoView.setOnCompletetionListener(new QihooMediaPlayer.OnCompletionListener() {
            public void onCompletion(QihooMediaPlayer qihooMediaPlayer) {
                VideoFullActivity.this.tvCurrentProgress.setText(bk.a((long) qihooMediaPlayer.getDuration()));
                VideoFullActivity.this.finish();
            }
        });
        this.sbProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                if (z) {
                    this.progress = i;
                }
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                if (b.d(VideoFullActivity.this)) {
                    VideoFullActivity.videoView.seekTo((int) ((((float) this.progress) / 100.0f) * ((float) VideoFullActivity.videoView.getDuration())));
                } else {
                    ay.a(VideoFullActivity.this).a((int) R.string.net_error);
                    VideoFullActivity.this.release();
                }
                VideoFullActivity.this.hideControllerMessage();
            }
        });
        videoView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (VideoFullActivity.this.rlControllerTop.isShown()) {
                    VideoFullActivity.this.hideBrightness();
                    VideoFullActivity.this.hideVolume();
                    VideoFullActivity.this.hideController();
                    VideoFullActivity.this.handler.removeCallbacksAndMessages(null);
                    return;
                }
                VideoFullActivity.this.showController();
                VideoFullActivity.this.hideControllerMessage();
            }
        });
        ibPlay.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (VideoFullActivity.this.isPrepared) {
                    if (VideoFullActivity.videoView.isPlaying()) {
                        VideoFullActivity.ibPlay.setImageResource(R.drawable.ib_play_full_video);
                        VideoFullActivity.videoView.pause();
                    } else if (b.d(VideoFullActivity.this)) {
                        VideoFullActivity.ibPlay.setImageResource(R.drawable.ib_pause_full_video);
                        VideoFullActivity.videoView.start();
                    } else {
                        ay.a(VideoFullActivity.this).a((int) R.string.can_not_play);
                        VideoFullActivity.ibPlay.setImageResource(R.drawable.ib_play_full_video);
                        ay.a(VideoFullActivity.this).a((int) R.string.can_not_play);
                        VideoFullActivity.videoView.pause();
                        VideoFullActivity.this.rlBuffer.setVisibility(8);
                    }
                }
                VideoFullActivity.this.hideControllerMessage();
            }
        });
        videoView.setOnErrorListener(new QihooMediaPlayer.OnErrorListener() {
            public boolean onError(QihooMediaPlayer qihooMediaPlayer, int i, int i2) {
                if (b.d(VideoFullActivity.this)) {
                    return true;
                }
                VideoFullActivity.ibPlay.setImageResource(R.drawable.ib_play_full_video);
                ay.a(VideoFullActivity.this).a((int) R.string.can_not_play);
                qihooMediaPlayer.pause();
                VideoFullActivity.this.rlBuffer.setVisibility(8);
                return true;
            }
        });
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        videoView.initVideoWidAndHeight(defaultDisplay.getWidth(), defaultDisplay.getHeight());
        videoView.setDataSource(this.url);
        getWindow().addFlags(128);
    }

    /* access modifiers changed from: private */
    public void release() {
        restoreScreenBrightness();
        if (videoView != null) {
            videoView.stop();
            videoView.release();
            this.decorView.removeView(videoView);
            this.rlBuffer.setVisibility(8);
            videoView = null;
            ibPlay = null;
            this.isPrepared = false;
            if (this.sbProgress != null) {
                this.sbProgress.setEnabled(false);
            }
        }
    }

    private void restoreScreenBrightness() {
        if (this.isBrightnessAuto) {
            setScreenMode(1);
        } else {
            saveScreenBrightness(this.defaultBright);
        }
    }

    /* access modifiers changed from: private */
    public void saveScreenBrightness(int i) {
        if (i == 0) {
            i = 1;
        }
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.screenBrightness = ((float) i) / 255.0f;
        getWindow().setAttributes(attributes);
    }

    private void setScreenMode(int i) {
        try {
            Settings.System.putInt(getContentResolver(), "screen_brightness_mode", i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void setVolume(int i) {
        ((AudioManager) getSystemService("audio")).setStreamVolume(3, i, 0);
    }

    /* access modifiers changed from: private */
    public void showController() {
        this.rlControllerTop.setVisibility(0);
        this.rlControllerBottom.setVisibility(0);
        this.rlControllerTop.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_top_in));
        this.rlControllerBottom.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_bottom_in));
    }

    public static void tryPause() {
        if (videoView != null && videoView.isPlaying() && ibPlay != null) {
            ibPlay.setImageResource(R.drawable.ib_play_full_video);
            videoView.pause();
        }
    }

    public void finish() {
        release();
        super.finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!i.b()) {
            ay.a(this).a((int) R.string.cpu_no_support);
            finish();
        }
        if (b.d(this)) {
            getData();
            initView();
            return;
        }
        ay.a(this).a((int) R.string.net_error);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (videoView != null) {
            release();
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.rlController != null) {
            hideControllerMessage();
            this.rlControllerTop.setVisibility(0);
            this.rlControllerBottom.setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
