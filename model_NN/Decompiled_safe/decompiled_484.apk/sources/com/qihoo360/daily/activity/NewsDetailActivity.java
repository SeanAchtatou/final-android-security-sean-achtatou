package com.qihoo360.daily.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import com.mediav.ads.sdk.adcore.Config;
import com.mediav.ads.sdk.adcore.Mvad;
import com.mediav.ads.sdk.interfaces.IMvNativeAd;
import com.mediav.ads.sdk.interfaces.IMvNativeAdListener;
import com.mediav.ads.sdk.interfaces.IMvNativeAdLoader;
import com.qihoo360.daily.R;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.f.a;
import com.qihoo360.daily.f.b;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.g.ar;
import com.qihoo360.daily.g.av;
import com.qihoo360.daily.g.ay;
import com.qihoo360.daily.g.f;
import com.qihoo360.daily.g.s;
import com.qihoo360.daily.h.ab;
import com.qihoo360.daily.h.ad;
import com.qihoo360.daily.h.ai;
import com.qihoo360.daily.h.bj;
import com.qihoo360.daily.h.y;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.NewComment;
import com.qihoo360.daily.model.NewCommentResponse;
import com.qihoo360.daily.model.NewsContent;
import com.qihoo360.daily.model.NewsDetail;
import com.qihoo360.daily.model.NewsRelated;
import com.qihoo360.daily.model.Related;
import com.qihoo360.daily.model.ResponseBean;
import com.qihoo360.daily.model.Result;
import com.qihoo360.daily.model.User;
import com.qihoo360.daily.widget.FindWebView;
import com.qihoo360.daily.widget.GestureView;
import com.qihoo360.daily.widget.webview.JavascriptInterface;
import com.qihoo360.daily.widget.webview.WebChromeClientDaily;
import com.qihoo360.daily.widget.webview.WebViewClientDaily;
import com.qihoo360.daily.wxapi.WXUser;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

public class NewsDetailActivity extends BaseDetailActivity implements View.OnClickListener, ab, FindWebView.OnScrollChangedListener, GestureView.GestureListener, JavascriptInterface, WebViewClientDaily.WebViewClientListener {
    private static final int CMTS_REQUESTCODE = 10;
    private static final int SEND_CMT_REQUESTCODE = 9;
    private static final String TAG_CONTENT = "content";
    private final int MSG_SEND_CMT = 4;
    private float alpha = 1.0f;
    private float alphaLen;
    private String base_url = "file:///android_asset/news-detail.html";
    /* access modifiers changed from: private */
    public boolean cmtAble = true;
    /* access modifiers changed from: private */
    public c<Void, Result<NewCommentResponse>> cmtOnNetRequestListener = new c<Void, Result<NewCommentResponse>>() {
        public void onNetRequest(int i, Result<NewCommentResponse> result) {
            new ay(NewsDetailActivity.this.getApplicationContext(), NewsDetailActivity.this.nid, NewsDetailActivity.this.info_url, NewsDetailActivity.this.v_t, 1, null).a(NewsDetailActivity.this.relatedOnNetRequestListener, new Void[0]);
            if (result != null) {
                int status = result.getStatus();
                if (status == 0) {
                    NewCommentResponse data = result.getData();
                    if (data != null && NewsDetailActivity.this.mWebView != null) {
                        int unused = NewsDetailActivity.this.total = data.getComment_num();
                        if (NewsDetailActivity.this.total > 0) {
                            NewsDetailActivity.this.mWebView.loadUrl("javascript:$ContentRender.renderSeparator();");
                        }
                        NewsDetailActivity.this.mInfo.setCmt_cnt(NewsDetailActivity.this.total + "");
                        NewsDetailActivity.this.setCmtNum(NewsDetailActivity.this.total);
                        Intent intent = new Intent();
                        intent.putExtra("Info", NewsDetailActivity.this.mInfo);
                        intent.putExtra("position", NewsDetailActivity.this.position);
                        NewsDetailActivity.this.setResult(-1, intent);
                        NewsDetailActivity.this.mWebView.loadUrl("javascript:$ContentRender.renderComments(" + Application.getGson().a(data) + ");");
                    }
                } else if (status == 110) {
                    boolean unused2 = NewsDetailActivity.this.cmtAble = false;
                } else {
                    com.qihoo360.daily.h.ay.a(NewsDetailActivity.this.getApplicationContext()).a(result.getMsg());
                }
            }
        }

        public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
            onNetRequest(i, (Result<NewCommentResponse>) ((Result) obj));
        }
    };
    private FrameLayout container;
    /* access modifiers changed from: private */
    public String content;
    private c<ResponseBean<NewsDetail>, ResponseBean<NewsDetail>> detailOnNetRequestListener = new c<ResponseBean<NewsDetail>, ResponseBean<NewsDetail>>() {
        public void onNetRequest(int i, ResponseBean<NewsDetail> responseBean) {
        }

        public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
            onNetRequest(i, (ResponseBean<NewsDetail>) ((ResponseBean) obj));
        }

        public void onProgressUpdate(int i, ResponseBean<NewsDetail> responseBean) {
            NewsDetailActivity.this.loading_layout.setVisibility(8);
            if (responseBean != null) {
                int errno = responseBean.getErrno();
                if (errno == 0) {
                    NewsDetail unused = NewsDetailActivity.this.mNewsDetail = responseBean.getData();
                    if (NewsDetailActivity.this.mNewsDetail != null && NewsDetailActivity.this.mWebView != null) {
                        NewsDetailActivity.this.mNewsDetail.setSource(NewsDetailActivity.this.mInfo.getSrc());
                        String shorturl = NewsDetailActivity.this.mNewsDetail.getShorturl();
                        NewsDetailActivity.this.mNewsDetail.setShorturl(NewsDetailActivity.this.info_url);
                        String wapurl = NewsDetailActivity.this.mNewsDetail.getWapurl();
                        ArrayList<NewsContent> content = NewsDetailActivity.this.mNewsDetail.getContent();
                        if (TextUtils.isEmpty(wapurl) || (content != null && !content.isEmpty())) {
                            String a2 = Application.getGson().a(NewsDetailActivity.this.mNewsDetail);
                            NewsDetailActivity.this.mNewsDetail.setShorturl(shorturl);
                            boolean z = !"2".equals(NewsDetailActivity.this.mInfo.getN_t());
                            NewsDetailActivity.this.mWebView.loadUrl("javascript:window.$debug=false");
                            NewsDetailActivity.this.mWebView.loadUrl("javascript:$ContentRender.renderArticle(" + a2 + "," + z + ");");
                            if (TextUtils.isEmpty(NewsDetailActivity.this.mFirstImageUrl)) {
                                NewsDetailActivity.this.mWebViewClient.setNewsDetail(NewsDetailActivity.this.mNewsDetail);
                            }
                            new ar(NewsDetailActivity.this.getApplicationContext(), NewsDetailActivity.this.info_url, "0").a(NewsDetailActivity.this.cmtOnNetRequestListener, new Void[0]);
                            return;
                        }
                        Intent intent = new Intent(NewsDetailActivity.this.getApplicationContext(), SearchDetailActivity.class);
                        intent.putExtra(SearchActivity.TAG_URL, wapurl);
                        NewsDetailActivity.this.startActivity(intent);
                        NewsDetailActivity.this.finish();
                        return;
                    }
                    return;
                }
                com.qihoo360.daily.h.ay.a(NewsDetailActivity.this.getApplicationContext()).a(NewsDetailActivity.this.getString(R.string.error_code, new Object[]{Integer.valueOf(errno)}));
                NewsDetailActivity.this.error_layout.setVisibility(0);
                return;
            }
            com.qihoo360.daily.h.ay.a(NewsDetailActivity.this.getApplicationContext()).a((int) R.string.net_error);
            NewsDetailActivity.this.error_layout.setVisibility(0);
        }

        public /* bridge */ /* synthetic */ void onProgressUpdate(int i, Object obj) {
            onProgressUpdate(i, (ResponseBean<NewsDetail>) ((ResponseBean) obj));
        }
    };
    /* access modifiers changed from: private */
    public View error_layout;
    /* access modifiers changed from: private */
    public String from;
    /* access modifiers changed from: private */
    public String info_url;
    /* access modifiers changed from: private */
    public boolean isCollected;
    /* access modifiers changed from: private */
    public View loading_layout;
    private String m;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public String mFirstImageUrl;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 4:
                    if (!NewsDetailActivity.this.cmtAble) {
                        com.qihoo360.daily.h.ay.a(NewsDetailActivity.this.getApplicationContext()).a((int) R.string.cmt_close);
                        return;
                    } else if (a.h(NewsDetailActivity.this.getApplicationContext())) {
                        NewsDetailActivity.this.startActivityForResult(new Intent(NewsDetailActivity.this.getApplicationContext(), SendCmtActivity.class).putExtra("Info", NewsDetailActivity.this.mInfo).putExtra("from", NewsDetailActivity.this.from).putExtra(NewsDetailActivity.TAG_CONTENT, NewsDetailActivity.this.content), 9);
                        return;
                    } else {
                        NewsDetailActivity.this.login();
                        return;
                    }
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public Info mInfo;
    /* access modifiers changed from: private */
    public NewsDetail mNewsDetail;
    /* access modifiers changed from: private */
    public Toolbar mToolbar;
    /* access modifiers changed from: private */
    public FindWebView mWebView;
    /* access modifiers changed from: private */
    public WebViewClientDaily mWebViewClient;
    /* access modifiers changed from: private */
    public ArrayList<IMvNativeAd> nativeAds;
    /* access modifiers changed from: private */
    public NewsRelated newsRelated;
    private String newtrans;
    /* access modifiers changed from: private */
    public String nid;
    /* access modifiers changed from: private */
    public int position;
    private String pushId;
    /* access modifiers changed from: private */
    public c<Void, Result<NewsRelated>> relatedOnNetRequestListener = new c<Void, Result<NewsRelated>>() {
        public void onNetRequest(int i, Result<NewsRelated> result) {
            if (result != null) {
                if (result.getStatus() == 0) {
                    NewsRelated unused = NewsDetailActivity.this.newsRelated = result.getData();
                    if (!(NewsDetailActivity.this.newsRelated == null || NewsDetailActivity.this.mWebView == null)) {
                        if (Config.CHANNEL_ID.equals(NewsDetailActivity.this.newsRelated.getIsCollected())) {
                            boolean unused2 = NewsDetailActivity.this.isCollected = true;
                        } else {
                            boolean unused3 = NewsDetailActivity.this.isCollected = false;
                        }
                        ArrayList<Related> related = NewsDetailActivity.this.newsRelated.getRelated();
                        if (related != null && related.size() > 0) {
                            NewsDetailActivity.this.mWebView.loadUrl("javascript:$ContentRender.renderRecommendArtices(" + Application.getGson().a(NewsDetailActivity.this.newsRelated) + ");");
                        }
                    }
                } else {
                    com.qihoo360.daily.h.ay.a(NewsDetailActivity.this.getApplicationContext()).a(result.getMsg());
                }
            }
            if (NewsDetailActivity.this.mWebView != null) {
                NewsDetailActivity.this.mWebView.loadUrl("javascript:$ContentRender.lazyLoadImage();");
                NewsDetailActivity.this.mWebView.loadUrl("javascript:$ContentRender.done()");
                NewsDetailActivity.this.mWebView.getSettings().setBlockNetworkImage(false);
            }
            if (Config.CHANNEL_ID.equals(d.a(NewsDetailActivity.this.getApplicationContext(), "ad_JX_Enable"))) {
                NewsDetailActivity.this.loadAd();
            }
        }

        public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
            onNetRequest(i, (Result<NewsRelated>) ((Result) obj));
        }
    };
    /* access modifiers changed from: private */
    public int total;
    /* access modifiers changed from: private */
    public int v_t;

    /* renamed from: com.qihoo360.daily.activity.NewsDetailActivity$8  reason: invalid class name */
    /* synthetic */ class AnonymousClass8 {
        static final /* synthetic */ int[] $SwitchMap$android$webkit$WebSettings$TextSize = new int[WebSettings.TextSize.values().length];

        static {
            try {
                $SwitchMap$android$webkit$WebSettings$TextSize[WebSettings.TextSize.SMALLER.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$android$webkit$WebSettings$TextSize[WebSettings.TextSize.NORMAL.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$android$webkit$WebSettings$TextSize[WebSettings.TextSize.LARGER.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$android$webkit$WebSettings$TextSize[WebSettings.TextSize.LARGEST.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            $SwitchMap$com$qihoo360$daily$sp$Settings$FontSize = new int[b.values().length];
            try {
                $SwitchMap$com$qihoo360$daily$sp$Settings$FontSize[b.SMALL.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$qihoo360$daily$sp$Settings$FontSize[b.MEDIUM.ordinal()] = 2;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$qihoo360$daily$sp$Settings$FontSize[b.LARGE.ordinal()] = 3;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$qihoo360$daily$sp$Settings$FontSize[b.X_LARGE.ordinal()] = 4;
            } catch (NoSuchFieldError e8) {
            }
        }
    }

    private Bitmap getCmtNumBm(Bitmap bitmap, String str) {
        if (bitmap == null) {
            return null;
        }
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.draw_text_size);
        Paint paint = new Paint();
        paint.setTextSize((float) dimensionPixelSize);
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        float measureText = paint.measureText(str);
        float f = fontMetrics.bottom - fontMetrics.top;
        return Bitmap.createScaledBitmap(bitmap, (int) Math.max(measureText + ((float) getResources().getDimensionPixelSize(R.dimen.margin_xsmall)), (float) bitmap.getWidth()), (int) (f + ((float) getResources().getDimensionPixelSize(R.dimen.margin_xxsmall))), true);
    }

    private void initToolbar() {
        this.mToolbar = (Toolbar) findViewById(R.id.toolbar);
        this.mToolbar.inflateMenu(R.menu.menu_news_detail);
        this.mToolbar.setNavigationIcon((int) R.drawable.ic_actionbar_back);
        this.mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) NewsDetailActivity.this.getSystemService("activity")).getRunningTasks(10);
                if (runningTasks != null && runningTasks.size() > 0 && runningTasks.get(0).numActivities == 1) {
                    NewsDetailActivity.this.startActivity(new Intent(NewsDetailActivity.this, IndexActivity.class));
                }
                com.qihoo360.daily.h.b.b(NewsDetailActivity.this.mContext, "Bottombar_bottom_back");
                NewsDetailActivity.this.finish();
            }
        });
        this.mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_cmt:
                        if (NewsDetailActivity.this.cmtAble) {
                            Intent intent = new Intent(NewsDetailActivity.this.getApplicationContext(), CommentActivity.class);
                            intent.putExtra("Info", NewsDetailActivity.this.mInfo);
                            intent.putExtra("clickType", NewsDetailActivity.this.clickType);
                            intent.putExtra("from", NewsDetailActivity.this.from);
                            NewsDetailActivity.this.startActivityForResult(intent, 10);
                        } else {
                            com.qihoo360.daily.h.ay.a(NewsDetailActivity.this.getApplicationContext()).a((int) R.string.cmt_close);
                        }
                        com.qihoo360.daily.h.b.b(NewsDetailActivity.this.mContext, "Bottombar_bottom_comment");
                        break;
                    case R.id.action_share:
                        if (TextUtils.isEmpty(NewsDetailActivity.this.mFirstImageUrl)) {
                            String unused = NewsDetailActivity.this.mFirstImageUrl = NewsDetailActivity.this.mWebViewClient.getFirstImgUrl();
                        }
                        new ai(NewsDetailActivity.this, NewsDetailActivity.this.mInfo, NewsDetailActivity.this.mNewsDetail, NewsDetailActivity.this.mFirstImageUrl, NewsDetailActivity.this.clickType, NewsDetailActivity.this.from).a();
                        com.qihoo360.daily.h.b.b(NewsDetailActivity.this.mContext, "Bottombar_bottom_share");
                        break;
                    case R.id.action_more:
                        new y(NewsDetailActivity.this, NewsDetailActivity.this, NewsDetailActivity.this.nid, NewsDetailActivity.this.isCollected, true).a(NewsDetailActivity.this.mToolbar);
                        com.qihoo360.daily.h.b.b(NewsDetailActivity.this.mContext, "Bottombar_bottom_menu");
                        break;
                }
                return true;
            }
        });
    }

    private void initViews() {
        ((GestureView) findViewById(R.id.gesture_view)).setGestureListener(this);
        findViewById(R.id.go_cmt).setOnClickListener(this);
        this.error_layout = findViewById(R.id.error_layout);
        this.error_layout.setOnClickListener(this);
        this.loading_layout = findViewById(R.id.loading_layout);
        this.loading_layout.setVisibility(0);
    }

    @SuppressLint({"JavascriptInterface", "SetJavaScriptEnabled"})
    @TargetApi(11)
    private void initWebView() {
        this.container = (FrameLayout) findViewById(R.id.container);
        this.mWebView = new FindWebView(this);
        this.mWebView.setOnScrollChangedListener(this);
        this.container.addView(this.mWebView);
        this.mWebView.setWebChromeClient(new WebChromeClientDaily());
        this.mWebViewClient = new WebViewClientDaily(this);
        this.mWebView.setWebViewClient(this.mWebViewClient);
        WebSettings settings = this.mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setBuiltInZoomControls(true);
        if (bj.a()) {
            settings.setDisplayZoomControls(false);
        }
        settings.setSupportZoom(false);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setDomStorageEnabled(true);
        settings.setBlockNetworkImage(true);
        this.mWebView.addJavascriptInterface(this, "$_news");
        this.mWebView.addJavascriptInterface(this, "$_cmt");
        this.mWebView.addJavascriptInterface(this, "$_related");
        if (!bj.a()) {
            this.mWebView.setVerticalScrollBarEnabled(false);
        }
        try {
            if (bj.a()) {
                this.mWebView.removeJavascriptInterface("searchBoxJavaBridge_");
                this.mWebView.removeJavascriptInterface("accessibility");
                this.mWebView.removeJavascriptInterface("accessibilityTraversal");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void loadAd() {
        IMvNativeAdLoader initNativeAdLoader = Mvad.initNativeAdLoader(this, "PkkQuSiGBA", new IMvNativeAdListener() {
            public void onNativeAdLoadFailed() {
                ad.a("onNativeAdLoadFailed");
            }

            public void onNativeAdLoadSucceeded(ArrayList<IMvNativeAd> arrayList) {
                ad.a("onNativeAdLoadSucceeded");
                if (arrayList != null && NewsDetailActivity.this.mWebView != null) {
                    ArrayList unused = NewsDetailActivity.this.nativeAds = arrayList;
                    StringBuilder sb = new StringBuilder();
                    sb.append("[");
                    Iterator<IMvNativeAd> it = arrayList.iterator();
                    while (true) {
                        if (it.hasNext()) {
                            JSONObject content = it.next().getContent();
                            if (content != null) {
                                sb.append(content.toString());
                                sb.append(",");
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                    sb.deleteCharAt(sb.length() - 1);
                    sb.append("]");
                    String sb2 = sb.toString();
                    ad.a("ad json: " + sb2);
                    NewsDetailActivity.this.mWebView.loadUrl("javascript:$ContentRender.renderAd(" + sb2 + ");");
                }
            }
        }, false);
        HashSet hashSet = new HashSet();
        String keywords = this.mInfo.getKeywords();
        if (!TextUtils.isEmpty(keywords)) {
            for (String str : keywords.split(" ")) {
                ad.a("ad word: " + str);
                hashSet.add(str);
            }
        }
        initNativeAdLoader.setKeywords(hashSet);
        initNativeAdLoader.loadAds();
    }

    /* access modifiers changed from: private */
    public void setCmtNum(int i) {
        MenuItem findItem = this.mToolbar.getMenu().findItem(R.id.action_cmt);
        com.qihoo360.daily.c.b bVar = null;
        if (i > 0) {
            String a2 = com.qihoo360.daily.h.b.a(i);
            bVar = new com.qihoo360.daily.c.b(getResources(), getCmtNumBm(BitmapFactory.decodeResource(getResources(), R.drawable.bg_action_comment_num), a2), a2);
            bVar.setBounds(0, 0, bVar.getIntrinsicWidth(), bVar.getIntrinsicHeight());
        }
        findItem.setIcon(new com.qihoo360.daily.c.a(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.ic_actionbar_comment), bVar));
    }

    private void setToolBarEnabled(boolean z) {
        if (z) {
            this.mToolbar.setNavigationIcon((int) R.drawable.ic_actionbar_back);
        } else {
            this.mToolbar.setNavigationIcon((Drawable) null);
        }
        Menu menu = this.mToolbar.getMenu();
        int size = menu.size();
        for (int i = 0; i < size; i++) {
            menu.getItem(i).setEnabled(z);
        }
    }

    @android.webkit.JavascriptInterface
    public void OnClickAd(String str) {
        IMvNativeAd iMvNativeAd;
        try {
            int intValue = Integer.valueOf(str).intValue();
            if (intValue >= 0 && this.nativeAds != null && this.nativeAds.size() > intValue && (iMvNativeAd = this.nativeAds.get(intValue)) != null) {
                iMvNativeAd.onAdClicked();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @android.webkit.JavascriptInterface
    public void OnClickBury() {
        ad.a("OnClickBury");
    }

    @android.webkit.JavascriptInterface
    public void OnClickCmt() {
    }

    @android.webkit.JavascriptInterface
    public void OnClickCmtLike(String str) {
        ad.a("OnClickCmtLike");
        new f(getApplicationContext(), str, this.info_url).a(null, new Object[0]);
        if (this.mInfo != null) {
            new av(Application.getInstance(), this.info_url, this.from, this.nid, this.mInfo.getA(), "b88f54f7f545e6b5", this.mInfo.getTitle(), "2", this.clickType).a(null, new Void[0]);
        }
    }

    @android.webkit.JavascriptInterface
    public void OnClickCmtMore() {
        ad.a("OnClickCmtMore");
        Intent intent = new Intent(getApplicationContext(), CommentActivity.class);
        intent.putExtra("Info", this.mInfo);
        intent.putExtra("clickType", this.clickType);
        intent.putExtra("from", this.from);
        startActivityForResult(intent, 10);
    }

    @android.webkit.JavascriptInterface
    public void OnClickDigg() {
        ad.a("OnClickDigg");
    }

    @android.webkit.JavascriptInterface
    public void OnClickImg(String str, int i, int i2, int i3, int i4) {
        if (this.mNewsDetail != null) {
            ArrayList<NewsContent> content2 = this.mNewsDetail.getContent();
            ArrayList arrayList = new ArrayList();
            int size = content2.size();
            for (int i5 = 0; i5 < size; i5++) {
                NewsContent newsContent = content2.get(i5);
                String type = newsContent.getType();
                String value = newsContent.getValue();
                if ("img".equals(type)) {
                    arrayList.add(value);
                }
            }
            int i6 = 0;
            while (true) {
                if (i6 >= arrayList.size()) {
                    i6 = 0;
                    break;
                } else if (str.equals(arrayList.get(i6))) {
                    break;
                } else {
                    i6++;
                }
            }
            Intent intent = new Intent(this, ImageDetailActivity.class);
            intent.putExtra(ImageDetailActivity.IMAGE_URLS, arrayList);
            intent.putExtra("index", i6);
            startActivity(intent);
        }
    }

    @android.webkit.JavascriptInterface
    public void OnClickRelated(String str) {
        ArrayList<Related> related;
        ad.a("OnClickRelated:" + str);
        if (this.newsRelated != null && str != null && (related = this.newsRelated.getRelated()) != null && related.size() > 0) {
            Iterator<Related> it = related.iterator();
            while (it.hasNext()) {
                Related next = it.next();
                if (str.equals(next.getUrl())) {
                    Intent intent = new Intent(getApplicationContext(), NewsDetailActivity.class);
                    intent.putExtra("Info", next);
                    intent.putExtra("from", ChannelType.TYPE_RELATED);
                    startActivity(intent);
                }
            }
        }
    }

    @android.webkit.JavascriptInterface
    public void OnClickShare(String str) {
        if (TextUtils.isEmpty(this.mFirstImageUrl)) {
            this.mFirstImageUrl = this.mWebViewClient.getFirstImgUrl();
        }
        ai aiVar = new ai(this, this.mInfo, this.mNewsDetail, this.mFirstImageUrl, this.clickType, this.from);
        ad.a("onClickShare:" + str);
        if ("circle".equals(str)) {
            aiVar.d();
        } else if ("weixin".equals(str)) {
            aiVar.c();
        } else if ("weibo".equals(str)) {
            aiVar.b();
        }
    }

    @android.webkit.JavascriptInterface
    public void OnClickText() {
    }

    @android.webkit.JavascriptInterface
    public void OnSearchTag(String str) {
        ad.a("OnSearchTag:" + str);
        Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
        intent.putExtra(SearchActivity.TAG_SEARCH, str);
        startActivity(intent);
    }

    @android.webkit.JavascriptInterface
    public void OnShowAd(String str) {
        IMvNativeAd iMvNativeAd;
        try {
            int intValue = Integer.valueOf(str).intValue();
            if (intValue >= 0 && this.nativeAds != null && this.nativeAds.size() > intValue && (iMvNativeAd = this.nativeAds.get(intValue)) != null) {
                iMvNativeAd.onAdShowed();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void OnWeiboLoginCancel() {
    }

    public void OnWeiboLoginError(User user) {
    }

    public void OnWeiboLoginException(com.sina.weibo.sdk.c.c cVar) {
    }

    public void OnWeiboLoginSuccess(User user) {
        if (a.h(getApplicationContext())) {
            startActivityForResult(new Intent(getApplicationContext(), SendCmtActivity.class).putExtra("Info", this.mInfo).putExtra(TAG_CONTENT, this.content), 9);
        }
    }

    public void OnWeixinLoginCancel() {
    }

    public void OnWeixinLoginError(WXUser wXUser) {
    }

    public void OnWeixinLoginException(int i) {
    }

    public void OnWeixinLoginSuccess(WXUser wXUser) {
        if (a.h(getApplicationContext())) {
            startActivityForResult(new Intent(getApplicationContext(), SendCmtActivity.class).putExtra("Info", this.mInfo).putExtra(TAG_CONTENT, this.content), 9);
        }
    }

    @android.webkit.JavascriptInterface
    public void comment() {
        if (this.mHandler != null) {
            this.mHandler.sendEmptyMessage(4);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        NewComment newComment;
        super.onActivityResult(i, i2, intent);
        if (!(i2 != -1 || intent == null || (newComment = (NewComment) intent.getParcelableExtra(SendCmtActivity.TAG_DATA)) == null)) {
            this.mWebView.loadUrl("javascript:$ContentRender.prependComment(" + Application.getGson().a(newComment) + ");");
            this.total++;
            setCmtNum(this.total);
            this.mInfo.setCmt_cnt(this.total + "");
            Intent intent2 = new Intent();
            intent2.putExtra("Info", this.mInfo);
            intent2.putExtra("position", this.position);
            setResult(-1, intent2);
        }
        if (i == 9 && intent != null) {
            this.content = intent.getStringExtra(TAG_CONTENT);
        }
        if (i == 10 && intent != null) {
            this.total = intent.getIntExtra("cmt_cnt", this.total);
            setCmtNum(this.total);
            this.mInfo.setCmt_cnt(this.total + "");
            Intent intent3 = new Intent();
            intent3.putExtra("Info", this.mInfo);
            intent3.putExtra("position", this.position);
            setResult(-1, intent3);
        }
    }

    public void onBackPressed() {
        List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) getSystemService("activity")).getRunningTasks(10);
        if (runningTasks != null && runningTasks.size() > 0 && runningTasks.get(0).numActivities == 1) {
            startActivity(new Intent(this, IndexActivity.class));
        }
        super.onBackPressed();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.error_layout:
                this.error_layout.setVisibility(8);
                this.loading_layout.setVisibility(0);
                if (this.mWebView != null) {
                    this.mWebView.loadUrl(this.base_url);
                    return;
                }
                return;
            case R.id.go_cmt:
                if (a.h(getApplicationContext())) {
                    startActivityForResult(new Intent(getApplicationContext(), SendCmtActivity.class).putExtra("Info", this.mInfo).putExtra("from", this.from).putExtra(TAG_CONTENT, this.content), 9);
                    return;
                } else {
                    login();
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_detail);
        this.mContext = this;
        this.mInfo = (Info) getIntent().getParcelableExtra("Info");
        this.pushId = getIntent().getStringExtra("PushId");
        this.position = getIntent().getIntExtra("position", -1);
        if (this.mInfo != null) {
            this.info_url = this.mInfo.getUrl();
            this.m = this.mInfo.getM();
            this.v_t = this.mInfo.getV_t();
            this.nid = this.mInfo.getNid();
            this.newtrans = this.mInfo.getNewtrans();
            if (Config.CHANNEL_ID.equals(this.mInfo.getNocmt())) {
                this.cmtAble = false;
            }
            this.from = getIntent().getStringExtra("from");
        }
        initViews();
        initWebView();
        initToolbar();
        this.mWebView.loadUrl(this.base_url);
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.mWebView != null) {
            this.container.removeAllViews();
            this.mWebView.stopLoading();
            this.mWebView.clearCache(false);
            this.mWebView.destroyDrawingCache();
            this.mWebView.setWebChromeClient(null);
            this.mWebView.setWebViewClient(null);
            this.mWebView.removeAllViews();
            this.mWebView.destroy();
            this.mWebView = null;
        }
    }

    public void onFavorChange(boolean z) {
        this.isCollected = z;
    }

    public void onLeftGesture() {
        if (this.cmtAble) {
            Intent intent = new Intent(getApplicationContext(), CommentActivity.class);
            intent.putExtra("Info", this.mInfo);
            intent.putExtra("clickType", this.clickType);
            intent.putExtra("from", this.from);
            startActivityForResult(intent, 10);
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        this.mInfo = (Info) getIntent().getParcelableExtra("Info");
        if (this.mInfo != null) {
            this.info_url = this.mInfo.getUrl();
            this.m = this.mInfo.getM();
            this.v_t = this.mInfo.getV_t();
            this.nid = this.mInfo.getNid();
            this.mWebView.loadUrl(this.base_url);
            if (Config.CHANNEL_ID.equals(this.mInfo.getNocmt())) {
                this.cmtAble = false;
            }
            this.from = getIntent().getStringExtra("from");
            new av(this, this.info_url, this.from, this.nid, this.mInfo.getA(), "b88f54f7f545e6b3", this.mInfo.getTitle(), null, null).a(null, new Void[0]);
        }
        super.onNewIntent(intent);
    }

    public void onPageFinished(WebView webView, String str) {
        float height = (((float) this.mToolbar.getHeight()) / com.qihoo360.daily.h.a.a((Activity) this)) - 1.0f;
        this.mWebView.loadUrl("javascript:window.$netType=" + (com.qihoo360.daily.h.b.b(Application.getInstance()) ? 1 : 0));
        this.mWebView.loadUrl("javascript:document.body.style.marginTop=\"" + height + "px\"; void 0");
        String str2 = null;
        if (this.mInfo != null) {
            str2 = this.mInfo.getPdate();
        }
        if (!TextUtils.isEmpty(this.pushId)) {
            this.info_url += "&" + this.pushId;
        }
        new s(getApplicationContext(), this.info_url, this.m, this.nid, this.newtrans, false, str2, this.pushId).a(this.detailOnNetRequestListener, new Void[0]);
        switch (a.b(this)) {
            case SMALL:
                onTextSizeChange(WebSettings.TextSize.SMALLER);
                return;
            case MEDIUM:
                onTextSizeChange(WebSettings.TextSize.NORMAL);
                return;
            case LARGE:
                onTextSizeChange(WebSettings.TextSize.LARGER);
                return;
            case X_LARGE:
                onTextSizeChange(WebSettings.TextSize.LARGEST);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.mWebView.pauseTimers();
        super.onPause();
        if (this.mInfo != null) {
            new av(this, this.info_url, this.from, this.nid, this.mInfo.getA(), "b88f54f7f545e6b4", this.mInfo.getTitle(), null, null).a(null, new Void[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.mWebView.resumeTimers();
        super.onResume();
        if (this.mInfo != null) {
            new av(this, this.info_url, this.from, this.nid, this.mInfo.getA(), "b88f54f7f545e6b3", this.mInfo.getTitle(), null, null).a(null, new Void[0]);
        }
    }

    public void onRightGesture() {
        finish();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public void onScroll(int i, int i2, int i3, int i4) {
        float f = (float) (i2 - i4);
        if (this.alphaLen == 0.0f) {
            this.alphaLen = (float) (getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material) * 2);
        }
        if (f < 0.0f) {
            this.alpha = ((-f) / this.alphaLen) + this.alpha;
        } else if (i2 > this.mToolbar.getHeight()) {
            this.alpha -= f / this.alphaLen;
        }
        ad.a("alpha:" + this.alpha);
        this.alpha = Math.max(this.alpha, 0.0f);
        this.alpha = Math.min(this.alpha, 1.0f);
        if (((double) this.alpha) > 0.5d) {
            showActionbarShadow();
            if (!this.mToolbar.isEnabled()) {
                setToolBarEnabled(true);
                this.mToolbar.setEnabled(true);
            }
            ad.a("mToolbar.setEnabled(true);");
        } else {
            hideActionbarShadow();
            if (this.mToolbar.isEnabled()) {
                setToolBarEnabled(false);
                this.mToolbar.setEnabled(false);
            }
            ad.a("mToolbar.setEnabled(false);");
        }
        ViewCompat.setAlpha(this.mToolbar, this.alpha);
    }

    @TargetApi(14)
    public void onTextSizeChange(WebSettings.TextSize textSize) {
        if (this.mWebView != null) {
            switch (AnonymousClass8.$SwitchMap$android$webkit$WebSettings$TextSize[textSize.ordinal()]) {
                case 1:
                    this.mWebView.loadUrl("javascript:adjustFontSize('small');");
                    return;
                case 2:
                    this.mWebView.loadUrl("javascript:adjustFontSize('normal');");
                    return;
                case 3:
                    this.mWebView.loadUrl("javascript:adjustFontSize('big');");
                    return;
                case 4:
                    this.mWebView.loadUrl("javascript:adjustFontSize('extraBig');");
                    return;
                default:
                    return;
            }
        }
    }

    @android.webkit.JavascriptInterface
    public void setData(String str, String str2) {
        if ("headimg".equals(str) && !TextUtils.isEmpty(str2)) {
            this.mFirstImageUrl = str2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.h.b.a(android.app.Activity, java.lang.String):boolean
     arg types: [com.qihoo360.daily.activity.NewsDetailActivity, java.lang.String]
     candidates:
      com.qihoo360.daily.h.b.a(android.content.Context, float):int
      com.qihoo360.daily.h.b.a(java.lang.String, java.text.SimpleDateFormat):long
      com.qihoo360.daily.h.b.a(java.lang.String, java.lang.String):java.lang.String
      com.qihoo360.daily.h.b.a(android.content.Context, java.lang.String):java.util.List<com.qihoo360.daily.model.Info>
      com.qihoo360.daily.h.b.a(long, long):boolean
      com.qihoo360.daily.h.b.a(android.app.Activity, java.lang.String):boolean */
    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        try {
            String decode = URLDecoder.decode(str, "utf8");
            ad.a(decode);
            if (TextUtils.isEmpty(decode) || decode.startsWith("qkw:") || com.qihoo360.daily.h.b.a((Activity) this, decode)) {
                return true;
            }
            Intent intent = new Intent(getApplicationContext(), SearchDetailActivity.class);
            intent.putExtra(SearchActivity.TAG_URL, decode);
            startActivity(intent);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }
}
