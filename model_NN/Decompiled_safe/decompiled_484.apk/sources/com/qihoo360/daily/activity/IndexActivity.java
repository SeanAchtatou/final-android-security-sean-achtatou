package com.qihoo360.daily.activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;
import com.f.a.g;
import com.qihoo.download.base.AbsDownloadTask;
import com.qihoo.download.impl.so.SoDownloadManager;
import com.qihoo.dynamic.DotCount;
import com.qihoo360.daily.R;
import com.qihoo360.daily.b.d;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.fragment.NavigationFragment;
import com.qihoo360.daily.fragment.SpecialChannelFragment;
import com.qihoo360.daily.g.bh;
import com.qihoo360.daily.g.l;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.ad;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.h.h;
import com.qihoo360.daily.h.p;
import com.qihoo360.daily.model.City;
import com.qihoo360.daily.model.Info;
import java.io.File;
import java.net.URI;

public class IndexActivity extends BaseActivity implements AMapLocationListener {
    public static boolean isDownloadSo = false;
    private static final long oneHour = 3600000;
    private long lastExitTime;
    private DrawerLayout mDrawerLayout;
    private LocationManagerProxy mLocationManagerProxy;
    /* access modifiers changed from: private */
    public NavigationFragment mNavigationFragment;
    private String mNewsSuggestText;

    private void clearCache(final Context context, final String str) {
        new Thread(new Runnable() {
            public void run() {
                new d(context).a(Info.class);
                if (!TextUtils.isEmpty(str)) {
                    a.a(new File(str));
                }
            }
        }).start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void createShortCut() {
        Intent intent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        intent.putExtra("android.intent.extra.shortcut.NAME", getString(R.string.app_name));
        intent.putExtra("duplicate", false);
        Intent intent2 = new Intent("android.intent.action.MAIN");
        intent2.setClassName(this, SplashActivity.class.getName());
        intent2.putExtra("isShortcut", true);
        intent.putExtra("android.intent.extra.shortcut.INTENT", intent2);
        intent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(this, R.drawable.ic_launcher));
        sendBroadcast(intent);
    }

    private void downLoadSo() {
        if (b.b(this) && SoDownloadManager.getInstance().needDownloadSo(this)) {
            isDownloadSo = true;
            SoDownloadManager.getInstance().setListener(new SoDownloadManager.IDowloadSoListener() {
                public void onDownloadFailed(AbsDownloadTask absDownloadTask) {
                    IndexActivity.isDownloadSo = false;
                }

                public void onDownloadSizeChanged(AbsDownloadTask absDownloadTask) {
                }

                public void onDownloadSucess(AbsDownloadTask absDownloadTask) {
                    IndexActivity.isDownloadSo = false;
                }
            });
            SoDownloadManager.getInstance().downloadSo(getApplicationContext());
        }
    }

    private void exit() {
        long currentTimeMillis = System.currentTimeMillis();
        this.lastExitTime = currentTimeMillis;
        if (currentTimeMillis - this.lastExitTime < 2000) {
            clearCache(getApplicationContext(), h.f1128a);
            exitToUploadRunTime();
            finish();
            return;
        }
        ay.a(getApplicationContext()).a((int) R.string.exit_text);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.b(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String):java.lang.String
      com.qihoo360.daily.f.d.b(android.content.Context, int):void
      com.qihoo360.daily.f.d.b(android.content.Context, long):void
      com.qihoo360.daily.f.d.b(android.content.Context, java.util.List<com.qihoo360.daily.model.Info>):void
      com.qihoo360.daily.f.d.b(android.content.Context, boolean):void */
    private void newInstall() {
        boolean equals = "Xiaomi".equals(Build.BRAND);
        int c = a.c(getApplicationContext());
        int o = com.qihoo360.daily.f.d.o(getApplicationContext());
        if (c > o) {
            if (o > 0) {
                if (o < 211) {
                    com.qihoo360.daily.f.d.a(this, new String[]{"dailyinfolist"}, "DAILY_NEWS");
                    com.qihoo360.daily.f.d.a(this, new String[]{"key_channels_json"}, "SETING");
                }
                if (!equals && o < 301) {
                    removeShortCut();
                    createShortCut();
                }
            }
            a.k(getApplicationContext());
            if (!com.qihoo360.daily.f.d.k(getApplicationContext())) {
                if (!equals) {
                    createShortCut();
                }
                com.qihoo360.daily.f.d.b(getApplicationContext(), true);
            }
        }
    }

    private void removeShortCut() {
        Intent intent = new Intent("com.android.launcher.action.UNINSTALL_SHORTCUT");
        intent.putExtra("android.intent.extra.shortcut.NAME", getString(R.string.app_name));
        intent.putExtra("android.intent.extra.shortcut.INTENT", new Intent("android.intent.action.MAIN").setComponent(new ComponentName(getPackageName(), SplashActivity.class.getName())));
        sendBroadcast(intent);
    }

    private void startLocation() {
        this.mLocationManagerProxy = LocationManagerProxy.getInstance((Activity) this);
        this.mLocationManagerProxy.setGpsEnable(false);
        this.mLocationManagerProxy.requestLocationData(LocationProviderProxy.AMapNetwork, oneHour, 500.0f, this);
    }

    private void stopLocation() {
        if (this.mLocationManagerProxy != null) {
            this.mLocationManagerProxy.removeUpdates(this);
            this.mLocationManagerProxy.destroy();
        }
        this.mLocationManagerProxy = null;
    }

    private void updateTemplates() {
        new bh(getApplicationContext()).a((c) null, new Void[0]);
    }

    public String getNewsSuggestText() {
        return this.mNewsSuggestText;
    }

    public void go2Activity(Class<? extends Activity> cls) {
        startActivity(new Intent(this, cls));
    }

    public void initToolBar() {
        Toolbar configToolbar = configToolbar(R.menu.menu_tool_bar, 0, 0);
        configToolbar.setLogo((int) R.drawable.daily_action_bar_logo);
        AnonymousClass1 r0 = new ActionBarDrawerToggle(this, this.mDrawerLayout, configToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                IndexActivity.this.mNavigationFragment.release();
            }

            public void onDrawerOpened(View view) {
                super.onDrawerOpened(view);
                new l(IndexActivity.this.getApplicationContext(), "personal").a(null, new Void[0]);
                b.b(IndexActivity.this, "Topbar_sidebar");
            }

            public void onDrawerSlide(View view, float f) {
                super.onDrawerSlide(view, f);
            }

            public void onDrawerStateChanged(int i) {
                super.onDrawerStateChanged(i);
            }
        };
        this.mDrawerLayout.setDrawerListener(r0);
        this.mDrawerLayout.setDrawerShadow((int) R.drawable.ic_personal_center_shadow, 8388611);
        this.mDrawerLayout.setScrimColor(Color.parseColor("#88000000"));
        r0.syncState();
        configToolbar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (IndexActivity.this.mNavigationFragment.getChannelSubscribeFragment() == null) {
                    b.b(view.getContext(), "Totop_onClick");
                    IndexActivity.this.mNavigationFragment.scrollToTopAndRefresh();
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        com.qihoo360.daily.i.b.INSTANCE.a(i, i2, intent);
        if (this.mNavigationFragment != null) {
            this.mNavigationFragment.onActivityResult(i, i2, intent);
        }
    }

    public void onBackPressed() {
        if (!this.mNavigationFragment.exitSubscribeEditMode() && !this.mNavigationFragment.getChildFragmentManager().popBackStackImmediate()) {
            exit();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        newInstall();
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_index);
        this.mNewsSuggestText = getIntent().getStringExtra(SplashActivity.NEWS_SUGGEST);
        int a2 = p.a(this);
        this.mNavigationFragment = (NavigationFragment) getSupportFragmentManager().findFragmentById(R.id.navigationFragment);
        View view = this.mNavigationFragment.getView();
        if (view != null) {
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            layoutParams.width = a2;
            view.setLayoutParams(layoutParams);
        }
        this.mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        updateTemplates();
        downLoadSo();
        startLocation();
        initToolBar();
        DotCount.setAppName(getString(R.string.dot_app_name));
        URI d = bi.d(getApplicationContext());
        if (d != null) {
            String uri = d.toString();
            ad.a("sendCount url:" + uri);
            ad.a("sendCount code:" + DotCount.sendCount(getApplicationContext(), uri));
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        stopLocation();
        g.d(Application.getInstance());
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return super.onKeyDown(i, keyEvent);
    }

    public void onLocationChanged(Location location) {
    }

    public void onLocationChanged(final AMapLocation aMapLocation) {
        if (aMapLocation != null && aMapLocation.getAMapException() != null && aMapLocation.getAMapException().getErrorCode() == 0) {
            final Handler handler = new Handler(new Handler.Callback() {
                public boolean handleMessage(Message message) {
                    SpecialChannelFragment.broadCastLocationChanged(IndexActivity.this.getApplicationContext());
                    return false;
                }
            });
            final com.qihoo360.daily.b.a aVar = new com.qihoo360.daily.b.a(getApplicationContext());
            new Thread(new Runnable() {
                public void run() {
                    City city = new City();
                    city.setCity(aMapLocation.getCity());
                    city.setCode(aMapLocation.getCityCode());
                    city.setProvince(aMapLocation.getProvince());
                    city.setAuto(true);
                    city.setDistrict(aMapLocation.getDistrict());
                    City a2 = aVar.a(city);
                    if (a2 == null || TextUtils.isEmpty(a2.getCitySpell()) || TextUtils.isEmpty(a2.getCode())) {
                        a2 = com.qihoo360.daily.f.d.a();
                    }
                    String city2 = a2.getCity();
                    City q = com.qihoo360.daily.f.d.q(IndexActivity.this.getApplicationContext());
                    if (TextUtils.isEmpty(city2) || ((q == null || city2.equals(q.getCity())) && q != null)) {
                        com.qihoo360.daily.f.d.a(IndexActivity.this.getApplicationContext(), a2);
                        return;
                    }
                    com.qihoo360.daily.f.d.a(IndexActivity.this.getApplicationContext(), a2);
                    handler.sendEmptyMessage(0);
                }
            }).start();
        }
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_search:
                startActivity(new Intent(getApplicationContext(), SearchActivity.class));
                b.b(getApplicationContext(), "Topbar_search");
                new l(getApplicationContext(), "personal").a(null, new Void[0]);
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.mNavigationFragment.refreshDaily();
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
