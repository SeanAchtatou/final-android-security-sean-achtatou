package com.qihoo360.daily.activity;

import com.f.a.g;

public class BaseNoFragmentActivity extends BaseActivity {
    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        g.b(getClass().getSimpleName());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        g.a(getClass().getSimpleName());
    }
}
