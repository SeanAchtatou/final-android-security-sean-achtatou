package com.qihoo360.daily.activity;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.qihoo360.daily.R;
import com.qihoo360.daily.a.ak;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.g.z;
import com.qihoo360.daily.h.ad;
import com.qihoo360.daily.h.ai;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.Result;
import com.qihoo360.daily.model.SpecialTopic;
import com.qihoo360.daily.widget.RecyclerViewDivider.SpecialTopicItemDecoration;

public class SpecialTopicActivity extends BaseNoFragmentActivity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public View error_layout;
    private RecyclerView.OnScrollListener listener = new RecyclerView.OnScrollListener() {
        private float alpha = 1.0f;
        private float alphaLen;
        private int t;

        private void setToolBarEnabled(boolean z) {
            if (z) {
                SpecialTopicActivity.this.mToolbar.setNavigationIcon((int) R.drawable.ic_actionbar_back);
            } else {
                SpecialTopicActivity.this.mToolbar.setNavigationIcon((Drawable) null);
            }
            Menu menu = SpecialTopicActivity.this.mToolbar.getMenu();
            int size = menu.size();
            for (int i = 0; i < size; i++) {
                menu.getItem(i).setEnabled(z);
            }
        }

        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            super.onScrollStateChanged(recyclerView, i);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.max(float, float):float}
         arg types: [float, int]
         candidates:
          ClspMth{java.lang.Math.max(double, double):double}
          ClspMth{java.lang.Math.max(int, int):int}
          ClspMth{java.lang.Math.max(long, long):long}
          ClspMth{java.lang.Math.max(float, float):float} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(float, float):float}
         arg types: [float, int]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(long, long):long}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(float, float):float} */
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            super.onScrolled(recyclerView, i, i2);
            this.t += i2;
            ad.a("dx: " + i);
            ad.a("dy: " + i2);
            ad.a("t: " + this.t);
            float f = (float) i2;
            if (this.alphaLen == 0.0f) {
                this.alphaLen = (float) (SpecialTopicActivity.this.getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material) * 2);
            }
            if (f < 0.0f) {
                this.alpha = ((-f) / this.alphaLen) + this.alpha;
            } else if (this.t > SpecialTopicActivity.this.mToolbar.getHeight()) {
                this.alpha -= f / this.alphaLen;
            }
            ad.a("alpha: " + this.alpha);
            this.alpha = Math.max(this.alpha, 0.0f);
            this.alpha = Math.min(this.alpha, 1.0f);
            if (((double) this.alpha) > 0.5d) {
                SpecialTopicActivity.this.showActionbarShadow();
                if (!SpecialTopicActivity.this.mToolbar.isEnabled()) {
                    setToolBarEnabled(true);
                    SpecialTopicActivity.this.mToolbar.setEnabled(true);
                }
            } else {
                SpecialTopicActivity.this.hideActionbarShadow();
                if (SpecialTopicActivity.this.mToolbar.isEnabled()) {
                    setToolBarEnabled(false);
                    SpecialTopicActivity.this.mToolbar.setEnabled(false);
                }
            }
            ViewCompat.setAlpha(SpecialTopicActivity.this.mToolbar, this.alpha);
        }
    };
    /* access modifiers changed from: private */
    public View loading_layout;
    private Info mInfo;
    /* access modifiers changed from: private */
    public Toolbar mToolbar;
    /* access modifiers changed from: private */
    public SpecialTopic mTopic;
    private c<Void, Result<SpecialTopic>> onNetRequestListener = new c<Void, Result<SpecialTopic>>() {
        public void onNetRequest(int i, Result<SpecialTopic> result) {
            SpecialTopicActivity.this.loading_layout.setVisibility(8);
            if (result != null) {
                SpecialTopic unused = SpecialTopicActivity.this.mTopic = result.getData();
                SpecialTopicActivity.this.mToolbar.setTitle(SpecialTopicActivity.this.mTopic.getTitle());
                SpecialTopicActivity.this.recyclerView.setAdapter(new ak(SpecialTopicActivity.this, SpecialTopicActivity.this.mTopic));
                return;
            }
            SpecialTopicActivity.this.error_layout.setVisibility(0);
        }

        public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
            onNetRequest(i, (Result<SpecialTopic>) ((Result) obj));
        }
    };
    /* access modifiers changed from: private */
    public RecyclerView recyclerView;

    private void refesh() {
        this.error_layout.setVisibility(8);
        this.loading_layout.setVisibility(0);
        new z(getApplicationContext(), this.mInfo.getM(), this.mInfo.getNid()).a(this.onNetRequestListener, new String[0]);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.error_layout:
                refesh();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_special_topic);
        this.mInfo = (Info) getIntent().getParcelableExtra("Info");
        this.recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        this.recyclerView.addOnScrollListener(this.listener);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this) {
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(-1, -2);
            }
        });
        this.recyclerView.addItemDecoration(new SpecialTopicItemDecoration(this, 1));
        this.error_layout = findViewById(R.id.error_layout);
        this.error_layout.setOnClickListener(this);
        this.loading_layout = findViewById(R.id.loading_layout);
        this.mToolbar = configToolbar(R.menu.menu_special_topic, 0, R.drawable.ic_actionbar_back);
        this.mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SpecialTopicActivity.this.onBackPressed();
            }
        });
        refesh();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.h.ai.<init>(android.app.Activity, com.qihoo360.daily.model.Info, java.lang.String, java.lang.String, boolean):void
     arg types: [com.qihoo360.daily.activity.SpecialTopicActivity, com.qihoo360.daily.model.Info, java.lang.String, java.lang.String, int]
     candidates:
      com.qihoo360.daily.h.ai.<init>(android.app.Activity, com.qihoo360.daily.model.Info, com.qihoo360.daily.model.NewsDetail, java.lang.String, java.lang.String):void
      com.qihoo360.daily.h.ai.<init>(android.app.Activity, com.qihoo360.daily.model.Info, java.lang.String, java.lang.String, boolean):void */
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_share:
                if (!(this.mTopic == null || this.mInfo == null)) {
                    new ai((Activity) this, this.mInfo, this.mTopic.getShorturl(), this.mInfo.getImgurl(), true).a();
                    break;
                }
        }
        return super.onMenuItemClick(menuItem);
    }
}
