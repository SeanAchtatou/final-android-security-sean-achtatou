package com.qihoo360.daily.activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.g.e;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.af;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.h.ba;
import com.qihoo360.daily.h.h;
import com.qihoo360.daily.h.r;
import com.qihoo360.daily.model.UpdateInfo;
import com.qihoo360.daily.model.User;
import com.qihoo360.daily.widget.DiscoveryDialog;
import com.qihoo360.daily.widget.FontSizeSelectDialog;
import com.qihoo360.daily.widget.MaterialCheckBox;
import com.qihoo360.daily.wxapi.WXHelper;
import com.qihoo360.daily.wxapi.WXInfoKeeper;
import com.qihoo360.daily.wxapi.WXUser;
import java.io.File;

public class SettingsActivity extends BaseNoFragmentActivity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public TextView checkSummary;
    private TextView checkTitle;
    /* access modifiers changed from: private */
    public ImageView imChecking;
    /* access modifiers changed from: private */
    public boolean isCheckUpdating;
    /* access modifiers changed from: private */
    public TextView mCacheSummary = null;
    private View mCleaning;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public DiscoveryDialog mDialog;
    /* access modifiers changed from: private */
    public View mExitBtn = null;
    /* access modifiers changed from: private */
    public TextView mFontSizeSummary = null;
    private MaterialCheckBox mImgModeSwitch = null;
    private MaterialCheckBox mPushSwitch = null;
    /* access modifiers changed from: private */
    public MaterialCheckBox mToneSwitch = null;
    private ViewGroup mToneSwitchItem = null;
    /* access modifiers changed from: private */
    public MaterialCheckBox mVirbateSwitch = null;
    private ViewGroup mVirbateSwitchItem = null;
    /* access modifiers changed from: private */
    public User mWBUser;
    /* access modifiers changed from: private */
    public WXUser mWXUser;
    private c<Void, UpdateInfo> onNetRequestListener = new c<Void, UpdateInfo>() {
        public void onNetRequest(int i, UpdateInfo updateInfo) {
            boolean unused = SettingsActivity.this.isCheckUpdating = false;
            if (updateInfo == null) {
                ay.a(Application.getInstance()).a((int) R.string.net_error);
                SettingsActivity.this.imChecking.clearAnimation();
                SettingsActivity.this.imChecking.setVisibility(8);
                SettingsActivity.this.checkSummary.setVisibility(0);
                return;
            }
            d.a(SettingsActivity.this.getApplicationContext(), updateInfo);
            SettingsActivity.this.imChecking.clearAnimation();
            SettingsActivity.this.imChecking.setVisibility(8);
            SettingsActivity.this.checkSummary.setVisibility(0);
            if (a.c(SettingsActivity.this) < updateInfo.getVersionCode()) {
                SettingsActivity.this.checkSummary.setText("可更新至" + updateInfo.getVersionName() + "版本");
                new ba(SettingsActivity.this, updateInfo).a(updateInfo.getForceUpdate());
                return;
            }
            SettingsActivity.this.checkSummary.setText("当前已是最新版本");
            ay.a(Application.getInstance()).a("您的版本已经是最新");
        }
    };

    private String getFontSizeText() {
        return getResources().getStringArray(R.array.settings_pref_font_size_list)[com.qihoo360.daily.f.a.b(this).ordinal()];
    }

    private void go2Activity(Class<? extends Activity> cls) {
        startActivity(new Intent(this, cls));
    }

    private void initGeneralViews() {
        ((TextView) findViewById(R.id.settings_category_general).findViewById(R.id.settings_categate_title)).setText((int) R.string.settings_pref_header_general);
        View findViewById = findViewById(R.id.settings_item_font_size);
        ((TextView) findViewById.findViewById(R.id.settings_item_title)).setText((int) R.string.settings_pref_title_font_size);
        this.mFontSizeSummary = (TextView) findViewById.findViewById(R.id.settings_item_summary);
        View findViewById2 = findViewById(R.id.settings_item_img_mode);
        ((TextView) findViewById2.findViewById(R.id.settings_item_title)).setText((int) R.string.settings_pref_title_img_mode);
        this.mImgModeSwitch = (MaterialCheckBox) findViewById2.findViewById(R.id.settings_checkbox);
        this.mImgModeSwitch.setOncheckListener(new MaterialCheckBox.OnCheckListener() {
            public void onCheck(boolean z, boolean z2) {
                com.qihoo360.daily.f.a.a(SettingsActivity.this, com.qihoo360.daily.f.c.IMG_MODE, z);
            }
        });
        View findViewById3 = findViewById(R.id.settings_item_cache);
        ((TextView) findViewById3.findViewById(R.id.settings_item_title)).setText((int) R.string.settings_pref_title_clean_cache);
        this.mCacheSummary = (TextView) findViewById3.findViewById(R.id.settings_item_summary);
        this.mCleaning = findViewById3.findViewById(R.id.setting_item_cleaning);
        loading(false);
        findViewById.setOnClickListener(this);
        findViewById2.setOnClickListener(this);
        findViewById3.setOnClickListener(this);
        View findViewById4 = findViewById(R.id.settings_item_check_update);
        findViewById4.setOnClickListener(this);
        this.checkTitle = (TextView) findViewById4.findViewById(R.id.settings_item_title);
        this.checkTitle.setText((int) R.string.settings_pref_title_check_update);
        this.checkSummary = (TextView) findViewById4.findViewById(R.id.settings_item_summary);
        this.checkSummary.setText("当前版本" + a.b((Context) this));
        this.imChecking = (ImageView) findViewById4.findViewById(R.id.im_update_checking);
    }

    private void initOtherViews() {
        ((TextView) findViewById(R.id.settings_category_other).findViewById(R.id.settings_categate_title)).setText((int) R.string.settings_pref_header_other);
        View findViewById = findViewById(R.id.settings_item_about);
        ((TextView) findViewById.findViewById(R.id.settings_item_title)).setText((int) R.string.settings_pref_title_about);
        findViewById.setOnClickListener(this);
        View findViewById2 = findViewById(R.id.settings_item_complain);
        ((TextView) findViewById2.findViewById(R.id.settings_item_title)).setText((int) R.string.settings_pref_title_complain);
        findViewById2.setOnClickListener(this);
    }

    private void initPushViews() {
        ((TextView) findViewById(R.id.settings_category_push).findViewById(R.id.settings_categate_title)).setText((int) R.string.settings_pref_header_push);
        View findViewById = findViewById(R.id.settings_item_push);
        ((TextView) findViewById.findViewById(R.id.settings_item_title)).setText((int) R.string.settings_pref_title_recive_push);
        this.mPushSwitch = (MaterialCheckBox) findViewById.findViewById(R.id.settings_checkbox);
        this.mPushSwitch.setOncheckListener(new MaterialCheckBox.OnCheckListener() {
            public void onCheck(boolean z, boolean z2) {
                if (!z2) {
                    SettingsActivity.this.mToneSwitch.setChecked(SettingsActivity.this.mToneSwitch.isChecked() ? z : SettingsActivity.this.mToneSwitch.isChecked());
                    SettingsActivity.this.mVirbateSwitch.setChecked(SettingsActivity.this.mVirbateSwitch.isChecked() ? z : SettingsActivity.this.mVirbateSwitch.isChecked());
                }
                com.qihoo360.daily.f.a.a(SettingsActivity.this, com.qihoo360.daily.f.c.PUSH, z);
                SettingsActivity.this.setupPushItems();
                b.b(SettingsActivity.this.mContext, "Setting_bottom_menu_push_switch");
            }
        });
        this.mToneSwitchItem = (ViewGroup) findViewById(R.id.settings_item_tone);
        ((TextView) this.mToneSwitchItem.findViewById(R.id.settings_item_title)).setText((int) R.string.settings_pref_title_voice);
        this.mToneSwitch = (MaterialCheckBox) this.mToneSwitchItem.findViewById(R.id.settings_checkbox);
        this.mToneSwitch.setOncheckListener(new MaterialCheckBox.OnCheckListener() {
            public void onCheck(boolean z, boolean z2) {
                com.qihoo360.daily.f.a.a(SettingsActivity.this, com.qihoo360.daily.f.c.TONE, z);
                b.b(SettingsActivity.this.mContext, "Setting_bottom_menu_push_sound");
            }
        });
        this.mVirbateSwitchItem = (ViewGroup) findViewById(R.id.settings_item_virbate);
        ((TextView) this.mVirbateSwitchItem.findViewById(R.id.settings_item_title)).setText((int) R.string.settings_pref_title_virbate);
        this.mVirbateSwitch = (MaterialCheckBox) this.mVirbateSwitchItem.findViewById(R.id.settings_checkbox);
        this.mVirbateSwitch.setOncheckListener(new MaterialCheckBox.OnCheckListener() {
            public void onCheck(boolean z, boolean z2) {
                com.qihoo360.daily.f.a.a(SettingsActivity.this, com.qihoo360.daily.f.c.VIRBATE, z);
                b.b(SettingsActivity.this.mContext, "Setting_bottom_menu_push_shake");
            }
        });
        findViewById.setOnClickListener(this);
        this.mToneSwitchItem.setOnClickListener(this);
        this.mVirbateSwitchItem.setOnClickListener(this);
    }

    private void initToolbar() {
        configToolbar(0, R.string.settings_pref_title, R.drawable.ic_actionbar_back).setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SettingsActivity.this.onBackPressed();
            }
        });
    }

    private void initViews() {
        initGeneralViews();
        initPushViews();
        initOtherViews();
        User a2 = d.a(getApplicationContext());
        this.mWBUser = a2;
        if (a2 == null) {
            WXUser userInfo4WX = WXInfoKeeper.getUserInfo4WX(getApplicationContext());
            this.mWXUser = userInfo4WX;
            if (userInfo4WX == null) {
                return;
            }
        }
        this.mExitBtn = findViewById(R.id.settings_item_exit);
        this.mExitBtn.setVisibility(0);
        this.mExitBtn.setOnClickListener(this);
    }

    private void loadSettings() {
        this.mFontSizeSummary.setText(getFontSizeText());
        this.mImgModeSwitch.setChecked(com.qihoo360.daily.f.a.c(this), true);
        this.mCacheSummary.setText("");
        this.mPushSwitch.setChecked(com.qihoo360.daily.f.a.d(this), true);
        this.mToneSwitch.setChecked(com.qihoo360.daily.f.a.e(this), true);
        this.mVirbateSwitch.setChecked(com.qihoo360.daily.f.a.f(this), true);
        setupPushItems();
    }

    /* access modifiers changed from: private */
    public void loading(boolean z) {
        if (z) {
            this.mCacheSummary.setVisibility(8);
            this.mCleaning.setVisibility(0);
            this.mCleaning.startAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.rotate_loading_anim));
            return;
        }
        this.mCleaning.clearAnimation();
        this.mCacheSummary.setVisibility(0);
        this.mCleaning.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void onCacheClear() {
        loading(true);
        new e().a(new c<Void, Boolean>() {
            public void onNetRequest(int i, Boolean bool) {
                SettingsActivity.this.loading(false);
                if (bool.booleanValue()) {
                    af.a();
                    ay.a(SettingsActivity.this.getBaseContext()).a(SettingsActivity.this.getString(R.string.settings_pref_toast_cache_cleaned, new Object[]{SettingsActivity.this.mCacheSummary.getText()}));
                    SettingsActivity.this.mCacheSummary.setText("");
                    return;
                }
                SettingsActivity.this.recountCacheSize();
            }
        }, new File(h.f1128a));
    }

    private void onConfirmClean() {
        this.mDialog = new DiscoveryDialog(this);
        this.mDialog.setContentText((int) R.string.settings_clean_confirm_msg);
        this.mDialog.setOnDialogViewClickListener(new DiscoveryDialog.OnDialogViewClickListener() {
            public void onLeftButtonClick() {
                SettingsActivity.this.mDialog.disMissDialog();
            }

            public void onRightButtonClick() {
                SettingsActivity.this.onCacheClear();
                SettingsActivity.this.mDialog.disMissDialog();
            }
        });
        this.mDialog.showDialog();
    }

    private void onExitAccount() {
        this.mDialog = new DiscoveryDialog(this);
        this.mDialog.setContentText((int) R.string.settings_logout_confirm_msg);
        this.mDialog.setOnDialogViewClickListener(new DiscoveryDialog.OnDialogViewClickListener() {
            public void onLeftButtonClick() {
                SettingsActivity.this.mDialog.disMissDialog();
            }

            public void onRightButtonClick() {
                if (SettingsActivity.this.mWBUser != null) {
                    com.qihoo360.daily.i.b.INSTANCE.a(SettingsActivity.this.getApplicationContext());
                } else if (SettingsActivity.this.mWXUser != null) {
                    WXHelper.INSTANCE.logoutWeixin(SettingsActivity.this.getApplicationContext());
                }
                SettingsActivity.this.mDialog.disMissDialog();
                SettingsActivity.this.mExitBtn.setVisibility(8);
            }
        });
        this.mDialog.showDialog();
    }

    private void onSelectFontSize() {
        FontSizeSelectDialog fontSizeSelectDialog = new FontSizeSelectDialog(this);
        fontSizeSelectDialog.setFoneSizeChangedListener(new FontSizeSelectDialog.FoneSizeChangedListener() {
            public void onFoneSizeChanged(CharSequence charSequence) {
                SettingsActivity.this.mFontSizeSummary.setText(charSequence);
            }
        });
        fontSizeSelectDialog.showDialog();
    }

    private void openUrl(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(str));
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void recountCacheSize() {
        new com.qihoo360.daily.g.d().a(new c<Void, Long>() {
            public void onNetRequest(int i, Long l) {
                SettingsActivity.this.mCacheSummary.setText(r.a(l.longValue()));
            }
        }, 0, new File(h.f1128a), af.b());
    }

    /* access modifiers changed from: private */
    public void setupPushItems() {
        boolean d = com.qihoo360.daily.f.a.d(this);
        this.mToneSwitchItem.setEnabled(d);
        this.mToneSwitch.setEnabled(d);
        this.mVirbateSwitch.setEnabled(d);
        this.mVirbateSwitchItem.setEnabled(d);
    }

    public void onClick(View view) {
        boolean z = true;
        switch (view.getId()) {
            case R.id.settings_item_font_size:
                onSelectFontSize();
                b.b(this.mContext, "Setting_bottom_menu_set_font");
                return;
            case R.id.settings_item_img_mode:
                MaterialCheckBox materialCheckBox = this.mImgModeSwitch;
                if (this.mImgModeSwitch.isChecked()) {
                    z = false;
                }
                materialCheckBox.setChecked(z);
                return;
            case R.id.settings_item_cache:
                if (this.mCleaning.getVisibility() != 0) {
                    if (TextUtils.isEmpty(this.mCacheSummary.getText())) {
                        ay.a(this).a((int) R.string.settings_pref_toast_cache_empty);
                    } else {
                        onConfirmClean();
                    }
                    b.b(this.mContext, "Setting_bottom_menu_set_clear");
                    return;
                }
                return;
            case R.id.settings_category_push:
            case R.id.settings_category_other:
            default:
                return;
            case R.id.settings_item_push:
                MaterialCheckBox materialCheckBox2 = this.mPushSwitch;
                if (this.mPushSwitch.isChecked()) {
                    z = false;
                }
                materialCheckBox2.setChecked(z);
                return;
            case R.id.settings_item_tone:
                MaterialCheckBox materialCheckBox3 = this.mToneSwitch;
                if (this.mToneSwitch.isChecked()) {
                    z = false;
                }
                materialCheckBox3.setChecked(z);
                return;
            case R.id.settings_item_virbate:
                MaterialCheckBox materialCheckBox4 = this.mVirbateSwitch;
                if (this.mVirbateSwitch.isChecked()) {
                    z = false;
                }
                materialCheckBox4.setChecked(z);
                return;
            case R.id.settings_item_about:
                go2Activity(AboutActivity.class);
                b.b(this.mContext, "Setting_bottom_menu_about_about");
                return;
            case R.id.settings_item_complain:
                openUrl("http://www.haosou.com/help/help_right.html");
                b.b(this.mContext, "Setting_bottom_menu_about_legalguidance");
                return;
            case R.id.settings_item_check_update:
                this.checkSummary.setVisibility(8);
                this.imChecking.setVisibility(0);
                this.imChecking.startAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.rotate_loading_anim));
                if (!this.isCheckUpdating) {
                    this.isCheckUpdating = true;
                    new com.qihoo360.daily.g.b(this).a(this.onNetRequestListener, new Object[0]);
                    b.b(this.mContext, "Setting_bottom_menu_about_update");
                    return;
                }
                return;
            case R.id.settings_item_exit:
                onExitAccount();
                b.b(this.mContext, "Setting_bottom_menu_logout");
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_settings);
        this.mContext = this;
        initViews();
        loadSettings();
        initToolbar();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        recountCacheSize();
    }
}
