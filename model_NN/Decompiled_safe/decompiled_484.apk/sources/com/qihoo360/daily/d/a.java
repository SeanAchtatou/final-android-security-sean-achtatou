package com.qihoo360.daily.d;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import com.mediav.ads.sdk.adcore.Config;
import com.qihoo.messenger.util.QHeader;
import com.qihoo360.daily.activity.Application;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class a {

    /* renamed from: a  reason: collision with root package name */
    public static final String f962a = String.valueOf(com.qihoo360.daily.h.a.c(Application.getInstance()));

    /* renamed from: b  reason: collision with root package name */
    public static final String f963b = com.qihoo360.daily.h.a.j(Application.getInstance());
    public static final String c = Build.DISPLAY;
    public static final String d = String.valueOf(Build.VERSION.SDK_INT);
    public static final String e = ((TelephonyManager) Application.getInstance().getSystemService("phone")).getNetworkOperatorName();
    public static String f = "";
    public static String g = "";

    public static void a() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) Application.getInstance().getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting()) {
            switch (activeNetworkInfo.getType()) {
                case 0:
                    f = "mobile";
                    switch (activeNetworkInfo.getSubtype()) {
                        case 1:
                        case 2:
                            g = "2G";
                            return;
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 12:
                            g = "3G";
                            return;
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                        case 11:
                        default:
                            return;
                        case 13:
                        case 14:
                        case 15:
                            g = "4G";
                            return;
                    }
                case 1:
                case 6:
                case 9:
                    f = "wifi";
                    g = "";
                    return;
                default:
                    return;
            }
        }
    }

    public static void a(List<NameValuePair> list) {
        list.add(new BasicNameValuePair("ver", f962a));
        list.add(new BasicNameValuePair("channel", f963b));
        list.add(new BasicNameValuePair("os_type", QHeader.os));
        list.add(new BasicNameValuePair("os", c));
        list.add(new BasicNameValuePair("os_ver", d));
        list.add(new BasicNameValuePair("carrier", e));
        a();
        list.add(new BasicNameValuePair("access", f));
        list.add(new BasicNameValuePair("access_subtype", g));
        list.add(new BasicNameValuePair("optimize_img", Config.CHANNEL_ID));
    }
}
