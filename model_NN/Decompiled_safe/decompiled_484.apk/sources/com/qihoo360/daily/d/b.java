package com.qihoo360.daily.d;

import com.e.a.ab;
import com.e.a.am;
import com.e.a.ar;
import com.e.a.as;
import com.e.a.au;
import com.e.a.c;
import java.io.InputStream;
import java.net.URI;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.http.Header;
import org.apache.http.NameValuePair;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private static am f964a = a();

    private static am a() {
        am amVar = new am();
        amVar.a((c) null);
        amVar.a(10, TimeUnit.SECONDS);
        amVar.b(10, TimeUnit.SECONDS);
        amVar.c(10, TimeUnit.SECONDS);
        amVar.a(true);
        return amVar;
    }

    public static au a(String str, Header... headerArr) {
        ar arVar = new ar();
        arVar.a(str);
        if (headerArr != null) {
            for (Header header : headerArr) {
                if (header != null) {
                    arVar.b(header.getName(), header.getValue());
                }
            }
        }
        return f964a.a(arVar.a()).a();
    }

    public static InputStream a(String str) {
        au a2 = a(str, new Header[0]);
        if (a2 == null || !a2.d() || a2.h() == null) {
            return null;
        }
        return a2.h().d();
    }

    public static String a(URI uri, List<NameValuePair> list, Header... headerArr) {
        ab abVar = new ab();
        if (list != null) {
            for (NameValuePair next : list) {
                abVar.a(next.getName(), next.getValue());
            }
        }
        as a2 = abVar.a();
        ar arVar = new ar();
        if (headerArr != null) {
            for (Header header : headerArr) {
                if (header != null) {
                    arVar.b(header.getName(), header.getValue());
                }
            }
        }
        arVar.a(uri.toURL());
        arVar.a(a2);
        au a3 = f964a.a(arVar.a()).a();
        if (a3 == null || !a3.d() || a3.h() == null) {
            return null;
        }
        return a3.h().f();
    }

    public static String a(URI uri, Header... headerArr) {
        au a2 = a(uri.toURL().toString(), headerArr);
        if (a2 == null || !a2.d() || a2.h() == null) {
            return null;
        }
        return a2.h().f();
    }

    public static byte[] b(String str) {
        au a2 = a(str, new Header[0]);
        if (a2 == null || !a2.d() || a2.h() == null) {
            return null;
        }
        return a2.h().e();
    }
}
