package com.qihoo360.daily.f;

public enum c {
    IMG_MODE("settings_img_mode"),
    PUSH("settings_push"),
    TONE("settings_tone"),
    VIRBATE("settings_virbate"),
    IMPROVE_JOIN("about_improve_join");
    
    private String f;

    private c(String str) {
        this.f = str;
    }

    public String toString() {
        return this.f;
    }
}
