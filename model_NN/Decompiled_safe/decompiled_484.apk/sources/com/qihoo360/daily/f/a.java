package com.qihoo360.daily.f;

import android.content.Context;
import com.qihoo360.daily.wxapi.WXInfoKeeper;

public final class a {
    public static int a(Context context) {
        int d = d.d(context, "settings_font_size");
        if (-1 != d) {
            return d;
        }
        a(context, b.MEDIUM);
        return b.MEDIUM.a();
    }

    public static void a(Context context, b bVar) {
        d.a(context, "settings_font_size", bVar.a());
    }

    public static void a(Context context, boolean z) {
        b(context, "settings_push", z);
    }

    public static boolean a(Context context, c cVar, boolean z) {
        b(context, cVar.toString(), z);
        return z;
    }

    private static boolean a(Context context, String str, boolean z) {
        return d.b(context, str, z);
    }

    public static b b(Context context) {
        return b.b(a(context));
    }

    private static void b(Context context, String str, boolean z) {
        d.a(context, str, z);
    }

    public static void b(Context context, boolean z) {
        b(context, "settings_tone", z);
    }

    public static void c(Context context, boolean z) {
        b(context, "settings_virbate", z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, int):int
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, long):long
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, java.lang.String):void
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean c(Context context) {
        return d.b(context, "settings_img_mode", true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, int):int
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, long):long
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, java.lang.String):void
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean d(Context context) {
        return d.b(context, "settings_push", true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.a.a(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.qihoo360.daily.f.a.a(android.content.Context, com.qihoo360.daily.f.c, boolean):boolean
      com.qihoo360.daily.f.a.a(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean e(Context context) {
        return a(context, "settings_tone", false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.a.a(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.qihoo360.daily.f.a.a(android.content.Context, com.qihoo360.daily.f.c, boolean):boolean
      com.qihoo360.daily.f.a.a(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean f(Context context) {
        return a(context, "settings_virbate", false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.a.a(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.qihoo360.daily.f.a.a(android.content.Context, com.qihoo360.daily.f.c, boolean):boolean
      com.qihoo360.daily.f.a.a(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean g(Context context) {
        return a(context, "about_improve_join", true);
    }

    public static boolean h(Context context) {
        return (d.a(context) == null && WXInfoKeeper.getUserInfo4WX(context) == null) ? false : true;
    }

    public static void i(Context context) {
        if (!context.getSharedPreferences("SETING", 0).contains("settings_push")) {
            a(context, d(context));
            b(context, e(context));
            c(context, f(context));
            b(context, "about_improve_join", true);
        }
    }
}
