package com.qihoo360.daily.f;

public enum b {
    SMALL,
    MEDIUM,
    LARGE,
    X_LARGE;
    
    private int e = (ordinal() + 1);

    public static b a(int i) {
        return values()[i];
    }

    public static b b(int i) {
        return values()[i - 1];
    }

    public int a() {
        return this.e;
    }
}
