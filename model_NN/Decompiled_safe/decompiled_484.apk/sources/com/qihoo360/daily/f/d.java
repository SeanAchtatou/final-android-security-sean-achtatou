package com.qihoo360.daily.f;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Base64;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.activity.LoginActivity;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.h.bj;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.City;
import com.qihoo360.daily.model.FavouriteInfo;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.QdData;
import com.qihoo360.daily.model.UpdateInfo;
import com.qihoo360.daily.model.User;
import java.util.ArrayList;
import java.util.List;

public class d {
    public static City a() {
        City city = new City();
        city.setProvince("北京");
        city.setCity("北京");
        city.setDistrict("朝阳");
        city.setCode("101010100");
        city.setCitySpell("beijing");
        city.setProvinceSpell("beijing");
        city.setAuto(true);
        return city;
    }

    public static User a(Context context) {
        User user;
        if (context == null) {
            return null;
        }
        try {
            String b2 = b(context, "userJson4wb");
            if (!TextUtils.isEmpty(b2)) {
                b(context);
                e(context, b2);
            } else {
                String b3 = b(context, LoginActivity.KEY_USER_INFO);
                if (TextUtils.isEmpty(b3)) {
                    return null;
                }
                b2 = new String(Base64.decode(b3, 0));
            }
            user = (User) Application.getGson().a(b2, new e().getType());
        } catch (Exception e) {
            e.printStackTrace();
            user = null;
        }
        return user;
    }

    public static String a(Context context, String str) {
        if (context == null) {
            return null;
        }
        return c(context, str, "SETING");
    }

    public static void a(Context context, int i) {
        c(context, "", i);
    }

    public static void a(Context context, long j) {
        a(context, "crash_at", j);
    }

    public static void a(Context context, long j, String str) {
        if (context != null && !TextUtils.isEmpty(str)) {
            c(context, "last_refresh_" + str, j);
        }
    }

    public static void a(Context context, City city) {
        if (city != null && context != null) {
            try {
                String a2 = Application.getGson().a(city);
                SharedPreferences.Editor edit = context.getSharedPreferences("SETING", 0).edit();
                edit.putString(ChannelType.TYPE_CHANNEL_LOCAL, a2);
                edit.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void a(Context context, QdData qdData) {
        if (qdData != null && context != null) {
            try {
                String a2 = Application.getGson().a(qdData);
                SharedPreferences.Editor edit = context.getSharedPreferences("SETING", 0).edit();
                edit.putString("qd_data", a2);
                edit.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void a(Context context, UpdateInfo updateInfo) {
        if (context != null) {
            SharedPreferences.Editor edit = context.getSharedPreferences("SETING", 0).edit();
            edit.putString("key_update_info", Application.toJson(updateInfo));
            edit.commit();
        }
    }

    public static void a(Context context, String str, int i) {
        if (context != null) {
            SharedPreferences.Editor edit = context.getSharedPreferences("SETING", 0).edit();
            edit.putInt(str, i);
            edit.commit();
        }
    }

    public static void a(Context context, String str, long j) {
        if (context != null) {
            SharedPreferences.Editor edit = context.getSharedPreferences("SETING", 0).edit();
            edit.putLong(str, j);
            edit.commit();
        }
    }

    public static void a(Context context, String str, String str2) {
        a(context, str, str2, "SETING");
    }

    public static void a(Context context, String str, String str2, String str3) {
        if (context != null && !TextUtils.isEmpty(str3)) {
            SharedPreferences.Editor edit = context.getSharedPreferences(str3, 0).edit();
            edit.putString(str, str2);
            edit.commit();
        }
    }

    public static void a(Context context, String str, boolean z) {
        if (context != null) {
            SharedPreferences.Editor edit = context.getSharedPreferences("SETING", 0).edit();
            edit.putBoolean(str, z);
            edit.commit();
        }
    }

    public static void a(Context context, List<FavouriteInfo> list) {
        g(context, Application.getGson().a(list));
    }

    public static void a(Context context, boolean z) {
        a(context, "favor_edit", z);
    }

    public static void a(Context context, String[] strArr, String str) {
        if (context != null && strArr != null && strArr.length > 0 && !TextUtils.isEmpty(str)) {
            SharedPreferences.Editor edit = context.getSharedPreferences(str, 0).edit();
            for (String remove : strArr) {
                edit.remove(remove);
            }
            edit.commit();
        }
    }

    public static int b(Context context, String str, int i) {
        return context == null ? i : context.getSharedPreferences("SETING", 0).getInt(str, i);
    }

    public static long b(Context context, String str, long j) {
        return (context == null || TextUtils.isEmpty(str)) ? j : context.getSharedPreferences("SETING", 0).getLong(str, j);
    }

    @SuppressLint({"InlinedApi"})
    public static String b(Context context, String str) {
        return d(context, "SETING", str);
    }

    public static void b(Context context) {
        SharedPreferences sharedPreferences = bj.a() ? context.getSharedPreferences("SETING", 4) : context.getSharedPreferences("SETING", 0);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        if (sharedPreferences.contains("userJson4wb")) {
            edit.remove("userJson4wb");
        }
        edit.remove(LoginActivity.KEY_USER_INFO);
        edit.commit();
    }

    public static void b(Context context, int i) {
        if (context != null) {
            SharedPreferences.Editor edit = context.getSharedPreferences("DATA", 0).edit();
            edit.putInt("old_version_code", i);
            edit.commit();
        }
    }

    public static void b(Context context, long j) {
        if (context != null) {
            c(context, "key_dialog_video_time", j);
        }
    }

    @SuppressLint({"InlinedApi"})
    public static void b(Context context, String str, String str2) {
        b(context, "SETING", str, str2);
    }

    @SuppressLint({"InlinedApi"})
    public static void b(Context context, String str, String str2, String str3) {
        if (context != null) {
            SharedPreferences.Editor edit = (bj.a() ? context.getSharedPreferences(str, 4) : context.getSharedPreferences(str, 0)).edit();
            edit.putString(str2, str3);
            edit.commit();
        }
    }

    public static void b(Context context, List<Info> list) {
        h(context, Application.getGson().a(list));
    }

    public static void b(Context context, boolean z) {
        if (context != null) {
            a(context, "is_shortcut_created", z);
        }
    }

    public static boolean b(Context context, String str, boolean z) {
        return context == null ? z : context.getSharedPreferences("SETING", 0).getBoolean(str, z);
    }

    public static long c(Context context, long j) {
        return b(context, "key_dialog_video_time", j);
    }

    public static long c(Context context, String str) {
        if (context == null) {
            return -1;
        }
        return context.getSharedPreferences("SETING", 0).getLong(str, -1);
    }

    public static String c(Context context) {
        if (context == null) {
            return null;
        }
        return context.getSharedPreferences("DATA", 0).getString("keywords", null);
    }

    public static String c(Context context, String str, String str2) {
        if (context == null || TextUtils.isEmpty(str2)) {
            return null;
        }
        return context.getSharedPreferences(str2, 0).getString(str, null);
    }

    public static void c(Context context, String str, int i) {
        if (context != null) {
            SharedPreferences.Editor edit = context.getSharedPreferences("SETING", 0).edit();
            edit.putInt("dot_hour" + str, i);
            edit.commit();
        }
    }

    public static void c(Context context, String str, long j) {
        if (context != null && !TextUtils.isEmpty(str)) {
            SharedPreferences.Editor edit = context.getSharedPreferences("SETING", 0).edit();
            edit.putLong(str, j);
            edit.commit();
        }
    }

    public static void c(Context context, String str, boolean z) {
        if (z) {
            SharedPreferences.Editor edit = context.getSharedPreferences("DATA", 0).edit();
            edit.putString("key_mark_info", str);
            edit.commit();
            return;
        }
        k(context, "key_mark_info");
    }

    public static void c(Context context, boolean z) {
        if (context != null) {
            a(context, "key_checked_video_24", z);
        }
    }

    public static int d(Context context, String str) {
        if (context == null) {
            return -1;
        }
        return context.getSharedPreferences("SETING", 0).getInt(str, -1);
    }

    @SuppressLint({"InlinedApi"})
    public static String d(Context context, String str, String str2) {
        if (context == null) {
            return null;
        }
        return (bj.a() ? context.getSharedPreferences(str, 4) : context.getSharedPreferences(str, 0)).getString(str2, null);
    }

    public static ArrayList<FavouriteInfo> d(Context context) {
        String h = h(context);
        if (!TextUtils.isEmpty(h)) {
            try {
                return (ArrayList) Application.getGson().a(h, new f().getType());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static void d(Context context, long j) {
        if (context != null) {
            c(context, "key_dialog_gif_time", j);
        }
    }

    public static void d(Context context, boolean z) {
        if (context != null) {
            a(context, "key_checked_gif_24", z);
        }
    }

    public static long e(Context context, long j) {
        return b(context, "key_dialog_gif_time", j);
    }

    public static ArrayList<Info> e(Context context) {
        String i = i(context);
        if (!TextUtils.isEmpty(i)) {
            try {
                return (ArrayList) Application.getGson().a(i, new g().getType());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static void e(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            str = Base64.encodeToString(str.getBytes(), 0);
        }
        b(context, LoginActivity.KEY_USER_INFO, str);
    }

    public static void f(Context context, long j) {
        if (context != null) {
            SharedPreferences.Editor edit = context.getSharedPreferences("SETING", 0).edit();
            edit.putLong("key_tianqi_update_time", j);
            edit.commit();
        }
    }

    public static void f(Context context, String str) {
        if (context != null) {
            SharedPreferences.Editor edit = context.getSharedPreferences("DATA", 0).edit();
            edit.putString("keywords", str);
            edit.commit();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, int):int
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, long):long
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, java.lang.String):void
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean f(Context context) {
        return b(context, "favor_edit", false);
    }

    public static void g(Context context) {
        k(context, "favour_news" + b.c(context));
    }

    public static void g(Context context, String str) {
        if (context != null) {
            SharedPreferences.Editor edit = context.getSharedPreferences("DATA", 0).edit();
            edit.putString("favour_news" + b.c(context), str);
            edit.commit();
        }
    }

    public static String h(Context context) {
        if (context == null) {
            return null;
        }
        return context.getSharedPreferences("DATA", 0).getString("favour_news" + b.c(context), null);
    }

    public static void h(Context context, String str) {
        if (context != null) {
            SharedPreferences.Editor edit = context.getSharedPreferences("DATA", 0).edit();
            edit.putString("push_news" + b.c(context), str);
            edit.commit();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, long):long
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, int):int
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, java.lang.String):void
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, boolean):boolean
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, long):long */
    public static long i(Context context, String str) {
        if (TextUtils.isEmpty(str) || context == null) {
            return 0;
        }
        return b(context, "last_refresh_" + str, 0L);
    }

    public static String i(Context context) {
        if (context == null) {
            return null;
        }
        return context.getSharedPreferences("DATA", 0).getString("push_news" + b.c(context), null);
    }

    public static int j(Context context, String str) {
        if (context == null) {
            return -1;
        }
        return context.getSharedPreferences("SETING", 0).getInt("dot_hour" + str, -1);
    }

    public static long j(Context context) {
        return c(context, "crash_at");
    }

    private static void k(Context context, String str) {
        if (context != null) {
            SharedPreferences.Editor edit = context.getSharedPreferences("DATA", 0).edit();
            edit.remove(str);
            edit.commit();
        }
    }

    public static boolean k(Context context) {
        return context != null && b(context, "is_shortcut_created", false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, int):int
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, long):long
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, java.lang.String):void
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean l(Context context) {
        return b(context, "key_checked_video_24", false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, int):int
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, long):long
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, java.lang.String):void
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean m(Context context) {
        return b(context, "key_checked_gif_24", false);
    }

    public static String n(Context context) {
        return context.getSharedPreferences("DATA", 0).getString("key_mark_info", null);
    }

    public static int o(Context context) {
        if (context == null) {
            return -1;
        }
        return context.getSharedPreferences("DATA", 0).getInt("old_version_code", 0);
    }

    public static long p(Context context) {
        SharedPreferences sharedPreferences;
        if (context == null || (sharedPreferences = context.getSharedPreferences("SETING", 0)) == null) {
            return 0;
        }
        return sharedPreferences.getLong("key_tianqi_update_time", -1);
    }

    public static City q(Context context) {
        City a2 = a();
        String string = context.getSharedPreferences("SETING", 0).getString(ChannelType.TYPE_CHANNEL_LOCAL, null);
        if (string == null) {
            return a2;
        }
        try {
            return (City) Application.getGson().a(string, City.class);
        } catch (Exception e) {
            e.printStackTrace();
            return a2;
        }
    }

    public static QdData r(Context context) {
        String string = context.getSharedPreferences("SETING", 0).getString("qd_data", null);
        if (TextUtils.isEmpty(string)) {
            return null;
        }
        try {
            return (QdData) Application.getGson().a(string, QdData.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
