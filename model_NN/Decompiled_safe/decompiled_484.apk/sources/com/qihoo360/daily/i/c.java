package com.qihoo360.daily.i;

import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.model.User;

class c extends com.qihoo360.daily.d.c<Void, User> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f1153a;

    c(b bVar) {
        this.f1153a = bVar;
    }

    /* renamed from: a */
    public void onNetRequest(int i, User user) {
        if (b.b(this.f1153a) != null) {
            if (user != null) {
                ay.a(Application.getInstance()).a((int) R.string.navigation_drawer_login_success);
                b.b(this.f1153a).OnWeiboLoginSuccess(user);
            } else {
                ay.a(Application.getInstance()).a("failed:" + user);
                b.b(this.f1153a).OnWeiboLoginError(user);
            }
            b.INSTANCE.a();
        }
    }
}
