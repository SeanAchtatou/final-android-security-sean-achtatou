package com.qihoo360.daily.i;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.model.User;
import com.sina.weibo.sdk.a.a.a;
import com.sina.weibo.sdk.d.f;

public enum b {
    INSTANCE;
    

    /* renamed from: b  reason: collision with root package name */
    private a f1152b;
    private e c;
    private c<Void, User> d = new c(this);

    public void a() {
        this.c = null;
    }

    public void a(int i, int i2, Intent intent) {
        if (this.f1152b != null) {
            this.f1152b.a(i, i2, intent);
        }
    }

    public void a(Activity activity, e eVar) {
        f.a();
        this.f1152b = new a(activity, new com.sina.weibo.sdk.a.a(activity, "432085026", "http://sh.qihoo.com/api/callback.php", "email,direct_messages_read,direct_messages_write,friendships_groups_read,friendships_groups_write,statuses_to_me_read,follow_app_official_microblog,invitation_write"));
        this.c = eVar;
    }

    public void a(Context context) {
        a.b(context);
        d.b(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, boolean):void
     arg types: [com.qihoo360.daily.activity.Application, java.lang.String, int]
     candidates:
      com.qihoo360.daily.f.d.a(android.content.Context, long, java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, int):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, long):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String[], java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, boolean):void */
    public boolean b(Activity activity, e eVar) {
        a(activity, eVar);
        if (this.f1152b == null) {
            d.a((Context) Application.getInstance(), "login_status", false);
            ay.a(activity).a("请重试");
            return false;
        } else if (!this.f1152b.a()) {
            ay.a(activity).a((int) R.string.login_weibo_uninstalled);
            d.a((Context) Application.getInstance(), "login_status", false);
            return false;
        } else {
            this.f1152b.a(new d(this));
            if (!com.qihoo360.daily.h.b.d(Application.getInstance())) {
                d.a((Context) Application.getInstance(), "login_status", false);
            }
            return true;
        }
    }
}
