package com.qihoo360.daily.i;

import android.content.Context;
import android.os.Bundle;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.g.af;
import com.qihoo360.daily.h.ay;
import com.sina.weibo.sdk.a.b;
import com.sina.weibo.sdk.a.c;

class d implements c {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f1154a;

    d(b bVar) {
        this.f1154a = bVar;
    }

    public void a() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, boolean):void
     arg types: [com.qihoo360.daily.activity.Application, java.lang.String, int]
     candidates:
      com.qihoo360.daily.f.d.a(android.content.Context, long, java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, int):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, long):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String[], java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, boolean):void */
    public void a(Bundle bundle) {
        com.qihoo360.daily.f.d.a((Context) Application.getInstance(), "login_status", false);
        b a2 = b.a(bundle);
        if (a2 != null && a2.a()) {
            ay.a(Application.getInstance()).a((int) R.string.navigation_drawer_login_success);
            com.qihoo360.daily.h.b.b(Application.getInstance(), "Login_with_weibo");
            a.a(Application.getInstance(), a2);
            new af(Application.getInstance(), a2.b(), a2.c()).a(b.a(this.f1154a), new Void[0]);
        }
    }

    public void a(com.sina.weibo.sdk.c.c cVar) {
    }
}
