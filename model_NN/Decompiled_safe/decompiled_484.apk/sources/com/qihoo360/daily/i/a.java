package com.qihoo360.daily.i;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Base64;
import com.qihoo360.daily.h.bj;
import com.qihoo360.daily.wxapi.WXConfig;
import com.sina.weibo.sdk.a.b;

public class a {
    @SuppressLint({"InlinedApi"})
    public static b a(Context context) {
        if (context == null) {
            return null;
        }
        b bVar = new b();
        SharedPreferences sharedPreferences = bj.a() ? context.getSharedPreferences("weibo_android", 4) : context.getSharedPreferences("weibo_android", 0);
        if (sharedPreferences.contains("uid")) {
            bVar.a(sharedPreferences.getString("uid", ""));
            bVar.b(sharedPreferences.getString(WXConfig.WX_ACCCESS_TOKEN, ""));
            bVar.a(sharedPreferences.getLong("expires_in", 0));
            b(context);
            a(context, bVar);
        } else {
            String string = sharedPreferences.getString("key_uid", "");
            String string2 = sharedPreferences.getString(WXConfig.WX_ACCCESS_TOKEN, "");
            if (TextUtils.isEmpty(string) || TextUtils.isEmpty(string2)) {
                return null;
            }
            bVar.a(new String(Base64.decode(string, 0)));
            bVar.b(new String(Base64.decode(string2, 0)));
            bVar.a(sharedPreferences.getLong("expires_in", 0));
        }
        return bVar;
    }

    @SuppressLint({"InlinedApi"})
    public static void a(Context context, b bVar) {
        if (context != null && bVar != null && !TextUtils.isEmpty(bVar.b()) && !TextUtils.isEmpty(bVar.c())) {
            SharedPreferences.Editor edit = (bj.a() ? context.getSharedPreferences("weibo_android", 4) : context.getSharedPreferences("weibo_android", 0)).edit();
            edit.putString("key_uid", Base64.encodeToString(bVar.b().getBytes(), 0));
            edit.putString(WXConfig.WX_ACCCESS_TOKEN, Base64.encodeToString(bVar.c().getBytes(), 0));
            edit.putLong("expires_in", bVar.d());
            edit.commit();
        }
    }

    @SuppressLint({"InlinedApi"})
    public static void b(Context context) {
        if (context != null) {
            SharedPreferences.Editor edit = (bj.a() ? context.getSharedPreferences("weibo_android", 4) : context.getSharedPreferences("weibo_android", 0)).edit();
            edit.clear();
            edit.commit();
        }
    }
}
