package com.qihoo360.daily.e;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.h.af;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.Info;

public class bt {
    public static RecyclerView.ViewHolder a(Context context) {
        View inflate = View.inflate(context, R.layout.row_special_topic_news_one_img, null);
        bv bvVar = new bv(inflate);
        TextView unused = bvVar.f1008a = (TextView) inflate.findViewById(R.id.title);
        TextView unused2 = bvVar.f1009b = (TextView) inflate.findViewById(R.id.source);
        TextView unused3 = bvVar.c = (TextView) inflate.findViewById(R.id.time);
        ImageView unused4 = bvVar.e = (ImageView) inflate.findViewById(R.id.image);
        TextView unused5 = bvVar.d = (TextView) inflate.findViewById(R.id.commentCount);
        return bvVar;
    }

    public static void a(Context context, RecyclerView.ViewHolder viewHolder, Info info) {
        if (viewHolder != null && (viewHolder instanceof bv)) {
            bv bvVar = (bv) viewHolder;
            viewHolder.itemView.setOnClickListener(new bu(info, context, bvVar));
            a.a(info, bvVar.f1008a);
            bvVar.f1008a.setText(info.getTitle());
            bvVar.f1009b.setText(info.getSrc());
            bvVar.c.setText(b.c(info.getPdate()));
            String cmt_cnt = info.getCmt_cnt();
            if (!TextUtils.isEmpty(cmt_cnt)) {
                bvVar.d.setText(cmt_cnt);
            }
            String imgurl = info.getImgurl();
            if (!TextUtils.isEmpty(imgurl)) {
                bvVar.e.setVisibility(0);
                String[] split = imgurl.split("\\|");
                if (split.length > 0) {
                    imgurl = split[0];
                }
                af.b(null, bvVar.e, imgurl, R.dimen.single_img_width, R.dimen.single_img_height, false, R.drawable.img_fun_pic_holder_small);
                return;
            }
            bvVar.e.setVisibility(8);
        }
    }
}
