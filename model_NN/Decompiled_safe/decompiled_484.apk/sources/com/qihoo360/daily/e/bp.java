package com.qihoo360.daily.e;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.af;
import com.qihoo360.daily.model.Banner;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.SpecialTopic;

public class bp {

    /* renamed from: a  reason: collision with root package name */
    private static int f1001a = a.d(Application.getInstance());

    /* renamed from: b  reason: collision with root package name */
    private static int f1002b = (f1001a / 2);

    public static RecyclerView.ViewHolder a(Context context) {
        View inflate = View.inflate(context, R.layout.row_special_topic_header, null);
        bq bqVar = new bq(inflate);
        bqVar.f1003a = (ImageView) inflate.findViewById(R.id.header_iv);
        bqVar.f1004b = (TextView) inflate.findViewById(R.id.summary_tv);
        bqVar.c = (TextView) inflate.findViewById(R.id.desc);
        return bqVar;
    }

    public static void a(Context context, RecyclerView.ViewHolder viewHolder, Info info) {
        if (viewHolder != null && info != null && (viewHolder instanceof bq) && (info instanceof SpecialTopic)) {
            SpecialTopic specialTopic = (SpecialTopic) info;
            bq bqVar = (bq) viewHolder;
            Banner banner = specialTopic.getBanner();
            if (banner != null) {
                af.a(null, bqVar.f1003a, banner.getImgurl(), f1001a, f1002b, false, R.drawable.img_holder_bg);
            }
            if (banner == null || TextUtils.isEmpty(banner.getDesc())) {
                bqVar.c.setText("");
                bqVar.c.setBackgroundColor(0);
            } else {
                bqVar.c.setText(specialTopic.getBanner().getDesc());
                bqVar.c.setBackgroundResource(R.drawable.bg_gradient);
            }
            bqVar.f1004b.setText(specialTopic.getSummary());
        }
    }
}
