package com.qihoo360.daily.e;

import android.content.Context;
import com.qihoo360.daily.b.d;
import com.qihoo360.daily.model.Info;

final class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f1014a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Info f1015b;
    final /* synthetic */ String[] c;
    final /* synthetic */ String[] d;

    c(Context context, Info info, String[] strArr, String[] strArr2) {
        this.f1014a = context;
        this.f1015b = info;
        this.c = strArr;
        this.d = strArr2;
    }

    public void run() {
        new d(this.f1014a).a(this.f1015b, this.c, this.d);
    }
}
