package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.DailyDetailActivity;
import com.qihoo360.daily.activity.NewsDetailActivity;
import com.qihoo360.daily.activity.SearchActivity;
import com.qihoo360.daily.activity.SearchDetailActivity;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.Info;

final class bn implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bo f997a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Info f998b;
    final /* synthetic */ RecyclerView.ViewHolder c;
    final /* synthetic */ int d;
    final /* synthetic */ String e;
    final /* synthetic */ Activity f;
    final /* synthetic */ int g;

    bn(bo boVar, Info info, RecyclerView.ViewHolder viewHolder, int i, String str, Activity activity, int i2) {
        this.f997a = boVar;
        this.f998b = info;
        this.c = viewHolder;
        this.d = i;
        this.e = str;
        this.f = activity;
        this.g = i2;
    }

    public void onClick(View view) {
        Context context = view.getContext();
        if (context != null) {
            this.f997a.c.setTextColor(context.getResources().getColor(R.color.news_title_read));
            this.f998b.setRead(1);
            a.a(context, this.f998b);
            int itemViewType = this.c.getItemViewType();
            if (itemViewType == 2) {
                Intent intent = new Intent(context, this.f998b.isDailyInfo() ? DailyDetailActivity.class : NewsDetailActivity.class);
                intent.addFlags(67108864);
                intent.putExtra("Info", this.f998b);
                intent.putExtra("position", this.d);
                intent.putExtra("from", this.e);
                this.f.startActivityForResult(intent, this.g);
            } else if (itemViewType == 11) {
                Intent intent2 = new Intent(this.f, SearchDetailActivity.class);
                intent2.putExtra("Info", this.f998b);
                intent2.putExtra(SearchActivity.TAG_URL, this.f998b.getUrl());
                intent2.putExtra("from", ChannelType.TYPE_FAVOR);
                this.f.startActivity(intent2);
            }
            a.a(this.f, this.e);
        }
    }
}
