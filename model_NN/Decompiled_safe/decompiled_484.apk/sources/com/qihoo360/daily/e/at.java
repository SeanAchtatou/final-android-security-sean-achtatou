package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.a.a.j;
import com.qihoo360.daily.R;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.fragment.BaseListFragment;
import com.qihoo360.daily.g.al;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.City;
import com.qihoo360.daily.model.Realtime;
import com.qihoo360.daily.model.Tianqi;
import com.qihoo360.daily.model.Weather;
import com.qihoo360.daily.model.Wind;
import java.util.Calendar;

public class at extends a {
    public static RecyclerView.ViewHolder a(Context context) {
        View inflate = View.inflate(context, R.layout.local_list_item_header, null);
        aw awVar = new aw(inflate);
        TextView unused = awVar.f979a = (TextView) inflate.findViewById(R.id.tv_local_city);
        TextView unused2 = awVar.f980b = (TextView) inflate.findViewById(R.id.tv_local_pm);
        TextView unused3 = awVar.c = (TextView) inflate.findViewById(R.id.tv_local_temperature);
        TextView unused4 = awVar.d = (TextView) inflate.findViewById(R.id.tv_local_date);
        TextView unused5 = awVar.e = (TextView) inflate.findViewById(R.id.tv_local_weather_lunar);
        return awVar;
    }

    private static Tianqi a(Activity activity, aw awVar, String str) {
        String a2 = d.a(activity, str);
        if (!TextUtils.isEmpty(a2)) {
            try {
                return (Tianqi) new j().a(a2, new av().getType());
            } catch (Exception e) {
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001b A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x001e A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0021 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0024 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0027 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x002a A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x002d A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0030 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0033 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0036 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0039 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003c A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0015 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0018 A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String a(int r2, int r3) {
        /*
            int r1 = r2 % 7
            int r0 = r1 * r3
            if (r1 != 0) goto L_0x0009
            switch(r3) {
                case -1: goto L_0x0012;
                case 0: goto L_0x0009;
                case 1: goto L_0x000f;
                default: goto L_0x0009;
            }
        L_0x0009:
            r1 = r0
        L_0x000a:
            r0 = 0
            switch(r1) {
                case -7: goto L_0x0039;
                case -6: goto L_0x0036;
                case -5: goto L_0x0033;
                case -4: goto L_0x0030;
                case -3: goto L_0x002d;
                case -2: goto L_0x002a;
                case -1: goto L_0x003c;
                case 0: goto L_0x000e;
                case 1: goto L_0x0027;
                case 2: goto L_0x0015;
                case 3: goto L_0x0018;
                case 4: goto L_0x001b;
                case 5: goto L_0x001e;
                case 6: goto L_0x0021;
                case 7: goto L_0x0024;
                default: goto L_0x000e;
            }
        L_0x000e:
            return r0
        L_0x000f:
            r0 = 7
            r1 = r0
            goto L_0x000a
        L_0x0012:
            r0 = -7
            r1 = r0
            goto L_0x000a
        L_0x0015:
            java.lang.String r0 = "星期一"
            goto L_0x000e
        L_0x0018:
            java.lang.String r0 = "星期二"
            goto L_0x000e
        L_0x001b:
            java.lang.String r0 = "星期三"
            goto L_0x000e
        L_0x001e:
            java.lang.String r0 = "星期四"
            goto L_0x000e
        L_0x0021:
            java.lang.String r0 = "星期五"
            goto L_0x000e
        L_0x0024:
            java.lang.String r0 = "星期六"
            goto L_0x000e
        L_0x0027:
            java.lang.String r0 = "星期日"
            goto L_0x000e
        L_0x002a:
            java.lang.String r0 = "MON"
            goto L_0x000e
        L_0x002d:
            java.lang.String r0 = "TUE"
            goto L_0x000e
        L_0x0030:
            java.lang.String r0 = "WED"
            goto L_0x000e
        L_0x0033:
            java.lang.String r0 = "THU"
            goto L_0x000e
        L_0x0036:
            java.lang.String r0 = "FRI"
            goto L_0x000e
        L_0x0039:
            java.lang.String r0 = "SAT"
            goto L_0x000e
        L_0x003c:
            java.lang.String r0 = "SUN"
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.e.at.a(int, int):java.lang.String");
    }

    public static void a(Activity activity, RecyclerView.ViewHolder viewHolder, BaseListFragment baseListFragment) {
        aw awVar = (aw) viewHolder;
        City q = d.q(activity);
        awVar.f979a.setText(q.getCity());
        awVar.f979a.setOnClickListener(new au(activity));
        long p = d.p(activity);
        if (p == -1) {
            a(activity, q, awVar);
            a(activity, awVar, q);
        } else if (!b.a(p, System.currentTimeMillis())) {
            a(activity, q, awVar);
            a(activity, awVar, q);
        } else if (!a(activity, q, awVar)) {
            a(activity, awVar, q);
        }
    }

    private static void a(Activity activity, aw awVar, City city) {
        new al(activity, city.getCode()).a(new ax(awVar, city, activity), new Void[0]);
    }

    private static boolean a(Activity activity, City city, aw awVar) {
        Tianqi a2;
        if (city == null) {
            city = d.q(activity);
        }
        if (city != null) {
            String code = city.getCode();
            if (!TextUtils.isEmpty(code) && (a2 = a(activity, awVar, code)) != null) {
                b(a2, activity, awVar);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public static void b(Tianqi tianqi, Context context, aw awVar) {
        String str;
        String str2 = null;
        if (tianqi != null) {
            String pm = tianqi.getPm();
            if (!TextUtils.isEmpty(pm)) {
                pm = "PM2.5: " + pm;
            }
            Realtime realtime = tianqi.getRealtime();
            awVar.f979a.setText(realtime.getCity_name());
            Weather weather = realtime.getWeather();
            Wind wind = realtime.getWind();
            String moon = realtime.getMoon();
            if (weather != null) {
                str = weather.getTemperature();
                String info = weather.getInfo();
                if (!TextUtils.isEmpty(info)) {
                    str2 = info + "/";
                }
            } else {
                str = null;
            }
            if (wind != null) {
                String direct = wind.getDirect();
                if (!TextUtils.isEmpty(direct)) {
                    str2 = str2 + direct;
                }
            }
            if (!TextUtils.isEmpty(moon)) {
                str2 = str2 + ("　农历" + moon);
            }
            if (!TextUtils.isEmpty(str)) {
                str = str + "°";
            }
            awVar.f980b.setText(pm);
            awVar.c.setText(str);
            awVar.e.setText(str2);
            long time = tianqi.getTime() * 1000;
            Calendar instance = Calendar.getInstance();
            instance.setTimeInMillis(time);
            awVar.d.setText(a(instance.get(7), 1) + "," + b.c(time));
        }
    }
}
