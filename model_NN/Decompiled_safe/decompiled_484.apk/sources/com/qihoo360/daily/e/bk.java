package com.qihoo360.daily.e;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;
import com.qihoo360.daily.fragment.DailyFragment;
import com.qihoo360.daily.model.Info;

final class bk implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Info f995a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ bl f996b;
    final /* synthetic */ int c;
    final /* synthetic */ String d;
    final /* synthetic */ int e;
    final /* synthetic */ Activity f;
    final /* synthetic */ Fragment g;

    bk(Info info, bl blVar, int i, String str, int i2, Activity activity, Fragment fragment) {
        this.f995a = info;
        this.f996b = blVar;
        this.c = i;
        this.d = str;
        this.e = i2;
        this.f = activity;
        this.g = fragment;
    }

    public void onClick(View view) {
        bj.b(this.f995a, this.f996b, this.c, this.d, -1, this.e, this.f);
        if (this.g instanceof DailyFragment) {
            ((DailyFragment) this.g).setNewsClicked(true);
        }
    }
}
