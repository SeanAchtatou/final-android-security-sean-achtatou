package com.qihoo360.daily.e;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.SpecialTopicLanmu;

public class br {
    public static RecyclerView.ViewHolder a(Context context) {
        View inflate = View.inflate(context, R.layout.row_special_topic_lanmu, null);
        bs bsVar = new bs(inflate);
        bsVar.f1005a = (TextView) inflate.findViewById(R.id.topic_name_tv);
        return bsVar;
    }

    public static void a(Context context, RecyclerView.ViewHolder viewHolder, Info info) {
        if (viewHolder != null && info != null && (viewHolder instanceof bs) && (info instanceof SpecialTopicLanmu)) {
            ((bs) viewHolder).f1005a.setText(((SpecialTopicLanmu) info).getColumnName());
        }
    }
}
