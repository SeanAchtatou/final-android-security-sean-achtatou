package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.DailyDetailActivity;
import com.qihoo360.daily.activity.NewsDetailActivity;
import com.qihoo360.daily.activity.SearchActivity;
import com.qihoo360.daily.activity.SearchDetailActivity;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.af;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.Info;

public class cc extends a {
    public static RecyclerView.ViewHolder a(Context context) {
        View inflate = View.inflate(context, R.layout.row_subnews_single_image, null);
        ce ceVar = new ce(inflate);
        View unused = ceVar.n = inflate.findViewById(R.id.ll_card_root);
        ImageView unused2 = ceVar.p = (ImageView) inflate.findViewById(R.id.iv_image);
        TextView unused3 = ceVar.o = (TextView) inflate.findViewById(R.id.title);
        return ceVar;
    }

    public static void a(Activity activity, Fragment fragment, RecyclerView.ViewHolder viewHolder, int i, Info info, String str, int i2) {
        ce ceVar = (ce) viewHolder;
        viewHolder.itemView.setOnClickListener(new cd(info, ceVar, i, str, i2, activity, fragment));
        String title = info.getTitle();
        if (!TextUtils.isEmpty(title)) {
            ceVar.o.setText(title);
        } else {
            ceVar.o.setText("");
        }
        String imgurl = info.getImgurl();
        if (!TextUtils.isEmpty(imgurl)) {
            String[] split = imgurl.split("\\|");
            if (split.length > 0) {
                String str2 = str;
                af.b(str2, ceVar.p, split[0], R.dimen.single_img_width, R.dimen.single_img_height, false, R.drawable.img_fun_pic_holder_small);
            }
        } else {
            ceVar.p.setScaleType(ImageView.ScaleType.CENTER);
            ceVar.p.setImageResource(R.drawable.img_fun_pic_holder_small);
        }
        a(info, ceVar.itemView);
        View c = ceVar.n;
        int a2 = (int) a.a(activity, 16.0f);
        if (info.isCardTail()) {
            c.setPadding(a2, a2, a2, (int) a.a(activity, 25.0f));
        } else {
            c.setPadding(a2, a2, a2, a2);
        }
    }

    /* access modifiers changed from: private */
    public static void b(Info info, ce ceVar, int i, String str, int i2, int i3, Activity activity) {
        Intent intent;
        if (11 == info.getV_t()) {
            info.setRead(1);
            info.setView(info.getView() + 1);
            Intent intent2 = new Intent(activity, SearchDetailActivity.class);
            intent2.putExtra("Info", info);
            intent2.putExtra(SearchActivity.TAG_URL, info.getUrl());
            intent2.putExtra("from", str);
            activity.startActivity(intent2);
            a.a(info, ceVar.o);
            b.b(activity, "daily_onClick_wailian");
            return;
        }
        info.setView(info.getView() + 1);
        info.setRead(1);
        a.a(info, ceVar.o);
        a.a(activity, info);
        if (info.isDailyInfo()) {
            intent = new Intent(activity, DailyDetailActivity.class);
            b.b(activity, "daily_onClick_kandian");
        } else {
            intent = new Intent(activity, NewsDetailActivity.class);
            b.b(activity, "daily_onClick_common");
        }
        intent.addFlags(67108864);
        intent.putExtra("Info", info);
        intent.putExtra("position", i);
        intent.putExtra("from", str);
        activity.startActivityForResult(intent, i3);
    }
}
