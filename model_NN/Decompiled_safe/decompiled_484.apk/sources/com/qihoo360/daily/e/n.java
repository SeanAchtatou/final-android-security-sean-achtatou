package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.h.af;
import com.qihoo360.daily.model.Info;
import pl.droidsonroids.gif.GifView;

public class n extends a {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.e.q.a(com.qihoo360.daily.e.q, android.view.View):android.view.View
     arg types: [com.qihoo360.daily.e.q, android.widget.TextView]
     candidates:
      com.qihoo360.daily.e.q.a(com.qihoo360.daily.e.q, android.widget.TextView):android.widget.TextView
      com.qihoo360.daily.e.q.a(com.qihoo360.daily.e.q, pl.droidsonroids.gif.GifView):pl.droidsonroids.gif.GifView
      com.qihoo360.daily.e.q.a(com.qihoo360.daily.e.q, android.view.View):android.view.View */
    public static RecyclerView.ViewHolder a(Context context) {
        View inflate = View.inflate(context, R.layout.row_gif, null);
        inflate.setBackgroundResource(R.drawable.list_selector_background);
        q qVar = new q(inflate);
        GifView unused = qVar.n = (GifView) inflate.findViewById(R.id.iv_image);
        View unused2 = qVar.p = (View) ((TextView) inflate.findViewById(R.id.play_gif));
        TextView unused3 = qVar.o = (TextView) inflate.findViewById(R.id.title);
        qVar.f = (TextView) inflate.findViewById(R.id.tv_praise);
        qVar.g = (TextView) inflate.findViewById(R.id.tv_bury);
        qVar.j = inflate.findViewById(R.id.share);
        qVar.d = (ImageView) inflate.findViewById(R.id.icon_praise);
        qVar.e = (ImageView) inflate.findViewById(R.id.icon_bury);
        qVar.f1029b = inflate.findViewById(R.id.layout_praise);
        qVar.f1028a = inflate.findViewById(R.id.layout_share);
        qVar.c = inflate.findViewById(R.id.layout_bury);
        qVar.h = (TextView) inflate.findViewById(R.id.buryOne);
        qVar.i = (TextView) inflate.findViewById(R.id.plusOne);
        return qVar;
    }

    private static String a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return str.split("\\|")[0];
    }

    public static void a(Activity activity, RecyclerView.ViewHolder viewHolder, int i, Info info, String str, int i2) {
        q qVar = (q) viewHolder;
        qVar.o.setText(info.getTitle());
        a(info, qVar.o);
        qVar.itemView.setOnClickListener(new o(activity, qVar, info, i, str, i2));
        String b2 = b(info.getImgurl());
        String a2 = a(info.getImgurl());
        int[] a3 = af.a(b2);
        String a4 = af.a(b2, a3[0], a3[1]);
        qVar.n.a(a4, af.a(a2, a3[0], a3[1]), a3[0], a3[1], new p(activity, qVar, info));
        a(activity, qVar, info);
        View.OnClickListener onClickListener = null;
        if (!d.f(activity)) {
            onClickListener = a(activity, qVar, info, a4, false);
        }
        qVar.f1028a.setOnClickListener(onClickListener);
        qVar.f1029b.setOnClickListener(onClickListener);
        qVar.c.setOnClickListener(onClickListener);
        qVar.g.setOnClickListener(onClickListener);
        qVar.j.setOnClickListener(onClickListener);
        qVar.p.setOnClickListener(onClickListener);
    }

    public static void a(RecyclerView.ViewHolder viewHolder) {
        GifView b2;
        if (viewHolder != null && viewHolder.itemView != null && (b2 = ((q) viewHolder).n) != null) {
            b2.a();
        }
    }

    private static String b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        String[] split = str.split("\\|");
        if (split.length > 1) {
            return split[1];
        }
        return null;
    }
}
