package com.qihoo360.daily.e;

import android.view.View;
import com.qihoo360.daily.activity.FavouriteActivity;

final class al implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FavouriteActivity f972a;

    al(FavouriteActivity favouriteActivity) {
        this.f972a = favouriteActivity;
    }

    public void onClick(View view) {
        if (this.f972a != null) {
            this.f972a.loadMore();
        }
    }
}
