package com.qihoo360.daily.e;

import android.app.Activity;
import com.qihoo360.daily.R;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.Info;
import pl.droidsonroids.gif.s;

final class p implements s {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Activity f1040a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ q f1041b;
    final /* synthetic */ Info c;

    p(Activity activity, q qVar, Info info) {
        this.f1040a = activity;
        this.f1041b = qVar;
        this.c = info;
    }

    public void a() {
        this.f1041b.o.setTextColor(this.f1040a.getResources().getColor(R.color.news_title_read));
        this.c.setRead(1);
        if (!ChannelType.TYPE_CHANNEL_GIF.equals(this.c.getChannelId())) {
            a.a(this.f1040a, this.c);
        }
    }
}
