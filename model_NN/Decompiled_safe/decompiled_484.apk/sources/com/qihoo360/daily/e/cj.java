package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import com.qihoo360.daily.activity.DailyDetailActivity;
import com.qihoo360.daily.activity.NewsDetailActivity;
import com.qihoo360.daily.model.Info;

final class cj implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Info f1024a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ck f1025b;
    final /* synthetic */ Activity c;
    final /* synthetic */ int d;
    final /* synthetic */ String e;
    final /* synthetic */ int f;

    cj(Info info, ck ckVar, Activity activity, int i, String str, int i2) {
        this.f1024a = info;
        this.f1025b = ckVar;
        this.c = activity;
        this.d = i;
        this.e = str;
        this.f = i2;
    }

    public void onClick(View view) {
        this.f1024a.setRead(1);
        a.a(this.f1024a, this.f1025b.c);
        a.a(this.c, this.f1024a);
        Intent intent = new Intent(this.c, this.f1024a.isDailyInfo() ? DailyDetailActivity.class : NewsDetailActivity.class);
        intent.addFlags(67108864);
        intent.putExtra("Info", this.f1024a);
        intent.putExtra("position", this.d);
        intent.putExtra("from", this.e);
        this.c.startActivityForResult(intent, this.f);
        a.a(this.c, this.e);
    }
}
