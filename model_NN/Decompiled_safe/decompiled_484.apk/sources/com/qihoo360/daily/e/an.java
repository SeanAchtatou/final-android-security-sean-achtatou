package com.qihoo360.daily.e;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.FavouriteActivity;
import com.qihoo360.daily.fragment.BaseListFragment;

public class an {
    public static RecyclerView.ViewHolder a(Context context) {
        View inflate = View.inflate(context, R.layout.row_daily_foorer, null);
        aq aqVar = new aq(inflate);
        View unused = aqVar.f976a = inflate.findViewById(R.id.ll_parent);
        ImageView unused2 = aqVar.f977b = (ImageView) inflate.findViewById(R.id.go_top);
        TextView unused3 = aqVar.c = (TextView) inflate.findViewById(R.id.tv_loadover_hint);
        return aqVar;
    }

    public static void a(FavouriteActivity favouriteActivity, RecyclerView.ViewHolder viewHolder) {
        ((aq) viewHolder).f976a.setOnClickListener(new ap(favouriteActivity));
    }

    public static void a(FavouriteActivity favouriteActivity, RecyclerView.ViewHolder viewHolder, boolean z) {
        aq aqVar = (aq) viewHolder;
        aqVar.f977b.setVisibility(4);
        aqVar.c.setVisibility(4);
        if (!z) {
            a(favouriteActivity, viewHolder);
            if (viewHolder.itemView.getVisibility() != 0) {
                viewHolder.itemView.setVisibility(0);
            }
        } else if (viewHolder.itemView.getVisibility() != 8) {
            viewHolder.itemView.setVisibility(8);
        }
    }

    public static void a(BaseListFragment baseListFragment, RecyclerView.ViewHolder viewHolder) {
        aq aqVar = (aq) viewHolder;
        aqVar.f976a.setOnClickListener(new ao(aqVar, baseListFragment));
    }
}
