package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.DailyDetailActivity;
import com.qihoo360.daily.activity.NewsDetailActivity;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.Info;

public class bz extends a {
    public static RecyclerView.ViewHolder a(Context context) {
        View inflate = View.inflate(context, R.layout.row_subnews_no_image, null);
        cb cbVar = new cb(inflate);
        View unused = cbVar.n = inflate.findViewById(R.id.ll_card_root);
        TextView unused2 = cbVar.o = (TextView) inflate.findViewById(R.id.title);
        return cbVar;
    }

    public static void a(Activity activity, Fragment fragment, RecyclerView.ViewHolder viewHolder, int i, Info info, String str, int i2) {
        cb cbVar = (cb) viewHolder;
        viewHolder.itemView.setOnClickListener(new ca(info, cbVar, i, str, i2, activity, fragment));
        String title = info.getTitle();
        if (!TextUtils.isEmpty(title)) {
            cbVar.o.setText(title);
        } else {
            cbVar.o.setText("");
        }
        a(info, cbVar.itemView);
        View b2 = cbVar.n;
        int a2 = (int) a.a(activity, 16.0f);
        if (info.isCardTail()) {
            b2.setPadding(a2, a2, a2, (int) a.a(activity, 25.0f));
        } else {
            b2.setPadding(a2, a2, a2, a2);
        }
    }

    /* access modifiers changed from: private */
    public static void b(Info info, cb cbVar, int i, String str, int i2, int i3, Activity activity) {
        Intent intent;
        info.setView(info.getView() + 1);
        info.setRead(1);
        a.a(info, cbVar.o);
        a.a(activity, info);
        if (info.isDailyInfo()) {
            intent = new Intent(activity, DailyDetailActivity.class);
            b.b(activity, "daily_onClick_kandian");
        } else {
            intent = new Intent(activity, NewsDetailActivity.class);
            b.b(activity, "daily_onClick_common");
        }
        intent.addFlags(67108864);
        intent.putExtra("Info", info);
        intent.putExtra("position", i);
        intent.putExtra("from", str);
        activity.startActivityForResult(intent, i3);
    }
}
