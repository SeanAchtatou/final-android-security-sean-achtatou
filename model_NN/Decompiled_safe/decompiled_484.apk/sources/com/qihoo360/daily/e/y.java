package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.af;
import com.qihoo360.daily.model.Info;

public class y extends a {

    /* renamed from: a  reason: collision with root package name */
    private static int f1052a = a.d(Application.getInstance());

    /* renamed from: b  reason: collision with root package name */
    private static int f1053b = ((int) (((double) f1052a) / 2.17d));

    public static RecyclerView.ViewHolder a(Context context) {
        View inflate = View.inflate(context, R.layout.row_imgs, null);
        aa aaVar = new aa(inflate);
        aaVar.f965a = (ImageView) inflate.findViewById(R.id.iv_img);
        aaVar.f966b = (TextView) inflate.findViewById(R.id.tv_img);
        return aaVar;
    }

    public static void a(Activity activity, RecyclerView.ViewHolder viewHolder, String str, Info info) {
        String[] split;
        if (info != null && viewHolder != null && activity != null) {
            aa aaVar = (aa) viewHolder;
            aaVar.f966b.setText(info.getTitle());
            String imgurl = info.getImgurl();
            if (!TextUtils.isEmpty(imgurl) && (split = imgurl.split("\\|")) != null && split.length > 0) {
                imgurl = split[0];
            }
            af.a(str, aaVar.f965a, imgurl, f1052a, f1053b, false, R.drawable.img_fun_pic_holder);
            aaVar.itemView.setOnClickListener(new z(info, activity));
        }
    }
}
