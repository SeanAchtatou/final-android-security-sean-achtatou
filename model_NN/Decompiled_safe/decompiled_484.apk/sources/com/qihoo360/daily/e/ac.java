package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.qihoo360.daily.activity.DailyDetailActivity;
import com.qihoo360.daily.activity.JokeDetailActivity;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.Info;
import java.util.ArrayList;
import java.util.List;

final class ac implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Info f967a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ad f968b;
    final /* synthetic */ List c;
    final /* synthetic */ int d;
    final /* synthetic */ String e;
    final /* synthetic */ Activity f;
    final /* synthetic */ int g;

    ac(Info info, ad adVar, List list, int i, String str, Activity activity, int i2) {
        this.f967a = info;
        this.f968b = adVar;
        this.c = list;
        this.d = i;
        this.e = str;
        this.f = activity;
        this.g = i2;
    }

    public void onClick(View view) {
        Context context = view.getContext();
        if (context != null) {
            this.f967a.setRead(1);
            a.a(this.f967a, this.f968b.n, this.f968b.o);
            if (!ChannelType.TYPE_CHANNEL_JOKE.equals(this.f967a.getChannelId())) {
                a.a(context, this.f967a);
            }
            Intent intent = new Intent(context, this.f967a.isDailyInfo() ? DailyDetailActivity.class : JokeDetailActivity.class);
            intent.putParcelableArrayListExtra("Info", (ArrayList) this.c);
            intent.putExtra("position", this.d);
            intent.putExtra("from", this.e);
            this.f.startActivityForResult(intent, this.g);
            a.a(this.f, this.e);
        }
    }
}
