package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import com.qihoo360.daily.activity.ImagesActivity;
import com.qihoo360.daily.activity.SearchActivity;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.Info;

final class z implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Info f1054a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Activity f1055b;

    z(Info info, Activity activity) {
        this.f1054a = info;
        this.f1055b = activity;
    }

    public void onClick(View view) {
        this.f1054a.setRead(1);
        this.f1054a.setView(this.f1054a.getView() + 1);
        a.a(this.f1055b, this.f1054a);
        Intent intent = new Intent(this.f1055b, ImagesActivity.class);
        intent.putExtra("Info", this.f1054a);
        intent.putExtra(SearchActivity.TAG_URL, this.f1054a.getUrl());
        intent.putExtra("from", ChannelType.TYPE_CHANNEL_IMGS);
        this.f1055b.startActivity(intent);
        a.a(this.f1055b, ChannelType.TYPE_CHANNEL_IMGS);
    }
}
