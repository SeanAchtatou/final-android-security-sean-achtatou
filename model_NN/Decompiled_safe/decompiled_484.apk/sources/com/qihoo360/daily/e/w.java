package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.View;
import com.qihoo360.daily.activity.SearchActivity;
import com.qihoo360.daily.fragment.DailyFragment;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.HotSearchItem;

final class w implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Activity f1050a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f1051b;
    final /* synthetic */ Fragment c;

    w(Activity activity, String str, Fragment fragment) {
        this.f1050a = activity;
        this.f1051b = str;
        this.c = fragment;
    }

    public void onClick(View view) {
        HotSearchItem hotSearchItem = (HotSearchItem) view.getTag();
        Intent intent = new Intent(this.f1050a, SearchActivity.class);
        intent.putExtra(SearchActivity.TAG_URL, hotSearchItem.getQuery_url());
        intent.putExtra(SearchActivity.TAG_SEARCH, hotSearchItem.getQuery_key());
        intent.putExtra("from", this.f1051b);
        this.f1050a.startActivity(intent);
        if (this.c instanceof DailyFragment) {
            ((DailyFragment) this.c).setNewsClicked(true);
        }
        b.b(this.f1050a, "kandian_haoso_onclick ");
    }
}
