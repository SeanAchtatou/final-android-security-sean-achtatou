package com.qihoo360.daily.e;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.model.Info;

public class bg {
    public static RecyclerView.ViewHolder a(Context context) {
        View inflate = View.inflate(context, R.layout.row_seek_more, null);
        bi biVar = new bi(inflate);
        TextView unused = biVar.f992a = (TextView) inflate.findViewById(R.id.tv_text);
        return biVar;
    }

    public static void a(Fragment fragment, RecyclerView.ViewHolder viewHolder, Info info) {
        viewHolder.itemView.setOnClickListener(new bh(fragment, info));
        ((bi) viewHolder).f992a.setText(info.getAndroid_channeltargetname());
    }
}
