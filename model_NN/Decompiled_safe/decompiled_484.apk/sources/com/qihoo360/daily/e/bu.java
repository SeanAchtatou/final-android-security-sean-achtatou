package com.qihoo360.daily.e;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.qihoo360.daily.activity.DailyDetailActivity;
import com.qihoo360.daily.activity.ImagesActivity;
import com.qihoo360.daily.activity.SearchActivity;
import com.qihoo360.daily.activity.SearchDetailActivity;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.Info;

final class bu implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Info f1006a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Context f1007b;
    final /* synthetic */ bv c;

    bu(Info info, Context context, bv bvVar) {
        this.f1006a = info;
        this.f1007b = context;
        this.c = bvVar;
    }

    public void onClick(View view) {
        if (this.f1006a.getV_t() == 11) {
            Intent intent = new Intent(this.f1007b, SearchDetailActivity.class);
            intent.putExtra("Info", this.f1006a);
            intent.putExtra(SearchActivity.TAG_URL, this.f1006a.getUrl());
            intent.putExtra("from", ChannelType.TYPE_FAVOR);
            this.f1007b.startActivity(intent);
        } else if (this.f1006a.getV_t() == 20) {
            Intent intent2 = new Intent(this.f1007b, ImagesActivity.class);
            intent2.putExtra("Info", this.f1006a);
            intent2.putExtra(SearchActivity.TAG_URL, this.f1006a.getUrl());
            intent2.putExtra("from", ChannelType.TYPE_FAVOR);
            this.f1007b.startActivity(intent2);
        } else {
            Intent intent3 = new Intent(this.f1007b, DailyDetailActivity.class);
            intent3.putExtra("Info", this.f1006a);
            intent3.putExtra("from", "special_topic");
            this.f1007b.startActivity(intent3);
        }
        this.f1006a.setRead(1);
        a.a(this.f1006a, this.c.f1008a);
    }
}
