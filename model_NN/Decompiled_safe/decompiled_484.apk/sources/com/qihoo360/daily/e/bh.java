package com.qihoo360.daily.e;

import android.support.v4.app.Fragment;
import android.view.View;
import com.qihoo360.daily.fragment.DailyFragment;
import com.qihoo360.daily.fragment.NavigationFragment;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.widget.dragdropgrid.ChannelManage;

final class bh implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Fragment f990a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Info f991b;

    bh(Fragment fragment, Info info) {
        this.f990a = fragment;
        this.f991b = info;
    }

    public void onClick(View view) {
        int appendToSubscribedChannelsIfPossible;
        b.b(view.getContext(), "kandian_to_channel_onclick");
        if (this.f990a.getParentFragment() != null) {
            NavigationFragment navigationFragment = (NavigationFragment) this.f990a.getParentFragment();
            String android_channeltarget = this.f991b.getAndroid_channeltarget();
            if (navigationFragment.jumpToTargetChannelInActionbarListIfPossible(android_channeltarget) == -1 && (appendToSubscribedChannelsIfPossible = ChannelManage.appendToSubscribedChannelsIfPossible(android_channeltarget)) >= 0) {
                navigationFragment.updateChannelData(appendToSubscribedChannelsIfPossible);
            }
            if (this.f990a instanceof DailyFragment) {
                ((DailyFragment) this.f990a).setNewsClicked(true);
            }
        }
    }
}
