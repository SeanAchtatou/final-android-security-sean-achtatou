package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.af;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.widget.RatioImageView;

public class e extends a {
    public static float a(String str) {
        int[] a2 = af.a(str);
        try {
            return ((float) a2[0]) / ((float) a2[1]);
        } catch (Exception e) {
            e.printStackTrace();
            return 2.0f;
        }
    }

    public static RecyclerView.ViewHolder a(Context context) {
        View inflate = View.inflate(context, R.layout.row_funny_pic, null);
        inflate.setBackgroundResource(R.drawable.list_selector_background);
        g gVar = new g(inflate);
        RatioImageView unused = gVar.o = (RatioImageView) inflate.findViewById(R.id.iv_image);
        TextView unused2 = gVar.n = (TextView) inflate.findViewById(R.id.title);
        gVar.f = (TextView) inflate.findViewById(R.id.tv_praise);
        gVar.g = (TextView) inflate.findViewById(R.id.tv_bury);
        gVar.j = inflate.findViewById(R.id.share);
        gVar.d = (ImageView) inflate.findViewById(R.id.icon_praise);
        gVar.e = (ImageView) inflate.findViewById(R.id.icon_bury);
        gVar.f1029b = inflate.findViewById(R.id.layout_praise);
        gVar.f1028a = inflate.findViewById(R.id.layout_share);
        gVar.c = inflate.findViewById(R.id.layout_bury);
        gVar.h = (TextView) inflate.findViewById(R.id.buryOne);
        gVar.i = (TextView) inflate.findViewById(R.id.plusOne);
        return gVar;
    }

    public static void a(Activity activity, RecyclerView.ViewHolder viewHolder, int i, Info info, String str, int i2) {
        Context context = viewHolder.itemView.getContext();
        g gVar = (g) viewHolder;
        viewHolder.itemView.setOnClickListener(new f(gVar, info, i, str, activity, i2));
        String title = info.getTitle();
        if (!TextUtils.isEmpty(title)) {
            gVar.n.setText(title);
            a(info, gVar.n);
        }
        String b2 = b(info.getImgurl());
        float a2 = a(b2);
        if (a2 != 2.0f) {
            gVar.o.setRatio(a2);
        }
        int d = a.d(Application.getInstance()) - b.a(Application.getInstance(), 32.0f);
        int i3 = d / 2;
        if (a2 != 0.0f) {
            i3 = (int) (((float) d) / a2);
        }
        af.a(str, gVar.o, b2, d, i3, false, R.drawable.img_fun_pic_holder);
        gVar.n.setText(info.getTitle());
        a(context, gVar, info);
        View.OnClickListener onClickListener = null;
        if (!d.f(activity)) {
            onClickListener = a(activity, gVar, info, b2, true);
        }
        gVar.f1028a.setOnClickListener(onClickListener);
        gVar.f1029b.setOnClickListener(onClickListener);
        gVar.c.setOnClickListener(onClickListener);
        gVar.g.setOnClickListener(onClickListener);
        gVar.j.setOnClickListener(onClickListener);
    }

    public static String b(String str) {
        String[] split;
        return (TextUtils.isEmpty(str) || (split = str.split("\\|")) == null || split.length <= 0) ? str : split[0];
    }
}
