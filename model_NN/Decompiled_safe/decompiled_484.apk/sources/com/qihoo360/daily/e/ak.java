package com.qihoo360.daily.e;

import android.support.v4.app.Fragment;
import android.view.View;
import com.qihoo360.daily.fragment.CommonNewsFragment;
import com.qihoo360.daily.fragment.DailyFragment;
import com.qihoo360.daily.fragment.SpecialChannelFragment;

final class ak implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Fragment f971a;

    ak(Fragment fragment) {
        this.f971a = fragment;
    }

    public void onClick(View view) {
        if (this.f971a != null) {
            if (this.f971a instanceof CommonNewsFragment) {
                ((CommonNewsFragment) this.f971a).loadMore();
            } else if (this.f971a instanceof SpecialChannelFragment) {
                ((SpecialChannelFragment) this.f971a).loadMore();
            } else if (this.f971a instanceof DailyFragment) {
                ((DailyFragment) this.f971a).loadMore();
            }
        }
    }
}
