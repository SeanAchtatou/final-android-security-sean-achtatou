package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.SearchActivity;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.FavouriteInfo;
import com.qihoo360.daily.model.Info;

public class a {
    public static View.OnClickListener a(Activity activity, d dVar, Info info, String str, boolean z) {
        return new b(info, activity, dVar, str, z);
    }

    public static void a(Context context, d dVar, Info info) {
        dVar.g.setTextColor(context.getResources().getColor(R.color.praiseed_burry_normal_color));
        dVar.f.setTextColor(context.getResources().getColor(R.color.praiseed_burry_normal_color));
        if (c(info)) {
            a(dVar.f, info.getDigg(), "0");
            dVar.f.setTextColor(context.getResources().getColor(R.color.praiseed_color));
            dVar.d.setImageResource(R.drawable.ic_thumb_up_checked);
        } else {
            a(dVar.f, info.getDigg(), "0");
            dVar.d.setImageResource(R.drawable.ic_thumb_up);
        }
        if (d(info)) {
            a(dVar.g, info.getBury(), "0");
            dVar.g.setTextColor(context.getResources().getColor(R.color.praiseed_color));
            dVar.e.setImageResource(R.drawable.ic_thumb_down_checked);
            return;
        }
        a(dVar.g, info.getBury(), "0");
        dVar.e.setImageResource(R.drawable.ic_thumb_down);
    }

    public static void a(Context context, Info info) {
        if (context != null && info != null && !(info instanceof FavouriteInfo)) {
            new Thread(new c(context, info, new String[]{SearchActivity.TAG_URL, "channelId"}, new String[]{info.getUrl(), info.getChannelId()})).start();
        }
    }

    public static void a(Context context, String str) {
        b.b(context, str + "_channel_godetail");
    }

    private static void a(TextView textView, String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            textView.setText(str2);
        } else {
            textView.setText(str);
        }
    }

    public static void a(Info info, View view) {
        TextView textView = (TextView) view.findViewById(R.id.title);
        TextView textView2 = (TextView) view.findViewById(R.id.source);
        TextView textView3 = (TextView) view.findViewById(R.id.time);
        TextView textView4 = (TextView) view.findViewById(R.id.sign);
        TextView textView5 = (TextView) view.findViewById(R.id.tv_comment_count);
        if (!TextUtils.isEmpty(info.getTitle())) {
            textView.setText(info.getTitle());
        } else {
            textView.setText("");
        }
        a(info, textView);
        if (!TextUtils.isEmpty(info.getSrc())) {
            textView2.setText(info.getSrc());
        } else {
            textView2.setText("");
        }
        if (!TextUtils.isEmpty(info.getPdate())) {
            textView3.setText(b.c(info.getPdate()));
        } else {
            textView3.setText("");
        }
        if (!TextUtils.isEmpty(info.getCmt_cnt())) {
            textView5.setText(info.getCmt_cnt());
        } else {
            textView5.setText("0");
        }
        if (!TextUtils.isEmpty(info.getNewstag())) {
            textView4.setText(info.getNewstag());
            textView4.setVisibility(0);
            return;
        }
        textView4.setText("");
        textView4.setVisibility(8);
    }

    public static void a(Info info, TextView textView) {
        int color = textView.getContext().getResources().getColor(R.color.news_title_read);
        int color2 = textView.getContext().getResources().getColor(R.color.news_title_unread);
        if (info.getRead() != 1) {
            color = color2;
        }
        textView.setTextColor(color);
    }

    public static void a(Info info, TextView textView, TextView textView2) {
        a(info, textView);
        int color = textView.getContext().getResources().getColor(R.color.news_summary_read);
        int color2 = textView.getContext().getResources().getColor(R.color.news_summary_unread);
        if (info.getRead() != 1) {
            color = color2;
        }
        textView2.setTextColor(color);
    }

    /* access modifiers changed from: private */
    public static boolean b(Info info) {
        return (info == null || info.getDigg_type() == 0) ? false : true;
    }

    private static boolean c(Info info) {
        boolean z = true;
        if (info == null) {
            return false;
        }
        if (info.getDigg_type() != 1) {
            z = false;
        }
        return z;
    }

    private static boolean d(Info info) {
        return info != null && info.getDigg_type() == 2;
    }
}
