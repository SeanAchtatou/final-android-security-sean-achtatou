package com.qihoo360.daily.e;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.qihoo360.daily.activity.SearchActivity;
import com.qihoo360.daily.model.Info;

final class az implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f983a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Info f984b;

    az(Context context, Info info) {
        this.f983a = context;
        this.f984b = info;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.f983a, SearchActivity.class);
        intent.putExtra(SearchActivity.TAG_SEARCH, this.f984b.getKeywords());
        this.f983a.startActivity(intent);
    }
}
