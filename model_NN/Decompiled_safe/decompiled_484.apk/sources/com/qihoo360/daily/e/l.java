package com.qihoo360.daily.e;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;
import com.qihoo360.daily.fragment.DailyFragment;
import com.qihoo360.daily.model.Info;

final class l implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Info f1036a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ m f1037b;
    final /* synthetic */ int c;
    final /* synthetic */ String d;
    final /* synthetic */ int e;
    final /* synthetic */ Activity f;
    final /* synthetic */ Fragment g;

    l(Info info, m mVar, int i, String str, int i2, Activity activity, Fragment fragment) {
        this.f1036a = info;
        this.f1037b = mVar;
        this.c = i;
        this.d = str;
        this.e = i2;
        this.f = activity;
        this.g = fragment;
    }

    public void onClick(View view) {
        k.b(this.f1036a, this.f1037b, this.c, this.d, -1, this.e, this.f);
        if (this.g instanceof DailyFragment) {
            ((DailyFragment) this.g).setNewsClicked(true);
        }
    }
}
