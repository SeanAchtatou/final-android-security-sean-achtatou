package com.qihoo360.daily.e;

import android.content.Context;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.model.City;
import com.qihoo360.daily.model.Tianqi;

class ax extends c<Void, Tianqi> {

    /* renamed from: a  reason: collision with root package name */
    private aw f981a;

    /* renamed from: b  reason: collision with root package name */
    private City f982b;
    private Context c;

    public ax(aw awVar, City city, Context context) {
        this.f981a = awVar;
        this.f982b = city;
        this.c = context;
    }

    /* renamed from: a */
    public void onNetRequest(int i, Tianqi tianqi) {
        if (tianqi != null && this.f982b != null) {
            at.b(tianqi, this.c, this.f981a);
        }
    }
}
