package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.DailyDetailActivity;
import com.qihoo360.daily.activity.GifDetailActivity;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.Info;

final class f implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f1030a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Info f1031b;
    final /* synthetic */ int c;
    final /* synthetic */ String d;
    final /* synthetic */ Activity e;
    final /* synthetic */ int f;

    f(g gVar, Info info, int i, String str, Activity activity, int i2) {
        this.f1030a = gVar;
        this.f1031b = info;
        this.c = i;
        this.d = str;
        this.e = activity;
        this.f = i2;
    }

    public void onClick(View view) {
        Context context = view.getContext();
        if (context != null) {
            this.f1030a.n.setTextColor(context.getResources().getColor(R.color.news_title_read));
            this.f1031b.setRead(1);
            if (!ChannelType.TYPE_CHANNEL_FUN_PIC.equals(this.f1031b.getChannelId())) {
                a.a(context, this.f1031b);
            }
            Intent intent = new Intent(context, this.f1031b.isDailyInfo() ? DailyDetailActivity.class : GifDetailActivity.class);
            intent.putExtra("Info", this.f1031b);
            intent.putExtra("position", this.c);
            intent.putExtra("from", this.d);
            this.e.startActivityForResult(intent, this.f);
            a.a(this.e, this.d);
        }
    }
}
