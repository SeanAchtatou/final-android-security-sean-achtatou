package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.FavouriteInfo;
import com.qihoo360.daily.model.Info;

public class ci extends a {
    public static RecyclerView.ViewHolder a(Context context) {
        LinearLayout linearLayout = (LinearLayout) View.inflate(context, R.layout.row_text, null);
        linearLayout.setBackgroundResource(R.drawable.list_selector_background);
        ck ckVar = new ck(linearLayout);
        TextView unused = ckVar.c = (TextView) linearLayout.findViewById(R.id.title);
        View unused2 = ckVar.f1026a = linearLayout.findViewById(R.id.card_divider);
        View unused3 = ckVar.f1027b = linearLayout.findViewById(R.id.card_divider_line);
        TextView unused4 = ckVar.d = (TextView) linearLayout.findViewById(R.id.source);
        TextView unused5 = ckVar.e = (TextView) linearLayout.findViewById(R.id.time);
        TextView unused6 = ckVar.f = (TextView) linearLayout.findViewById(R.id.commentCount);
        ImageView unused7 = ckVar.g = (ImageView) linearLayout.findViewById(R.id.icon_time_divider);
        return ckVar;
    }

    public static void a(Activity activity, RecyclerView.ViewHolder viewHolder, int i, Info info, String str, int i2, int i3) {
        ck ckVar = (ck) viewHolder;
        viewHolder.itemView.setOnClickListener(new cj(info, ckVar, activity, i, str, i2));
        String title = info.getTitle();
        if (!TextUtils.isEmpty(title)) {
            ckVar.c.setText(title);
        }
        String summary = info.getSummary();
        if (!TextUtils.isEmpty(summary) && summary.length() > 80) {
            summary.substring(0, 79) + "...";
        }
        if (i3 == 1) {
            String favoTime = ((FavouriteInfo) info).getFavoTime();
            if (TextUtils.isEmpty(favoTime)) {
                ckVar.d.setVisibility(4);
                ckVar.e.setVisibility(4);
            } else {
                ckVar.d.setVisibility(0);
                ckVar.e.setVisibility(4);
                ckVar.d.setText(activity.getString(R.string.favorite_time) + favoTime);
            }
        } else {
            String src = info.getSrc();
            if (!TextUtils.isEmpty(src)) {
                ckVar.d.setText(src);
            }
            if (!"daily".equals(str)) {
                ckVar.e.setText(b.c(info.getPdate()));
            }
        }
        String cmt_cnt = info.getCmt_cnt();
        if (!TextUtils.isEmpty(cmt_cnt)) {
            ckVar.f.setText(cmt_cnt);
        }
        a(info, ckVar.c);
        ckVar.f1026a.setVisibility(info.recommendCardHeadFlag ? 0 : 8);
        ckVar.f1027b.setVisibility(info.recommendCardHeadFlag ? 0 : 8);
    }
}
