package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import com.qihoo360.daily.activity.CitylistActivity;

final class au implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Activity f978a;

    au(Activity activity) {
        this.f978a = activity;
    }

    public void onClick(View view) {
        this.f978a.startActivityForResult(new Intent(this.f978a, CitylistActivity.class), 7);
    }
}
