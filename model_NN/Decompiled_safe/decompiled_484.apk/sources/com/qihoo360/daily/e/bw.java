package com.qihoo360.daily.e;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.af;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.Info;

public class bw extends a {

    /* renamed from: a  reason: collision with root package name */
    private static int f1010a = ((a.d(Application.getInstance()) - ((int) a.a(Application.getInstance(), 54.0f))) / 3);

    /* renamed from: b  reason: collision with root package name */
    private static int f1011b = ((int) (((double) f1010a) / 1.333d));

    public static RecyclerView.ViewHolder a(Context context) {
        View inflate = View.inflate(context, R.layout.row_subnews_three, null);
        by byVar = new by(inflate);
        ImageView unused = byVar.r = (ImageView) inflate.findViewById(R.id.iv_image1);
        ImageView unused2 = byVar.s = (ImageView) inflate.findViewById(R.id.iv_image2);
        ImageView unused3 = byVar.t = (ImageView) inflate.findViewById(R.id.iv_image3);
        TextView unused4 = byVar.n = (TextView) inflate.findViewById(R.id.title);
        TextView unused5 = byVar.o = (TextView) inflate.findViewById(R.id.source);
        TextView unused6 = byVar.p = (TextView) inflate.findViewById(R.id.time);
        TextView unused7 = byVar.q = (TextView) inflate.findViewById(R.id.tv_comment_count);
        View unused8 = byVar.u = inflate.findViewById(R.id.sign);
        byVar.u.setVisibility(8);
        return byVar;
    }

    public static void a(Context context, RecyclerView.ViewHolder viewHolder, Info info) {
        by byVar = (by) viewHolder;
        viewHolder.itemView.setOnClickListener(new bx(context, info));
        byVar.n.setText(info.getTitle());
        byVar.o.setText(info.getSrc());
        byVar.p.setText(b.c(info.getPdate()));
        String cmt_cnt = info.getCmt_cnt();
        if (!TextUtils.isEmpty(cmt_cnt)) {
            byVar.q.setText(cmt_cnt);
        }
        String imgurl = info.getImgurl();
        String[] split = !TextUtils.isEmpty(imgurl) ? imgurl.split("\\|") : null;
        if (split != null) {
            if (split.length > 0) {
                af.a(null, byVar.r, split[0], f1010a, f1011b, false, R.drawable.img_fun_pic_holder_small);
                byVar.r.setVisibility(0);
            }
            if (split.length > 1) {
                af.a(null, byVar.s, split[1], f1010a, f1011b, false, R.drawable.img_fun_pic_holder_small);
                byVar.s.setVisibility(0);
            }
            if (split.length > 2) {
                af.a(null, byVar.t, split[2], f1010a, f1011b, false, R.drawable.img_fun_pic_holder_small);
                byVar.t.setVisibility(0);
            }
        }
    }
}
