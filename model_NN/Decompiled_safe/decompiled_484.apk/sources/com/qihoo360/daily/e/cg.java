package com.qihoo360.daily.e;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;
import com.qihoo360.daily.fragment.DailyFragment;
import com.qihoo360.daily.model.Info;

final class cg implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Info f1022a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ch f1023b;
    final /* synthetic */ int c;
    final /* synthetic */ String d;
    final /* synthetic */ int e;
    final /* synthetic */ Activity f;
    final /* synthetic */ Fragment g;

    cg(Info info, ch chVar, int i, String str, int i2, Activity activity, Fragment fragment) {
        this.f1022a = info;
        this.f1023b = chVar;
        this.c = i;
        this.d = str;
        this.e = i2;
        this.f = activity;
        this.g = fragment;
    }

    public void onClick(View view) {
        cf.b(this.f1022a, this.f1023b, this.c, this.d, -1, this.e, this.f);
        if (this.g instanceof DailyFragment) {
            ((DailyFragment) this.g).setNewsClicked(true);
        }
    }
}
