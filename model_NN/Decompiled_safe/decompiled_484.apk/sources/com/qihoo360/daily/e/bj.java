package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.activity.DailyDetailActivity;
import com.qihoo360.daily.activity.NewsDetailActivity;
import com.qihoo360.daily.activity.SpecialTopicActivity;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.af;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.Info;

public class bj extends a {

    /* renamed from: a  reason: collision with root package name */
    private static int f993a = a.d(Application.getInstance());

    /* renamed from: b  reason: collision with root package name */
    private static int f994b = (f993a / 2);
    private static int c = b.a(Application.getInstance(), 25.0f);

    public static RecyclerView.ViewHolder a(Context context) {
        View inflate = View.inflate(context, R.layout.row_shendu_daily, null);
        inflate.setBackgroundResource(R.drawable.list_selector_background);
        bl blVar = new bl(inflate);
        ImageView unused = blVar.u = (ImageView) inflate.findViewById(R.id.iv_image);
        ImageView unused2 = blVar.v = (ImageView) inflate.findViewById(R.id.head);
        TextView unused3 = blVar.w = (TextView) inflate.findViewById(R.id.name);
        TextView unused4 = blVar.o = (TextView) inflate.findViewById(R.id.card_divider);
        TextView unused5 = blVar.x = (TextView) inflate.findViewById(R.id.time);
        TextView unused6 = blVar.p = (TextView) inflate.findViewById(R.id.title);
        TextView unused7 = blVar.q = (TextView) inflate.findViewById(R.id.summary);
        TextView unused8 = blVar.n = (TextView) inflate.findViewById(R.id.topic);
        TextView unused9 = blVar.r = (TextView) inflate.findViewById(R.id.tv_hot_lable);
        ImageView unused10 = blVar.s = (ImageView) inflate.findViewById(R.id.icon_comment);
        TextView unused11 = blVar.t = (TextView) inflate.findViewById(R.id.tv_comment_count);
        return blVar;
    }

    public static void a(Activity activity, Fragment fragment, RecyclerView.ViewHolder viewHolder, int i, Info info, String str, int i2) {
        bl blVar = (bl) viewHolder;
        viewHolder.itemView.setOnClickListener(new bk(info, blVar, i, str, i2, activity, fragment));
        String title = info.getTitle();
        if (!TextUtils.isEmpty(title)) {
            blVar.p.setText(title);
        } else {
            blVar.p.setText("");
        }
        a.a(info, blVar.p);
        String summary = info.getSummary();
        TextView b2 = blVar.q;
        if (TextUtils.isEmpty(summary)) {
            summary = "";
        }
        b2.setText(summary);
        blVar.t.setText(info.getCmt_cnt() == null ? "0" : info.getCmt_cnt());
        blVar.s.setVisibility(info.getV_t() == 22 ? 8 : 0);
        blVar.t.setVisibility(info.getV_t() == 22 ? 8 : 0);
        String imgurl = info.getImgurl();
        if (!TextUtils.isEmpty(imgurl)) {
            String[] split = imgurl.split("\\|");
            if (split.length > 0) {
                af.a(str, blVar.u, split[0], f993a, f994b, false, R.drawable.img_fun_pic_holder);
            }
        }
        String conhotwords = info.getConhotwords();
        if (!TextUtils.isEmpty(conhotwords)) {
            blVar.n.setText(conhotwords);
        } else {
            blVar.n.setText("");
        }
        String hotlabel = info.getHotlabel();
        if (TextUtils.isEmpty(hotlabel)) {
            blVar.r.setVisibility(8);
        } else {
            blVar.r.setVisibility(0);
            blVar.r.setText(hotlabel);
        }
        if (TextUtils.isEmpty(info.getEditor_pic())) {
            blVar.v.setVisibility(8);
        } else {
            blVar.v.setVisibility(0);
            af.a(str, blVar.v, info.getEditor_pic(), c, c, true, R.drawable.img_fun_pic_holder);
        }
        blVar.w.setText(info.getEditor_name());
        blVar.w.setVisibility(TextUtils.isEmpty(info.getEditor_name()) ? 8 : 0);
        a(blVar, info, str);
        if (!TextUtils.isEmpty(info.getPdate())) {
            blVar.x.setText(b.c(info.getPdate()));
        } else {
            blVar.x.setText("");
        }
        if (i == 0) {
            ViewGroup.LayoutParams layoutParams = blVar.o.getLayoutParams();
            layoutParams.height = (int) a.a(activity, 16.0f);
            blVar.o.setLayoutParams(layoutParams);
            return;
        }
        ViewGroup.LayoutParams layoutParams2 = blVar.o.getLayoutParams();
        layoutParams2.height = (int) a.a(activity, 20.0f);
        blVar.o.setLayoutParams(layoutParams2);
    }

    private static void a(bl blVar, Info info, String str) {
        String conhotwords = info.getConhotwords();
        if (!TextUtils.isEmpty(conhotwords)) {
            blVar.n.setText(conhotwords);
        } else {
            blVar.n.setText("");
        }
        String hotlabel = info.getHotlabel();
        if (TextUtils.isEmpty(hotlabel)) {
            blVar.r.setVisibility(8);
        } else {
            blVar.r.setVisibility(0);
            blVar.r.setText(hotlabel);
        }
        if (TextUtils.isEmpty(info.getEditor_pic())) {
            blVar.v.setVisibility(8);
        } else {
            blVar.v.setVisibility(0);
            af.a(str, blVar.v, info.getEditor_pic(), c, c, true, R.drawable.img_fun_pic_holder);
        }
        blVar.w.setText(info.getEditor_name());
        blVar.w.setVisibility(TextUtils.isEmpty(info.getEditor_name()) ? 8 : 0);
    }

    /* access modifiers changed from: private */
    public static void b(Info info, bl blVar, int i, String str, int i2, int i3, Activity activity) {
        Intent intent;
        info.setView(info.getView() + 1);
        info.setRead(1);
        a.a(activity, info);
        a.a(info, blVar.p);
        if (info.getV_t() == 22) {
            Intent intent2 = new Intent(activity, SpecialTopicActivity.class);
            intent2.putExtra("Info", info);
            activity.startActivityForResult(intent2, i3);
            return;
        }
        if (info.isDailyInfo()) {
            intent = new Intent(activity, DailyDetailActivity.class);
            b.b(activity, "daily_onClick_kandian");
        } else {
            intent = new Intent(activity, NewsDetailActivity.class);
            b.b(activity, "daily_onClick_common");
        }
        intent.addFlags(67108864);
        intent.putExtra("Info", info);
        intent.putExtra("position", i);
        intent.putExtra("from", str);
        activity.startActivityForResult(intent, i3);
    }
}
