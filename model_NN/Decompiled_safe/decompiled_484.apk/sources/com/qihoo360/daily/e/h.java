package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.activity.SearchActivity;
import com.qihoo360.daily.activity.SearchDetailActivity;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.af;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.Info;

public class h extends a {

    /* renamed from: a  reason: collision with root package name */
    private static int f1032a = a.d(Application.getInstance());

    /* renamed from: b  reason: collision with root package name */
    private static int f1033b = (f1032a / 2);

    public static RecyclerView.ViewHolder a(Context context) {
        View inflate = View.inflate(context, R.layout.row_shendu_daily, null);
        inflate.setBackgroundResource(R.drawable.list_selector_background);
        j jVar = new j(inflate);
        ImageView unused = jVar.s = (ImageView) inflate.findViewById(R.id.iv_image);
        TextView unused2 = jVar.t = (TextView) inflate.findViewById(R.id.name);
        TextView unused3 = jVar.o = (TextView) inflate.findViewById(R.id.card_divider);
        TextView unused4 = jVar.u = (TextView) inflate.findViewById(R.id.time);
        TextView unused5 = jVar.p = (TextView) inflate.findViewById(R.id.title);
        TextView unused6 = jVar.q = (TextView) inflate.findViewById(R.id.summary);
        TextView unused7 = jVar.n = (TextView) inflate.findViewById(R.id.topic);
        TextView unused8 = jVar.r = (TextView) inflate.findViewById(R.id.tv_hot_lable);
        inflate.findViewById(R.id.tv_comment_count).setVisibility(8);
        inflate.findViewById(R.id.icon_comment).setVisibility(8);
        inflate.findViewById(R.id.head).setVisibility(8);
        return jVar;
    }

    public static void a(Activity activity, Fragment fragment, RecyclerView.ViewHolder viewHolder, int i, Info info, String str, int i2) {
        j jVar = (j) viewHolder;
        viewHolder.itemView.setOnClickListener(new i(info, jVar, i, str, i2, activity, fragment));
        String title = info.getTitle();
        if (!TextUtils.isEmpty(title)) {
            jVar.p.setText(title);
        } else {
            jVar.p.setText("");
        }
        a.a(info, jVar.p);
        String conhotwords = info.getConhotwords();
        if (!TextUtils.isEmpty(conhotwords)) {
            jVar.n.setText(conhotwords);
        } else {
            jVar.n.setText("");
        }
        String hotlabel = info.getHotlabel();
        if (TextUtils.isEmpty(hotlabel)) {
            jVar.r.setVisibility(8);
        } else {
            jVar.r.setVisibility(0);
            jVar.r.setText(hotlabel);
        }
        String summary = info.getSummary();
        TextView d = jVar.q;
        if (TextUtils.isEmpty(summary)) {
            summary = "";
        }
        d.setText(summary);
        String imgurl = info.getImgurl();
        if (!TextUtils.isEmpty(imgurl)) {
            String[] split = imgurl.split("\\|");
            if (split.length > 0) {
                af.a(str, jVar.s, split[0], f1032a, f1033b, false, R.drawable.img_fun_pic_holder);
            }
        }
        if (!TextUtils.isEmpty(info.getPdate())) {
            jVar.u.setText(b.c(info.getPdate()));
        } else {
            jVar.u.setText("");
        }
        jVar.t.setText(info.getSrc());
        jVar.t.setVisibility(TextUtils.isEmpty(info.getSrc()) ? 8 : 0);
        if (i == 0) {
            ViewGroup.LayoutParams layoutParams = jVar.o.getLayoutParams();
            layoutParams.height = (int) a.a(activity, 16.0f);
            jVar.o.setLayoutParams(layoutParams);
            return;
        }
        ViewGroup.LayoutParams layoutParams2 = jVar.o.getLayoutParams();
        layoutParams2.height = (int) a.a(activity, 20.0f);
        jVar.o.setLayoutParams(layoutParams2);
    }

    /* access modifiers changed from: private */
    public static void b(Info info, j jVar, int i, String str, int i2, int i3, Activity activity) {
        info.setRead(1);
        info.setView(info.getView() + 1);
        Intent intent = new Intent(activity, SearchDetailActivity.class);
        intent.putExtra("Info", info);
        intent.putExtra(SearchActivity.TAG_URL, info.getUrl());
        intent.putExtra("from", str);
        activity.startActivity(intent);
        a.a(info, jVar.p);
        b.b(activity, "daily_onClick_wailian");
    }
}
