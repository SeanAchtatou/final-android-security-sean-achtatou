package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.HotSearchItem;
import com.qihoo360.daily.model.Info;
import java.util.List;

public class v extends a {
    public static RecyclerView.ViewHolder a(Context context) {
        View inflate = View.inflate(context, R.layout.row_hot_search_daily, null);
        inflate.setBackgroundResource(R.drawable.list_selector_background);
        x xVar = new x(inflate);
        TextView unused = xVar.p = (TextView) inflate.findViewById(R.id.name);
        TextView unused2 = xVar.q = (TextView) inflate.findViewById(R.id.time);
        xVar.l = (TextView) inflate.findViewById(R.id.topic);
        TextView unused3 = xVar.o = (TextView) inflate.findViewById(R.id.card_divider);
        TextView[] unused4 = xVar.n = new TextView[6];
        xVar.n[0] = (TextView) inflate.findViewById(R.id.tv_hot_search1);
        xVar.n[1] = (TextView) inflate.findViewById(R.id.tv_hot_search2);
        xVar.n[2] = (TextView) inflate.findViewById(R.id.tv_hot_search3);
        xVar.n[3] = (TextView) inflate.findViewById(R.id.tv_hot_search4);
        xVar.n[4] = (TextView) inflate.findViewById(R.id.tv_hot_search5);
        xVar.n[5] = (TextView) inflate.findViewById(R.id.tv_hot_search6);
        return xVar;
    }

    public static void a(Activity activity, Fragment fragment, RecyclerView.ViewHolder viewHolder, int i, Info info, String str, int i2) {
        x xVar = (x) viewHolder;
        if (!TextUtils.isEmpty(info.getPdate())) {
            xVar.q.setText(b.c(info.getPdate()));
        } else {
            xVar.q.setText("");
        }
        xVar.p.setText(info.getSrc());
        xVar.p.setVisibility(TextUtils.isEmpty(info.getSrc()) ? 8 : 0);
        String conhotwords = info.getConhotwords();
        if (!TextUtils.isEmpty(conhotwords)) {
            xVar.l.setText(conhotwords);
        } else {
            xVar.l.setText("");
        }
        w wVar = new w(activity, str, fragment);
        List<HotSearchItem> hot_search = info.getHot_search();
        if (hot_search != null && hot_search.size() > 0) {
            for (int i3 = 0; i3 < hot_search.size(); i3++) {
                xVar.n[i3].setText(hot_search.get(i3).getHot_word());
                ((View) xVar.n[i3].getParent()).setTag(hot_search.get(i3));
                ((View) xVar.n[i3].getParent()).setOnClickListener(wVar);
                if (hot_search.get(i3).getHot_label() > 0) {
                    xVar.n[i3].setCompoundDrawablesWithIntrinsicBounds(0, 0, (int) R.drawable.ic_hot, 0);
                } else {
                    xVar.n[i3].setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
            }
        }
        if (i == 0) {
            ViewGroup.LayoutParams layoutParams = xVar.o.getLayoutParams();
            layoutParams.height = (int) a.a(activity, 16.0f);
            xVar.o.setLayoutParams(layoutParams);
            return;
        }
        ViewGroup.LayoutParams layoutParams2 = xVar.o.getLayoutParams();
        layoutParams2.height = (int) a.a(activity, 20.0f);
        xVar.o.setLayoutParams(layoutParams2);
    }
}
