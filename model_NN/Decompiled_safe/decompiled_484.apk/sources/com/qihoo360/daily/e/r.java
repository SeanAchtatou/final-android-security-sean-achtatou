package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.af;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.FavouriteInfo;
import com.qihoo360.daily.model.Info;

public class r extends a {

    /* renamed from: a  reason: collision with root package name */
    private static int f1042a = ((a.d(Application.getInstance()) - ((int) a.a(Application.getInstance(), 54.0f))) / 3);

    /* renamed from: b  reason: collision with root package name */
    private static int f1043b = ((int) (((double) f1042a) / 1.333d));

    public static RecyclerView.ViewHolder a(Context context) {
        return a(context, (int) R.layout.row_horizonal_images);
    }

    private static RecyclerView.ViewHolder a(Context context, int i) {
        View inflate = View.inflate(context, i, null);
        inflate.setBackgroundResource(R.drawable.list_selector_background);
        u uVar = new u(inflate);
        TextView unused = uVar.d = (TextView) inflate.findViewById(R.id.title);
        View unused2 = uVar.f1049b = inflate.findViewById(R.id.card_divider);
        View unused3 = uVar.c = inflate.findViewById(R.id.card_divider_line);
        TextView unused4 = uVar.h = (TextView) inflate.findViewById(R.id.source);
        TextView unused5 = uVar.i = (TextView) inflate.findViewById(R.id.time);
        TextView unused6 = uVar.j = (TextView) inflate.findViewById(R.id.tv_comment_count);
        uVar.f1048a = inflate.findViewById(R.id.ll_card_root);
        TextView unused7 = uVar.k = (TextView) inflate.findViewById(R.id.last_hint);
        ImageView unused8 = uVar.e = (ImageView) inflate.findViewById(R.id.iv_image1);
        ImageView unused9 = uVar.f = (ImageView) inflate.findViewById(R.id.iv_image2);
        ImageView unused10 = uVar.g = (ImageView) inflate.findViewById(R.id.iv_image3);
        return uVar;
    }

    public static void a(Activity activity, RecyclerView.ViewHolder viewHolder, int i, Info info, String str, int i2, int i3) {
        u uVar = (u) viewHolder;
        String title = info.getTitle();
        if (!TextUtils.isEmpty(title)) {
            uVar.d.setText(title);
            a(info, uVar.d);
        }
        if (i3 == 1) {
            String favoTime = ((FavouriteInfo) info).getFavoTime();
            if (TextUtils.isEmpty(favoTime)) {
                uVar.h.setVisibility(4);
                uVar.i.setVisibility(4);
            } else {
                uVar.h.setVisibility(0);
                uVar.i.setVisibility(4);
                uVar.h.setText(activity.getString(R.string.favorite_time) + favoTime);
            }
        } else {
            if (!TextUtils.isEmpty(info.getTitle())) {
                uVar.d.setText(info.getTitle());
            } else {
                uVar.d.setText("");
            }
            a.a(info, uVar.d);
            if (!TextUtils.isEmpty(info.getSrc())) {
                uVar.h.setText(info.getSrc());
            } else {
                uVar.h.setText("");
            }
            if (!TextUtils.isEmpty(info.getPdate())) {
                uVar.i.setText(b.c(info.getPdate()));
            } else {
                uVar.i.setText("");
            }
            if (!TextUtils.isEmpty(info.getCmt_cnt())) {
                uVar.j.setText(info.getCmt_cnt());
            } else {
                uVar.j.setText("0");
            }
        }
        String[] strArr = null;
        String imgurl = info.getImgurl();
        if (!TextUtils.isEmpty(imgurl)) {
            strArr = imgurl.split("\\|");
        }
        s sVar = new s(activity, str, info, i, i2, uVar, strArr);
        if (strArr != null) {
            if (strArr.length > 0) {
                uVar.e.setScaleType(ImageView.ScaleType.CENTER);
                uVar.e.setImageResource(R.drawable.img_fun_pic_holder_small);
                af.a(str, uVar.e, strArr[0], f1042a, f1043b, false, R.drawable.img_fun_pic_holder_small);
                uVar.e.setVisibility(0);
                uVar.e.setOnClickListener(sVar);
                uVar.e.setTag(R.id.tag_image_position, 0);
            }
            if (strArr.length > 1) {
                af.a(str, uVar.f, strArr[1], f1042a, f1043b, false, R.drawable.img_fun_pic_holder_small);
                uVar.f.setVisibility(0);
                uVar.f.setOnClickListener(sVar);
                uVar.f.setTag(R.id.tag_image_position, 1);
            }
            if (strArr.length > 2) {
                uVar.g.setScaleType(ImageView.ScaleType.CENTER);
                uVar.g.setImageResource(R.drawable.img_fun_pic_holder_small);
                af.a(str, uVar.g, strArr[2], f1042a, f1043b, false, R.drawable.img_fun_pic_holder_small);
                uVar.g.setVisibility(0);
                uVar.g.setOnClickListener(sVar);
                uVar.g.setTag(R.id.tag_image_position, 2);
            }
        }
        viewHolder.itemView.setOnClickListener(new t(activity, uVar, info, strArr, i, str, i2));
        uVar.d.setTextColor(activity.getResources().getColor(info.getRead() == 1 ? R.color.news_title_read : R.color.news_title_unread));
        uVar.f1049b.setVisibility(info.recommendCardHeadFlag ? 0 : 8);
        uVar.c.setVisibility(info.recommendCardHeadFlag ? 0 : 8);
    }

    public static RecyclerView.ViewHolder b(Context context) {
        return a(context, (int) R.layout.row_horizonal_images_favor);
    }
}
