package com.qihoo360.daily.e;

import android.view.View;
import com.qihoo360.daily.activity.FavouriteActivity;

final class ap implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FavouriteActivity f975a;

    ap(FavouriteActivity favouriteActivity) {
        this.f975a = favouriteActivity;
    }

    public void onClick(View view) {
        if (this.f975a != null) {
            this.f975a.backToTop();
        }
    }
}
