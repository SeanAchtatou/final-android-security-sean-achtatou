package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.DailyDetailActivity;
import com.qihoo360.daily.activity.ImagesActivity;
import com.qihoo360.daily.model.Info;

final class t implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Activity f1046a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ u f1047b;
    final /* synthetic */ Info c;
    final /* synthetic */ String[] d;
    final /* synthetic */ int e;
    final /* synthetic */ String f;
    final /* synthetic */ int g;

    t(Activity activity, u uVar, Info info, String[] strArr, int i, String str, int i2) {
        this.f1046a = activity;
        this.f1047b = uVar;
        this.c = info;
        this.d = strArr;
        this.e = i;
        this.f = str;
        this.g = i2;
    }

    public void onClick(View view) {
        this.f1047b.d.setTextColor(this.f1046a.getResources().getColor(R.color.news_title_read));
        this.c.setRead(1);
        a.a(this.f1046a, this.c);
        Intent intent = new Intent(view.getContext(), this.c.isDailyInfo() ? DailyDetailActivity.class : ImagesActivity.class);
        intent.putExtra("pics", this.d);
        intent.putExtra("Info", this.c);
        intent.putExtra("position", this.e);
        intent.putExtra("from", this.f);
        this.f1046a.startActivityForResult(intent, this.g);
        a.a(this.f1046a, this.f);
    }
}
