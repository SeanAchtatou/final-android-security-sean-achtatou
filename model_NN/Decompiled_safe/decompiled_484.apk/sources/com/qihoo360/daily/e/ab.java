package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.model.Info;
import java.util.ArrayList;
import java.util.List;

public class ab extends a {
    public static RecyclerView.ViewHolder a(Context context) {
        FrameLayout frameLayout = (FrameLayout) View.inflate(context, R.layout.row_joke, null);
        frameLayout.setBackgroundResource(R.drawable.list_selector_background);
        ad adVar = new ad(frameLayout);
        TextView unused = adVar.n = (TextView) frameLayout.findViewById(R.id.title);
        TextView unused2 = adVar.o = (TextView) frameLayout.findViewById(R.id.summary);
        adVar.f = (TextView) frameLayout.findViewById(R.id.tv_praise);
        adVar.g = (TextView) frameLayout.findViewById(R.id.tv_bury);
        adVar.j = frameLayout.findViewById(R.id.share);
        adVar.d = (ImageView) frameLayout.findViewById(R.id.icon_praise);
        adVar.e = (ImageView) frameLayout.findViewById(R.id.icon_bury);
        adVar.f1029b = frameLayout.findViewById(R.id.layout_praise);
        adVar.f1028a = frameLayout.findViewById(R.id.layout_share);
        adVar.c = frameLayout.findViewById(R.id.layout_bury);
        adVar.h = (TextView) frameLayout.findViewById(R.id.buryOne);
        adVar.i = (TextView) frameLayout.findViewById(R.id.plusOne);
        return adVar;
    }

    public static void a(Activity activity, RecyclerView.ViewHolder viewHolder, int i, Info info, String str, int i2) {
        ArrayList arrayList = new ArrayList();
        if (info != null) {
            arrayList.add(info);
        }
        a(activity, viewHolder, 0, arrayList, str, i2);
    }

    public static void a(Activity activity, RecyclerView.ViewHolder viewHolder, int i, List<Info> list, String str, int i2) {
        if (list != null && list.size() > 0 && list.size() > i) {
            Info info = list.get(i);
            ad adVar = (ad) viewHolder;
            viewHolder.itemView.setOnClickListener(new ac(info, adVar, list, i, str, activity, i2));
            String title = info.getTitle();
            if (!TextUtils.isEmpty(title)) {
                adVar.n.setText(title);
            }
            String summary = info.getSummary();
            if (!TextUtils.isEmpty(summary)) {
                adVar.o.setText(summary);
            }
            adVar.n.setText(info.getTitle());
            a(info, adVar.n, adVar.o);
            View.OnClickListener a2 = !d.f(activity) ? a(activity, adVar, info, null, false) : null;
            adVar.f1028a.setOnClickListener(a2);
            adVar.f1029b.setOnClickListener(a2);
            adVar.c.setOnClickListener(a2);
            adVar.g.setOnClickListener(a2);
            adVar.j.setOnClickListener(a2);
            a(activity, adVar, info);
        }
    }
}
