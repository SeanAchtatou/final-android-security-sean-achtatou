package com.qihoo360.daily.e;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.FavouriteActivity;

public class aj {
    public static RecyclerView.ViewHolder a(Context context) {
        return new am(View.inflate(context, R.layout.row_loadmore_failed, null));
    }

    public static void a(RecyclerView.ViewHolder viewHolder, Fragment fragment) {
        ((am) viewHolder).itemView.setOnClickListener(new ak(fragment));
    }

    public static void a(RecyclerView.ViewHolder viewHolder, FavouriteActivity favouriteActivity) {
        ((am) viewHolder).itemView.setOnClickListener(new al(favouriteActivity));
    }

    public static void a(RecyclerView.ViewHolder viewHolder, FavouriteActivity favouriteActivity, boolean z) {
        if (!z) {
            a(viewHolder, favouriteActivity);
            if (viewHolder.itemView.getVisibility() != 0) {
                viewHolder.itemView.setVisibility(0);
            }
        } else if (viewHolder.itemView.getVisibility() != 8) {
            viewHolder.itemView.setVisibility(8);
        }
    }
}
