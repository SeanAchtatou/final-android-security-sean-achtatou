package com.qihoo360.daily.e;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.qihoo360.daily.R;

public class ah {
    public static RecyclerView.ViewHolder a(Context context) {
        return new ai(View.inflate(context, R.layout.row_loadmore, null));
    }

    public static void a(RecyclerView.ViewHolder viewHolder) {
    }

    public static void a(RecyclerView.ViewHolder viewHolder, boolean z) {
        ai aiVar = (ai) viewHolder;
        if (!z) {
            if (aiVar.itemView.getVisibility() != 0) {
                aiVar.itemView.setVisibility(0);
            }
        } else if (aiVar.itemView.getVisibility() != 8) {
            aiVar.itemView.setVisibility(8);
        }
    }
}
