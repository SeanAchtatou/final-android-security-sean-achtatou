package com.qihoo360.daily.e;

import android.app.Activity;
import android.view.View;
import android.view.animation.AnimationUtils;
import com.qihoo360.daily.R;
import com.qihoo360.daily.g.g;
import com.qihoo360.daily.h.ai;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.model.Info;

final class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Info f985a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Activity f986b;
    final /* synthetic */ d c;
    final /* synthetic */ String d;
    final /* synthetic */ boolean e;

    b(Info info, Activity activity, d dVar, String str, boolean z) {
        this.f985a = info;
        this.f986b = activity;
        this.c = dVar;
        this.d = str;
        this.e = z;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_praise:
                if (!a.b(this.f985a)) {
                    this.f985a.setDigg_type(1);
                    String digg = this.f985a.getDigg();
                    if (digg == null || "".equals(digg)) {
                        digg = "0";
                    }
                    try {
                        this.f985a.setDigg((Integer.parseInt(digg) + 1) + "");
                    } catch (NumberFormatException e2) {
                        e2.printStackTrace();
                    }
                    new g(this.f986b, 1, this.f985a.getUrl()).a(null, new String[0]);
                    this.c.f.setText(this.f985a.getDigg());
                    this.c.d.setImageResource(R.drawable.ic_thumb_up_checked);
                    this.c.d.startAnimation(AnimationUtils.loadAnimation(view.getContext(), R.anim.praise_anim_daily));
                    this.c.f.setTextColor(this.f986b.getResources().getColor(R.color.praiseed_color));
                    return;
                }
                ay.a(this.f986b).a((int) R.string.comment_digged_or_burry);
                return;
            case R.id.icon_praise:
            case R.id.tv_praise:
            case R.id.icon_bury:
            case R.id.tv_bury:
            default:
                return;
            case R.id.layout_bury:
                if (!a.b(this.f985a)) {
                    this.f985a.setDigg_type(2);
                    String bury = this.f985a.getBury();
                    if (bury == null || "".equals(bury)) {
                        bury = "0";
                    }
                    try {
                        this.f985a.setBury((Integer.parseInt(bury) + 1) + "");
                    } catch (NumberFormatException e3) {
                        e3.printStackTrace();
                    }
                    new g(this.f986b, 2, this.f985a.getUrl()).a(null, new String[0]);
                    this.c.g.setText(this.f985a.getBury());
                    this.c.e.setImageResource(R.drawable.ic_thumb_down_checked);
                    this.c.g.setTextColor(view.getContext().getResources().getColor(R.color.praiseed_color));
                    this.c.e.startAnimation(AnimationUtils.loadAnimation(view.getContext(), R.anim.praise_anim));
                    return;
                }
                ay.a(this.f986b).a((int) R.string.comment_digged_or_burry);
                return;
            case R.id.layout_share:
            case R.id.share:
                new ai(this.f986b, this.f985a, this.f985a.getUrl(), this.d, this.e).a();
                return;
        }
    }
}
