package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.DailyDetailActivity;
import com.qihoo360.daily.activity.ImagesActivity;
import com.qihoo360.daily.activity.NewsDetailActivity;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.Info;

final class s implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Activity f1044a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f1045b;
    final /* synthetic */ Info c;
    final /* synthetic */ int d;
    final /* synthetic */ int e;
    final /* synthetic */ u f;
    final /* synthetic */ String[] g;

    s(Activity activity, String str, Info info, int i, int i2, u uVar, String[] strArr) {
        this.f1044a = activity;
        this.f1045b = str;
        this.c = info;
        this.d = i;
        this.e = i2;
        this.f = uVar;
        this.g = strArr;
    }

    public void onClick(View view) {
        a.a(this.f1044a, this.f1045b);
        if (this.f1045b.equals(ChannelType.TYPE_PUSH_LIST)) {
            Intent intent = new Intent(this.f1044a, this.c.isDailyInfo() ? DailyDetailActivity.class : NewsDetailActivity.class);
            intent.addFlags(67108864);
            intent.putExtra("Info", this.c);
            intent.putExtra("position", this.d);
            intent.putExtra("from", this.f1045b);
            this.f1044a.startActivityForResult(intent, this.e);
            return;
        }
        int intValue = ((Integer) view.getTag(R.id.tag_image_position)).intValue();
        this.c.setRead(1);
        a.a(this.f1044a, this.c);
        a.a(this.c, this.f.d);
        Intent intent2 = new Intent(this.f1044a, ImagesActivity.class);
        intent2.putExtra("Info", this.c);
        intent2.putExtra("p", intValue);
        intent2.putExtra("position", this.d);
        intent2.putExtra("pics", this.g);
        intent2.putExtra("from", this.f1045b);
        this.f1044a.startActivityForResult(intent2, this.e);
    }
}
