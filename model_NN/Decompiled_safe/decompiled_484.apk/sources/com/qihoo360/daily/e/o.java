package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.DailyDetailActivity;
import com.qihoo360.daily.activity.GifDetailActivity;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.Info;

final class o implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Activity f1038a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ q f1039b;
    final /* synthetic */ Info c;
    final /* synthetic */ int d;
    final /* synthetic */ String e;
    final /* synthetic */ int f;

    o(Activity activity, q qVar, Info info, int i, String str, int i2) {
        this.f1038a = activity;
        this.f1039b = qVar;
        this.c = info;
        this.d = i;
        this.e = str;
        this.f = i2;
    }

    public void onClick(View view) {
        this.f1039b.o.setTextColor(this.f1038a.getResources().getColor(R.color.news_title_read));
        this.c.setRead(1);
        if (!ChannelType.TYPE_CHANNEL_GIF.equals(this.c.getChannelId())) {
            a.a(this.f1038a, this.c);
        }
        Intent intent = new Intent(this.f1038a, this.c.isDailyInfo() ? DailyDetailActivity.class : GifDetailActivity.class);
        intent.putExtra("Info", this.c);
        intent.putExtra("position", this.d);
        intent.putExtra("from", this.e);
        this.f1038a.startActivityForResult(intent, this.f);
        a.a(this.f1038a, this.e);
    }
}
