package com.qihoo360.daily.e;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;
import com.qihoo360.daily.fragment.DailyFragment;
import com.qihoo360.daily.model.Info;

final class be implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Info f988a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ bf f989b;
    final /* synthetic */ int c;
    final /* synthetic */ String d;
    final /* synthetic */ int e;
    final /* synthetic */ Activity f;
    final /* synthetic */ Fragment g;

    be(Info info, bf bfVar, int i, String str, int i2, Activity activity, Fragment fragment) {
        this.f988a = info;
        this.f989b = bfVar;
        this.c = i;
        this.d = str;
        this.e = i2;
        this.f = activity;
        this.g = fragment;
    }

    public void onClick(View view) {
        bd.b(this.f988a, this.f989b, this.c, this.d, -1, this.e, this.f);
        if (this.g instanceof DailyFragment) {
            ((DailyFragment) this.g).setNewsClicked(true);
        }
    }
}
