package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.activity.DailyDetailActivity;
import com.qihoo360.daily.activity.NewsDetailActivity;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.af;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.Info;

public class cf extends a {

    /* renamed from: a  reason: collision with root package name */
    private static int f1020a = ((a.d(Application.getInstance()) - ((int) a.a(Application.getInstance(), 54.0f))) / 3);

    /* renamed from: b  reason: collision with root package name */
    private static int f1021b = ((int) (((double) f1020a) / 1.333d));

    public static RecyclerView.ViewHolder a(Context context) {
        View inflate = View.inflate(context, R.layout.row_subnews_three, null);
        ch chVar = new ch(inflate);
        chVar.n = inflate.findViewById(R.id.ll_card_root);
        ImageView unused = chVar.p = (ImageView) inflate.findViewById(R.id.iv_image1);
        ImageView unused2 = chVar.q = (ImageView) inflate.findViewById(R.id.iv_image2);
        ImageView unused3 = chVar.r = (ImageView) inflate.findViewById(R.id.iv_image3);
        TextView unused4 = chVar.o = (TextView) inflate.findViewById(R.id.title);
        return chVar;
    }

    public static void a(Activity activity, Fragment fragment, RecyclerView.ViewHolder viewHolder, int i, Info info, String str, int i2) {
        ch chVar = (ch) viewHolder;
        activity.getApplicationContext();
        viewHolder.itemView.setOnClickListener(new cg(info, chVar, i, str, i2, activity, fragment));
        String title = info.getTitle();
        if (!TextUtils.isEmpty(title)) {
            chVar.o.setText(title);
        } else {
            chVar.o.setText("");
        }
        String[] strArr = null;
        String imgurl = info.getImgurl();
        if (!TextUtils.isEmpty(imgurl)) {
            strArr = imgurl.split("\\|");
        }
        if (strArr != null) {
            if (strArr.length > 0) {
                af.a(str, chVar.p, strArr[0], f1020a, f1021b, false, R.drawable.img_fun_pic_holder_small);
                chVar.p.setVisibility(0);
            }
            if (strArr.length > 1) {
                af.a(str, chVar.q, strArr[1], f1020a, f1021b, false, R.drawable.img_fun_pic_holder_small);
                chVar.q.setVisibility(0);
            }
            if (strArr.length > 2) {
                af.a(str, chVar.r, strArr[2], f1020a, f1021b, false, R.drawable.img_fun_pic_holder_small);
                chVar.r.setVisibility(0);
            }
        }
        a(info, chVar.itemView);
        View view = chVar.n;
        int a2 = (int) a.a(activity, 16.0f);
        if (info.isCardTail()) {
            view.setPadding(a2, a2, a2, (int) a.a(activity, 25.0f));
        } else {
            view.setPadding(a2, a2, a2, a2);
        }
    }

    /* access modifiers changed from: private */
    public static void b(Info info, ch chVar, int i, String str, int i2, int i3, Activity activity) {
        Intent intent;
        info.setView(info.getView() + 1);
        info.setRead(1);
        a.a(info, chVar.o);
        a.a(activity, info);
        if (info.isDailyInfo()) {
            intent = new Intent(activity, DailyDetailActivity.class);
            b.b(activity, "daily_onClick_kandian");
        } else {
            intent = new Intent(activity, NewsDetailActivity.class);
            b.b(activity, "daily_onClick_common");
        }
        intent.addFlags(67108864);
        intent.putExtra("Info", info);
        intent.putExtra("position", i);
        intent.putExtra("from", str);
        activity.startActivityForResult(intent, i3);
    }
}
