package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.SearchActivity;
import com.qihoo360.daily.activity.SearchDetailActivity;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.af;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.Info;

public class k extends a {
    public static RecyclerView.ViewHolder a(Context context) {
        View inflate = View.inflate(context, R.layout.row_subnews_single_image, null);
        m mVar = new m(inflate);
        mVar.k = inflate.findViewById(R.id.ll_card_root);
        ImageView unused = mVar.n = (ImageView) inflate.findViewById(R.id.iv_image);
        mVar.m = (TextView) inflate.findViewById(R.id.title);
        inflate.findViewById(R.id.tv_comment_count).setVisibility(8);
        inflate.findViewById(R.id.icon_comment).setVisibility(8);
        return mVar;
    }

    public static void a(Activity activity, Fragment fragment, RecyclerView.ViewHolder viewHolder, int i, Info info, String str, int i2) {
        m mVar = (m) viewHolder;
        viewHolder.itemView.setOnClickListener(new l(info, mVar, i, str, i2, activity, fragment));
        String title = info.getTitle();
        if (!TextUtils.isEmpty(title)) {
            mVar.m.setText(title);
        } else {
            mVar.m.setText("");
        }
        String imgurl = info.getImgurl();
        if (!TextUtils.isEmpty(imgurl)) {
            String[] split = imgurl.split("\\|");
            if (split.length > 0) {
                String str2 = str;
                af.b(str2, mVar.n, split[0], R.dimen.single_img_width, R.dimen.single_img_height, false, R.drawable.img_fun_pic_holder_small);
            }
        } else {
            mVar.n.setScaleType(ImageView.ScaleType.CENTER);
            mVar.n.setImageResource(R.drawable.img_fun_pic_holder_small);
        }
        a(info, mVar.itemView);
        if (info.isCardTail()) {
            mVar.k.setPadding(activity.getResources().getDimensionPixelSize(R.dimen.card_margin_horizonal), activity.getResources().getDimensionPixelSize(R.dimen.card_margin_horizonal), activity.getResources().getDimensionPixelSize(R.dimen.card_margin_horizonal), (int) a.a(activity, 25.0f));
        } else {
            mVar.k.setPadding(activity.getResources().getDimensionPixelSize(R.dimen.card_margin_horizonal), activity.getResources().getDimensionPixelSize(R.dimen.card_margin_horizonal), activity.getResources().getDimensionPixelSize(R.dimen.card_margin_horizonal), activity.getResources().getDimensionPixelSize(R.dimen.card_margin_horizonal));
        }
    }

    /* access modifiers changed from: private */
    public static void b(Info info, m mVar, int i, String str, int i2, int i3, Activity activity) {
        info.setRead(1);
        info.setView(info.getView() + 1);
        Intent intent = new Intent(activity, SearchDetailActivity.class);
        intent.putExtra("Info", info);
        intent.putExtra(SearchActivity.TAG_URL, info.getUrl());
        intent.putExtra("from", str);
        activity.startActivity(intent);
        a.a(info, mVar.m);
        b.b(activity, "daily_onClick_wailian");
    }
}
