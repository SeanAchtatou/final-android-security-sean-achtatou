package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.ImagesActivity;
import com.qihoo360.daily.c.d;
import com.qihoo360.daily.c.e;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.af;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.Info;

public class bd extends a {
    public static RecyclerView.ViewHolder a(Context context) {
        View inflate = View.inflate(context, R.layout.row_photos_daily, null);
        inflate.setBackgroundResource(R.drawable.list_selector_background);
        bf bfVar = new bf(inflate);
        ImageView[] unused = bfVar.t = new ImageView[6];
        bfVar.t[0] = (ImageView) inflate.findViewById(R.id.iv_photo1);
        bfVar.t[1] = (ImageView) inflate.findViewById(R.id.iv_photo2);
        bfVar.t[2] = (ImageView) inflate.findViewById(R.id.iv_photo3);
        bfVar.t[3] = (ImageView) inflate.findViewById(R.id.iv_photo4);
        bfVar.t[4] = (ImageView) inflate.findViewById(R.id.iv_photo5);
        bfVar.t[5] = (ImageView) inflate.findViewById(R.id.iv_photo6);
        ImageView unused2 = bfVar.u = (ImageView) inflate.findViewById(R.id.head);
        TextView unused3 = bfVar.v = (TextView) inflate.findViewById(R.id.name);
        TextView unused4 = bfVar.o = (TextView) inflate.findViewById(R.id.card_divider);
        TextView unused5 = bfVar.w = (TextView) inflate.findViewById(R.id.time);
        TextView unused6 = bfVar.p = (TextView) inflate.findViewById(R.id.title);
        TextView unused7 = bfVar.q = (TextView) inflate.findViewById(R.id.summary);
        TextView unused8 = bfVar.n = (TextView) inflate.findViewById(R.id.topic);
        TextView unused9 = bfVar.r = (TextView) inflate.findViewById(R.id.tv_hot_lable);
        TextView unused10 = bfVar.s = (TextView) inflate.findViewById(R.id.tv_comment_count);
        return bfVar;
    }

    public static void a(Activity activity, Fragment fragment, RecyclerView.ViewHolder viewHolder, int i, Info info, String str, int i2) {
        bf bfVar = (bf) viewHolder;
        viewHolder.itemView.setOnClickListener(new be(info, bfVar, i, str, i2, activity, fragment));
        String title = info.getTitle();
        if (!TextUtils.isEmpty(title)) {
            bfVar.p.setText(title);
        } else {
            bfVar.p.setText("");
        }
        String conhotwords = info.getConhotwords();
        if (!TextUtils.isEmpty(conhotwords)) {
            bfVar.n.setText(conhotwords);
        } else {
            bfVar.n.setText("");
        }
        String hotlabel = info.getHotlabel();
        if (TextUtils.isEmpty(hotlabel)) {
            bfVar.r.setVisibility(8);
        } else {
            bfVar.r.setVisibility(0);
            bfVar.r.setText(hotlabel);
        }
        String summary = info.getSummary();
        TextView e = bfVar.q;
        if (TextUtils.isEmpty(summary)) {
            summary = "";
        }
        e.setText(summary);
        bfVar.s.setText(info.getCmt_cnt() == null ? "0" : info.getCmt_cnt());
        String imgurl = info.getImgurl();
        int d = (int) (((double) a.d(activity)) * 0.664d);
        int d2 = (int) (((double) a.d(activity)) * 0.328d);
        if (!TextUtils.isEmpty(imgurl)) {
            String[] split = imgurl.split("\\|");
            if (split.length > 0) {
                int i3 = 0;
                while (i3 < split.length) {
                    af.a(str, bfVar.t[i3], split[i3], i3 == 0 ? d : d2, i3 == 0 ? d : d2, false, R.drawable.img_fun_pic_holder_small);
                    i3++;
                }
            }
        }
        if (TextUtils.isEmpty(info.getEditor_pic())) {
            bfVar.u.setVisibility(8);
        } else {
            bfVar.u.setVisibility(0);
            e.a().a(activity, info.getEditor_pic(), new d(bfVar.u), 1, true);
        }
        if (!TextUtils.isEmpty(info.getPdate())) {
            bfVar.w.setText(b.c(info.getPdate()));
        } else {
            bfVar.w.setText("");
        }
        bfVar.v.setText(info.getSrc());
        bfVar.v.setVisibility(TextUtils.isEmpty(info.getSrc()) ? 8 : 0);
        if (i == 0) {
            ViewGroup.LayoutParams layoutParams = bfVar.o.getLayoutParams();
            layoutParams.height = (int) a.a(activity, 16.0f);
            bfVar.o.setLayoutParams(layoutParams);
            return;
        }
        ViewGroup.LayoutParams layoutParams2 = bfVar.o.getLayoutParams();
        layoutParams2.height = (int) a.a(activity, 20.0f);
        bfVar.o.setLayoutParams(layoutParams2);
    }

    /* access modifiers changed from: private */
    public static void b(Info info, bf bfVar, int i, String str, int i2, int i3, Activity activity) {
        info.setView(info.getView() + 1);
        info.setRead(1);
        a.a(activity, info);
        a.a(info, bfVar.p);
        Intent intent = new Intent(activity, ImagesActivity.class);
        intent.putExtra("Info", info);
        intent.putExtra("from", str);
        activity.startActivityForResult(intent, i3);
        b.b(activity, "kandian_atlas_onclick ");
    }
}
