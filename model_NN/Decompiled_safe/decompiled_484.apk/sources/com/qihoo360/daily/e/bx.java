package com.qihoo360.daily.e;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.qihoo360.daily.activity.DailyDetailActivity;
import com.qihoo360.daily.model.Info;

final class bx implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f1012a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Info f1013b;

    bx(Context context, Info info) {
        this.f1012a = context;
        this.f1013b = info;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.f1012a, DailyDetailActivity.class);
        intent.putExtra("Info", this.f1013b);
        intent.putExtra("from", "special_topic");
        this.f1012a.startActivity(intent);
    }
}
