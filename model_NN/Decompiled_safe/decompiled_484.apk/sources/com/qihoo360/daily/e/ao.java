package com.qihoo360.daily.e;

import android.view.View;
import com.qihoo360.daily.fragment.BaseListFragment;
import com.qihoo360.daily.h.b;

final class ao implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ aq f973a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ BaseListFragment f974b;

    ao(aq aqVar, BaseListFragment baseListFragment) {
        this.f973a = aqVar;
        this.f974b = baseListFragment;
    }

    public void onClick(View view) {
        b.b(this.f973a.f977b.getContext(), "list_bottom_to_top");
        this.f974b.backToTop(true);
    }
}
