package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.h.af;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.FavouriteInfo;
import com.qihoo360.daily.model.Info;

public class bm extends a {
    public static RecyclerView.ViewHolder a(Context context) {
        View inflate = View.inflate(context, R.layout.row_single_image, null);
        inflate.setBackgroundResource(R.drawable.list_selector_background);
        bo boVar = new bo(inflate);
        View unused = boVar.f999a = inflate.findViewById(R.id.card_divider);
        View unused2 = boVar.f1000b = inflate.findViewById(R.id.card_divider_line);
        ImageView unused3 = boVar.h = (ImageView) inflate.findViewById(R.id.iv_image);
        TextView unused4 = boVar.c = (TextView) inflate.findViewById(R.id.title);
        TextView unused5 = boVar.d = (TextView) inflate.findViewById(R.id.source);
        TextView unused6 = boVar.e = (TextView) inflate.findViewById(R.id.time);
        TextView unused7 = boVar.f = (TextView) inflate.findViewById(R.id.commentCount);
        View unused8 = boVar.g = inflate.findViewById(R.id.icon_comment);
        ImageView unused9 = boVar.i = (ImageView) inflate.findViewById(R.id.icon_time_divider);
        return boVar;
    }

    public static void a(Activity activity, RecyclerView.ViewHolder viewHolder, int i, Info info, String str, int i2, int i3) {
        String[] split;
        Context context = viewHolder.itemView.getContext();
        bo boVar = (bo) viewHolder;
        viewHolder.itemView.setOnClickListener(new bn(boVar, info, viewHolder, i, str, activity, i2));
        String title = info.getTitle();
        if (!TextUtils.isEmpty(title)) {
            boVar.c.setText(title);
            int color = context.getResources().getColor(R.color.news_title_read);
            int color2 = context.getResources().getColor(R.color.news_title_unread);
            TextView a2 = boVar.c;
            if (info.getRead() != 1) {
                color = color2;
            }
            a2.setTextColor(color);
        }
        if (i3 == 1) {
            String favoTime = ((FavouriteInfo) info).getFavoTime();
            if (TextUtils.isEmpty(favoTime)) {
                boVar.d.setVisibility(4);
                boVar.e.setVisibility(4);
            } else {
                boVar.d.setVisibility(0);
                boVar.e.setVisibility(4);
                boVar.d.setText(activity.getString(R.string.favorite_time) + favoTime);
            }
        } else {
            String src = info.getSrc();
            if (!TextUtils.isEmpty(src)) {
                boVar.d.setText(src);
            }
            if (!"daily".equals(str)) {
                boVar.e.setText(b.c(info.getPdate()));
            }
        }
        String cmt_cnt = info.getCmt_cnt();
        if (!TextUtils.isEmpty(cmt_cnt)) {
            boVar.f.setText(cmt_cnt);
        }
        switch (viewHolder.getItemViewType()) {
            case 11:
                boVar.g.setVisibility(8);
                boVar.f.setVisibility(8);
                break;
            default:
                boVar.g.setVisibility(0);
                boVar.f.setVisibility(0);
                break;
        }
        String imgurl = info.getImgurl();
        if (!TextUtils.isEmpty(imgurl) && (split = imgurl.split("\\|")) != null && split.length > 0) {
            imgurl = split[0];
        }
        af.b(str, boVar.h, imgurl, R.dimen.single_img_width, R.dimen.single_img_height, false, R.drawable.img_fun_pic_holder_small);
        boVar.f999a.setVisibility(info.recommendCardHeadFlag ? 0 : 8);
        boVar.f1000b.setVisibility(info.recommendCardHeadFlag ? 0 : 8);
    }
}
