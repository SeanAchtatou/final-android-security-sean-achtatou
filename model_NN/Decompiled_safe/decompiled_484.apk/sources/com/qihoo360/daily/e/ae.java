package com.qihoo360.daily.e;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.DailyDetailActivity;
import com.qihoo360.daily.activity.NewsDetailActivity;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.af;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.Info;

public class ae extends a {
    public static RecyclerView.ViewHolder a(Context context) {
        View inflate = View.inflate(context, R.layout.row_juhe_daily, null);
        inflate.setBackgroundResource(R.drawable.list_selector_background);
        ag agVar = new ag(inflate);
        agVar.k = inflate.findViewById(R.id.ll_card_root);
        ImageView unused = agVar.n = (ImageView) inflate.findViewById(R.id.iv_image);
        agVar.m = (TextView) inflate.findViewById(R.id.title);
        agVar.l = (TextView) inflate.findViewById(R.id.topic);
        TextView unused2 = agVar.o = (TextView) inflate.findViewById(R.id.card_divider);
        return agVar;
    }

    public static void a(Activity activity, Fragment fragment, RecyclerView.ViewHolder viewHolder, int i, Info info, String str, int i2) {
        ag agVar = (ag) viewHolder;
        viewHolder.itemView.setOnClickListener(new af(info, agVar, i, str, i2, activity, fragment));
        String title = info.getTitle();
        if (!TextUtils.isEmpty(title)) {
            agVar.m.setText(title);
        } else {
            agVar.m.setText("");
        }
        String conhotwords = info.getConhotwords();
        if (!TextUtils.isEmpty(conhotwords)) {
            agVar.l.setText(conhotwords + ((TextUtils.isEmpty(info.getConhotwordstime()) || "null".equals(info.getConhotwordstime())) ? "" : info.getConhotwordstime()));
        } else {
            agVar.l.setText("");
        }
        String imgurl = info.getImgurl();
        if (!TextUtils.isEmpty(imgurl)) {
            String[] split = imgurl.split("\\|");
            if (split.length > 0) {
                af.b(str, agVar.n, split[0], R.dimen.single_img_width, R.dimen.single_img_height, false, R.drawable.img_fun_pic_holder_small);
            }
        } else {
            agVar.n.setScaleType(ImageView.ScaleType.CENTER);
            agVar.n.setImageResource(R.drawable.img_fun_pic_holder_small);
        }
        a(info, agVar.itemView);
        if (i == 0) {
            ViewGroup.LayoutParams layoutParams = agVar.o.getLayoutParams();
            layoutParams.height = (int) a.a(activity, 16.0f);
            agVar.o.setLayoutParams(layoutParams);
            return;
        }
        ViewGroup.LayoutParams layoutParams2 = agVar.o.getLayoutParams();
        layoutParams2.height = (int) a.a(activity, 20.0f);
        agVar.o.setLayoutParams(layoutParams2);
    }

    /* access modifiers changed from: private */
    public static void b(Info info, ag agVar, int i, String str, int i2, int i3, Activity activity) {
        Intent intent;
        info.setView(info.getView() + 1);
        info.setRead(1);
        a.a(info, agVar.m);
        a.a(activity, info);
        if (info.isDailyInfo()) {
            intent = new Intent(activity, DailyDetailActivity.class);
            b.b(activity, "daily_onClick_kandian");
        } else {
            intent = new Intent(activity, NewsDetailActivity.class);
            b.b(activity, "daily_onClick_common");
        }
        intent.addFlags(67108864);
        intent.putExtra("Info", info);
        intent.putExtra("position", i);
        intent.putExtra("from", str);
        activity.startActivityForResult(intent, i3);
    }
}
