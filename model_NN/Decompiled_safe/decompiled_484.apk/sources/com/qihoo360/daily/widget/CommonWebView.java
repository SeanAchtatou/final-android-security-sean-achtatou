package com.qihoo360.daily.widget;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.webkit.JsPromptResult;
import android.webkit.WebView;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class CommonWebView extends WebView {
    private static final boolean DEBUG = false;
    private static final String KEY_ARG_ARRAY = "args";
    private static final String KEY_FUNCTION_NAME = "func";
    private static final String KEY_INTERFACE_NAME = "obj";
    private static final String MSG_PROMPT_HEADER = "MyApp:";
    private static final String VAR_ARG_PREFIX = "arg";
    private static final String[] mFilterMethods = {"getClass", "hashCode", "notify", "notifyAll", "equals", "toString", "wait"};
    private final HashMap<String, Object> mJsInterfaceMap = new HashMap<>();
    private String mJsStringCache = null;

    public CommonWebView(Context context) {
        super(context);
        init(context);
    }

    public CommonWebView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    public CommonWebView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init(context);
    }

    private void createJsMethod(String str, Object obj, StringBuilder sb) {
        if (!TextUtils.isEmpty(str) && obj != null && sb != null) {
            Class<?> cls = obj.getClass();
            sb.append("if(typeof(window.").append(str).append(")!='undefined'){");
            sb.append("}else {");
            sb.append("    window.").append(str).append("={");
            for (Method method : cls.getMethods()) {
                String name = method.getName();
                if (!filterMethods(name)) {
                    sb.append("        ").append(name).append(":function(");
                    int length = method.getParameterTypes().length;
                    if (length > 0) {
                        int i = length - 1;
                        for (int i2 = 0; i2 < i; i2++) {
                            sb.append(VAR_ARG_PREFIX).append(i2).append(",");
                        }
                        sb.append(VAR_ARG_PREFIX).append(length - 1);
                    }
                    sb.append(") {");
                    if (method.getReturnType() != Void.TYPE) {
                        sb.append("            return ").append("prompt('").append(MSG_PROMPT_HEADER).append("'+");
                    } else {
                        sb.append("            prompt('").append(MSG_PROMPT_HEADER).append("'+");
                    }
                    sb.append("JSON.stringify({");
                    sb.append(KEY_INTERFACE_NAME).append(":'").append(str).append("',");
                    sb.append(KEY_FUNCTION_NAME).append(":'").append(name).append("',");
                    sb.append(KEY_ARG_ARRAY).append(":[");
                    if (length > 0) {
                        int i3 = length - 1;
                        for (int i4 = 0; i4 < i3; i4++) {
                            sb.append(VAR_ARG_PREFIX).append(i4).append(",");
                        }
                        sb.append(VAR_ARG_PREFIX).append(i3);
                    }
                    sb.append("]})");
                    sb.append(");");
                    sb.append("        }, ");
                }
            }
            sb.append("    };");
            sb.append("}");
        }
    }

    private boolean filterMethods(String str) {
        for (String equals : mFilterMethods) {
            if (equals.equals(str)) {
                return true;
            }
        }
        return false;
    }

    private String genJavascriptInterfacesString() {
        if (this.mJsInterfaceMap.size() == 0) {
            this.mJsStringCache = null;
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("javascript:(function JsAddJavascriptInterface_(){");
        for (Map.Entry next : this.mJsInterfaceMap.entrySet()) {
            try {
                createJsMethod((String) next.getKey(), next.getValue(), sb);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        sb.append("})()");
        return sb.toString();
    }

    private Class<?> getClassFromJsonObject(Object obj) {
        Class<?> cls = obj.getClass();
        return cls == Integer.class ? Integer.TYPE : cls == Boolean.class ? Boolean.TYPE : String.class;
    }

    private void init(Context context) {
        removeSearchBoxImpl();
    }

    private boolean invokeJSInterfaceMethod(JsPromptResult jsPromptResult, String str, String str2, Object[] objArr) {
        boolean z = true;
        Object obj = this.mJsInterfaceMap.get(str);
        if (obj == null) {
            jsPromptResult.cancel();
            return false;
        }
        Class[] clsArr = null;
        int length = objArr != null ? objArr.length : 0;
        if (length > 0) {
            clsArr = new Class[length];
            for (int i = 0; i < length; i++) {
                clsArr[i] = getClassFromJsonObject(objArr[i]);
            }
        }
        try {
            Object invoke = obj.getClass().getMethod(str2, clsArr).invoke(obj, objArr);
            jsPromptResult.confirm(invoke == null || invoke.getClass() == Void.TYPE ? "" : invoke.toString());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            z = false;
        } catch (Exception e2) {
            e2.printStackTrace();
            z = false;
        }
        jsPromptResult.cancel();
        return z;
    }

    private void invokeMethod(String str, String str2) {
        try {
            Method declaredMethod = WebView.class.getDeclaredMethod(str, String.class);
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(this, str2);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e2) {
            e2.printStackTrace();
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
        } catch (InvocationTargetException e4) {
            e4.printStackTrace();
        }
    }

    private void loadJavascriptInterfaces() {
        try {
            if (!TextUtils.isEmpty(this.mJsStringCache)) {
                loadUrl(this.mJsStringCache);
            }
        } catch (Exception e) {
        }
    }

    private void removeSearchBoxImpl() {
        try {
            invokeMethod("removeJavascriptInterface", "searchBoxJavaBridge_");
        } catch (Exception e) {
        }
        try {
            invokeMethod("removeJavascriptInterface", "accessibility");
        } catch (Exception e2) {
        }
        try {
            invokeMethod("removeJavascriptInterface", "accessibilityTraversal");
        } catch (Exception e3) {
        }
    }

    public void addJavascriptInterface(Object obj, String str) {
        if (!TextUtils.isEmpty(str)) {
            this.mJsInterfaceMap.put(str, obj);
            injectJavascriptInterfaces();
        }
    }

    public boolean handleJsInterface(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        int length;
        if (!str2.startsWith(MSG_PROMPT_HEADER)) {
            return false;
        }
        try {
            JSONObject jSONObject = new JSONObject(str2.substring(MSG_PROMPT_HEADER.length()));
            String string = jSONObject.getString(KEY_INTERFACE_NAME);
            String string2 = jSONObject.getString(KEY_FUNCTION_NAME);
            JSONArray jSONArray = jSONObject.getJSONArray(KEY_ARG_ARRAY);
            Object[] objArr = null;
            if (jSONArray != null && (length = jSONArray.length()) > 0) {
                objArr = new Object[length];
                for (int i = 0; i < length; i++) {
                    objArr[i] = jSONArray.get(i);
                }
            }
            if (invokeJSInterfaceMethod(jsPromptResult, string, string2, objArr)) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        jsPromptResult.cancel();
        return false;
    }

    public boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= 11;
    }

    public boolean hasJellyBeanMR1() {
        return Build.VERSION.SDK_INT >= 17;
    }

    public void injectJavascriptInterfaces() {
        if (!TextUtils.isEmpty(this.mJsStringCache)) {
            loadJavascriptInterfaces();
            return;
        }
        this.mJsStringCache = genJavascriptInterfacesString();
        loadJavascriptInterfaces();
    }

    public void injectJavascriptInterfaces(WebView webView) {
        injectJavascriptInterfaces();
    }

    public void removeJsInterface(String str) {
        if (hasJellyBeanMR1()) {
            invokeMethod("removeJavascriptInterface", str);
            return;
        }
        this.mJsInterfaceMap.remove(str);
        this.mJsStringCache = null;
        injectJavascriptInterfaces();
    }
}
