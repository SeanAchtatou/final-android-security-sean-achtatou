package com.qihoo360.daily.widget;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.qihoo360.daily.R;

public class DiscoveryDialog extends DialogView implements View.OnClickListener {
    private OnDialogViewClickListener dialogViewClick;

    public interface OnDialogViewClickListener {
        void onLeftButtonClick();

        void onRightButtonClick();
    }

    public DiscoveryDialog(Activity activity) {
        this(activity, R.layout.layout_dialog);
    }

    public DiscoveryDialog(Activity activity, int i) {
        super(activity, i);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_dialog_left:
                if (this.dialogViewClick != null) {
                    this.dialogViewClick.onLeftButtonClick();
                    break;
                }
                break;
            case R.id.btn_dialog_right:
                if (this.dialogViewClick != null) {
                    this.dialogViewClick.onRightButtonClick();
                    break;
                }
                break;
        }
        disMissDialog();
    }

    /* access modifiers changed from: protected */
    public void onLoadLayout(View view) {
        view.findViewById(R.id.btn_dialog_left).setOnClickListener(this);
        view.findViewById(R.id.btn_dialog_right).setOnClickListener(this);
    }

    public void setContentText(int i) {
        ((TextView) this.view.findViewById(R.id.tv_dialog_content)).setText(i);
    }

    public void setContentText(String str) {
        ((TextView) this.view.findViewById(R.id.tv_dialog_content)).setText(str);
    }

    public void setLeftButtonText(int i) {
        ((Button) this.view.findViewById(R.id.btn_dialog_left)).setText(i);
    }

    public void setLeftButtonText(String str) {
        ((Button) this.view.findViewById(R.id.btn_dialog_left)).setText(str);
    }

    public void setLeftButtonVisibility(int i) {
        this.view.findViewById(R.id.btn_dialog_left).setVisibility(i);
    }

    public void setOnDialogViewClickListener(OnDialogViewClickListener onDialogViewClickListener) {
        this.dialogViewClick = onDialogViewClickListener;
    }

    public void setRightButtonText(int i) {
        ((Button) this.view.findViewById(R.id.btn_dialog_right)).setText(i);
    }

    public void setRightButtonText(String str) {
        ((Button) this.view.findViewById(R.id.btn_dialog_right)).setText(str);
    }
}
