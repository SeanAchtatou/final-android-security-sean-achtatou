package com.qihoo360.daily.widget;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.Scroller;
import android.widget.TextView;
import com.c.c.a;
import com.qihoo360.daily.R;
import java.util.List;

public class MHorizontalScrollView extends HorizontalScrollView {
    private final int DURATION = 300;
    private final int FIRST_SELECT_DURATION = QMediaPlayer.MEDIA_INFO_VIDEO_TRACK_LAGGING;
    private final float MAX_ALPHA = 1.0f;
    private final float MAX_SCALE = 1.0f;
    private final float MIN_ALPHA = 0.6f;
    private final float MIN_SCALE = 0.6f;
    private final int PADDING = 15;
    private final int PADDING_UP_BOTTOM = 0;
    private final float TEXTSIZE = 18.0f;
    private int center;
    /* access modifiers changed from: private */
    public View child;
    /* access modifiers changed from: private */
    public int defaultIndex = 5;
    private int dx = -1;
    private int fromX = -1;
    private Interpolator interpolator;
    private boolean isFling = false;
    /* access modifiers changed from: private */
    public LinearLayout linearLayout;
    private OnScroll onScroll;
    private OnSelectedListener onSelectedListener;
    private Scroller scroller;
    /* access modifiers changed from: private */
    public int selectedIndex = 0;
    private List<String> titles;
    private int width;

    class MOnScroll implements OnScroll {
        private MOnScroll() {
        }

        public void scroll(int i, int i2) {
            for (int i3 = 0; i3 < MHorizontalScrollView.this.linearLayout.getChildCount(); i3++) {
                View unused = MHorizontalScrollView.this.child = MHorizontalScrollView.this.linearLayout.getChildAt(i3);
                float access$500 = MHorizontalScrollView.this.getPercent(i, MHorizontalScrollView.this.child);
                a.b(MHorizontalScrollView.this.child, MHorizontalScrollView.this.getMapScalePercent(access$500) * 1.0f);
                a.c(MHorizontalScrollView.this.child, MHorizontalScrollView.this.getMapScalePercent(access$500) * 1.0f);
                a.a(MHorizontalScrollView.this.child, MHorizontalScrollView.this.getMapAlphaPercent(access$500) * 1.0f);
            }
        }

        public void scrollEnd() {
            MHorizontalScrollView.this.compute();
            MHorizontalScrollView.this.select(MHorizontalScrollView.this.selectedIndex - 2);
        }
    }

    interface OnScroll {
        void scroll(int i, int i2);

        void scrollEnd();
    }

    public interface OnSelectedListener {
        void onSelected(View view, int i);
    }

    public MHorizontalScrollView(Context context) {
        super(context);
    }

    public MHorizontalScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MHorizontalScrollView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    private void addTitles() {
        this.linearLayout = (LinearLayout) getChildAt(0);
        if (this.linearLayout != null && this.titles != null) {
            this.linearLayout.removeAllViews();
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(this.width / 5, -2);
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(this.width / 5, -2);
            for (int i = 0; i < 2; i++) {
                TextView textView = new TextView(getContext());
                textView.setLayoutParams(layoutParams2);
                textView.setPadding(0, 0, 0, 0);
                this.linearLayout.addView(textView);
            }
            for (int i2 = 0; i2 < this.titles.size(); i2++) {
                TextView textView2 = new TextView(getContext());
                textView2.setLayoutParams(layoutParams);
                textView2.setText(this.titles.get(i2));
                textView2.setTextSize(18.0f);
                textView2.setTextColor(getResources().getColorStateList(R.drawable.title_text_color));
                textView2.setPadding(0, 0, 0, 0);
                textView2.setGravity(17);
                this.linearLayout.addView(textView2);
            }
            for (int i3 = 0; i3 < 2; i3++) {
                TextView textView3 = new TextView(getContext());
                textView3.setLayoutParams(layoutParams2);
                textView3.setPadding(0, 0, 0, 0);
                this.linearLayout.addView(textView3);
            }
            for (final int i4 = 2; i4 < this.linearLayout.getChildCount() - 2; i4++) {
                this.linearLayout.getChildAt(i4).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        MHorizontalScrollView.this.select(i4 - 2);
                    }
                });
            }
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(1000);
            alphaAnimation.setFillAfter(true);
            startAnimation(alphaAnimation);
            new Handler(new Handler.Callback() {
                public boolean handleMessage(Message message) {
                    MHorizontalScrollView.this.setSelectIndex(MHorizontalScrollView.this.defaultIndex, QMediaPlayer.MEDIA_INFO_VIDEO_TRACK_LAGGING);
                    return false;
                }
            }).sendEmptyMessageDelayed(0, 200);
        }
    }

    /* access modifiers changed from: private */
    public void compute() {
        int scrollX = getScrollX();
        this.fromX = scrollX;
        this.dx = this.width;
        int childCount = this.linearLayout.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = this.linearLayout.getChildAt(i);
            int width2 = (childAt.getWidth() / 2) + (childAt.getLeft() - scrollX);
            if (Math.abs(this.dx) > Math.abs(this.center - width2)) {
                this.dx = width2 - this.center;
                this.selectedIndex = i;
            }
        }
    }

    private Interpolator getInterpalator() {
        return new LinearInterpolator();
    }

    /* access modifiers changed from: private */
    public float getMapAlphaPercent(float f) {
        return (0.39999998f * f) + 0.6f;
    }

    /* access modifiers changed from: private */
    public float getMapScalePercent(float f) {
        return (0.39999998f * f) + 0.6f;
    }

    /* access modifiers changed from: private */
    public float getPercent(int i, View view) {
        int left = ((view.getLeft() - i) + (view.getRight() - i)) / 2;
        if (left <= 0 || left >= this.width) {
            return 0.0f;
        }
        return left <= this.center ? (((float) left) * 1.0f) / ((float) this.center) : (((float) ((this.center * 2) - left)) * 1.0f) / ((float) this.center);
    }

    private void initView() {
        this.width = getResources().getDisplayMetrics().widthPixels;
        this.center = this.width / 2;
        addTitles();
        setOnScroll(new MOnScroll());
    }

    private void scrollBack(int i, int i2, int i3) {
        if (this.interpolator == null) {
            this.interpolator = getInterpalator();
        }
        this.fromX = i2;
        if (this.scroller == null) {
            this.scroller = new Scroller(getContext(), this.interpolator);
        }
        this.scroller.startScroll(0, 0, i, 0, i3);
        invalidate();
    }

    private void setOnScroll(OnScroll onScroll2) {
        this.onScroll = onScroll2;
    }

    /* access modifiers changed from: private */
    public void setSelectIndex(int i, int i2) {
        if (i <= 1) {
            i = 2;
        }
        if (i >= this.linearLayout.getChildCount() - 2) {
            i = this.linearLayout.getChildCount() - 3;
        }
        int i3 = 0;
        while (i3 < this.linearLayout.getChildCount()) {
            ((TextView) this.linearLayout.getChildAt(i3)).setSelected(i == i3);
            i3++;
        }
        int scrollX = getScrollX();
        View childAt = this.linearLayout.getChildAt(i);
        if (this.onSelectedListener != null) {
            this.onSelectedListener.onSelected(childAt, i - 2);
        }
        this.dx = ((childAt.getWidth() / 2) + (childAt.getLeft() - scrollX)) - this.center;
        scrollBack(this.dx, scrollX, i2);
    }

    public void computeScroll() {
        if (this.scroller != null && this.scroller.computeScrollOffset()) {
            scrollTo(this.scroller.getCurrX() + this.fromX, 0);
            invalidate();
        }
        super.computeScroll();
    }

    public void fling(int i) {
        super.fling(i);
        this.isFling = true;
    }

    public void initTitles(List<String> list, int i) {
        this.titles = list;
        this.defaultIndex = i + 2;
        initView();
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
        if (this.onScroll != null) {
            this.onScroll.scroll(i, i2);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 1:
                this.isFling = true;
                break;
        }
        return super.onTouchEvent(motionEvent);
    }

    public void select(int i) {
        setSelectIndex(i + 2, 300);
    }

    public void setOnSelectedListener(OnSelectedListener onSelectedListener2) {
        this.onSelectedListener = onSelectedListener2;
    }
}
