package com.qihoo360.daily.widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.qihoo.messenger.util.QDefine;
import com.qihoo.qplayer.QihooMediaPlayer;
import com.qihoo.qplayer.view.QihooVideoView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.SearchActivity;
import com.qihoo360.daily.activity.VideoFullActivity;
import com.qihoo360.daily.c.e;
import com.qihoo360.daily.c.l;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.h.bk;
import com.qihoo360.daily.h.i;
import com.qihoo360.daily.widget.TrafficDialog;
import java.lang.ref.WeakReference;

public class QHVideoController extends FrameLayout {
    private static final int MSG_PAUSE = 2;
    private static final int MSG_RELEASE = 1;
    private Context context;
    /* access modifiers changed from: private */
    public ControllerHandler handler = new ControllerHandler(new WeakReference(this));
    private ImageButton ibFull;
    /* access modifiers changed from: private */
    public boolean isPrepared = false;
    /* access modifiers changed from: private */
    public boolean isStart = false;
    /* access modifiers changed from: private */
    public ImageView ivPause;
    private final int ivPauseId = 305419896;
    /* access modifiers changed from: private */
    public ImageView ivThumb;
    private OnStartPlayListener onStartPlayListener;
    private ProgressBar pbBuffer;
    /* access modifiers changed from: private */
    public int position = 0;
    /* access modifiers changed from: private */
    public RelativeLayout rlBuffer;
    /* access modifiers changed from: private */
    public RelativeLayout rlController;
    /* access modifiers changed from: private */
    public RelativeLayout rlPause;
    /* access modifiers changed from: private */
    public SeekBar sbProgress;
    /* access modifiers changed from: private */
    public String title;
    /* access modifiers changed from: private */
    public TextView tvBuffer;
    private TextView tvTime;
    private final int tvTimeMarginTopDp = 5;
    /* access modifiers changed from: private */
    public String url;
    private int videoHeight;
    private final int videoHeightRatio = 9;
    /* access modifiers changed from: private */
    public QihooVideoView videoView;
    private int videoWidth;
    private final int videoWidthRatio = 16;

    class ControllerHandler extends Handler {
        private WeakReference<QHVideoController> reference;

        public ControllerHandler(WeakReference<QHVideoController> weakReference) {
            this.reference = weakReference;
        }

        public void handleMessage(Message message) {
            QHVideoController qHVideoController = this.reference.get();
            switch (message.what) {
                case 1:
                    if (qHVideoController != null) {
                        qHVideoController.release();
                        return;
                    }
                    return;
                case 2:
                    if (qHVideoController != null) {
                        qHVideoController.pause();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public interface OnStartPlayListener {
        void onStart();
    }

    public QHVideoController(Context context2) {
        super(context2);
        initView(context2);
    }

    public QHVideoController(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        initView(context2);
    }

    public QHVideoController(Context context2, AttributeSet attributeSet, int i) {
        super(context2, attributeSet, i);
        initView(context2);
    }

    private void initVideoView(final Context context2) {
        this.isPrepared = false;
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        this.videoView = new QihooVideoView(context2);
        layoutParams.gravity = 17;
        addView(this.videoView, 0, layoutParams);
        this.videoView.setKeepScreenOn(true);
        this.videoView.setOnPreparedListener(new QihooMediaPlayer.OnPreparedListener() {
            public void onPrepared(QihooMediaPlayer qihooMediaPlayer) {
                boolean unused = QHVideoController.this.isPrepared = true;
                boolean unused2 = QHVideoController.this.isStart = false;
                if (QHVideoController.this.position == 0) {
                    QHVideoController.this.videoView.start();
                } else {
                    QHVideoController.this.videoView.seekTo(QHVideoController.this.position);
                    int unused3 = QHVideoController.this.position = 0;
                }
                QHVideoController.this.rlBuffer.setVisibility(8);
                QHVideoController.this.ivThumb.setVisibility(4);
                if (QHVideoController.this.rlController != null) {
                    QHVideoController.this.rlController.setVisibility(0);
                }
            }
        });
        this.videoView.setOnSeekCompleteListener(new QihooMediaPlayer.OnSeekCompleteListener() {
            public void onSeekComplete(QihooMediaPlayer qihooMediaPlayer) {
                QHVideoController.this.sbProgress.setProgress((int) ((((float) qihooMediaPlayer.getCurrentPosition()) / ((float) qihooMediaPlayer.getDuration())) * 100.0f));
                qihooMediaPlayer.start();
            }
        });
        this.videoView.setOnPositionChangeListener(new QihooMediaPlayer.OnPositionChangeListener() {
            public void onPlayPositionChanged(QihooMediaPlayer qihooMediaPlayer, int i) {
                QHVideoController.this.sbProgress.setProgress((int) ((((float) i) / ((float) qihooMediaPlayer.getDuration())) * 100.0f));
            }
        });
        this.videoView.setOnBufferListener(new QihooMediaPlayer.OnBufferingUpdateListener() {
            public void onBufferingUpdate(QihooMediaPlayer qihooMediaPlayer, int i) {
                if (QHVideoController.this.tvBuffer != null) {
                    QHVideoController.this.tvBuffer.setText(String.format(QHVideoController.this.getResources().getString(R.string.already_loading), Integer.valueOf(i)));
                }
                if (i < 0 || i >= 100) {
                    if (i == 100 && QHVideoController.this.rlBuffer.isShown()) {
                        QHVideoController.this.rlBuffer.setVisibility(8);
                    }
                } else if (!QHVideoController.this.rlBuffer.isShown()) {
                    QHVideoController.this.rlBuffer.setVisibility(0);
                }
            }
        });
        this.videoView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!QHVideoController.this.isPrepared) {
                    return;
                }
                if (QHVideoController.this.videoView.isPlaying()) {
                    QHVideoController.this.pause();
                } else if (b.d(context2)) {
                    if (QHVideoController.this.position == 0) {
                        QHVideoController.this.start();
                    } else {
                        QHVideoController.this.startVideo();
                    }
                    QHVideoController.this.rlPause.setVisibility(8);
                } else {
                    ay.a(context2).a((int) R.string.can_not_play);
                    QHVideoController.this.handler.sendEmptyMessage(2);
                }
            }
        });
        this.videoView.setOnCompletetionListener(new QihooMediaPlayer.OnCompletionListener() {
            public void onCompletion(QihooMediaPlayer qihooMediaPlayer) {
                QHVideoController.this.handler.sendEmptyMessage(1);
            }
        });
        this.videoView.setOnErrorListener(new QihooMediaPlayer.OnErrorListener() {
            public boolean onError(QihooMediaPlayer qihooMediaPlayer, int i, int i2) {
                if (b.d(context2)) {
                    return false;
                }
                ay.a(context2).a((int) R.string.can_not_play);
                QHVideoController.this.handler.sendEmptyMessage(2);
                return false;
            }
        });
        requestFocus();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.qihoo360.daily.widget.QHVideoController, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void initView(final Context context2) {
        setBackgroundResource(R.color.black);
        this.context = context2;
        LayoutInflater from = LayoutInflater.from(context2);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -2);
        this.rlController = (RelativeLayout) from.inflate((int) R.layout.controller_strip_video, (ViewGroup) this, false);
        layoutParams.gravity = 81;
        this.rlController.setBackgroundColor(0);
        addView(this.rlController, layoutParams);
        this.ibFull = (ImageButton) findViewById(R.id.ib_full);
        this.sbProgress = (SeekBar) findViewById(R.id.sb_progress);
        this.ivThumb = new ImageView(context2);
        this.ivThumb.setId(R.id.thumb_video);
        this.ivThumb.setBackgroundColor(getResources().getColor(R.color.bg_video));
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-1, -1);
        layoutParams2.gravity = 17;
        addView(this.ivThumb, layoutParams2);
        this.rlBuffer = (RelativeLayout) from.inflate((int) R.layout.buffer_video, (ViewGroup) this, false);
        this.pbBuffer = (ProgressBar) this.rlBuffer.findViewById(R.id.pb_buffer);
        RelativeLayout.LayoutParams layoutParams3 = (RelativeLayout.LayoutParams) this.pbBuffer.getLayoutParams();
        layoutParams3.width = b.a(context2, 30.0f);
        layoutParams3.height = layoutParams3.width;
        this.pbBuffer.setLayoutParams(layoutParams3);
        this.tvBuffer = (TextView) this.rlBuffer.findViewById(R.id.tv_buffer);
        this.tvBuffer.setText(String.format(getResources().getString(R.string.already_loading), 0));
        FrameLayout.LayoutParams layoutParams4 = new FrameLayout.LayoutParams(-2, -2);
        layoutParams4.gravity = 17;
        addView(this.rlBuffer, layoutParams4);
        this.rlPause = new RelativeLayout(context2);
        this.ivPause = new ImageView(context2);
        this.ivPause.setBackgroundResource(R.drawable.un_play_no_thumb);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams5.addRule(14, -1);
        this.ivPause.setId(305419896);
        this.rlPause.addView(this.ivPause, layoutParams5);
        RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams6.addRule(3, 305419896);
        layoutParams6.topMargin = bk.a(context2, 5.0f);
        layoutParams6.addRule(14, -1);
        this.tvTime = new TextView(context2);
        this.tvTime.setPadding(b.a(context2, 2.0f), 0, b.a(context2, 2.0f), 0);
        this.tvTime.setText(bk.a(0));
        this.tvTime.setGravity(17);
        this.tvTime.setTextSize((float) b.a(context2, 4.0f));
        this.tvTime.setBackgroundResource(R.drawable.bg_time_video);
        this.rlPause.addView(this.tvTime, layoutParams6);
        FrameLayout.LayoutParams layoutParams7 = new FrameLayout.LayoutParams(-2, -2);
        layoutParams7.gravity = 17;
        addView(this.rlPause, layoutParams7);
        this.rlBuffer.setVisibility(8);
        this.rlController.setVisibility(8);
        this.ivThumb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                boolean z = true;
                if (QHVideoController.this.ivThumb.isShown() && !QHVideoController.this.isStart) {
                    if (!i.b()) {
                        ay.a(context2).a((int) R.string.cpu_no_support);
                    } else if (b.a(context2)) {
                        if (d.l(context2)) {
                            if (System.currentTimeMillis() - d.c(context2, 0) <= QDefine.ONE_DAY) {
                                z = false;
                            }
                        }
                        if (!z || context2 == null || !(context2 instanceof Activity)) {
                            QHVideoController.this.startVideo();
                            return;
                        }
                        TrafficDialog trafficDialog = new TrafficDialog((Activity) context2, d.l(context2));
                        trafficDialog.showDialog();
                        trafficDialog.setOnDialogViewClickListener(new TrafficDialog.OnDialogViewClickListener() {
                            private boolean isChecked = d.l(context2);

                            public void onChooseClick(boolean z) {
                                this.isChecked = z;
                            }

                            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                             method: com.qihoo360.daily.f.d.c(android.content.Context, boolean):void
                             arg types: [android.content.Context, int]
                             candidates:
                              com.qihoo360.daily.f.d.c(android.content.Context, long):long
                              com.qihoo360.daily.f.d.c(android.content.Context, java.lang.String):long
                              com.qihoo360.daily.f.d.c(android.content.Context, boolean):void */
                            public void onLeftButtonClick() {
                                d.c(context2, false);
                            }

                            public void onRightButtonClick() {
                                d.c(context2, this.isChecked);
                                QHVideoController.this.startVideo();
                                if (this.isChecked) {
                                    d.b(context2, System.currentTimeMillis());
                                }
                            }
                        });
                    } else {
                        QHVideoController.this.startVideo();
                    }
                }
            }
        });
        this.sbProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                if (z) {
                    QHVideoController.this.videoView.seekTo((int) ((((float) i) / 100.0f) * ((float) QHVideoController.this.videoView.getDuration())));
                }
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        this.ibFull.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int currentPosition = QHVideoController.this.videoView.getCurrentPosition();
                QHVideoController.this.release();
                Intent intent = new Intent(context2, VideoFullActivity.class);
                intent.putExtra("position", currentPosition);
                intent.putExtra(SearchActivity.TAG_URL, QHVideoController.this.url);
                intent.putExtra("title", QHVideoController.this.title);
                context2.startActivity(intent);
            }
        });
    }

    private void setThumb(String str) {
        if (str != null && !"".equals(str.trim())) {
            e.a().a(this.context, str, new l() {
                public ImageView getView() {
                    return QHVideoController.this.ivThumb;
                }

                public int getViewID() {
                    return QHVideoController.this.ivThumb.getId();
                }

                public Object getViewTag() {
                    return QHVideoController.this.ivThumb.getTag();
                }

                public void imageLoaded(Drawable drawable, String str, boolean z) {
                    if (QHVideoController.this.ivThumb.getTag() != null && !"".equals((String) QHVideoController.this.ivThumb.getTag()) && str.trim().equals((String) QHVideoController.this.ivThumb.getTag())) {
                        QHVideoController.this.ivThumb.setImageDrawable(drawable);
                        QHVideoController.this.ivPause.setBackgroundResource(R.drawable.un_play_video);
                    }
                }

                public void setViewTag(String str) {
                    QHVideoController.this.ivThumb.setTag(str);
                }
            }, 0);
        }
    }

    private void setTime(String str) {
        if (this.tvTime != null) {
            this.tvTime.setText(str);
        }
    }

    public QihooVideoView getVideoView() {
        return this.videoView;
    }

    public void init(String str, String str2, String str3, String str4) {
        if (this.ivThumb != null) {
            this.ivPause.setBackgroundResource(R.drawable.un_play_no_thumb);
            this.ivThumb.setImageDrawable(null);
            this.ivThumb.setBackgroundColor(getResources().getColor(R.color.bg_video));
        }
        setThumb(str3);
        setTime(str4);
        this.title = str;
        this.url = bk.a(str2);
    }

    public boolean isPlay() {
        if (this.videoView != null) {
            return this.videoView.isPlaying();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int size = View.MeasureSpec.getSize(i);
        if (size > 0) {
            this.videoWidth = size;
            this.videoHeight = (size * 9) / 16;
        }
        if (this.ivThumb != null) {
            ViewGroup.LayoutParams layoutParams = this.ivThumb.getLayoutParams();
            layoutParams.width = this.videoWidth;
            layoutParams.height = this.videoHeight;
        }
        super.onMeasure(i, i2);
    }

    public void pause() {
        if (this.videoView != null) {
            this.videoView.pause();
            this.ivPause.setBackgroundResource(R.drawable.un_play_video);
            this.rlPause.setVisibility(0);
            this.rlBuffer.setVisibility(8);
        }
    }

    public void release() {
        if (this.videoView != null) {
            this.videoView.stop();
            this.videoView.release();
            this.videoView.setVisibility(8);
            removeView(this.videoView);
            this.videoView = null;
        }
        this.isPrepared = false;
        this.isStart = false;
        this.rlBuffer.setVisibility(8);
        this.ivThumb.setVisibility(0);
        this.rlController.setVisibility(8);
        if (this.ivThumb.getDrawable() != null) {
            this.ivPause.setBackgroundResource(R.drawable.un_play_video);
        } else {
            this.ivPause.setBackgroundResource(R.drawable.un_play_no_thumb);
        }
        this.rlPause.setVisibility(0);
        this.tvTime.setVisibility(0);
    }

    public void setOnStartPlayListener(OnStartPlayListener onStartPlayListener2) {
        this.onStartPlayListener = onStartPlayListener2;
    }

    public void start() {
        if (this.videoView != null) {
            this.videoView.start();
        }
    }

    public void startVideo() {
        this.isStart = true;
        if (this.onStartPlayListener != null) {
            this.onStartPlayListener.onStart();
        }
        this.rlPause.setVisibility(8);
        this.tvTime.setVisibility(8);
        this.rlBuffer.setVisibility(0);
        if (this.videoView == null) {
            initVideoView(this.context);
        }
        this.videoView.setVisibility(0);
        this.videoView.initVideoWidAndHeight(this.videoWidth, this.videoHeight);
        this.videoView.setDataSource(this.url);
    }

    public void stop() {
        if (this.videoView != null) {
            this.videoView.stop();
        }
    }
}
