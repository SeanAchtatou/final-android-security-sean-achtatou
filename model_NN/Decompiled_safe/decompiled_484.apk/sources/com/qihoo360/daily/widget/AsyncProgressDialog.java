package com.qihoo360.daily.widget;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qihoo360.daily.R;

public class AsyncProgressDialog extends ProgressDialog {
    private Context context;
    private RelativeLayout rlDialogView;
    private TextView tvMsg;

    public AsyncProgressDialog(Context context2) {
        this(context2, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public AsyncProgressDialog(Context context2, int i) {
        super(context2, i);
        this.context = context2;
        this.rlDialogView = (RelativeLayout) LayoutInflater.from(context2).inflate((int) R.layout.progressdialog_async, (ViewGroup) null, false);
        this.tvMsg = (TextView) this.rlDialogView.findViewById(R.id.progress_msg);
    }

    public void setContent(int i) {
        if (i > 0) {
            setContent(this.context.getResources().getString(i));
        }
    }

    public void setContent(String str) {
        if (str != null && !"".equals(str.trim()) && this.tvMsg != null) {
            this.tvMsg.setText(str);
        }
    }

    public void show() {
        super.show();
        setContentView(this.rlDialogView);
    }
}
