package com.qihoo360.daily.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.qihoo360.daily.R;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.widget.ScrollContentTextView;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ProgressTextView extends FrameLayout {
    private ArcProgressBar arcProgressBar;
    private ScrollContentTextView keyWordsTextView;
    private List<String> keywords;
    private ScrollContentTextView numberTextView;
    private OnProgressListener onProgressListener;

    public interface OnProgressListener {
        void onComplete(ArcProgressBar arcProgressBar);
    }

    public ProgressTextView(Context context) {
        this(context, null);
    }

    public ProgressTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ProgressTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initView();
    }

    private List<String> getNumbers() {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < 100; i++) {
            if (i < 10) {
                arrayList.add("0" + (i + 1));
            }
        }
        return arrayList;
    }

    private void initView() {
        setBackgroundColor(0);
        this.arcProgressBar = new ArcProgressBar(getContext());
        if (this.onProgressListener != null) {
            this.arcProgressBar.setOnProgressListener(this.onProgressListener);
        }
        this.arcProgressBar.setArcDefaultColor(getResources().getColor(R.color.arc_inner));
        this.arcProgressBar.setArcDefaultWidth(1.0f);
        this.arcProgressBar.setArcDefaultRadius((float) b.a(getContext(), 58.0f));
        this.arcProgressBar.setArcProgressColor(getResources().getColor(R.color.arc_outer));
        this.arcProgressBar.setArcProgressWidth((float) b.a(getContext(), 9.0f));
        this.arcProgressBar.setArcProgressRadius((float) b.a(getContext(), 70.0f));
        addView(this.arcProgressBar);
        this.keyWordsTextView = new ScrollContentTextView(getContext());
        this.keyWordsTextView.setWidth(b.a(getContext(), 100.0f));
        this.keyWordsTextView.setHeight(b.a(getContext(), 60.0f));
        this.keyWordsTextView.setTextColor(getResources().getColor(R.color.keywords));
        this.keyWordsTextView.setTextSize((float) b.a(getContext(), 16.0f));
        this.keyWordsTextView.setKeywords(getKeywords());
        addView(this.keyWordsTextView);
        this.numberTextView = new ScrollContentTextView(getContext());
        this.numberTextView.setWidth(b.a(getContext(), 45.0f));
        this.numberTextView.setHeight(b.a(getContext(), 35.0f));
        this.numberTextView.setTextColor(getResources().getColor(R.color.number));
        this.numberTextView.setTextSize((float) b.a(getContext(), 37.0f));
        Random random = new Random();
        int nextInt = random.nextInt(100) + 200;
        int nextInt2 = random.nextInt(100) + 300;
        this.keyWordsTextView.setKeywordShowTime((long) nextInt);
        this.keyWordsTextView.setKeywordAnimationTime((long) nextInt2);
        this.numberTextView.setKeywordShowTime((long) nextInt);
        this.numberTextView.setKeywordAnimationTime((long) nextInt2);
        if (getKeywords() != null && getKeywords().size() > 0) {
            this.numberTextView.setKeywords(getNumbers());
        }
        addView(this.numberTextView);
    }

    public List<String> getKeywords() {
        return this.keywords;
    }

    public OnProgressListener getOnProgressListener() {
        return this.onProgressListener;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        ((FrameLayout.LayoutParams) this.arcProgressBar.getLayoutParams()).gravity = 1;
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.keyWordsTextView.getLayoutParams();
        layoutParams.topMargin = (this.arcProgressBar.getMeasuredWidth() / 2) - (this.keyWordsTextView.getMeasuredHeight() / 2);
        layoutParams.gravity = 1;
        FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) this.numberTextView.getLayoutParams();
        layoutParams2.topMargin = (this.arcProgressBar.getMeasuredWidth() - this.numberTextView.getMeasuredHeight()) - b.a(getContext(), 5.0f);
        layoutParams2.gravity = 1;
    }

    public void setKeywords(List<String> list) {
        this.keywords = list;
        if (this.keyWordsTextView != null) {
            this.keyWordsTextView.setKeywords(list);
        }
        if (this.numberTextView != null) {
            this.numberTextView.setKeywords(getNumbers());
        }
    }

    public void setOnProgressListener(OnProgressListener onProgressListener2) {
        this.onProgressListener = onProgressListener2;
        if (this.arcProgressBar != null) {
            this.arcProgressBar.setOnProgressListener(onProgressListener2);
        }
    }

    public void start() {
        this.arcProgressBar.start();
        this.keyWordsTextView.setType(ScrollContentTextView.Type.oneNextOne);
        this.keyWordsTextView.start();
        this.numberTextView.start();
    }

    public void stop() {
        if (this.arcProgressBar != null) {
            this.arcProgressBar.stop();
        }
        if (this.keyWordsTextView != null) {
            this.keyWordsTextView.stop();
        }
        if (this.numberTextView != null) {
            this.numberTextView.stop();
        }
    }
}
