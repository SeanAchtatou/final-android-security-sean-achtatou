package com.qihoo360.daily.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

public class InnerScrollableViewPager extends FixedViewPager {
    public InnerScrollableViewPager(Context context) {
        super(context);
    }

    public InnerScrollableViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    private boolean customCanScroll(View view, int i) {
        if (view instanceof HorizontalListViewInListView) {
            return true;
        }
        return view != this && (view instanceof ViewPager);
    }

    /* access modifiers changed from: protected */
    public boolean canScroll(View view, boolean z, int i, int i2, int i3) {
        return super.canScroll(view, z, i, i2, i3) || customCanScroll(view, i);
    }
}
