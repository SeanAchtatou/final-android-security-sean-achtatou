package com.qihoo360.daily.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import com.qihoo360.daily.R;

public class DialogView {
    protected Activity activity;
    private Dialog dialog;
    private DialogInterface.OnDismissListener mOnDialogDismissListener;
    protected View view;

    protected DialogView(Activity activity2, int i) {
        this.activity = activity2;
        this.view = loadLayout(i);
        initDialog();
    }

    public DialogView(Activity activity2, View view2) {
        this.activity = activity2;
        this.view = view2;
        initDialog();
    }

    protected DialogView(Context context) {
    }

    private View loadLayout(int i) {
        View inflate = LayoutInflater.from(this.activity).inflate(i, (ViewGroup) null);
        onLoadLayout(inflate);
        return inflate;
    }

    public void disMissDialog() {
        if (this.dialog != null && this.dialog.isShowing()) {
            this.dialog.dismiss();
        }
    }

    public DialogInterface.OnDismissListener getOnDialogDismissListener() {
        return this.mOnDialogDismissListener;
    }

    public View getView() {
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void initDialog() {
        this.dialog = new Dialog(this.activity, R.style.dialog_view_theme);
        this.dialog.getWindow().setGravity(17);
        this.dialog.getWindow().setWindowAnimations(R.style.dialog_view_animation);
        this.dialog.setCancelable(true);
        this.dialog.setCanceledOnTouchOutside(true);
        this.dialog.setContentView(this.view);
    }

    public boolean isShowing() {
        return this.dialog != null && this.dialog.isShowing();
    }

    /* access modifiers changed from: protected */
    public void onLoadLayout(View view2) {
    }

    public void setCancelable(boolean z) {
        if (this.dialog != null) {
            this.dialog.setCancelable(z);
        }
    }

    public void setCanceledOnTouchOutside(boolean z) {
        if (this.dialog != null) {
            this.dialog.setCanceledOnTouchOutside(z);
        }
    }

    public void setFullScreen(boolean z) {
        if (z && this.dialog != null) {
            WindowManager.LayoutParams attributes = this.dialog.getWindow().getAttributes();
            attributes.width = -1;
            attributes.height = -1;
        }
    }

    public void setFullWidth(boolean z) {
        if (z && this.dialog != null) {
            WindowManager.LayoutParams attributes = this.dialog.getWindow().getAttributes();
            attributes.width = -1;
            attributes.height = -2;
        }
    }

    public void setGravity(int i) {
        this.dialog.getWindow().setGravity(i);
    }

    public void setOnDialogDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        this.mOnDialogDismissListener = onDismissListener;
        if (this.dialog != null) {
            this.dialog.setOnDismissListener(this.mOnDialogDismissListener);
        }
    }

    public void setView(View view2) {
        this.view = view2;
    }

    public void showDialog() {
        if (this.dialog != null && !this.dialog.isShowing()) {
            this.dialog.show();
        }
    }
}
