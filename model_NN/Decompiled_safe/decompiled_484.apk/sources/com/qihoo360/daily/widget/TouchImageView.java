package com.qihoo360.daily.widget;

import android.content.Context;
import android.graphics.Matrix;
import android.os.Handler;
import android.support.v7.internal.widget.ActivityChooserView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import android.widget.Scroller;
import com.qihoo.messenger.util.QDefine;

public class TouchImageView extends BigImageView {
    private boolean isFling;
    private boolean isMultiplePoint;
    private boolean isTransing;
    private int mFlingLastX;
    private int mFlingLastY;
    private Scroller mFlingScroller;
    private double mLastSpace;
    private float mLastX;
    private float mLastY;
    private int mMaximumVelocity;
    private int mMinimumVelocity;
    private float mStartX;
    private float mStartY;
    private int mTouchSlop;
    private Scroller mTranScroller;
    private int mTransLastX;
    private int mTransLastY;
    private VelocityTracker mVelocityTracker;

    public TouchImageView(Context context) {
        super(context);
        init();
    }

    public TouchImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public TouchImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }

    private void checkEvent() {
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.computeCurrentVelocity(QDefine.ONE_SECOND, (float) this.mMaximumVelocity);
            float xVelocity = this.mVelocityTracker.getXVelocity();
            float abs = Math.abs(xVelocity / this.mVelocityTracker.getYVelocity());
            float abs2 = Math.abs(this.mLastX - this.mStartX);
            if (abs > 2.0f && abs2 < 100.0f) {
                Matrix matrix = new Matrix();
                matrix.set(getImageMatrix());
                float matrixValue = getMatrixValue(matrix, 0);
                float matrixValue2 = getMatrixValue(matrix, 2);
                int i = (int) (matrixValue * ((float) this.dWidth));
                if (xVelocity > 0.0f) {
                    if (matrixValue2 >= -10.0f) {
                        getParent().requestDisallowInterceptTouchEvent(false);
                    }
                } else if (((float) i) + matrixValue2 <= ((float) (this.vWidth + 10))) {
                    getParent().requestDisallowInterceptTouchEvent(false);
                }
            }
        }
    }

    private void completeFling() {
        if (this.isFling) {
            this.mFlingScroller.abortAnimation();
            int finalX = this.mFlingScroller.getFinalX();
            int finalY = this.mFlingScroller.getFinalY();
            drawableTranslate((float) (finalX - this.mFlingLastX), (float) (finalY - this.mFlingLastY));
            this.mFlingLastX = finalX;
            this.mFlingLastY = finalY;
            setLongClickable(true);
            setClickable(true);
        }
        this.isFling = false;
    }

    private void completeTrans() {
        if (this.isTransing) {
            this.mTranScroller.abortAnimation();
            new Matrix().set(getImageMatrix());
            int finalX = this.mTranScroller.getFinalX();
            int finalY = this.mTranScroller.getFinalY();
            drawableTranslate((float) (finalX - this.mTransLastX), (float) (finalY - this.mTransLastY));
            this.mTransLastX = finalX;
            this.mTransLastY = finalY;
        }
        this.isTransing = false;
    }

    private void computeFling() {
        if (this.mFlingScroller.isFinished() || !this.mFlingScroller.computeScrollOffset()) {
            completeFling();
            return;
        }
        int currX = this.mFlingScroller.getCurrX();
        int currY = this.mFlingScroller.getCurrY();
        drawableTranslate((float) (currX - this.mFlingLastX), (float) (currY - this.mFlingLastY));
        this.mFlingLastX = currX;
        this.mFlingLastY = currY;
        postInvalidate();
    }

    private void computeTrans() {
        if (this.mTranScroller.isFinished() || !this.mTranScroller.computeScrollOffset()) {
            completeTrans();
            return;
        }
        Matrix matrix = new Matrix();
        matrix.set(getImageMatrix());
        int currX = this.mTranScroller.getCurrX();
        int currY = this.mTranScroller.getCurrY();
        matrix.postTranslate((float) (currX - this.mTransLastX), (float) (currY - this.mTransLastY));
        setImageMatrix(matrix);
        this.mTransLastX = currX;
        this.mTransLastY = currY;
        postInvalidate();
    }

    private synchronized void drawablePostScale(float f, float f2, float f3) {
        Matrix matrix = new Matrix();
        matrix.set(getImageMatrix());
        float matrixValue = getMatrixValue(matrix, 0);
        float f4 = matrixValue * f > this.maxScale ? this.maxScale / matrixValue : f;
        if (matrixValue * f4 < this.minScale) {
            f4 = this.minScale / matrixValue;
        }
        if (f4 != 1.0f) {
            matrix.postScale(f4, f4, f2, f3);
            setImageMatrix(matrix);
        }
    }

    private synchronized void drawableTranslate(float f, float f2) {
        float f3;
        Matrix matrix = new Matrix();
        matrix.set(getImageMatrix());
        float matrixValue = getMatrixValue(matrix, 0);
        float matrixValue2 = getMatrixValue(matrix, 2);
        float matrixValue3 = getMatrixValue(matrix, 5);
        if (matrixValue3 <= 0.0f || f2 <= 0.0f) {
            f3 = f2;
        } else {
            float f4 = ((float) this.vHeight) / 2.0f;
            f3 = f2 * ((f4 - matrixValue3) / f4);
        }
        float f5 = ((float) this.dHeight) * matrixValue;
        if (f3 < 0.0f && matrixValue3 < ((float) this.vHeight) - f5) {
            float f6 = ((float) this.vHeight) / 2.0f;
            f3 *= (f6 - (((-matrixValue3) + ((float) this.vHeight)) - f5)) / f6;
        }
        int i = (int) (matrixValue * ((float) this.dWidth));
        float f7 = (i > this.vWidth || f5 > ((float) this.vHeight)) ? f3 : 0.0f;
        float f8 = matrixValue2 + f;
        float f9 = (f8 >= 0.0f || ((float) i) + f8 >= ((float) this.vWidth)) ? f : (((float) this.vWidth) - matrixValue2) - ((float) i);
        if (f8 > 0.0f) {
            f9 = -matrixValue2;
        }
        if (i < this.vWidth) {
            f9 = 0.0f;
        }
        if (!(f9 == 0.0f && f7 == 0.0f)) {
            matrix.postTranslate(f9, f7);
            setImageMatrix(matrix);
        }
    }

    private float getMatrixValue(Matrix matrix, int i) {
        if (matrix == null || i < 0 || i > 8) {
            return 0.0f;
        }
        float[] fArr = new float[9];
        matrix.getValues(fArr);
        return fArr[i];
    }

    private double getMultiplePointSpace(MotionEvent motionEvent) {
        float x = motionEvent.getX(0);
        float y = motionEvent.getY(0);
        return Math.hypot((double) (motionEvent.getX(1) - x), (double) (motionEvent.getY(1) - y));
    }

    private float getPointX(MotionEvent motionEvent) {
        return (motionEvent.getX(0) + motionEvent.getX(1)) / 2.0f;
    }

    private float getPointY(MotionEvent motionEvent) {
        return (motionEvent.getY(0) + motionEvent.getY(1)) / 2.0f;
    }

    private void init() {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        this.mMinimumVelocity = 0;
        this.mMaximumVelocity = viewConfiguration.getScaledMaximumFlingVelocity() / 2;
        this.mTouchSlop = viewConfiguration.getScaledTouchSlop();
        this.mTranScroller = new Scroller(getContext());
        this.mFlingScroller = new Scroller(getContext());
    }

    private void startFling(int i) {
        int i2;
        int i3;
        int i4 = ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        int i5 = -2147483647;
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.computeCurrentVelocity(QDefine.ONE_SECOND, (float) this.mMaximumVelocity);
            int xVelocity = (int) this.mVelocityTracker.getXVelocity();
            int yVelocity = (int) this.mVelocityTracker.getYVelocity();
            if (Math.abs(xVelocity) > this.mMinimumVelocity || Math.abs(yVelocity) > this.mMinimumVelocity) {
                if (xVelocity < 0) {
                    i2 = 0;
                    i3 = -2147483647;
                } else {
                    i2 = Integer.MAX_VALUE;
                    i3 = 0;
                }
                if (yVelocity < 0) {
                    i4 = 0;
                } else {
                    i5 = 0;
                }
                if (i == 1) {
                    i4 = 0;
                    i5 = 0;
                    yVelocity = 0;
                }
                if (i == 2) {
                    i2 = 0;
                    i3 = 0;
                    xVelocity = 0;
                }
                this.mFlingScroller.fling(0, 0, xVelocity, yVelocity, i3, i2, i5, i4);
                this.isFling = true;
                this.mFlingLastX = 0;
                this.mFlingLastY = 0;
                postInvalidate();
            }
        }
    }

    private void startTrans() {
        Matrix matrix = new Matrix();
        matrix.set(getImageMatrix());
        float matrixValue = getMatrixValue(matrix, 0);
        float matrixValue2 = getMatrixValue(matrix, 2);
        float matrixValue3 = getMatrixValue(matrix, 5);
        float f = ((float) this.dWidth) * matrixValue;
        float f2 = matrixValue * ((float) this.dHeight);
        int i = f2 <= ((float) this.vHeight) ? (int) (((float) (this.vHeight / 2)) - (((matrixValue3 + matrixValue3) + f2) / 2.0f)) : 0;
        float f3 = matrixValue2 + f;
        int i2 = f3 < ((float) this.vWidth) ? (int) (((float) this.vWidth) - f3) : matrixValue2 > 0.0f ? (int) (-matrixValue2) : 0;
        if (i2 != 0 || i != 0) {
            this.mTranScroller.startScroll(0, 0, i2, i, 250);
            this.mTransLastX = 0;
            this.mTransLastY = 0;
            postInvalidate();
            this.isTransing = true;
        }
    }

    public void computeScroll() {
        computeTrans();
        computeFling();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00ec  */
    @android.annotation.SuppressLint({"ClickableViewAccessibility"})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r10) {
        /*
            r9 = this;
            r8 = 1084227584(0x40a00000, float:5.0)
            r7 = 1
            r3 = -1082130432(0xffffffffbf800000, float:-1.0)
            r6 = 0
            super.onTouchEvent(r10)
            android.view.VelocityTracker r0 = r9.mVelocityTracker
            if (r0 != 0) goto L_0x0013
            android.view.VelocityTracker r0 = android.view.VelocityTracker.obtain()
            r9.mVelocityTracker = r0
        L_0x0013:
            android.view.VelocityTracker r0 = r9.mVelocityTracker
            r0.addMovement(r10)
            float r0 = r10.getX()
            float r1 = r10.getY()
            int r2 = r10.getAction()
            r2 = r2 & 255(0xff, float:3.57E-43)
            switch(r2) {
                case 0: goto L_0x002a;
                case 1: goto L_0x00bd;
                case 2: goto L_0x004f;
                case 3: goto L_0x00b7;
                case 4: goto L_0x0029;
                case 5: goto L_0x0040;
                case 6: goto L_0x00ac;
                default: goto L_0x0029;
            }
        L_0x0029:
            return r7
        L_0x002a:
            android.view.ViewParent r2 = r9.getParent()
            r2.requestDisallowInterceptTouchEvent(r7)
            r9.mLastX = r3
            r9.mLastY = r3
            r9.mStartX = r0
            r9.mStartY = r1
            r9.setClickable(r7)
            r9.setLongClickable(r7)
            goto L_0x0029
        L_0x0040:
            r9.isMultiplePoint = r7
            double r0 = r9.getMultiplePointSpace(r10)
            r9.mLastSpace = r0
            r9.setClickable(r6)
            r9.setLongClickable(r6)
            goto L_0x0029
        L_0x004f:
            boolean r2 = r9.isTransing
            if (r2 != 0) goto L_0x0029
            boolean r2 = r9.isMultiplePoint
            if (r2 == 0) goto L_0x006e
            double r0 = r9.getMultiplePointSpace(r10)
            double r2 = r9.mLastSpace
            double r2 = r0 / r2
            float r2 = (float) r2
            float r3 = r9.getPointX(r10)
            float r4 = r9.getPointY(r10)
            r9.drawablePostScale(r2, r3, r4)
            r9.mLastSpace = r0
            goto L_0x0029
        L_0x006e:
            float r2 = r9.mLastX
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 == 0) goto L_0x00a3
            float r2 = r9.mLastY
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 == 0) goto L_0x00a3
            float r2 = r9.mLastX
            float r2 = r0 - r2
            float r3 = r9.mLastY
            float r3 = r1 - r3
            float r4 = r9.mStartX
            float r4 = r0 - r4
            float r4 = java.lang.Math.abs(r4)
            float r5 = r9.mStartY
            float r5 = r1 - r5
            float r5 = java.lang.Math.abs(r5)
            int r4 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r4 > 0) goto L_0x009a
            int r4 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r4 <= 0) goto L_0x00a0
        L_0x009a:
            r9.setClickable(r6)
            r9.setLongClickable(r6)
        L_0x00a0:
            r9.drawableTranslate(r2, r3)
        L_0x00a3:
            r9.mLastX = r0
            r9.mLastY = r1
            r9.checkEvent()
            goto L_0x0029
        L_0x00ac:
            r9.startTrans()
            r9.mLastX = r3
            r9.mLastY = r3
            r9.isMultiplePoint = r6
            goto L_0x0029
        L_0x00b7:
            r9.setLongClickable(r6)
            r9.setClickable(r6)
        L_0x00bd:
            r9.mLastX = r3
            r9.mLastY = r3
            r9.isMultiplePoint = r6
            boolean r0 = r9.isTransing
            if (r0 != 0) goto L_0x00e8
            android.graphics.Matrix r0 = new android.graphics.Matrix
            r0.<init>()
            android.graphics.Matrix r1 = r9.getImageMatrix()
            r0.set(r1)
            float r0 = r9.getMatrixValue(r0, r6)
            int r1 = r9.dHeight
            float r1 = (float) r1
            float r0 = r0 * r1
            int r1 = r9.vHeight
            float r1 = (float) r1
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x00f6
            r9.startTrans()
            r9.startFling(r7)
        L_0x00e8:
            android.view.VelocityTracker r0 = r9.mVelocityTracker
            if (r0 == 0) goto L_0x0029
            android.view.VelocityTracker r0 = r9.mVelocityTracker
            r0.recycle()
            r0 = 0
            r9.mVelocityTracker = r0
            goto L_0x0029
        L_0x00f6:
            r9.startFling(r6)
            goto L_0x00e8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.widget.TouchImageView.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void reset() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                TouchImageView.this.setLongClickable(true);
                TouchImageView.this.setClickable(true);
            }
        }, 500);
        initMatrix();
    }

    public void setClickable(boolean z) {
        super.setClickable(z);
    }

    public void setLongClickable(boolean z) {
        super.setLongClickable(z);
    }
}
