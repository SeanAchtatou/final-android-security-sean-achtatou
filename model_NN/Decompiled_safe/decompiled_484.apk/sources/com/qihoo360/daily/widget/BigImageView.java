package com.qihoo360.daily.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.qihoo360.daily.c.n;
import java.util.ArrayList;
import java.util.Iterator;

public class BigImageView extends ImageView {
    protected int dHeight;
    private float dRatio;
    protected int dWidth;
    private ArrayList<Drawable> drawables;
    private boolean isFirst = true;
    protected float maxScale = 3.0f;
    protected float minScale = 0.8f;
    protected int vHeight;
    private float vRatio;
    protected int vWidth;

    public BigImageView(Context context) {
        super(context);
        init();
    }

    public BigImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public BigImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }

    private Bitmap getImageBitmap() {
        Drawable drawable = getDrawable();
        if (drawable == null || !(drawable instanceof BitmapDrawable)) {
            return null;
        }
        return ((BitmapDrawable) drawable).getBitmap();
    }

    private void init() {
        setScaleType(ImageView.ScaleType.MATRIX);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x003e, code lost:
        if (r0 != 0) goto L_0x0040;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void initDrawable() {
        /*
            r10 = this;
            r0 = 0
            android.graphics.Bitmap r3 = r10.getImageBitmap()
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r10.drawables = r1
            if (r3 == 0) goto L_0x0070
            r10.isFirst = r0
            int r1 = r3.getWidth()
            r10.dWidth = r1
            int r1 = r3.getHeight()
            r10.dHeight = r1
            int r1 = r10.dWidth
            float r1 = (float) r1
            int r2 = r10.dHeight
            float r2 = (float) r2
            float r1 = r1 / r2
            r10.dRatio = r1
            r1 = 1000(0x3e8, float:1.401E-42)
            int r2 = r10.dHeight
            double r4 = (double) r2
            double r6 = (double) r1
            double r4 = r4 / r6
            double r4 = java.lang.Math.ceil(r4)
            int r4 = (int) r4
            r5 = 0
            r2 = r0
        L_0x0033:
            if (r2 >= r4) goto L_0x0070
            int r6 = r1 * r2
            int r0 = r4 + -1
            if (r2 != r0) goto L_0x0071
            int r0 = r10.dHeight     // Catch:{ Exception -> 0x005f }
            int r0 = r0 % r1
            if (r0 == 0) goto L_0x0071
        L_0x0040:
            int r7 = r10.dWidth     // Catch:{ Exception -> 0x005f }
            android.graphics.Bitmap r7 = android.graphics.Bitmap.createBitmap(r3, r5, r6, r7, r0)     // Catch:{ Exception -> 0x005f }
            android.graphics.drawable.BitmapDrawable r8 = new android.graphics.drawable.BitmapDrawable     // Catch:{ Exception -> 0x005f }
            android.content.res.Resources r9 = r10.getResources()     // Catch:{ Exception -> 0x005f }
            r8.<init>(r9, r7)     // Catch:{ Exception -> 0x005f }
            r7 = 0
            int r9 = r10.dWidth     // Catch:{ Exception -> 0x005f }
            int r0 = r0 + r6
            r8.setBounds(r7, r6, r9, r0)     // Catch:{ Exception -> 0x005f }
            java.util.ArrayList<android.graphics.drawable.Drawable> r0 = r10.drawables     // Catch:{ Exception -> 0x005f }
            r0.add(r8)     // Catch:{ Exception -> 0x005f }
            int r0 = r2 + 1
            r2 = r0
            goto L_0x0033
        L_0x005f:
            r0 = move-exception
            r1 = 1
            r10.isFirst = r1
            r1 = 0
            r10.drawables = r1
            com.qihoo360.daily.c.e r1 = com.qihoo360.daily.c.e.a()
            r1.b()
            r0.printStackTrace()
        L_0x0070:
            return
        L_0x0071:
            r0 = r1
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.widget.BigImageView.initDrawable():void");
    }

    /* access modifiers changed from: protected */
    public void initMatrix() {
        Matrix matrix = new Matrix();
        matrix.set(getImageMatrix());
        matrix.reset();
        if (this.vRatio <= this.dRatio) {
            float f = ((float) this.vWidth) / ((float) this.dWidth);
            this.maxScale = f * 3.0f;
            this.minScale = f;
            matrix.postScale(f, f);
            matrix.postTranslate(0.0f, (((float) this.vHeight) - (f * ((float) this.dHeight))) * 0.5f);
        } else {
            float f2 = ((float) this.vWidth) / ((float) this.dWidth);
            this.maxScale = f2 * 3.0f;
            this.minScale = f2;
            matrix.postScale(f2, f2);
        }
        setImageMatrix(matrix);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.isFirst) {
            initDrawable();
            initMatrix();
        }
        if (this.drawables != null && this.drawables.size() > 0) {
            Matrix imageMatrix = getImageMatrix();
            int saveCount = canvas.getSaveCount();
            canvas.save();
            if (imageMatrix != null) {
                canvas.concat(imageMatrix);
            }
            if (this.drawables != null) {
                Iterator<Drawable> it = this.drawables.iterator();
                while (it.hasNext()) {
                    it.next().draw(canvas);
                }
            }
            canvas.restoreToCount(saveCount);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        this.vWidth = getMeasuredWidth();
        this.vHeight = getMeasuredHeight();
        this.vRatio = ((float) this.vWidth) / ((float) this.vHeight);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        this.isFirst = true;
        super.onSizeChanged(i, i2, i3, i4);
    }

    public void setImageDrawable(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            super.setImageDrawable(drawable);
            initDrawable();
            initMatrix();
            return;
        }
        try {
            throw new n();
        } catch (n e) {
            e.printStackTrace();
        }
    }

    public void setImageResource(int i) {
        super.setImageResource(i);
        initDrawable();
        initMatrix();
    }

    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        initDrawable();
        initMatrix();
    }
}
