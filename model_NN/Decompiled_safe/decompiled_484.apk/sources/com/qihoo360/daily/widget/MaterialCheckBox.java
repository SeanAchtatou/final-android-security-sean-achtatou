package com.qihoo360.daily.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;
import android.widget.RelativeLayout;
import com.c.a.b;
import com.c.a.k;
import com.c.c.a;
import com.qihoo360.daily.R;

public class MaterialCheckBox extends CustomView implements Checkable {
    /* access modifiers changed from: private */
    public Ball ball;
    /* access modifiers changed from: private */
    public int ballCheckedColor = getContext().getResources().getColor(R.color.setting_checkbox_checked_ball_color);
    private Bitmap bitmap;
    /* access modifiers changed from: private */
    public boolean check = false;
    private int checkedLineColor = getContext().getResources().getColor(R.color.setting_checkbox_checked_line_color);
    /* access modifiers changed from: private */
    public volatile boolean eventCheck = false;
    /* access modifiers changed from: private */
    public OnCheckListener onCheckListener;
    boolean placedBall = false;
    /* access modifiers changed from: private */
    public boolean suddenly;
    private Canvas temp;
    private int uncheckedLineColor = getContext().getResources().getColor(R.color.setting_checkbox_disable_color);

    class Ball extends View {
        private k objectAnimator;
        float xCen;
        float xFin;
        float xIni;

        public Ball(Context context) {
            super(context);
            setBackgroundResource(R.drawable.background_switch_ball_uncheck);
        }

        private void animateCheck(int i) {
            changeBackground();
            if (MaterialCheckBox.this.suddenly) {
                if (MaterialCheckBox.this.eventCheck) {
                    a.d(this, MaterialCheckBox.this.ball.xFin);
                } else {
                    a.d(this, MaterialCheckBox.this.ball.xIni);
                }
                if (MaterialCheckBox.this.onCheckListener != null) {
                    MaterialCheckBox.this.onCheckListener.onCheck(MaterialCheckBox.this.eventCheck, MaterialCheckBox.this.suddenly);
                    return;
                }
                return;
            }
            if (MaterialCheckBox.this.eventCheck) {
                this.objectAnimator = k.a(this, "x", MaterialCheckBox.this.ball.xFin);
            } else {
                this.objectAnimator = k.a(this, "x", MaterialCheckBox.this.ball.xIni);
            }
            this.objectAnimator.b((long) i);
            this.objectAnimator.a(new b() {
                public void onAnimationCancel(com.c.a.a aVar) {
                }

                public void onAnimationEnd(com.c.a.a aVar) {
                    if (MaterialCheckBox.this.onCheckListener != null) {
                        MaterialCheckBox.this.onCheckListener.onCheck(MaterialCheckBox.this.eventCheck, MaterialCheckBox.this.suddenly);
                    }
                }

                public void onAnimationRepeat(com.c.a.a aVar) {
                }

                public void onAnimationStart(com.c.a.a aVar) {
                }
            });
            this.objectAnimator.a();
        }

        public void animate(boolean z) {
            if (z) {
                animateCheck(0);
            } else {
                animateCheck(300);
            }
        }

        public void changeBackground() {
            if (MaterialCheckBox.this.eventCheck && isEnabled()) {
                setBackgroundResource(R.drawable.background_checkbox);
                ((GradientDrawable) ((LayerDrawable) getBackground()).findDrawableByLayerId(R.id.shape_bacground)).setColor(MaterialCheckBox.this.ballCheckedColor);
            } else if (!isEnabled()) {
                setBackgroundResource(R.drawable.background_switch_ball_disable);
            } else {
                setBackgroundResource(R.drawable.background_switch_ball_uncheck);
            }
        }

        public void setEnabled(boolean z) {
            super.setEnabled(z);
            changeBackground();
        }
    }

    public interface OnCheckListener {
        void onCheck(boolean z, boolean z2);
    }

    public MaterialCheckBox(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setAttributes(attributeSet);
        setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (MaterialCheckBox.this.isEnabled()) {
                    if (MaterialCheckBox.this.check) {
                        MaterialCheckBox.this.setChecked(false);
                    } else {
                        MaterialCheckBox.this.setChecked(true);
                    }
                }
            }
        });
    }

    private void placeBall() {
        a.d(this.ball, (float) ((getHeight() / 2) - (this.ball.getWidth() / 2)));
        this.ball.xIni = a.a(this.ball);
        this.ball.xFin = (float) ((getWidth() - (getHeight() / 2)) - (this.ball.getWidth() / 2));
        this.ball.xCen = (float) ((getWidth() / 2) - (this.ball.getWidth() / 2));
        this.placedBall = true;
        this.ball.animate(this.suddenly);
    }

    public boolean isChecked() {
        return this.check;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (!this.placedBall) {
            placeBall();
            this.placedBall = true;
        }
        if (this.bitmap == null) {
            this.bitmap = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Bitmap.Config.ARGB_8888);
            this.temp = new Canvas(this.bitmap);
        }
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor((!this.eventCheck || !isEnabled()) ? isEnabled() ? this.uncheckedLineColor : this.uncheckedLineColor : this.checkedLineColor);
        paint.setStrokeWidth((float) com.qihoo360.daily.h.b.a(getContext(), 2.0f));
        this.temp.drawLine((float) (getHeight() / 2), (float) (getHeight() / 2), (float) (getWidth() - (getHeight() / 2)), (float) (getHeight() / 2), paint);
        canvas.drawBitmap(this.bitmap, 0.0f, 0.0f, new Paint());
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void setAttributes(AttributeSet attributeSet) {
        setBackgroundResource(R.drawable.background_transparent);
        setMinimumHeight(com.qihoo360.daily.h.b.a(getContext(), 48.0f));
        setMinimumWidth(com.qihoo360.daily.h.b.a(getContext(), 80.0f));
        int attributeResourceValue = attributeSet.getAttributeResourceValue("http://schemas.android.com/apk/res/android", "background", -1);
        if (attributeResourceValue != -1) {
            setBackgroundColor(getResources().getColor(attributeResourceValue));
        } else {
            int attributeIntValue = attributeSet.getAttributeIntValue("http://schemas.android.com/apk/res/android", "background", -1);
            if (attributeIntValue != -1) {
                setBackgroundColor(attributeIntValue);
            }
        }
        this.check = attributeSet.getAttributeBooleanValue("http://schemas.android.com/apk/res-auto", "check", false);
        this.eventCheck = this.check;
        this.ball = new Ball(getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(com.qihoo360.daily.h.b.a(getContext(), 20.0f), com.qihoo360.daily.h.b.a(getContext(), 20.0f));
        layoutParams.addRule(15, -1);
        this.ball.setLayoutParams(layoutParams);
        addView(this.ball);
    }

    public void setBackgroundColor(int i) {
        this.ballCheckedColor = i;
        if (isEnabled()) {
            this.beforeBackground = this.ballCheckedColor;
        }
    }

    public void setChecked(boolean z) {
        setChecked(z, false);
    }

    public void setChecked(boolean z, boolean z2) {
        this.suddenly = z2;
        invalidate();
        this.check = z;
        this.eventCheck = z;
        this.ball.animate(z2);
    }

    public void setEnabled(boolean z) {
        if (this.ball != null) {
            this.ball.setEnabled(z);
        }
        super.setEnabled(z);
    }

    public void setOncheckListener(OnCheckListener onCheckListener2) {
        this.onCheckListener = onCheckListener2;
    }

    public void toggle() {
        setChecked(!this.check);
    }
}
