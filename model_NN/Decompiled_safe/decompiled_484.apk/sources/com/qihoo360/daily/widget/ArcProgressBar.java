package com.qihoo360.daily.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import com.qihoo360.daily.widget.ProgressTextView;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ArcProgressBar extends View {
    private AnimationRunnable animationRunnable;
    private int arcDefaultColor;
    private float arcDefaultRadius;
    private float arcDefaultWidth;
    private int arcProgressColor;
    private float arcProgressRadius;
    private float arcProgressWidth;
    private ExecutorService executorAnimation;
    private ExecutorService executorProgress;
    /* access modifiers changed from: private */
    public boolean isAnimation;
    /* access modifiers changed from: private */
    public int max;
    /* access modifiers changed from: private */
    public ProgressTextView.OnProgressListener onProgressListener;
    private RectF oval;
    private Paint paint;
    /* access modifiers changed from: private */
    public int progress;
    private ProgressRunnable progressRunnable;

    class AnimationRunnable implements Runnable {
        private AnimationRunnable() {
        }

        public void run() {
            while (ArcProgressBar.this.progress <= ArcProgressBar.this.max && ArcProgressBar.this.isAnimation) {
                ArcProgressBar.this.postInvalidate();
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (ArcProgressBar.this.progress >= ArcProgressBar.this.max && ArcProgressBar.this.onProgressListener != null) {
                ArcProgressBar.this.onProgressListener.onComplete(ArcProgressBar.this);
            }
        }
    }

    class ProgressRunnable implements Runnable {
        private ProgressRunnable() {
        }

        public void run() {
            while (ArcProgressBar.this.progress <= ArcProgressBar.this.max && ArcProgressBar.this.isAnimation) {
                try {
                    Thread.sleep(30);
                    ArcProgressBar.access$212(ArcProgressBar.this, 1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ArcProgressBar(Context context) {
        this(context, null);
    }

    public ArcProgressBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ArcProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.arcDefaultColor = ViewCompat.MEASURED_STATE_MASK;
        this.arcProgressColor = ViewCompat.MEASURED_STATE_MASK;
        this.arcDefaultWidth = 1.0f;
        this.arcProgressWidth = 10.0f;
        this.arcDefaultRadius = 200.0f;
        this.arcProgressRadius = 210.0f;
        this.max = 100;
        this.isAnimation = false;
        this.executorProgress = Executors.newSingleThreadExecutor();
        this.executorAnimation = Executors.newSingleThreadExecutor();
        this.paint = new Paint();
        this.oval = new RectF();
        setBackgroundColor(0);
    }

    static /* synthetic */ int access$212(ArcProgressBar arcProgressBar, int i) {
        int i2 = arcProgressBar.progress + i;
        arcProgressBar.progress = i2;
        return i2;
    }

    public int getArcDefaultColor() {
        return this.arcDefaultColor;
    }

    public float getArcDefaultRadius() {
        return this.arcDefaultRadius;
    }

    public float getArcDefaultWidth() {
        return this.arcDefaultWidth;
    }

    public int getArcProgressColor() {
        return this.arcProgressColor;
    }

    public float getArcProgressRadius() {
        return this.arcProgressRadius;
    }

    public float getArcProgressWidth() {
        return this.arcProgressWidth;
    }

    public synchronized int getMax() {
        return this.max;
    }

    public ProgressTextView.OnProgressListener getOnProgressListener() {
        return this.onProgressListener;
    }

    public synchronized int getProgress() {
        return this.progress;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int measuredWidth = getMeasuredWidth() / 2;
        this.paint.setColor(this.arcDefaultColor);
        this.paint.setStyle(Paint.Style.STROKE);
        this.paint.setStrokeWidth(this.arcDefaultWidth);
        this.paint.setAntiAlias(true);
        if (this.oval != null) {
            this.oval.left = ((float) measuredWidth) - this.arcDefaultRadius;
            this.oval.top = ((float) measuredWidth) - this.arcDefaultRadius;
            this.oval.right = ((float) measuredWidth) + this.arcDefaultRadius;
            this.oval.bottom = ((float) measuredWidth) + this.arcDefaultRadius;
        }
        canvas.drawArc(this.oval, 135.0f, 270.0f, false, this.paint);
        this.paint.setStrokeWidth(this.arcProgressWidth);
        this.paint.setColor(this.arcProgressColor);
        if (this.oval != null) {
            this.oval.left = ((float) measuredWidth) - this.arcProgressRadius;
            this.oval.top = ((float) measuredWidth) - this.arcProgressRadius;
            this.oval.right = ((float) measuredWidth) + this.arcProgressRadius;
            this.oval.bottom = ((float) measuredWidth) + this.arcProgressRadius;
        }
        this.paint.setStrokeCap(Paint.Cap.ROUND);
        canvas.drawArc(this.oval, 135.0f, (float) ((this.progress * 270) / this.max), false, this.paint);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4 = 0;
        if (this.arcProgressRadius > 0.0f) {
            i3 = View.MeasureSpec.makeMeasureSpec((int) ((this.arcProgressRadius * 2.0f) + this.arcProgressWidth), 1073741824);
            i4 = View.MeasureSpec.makeMeasureSpec((int) ((this.arcProgressRadius * 2.0f) + this.arcProgressWidth), 1073741824);
        } else {
            i3 = 0;
        }
        super.onMeasure(i4, i3);
    }

    public void setArcDefaultColor(int i) {
        this.arcDefaultColor = i;
    }

    public void setArcDefaultRadius(float f) {
        this.arcDefaultRadius = f;
    }

    public void setArcDefaultWidth(float f) {
        this.arcDefaultWidth = f;
    }

    public void setArcProgressColor(int i) {
        this.arcProgressColor = i;
    }

    public void setArcProgressRadius(float f) {
        this.arcProgressRadius = f;
    }

    public void setArcProgressWidth(float f) {
        this.arcProgressWidth = f;
    }

    public synchronized void setMax(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("max not less than 0");
        }
        this.max = i;
    }

    public void setOnProgressListener(ProgressTextView.OnProgressListener onProgressListener2) {
        this.onProgressListener = onProgressListener2;
    }

    public synchronized void setProgress(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("progress not less than 0");
        }
        if (i > this.max) {
            i = this.max;
        }
        if (i <= this.max) {
            this.progress = i;
            postInvalidate();
        }
    }

    public void start() {
        if (!this.isAnimation && this.executorAnimation != null) {
            this.isAnimation = true;
            if (this.animationRunnable == null) {
                this.animationRunnable = new AnimationRunnable();
            }
            this.executorAnimation.execute(this.animationRunnable);
            if (this.progressRunnable == null) {
                this.progressRunnable = new ProgressRunnable();
            }
            this.executorProgress.execute(this.progressRunnable);
        }
    }

    public void stop() {
        this.isAnimation = false;
    }
}
