package com.qihoo360.daily.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ListAdapter;
import android.widget.ListView;

public class IndexableListView extends ListView {
    private GestureDetector mGestureDetector = null;
    private boolean mIsFastScrollEnabled = false;
    private IndexScroller mScroller = null;

    public IndexableListView(Context context) {
        super(context);
    }

    public IndexableListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public IndexableListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.mScroller != null) {
            this.mScroller.draw(canvas);
        }
    }

    public void hideIndexScroller() {
        if (this.mScroller != null) {
            this.mScroller.hide();
        }
    }

    public boolean isFastScrollEnabled() {
        return this.mIsFastScrollEnabled;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (this.mScroller != null) {
            this.mScroller.onSizeChanged(i, i2, i3, i4);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.mScroller != null && this.mScroller.onTouchEvent(motionEvent)) {
            return true;
        }
        if (this.mGestureDetector == null) {
            this.mGestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {
                public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
                    return super.onFling(motionEvent, motionEvent2, f, f2);
                }
            });
        }
        this.mGestureDetector.onTouchEvent(motionEvent);
        return super.onTouchEvent(motionEvent);
    }

    public void setAdapter(ListAdapter listAdapter) {
        super.setAdapter(listAdapter);
        if (this.mScroller != null) {
            this.mScroller.setAdapter(listAdapter);
        }
    }

    public void setFastScrollEnabled(boolean z) {
        this.mIsFastScrollEnabled = z;
        if (this.mIsFastScrollEnabled) {
            if (this.mScroller == null) {
                this.mScroller = new IndexScroller(getContext(), this);
            }
            this.mScroller.show();
        } else if (this.mScroller != null) {
            this.mScroller.hide();
            this.mScroller = null;
        }
    }

    public void showIndexScroller() {
        if (this.mScroller != null) {
            this.mScroller.show();
        }
    }
}
