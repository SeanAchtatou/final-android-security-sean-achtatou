package com.qihoo360.daily.widget.dragdropgrid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.qihoo360.daily.R;
import com.qihoo360.daily.model.Channel;
import com.qihoo360.daily.model.ChannelType;
import java.util.ArrayList;

public class ChannelsAdapter extends BaseAdapter {
    private ArrayList<Channel> mChannels;
    private Channel mCheckedChannel;
    private Context mContext;
    private boolean mIsEditting;
    private boolean mIsUserAdapter;

    public ChannelsAdapter(Context context, ArrayList<Channel> arrayList, Channel channel, boolean z) {
        this.mContext = context;
        this.mChannels = arrayList;
        this.mCheckedChannel = channel;
        this.mIsUserAdapter = z;
    }

    public void addChannel(Channel channel) {
        if (this.mChannels == null) {
            this.mChannels = new ArrayList<>();
        }
        this.mChannels.add(0, channel);
    }

    public void addEmptyChannel() {
        addChannel(new Channel());
    }

    public void appendChannel(Channel channel) {
        if (this.mChannels == null) {
            this.mChannels = new ArrayList<>();
        }
        this.mChannels.add(channel);
    }

    public void appendEmptyChannel() {
        appendChannel(new Channel());
    }

    public ArrayList<Channel> getChannels() {
        return this.mChannels;
    }

    public int getCount() {
        if (this.mChannels != null) {
            return this.mChannels.size();
        }
        return 0;
    }

    public int getIndex(Channel channel) {
        int indexOf;
        if (this.mChannels == null || (indexOf = this.mChannels.indexOf(channel)) < 0) {
            return 0;
        }
        return indexOf;
    }

    public Channel getItem(int i) {
        return this.mChannels.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? LayoutInflater.from(this.mContext).inflate((int) R.layout.layout_channels_item, viewGroup, false) : view;
        ChannelItemLayout channelItemLayout = (ChannelItemLayout) inflate;
        boolean z = i < 1 ? false : this.mIsEditting;
        if (!z) {
            channelItemLayout.getChannelDelView().setVisibility(8);
        } else {
            channelItemLayout.getChannelDelView().setVisibility(0);
        }
        Channel channel = this.mChannels.get(i);
        if (channel != null) {
            String alias = channel.getAlias();
            if (ChannelType.TYPE_CHANNEL_LOCAL.equals(alias)) {
                channelItemLayout.getChannelNameView().setText((int) R.string.channel_local);
            } else {
                channelItemLayout.getChannelNameView().setText(channel.getTitle());
            }
            if (this.mIsUserAdapter) {
                if (this.mCheckedChannel == null || alias == null || !alias.equals(this.mCheckedChannel.getAlias())) {
                    channelItemLayout.getChannelNameView().setSelected(false);
                } else {
                    channelItemLayout.getChannelNameView().setSelected(true);
                }
                if (z) {
                    channelItemLayout.getChannelNameView().setBackgroundResource(R.drawable.sl_channel_bg2);
                } else {
                    channelItemLayout.getChannelNameView().setBackgroundResource(R.drawable.sl_channel_bg);
                }
            } else {
                channelItemLayout.getChannelNameView().setBackgroundResource(R.drawable.sl_channel_bg);
            }
        }
        return inflate;
    }

    public boolean isEditting() {
        return this.mIsEditting;
    }

    public Channel removeChannel(int i) {
        if (this.mChannels == null || this.mChannels.size() <= i || i < 0) {
            return null;
        }
        return this.mChannels.remove(i);
    }

    public void setChannel(Channel channel) {
        this.mCheckedChannel = channel;
    }

    public void setChannel(Channel channel, int i) {
        if (i >= 0 && i < this.mChannels.size()) {
            this.mChannels.set(i, channel);
        }
    }

    public void setChannels(ArrayList<Channel> arrayList) {
        this.mChannels = arrayList;
    }

    public void setEditting(boolean z) {
        this.mIsEditting = z;
        notifyDataSetChanged();
    }

    public void switchItem(int i, int i2) {
        if (i >= 0 && i2 >= 0 && i < getCount() && i2 < getCount()) {
            this.mChannels.add(i2, this.mChannels.remove(i));
            notifyDataSetChanged();
        }
    }
}
