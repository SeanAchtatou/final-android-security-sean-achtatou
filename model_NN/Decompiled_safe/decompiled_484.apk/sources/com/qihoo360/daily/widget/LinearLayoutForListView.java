package com.qihoo360.daily.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import com.qihoo360.daily.R;

public class LinearLayoutForListView extends LinearLayout {
    private ListAdapter adapter;

    public LinearLayoutForListView(Context context) {
        super(context);
    }

    public LinearLayoutForListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @TargetApi(11)
    public LinearLayoutForListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    private void bindLinearLayout() {
        int count = this.adapter.getCount();
        if (count != 0) {
            LinearLayout.LayoutParams layoutParams = getOrientation() == 1 ? new LinearLayout.LayoutParams(-1, -2) : new LinearLayout.LayoutParams(0, -1, 1.0f);
            for (int i = 0; i < count; i++) {
                View view = this.adapter.getView(i, null, this);
                if (view != null) {
                    if (getOrientation() == 0) {
                        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.padding_medium) / 2;
                        if (i == 0) {
                            layoutParams.leftMargin = dimensionPixelSize;
                        }
                        if (i == count - 1) {
                            layoutParams.rightMargin = dimensionPixelSize;
                        }
                    }
                    addView(view, layoutParams);
                }
            }
        }
    }

    public ListAdapter getAdpater() {
        return this.adapter;
    }

    public void setAdapter(ListAdapter listAdapter) {
        this.adapter = listAdapter;
        removeAllViews();
        bindLinearLayout();
    }
}
