package com.qihoo360.daily.widget.dragdropgrid;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.qihoo360.daily.R;
import com.qihoo360.daily.model.Channel;

public class ChannelManageLayout extends LinearLayout implements AdapterView.OnItemClickListener {
    public static final int DISABLE_CHANNEL_NUM = 1;
    public static final int MOVE_TO_ADD = 2;
    public static final int MOVE_TO_REBOUNCE = 0;
    public static final int MOVE_TO_REMOVE = 1;
    private float mFirstX;
    private float mFirstY;
    /* access modifiers changed from: private */
    public boolean mIsAnimating;
    private boolean mIsEditting;
    private boolean mIsMoveAble;
    /* access modifiers changed from: private */
    public ImageView mIvMoveItem;
    private int mLastBottom;
    private int mLastLeft;
    /* access modifiers changed from: private */
    public int mLastPosition = -1;
    private int mLastRight;
    private int mLastTop;
    private float mLastX;
    private float mLastY;
    int[] mLocation = new int[2];
    /* access modifiers changed from: private */
    public MovableGridView mMoreChannelsGridView;
    /* access modifiers changed from: private */
    public MovableGridView mMyChannelsGridView;
    /* access modifiers changed from: private */
    public OnItemClickListener mOnItemClickListener;
    private OnSwichChangeListener mOnSwichChangeListener;
    private ViewConfiguration mViewConfig;
    Rect outRect = new Rect();

    public interface OnItemClickListener {
        void onItemClick(AdapterView<?> adapterView, int i, Channel channel);
    }

    public interface OnSwichChangeListener {
        void OnSwitch(int i, int i2);
    }

    public ChannelManageLayout(Context context) {
        super(context, null);
    }

    public ChannelManageLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        LayoutInflater.from(context).inflate((int) R.layout.layout_channels_manage_internal, this);
        this.mViewConfig = ViewConfiguration.get(getContext());
        this.mMoreChannelsGridView = (MovableGridView) findViewById(R.id.gv_more_channels);
        this.mMoreChannelsGridView.setOnItemClickListener(this);
        this.mMyChannelsGridView = (MovableGridView) findViewById(R.id.gv_dragable_channels);
        this.mMyChannelsGridView.setOnItemClickListener(this);
        this.mMyChannelsGridView.setDrawingCacheEnabled(true);
        this.mIvMoveItem = new ImageView(getContext());
        if (this.mIvMoveItem != null) {
            addView(this.mIvMoveItem, new LinearLayout.LayoutParams(-2, -2));
            this.mIvMoveItem.setVisibility(8);
        }
    }

    private int checkPositionWhetherNeedToSwitch(float f, float f2) {
        View childAt;
        if (this.mMyChannelsGridView == null) {
            return -1;
        }
        int childCount = this.mMyChannelsGridView.getChildCount();
        int i = 0;
        while (i < childCount) {
            if (i == this.mLastPosition || i <= 0 || (childAt = this.mMyChannelsGridView.getChildAt(i)) == null || !inViewInBounds(childAt, (int) f, (int) f2)) {
                i++;
            } else {
                View childAt2 = this.mMyChannelsGridView.getChildAt(this.mLastPosition);
                if (childAt2 != null) {
                    childAt2.setVisibility(0);
                }
                switchView(this.mLastPosition, i);
                this.mLastPosition = i;
                return i;
            }
        }
        return -1;
    }

    private boolean ensureMoveItem(View view, MovableGridView movableGridView) {
        view.destroyDrawingCache();
        view.buildDrawingCache();
        Bitmap createBitmap = Bitmap.createBitmap(view.getDrawingCache());
        if (createBitmap == null || createBitmap.isRecycled()) {
            return false;
        }
        if (createBitmap == null || createBitmap.isRecycled()) {
            this.mIvMoveItem.setImageBitmap(null);
        } else {
            this.mIvMoveItem.setImageBitmap(createBitmap);
        }
        this.mLastLeft = view.getLeft() + movableGridView.getLeft();
        this.mLastTop = view.getTop() + movableGridView.getTop();
        this.mLastRight = view.getRight() + movableGridView.getLeft();
        this.mLastBottom = view.getBottom() + movableGridView.getTop();
        this.mIvMoveItem.layout(this.mLastLeft, this.mLastTop, this.mLastRight, this.mLastBottom);
        this.mIvMoveItem.setVisibility(0);
        return true;
    }

    private int getPositionInGridView(float f, float f2) {
        if (this.mMyChannelsGridView == null) {
            return -1;
        }
        int childCount = this.mMyChannelsGridView.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = this.mMyChannelsGridView.getChildAt(i);
            if (childAt != null && inViewInBounds(childAt, (int) f, (int) f2)) {
                return i;
            }
        }
        return -1;
    }

    private boolean inViewInBounds(View view, int i, int i2) {
        view.getDrawingRect(this.outRect);
        view.getLocationOnScreen(this.mLocation);
        this.outRect.offset(this.mLocation[0], this.mLocation[1]);
        return this.outRect.contains(i, i2);
    }

    private void moveToDestination(View view, MovableGridView movableGridView, MovableGridView movableGridView2, int i) {
        moveToDestination(view, movableGridView, movableGridView2, i, null, -1);
    }

    /* access modifiers changed from: private */
    public void moveToDestination(View view, MovableGridView movableGridView, MovableGridView movableGridView2, int i, Channel channel, int i2) {
        if (view != null && movableGridView2 != null) {
            int left = this.mIvMoveItem.getLeft();
            int top = this.mIvMoveItem.getTop();
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, (float) (-(left - (view.getLeft() + movableGridView2.getLeft()))), 0.0f, (float) (-(top - (view.getTop() + movableGridView2.getTop()))));
            translateAnimation.setDuration(250);
            final int i3 = i;
            final MovableGridView movableGridView3 = movableGridView2;
            final int i4 = i2;
            final Channel channel2 = channel;
            final MovableGridView movableGridView4 = movableGridView;
            translateAnimation.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation animation) {
                    ChannelManageLayout.this.mIvMoveItem.setVisibility(8);
                    boolean unused = ChannelManageLayout.this.mIsAnimating = false;
                    switch (i3) {
                        case 0:
                            View childAt = movableGridView3.getChildAt(ChannelManageLayout.this.mLastPosition);
                            if (childAt != null) {
                                childAt.setVisibility(0);
                            }
                            int unused2 = ChannelManageLayout.this.mLastPosition = -1;
                            movableGridView3.restoreSwitch();
                            return;
                        case 1:
                            ChannelsAdapter channelsAdapter = (ChannelsAdapter) ChannelManageLayout.this.mMyChannelsGridView.getAdapter();
                            channelsAdapter.removeChannel(i4);
                            channelsAdapter.notifyDataSetChanged();
                            ChannelsAdapter channelsAdapter2 = (ChannelsAdapter) ChannelManageLayout.this.mMoreChannelsGridView.getAdapter();
                            channelsAdapter2.setChannel(channel2, 0);
                            channelsAdapter2.notifyDataSetChanged();
                            if (ChannelManageLayout.this.mOnItemClickListener != null) {
                                ChannelManageLayout.this.mOnItemClickListener.onItemClick(movableGridView4, i4, channel2);
                                return;
                            }
                            return;
                        case 2:
                            ChannelsAdapter channelsAdapter3 = (ChannelsAdapter) ChannelManageLayout.this.mMoreChannelsGridView.getAdapter();
                            channelsAdapter3.removeChannel(i4);
                            channelsAdapter3.notifyDataSetChanged();
                            ChannelsAdapter channelsAdapter4 = (ChannelsAdapter) ChannelManageLayout.this.mMyChannelsGridView.getAdapter();
                            channelsAdapter4.setChannel(channel2, channelsAdapter4.getCount() - 1);
                            channelsAdapter4.notifyDataSetChanged();
                            if (ChannelManageLayout.this.mOnItemClickListener != null) {
                                ChannelManageLayout.this.mOnItemClickListener.onItemClick(movableGridView4, i4, channel2);
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                    boolean unused = ChannelManageLayout.this.mIsAnimating = true;
                }
            });
            this.mIvMoveItem.startAnimation(translateAnimation);
        }
    }

    private void switchView(int i, int i2) {
        if (this.mOnSwichChangeListener != null) {
            this.mOnSwichChangeListener.OnSwitch(i, i2);
        }
    }

    public MovableGridView getGridView() {
        return this.mMyChannelsGridView;
    }

    public boolean isUnderEditting() {
        return this.mIsEditting;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x008d, code lost:
        if (r7.mIsMoveAble != false) goto L_0x008f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onInterceptTouchEvent(android.view.MotionEvent r8) {
        /*
            r7 = this;
            r6 = 0
            r0 = 1
            boolean r1 = super.onInterceptTouchEvent(r8)
            boolean r2 = r7.mIsAnimating
            if (r2 == 0) goto L_0x000b
        L_0x000a:
            return r1
        L_0x000b:
            float r2 = r8.getRawX()
            float r3 = r8.getRawY()
            int r4 = r8.getAction()
            switch(r4) {
                case 0: goto L_0x001f;
                case 1: goto L_0x0091;
                case 2: goto L_0x0041;
                case 3: goto L_0x0091;
                default: goto L_0x001a;
            }
        L_0x001a:
            r7.mLastX = r2
            r7.mLastY = r3
            goto L_0x000a
        L_0x001f:
            r7.mFirstX = r2
            r7.mFirstY = r3
            boolean r4 = r7.isUnderEditting()
            if (r4 == 0) goto L_0x000a
            com.qihoo360.daily.widget.dragdropgrid.MovableGridView r4 = r7.mMyChannelsGridView
            int r5 = (int) r2
            int r6 = (int) r3
            boolean r4 = r7.inViewInBounds(r4, r5, r6)
            if (r4 == 0) goto L_0x000a
            int r4 = r7.getPositionInGridView(r2, r3)
            if (r4 <= 0) goto L_0x000a
            android.view.ViewParent r4 = r7.getParent()
            r4.requestDisallowInterceptTouchEvent(r0)
            goto L_0x001a
        L_0x0041:
            float r4 = r7.mFirstX
            float r4 = r2 - r4
            float r4 = java.lang.Math.abs(r4)
            float r5 = r7.mFirstY
            float r5 = r3 - r5
            float r5 = java.lang.Math.abs(r5)
            boolean r6 = r7.isUnderEditting()
            if (r6 == 0) goto L_0x001a
            android.view.ViewConfiguration r6 = r7.mViewConfig
            int r6 = r6.getScaledTouchSlop()
            float r6 = (float) r6
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 > 0) goto L_0x006d
            android.view.ViewConfiguration r4 = r7.mViewConfig
            int r4 = r4.getScaledTouchSlop()
            float r4 = (float) r4
            int r4 = (r5 > r4 ? 1 : (r5 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x001a
        L_0x006d:
            int r4 = r7.getPositionInGridView(r2, r3)
            if (r4 <= 0) goto L_0x00a8
            com.qihoo360.daily.widget.dragdropgrid.MovableGridView r5 = r7.mMyChannelsGridView
            android.view.View r5 = r5.getChildAt(r4)
            if (r5 == 0) goto L_0x008b
            com.qihoo360.daily.widget.dragdropgrid.MovableGridView r6 = r7.mMyChannelsGridView
            boolean r6 = r7.ensureMoveItem(r5, r6)
            if (r6 == 0) goto L_0x000a
            r7.mIsMoveAble = r0
            r6 = 4
            r5.setVisibility(r6)
            r7.mLastPosition = r4
        L_0x008b:
            boolean r4 = r7.mIsMoveAble
            if (r4 == 0) goto L_0x00a8
        L_0x008f:
            r1 = r0
            goto L_0x001a
        L_0x0091:
            boolean r0 = r7.mIsMoveAble
            if (r0 == 0) goto L_0x001a
            r7.mIsMoveAble = r6
            com.qihoo360.daily.widget.dragdropgrid.MovableGridView r0 = r7.mMyChannelsGridView
            int r4 = r7.mLastPosition
            android.view.View r0 = r0.getChildAt(r4)
            com.qihoo360.daily.widget.dragdropgrid.MovableGridView r4 = r7.mMyChannelsGridView
            com.qihoo360.daily.widget.dragdropgrid.MovableGridView r5 = r7.mMyChannelsGridView
            r7.moveToDestination(r0, r4, r5, r6)
            goto L_0x001a
        L_0x00a8:
            r0 = r1
            goto L_0x008f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.widget.dragdropgrid.ChannelManageLayout.onInterceptTouchEvent(android.view.MotionEvent):boolean");
    }

    public void onItemClick(AdapterView<?> adapterView, View view, final int i, long j) {
        if (!this.mIsAnimating) {
            int id = adapterView.getId();
            if (id == R.id.gv_dragable_channels) {
                final Channel item = ((ChannelsAdapter) this.mMyChannelsGridView.getAdapter()).getItem(i);
                if (isUnderEditting() && i >= 1) {
                    ensureMoveItem(view, (MovableGridView) adapterView);
                    if (view instanceof ChannelItemLayout) {
                        ((ChannelItemLayout) view).setChannelNameEmpty();
                    }
                    ChannelsAdapter channelsAdapter = (ChannelsAdapter) this.mMoreChannelsGridView.getAdapter();
                    channelsAdapter.addEmptyChannel();
                    channelsAdapter.notifyDataSetChanged();
                    this.mMoreChannelsGridView.post(new Runnable() {
                        public void run() {
                            ChannelManageLayout.this.moveToDestination(ChannelManageLayout.this.mMoreChannelsGridView.getChildAt(0), ChannelManageLayout.this.mMyChannelsGridView, ChannelManageLayout.this.mMoreChannelsGridView, 1, item, i);
                        }
                    });
                } else if (!isUnderEditting() && this.mOnItemClickListener != null) {
                    this.mOnItemClickListener.onItemClick(adapterView, i, item);
                }
            } else if (id == R.id.gv_more_channels) {
                final Channel item2 = ((ChannelsAdapter) this.mMoreChannelsGridView.getAdapter()).getItem(i);
                ensureMoveItem(view, (MovableGridView) adapterView);
                if (view instanceof ChannelItemLayout) {
                    ((ChannelItemLayout) view).setChannelNameEmpty();
                }
                final ChannelsAdapter channelsAdapter2 = (ChannelsAdapter) this.mMyChannelsGridView.getAdapter();
                channelsAdapter2.appendEmptyChannel();
                channelsAdapter2.notifyDataSetChanged();
                this.mMyChannelsGridView.post(new Runnable() {
                    public void run() {
                        ChannelManageLayout.this.moveToDestination(ChannelManageLayout.this.mMyChannelsGridView.getChildAt(channelsAdapter2.getCount() - 1), ChannelManageLayout.this.mMoreChannelsGridView, ChannelManageLayout.this.mMyChannelsGridView, 2, item2, i);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (this.mIvMoveItem != null && this.mLastRight != 0 && this.mLastBottom != 0) {
            this.mIvMoveItem.layout(this.mLastLeft, this.mLastTop, this.mLastRight, this.mLastBottom);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.mIsMoveAble || this.mIvMoveItem == null || this.mIsAnimating) {
            return false;
        }
        float rawX = motionEvent.getRawX();
        float rawY = motionEvent.getRawY();
        switch (motionEvent.getAction()) {
            case 1:
            case 3:
                if (this.mIsMoveAble) {
                    this.mIsMoveAble = false;
                    moveToDestination(this.mMyChannelsGridView.getChildAt(this.mLastPosition), this.mMyChannelsGridView, this.mMyChannelsGridView, 0);
                    break;
                }
                break;
            case 2:
                this.mLastLeft = ((int) (rawX - this.mLastX)) + this.mLastLeft;
                this.mLastTop += (int) (rawY - this.mLastY);
                this.mLastRight = this.mLastLeft + this.mIvMoveItem.getWidth();
                this.mLastBottom = this.mLastTop + this.mIvMoveItem.getHeight();
                this.mIvMoveItem.layout(this.mLastLeft, this.mLastTop, this.mLastRight, this.mLastBottom);
                checkPositionWhetherNeedToSwitch(rawX, rawY);
                break;
        }
        this.mLastX = rawX;
        this.mLastY = rawY;
        return true;
    }

    public void setEdit(boolean z) {
        this.mIsEditting = z;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    public void setOnSwichChangeListener(OnSwichChangeListener onSwichChangeListener) {
        this.mOnSwichChangeListener = onSwichChangeListener;
    }
}
