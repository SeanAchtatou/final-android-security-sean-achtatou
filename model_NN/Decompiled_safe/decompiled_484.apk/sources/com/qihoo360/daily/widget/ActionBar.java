package com.qihoo360.daily.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.HorizontalScrollView;
import android.widget.Scroller;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.h.bj;

public class ActionBar extends HorizontalScrollView implements View.OnClickListener {
    private int bottomTextColorId;
    private int currentPosition;
    private boolean isFirstInit = true;
    private Scroller mScroller;
    private OnItemSelectedListener onItemSelectedListener;
    private ViewGroup view;

    public interface OnItemSelectedListener {
        void onReClick(int i);

        void onSelected(int i);
    }

    public ActionBar(Context context) {
        super(context);
        this.mScroller = new Scroller(context);
        this.bottomTextColorId = R.drawable.channel_tab_text_color;
    }

    public ActionBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mScroller = new Scroller(context);
        this.bottomTextColorId = R.drawable.channel_tab_text_color;
    }

    @TargetApi(11)
    public ActionBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mScroller = new Scroller(context);
        this.bottomTextColorId = R.drawable.channel_tab_text_color;
    }

    private void setChildState(int i) {
        int childCount = this.view.getChildCount();
        if (i <= childCount && i >= 0 && childCount > 0) {
            for (int i2 = 0; i2 < childCount; i2++) {
                TextView textView = (TextView) this.view.getChildAt(i2).findViewById(R.id.title);
                if (textView != null) {
                    textView.setTextColor(getResources().getColorStateList(this.bottomTextColorId));
                    if (i2 == i) {
                        textView.setSelected(true);
                    } else {
                        textView.setSelected(false);
                    }
                }
            }
        }
    }

    private void startScroll(int i) {
        View childAt = this.view.getChildAt(i);
        if (childAt != null) {
            int left = (childAt.getLeft() + (childAt.getMeasuredWidth() / 2)) - (getMeasuredWidth() / 2);
            int scrollX = getScrollX();
            if (left < 0) {
                left = 0;
            }
            int measuredWidth = this.view.getMeasuredWidth() - getMeasuredWidth();
            if (left > measuredWidth) {
                left = measuredWidth;
            }
            this.mScroller.startScroll(scrollX, 0, left - scrollX, 0, 300);
            toInvalidate();
        }
    }

    @TargetApi(16)
    private void toInvalidate() {
        if (bj.d()) {
            postInvalidateOnAnimation();
        } else {
            postInvalidate();
        }
    }

    public void computeScroll() {
        super.computeScroll();
        if (this.mScroller.computeScrollOffset()) {
            scrollTo(this.mScroller.getCurrX(), -this.mScroller.getCurrY());
            toInvalidate();
        }
    }

    public void onClick(View view2) {
        if (this.view != null) {
            int indexOfChild = this.view.indexOfChild(view2);
            if (this.currentPosition == indexOfChild) {
                if (this.onItemSelectedListener != null) {
                    this.onItemSelectedListener.onReClick(indexOfChild);
                }
            } else if (indexOfChild >= 0) {
                setCurrentPosition(indexOfChild);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.view = (ViewGroup) getChildAt(0);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                ViewParent parent = getParent();
                if (parent != null) {
                    parent.requestDisallowInterceptTouchEvent(true);
                    break;
                }
                break;
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int childCount;
        super.onLayout(z, i, i2, i3, i4);
        if (this.isFirstInit) {
            if (this.view != null && (childCount = this.view.getChildCount()) > 0) {
                for (int i5 = 0; i5 < childCount; i5++) {
                    this.view.getChildAt(i5).setOnClickListener(this);
                }
            }
            setCurrentPosition(this.currentPosition);
            this.isFirstInit = false;
        }
    }

    public void setCurrentPosition(int i) {
        if (this.view != null) {
            setChildState(i);
            if (this.onItemSelectedListener != null) {
                this.onItemSelectedListener.onSelected(i);
            }
            this.currentPosition = i;
            startScroll(i);
        }
    }

    public void setOnItemSelectedListener(OnItemSelectedListener onItemSelectedListener2) {
        this.onItemSelectedListener = onItemSelectedListener2;
    }

    public void setPosition(int i) {
        this.isFirstInit = true;
    }
}
