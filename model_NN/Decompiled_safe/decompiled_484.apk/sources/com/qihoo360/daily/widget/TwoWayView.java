package com.qihoo360.daily.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.media.TransportMediator;
import android.support.v4.util.LongSparseArray;
import android.support.v4.util.SparseArrayCompat;
import android.support.v4.view.AccessibilityDelegateCompat;
import android.support.v4.view.KeyEventCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.VelocityTrackerCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v4.widget.EdgeEffectCompat;
import android.support.v7.internal.widget.ActivityChooserView;
import android.util.AttributeSet;
import android.util.SparseBooleanArray;
import android.view.ContextMenu;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.AdapterView;
import android.widget.Checkable;
import android.widget.ListAdapter;
import android.widget.Scroller;
import com.qihoo.messenger.util.QDefine;
import com.qihoo360.daily.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TwoWayView extends AdapterView<ListAdapter> implements ViewTreeObserver.OnTouchModeChangeListener {
    private static final int CHECK_POSITION_SEARCH_DISTANCE = 20;
    private static final int INVALID_POINTER = -1;
    private static final int LAYOUT_FORCE_BOTTOM = 3;
    private static final int LAYOUT_FORCE_TOP = 1;
    private static final int LAYOUT_MOVE_SELECTION = 6;
    private static final int LAYOUT_NORMAL = 0;
    private static final int LAYOUT_SET_SELECTION = 2;
    private static final int LAYOUT_SPECIFIC = 4;
    private static final int LAYOUT_SYNC = 5;
    private static final String LOGTAG = "TwoWayView";
    private static final float MAX_SCROLL_FACTOR = 0.33f;
    private static final int MIN_SCROLL_PREVIEW_PIXELS = 10;
    private static final int NO_POSITION = -1;
    public static final int[] STATE_NOTHING = {0};
    private static final int SYNC_FIRST_POSITION = 1;
    private static final int SYNC_MAX_DURATION_MILLIS = 100;
    private static final int SYNC_SELECTED_POSITION = 0;
    private static final int TOUCH_MODE_DONE_WAITING = 2;
    private static final int TOUCH_MODE_DOWN = 0;
    private static final int TOUCH_MODE_DRAGGING = 3;
    private static final int TOUCH_MODE_FLINGING = 4;
    private static final int TOUCH_MODE_OFF = 1;
    private static final int TOUCH_MODE_ON = 0;
    private static final int TOUCH_MODE_OVERSCROLL = 5;
    private static final int TOUCH_MODE_REST = -1;
    private static final int TOUCH_MODE_TAP = 1;
    private static final int TOUCH_MODE_UNKNOWN = -1;
    private ListItemAccessibilityDelegate mAccessibilityDelegate;
    private int mActivePointerId;
    /* access modifiers changed from: private */
    public ListAdapter mAdapter;
    private boolean mAreAllItemsSelectable;
    private final ArrowScrollFocusResult mArrowScrollFocusResult;
    private boolean mBlockLayoutRequests;
    private SparseBooleanArray mCheckStates;
    LongSparseArray<Integer> mCheckedIdStates;
    private int mCheckedItemCount;
    private ChoiceMode mChoiceMode;
    private ContextMenu.ContextMenuInfo mContextMenuInfo;
    /* access modifiers changed from: private */
    public boolean mDataChanged;
    private AdapterDataSetObserver mDataSetObserver;
    private boolean mDesiredFocusableInTouchModeState;
    private boolean mDesiredFocusableState;
    private boolean mDrawSelectorOnTop;
    private View mEmptyView;
    private EdgeEffectCompat mEndEdge;
    /* access modifiers changed from: private */
    public int mFirstPosition;
    private final int mFlingVelocity;
    /* access modifiers changed from: private */
    public boolean mHasStableIds;
    private boolean mInLayout;
    private boolean mIsAttached;
    private boolean mIsChildViewEnabled;
    final boolean[] mIsScrap;
    private boolean mIsVertical;
    /* access modifiers changed from: private */
    public int mItemCount;
    private int mItemMargin;
    private boolean mItemsCanFocus;
    private int mLastAccessibilityScrollEventFromIndex;
    private int mLastAccessibilityScrollEventToIndex;
    private int mLastScrollState;
    private int mLastTouchMode;
    private float mLastTouchPos;
    /* access modifiers changed from: private */
    public int mLayoutMode;
    private final int mMaximumVelocity;
    /* access modifiers changed from: private */
    public int mMotionPosition;
    /* access modifiers changed from: private */
    public boolean mNeedSync;
    /* access modifiers changed from: private */
    public int mNextSelectedPosition;
    /* access modifiers changed from: private */
    public long mNextSelectedRowId;
    /* access modifiers changed from: private */
    public int mOldItemCount;
    private int mOldSelectedPosition;
    private long mOldSelectedRowId;
    private OnScrollListener mOnScrollListener;
    private int mOverScroll;
    private final int mOverscrollDistance;
    private CheckForKeyLongPress mPendingCheckForKeyLongPress;
    private CheckForLongPress mPendingCheckForLongPress;
    private CheckForTap mPendingCheckForTap;
    private SavedState mPendingSync;
    private PerformClick mPerformClick;
    private final RecycleBin mRecycler;
    private int mResurrectToPosition;
    private final Scroller mScroller;
    /* access modifiers changed from: private */
    public int mSelectedPosition;
    /* access modifiers changed from: private */
    public long mSelectedRowId;
    private int mSelectedStart;
    private SelectionNotifier mSelectionNotifier;
    /* access modifiers changed from: private */
    public Drawable mSelector;
    private int mSelectorPosition;
    private final Rect mSelectorRect;
    private int mSpecificStart;
    private EdgeEffectCompat mStartEdge;
    private long mSyncHeight;
    private int mSyncMode;
    private int mSyncPosition;
    private long mSyncRowId;
    private final Rect mTempRect;
    private Rect mTouchFrame;
    /* access modifiers changed from: private */
    public int mTouchMode;
    /* access modifiers changed from: private */
    public Runnable mTouchModeReset;
    private float mTouchRemainderPos;
    private final int mTouchSlop;
    private VelocityTracker mVelocityTracker;

    class AdapterDataSetObserver extends DataSetObserver {
        private Parcelable mInstanceState;

        private AdapterDataSetObserver() {
            this.mInstanceState = null;
        }

        public void onChanged() {
            boolean unused = TwoWayView.this.mDataChanged = true;
            int unused2 = TwoWayView.this.mOldItemCount = TwoWayView.this.mItemCount;
            int unused3 = TwoWayView.this.mItemCount = TwoWayView.this.getAdapter().getCount();
            if (!TwoWayView.this.mHasStableIds || this.mInstanceState == null || TwoWayView.this.mOldItemCount != 0 || TwoWayView.this.mItemCount <= 0) {
                TwoWayView.this.rememberSyncState();
            } else {
                TwoWayView.this.onRestoreInstanceState(this.mInstanceState);
                this.mInstanceState = null;
            }
            TwoWayView.this.checkFocus();
            TwoWayView.this.requestLayout();
        }

        public void onInvalidated() {
            boolean unused = TwoWayView.this.mDataChanged = true;
            if (TwoWayView.this.mHasStableIds) {
                this.mInstanceState = TwoWayView.this.onSaveInstanceState();
            }
            int unused2 = TwoWayView.this.mOldItemCount = TwoWayView.this.mItemCount;
            int unused3 = TwoWayView.this.mItemCount = 0;
            int unused4 = TwoWayView.this.mSelectedPosition = -1;
            long unused5 = TwoWayView.this.mSelectedRowId = Long.MIN_VALUE;
            int unused6 = TwoWayView.this.mNextSelectedPosition = -1;
            long unused7 = TwoWayView.this.mNextSelectedRowId = Long.MIN_VALUE;
            boolean unused8 = TwoWayView.this.mNeedSync = false;
            TwoWayView.this.checkFocus();
            TwoWayView.this.requestLayout();
        }
    }

    class ArrowScrollFocusResult {
        private int mAmountToScroll;
        private int mSelectedPosition;

        private ArrowScrollFocusResult() {
        }

        public int getAmountToScroll() {
            return this.mAmountToScroll;
        }

        public int getSelectedPosition() {
            return this.mSelectedPosition;
        }

        /* access modifiers changed from: package-private */
        public void populate(int i, int i2) {
            this.mSelectedPosition = i;
            this.mAmountToScroll = i2;
        }
    }

    class CheckForKeyLongPress extends WindowRunnnable implements Runnable {
        private CheckForKeyLongPress() {
            super();
        }

        public void run() {
            if (TwoWayView.this.isPressed() && TwoWayView.this.mSelectedPosition >= 0) {
                View childAt = TwoWayView.this.getChildAt(TwoWayView.this.mSelectedPosition - TwoWayView.this.mFirstPosition);
                if (!TwoWayView.this.mDataChanged) {
                    if (sameWindow() ? TwoWayView.this.performLongPress(childAt, TwoWayView.this.mSelectedPosition, TwoWayView.this.mSelectedRowId) : false) {
                        TwoWayView.this.setPressed(false);
                        childAt.setPressed(false);
                        return;
                    }
                    return;
                }
                TwoWayView.this.setPressed(false);
                if (childAt != null) {
                    childAt.setPressed(false);
                }
            }
        }
    }

    class CheckForLongPress extends WindowRunnnable implements Runnable {
        private CheckForLongPress() {
            super();
        }

        public void run() {
            int access$3600 = TwoWayView.this.mMotionPosition;
            View childAt = TwoWayView.this.getChildAt(access$3600 - TwoWayView.this.mFirstPosition);
            if (childAt != null) {
                if ((!sameWindow() || TwoWayView.this.mDataChanged) ? false : TwoWayView.this.performLongPress(childAt, access$3600, TwoWayView.this.mAdapter.getItemId(TwoWayView.this.mMotionPosition))) {
                    int unused = TwoWayView.this.mTouchMode = -1;
                    TwoWayView.this.setPressed(false);
                    childAt.setPressed(false);
                    return;
                }
                int unused2 = TwoWayView.this.mTouchMode = 2;
            }
        }
    }

    final class CheckForTap implements Runnable {
        private CheckForTap() {
        }

        public void run() {
            Drawable current;
            if (TwoWayView.this.mTouchMode == 0) {
                int unused = TwoWayView.this.mTouchMode = 1;
                View childAt = TwoWayView.this.getChildAt(TwoWayView.this.mMotionPosition - TwoWayView.this.mFirstPosition);
                if (childAt != null && !childAt.hasFocusable()) {
                    int unused2 = TwoWayView.this.mLayoutMode = 0;
                    if (!TwoWayView.this.mDataChanged) {
                        TwoWayView.this.setPressed(true);
                        childAt.setPressed(true);
                        TwoWayView.this.layoutChildren();
                        TwoWayView.this.positionSelector(TwoWayView.this.mMotionPosition, childAt);
                        TwoWayView.this.refreshDrawableState();
                        TwoWayView.this.positionSelector(TwoWayView.this.mMotionPosition, childAt);
                        TwoWayView.this.refreshDrawableState();
                        boolean isLongClickable = TwoWayView.this.isLongClickable();
                        if (!(TwoWayView.this.mSelector == null || (current = TwoWayView.this.mSelector.getCurrent()) == null || !(current instanceof TransitionDrawable))) {
                            if (isLongClickable) {
                                ((TransitionDrawable) current).startTransition(ViewConfiguration.getLongPressTimeout());
                            } else {
                                ((TransitionDrawable) current).resetTransition();
                            }
                        }
                        if (isLongClickable) {
                            TwoWayView.this.triggerCheckForLongPress();
                        } else {
                            int unused3 = TwoWayView.this.mTouchMode = 2;
                        }
                    } else {
                        int unused4 = TwoWayView.this.mTouchMode = 2;
                    }
                }
            }
        }
    }

    public enum ChoiceMode {
        NONE,
        SINGLE,
        MULTIPLE
    }

    public class LayoutParams extends ViewGroup.LayoutParams {
        boolean forceAdd;
        long id = -1;
        int scrappedFromPosition;
        int viewType;

        public LayoutParams(int i, int i2) {
            super(i, i2);
            if (this.width == -1) {
                this.width = -2;
            }
            if (this.height == -1) {
                this.height = -2;
            }
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            if (this.width == -1) {
                this.width = -1;
            }
            if (this.height == -1) {
                this.height = -2;
            }
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            if (this.width == -1) {
                this.width = -2;
            }
            if (this.height == -1) {
                this.height = -2;
            }
        }
    }

    class ListItemAccessibilityDelegate extends AccessibilityDelegateCompat {
        private ListItemAccessibilityDelegate() {
        }

        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat);
            int positionForView = TwoWayView.this.getPositionForView(view);
            ListAdapter adapter = TwoWayView.this.getAdapter();
            if (positionForView != -1 && adapter != null && TwoWayView.this.isEnabled() && adapter.isEnabled(positionForView)) {
                if (positionForView == TwoWayView.this.getSelectedItemPosition()) {
                    accessibilityNodeInfoCompat.setSelected(true);
                    accessibilityNodeInfoCompat.addAction(8);
                } else {
                    accessibilityNodeInfoCompat.addAction(4);
                }
                if (TwoWayView.this.isClickable()) {
                    accessibilityNodeInfoCompat.addAction(16);
                    accessibilityNodeInfoCompat.setClickable(true);
                }
                if (TwoWayView.this.isLongClickable()) {
                    accessibilityNodeInfoCompat.addAction(32);
                    accessibilityNodeInfoCompat.setLongClickable(true);
                }
            }
        }

        public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
            if (super.performAccessibilityAction(view, i, bundle)) {
                return true;
            }
            int positionForView = TwoWayView.this.getPositionForView(view);
            ListAdapter adapter = TwoWayView.this.getAdapter();
            if (positionForView == -1 || adapter == null) {
                return false;
            }
            if (!TwoWayView.this.isEnabled() || !adapter.isEnabled(positionForView)) {
                return false;
            }
            long itemIdAtPosition = TwoWayView.this.getItemIdAtPosition(positionForView);
            switch (i) {
                case 4:
                    if (TwoWayView.this.getSelectedItemPosition() == positionForView) {
                        return false;
                    }
                    TwoWayView.this.setSelection(positionForView);
                    return true;
                case 8:
                    if (TwoWayView.this.getSelectedItemPosition() != positionForView) {
                        return false;
                    }
                    TwoWayView.this.setSelection(-1);
                    return true;
                case 16:
                    return TwoWayView.this.isClickable() && TwoWayView.this.performItemClick(view, positionForView, itemIdAtPosition);
                case 32:
                    return TwoWayView.this.isLongClickable() && TwoWayView.this.performLongPress(view, positionForView, itemIdAtPosition);
                default:
                    return false;
            }
        }
    }

    public interface OnScrollListener {
        public static final int SCROLL_STATE_FLING = 2;
        public static final int SCROLL_STATE_IDLE = 0;
        public static final int SCROLL_STATE_TOUCH_SCROLL = 1;

        void onScroll(TwoWayView twoWayView, int i, int i2, int i3);

        void onScrollStateChanged(TwoWayView twoWayView, int i);
    }

    public enum Orientation {
        HORIZONTAL,
        VERTICAL
    }

    class PerformClick extends WindowRunnnable implements Runnable {
        int mClickMotionPosition;

        private PerformClick() {
            super();
        }

        public void run() {
            View childAt;
            if (!TwoWayView.this.mDataChanged) {
                ListAdapter access$1400 = TwoWayView.this.mAdapter;
                int i = this.mClickMotionPosition;
                if (access$1400 != null && TwoWayView.this.mItemCount > 0 && i != -1 && i < access$1400.getCount() && sameWindow() && (childAt = TwoWayView.this.getChildAt(i - TwoWayView.this.mFirstPosition)) != null) {
                    TwoWayView.this.performItemClick(childAt, i, access$1400.getItemId(i));
                }
            }
        }
    }

    class RecycleBin {
        private View[] mActiveViews = new View[0];
        private ArrayList<View> mCurrentScrap;
        private int mFirstActivePosition;
        /* access modifiers changed from: private */
        public RecyclerListener mRecyclerListener;
        private ArrayList<View>[] mScrapViews;
        private SparseArrayCompat<View> mTransientStateViews;
        private int mViewTypeCount;

        RecycleBin() {
        }

        private void pruneScrapViews() {
            int i = 0;
            int length = this.mActiveViews.length;
            int i2 = this.mViewTypeCount;
            ArrayList<View>[] arrayListArr = this.mScrapViews;
            for (int i3 = 0; i3 < i2; i3++) {
                ArrayList<View> arrayList = arrayListArr[i3];
                int size = arrayList.size();
                int i4 = size - length;
                int i5 = size - 1;
                int i6 = 0;
                while (i6 < i4) {
                    TwoWayView.this.removeDetachedView(arrayList.remove(i5), false);
                    i6++;
                    i5--;
                }
            }
            if (this.mTransientStateViews != null) {
                while (i < this.mTransientStateViews.size()) {
                    if (!ViewCompat.hasTransientState(this.mTransientStateViews.valueAt(i))) {
                        this.mTransientStateViews.removeAt(i);
                        i--;
                    }
                    i++;
                }
            }
        }

        /* access modifiers changed from: package-private */
        @TargetApi(14)
        public void addScrapView(View view, int i) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            if (layoutParams != null) {
                layoutParams.scrappedFromPosition = i;
                int i2 = layoutParams.viewType;
                boolean hasTransientState = ViewCompat.hasTransientState(view);
                if (shouldRecycleViewType(i2) && !hasTransientState) {
                    if (this.mViewTypeCount == 1) {
                        this.mCurrentScrap.add(view);
                    } else {
                        this.mScrapViews[i2].add(view);
                    }
                    if (Build.VERSION.SDK_INT >= 14) {
                        view.setAccessibilityDelegate(null);
                    }
                    if (this.mRecyclerListener != null) {
                        this.mRecyclerListener.onMovedToScrapHeap(view);
                    }
                } else if (hasTransientState) {
                    if (this.mTransientStateViews == null) {
                        this.mTransientStateViews = new SparseArrayCompat<>();
                    }
                    this.mTransientStateViews.put(i, view);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void clear() {
            if (this.mViewTypeCount == 1) {
                ArrayList<View> arrayList = this.mCurrentScrap;
                int size = arrayList.size();
                for (int i = 0; i < size; i++) {
                    TwoWayView.this.removeDetachedView(arrayList.remove((size - 1) - i), false);
                }
            } else {
                int i2 = this.mViewTypeCount;
                for (int i3 = 0; i3 < i2; i3++) {
                    ArrayList<View> arrayList2 = this.mScrapViews[i3];
                    int size2 = arrayList2.size();
                    for (int i4 = 0; i4 < size2; i4++) {
                        TwoWayView.this.removeDetachedView(arrayList2.remove((size2 - 1) - i4), false);
                    }
                }
            }
            if (this.mTransientStateViews != null) {
                this.mTransientStateViews.clear();
            }
        }

        /* access modifiers changed from: package-private */
        public void clearTransientStateViews() {
            if (this.mTransientStateViews != null) {
                this.mTransientStateViews.clear();
            }
        }

        /* access modifiers changed from: package-private */
        public void fillActiveViews(int i, int i2) {
            if (this.mActiveViews.length < i) {
                this.mActiveViews = new View[i];
            }
            this.mFirstActivePosition = i2;
            View[] viewArr = this.mActiveViews;
            for (int i3 = 0; i3 < i; i3++) {
                viewArr[i3] = TwoWayView.this.getChildAt(i3);
            }
        }

        /* access modifiers changed from: package-private */
        public View getActiveView(int i) {
            int i2 = i - this.mFirstActivePosition;
            View[] viewArr = this.mActiveViews;
            if (i2 < 0 || i2 >= viewArr.length) {
                return null;
            }
            View view = viewArr[i2];
            viewArr[i2] = null;
            return view;
        }

        /* access modifiers changed from: package-private */
        public View getScrapView(int i) {
            if (this.mViewTypeCount == 1) {
                return retrieveFromScrap(this.mCurrentScrap, i);
            }
            int itemViewType = TwoWayView.this.mAdapter.getItemViewType(i);
            if (itemViewType < 0 || itemViewType >= this.mScrapViews.length) {
                return null;
            }
            return retrieveFromScrap(this.mScrapViews[itemViewType], i);
        }

        /* access modifiers changed from: package-private */
        public View getTransientStateView(int i) {
            int indexOfKey;
            if (this.mTransientStateViews == null || (indexOfKey = this.mTransientStateViews.indexOfKey(i)) < 0) {
                return null;
            }
            View valueAt = this.mTransientStateViews.valueAt(indexOfKey);
            this.mTransientStateViews.removeAt(indexOfKey);
            return valueAt;
        }

        public void markChildrenDirty() {
            if (this.mViewTypeCount == 1) {
                ArrayList<View> arrayList = this.mCurrentScrap;
                int size = arrayList.size();
                for (int i = 0; i < size; i++) {
                    arrayList.get(i).forceLayout();
                }
            } else {
                int i2 = this.mViewTypeCount;
                for (int i3 = 0; i3 < i2; i3++) {
                    Iterator<View> it = this.mScrapViews[i3].iterator();
                    while (it.hasNext()) {
                        it.next().forceLayout();
                    }
                }
            }
            if (this.mTransientStateViews != null) {
                int size2 = this.mTransientStateViews.size();
                for (int i4 = 0; i4 < size2; i4++) {
                    this.mTransientStateViews.valueAt(i4).forceLayout();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void reclaimScrapViews(List<View> list) {
            if (this.mViewTypeCount == 1) {
                list.addAll(this.mCurrentScrap);
                return;
            }
            int i = this.mViewTypeCount;
            ArrayList<View>[] arrayListArr = this.mScrapViews;
            for (int i2 = 0; i2 < i; i2++) {
                list.addAll(arrayListArr[i2]);
            }
        }

        /* access modifiers changed from: package-private */
        public View retrieveFromScrap(ArrayList<View> arrayList, int i) {
            int size = arrayList.size();
            if (size <= 0) {
                return null;
            }
            for (int i2 = 0; i2 < size; i2++) {
                View view = arrayList.get(i2);
                if (((LayoutParams) view.getLayoutParams()).scrappedFromPosition == i) {
                    arrayList.remove(i2);
                    return view;
                }
            }
            return arrayList.remove(size - 1);
        }

        /* access modifiers changed from: package-private */
        @TargetApi(14)
        public void scrapActiveViews() {
            View[] viewArr = this.mActiveViews;
            boolean z = this.mViewTypeCount > 1;
            ArrayList<View> arrayList = this.mCurrentScrap;
            for (int length = viewArr.length - 1; length >= 0; length--) {
                View view = viewArr[length];
                if (view != null) {
                    LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
                    int i = layoutParams.viewType;
                    viewArr[length] = null;
                    boolean hasTransientState = ViewCompat.hasTransientState(view);
                    if (shouldRecycleViewType(i) && !hasTransientState) {
                        if (z) {
                            arrayList = this.mScrapViews[i];
                        }
                        layoutParams.scrappedFromPosition = this.mFirstActivePosition + length;
                        arrayList.add(view);
                        if (Build.VERSION.SDK_INT >= 14) {
                            view.setAccessibilityDelegate(null);
                        }
                        if (this.mRecyclerListener != null) {
                            this.mRecyclerListener.onMovedToScrapHeap(view);
                        }
                    } else if (hasTransientState) {
                        TwoWayView.this.removeDetachedView(view, false);
                        if (this.mTransientStateViews == null) {
                            this.mTransientStateViews = new SparseArrayCompat<>();
                        }
                        this.mTransientStateViews.put(this.mFirstActivePosition + length, view);
                    }
                }
            }
            pruneScrapViews();
        }

        public void setViewTypeCount(int i) {
            if (i < 1) {
                throw new IllegalArgumentException("Can't have a viewTypeCount < 1");
            }
            ArrayList<View>[] arrayListArr = new ArrayList[i];
            for (int i2 = 0; i2 < i; i2++) {
                arrayListArr[i2] = new ArrayList<>();
            }
            this.mViewTypeCount = i;
            this.mCurrentScrap = arrayListArr[0];
            this.mScrapViews = arrayListArr;
        }

        public boolean shouldRecycleViewType(int i) {
            return i >= 0;
        }
    }

    public interface RecyclerListener {
        void onMovedToScrapHeap(View view);
    }

    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };
        LongSparseArray<Integer> checkIdState;
        SparseBooleanArray checkState;
        int checkedItemCount;
        long firstId;
        int height;
        int position;
        long selectedId;
        int viewStart;

        private SavedState(Parcel parcel) {
            super(parcel);
            this.selectedId = parcel.readLong();
            this.firstId = parcel.readLong();
            this.viewStart = parcel.readInt();
            this.position = parcel.readInt();
            this.height = parcel.readInt();
            this.checkedItemCount = parcel.readInt();
            this.checkState = parcel.readSparseBooleanArray();
            int readInt = parcel.readInt();
            if (readInt > 0) {
                this.checkIdState = new LongSparseArray<>();
                for (int i = 0; i < readInt; i++) {
                    this.checkIdState.put(parcel.readLong(), Integer.valueOf(parcel.readInt()));
                }
            }
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public int getPosition() {
            return this.position;
        }

        public String toString() {
            return "TwoWayView.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " selectedId=" + this.selectedId + " firstId=" + this.firstId + " viewStart=" + this.viewStart + " height=" + this.height + " position=" + this.position + " checkState=" + this.checkState + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeLong(this.selectedId);
            parcel.writeLong(this.firstId);
            parcel.writeInt(this.viewStart);
            parcel.writeInt(this.position);
            parcel.writeInt(this.height);
            parcel.writeInt(this.checkedItemCount);
            parcel.writeSparseBooleanArray(this.checkState);
            int size = this.checkIdState != null ? this.checkIdState.size() : 0;
            parcel.writeInt(size);
            for (int i2 = 0; i2 < size; i2++) {
                parcel.writeLong(this.checkIdState.keyAt(i2));
                parcel.writeInt(this.checkIdState.valueAt(i2).intValue());
            }
        }
    }

    class SelectionNotifier implements Runnable {
        private SelectionNotifier() {
        }

        public void run() {
            if (!TwoWayView.this.mDataChanged) {
                TwoWayView.this.fireOnSelected();
                TwoWayView.this.performAccessibilityActionsOnSelected();
            } else if (TwoWayView.this.mAdapter != null) {
                TwoWayView.this.post(this);
            }
        }
    }

    class WindowRunnnable {
        private int mOriginalAttachCount;

        private WindowRunnnable() {
        }

        public void rememberWindowAttachCount() {
            this.mOriginalAttachCount = TwoWayView.this.getWindowAttachCount();
        }

        public boolean sameWindow() {
            return TwoWayView.this.hasWindowFocus() && TwoWayView.this.getWindowAttachCount() == this.mOriginalAttachCount;
        }
    }

    public TwoWayView(Context context) {
        this(context, null);
    }

    public TwoWayView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public TwoWayView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mIsScrap = new boolean[1];
        this.mNeedSync = false;
        this.mVelocityTracker = null;
        this.mLayoutMode = 0;
        this.mTouchMode = -1;
        this.mLastTouchMode = -1;
        this.mIsAttached = false;
        this.mContextMenuInfo = null;
        this.mOnScrollListener = null;
        this.mLastScrollState = 0;
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        this.mTouchSlop = viewConfiguration.getScaledTouchSlop();
        this.mMaximumVelocity = viewConfiguration.getScaledMaximumFlingVelocity();
        this.mFlingVelocity = viewConfiguration.getScaledMinimumFlingVelocity();
        this.mOverscrollDistance = getScaledOverscrollDistance(viewConfiguration);
        this.mOverScroll = 0;
        this.mScroller = new Scroller(context);
        this.mIsVertical = true;
        this.mItemsCanFocus = false;
        this.mTempRect = new Rect();
        this.mArrowScrollFocusResult = new ArrowScrollFocusResult();
        this.mSelectorPosition = -1;
        this.mSelectorRect = new Rect();
        this.mSelectedStart = 0;
        this.mResurrectToPosition = -1;
        this.mSelectedStart = 0;
        this.mNextSelectedPosition = -1;
        this.mNextSelectedRowId = Long.MIN_VALUE;
        this.mSelectedPosition = -1;
        this.mSelectedRowId = Long.MIN_VALUE;
        this.mOldSelectedPosition = -1;
        this.mOldSelectedRowId = Long.MIN_VALUE;
        this.mChoiceMode = ChoiceMode.NONE;
        this.mCheckedItemCount = 0;
        this.mCheckedIdStates = null;
        this.mCheckStates = null;
        this.mRecycler = new RecycleBin();
        this.mDataSetObserver = null;
        this.mAreAllItemsSelectable = true;
        this.mStartEdge = null;
        this.mEndEdge = null;
        setClickable(true);
        setFocusableInTouchMode(true);
        setWillNotDraw(false);
        setAlwaysDrawnWithCacheEnabled(false);
        setWillNotDraw(false);
        setClipToPadding(false);
        ViewCompat.setOverScrollMode(this, 1);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.TwoWayView, i, 0);
        this.mDrawSelectorOnTop = obtainStyledAttributes.getBoolean(2, false);
        Drawable drawable = obtainStyledAttributes.getDrawable(1);
        if (drawable != null) {
            setSelector(drawable);
        }
        int i2 = obtainStyledAttributes.getInt(0, -1);
        if (i2 >= 0) {
            setOrientation(Orientation.values()[i2]);
        }
        int i3 = obtainStyledAttributes.getInt(3, -1);
        if (i3 >= 0) {
            setChoiceMode(ChoiceMode.values()[i3]);
        }
        obtainStyledAttributes.recycle();
    }

    private void adjustViewsStartOrEnd() {
        int i = 0;
        if (getChildCount() != 0) {
            View childAt = getChildAt(0);
            int top = this.mIsVertical ? (childAt.getTop() - getPaddingTop()) - this.mItemMargin : (childAt.getLeft() - getPaddingLeft()) - this.mItemMargin;
            if (top >= 0) {
                i = top;
            }
            if (i != 0) {
                offsetChildren(-i);
            }
        }
    }

    private int amountToScroll(int i, int i2) {
        forceValidFocusDirection(i);
        int childCount = getChildCount();
        if (i == 130 || i == 66) {
            int endEdge = getEndEdge();
            int i3 = childCount - 1;
            if (i2 != -1) {
                i3 = i2 - this.mFirstPosition;
            }
            int i4 = this.mFirstPosition + i3;
            View childAt = getChildAt(i3);
            int arrowScrollPreviewLength = i4 < this.mItemCount + -1 ? endEdge - getArrowScrollPreviewLength() : endEdge;
            int childStartEdge = getChildStartEdge(childAt);
            int childEndEdge = getChildEndEdge(childAt);
            if (childEndEdge <= arrowScrollPreviewLength) {
                return 0;
            }
            if (i2 != -1 && arrowScrollPreviewLength - childStartEdge >= getMaxScrollAmount()) {
                return 0;
            }
            int i5 = childEndEdge - arrowScrollPreviewLength;
            if (this.mFirstPosition + childCount == this.mItemCount) {
                i5 = Math.min(i5, getChildEndEdge(getChildAt(childCount - 1)) - endEdge);
            }
            return Math.min(i5, getMaxScrollAmount());
        }
        int startEdge = getStartEdge();
        int i6 = i2 != -1 ? i2 - this.mFirstPosition : 0;
        int i7 = this.mFirstPosition + i6;
        View childAt2 = getChildAt(i6);
        int arrowScrollPreviewLength2 = i7 > 0 ? getArrowScrollPreviewLength() + startEdge : startEdge;
        int childStartEdge2 = getChildStartEdge(childAt2);
        int childEndEdge2 = getChildEndEdge(childAt2);
        if (childStartEdge2 >= arrowScrollPreviewLength2) {
            return 0;
        }
        if (i2 != -1 && childEndEdge2 - arrowScrollPreviewLength2 >= getMaxScrollAmount()) {
            return 0;
        }
        int i8 = arrowScrollPreviewLength2 - childStartEdge2;
        if (this.mFirstPosition == 0) {
            i8 = Math.min(i8, startEdge - getChildStartEdge(getChildAt(0)));
        }
        return Math.min(i8, getMaxScrollAmount());
    }

    private int amountToScrollToNewFocus(int i, View view, int i2) {
        forceValidFocusDirection(i);
        view.getDrawingRect(this.mTempRect);
        offsetDescendantRectToMyCoords(view, this.mTempRect);
        if (i == 33 || i == 17) {
            int startEdge = getStartEdge();
            int i3 = this.mIsVertical ? this.mTempRect.top : this.mTempRect.left;
            if (i3 >= startEdge) {
                return 0;
            }
            int i4 = startEdge - i3;
            return i2 > 0 ? i4 + getArrowScrollPreviewLength() : i4;
        }
        int endEdge = getEndEdge();
        int i5 = this.mIsVertical ? this.mTempRect.bottom : this.mTempRect.right;
        if (i5 <= endEdge) {
            return 0;
        }
        int i6 = i5 - endEdge;
        return i2 < this.mItemCount + -1 ? i6 + getArrowScrollPreviewLength() : i6;
    }

    /* JADX INFO: finally extract failed */
    private boolean arrowScroll(int i) {
        forceValidFocusDirection(i);
        try {
            this.mInLayout = true;
            boolean arrowScrollImpl = arrowScrollImpl(i);
            if (arrowScrollImpl) {
                playSoundEffect(SoundEffectConstants.getContantForFocusDirection(i));
            }
            this.mInLayout = false;
            return arrowScrollImpl;
        } catch (Throwable th) {
            this.mInLayout = false;
            throw th;
        }
    }

    private ArrowScrollFocusResult arrowScrollFocused(int i) {
        int i2;
        View findNextFocusFromRect;
        boolean z = false;
        forceValidFocusDirection(i);
        View selectedView = getSelectedView();
        if (selectedView == null || !selectedView.hasFocus()) {
            if (i == 130 || i == 66) {
                int startEdge = getStartEdge();
                i2 = Math.max(selectedView != null ? this.mIsVertical ? selectedView.getTop() : selectedView.getLeft() : startEdge, startEdge);
            } else {
                int endEdge = getEndEdge();
                i2 = Math.min(selectedView != null ? getChildEndEdge(selectedView) : endEdge, endEdge);
            }
            int i3 = this.mIsVertical ? 0 : i2;
            if (!this.mIsVertical) {
                i2 = 0;
            }
            this.mTempRect.set(i3, i2, i3, i2);
            findNextFocusFromRect = FocusFinder.getInstance().findNextFocusFromRect(this, this.mTempRect, i);
        } else {
            findNextFocusFromRect = FocusFinder.getInstance().findNextFocus(this, selectedView.findFocus(), i);
        }
        if (findNextFocusFromRect != null) {
            int positionOfNewFocus = positionOfNewFocus(findNextFocusFromRect);
            if (!(this.mSelectedPosition == -1 || positionOfNewFocus == this.mSelectedPosition)) {
                int lookForSelectablePositionOnScreen = lookForSelectablePositionOnScreen(i);
                boolean z2 = i == 130 || i == 66;
                if (i == 33 || i == 17) {
                    z = true;
                }
                if (lookForSelectablePositionOnScreen != -1 && ((z2 && lookForSelectablePositionOnScreen < positionOfNewFocus) || (z && lookForSelectablePositionOnScreen > positionOfNewFocus))) {
                    return null;
                }
            }
            int amountToScrollToNewFocus = amountToScrollToNewFocus(i, findNextFocusFromRect, positionOfNewFocus);
            int maxScrollAmount = getMaxScrollAmount();
            if (amountToScrollToNewFocus < maxScrollAmount) {
                findNextFocusFromRect.requestFocus(i);
                this.mArrowScrollFocusResult.populate(positionOfNewFocus, amountToScrollToNewFocus);
                return this.mArrowScrollFocusResult;
            } else if (distanceToView(findNextFocusFromRect) < maxScrollAmount) {
                findNextFocusFromRect.requestFocus(i);
                this.mArrowScrollFocusResult.populate(positionOfNewFocus, maxScrollAmount);
                return this.mArrowScrollFocusResult;
            }
        }
        return null;
    }

    private boolean arrowScrollImpl(int i) {
        View view;
        int i2;
        View view2;
        View focusedChild;
        forceValidFocusDirection(i);
        if (getChildCount() <= 0) {
            return false;
        }
        View selectedView = getSelectedView();
        int i3 = this.mSelectedPosition;
        int lookForSelectablePositionOnScreen = lookForSelectablePositionOnScreen(i);
        int amountToScroll = amountToScroll(i, lookForSelectablePositionOnScreen);
        ArrowScrollFocusResult arrowScrollFocused = this.mItemsCanFocus ? arrowScrollFocused(i) : null;
        if (arrowScrollFocused != null) {
            lookForSelectablePositionOnScreen = arrowScrollFocused.getSelectedPosition();
            amountToScroll = arrowScrollFocused.getAmountToScroll();
        }
        boolean z = arrowScrollFocused != null;
        if (lookForSelectablePositionOnScreen != -1) {
            handleNewSelectionChange(selectedView, i, lookForSelectablePositionOnScreen, arrowScrollFocused != null);
            setSelectedPositionInt(lookForSelectablePositionOnScreen);
            setNextSelectedPositionInt(lookForSelectablePositionOnScreen);
            view = getSelectedView();
            if (this.mItemsCanFocus && arrowScrollFocused == null && (focusedChild = getFocusedChild()) != null) {
                focusedChild.clearFocus();
            }
            checkSelectionChanged();
            z = true;
            i2 = lookForSelectablePositionOnScreen;
        } else {
            int i4 = i3;
            view = selectedView;
            i2 = i4;
        }
        if (amountToScroll > 0) {
            scrollListItemsBy((i == 33 || i == 17) ? amountToScroll : -amountToScroll);
            z = true;
        }
        if (this.mItemsCanFocus && arrowScrollFocused == null && view != null && view.hasFocus()) {
            View findFocus = view.findFocus();
            if (!isViewAncestorOf(findFocus, this) || distanceToView(findFocus) > 0) {
                findFocus.clearFocus();
            }
        }
        if (lookForSelectablePositionOnScreen != -1 || view == null || isViewAncestorOf(view, this)) {
            view2 = view;
        } else {
            hideSelector();
            this.mResurrectToPosition = -1;
            view2 = null;
        }
        if (!z) {
            return false;
        }
        if (view2 != null) {
            positionSelector(i2, view2);
            this.mSelectedStart = view2.getTop();
        }
        if (!awakenScrollbarsInternal()) {
            invalidate();
        }
        invokeOnItemScrollListener();
        return true;
    }

    @TargetApi(5)
    private boolean awakenScrollbarsInternal() {
        return Build.VERSION.SDK_INT >= 5 && super.awakenScrollBars();
    }

    private void cancelCheckForLongPress() {
        if (this.mPendingCheckForLongPress != null) {
            removeCallbacks(this.mPendingCheckForLongPress);
        }
    }

    private void cancelCheckForTap() {
        if (this.mPendingCheckForTap != null) {
            removeCallbacks(this.mPendingCheckForTap);
        }
    }

    /* access modifiers changed from: private */
    public void checkFocus() {
        boolean z = true;
        ListAdapter adapter = getAdapter();
        boolean z2 = adapter != null && adapter.getCount() > 0;
        super.setFocusableInTouchMode(z2 && this.mDesiredFocusableInTouchModeState);
        if (!z2 || !this.mDesiredFocusableState) {
            z = false;
        }
        super.setFocusable(z);
        if (this.mEmptyView != null) {
            updateEmptyStatus();
        }
    }

    private void checkSelectionChanged() {
        if (this.mSelectedPosition != this.mOldSelectedPosition || this.mSelectedRowId != this.mOldSelectedRowId) {
            selectionChanged();
            this.mOldSelectedPosition = this.mSelectedPosition;
            this.mOldSelectedRowId = this.mSelectedRowId;
        }
    }

    @TargetApi(14)
    private SparseBooleanArray cloneCheckStates() {
        if (this.mCheckStates == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT >= 14) {
            return this.mCheckStates.clone();
        }
        SparseBooleanArray sparseBooleanArray = new SparseBooleanArray();
        for (int i = 0; i < this.mCheckStates.size(); i++) {
            sparseBooleanArray.put(this.mCheckStates.keyAt(i), this.mCheckStates.valueAt(i));
        }
        return sparseBooleanArray;
    }

    private boolean contentFits() {
        int childCount = getChildCount();
        if (childCount == 0) {
            return true;
        }
        if (childCount != this.mItemCount) {
            return false;
        }
        return getChildStartEdge(getChildAt(0)) >= getStartEdge() && getChildEndEdge(getChildAt(childCount + -1)) <= getEndEdge();
    }

    private void correctTooHigh(int i) {
        if ((this.mFirstPosition + i) - 1 == this.mItemCount - 1 && i != 0) {
            int childEndEdge = getChildEndEdge(getChildAt(i - 1));
            int startEdge = getStartEdge();
            int endEdge = getEndEdge() - childEndEdge;
            View childAt = getChildAt(0);
            int childStartEdge = getChildStartEdge(childAt);
            if (endEdge <= 0) {
                return;
            }
            if (this.mFirstPosition > 0 || childStartEdge < startEdge) {
                if (this.mFirstPosition == 0) {
                    endEdge = Math.min(endEdge, startEdge - childStartEdge);
                }
                offsetChildren(endEdge);
                if (this.mFirstPosition > 0) {
                    fillBefore(this.mFirstPosition - 1, getChildStartEdge(childAt) - this.mItemMargin);
                    adjustViewsStartOrEnd();
                }
            }
        }
    }

    private void correctTooLow(int i) {
        if (this.mFirstPosition == 0 && i != 0) {
            View childAt = getChildAt(0);
            int top = this.mIsVertical ? childAt.getTop() : childAt.getLeft();
            int startEdge = getStartEdge();
            int endEdge = getEndEdge();
            int i2 = top - startEdge;
            View childAt2 = getChildAt(i - 1);
            int childEndEdge = getChildEndEdge(childAt2);
            int i3 = (this.mFirstPosition + i) - 1;
            if (i2 <= 0) {
                return;
            }
            if (i3 < this.mItemCount - 1 || childEndEdge > endEdge) {
                if (i3 == this.mItemCount - 1) {
                    i2 = Math.min(i2, childEndEdge - endEdge);
                }
                offsetChildren(-i2);
                if (i3 < this.mItemCount - 1) {
                    fillAfter(i3 + 1, getChildEndEdge(childAt2) + this.mItemMargin);
                    adjustViewsStartOrEnd();
                }
            } else if (i3 == this.mItemCount - 1) {
                adjustViewsStartOrEnd();
            }
        }
    }

    private ContextMenu.ContextMenuInfo createContextMenuInfo(View view, int i, long j) {
        return new AdapterView.AdapterContextMenuInfo(view, i, j);
    }

    private int distanceToView(View view) {
        view.getDrawingRect(this.mTempRect);
        offsetDescendantRectToMyCoords(view, this.mTempRect);
        int startEdge = getStartEdge();
        int endEdge = getEndEdge();
        int i = this.mIsVertical ? this.mTempRect.top : this.mTempRect.left;
        int i2 = this.mIsVertical ? this.mTempRect.bottom : this.mTempRect.right;
        if (i2 < startEdge) {
            return startEdge - i2;
        }
        if (i > endEdge) {
            return i - endEdge;
        }
        return 0;
    }

    private boolean drawEndEdge(Canvas canvas) {
        if (this.mEndEdge.isFinished()) {
            return false;
        }
        int save = canvas.save();
        int width = getWidth();
        int height = getHeight();
        if (this.mIsVertical) {
            canvas.translate((float) (-width), (float) height);
            canvas.rotate(180.0f, (float) width, 0.0f);
        } else {
            canvas.translate((float) width, 0.0f);
            canvas.rotate(90.0f);
        }
        boolean draw = this.mEndEdge.draw(canvas);
        canvas.restoreToCount(save);
        return draw;
    }

    private void drawSelector(Canvas canvas) {
        if (!this.mSelectorRect.isEmpty()) {
            Drawable drawable = this.mSelector;
            drawable.setBounds(this.mSelectorRect);
            drawable.draw(canvas);
        }
    }

    private boolean drawStartEdge(Canvas canvas) {
        if (this.mStartEdge.isFinished()) {
            return false;
        }
        if (this.mIsVertical) {
            return this.mStartEdge.draw(canvas);
        }
        int save = canvas.save();
        canvas.translate(0.0f, (float) getHeight());
        canvas.rotate(270.0f);
        boolean draw = this.mStartEdge.draw(canvas);
        canvas.restoreToCount(save);
        return draw;
    }

    private View fillAfter(int i, int i2) {
        View view = null;
        int endEdge = getEndEdge();
        while (i2 < endEdge && i < this.mItemCount) {
            boolean z = i == this.mSelectedPosition;
            View makeAndAddView = makeAndAddView(i, i2, true, z);
            i2 = getChildEndEdge(makeAndAddView) + this.mItemMargin;
            if (!z) {
                makeAndAddView = view;
            }
            i++;
            view = makeAndAddView;
        }
        return view;
    }

    private View fillBefore(int i, int i2) {
        View view = null;
        int startEdge = getStartEdge();
        while (i2 > startEdge && i >= 0) {
            boolean z = i == this.mSelectedPosition;
            View makeAndAddView = makeAndAddView(i, i2, false, z);
            int top = this.mIsVertical ? makeAndAddView.getTop() - this.mItemMargin : makeAndAddView.getLeft() - this.mItemMargin;
            if (!z) {
                makeAndAddView = view;
            }
            i--;
            view = makeAndAddView;
            i2 = top;
        }
        this.mFirstPosition = i + 1;
        return view;
    }

    private void fillBeforeAndAfter(View view, int i) {
        fillBefore(i - 1, getChildStartEdge(view) + this.mItemMargin);
        adjustViewsStartOrEnd();
        fillAfter(i + 1, getChildEndEdge(view) + this.mItemMargin);
    }

    private View fillFromMiddle(int i, int i2) {
        int i3 = i2 - i;
        int reconcileSelectedPosition = reconcileSelectedPosition();
        View makeAndAddView = makeAndAddView(reconcileSelectedPosition, i, true, true);
        this.mFirstPosition = reconcileSelectedPosition;
        if (this.mIsVertical) {
            int measuredHeight = makeAndAddView.getMeasuredHeight();
            if (measuredHeight <= i3) {
                makeAndAddView.offsetTopAndBottom((i3 - measuredHeight) / 2);
            }
        } else {
            int measuredWidth = makeAndAddView.getMeasuredWidth();
            if (measuredWidth <= i3) {
                makeAndAddView.offsetLeftAndRight((i3 - measuredWidth) / 2);
            }
        }
        fillBeforeAndAfter(makeAndAddView, reconcileSelectedPosition);
        correctTooHigh(getChildCount());
        return makeAndAddView;
    }

    private View fillFromOffset(int i) {
        this.mFirstPosition = Math.min(this.mFirstPosition, this.mSelectedPosition);
        this.mFirstPosition = Math.min(this.mFirstPosition, this.mItemCount - 1);
        if (this.mFirstPosition < 0) {
            this.mFirstPosition = 0;
        }
        return fillAfter(this.mFirstPosition, i);
    }

    private View fillFromSelection(int i, int i2, int i3) {
        int i4 = this.mSelectedPosition;
        View makeAndAddView = makeAndAddView(i4, i, true, true);
        int childStartEdge = getChildStartEdge(makeAndAddView);
        int childEndEdge = getChildEndEdge(makeAndAddView);
        if (childEndEdge > i3) {
            makeAndAddView.offsetTopAndBottom(-Math.min(childStartEdge - i2, childEndEdge - i3));
        } else if (childStartEdge < i2) {
            makeAndAddView.offsetTopAndBottom(Math.min(i2 - childStartEdge, i3 - childEndEdge));
        }
        fillBeforeAndAfter(makeAndAddView, i4);
        correctTooHigh(getChildCount());
        return makeAndAddView;
    }

    private View fillSpecific(int i, int i2) {
        boolean z = i == this.mSelectedPosition;
        View makeAndAddView = makeAndAddView(i, i2, true, z);
        this.mFirstPosition = i;
        View fillBefore = fillBefore(i - 1, getChildStartEdge(makeAndAddView) + this.mItemMargin);
        adjustViewsStartOrEnd();
        View fillAfter = fillAfter(i + 1, getChildEndEdge(makeAndAddView) + this.mItemMargin);
        int childCount = getChildCount();
        if (childCount > 0) {
            correctTooHigh(childCount);
        }
        return z ? makeAndAddView : fillBefore != null ? fillBefore : fillAfter;
    }

    private int findClosestMotionRowOrColumn(int i) {
        int childCount = getChildCount();
        if (childCount == 0) {
            return -1;
        }
        int findMotionRowOrColumn = findMotionRowOrColumn(i);
        return findMotionRowOrColumn != -1 ? findMotionRowOrColumn : (this.mFirstPosition + childCount) - 1;
    }

    private int findMotionRowOrColumn(int i) {
        int childCount = getChildCount();
        if (childCount == 0) {
            return -1;
        }
        for (int i2 = 0; i2 < childCount; i2++) {
            if (i <= getChildEndEdge(getChildAt(i2))) {
                return this.mFirstPosition + i2;
            }
        }
        return -1;
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 140 */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005a, code lost:
        if (r5 != false) goto L_0x0060;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x005c, code lost:
        if (r0 != false) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x005e, code lost:
        if (r4 != false) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0060, code lost:
        r2 = r2 - 1;
        r0 = true;
        r3 = r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int findSyncPosition() {
        /*
            r12 = this;
            int r6 = r12.mItemCount
            if (r6 != 0) goto L_0x0006
            r3 = -1
        L_0x0005:
            return r3
        L_0x0006:
            long r8 = r12.mSyncRowId
            r0 = -9223372036854775808
            int r0 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
            if (r0 != 0) goto L_0x0010
            r3 = -1
            goto L_0x0005
        L_0x0010:
            int r0 = r12.mSyncPosition
            r1 = 0
            int r0 = java.lang.Math.max(r1, r0)
            int r1 = r6 + -1
            int r1 = java.lang.Math.min(r1, r0)
            long r2 = android.os.SystemClock.uptimeMillis()
            r4 = 100
            long r10 = r2 + r4
            r0 = 0
            android.widget.ListAdapter r7 = r12.mAdapter
            if (r7 != 0) goto L_0x0065
            r3 = -1
            goto L_0x0005
        L_0x002c:
            if (r4 != 0) goto L_0x0032
            if (r0 == 0) goto L_0x005a
            if (r5 != 0) goto L_0x005a
        L_0x0032:
            int r1 = r1 + 1
            r0 = 0
            r3 = r1
        L_0x0036:
            long r4 = android.os.SystemClock.uptimeMillis()
            int r4 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r4 > 0) goto L_0x0053
            long r4 = r7.getItemId(r3)
            int r4 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r4 == 0) goto L_0x0005
            int r4 = r6 + -1
            if (r1 != r4) goto L_0x0055
            r4 = 1
            r5 = r4
        L_0x004c:
            if (r2 != 0) goto L_0x0058
            r4 = 1
        L_0x004f:
            if (r5 == 0) goto L_0x002c
            if (r4 == 0) goto L_0x002c
        L_0x0053:
            r3 = -1
            goto L_0x0005
        L_0x0055:
            r4 = 0
            r5 = r4
            goto L_0x004c
        L_0x0058:
            r4 = 0
            goto L_0x004f
        L_0x005a:
            if (r5 != 0) goto L_0x0060
            if (r0 != 0) goto L_0x0036
            if (r4 != 0) goto L_0x0036
        L_0x0060:
            int r2 = r2 + -1
            r0 = 1
            r3 = r2
            goto L_0x0036
        L_0x0065:
            r2 = r1
            r3 = r1
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.widget.TwoWayView.findSyncPosition():int");
    }

    private void finishEdgeGlows() {
        if (this.mStartEdge != null) {
            this.mStartEdge.finish();
        }
        if (this.mEndEdge != null) {
            this.mEndEdge.finish();
        }
    }

    /* access modifiers changed from: private */
    public void fireOnSelected() {
        AdapterView.OnItemSelectedListener onItemSelectedListener = getOnItemSelectedListener();
        if (onItemSelectedListener != null) {
            int selectedItemPosition = getSelectedItemPosition();
            if (selectedItemPosition >= 0) {
                onItemSelectedListener.onItemSelected(this, getSelectedView(), selectedItemPosition, this.mAdapter.getItemId(selectedItemPosition));
            } else {
                onItemSelectedListener.onNothingSelected(this);
            }
        }
    }

    private void forceValidFocusDirection(int i) {
        if (this.mIsVertical && i != 33 && i != 130) {
            throw new IllegalArgumentException("Focus direction must be one of {View.FOCUS_UP, View.FOCUS_DOWN} for vertical orientation");
        } else if (!this.mIsVertical && i != 17 && i != 66) {
            throw new IllegalArgumentException("Focus direction must be one of {View.FOCUS_LEFT, View.FOCUS_RIGHT} for vertical orientation");
        }
    }

    private void forceValidInnerFocusDirection(int i) {
        if (this.mIsVertical && i != 17 && i != 66) {
            throw new IllegalArgumentException("Direction must be one of {View.FOCUS_LEFT, View.FOCUS_RIGHT} for vertical orientation");
        } else if (!this.mIsVertical && i != 33 && i != 130) {
            throw new IllegalArgumentException("direction must be one of {View.FOCUS_UP, View.FOCUS_DOWN} for horizontal orientation");
        }
    }

    private int getArrowScrollPreviewLength() {
        return Math.max(10, this.mIsVertical ? getVerticalFadingEdgeLength() : getHorizontalFadingEdgeLength()) + this.mItemMargin;
    }

    private int getChildEndEdge(View view) {
        if (view == null) {
            return 0;
        }
        return this.mIsVertical ? view.getBottom() : view.getRight();
    }

    private int getChildHeightMeasureSpec(LayoutParams layoutParams) {
        return (!this.mIsVertical || layoutParams.height != -2) ? !this.mIsVertical ? View.MeasureSpec.makeMeasureSpec((getHeight() - getPaddingTop()) - getPaddingBottom(), 1073741824) : View.MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824) : View.MeasureSpec.makeMeasureSpec(0, 0);
    }

    private int getChildStartEdge(View view) {
        return this.mIsVertical ? view.getTop() : view.getLeft();
    }

    private int getChildWidthMeasureSpec(LayoutParams layoutParams) {
        return (this.mIsVertical || layoutParams.width != -2) ? this.mIsVertical ? View.MeasureSpec.makeMeasureSpec((getWidth() - getPaddingLeft()) - getPaddingRight(), 1073741824) : View.MeasureSpec.makeMeasureSpec(layoutParams.width, 1073741824) : View.MeasureSpec.makeMeasureSpec(0, 0);
    }

    @TargetApi(14)
    private final float getCurrVelocity() {
        if (Build.VERSION.SDK_INT >= 14) {
            return this.mScroller.getCurrVelocity();
        }
        return 0.0f;
    }

    private static int getDistance(Rect rect, Rect rect2, int i) {
        int width;
        int height;
        int width2;
        int height2;
        switch (i) {
            case 1:
            case 2:
                width = rect.right + (rect.width() / 2);
                height = rect.top + (rect.height() / 2);
                width2 = (rect2.width() / 2) + rect2.left;
                height2 = rect2.top + (rect2.height() / 2);
                break;
            case 17:
                width = rect.left;
                height = rect.top + (rect.height() / 2);
                width2 = rect2.right;
                height2 = rect2.top + (rect2.height() / 2);
                break;
            case 33:
                width = rect.left + (rect.width() / 2);
                height = rect.top;
                width2 = (rect2.width() / 2) + rect2.left;
                height2 = rect2.bottom;
                break;
            case 66:
                width = rect.right;
                height = rect.top + (rect.height() / 2);
                width2 = rect2.left;
                height2 = rect2.top + (rect2.height() / 2);
                break;
            case TransportMediator.KEYCODE_MEDIA_RECORD:
                width = rect.left + (rect.width() / 2);
                height = rect.bottom;
                width2 = (rect2.width() / 2) + rect2.left;
                height2 = rect2.top;
                break;
            default:
                throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT, FOCUS_FORWARD, FOCUS_BACKWARD}.");
        }
        int i2 = width2 - width;
        int i3 = height2 - height;
        return (i3 * i3) + (i2 * i2);
    }

    private int getEndEdge() {
        return this.mIsVertical ? getHeight() - getPaddingBottom() : getWidth() - getPaddingRight();
    }

    @TargetApi(9)
    private int getScaledOverscrollDistance(ViewConfiguration viewConfiguration) {
        if (Build.VERSION.SDK_INT < 9) {
            return 0;
        }
        return viewConfiguration.getScaledOverscrollDistance();
    }

    private int getStartEdge() {
        return this.mIsVertical ? getPaddingTop() : getPaddingLeft();
    }

    private void handleDataChanged() {
        if (!(this.mChoiceMode == ChoiceMode.NONE || this.mAdapter == null || !this.mAdapter.hasStableIds())) {
            confirmCheckedPositionsById();
        }
        this.mRecycler.clearTransientStateViews();
        int i = this.mItemCount;
        if (i > 0) {
            if (this.mNeedSync) {
                this.mNeedSync = false;
                this.mPendingSync = null;
                switch (this.mSyncMode) {
                    case 1:
                        this.mLayoutMode = 5;
                        this.mSyncPosition = Math.min(Math.max(0, this.mSyncPosition), i - 1);
                        return;
                    case 0:
                        if (isInTouchMode()) {
                            this.mLayoutMode = 5;
                            this.mSyncPosition = Math.min(Math.max(0, this.mSyncPosition), i - 1);
                            return;
                        }
                        int findSyncPosition = findSyncPosition();
                        if (findSyncPosition >= 0 && lookForSelectablePosition(findSyncPosition, true) == findSyncPosition) {
                            this.mSyncPosition = findSyncPosition;
                            if (this.mSyncHeight == ((long) getHeight())) {
                                this.mLayoutMode = 5;
                            } else {
                                this.mLayoutMode = 2;
                            }
                            setNextSelectedPositionInt(findSyncPosition);
                            return;
                        }
                }
            }
            if (!isInTouchMode()) {
                int selectedItemPosition = getSelectedItemPosition();
                if (selectedItemPosition >= i) {
                    selectedItemPosition = i - 1;
                }
                if (selectedItemPosition < 0) {
                    selectedItemPosition = 0;
                }
                int lookForSelectablePosition = lookForSelectablePosition(selectedItemPosition, true);
                if (lookForSelectablePosition >= 0) {
                    setNextSelectedPositionInt(lookForSelectablePosition);
                    return;
                }
                int lookForSelectablePosition2 = lookForSelectablePosition(selectedItemPosition, false);
                if (lookForSelectablePosition2 >= 0) {
                    setNextSelectedPositionInt(lookForSelectablePosition2);
                    return;
                }
            } else if (this.mResurrectToPosition >= 0) {
                return;
            }
        }
        this.mLayoutMode = 1;
        this.mSelectedPosition = -1;
        this.mSelectedRowId = Long.MIN_VALUE;
        this.mNextSelectedPosition = -1;
        this.mNextSelectedRowId = Long.MIN_VALUE;
        this.mNeedSync = false;
        this.mPendingSync = null;
        this.mSelectorPosition = -1;
        checkSelectionChanged();
    }

    private void handleDragChange(int i) {
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(true);
        }
        int childCount = this.mMotionPosition >= 0 ? this.mMotionPosition - this.mFirstPosition : getChildCount() / 2;
        int i2 = 0;
        View childAt = getChildAt(childCount);
        if (childAt != null) {
            i2 = this.mIsVertical ? childAt.getTop() : childAt.getLeft();
        }
        boolean scrollListItemsBy = scrollListItemsBy(i);
        View childAt2 = getChildAt(childCount);
        if (childAt2 != null) {
            int top = this.mIsVertical ? childAt2.getTop() : childAt2.getLeft();
            if (scrollListItemsBy) {
                updateOverScrollState(i, (-i) - (top - i2));
            }
        }
    }

    private boolean handleFocusWithinItem(int i) {
        View selectedView;
        forceValidInnerFocusDirection(i);
        int childCount = getChildCount();
        if (this.mItemsCanFocus && childCount > 0 && this.mSelectedPosition != -1 && (selectedView = getSelectedView()) != null && selectedView.hasFocus() && (selectedView instanceof ViewGroup)) {
            View findFocus = selectedView.findFocus();
            View findNextFocus = FocusFinder.getInstance().findNextFocus((ViewGroup) selectedView, findFocus, i);
            if (findNextFocus != null) {
                findFocus.getFocusedRect(this.mTempRect);
                offsetDescendantRectToMyCoords(findFocus, this.mTempRect);
                offsetRectIntoDescendantCoords(findNextFocus, this.mTempRect);
                if (findNextFocus.requestFocus(i, this.mTempRect)) {
                    return true;
                }
            }
            View findNextFocus2 = FocusFinder.getInstance().findNextFocus((ViewGroup) getRootView(), findFocus, i);
            if (findNextFocus2 != null) {
                return isViewAncestorOf(findNextFocus2, this);
            }
        }
        return false;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:154:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean handleKeyEvent(int r9, int r10, android.view.KeyEvent r11) {
        /*
            r8 = this;
            r0 = 66
            r1 = 33
            r2 = 17
            r3 = 0
            r4 = 1
            android.widget.ListAdapter r5 = r8.mAdapter
            if (r5 == 0) goto L_0x0010
            boolean r5 = r8.mIsAttached
            if (r5 != 0) goto L_0x0012
        L_0x0010:
            r4 = r3
        L_0x0011:
            return r4
        L_0x0012:
            boolean r5 = r8.mDataChanged
            if (r5 == 0) goto L_0x0019
            r8.layoutChildren()
        L_0x0019:
            int r5 = r11.getAction()
            if (r5 == r4) goto L_0x0022
            switch(r9) {
                case 19: goto L_0x002a;
                case 20: goto L_0x003e;
                case 21: goto L_0x0056;
                case 22: goto L_0x006a;
                case 23: goto L_0x007e;
                case 62: goto L_0x009b;
                case 66: goto L_0x007e;
                case 92: goto L_0x00d0;
                case 93: goto L_0x010d;
                case 122: goto L_0x014a;
                case 123: goto L_0x0168;
                default: goto L_0x0022;
            }
        L_0x0022:
            r0 = r3
        L_0x0023:
            if (r0 != 0) goto L_0x0011
            switch(r5) {
                case 0: goto L_0x0186;
                case 1: goto L_0x018c;
                case 2: goto L_0x01cd;
                default: goto L_0x0028;
            }
        L_0x0028:
            r4 = r3
            goto L_0x0011
        L_0x002a:
            boolean r0 = r8.mIsVertical
            if (r0 == 0) goto L_0x0033
            boolean r0 = r8.handleKeyScroll(r11, r10, r1)
            goto L_0x0023
        L_0x0033:
            boolean r0 = android.support.v4.view.KeyEventCompat.hasNoModifiers(r11)
            if (r0 == 0) goto L_0x0022
            boolean r0 = r8.handleFocusWithinItem(r1)
            goto L_0x0023
        L_0x003e:
            boolean r0 = r8.mIsVertical
            if (r0 == 0) goto L_0x0049
            r0 = 130(0x82, float:1.82E-43)
            boolean r0 = r8.handleKeyScroll(r11, r10, r0)
            goto L_0x0023
        L_0x0049:
            boolean r0 = android.support.v4.view.KeyEventCompat.hasNoModifiers(r11)
            if (r0 == 0) goto L_0x0022
            r0 = 130(0x82, float:1.82E-43)
            boolean r0 = r8.handleFocusWithinItem(r0)
            goto L_0x0023
        L_0x0056:
            boolean r0 = r8.mIsVertical
            if (r0 != 0) goto L_0x005f
            boolean r0 = r8.handleKeyScroll(r11, r10, r2)
            goto L_0x0023
        L_0x005f:
            boolean r0 = android.support.v4.view.KeyEventCompat.hasNoModifiers(r11)
            if (r0 == 0) goto L_0x0022
            boolean r0 = r8.handleFocusWithinItem(r2)
            goto L_0x0023
        L_0x006a:
            boolean r1 = r8.mIsVertical
            if (r1 != 0) goto L_0x0073
            boolean r0 = r8.handleKeyScroll(r11, r10, r0)
            goto L_0x0023
        L_0x0073:
            boolean r1 = android.support.v4.view.KeyEventCompat.hasNoModifiers(r11)
            if (r1 == 0) goto L_0x0022
            boolean r0 = r8.handleFocusWithinItem(r0)
            goto L_0x0023
        L_0x007e:
            boolean r0 = android.support.v4.view.KeyEventCompat.hasNoModifiers(r11)
            if (r0 == 0) goto L_0x0022
            boolean r0 = r8.resurrectSelectionIfNeeded()
            if (r0 != 0) goto L_0x0023
            int r1 = r11.getRepeatCount()
            if (r1 != 0) goto L_0x0023
            int r1 = r8.getChildCount()
            if (r1 <= 0) goto L_0x0023
            r8.keyPressed()
            r0 = r4
            goto L_0x0023
        L_0x009b:
            boolean r6 = android.support.v4.view.KeyEventCompat.hasNoModifiers(r11)
            if (r6 == 0) goto L_0x00b6
            boolean r1 = r8.resurrectSelectionIfNeeded()
            if (r1 != 0) goto L_0x00b3
            boolean r1 = r8.mIsVertical
            if (r1 == 0) goto L_0x00ad
            r0 = 130(0x82, float:1.82E-43)
        L_0x00ad:
            boolean r0 = r8.pageScroll(r0)
            if (r0 == 0) goto L_0x00b3
        L_0x00b3:
            r0 = r4
            goto L_0x0023
        L_0x00b6:
            boolean r0 = android.support.v4.view.KeyEventCompat.hasModifiers(r11, r4)
            if (r0 == 0) goto L_0x00b3
            boolean r0 = r8.resurrectSelectionIfNeeded()
            if (r0 != 0) goto L_0x00b3
            boolean r0 = r8.mIsVertical
            if (r0 == 0) goto L_0x00ce
            r0 = r1
        L_0x00c7:
            boolean r0 = r8.fullScroll(r0)
            if (r0 == 0) goto L_0x00b3
            goto L_0x00b3
        L_0x00ce:
            r0 = r2
            goto L_0x00c7
        L_0x00d0:
            boolean r0 = android.support.v4.view.KeyEventCompat.hasNoModifiers(r11)
            if (r0 == 0) goto L_0x00ee
            boolean r0 = r8.resurrectSelectionIfNeeded()
            if (r0 != 0) goto L_0x00e6
            boolean r0 = r8.mIsVertical
            if (r0 == 0) goto L_0x00e9
        L_0x00e0:
            boolean r0 = r8.pageScroll(r1)
            if (r0 == 0) goto L_0x00eb
        L_0x00e6:
            r0 = r4
            goto L_0x0023
        L_0x00e9:
            r1 = r2
            goto L_0x00e0
        L_0x00eb:
            r0 = r3
            goto L_0x0023
        L_0x00ee:
            r0 = 2
            boolean r0 = android.support.v4.view.KeyEventCompat.hasModifiers(r11, r0)
            if (r0 == 0) goto L_0x0022
            boolean r0 = r8.resurrectSelectionIfNeeded()
            if (r0 != 0) goto L_0x0105
            boolean r0 = r8.mIsVertical
            if (r0 == 0) goto L_0x0108
        L_0x00ff:
            boolean r0 = r8.fullScroll(r1)
            if (r0 == 0) goto L_0x010a
        L_0x0105:
            r0 = r4
            goto L_0x0023
        L_0x0108:
            r1 = r2
            goto L_0x00ff
        L_0x010a:
            r0 = r3
            goto L_0x0023
        L_0x010d:
            boolean r1 = android.support.v4.view.KeyEventCompat.hasNoModifiers(r11)
            if (r1 == 0) goto L_0x012b
            boolean r1 = r8.resurrectSelectionIfNeeded()
            if (r1 != 0) goto L_0x0125
            boolean r1 = r8.mIsVertical
            if (r1 == 0) goto L_0x011f
            r0 = 130(0x82, float:1.82E-43)
        L_0x011f:
            boolean r0 = r8.pageScroll(r0)
            if (r0 == 0) goto L_0x0128
        L_0x0125:
            r0 = r4
            goto L_0x0023
        L_0x0128:
            r0 = r3
            goto L_0x0023
        L_0x012b:
            r1 = 2
            boolean r1 = android.support.v4.view.KeyEventCompat.hasModifiers(r11, r1)
            if (r1 == 0) goto L_0x0022
            boolean r1 = r8.resurrectSelectionIfNeeded()
            if (r1 != 0) goto L_0x0144
            boolean r1 = r8.mIsVertical
            if (r1 == 0) goto L_0x013e
            r0 = 130(0x82, float:1.82E-43)
        L_0x013e:
            boolean r0 = r8.fullScroll(r0)
            if (r0 == 0) goto L_0x0147
        L_0x0144:
            r0 = r4
            goto L_0x0023
        L_0x0147:
            r0 = r3
            goto L_0x0023
        L_0x014a:
            boolean r0 = android.support.v4.view.KeyEventCompat.hasNoModifiers(r11)
            if (r0 == 0) goto L_0x0022
            boolean r0 = r8.resurrectSelectionIfNeeded()
            if (r0 != 0) goto L_0x0160
            boolean r0 = r8.mIsVertical
            if (r0 == 0) goto L_0x0163
        L_0x015a:
            boolean r0 = r8.fullScroll(r1)
            if (r0 == 0) goto L_0x0165
        L_0x0160:
            r0 = r4
            goto L_0x0023
        L_0x0163:
            r1 = r2
            goto L_0x015a
        L_0x0165:
            r0 = r3
            goto L_0x0023
        L_0x0168:
            boolean r1 = android.support.v4.view.KeyEventCompat.hasNoModifiers(r11)
            if (r1 == 0) goto L_0x0022
            boolean r1 = r8.resurrectSelectionIfNeeded()
            if (r1 != 0) goto L_0x0180
            boolean r1 = r8.mIsVertical
            if (r1 == 0) goto L_0x017a
            r0 = 130(0x82, float:1.82E-43)
        L_0x017a:
            boolean r0 = r8.fullScroll(r0)
            if (r0 == 0) goto L_0x0183
        L_0x0180:
            r0 = r4
            goto L_0x0023
        L_0x0183:
            r0 = r3
            goto L_0x0023
        L_0x0186:
            boolean r4 = super.onKeyDown(r9, r11)
            goto L_0x0011
        L_0x018c:
            boolean r0 = r8.isEnabled()
            if (r0 == 0) goto L_0x0011
            boolean r0 = r8.isClickable()
            if (r0 == 0) goto L_0x01ca
            boolean r0 = r8.isPressed()
            if (r0 == 0) goto L_0x01ca
            int r0 = r8.mSelectedPosition
            if (r0 < 0) goto L_0x01ca
            android.widget.ListAdapter r0 = r8.mAdapter
            if (r0 == 0) goto L_0x01ca
            int r0 = r8.mSelectedPosition
            android.widget.ListAdapter r1 = r8.mAdapter
            int r1 = r1.getCount()
            if (r0 >= r1) goto L_0x01ca
            int r0 = r8.mSelectedPosition
            int r1 = r8.mFirstPosition
            int r0 = r0 - r1
            android.view.View r0 = r8.getChildAt(r0)
            if (r0 == 0) goto L_0x01c5
            int r1 = r8.mSelectedPosition
            long r6 = r8.mSelectedRowId
            r8.performItemClick(r0, r1, r6)
            r0.setPressed(r3)
        L_0x01c5:
            r8.setPressed(r3)
            goto L_0x0011
        L_0x01ca:
            r4 = r3
            goto L_0x0011
        L_0x01cd:
            boolean r4 = super.onKeyMultiple(r9, r10, r11)
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.widget.TwoWayView.handleKeyEvent(int, int, android.view.KeyEvent):boolean");
    }

    private boolean handleKeyScroll(KeyEvent keyEvent, int i, int i2) {
        if (KeyEventCompat.hasNoModifiers(keyEvent)) {
            boolean resurrectSelectionIfNeeded = resurrectSelectionIfNeeded();
            if (resurrectSelectionIfNeeded) {
                return resurrectSelectionIfNeeded;
            }
            while (true) {
                int i3 = i - 1;
                if (i <= 0 || !arrowScroll(i2)) {
                    return resurrectSelectionIfNeeded;
                }
                resurrectSelectionIfNeeded = true;
                i = i3;
            }
        } else if (KeyEventCompat.hasModifiers(keyEvent, 2)) {
            return resurrectSelectionIfNeeded() || fullScroll(i2);
        } else {
            return false;
        }
    }

    private void handleNewSelectionChange(View view, int i, int i2, boolean z) {
        int i3;
        boolean z2;
        View view2;
        boolean z3 = true;
        forceValidFocusDirection(i);
        if (i2 == -1) {
            throw new IllegalArgumentException("newSelectedPosition needs to be valid");
        }
        int i4 = this.mSelectedPosition - this.mFirstPosition;
        int i5 = i2 - this.mFirstPosition;
        if (i == 33 || i == 17) {
            view2 = getChildAt(i5);
            i3 = i5;
            z2 = true;
        } else {
            View childAt = getChildAt(i5);
            i3 = i4;
            i4 = i5;
            z2 = false;
            View view3 = view;
            view = childAt;
            view2 = view3;
        }
        int childCount = getChildCount();
        if (view2 != null) {
            view2.setSelected(!z && z2);
            measureAndAdjustDown(view2, i3, childCount);
        }
        if (view != null) {
            if (z || z2) {
                z3 = false;
            }
            view.setSelected(z3);
            measureAndAdjustDown(view, i4, childCount);
        }
    }

    private void handleOverScrollChange(int i) {
        int i2;
        int i3 = this.mOverScroll;
        int i4 = i3 - i;
        int i5 = -i;
        if ((i4 >= 0 || i3 < 0) && (i4 <= 0 || i3 > 0)) {
            i2 = 0;
        } else {
            i5 = -i3;
            i2 = i + i5;
        }
        if (i5 != 0) {
            updateOverScrollState(i2, i5);
        }
        if (i2 != 0) {
            if (this.mOverScroll != 0) {
                this.mOverScroll = 0;
                ViewCompat.postInvalidateOnAnimation(this);
            }
            scrollListItemsBy(i2);
            this.mTouchMode = 3;
            this.mMotionPosition = findClosestMotionRowOrColumn((int) this.mLastTouchPos);
            this.mTouchRemainderPos = 0.0f;
        }
    }

    private void hideSelector() {
        if (this.mSelectedPosition != -1) {
            if (this.mLayoutMode != 4) {
                this.mResurrectToPosition = this.mSelectedPosition;
            }
            if (this.mNextSelectedPosition >= 0 && this.mNextSelectedPosition != this.mSelectedPosition) {
                this.mResurrectToPosition = this.mNextSelectedPosition;
            }
            setSelectedPositionInt(-1);
            setNextSelectedPositionInt(-1);
            this.mSelectedStart = 0;
        }
    }

    private void initOrResetVelocityTracker() {
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        } else {
            this.mVelocityTracker.clear();
        }
    }

    private void initVelocityTrackerIfNotExists() {
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
    }

    private void invokeOnItemScrollListener() {
        if (this.mOnScrollListener != null) {
            this.mOnScrollListener.onScroll(this, this.mFirstPosition, getChildCount(), this.mItemCount);
        }
        onScrollChanged(0, 0, 0, 0);
    }

    private boolean isViewAncestorOf(View view, View view2) {
        if (view == view2) {
            return true;
        }
        ViewParent parent = view.getParent();
        return (parent instanceof ViewGroup) && isViewAncestorOf((View) parent, view2);
    }

    private void keyPressed() {
        if (isEnabled() && isClickable()) {
            Drawable drawable = this.mSelector;
            Rect rect = this.mSelectorRect;
            if (drawable == null) {
                return;
            }
            if ((isFocused() || touchModeDrawsInPressedState()) && !rect.isEmpty()) {
                View childAt = getChildAt(this.mSelectedPosition - this.mFirstPosition);
                if (childAt != null) {
                    if (!childAt.hasFocusable()) {
                        childAt.setPressed(true);
                    } else {
                        return;
                    }
                }
                setPressed(true);
                boolean isLongClickable = isLongClickable();
                Drawable current = drawable.getCurrent();
                if (current != null && (current instanceof TransitionDrawable)) {
                    if (isLongClickable) {
                        ((TransitionDrawable) current).startTransition(ViewConfiguration.getLongPressTimeout());
                    } else {
                        ((TransitionDrawable) current).resetTransition();
                    }
                }
                if (isLongClickable && !this.mDataChanged) {
                    if (this.mPendingCheckForKeyLongPress == null) {
                        this.mPendingCheckForKeyLongPress = new CheckForKeyLongPress();
                    }
                    this.mPendingCheckForKeyLongPress.rememberWindowAttachCount();
                    postDelayed(this.mPendingCheckForKeyLongPress, (long) ViewConfiguration.getLongPressTimeout());
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void layoutChildren() {
        boolean z;
        View view;
        View view2;
        View view3;
        View moveSelection;
        if (getWidth() != 0 && getHeight() != 0 && !(z = this.mBlockLayoutRequests)) {
            this.mBlockLayoutRequests = true;
            try {
                invalidate();
                if (this.mAdapter == null) {
                    resetState();
                    if (z) {
                        return;
                    }
                    return;
                }
                int startEdge = getStartEdge();
                int endEdge = getEndEdge();
                int childCount = getChildCount();
                int i = 0;
                View view4 = null;
                View view5 = null;
                View view6 = null;
                switch (this.mLayoutMode) {
                    case 1:
                    case 3:
                    case 4:
                    case 5:
                        view = null;
                        break;
                    case 2:
                        int i2 = this.mNextSelectedPosition - this.mFirstPosition;
                        if (i2 >= 0 && i2 < childCount) {
                            view6 = getChildAt(i2);
                            view = null;
                            break;
                        } else {
                            view = null;
                            break;
                        }
                    default:
                        int i3 = this.mSelectedPosition - this.mFirstPosition;
                        if (i3 >= 0 && i3 < childCount) {
                            view5 = getChildAt(i3);
                        }
                        View childAt = getChildAt(0);
                        if (this.mNextSelectedPosition >= 0) {
                            i = this.mNextSelectedPosition - this.mSelectedPosition;
                        }
                        view6 = getChildAt(i3 + i);
                        view = childAt;
                        break;
                }
                boolean z2 = this.mDataChanged;
                if (z2) {
                    handleDataChanged();
                }
                if (this.mItemCount == 0) {
                    resetState();
                    if (!z) {
                        this.mBlockLayoutRequests = false;
                        this.mDataChanged = false;
                    }
                } else if (this.mItemCount != this.mAdapter.getCount()) {
                    throw new IllegalStateException("The content of the adapter has changed but TwoWayView did not receive a notification. Make sure the content of your adapter is not modified from a background thread, but only from the UI thread. [in TwoWayView(" + getId() + ", " + getClass() + ") with Adapter(" + this.mAdapter.getClass() + ")]");
                } else {
                    setSelectedPositionInt(this.mNextSelectedPosition);
                    View view7 = null;
                    int i4 = this.mFirstPosition;
                    RecycleBin recycleBin = this.mRecycler;
                    if (z2) {
                        for (int i5 = 0; i5 < childCount; i5++) {
                            recycleBin.addScrapView(getChildAt(i5), i4 + i5);
                        }
                    } else {
                        recycleBin.fillActiveViews(childCount, i4);
                    }
                    View focusedChild = getFocusedChild();
                    if (focusedChild != null) {
                        if (!z2) {
                            view4 = findFocus();
                            if (view4 != null) {
                                view4.onStartTemporaryDetach();
                            }
                            view7 = focusedChild;
                        }
                        requestFocus();
                        view2 = view4;
                        view3 = view7;
                    } else {
                        view2 = null;
                        view3 = null;
                    }
                    detachAllViewsFromParent();
                    switch (this.mLayoutMode) {
                        case 1:
                            this.mFirstPosition = 0;
                            View fillFromOffset = fillFromOffset(startEdge);
                            adjustViewsStartOrEnd();
                            moveSelection = fillFromOffset;
                            break;
                        case 2:
                            if (view6 == null) {
                                moveSelection = fillFromMiddle(startEdge, endEdge);
                                break;
                            } else {
                                moveSelection = fillFromSelection(this.mIsVertical ? view6.getTop() : view6.getLeft(), startEdge, endEdge);
                                break;
                            }
                        case 3:
                            View fillBefore = fillBefore(this.mItemCount - 1, endEdge);
                            adjustViewsStartOrEnd();
                            moveSelection = fillBefore;
                            break;
                        case 4:
                            moveSelection = fillSpecific(reconcileSelectedPosition(), this.mSpecificStart);
                            break;
                        case 5:
                            moveSelection = fillSpecific(this.mSyncPosition, this.mSpecificStart);
                            break;
                        case 6:
                            moveSelection = moveSelection(view5, view6, i, startEdge, endEdge);
                            break;
                        default:
                            if (childCount != 0) {
                                if (this.mSelectedPosition < 0 || this.mSelectedPosition >= this.mItemCount) {
                                    if (this.mFirstPosition >= this.mItemCount) {
                                        moveSelection = fillSpecific(0, startEdge);
                                        break;
                                    } else {
                                        moveSelection = fillSpecific(this.mFirstPosition, view != null ? this.mIsVertical ? view.getTop() : view.getLeft() : startEdge);
                                        break;
                                    }
                                } else {
                                    moveSelection = fillSpecific(this.mSelectedPosition, view5 != null ? this.mIsVertical ? view5.getTop() : view5.getLeft() : startEdge);
                                    break;
                                }
                            } else {
                                setSelectedPositionInt(lookForSelectablePosition(0));
                                moveSelection = fillFromOffset(startEdge);
                                break;
                            }
                            break;
                    }
                    recycleBin.scrapActiveViews();
                    if (moveSelection != null) {
                        if (!this.mItemsCanFocus || !hasFocus() || moveSelection.hasFocus()) {
                            positionSelector(-1, moveSelection);
                        } else {
                            if (!((moveSelection == view3 && view2 != null && view2.requestFocus()) || moveSelection.requestFocus())) {
                                View focusedChild2 = getFocusedChild();
                                if (focusedChild2 != null) {
                                    focusedChild2.clearFocus();
                                }
                                positionSelector(-1, moveSelection);
                            } else {
                                moveSelection.setSelected(false);
                                this.mSelectorRect.setEmpty();
                            }
                        }
                        this.mSelectedStart = this.mIsVertical ? moveSelection.getTop() : moveSelection.getLeft();
                    } else {
                        if (this.mTouchMode <= 0 || this.mTouchMode >= 3) {
                            this.mSelectedStart = 0;
                            this.mSelectorRect.setEmpty();
                        } else {
                            View childAt2 = getChildAt(this.mMotionPosition - this.mFirstPosition);
                            if (childAt2 != null) {
                                positionSelector(this.mMotionPosition, childAt2);
                            }
                        }
                        if (hasFocus() && view2 != null) {
                            view2.requestFocus();
                        }
                    }
                    if (!(view2 == null || view2.getWindowToken() == null)) {
                        view2.onFinishTemporaryDetach();
                    }
                    this.mLayoutMode = 0;
                    this.mDataChanged = false;
                    this.mNeedSync = false;
                    setNextSelectedPositionInt(this.mSelectedPosition);
                    if (this.mItemCount > 0) {
                        checkSelectionChanged();
                    }
                    invokeOnItemScrollListener();
                    if (!z) {
                        this.mBlockLayoutRequests = false;
                        this.mDataChanged = false;
                    }
                }
            } finally {
                if (!z) {
                    this.mBlockLayoutRequests = false;
                    this.mDataChanged = false;
                }
            }
        }
    }

    private int lookForSelectablePosition(int i) {
        return lookForSelectablePosition(i, true);
    }

    private int lookForSelectablePosition(int i, boolean z) {
        int i2;
        ListAdapter listAdapter = this.mAdapter;
        if (listAdapter == null || isInTouchMode()) {
            return -1;
        }
        int i3 = this.mItemCount;
        if (!this.mAreAllItemsSelectable) {
            if (z) {
                i2 = Math.max(0, i);
                while (i2 < i3 && !listAdapter.isEnabled(i2)) {
                    i2++;
                }
            } else {
                int min = Math.min(i, i3 - 1);
                while (i2 >= 0 && !listAdapter.isEnabled(i2)) {
                    min = i2 - 1;
                }
            }
            if (i2 < 0 || i2 >= i3) {
                return -1;
            }
            return i2;
        } else if (i < 0 || i >= i3) {
            return -1;
        } else {
            return i;
        }
    }

    private int lookForSelectablePositionOnScreen(int i) {
        forceValidFocusDirection(i);
        int i2 = this.mFirstPosition;
        ListAdapter adapter = getAdapter();
        if (i == 130 || i == 66) {
            int i3 = this.mSelectedPosition != -1 ? this.mSelectedPosition + 1 : i2;
            if (i3 >= adapter.getCount()) {
                return -1;
            }
            if (i3 < i2) {
                i3 = i2;
            }
            int lastVisiblePosition = getLastVisiblePosition();
            while (i3 <= lastVisiblePosition) {
                if (adapter.isEnabled(i3) && getChildAt(i3 - i2).getVisibility() == 0) {
                    return i3;
                }
                i3++;
            }
        } else {
            int childCount = (getChildCount() + i2) - 1;
            int childCount2 = this.mSelectedPosition != -1 ? this.mSelectedPosition - 1 : (getChildCount() + i2) - 1;
            if (childCount2 < 0 || childCount2 >= adapter.getCount()) {
                return -1;
            }
            if (childCount2 <= childCount) {
                childCount = childCount2;
            }
            while (childCount >= i2) {
                if (adapter.isEnabled(childCount) && getChildAt(childCount - i2).getVisibility() == 0) {
                    return childCount;
                }
                childCount--;
            }
        }
        return -1;
    }

    private View makeAndAddView(int i, int i2, boolean z, boolean z2) {
        int paddingTop;
        int i3;
        View activeView;
        if (this.mIsVertical) {
            i3 = getPaddingLeft();
            paddingTop = i2;
        } else {
            paddingTop = getPaddingTop();
            i3 = i2;
        }
        if (this.mDataChanged || (activeView = this.mRecycler.getActiveView(i)) == null) {
            View obtainView = obtainView(i, this.mIsScrap);
            setupChild(obtainView, i, paddingTop, i3, z, z2, this.mIsScrap[0]);
            return obtainView;
        }
        setupChild(activeView, i, paddingTop, i3, z, z2, true);
        return activeView;
    }

    private void maybeScroll(int i) {
        if (this.mTouchMode == 3) {
            handleDragChange(i);
        } else if (this.mTouchMode == 5) {
            handleOverScrollChange(i);
        }
    }

    private boolean maybeStartScrolling(int i) {
        boolean z = this.mOverScroll != 0;
        if (Math.abs(i) <= this.mTouchSlop && !z) {
            return false;
        }
        if (z) {
            this.mTouchMode = 5;
        } else {
            this.mTouchMode = 3;
        }
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(true);
        }
        cancelCheckForLongPress();
        setPressed(false);
        View childAt = getChildAt(this.mMotionPosition - this.mFirstPosition);
        if (childAt != null) {
            childAt.setPressed(false);
        }
        reportScrollStateChange(1);
        return true;
    }

    private void measureAndAdjustDown(View view, int i, int i2) {
        int height = view.getHeight();
        measureChild(view);
        if (view.getMeasuredHeight() != height) {
            relayoutMeasuredChild(view);
            int measuredHeight = view.getMeasuredHeight() - height;
            for (int i3 = i + 1; i3 < i2; i3++) {
                getChildAt(i3).offsetTopAndBottom(measuredHeight);
            }
        }
    }

    private void measureChild(View view) {
        measureChild(view, (LayoutParams) view.getLayoutParams());
    }

    private void measureChild(View view, LayoutParams layoutParams) {
        view.measure(getChildWidthMeasureSpec(layoutParams), getChildHeightMeasureSpec(layoutParams));
    }

    private int measureHeightOfChildren(int i, int i2, int i3, int i4, int i5) {
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        ListAdapter listAdapter = this.mAdapter;
        if (listAdapter == null) {
            return paddingTop + paddingBottom;
        }
        int i6 = paddingBottom + paddingTop;
        int i7 = this.mItemMargin;
        int i8 = 0;
        if (i3 == -1) {
            i3 = listAdapter.getCount() - 1;
        }
        RecycleBin recycleBin = this.mRecycler;
        boolean recycleOnMeasure = recycleOnMeasure();
        boolean[] zArr = this.mIsScrap;
        while (i2 <= i3) {
            View obtainView = obtainView(i2, zArr);
            measureScrapChild(obtainView, i2, i);
            if (i2 > 0) {
                i6 += i7;
            }
            if (recycleOnMeasure) {
                recycleBin.addScrapView(obtainView, -1);
            }
            i6 += obtainView.getMeasuredHeight();
            if (i6 >= i4) {
                return (i5 < 0 || i2 <= i5 || i8 <= 0 || i6 == i4) ? i4 : i8;
            }
            if (i5 >= 0 && i2 >= i5) {
                i8 = i6;
            }
            i2++;
        }
        return i6;
    }

    private void measureScrapChild(View view, int i, int i2) {
        int childWidthMeasureSpec;
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = generateDefaultLayoutParams();
            view.setLayoutParams(layoutParams);
        }
        layoutParams.viewType = this.mAdapter.getItemViewType(i);
        layoutParams.forceAdd = true;
        if (this.mIsVertical) {
            int childHeightMeasureSpec = getChildHeightMeasureSpec(layoutParams);
            childWidthMeasureSpec = i2;
            i2 = childHeightMeasureSpec;
        } else {
            childWidthMeasureSpec = getChildWidthMeasureSpec(layoutParams);
        }
        view.measure(childWidthMeasureSpec, i2);
    }

    private int measureWidthOfChildren(int i, int i2, int i3, int i4, int i5) {
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        ListAdapter listAdapter = this.mAdapter;
        if (listAdapter == null) {
            return paddingLeft + paddingRight;
        }
        int i6 = paddingRight + paddingLeft;
        int i7 = this.mItemMargin;
        int i8 = 0;
        if (i3 == -1) {
            i3 = listAdapter.getCount() - 1;
        }
        RecycleBin recycleBin = this.mRecycler;
        boolean recycleOnMeasure = recycleOnMeasure();
        boolean[] zArr = this.mIsScrap;
        while (i2 <= i3) {
            View obtainView = obtainView(i2, zArr);
            measureScrapChild(obtainView, i2, i);
            if (i2 > 0) {
                i6 += i7;
            }
            if (recycleOnMeasure) {
                recycleBin.addScrapView(obtainView, -1);
            }
            i6 += obtainView.getMeasuredWidth();
            if (i6 >= i4) {
                return (i5 < 0 || i2 <= i5 || i8 <= 0 || i6 == i4) ? i4 : i8;
            }
            if (i5 >= 0 && i2 >= i5) {
                i8 = i6;
            }
            i2++;
        }
        return i6;
    }

    private View moveSelection(View view, View view2, int i, int i2, int i3) {
        View makeAndAddView;
        int i4 = this.mSelectedPosition;
        int childStartEdge = getChildStartEdge(view);
        int childEndEdge = getChildEndEdge(view);
        if (i > 0) {
            View makeAndAddView2 = makeAndAddView(i4 - 1, childStartEdge, true, false);
            int i5 = this.mItemMargin;
            makeAndAddView = makeAndAddView(i4, childEndEdge + i5, true, true);
            int childStartEdge2 = getChildStartEdge(makeAndAddView);
            int childEndEdge2 = getChildEndEdge(makeAndAddView);
            if (childEndEdge2 > i3) {
                int min = Math.min(Math.min(childStartEdge2 - i2, childEndEdge2 - i3), (i3 - i2) / 2);
                if (this.mIsVertical) {
                    makeAndAddView2.offsetTopAndBottom(-min);
                    makeAndAddView.offsetTopAndBottom(-min);
                } else {
                    makeAndAddView2.offsetLeftAndRight(-min);
                    makeAndAddView.offsetLeftAndRight(-min);
                }
            }
            fillBefore(this.mSelectedPosition - 2, childStartEdge2 - i5);
            adjustViewsStartOrEnd();
            fillAfter(this.mSelectedPosition + 1, childEndEdge2 + i5);
        } else if (i < 0) {
            if (view2 != null) {
                makeAndAddView = makeAndAddView(i4, this.mIsVertical ? view2.getTop() : view2.getLeft(), true, true);
            } else {
                makeAndAddView = makeAndAddView(i4, childStartEdge, false, true);
            }
            int childStartEdge3 = getChildStartEdge(makeAndAddView);
            int childEndEdge3 = getChildEndEdge(makeAndAddView);
            if (childStartEdge3 < i2) {
                int min2 = Math.min(Math.min(i2 - childStartEdge3, i3 - childEndEdge3), (i3 - i2) / 2);
                if (this.mIsVertical) {
                    makeAndAddView.offsetTopAndBottom(min2);
                } else {
                    makeAndAddView.offsetLeftAndRight(min2);
                }
            }
            fillBeforeAndAfter(makeAndAddView, i4);
        } else {
            makeAndAddView = makeAndAddView(i4, childStartEdge, true, true);
            int childStartEdge4 = getChildStartEdge(makeAndAddView);
            int childEndEdge4 = getChildEndEdge(makeAndAddView);
            if (childStartEdge < i2 && childEndEdge4 < i2 + 20) {
                if (this.mIsVertical) {
                    makeAndAddView.offsetTopAndBottom(i2 - childStartEdge4);
                } else {
                    makeAndAddView.offsetLeftAndRight(i2 - childStartEdge4);
                }
            }
            fillBeforeAndAfter(makeAndAddView, i4);
        }
        return makeAndAddView;
    }

    @TargetApi(16)
    private View obtainView(int i, boolean[] zArr) {
        zArr[0] = false;
        View transientStateView = this.mRecycler.getTransientStateView(i);
        if (transientStateView == null) {
            View scrapView = this.mRecycler.getScrapView(i);
            if (scrapView != null) {
                View view = this.mAdapter.getView(i, scrapView, this);
                if (view != scrapView) {
                    this.mRecycler.addScrapView(scrapView, i);
                    transientStateView = view;
                } else {
                    zArr[0] = true;
                    transientStateView = view;
                }
            } else {
                transientStateView = this.mAdapter.getView(i, null, this);
            }
            if (ViewCompat.getImportantForAccessibility(transientStateView) == 0) {
                ViewCompat.setImportantForAccessibility(transientStateView, 1);
            }
            if (this.mHasStableIds) {
                LayoutParams layoutParams = (LayoutParams) transientStateView.getLayoutParams();
                if (layoutParams == null) {
                    layoutParams = generateDefaultLayoutParams();
                } else if (!checkLayoutParams(layoutParams)) {
                    layoutParams = generateLayoutParams((ViewGroup.LayoutParams) layoutParams);
                }
                layoutParams.id = this.mAdapter.getItemId(i);
                transientStateView.setLayoutParams(layoutParams);
            }
            if (this.mAccessibilityDelegate == null) {
                this.mAccessibilityDelegate = new ListItemAccessibilityDelegate();
            }
            ViewCompat.setAccessibilityDelegate(transientStateView, this.mAccessibilityDelegate);
        }
        return transientStateView;
    }

    private void offsetChildren(int i) {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (this.mIsVertical) {
                childAt.offsetTopAndBottom(i);
            } else {
                childAt.offsetLeftAndRight(i);
            }
        }
    }

    @TargetApi(9)
    private boolean overScrollByInternal(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, boolean z) {
        if (Build.VERSION.SDK_INT < 9) {
            return false;
        }
        return super.overScrollBy(i, i2, i3, i4, i5, i6, i7, i8, z);
    }

    /* access modifiers changed from: private */
    public void performAccessibilityActionsOnSelected() {
        if (getSelectedItemPosition() >= 0) {
            sendAccessibilityEvent(4);
        }
    }

    /* access modifiers changed from: private */
    public boolean performLongPress(View view, int i, long j) {
        AdapterView.OnItemLongClickListener onItemLongClickListener = getOnItemLongClickListener();
        boolean onItemLongClick = onItemLongClickListener != null ? onItemLongClickListener.onItemLongClick(this, view, i, j) : false;
        if (!onItemLongClick) {
            this.mContextMenuInfo = createContextMenuInfo(view, i, j);
            onItemLongClick = super.showContextMenuForChild(this);
        }
        if (onItemLongClick) {
            performHapticFeedback(0);
        }
        return onItemLongClick;
    }

    private int positionOfNewFocus(View view) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (isViewAncestorOf(view, getChildAt(i))) {
                return i + this.mFirstPosition;
            }
        }
        throw new IllegalArgumentException("newFocus is not a child of any of the children of the list!");
    }

    /* access modifiers changed from: private */
    public void positionSelector(int i, View view) {
        if (i != -1) {
            this.mSelectorPosition = i;
        }
        this.mSelectorRect.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        boolean z = this.mIsChildViewEnabled;
        if (view.isEnabled() != z) {
            this.mIsChildViewEnabled = !z;
            if (getSelectedItemPosition() != -1) {
                refreshDrawableState();
            }
        }
    }

    private int reconcileSelectedPosition() {
        int i = this.mSelectedPosition;
        if (i < 0) {
            i = this.mResurrectToPosition;
        }
        return Math.min(Math.max(0, i), this.mItemCount - 1);
    }

    private void recycleVelocityTracker() {
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }
    }

    private void relayoutMeasuredChild(View view) {
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        int paddingLeft = getPaddingLeft();
        int top = view.getTop();
        view.layout(paddingLeft, top, measuredWidth + paddingLeft, measuredHeight + top);
    }

    /* access modifiers changed from: private */
    public void rememberSyncState() {
        if (getChildCount() != 0) {
            this.mNeedSync = true;
            if (this.mSelectedPosition >= 0) {
                View childAt = getChildAt(this.mSelectedPosition - this.mFirstPosition);
                this.mSyncRowId = this.mNextSelectedRowId;
                this.mSyncPosition = this.mNextSelectedPosition;
                if (childAt != null) {
                    this.mSpecificStart = this.mIsVertical ? childAt.getTop() : childAt.getLeft();
                }
                this.mSyncMode = 0;
                return;
            }
            View childAt2 = getChildAt(0);
            ListAdapter adapter = getAdapter();
            if (this.mFirstPosition < 0 || this.mFirstPosition >= adapter.getCount()) {
                this.mSyncRowId = -1;
            } else {
                this.mSyncRowId = adapter.getItemId(this.mFirstPosition);
            }
            this.mSyncPosition = this.mFirstPosition;
            if (childAt2 != null) {
                this.mSpecificStart = this.mIsVertical ? childAt2.getTop() : childAt2.getLeft();
            }
            this.mSyncMode = 1;
        }
    }

    private void reportScrollStateChange(int i) {
        if (i != this.mLastScrollState && this.mOnScrollListener != null) {
            this.mLastScrollState = i;
            this.mOnScrollListener.onScrollStateChanged(this, i);
        }
    }

    private boolean scrollListItemsBy(int i) {
        int i2;
        int childCount = getChildCount();
        if (childCount == 0) {
            return true;
        }
        int childStartEdge = getChildStartEdge(getChildAt(0));
        int childEndEdge = getChildEndEdge(getChildAt(childCount - 1));
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int i3 = this.mIsVertical ? paddingTop : paddingLeft;
        int i4 = i3 - childStartEdge;
        int endEdge = getEndEdge();
        int i5 = childEndEdge - endEdge;
        int height = this.mIsVertical ? (getHeight() - paddingBottom) - paddingTop : (getWidth() - paddingRight) - paddingLeft;
        int max = i < 0 ? Math.max(-(height - 1), i) : Math.min(height - 1, i);
        int i6 = this.mFirstPosition;
        boolean z = i6 == 0 && childStartEdge >= i3 && max >= 0;
        boolean z2 = i6 + childCount == this.mItemCount && childEndEdge <= endEdge && max <= 0;
        if (z || z2) {
            return max != 0;
        }
        boolean isInTouchMode = isInTouchMode();
        if (isInTouchMode) {
            hideSelector();
        }
        int i7 = 0;
        boolean z3 = max < 0;
        if (!z3) {
            int i8 = endEdge - max;
            int i9 = 0;
            for (int i10 = childCount - 1; i10 >= 0; i10--) {
                View childAt = getChildAt(i10);
                if (getChildStartEdge(childAt) <= i8) {
                    break;
                }
                this.mRecycler.addScrapView(childAt, i6 + i10);
                i9 = i2 + 1;
                i7 = i10;
            }
        } else {
            int i11 = (-max) + i3;
            i2 = 0;
            int i12 = 0;
            while (i12 < childCount) {
                View childAt2 = getChildAt(i12);
                if (getChildEndEdge(childAt2) >= i11) {
                    break;
                }
                this.mRecycler.addScrapView(childAt2, i6 + i12);
                i12++;
                i2++;
            }
        }
        this.mBlockLayoutRequests = true;
        if (i2 > 0) {
            detachViewsFromParent(i7, i2);
        }
        if (!awakenScrollbarsInternal()) {
            invalidate();
        }
        offsetChildren(max);
        if (z3) {
            this.mFirstPosition = i2 + this.mFirstPosition;
        }
        int abs = Math.abs(max);
        if (i4 < abs || i5 < abs) {
            fillGap(z3);
        }
        if (!isInTouchMode && this.mSelectedPosition != -1) {
            int i13 = this.mSelectedPosition - this.mFirstPosition;
            if (i13 >= 0 && i13 < getChildCount()) {
                positionSelector(this.mSelectedPosition, getChildAt(i13));
            }
        } else if (this.mSelectorPosition != -1) {
            int i14 = this.mSelectorPosition - this.mFirstPosition;
            if (i14 >= 0 && i14 < getChildCount()) {
                positionSelector(-1, getChildAt(i14));
            }
        } else {
            this.mSelectorRect.setEmpty();
        }
        this.mBlockLayoutRequests = false;
        invokeOnItemScrollListener();
        return false;
    }

    private void selectionChanged() {
        if (getOnItemSelectedListener() != null) {
            if (this.mInLayout || this.mBlockLayoutRequests) {
                if (this.mSelectionNotifier == null) {
                    this.mSelectionNotifier = new SelectionNotifier();
                }
                post(this.mSelectionNotifier);
                return;
            }
            fireOnSelected();
            performAccessibilityActionsOnSelected();
        }
    }

    private void setNextSelectedPositionInt(int i) {
        this.mNextSelectedPosition = i;
        this.mNextSelectedRowId = getItemIdAtPosition(i);
        if (this.mNeedSync && this.mSyncMode == 0 && i >= 0) {
            this.mSyncPosition = i;
            this.mSyncRowId = this.mNextSelectedRowId;
        }
    }

    private void setSelectedPositionInt(int i) {
        this.mSelectedPosition = i;
        this.mSelectedRowId = getItemIdAtPosition(i);
    }

    private void setSelectionInt(int i) {
        boolean z = true;
        setNextSelectedPositionInt(i);
        int i2 = this.mSelectedPosition;
        if (i2 < 0 || !(i == i2 - 1 || i == i2 + 1)) {
            z = false;
        }
        layoutChildren();
        if (z) {
            awakenScrollbarsInternal();
        }
    }

    @TargetApi(11)
    private void setupChild(View view, int i, int i2, int i3, boolean z, boolean z2, boolean z3) {
        boolean z4 = z2 && shouldShowSelector();
        boolean z5 = z4 != view.isSelected();
        int i4 = this.mTouchMode;
        boolean z6 = i4 > 0 && i4 < 3 && this.mMotionPosition == i;
        boolean z7 = z6 != view.isPressed();
        boolean z8 = !z3 || z5 || view.isLayoutRequested();
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        LayoutParams generateDefaultLayoutParams = layoutParams == null ? generateDefaultLayoutParams() : layoutParams;
        generateDefaultLayoutParams.viewType = this.mAdapter.getItemViewType(i);
        if (!z3 || generateDefaultLayoutParams.forceAdd) {
            generateDefaultLayoutParams.forceAdd = false;
            addViewInLayout(view, z ? -1 : 0, generateDefaultLayoutParams, true);
        } else {
            attachViewToParent(view, z ? -1 : 0, generateDefaultLayoutParams);
        }
        if (z5) {
            view.setSelected(z4);
        }
        if (z7) {
            view.setPressed(z6);
        }
        if (!(this.mChoiceMode == ChoiceMode.NONE || this.mCheckStates == null)) {
            if (view instanceof Checkable) {
                ((Checkable) view).setChecked(this.mCheckStates.get(i));
            } else if (Build.VERSION.SDK_INT >= 11) {
                view.setActivated(this.mCheckStates.get(i));
            }
        }
        if (z8) {
            measureChild(view, generateDefaultLayoutParams);
        } else {
            cleanupLayoutState(view);
        }
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        if (this.mIsVertical && !z) {
            i2 -= measuredHeight;
        }
        if (!this.mIsVertical && !z) {
            i3 -= measuredWidth;
        }
        if (z8) {
            view.layout(i3, i2, measuredWidth + i3, measuredHeight + i2);
            return;
        }
        view.offsetLeftAndRight(i3 - view.getLeft());
        view.offsetTopAndBottom(i2 - view.getTop());
    }

    private boolean shouldShowSelector() {
        return (hasFocus() && !isInTouchMode()) || touchModeDrawsInPressedState();
    }

    private boolean touchModeDrawsInPressedState() {
        switch (this.mTouchMode) {
            case 1:
            case 2:
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: private */
    public void triggerCheckForLongPress() {
        if (this.mPendingCheckForLongPress == null) {
            this.mPendingCheckForLongPress = new CheckForLongPress();
        }
        this.mPendingCheckForLongPress.rememberWindowAttachCount();
        postDelayed(this.mPendingCheckForLongPress, (long) ViewConfiguration.getLongPressTimeout());
    }

    private void triggerCheckForTap() {
        if (this.mPendingCheckForTap == null) {
            this.mPendingCheckForTap = new CheckForTap();
        }
        postDelayed(this.mPendingCheckForTap, (long) ViewConfiguration.getTapTimeout());
    }

    private void updateEmptyStatus() {
        if (this.mAdapter == null || this.mAdapter.isEmpty()) {
            if (this.mEmptyView != null) {
                this.mEmptyView.setVisibility(0);
                setVisibility(8);
            } else {
                setVisibility(0);
            }
            if (this.mDataChanged) {
                layout(getLeft(), getTop(), getRight(), getBottom());
                return;
            }
            return;
        }
        if (this.mEmptyView != null) {
            this.mEmptyView.setVisibility(8);
        }
        setVisibility(0);
    }

    @TargetApi(11)
    private void updateOnScreenCheckedViews() {
        int i = this.mFirstPosition;
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            int i3 = i + i2;
            if (childAt instanceof Checkable) {
                ((Checkable) childAt).setChecked(this.mCheckStates.get(i3));
            } else if (Build.VERSION.SDK_INT >= 11) {
                childAt.setActivated(this.mCheckStates.get(i3));
            }
        }
    }

    private void updateOverScrollState(int i, int i2) {
        overScrollByInternal(this.mIsVertical ? 0 : i2, this.mIsVertical ? i2 : 0, this.mIsVertical ? 0 : this.mOverScroll, this.mIsVertical ? this.mOverScroll : 0, 0, 0, this.mIsVertical ? 0 : this.mOverscrollDistance, this.mIsVertical ? this.mOverscrollDistance : 0, true);
        if (Math.abs(this.mOverscrollDistance) == Math.abs(this.mOverScroll) && this.mVelocityTracker != null) {
            this.mVelocityTracker.clear();
        }
        int overScrollMode = ViewCompat.getOverScrollMode(this);
        if (overScrollMode == 0 || (overScrollMode == 1 && !contentFits())) {
            this.mTouchMode = 5;
            float height = ((float) i2) / ((float) (this.mIsVertical ? getHeight() : getWidth()));
            if (i > 0) {
                this.mStartEdge.onPull(height);
                if (!this.mEndEdge.isFinished()) {
                    this.mEndEdge.onRelease();
                }
            } else if (i < 0) {
                this.mEndEdge.onPull(height);
                if (!this.mStartEdge.isFinished()) {
                    this.mStartEdge.onRelease();
                }
            }
            if (i != 0) {
                ViewCompat.postInvalidateOnAnimation(this);
            }
        }
    }

    private void updateSelectorState() {
        if (this.mSelector == null) {
            return;
        }
        if (shouldShowSelector()) {
            this.mSelector.setState(getDrawableState());
        } else {
            this.mSelector.setState(STATE_NOTHING);
        }
    }

    private void useDefaultSelector() {
        setSelector(getResources().getDrawable(17301602));
    }

    /* access modifiers changed from: protected */
    public boolean canAnimate() {
        return super.canAnimate() && this.mItemCount > 0;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public void clearChoices() {
        if (this.mCheckStates != null) {
            this.mCheckStates.clear();
        }
        if (this.mCheckedIdStates != null) {
            this.mCheckedIdStates.clear();
        }
        this.mCheckedItemCount = 0;
    }

    /* access modifiers changed from: protected */
    public int computeHorizontalScrollExtent() {
        int childCount = getChildCount();
        if (childCount == 0) {
            return 0;
        }
        int i = childCount * 100;
        View childAt = getChildAt(0);
        int left = childAt.getLeft();
        int width = childAt.getWidth();
        if (width > 0) {
            i += (left * 100) / width;
        }
        View childAt2 = getChildAt(childCount - 1);
        int right = childAt2.getRight();
        int width2 = childAt2.getWidth();
        return width2 > 0 ? i - (((right - getWidth()) * 100) / width2) : i;
    }

    /* access modifiers changed from: protected */
    public int computeHorizontalScrollOffset() {
        int i = this.mFirstPosition;
        int childCount = getChildCount();
        if (i < 0 || childCount == 0) {
            return 0;
        }
        View childAt = getChildAt(0);
        int left = childAt.getLeft();
        int width = childAt.getWidth();
        if (width > 0) {
            return Math.max((i * 100) - ((left * 100) / width), 0);
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public int computeHorizontalScrollRange() {
        int max = Math.max(this.mItemCount * 100, 0);
        return (this.mIsVertical || this.mOverScroll == 0) ? max : max + Math.abs((int) ((((float) this.mOverScroll) / ((float) getWidth())) * ((float) this.mItemCount) * 100.0f));
    }

    public void computeScroll() {
        if (this.mScroller.computeScrollOffset()) {
            int currY = this.mIsVertical ? this.mScroller.getCurrY() : this.mScroller.getCurrX();
            int i = (int) (((float) currY) - this.mLastTouchPos);
            this.mLastTouchPos = (float) currY;
            boolean scrollListItemsBy = scrollListItemsBy(i);
            if (scrollListItemsBy || this.mScroller.isFinished()) {
                if (scrollListItemsBy) {
                    if (ViewCompat.getOverScrollMode(this) != 2) {
                        if ((i > 0 ? this.mStartEdge : this.mEndEdge).onAbsorb(Math.abs((int) getCurrVelocity()))) {
                            ViewCompat.postInvalidateOnAnimation(this);
                        }
                    }
                    this.mScroller.abortAnimation();
                }
                this.mTouchMode = -1;
                reportScrollStateChange(0);
                return;
            }
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    /* access modifiers changed from: protected */
    public int computeVerticalScrollExtent() {
        int childCount = getChildCount();
        if (childCount == 0) {
            return 0;
        }
        int i = childCount * 100;
        View childAt = getChildAt(0);
        int top = childAt.getTop();
        int height = childAt.getHeight();
        if (height > 0) {
            i += (top * 100) / height;
        }
        View childAt2 = getChildAt(childCount - 1);
        int bottom = childAt2.getBottom();
        int height2 = childAt2.getHeight();
        return height2 > 0 ? i - (((bottom - getHeight()) * 100) / height2) : i;
    }

    /* access modifiers changed from: protected */
    public int computeVerticalScrollOffset() {
        int i = this.mFirstPosition;
        int childCount = getChildCount();
        if (i < 0 || childCount == 0) {
            return 0;
        }
        View childAt = getChildAt(0);
        int top = childAt.getTop();
        int height = childAt.getHeight();
        if (height > 0) {
            return Math.max((i * 100) - ((top * 100) / height), 0);
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public int computeVerticalScrollRange() {
        int max = Math.max(this.mItemCount * 100, 0);
        return (!this.mIsVertical || this.mOverScroll == 0) ? max : max + Math.abs((int) ((((float) this.mOverScroll) / ((float) getHeight())) * ((float) this.mItemCount) * 100.0f));
    }

    /* access modifiers changed from: package-private */
    public void confirmCheckedPositionsById() {
        boolean z;
        this.mCheckStates.clear();
        int i = 0;
        while (i < this.mCheckedIdStates.size()) {
            long keyAt = this.mCheckedIdStates.keyAt(i);
            int intValue = this.mCheckedIdStates.valueAt(i).intValue();
            if (keyAt != this.mAdapter.getItemId(intValue)) {
                int max = Math.max(0, intValue - 20);
                int min = Math.min(intValue + 20, this.mItemCount);
                while (true) {
                    if (max >= min) {
                        z = false;
                        break;
                    } else if (keyAt == this.mAdapter.getItemId(max)) {
                        this.mCheckStates.put(max, true);
                        this.mCheckedIdStates.setValueAt(i, Integer.valueOf(max));
                        z = true;
                        break;
                    } else {
                        max++;
                    }
                }
                if (!z) {
                    this.mCheckedIdStates.delete(keyAt);
                    i--;
                    this.mCheckedItemCount--;
                }
            } else {
                this.mCheckStates.put(intValue, true);
            }
            i++;
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        boolean z = this.mDrawSelectorOnTop;
        if (!z) {
            drawSelector(canvas);
        }
        super.dispatchDraw(canvas);
        if (z) {
            drawSelector(canvas);
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        boolean dispatchKeyEvent = super.dispatchKeyEvent(keyEvent);
        return (dispatchKeyEvent || getFocusedChild() == null || keyEvent.getAction() != 0) ? dispatchKeyEvent : onKeyDown(keyEvent.getKeyCode(), keyEvent);
    }

    /* access modifiers changed from: protected */
    public void dispatchSetPressed(boolean z) {
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        boolean z = false;
        if (this.mStartEdge != null) {
            z = false | drawStartEdge(canvas);
        }
        if (this.mEndEdge != null) {
            z |= drawEndEdge(canvas);
        }
        if (z) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        updateSelectorState();
    }

    /* access modifiers changed from: package-private */
    public void fillGap(boolean z) {
        int childCount = getChildCount();
        if (z) {
            int paddingTop = this.mIsVertical ? getPaddingTop() : getPaddingLeft();
            int childEndEdge = getChildEndEdge(getChildAt(childCount - 1));
            if (childCount > 0) {
                paddingTop = this.mItemMargin + childEndEdge;
            }
            fillAfter(this.mFirstPosition + childCount, paddingTop);
            correctTooHigh(getChildCount());
            return;
        }
        int endEdge = getEndEdge();
        int top = this.mIsVertical ? getChildAt(0).getTop() : getChildAt(0).getLeft();
        if (childCount > 0) {
            endEdge = top - this.mItemMargin;
        }
        fillBefore(this.mFirstPosition - 1, endEdge);
        correctTooLow(getChildCount());
    }

    /* access modifiers changed from: package-private */
    public boolean fullScroll(int i) {
        boolean z = false;
        forceValidFocusDirection(i);
        if (i == 33 || i == 17) {
            if (this.mSelectedPosition != 0) {
                int lookForSelectablePosition = lookForSelectablePosition(0, true);
                if (lookForSelectablePosition >= 0) {
                    this.mLayoutMode = 1;
                    setSelectionInt(lookForSelectablePosition);
                    invokeOnItemScrollListener();
                }
                z = true;
            }
        } else if ((i == 130 || i == 66) && this.mSelectedPosition < this.mItemCount - 1) {
            int lookForSelectablePosition2 = lookForSelectablePosition(this.mItemCount - 1, true);
            if (lookForSelectablePosition2 >= 0) {
                this.mLayoutMode = 3;
                setSelectionInt(lookForSelectablePosition2);
                invokeOnItemScrollListener();
            }
            z = true;
        }
        if (z && !awakenScrollbarsInternal()) {
            awakenScrollbarsInternal();
            invalidate();
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateDefaultLayoutParams() {
        return this.mIsVertical ? new LayoutParams(-1, -2) : new LayoutParams(-2, -1);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    public ListAdapter getAdapter() {
        return this.mAdapter;
    }

    public int getCheckedItemCount() {
        return this.mCheckedItemCount;
    }

    public long[] getCheckedItemIds() {
        if (this.mChoiceMode == ChoiceMode.NONE || this.mCheckedIdStates == null || this.mAdapter == null) {
            return new long[0];
        }
        LongSparseArray<Integer> longSparseArray = this.mCheckedIdStates;
        int size = longSparseArray.size();
        long[] jArr = new long[size];
        for (int i = 0; i < size; i++) {
            jArr[i] = longSparseArray.keyAt(i);
        }
        return jArr;
    }

    public int getCheckedItemPosition() {
        if (this.mChoiceMode == ChoiceMode.SINGLE && this.mCheckStates != null && this.mCheckStates.size() == 1) {
            return this.mCheckStates.keyAt(0);
        }
        return -1;
    }

    public SparseBooleanArray getCheckedItemPositions() {
        if (this.mChoiceMode != ChoiceMode.NONE) {
            return this.mCheckStates;
        }
        return null;
    }

    public ChoiceMode getChoiceMode() {
        return this.mChoiceMode;
    }

    /* access modifiers changed from: protected */
    public ContextMenu.ContextMenuInfo getContextMenuInfo() {
        return this.mContextMenuInfo;
    }

    public int getCount() {
        return this.mItemCount;
    }

    public int getFirstVisiblePosition() {
        return this.mFirstPosition;
    }

    public void getFocusedRect(Rect rect) {
        View selectedView = getSelectedView();
        if (selectedView == null || selectedView.getParent() != this) {
            super.getFocusedRect(rect);
            return;
        }
        selectedView.getFocusedRect(rect);
        offsetDescendantRectToMyCoords(selectedView, rect);
    }

    public int getItemMargin() {
        return this.mItemMargin;
    }

    public boolean getItemsCanFocus() {
        return this.mItemsCanFocus;
    }

    public int getLastVisiblePosition() {
        return (this.mFirstPosition + getChildCount()) - 1;
    }

    public int getMaxScrollAmount() {
        return (int) (MAX_SCROLL_FACTOR * ((float) getHeight()));
    }

    public Orientation getOrientation() {
        return this.mIsVertical ? Orientation.VERTICAL : Orientation.HORIZONTAL;
    }

    public int getPositionForView(View view) {
        while (true) {
            try {
                View view2 = (View) view.getParent();
                if (view2.equals(this)) {
                    break;
                }
                view = view2;
            } catch (ClassCastException e) {
                return -1;
            }
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (getChildAt(i).equals(view)) {
                return i + this.mFirstPosition;
            }
        }
        return -1;
    }

    public long getSelectedItemId() {
        return this.mNextSelectedRowId;
    }

    public int getSelectedItemPosition() {
        return this.mNextSelectedPosition;
    }

    public View getSelectedView() {
        if (this.mItemCount <= 0 || this.mSelectedPosition < 0) {
            return null;
        }
        return getChildAt(this.mSelectedPosition - this.mFirstPosition);
    }

    public Drawable getSelector() {
        return this.mSelector;
    }

    public boolean isItemChecked(int i) {
        if (this.mChoiceMode != ChoiceMode.NONE || this.mCheckStates == null) {
            return false;
        }
        return this.mCheckStates.get(i);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        getViewTreeObserver().addOnTouchModeChangeListener(this);
        if (this.mAdapter != null && this.mDataSetObserver == null) {
            this.mDataSetObserver = new AdapterDataSetObserver();
            this.mAdapter.registerDataSetObserver(this.mDataSetObserver);
            this.mDataChanged = true;
            this.mOldItemCount = this.mItemCount;
            this.mItemCount = this.mAdapter.getCount();
        }
        this.mIsAttached = true;
    }

    /* access modifiers changed from: protected */
    public int[] onCreateDrawableState(int i) {
        if (this.mIsChildViewEnabled) {
            return super.onCreateDrawableState(i);
        }
        int i2 = ENABLED_STATE_SET[0];
        int[] onCreateDrawableState = super.onCreateDrawableState(i + 1);
        int length = onCreateDrawableState.length - 1;
        while (true) {
            if (length < 0) {
                length = -1;
                break;
            } else if (onCreateDrawableState[length] == i2) {
                break;
            } else {
                length--;
            }
        }
        if (length < 0) {
            return onCreateDrawableState;
        }
        System.arraycopy(onCreateDrawableState, length + 1, onCreateDrawableState, length, (onCreateDrawableState.length - length) - 1);
        return onCreateDrawableState;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mRecycler.clear();
        getViewTreeObserver().removeOnTouchModeChangeListener(this);
        if (this.mAdapter != null) {
            this.mAdapter.unregisterDataSetObserver(this.mDataSetObserver);
            this.mDataSetObserver = null;
        }
        if (this.mPerformClick != null) {
            removeCallbacks(this.mPerformClick);
        }
        if (this.mTouchModeReset != null) {
            removeCallbacks(this.mTouchModeReset);
            this.mTouchModeReset.run();
        }
        this.mIsAttached = false;
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean z, int i, Rect rect) {
        int i2;
        int i3;
        int i4 = 0;
        super.onFocusChanged(z, i, rect);
        if (z && this.mSelectedPosition < 0 && !isInTouchMode()) {
            if (!this.mIsAttached && this.mAdapter != null) {
                this.mDataChanged = true;
                this.mOldItemCount = this.mItemCount;
                this.mItemCount = this.mAdapter.getCount();
            }
            resurrectSelection();
        }
        ListAdapter listAdapter = this.mAdapter;
        int i5 = -1;
        if (!(listAdapter == null || !z || rect == null)) {
            rect.offset(getScrollX(), getScrollY());
            if (listAdapter.getCount() < getChildCount() + this.mFirstPosition) {
                this.mLayoutMode = 0;
                layoutChildren();
            }
            Rect rect2 = this.mTempRect;
            int i6 = ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
            int childCount = getChildCount();
            int i7 = this.mFirstPosition;
            int i8 = 0;
            while (i8 < childCount) {
                if (!listAdapter.isEnabled(i7 + i8)) {
                    i2 = i4;
                    i3 = i5;
                } else {
                    View childAt = getChildAt(i8);
                    childAt.getDrawingRect(rect2);
                    offsetDescendantRectToMyCoords(childAt, rect2);
                    int distance = getDistance(rect, rect2, i);
                    if (distance < i6) {
                        i3 = i8;
                        i6 = distance;
                        i2 = this.mIsVertical ? childAt.getTop() : childAt.getLeft();
                    } else {
                        i2 = i4;
                        i3 = i5;
                    }
                }
                i8++;
                i5 = i3;
                i4 = i2;
            }
        }
        if (i5 >= 0) {
            setSelectionFromOffset(this.mFirstPosition + i5, i4);
        } else {
            requestLayout();
        }
    }

    @TargetApi(14)
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(TwoWayView.class.getName());
    }

    @TargetApi(14)
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(TwoWayView.class.getName());
        AccessibilityNodeInfoCompat accessibilityNodeInfoCompat = new AccessibilityNodeInfoCompat(accessibilityNodeInfo);
        if (isEnabled()) {
            if (getFirstVisiblePosition() > 0) {
                accessibilityNodeInfoCompat.addAction(8192);
            }
            if (getLastVisiblePosition() < getCount() - 1) {
                accessibilityNodeInfoCompat.addAction(4096);
            }
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (!this.mIsAttached || this.mAdapter == null) {
            return false;
        }
        switch (motionEvent.getAction() & 255) {
            case 0:
                initOrResetVelocityTracker();
                this.mVelocityTracker.addMovement(motionEvent);
                this.mScroller.abortAnimation();
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                if (!this.mIsVertical) {
                    y = x;
                }
                this.mLastTouchPos = y;
                int findMotionRowOrColumn = findMotionRowOrColumn((int) this.mLastTouchPos);
                this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent, 0);
                this.mTouchRemainderPos = 0.0f;
                if (this.mTouchMode != 4) {
                    if (findMotionRowOrColumn >= 0) {
                        this.mMotionPosition = findMotionRowOrColumn;
                        this.mTouchMode = 0;
                        break;
                    }
                } else {
                    return true;
                }
                break;
            case 1:
            case 3:
                this.mActivePointerId = -1;
                this.mTouchMode = -1;
                recycleVelocityTracker();
                reportScrollStateChange(0);
                break;
            case 2:
                if (this.mTouchMode == 0) {
                    initVelocityTrackerIfNotExists();
                    this.mVelocityTracker.addMovement(motionEvent);
                    int findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent, this.mActivePointerId);
                    if (findPointerIndex < 0) {
                        return false;
                    }
                    float y2 = ((this.mIsVertical ? MotionEventCompat.getY(motionEvent, findPointerIndex) : MotionEventCompat.getX(motionEvent, findPointerIndex)) - this.mLastTouchPos) + this.mTouchRemainderPos;
                    int i = (int) y2;
                    this.mTouchRemainderPos = y2 - ((float) i);
                    if (maybeStartScrolling(i)) {
                        return true;
                    }
                }
                break;
        }
        return false;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return handleKeyEvent(i, 1, keyEvent);
    }

    public boolean onKeyMultiple(int i, int i2, KeyEvent keyEvent) {
        return handleKeyEvent(i, i2, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        return handleKeyEvent(i, 1, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        this.mInLayout = true;
        if (z) {
            int childCount = getChildCount();
            for (int i5 = 0; i5 < childCount; i5++) {
                getChildAt(i5).forceLayout();
            }
            this.mRecycler.markChildrenDirty();
        }
        layoutChildren();
        this.mInLayout = false;
        int paddingLeft = ((i3 - i) - getPaddingLeft()) - getPaddingRight();
        int paddingTop = ((i4 - i2) - getPaddingTop()) - getPaddingBottom();
        if (this.mStartEdge != null && this.mEndEdge != null) {
            if (this.mIsVertical) {
                this.mStartEdge.setSize(paddingLeft, paddingTop);
                this.mEndEdge.setSize(paddingLeft, paddingTop);
                return;
            }
            this.mStartEdge.setSize(paddingTop, paddingLeft);
            this.mEndEdge.setSize(paddingTop, paddingLeft);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        if (this.mSelector == null) {
            useDefaultSelector();
        }
        int mode = View.MeasureSpec.getMode(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2);
        this.mItemCount = this.mAdapter == null ? 0 : this.mAdapter.getCount();
        if (this.mItemCount <= 0 || !(mode == 0 || mode2 == 0)) {
            i3 = 0;
            i4 = 0;
        } else {
            View obtainView = obtainView(0, this.mIsScrap);
            measureScrapChild(obtainView, 0, this.mIsVertical ? i : i2);
            i4 = obtainView.getMeasuredWidth();
            i3 = obtainView.getMeasuredHeight();
            if (recycleOnMeasure()) {
                this.mRecycler.addScrapView(obtainView, -1);
            }
        }
        if (mode == 0) {
            size = getPaddingLeft() + getPaddingRight() + i4;
            if (this.mIsVertical) {
                size += getVerticalScrollbarWidth();
            }
        }
        if (mode2 == 0) {
            size2 = getPaddingTop() + getPaddingBottom() + i3;
            if (!this.mIsVertical) {
                size2 += getHorizontalScrollbarHeight();
            }
        }
        setMeasuredDimension((this.mIsVertical || mode != Integer.MIN_VALUE) ? size : measureWidthOfChildren(i2, 0, -1, size, -1), (!this.mIsVertical || mode2 != Integer.MIN_VALUE) ? size2 : measureHeightOfChildren(i, 0, -1, size2, -1));
    }

    /* access modifiers changed from: protected */
    public void onOverScrolled(int i, int i2, boolean z, boolean z2) {
        boolean z3 = true;
        if (this.mIsVertical && this.mOverScroll != i2) {
            onScrollChanged(getScrollX(), i2, getScrollX(), this.mOverScroll);
            this.mOverScroll = i2;
        } else if (this.mIsVertical || this.mOverScroll == i) {
            z3 = false;
        } else {
            onScrollChanged(i, getScrollY(), this.mOverScroll, getScrollY());
            this.mOverScroll = i;
        }
        if (z3) {
            invalidate();
            awakenScrollbarsInternal();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z;
        Drawable current;
        int i = Integer.MIN_VALUE;
        boolean z2 = false;
        if (!isEnabled()) {
            return isClickable() || isLongClickable();
        }
        if (!this.mIsAttached || this.mAdapter == null) {
            return false;
        }
        initVelocityTrackerIfNotExists();
        this.mVelocityTracker.addMovement(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                if (!this.mDataChanged) {
                    this.mVelocityTracker.clear();
                    this.mScroller.abortAnimation();
                    float x = motionEvent.getX();
                    float y = motionEvent.getY();
                    this.mLastTouchPos = this.mIsVertical ? y : x;
                    int pointToPosition = pointToPosition((int) x, (int) y);
                    this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent, 0);
                    this.mTouchRemainderPos = 0.0f;
                    if (!this.mDataChanged) {
                        if (this.mTouchMode != 4) {
                            if (this.mMotionPosition >= 0 && this.mAdapter.isEnabled(this.mMotionPosition)) {
                                this.mTouchMode = 0;
                                triggerCheckForTap();
                            }
                            this.mMotionPosition = pointToPosition;
                            break;
                        } else {
                            this.mTouchMode = 3;
                            reportScrollStateChange(1);
                            findMotionRowOrColumn((int) this.mLastTouchPos);
                            return true;
                        }
                    }
                }
                break;
            case 1:
                switch (this.mTouchMode) {
                    case 0:
                    case 1:
                    case 2:
                        int i2 = this.mMotionPosition;
                        final View childAt = getChildAt(i2 - this.mFirstPosition);
                        float x2 = motionEvent.getX();
                        float y2 = motionEvent.getY();
                        boolean z3 = this.mIsVertical ? x2 > ((float) getPaddingLeft()) && x2 < ((float) (getWidth() - getPaddingRight())) : y2 > ((float) getPaddingTop()) && y2 < ((float) (getHeight() - getPaddingBottom()));
                        if (childAt != null && !childAt.hasFocusable() && z3) {
                            if (this.mTouchMode != 0) {
                                childAt.setPressed(false);
                            }
                            if (this.mPerformClick == null) {
                                this.mPerformClick = new PerformClick();
                            }
                            final PerformClick performClick = this.mPerformClick;
                            performClick.mClickMotionPosition = i2;
                            performClick.rememberWindowAttachCount();
                            this.mResurrectToPosition = i2;
                            if (this.mTouchMode == 0 || this.mTouchMode == 1) {
                                if (this.mTouchMode == 0) {
                                    cancelCheckForTap();
                                } else {
                                    cancelCheckForLongPress();
                                }
                                this.mLayoutMode = 0;
                                if (this.mDataChanged || !this.mAdapter.isEnabled(i2)) {
                                    this.mTouchMode = -1;
                                    updateSelectorState();
                                } else {
                                    this.mTouchMode = 1;
                                    setPressed(true);
                                    positionSelector(this.mMotionPosition, childAt);
                                    childAt.setPressed(true);
                                    if (!(this.mSelector == null || (current = this.mSelector.getCurrent()) == null || !(current instanceof TransitionDrawable))) {
                                        ((TransitionDrawable) current).resetTransition();
                                    }
                                    if (this.mTouchModeReset != null) {
                                        removeCallbacks(this.mTouchModeReset);
                                    }
                                    this.mTouchModeReset = new Runnable() {
                                        public void run() {
                                            int unused = TwoWayView.this.mTouchMode = -1;
                                            TwoWayView.this.setPressed(false);
                                            childAt.setPressed(false);
                                            if (!TwoWayView.this.mDataChanged) {
                                                performClick.run();
                                            }
                                            Runnable unused2 = TwoWayView.this.mTouchModeReset = null;
                                        }
                                    };
                                    postDelayed(this.mTouchModeReset, (long) ViewConfiguration.getPressedStateDuration());
                                }
                            } else if (!this.mDataChanged && this.mAdapter.isEnabled(i2)) {
                                performClick.run();
                            }
                        }
                        this.mTouchMode = -1;
                        updateSelectorState();
                        z = false;
                        break;
                    case 3:
                        if (!contentFits()) {
                            this.mVelocityTracker.computeCurrentVelocity(QDefine.ONE_SECOND, (float) this.mMaximumVelocity);
                            float yVelocity = this.mIsVertical ? VelocityTrackerCompat.getYVelocity(this.mVelocityTracker, this.mActivePointerId) : VelocityTrackerCompat.getXVelocity(this.mVelocityTracker, this.mActivePointerId);
                            if (Math.abs(yVelocity) < ((float) this.mFlingVelocity)) {
                                this.mTouchMode = -1;
                                reportScrollStateChange(0);
                                z = false;
                                break;
                            } else {
                                this.mTouchMode = 4;
                                reportScrollStateChange(2);
                                Scroller scroller = this.mScroller;
                                int i3 = (int) (this.mIsVertical ? 0.0f : yVelocity);
                                if (!this.mIsVertical) {
                                    yVelocity = 0.0f;
                                }
                                int i4 = (int) yVelocity;
                                int i5 = this.mIsVertical ? 0 : Integer.MIN_VALUE;
                                int i6 = this.mIsVertical ? 0 : ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
                                if (!this.mIsVertical) {
                                    i = 0;
                                }
                                scroller.fling(0, 0, i3, i4, i5, i6, i, this.mIsVertical ? ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED : 0);
                                this.mLastTouchPos = 0.0f;
                                z = true;
                                break;
                            }
                        } else {
                            this.mTouchMode = -1;
                            reportScrollStateChange(0);
                            z = false;
                            break;
                        }
                    case 5:
                        this.mTouchMode = -1;
                        reportScrollStateChange(0);
                    case 4:
                    default:
                        z = false;
                        break;
                }
                cancelCheckForTap();
                cancelCheckForLongPress();
                setPressed(false);
                if (!(this.mStartEdge == null || this.mEndEdge == null)) {
                    z |= this.mStartEdge.onRelease() | this.mEndEdge.onRelease();
                }
                recycleVelocityTracker();
                z2 = z;
                break;
            case 2:
                int findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent, this.mActivePointerId);
                if (findPointerIndex >= 0) {
                    float y3 = this.mIsVertical ? MotionEventCompat.getY(motionEvent, findPointerIndex) : MotionEventCompat.getX(motionEvent, findPointerIndex);
                    if (this.mDataChanged) {
                        layoutChildren();
                    }
                    float f = (y3 - this.mLastTouchPos) + this.mTouchRemainderPos;
                    int i7 = (int) f;
                    this.mTouchRemainderPos = f - ((float) i7);
                    switch (this.mTouchMode) {
                        case 0:
                        case 1:
                        case 2:
                            maybeStartScrolling(i7);
                            break;
                        case 3:
                        case 5:
                            this.mLastTouchPos = y3;
                            maybeScroll(i7);
                            break;
                    }
                } else {
                    return false;
                }
            case 3:
                cancelCheckForTap();
                this.mTouchMode = -1;
                reportScrollStateChange(0);
                setPressed(false);
                View childAt2 = getChildAt(this.mMotionPosition - this.mFirstPosition);
                if (childAt2 != null) {
                    childAt2.setPressed(false);
                }
                if (!(this.mStartEdge == null || this.mEndEdge == null)) {
                    z2 = this.mEndEdge.onRelease() | this.mStartEdge.onRelease();
                }
                recycleVelocityTracker();
                break;
        }
        if (z2) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
        return true;
    }

    public void onTouchModeChanged(boolean z) {
        if (z) {
            hideSelector();
            if (getWidth() > 0 && getHeight() > 0 && getChildCount() > 0) {
                layoutChildren();
            }
            updateSelectorState();
        } else if (this.mTouchMode == 5 && this.mOverScroll != 0) {
            this.mOverScroll = 0;
            finishEdgeGlows();
            invalidate();
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        int i = isInTouchMode() ? 0 : 1;
        if (!z) {
            if (i == 1) {
                this.mResurrectToPosition = this.mSelectedPosition;
            }
        } else if (!(i == this.mLastTouchMode || this.mLastTouchMode == -1)) {
            if (i == 1) {
                resurrectSelection();
            } else {
                hideSelector();
                this.mLayoutMode = 0;
                layoutChildren();
            }
        }
        this.mLastTouchMode = i;
    }

    /* access modifiers changed from: package-private */
    public boolean pageScroll(int i) {
        boolean z;
        int lookForSelectablePosition;
        forceValidFocusDirection(i);
        int i2 = -1;
        if (i == 33 || i == 17) {
            i2 = Math.max(0, (this.mSelectedPosition - getChildCount()) - 1);
            z = false;
        } else if (i == 130 || i == 66) {
            i2 = Math.min(this.mItemCount - 1, (this.mSelectedPosition + getChildCount()) - 1);
            z = true;
        } else {
            z = false;
        }
        if (i2 < 0 || (lookForSelectablePosition = lookForSelectablePosition(i2, z)) < 0) {
            return false;
        }
        this.mLayoutMode = 4;
        this.mSpecificStart = this.mIsVertical ? getPaddingTop() : getPaddingLeft();
        if (z && lookForSelectablePosition > this.mItemCount - getChildCount()) {
            this.mLayoutMode = 3;
        }
        if (!z && lookForSelectablePosition < getChildCount()) {
            this.mLayoutMode = 1;
        }
        setSelectionInt(lookForSelectablePosition);
        invokeOnItemScrollListener();
        if (!awakenScrollbarsInternal()) {
            invalidate();
        }
        return true;
    }

    @TargetApi(16)
    public boolean performAccessibilityAction(int i, Bundle bundle) {
        if (super.performAccessibilityAction(i, bundle)) {
            return true;
        }
        switch (i) {
            case 4096:
                if (!isEnabled() || getLastVisiblePosition() >= getCount() - 1) {
                    return false;
                }
                scrollListItemsBy(this.mIsVertical ? (getHeight() - getPaddingTop()) - getPaddingBottom() : (getWidth() - getPaddingLeft()) - getPaddingRight());
                return true;
            case 8192:
                if (!isEnabled() || this.mFirstPosition <= 0) {
                    return false;
                }
                scrollListItemsBy(-(this.mIsVertical ? (getHeight() - getPaddingTop()) - getPaddingBottom() : (getWidth() - getPaddingLeft()) - getPaddingRight()));
                return true;
            default:
                return false;
        }
    }

    public boolean performItemClick(View view, int i, long j) {
        boolean z = true;
        boolean z2 = false;
        if (this.mChoiceMode == ChoiceMode.MULTIPLE) {
            if (!this.mCheckStates.get(i, false)) {
                z2 = true;
            }
            this.mCheckStates.put(i, z2);
            if (this.mCheckedIdStates != null && this.mAdapter.hasStableIds()) {
                if (z2) {
                    this.mCheckedIdStates.put(this.mAdapter.getItemId(i), Integer.valueOf(i));
                } else {
                    this.mCheckedIdStates.delete(this.mAdapter.getItemId(i));
                }
            }
            if (z2) {
                this.mCheckedItemCount++;
            } else {
                this.mCheckedItemCount--;
            }
        } else if (this.mChoiceMode == ChoiceMode.SINGLE) {
            if (!this.mCheckStates.get(i, false)) {
                this.mCheckStates.clear();
                this.mCheckStates.put(i, true);
                if (this.mCheckedIdStates != null && this.mAdapter.hasStableIds()) {
                    this.mCheckedIdStates.clear();
                    this.mCheckedIdStates.put(this.mAdapter.getItemId(i), Integer.valueOf(i));
                }
                this.mCheckedItemCount = 1;
            } else if (this.mCheckStates.size() == 0 || !this.mCheckStates.valueAt(0)) {
                this.mCheckedItemCount = 0;
            }
        } else {
            z = false;
        }
        if (z) {
            updateOnScreenCheckedViews();
        }
        return super.performItemClick(view, i, j);
    }

    public int pointToPosition(int i, int i2) {
        Rect rect = this.mTouchFrame;
        if (rect == null) {
            this.mTouchFrame = new Rect();
            rect = this.mTouchFrame;
        }
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = getChildAt(childCount);
            if (childAt.getVisibility() == 0) {
                childAt.getHitRect(rect);
                if (rect.contains(i, i2)) {
                    return this.mFirstPosition + childCount;
                }
            }
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public boolean recycleOnMeasure() {
        return true;
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        if (z) {
            recycleVelocityTracker();
        }
        super.requestDisallowInterceptTouchEvent(z);
    }

    public void requestLayout() {
        if (!this.mInLayout && !this.mBlockLayoutRequests) {
            super.requestLayout();
        }
    }

    /* access modifiers changed from: package-private */
    public void resetState() {
        this.mScroller.forceFinished(true);
        removeAllViewsInLayout();
        this.mSelectedStart = 0;
        this.mFirstPosition = 0;
        this.mDataChanged = false;
        this.mNeedSync = false;
        this.mPendingSync = null;
        this.mOldSelectedPosition = -1;
        this.mOldSelectedRowId = Long.MIN_VALUE;
        this.mOverScroll = 0;
        setSelectedPositionInt(-1);
        setNextSelectedPositionInt(-1);
        this.mSelectorPosition = -1;
        this.mSelectorRect.setEmpty();
        invalidate();
    }

    public void restoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.mDataChanged = true;
        this.mSyncHeight = (long) savedState.height;
        if (savedState.selectedId >= 0) {
            this.mNeedSync = true;
            this.mPendingSync = savedState;
            this.mSyncRowId = savedState.selectedId;
            this.mSyncPosition = savedState.position;
            this.mSpecificStart = savedState.viewStart;
            this.mSyncMode = 0;
        } else if (savedState.firstId >= 0) {
            setSelectedPositionInt(-1);
            setNextSelectedPositionInt(-1);
            this.mSelectorPosition = -1;
            this.mNeedSync = true;
            this.mPendingSync = savedState;
            this.mSyncRowId = savedState.firstId;
            this.mSyncPosition = savedState.position;
            this.mSpecificStart = savedState.viewStart;
            this.mSyncMode = 1;
        }
        if (savedState.checkState != null) {
            this.mCheckStates = savedState.checkState;
        }
        if (savedState.checkIdState != null) {
            this.mCheckedIdStates = savedState.checkIdState;
        }
        this.mCheckedItemCount = savedState.checkedItemCount;
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public boolean resurrectSelection() {
        int i;
        boolean z;
        int i2;
        int i3;
        boolean z2 = true;
        int childCount = getChildCount();
        if (childCount <= 0) {
            return false;
        }
        int startEdge = getStartEdge();
        int endEdge = getEndEdge();
        int i4 = this.mFirstPosition;
        int i5 = this.mResurrectToPosition;
        if (i5 < i4 || i5 >= i4 + childCount) {
            if (i5 >= i4) {
                int i6 = (i4 + childCount) - 1;
                int i7 = childCount - 1;
                i = 0;
                while (true) {
                    if (i7 < 0) {
                        i5 = i6;
                        z = false;
                        break;
                    }
                    View childAt = getChildAt(i7);
                    int childStartEdge = getChildStartEdge(childAt);
                    int childEndEdge = getChildEndEdge(childAt);
                    if (i7 == childCount - 1) {
                        i = childStartEdge;
                    }
                    if (childEndEdge <= endEdge) {
                        i = childStartEdge;
                        i5 = i4 + i7;
                        z = false;
                        break;
                    }
                    i7--;
                }
            } else {
                int i8 = 0;
                int i9 = 0;
                while (true) {
                    if (i8 >= childCount) {
                        i2 = i9;
                        i3 = i4;
                        break;
                    }
                    View childAt2 = getChildAt(i8);
                    i2 = this.mIsVertical ? childAt2.getTop() : childAt2.getLeft();
                    if (i8 == 0) {
                        i9 = i2;
                    }
                    if (i2 >= startEdge) {
                        i3 = i4 + i8;
                        break;
                    }
                    i8++;
                }
                i = i2;
                i5 = i3;
                z = true;
            }
        } else {
            View childAt3 = getChildAt(i5 - this.mFirstPosition);
            i = this.mIsVertical ? childAt3.getTop() : childAt3.getLeft();
            z = true;
        }
        this.mResurrectToPosition = -1;
        this.mTouchMode = -1;
        reportScrollStateChange(0);
        this.mSpecificStart = i;
        int lookForSelectablePosition = lookForSelectablePosition(i5, z);
        if (lookForSelectablePosition < i4 || lookForSelectablePosition > getLastVisiblePosition()) {
            lookForSelectablePosition = -1;
        } else {
            this.mLayoutMode = 4;
            updateSelectorState();
            setSelectionInt(lookForSelectablePosition);
            invokeOnItemScrollListener();
        }
        if (lookForSelectablePosition < 0) {
            z2 = false;
        }
        return z2;
    }

    /* access modifiers changed from: package-private */
    public boolean resurrectSelectionIfNeeded() {
        if (this.mSelectedPosition >= 0 || !resurrectSelection()) {
            return false;
        }
        updateSelectorState();
        return true;
    }

    public Parcelable saveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        if (this.mPendingSync != null) {
            savedState.selectedId = this.mPendingSync.selectedId;
            savedState.firstId = this.mPendingSync.firstId;
            savedState.viewStart = this.mPendingSync.viewStart;
            savedState.position = this.mPendingSync.position;
            savedState.height = this.mPendingSync.height;
            return savedState;
        }
        boolean z = getChildCount() > 0 && this.mItemCount > 0;
        long selectedItemId = getSelectedItemId();
        savedState.selectedId = selectedItemId;
        savedState.height = getHeight();
        if (selectedItemId >= 0) {
            savedState.viewStart = this.mSelectedStart;
            savedState.position = getSelectedItemPosition();
            savedState.firstId = -1;
        } else if (!z || this.mFirstPosition <= 0) {
            savedState.viewStart = 0;
            savedState.firstId = -1;
            savedState.position = 0;
        } else {
            View childAt = getChildAt(0);
            savedState.viewStart = this.mIsVertical ? childAt.getTop() : childAt.getLeft();
            int i = this.mFirstPosition;
            if (i >= this.mItemCount) {
                i = this.mItemCount - 1;
            }
            savedState.position = i;
            savedState.firstId = this.mAdapter.getItemId(i);
        }
        if (this.mCheckStates != null) {
            savedState.checkState = cloneCheckStates();
        }
        if (this.mCheckedIdStates != null) {
            LongSparseArray<Integer> longSparseArray = new LongSparseArray<>();
            int size = this.mCheckedIdStates.size();
            for (int i2 = 0; i2 < size; i2++) {
                longSparseArray.put(this.mCheckedIdStates.keyAt(i2), this.mCheckedIdStates.valueAt(i2));
            }
            savedState.checkIdState = longSparseArray;
        }
        savedState.checkedItemCount = this.mCheckedItemCount;
        return savedState;
    }

    public void scrollBy(int i) {
        scrollListItemsBy(i);
    }

    public void sendAccessibilityEvent(int i) {
        if (i == 4096) {
            int firstVisiblePosition = getFirstVisiblePosition();
            int lastVisiblePosition = getLastVisiblePosition();
            if (this.mLastAccessibilityScrollEventFromIndex != firstVisiblePosition || this.mLastAccessibilityScrollEventToIndex != lastVisiblePosition) {
                this.mLastAccessibilityScrollEventFromIndex = firstVisiblePosition;
                this.mLastAccessibilityScrollEventToIndex = lastVisiblePosition;
            } else {
                return;
            }
        }
        super.sendAccessibilityEvent(i);
    }

    public void setAdapter(ListAdapter listAdapter) {
        if (!(this.mAdapter == null || this.mDataSetObserver == null)) {
            this.mAdapter.unregisterDataSetObserver(this.mDataSetObserver);
        }
        resetState();
        this.mRecycler.clear();
        this.mAdapter = listAdapter;
        this.mDataChanged = true;
        this.mOldSelectedPosition = -1;
        this.mOldSelectedRowId = Long.MIN_VALUE;
        if (this.mCheckStates != null) {
            this.mCheckStates.clear();
        }
        if (this.mCheckedIdStates != null) {
            this.mCheckedIdStates.clear();
        }
        if (this.mAdapter != null) {
            this.mOldItemCount = this.mItemCount;
            this.mItemCount = listAdapter.getCount();
            this.mDataSetObserver = new AdapterDataSetObserver();
            this.mAdapter.registerDataSetObserver(this.mDataSetObserver);
            this.mRecycler.setViewTypeCount(listAdapter.getViewTypeCount());
            this.mHasStableIds = listAdapter.hasStableIds();
            this.mAreAllItemsSelectable = listAdapter.areAllItemsEnabled();
            if (this.mChoiceMode != ChoiceMode.NONE && this.mHasStableIds && this.mCheckedIdStates == null) {
                this.mCheckedIdStates = new LongSparseArray<>();
            }
            int lookForSelectablePosition = lookForSelectablePosition(0);
            setSelectedPositionInt(lookForSelectablePosition);
            setNextSelectedPositionInt(lookForSelectablePosition);
            if (this.mItemCount == 0) {
                checkSelectionChanged();
            }
        } else {
            this.mItemCount = 0;
            this.mHasStableIds = false;
            this.mAreAllItemsSelectable = true;
            checkSelectionChanged();
        }
        checkFocus();
        requestLayout();
    }

    public void setChoiceMode(ChoiceMode choiceMode) {
        this.mChoiceMode = choiceMode;
        if (this.mChoiceMode != ChoiceMode.NONE) {
            if (this.mCheckStates == null) {
                this.mCheckStates = new SparseBooleanArray();
            }
            if (this.mCheckedIdStates == null && this.mAdapter != null && this.mAdapter.hasStableIds()) {
                this.mCheckedIdStates = new LongSparseArray<>();
            }
        }
    }

    public void setDrawSelectorOnTop(boolean z) {
        this.mDrawSelectorOnTop = z;
    }

    public void setEmptyView(View view) {
        super.setEmptyView(view);
        this.mEmptyView = view;
        updateEmptyStatus();
    }

    public void setFocusable(boolean z) {
        boolean z2 = true;
        ListAdapter adapter = getAdapter();
        boolean z3 = adapter == null || adapter.getCount() == 0;
        this.mDesiredFocusableState = z;
        if (!z) {
            this.mDesiredFocusableInTouchModeState = false;
        }
        if (!z || z3) {
            z2 = false;
        }
        super.setFocusable(z2);
    }

    public void setFocusableInTouchMode(boolean z) {
        boolean z2 = true;
        ListAdapter adapter = getAdapter();
        boolean z3 = adapter == null || adapter.getCount() == 0;
        this.mDesiredFocusableInTouchModeState = z;
        if (z) {
            this.mDesiredFocusableState = true;
        }
        if (!z || z3) {
            z2 = false;
        }
        super.setFocusableInTouchMode(z2);
    }

    public void setItemChecked(int i, boolean z) {
        if (this.mChoiceMode != ChoiceMode.NONE) {
            if (this.mChoiceMode == ChoiceMode.MULTIPLE) {
                boolean z2 = this.mCheckStates.get(i);
                this.mCheckStates.put(i, z);
                if (this.mCheckedIdStates != null && this.mAdapter.hasStableIds()) {
                    if (z) {
                        this.mCheckedIdStates.put(this.mAdapter.getItemId(i), Integer.valueOf(i));
                    } else {
                        this.mCheckedIdStates.delete(this.mAdapter.getItemId(i));
                    }
                }
                if (z2 != z) {
                    if (z) {
                        this.mCheckedItemCount++;
                    } else {
                        this.mCheckedItemCount--;
                    }
                }
            } else {
                boolean z3 = this.mCheckedIdStates != null && this.mAdapter.hasStableIds();
                if (z || isItemChecked(i)) {
                    this.mCheckStates.clear();
                    if (z3) {
                        this.mCheckedIdStates.clear();
                    }
                }
                if (z) {
                    this.mCheckStates.put(i, true);
                    if (z3) {
                        this.mCheckedIdStates.put(this.mAdapter.getItemId(i), Integer.valueOf(i));
                    }
                    this.mCheckedItemCount = 1;
                } else if (this.mCheckStates.size() == 0 || !this.mCheckStates.valueAt(0)) {
                    this.mCheckedItemCount = 0;
                }
            }
            if (!this.mInLayout && !this.mBlockLayoutRequests) {
                this.mDataChanged = true;
                rememberSyncState();
                requestLayout();
            }
        }
    }

    public void setItemMargin(int i) {
        if (this.mItemMargin != i) {
            this.mItemMargin = i;
            requestLayout();
        }
    }

    public void setItemsCanFocus(boolean z) {
        this.mItemsCanFocus = z;
        if (!z) {
            setDescendantFocusability(393216);
        }
    }

    public void setOnScrollListener(OnScrollListener onScrollListener) {
        this.mOnScrollListener = onScrollListener;
        invokeOnItemScrollListener();
    }

    public void setOrientation(Orientation orientation) {
        boolean z = orientation == Orientation.VERTICAL;
        if (this.mIsVertical != z) {
            this.mIsVertical = z;
            resetState();
            this.mRecycler.clear();
            requestLayout();
        }
    }

    @TargetApi(9)
    public void setOverScrollMode(int i) {
        if (Build.VERSION.SDK_INT >= 9) {
            if (i == 2) {
                this.mStartEdge = null;
                this.mEndEdge = null;
            } else if (this.mStartEdge == null) {
                Context context = getContext();
                this.mStartEdge = new EdgeEffectCompat(context);
                this.mEndEdge = new EdgeEffectCompat(context);
            }
            super.setOverScrollMode(i);
        }
    }

    public void setRecyclerListener(RecyclerListener recyclerListener) {
        RecyclerListener unused = this.mRecycler.mRecyclerListener = recyclerListener;
    }

    public void setSelection(int i) {
        setSelectionFromOffset(i, 0);
    }

    public void setSelectionFromOffset(int i, int i2) {
        if (this.mAdapter != null) {
            if (!isInTouchMode()) {
                i = lookForSelectablePosition(i);
                if (i >= 0) {
                    setNextSelectedPositionInt(i);
                }
            } else {
                this.mResurrectToPosition = i;
            }
            if (i >= 0) {
                this.mLayoutMode = 4;
                if (this.mIsVertical) {
                    this.mSpecificStart = getPaddingTop() + i2;
                } else {
                    this.mSpecificStart = getPaddingLeft() + i2;
                }
                if (this.mNeedSync) {
                    this.mSyncPosition = i;
                    this.mSyncRowId = this.mAdapter.getItemId(i);
                }
                requestLayout();
            }
        }
    }

    public void setSelector(int i) {
        setSelector(getResources().getDrawable(i));
    }

    public void setSelector(Drawable drawable) {
        if (this.mSelector != null) {
            this.mSelector.setCallback(null);
            unscheduleDrawable(this.mSelector);
        }
        this.mSelector = drawable;
        drawable.getPadding(new Rect());
        drawable.setCallback(this);
        updateSelectorState();
    }

    public boolean showContextMenuForChild(View view) {
        int positionForView = getPositionForView(view);
        if (positionForView < 0) {
            return false;
        }
        long itemId = this.mAdapter.getItemId(positionForView);
        AdapterView.OnItemLongClickListener onItemLongClickListener = getOnItemLongClickListener();
        boolean onItemLongClick = onItemLongClickListener != null ? onItemLongClickListener.onItemLongClick(this, view, positionForView, itemId) : false;
        if (onItemLongClick) {
            return onItemLongClick;
        }
        this.mContextMenuInfo = createContextMenuInfo(getChildAt(positionForView - this.mFirstPosition), positionForView, itemId);
        return super.showContextMenuForChild(view);
    }
}
