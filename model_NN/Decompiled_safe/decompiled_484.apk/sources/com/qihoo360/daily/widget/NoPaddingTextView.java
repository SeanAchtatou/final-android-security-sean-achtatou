package com.qihoo360.daily.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.TextView;

public class NoPaddingTextView extends TextView {
    Paint.FontMetricsInt fontMetricsInt;

    public NoPaddingTextView(Context context) {
        super(context);
    }

    public NoPaddingTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NoPaddingTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.fontMetricsInt == null) {
            this.fontMetricsInt = new Paint.FontMetricsInt();
            getPaint().getFontMetricsInt(this.fontMetricsInt);
        }
        canvas.translate(0.0f, (float) (this.fontMetricsInt.top - this.fontMetricsInt.ascent));
        super.onDraw(canvas);
    }
}
