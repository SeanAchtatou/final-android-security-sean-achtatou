package com.qihoo360.daily.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.qihoo360.daily.R;

public class BlockLayout extends ViewGroup {
    private int colCount;
    private int gapSize;
    private int rowCount;

    public class LayoutParams extends ViewGroup.LayoutParams {
        public String block;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.BlockLayout);
            this.block = obtainStyledAttributes.getString(0);
            obtainStyledAttributes.recycle();
        }
    }

    public BlockLayout(Context context) {
        super(context);
    }

    public BlockLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.BlockLayout);
        this.rowCount = obtainStyledAttributes.getInt(1, 0);
        this.colCount = obtainStyledAttributes.getInt(2, 0);
        this.gapSize = obtainStyledAttributes.getDimensionPixelSize(3, 0);
        if (this.rowCount == 0 || this.colCount == 0) {
            throw new IllegalStateException("no rowCount or colCount found!");
        }
        obtainStyledAttributes.recycle();
    }

    public BlockLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int childCount = getChildCount();
        int width = (int) ((((double) (getWidth() - (this.gapSize * (this.colCount - 1)))) * 1.0d) / ((double) this.colCount));
        int height = (int) ((((double) (getHeight() - (this.gapSize * (this.rowCount - 1)))) * 1.0d) / ((double) this.rowCount));
        for (int i5 = 0; i5 < childCount; i5++) {
            View childAt = getChildAt(i5);
            String[] split = ((LayoutParams) childAt.getLayoutParams()).block.split(",");
            int intValue = Integer.valueOf(split[0]).intValue();
            int intValue2 = Integer.valueOf(split[1]).intValue();
            int intValue3 = Integer.valueOf(split[2]).intValue();
            int intValue4 = Integer.valueOf(split[3]).intValue();
            int i6 = intValue * (this.gapSize + width);
            int i7 = intValue2 * (this.gapSize + height);
            childAt.layout(i6, i7, ((intValue3 - 1) * this.gapSize) + (width * intValue3) + i6, ((intValue4 - 1) * this.gapSize) + (height * intValue4) + i7);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int measuredWidth = getMeasuredWidth();
        setMeasuredDimension(measuredWidth, measuredWidth);
    }
}
