package com.qihoo360.daily.widget;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.qihoo360.daily.R;

public class ProgressBarView extends ImageView {
    public ProgressBarView(Context context) {
        super(context);
        setImageResource(R.anim.anim_loading);
    }

    public ProgressBarView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setImageResource(R.anim.anim_loading);
    }

    public ProgressBarView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setImageResource(R.anim.anim_loading);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        startLoading();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopLoading();
    }

    public void startLoading() {
        ((AnimationDrawable) getDrawable()).start();
    }

    public void stopLoading() {
        ((AnimationDrawable) getDrawable()).stop();
    }
}
