package com.qihoo360.daily.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ScrollContentTextView extends View {
    private AnimationRunnable animationRunnable;
    /* access modifiers changed from: private */
    public int currentKeywordIndex;
    private ExecutorService executorAnimation;
    private ExecutorService executorKeyword;
    private int height;
    /* access modifiers changed from: private */
    public boolean isAnimation;
    /* access modifiers changed from: private */
    public boolean isKeywordAnimation;
    /* access modifiers changed from: private */
    public long keywordAnimationTime;
    private int keywordColor;
    private KeywordRunnable keywordRunnable;
    /* access modifiers changed from: private */
    public long keywordShowTime;
    private float keywordSize;
    /* access modifiers changed from: private */
    public List<String> keywords;
    private Paint paint;
    /* access modifiers changed from: private */
    public long startAnimationTime;
    private Type type;
    private Typeface typeface;
    private int width;

    class AnimationRunnable implements Runnable {
        private AnimationRunnable() {
        }

        public void run() {
            while (ScrollContentTextView.this.isAnimation) {
                ScrollContentTextView.this.postInvalidate();
                try {
                    Thread.sleep(15);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class KeywordRunnable implements Runnable {
        private KeywordRunnable() {
        }

        public void run() {
            while (ScrollContentTextView.this.isAnimation) {
                if (ScrollContentTextView.this.isKeywordAnimation) {
                    try {
                        long unused = ScrollContentTextView.this.startAnimationTime = System.currentTimeMillis();
                        Thread.sleep(ScrollContentTextView.this.keywordAnimationTime);
                        boolean unused2 = ScrollContentTextView.this.isKeywordAnimation = false;
                        if (ScrollContentTextView.this.keywords != null) {
                            if (ScrollContentTextView.this.currentKeywordIndex < ScrollContentTextView.this.keywords.size() - 1) {
                                ScrollContentTextView.access$712(ScrollContentTextView.this, 1);
                            } else {
                                int unused3 = ScrollContentTextView.this.currentKeywordIndex = 0;
                            }
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        Thread.sleep(ScrollContentTextView.this.keywordShowTime);
                        boolean unused4 = ScrollContentTextView.this.isKeywordAnimation = true;
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }
    }

    public enum Type {
        oneNextOne,
        oneOverOne
    }

    public ScrollContentTextView(Context context) {
        this(context, null);
    }

    public ScrollContentTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ScrollContentTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.keywordColor = ViewCompat.MEASURED_STATE_MASK;
        this.keywordSize = 30.0f;
        this.isAnimation = false;
        this.executorKeyword = Executors.newSingleThreadExecutor();
        this.keywordShowTime = 300;
        this.keywordAnimationTime = 400;
        this.isKeywordAnimation = false;
        this.currentKeywordIndex = 0;
        this.executorAnimation = Executors.newSingleThreadExecutor();
        this.type = Type.oneOverOne;
        this.typeface = Typeface.DEFAULT;
        this.height = 100;
        this.width = 100;
        this.paint = new Paint();
        setBackgroundColor(-1);
        setBackgroundColor(0);
    }

    static /* synthetic */ int access$712(ScrollContentTextView scrollContentTextView, int i) {
        int i2 = scrollContentTextView.currentKeywordIndex + i;
        scrollContentTextView.currentKeywordIndex = i2;
        return i2;
    }

    public long getKeywordAnimationTime() {
        return this.keywordAnimationTime;
    }

    public long getKeywordShowTime() {
        return this.keywordShowTime;
    }

    public List<String> getKeywords() {
        return this.keywords;
    }

    public float getTextSize() {
        return this.keywordSize;
    }

    public Type getType() {
        return this.type;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        float f;
        int i = 0;
        super.onDraw(canvas);
        int width2 = getWidth() / 2;
        float height2 = (((float) (getHeight() / 2)) + (this.keywordSize / 2.0f)) - 5.0f;
        this.paint.setAntiAlias(true);
        this.paint.setStrokeWidth(0.0f);
        this.paint.setColor(this.keywordColor);
        this.paint.setTextSize(this.keywordSize);
        if (this.typeface != null) {
            this.paint.setTypeface(this.typeface);
        }
        if (this.keywords != null && this.keywords.size() > 0) {
            if (this.currentKeywordIndex >= this.keywords.size()) {
                this.currentKeywordIndex = 0;
            }
            String trim = this.keywords.get(this.currentKeywordIndex).trim();
            if (this.isKeywordAnimation) {
                switch (this.type) {
                    case oneNextOne:
                        f = ((float) this.height) + this.keywordSize;
                        break;
                    default:
                        f = (float) this.height;
                        break;
                }
                float measureText = this.paint.measureText(trim);
                float currentTimeMillis = ((float) (System.currentTimeMillis() - this.startAnimationTime)) / ((float) this.keywordAnimationTime);
                canvas.drawText(trim, ((float) width2) - (measureText / 2.0f), height2 - (f * currentTimeMillis), this.paint);
                if (this.currentKeywordIndex < this.keywords.size() - 1) {
                    i = this.currentKeywordIndex + 1;
                }
                canvas.drawText(this.keywords.get(i), ((float) width2) - (this.paint.measureText(this.keywords.get(i).trim()) / 2.0f), (f + height2) - (f * currentTimeMillis), this.paint);
                return;
            }
            canvas.drawText(trim, ((float) width2) - (this.paint.measureText(trim) / 2.0f), height2, this.paint);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3 = 0;
        int makeMeasureSpec = this.height > 0 ? View.MeasureSpec.makeMeasureSpec(this.height, 1073741824) : 0;
        if (this.width > 0) {
            i3 = View.MeasureSpec.makeMeasureSpec(this.width, 1073741824);
        }
        super.onMeasure(i3, makeMeasureSpec);
    }

    public void setHeight(int i) {
        this.height = i;
    }

    public void setKeywordAnimationTime(long j) {
        this.keywordAnimationTime = j;
    }

    public void setKeywordShowTime(long j) {
        this.keywordShowTime = j;
    }

    public void setKeywords(List<String> list) {
        this.keywords = list;
    }

    public void setTextColor(int i) {
        this.keywordColor = i;
    }

    public void setTextSize(float f) {
        this.keywordSize = f;
    }

    public void setTextTypeface(Typeface typeface2) {
        this.typeface = typeface2;
    }

    public void setType(Type type2) {
        this.type = type2;
    }

    public void setWidth(int i) {
        this.width = i;
    }

    public void start() {
        if (!this.isAnimation) {
            this.isAnimation = true;
            if (this.keywordRunnable == null) {
                this.keywordRunnable = new KeywordRunnable();
            }
            this.executorKeyword.execute(this.keywordRunnable);
            if (this.animationRunnable == null) {
                this.animationRunnable = new AnimationRunnable();
            }
            this.executorAnimation.execute(this.animationRunnable);
        }
    }

    public void stop() {
        if (this.executorAnimation != null && !this.executorAnimation.isShutdown()) {
            this.executorAnimation.shutdownNow();
        }
    }
}
