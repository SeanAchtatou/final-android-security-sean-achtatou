package com.qihoo360.daily.widget.webview;

public interface JavascriptInterface {
    @android.webkit.JavascriptInterface
    void OnClickBury();

    @android.webkit.JavascriptInterface
    void OnClickCmt();

    @android.webkit.JavascriptInterface
    void OnClickCmtLike(String str);

    @android.webkit.JavascriptInterface
    void OnClickCmtMore();

    @android.webkit.JavascriptInterface
    void OnClickDigg();

    @android.webkit.JavascriptInterface
    void OnClickImg(String str, int i, int i2, int i3, int i4);

    @android.webkit.JavascriptInterface
    void OnClickRelated(String str);

    @android.webkit.JavascriptInterface
    void OnClickText();

    @android.webkit.JavascriptInterface
    void comment();

    @android.webkit.JavascriptInterface
    void setData(String str, String str2);
}
