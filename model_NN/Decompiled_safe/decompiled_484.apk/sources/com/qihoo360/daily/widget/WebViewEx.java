package com.qihoo360.daily.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.webkit.JsPromptResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class WebViewEx extends MyWebView {
    private static final boolean DEBUG = true;
    private static final String KEY_ARG_ARRAY = "args";
    private static final String KEY_FUNCTION_NAME = "func";
    private static final String KEY_INTERFACE_NAME = "obj";
    private static final String MSG_PROMPT_HEADER = "MyApp:";
    private static final String VAR_ARG_PREFIX = "arg";
    private static final String[] mFilterMethods = {"getClass", "hashCode", "notify", "notifyAll", "equals", "toString", "wait"};
    private HashMap<String, Object> mJsInterfaceMap = new HashMap<>();
    private String mJsStringCache = null;
    /* access modifiers changed from: private */
    public ProgressListener progressListener;
    /* access modifiers changed from: private */
    public WebViewClientListener webViewClientListener;

    public interface ProgressListener {
        void onProgressChanged(WebView webView, int i);
    }

    public class WebChromeClientEx extends WebChromeClient {
        public WebChromeClientEx() {
        }

        public final boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
            return (!(webView instanceof WebViewEx) || !WebViewEx.this.handleJsInterface(webView, str, str2, str3, jsPromptResult)) ? super.onJsPrompt(webView, str, str2, str3, jsPromptResult) : WebViewEx.DEBUG;
        }

        public final void onProgressChanged(WebView webView, int i) {
            WebViewEx.this.injectJavascriptInterfaces(webView);
            super.onProgressChanged(webView, i);
            if (WebViewEx.this.progressListener != null) {
                WebViewEx.this.progressListener.onProgressChanged(webView, i);
            }
        }

        public final void onReceivedTitle(WebView webView, String str) {
            WebViewEx.this.injectJavascriptInterfaces(webView);
        }
    }

    public class WebViewClientEx extends WebViewClient {
        public WebViewClientEx() {
        }

        public void doUpdateVisitedHistory(WebView webView, String str, boolean z) {
            WebViewEx.this.injectJavascriptInterfaces(webView);
            super.doUpdateVisitedHistory(webView, str, z);
        }

        public void onLoadResource(WebView webView, String str) {
            WebViewEx.this.injectJavascriptInterfaces(webView);
            super.onLoadResource(webView, str);
        }

        public void onPageFinished(WebView webView, String str) {
            WebViewEx.this.injectJavascriptInterfaces(webView);
            super.onPageFinished(webView, str);
            if (WebViewEx.this.webViewClientListener != null) {
                WebViewEx.this.webViewClientListener.onPageFinished(webView, str);
            }
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            WebViewEx.this.injectJavascriptInterfaces(webView);
            super.onPageStarted(webView, str, bitmap);
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            super.onReceivedError(webView, i, str, str2);
            if (WebViewEx.this.webViewClientListener != null) {
                WebViewEx.this.webViewClientListener.onReceivedError(webView, i, str, str2);
            }
        }

        @TargetApi(11)
        public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
            return WebViewEx.this.webViewClientListener != null ? WebViewEx.this.webViewClientListener.shouldInterceptRequest(webView, str) : super.shouldInterceptRequest(webView, str);
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            return WebViewEx.this.webViewClientListener != null ? WebViewEx.this.webViewClientListener.shouldOverrideUrlLoading(webView, str) : super.shouldOverrideUrlLoading(webView, str);
        }
    }

    public interface WebViewClientListener {
        void onPageFinished(WebView webView, String str);

        void onReceivedError(WebView webView, int i, String str, String str2);

        WebResourceResponse shouldInterceptRequest(WebView webView, String str);

        boolean shouldOverrideUrlLoading(WebView webView, String str);
    }

    public WebViewEx(Context context) {
        super(context);
        init(context);
    }

    public WebViewEx(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    public WebViewEx(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init(context);
    }

    private void createJsMethod(String str, Object obj, StringBuilder sb) {
        if (!TextUtils.isEmpty(str) && obj != null && sb != null) {
            Class<?> cls = obj.getClass();
            sb.append("if(typeof(window.").append(str).append(")!='undefined'){");
            sb.append("    console.log('window." + str + "_js_interface_name is exist!!');");
            sb.append("}else {");
            sb.append("    window.").append(str).append("={");
            for (Method method : cls.getMethods()) {
                String name = method.getName();
                if (!filterMethods(name)) {
                    sb.append("        ").append(name).append(":function(");
                    int length = method.getParameterTypes().length;
                    if (length > 0) {
                        int i = length - 1;
                        for (int i2 = 0; i2 < i; i2++) {
                            sb.append(VAR_ARG_PREFIX).append(i2).append(",");
                        }
                        sb.append(VAR_ARG_PREFIX).append(length - 1);
                    }
                    sb.append(") {");
                    if (method.getReturnType() != Void.TYPE) {
                        sb.append("            return ").append("prompt('").append(MSG_PROMPT_HEADER).append("'+");
                    } else {
                        sb.append("            prompt('").append(MSG_PROMPT_HEADER).append("'+");
                    }
                    sb.append("JSON.stringify({");
                    sb.append(KEY_INTERFACE_NAME).append(":'").append(str).append("',");
                    sb.append(KEY_FUNCTION_NAME).append(":'").append(name).append("',");
                    sb.append(KEY_ARG_ARRAY).append(":[");
                    if (length > 0) {
                        int i3 = length - 1;
                        for (int i4 = 0; i4 < i3; i4++) {
                            sb.append(VAR_ARG_PREFIX).append(i4).append(",");
                        }
                        sb.append(VAR_ARG_PREFIX).append(i3);
                    }
                    sb.append("]})");
                    sb.append(");");
                    sb.append("        }, ");
                }
            }
            sb.append("    };");
            sb.append("}");
        }
    }

    private boolean filterMethods(String str) {
        for (String equals : mFilterMethods) {
            if (equals.equals(str)) {
                return DEBUG;
            }
        }
        return false;
    }

    private String genJavascriptInterfacesString() {
        if (this.mJsInterfaceMap.size() == 0) {
            this.mJsStringCache = null;
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("javascript:(function JsAddJavascriptInterface_(){");
        for (Map.Entry next : this.mJsInterfaceMap.entrySet()) {
            try {
                createJsMethod((String) next.getKey(), next.getValue(), sb);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        sb.append("})()");
        return sb.toString();
    }

    private Class<?> getClassFromJsonObject(Object obj) {
        Class<?> cls = obj.getClass();
        return cls == Integer.class ? Integer.TYPE : cls == Boolean.class ? Boolean.TYPE : String.class;
    }

    /* access modifiers changed from: private */
    public boolean handleJsInterface(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        int length;
        if (!str2.startsWith(MSG_PROMPT_HEADER)) {
            return false;
        }
        try {
            JSONObject jSONObject = new JSONObject(str2.substring(MSG_PROMPT_HEADER.length()));
            String string = jSONObject.getString(KEY_INTERFACE_NAME);
            String string2 = jSONObject.getString(KEY_FUNCTION_NAME);
            JSONArray jSONArray = jSONObject.getJSONArray(KEY_ARG_ARRAY);
            Object[] objArr = null;
            if (jSONArray != null && (length = jSONArray.length()) > 0) {
                objArr = new Object[length];
                for (int i = 0; i < length; i++) {
                    objArr[i] = jSONArray.get(i);
                }
            }
            if (invokeJSInterfaceMethod(jsPromptResult, string, string2, objArr)) {
                return DEBUG;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        jsPromptResult.cancel();
        return false;
    }

    private boolean hasHoneycomb() {
        if (Build.VERSION.SDK_INT >= 11) {
            return DEBUG;
        }
        return false;
    }

    private boolean hasJellyBeanMR1() {
        if (Build.VERSION.SDK_INT >= 17) {
            return DEBUG;
        }
        return false;
    }

    private void init(Context context) {
        super.setWebChromeClient(new WebChromeClientEx());
        super.setWebViewClient(new WebViewClientEx());
        removeSearchBoxImpl();
    }

    private void injectJavascriptInterfaces() {
        if (!TextUtils.isEmpty(this.mJsStringCache)) {
            loadJavascriptInterfaces();
            return;
        }
        this.mJsStringCache = genJavascriptInterfacesString();
        loadJavascriptInterfaces();
    }

    /* access modifiers changed from: private */
    public void injectJavascriptInterfaces(WebView webView) {
        if (webView instanceof WebViewEx) {
            injectJavascriptInterfaces();
        }
    }

    private boolean invokeJSInterfaceMethod(JsPromptResult jsPromptResult, String str, String str2, Object[] objArr) {
        boolean z = DEBUG;
        Object obj = this.mJsInterfaceMap.get(str);
        if (obj == null) {
            jsPromptResult.cancel();
            return false;
        }
        Class[] clsArr = null;
        int length = objArr != null ? objArr.length : 0;
        if (length > 0) {
            clsArr = new Class[length];
            for (int i = 0; i < length; i++) {
                clsArr[i] = getClassFromJsonObject(objArr[i]);
            }
        }
        try {
            Object invoke = obj.getClass().getMethod(str2, clsArr).invoke(obj, objArr);
            jsPromptResult.confirm(invoke == null || invoke.getClass() == Void.TYPE ? "" : invoke.toString());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            z = false;
        } catch (Exception e2) {
            e2.printStackTrace();
            z = false;
        }
        jsPromptResult.cancel();
        return z;
    }

    private void loadJavascriptInterfaces() {
        loadUrl(this.mJsStringCache);
    }

    @TargetApi(11)
    private boolean removeSearchBoxImpl() {
        if (!hasHoneycomb() || hasJellyBeanMR1()) {
            return false;
        }
        super.removeJavascriptInterface("searchBoxJavaBridge_");
        return DEBUG;
    }

    public void addJavascriptInterface(Object obj, String str) {
        if (!TextUtils.isEmpty(str)) {
            if (hasJellyBeanMR1()) {
                super.addJavascriptInterface(obj, str);
            } else {
                this.mJsInterfaceMap.put(str, obj);
            }
        }
    }

    @TargetApi(11)
    public void removeJavascriptInterface(String str) {
        if (hasJellyBeanMR1()) {
            super.removeJavascriptInterface(str);
            return;
        }
        this.mJsInterfaceMap.remove(str);
        this.mJsStringCache = null;
        injectJavascriptInterfaces();
    }

    public void setProgressListener(ProgressListener progressListener2) {
        this.progressListener = progressListener2;
    }

    public void setWebViewClientListener(WebViewClientListener webViewClientListener2) {
        this.webViewClientListener = webViewClientListener2;
    }
}
