package com.qihoo360.daily.widget;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.qihoo360.daily.activity.SearchActivity;

@TargetApi(11)
public class FindWebView extends WebView {
    public ActionMode.Callback callback;
    private OnScrollChangedListener mOnScrollChangedListener;

    public class CustomizedSelectActionModeCallback implements ActionMode.Callback {
        private ActionMode.Callback callback;

        public CustomizedSelectActionModeCallback(ActionMode.Callback callback2) {
            this.callback = callback2;
        }

        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            if (menuItem == null || TextUtils.isEmpty(menuItem.getTitle())) {
                return this.callback.onActionItemClicked(actionMode, menuItem);
            }
            if (!menuItem.getTitle().toString().contains("搜索") && !menuItem.getTitle().toString().contains("search")) {
                return this.callback.onActionItemClicked(actionMode, menuItem);
            }
            FindWebView.this.loadUrl("javascript:window.search.show(window.getSelection().toString());");
            FindWebView.this.clearFocus();
            return true;
        }

        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            return this.callback.onCreateActionMode(actionMode, menu);
        }

        public void onDestroyActionMode(ActionMode actionMode) {
            this.callback.onDestroyActionMode(actionMode);
        }

        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return this.callback.onPrepareActionMode(actionMode, menu);
        }
    }

    public interface OnScrollChangedListener {
        void onScroll(int i, int i2, int i3, int i4);
    }

    public class SelectedText {
        public SelectedText() {
        }

        @JavascriptInterface
        public void show(String str) {
            Intent intent = new Intent(FindWebView.this.getContext(), SearchActivity.class);
            intent.putExtra(SearchActivity.TAG_SEARCH, str);
            FindWebView.this.getContext().startActivity(intent);
        }
    }

    public FindWebView(Context context) {
        super(context);
        init();
    }

    public FindWebView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public FindWebView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    @TargetApi(19)
    private void init() {
        WebSettings settings = getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        addJavascriptInterface(new SelectedText(), "search");
        if (Build.VERSION.SDK_INT >= 19) {
        }
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
        if (this.mOnScrollChangedListener != null) {
            this.mOnScrollChangedListener.onScroll(i, i2, i3, i4);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return super.onTouchEvent(motionEvent);
    }

    public void setOnScrollChangedListener(OnScrollChangedListener onScrollChangedListener) {
        this.mOnScrollChangedListener = onScrollChangedListener;
    }

    public ActionMode startActionMode(ActionMode.Callback callback2) {
        return super.startActionMode(new CustomizedSelectActionModeCallback(callback2));
    }
}
