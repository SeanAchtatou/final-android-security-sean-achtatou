package com.qihoo360.daily.widget;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.View;
import java.lang.reflect.Field;

public class FixedSwipeRefreshLayout extends SwipeRefreshLayout {
    private View mTarget;

    public FixedSwipeRefreshLayout(Context context) {
        super(context);
    }

    public FixedSwipeRefreshLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    private View getTargetByReflection() {
        try {
            Field declaredField = SwipeRefreshLayout.class.getDeclaredField("mTarget");
            declaredField.setAccessible(true);
            return (View) declaredField.get(this);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public boolean canChildScrollUp() {
        if (this.mTarget == null) {
            this.mTarget = getTargetByReflection();
        }
        return (this.mTarget == null || !(this.mTarget instanceof PullRecyclerView)) ? super.canChildScrollUp() : !((PullRecyclerView) this.mTarget).isPullDownAble();
    }
}
