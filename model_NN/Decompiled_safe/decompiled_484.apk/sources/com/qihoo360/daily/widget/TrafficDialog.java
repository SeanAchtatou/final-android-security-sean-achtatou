package com.qihoo360.daily.widget;

import android.app.Activity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.qihoo360.daily.R;

public class TrafficDialog extends DialogView implements View.OnClickListener {
    private TextView btLeft = ((TextView) this.view.findViewById(R.id.btn_dialog_left));
    private TextView btRight = ((TextView) this.view.findViewById(R.id.btn_dialog_right));
    /* access modifiers changed from: private */
    public CheckBox cbChoose = ((CheckBox) this.view.findViewById(R.id.cb_choose));
    /* access modifiers changed from: private */
    public OnDialogViewClickListener dialogViewClick;
    private TextView tvContent = ((TextView) this.view.findViewById(R.id.tv_dialog));

    public interface OnDialogViewClickListener {
        void onChooseClick(boolean z);

        void onLeftButtonClick();

        void onRightButtonClick();
    }

    public TrafficDialog(Activity activity, boolean z) {
        super(activity, (int) R.layout.dialog_traffic);
        this.btLeft.setOnClickListener(this);
        this.btRight.setOnClickListener(this);
        this.cbChoose.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                if (TrafficDialog.this.dialogViewClick != null) {
                    TrafficDialog.this.dialogViewClick.onChooseClick(TrafficDialog.this.cbChoose.isChecked());
                }
                if (z) {
                    TrafficDialog.this.cbChoose.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.cb_checked, 0, 0, 0);
                } else {
                    TrafficDialog.this.cbChoose.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.cb_uncheck, 0, 0, 0);
                }
            }
        });
        this.cbChoose.setChecked(z);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_dialog_left:
                if (this.dialogViewClick != null) {
                    this.dialogViewClick.onLeftButtonClick();
                    break;
                }
                break;
            case R.id.btn_dialog_right:
                if (this.dialogViewClick != null) {
                    this.dialogViewClick.onRightButtonClick();
                    break;
                }
                break;
        }
        disMissDialog();
    }

    public void setContentText(int i) {
        this.tvContent.setText(i);
    }

    public void setContentText(String str) {
        this.tvContent.setText(str);
    }

    public void setLeftButtonText(int i) {
        this.btRight.setText(i);
    }

    public void setLeftButtonText(String str) {
        this.btLeft.setText(str);
    }

    public void setOnDialogViewClickListener(OnDialogViewClickListener onDialogViewClickListener) {
        this.dialogViewClick = onDialogViewClickListener;
    }

    public void setRightButtonText(int i) {
        this.btRight.setText(i);
    }

    public void setRightButtonText(String str) {
        this.btLeft.setText(str);
    }
}
