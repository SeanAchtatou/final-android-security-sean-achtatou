package com.qihoo360.daily.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.view.ViewCompat;
import android.view.MotionEvent;
import android.widget.Adapter;
import android.widget.ListView;
import android.widget.SectionIndexer;

public class IndexScroller {
    private static final int STATE_HIDDEN = 0;
    private static final int STATE_HIDING = 3;
    private static final int STATE_SHOWING = 1;
    private static final int STATE_SHOWN = 2;
    /* access modifiers changed from: private */
    public float mAlphaRate;
    /* access modifiers changed from: private */
    public boolean mAutoHide;
    private int mCurrentSection = -1;
    private float mDensity;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            switch (IndexScroller.this.mState) {
                case 1:
                    IndexScroller.access$118(IndexScroller.this, ((double) (1.0f - IndexScroller.this.mAlphaRate)) * 0.2d);
                    if (((double) IndexScroller.this.mAlphaRate) > 0.9d) {
                        float unused = IndexScroller.this.mAlphaRate = 1.0f;
                        IndexScroller.this.setState(2);
                    }
                    IndexScroller.this.mListView.invalidate();
                    IndexScroller.this.fade(10);
                    return;
                case 2:
                    if (IndexScroller.this.mAutoHide) {
                        IndexScroller.this.setState(3);
                        return;
                    }
                    return;
                case 3:
                    IndexScroller.access$126(IndexScroller.this, ((double) IndexScroller.this.mAlphaRate) * 0.2d);
                    if (((double) IndexScroller.this.mAlphaRate) < 0.1d) {
                        float unused2 = IndexScroller.this.mAlphaRate = 0.0f;
                        IndexScroller.this.setState(0);
                    }
                    IndexScroller.this.mListView.invalidate();
                    IndexScroller.this.fade(10);
                    return;
                default:
                    return;
            }
        }
    };
    private float mIndexbarMargin;
    private RectF mIndexbarRect;
    private float mIndexbarWidth;
    private SectionIndexer mIndexer = null;
    private boolean mIsIndexing = false;
    /* access modifiers changed from: private */
    public ListView mListView = null;
    private int mListViewHeight;
    private int mListViewWidth;
    private float mPreviewPadding;
    private float mScaledDensity;
    private String[] mSections = null;
    /* access modifiers changed from: private */
    public int mState = 0;

    public IndexScroller(Context context, ListView listView) {
        this.mDensity = context.getResources().getDisplayMetrics().density;
        this.mScaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        this.mListView = listView;
        setAdapter(this.mListView.getAdapter());
        this.mIndexbarWidth = 20.0f * this.mDensity;
        this.mIndexbarMargin = 10.0f * this.mDensity;
        this.mPreviewPadding = 5.0f * this.mDensity;
    }

    static /* synthetic */ float access$118(IndexScroller indexScroller, double d) {
        float f = (float) (((double) indexScroller.mAlphaRate) + d);
        indexScroller.mAlphaRate = f;
        return f;
    }

    static /* synthetic */ float access$126(IndexScroller indexScroller, double d) {
        float f = (float) (((double) indexScroller.mAlphaRate) - d);
        indexScroller.mAlphaRate = f;
        return f;
    }

    private boolean contains(float f, float f2) {
        return f >= this.mIndexbarRect.left && f2 >= this.mIndexbarRect.top && f2 <= this.mIndexbarRect.top + this.mIndexbarRect.height();
    }

    /* access modifiers changed from: private */
    public void fade(long j) {
        this.mHandler.removeMessages(0);
        this.mHandler.sendEmptyMessageAtTime(0, SystemClock.uptimeMillis() + j);
    }

    private int getSectionByPoint(float f) {
        if (this.mSections == null || this.mSections.length == 0 || f < this.mIndexbarRect.top + this.mIndexbarMargin) {
            return 0;
        }
        return f >= (this.mIndexbarRect.top + this.mIndexbarRect.height()) - this.mIndexbarMargin ? this.mSections.length - 1 : (int) (((f - this.mIndexbarRect.top) - this.mIndexbarMargin) / ((this.mIndexbarRect.height() - (2.0f * this.mIndexbarMargin)) / ((float) this.mSections.length)));
    }

    /* access modifiers changed from: private */
    public void setState(int i) {
        if (i >= 0 && i <= 3) {
            this.mState = i;
            switch (this.mState) {
                case 0:
                    this.mHandler.removeMessages(0);
                    return;
                case 1:
                    this.mAlphaRate = 0.0f;
                    fade(0);
                    return;
                case 2:
                    this.mHandler.removeMessages(0);
                    return;
                case 3:
                    this.mAlphaRate = 1.0f;
                    fade(30);
                    return;
                default:
                    return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [int, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    public void draw(Canvas canvas) {
        if (this.mState != 0 && this.mSections != null && this.mSections.length > 0) {
            if (this.mCurrentSection >= 0) {
                Paint paint = new Paint();
                paint.setColor((int) ViewCompat.MEASURED_STATE_MASK);
                paint.setAlpha(96);
                paint.setAntiAlias(true);
                paint.setShadowLayer(3.0f, 0.0f, 0.0f, Color.argb(64, 0, 0, 0));
                Paint paint2 = new Paint();
                paint2.setColor(-1);
                paint2.setAntiAlias(true);
                paint2.setTextSize(50.0f * this.mScaledDensity);
                float measureText = paint2.measureText(this.mSections[this.mCurrentSection]);
                float descent = ((this.mPreviewPadding * 2.0f) + paint2.descent()) - paint2.ascent();
                RectF rectF = new RectF((((float) this.mListViewWidth) - descent) / 2.0f, (((float) this.mListViewHeight) - descent) / 2.0f, ((((float) this.mListViewWidth) - descent) / 2.0f) + descent, ((((float) this.mListViewHeight) - descent) / 2.0f) + descent);
                canvas.drawRoundRect(rectF, 5.0f * this.mDensity, 5.0f * this.mDensity, paint);
                canvas.drawText(this.mSections[this.mCurrentSection], (((descent - measureText) / 2.0f) + rectF.left) - 1.0f, ((rectF.top + this.mPreviewPadding) - paint2.ascent()) + 1.0f, paint2);
            }
            Paint paint3 = new Paint();
            paint3.setColor(Color.parseColor("#ff5a00"));
            paint3.setAlpha((int) (255.0f * this.mAlphaRate));
            paint3.setAntiAlias(true);
            paint3.setFakeBoldText(true);
            paint3.setTextSize(14.0f * this.mScaledDensity);
            float height = (this.mIndexbarRect.height() - (this.mIndexbarMargin * 2.0f)) / ((float) this.mSections.length);
            float descent2 = (height - (paint3.descent() - paint3.ascent())) / 2.0f;
            for (int i = 0; i < this.mSections.length; i++) {
                canvas.drawText(this.mSections[i], ((this.mIndexbarWidth - paint3.measureText(this.mSections[i])) / 2.0f) + this.mIndexbarRect.left, (((this.mIndexbarRect.top + this.mIndexbarMargin) + (((float) i) * height)) + descent2) - paint3.ascent(), paint3);
            }
        }
    }

    public void hide() {
        if (this.mState == 2) {
            setState(3);
        }
    }

    public void onSizeChanged(int i, int i2, int i3, int i4) {
        this.mListViewWidth = i;
        this.mListViewHeight = i2;
        this.mIndexbarRect = new RectF((((float) i) - this.mIndexbarMargin) - this.mIndexbarWidth, this.mIndexbarMargin, ((float) i) - this.mIndexbarMargin, ((float) i2) - this.mIndexbarMargin);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                if (this.mState != 0 && contains(motionEvent.getX(), motionEvent.getY())) {
                    setState(2);
                    this.mIsIndexing = true;
                    this.mCurrentSection = getSectionByPoint(motionEvent.getY());
                    this.mListView.setSelection(this.mIndexer.getPositionForSection(this.mCurrentSection));
                    return true;
                }
            case 1:
                if (this.mIsIndexing) {
                    this.mIsIndexing = false;
                    this.mCurrentSection = -1;
                }
                if (this.mAutoHide && this.mState == 2) {
                    setState(3);
                    break;
                }
            case 2:
                if (this.mIsIndexing) {
                    if (!contains(motionEvent.getX(), motionEvent.getY())) {
                        return true;
                    }
                    this.mCurrentSection = getSectionByPoint(motionEvent.getY());
                    this.mListView.setSelection(this.mIndexer.getPositionForSection(this.mCurrentSection));
                    return true;
                }
                break;
        }
        return false;
    }

    public void setAdapter(Adapter adapter) {
        if (adapter instanceof SectionIndexer) {
            this.mIndexer = (SectionIndexer) adapter;
            this.mSections = (String[]) this.mIndexer.getSections();
        }
    }

    public void show() {
        if (this.mState == 0) {
            setState(1);
        } else if (this.mState == 3) {
            setState(3);
        }
    }
}
