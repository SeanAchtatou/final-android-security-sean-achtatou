package com.qihoo360.daily.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.qihoo360.daily.R;

public class BadgeView extends TextView {
    private static final int DEFAULT_LR_PADDING_DIP = 5;
    private static final int DEFAULT_MARGIN_DIP = 10;
    private static final int DEFAULT_POSITION = 2;
    public static final int POSITION_BOTTOM_LEFT = 3;
    public static final int POSITION_BOTTOM_RIGHT = 4;
    public static final int POSITION_CENTER = 5;
    public static final int POSITION_TOP_LEFT = 1;
    public static final int POSITION_TOP_RIGHT = 2;
    private int badgeMarginH;
    private int badgeMarginV;
    private int badgePosition;
    private Context context;
    private boolean isShown;
    private View target;

    public BadgeView(Context context2) {
        this(context2, null, 16842884);
    }

    public BadgeView(Context context2, AttributeSet attributeSet) {
        this(context2, attributeSet, 16842884);
    }

    public BadgeView(Context context2, AttributeSet attributeSet, int i) {
        this(context2, attributeSet, i, null);
    }

    public BadgeView(Context context2, AttributeSet attributeSet, int i, View view) {
        super(context2, attributeSet, i);
        init(context2, view);
    }

    public BadgeView(Context context2, View view) {
        this(context2, null, 16842884, view);
    }

    private void applyLayoutParams() {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        switch (this.badgePosition) {
            case 1:
                layoutParams.gravity = 51;
                layoutParams.setMargins(this.badgeMarginH, this.badgeMarginV, 0, 0);
                break;
            case 2:
                layoutParams.gravity = 53;
                layoutParams.setMargins(0, this.badgeMarginV, this.badgeMarginH, 0);
                break;
            case 3:
                layoutParams.gravity = 83;
                layoutParams.setMargins(this.badgeMarginH, 0, 0, this.badgeMarginV);
                break;
            case 4:
                layoutParams.gravity = 85;
                layoutParams.setMargins(0, 0, this.badgeMarginH, this.badgeMarginV);
                break;
            case 5:
                layoutParams.gravity = 17;
                layoutParams.setMargins(0, 0, 0, 0);
                break;
        }
        setLayoutParams(layoutParams);
    }

    private void applyTo(View view) {
        ViewGroup viewGroup;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        ViewParent parent = view.getParent();
        FrameLayout frameLayout = new FrameLayout(this.context);
        ViewGroup viewGroup2 = (ViewGroup) parent;
        if (viewGroup2 == null) {
            viewGroup = (ViewGroup) view;
            setVisibility(8);
            viewGroup.addView(this);
        } else {
            int indexOfChild = viewGroup2.indexOfChild(view);
            viewGroup2.removeView(view);
            viewGroup2.addView(frameLayout, indexOfChild, layoutParams);
            frameLayout.addView(view);
            setVisibility(8);
            frameLayout.addView(this);
            viewGroup = viewGroup2;
        }
        viewGroup.invalidate();
    }

    private int dipToPixels(int i) {
        return (int) TypedValue.applyDimension(1, (float) i, getResources().getDisplayMetrics());
    }

    private void init(Context context2, View view) {
        this.context = context2;
        this.target = view;
        this.badgePosition = 2;
        this.badgeMarginH = dipToPixels(10);
        this.badgeMarginV = this.badgeMarginH;
        setTypeface(Typeface.DEFAULT_BOLD);
        int dipToPixels = dipToPixels(5);
        setPadding(dipToPixels, 0, dipToPixels, 0);
        this.isShown = false;
        setBackgroundResource(R.drawable.favourite_edit_selecter);
        if (this.target != null) {
            applyTo(this.target);
        } else {
            show();
        }
    }

    public int getBadgePosition() {
        return this.badgePosition;
    }

    public int getHorizontalBadgeMargin() {
        return this.badgeMarginH;
    }

    public View getTarget() {
        return this.target;
    }

    public int getVerticalBadgeMargin() {
        return this.badgeMarginV;
    }

    public void hide() {
        setVisibility(8);
        this.isShown = false;
    }

    public boolean isShown() {
        return this.isShown;
    }

    public void setBadgeMargin(int i) {
        this.badgeMarginH = i;
        this.badgeMarginV = i;
    }

    public void setBadgeMargin(int i, int i2) {
        this.badgeMarginH = i;
        this.badgeMarginV = i2;
    }

    public void setBadgePosition(int i) {
        this.badgePosition = i;
    }

    public void show() {
        applyLayoutParams();
        setVisibility(0);
        this.isShown = true;
    }
}
