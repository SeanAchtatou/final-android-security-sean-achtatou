package com.qihoo360.daily.widget.dragdropgrid;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qihoo360.daily.R;

public class ChannelItemLayout extends RelativeLayout {
    private ImageView mIvDel;
    private TextView mTvChannelName;

    public ChannelItemLayout(Context context) {
        super(context, null);
    }

    public ChannelItemLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        View inflate = LayoutInflater.from(context).inflate((int) R.layout.layout_channels_item_internal, this);
        this.mTvChannelName = (TextView) inflate.findViewById(R.id.tv_channel_name);
        this.mIvDel = (ImageView) inflate.findViewById(R.id.iv_del);
    }

    public ImageView getChannelDelView() {
        return this.mIvDel;
    }

    public TextView getChannelNameView() {
        return this.mTvChannelName;
    }

    public void setChannelNameEmpty() {
        this.mTvChannelName.setText((CharSequence) null);
        this.mTvChannelName.setBackgroundResource(R.drawable.sl_channel_bg2);
        this.mIvDel.setVisibility(8);
    }
}
