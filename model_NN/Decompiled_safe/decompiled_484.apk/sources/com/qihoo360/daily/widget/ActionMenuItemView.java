package com.qihoo360.daily.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Toast;

public class ActionMenuItemView extends android.support.v7.internal.view.menu.ActionMenuItemView {
    public ActionMenuItemView(Context context) {
        super(context);
    }

    public ActionMenuItemView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ActionMenuItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public boolean onLongClick(View view) {
        if (!hasText()) {
            int[] iArr = new int[2];
            Rect rect = new Rect();
            getLocationOnScreen(iArr);
            getWindowVisibleDisplayFrame(rect);
            Context applicationContext = getContext().getApplicationContext();
            int width = getWidth();
            int height = getHeight();
            int i = iArr[1] + (height / 2);
            int i2 = iArr[0] + (width / 2);
            if (ViewCompat.getLayoutDirection(view) == 0) {
                i2 = applicationContext.getResources().getDisplayMetrics().widthPixels - i2;
            }
            Toast makeText = Toast.makeText(applicationContext, getItemData().getTitle(), 0);
            if (i < rect.height()) {
                makeText.setGravity(8388661, i2, height);
            } else {
                makeText.setGravity(81, 0, height);
            }
            makeText.show();
        }
        return false;
    }
}
