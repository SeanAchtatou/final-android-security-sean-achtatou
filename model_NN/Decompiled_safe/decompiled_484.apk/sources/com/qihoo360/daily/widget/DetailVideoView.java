package com.qihoo360.daily.widget;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.c.a.af;
import com.c.a.am;
import com.qihoo.download.base.AbsDownloadTask;
import com.qihoo.download.impl.so.SoDownloadManager;
import com.qihoo.qplayer.QihooMediaPlayer;
import com.qihoo.qplayer.view.QihooVideoView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.SearchActivity;
import com.qihoo360.daily.activity.VideoFullActivity;
import com.qihoo360.daily.fragment.SplashFragment;
import com.qihoo360.daily.h.ad;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.h.bk;
import java.lang.ref.WeakReference;

public class DetailVideoView extends FrameLayout {
    private static final int MSG_DISS_PLACE = 5;
    private static final int MSG_PAUSE = 3;
    private static final int MSG_RELEASE = 1;
    private static final int MSG_RESIZE_VIDEO = 2;
    private static final int MSG_SHOW_PLACE = 4;
    private static final int controllerBottomMarginDp = 3;
    private static ImageView ivPausePlace;
    private final int closeMarginRightDp = 8;
    private final int closeMarginTopDp = 8;
    private Context context;
    /* access modifiers changed from: private */
    public boolean firstStart = true;
    /* access modifiers changed from: private */
    public ControllerHandler handler = new ControllerHandler(new WeakReference(this));
    private ImageButton ibFull;
    /* access modifiers changed from: private */
    public boolean isPrepared = false;
    /* access modifiers changed from: private */
    public ImageView ivClose;
    private ImageView ivCloseLoading;
    private ImageView ivPause;
    private final int ivPauseId = 305419896;
    private OnStartPlayListener onPlayListener;
    private ProgressBar pbBuffer;
    /* access modifiers changed from: private */
    public int position = 0;
    /* access modifiers changed from: private */
    public RelativeLayout rlBuffer;
    /* access modifiers changed from: private */
    public RelativeLayout rlController;
    private RelativeLayout rlLoading;
    /* access modifiers changed from: private */
    public RelativeLayout rlPause;
    /* access modifiers changed from: private */
    public SeekBar sbProgress;
    /* access modifiers changed from: private */
    public int soBuffer = 0;
    /* access modifiers changed from: private */
    public String title;
    /* access modifiers changed from: private */
    public TextView tvBuffer;
    /* access modifiers changed from: private */
    public TextView tvLoading;
    private final int tvTimeMarginTopDp = 5;
    /* access modifiers changed from: private */
    public String url;
    private int videoHeight;
    private final int videoHeightRatio = 9;
    /* access modifiers changed from: private */
    public QihooVideoView videoView;
    /* access modifiers changed from: private */
    public int videoWidth;
    private final int videoWidthRatio = 16;

    class ControllerHandler extends Handler {
        private WeakReference<DetailVideoView> reference;

        public ControllerHandler(WeakReference<DetailVideoView> weakReference) {
            this.reference = weakReference;
        }

        public void handleMessage(Message message) {
            DetailVideoView detailVideoView = this.reference.get();
            switch (message.what) {
                case 1:
                    if (detailVideoView != null) {
                        detailVideoView.release();
                        return;
                    }
                    return;
                case 2:
                    if (detailVideoView != null) {
                        detailVideoView.resetVideoWidAndHeight();
                        return;
                    }
                    return;
                case 3:
                    if (detailVideoView != null) {
                        detailVideoView.pause();
                        return;
                    }
                    return;
                case 4:
                    if (detailVideoView != null) {
                        detailVideoView.showPlace();
                        return;
                    }
                    return;
                case 5:
                    if (detailVideoView != null) {
                        detailVideoView.dissPlace();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public interface OnStartPlayListener {
        void onVideoPlay();

        void onVideoStop();
    }

    public DetailVideoView(Context context2) {
        super(context2);
        initView(context2);
    }

    public DetailVideoView(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        initView(context2);
    }

    public DetailVideoView(Context context2, AttributeSet attributeSet, int i) {
        super(context2, attributeSet, i);
        initView(context2);
    }

    /* access modifiers changed from: private */
    public void dissPlace() {
        if (ivPausePlace != null) {
            ivPausePlace.setVisibility(8);
        }
    }

    private void initVideoView(final Context context2) {
        this.isPrepared = false;
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        this.videoView = new QihooVideoView(context2);
        layoutParams.gravity = 49;
        addView(this.videoView, 0, layoutParams);
        this.videoView.setKeepScreenOn(true);
        this.videoView.setOnPreparedListener(new QihooMediaPlayer.OnPreparedListener() {
            public void onPrepared(QihooMediaPlayer qihooMediaPlayer) {
                boolean unused = DetailVideoView.this.isPrepared = true;
                if (DetailVideoView.this.videoView == null) {
                    return;
                }
                if (DetailVideoView.this.position == 0) {
                    boolean unused2 = DetailVideoView.this.firstStart = true;
                    DetailVideoView.this.videoView.start();
                    return;
                }
                boolean unused3 = DetailVideoView.this.firstStart = false;
                DetailVideoView.this.videoView.seekTo(DetailVideoView.this.position);
                int unused4 = DetailVideoView.this.position = 0;
            }
        });
        this.videoView.setOnSeekCompleteListener(new QihooMediaPlayer.OnSeekCompleteListener() {
            public void onSeekComplete(QihooMediaPlayer qihooMediaPlayer) {
                DetailVideoView.this.sbProgress.setProgress((int) ((((float) qihooMediaPlayer.getCurrentPosition()) / ((float) qihooMediaPlayer.getDuration())) * 100.0f));
                qihooMediaPlayer.start();
            }
        });
        this.videoView.setOnPositionChangeListener(new QihooMediaPlayer.OnPositionChangeListener() {
            public void onPlayPositionChanged(QihooMediaPlayer qihooMediaPlayer, int i) {
                DetailVideoView.this.sbProgress.setProgress((int) ((((float) i) / ((float) qihooMediaPlayer.getDuration())) * 100.0f));
            }
        });
        this.videoView.setOnBufferListener(new QihooMediaPlayer.OnBufferingUpdateListener() {
            public void onBufferingUpdate(QihooMediaPlayer qihooMediaPlayer, int i) {
                if (!DetailVideoView.this.firstStart) {
                    if (DetailVideoView.this.tvBuffer != null) {
                        DetailVideoView.this.tvBuffer.setText(String.format(DetailVideoView.this.getResources().getString(R.string.already_loading), Integer.valueOf(i)));
                    }
                    if (i < 0 || i >= 100) {
                        if (i == 100 && DetailVideoView.this.rlBuffer.isShown()) {
                            DetailVideoView.this.rlBuffer.setVisibility(8);
                        }
                    } else if (!DetailVideoView.this.rlBuffer.isShown()) {
                        DetailVideoView.this.rlBuffer.setVisibility(0);
                    }
                } else if (i >= 0 && i < 100) {
                    if (DetailVideoView.this.soBuffer > 0) {
                        i = (DetailVideoView.this.soBuffer + i) / 2;
                    }
                    DetailVideoView.this.tvLoading.setText(String.format(DetailVideoView.this.getResources().getString(R.string.video_is_loading), Integer.valueOf(i)));
                } else if (i == 100) {
                    int unused = DetailVideoView.this.soBuffer = 0;
                    DetailVideoView.this.ivClose.setVisibility(0);
                    DetailVideoView.this.rlController.setVisibility(0);
                    DetailVideoView.this.handler.sendEmptyMessage(2);
                    DetailVideoView.this.setLoadingVisibility(8);
                    boolean unused2 = DetailVideoView.this.firstStart = false;
                    DetailVideoView.this.dissmissPlacePause();
                    if (!((PowerManager) DetailVideoView.this.getContext().getSystemService("power")).isScreenOn()) {
                        DetailVideoView.this.pause();
                    }
                }
            }
        });
        this.videoView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!DetailVideoView.this.isPrepared) {
                    return;
                }
                if (DetailVideoView.this.videoView.isPlaying()) {
                    DetailVideoView.this.pause();
                    return;
                }
                DetailVideoView.this.dissmissPlacePause();
                if (b.d(context2)) {
                    if (DetailVideoView.this.position == 0) {
                        DetailVideoView.this.start();
                    } else {
                        DetailVideoView.this.startVideo();
                    }
                    DetailVideoView.this.rlPause.setVisibility(8);
                    return;
                }
                ay.a(context2).a((int) R.string.can_not_play);
                DetailVideoView.this.handler.sendEmptyMessage(3);
            }
        });
        this.videoView.setOnCompletetionListener(new QihooMediaPlayer.OnCompletionListener() {
            public void onCompletion(QihooMediaPlayer qihooMediaPlayer) {
                DetailVideoView.this.handler.sendEmptyMessage(1);
            }
        });
        this.videoView.setOnErrorListener(new QihooMediaPlayer.OnErrorListener() {
            public boolean onError(QihooMediaPlayer qihooMediaPlayer, int i, int i2) {
                int unused = DetailVideoView.this.position = qihooMediaPlayer.getCurrentPosition();
                if (DetailVideoView.this.firstStart) {
                    DetailVideoView.this.tvLoading.setText(DetailVideoView.this.getResources().getString(R.string.video_is_loading_retry));
                    b.b(context2, "Detail_video_play");
                    return false;
                } else if (b.d(context2)) {
                    return false;
                } else {
                    ay.a(context2).a((int) R.string.can_not_play);
                    DetailVideoView.this.handler.sendEmptyMessage(3);
                    return false;
                }
            }
        });
        requestFocus();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.qihoo360.daily.widget.DetailVideoView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void initView(final Context context2) {
        setBackgroundResource(R.color.black);
        this.context = context2;
        LayoutInflater from = LayoutInflater.from(context2);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -2);
        this.rlController = (RelativeLayout) from.inflate((int) R.layout.controller_strip_video, (ViewGroup) this, false);
        layoutParams.gravity = 81;
        layoutParams.bottomMargin = b.a(context2, 3.0f);
        this.rlController.setBackgroundColor(0);
        addView(this.rlController, layoutParams);
        this.ibFull = (ImageButton) findViewById(R.id.ib_full);
        this.sbProgress = (SeekBar) findViewById(R.id.sb_progress);
        this.sbProgress.setEnabled(false);
        this.rlBuffer = (RelativeLayout) from.inflate((int) R.layout.buffer_video, (ViewGroup) this, false);
        this.pbBuffer = (ProgressBar) this.rlBuffer.findViewById(R.id.pb_buffer);
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.pbBuffer.getLayoutParams();
        layoutParams2.width = b.a(context2, 30.0f);
        layoutParams2.height = layoutParams2.width;
        this.pbBuffer.setLayoutParams(layoutParams2);
        this.tvBuffer = (TextView) this.rlBuffer.findViewById(R.id.tv_buffer);
        this.tvBuffer.setText(String.format(getResources().getString(R.string.already_loading), 0));
        FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(-2, -2);
        layoutParams3.gravity = 17;
        addView(this.rlBuffer, layoutParams3);
        ivPausePlace = new ImageView(context2);
        ivPausePlace.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        addView(ivPausePlace, new RelativeLayout.LayoutParams(-1, -1));
        ivPausePlace.setVisibility(8);
        this.rlPause = new RelativeLayout(context2);
        this.ivPause = new ImageView(context2);
        this.ivPause.setBackgroundResource(R.drawable.un_play_video);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams4.addRule(14, -1);
        this.ivPause.setId(305419896);
        this.rlPause.addView(this.ivPause, layoutParams4);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams5.addRule(3, 305419896);
        layoutParams5.topMargin = bk.a(context2, 5.0f);
        layoutParams5.addRule(14, -1);
        FrameLayout.LayoutParams layoutParams6 = new FrameLayout.LayoutParams(-2, -2);
        layoutParams6.gravity = 17;
        addView(this.rlPause, layoutParams6);
        this.rlPause.setVisibility(8);
        this.rlBuffer.setVisibility(8);
        this.rlController.setVisibility(8);
        this.sbProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                if (z) {
                    DetailVideoView.this.videoView.seekTo((int) ((((float) i) / 100.0f) * ((float) DetailVideoView.this.videoView.getDuration())));
                    DetailVideoView.this.rlPause.setVisibility(8);
                }
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        this.ibFull.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int currentPosition = DetailVideoView.this.videoView.getCurrentPosition();
                Intent intent = new Intent(context2, VideoFullActivity.class);
                intent.putExtra("position", currentPosition);
                intent.putExtra(SearchActivity.TAG_URL, DetailVideoView.this.url);
                intent.putExtra("title", DetailVideoView.this.title);
                context2.startActivity(intent);
            }
        });
        this.ivClose = new ImageView(context2);
        FrameLayout.LayoutParams layoutParams7 = new FrameLayout.LayoutParams(-2, -2);
        layoutParams7.gravity = 53;
        layoutParams7.rightMargin = b.a(context2, 8.0f);
        layoutParams7.topMargin = b.a(context2, 8.0f);
        this.ivClose.setImageResource(R.drawable.video_close);
        addView(this.ivClose, layoutParams7);
        this.ivClose.setVisibility(8);
        this.ivClose.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DetailVideoView.this.release();
            }
        });
        this.rlLoading = (RelativeLayout) from.inflate((int) R.layout.layout_video_loading_detail, (ViewGroup) this, false);
        this.tvLoading = (TextView) this.rlLoading.findViewById(R.id.tv_loading_detail);
        this.tvLoading.setText(String.format(getResources().getString(R.string.video_is_loading), 0));
        this.tvLoading.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (DetailVideoView.this.tvLoading.getText().toString().trim().equals(DetailVideoView.this.getResources().getString(R.string.video_is_loading_retry)) && DetailVideoView.this.videoView != null && DetailVideoView.this.url != null && !"".equals(DetailVideoView.this.url.trim())) {
                    DetailVideoView.this.tvLoading.setText(String.format(DetailVideoView.this.getResources().getString(R.string.video_is_loading), 0));
                    DetailVideoView.this.videoView.setDataSource(DetailVideoView.this.url);
                }
            }
        });
        this.ivCloseLoading = (ImageView) this.rlLoading.findViewById(R.id.iv_close);
        this.ivCloseLoading.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DetailVideoView.this.release();
            }
        });
        addView(this.rlLoading);
        this.ivClose.setVisibility(8);
        this.rlController.setVisibility(8);
        setLoadingVisibility(0);
    }

    /* access modifiers changed from: private */
    public void resetVideoWidAndHeight() {
        if (this.videoView != null) {
            int measuredHeight = this.rlLoading.getMeasuredHeight();
            ad.a("srcheight..." + measuredHeight + ",videoheight..." + this.videoHeight);
            af b2 = af.b(measuredHeight, this.videoHeight);
            b2.b(500);
            b2.a(new LinearInterpolator());
            b2.a(new am() {
                public void onAnimationUpdate(af afVar) {
                    int intValue = ((Integer) afVar.g()).intValue();
                    if (DetailVideoView.this.videoView != null) {
                        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) DetailVideoView.this.videoView.getLayoutParams();
                        layoutParams.width = DetailVideoView.this.videoWidth;
                        layoutParams.height = intValue;
                        DetailVideoView.this.videoView.setLayoutParams(layoutParams);
                        DetailVideoView.this.videoView.resetVideoWidAndHeight(DetailVideoView.this.videoWidth, intValue);
                        DetailVideoView.this.requestLayout();
                    }
                }
            });
            b2.a();
        }
    }

    /* access modifiers changed from: private */
    public void setLoadingVisibility(int i) {
        this.rlLoading.setVisibility(i);
    }

    /* access modifiers changed from: private */
    public void showPlace() {
        if (ivPausePlace != null) {
            ivPausePlace.setVisibility(0);
        }
    }

    public void click() {
        if (this.videoView != null) {
            this.videoView.performClick();
        }
    }

    public void dissmissPlacePause() {
        if (this.handler != null) {
            this.handler.sendEmptyMessage(5);
        }
    }

    public QihooVideoView getVideoView() {
        return this.videoView;
    }

    public void init(String str, String str2, String str3) {
        this.title = str;
        this.url = bk.a(str2);
    }

    public boolean isPlay() {
        if (this.videoView != null) {
            return this.videoView.isPlaying();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.videoView != null && !this.firstStart) {
            setMeasuredDimension(this.videoWidth, this.videoView.getLayoutParams().height + b.a(this.context, 3.0f));
        }
    }

    public void pause() {
        if (this.videoView != null && !this.firstStart) {
            this.videoView.pause();
            this.rlPause.setVisibility(0);
            this.rlBuffer.setVisibility(8);
        }
    }

    public void release() {
        if (this.onPlayListener != null) {
            this.onPlayListener.onVideoStop();
        }
        if (this.videoView != null) {
            this.videoView.stop();
            this.videoView.release();
            removeView(this.videoView);
            this.videoView = null;
        }
        this.isPrepared = false;
        this.rlBuffer.setVisibility(8);
        this.rlController.setVisibility(8);
        this.rlPause.setVisibility(0);
        this.firstStart = true;
        ivPausePlace = null;
        ViewParent parent = getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(this);
        }
    }

    public void setOnPlayListener(OnStartPlayListener onStartPlayListener) {
        this.onPlayListener = onStartPlayListener;
    }

    public void showPlacePause() {
        if (this.handler != null) {
            this.handler.sendEmptyMessage(4);
        }
    }

    public void start() {
        if (this.onPlayListener != null) {
            this.onPlayListener.onVideoPlay();
        }
        if (this.videoView != null) {
            this.rlPause.setVisibility(8);
            this.videoView.start();
        }
    }

    public void startVideo() {
        if (!SoDownloadManager.getInstance().needDownloadSo(this.context) || SplashFragment.isDownloadSo) {
            if (this.onPlayListener != null) {
                this.onPlayListener.onVideoPlay();
            }
            this.rlPause.setVisibility(8);
            if (this.videoView == null) {
                initVideoView(this.context);
            }
            int i = getResources().getDisplayMetrics().widthPixels;
            if (i > 0) {
                this.videoWidth = i;
                this.videoHeight = (i * 9) / 16;
            }
            if (this.position == 0) {
                this.videoView.initVideoWidAndHeight(0, 0);
            } else {
                this.videoView.resetVideoWidAndHeight(this.videoWidth, this.videoHeight);
            }
            this.videoView.setDataSource(this.url);
            return;
        }
        SoDownloadManager.getInstance().setListener(new SoDownloadManager.IDowloadSoListener() {
            public void onDownloadFailed(AbsDownloadTask absDownloadTask) {
                if (DetailVideoView.this.tvLoading != null) {
                    DetailVideoView.this.tvLoading.setText(DetailVideoView.this.getResources().getString(R.string.video_is_loading_retry));
                }
            }

            public void onDownloadSizeChanged(AbsDownloadTask absDownloadTask) {
                int unused = DetailVideoView.this.soBuffer = (int) ((((float) absDownloadTask.getDownloadSize()) / ((float) absDownloadTask.getTotalSize())) * 100.0f);
                if (DetailVideoView.this.tvLoading != null) {
                    DetailVideoView.this.tvLoading.setText(String.format(DetailVideoView.this.getResources().getString(R.string.video_is_loading), Integer.valueOf(DetailVideoView.this.soBuffer / 2)));
                }
            }

            public void onDownloadSucess(AbsDownloadTask absDownloadTask) {
                DetailVideoView.this.startVideo();
            }
        });
        SoDownloadManager.getInstance().downloadSo(this.context);
    }

    public void stop() {
        if (this.videoView != null) {
            this.videoView.stop();
        }
    }
}
