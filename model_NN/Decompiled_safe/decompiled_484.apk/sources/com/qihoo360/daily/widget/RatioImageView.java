package com.qihoo360.daily.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RemoteViews;
import com.qihoo360.daily.R;

@RemoteViews.RemoteView
public class RatioImageView extends ImageView {
    public static final int ADJUST_SIZE_BY_HEIGHT = 2;
    public static final int ADJUST_SIZE_BY_WIDTH = 1;
    private Bitmap bitmap;
    private int limit = 1;
    private float ratio = 1.35f;
    private Rect rect = new Rect();
    private int width;

    public RatioImageView(Context context) {
        super(context);
    }

    public RatioImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.RatioImageView);
        this.ratio = obtainStyledAttributes.getFloat(1, 1.35f);
        this.limit = obtainStyledAttributes.getInt(0, 1);
        int resourceId = obtainStyledAttributes.getResourceId(2, -1);
        if (resourceId != -1) {
            this.bitmap = BitmapFactory.decodeResource(getResources(), resourceId);
        }
        obtainStyledAttributes.recycle();
    }

    public RatioImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.bitmap != null) {
            canvas.drawBitmap(this.bitmap, (Rect) null, this.rect, (Paint) null);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        if (this.limit == 1) {
            if (this.width > 0) {
                i = View.MeasureSpec.makeMeasureSpec(this.width, 1073741824);
            }
            i2 = View.MeasureSpec.makeMeasureSpec(((int) (((float) ((View.MeasureSpec.getSize(i) - getPaddingLeft()) - getPaddingRight())) / this.ratio)) + getPaddingTop() + getPaddingBottom(), 1073741824);
        } else {
            i = View.MeasureSpec.makeMeasureSpec((int) (((float) View.MeasureSpec.getSize(i2)) * this.ratio), 1073741824);
        }
        this.rect.set(0, 0, getMeasuredWidth(), getMeasuredHeight());
        super.onMeasure(i, i2);
    }

    public void setRatio(float f) {
        this.ratio = f;
        requestLayout();
    }

    public void setWidth(int i) {
        this.width = i;
    }
}
