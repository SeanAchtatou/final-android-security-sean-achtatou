package com.qihoo360.daily.widget.dragdropgrid;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.qihoo360.daily.R;

public class DragableGridView extends FrameLayout implements Animation.AnimationListener {
    public static final int DISABLE_CHANNEL_NUM = 1;
    private float mFirstX;
    private float mFirstY;
    private MovableGridView mGridView;
    private boolean mIsAnimating;
    private boolean mIsEditting;
    private boolean mIsMoveAble;
    private ImageView mIvMoveItem;
    private int mLastBottom;
    private int mLastLeft;
    private int mLastPosition = -1;
    private int mLastRight;
    private int mLastTop;
    private float mLastX;
    private float mLastY;
    private OnSwichChangeListener mOnSwichChangeListener;
    private ViewConfiguration mViewConfig = ViewConfiguration.get(getContext());

    public interface OnSwichChangeListener {
        void OnSwitch(int i, int i2);
    }

    public DragableGridView(Context context) {
        super(context);
    }

    public DragableGridView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public DragableGridView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    private int checkPositionWhetherNeedToSwitch(float f, float f2) {
        View childAt;
        if (this.mGridView == null) {
            return -1;
        }
        int childCount = this.mGridView.getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (!(i == this.mLastPosition || i <= 0 || (childAt = this.mGridView.getChildAt(i)) == null)) {
                int left = childAt.getLeft();
                int right = childAt.getRight();
                int top = childAt.getTop();
                int bottom = childAt.getBottom();
                if (f > ((float) left) && f < ((float) right) && f2 > ((float) top) && f2 < ((float) bottom)) {
                    View childAt2 = this.mGridView.getChildAt(this.mLastPosition);
                    if (childAt2 != null) {
                        childAt2.setVisibility(0);
                    }
                    switchView(this.mLastPosition, i);
                    this.mLastPosition = i;
                    return i;
                }
            }
        }
        return -1;
    }

    private int getPositionInGridView(float f, float f2) {
        if (this.mGridView == null) {
            return -1;
        }
        int childCount = this.mGridView.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = this.mGridView.getChildAt(i);
            if (childAt != null) {
                int left = childAt.getLeft();
                int right = childAt.getRight();
                int top = childAt.getTop();
                int bottom = childAt.getBottom();
                if (f > ((float) left) && f < ((float) right) && f2 > ((float) top) && f2 < ((float) bottom)) {
                    return i;
                }
            }
        }
        return -1;
    }

    private void moveToDestination(View view) {
        if (view != null) {
            int left = this.mIvMoveItem.getLeft();
            int top = this.mIvMoveItem.getTop();
            TranslateAnimation translateAnimation = new TranslateAnimation((float) (left - view.getLeft()), 0.0f, (float) (top - view.getTop()), 0.0f);
            translateAnimation.setDuration(250);
            translateAnimation.setAnimationListener(this);
            view.startAnimation(translateAnimation);
            this.mGridView.restoreSwitch();
        }
    }

    private void switchView(int i, int i2) {
        if (this.mOnSwichChangeListener != null) {
            this.mOnSwichChangeListener.OnSwitch(i, i2);
        }
    }

    public MovableGridView getGridView() {
        return this.mGridView;
    }

    public boolean isEditting() {
        return this.mIsEditting;
    }

    public void onAnimationEnd(Animation animation) {
        this.mIsAnimating = false;
        View childAt = this.mGridView.getChildAt(this.mLastPosition);
        if (childAt != null) {
            childAt.setVisibility(0);
        }
        this.mLastPosition = -1;
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
        this.mIsAnimating = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.qihoo360.daily.widget.dragdropgrid.DragableGridView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        View inflate = LayoutInflater.from(getContext()).inflate((int) R.layout.layout_movable_gridview, (ViewGroup) this, false);
        if (inflate instanceof MovableGridView) {
            this.mGridView = (MovableGridView) inflate;
            addView(this.mGridView);
            this.mGridView.setDrawingCacheEnabled(true);
        }
        this.mIvMoveItem = new ImageView(getContext());
        if (this.mIvMoveItem != null) {
            addView(this.mIvMoveItem, new FrameLayout.LayoutParams(-2, -2));
            this.mIvMoveItem.setVisibility(8);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00c8, code lost:
        if (r10.mIsMoveAble != false) goto L_0x00ca;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onInterceptTouchEvent(android.view.MotionEvent r11) {
        /*
            r10 = this;
            r8 = 0
            r0 = 1
            boolean r1 = super.onInterceptTouchEvent(r11)
            boolean r2 = r10.mIsAnimating
            if (r2 == 0) goto L_0x000b
        L_0x000a:
            return r1
        L_0x000b:
            float r2 = r11.getX()
            float r3 = r11.getY()
            int r4 = r11.getAction()
            switch(r4) {
                case 0: goto L_0x001f;
                case 1: goto L_0x00d4;
                case 2: goto L_0x0037;
                case 3: goto L_0x00d4;
                default: goto L_0x001a;
            }
        L_0x001a:
            r10.mLastX = r2
            r10.mLastY = r3
            goto L_0x000a
        L_0x001f:
            r10.mFirstX = r2
            r10.mFirstY = r3
            boolean r4 = r10.isEditting()
            if (r4 == 0) goto L_0x001a
            int r4 = r10.getPositionInGridView(r2, r3)
            if (r4 <= 0) goto L_0x001a
            android.view.ViewParent r4 = r10.getParent()
            r4.requestDisallowInterceptTouchEvent(r0)
            goto L_0x001a
        L_0x0037:
            float r4 = r10.mFirstX
            float r4 = r2 - r4
            float r4 = java.lang.Math.abs(r4)
            float r5 = r10.mFirstY
            float r5 = r3 - r5
            float r5 = java.lang.Math.abs(r5)
            boolean r6 = r10.isEditting()
            if (r6 == 0) goto L_0x001a
            android.view.ViewConfiguration r6 = r10.mViewConfig
            int r6 = r6.getScaledTouchSlop()
            float r6 = (float) r6
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 > 0) goto L_0x0063
            android.view.ViewConfiguration r4 = r10.mViewConfig
            int r4 = r4.getScaledTouchSlop()
            float r4 = (float) r4
            int r4 = (r5 > r4 ? 1 : (r5 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x001a
        L_0x0063:
            int r4 = r10.getPositionInGridView(r2, r3)
            if (r4 <= 0) goto L_0x00ee
            com.qihoo360.daily.widget.dragdropgrid.MovableGridView r5 = r10.mGridView
            android.view.View r5 = r5.getChildAt(r4)
            if (r5 == 0) goto L_0x00c6
            r5.destroyDrawingCache()
            r5.buildDrawingCache()
            android.graphics.Bitmap r6 = r5.getDrawingCache()
            android.graphics.Bitmap r6 = android.graphics.Bitmap.createBitmap(r6)
            if (r6 == 0) goto L_0x001a
            boolean r7 = r6.isRecycled()
            if (r7 != 0) goto L_0x001a
            r10.mIsMoveAble = r0
            r7 = 4
            r5.setVisibility(r7)
            if (r6 == 0) goto L_0x00cd
            boolean r7 = r6.isRecycled()
            if (r7 != 0) goto L_0x00cd
            android.widget.ImageView r7 = r10.mIvMoveItem
            r7.setImageBitmap(r6)
        L_0x009a:
            android.widget.ImageView r6 = r10.mIvMoveItem
            r6.setVisibility(r8)
            int r6 = r5.getLeft()
            r10.mLastLeft = r6
            int r6 = r5.getTop()
            r10.mLastTop = r6
            int r6 = r5.getRight()
            r10.mLastRight = r6
            int r5 = r5.getBottom()
            r10.mLastBottom = r5
            android.widget.ImageView r5 = r10.mIvMoveItem
            int r6 = r10.mLastLeft
            int r7 = r10.mLastTop
            int r8 = r10.mLastRight
            int r9 = r10.mLastBottom
            r5.layout(r6, r7, r8, r9)
            r10.mLastPosition = r4
        L_0x00c6:
            boolean r4 = r10.mIsMoveAble
            if (r4 == 0) goto L_0x00ee
        L_0x00ca:
            r1 = r0
            goto L_0x001a
        L_0x00cd:
            android.widget.ImageView r6 = r10.mIvMoveItem
            r7 = 0
            r6.setImageBitmap(r7)
            goto L_0x009a
        L_0x00d4:
            boolean r0 = r10.mIsMoveAble
            if (r0 == 0) goto L_0x001a
            r10.mIsMoveAble = r8
            android.widget.ImageView r0 = r10.mIvMoveItem
            r4 = 8
            r0.setVisibility(r4)
            com.qihoo360.daily.widget.dragdropgrid.MovableGridView r0 = r10.mGridView
            int r4 = r10.mLastPosition
            android.view.View r0 = r0.getChildAt(r4)
            r10.moveToDestination(r0)
            goto L_0x001a
        L_0x00ee:
            r0 = r1
            goto L_0x00ca
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.widget.dragdropgrid.DragableGridView.onInterceptTouchEvent(android.view.MotionEvent):boolean");
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (this.mIvMoveItem != null && this.mLastRight != 0 && this.mLastBottom != 0) {
            this.mIvMoveItem.layout(this.mLastLeft, this.mLastTop, this.mLastRight, this.mLastBottom);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.mIsMoveAble || this.mIvMoveItem == null || this.mIsAnimating) {
            return false;
        }
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        switch (motionEvent.getAction()) {
            case 1:
            case 3:
                if (this.mIsMoveAble) {
                    this.mIsMoveAble = false;
                    this.mIvMoveItem.setVisibility(8);
                    moveToDestination(this.mGridView.getChildAt(this.mLastPosition));
                    break;
                }
                break;
            case 2:
                this.mLastLeft = ((int) (x - this.mLastX)) + this.mLastLeft;
                this.mLastTop += (int) (y - this.mLastY);
                this.mLastRight = this.mLastLeft + this.mIvMoveItem.getWidth();
                this.mLastBottom = this.mLastTop + this.mIvMoveItem.getHeight();
                this.mIvMoveItem.layout(this.mLastLeft, this.mLastTop, this.mLastRight, this.mLastBottom);
                checkPositionWhetherNeedToSwitch(x, y);
                break;
        }
        this.mLastX = x;
        this.mLastY = y;
        return true;
    }

    public void setEdit(boolean z) {
        this.mIsEditting = z;
    }

    public void setOnSwichChangeListener(OnSwichChangeListener onSwichChangeListener) {
        this.mOnSwichChangeListener = onSwichChangeListener;
    }
}
