package com.qihoo360.daily.widget;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.widget.RelativeLayout;
import com.d.a.a;
import com.qihoo.messenger.util.QDefine;
import com.qihoo360.daily.h.ad;

public class GestureView extends RelativeLayout {
    private GestureListener gestureListener = new GestureListener() {
        public void onLeftGesture() {
        }

        public void onRightGesture() {
            Context context = GestureView.this.getContext();
            if (context instanceof Activity) {
                ad.a("onBackPressed");
                ((Activity) context).onBackPressed();
            }
        }
    };
    private boolean isFirst = true;
    private VelocityTracker mVelocityTracker;
    private int pBottom;
    private int pLeft;
    private int pRight;
    private int pTop;
    private float startX;
    private float startY;

    public interface GestureListener {
        void onLeftGesture();

        void onRightGesture();
    }

    public GestureView(Context context) {
        super(context);
    }

    public GestureView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public GestureView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z = true;
        boolean onInterceptTouchEvent = super.onInterceptTouchEvent(motionEvent);
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        this.mVelocityTracker.addMovement(motionEvent);
        float rawX = motionEvent.getRawX();
        float rawY = motionEvent.getRawY();
        switch (motionEvent.getAction()) {
            case 0:
                this.startX = rawX;
                this.startY = rawY;
                return onInterceptTouchEvent;
            case 1:
            case 3:
                if (Math.abs((rawX - this.startX) / (rawY - this.startY)) > 2.0f) {
                    VelocityTracker velocityTracker = this.mVelocityTracker;
                    velocityTracker.computeCurrentVelocity(QDefine.ONE_SECOND);
                    float xVelocity = velocityTracker.getXVelocity();
                    float abs = Math.abs(xVelocity / velocityTracker.getYVelocity());
                    if (abs > 2.0f && xVelocity > 1000.0f && this.gestureListener != null) {
                        this.gestureListener.onRightGesture();
                        onInterceptTouchEvent = true;
                    }
                    if (abs <= 2.0f || xVelocity >= -1000.0f || this.gestureListener == null) {
                        z = onInterceptTouchEvent;
                    } else {
                        this.gestureListener.onLeftGesture();
                    }
                } else {
                    z = onInterceptTouchEvent;
                }
                if (this.mVelocityTracker != null) {
                    this.mVelocityTracker.recycle();
                    this.mVelocityTracker = null;
                }
                return z;
            case 2:
            default:
                return onInterceptTouchEvent;
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (Build.VERSION.SDK_INT == 19) {
            if (this.isFirst) {
                this.isFirst = false;
                this.pTop = getPaddingTop();
                this.pBottom = getPaddingBottom();
                this.pLeft = getPaddingLeft();
                this.pRight = getPaddingRight();
            }
            Context context = getContext();
            if ((context instanceof Activity) && new a((Activity) context).a().a(false) == this.pTop) {
                if (i4 > i2) {
                    setPadding(this.pLeft, 0, this.pRight, this.pBottom);
                } else {
                    setPadding(this.pLeft, this.pTop, this.pRight, this.pBottom);
                }
            }
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        this.mVelocityTracker.addMovement(motionEvent);
        float rawX = motionEvent.getRawX();
        float rawY = motionEvent.getRawY();
        switch (motionEvent.getAction()) {
            case 0:
                this.startX = rawX;
                this.startY = rawY;
                return true;
            case 1:
            case 3:
                if (Math.abs((rawX - this.startX) / (rawY - this.startY)) > 2.0f) {
                    VelocityTracker velocityTracker = this.mVelocityTracker;
                    velocityTracker.computeCurrentVelocity(QDefine.ONE_SECOND);
                    float xVelocity = velocityTracker.getXVelocity();
                    float abs = Math.abs(xVelocity / velocityTracker.getYVelocity());
                    if (abs > 2.0f && xVelocity > 1000.0f && this.gestureListener != null) {
                        this.gestureListener.onRightGesture();
                    }
                    if (abs > 2.0f && xVelocity < -1000.0f && this.gestureListener != null) {
                        this.gestureListener.onLeftGesture();
                    }
                    ad.a("velocityX:" + xVelocity);
                }
                if (this.mVelocityTracker == null) {
                    return true;
                }
                this.mVelocityTracker.recycle();
                this.mVelocityTracker = null;
                return true;
            case 2:
            default:
                return true;
        }
    }

    public void setGestureListener(GestureListener gestureListener2) {
        this.gestureListener = gestureListener2;
    }
}
