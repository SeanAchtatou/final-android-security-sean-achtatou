package com.qihoo360.daily.widget.webview;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.qihoo.dynamic.util.Md5Util;
import com.qihoo360.daily.c.m;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.h.ad;
import com.qihoo360.daily.h.ak;
import com.qihoo360.daily.h.bj;
import com.qihoo360.daily.model.NewsDetail;
import java.io.IOException;
import java.io.InputStream;

public class WebViewClientDaily extends WebViewClient {
    private String mFirstImgUrl;
    /* access modifiers changed from: private */
    public String mImgUrl;
    private NewsDetail mNewsDetail;
    private WebViewClientListener mWebViewClientListener;

    public interface WebViewClientListener {
        void onPageFinished(WebView webView, String str);

        boolean shouldOverrideUrlLoading(WebView webView, String str);
    }

    public WebViewClientDaily(WebViewClientListener webViewClientListener) {
        this.mWebViewClientListener = webViewClientListener;
    }

    public void doUpdateVisitedHistory(WebView webView, String str, boolean z) {
        super.doUpdateVisitedHistory(webView, str, z);
    }

    public String getFirstImgUrl() {
        return this.mImgUrl;
    }

    public void onFormResubmission(WebView webView, Message message, Message message2) {
        super.onFormResubmission(webView, message, message2);
    }

    public void onLoadResource(WebView webView, String str) {
        ad.a("onLoadResource: " + str);
        super.onLoadResource(webView, str);
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        if (this.mWebViewClientListener != null) {
            this.mWebViewClientListener.onPageFinished(webView, str);
        }
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
    }

    public void onReceivedHttpAuthRequest(WebView webView, HttpAuthHandler httpAuthHandler, String str, String str2) {
        super.onReceivedHttpAuthRequest(webView, httpAuthHandler, str, str2);
    }

    @TargetApi(12)
    public void onReceivedLoginRequest(WebView webView, String str, String str2, String str3) {
        super.onReceivedLoginRequest(webView, str, str2, str3);
    }

    public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        super.onReceivedSslError(webView, sslErrorHandler, sslError);
    }

    public void onScaleChanged(WebView webView, float f, float f2) {
        super.onScaleChanged(webView, f, f2);
    }

    public void onUnhandledKeyEvent(WebView webView, KeyEvent keyEvent) {
        super.onUnhandledKeyEvent(webView, keyEvent);
    }

    public void setNewsDetail(NewsDetail newsDetail) {
        this.mNewsDetail = newsDetail;
        this.mFirstImgUrl = ak.a(this.mNewsDetail);
    }

    public void setTopImageUrl(String str) {
        this.mFirstImgUrl = ak.a(str);
    }

    @TargetApi(11)
    public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        InputStream a2;
        String a3;
        ad.a("shouldInterceptRequest: " + str);
        if (!bj.a() || TextUtils.isEmpty(str) || str.startsWith("file://")) {
            return super.shouldInterceptRequest(webView, str);
        }
        if (!str.startsWith("http")) {
            return super.shouldInterceptRequest(webView, str);
        }
        if (TextUtils.isEmpty(this.mImgUrl) && !TextUtils.isEmpty(this.mFirstImgUrl) && (a3 = ak.a(str)) != null && this.mFirstImgUrl.startsWith(a3)) {
            this.mImgUrl = str;
            new Thread() {
                public void run() {
                    if (!m.d(WebViewClientDaily.this.mImgUrl)) {
                        byte[] bArr = null;
                        try {
                            bArr = b.b(WebViewClientDaily.this.mImgUrl);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        m.a(WebViewClientDaily.this.mImgUrl, bArr);
                        ad.a("down share: " + WebViewClientDaily.this.mImgUrl);
                    }
                }
            }.start();
        }
        return (!m.d(str) || (a2 = m.a(str)) == null) ? super.shouldInterceptRequest(webView, str) : new WebResourceResponse("image/*", Md5Util.DEFAULT_CHARSET, a2);
    }

    public boolean shouldOverrideKeyEvent(WebView webView, KeyEvent keyEvent) {
        return super.shouldOverrideKeyEvent(webView, keyEvent);
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        return this.mWebViewClientListener != null ? this.mWebViewClientListener.shouldOverrideUrlLoading(webView, str) : super.shouldOverrideUrlLoading(webView, str);
    }
}
