package com.qihoo360.daily.widget.dragdropgrid;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.GridView;

public class MovableGridView extends GridView {
    private int mNewPos = -1;
    private int mOldPos = -1;

    public MovableGridView(Context context) {
        super(context);
    }

    public MovableGridView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MovableGridView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (this.mOldPos >= 0 && this.mNewPos >= 0) {
            int childCount = getChildCount();
            for (int i5 = 0; i5 < childCount; i5++) {
                View childAt = getChildAt(i5);
                childAt.clearAnimation();
                if (i5 == 0) {
                    childAt.setVisibility(0);
                } else {
                    if (i5 == this.mNewPos) {
                        childAt.setVisibility(4);
                    } else {
                        childAt.setVisibility(0);
                    }
                    int min = Math.min(this.mOldPos, this.mNewPos);
                    int max = Math.max(this.mOldPos, this.mNewPos);
                    int i6 = 0;
                    if (this.mNewPos < this.mOldPos) {
                        i6 = -1;
                    }
                    if (this.mNewPos > this.mOldPos) {
                        i6 = 1;
                    }
                    if (i5 >= min && i5 <= max && i5 != this.mNewPos && this.mNewPos != this.mOldPos) {
                        View childAt2 = getChildAt(i6 + i5);
                        if (childAt2 != null) {
                            TranslateAnimation translateAnimation = new TranslateAnimation((float) (childAt2.getLeft() - childAt.getLeft()), 0.0f, (float) (childAt2.getTop() - childAt.getTop()), 0.0f);
                            translateAnimation.setDuration(250);
                            childAt.setAnimation(translateAnimation);
                        } else {
                            return;
                        }
                    }
                }
            }
            restoreSwitch();
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, View.MeasureSpec.makeMeasureSpec(536870911, Integer.MIN_VALUE));
    }

    public void restoreSwitch() {
        this.mOldPos = -1;
        this.mNewPos = -1;
    }

    public void switchView(int i, int i2) {
        this.mOldPos = i;
        this.mNewPos = i2;
    }
}
