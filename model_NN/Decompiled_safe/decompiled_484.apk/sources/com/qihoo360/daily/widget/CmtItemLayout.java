package com.qihoo360.daily.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RelativeLayout;
import com.qihoo360.daily.h.ad;

public class CmtItemLayout extends RelativeLayout {
    private float lastX;
    private float lastY;

    public CmtItemLayout(Context context) {
        super(context);
    }

    public CmtItemLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CmtItemLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public float getLastX() {
        return this.lastX;
    }

    public float getLastY() {
        return this.lastY;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 1:
                this.lastX = motionEvent.getX();
                this.lastY = motionEvent.getY();
                ad.a("onTouchEvent:ACTION_UP lastX: " + this.lastX);
                ad.a("onTouchEvent:ACTION_UP lastY: " + this.lastY);
                break;
        }
        return super.onTouchEvent(motionEvent);
    }
}
