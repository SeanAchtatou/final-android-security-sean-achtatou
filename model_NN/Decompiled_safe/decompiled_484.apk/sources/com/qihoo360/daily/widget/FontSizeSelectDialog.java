package com.qihoo360.daily.widget;

import android.app.Activity;
import android.view.View;
import android.widget.RadioGroup;
import com.qihoo360.daily.R;
import com.qihoo360.daily.f.a;
import com.qihoo360.daily.f.b;

public class FontSizeSelectDialog extends DialogView {
    private int mCurrentIndex;
    /* access modifiers changed from: private */
    public FoneSizeChangedListener mFoneSizeChangedListener;
    /* access modifiers changed from: private */
    public RadioGroup mRgFont;

    public interface FoneSizeChangedListener {
        void onFoneSizeChanged(CharSequence charSequence);
    }

    public FontSizeSelectDialog(Activity activity) {
        super(activity, (int) R.layout.dialog_font_size_select);
        init(this.view);
    }

    public FontSizeSelectDialog(Activity activity, int i) {
        super(activity, i);
        init(this.view);
    }

    public FontSizeSelectDialog(Activity activity, View view) {
        super(activity, view);
        init(view);
    }

    private int getCheckedId(int i) {
        switch (i) {
            case 0:
            default:
                return R.id.rb_small;
            case 1:
                return R.id.rb_normal;
            case 2:
                return R.id.rb_big;
            case 3:
                return R.id.rb_verybig;
        }
    }

    private void init(View view) {
        view.findViewById(R.id.btn_dialog_left).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                FontSizeSelectDialog.this.disMissDialog();
            }
        });
        this.mRgFont = (RadioGroup) view.findViewById(R.id.rg_font_setting);
        view.findViewById(R.id.btn_dialog_right).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int i = 0;
                String str = "小";
                if (FontSizeSelectDialog.this.mRgFont != null) {
                    switch (FontSizeSelectDialog.this.mRgFont.getCheckedRadioButtonId()) {
                        case R.id.rb_verybig:
                            str = "特大";
                            i = 3;
                            break;
                        case R.id.rb_big:
                            str = "大";
                            i = 2;
                            break;
                        case R.id.rb_normal:
                            str = "中";
                            i = 1;
                            break;
                        case R.id.rb_small:
                            str = "小";
                            break;
                    }
                    a.a(FontSizeSelectDialog.this.activity, b.a(i));
                    if (FontSizeSelectDialog.this.mFoneSizeChangedListener != null) {
                        FontSizeSelectDialog.this.mFoneSizeChangedListener.onFoneSizeChanged(str);
                    }
                    FontSizeSelectDialog.this.disMissDialog();
                }
            }
        });
        this.mCurrentIndex = a.b(this.activity).ordinal();
        this.mRgFont.check(getCheckedId(this.mCurrentIndex));
    }

    public FoneSizeChangedListener getFoneSizeChangedListener() {
        return this.mFoneSizeChangedListener;
    }

    public void setFoneSizeChangedListener(FoneSizeChangedListener foneSizeChangedListener) {
        this.mFoneSizeChangedListener = foneSizeChangedListener;
    }
}
