package com.qihoo360.daily.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class SoftInputRelativeLayout extends RelativeLayout {
    private OnSoftInputWindowChangeListener mOnSoftInputWindowChangeListener;
    private int vH;

    public interface OnSoftInputWindowChangeListener {
        void onChange(boolean z);
    }

    public SoftInputRelativeLayout(Context context) {
        super(context);
    }

    public SoftInputRelativeLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SoftInputRelativeLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i2 > this.vH) {
            this.vH = i2;
        }
        if (i4 > 0 && i2 == this.vH) {
            this.mOnSoftInputWindowChangeListener.onChange(true);
        }
    }

    public void setOnSoftInputWindowChangeListener(OnSoftInputWindowChangeListener onSoftInputWindowChangeListener) {
        this.mOnSoftInputWindowChangeListener = onSoftInputWindowChangeListener;
    }
}
