package com.qihoo360.daily.widget;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

public class PullRecyclerView extends RecyclerView {
    public static RecyclerView.LayoutManager mLayoutManager;
    private boolean mIntercerpt = false;
    ViewConfiguration mViewConfiguration = ViewConfiguration.get(getContext());

    public PullRecyclerView(Context context) {
        super(context);
    }

    public PullRecyclerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public PullRecyclerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public boolean isPullDownAble() {
        RecyclerView.LayoutManager layoutManager = getLayoutManager();
        mLayoutManager = layoutManager;
        if (layoutManager == null || !(layoutManager instanceof LinearLayoutManager) || ((LinearLayoutManager) layoutManager).getOrientation() != 1) {
            return false;
        }
        if (layoutManager.getChildCount() <= 0) {
            return true;
        }
        View childAt = layoutManager.getChildAt(0);
        return layoutManager.getDecoratedTop(childAt) > (-this.mViewConfiguration.getScaledTouchSlop()) && getChildPosition(childAt) == 0;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.mIntercerpt = false;
                break;
            case 1:
            case 3:
                this.mIntercerpt = false;
                break;
        }
        return this.mIntercerpt || super.onInterceptTouchEvent(motionEvent);
    }

    public void setIntercept(boolean z) {
        this.mIntercerpt = z;
    }
}
