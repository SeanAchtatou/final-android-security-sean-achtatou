package com.qihoo360.daily.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import com.qihoo360.daily.widget.TwoWayView;

public class HorizontalListViewInListView extends TwoWayView {
    private float lastX;
    private float lastY;
    private int mMaximumFlingVelocity;
    private int mMinimumFlingVelocity;
    private int mTouchSlot;
    private VelocityTracker mVelocityTracker;
    private ViewConfiguration mViewConfig;
    private WhichEdge whichEdge = WhichEdge.MIDDLE;

    public enum WhichEdge {
        LEFT,
        MIDDLE,
        RIGHT
    }

    public HorizontalListViewInListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    private void init(Context context) {
        this.mViewConfig = ViewConfiguration.get(context);
        this.mTouchSlot = this.mViewConfig.getScaledTouchSlop();
        this.mMinimumFlingVelocity = this.mViewConfig.getScaledMinimumFlingVelocity();
        this.mMaximumFlingVelocity = this.mViewConfig.getScaledMaximumFlingVelocity();
        setOrientation(TwoWayView.Orientation.HORIZONTAL);
    }

    /* JADX WARN: Type inference failed for: r0v7, types: [android.view.View] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void setRecyclerViewInterceptTouchEvent() {
        /*
            r2 = this;
            r1 = 1
        L_0x0001:
            android.view.ViewParent r0 = r2.getParent()
            boolean r0 = r0 instanceof android.view.View
            if (r0 == 0) goto L_0x0031
            android.view.ViewParent r0 = r2.getParent()
            boolean r0 = r0 instanceof com.qihoo360.daily.widget.PullRecyclerView
            if (r0 == 0) goto L_0x001a
            android.view.ViewParent r0 = r2.getParent()
            com.qihoo360.daily.widget.PullRecyclerView r0 = (com.qihoo360.daily.widget.PullRecyclerView) r0
            r0.setIntercept(r1)
        L_0x001a:
            android.view.ViewParent r0 = r2.getParent()
            boolean r0 = r0 instanceof android.support.v4.view.ViewPager
            if (r0 == 0) goto L_0x0029
            android.view.ViewParent r0 = r2.getParent()
            r0.requestDisallowInterceptTouchEvent(r1)
        L_0x0029:
            android.view.ViewParent r0 = r2.getParent()
            android.view.View r0 = (android.view.View) r0
            r2 = r0
            goto L_0x0001
        L_0x0031:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.widget.HorizontalListViewInListView.setRecyclerViewInterceptTouchEvent():void");
    }

    private void whichEdgeBePlaced(int i) {
        this.whichEdge = WhichEdge.MIDDLE;
        if (getAdapter() == null || getAdapter().getCount() <= 0) {
            this.whichEdge = WhichEdge.LEFT;
            return;
        }
        int firstVisiblePosition = getFirstVisiblePosition();
        if (firstVisiblePosition == 0) {
            View childAt = getChildAt(firstVisiblePosition);
            if (i > 0 && Math.abs(childAt.getLeft()) <= 0) {
                this.whichEdge = WhichEdge.LEFT;
            }
        }
        if (getLastVisiblePosition() == getAdapter().getCount() - 1) {
            View childAt2 = getChildAt(getChildCount() - 1);
            if (i < 0 && Math.abs(childAt2.getRight() - getWidth()) <= 0) {
                this.whichEdge = WhichEdge.RIGHT;
            }
        }
    }

    public boolean canFlip(int i) {
        switch (this.whichEdge) {
            case LEFT:
                if (i > 0) {
                    return true;
                }
                break;
            case RIGHT:
                if (i < 0) {
                    return true;
                }
                break;
        }
        return false;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean onInterceptTouchEvent = super.onInterceptTouchEvent(motionEvent);
        switch (motionEvent.getAction()) {
            case 0:
                this.lastX = motionEvent.getX();
                this.lastY = motionEvent.getY();
                break;
            case 2:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                int i = (int) (x - this.lastX);
                int i2 = (int) (y - this.lastY);
                if (Math.abs(i) != 0 && ((double) (Math.abs(i2) / Math.abs(i))) > 1.2d && Math.abs(i2) > this.mTouchSlot) {
                    setRecyclerViewInterceptTouchEvent();
                    break;
                }
        }
        return onInterceptTouchEvent;
    }
}
