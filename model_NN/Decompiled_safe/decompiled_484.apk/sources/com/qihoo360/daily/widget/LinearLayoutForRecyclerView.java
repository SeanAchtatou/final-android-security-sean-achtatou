package com.qihoo360.daily.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.qihoo360.daily.R;
import com.qihoo360.daily.h.ad;

public class LinearLayoutForRecyclerView extends LinearLayout {
    private static final int DIVIDER_HEIGHT = 1;
    private RecyclerView.Adapter<RecyclerView.ViewHolder> adapter;
    private Drawable mDivider;

    public LinearLayoutForRecyclerView(Context context) {
        super(context);
    }

    public LinearLayoutForRecyclerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @TargetApi(11)
    public LinearLayoutForRecyclerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    private void bindLinearLayout() {
        int itemCount = this.adapter.getItemCount();
        if (itemCount != 0) {
            LinearLayout.LayoutParams layoutParams = getOrientation() == 1 ? new LinearLayout.LayoutParams(-1, -2) : new LinearLayout.LayoutParams(0, -1, 1.0f);
            for (int i = 0; i < itemCount; i++) {
                RecyclerView.ViewHolder createViewHolder = this.adapter.createViewHolder(this, this.adapter.getItemViewType(i));
                this.adapter.onBindViewHolder(createViewHolder, i);
                View view = createViewHolder.itemView;
                if (view != null) {
                    addView(view, layoutParams);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (getOrientation() == 1) {
            drawVertical(canvas);
        } else {
            drawHorizontal(canvas);
        }
    }

    public void drawHorizontal(Canvas canvas) {
        int paddingTop = getPaddingTop();
        int height = getHeight() - getPaddingBottom();
        int childCount = getChildCount();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < childCount - 1) {
                View childAt = getChildAt(i2);
                int right = ((RecyclerView.LayoutParams) childAt.getLayoutParams()).rightMargin + childAt.getRight();
                int i3 = right + 1;
                if (this.mDivider == null) {
                    this.mDivider = new ColorDrawable(getResources().getColor(R.color.list_divider));
                }
                this.mDivider.setBounds(right, paddingTop, i3, height);
                canvas.save();
                canvas.clipRect(right, paddingTop, i3, height);
                this.mDivider.draw(canvas);
                canvas.restore();
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void drawVertical(Canvas canvas) {
        int paddingLeft = getPaddingLeft();
        int width = getWidth() - getPaddingRight();
        int childCount = getChildCount();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < childCount - 1) {
                View childAt = getChildAt(i2);
                int bottom = ((LinearLayout.LayoutParams) childAt.getLayoutParams()).bottomMargin + childAt.getBottom();
                int i3 = bottom + 1;
                if (this.mDivider == null) {
                    this.mDivider = new ColorDrawable(getResources().getColor(R.color.list_divider));
                }
                this.mDivider.setBounds(paddingLeft, bottom, width, i3);
                canvas.save();
                canvas.clipRect(paddingLeft, bottom, width, i3);
                this.mDivider.draw(canvas);
                canvas.restore();
                ad.a("drawVertical left:" + paddingLeft);
                ad.a("drawVertical top:" + bottom);
                ad.a("drawVertical right:" + width);
                ad.a("drawVertical bottom:" + i3);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public RecyclerView.Adapter<RecyclerView.ViewHolder> getAdpater() {
        return this.adapter;
    }

    public void setAdapter(RecyclerView.Adapter<RecyclerView.ViewHolder> adapter2) {
        this.adapter = adapter2;
        removeAllViews();
        bindLinearLayout();
    }
}
