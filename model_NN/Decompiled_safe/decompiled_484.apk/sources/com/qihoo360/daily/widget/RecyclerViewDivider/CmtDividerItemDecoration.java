package com.qihoo360.daily.widget.RecyclerViewDivider;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.qihoo360.daily.R;

public class CmtDividerItemDecoration extends RecyclerView.ItemDecoration {
    private static final int DIVIDER_HEIGHT = 1;
    public static final int HORIZONTAL_LIST = 0;
    public static final int VERTICAL_LIST = 1;
    private Drawable mDivider;
    private int mOrientation;

    public CmtDividerItemDecoration(Context context, int i) {
        this.mDivider = new ColorDrawable(context.getResources().getColor(R.color.list_divider));
        setOrientation(i);
    }

    public void drawHorizontal(Canvas canvas, RecyclerView recyclerView) {
        int paddingTop = recyclerView.getPaddingTop();
        int height = recyclerView.getHeight() - recyclerView.getPaddingBottom();
        int childCount = recyclerView.getChildCount();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < childCount - 1) {
                View childAt = recyclerView.getChildAt(i2);
                int right = ((RecyclerView.LayoutParams) childAt.getLayoutParams()).rightMargin + childAt.getRight();
                this.mDivider.setBounds(right, paddingTop, right + 1, height);
                this.mDivider.draw(canvas);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void drawVertical(Canvas canvas, RecyclerView recyclerView) {
        int paddingLeft = recyclerView.getPaddingLeft();
        int width = recyclerView.getWidth() - recyclerView.getPaddingRight();
        int childCount = recyclerView.getChildCount();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < childCount - 1) {
                if (i2 != 0) {
                    View childAt = recyclerView.getChildAt(i2);
                    int bottom = ((RecyclerView.LayoutParams) childAt.getLayoutParams()).bottomMargin + childAt.getBottom();
                    this.mDivider.setBounds(paddingLeft, bottom, width, bottom + 1);
                    this.mDivider.draw(canvas);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void getItemOffsets(Rect rect, int i, RecyclerView recyclerView) {
        if (this.mOrientation == 1) {
            rect.set(0, 0, 0, 1);
        } else {
            rect.set(0, 0, 1, 0);
        }
    }

    public void onDraw(Canvas canvas, RecyclerView recyclerView) {
        if (this.mOrientation == 1) {
            drawVertical(canvas, recyclerView);
        } else {
            drawHorizontal(canvas, recyclerView);
        }
    }

    public void setOrientation(int i) {
        if (i == 0 || i == 1) {
            this.mOrientation = i;
            return;
        }
        throw new IllegalArgumentException("invalid orientation");
    }
}
