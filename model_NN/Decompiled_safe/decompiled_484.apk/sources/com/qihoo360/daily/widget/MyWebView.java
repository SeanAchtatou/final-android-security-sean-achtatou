package com.qihoo360.daily.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;
import java.lang.reflect.Field;

public class MyWebView extends WebView {
    private static Field sConfigCallback;

    static {
        try {
            sConfigCallback = Class.forName("android.webkit.BrowserFrame").getDeclaredField("sConfigCallback");
            sConfigCallback.setAccessible(true);
        } catch (Exception e) {
        }
    }

    public MyWebView(Context context) {
        super(context);
    }

    public MyWebView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MyWebView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void destroy() {
        super.destroy();
        try {
            if (sConfigCallback != null) {
                sConfigCallback.set(null, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
