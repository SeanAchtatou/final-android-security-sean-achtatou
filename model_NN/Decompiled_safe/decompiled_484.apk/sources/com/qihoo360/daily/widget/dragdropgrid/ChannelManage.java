package com.qihoo360.daily.widget.dragdropgrid;

import android.content.Context;
import android.text.TextUtils;
import com.a.a.j;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.model.Channel;
import com.qihoo360.daily.model.ChannelType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class ChannelManage {
    public static int appendToSubscribedChannelsIfPossible(String str) {
        int i;
        ArrayList<Channel> arrayList;
        if (TextUtils.isEmpty(str)) {
            return -1;
        }
        ArrayList<Channel> unsubscribedChannels = getUnsubscribedChannels();
        if (unsubscribedChannels == null) {
            return -1;
        }
        int i2 = 0;
        Iterator<Channel> it = unsubscribedChannels.iterator();
        while (true) {
            i = i2;
            if (it.hasNext() && !str.equals(it.next().getAlias())) {
                i2 = i + 1;
            }
        }
        if (i == unsubscribedChannels.size()) {
            return -1;
        }
        Channel remove = unsubscribedChannels.remove(i);
        ArrayList<Channel> subscribedChannels = getSubscribedChannels();
        if (subscribedChannels != null) {
            subscribedChannels.add(remove);
            arrayList = subscribedChannels;
        } else {
            ArrayList<Channel> arrayList2 = new ArrayList<>();
            arrayList2.add(remove);
            arrayList = arrayList2;
        }
        j gson = Application.getGson();
        d.a(Application.getInstance(), "news_uchannels", gson.a(unsubscribedChannels));
        d.a(Application.getInstance(), "news_dchannels", gson.a(arrayList));
        return arrayList.size() - 1;
    }

    private static ArrayList<Channel> getDefaultSubscribedChannels() {
        ArrayList<Channel> arrayList = new ArrayList<>();
        arrayList.add(new Channel("最头条", ChannelType.TYPE_CHANNEL_RECOMMEND, 0));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_FUN_NAME, ChannelType.TYPE_CHANNEL_FUN, 0));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_LOCAL_NAME, ChannelType.TYPE_CHANNEL_LOCAL, 1));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_SOCIAL_NAME, "social", 0));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_TECH_NAME, ChannelType.TYPE_CHANNEL_TECH, 0));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_IT_NAME, ChannelType.TYPE_CHANNEL_IT, 0));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_AUTO_NAME, ChannelType.TYPE_CHANNEL_AUTO, 0));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_SPORTS_NAME, ChannelType.TYPE_CHANNEL_SPORTS, 0));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_IMGS_NAME, ChannelType.TYPE_CHANNEL_IMGS, 1));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_FINANCE_NAME, ChannelType.TYPE_CHANNEL_FINANCE, 0));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_MILITARY_NAME, ChannelType.TYPE_CHANNEL_MILITARY, 0));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_DOMESTIC_NAME, ChannelType.TYPE_CHANNEL_DOMESTIC, 0));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_JOKE_NAME, ChannelType.TYPE_CHANNEL_JOKE, 1));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_FUN_PIC_NAME, ChannelType.TYPE_CHANNEL_FUN_PIC, 1));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_GIF_NAME, ChannelType.TYPE_CHANNEL_GIF, 1));
        return arrayList;
    }

    private static ArrayList<Channel> getDefaultUnsubscribedChannels() {
        ArrayList<Channel> arrayList = new ArrayList<>();
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_INTERNATIONAL_NAME, ChannelType.TYPE_CHANNEL_INTERNATIONAL, 0));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_WOMEN_NAME, ChannelType.TYPE_CHANNEL_WOMEN, 0));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_HEALTH_NAME, ChannelType.TYPE_CHANNEL_HEALTH, 0));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_EDU_NAME, ChannelType.TYPE_CHANNEL_EDU, 0));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_GAME_NAME, ChannelType.TYPE_CHANNEL_GAME, 0));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_TRAVEL_NAME, ChannelType.TYPE_CHANNEL_TRAVEL, 0));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_BABY_NAME, ChannelType.TYPE_CHANNEL_BABY, 0));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_LISHI_NAME, ChannelType.TYPE_CHANNEL_LISHI, 0));
        arrayList.add(new Channel(ChannelType.TYPE_CHANNEL_HOUSE_NAME, ChannelType.TYPE_CHANNEL_HOUSE, 0));
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.ArrayList<com.qihoo360.daily.model.Channel> getSubscribedChannels() {
        /*
            r1 = 0
            com.qihoo360.daily.activity.Application r0 = com.qihoo360.daily.activity.Application.getInstance()
            java.lang.String r2 = "news_dchannels"
            java.lang.String r0 = com.qihoo360.daily.f.d.a(r0, r2)
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x0072
            com.a.a.j r2 = com.qihoo360.daily.activity.Application.getGson()
            com.qihoo360.daily.widget.dragdropgrid.ChannelManage$1 r3 = new com.qihoo360.daily.widget.dragdropgrid.ChannelManage$1
            r3.<init>()
            java.lang.reflect.Type r3 = r3.getType()
            java.lang.Object r0 = r2.a(r0, r3)     // Catch:{ Exception -> 0x0067 }
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ Exception -> 0x0067 }
            com.qihoo360.daily.activity.Application r1 = com.qihoo360.daily.activity.Application.getInstance()     // Catch:{ Exception -> 0x006d }
            int r1 = com.qihoo360.daily.h.a.c(r1)     // Catch:{ Exception -> 0x006d }
            r3 = 314(0x13a, float:4.4E-43)
            if (r1 != r3) goto L_0x004f
            r1 = 0
            java.lang.Object r1 = r0.get(r1)     // Catch:{ Exception -> 0x006d }
            com.qihoo360.daily.model.Channel r1 = (com.qihoo360.daily.model.Channel) r1     // Catch:{ Exception -> 0x006d }
            int r1 = r1.getType()     // Catch:{ Exception -> 0x006d }
            r3 = 2
            if (r1 != r3) goto L_0x004f
            r1 = 0
            r0.remove(r1)     // Catch:{ Exception -> 0x006d }
            java.lang.String r1 = r2.a(r0)     // Catch:{ Exception -> 0x006d }
            com.qihoo360.daily.activity.Application r2 = com.qihoo360.daily.activity.Application.getInstance()     // Catch:{ Exception -> 0x006d }
            java.lang.String r3 = "news_dchannels"
            com.qihoo360.daily.f.d.a(r2, r3, r1)     // Catch:{ Exception -> 0x006d }
        L_0x004f:
            if (r0 != 0) goto L_0x0066
            java.util.ArrayList r0 = getDefaultSubscribedChannels()
            com.a.a.j r1 = com.qihoo360.daily.activity.Application.getGson()
            java.lang.String r1 = r1.a(r0)
            com.qihoo360.daily.activity.Application r2 = com.qihoo360.daily.activity.Application.getInstance()
            java.lang.String r3 = "news_dchannels"
            com.qihoo360.daily.f.d.a(r2, r3, r1)
        L_0x0066:
            return r0
        L_0x0067:
            r0 = move-exception
        L_0x0068:
            r0.printStackTrace()
            r0 = r1
            goto L_0x004f
        L_0x006d:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0068
        L_0x0072:
            r0 = r1
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.widget.dragdropgrid.ChannelManage.getSubscribedChannels():java.util.ArrayList");
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.ArrayList<com.qihoo360.daily.model.Channel> getUnsubscribedChannels() {
        /*
            r1 = 0
            com.qihoo360.daily.activity.Application r0 = com.qihoo360.daily.activity.Application.getInstance()
            java.lang.String r2 = "news_uchannels"
            java.lang.String r0 = com.qihoo360.daily.f.d.a(r0, r2)
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x0040
            com.a.a.j r2 = com.qihoo360.daily.activity.Application.getGson()
            com.qihoo360.daily.widget.dragdropgrid.ChannelManage$2 r3 = new com.qihoo360.daily.widget.dragdropgrid.ChannelManage$2
            r3.<init>()
            java.lang.reflect.Type r3 = r3.getType()
            java.lang.Object r0 = r2.a(r0, r3)     // Catch:{ Exception -> 0x003c }
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ Exception -> 0x003c }
        L_0x0024:
            if (r0 != 0) goto L_0x003b
            java.util.ArrayList r0 = getDefaultUnsubscribedChannels()
            com.a.a.j r1 = com.qihoo360.daily.activity.Application.getGson()
            java.lang.String r1 = r1.a(r0)
            com.qihoo360.daily.activity.Application r2 = com.qihoo360.daily.activity.Application.getInstance()
            java.lang.String r3 = "news_uchannels"
            com.qihoo360.daily.f.d.a(r2, r3, r1)
        L_0x003b:
            return r0
        L_0x003c:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0040:
            r0 = r1
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.widget.dragdropgrid.ChannelManage.getUnsubscribedChannels():java.util.ArrayList");
    }

    public static void initDefaultChannels(Context context) {
        saveSubscribedChannels(context, getDefaultSubscribedChannels());
        saveUnsubscribedChannels(context, getDefaultUnsubscribedChannels());
    }

    public static void saveSubscribedChannels(Context context, List<Channel> list) {
        d.a(context, "news_dchannels", Application.getGson().a(list));
    }

    public static void saveUnsubscribedChannels(Context context, List<Channel> list) {
        d.a(context, "news_uchannels", Application.getGson().a(list));
    }
}
