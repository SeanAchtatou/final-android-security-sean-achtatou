package com.qihoo360.daily.h;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.ImageView;
import com.e.b.al;
import com.e.b.bb;
import com.e.b.bj;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.c.c;
import java.io.File;

public class af {

    /* renamed from: a  reason: collision with root package name */
    private static int f1091a = 500;

    /* renamed from: b  reason: collision with root package name */
    private static int f1092b = 400;

    public static String a(String str, int i, int i2) {
        if (i <= 0 || i2 <= 0 || TextUtils.isEmpty(str)) {
            return str;
        }
        if (!str.contains("/dmfd/__")) {
            return str.replaceFirst(".com/", ".com/dmfd/" + i + "_" + i2 + "_" + "70/");
        }
        String queryParameter = Uri.parse(str).getQueryParameter("zoom_out");
        if (!TextUtils.isEmpty(queryParameter)) {
            try {
                int parseInt = Integer.parseInt(queryParameter);
                if (parseInt > 0) {
                    i = (i * parseInt) / 100;
                    i2 = (parseInt * i2) / 100;
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return str.replaceFirst("/dmfd/__", "/dmfd/" + i + "_" + i2 + "_");
    }

    public static void a() {
        File file = new File(Application.getInstance().getCacheDir(), "picasso-cache");
        if (file.exists()) {
            a(file);
        }
    }

    private static void a(File file) {
        if (file.isDirectory()) {
            for (File a2 : file.listFiles()) {
                a(a2);
            }
        }
        file.delete();
    }

    public static void a(Object obj, ImageView imageView, String str, int i, int i2, boolean z, int i3) {
        String a2 = a(str, i, i2);
        if (!TextUtils.isEmpty(a2)) {
            bb b2 = al.a((Context) Application.getInstance()).a(a2).a(i3).a(i, i2).b();
            if (obj != null) {
                b2.a(obj);
            }
            if (z) {
                b2.a((bj) new c());
            }
            b2.a(imageView);
        }
    }

    public static int[] a(String str) {
        int[] iArr = {f1091a, f1092b};
        if (!TextUtils.isEmpty(str)) {
            try {
                String queryParameter = Uri.parse(str).getQueryParameter("size");
                if (!TextUtils.isEmpty(queryParameter)) {
                    String[] split = queryParameter.split("x");
                    if (split.length >= 2) {
                        if (!TextUtils.isEmpty(split[0])) {
                            iArr[0] = Integer.parseInt(split[0]);
                        }
                        if (!TextUtils.isEmpty(split[1])) {
                            iArr[1] = Integer.parseInt(split[1]);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return iArr;
    }

    public static File b() {
        return new File(Application.getInstance().getCacheDir(), "picasso-cache");
    }

    public static void b(Object obj, ImageView imageView, String str, int i, int i2, boolean z, int i3) {
        Resources resources = Application.getInstance().getResources();
        a(obj, imageView, str, resources.getDimensionPixelSize(i), resources.getDimensionPixelSize(i2), z, i3);
    }
}
