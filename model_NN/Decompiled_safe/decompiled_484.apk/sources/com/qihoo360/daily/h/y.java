package com.qihoo360.daily.h;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.SearchActivity;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.f.a;
import com.qihoo360.daily.f.b;
import com.qihoo360.daily.g.h;
import com.qihoo360.daily.g.k;
import com.qihoo360.daily.model.Result;

public class y implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Activity f1148a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public ab f1149b;
    private PopupWindow c;
    /* access modifiers changed from: private */
    public boolean d;
    private TextView e;
    /* access modifiers changed from: private */
    public String f;
    private c<Void, Result<Object>> g = new z(this);

    public y(Activity activity, ab abVar, String str, boolean z, boolean z2) {
        this.f1148a = activity;
        this.f1149b = abVar;
        this.f = str;
        this.d = z;
        a(z2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void}
     arg types: [android.view.View, int, int, int]
     candidates:
      ClspMth{android.widget.PopupWindow.<init>(android.content.Context, android.util.AttributeSet, int, int):void}
      ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void} */
    private void a(boolean z) {
        View inflate = LayoutInflater.from(this.f1148a).inflate((int) R.layout.more_layout, (ViewGroup) null);
        View findViewById = inflate.findViewById(R.id.more_text);
        RadioGroup radioGroup = (RadioGroup) inflate.findViewById(R.id.more_rg);
        if (z) {
            findViewById.setVisibility(0);
            radioGroup.setOnCheckedChangeListener(this);
            switch (aa.f1088a[a.b(this.f1148a).ordinal()]) {
                case 1:
                    radioGroup.check(R.id.more_rb_s);
                    break;
                case 2:
                    radioGroup.check(R.id.more_rb_m);
                    break;
                case 3:
                    radioGroup.check(R.id.more_rb_l);
                    break;
                case 4:
                    radioGroup.check(R.id.more_rb_xl);
                    break;
            }
        } else {
            findViewById.setVisibility(8);
        }
        inflate.findViewById(R.id.more_search).setOnClickListener(this);
        this.e = (TextView) inflate.findViewById(R.id.more_favor);
        this.e.setOnClickListener(this);
        if (this.d) {
            this.e.setText((int) R.string.more_favor_cancel);
            this.e.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.ic_personal_center_collect, 0, 0, 0);
        } else {
            this.e.setText((int) R.string.more_favor);
        }
        this.c = new PopupWindow(inflate, -2, -2, true);
        this.c.setTouchable(true);
        this.c.setOutsideTouchable(true);
        this.c.setBackgroundDrawable(this.f1148a.getResources().getDrawable(R.drawable.popwindow));
    }

    public void a() {
    }

    public void a(View view) {
        if (this.c != null) {
            this.c.showAsDropDown(view, view.getWidth(), -view.getHeight());
        }
    }

    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i) {
            case R.id.more_rb_xl:
                a.a(this.f1148a, b.X_LARGE);
                if (this.f1149b != null) {
                    this.f1149b.onTextSizeChange(WebSettings.TextSize.LARGEST);
                    break;
                }
                break;
            case R.id.more_rb_l:
                a.a(this.f1148a, b.LARGE);
                if (this.f1149b != null) {
                    this.f1149b.onTextSizeChange(WebSettings.TextSize.LARGER);
                    break;
                }
                break;
            case R.id.more_rb_m:
                a.a(this.f1148a, b.MEDIUM);
                if (this.f1149b != null) {
                    this.f1149b.onTextSizeChange(WebSettings.TextSize.NORMAL);
                    break;
                }
                break;
            case R.id.more_rb_s:
                a.a(this.f1148a, b.SMALL);
                if (this.f1149b != null) {
                    this.f1149b.onTextSizeChange(WebSettings.TextSize.SMALLER);
                    break;
                }
                break;
        }
        b.b(this.f1148a, "Bottom_menu_fontset");
    }

    public void onClick(View view) {
        k kVar;
        boolean z;
        switch (view.getId()) {
            case R.id.more_search:
                this.f1148a.startActivity(new Intent(this.f1148a, SearchActivity.class));
                if (this.c != null) {
                    this.c.dismiss();
                }
                b.b(this.f1148a, "Bottom_menu_search");
                return;
            case R.id.more_favor:
                if (this.c != null) {
                    this.c.dismiss();
                }
                k kVar2 = k.ADD;
                if (this.d) {
                    kVar = k.DEL;
                    z = true;
                } else {
                    kVar = kVar2;
                    z = false;
                }
                new h(kVar, this.f).a(this.g, z, new Object[0]);
                b.b(this.f1148a, "Bottom_menu_bookmark");
                return;
            default:
                return;
        }
    }
}
