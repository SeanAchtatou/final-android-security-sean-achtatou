package com.qihoo360.daily.h;

import android.app.Activity;
import android.content.Context;
import com.qihoo360.daily.R;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.model.Result;

class z extends c<Void, Result<Object>> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ y f1150a;

    z(y yVar) {
        this.f1150a = yVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.c(android.content.Context, java.lang.String, boolean):void
     arg types: [android.app.Activity, java.lang.String, boolean]
     candidates:
      com.qihoo360.daily.f.d.c(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.qihoo360.daily.f.d.c(android.content.Context, java.lang.String, int):void
      com.qihoo360.daily.f.d.c(android.content.Context, java.lang.String, long):void
      com.qihoo360.daily.f.d.c(android.content.Context, java.lang.String, boolean):void */
    /* renamed from: a */
    public void onNetRequest(int i, Result<Object> result) {
        int i2;
        boolean z = true;
        if (result != null) {
            int status = result.getStatus();
            if (status == 0) {
                switch (i) {
                    case 0:
                        boolean unused = this.f1150a.d = true;
                        i2 = R.string.favor_ok;
                        break;
                    default:
                        boolean unused2 = this.f1150a.d = false;
                        i2 = R.string.favor_cancel;
                        break;
                }
                Activity a2 = this.f1150a.f1148a;
                String b2 = this.f1150a.f;
                if (this.f1150a.d) {
                    z = false;
                }
                d.c((Context) a2, b2, z);
                ay.a(this.f1150a.f1148a).a(i2);
            } else if (status == 103) {
                ay.a(this.f1150a.f1148a).a((int) R.string.login_dialog_title_relogin);
            } else {
                ay.a(this.f1150a.f1148a).a(result.getMsg());
            }
            if (this.f1150a.f1149b != null) {
                this.f1150a.f1149b.onFavorChange(this.f1150a.d);
                return;
            }
            return;
        }
        ay.a(this.f1150a.f1148a).a((int) R.string.net_error);
    }
}
