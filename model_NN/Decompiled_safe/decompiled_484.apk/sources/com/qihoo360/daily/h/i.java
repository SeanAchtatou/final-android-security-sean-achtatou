package com.qihoo360.daily.h;

import java.io.IOException;

public class i {

    /* renamed from: a  reason: collision with root package name */
    public static j f1130a = null;

    public static synchronized j a() {
        j jVar;
        synchronized (i.class) {
            if (f1130a == null) {
                try {
                    f1130a = c();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            jVar = f1130a;
        }
        return jVar;
    }

    public static boolean b() {
        f1130a = a();
        if (f1130a == null) {
            return false;
        }
        return f1130a.c;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0048 A[Catch:{ IOException -> 0x00e2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x006e A[SYNTHETIC, Splitter:B:41:0x006e] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0101  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.qihoo360.daily.h.j c() {
        /*
            r1 = -1082130432(0xffffffffbf800000, float:-1.0)
            r2 = 0
            r10 = 1
            java.io.FileReader r11 = new java.io.FileReader     // Catch:{ IOException -> 0x00b6 }
            java.lang.String r0 = "/proc/cpuinfo"
            r11.<init>(r0)     // Catch:{ IOException -> 0x00b6 }
            java.io.BufferedReader r12 = new java.io.BufferedReader     // Catch:{ IOException -> 0x00b6 }
            r12.<init>(r11)     // Catch:{ IOException -> 0x00b6 }
            r0 = r2
            r3 = r2
            r4 = r2
            r5 = r2
            r6 = r2
            r7 = r2
            r8 = r2
            r2 = r1
        L_0x0018:
            java.lang.String r13 = r12.readLine()     // Catch:{ IOException -> 0x00c2 }
            if (r13 == 0) goto L_0x0096
            if (r5 != 0) goto L_0x0107
            java.lang.String r9 = "ARMv7"
            boolean r9 = r13.contains(r9)     // Catch:{ IOException -> 0x00c2 }
            if (r9 == 0) goto L_0x0107
            r6 = r10
            r5 = r10
        L_0x002a:
            if (r6 != 0) goto L_0x0104
            if (r5 != 0) goto L_0x0104
            java.lang.String r9 = "ARMv6"
            boolean r9 = r13.contains(r9)     // Catch:{ IOException -> 0x00cd }
            if (r9 == 0) goto L_0x0104
            r9 = r10
        L_0x0037:
            java.lang.String r5 = "clflush size"
            boolean r5 = r13.contains(r5)     // Catch:{ IOException -> 0x00d7 }
            if (r5 == 0) goto L_0x0101
            r5 = r10
        L_0x0040:
            java.lang.String r3 = "microsecond timers"
            boolean r3 = r13.contains(r3)     // Catch:{ IOException -> 0x00e2 }
            if (r3 == 0) goto L_0x0049
            r4 = r10
        L_0x0049:
            if (r8 != 0) goto L_0x0054
            java.lang.String r3 = "neon"
            boolean r3 = r13.contains(r3)     // Catch:{ IOException -> 0x00e2 }
            if (r3 == 0) goto L_0x0054
            r8 = r10
        L_0x0054:
            if (r7 != 0) goto L_0x005f
            java.lang.String r3 = "vfp"
            boolean r3 = r13.contains(r3)     // Catch:{ IOException -> 0x00e2 }
            if (r3 == 0) goto L_0x005f
            r7 = r10
        L_0x005f:
            java.lang.String r3 = "processor"
            boolean r3 = r13.startsWith(r3)     // Catch:{ IOException -> 0x00e2 }
            if (r3 == 0) goto L_0x00fe
            int r3 = r0 + 1
        L_0x0069:
            r0 = 0
            int r0 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x00f8
            java.util.Locale r0 = java.util.Locale.ENGLISH     // Catch:{ IOException -> 0x00ec }
            java.lang.String r0 = r13.toLowerCase(r0)     // Catch:{ IOException -> 0x00ec }
            java.lang.String r14 = "bogomips"
            boolean r0 = r0.contains(r14)     // Catch:{ IOException -> 0x00ec }
            if (r0 == 0) goto L_0x00f8
            java.lang.String r0 = ":"
            java.lang.String[] r0 = r13.split(r0)     // Catch:{ IOException -> 0x00ec }
            r13 = 1
            r0 = r0[r13]     // Catch:{ NumberFormatException -> 0x0093 }
            java.lang.String r0 = r0.trim()     // Catch:{ NumberFormatException -> 0x0093 }
            float r0 = java.lang.Float.parseFloat(r0)     // Catch:{ NumberFormatException -> 0x0093 }
        L_0x008d:
            r2 = r0
            r0 = r3
            r3 = r5
            r5 = r6
            r6 = r9
            goto L_0x0018
        L_0x0093:
            r0 = move-exception
            r0 = r1
            goto L_0x008d
        L_0x0096:
            r11.close()     // Catch:{ IOException -> 0x00c2 }
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r5
            r5 = r6
            r6 = r7
            r7 = r8
        L_0x00a0:
            com.qihoo360.daily.h.j r8 = new com.qihoo360.daily.h.j
            r8.<init>()
            r8.c = r5
            r8.d = r4
            r8.f1132b = r6
            r8.e = r3
            r8.f1131a = r7
            r8.f = r2
            r8.g = r1
            r8.h = r0
            return r8
        L_0x00b6:
            r0 = move-exception
            r8 = r0
            r3 = r2
            r4 = r2
            r5 = r2
            r6 = r2
            r7 = r2
            r0 = r2
        L_0x00be:
            r8.printStackTrace()
            goto L_0x00a0
        L_0x00c2:
            r1 = move-exception
            r15 = r1
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r5
            r5 = r6
            r6 = r7
            r7 = r8
            r8 = r15
            goto L_0x00be
        L_0x00cd:
            r1 = move-exception
            r15 = r1
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r6
            r6 = r7
            r7 = r8
            r8 = r15
            goto L_0x00be
        L_0x00d7:
            r1 = move-exception
            r5 = r9
            r15 = r2
            r2 = r3
            r3 = r4
            r4 = r6
            r6 = r7
            r7 = r8
            r8 = r1
            r1 = r15
            goto L_0x00be
        L_0x00e2:
            r1 = move-exception
            r3 = r4
            r4 = r6
            r6 = r7
            r7 = r8
            r8 = r1
            r1 = r2
            r2 = r5
            r5 = r9
            goto L_0x00be
        L_0x00ec:
            r0 = move-exception
            r1 = r2
            r2 = r5
            r5 = r9
            r15 = r3
            r3 = r4
            r4 = r6
            r6 = r7
            r7 = r8
            r8 = r0
            r0 = r15
            goto L_0x00be
        L_0x00f8:
            r0 = r3
            r3 = r5
            r5 = r6
            r6 = r9
            goto L_0x0018
        L_0x00fe:
            r3 = r0
            goto L_0x0069
        L_0x0101:
            r5 = r3
            goto L_0x0040
        L_0x0104:
            r9 = r5
            goto L_0x0037
        L_0x0107:
            r15 = r5
            r5 = r6
            r6 = r15
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.h.i.c():com.qihoo360.daily.h.j");
    }
}
