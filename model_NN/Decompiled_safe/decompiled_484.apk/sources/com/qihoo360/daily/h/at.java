package com.qihoo360.daily.h;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class at {

    /* renamed from: a  reason: collision with root package name */
    public boolean f1110a;

    /* renamed from: b  reason: collision with root package name */
    private au f1111b;

    private FileOutputStream a(String str, InputStream inputStream, long j) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        File file = new File(str);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
            file.createNewFile();
        }
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        byte[] bArr = new byte[32768];
        long j2 = 0;
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                break;
            } else if (this.f1110a) {
                a();
                break;
            } else {
                j2 += (long) read;
                int i = (int) ((((float) (100 * j2)) * 1.0f) / ((float) j));
                byteArrayOutputStream.write(bArr, 0, read);
                a(i);
                if (i == 100) {
                }
                if (fileOutputStream != null) {
                    fileOutputStream.write(bArr, 0, read);
                }
            }
        }
        a(byteArrayOutputStream.toByteArray());
        return fileOutputStream;
    }

    private void a() {
        if (this.f1111b != null) {
            this.f1111b.d();
        }
    }

    private void a(int i) {
        if (this.f1111b != null) {
            this.f1111b.a(i);
        }
    }

    private void a(byte[] bArr) {
        if (this.f1111b != null) {
            this.f1111b.b();
        }
    }

    private void b() {
        if (this.f1111b != null) {
            this.f1111b.c();
        }
    }

    private void c() {
        if (this.f1111b != null) {
            this.f1111b.a();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x00aa A[SYNTHETIC, Splitter:B:43:0x00aa] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00af A[Catch:{ IOException -> 0x00b8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00b4 A[Catch:{ IOException -> 0x00b8 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r9, java.lang.String r10, com.qihoo360.daily.h.au r11) {
        /*
            r8 = this;
            r2 = 0
            boolean r0 = android.text.TextUtils.isEmpty(r9)
            if (r0 != 0) goto L_0x000d
            boolean r0 = android.text.TextUtils.isEmpty(r10)
            if (r0 == 0) goto L_0x000e
        L_0x000d:
            return
        L_0x000e:
            r8.f1111b = r11
            r4 = 0
            r8.c()
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ IOException -> 0x0085, all -> 0x00a5 }
            r1 = 9
            if (r0 < r1) goto L_0x0051
            java.net.URL r0 = new java.net.URL     // Catch:{ IOException -> 0x0085, all -> 0x00a5 }
            r0.<init>(r9)     // Catch:{ IOException -> 0x0085, all -> 0x00a5 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IOException -> 0x0085, all -> 0x00a5 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x0085, all -> 0x00a5 }
            r1 = 15000(0x3a98, float:2.102E-41)
            r0.setConnectTimeout(r1)     // Catch:{ IOException -> 0x0085, all -> 0x00a5 }
            r1 = 15000(0x3a98, float:2.102E-41)
            r0.setReadTimeout(r1)     // Catch:{ IOException -> 0x0085, all -> 0x00a5 }
            int r1 = r0.getContentLength()     // Catch:{ IOException -> 0x0085, all -> 0x00a5 }
            long r6 = (long) r1     // Catch:{ IOException -> 0x0085, all -> 0x00a5 }
            java.io.InputStream r1 = r0.getInputStream()     // Catch:{ IOException -> 0x0085, all -> 0x00a5 }
            java.io.FileOutputStream r0 = r8.a(r10, r1, r6)     // Catch:{ IOException -> 0x00c6, all -> 0x00bd }
        L_0x003c:
            if (r0 == 0) goto L_0x0041
            r0.close()     // Catch:{ IOException -> 0x004c }
        L_0x0041:
            if (r1 == 0) goto L_0x0046
            r1.close()     // Catch:{ IOException -> 0x004c }
        L_0x0046:
            if (r2 == 0) goto L_0x000d
            r2.close()     // Catch:{ IOException -> 0x004c }
            goto L_0x000d
        L_0x004c:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x000d
        L_0x0051:
            org.apache.http.client.methods.HttpGet r0 = new org.apache.http.client.methods.HttpGet     // Catch:{ IOException -> 0x0085, all -> 0x00a5 }
            r0.<init>(r9)     // Catch:{ IOException -> 0x0085, all -> 0x00a5 }
            org.apache.http.params.HttpParams r1 = r0.getParams()     // Catch:{ IOException -> 0x0085, all -> 0x00a5 }
            r3 = 15000(0x3a98, float:2.102E-41)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r1, r3)     // Catch:{ IOException -> 0x0085, all -> 0x00a5 }
            r3 = 15000(0x3a98, float:2.102E-41)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r1, r3)     // Catch:{ IOException -> 0x0085, all -> 0x00a5 }
            java.lang.String r1 = "client/android"
            android.net.http.AndroidHttpClient r1 = android.net.http.AndroidHttpClient.newInstance(r1)     // Catch:{ IOException -> 0x0085, all -> 0x00a5 }
            org.apache.http.HttpResponse r0 = r1.execute(r0)     // Catch:{ IOException -> 0x00ca, all -> 0x00c1 }
            org.apache.http.HttpEntity r3 = r0.getEntity()     // Catch:{ IOException -> 0x00ca, all -> 0x00c1 }
            java.io.InputStream r3 = r3.getContent()     // Catch:{ IOException -> 0x00ca, all -> 0x00c1 }
            org.apache.http.HttpEntity r0 = r0.getEntity()     // Catch:{ IOException -> 0x00cd }
            long r6 = r0.getContentLength()     // Catch:{ IOException -> 0x00cd }
            java.io.FileOutputStream r0 = r8.a(r10, r3, r6)     // Catch:{ IOException -> 0x00cd }
            r2 = r1
            r1 = r3
            goto L_0x003c
        L_0x0085:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x0088:
            r0.printStackTrace()     // Catch:{ all -> 0x00c4 }
            r8.b()     // Catch:{ all -> 0x00c4 }
            if (r2 == 0) goto L_0x0093
            r4.close()     // Catch:{ IOException -> 0x009f }
        L_0x0093:
            if (r3 == 0) goto L_0x0098
            r3.close()     // Catch:{ IOException -> 0x009f }
        L_0x0098:
            if (r1 == 0) goto L_0x000d
            r1.close()     // Catch:{ IOException -> 0x009f }
            goto L_0x000d
        L_0x009f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x000d
        L_0x00a5:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x00a8:
            if (r2 == 0) goto L_0x00ad
            r4.close()     // Catch:{ IOException -> 0x00b8 }
        L_0x00ad:
            if (r3 == 0) goto L_0x00b2
            r3.close()     // Catch:{ IOException -> 0x00b8 }
        L_0x00b2:
            if (r1 == 0) goto L_0x00b7
            r1.close()     // Catch:{ IOException -> 0x00b8 }
        L_0x00b7:
            throw r0
        L_0x00b8:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00b7
        L_0x00bd:
            r0 = move-exception
            r3 = r1
            r1 = r2
            goto L_0x00a8
        L_0x00c1:
            r0 = move-exception
            r3 = r2
            goto L_0x00a8
        L_0x00c4:
            r0 = move-exception
            goto L_0x00a8
        L_0x00c6:
            r0 = move-exception
            r3 = r1
            r1 = r2
            goto L_0x0088
        L_0x00ca:
            r0 = move-exception
            r3 = r2
            goto L_0x0088
        L_0x00cd:
            r0 = move-exception
            goto L_0x0088
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.h.at.a(java.lang.String, java.lang.String, com.qihoo360.daily.h.au):void");
    }
}
