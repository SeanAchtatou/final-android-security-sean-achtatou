package com.qihoo360.daily.h;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Process;
import com.f.a.g;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.f.d;

class bd implements DialogInterface.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f1122a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ba f1123b;

    bd(ba baVar, boolean z) {
        this.f1123b = baVar;
        this.f1122a = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.a(android.content.Context, int):void
     arg types: [android.app.Activity, int]
     candidates:
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String):java.lang.String
      com.qihoo360.daily.f.d.a(android.content.Context, long):void
      com.qihoo360.daily.f.d.a(android.content.Context, com.qihoo360.daily.model.City):void
      com.qihoo360.daily.f.d.a(android.content.Context, com.qihoo360.daily.model.QdData):void
      com.qihoo360.daily.f.d.a(android.content.Context, com.qihoo360.daily.model.UpdateInfo):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.util.List<com.qihoo360.daily.model.FavouriteInfo>):void
      com.qihoo360.daily.f.d.a(android.content.Context, boolean):void
      com.qihoo360.daily.f.d.a(android.content.Context, int):void */
    public void onDismiss(DialogInterface dialogInterface) {
        if (this.f1122a && !this.f1123b.h) {
            d.a((Context) this.f1123b.f1116a, 0);
            g.d(Application.getInstance());
            Process.killProcess(Process.myPid());
        }
    }
}
