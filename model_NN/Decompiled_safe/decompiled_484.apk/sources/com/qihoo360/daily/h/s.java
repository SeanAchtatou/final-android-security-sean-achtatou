package com.qihoo360.daily.h;

import android.content.Context;
import android.graphics.Bitmap;

final class s implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f1143a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f1144b;
    final /* synthetic */ String c;
    final /* synthetic */ Bitmap d;

    s(Context context, String str, String str2, Bitmap bitmap) {
        this.f1143a = context;
        this.f1144b = str;
        this.c = str2;
        this.d = bitmap;
    }

    public void run() {
        r.a(this.f1143a, this.f1144b, this.c, this.d);
    }
}
