package com.qihoo360.daily.h;

import android.app.Activity;
import com.qihoo360.daily.activity.IndexActivity;
import com.qihoo360.daily.activity.SettingsActivity;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.g.b;
import com.qihoo360.daily.model.UpdateInfo;

public class bg extends c<Void, UpdateInfo> {

    /* renamed from: a  reason: collision with root package name */
    private Activity f1126a;

    /* renamed from: b  reason: collision with root package name */
    private bh f1127b;

    public bg(Activity activity) {
        this.f1126a = activity;
    }

    public void a() {
        new b(this.f1126a).a(this, new Object[0]);
    }

    /* renamed from: a */
    public void onNetRequest(int i, UpdateInfo updateInfo) {
        int c;
        if (updateInfo != null && this.f1126a != null && !this.f1126a.isFinishing() && (c = a.c(this.f1126a)) >= 0 && updateInfo.getVersionCode() > c) {
            if (this.f1127b != null) {
                this.f1127b.checkOver(updateInfo);
            }
            d.a(this.f1126a, updateInfo);
            if ((this.f1126a instanceof IndexActivity) || (this.f1126a instanceof SettingsActivity)) {
                new ba(this.f1126a, updateInfo).a(updateInfo.getForceUpdate());
            }
        }
    }

    public void a(bh bhVar) {
        this.f1127b = bhVar;
    }
}
