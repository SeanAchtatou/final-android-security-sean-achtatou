package com.qihoo360.daily.h;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

public final class p {
    public static int a(Context context) {
        if (context == null) {
            return 0;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        int i = displayMetrics.widthPixels;
        if (i <= 0) {
            return 720;
        }
        return i;
    }

    public static String a() {
        try {
            Class<?> cls = Class.forName("android.os.SystemProperties");
            return (String) cls.getMethod("get", String.class).invoke(cls, "ro.serialno");
        } catch (Exception e) {
            return "";
        }
    }
}
