package com.qihoo360.daily.h;

import android.app.Activity;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Base64;
import com.a.a.ab;
import com.f.a.g;
import com.mediav.ads.sdk.adcore.HttpCacher;
import com.qihoo.dynamic.util.Md5Util;
import com.qihoo.video.DateUtil;
import com.qihoo360.daily.a.ac;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.activity.DailyDetailActivity;
import com.qihoo360.daily.activity.NewsDetailActivity;
import com.qihoo360.daily.activity.SendCmtActivity;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.i.a;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.Result;
import com.qihoo360.daily.wxapi.WXInfoKeeper;
import com.qihoo360.daily.wxapi.WXToken;
import java.io.File;
import java.lang.reflect.Field;
import java.net.URI;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private static final SimpleDateFormat f1114a = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

    /* renamed from: b  reason: collision with root package name */
    private static final SimpleDateFormat f1115b = new SimpleDateFormat(DateUtil.FORMAT_DEFAULT, Locale.getDefault());

    public static int a(Context context, float f) {
        return (int) ((context.getResources().getDisplayMetrics().density * f) + 0.5f);
    }

    public static int a(ac acVar) {
        return (acVar == null || !acVar.i()) ? 10 : 20;
    }

    public static int a(ac acVar, Context context, String str) {
        int i = 6;
        if (acVar != null && acVar.i()) {
            return 20;
        }
        if (context != null && !TextUtils.isEmpty(str)) {
            i = (int) (((System.currentTimeMillis() - d.i(context, str)) / 30000) + ((long) 6));
            if (i > 15) {
                return 15;
            }
        }
        return i;
    }

    public static long a(String str, SimpleDateFormat simpleDateFormat) {
        if (TextUtils.isEmpty(str)) {
            return 0;
        }
        try {
            return simpleDateFormat.parse(str).getTime();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static Object a(Object obj) {
        Field[] declaredFields;
        if (!(obj == null || (declaredFields = obj.getClass().getDeclaredFields()) == null || declaredFields.length <= 0)) {
            for (Field field : declaredFields) {
                try {
                    field.setAccessible(true);
                    Object obj2 = field.get(obj);
                    if (obj2 != null && !Integer.class.isInstance(obj2) && !Boolean.class.isInstance(obj2) && !Double.class.isInstance(obj2) && !Long.class.isInstance(obj2) && !Byte.class.isInstance(obj2) && !Short.class.isInstance(obj2) && !Float.class.isInstance(obj2) && !Character.class.isInstance(obj2)) {
                        if (obj2 instanceof String) {
                            field.set(obj, URLDecoder.decode((String) obj2, "utf-8"));
                        } else if (obj2 instanceof List) {
                            List<Object> list = (List) obj2;
                            if (list.size() != 0) {
                                for (Object a2 : list) {
                                    a(a2);
                                }
                            }
                        } else {
                            a(obj2);
                        }
                        ad.a("fieldName:" + field.getName());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return obj;
    }

    public static String a(int i) {
        String str = null;
        if (i >= 0 && i < 10000) {
            str = String.valueOf(i);
        }
        if (i < 10000) {
            return str;
        }
        String str2 = (((float) Math.round(((float) i) / 1000.0f)) / 10.0f) + "万";
        return str2.contains(".0") ? str2.replace(".0", "") : str2;
    }

    public static String a(long j) {
        if (j == 0) {
            return null;
        }
        int max = Math.max(1, (int) ((System.currentTimeMillis() - j) / 1000));
        return max <= 60 ? "刚刚" : (max > 3600 || max <= 60) ? max < 86400 ? ((int) Math.floor((double) (max / HttpCacher.TIME_HOUR))) + "小时前" : (max <= 86400 || max >= 259200) ? new SimpleDateFormat("MM月dd日", Locale.getDefault()).format(Long.valueOf(j)) : ((int) Math.floor((double) (max / HttpCacher.TIME_DAY))) + "天前" : ((int) Math.max(1.0d, Math.floor((double) (max / 60)))) + "分钟前";
    }

    public static String a(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        String str2 = str + "#:DHiE'Q.@X]G:Pz'vZa`@e`7YNa<fEjRY(sL|94?\"SJXi85'.h5ZY9X7#hj5p/k";
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.reset();
            instance.update(str2.getBytes(Md5Util.DEFAULT_CHARSET));
            return a(instance.digest());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0066 A[SYNTHETIC, Splitter:B:35:0x0066] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0074 A[SYNTHETIC, Splitter:B:42:0x0074] */
    /* JADX WARNING: Removed duplicated region for block: B:55:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r5, java.lang.String r6) {
        /*
            r0 = 0
            boolean r1 = android.text.TextUtils.isEmpty(r5)
            if (r1 == 0) goto L_0x0008
        L_0x0007:
            return r0
        L_0x0008:
            java.lang.String r1 = b(r5)
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 != 0) goto L_0x0007
            boolean r2 = a()
            if (r2 == 0) goto L_0x0007
            java.lang.String r2 = j(r6)
            java.io.File r3 = new java.io.File
            r3.<init>(r2)
            boolean r4 = r3.exists()
            if (r4 != 0) goto L_0x002a
            r3.mkdirs()
        L_0x002a:
            java.io.File r3 = new java.io.File
            r3.<init>(r2, r1)
            boolean r1 = r3.exists()     // Catch:{ Exception -> 0x005f, all -> 0x006f }
            if (r1 != 0) goto L_0x0038
            r3.createNewFile()     // Catch:{ Exception -> 0x005f, all -> 0x006f }
        L_0x0038:
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x005f, all -> 0x006f }
            r2.<init>(r3)     // Catch:{ Exception -> 0x005f, all -> 0x006f }
            byte[] r3 = com.qihoo360.daily.h.aw.a(r2)     // Catch:{ Exception -> 0x007f }
            if (r3 == 0) goto L_0x0054
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x007f }
            r1.<init>(r3)     // Catch:{ Exception -> 0x007f }
            if (r2 == 0) goto L_0x004d
            r2.close()     // Catch:{ Exception -> 0x004f }
        L_0x004d:
            r0 = r1
            goto L_0x0007
        L_0x004f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x004d
        L_0x0054:
            if (r2 == 0) goto L_0x0007
            r2.close()     // Catch:{ Exception -> 0x005a }
            goto L_0x0007
        L_0x005a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0007
        L_0x005f:
            r1 = move-exception
            r2 = r0
        L_0x0061:
            r1.printStackTrace()     // Catch:{ all -> 0x007d }
            if (r2 == 0) goto L_0x0007
            r2.close()     // Catch:{ Exception -> 0x006a }
            goto L_0x0007
        L_0x006a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0007
        L_0x006f:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0072:
            if (r2 == 0) goto L_0x0077
            r2.close()     // Catch:{ Exception -> 0x0078 }
        L_0x0077:
            throw r0
        L_0x0078:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0077
        L_0x007d:
            r0 = move-exception
            goto L_0x0072
        L_0x007f:
            r1 = move-exception
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.h.b.a(java.lang.String, java.lang.String):java.lang.String");
    }

    private static String a(byte[] bArr) {
        Formatter formatter = new Formatter();
        int length = bArr.length;
        for (int i = 0; i < length; i++) {
            formatter.format("%02x", Byte.valueOf(bArr[i]));
        }
        String formatter2 = formatter.toString();
        formatter.close();
        return formatter2;
    }

    public static String a(String... strArr) {
        if (strArr == null || strArr.length == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (String str : strArr) {
            if (TextUtils.isEmpty(str)) {
                str = "0";
            }
            sb.append(str);
        }
        String d = d();
        sb.append(d);
        return b(sb.toString()) + d;
    }

    public static List<Info> a(Context context, String str) {
        if (context == null || TextUtils.isEmpty(str)) {
            return null;
        }
        return f(d.a(context, "channel_news_" + str));
    }

    public static void a(Context context, String str, ac acVar) {
        List<Info> a2;
        if (context != null && !TextUtils.isEmpty(str) && acVar != null && (a2 = acVar.a()) != null && a2.size() > 0) {
            ArrayList arrayList = new ArrayList();
            for (Info next : a2) {
                if (!a(next)) {
                    arrayList.add(next);
                }
            }
            d.a(context, "channel_news_" + str, Application.getGson().a(arrayList));
        }
    }

    public static boolean a() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static boolean a(long j, long j2) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTimeInMillis(j);
        GregorianCalendar gregorianCalendar2 = new GregorianCalendar();
        gregorianCalendar2.setTimeInMillis(j2);
        return gregorianCalendar.get(5) == gregorianCalendar2.get(5);
    }

    public static boolean a(Activity activity, String str) {
        boolean z = false;
        if (TextUtils.isEmpty(str) || activity == null) {
            return false;
        }
        try {
            List parse = URLEncodedUtils.parse(new URI(str), Md5Util.DEFAULT_CHARSET);
            if (parse != null && parse.size() > 0) {
                NameValuePair nameValuePair = null;
                int i = 0;
                while (i < parse.size()) {
                    nameValuePair = (NameValuePair) parse.get(i);
                    if (SendCmtActivity.TAG_DATA.equals(nameValuePair.getName())) {
                        break;
                    }
                    i++;
                }
                if (i < parse.size() && nameValuePair != null) {
                    Info info = (Info) Application.getGson().a(new String(Base64.decode(nameValuePair.getValue(), 0)), new e().getType());
                    if (info != null) {
                        Intent intent = new Intent(activity, info.isDailyInfo() ? DailyDetailActivity.class : NewsDetailActivity.class);
                        intent.putExtra("Info", info);
                        intent.putExtra("from", ChannelType.TYPE_INNER);
                        activity.startActivity(intent);
                        z = true;
                        if (info.isDailyInfo()) {
                            ad.a("今日新闻内部跳转");
                            b(activity, "Detail_inner_bounce");
                            return true;
                        }
                        ad.a("普通新闻跳转");
                        b(activity, "Detail_normal_bounce");
                    }
                }
            }
            return z;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean a(Context context) {
        if (context == null) {
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return false;
        }
        return activeNetworkInfo.getType() == 0;
    }

    public static boolean a(Info info) {
        if (info == null) {
            return false;
        }
        int v_t = info.getV_t();
        return v_t == -1 || v_t == -2 || v_t == -4;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0060 A[SYNTHETIC, Splitter:B:30:0x0060] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x006f A[SYNTHETIC, Splitter:B:36:0x006f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.lang.String r5, java.lang.String r6, java.lang.String r7) {
        /*
            r0 = 0
            boolean r1 = android.text.TextUtils.isEmpty(r5)
            if (r1 != 0) goto L_0x000d
            boolean r1 = android.text.TextUtils.isEmpty(r6)
            if (r1 == 0) goto L_0x000e
        L_0x000d:
            return r0
        L_0x000e:
            java.lang.String r1 = b(r5)
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 != 0) goto L_0x000d
            boolean r2 = a()
            if (r2 == 0) goto L_0x000d
            java.lang.String r2 = j(r7)
            java.io.File r3 = new java.io.File
            r3.<init>(r2)
            boolean r4 = r3.exists()
            if (r4 != 0) goto L_0x0030
            r3.mkdirs()
        L_0x0030:
            java.io.File r4 = new java.io.File
            r4.<init>(r2, r1)
            r2 = 0
            boolean r1 = r4.exists()     // Catch:{ Exception -> 0x005a }
            if (r1 != 0) goto L_0x003f
            r4.createNewFile()     // Catch:{ Exception -> 0x005a }
        L_0x003f:
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x005a }
            r3.<init>(r4)     // Catch:{ Exception -> 0x005a }
            byte[] r1 = r6.getBytes()     // Catch:{ Exception -> 0x007e, all -> 0x007b }
            r3.write(r1)     // Catch:{ Exception -> 0x007e, all -> 0x007b }
            r0 = 1
            if (r3 == 0) goto L_0x000d
            r3.flush()     // Catch:{ Exception -> 0x0055 }
            r3.close()     // Catch:{ Exception -> 0x0055 }
            goto L_0x000d
        L_0x0055:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000d
        L_0x005a:
            r1 = move-exception
        L_0x005b:
            r1.printStackTrace()     // Catch:{ all -> 0x006c }
            if (r2 == 0) goto L_0x000d
            r2.flush()     // Catch:{ Exception -> 0x0067 }
            r2.close()     // Catch:{ Exception -> 0x0067 }
            goto L_0x000d
        L_0x0067:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000d
        L_0x006c:
            r0 = move-exception
        L_0x006d:
            if (r2 == 0) goto L_0x0075
            r2.flush()     // Catch:{ Exception -> 0x0076 }
            r2.close()     // Catch:{ Exception -> 0x0076 }
        L_0x0075:
            throw r0
        L_0x0076:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0075
        L_0x007b:
            r0 = move-exception
            r2 = r3
            goto L_0x006d
        L_0x007e:
            r1 = move-exception
            r2 = r3
            goto L_0x005b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.h.b.a(java.lang.String, java.lang.String, java.lang.String):boolean");
    }

    public static byte[] a(Bitmap bitmap) {
        byte[] a2 = aw.a(bitmap, true);
        while (a2 != null && a2.length >= 32768) {
            a2 = aw.a(BitmapFactory.decodeByteArray(a2, 0, a2.length), true);
        }
        return a2;
    }

    public static String b() {
        return Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "360/daily" + File.separator;
    }

    public static String b(long j) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(Long.valueOf(j));
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v8, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v12, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v13, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String b(java.lang.String r6) {
        /*
            r0 = 0
            boolean r1 = android.text.TextUtils.isEmpty(r6)
            if (r1 == 0) goto L_0x0008
        L_0x0007:
            return r0
        L_0x0008:
            java.lang.String r1 = "MD5"
            java.security.MessageDigest r1 = java.security.MessageDigest.getInstance(r1)     // Catch:{ Exception -> 0x0044 }
            byte[] r2 = r6.getBytes()     // Catch:{ Exception -> 0x0044 }
            r1.update(r2)     // Catch:{ Exception -> 0x0044 }
            byte[] r3 = r1.digest()     // Catch:{ Exception -> 0x0044 }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0044 }
            java.lang.String r1 = ""
            r4.<init>(r1)     // Catch:{ Exception -> 0x0044 }
            r1 = 0
            r2 = r1
        L_0x0022:
            int r1 = r3.length     // Catch:{ Exception -> 0x0044 }
            if (r2 >= r1) goto L_0x003f
            byte r1 = r3[r2]     // Catch:{ Exception -> 0x0044 }
            if (r1 >= 0) goto L_0x002b
            int r1 = r1 + 256
        L_0x002b:
            r5 = 16
            if (r1 >= r5) goto L_0x0034
            java.lang.String r5 = "0"
            r4.append(r5)     // Catch:{ Exception -> 0x0044 }
        L_0x0034:
            java.lang.String r1 = java.lang.Integer.toHexString(r1)     // Catch:{ Exception -> 0x0044 }
            r4.append(r1)     // Catch:{ Exception -> 0x0044 }
            int r1 = r2 + 1
            r2 = r1
            goto L_0x0022
        L_0x003f:
            java.lang.String r0 = r4.toString()     // Catch:{ Exception -> 0x0044 }
            goto L_0x0007
        L_0x0044:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.h.b.b(java.lang.String):java.lang.String");
    }

    public static void b(Context context, String str) {
        if (context != null) {
            g.a(context, str);
        }
    }

    public static boolean b(Context context) {
        if (context == null) {
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return false;
        }
        return 1 == activeNetworkInfo.getType();
    }

    public static String c() {
        return f1114a.format(new Date());
    }

    public static String c(long j) {
        return new SimpleDateFormat("MM月dd日　yyyy", Locale.getDefault()).format(Long.valueOf(j));
    }

    public static String c(Context context) {
        com.sina.weibo.sdk.a.b a2 = a.a(context);
        return (a2 == null || TextUtils.isEmpty(a2.b())) ? a.a(context) : a2.b();
    }

    public static String c(String str) {
        return a(d(str));
    }

    public static void c(Context context, String str) {
        if (Build.VERSION.SDK_INT > 11) {
            ((ClipboardManager) context.getSystemService("clipboard")).setText(str.trim());
        } else {
            ((android.text.ClipboardManager) context.getSystemService("clipboard")).setText(str.trim());
        }
    }

    public static long d(String str) {
        return a(str, f1114a);
    }

    public static String d() {
        String b2 = b(String.valueOf(Math.random()));
        try {
            return (TextUtils.isEmpty(b2) || b2.length() <= 8) ? b2 : b2.substring(0, 8);
        } catch (Exception e) {
            e.printStackTrace();
            return b2;
        }
    }

    public static void d(Context context, String str) {
        new f(context, str).a(null, new Void[0]);
    }

    public static boolean d(Context context) {
        ConnectivityManager connectivityManager;
        if (context == null || (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isAvailable();
    }

    public static Result<List<Info>> e(String str) {
        try {
            return (Result) Application.getGson().a(str, new c().getType());
        } catch (ab e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void e(Context context) {
        new g(context).a(null, new Void[0]);
    }

    public static List<Info> f(String str) {
        try {
            return (List) Application.getGson().a(str, new d().getType());
        } catch (ab e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean g(String str) {
        return Pattern.compile("[0-9]*").matcher(str).matches();
    }

    public static boolean h(String str) {
        return System.currentTimeMillis() - d.i(Application.getInstance(), str) > 3600000;
    }

    public static boolean i(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        Application instance = Application.getInstance();
        String a2 = a.a(instance);
        com.sina.weibo.sdk.a.b a3 = a.a(instance);
        if (a3 == null || TextUtils.isEmpty(a3.b())) {
            WXToken tokenInfo4WX = WXInfoKeeper.getTokenInfo4WX(instance);
            if (tokenInfo4WX != null && !TextUtils.isEmpty(tokenInfo4WX.getOpenid())) {
                a2 = tokenInfo4WX.getOpenid();
            }
        } else {
            a2 = a3.b();
        }
        return str.equals(a2);
    }

    private static String j(String str) {
        return b() + str + File.separator;
    }
}
