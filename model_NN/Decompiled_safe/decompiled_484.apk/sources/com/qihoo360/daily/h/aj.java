package com.qihoo360.daily.h;

import android.content.Context;
import android.graphics.Bitmap;
import com.e.b.al;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.model.Info;
import java.io.IOException;

class aj extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1098a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Info f1099b;
    final /* synthetic */ ai c;

    aj(ai aiVar, String str, Info info) {
        this.c = aiVar;
        this.f1098a = str;
        this.f1099b = info;
    }

    public void run() {
        try {
            Bitmap unused = this.c.i = al.a((Context) Application.getInstance()).a(this.f1098a).c();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.c.a(this.c.c, this.f1099b);
    }
}
