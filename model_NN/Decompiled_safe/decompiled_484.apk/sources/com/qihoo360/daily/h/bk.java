package com.qihoo360.daily.h;

import android.annotation.SuppressLint;
import android.content.Context;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;

public class bk {
    public static int a(Context context, float f) {
        return (int) ((context.getResources().getDisplayMetrics().density * f) + 0.5f);
    }

    @SuppressLint({"SimpleDateFormat"})
    public static String a(long j) {
        return new SimpleDateFormat("mm:ss").format(Long.valueOf(j));
    }

    public static String a(String str) {
        if (str == null || "".equals(str.trim())) {
            return str;
        }
        try {
            str = URLDecoder.decode(str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (str == null || "".equals(str.trim()) || !str.contains("url=")) {
            return str;
        }
        String[] split = str.split("url=");
        if (split.length <= 1) {
            return str;
        }
        String str2 = split[1];
        try {
            str2 = URLEncoder.encode(split[1], "utf-8");
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        return split[0] + "url=" + str2;
    }
}
