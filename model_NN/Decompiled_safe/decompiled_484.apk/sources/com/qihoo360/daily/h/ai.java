package com.qihoo360.daily.h;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mediav.ads.sdk.adcore.Config;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.c.m;
import com.qihoo360.daily.g.av;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.NewsDetail;
import com.qihoo360.daily.model.ShareInfo;
import com.qihoo360.daily.widget.DialogView;
import java.io.File;
import java.io.IOException;

public class ai implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private DialogView f1096a;

    /* renamed from: b  reason: collision with root package name */
    private View f1097b;
    /* access modifiers changed from: private */
    public Activity c;
    private Info d;
    private String e;
    private String f;
    private String g;
    private String h;
    /* access modifiers changed from: private */
    public Bitmap i;
    private ShareInfo j;
    private String k = "0";
    private String l;
    private View m;
    private View n;
    private View o;
    private View p;
    private View q;

    public ai(Activity activity, Info info, NewsDetail newsDetail, String str, String str2) {
        this.c = activity;
        this.d = info;
        this.k = str;
        this.l = str2;
        if (newsDetail != null) {
            this.e = newsDetail.getShorturl();
            if (this.e == null) {
                this.e = newsDetail.getWapurl();
            }
        }
        this.h = m.e(ak.a(newsDetail));
        a(activity, info);
    }

    public ai(Activity activity, Info info, NewsDetail newsDetail, String str, String str2, String str3) {
        this.c = activity;
        this.d = info;
        this.k = str2;
        this.l = str3;
        if (newsDetail != null) {
            this.e = newsDetail.getShorturl();
            if (this.e == null) {
                this.e = newsDetail.getWapurl();
            }
        }
        this.h = m.e(str);
        a(activity, info);
    }

    public ai(Activity activity, Info info, String str, String str2, boolean z) {
        this.c = activity;
        this.d = info;
        this.e = str;
        if (z) {
            new aj(this, str2, info).start();
            return;
        }
        this.h = m.e(str2);
        a(activity, info);
    }

    public ai(Activity activity, String str, String str2, String str3, Bitmap bitmap, String str4, String str5) {
        this.c = activity;
        this.d = new Info();
        this.d.setTitle(str);
        this.d.setUrl(str3);
        this.k = str4;
        this.l = str5;
        this.f = str;
        this.g = str2;
        this.e = str3;
        this.i = bitmap;
        a(activity, (Info) null);
    }

    /* access modifiers changed from: private */
    public void a(Activity activity, Info info) {
        boolean z;
        int i2;
        if (TextUtils.isEmpty(this.e) && info != null) {
            this.e = info.getUrl();
        }
        if (info != null) {
            this.f = info.getTitle();
            this.g = info.getSummary();
            z = info.isDeepRead();
        } else {
            z = false;
        }
        if (this.e == null) {
            this.e = "";
        }
        if (this.f == null) {
            this.f = "";
        }
        if (this.g == null || "null".equals(this.g)) {
            this.g = "";
        }
        File fileStreamPath = activity.getFileStreamPath("discovery_share_image.jpg");
        if (!TextUtils.isEmpty(this.h) || this.i == null) {
            if (TextUtils.isEmpty(this.h)) {
                i2 = 1;
            }
            i2 = 0;
        } else {
            if (fileStreamPath != null && !fileStreamPath.exists()) {
                try {
                    fileStreamPath.createNewFile();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            r.b(activity.getApplicationContext(), null, "discovery_share_image.jpg", this.i);
            if (fileStreamPath != null) {
                this.h = fileStreamPath.getAbsolutePath();
                i2 = 0;
            }
            i2 = 0;
        }
        if (this.e.contains("?")) {
            this.e += "&src=";
        } else {
            this.e += "?src=";
        }
        this.j = new ShareInfo(this.e, this.f, this.g, this.h, z);
        this.j.setType(i2);
    }

    private void a(Context context, String str, ShareInfo shareInfo) {
        if (context != null && shareInfo != null) {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("text/*");
            String str2 = shareInfo.getUrl() + "other";
            String str3 = "【" + this.f + "】" + " " + str2 + " " + "（分享自@360每日看点）。" + "下载：" + "http://kandian.360.cn";
            if (shareInfo.isDeepRead()) {
                str3 = "【深度】" + this.f + " " + str2 + " " + "（分享自@360每日看点）。" + "下载：" + "http://kandian.360.cn";
            }
            intent.putExtra("android.intent.extra.TEXT", str3);
            context.startActivity(Intent.createChooser(intent, str));
        }
    }

    private void e() {
        this.f1096a.disMissDialog();
    }

    public void a() {
        if (this.f1096a == null) {
            this.f1097b = LayoutInflater.from(this.c).inflate((int) R.layout.layout_share_box, (ViewGroup) null);
            this.q = this.f1097b.findViewById(R.id.tv_share_cancel);
            this.q.setOnClickListener(this);
            this.m = this.f1097b.findViewById(R.id.tv_share_weibo);
            this.m.setOnClickListener(this);
            this.n = this.f1097b.findViewById(R.id.tv_share_weixin);
            this.n.setOnClickListener(this);
            this.o = this.f1097b.findViewById(R.id.tv_share_circle);
            this.o.setOnClickListener(this);
            this.p = this.f1097b.findViewById(R.id.tv_share_more);
            this.p.setOnClickListener(this);
            this.f1096a = new DialogView(this.c, this.f1097b);
        }
        this.f1096a.setFullWidth(true);
        this.f1096a.showDialog();
    }

    public void b() {
        ak.a(this.c, this.j);
        b.b(this.c, "Detail_share_sina");
        if (this.d != null) {
            new av(Application.getInstance(), this.d.getUrl(), this.l, this.d.getNid(), this.d.getA(), "b88f54f7f545e6b6", this.d.getTitle(), "2", this.k).a(null, new Void[0]);
        }
    }

    public void c() {
        ak.a(this.c, this.j, false);
        b.b(this.c, "Detail_share_weixin");
        if (this.d != null) {
            new av(Application.getInstance(), this.d.getUrl(), this.l, this.d.getNid(), this.d.getA(), "b88f54f7f545e6b6", this.d.getTitle(), "0", this.k).a(null, new Void[0]);
        }
    }

    public void d() {
        ak.a(this.c, this.j, true);
        b.b(this.c, "Detail_share_pengyouquan");
        if (this.d != null) {
            new av(Application.getInstance(), this.d.getUrl(), this.l, this.d.getNid(), this.d.getA(), "b88f54f7f545e6b6", this.d.getTitle(), Config.CHANNEL_ID, this.k).a(null, new Void[0]);
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_share_circle:
                ak.a(this.c, this.j, true);
                b.b(this.c, "Bottom_share_pengyouquan");
                if (this.d != null) {
                    new av(Application.getInstance(), this.d.getUrl(), this.l, this.d.getNid(), this.d.getA(), "b88f54f7f545e6b6", this.d.getTitle(), Config.CHANNEL_ID, this.k).a(null, new Void[0]);
                    break;
                }
                break;
            case R.id.tv_share_weixin:
                ak.a(this.c, this.j, false);
                b.b(this.c, "Bottom_share_weixin");
                if (this.d != null) {
                    new av(Application.getInstance(), this.d.getUrl(), this.l, this.d.getNid(), this.d.getA(), "b88f54f7f545e6b6", this.d.getTitle(), "0", this.k).a(null, new Void[0]);
                    break;
                }
                break;
            case R.id.tv_share_weibo:
                if (this.d != null) {
                    new av(Application.getInstance(), this.d.getUrl(), this.l, this.d.getNid(), this.d.getA(), "b88f54f7f545e6b6", this.d.getTitle(), "2", this.k).a(null, new Void[0]);
                }
                ak.a(this.c, this.j);
                b.b(this.c, "Bottom_share_sina");
                break;
            case R.id.tv_share_more:
                a(this.c, this.c.getResources().getString(R.string.share_more), this.j);
                b.b(this.c, "Bottom_share_more");
                if (this.d != null) {
                    new av(Application.getInstance(), this.d.getUrl(), this.l, this.d.getNid(), this.d.getA(), "b88f54f7f545e6b6", this.d.getTitle(), "3", this.k).a(null, new Void[0]);
                    break;
                }
                break;
        }
        e();
    }
}
