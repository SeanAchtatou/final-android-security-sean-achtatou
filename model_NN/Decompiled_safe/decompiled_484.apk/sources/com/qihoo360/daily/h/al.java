package com.qihoo360.daily.h;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.qihoo360.daily.c.e;

public class al {

    /* renamed from: a  reason: collision with root package name */
    private Activity f1100a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public ImageView f1101b;
    private ImageView c;
    /* access modifiers changed from: private */
    public RelativeLayout d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public int f;
    private int g;
    /* access modifiers changed from: private */
    public float h;
    /* access modifiers changed from: private */
    public float i;
    /* access modifiers changed from: private */
    public float j;
    /* access modifiers changed from: private */
    public boolean k = true;
    /* access modifiers changed from: private */
    public int l;
    /* access modifiers changed from: private */
    public int m;
    private int n;
    /* access modifiers changed from: private */
    public int o;
    /* access modifiers changed from: private */
    public int p;
    /* access modifiers changed from: private */
    public int q;
    private String r;
    /* access modifiers changed from: private */
    public String s;
    /* access modifiers changed from: private */
    public as t;
    private ImageView u;

    public al(Activity activity, int i2, int i3, int i4, int i5, RelativeLayout relativeLayout, String str, String str2, as asVar) {
        this.f1100a = activity;
        this.d = relativeLayout;
        this.r = str;
        this.s = str2;
        this.f = i2;
        this.e = i3;
        this.o = i4;
        this.n = i5;
        this.t = asVar;
    }

    /* access modifiers changed from: private */
    public void a(float f2, float f3, long j2) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(f2, f3);
        alphaAnimation.setDuration(j2);
        alphaAnimation.setAnimationListener(new an(this, f3));
        this.d.startAnimation(alphaAnimation);
    }

    /* access modifiers changed from: private */
    public void b() {
        c();
    }

    private void c() {
        aq aqVar = new aq(this, false);
        if (d()) {
            ar arVar = new ar(this, 1.0f, this.j, 1.0f, this.j, 1, 0.5f, 1, 0.5f);
            arVar.setFillAfter(true);
            aqVar.addAnimation(arVar);
            TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 0, (float) this.l, 1, 0.0f, 0, (float) this.m);
            translateAnimation.setFillAfter(true);
            aqVar.addAnimation(translateAnimation);
        } else {
            aqVar.addAnimation(new TranslateAnimation(1, 0.5f, 0, (float) this.l, 1, 0.5f, 0, (float) this.m));
        }
        aqVar.setInterpolator(new AccelerateInterpolator());
        aqVar.setAnimationListener(new ao(this, 1));
        aqVar.setDuration(3000);
        aqVar.setFillAfter(true);
        this.u.startAnimation(aqVar);
    }

    private boolean d() {
        return true;
    }

    private void e() {
        this.f1101b = f();
        if (this.d.getChildCount() != 0) {
            this.d.removeAllViews();
        }
        this.d.addView(this.f1101b);
        this.u = f();
        this.u.setVisibility(4);
        this.d.addView(this.u);
        e.a().a(this.f1101b.getContext(), this.r, new ap(this, this.f1101b), 0, false);
    }

    private ImageView f() {
        ImageView imageView = new ImageView(this.f1100a);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.setMargins(this.f, this.e, 0, 0);
        imageView.setScaleType(ImageView.ScaleType.MATRIX);
        imageView.setLayoutParams(layoutParams);
        return imageView;
    }

    /* access modifiers changed from: private */
    public void g() {
        AnimationSet animationSet = new AnimationSet(false);
        ScaleAnimation scaleAnimation = new ScaleAnimation(this.j, 1.0f, this.j, 1.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setFillAfter(true);
        animationSet.addAnimation(scaleAnimation);
        TranslateAnimation translateAnimation = new TranslateAnimation(0, (float) this.l, 1, 0.0f, 0, (float) this.m, 1, 0.0f);
        translateAnimation.setFillAfter(true);
        animationSet.addAnimation(translateAnimation);
        animationSet.setAnimationListener(new ao(this, 2));
        animationSet.setDuration(3000);
        animationSet.setFillAfter(true);
        if (this.k) {
            this.f1101b.startAnimation(animationSet);
        }
    }

    private void h() {
        if (this.c != null) {
            int[] iArr = new int[2];
            this.c.getLocationInWindow(iArr);
            this.f = iArr[0];
            this.e = iArr[1];
            this.o = this.c.getMeasuredWidth();
            this.n = this.c.getMeasuredHeight();
        }
        this.g = i();
        this.e -= this.g;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        this.f1100a.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.p = displayMetrics.widthPixels;
        this.q = displayMetrics.heightPixels;
        this.q -= this.g;
        this.l = (this.p / 2) - (this.f + (this.o / 2));
        this.m = (this.q / 2) - (this.e + (this.n / 2));
        this.h = ((float) this.p) / ((float) this.o);
        this.i = ((float) this.q) / ((float) this.n);
        if (this.h > this.i) {
            this.j = this.i;
        } else {
            this.j = this.h;
        }
    }

    private int i() {
        try {
            Class<?> cls = Class.forName("com.android.internal.R$dimen");
            return this.f1100a.getResources().getDimensionPixelSize(Integer.parseInt(cls.getField("status_bar_height").get(cls.newInstance()).toString()));
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public void a() {
        this.d.setVisibility(0);
        a(0.5f, 1.0f, 3000);
        this.d.setOnClickListener(new am(this));
        h();
        e();
    }
}
