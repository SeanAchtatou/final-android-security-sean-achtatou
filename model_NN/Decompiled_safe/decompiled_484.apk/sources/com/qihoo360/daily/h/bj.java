package com.qihoo360.daily.h;

import android.os.Build;

public class bj {
    public static boolean a() {
        return Build.VERSION.SDK_INT >= 11;
    }

    public static boolean b() {
        return Build.VERSION.SDK_INT >= 14;
    }

    public static boolean c() {
        return Build.VERSION.SDK_INT >= 12;
    }

    public static boolean d() {
        return Build.VERSION.SDK_INT >= 16;
    }

    public static boolean e() {
        return Build.VERSION.SDK_INT >= 19;
    }

    public static boolean f() {
        return Build.VERSION.SDK_INT >= 21;
    }
}
