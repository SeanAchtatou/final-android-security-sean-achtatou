package com.qihoo360.daily.h;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.qihoo360.daily.c.d;

public class ap extends d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ al f1107a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ap(al alVar, ImageView imageView) {
        super(imageView);
        this.f1107a = alVar;
    }

    public void imageLoaded(Drawable drawable, String str, boolean z) {
        super.imageLoaded(drawable, str, z);
        if (!(this.f1107a.f1101b.getMeasuredWidth() == 0 || this.f1107a.f1101b.getHeight() == 0)) {
            float unused = this.f1107a.h = ((float) this.f1107a.p) / ((float) this.f1107a.f1101b.getWidth());
            float unused2 = this.f1107a.i = ((float) this.f1107a.q) / ((float) this.f1107a.f1101b.getHeight());
            if (this.f1107a.h > this.f1107a.i) {
                float unused3 = this.f1107a.j = this.f1107a.i;
            } else {
                float unused4 = this.f1107a.j = this.f1107a.h;
            }
            int unused5 = this.f1107a.l = (this.f1107a.p / 2) - (this.f1107a.f + (this.f1107a.f1101b.getWidth() / 2));
            int unused6 = this.f1107a.m = (this.f1107a.q / 2) - (this.f1107a.e + (this.f1107a.f1101b.getHeight() / 2));
        }
        this.f1107a.b();
    }
}
