package com.qihoo360.daily.h;

import android.app.Dialog;
import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.Application;

public class l extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    private TextView f1135a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public TextView f1136b;
    /* access modifiers changed from: private */
    public TextView c;

    private l() {
        super(Application.getInstance(), R.style.dialog_view_theme);
        a();
    }

    /* synthetic */ l(m mVar) {
        this();
    }

    /* access modifiers changed from: private */
    public l a(String str) {
        this.f1135a.setText(str);
        return this;
    }

    private void a() {
        getWindow().setType(2003);
        setContentView((int) R.layout.debugdialog);
        this.f1136b = (TextView) findViewById(R.id.tv_title);
        this.f1135a = (TextView) findViewById(R.id.tv_info);
        this.c = (TextView) findViewById(R.id.tv_content);
        this.f1136b.setLongClickable(true);
        this.c.setLongClickable(true);
        this.f1136b.setOnLongClickListener(new n(this));
        this.c.setOnLongClickListener(new o(this));
    }

    public static void a(long j, String str, String str2) {
        new Handler(Looper.getMainLooper()).post(new m(j, str, str2));
    }

    /* access modifiers changed from: private */
    public l b(String str) {
        this.f1136b.setText(str);
        return this;
    }

    /* access modifiers changed from: private */
    public l c(String str) {
        this.c.setText(t.a(str));
        return this;
    }
}
