package com.qihoo360.daily.h;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.qihoo360.daily.R;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.g.bb;
import com.qihoo360.daily.widget.DialogView;
import java.io.File;

public class ag implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Activity f1093a;

    /* renamed from: b  reason: collision with root package name */
    private String f1094b;
    private DialogView c;
    private c<Void, String> d = new ah(this);

    public ag(Activity activity, String str) {
        this.f1093a = activity;
        this.f1094b = str;
        b();
    }

    private void b() {
        View inflate = LayoutInflater.from(this.f1093a).inflate((int) R.layout.save_img_layout, (ViewGroup) null);
        inflate.findViewById(R.id.save_img).setOnClickListener(this);
        inflate.findViewById(R.id.save_img_cancel).setOnClickListener(this);
        this.c = new DialogView(this.f1093a, inflate);
        this.c.setFullWidth(true);
        this.c.setGravity(80);
    }

    public void a() {
        if (this.c != null) {
            this.c.showDialog();
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save_img:
                String a2 = bb.a();
                String a3 = bb.a(this.f1094b);
                File file = null;
                if (!TextUtils.isEmpty(a3)) {
                    file = new File(a2, a3);
                }
                if (file == null || !file.exists()) {
                    new bb(this.f1094b).a(this.d, new Void[0]);
                } else {
                    ay.a(this.f1093a).a((int) R.string.saved);
                }
                if (this.c != null) {
                    this.c.disMissDialog();
                    return;
                }
                return;
            case R.id.save_img_cancel:
                if (this.c != null) {
                    this.c.disMissDialog();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
