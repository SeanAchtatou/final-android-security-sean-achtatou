package com.qihoo360.daily.h;

import android.content.Context;
import android.content.DialogInterface;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.f.d;

class w implements DialogInterface.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ v f1147a;

    w(v vVar) {
        this.f1147a = vVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, boolean):void
     arg types: [com.qihoo360.daily.activity.Application, java.lang.String, int]
     candidates:
      com.qihoo360.daily.f.d.a(android.content.Context, long, java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, int):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, long):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String[], java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, boolean):void */
    public void onDismiss(DialogInterface dialogInterface) {
        d.a((Context) Application.getInstance(), "login_status", false);
    }
}
