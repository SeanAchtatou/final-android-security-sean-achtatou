package com.qihoo360.daily.h;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.NotificationActivity;
import com.qihoo360.daily.model.UpdateInfo;
import com.qihoo360.daily.widget.DialogView;
import java.io.File;
import java.util.List;

public class ba {
    /* access modifiers changed from: private */
    public static boolean i = false;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Activity f1116a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public String f1117b = (h.f1128a + "apk/");
    /* access modifiers changed from: private */
    public String c = "DailyDiscovery.apk";
    /* access modifiers changed from: private */
    public UpdateInfo d;
    /* access modifiers changed from: private */
    public NotificationManager e;
    /* access modifiers changed from: private */
    public PendingIntent f;
    /* access modifiers changed from: private */
    public NotificationCompat.Builder g;
    /* access modifiers changed from: private */
    public boolean h;

    public ba(Activity activity, UpdateInfo updateInfo) {
        this.f1116a = activity;
        this.d = updateInfo;
        if (!TextUtils.isEmpty(updateInfo.getSaveFileName())) {
            this.c = updateInfo.getSaveFileName();
        }
        if (!TextUtils.isEmpty(updateInfo.getSavePath())) {
            this.f1117b = Environment.getExternalStorageDirectory().getPath() + updateInfo.getSavePath();
        }
    }

    private String a() {
        if (this.d == null || this.d.getUpdateContent() == null || this.d.getUpdateContent().size() == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        List<String> updateContent = this.d.getUpdateContent();
        if (updateContent != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= updateContent.size()) {
                    break;
                }
                sb.append(i3 + 1).append(".");
                sb.append(updateContent.get(i3)).append("\n");
                i2 = i3 + 1;
            }
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    private void b() {
        if (!i) {
            new Thread(new be(this)).start();
            i = true;
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        File file = new File(this.f1117b + this.c);
        if (file.exists() && file.isFile()) {
            file.delete();
        }
    }

    private void c(boolean z) {
        if (!this.f1116a.isFinishing() && this.d != null) {
            View inflate = this.f1116a.getLayoutInflater().inflate((int) R.layout.dialog_update_new, (ViewGroup) null);
            DialogView dialogView = new DialogView(this.f1116a, inflate);
            dialogView.setFullScreen(true);
            dialogView.setCanceledOnTouchOutside(false);
            if (!z) {
                TextView textView = (TextView) inflate.findViewById(R.id.tv_not_now);
                textView.setVisibility(0);
                textView.getPaint().setFlags(8);
                textView.setOnClickListener(new bb(this, dialogView));
            }
            inflate.findViewById(R.id.btn_update).setOnClickListener(new bc(this, dialogView));
            dialogView.setOnDialogDismissListener(new bd(this, z));
            ((TextView) inflate.findViewById(R.id.tv_dialog_content)).setText(a());
            if (!this.f1116a.isFinishing()) {
                dialogView.showDialog();
            }
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        String str = this.f1117b + this.c;
        File file = new File(str);
        if (file.exists() && a.b(this.f1116a, str)) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.addFlags(268435456);
            intent.setDataAndType(Uri.parse("file://" + file.getAbsolutePath()), "application/vnd.android.package-archive");
            this.f1116a.getApplicationContext().startActivity(intent);
        }
    }

    public void a(int i2) {
        c(i2 != 0);
    }

    public void a(boolean z) {
        if (!this.f1116a.isFinishing()) {
            this.e = (NotificationManager) this.f1116a.getSystemService("notification");
            this.g = new NotificationCompat.Builder(this.f1116a);
            Intent intent = new Intent(this.f1116a, NotificationActivity.class);
            intent.addFlags(65536);
            if (z) {
                b();
                this.g.setOngoing(true);
                this.g.setContentText("正在下载" + this.d.getVersionName() + "版本,完成" + 0 + "%");
            } else {
                intent.putExtra(NotificationActivity.EXTRA_DOWNLOAD, this.d);
                this.g.setContentText("每日看点最新版本" + this.d.getVersionName() + ",立即点击下载");
            }
            this.f = PendingIntent.getActivity(this.f1116a, 1, intent, 134217728);
            this.g.setContentTitle("每日看点版本更新").setSmallIcon(R.drawable.notification_icon).setWhen(System.currentTimeMillis()).setTicker("每日看点版本更新").setLargeIcon(BitmapFactory.decodeResource(this.f1116a.getResources(), R.drawable.ic_launcher)).setContentIntent(this.f);
            this.e.notify(0, this.g.build());
        }
    }
}
