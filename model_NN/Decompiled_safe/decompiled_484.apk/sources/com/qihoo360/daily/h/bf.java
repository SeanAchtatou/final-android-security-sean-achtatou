package com.qihoo360.daily.h;

import android.app.PendingIntent;
import android.content.Intent;
import com.qihoo360.daily.activity.NotificationActivity;

class bf implements au {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ be f1125a;

    bf(be beVar) {
        this.f1125a = beVar;
    }

    public void a() {
        this.f1125a.f1124a.g.setSmallIcon(17301633);
        this.f1125a.f1124a.g.setProgress(100, 0, false);
        this.f1125a.f1124a.g.setContentText("正在下载" + this.f1125a.f1124a.d.getVersionName() + "版本,完成" + 0 + "%");
        this.f1125a.f1124a.e.notify(0, this.f1125a.f1124a.g.build());
    }

    public void a(int i) {
        this.f1125a.f1124a.g.setProgress(100, i, false);
        this.f1125a.f1124a.e.notify(0, this.f1125a.f1124a.g.build());
        this.f1125a.f1124a.g.setContentText("正在下载" + this.f1125a.f1124a.d.getVersionName() + "版本,完成" + i + "%");
    }

    public void b() {
        boolean unused = ba.i = false;
        this.f1125a.f1124a.g.setContentText(this.f1125a.f1124a.d.getVersionName() + ",下载完成").setProgress(0, 0, false);
        this.f1125a.f1124a.g.setSmallIcon(17301634);
        this.f1125a.f1124a.e.notify(0, this.f1125a.f1124a.g.build());
        this.f1125a.f1124a.d();
        this.f1125a.f1124a.e.cancel(0);
    }

    public void c() {
        boolean unused = ba.i = false;
        this.f1125a.f1124a.c();
        this.f1125a.f1124a.e.cancel(0);
        this.f1125a.f1124a.a(false);
        this.f1125a.f1124a.g.setProgress(100, 0, false);
        this.f1125a.f1124a.g.setOngoing(false);
        this.f1125a.f1124a.g.setContentText("下载失败,点击重新下载");
        Intent intent = new Intent(this.f1125a.f1124a.f1116a, NotificationActivity.class);
        intent.addFlags(65536);
        intent.putExtra(NotificationActivity.EXTRA_DOWNLOAD, this.f1125a.f1124a.d);
        PendingIntent unused2 = this.f1125a.f1124a.f = PendingIntent.getActivity(this.f1125a.f1124a.f1116a, 1, intent, 134217728);
        this.f1125a.f1124a.g.setContentIntent(this.f1125a.f1124a.f);
        this.f1125a.f1124a.e.notify(0, this.f1125a.f1124a.g.build());
    }

    public void d() {
        boolean unused = ba.i = false;
    }
}
