package com.qihoo360.daily.h;

import android.view.animation.Animation;

class an implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ float f1103a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ al f1104b;

    an(al alVar, float f) {
        this.f1104b = alVar;
        this.f1103a = f;
    }

    public void onAnimationEnd(Animation animation) {
        if (this.f1103a != 1.0f) {
            this.f1104b.d.setVisibility(8);
        } else {
            this.f1104b.d.setVisibility(0);
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
