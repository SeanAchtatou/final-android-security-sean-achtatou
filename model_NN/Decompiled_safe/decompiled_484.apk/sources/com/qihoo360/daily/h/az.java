package com.qihoo360.daily.h;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import com.qihoo360.daily.i.a;
import com.qihoo360.daily.wxapi.WXInfoKeeper;
import com.qihoo360.daily.wxapi.WXToken;
import com.sina.weibo.sdk.a.b;
import java.io.File;
import java.io.IOException;

public class az {

    /* renamed from: a  reason: collision with root package name */
    private static String f1113a = null;

    public static synchronized void a(Context context) {
        synchronized (az.class) {
            Log.d("bum", "init");
            if (f1113a == null) {
                try {
                    c(context);
                    b(context);
                } catch (Throwable th) {
                    Log.e("bum", th.getMessage());
                }
            }
        }
        return;
    }

    private static void a(Context context, String str) {
        for (File file : new File(str).listFiles()) {
            if (file.isFile() && file.exists()) {
                String name = file.getName();
                if (name.startsWith("um.") && name.equals("um.4")) {
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0034 A[SYNTHETIC, Splitter:B:17:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0039 A[SYNTHETIC, Splitter:B:20:0x0039] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0064 A[SYNTHETIC, Splitter:B:41:0x0064] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0069 A[SYNTHETIC, Splitter:B:44:0x0069] */
    /* JADX WARNING: Removed duplicated region for block: B:62:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(android.content.Context r5, java.lang.String r6, java.lang.String r7) {
        /*
            r2 = 0
            java.io.File r0 = new java.io.File
            r0.<init>(r7)
            boolean r1 = r0.exists()
            if (r1 != 0) goto L_0x0077
            android.content.res.Resources r1 = r5.getResources()     // Catch:{ IOException -> 0x0098, all -> 0x005f }
            android.content.res.AssetManager r1 = r1.getAssets()     // Catch:{ IOException -> 0x0098, all -> 0x005f }
            java.io.InputStream r3 = r1.open(r6)     // Catch:{ IOException -> 0x0098, all -> 0x005f }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x009b, all -> 0x0090 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x009b, all -> 0x0090 }
            r0 = 4096(0x1000, float:5.74E-42)
            byte[] r0 = new byte[r0]     // Catch:{ IOException -> 0x002d, all -> 0x0093 }
        L_0x0021:
            int r2 = r3.read(r0)     // Catch:{ IOException -> 0x002d, all -> 0x0093 }
            r4 = -1
            if (r2 == r4) goto L_0x003d
            r4 = 0
            r1.write(r0, r4, r2)     // Catch:{ IOException -> 0x002d, all -> 0x0093 }
            goto L_0x0021
        L_0x002d:
            r0 = move-exception
            r2 = r3
        L_0x002f:
            r0.printStackTrace()     // Catch:{ all -> 0x0095 }
            if (r2 == 0) goto L_0x0037
            r2.close()     // Catch:{ IOException -> 0x0055 }
        L_0x0037:
            if (r1 == 0) goto L_0x003c
            r1.close()     // Catch:{ IOException -> 0x005a }
        L_0x003c:
            return
        L_0x003d:
            r1.flush()     // Catch:{ IOException -> 0x002d, all -> 0x0093 }
            if (r3 == 0) goto L_0x0045
            r3.close()     // Catch:{ IOException -> 0x0050 }
        L_0x0045:
            if (r1 == 0) goto L_0x003c
            r1.close()     // Catch:{ IOException -> 0x004b }
            goto L_0x003c
        L_0x004b:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x003c
        L_0x0050:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0045
        L_0x0055:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0037
        L_0x005a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x003c
        L_0x005f:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x0062:
            if (r3 == 0) goto L_0x0067
            r3.close()     // Catch:{ IOException -> 0x006d }
        L_0x0067:
            if (r1 == 0) goto L_0x006c
            r1.close()     // Catch:{ IOException -> 0x0072 }
        L_0x006c:
            throw r0
        L_0x006d:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0067
        L_0x0072:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006c
        L_0x0077:
            java.lang.String r0 = "bum"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.String r2 = " exist in files!"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.d(r0, r1)
            goto L_0x003c
        L_0x0090:
            r0 = move-exception
            r1 = r2
            goto L_0x0062
        L_0x0093:
            r0 = move-exception
            goto L_0x0062
        L_0x0095:
            r0 = move-exception
            r3 = r2
            goto L_0x0062
        L_0x0098:
            r0 = move-exception
            r1 = r2
            goto L_0x002f
        L_0x009b:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.h.az.a(android.content.Context, java.lang.String, java.lang.String):void");
    }

    private static void a(String str, String str2) {
        try {
            Runtime.getRuntime().exec(String.format("chmod %s %s", str2, str));
        } catch (IOException e) {
            Log.d("bum", e.getMessage());
        }
    }

    public static synchronized void b(Context context) {
        synchronized (az.class) {
            if (new File(f1113a).exists()) {
                String d = d(context);
                if (d != null) {
                    try {
                        Runtime.getRuntime().exec(d);
                    } catch (Exception e) {
                        Log.e("bum", e.getMessage());
                    }
                }
            }
        }
        return;
    }

    private static boolean b(Context context, String str) {
        for (PackageInfo packageInfo : context.getPackageManager().getInstalledPackages(0)) {
            if (packageInfo.packageName.equals(str)) {
                return true;
            }
        }
        return false;
    }

    private static void c(Context context) {
        File filesDir = context.getFilesDir();
        if (filesDir == null) {
            throw new IllegalStateException("c.getFilesDir() failed");
        }
        String str = filesDir.getAbsolutePath() + File.separator + "so_libs";
        File file = new File(str);
        f1113a = str + File.separator + "um.4";
        File file2 = new File(f1113a);
        if (!file.exists()) {
            file.mkdir();
            a(str, "755");
        }
        if (!file2.exists()) {
            a(context, str);
            try {
                a(context, "um.4", f1113a);
                a(f1113a, "700");
            } catch (Exception e) {
                throw new IllegalStateException(e.getMessage());
            }
        }
    }

    private static String d(Context context) {
        String str;
        String str2 = String.format("%s %s --execute ", f1113a, context.getPackageName()) + "am start -n ";
        if (b(context, "com.android.browser")) {
            str2 = str2 + "com.android.browser/.BrowserActivity";
        } else if (b(context, "com.google.android.browser")) {
            str2 = str2 + "com.google.android.browser/com.android.browser.BrowserActivity";
        } else if (b(context, "com.android.chrome")) {
            str2 = str2 + "com.android.chrome/com.google.android.apps.chrome.Main";
        }
        String a2 = a.a(context);
        b a3 = a.a(context);
        if (a3 == null || TextUtils.isEmpty(a3.b())) {
            WXToken tokenInfo4WX = WXInfoKeeper.getTokenInfo4WX(context);
            str = (tokenInfo4WX == null || TextUtils.isEmpty(tokenInfo4WX.getOpenid())) ? a2 : tokenInfo4WX.getOpenid();
        } else {
            str = a3.b();
        }
        String str3 = str2 + " -a android.intent.action.VIEW -d http://www.kandian.me/app_uninstall.html?userid=" + str + "\\&mid=" + a2 + "\\&version_name=" + a.b(context) + "\\&code_version=" + a.c(context) + "\\&channel=" + a.j(context) + "\\&phone_type=" + Build.MODEL + "\\&screen=" + (a.d(context) + "*" + a.e(context)) + "\\&network_type=" + a.h(context) + "\\&rom=" + Build.VERSION.RELEASE;
        return Build.VERSION.SDK_INT >= 17 ? str3 + " --user 0 " : str3;
    }
}
