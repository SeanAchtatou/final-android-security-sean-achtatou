package com.qihoo360.daily.h;

import android.view.View;
import com.qihoo360.daily.R;
import com.qihoo360.daily.widget.DialogView;

class bc implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DialogView f1120a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ba f1121b;

    bc(ba baVar, DialogView dialogView) {
        this.f1121b = baVar;
        this.f1120a = dialogView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.h.ba.a(com.qihoo360.daily.h.ba, boolean):boolean
     arg types: [com.qihoo360.daily.h.ba, int]
     candidates:
      com.qihoo360.daily.h.ba.a(com.qihoo360.daily.h.ba, android.app.PendingIntent):android.app.PendingIntent
      com.qihoo360.daily.h.ba.a(com.qihoo360.daily.h.ba, boolean):boolean */
    public void onClick(View view) {
        b.b(this.f1121b.f1116a, "UpdateBox_ok");
        boolean unused = this.f1121b.h = true;
        this.f1120a.disMissDialog();
        if (!b.a()) {
            ay.a(this.f1121b.f1116a).a((int) R.string.sdcard_err);
        } else {
            this.f1121b.a(true);
        }
    }
}
