package com.qihoo360.daily.h;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.media.session.PlaybackStateCompat;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class r {

    /* renamed from: a  reason: collision with root package name */
    static boolean f1141a = false;

    /* renamed from: b  reason: collision with root package name */
    static boolean f1142b = false;

    public static long a(File file) {
        File[] listFiles;
        long j = 0;
        if (file == null || !file.exists()) {
            return 0;
        }
        if (file.isFile()) {
            return file.length();
        }
        if (!file.isDirectory() || (listFiles = file.listFiles()) == null) {
            return 0;
        }
        int length = listFiles.length;
        int i = 0;
        while (i < length) {
            long a2 = a(listFiles[i]) + j;
            i++;
            j = a2;
        }
        return j;
    }

    public static String a(long j) {
        return j == 0 ? "" : (j <= 0 || j >= PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID) ? (j < PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID || j >= 1048576) ? j >= 1048576 ? (j / 1048576) + "M" : "" : (j / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID) + "K" : j + "B";
    }

    public static void a(InputStream inputStream, File file) {
        a(inputStream, new FileOutputStream(file));
    }

    public static void a(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[4096];
        while (true) {
            int read = inputStream.read(bArr);
            if (read > 0) {
                outputStream.write(bArr, 0, read);
            } else {
                outputStream.flush();
                inputStream.close();
                outputStream.close();
                return;
            }
        }
    }

    public static synchronized boolean a(Context context, String str, String str2, Bitmap bitmap) {
        boolean z = false;
        synchronized (r.class) {
            FileOutputStream fileOutputStream = null;
            if (str == null) {
                try {
                    fileOutputStream = context.openFileOutput(str2, 0);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    fileOutputStream = new FileOutputStream(new File(str, str2));
                } catch (FileNotFoundException e2) {
                    e2.printStackTrace();
                }
            }
            try {
                int height = bitmap.getHeight() * bitmap.getRowBytes();
                int i = 100;
                if (height > 1000000) {
                    i = 100 * (1000000 / height);
                }
                if (bitmap.compress(Bitmap.CompressFormat.JPEG, i, fileOutputStream)) {
                    fileOutputStream.flush();
                }
                z = true;
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e3) {
                        e3.printStackTrace();
                    }
                }
            } catch (Throwable th) {
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e4) {
                        e4.printStackTrace();
                    }
                }
                throw th;
            }
        }
        return z;
    }

    public static void b(Context context, String str, String str2, Bitmap bitmap) {
        new Thread(new s(context, str, str2, bitmap)).start();
    }

    public static boolean b(File file) {
        File[] listFiles;
        boolean z = true;
        if (file == null || !file.exists() || !file.canWrite()) {
            return false;
        }
        if (file.isFile()) {
            return file.delete();
        }
        if (!file.isDirectory() || (listFiles = file.listFiles()) == null) {
            return true;
        }
        for (File b2 : listFiles) {
            z &= b(b2);
        }
        return z;
    }
}
