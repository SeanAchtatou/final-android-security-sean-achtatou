package com.qihoo360.daily.h;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.i.b;
import com.qihoo360.daily.i.e;
import com.qihoo360.daily.widget.DialogView;
import com.qihoo360.daily.wxapi.WXHelper;

public class v implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private Activity f1145a;

    /* renamed from: b  reason: collision with root package name */
    private DialogView f1146b;
    private e c;
    private WXHelper.OnGetWeixinInfoCb d;
    private x e;

    public v(Activity activity, e eVar, WXHelper.OnGetWeixinInfoCb onGetWeixinInfoCb, boolean z) {
        this.f1145a = activity;
        this.c = eVar;
        this.d = onGetWeixinInfoCb;
        a(z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.app.Activity, java.lang.String, int]
     candidates:
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, int):int
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, long):long
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, java.lang.String):void
      com.qihoo360.daily.f.d.b(android.content.Context, java.lang.String, boolean):boolean */
    @SuppressLint({"InflateParams"})
    private void a(boolean z) {
        View view;
        LayoutInflater from = LayoutInflater.from(this.f1145a);
        if (d.b((Context) this.f1145a, "login_status", false)) {
            view = from.inflate((int) R.layout.logining_layout, (ViewGroup) null);
            view.findViewById(R.id.logining_anim).startAnimation(AnimationUtils.loadAnimation(this.f1145a, R.anim.rotate_loading_anim));
        } else {
            View inflate = from.inflate((int) R.layout.login_layout, (ViewGroup) null);
            if (z) {
                ((TextView) inflate.findViewById(R.id.tv_login_title)).setText((int) R.string.login_dialog_title_relogin);
            }
            inflate.findViewById(R.id.rl_login_btn).setOnClickListener(this);
            inflate.findViewById(R.id.rl_login_weixin).setOnClickListener(this);
            view = inflate;
        }
        view.findViewById(R.id.btn_cancel).setOnClickListener(this);
        this.f1146b = new DialogView(this.f1145a, view);
        this.f1146b.setFullWidth(true);
        this.f1146b.setGravity(80);
        this.f1146b.setOnDialogDismissListener(new w(this));
    }

    public void a() {
        if (this.f1146b != null) {
            this.f1146b.showDialog();
        }
    }

    public void a(x xVar) {
        this.e = xVar;
    }

    public DialogView b() {
        return this.f1146b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, boolean):void
     arg types: [android.app.Activity, java.lang.String, int]
     candidates:
      com.qihoo360.daily.f.d.a(android.content.Context, long, java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, int):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, long):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String[], java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, boolean):void */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_login_weixin:
                if (this.f1146b != null) {
                    this.f1146b.disMissDialog();
                }
                if (this.e != null) {
                    this.e.onClickLogin();
                }
                d.a((Context) this.f1145a, "login_status", true);
                WXHelper.INSTANCE.loginWeixin(this.d);
                return;
            case R.id.rl_login_btn:
                if (this.f1146b != null) {
                    this.f1146b.disMissDialog();
                }
                if (this.e != null) {
                    this.e.onClickLogin();
                }
                d.a((Context) this.f1145a, "login_status", true);
                b.INSTANCE.b(this.f1145a, this.c);
                return;
            case R.id.btn_cancel:
                if (this.f1146b != null) {
                    this.f1146b.disMissDialog();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
