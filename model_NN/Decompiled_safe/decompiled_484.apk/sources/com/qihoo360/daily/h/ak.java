package com.qihoo360.daily.h;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import com.qihoo360.daily.R;
import com.qihoo360.daily.c.o;
import com.qihoo360.daily.model.NewsContent;
import com.qihoo360.daily.model.NewsDetail;
import com.qihoo360.daily.model.ShareInfo;
import com.qihoo360.daily.wxapi.WXConfig;
import com.sina.weibo.sdk.api.ImageObject;
import com.sina.weibo.sdk.api.TextObject;
import com.sina.weibo.sdk.api.WebpageObject;
import com.sina.weibo.sdk.api.a.g;
import com.sina.weibo.sdk.api.a.j;
import com.sina.weibo.sdk.api.a.p;
import com.sina.weibo.sdk.api.h;
import com.sina.weibo.sdk.api.i;
import com.sina.weibo.sdk.d.m;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import java.util.ArrayList;
import java.util.Iterator;

public class ak {
    public static Bitmap a(String str, int i, int i2, boolean z) {
        int i3;
        int i4;
        Bitmap bitmap;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        try {
            options.inJustDecodeBounds = true;
            Bitmap decodeFile = BitmapFactory.decodeFile(str, options);
            if (decodeFile != null && !decodeFile.isRecycled()) {
                decodeFile.recycle();
            }
            double d = (((double) options.outHeight) * 1.0d) / ((double) i);
            double d2 = (((double) options.outWidth) * 1.0d) / ((double) i2);
            options.inSampleSize = (int) (z ? d > d2 ? d2 : d : d < d2 ? d2 : d);
            if (options.inSampleSize <= 1) {
                options.inSampleSize = 1;
            }
            while ((options.outHeight * options.outWidth) / options.inSampleSize > 2764800) {
                options.inSampleSize++;
            }
            if (z) {
                if (d > d2) {
                    i3 = i2;
                    i4 = (int) (((((double) i2) * 1.0d) * ((double) options.outHeight)) / ((double) options.outWidth));
                } else {
                    i3 = (int) (((((double) i) * 1.0d) * ((double) options.outWidth)) / ((double) options.outHeight));
                    i4 = i;
                }
            } else if (d < d2) {
                i3 = i2;
                i4 = (int) (((((double) i2) * 1.0d) * ((double) options.outHeight)) / ((double) options.outWidth));
            } else {
                i3 = (int) (((((double) i) * 1.0d) * ((double) options.outWidth)) / ((double) options.outHeight));
                i4 = i;
            }
            options.inJustDecodeBounds = false;
            Bitmap decodeFile2 = BitmapFactory.decodeFile(str, options);
            if (decodeFile2 == null) {
                return null;
            }
            Bitmap createScaledBitmap = Bitmap.createScaledBitmap(decodeFile2, i3, i4, true);
            if (createScaledBitmap == null) {
                createScaledBitmap = decodeFile2;
            }
            if (z) {
                bitmap = Bitmap.createBitmap(createScaledBitmap, (createScaledBitmap.getWidth() - i2) >> 1, (createScaledBitmap.getHeight() - i) >> 1, i2, i);
                if (bitmap == null) {
                    return createScaledBitmap;
                }
                createScaledBitmap.recycle();
            } else {
                bitmap = createScaledBitmap;
            }
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String a(NewsDetail newsDetail) {
        String str;
        if (newsDetail == null) {
            return "";
        }
        ArrayList<NewsContent> content = newsDetail.getContent();
        if (content != null && content.size() > 0) {
            Iterator<NewsContent> it = content.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                NewsContent next = it.next();
                if (next.getType() != null && "img".equals(next.getType()) && !TextUtils.isEmpty(next.getValue())) {
                    str = next.getValue();
                    break;
                }
            }
            return str;
        }
        str = "";
        return str;
    }

    public static String a(String str) {
        return TextUtils.isEmpty(str) ? "" : str.replaceFirst("/dmfd/.*/", "/");
    }

    public static void a(Activity activity, ShareInfo shareInfo) {
        g a2 = p.a(activity, "432085026", false);
        if (!a2.a()) {
            ay.a(activity).a((int) R.string.share_weibo_uninstalled);
            return;
        }
        String str = shareInfo.getUrl() + "weibo";
        a2.c();
        int b2 = a2.b();
        String str2 = "【" + shareInfo.getTitle() + "】";
        if (shareInfo.isDeepRead()) {
            str2 = "【深度】" + shareInfo.getTitle();
        }
        String str3 = "【" + shareInfo.getTitle() + "】" + " " + str + " " + "（分享自@360每日看点）。" + "下载：" + "http://kandian.360.cn";
        if (shareInfo.isDeepRead()) {
            str3 = "【深度】" + shareInfo.getTitle() + " " + str + " " + "（分享自@360每日看点）。" + "下载：" + "http://kandian.360.cn";
        }
        Bitmap a3 = shareInfo.getType() != 1 ? a(shareInfo.getImgPath(), 150, 150, true) : BitmapFactory.decodeResource(activity.getResources(), R.drawable.share_logo);
        if (b2 >= 10351) {
            i iVar = new i();
            ImageObject imageObject = new ImageObject();
            imageObject.c = m.a();
            imageObject.a(shareInfo.getType() != 1 ? BitmapFactory.decodeFile(shareInfo.getImgPath(), o.a(shareInfo.getImgPath(), -1)) : a3);
            imageObject.f = aw.a(a3, true);
            iVar.f1192b = imageObject;
            TextObject textObject = new TextObject();
            textObject.g = str3;
            iVar.f1191a = textObject;
            j jVar = new j();
            jVar.f1181a = String.valueOf(System.currentTimeMillis());
            jVar.f1184b = iVar;
            a2.a(activity, jVar);
            return;
        }
        h hVar = new h();
        WebpageObject webpageObject = new WebpageObject();
        webpageObject.c = m.a();
        webpageObject.d = str2;
        webpageObject.e = shareInfo.getContent();
        webpageObject.f = aw.a(a3, true);
        webpageObject.f1179a = str;
        webpageObject.g = "（分享自@360每日看点）。下载：http://kandian.360.cn";
        hVar.f1190a = webpageObject;
        com.sina.weibo.sdk.api.a.h hVar2 = new com.sina.weibo.sdk.api.a.h();
        hVar2.f1181a = String.valueOf(System.currentTimeMillis());
        hVar2.f1183b = hVar;
        a2.a(activity, hVar2);
    }

    public static void a(Activity activity, ShareInfo shareInfo, boolean z) {
        if (activity != null && shareInfo != null) {
            IWXAPI createWXAPI = WXAPIFactory.createWXAPI(activity, WXConfig.WX_APP_ID, true);
            if (!createWXAPI.isWXAppInstalled()) {
                ay.a(activity).a((int) R.string.share_weixin_uninstalled);
            } else if (!z || a(activity)) {
                String str = z ? shareInfo.getUrl() + "pengyouquan" : shareInfo.getUrl() + "weixin";
                createWXAPI.registerApp(WXConfig.WX_APP_ID);
                WXWebpageObject wXWebpageObject = new WXWebpageObject();
                wXWebpageObject.webpageUrl = str;
                WXMediaMessage wXMediaMessage = new WXMediaMessage(wXWebpageObject);
                wXMediaMessage.title = "【" + shareInfo.getTitle() + "】";
                if (shareInfo.isDeepRead()) {
                    wXMediaMessage.title = "【深度】" + shareInfo.getTitle();
                }
                wXMediaMessage.description = shareInfo.getContent();
                wXMediaMessage.thumbData = b.a(shareInfo.getType() != 1 ? a(shareInfo.getImgPath(), 150, 150, true) : BitmapFactory.decodeResource(activity.getResources(), R.drawable.share_logo));
                com.tencent.mm.sdk.modelmsg.i iVar = new com.tencent.mm.sdk.modelmsg.i();
                iVar.f1370a = "webpage" + System.currentTimeMillis();
                iVar.c = wXMediaMessage;
                iVar.d = z ? 1 : 0;
                createWXAPI.sendReq(iVar);
            } else {
                ay.a(activity).a((int) R.string.share_weixin_uninstalled);
            }
        }
    }

    public static boolean a(Activity activity) {
        return WXAPIFactory.createWXAPI(activity, WXConfig.WX_APP_ID, true).getWXAppSupportAPI() >= 553779201;
    }
}
