package com.qihoo360.daily.h;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.mediav.ads.sdk.adcore.Config;
import com.qihoo.dynamic.EmulatorCheck;
import com.qihoo.dynamic.util.Md5Util;
import com.qihoo360.daily.activity.SearchActivity;
import com.qihoo360.daily.d.a;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.wxapi.WXConfig;
import com.qihoo360.daily.wxapi.WXInfoKeeper;
import com.qihoo360.daily.wxapi.WXToken;
import com.sina.weibo.sdk.a.b;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

public class bi {
    public static URI a(Context context) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("mo", "news"));
            arrayList.add(new BasicNameValuePair("ra", "feedback"));
            arrayList.add(new BasicNameValuePair("rom", String.valueOf(Build.VERSION.SDK_INT)));
            arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
            arrayList.add(new BasicNameValuePair("m2", a.a(context)));
            a.a(arrayList);
            URI createURI = URIUtils.createURI("http", "jk.kandian.360.cn", -1, "index.php", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI a(Context context, int i, String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mo", "news"));
        arrayList.add(new BasicNameValuePair("ro", "support"));
        arrayList.add(new BasicNameValuePair("ra", "support"));
        arrayList.add(new BasicNameValuePair("op", String.valueOf(i)));
        arrayList.add(new BasicNameValuePair(SearchActivity.TAG_URL, str));
        arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
        String a2 = a.a(context);
        arrayList.add(new BasicNameValuePair("token", a2));
        a(arrayList, context, a2);
        a.a(arrayList);
        try {
            URI createURI = URIUtils.createURI("http", "jk.kandian.360.cn", -1, "index.php", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI a(Context context, String str) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("type", "day0"));
            if (!TextUtils.isEmpty(str)) {
                arrayList.add(new BasicNameValuePair("sign", str));
            }
            arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
            arrayList.add(new BasicNameValuePair("token", a.a(context)));
            a.a(arrayList);
            URI createURI = URIUtils.createURI("http", "sh.qihoo.com/api", -1, "hot_new.json", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI a(Context context, String str, String str2) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("key", str));
            arrayList.add(new BasicNameValuePair("comment_id", (String) null));
            arrayList.add(new BasicNameValuePair("pointer", str2));
            arrayList.add(new BasicNameValuePair("src", "kandian_android"));
            arrayList.add(new BasicNameValuePair("verify", b.a("3DDDC1DC6A27A2EF649325B334109296", str, null, str2)));
            arrayList.add(new BasicNameValuePair("token", a.a(context)));
            arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
            a.a(arrayList);
            URI createURI = URIUtils.createURI("http", "comment.kandian.360.cn", -1, "fetchcomment", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.h.bi.a(java.util.List<org.apache.http.NameValuePair>, android.content.Context, java.lang.String, boolean):void
     arg types: [java.util.ArrayList, android.content.Context, java.lang.String, int]
     candidates:
      com.qihoo360.daily.h.bi.a(android.content.Context, java.lang.String, java.lang.String, int):java.net.URI
      com.qihoo360.daily.h.bi.a(java.util.List<org.apache.http.NameValuePair>, android.content.Context, java.lang.String, boolean):void */
    public static URI a(Context context, String str, String str2, int i) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("c_n", String.valueOf(i)));
        String a2 = a.a(context);
        arrayList.add(new BasicNameValuePair("token", a2));
        arrayList.add(new BasicNameValuePair("c_pos", str));
        arrayList.add(new BasicNameValuePair("c_c", str2));
        arrayList.add(new BasicNameValuePair("s_code", b.b(a2 + "_4!e@a#2$6^b&9*6_")));
        arrayList.add(new BasicNameValuePair("fr", "kandian"));
        arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
        a((List<NameValuePair>) arrayList, context, a2, true);
        a.a(arrayList);
        try {
            URI createURI = URIUtils.createURI("http", "recommend.kandian.360.cn", -1, "youlike", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI a(Context context, String str, String str2, int i, int i2, int i3, String str3) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair(SearchActivity.TAG_URL, str2));
            arrayList.add(new BasicNameValuePair("nid", str));
            arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
            String a2 = a.a(context);
            arrayList.add(new BasicNameValuePair("token", a2));
            a(arrayList, context, a2);
            arrayList.add(new BasicNameValuePair("v_t", i + ""));
            arrayList.add(new BasicNameValuePair("page", i2 + ""));
            arrayList.add(new BasicNameValuePair("paper", i3 + ""));
            arrayList.add(new BasicNameValuePair("mo", "news"));
            arrayList.add(new BasicNameValuePair("ro", "news"));
            arrayList.add(new BasicNameValuePair("ra", "detail"));
            arrayList.add(new BasicNameValuePair("star", Config.CHANNEL_ID));
            arrayList.add(new BasicNameValuePair("supp", str3));
            a.a(arrayList);
            URI createURI = URIUtils.createURI("http", "jk.kandian.360.cn", -1, "index.php", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.h.bi.a(java.util.List<org.apache.http.NameValuePair>, android.content.Context, java.lang.String, boolean):void
     arg types: [java.util.ArrayList, android.content.Context, java.lang.String, int]
     candidates:
      com.qihoo360.daily.h.bi.a(android.content.Context, java.lang.String, java.lang.String, int):java.net.URI
      com.qihoo360.daily.h.bi.a(java.util.List<org.apache.http.NameValuePair>, android.content.Context, java.lang.String, boolean):void */
    public static URI a(Context context, String str, String str2, int i, String str3, String str4) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("c_n", String.valueOf(i)));
        String a2 = a.a(context);
        arrayList.add(new BasicNameValuePair("token", a2));
        arrayList.add(new BasicNameValuePair("c_pos", str));
        arrayList.add(new BasicNameValuePair("c_c", str2));
        arrayList.add(new BasicNameValuePair("city", str3));
        arrayList.add(new BasicNameValuePair("prov", str4));
        arrayList.add(new BasicNameValuePair("s_code", b.b(a2 + "_4!e@a#2$6^b&9*6_")));
        arrayList.add(new BasicNameValuePair("fr", "kandian"));
        arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
        a((List<NameValuePair>) arrayList, context, a2, true);
        a.a(arrayList);
        try {
            URI createURI = URIUtils.createURI("http", "recommend.kandian.360.cn", -1, "youlike", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.h.bi.a(java.util.List<org.apache.http.NameValuePair>, android.content.Context, java.lang.String, boolean):void
     arg types: [java.util.ArrayList, android.content.Context, java.lang.String, int]
     candidates:
      com.qihoo360.daily.h.bi.a(android.content.Context, java.lang.String, java.lang.String, int):java.net.URI
      com.qihoo360.daily.h.bi.a(java.util.List<org.apache.http.NameValuePair>, android.content.Context, java.lang.String, boolean):void */
    public static URI a(Context context, String str, String str2, long j, long j2, int i, String str3) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("n", String.valueOf(i)));
            arrayList.add(new BasicNameValuePair("fr", "kandian"));
            arrayList.add(new BasicNameValuePair("option", str3));
            if (TextUtils.isEmpty(str) || ChannelType.TYPE_CHANNEL_RECOMMEND.equals(str)) {
                arrayList.add(new BasicNameValuePair("hb", Config.CHANNEL_ID));
            } else {
                arrayList.add(new BasicNameValuePair("c", str));
            }
            if (!TextUtils.isEmpty(str2)) {
                arrayList.add(new BasicNameValuePair("down", Config.CHANNEL_ID));
                arrayList.add(new BasicNameValuePair("t_from", String.valueOf(j / 1000)));
                arrayList.add(new BasicNameValuePair("t_to", String.valueOf(j2 / 1000)));
            }
            String a2 = a.a(context);
            arrayList.add(new BasicNameValuePair("token", a2));
            arrayList.add(new BasicNameValuePair("s_code", b.b(a2 + "_4!e@a#2$6^b&9*6_")));
            a((List<NameValuePair>) arrayList, context, a2, true);
            arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
            a.a(arrayList);
            URI createURI = URIUtils.createURI("http", "recommend.kandian.360.cn", -1, "youlike", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI a(Context context, String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("c", "index"));
            arrayList.add(new BasicNameValuePair("a", "index"));
            arrayList.add(new BasicNameValuePair("m", str));
            arrayList.add(new BasicNameValuePair("id", str2));
            arrayList.add(new BasicNameValuePair("newtrans", str6));
            String a2 = a.a(context);
            arrayList.add(new BasicNameValuePair("token", a2));
            if (!TextUtils.isEmpty(str7)) {
                arrayList.add(new BasicNameValuePair("pushId", str7));
            }
            a(arrayList, context, a2);
            arrayList.add(new BasicNameValuePair(SearchActivity.TAG_URL, str3));
            if (!TextUtils.isEmpty(str4)) {
                arrayList.add(new BasicNameValuePair("cutStr", str4));
            }
            arrayList.add(new BasicNameValuePair("fmt", "json"));
            arrayList.add(new BasicNameValuePair("src", "kandian"));
            arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
            a.a(arrayList);
            URI createURI = URIUtils.createURI("http", "zm.kandian.360.cn", -1, "index.php", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI a(Context context, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("p", "n"));
            if (!TextUtils.isEmpty(str5)) {
                arrayList.add(new BasicNameValuePair("u", str5));
            }
            String a2 = a.a(context);
            arrayList.add(new BasicNameValuePair("f", a2.substring(0, 12)));
            arrayList.add(new BasicNameValuePair("a", a2.substring(12, 24)));
            arrayList.add(new BasicNameValuePair("k", a2.substring(24, a2.length())));
            arrayList.add(new BasicNameValuePair("nid", str3));
            arrayList.add(new BasicNameValuePair("link", str));
            arrayList.add(new BasicNameValuePair("nt", str4));
            arrayList.add(new BasicNameValuePair("c_c", str2));
            arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
            arrayList.add(new BasicNameValuePair("s_code", b.b(a2 + "_4!e@a#2$6^b&9*6_")));
            arrayList.add(new BasicNameValuePair("ran", ((int) (100.0d * Math.random())) + ""));
            arrayList.add(new BasicNameValuePair("client", "0"));
            arrayList.add(new BasicNameValuePair("c", a.j(context)));
            if (!TextUtils.isEmpty(str6)) {
                arrayList.add(new BasicNameValuePair("title", str6));
            }
            if (!TextUtils.isEmpty(str7)) {
                arrayList.add(new BasicNameValuePair("type", str7));
            }
            if (!TextUtils.isEmpty(str8)) {
                arrayList.add(new BasicNameValuePair("clicktype", str8));
            }
            return URIUtils.createURI("http", "s.360.cn/so", -1, "click.gif", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI a(String str) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair(WXConfig.WX_CODE, str));
            arrayList.add(new BasicNameValuePair(WXConfig.WX_APPID, WXConfig.WX_APP_ID));
            arrayList.add(new BasicNameValuePair(WXConfig.WX_SECRET, WXConfig.WX_APP_SECRET));
            arrayList.add(new BasicNameValuePair(WXConfig.WX_GRAND, WXConfig.WX_GRANT_AUTH_CODE));
            URI createURI = URIUtils.createURI("https", WXConfig.WX_API_URL, -1, WXConfig.WX_API_ACCESS_TOKEN, URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI a(String str, Context context) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("mo", "news"));
            arrayList.add(new BasicNameValuePair("ra", "getmusic"));
            arrayList.add(new BasicNameValuePair("songid", str));
            arrayList.add(new BasicNameValuePair("s_code", b.b(str + "_4!e@a#2$6^b&9*6_")));
            arrayList.add(new BasicNameValuePair("token", a.a(context)));
            arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
            a.a(arrayList);
            return URIUtils.createURI("http", "jk.kandian.360.cn", -1, "index.php", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI a(String str, String str2) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("uid", str));
            arrayList.add(new BasicNameValuePair(WXConfig.WX_ACCCESS_TOKEN, str2));
            return URIUtils.createURI("https", "api.weibo.com/2", -1, "users/show.json", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI a(String str, String str2, Context context) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("mo", "news"));
            arrayList.add(new BasicNameValuePair("ra", "hottopic"));
            arrayList.add(new BasicNameValuePair("new_pos", str));
            arrayList.add(new BasicNameValuePair("old_pos", str2));
            arrayList.add(new BasicNameValuePair("new", Config.CHANNEL_ID));
            String a2 = a.a(context);
            arrayList.add(new BasicNameValuePair("token", a2));
            if (str2 == null) {
                arrayList.add(new BasicNameValuePair("n", "15"));
            } else {
                arrayList.add(new BasicNameValuePair("n", "10"));
            }
            arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
            a(arrayList, context, a2);
            a.a(arrayList);
            URI createURI = URIUtils.createURI("http", "jk.kandian.360.cn", -1, "index.php", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI a(List<NameValuePair> list) {
        try {
            a.a(list);
            URI createURI = URIUtils.createURI("http", "jk.kandian.360.cn", -1, "index.php", URLEncodedUtils.format(list, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void a(URI uri) {
        if (uri != null) {
            try {
                ad.a(uri.toURL().toString(), "HTTP");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.h.bi.a(java.util.List<org.apache.http.NameValuePair>, android.content.Context, java.lang.String, boolean):void
     arg types: [java.util.List<org.apache.http.NameValuePair>, android.content.Context, java.lang.String, int]
     candidates:
      com.qihoo360.daily.h.bi.a(android.content.Context, java.lang.String, java.lang.String, int):java.net.URI
      com.qihoo360.daily.h.bi.a(java.util.List<org.apache.http.NameValuePair>, android.content.Context, java.lang.String, boolean):void */
    private static void a(List<NameValuePair> list, Context context, String str) {
        a(list, context, str, false);
    }

    private static void a(List<NameValuePair> list, Context context, String str, boolean z) {
        String str2;
        b a2 = com.qihoo360.daily.i.a.a(context);
        if (a2 == null || TextUtils.isEmpty(a2.b())) {
            WXToken tokenInfo4WX = WXInfoKeeper.getTokenInfo4WX(context);
            if (tokenInfo4WX == null || TextUtils.isEmpty(tokenInfo4WX.getOpenid())) {
                str2 = "0";
            } else {
                str2 = "6";
                str = tokenInfo4WX.getOpenid();
            }
        } else {
            str2 = Config.CHANNEL_ID;
            str = a2.b();
        }
        list.add(new BasicNameValuePair("uid", str));
        list.add(new BasicNameValuePair("tp", str2));
        if (z) {
            list.add(new BasicNameValuePair("u", str));
        }
    }

    public static URI b(Context context) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("mo", "news"));
            arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
            arrayList.add(new BasicNameValuePair("token", a.a(context)));
            a.a(arrayList);
            URI createURI = URIUtils.createURI("http", "jk.kandian.360.cn", -1, "logback", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI b(Context context, String str) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("encodein", "utf-8"));
            arrayList.add(new BasicNameValuePair("encodeout", "utf-8"));
            arrayList.add(new BasicNameValuePair("word", str));
            arrayList.add(new BasicNameValuePair("token", a.a(context)));
            a.a(arrayList);
            URI createURI = URIUtils.createURI("http", "sug.so.360.cn/suggest", -1, "word", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI b(Context context, String str, String str2) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("key", str));
            arrayList.add(new BasicNameValuePair("attitude", "good"));
            arrayList.add(new BasicNameValuePair("comment_id", str2));
            arrayList.add(new BasicNameValuePair("src", "kandian_android"));
            arrayList.add(new BasicNameValuePair("verify", b.a("3DDDC1DC6A27A2EF649325B334109296", str, "good", str2)));
            arrayList.add(new BasicNameValuePair("token", a.a(context)));
            arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
            a.a(arrayList);
            URI createURI = URIUtils.createURI("http", "comment.kandian.360.cn", -1, "punch", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            ad.b(createURI.toURL().toString());
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI b(String str) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair(WXConfig.WX_APPID, WXConfig.WX_APP_ID));
            arrayList.add(new BasicNameValuePair(WXConfig.WX_GRAND, "refresh_token"));
            arrayList.add(new BasicNameValuePair("refresh_token", str));
            URI createURI = URIUtils.createURI("https", WXConfig.WX_API_URL, -1, WXConfig.WX_API_REFRESH_TOKEN, URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI b(String str, String str2) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair(WXConfig.WX_ACCCESS_TOKEN, str));
            arrayList.add(new BasicNameValuePair(WXConfig.WX_OPENID, str2));
            URI createURI = URIUtils.createURI("https", WXConfig.WX_API_URL, -1, WXConfig.WX_API_USERINFO, URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI c(Context context) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("token", a.a(context)));
            arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
            arrayList.add(new BasicNameValuePair("src", "kandian_android"));
            a.a(arrayList);
            URI createURI = URIUtils.createURI("http", "comment.kandian.360.cn", -1, "comment", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI c(Context context, String str) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("mo", "news"));
            arrayList.add(new BasicNameValuePair("ro", "collect"));
            arrayList.add(new BasicNameValuePair("ra", "mycollection"));
            arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
            arrayList.add(new BasicNameValuePair("pos", str));
            String a2 = a.a(context);
            arrayList.add(new BasicNameValuePair("token", a2));
            a(arrayList, context, a2);
            a.a(arrayList);
            URI createURI = URIUtils.createURI("http", "jk.kandian.360.cn", -1, "index.php", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI c(Context context, String str, String str2) {
        URI uri;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mo", "news"));
        arrayList.add(new BasicNameValuePair("ro", "support"));
        arrayList.add(new BasicNameValuePair("ra", "addmood"));
        arrayList.add(new BasicNameValuePair("typ", Config.CHANNEL_ID));
        arrayList.add(new BasicNameValuePair("mood", str2));
        arrayList.add(new BasicNameValuePair(SearchActivity.TAG_URL, str));
        arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
        a.a(arrayList);
        try {
            uri = URIUtils.createURI("http", "jk.kandian.360.cn", -1, "index.php", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            uri = null;
        }
        a(uri);
        return uri;
    }

    public static URI d(Context context) {
        try {
            ArrayList arrayList = new ArrayList();
            String a2 = a.a(context);
            arrayList.add(new BasicNameValuePair("token", a2));
            arrayList.add(new BasicNameValuePair("s_code", b.b(a2 + "_4!e@a#2$6^b&9*6_")));
            arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
            arrayList.add(new BasicNameValuePair("v", String.valueOf(a.c(context))));
            arrayList.add(new BasicNameValuePair("sv", Build.VERSION.RELEASE));
            int d = a.d(context);
            arrayList.add(new BasicNameValuePair("s", d + "*" + a.e(context)));
            arrayList.add(new BasicNameValuePair("t", Build.MODEL));
            arrayList.add(new BasicNameValuePair("isFirst", String.valueOf(a.i(context))));
            arrayList.add(new BasicNameValuePair("c", a.j(context)));
            arrayList.add(new BasicNameValuePair("net_c", a.f(context)));
            arrayList.add(new BasicNameValuePair("net_t", a.h(context)));
            arrayList.add(new BasicNameValuePair("isEmulator ", "" + EmulatorCheck.isEmulator(context)));
            a(arrayList, context, a2);
            a.a(arrayList);
            URI createURI = URIUtils.createURI("http", "sh.qihoo.com/api/newsreader", -1, "kandian_version.json", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI d(Context context, String str) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("p", "n"));
            arrayList.add(new BasicNameValuePair("u", "f270057e413525f2"));
            String a2 = a.a(context);
            arrayList.add(new BasicNameValuePair("f", a2.substring(0, 12)));
            arrayList.add(new BasicNameValuePair("a", a2.substring(12, 24)));
            arrayList.add(new BasicNameValuePair("k", a2.substring(24, a2.length())));
            arrayList.add(new BasicNameValuePair("duration", str));
            arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
            arrayList.add(new BasicNameValuePair("s_code", b.b(a2 + str + "_4!e@a#2$6^b&9*6_")));
            arrayList.add(new BasicNameValuePair("ran", ((int) (100.0d * Math.random())) + ""));
            URI createURI = URIUtils.createURI("http", "s.360.cn/so", -1, "click.gif", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI d(Context context, String str, String str2) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("c", "index"));
            arrayList.add(new BasicNameValuePair("a", "index"));
            arrayList.add(new BasicNameValuePair("m", str));
            arrayList.add(new BasicNameValuePair("id", str2));
            arrayList.add(new BasicNameValuePair("fmt", "json"));
            arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
            arrayList.add(new BasicNameValuePair("src", "kandian"));
            String a2 = a.a(context);
            arrayList.add(new BasicNameValuePair("token", a2));
            a(arrayList, context, a2);
            a.a(arrayList);
            URI createURI = URIUtils.createURI("http", "zm.kandian.360.cn", -1, "index.php", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI e(Context context) {
        URI uri;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mo", "update"));
        arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
        a.a(arrayList);
        try {
            uri = URIUtils.createURI("https", "update.kandian.360.cn", -1, "index.php", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            uri = null;
        }
        a(uri);
        return uri;
    }

    public static URI e(Context context, String str) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("p", "n"));
            arrayList.add(new BasicNameValuePair("u", "cc936dad15831e12"));
            String a2 = a.a(context);
            arrayList.add(new BasicNameValuePair("f", a2.substring(0, 12)));
            arrayList.add(new BasicNameValuePair("a", a2.substring(12, 24)));
            arrayList.add(new BasicNameValuePair("k", a2.substring(24, a2.length())));
            arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
            arrayList.add(new BasicNameValuePair("s_code", b.b(a2 + "_4!e@a#2$6^b&9*6_")));
            arrayList.add(new BasicNameValuePair("ran", ((int) (100.0d * Math.random())) + ""));
            if (!TextUtils.isEmpty(str)) {
                arrayList.add(new BasicNameValuePair("event", str));
            }
            URI createURI = URIUtils.createURI("http", "s.360.cn/so", -1, "click.gif", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI f(Context context) {
        URI uri;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mo", "news"));
        arrayList.add(new BasicNameValuePair("ro", "Start"));
        arrayList.add(new BasicNameValuePair("ra", "getinfo"));
        arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
        a.a(arrayList);
        try {
            uri = URIUtils.createURI("http", "jk.kandian.360.cn", -1, "index.php", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
        } catch (Exception e) {
            e.printStackTrace();
            uri = null;
        }
        a(uri);
        return uri;
    }

    public static URI f(Context context, String str) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("fmt", "json"));
            arrayList.add(new BasicNameValuePair("app", "clockWeather"));
            arrayList.add(new BasicNameValuePair(WXConfig.WX_CODE, str));
            URI createURI = URIUtils.createURI("http", "cdn.weather.hao.360.cn", -1, "sed_api_weather_info.php", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            ad.b(createURI.toURL().toString());
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URI g(Context context, String str) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("mo", "news"));
            arrayList.add(new BasicNameValuePair("ro", "Push"));
            arrayList.add(new BasicNameValuePair("ra", "getPushList"));
            arrayList.add(new BasicNameValuePair("_cv", a.b(context)));
            arrayList.add(new BasicNameValuePair("src", "kandian"));
            arrayList.add(new BasicNameValuePair("pos", str));
            String a2 = a.a(context);
            arrayList.add(new BasicNameValuePair("token", a2));
            a(arrayList, context, a2);
            a.a(arrayList);
            URI createURI = URIUtils.createURI("http", "jk.kandian.360.cn", -1, "index.php", URLEncodedUtils.format(arrayList, Md5Util.DEFAULT_CHARSET), (String) null);
            a(createURI);
            return createURI;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
