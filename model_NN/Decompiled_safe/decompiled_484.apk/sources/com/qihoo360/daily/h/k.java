package com.qihoo360.daily.h;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Process;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import com.qihoo360.daily.activity.CrashReportActivity;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.music.c;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class k implements Thread.UncaughtExceptionHandler {

    /* renamed from: b  reason: collision with root package name */
    private static k f1133b = null;

    /* renamed from: a  reason: collision with root package name */
    private Context f1134a;
    private Thread.UncaughtExceptionHandler c;

    private k() {
    }

    public static k a() {
        if (f1133b == null) {
            f1133b = new k();
        }
        return f1133b;
    }

    private String a(Context context, Throwable th) {
        HashMap hashMap = new HashMap();
        hashMap.put("VERSIONNAME", String.valueOf(a.b(context)));
        hashMap.put("OSVERSION", String.valueOf(Build.VERSION.SDK_INT));
        try {
            for (Field field : Build.class.getDeclaredFields()) {
                field.setAccessible(true);
                hashMap.put(field.getName(), field.get(null).toString());
            }
        } catch (Exception e) {
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (Map.Entry entry : hashMap.entrySet()) {
            stringBuffer.append(((String) entry.getKey()) + "=" + ((String) entry.getValue()) + "\n");
        }
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        for (Throwable cause = th.getCause(); cause != null; cause = cause.getCause()) {
            cause.printStackTrace(printWriter);
        }
        printWriter.close();
        stringBuffer.append(stringWriter.toString());
        return stringBuffer.toString();
    }

    private boolean a(Throwable th) {
        if (th == null) {
            return false;
        }
        if (System.currentTimeMillis() - d.j(this.f1134a) > 5000) {
            String a2 = a(this.f1134a, th);
            Intent intent = new Intent(this.f1134a, CrashReportActivity.class);
            intent.putExtra("crashInfo", a2);
            intent.setFlags(268435456);
            intent.addFlags(AccessibilityEventCompat.TYPE_GESTURE_DETECTION_END);
            intent.addFlags(67108864);
            this.f1134a.startActivity(intent);
            d.a(this.f1134a, System.currentTimeMillis());
        }
        b();
        return true;
    }

    private void b() {
        Process.killProcess(Process.myPid());
        System.exit(10);
    }

    public void a(Context context) {
        this.f1134a = context;
        this.c = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    public void uncaughtException(Thread thread, Throwable th) {
        c.a().b(this.f1134a);
        if (!a(th) && this.c != null) {
            this.c.uncaughtException(thread, th);
        }
    }
}
