package com.qihoo360.daily.h;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Build;
import android.view.View;
import android.widget.Toast;

class o implements View.OnLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f1140a;

    o(l lVar) {
        this.f1140a = lVar;
    }

    public boolean onLongClick(View view) {
        if (Build.VERSION.SDK_INT < 11) {
            return false;
        }
        ((ClipboardManager) view.getContext().getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("json", this.f1140a.c.getText().toString()));
        Toast.makeText(view.getContext(), "json已复制到剪切板", 0).show();
        return true;
    }
}
