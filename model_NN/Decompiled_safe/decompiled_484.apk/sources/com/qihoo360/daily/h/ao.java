package com.qihoo360.daily.h;

import android.view.animation.Animation;
import com.qihoo360.daily.c.d;
import com.qihoo360.daily.c.e;

class ao implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ al f1105a;

    /* renamed from: b  reason: collision with root package name */
    private int f1106b;

    public ao(al alVar, int i) {
        this.f1105a = alVar;
        this.f1106b = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.h.al.a(com.qihoo360.daily.h.al, boolean):boolean
     arg types: [com.qihoo360.daily.h.al, int]
     candidates:
      com.qihoo360.daily.h.al.a(com.qihoo360.daily.h.al, float):float
      com.qihoo360.daily.h.al.a(com.qihoo360.daily.h.al, int):int
      com.qihoo360.daily.h.al.a(com.qihoo360.daily.h.al, boolean):boolean */
    public void onAnimationEnd(Animation animation) {
        if (this.f1106b == 1) {
            boolean unused = this.f1105a.k = true;
            e.a().a(this.f1105a.f1101b.getContext(), this.f1105a.s, new d(this.f1105a.f1101b), 0, false);
            if (this.f1105a.t != null) {
                this.f1105a.t.onShowBigEnd();
            }
        } else if (this.f1106b == 2) {
            boolean unused2 = this.f1105a.k = false;
            if (this.f1105a.t != null) {
                this.f1105a.t.onShowSmallEnd();
            }
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.h.al.a(com.qihoo360.daily.h.al, boolean):boolean
     arg types: [com.qihoo360.daily.h.al, int]
     candidates:
      com.qihoo360.daily.h.al.a(com.qihoo360.daily.h.al, float):float
      com.qihoo360.daily.h.al.a(com.qihoo360.daily.h.al, int):int
      com.qihoo360.daily.h.al.a(com.qihoo360.daily.h.al, boolean):boolean */
    public void onAnimationStart(Animation animation) {
        boolean unused = this.f1105a.k = false;
    }
}
