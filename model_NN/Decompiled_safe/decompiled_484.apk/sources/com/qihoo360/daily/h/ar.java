package com.qihoo360.daily.h;

import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.widget.RelativeLayout;

class ar extends ScaleAnimation {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ al f1109a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ar(al alVar, float f, float f2, float f3, float f4, int i, float f5, int i2, float f6) {
        super(f, f2, f3, f4, i, f5, i2, f6);
        this.f1109a = alVar;
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f, Transformation transformation) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.f1109a.f1101b.getLayoutParams();
        layoutParams.width = ((int) (((float) (this.f1109a.p - this.f1109a.o)) * f)) + this.f1109a.o;
        layoutParams.height = -2;
        this.f1109a.d.requestLayout();
        super.applyTransformation(f, transformation);
    }
}
