package com.qihoo360.daily.h;

import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Build;
import android.view.View;
import android.widget.Toast;
import com.qihoo360.daily.activity.SearchActivity;

class n implements View.OnLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f1139a;

    n(l lVar) {
        this.f1139a = lVar;
    }

    @TargetApi(11)
    public boolean onLongClick(View view) {
        if (Build.VERSION.SDK_INT < 11) {
            return false;
        }
        ((ClipboardManager) view.getContext().getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText(SearchActivity.TAG_URL, this.f1139a.f1136b.getText().toString()));
        Toast.makeText(view.getContext(), "url已复制到剪切板", 0).show();
        return true;
    }
}
