package com.qihoo360.daily.h;

import android.text.TextUtils;
import com.qihoo360.daily.model.Info;
import java.util.ArrayList;
import java.util.List;

public class av {
    public static List<Info> a(List<Info> list) {
        boolean z;
        ArrayList arrayList = new ArrayList();
        boolean z2 = false;
        for (Info next : list) {
            arrayList.add(next);
            if (next.getV_t() == 11) {
                next.setV_t(-6);
            }
            if (next.getExtnews() != null && next.getExtnews().size() > 0) {
                for (int i = 0; i < next.getExtnews().size(); i++) {
                    Info info = next.getExtnews().get(i);
                    info.setC_pos(next.getC_pos());
                    info.setSubNews(true);
                    arrayList.add(info);
                    if (info.getV_t() == 11) {
                        info.setV_t(-7);
                    }
                    if (i == next.getExtnews().size() - 1) {
                        if (!TextUtils.isEmpty(next.getAndroid_channeltarget())) {
                            Info info2 = new Info();
                            info2.setV_t(-5);
                            info2.setAndroid_channeltarget(next.getAndroid_channeltarget());
                            info2.setAndroid_channeltargetname(next.getAndroid_channeltargetname());
                            info2.setC_pos(next.getC_pos());
                            arrayList.add(info2);
                        } else {
                            info.setCardTail(true);
                        }
                    }
                }
                z = false;
            } else if (next.getV_t() != 1 && next.getV_t() != 2 && next.getV_t() != 3) {
                if (!(TextUtils.isEmpty(next.getAndroid_channeltarget()) || next.getV_t() == -1 || next.getV_t() == -2 || next.getV_t() == -4 || next.getV_t() == -5 || next.isSeekMoreAttached())) {
                    next.setSeekMoreAttached(true);
                    Info info3 = new Info();
                    info3.setV_t(-5);
                    info3.setAndroid_channeltarget(next.getAndroid_channeltarget());
                    info3.setAndroid_channeltargetname(next.getAndroid_channeltargetname());
                    info3.setC_pos(next.getC_pos());
                    arrayList.add(info3);
                }
                z = false;
            } else if (!z2) {
                next.recommendCardHeadFlag = true;
                z = true;
            } else {
                z = z2;
            }
            z2 = z;
        }
        return arrayList;
    }
}
