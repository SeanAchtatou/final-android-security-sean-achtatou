package com.qihoo360.daily.h;

import android.content.Context;
import com.qihoo360.daily.g.a;

final class f extends a<Void, Void, Void> {
    final /* synthetic */ Context c;
    final /* synthetic */ String d;

    f(Context context, String str) {
        this.c = context;
        this.d = str;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [java.net.HttpURLConnection, java.lang.Void] */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.net.HttpURLConnection] */
    /* JADX WARN: Type inference failed for: r2v2 */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002b, code lost:
        r0.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002f, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0030, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0036, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0039, code lost:
        r2.disconnect();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0036 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0001] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0039  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Void doInBackground(java.lang.Void... r5) {
        /*
            r4 = this;
            r2 = 0
            android.content.Context r0 = r4.c     // Catch:{ Exception -> 0x000e, all -> 0x0036 }
            java.lang.String r1 = r4.d     // Catch:{ Exception -> 0x000e, all -> 0x0036 }
            com.b.a.a.a(r0, r1)     // Catch:{ Exception -> 0x000e, all -> 0x0036 }
            if (r2 == 0) goto L_0x000d
            r2.disconnect()
        L_0x000d:
            return r2
        L_0x000e:
            r0 = move-exception
            java.net.URL r0 = new java.net.URL     // Catch:{ IOException -> 0x002f, all -> 0x0036 }
            java.lang.String r1 = r4.d     // Catch:{ IOException -> 0x002f, all -> 0x0036 }
            r0.<init>(r1)     // Catch:{ IOException -> 0x002f, all -> 0x0036 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IOException -> 0x002f, all -> 0x0036 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x002f, all -> 0x0036 }
            if (r0 == 0) goto L_0x0029
            r1 = 0
            r0.setUseCaches(r1)     // Catch:{ IOException -> 0x0044, all -> 0x003d }
            r1 = 1
            r0.setInstanceFollowRedirects(r1)     // Catch:{ IOException -> 0x0044, all -> 0x003d }
            r0.connect()     // Catch:{ IOException -> 0x0044, all -> 0x003d }
        L_0x0029:
            if (r0 == 0) goto L_0x000d
            r0.disconnect()
            goto L_0x000d
        L_0x002f:
            r0 = move-exception
            r1 = r2
        L_0x0031:
            r0.printStackTrace()     // Catch:{ all -> 0x0041 }
            r0 = r1
            goto L_0x0029
        L_0x0036:
            r0 = move-exception
        L_0x0037:
            if (r2 == 0) goto L_0x003c
            r2.disconnect()
        L_0x003c:
            throw r0
        L_0x003d:
            r1 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x0037
        L_0x0041:
            r0 = move-exception
            r2 = r1
            goto L_0x0037
        L_0x0044:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.h.f.doInBackground(java.lang.Void[]):java.lang.Void");
    }
}
