package com.qihoo360.daily.h;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import com.qihoo360.daily.f.d;
import com.sina.weibo.sdk.d.g;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.Certificate;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public final class a {
    public static float a(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.density;
    }

    public static float a(Context context, float f) {
        return TypedValue.applyDimension(1, f, context.getResources().getDisplayMetrics());
    }

    public static String a(Context context) {
        if (context == null) {
            return null;
        }
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        String string = Settings.System.getString(context.getContentResolver(), "android_id");
        return b.b(deviceId + string + p.a());
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0069 A[SYNTHETIC, Splitter:B:34:0x0069] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0079 A[SYNTHETIC, Splitter:B:43:0x0079] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0086 A[SYNTHETIC, Splitter:B:50:0x0086] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:31:0x0064=Splitter:B:31:0x0064, B:40:0x0074=Splitter:B:40:0x0074} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String a(java.lang.String r8) {
        /*
            r1 = 0
            java.util.jar.JarFile r2 = new java.util.jar.JarFile     // Catch:{ IOException -> 0x0062, CertificateEncodingException -> 0x0072, all -> 0x0082 }
            r2.<init>(r8)     // Catch:{ IOException -> 0x0062, CertificateEncodingException -> 0x0072, all -> 0x0082 }
            java.util.Enumeration r4 = r2.entries()     // Catch:{ IOException -> 0x0093, CertificateEncodingException -> 0x0091 }
        L_0x000a:
            boolean r0 = r4.hasMoreElements()     // Catch:{ IOException -> 0x0093, CertificateEncodingException -> 0x0091 }
            if (r0 == 0) goto L_0x0056
            java.lang.Object r0 = r4.nextElement()     // Catch:{ IOException -> 0x0093, CertificateEncodingException -> 0x0091 }
            java.util.jar.JarEntry r0 = (java.util.jar.JarEntry) r0     // Catch:{ IOException -> 0x0093, CertificateEncodingException -> 0x0091 }
            boolean r3 = r0.isDirectory()     // Catch:{ IOException -> 0x0093, CertificateEncodingException -> 0x0091 }
            if (r3 != 0) goto L_0x000a
            java.lang.String r3 = r0.getName()     // Catch:{ IOException -> 0x0093, CertificateEncodingException -> 0x0091 }
            java.lang.String r5 = "META-INF/"
            boolean r3 = r3.startsWith(r5)     // Catch:{ IOException -> 0x0093, CertificateEncodingException -> 0x0091 }
            if (r3 != 0) goto L_0x000a
            r3 = 8192(0x2000, float:1.14794E-41)
            byte[] r3 = new byte[r3]     // Catch:{ IOException -> 0x0093, CertificateEncodingException -> 0x0091 }
            java.security.cert.Certificate[] r5 = a(r2, r0, r3)     // Catch:{ IOException -> 0x0093, CertificateEncodingException -> 0x0091 }
            if (r5 == 0) goto L_0x000a
            int r6 = r5.length     // Catch:{ IOException -> 0x0093, CertificateEncodingException -> 0x0091 }
            r0 = 0
            r3 = r0
        L_0x0035:
            if (r3 >= r6) goto L_0x000a
            r0 = r5[r3]     // Catch:{ IOException -> 0x0093, CertificateEncodingException -> 0x0091 }
            byte[] r0 = r0.getEncoded()     // Catch:{ IOException -> 0x0093, CertificateEncodingException -> 0x0091 }
            java.lang.String r0 = com.sina.weibo.sdk.d.g.a(r0)     // Catch:{ IOException -> 0x0093, CertificateEncodingException -> 0x0091 }
            boolean r7 = android.text.TextUtils.isEmpty(r0)     // Catch:{ IOException -> 0x0093, CertificateEncodingException -> 0x0091 }
            if (r7 != 0) goto L_0x0052
            if (r2 == 0) goto L_0x004c
            r2.close()     // Catch:{ IOException -> 0x004d }
        L_0x004c:
            return r0
        L_0x004d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004c
        L_0x0052:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x0035
        L_0x0056:
            if (r2 == 0) goto L_0x005b
            r2.close()     // Catch:{ IOException -> 0x005d }
        L_0x005b:
            r0 = r1
            goto L_0x004c
        L_0x005d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x005b
        L_0x0062:
            r0 = move-exception
            r2 = r1
        L_0x0064:
            r0.printStackTrace()     // Catch:{ all -> 0x008f }
            if (r2 == 0) goto L_0x005b
            r2.close()     // Catch:{ IOException -> 0x006d }
            goto L_0x005b
        L_0x006d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x005b
        L_0x0072:
            r0 = move-exception
            r2 = r1
        L_0x0074:
            r0.printStackTrace()     // Catch:{ all -> 0x008f }
            if (r2 == 0) goto L_0x005b
            r2.close()     // Catch:{ IOException -> 0x007d }
            goto L_0x005b
        L_0x007d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x005b
        L_0x0082:
            r0 = move-exception
            r2 = r1
        L_0x0084:
            if (r2 == 0) goto L_0x0089
            r2.close()     // Catch:{ IOException -> 0x008a }
        L_0x0089:
            throw r0
        L_0x008a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0089
        L_0x008f:
            r0 = move-exception
            goto L_0x0084
        L_0x0091:
            r0 = move-exception
            goto L_0x0074
        L_0x0093:
            r0 = move-exception
            goto L_0x0064
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.h.a.a(java.lang.String):java.lang.String");
    }

    public static void a(File file) {
        File[] listFiles;
        if (file == null || !file.exists()) {
            return;
        }
        if (file.isFile()) {
            if (System.currentTimeMillis() - file.lastModified() > 432000000) {
                file.delete();
            }
        } else if (file.isDirectory() && (listFiles = file.listFiles()) != null) {
            for (File a2 : listFiles) {
                a(a2);
            }
        }
    }

    public static boolean a(Context context, String str) {
        String n = n(context);
        String a2 = a(str);
        if (n == null || a2 == null) {
            return false;
        }
        return n.equals(a2);
    }

    private static Certificate[] a(JarFile jarFile, JarEntry jarEntry, byte[] bArr) {
        try {
            InputStream inputStream = jarFile.getInputStream(jarEntry);
            do {
            } while (inputStream.read(bArr, 0, bArr.length) != -1);
            inputStream.close();
            if (jarEntry != null) {
                return jarEntry.getCertificates();
            }
            return null;
        } catch (IOException e) {
            System.err.println("Exception reading " + jarEntry.getName() + " in " + jarFile.getName() + ": " + e);
            return null;
        }
    }

    public static String b(Context context) {
        if (context == null) {
            return "";
        }
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void b(Activity activity) {
        InputMethodManager inputMethodManager;
        if (activity != null && (inputMethodManager = (InputMethodManager) activity.getSystemService("input_method")) != null && activity.getCurrentFocus() != null) {
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static boolean b(Context context, String str) {
        if (context == null) {
            return true;
        }
        String packageName = context.getPackageName();
        String c = c(context, str);
        if (packageName != null) {
            return packageName.equals(c);
        }
        return true;
    }

    public static int c(Context context) {
        if (context == null) {
            return -1;
        }
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    private static String c(Context context, String str) {
        try {
            return context.getPackageManager().getPackageArchiveInfo(str, 1).packageName;
        } catch (Exception e) {
            return null;
        }
    }

    public static int d(Context context) {
        if (context == null) {
            return -1;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static int e(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public static String f(Context context) {
        String subscriberId = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        int i = -1;
        if (!TextUtils.isEmpty(subscriberId)) {
            if (subscriberId.startsWith("46000") || subscriberId.startsWith("46002")) {
                i = 1;
            } else if (subscriberId.startsWith("46001")) {
                i = 2;
            } else if (subscriberId.startsWith("46003")) {
                i = 3;
            }
        }
        return String.valueOf(i);
    }

    public static boolean g(Context context) {
        NetworkInfo activeNetworkInfo;
        if (context == null || (activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo()) == null) {
            return false;
        }
        return activeNetworkInfo.isAvailable();
    }

    public static String h(Context context) {
        int i;
        NetworkInfo.State state = null;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        NetworkInfo.State state2 = connectivityManager.getNetworkInfo(1) == null ? null : connectivityManager.getNetworkInfo(1).getState();
        if (connectivityManager.getNetworkInfo(0) != null) {
            state = connectivityManager.getNetworkInfo(0).getState();
        }
        if (!(state2 == null || state == null)) {
            if (NetworkInfo.State.CONNECTED != state2 && NetworkInfo.State.CONNECTED == state) {
                int subtype = connectivityManager.getNetworkInfo(0).getSubtype();
                if (subtype == 4 || subtype == 2 || subtype == 11 || subtype == 7 || subtype == 1) {
                    i = 2;
                    return "" + i;
                }
                i = subtype == 13 ? 4 : 3;
                return "" + i;
            } else if (NetworkInfo.State.CONNECTED == state2 && NetworkInfo.State.CONNECTED != state) {
                i = -1;
                return "" + i;
            }
        }
        i = 0;
        return "" + i;
    }

    public static int i(Context context) {
        return d.b(context, "IS_FIRST", 1);
    }

    public static String j(Context context) {
        String a2 = d.a(context, "UMENG_CHANNEL");
        if (TextUtils.isEmpty(a2)) {
            a2 = m(context);
            if (TextUtils.isEmpty(a2)) {
                try {
                    a2 = String.valueOf(context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.get("UMENG_CHANNEL"));
                    d.a(context, "UMENG_CHANNEL", a2);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (TextUtils.isEmpty(a2)) {
                    a2 = "";
                }
                ad.a("CHANNEL_VALUE:" + a2);
            }
        }
        return a2;
    }

    public static void k(Context context) {
        d.b(context, c(context));
    }

    public static int l(Context context) {
        try {
            Class<?> cls = Class.forName("com.android.internal.R$dimen");
            return context.getResources().getDimensionPixelSize(Integer.parseInt(cls.getField("status_bar_height").get(cls.newInstance()).toString()));
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0057 A[SYNTHETIC, Splitter:B:24:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0066 A[SYNTHETIC, Splitter:B:32:0x0066] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x006f A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0076  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String m(android.content.Context r5) {
        /*
            android.content.pm.ApplicationInfo r0 = r5.getApplicationInfo()
            java.lang.String r0 = r0.sourceDir
            java.lang.String r1 = ""
            r3 = 0
            java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x0050, all -> 0x0062 }
            java.util.zip.ZipFile r2 = new java.util.zip.ZipFile     // Catch:{ IOException -> 0x0050, all -> 0x0062 }
            r2.<init>(r0)     // Catch:{ IOException -> 0x0050, all -> 0x0062 }
            java.util.Enumeration r3 = r2.entries()     // Catch:{ IOException -> 0x0074 }
        L_0x0015:
            boolean r0 = r3.hasMoreElements()     // Catch:{ IOException -> 0x0074 }
            if (r0 == 0) goto L_0x0078
            java.lang.Object r0 = r3.nextElement()     // Catch:{ IOException -> 0x0074 }
            java.util.zip.ZipEntry r0 = (java.util.zip.ZipEntry) r0     // Catch:{ IOException -> 0x0074 }
            boolean r4 = r0.isDirectory()     // Catch:{ IOException -> 0x0074 }
            if (r4 != 0) goto L_0x0015
            java.lang.String r0 = r0.getName()     // Catch:{ IOException -> 0x0074 }
            java.lang.String r4 = "kdchannel_"
            boolean r4 = r0.contains(r4)     // Catch:{ IOException -> 0x0074 }
            if (r4 == 0) goto L_0x0015
        L_0x0033:
            if (r2 == 0) goto L_0x0038
            r2.close()     // Catch:{ IOException -> 0x004b }
        L_0x0038:
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 != 0) goto L_0x006f
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)
        L_0x004a:
            return r0
        L_0x004b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0038
        L_0x0050:
            r0 = move-exception
            r2 = r3
        L_0x0052:
            r0.printStackTrace()     // Catch:{ all -> 0x0072 }
            if (r2 == 0) goto L_0x0076
            r2.close()     // Catch:{ IOException -> 0x005c }
            r0 = r1
            goto L_0x0038
        L_0x005c:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r1
            goto L_0x0038
        L_0x0062:
            r0 = move-exception
            r2 = r3
        L_0x0064:
            if (r2 == 0) goto L_0x0069
            r2.close()     // Catch:{ IOException -> 0x006a }
        L_0x0069:
            throw r0
        L_0x006a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0069
        L_0x006f:
            java.lang.String r0 = ""
            goto L_0x004a
        L_0x0072:
            r0 = move-exception
            goto L_0x0064
        L_0x0074:
            r0 = move-exception
            goto L_0x0052
        L_0x0076:
            r0 = r1
            goto L_0x0038
        L_0x0078:
            r0 = r1
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.h.a.m(android.content.Context):java.lang.String");
    }

    private static String n(Context context) {
        try {
            String a2 = g.a(context.getPackageManager().getPackageInfo(context.getPackageName(), 64).signatures[0].toByteArray());
            ad.a("getPackageSign signMD5:" + a2);
            return a2;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
