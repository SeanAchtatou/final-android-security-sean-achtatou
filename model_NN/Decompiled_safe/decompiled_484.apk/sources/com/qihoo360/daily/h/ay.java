package com.qihoo360.daily.h;

import android.content.Context;
import android.widget.Toast;

public class ay {

    /* renamed from: a  reason: collision with root package name */
    private Context f1112a;

    private ay(Context context) {
        this.f1112a = context;
    }

    public static ay a(Context context) {
        return new ay(context);
    }

    public void a(int i) {
        if (this.f1112a != null) {
            a(this.f1112a.getString(i));
        }
    }

    public void a(String str) {
        this.f1112a = this.f1112a.getApplicationContext();
        Toast.makeText(this.f1112a, str, 0).show();
    }
}
