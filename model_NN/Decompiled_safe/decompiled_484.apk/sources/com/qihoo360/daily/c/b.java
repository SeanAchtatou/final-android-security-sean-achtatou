package com.qihoo360.daily.c;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import com.qihoo360.daily.R;

public class b extends BitmapDrawable {

    /* renamed from: a  reason: collision with root package name */
    private String f939a;

    /* renamed from: b  reason: collision with root package name */
    private Paint f940b = new Paint();

    public b(Resources resources, Bitmap bitmap, String str) {
        super(resources, bitmap);
        this.f939a = str;
        int dimensionPixelSize = resources.getDimensionPixelSize(R.dimen.draw_text_size);
        this.f940b.setColor(-1);
        this.f940b.setTextSize((float) dimensionPixelSize);
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        Paint.FontMetrics fontMetrics = this.f940b.getFontMetrics();
        float measureText = this.f940b.measureText(this.f939a);
        float f = fontMetrics.bottom - fontMetrics.top;
        int intrinsicHeight = getIntrinsicHeight();
        int intrinsicWidth = getIntrinsicWidth();
        Rect bounds = getBounds();
        canvas.drawText(this.f939a, ((((float) intrinsicWidth) - measureText) / 2.0f) + ((float) bounds.left), ((((float) intrinsicHeight) - ((((float) intrinsicHeight) - f) / 2.0f)) - fontMetrics.bottom) + ((float) bounds.top), this.f940b);
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }
}
