package com.qihoo360.daily.c;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import com.qihoo360.daily.h.b;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class m {
    public static Bitmap a(String str, int i) {
        if (TextUtils.isEmpty(str) || !d(str) || !b.a()) {
            return null;
        }
        String c = c(str);
        return BitmapFactory.decodeFile(c, o.a(c, i));
    }

    public static InputStream a(String str) {
        if (str == null || !d(str) || !b.a()) {
            return null;
        }
        try {
            return new FileInputStream(c(str));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String a() {
        return b.b() + ".images" + File.separator;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0054 A[SYNTHETIC, Splitter:B:24:0x0054] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0067 A[SYNTHETIC, Splitter:B:31:0x0067] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r4, byte[] r5) {
        /*
            r1 = 0
            boolean r0 = com.qihoo360.daily.h.b.a()
            if (r0 == 0) goto L_0x005a
            if (r5 == 0) goto L_0x005a
            java.lang.String r0 = a()
            java.io.File r2 = new java.io.File
            r2.<init>(r0)
            boolean r3 = r2.exists()
            if (r3 != 0) goto L_0x001b
            r2.mkdirs()
        L_0x001b:
            java.lang.String r2 = b(r4)
            java.io.File r3 = new java.io.File
            r3.<init>(r0, r2)
            java.lang.String r0 = r3.getAbsolutePath()
            boolean r2 = r3.exists()     // Catch:{ Exception -> 0x004a, all -> 0x0064 }
            if (r2 != 0) goto L_0x0031
            r3.createNewFile()     // Catch:{ Exception -> 0x004a, all -> 0x0064 }
        L_0x0031:
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x004a, all -> 0x0064 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x004a, all -> 0x0064 }
            r2.write(r5)     // Catch:{ Exception -> 0x0079 }
            if (r2 == 0) goto L_0x0041
            r2.flush()     // Catch:{ Exception -> 0x0042 }
            r2.close()     // Catch:{ Exception -> 0x0042 }
        L_0x0041:
            return r0
        L_0x0042:
            r1 = move-exception
            r3.delete()
            r1.printStackTrace()
            goto L_0x0041
        L_0x004a:
            r0 = move-exception
            r2 = r1
        L_0x004c:
            r3.delete()     // Catch:{ all -> 0x0076 }
            r0.printStackTrace()     // Catch:{ all -> 0x0076 }
            if (r2 == 0) goto L_0x005a
            r2.flush()     // Catch:{ Exception -> 0x005c }
            r2.close()     // Catch:{ Exception -> 0x005c }
        L_0x005a:
            r0 = r1
            goto L_0x0041
        L_0x005c:
            r0 = move-exception
            r3.delete()
            r0.printStackTrace()
            goto L_0x005a
        L_0x0064:
            r0 = move-exception
        L_0x0065:
            if (r1 == 0) goto L_0x006d
            r1.flush()     // Catch:{ Exception -> 0x006e }
            r1.close()     // Catch:{ Exception -> 0x006e }
        L_0x006d:
            throw r0
        L_0x006e:
            r1 = move-exception
            r3.delete()
            r1.printStackTrace()
            goto L_0x006d
        L_0x0076:
            r0 = move-exception
            r1 = r2
            goto L_0x0065
        L_0x0079:
            r0 = move-exception
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.c.m.a(java.lang.String, byte[]):java.lang.String");
    }

    public static String b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        String b2 = b.b(str);
        String str2 = ".jpg";
        if (str.contains(".gif")) {
            str2 = ".gif";
        } else if (str.contains(".webp")) {
            str2 = ".webp";
        } else if (str.contains(".png")) {
            str2 = ".png";
        }
        return b2 + str2;
    }

    public static String c(String str) {
        return a() + b(str);
    }

    public static boolean d(String str) {
        if (str != null && b.a()) {
            return new File(c(str)).exists();
        }
        return false;
    }

    public static String e(String str) {
        if (!TextUtils.isEmpty(str) && b.a() && d(str)) {
            return new File(c(str)).getAbsolutePath();
        }
        return null;
    }

    public static File f(String str) {
        File file;
        if (!TextUtils.isEmpty(str) && b.a() && (file = new File(c(str))) != null && file.exists()) {
            return file;
        }
        return null;
    }
}
