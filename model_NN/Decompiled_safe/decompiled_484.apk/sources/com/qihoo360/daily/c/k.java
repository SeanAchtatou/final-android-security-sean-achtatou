package com.qihoo360.daily.c;

import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.ImageView;

public abstract class k implements l {
    private ImageView view;

    public k(ImageView imageView) {
        this.view = imageView;
    }

    public ImageView getView() {
        return this.view;
    }

    public int getViewID() {
        return this.view != null ? this.view.hashCode() : hashCode();
    }

    public Object getViewTag() {
        if (this.view != null) {
            return this.view.getTag();
        }
        return null;
    }

    public abstract void imageLoaded(Drawable drawable, String str, boolean z);

    public void setViewTag(String str) {
        if (this.view != null && !TextUtils.isEmpty(str)) {
            this.view.setTag(str);
        }
    }
}
