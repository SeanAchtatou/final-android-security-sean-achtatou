package com.qihoo360.daily.c;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Message;

class g extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f946a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f947b;
    final /* synthetic */ int c;
    final /* synthetic */ l d;
    final /* synthetic */ boolean e;
    final /* synthetic */ e f;

    g(e eVar, Context context, String str, int i, l lVar, boolean z) {
        this.f = eVar;
        this.f946a = context;
        this.f947b = str;
        this.c = i;
        this.d = lVar;
        this.e = z;
    }

    public void handleMessage(Message message) {
        BitmapDrawable bitmapDrawable;
        Bitmap bitmap = (Bitmap) message.obj;
        if (bitmap != null) {
            bitmapDrawable = this.f.a(this.f946a, bitmap);
            this.f.a(this.f947b + this.c, bitmapDrawable);
        } else {
            bitmapDrawable = null;
        }
        if (bitmapDrawable == null || bitmapDrawable.getBitmap() == null || bitmapDrawable.getBitmap().isRecycled()) {
            bitmapDrawable = null;
        }
        if (this.d == null) {
            return;
        }
        if (!this.e) {
            this.d.imageLoaded(bitmapDrawable, this.f947b, true);
        } else if (bitmapDrawable != null) {
            this.d.imageLoaded(new q(bitmapDrawable.getBitmap()), this.f947b, true);
        } else {
            this.d.imageLoaded(bitmapDrawable, this.f947b, false);
        }
    }
}
