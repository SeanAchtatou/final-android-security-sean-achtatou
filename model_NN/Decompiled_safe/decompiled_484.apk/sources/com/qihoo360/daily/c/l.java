package com.qihoo360.daily.c;

import android.graphics.drawable.Drawable;

public interface l {
    int getViewID();

    Object getViewTag();

    void imageLoaded(Drawable drawable, String str, boolean z);

    void setViewTag(String str);
}
