package com.qihoo360.daily.c;

import android.graphics.drawable.BitmapDrawable;
import android.support.v4.util.LruCache;
import java.lang.ref.SoftReference;

class f extends LruCache<String, BitmapDrawable> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f945a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    f(e eVar, int i) {
        super(i);
        this.f945a = eVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int sizeOf(String str, BitmapDrawable bitmapDrawable) {
        int a2;
        if (bitmapDrawable == null || (a2 = this.f945a.a(bitmapDrawable) / 1024) == 0) {
            return 1;
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void entryRemoved(boolean z, String str, BitmapDrawable bitmapDrawable, BitmapDrawable bitmapDrawable2) {
        if (bitmapDrawable != null) {
            if (p.class.isInstance(bitmapDrawable)) {
                ((p) bitmapDrawable).b(false);
            } else {
                new SoftReference(bitmapDrawable);
            }
        }
    }
}
