package com.qihoo360.daily.c;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v4.util.LruCache;
import android.text.TextUtils;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.h.bj;

public class e {

    /* renamed from: a  reason: collision with root package name */
    public static int f943a = 600;
    private static e e = new e();

    /* renamed from: b  reason: collision with root package name */
    final int f944b = ((int) (Runtime.getRuntime().maxMemory() / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID));
    final int c = Math.min(this.f944b / 8, 16384);
    /* access modifiers changed from: private */
    public boolean d;
    private LruCache<String, BitmapDrawable> f = new f(this, this.c);

    private e() {
    }

    /* access modifiers changed from: private */
    @TargetApi(12)
    public int a(BitmapDrawable bitmapDrawable) {
        Bitmap bitmap;
        if (bitmapDrawable == null || (bitmap = bitmapDrawable.getBitmap()) == null) {
            return 1;
        }
        return bj.c() ? bitmap.getByteCount() : bitmap.getRowBytes() * bitmap.getHeight();
    }

    /* access modifiers changed from: private */
    public BitmapDrawable a(Context context, Bitmap bitmap) {
        return bj.a() ? new BitmapDrawable(context.getResources(), bitmap) : new p(context.getResources(), bitmap);
    }

    public static e a() {
        return e;
    }

    /* access modifiers changed from: private */
    public void a(String str, BitmapDrawable bitmapDrawable) {
        if (str != null && bitmapDrawable != null && bitmapDrawable.getBitmap() != null && !bitmapDrawable.getBitmap().isRecycled()) {
            if (p.class.isInstance(bitmapDrawable)) {
                ((p) bitmapDrawable).b(true);
            }
            this.f.put(str, bitmapDrawable);
        }
    }

    /* access modifiers changed from: private */
    public String b(String str) {
        try {
            byte[] b2 = b.b(str);
            if (b2 == null) {
                return null;
            }
            return m.a(str, b2);
        } catch (Exception e2) {
            a().b();
            e2.printStackTrace();
            return null;
        }
    }

    public BitmapDrawable a(String str) {
        return this.f.get(str);
    }

    public void a(Context context, String str, l lVar) {
        if (lVar != null) {
            lVar.setViewTag(str);
        }
        new Thread(new j(this, str, lVar, new i(this, lVar, str))).start();
    }

    public void a(Context context, String str, l lVar, int i) {
        a(context, str, lVar, i, false);
    }

    public void a(Context context, String str, l lVar, int i, boolean z) {
        if (TextUtils.isEmpty(str)) {
            lVar.imageLoaded(null, str, false);
            return;
        }
        if (lVar != null) {
            lVar.setViewTag(str);
        }
        BitmapDrawable a2 = a(str + i);
        if (a2 == null || a2.getBitmap() == null || a2.getBitmap().isRecycled()) {
            new Thread(new h(this, str, lVar, new g(this, context, str, i, lVar, z), i)).start();
        } else if (!z) {
            lVar.imageLoaded(a2, str, false);
        } else {
            lVar.imageLoaded(new q(a2.getBitmap()), str, false);
        }
    }

    public void b() {
        if (this.f != null) {
            this.f.evictAll();
        }
    }
}
