package com.qihoo360.daily.c;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

public class p extends BitmapDrawable {

    /* renamed from: a  reason: collision with root package name */
    private int f954a = 0;

    /* renamed from: b  reason: collision with root package name */
    private int f955b = 0;
    private boolean c;

    public p(Resources resources, Bitmap bitmap) {
        super(resources, bitmap);
    }

    private synchronized void a() {
        if (this.f954a <= 0 && this.f955b <= 0 && this.c && b()) {
            getBitmap().recycle();
        }
    }

    private synchronized boolean b() {
        Bitmap bitmap;
        bitmap = getBitmap();
        return bitmap != null && !bitmap.isRecycled();
    }

    public void a(boolean z) {
        synchronized (this) {
            if (z) {
                this.f955b++;
                this.c = true;
            } else {
                this.f955b--;
            }
        }
        a();
    }

    public void b(boolean z) {
        synchronized (this) {
            if (z) {
                this.f954a++;
            } else {
                this.f954a--;
            }
        }
        a();
    }
}
