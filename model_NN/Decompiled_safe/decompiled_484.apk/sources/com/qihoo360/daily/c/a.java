package com.qihoo360.daily.c;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

public class a extends BitmapDrawable {

    /* renamed from: a  reason: collision with root package name */
    private Drawable f938a;

    public a(Resources resources, Bitmap bitmap, Drawable drawable) {
        super(resources, bitmap);
        this.f938a = drawable;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.f938a != null) {
            int intrinsicWidth = getIntrinsicWidth();
            int intrinsicHeight = this.f938a.getIntrinsicHeight();
            this.f938a.setBounds(intrinsicWidth - this.f938a.getIntrinsicWidth(), 0, intrinsicWidth, intrinsicHeight);
            this.f938a.draw(canvas);
        }
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }
}
