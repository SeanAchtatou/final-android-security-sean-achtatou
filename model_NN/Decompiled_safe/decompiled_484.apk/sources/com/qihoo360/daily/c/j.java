package com.qihoo360.daily.c;

import android.os.Handler;
import java.io.File;
import java.io.IOException;
import pl.droidsonroids.gif.c;

class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f952a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ l f953b;
    final /* synthetic */ Handler c;
    final /* synthetic */ e d;

    j(e eVar, String str, l lVar, Handler handler) {
        this.d = eVar;
        this.f952a = str;
        this.f953b = lVar;
        this.c = handler;
    }

    public void run() {
        if (this.d.d) {
            try {
                Thread.sleep((long) e.f943a);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (!this.f952a.equals(this.f953b.getViewTag())) {
            this.c.sendMessage(this.c.obtainMessage(0, null));
            return;
        }
        File f = m.f(this.f952a);
        if (f != null && f.exists()) {
            try {
                this.c.sendMessage(this.c.obtainMessage(0, new c(f)));
                return;
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        try {
            this.c.sendMessage(this.c.obtainMessage(0, new c(new File(this.d.b(this.f952a)))));
        } catch (Exception e3) {
            e3.printStackTrace();
            this.c.sendMessage(this.c.obtainMessage(0, null));
        }
    }
}
