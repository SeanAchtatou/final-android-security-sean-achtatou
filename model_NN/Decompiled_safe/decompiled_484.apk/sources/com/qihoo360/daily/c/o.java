package com.qihoo360.daily.c;

import android.graphics.BitmapFactory;

public class o {
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0057 A[LOOP:0: B:19:0x0050->B:21:0x0057, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.graphics.BitmapFactory.Options a(android.graphics.BitmapFactory.Options r7, int r8) {
        /*
            r6 = 2
            r2 = 1
            r0 = 0
            r7.inJustDecodeBounds = r0
            int r3 = r7.outWidth
            int r4 = r7.outHeight
            r0 = 2073600(0x1fa400, float:2.905732E-39)
            if (r8 != r2) goto L_0x0011
            r0 = 102400(0x19000, float:1.43493E-40)
        L_0x0011:
            if (r8 != r6) goto L_0x0063
            r0 = 10000(0x2710, float:1.4013E-41)
            r1 = r0
        L_0x0016:
            com.qihoo360.daily.activity.Application r0 = com.qihoo360.daily.activity.Application.getInstance()
            if (r0 == 0) goto L_0x0061
            java.lang.String r5 = "window"
            java.lang.Object r0 = r0.getSystemService(r5)
            android.view.WindowManager r0 = (android.view.WindowManager) r0
            android.util.DisplayMetrics r5 = new android.util.DisplayMetrics
            r5.<init>()
            android.view.Display r0 = r0.getDefaultDisplay()
            r0.getMetrics(r5)
            int r0 = r5.widthPixels
            int r5 = r5.heightPixels
            if (r0 == 0) goto L_0x003c
            if (r5 == 0) goto L_0x003c
            int r1 = r0 * r5
            int r1 = r1 * 2
        L_0x003c:
            if (r8 != r2) goto L_0x0046
            if (r0 == 0) goto L_0x0046
            if (r5 == 0) goto L_0x0046
            int r1 = r0 * r5
            int r1 = r1 / 4
        L_0x0046:
            if (r8 != r6) goto L_0x0061
            if (r0 == 0) goto L_0x0061
            if (r5 == 0) goto L_0x0061
            int r0 = r0 * r5
            int r1 = r0 / 32
            r0 = r2
        L_0x0050:
            int r2 = r3 * r4
            int r5 = r0 * r0
            int r2 = r2 / r5
            if (r2 <= r1) goto L_0x005a
            int r0 = r0 * 2
            goto L_0x0050
        L_0x005a:
            r7.inSampleSize = r0
            android.graphics.Bitmap$Config r0 = android.graphics.Bitmap.Config.RGB_565
            r7.inPreferredConfig = r0
            return r7
        L_0x0061:
            r0 = r2
            goto L_0x0050
        L_0x0063:
            r1 = r0
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.c.o.a(android.graphics.BitmapFactory$Options, int):android.graphics.BitmapFactory$Options");
    }

    public static BitmapFactory.Options a(String str, int i) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(str, options);
        return a(options, i);
    }

    public static BitmapFactory.Options a(byte[] bArr, int i) {
        if (bArr == null) {
            return null;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
        return a(options, i);
    }
}
