package com.qihoo360.daily.c;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.text.TextUtils;
import android.widget.ImageView;

public class d implements l {

    /* renamed from: a  reason: collision with root package name */
    private ImageView f941a;

    /* renamed from: b  reason: collision with root package name */
    private int f942b = -1;

    public d(ImageView imageView) {
        this.f941a = imageView;
    }

    public int getViewID() {
        return this.f941a != null ? this.f941a.hashCode() : hashCode();
    }

    public Object getViewTag() {
        if (this.f941a != null) {
            return this.f941a.getTag();
        }
        return null;
    }

    public void imageLoaded(Drawable drawable, String str, boolean z) {
        if (this.f941a != null && str != null && str.equals(this.f941a.getTag())) {
            if (drawable != null) {
                this.f941a.setScaleType(ImageView.ScaleType.CENTER_CROP);
                if (z) {
                    TransitionDrawable transitionDrawable = new TransitionDrawable(new Drawable[]{new ColorDrawable(17170445), drawable});
                    this.f941a.setImageDrawable(transitionDrawable);
                    transitionDrawable.startTransition(250);
                    return;
                }
                this.f941a.setImageDrawable(drawable);
            } else if (this.f942b != -1) {
                this.f941a.setImageResource(this.f942b);
            }
        }
    }

    public void setViewTag(String str) {
        if (this.f941a != null && !TextUtils.isEmpty(str)) {
            this.f941a.setTag(str);
        }
    }
}
