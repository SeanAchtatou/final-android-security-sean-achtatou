package com.qihoo360.daily.c;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;

public class q extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    private final Bitmap f956a;

    /* renamed from: b  reason: collision with root package name */
    private final Paint f957b = new Paint();
    private final RectF c = new RectF();
    private final int d;
    private final int e;

    public q(Bitmap bitmap) {
        this.f956a = bitmap;
        this.f957b.setAntiAlias(true);
        this.f957b.setDither(true);
        this.f957b.setShader(new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
        this.d = this.f956a.getWidth();
        this.e = this.f956a.getHeight();
    }

    public void draw(Canvas canvas) {
        float f = (float) (this.d / 2);
        float f2 = (float) (this.e / 2);
        canvas.drawCircle(f, f2, this.d < this.e ? f : f2, this.f957b);
    }

    public int getIntrinsicHeight() {
        return this.e;
    }

    public int getIntrinsicWidth() {
        return this.d;
    }

    public int getOpacity() {
        return -3;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.c.set(rect);
    }

    public void setAlpha(int i) {
        if (this.f957b.getAlpha() != i) {
            this.f957b.setAlpha(i);
            invalidateSelf();
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f957b.setColorFilter(colorFilter);
    }

    public void setDither(boolean z) {
        this.f957b.setDither(z);
        invalidateSelf();
    }

    public void setFilterBitmap(boolean z) {
        this.f957b.setFilterBitmap(z);
        invalidateSelf();
    }
}
