package com.qihoo360.daily.c;

import android.graphics.Bitmap;
import android.os.Handler;

class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f948a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ l f949b;
    final /* synthetic */ Handler c;
    final /* synthetic */ int d;
    final /* synthetic */ e e;

    h(e eVar, String str, l lVar, Handler handler, int i) {
        this.e = eVar;
        this.f948a = str;
        this.f949b = lVar;
        this.c = handler;
        this.d = i;
    }

    public void run() {
        Bitmap bitmap;
        if (this.e.d) {
            try {
                Thread.sleep((long) e.f943a);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
        if (!this.f948a.equals(this.f949b.getViewTag())) {
            this.c.sendMessage(this.c.obtainMessage(0, null));
            return;
        }
        try {
            bitmap = m.a(this.f948a, this.d);
        } catch (Exception e3) {
            this.e.b();
            e3.printStackTrace();
            bitmap = null;
        }
        if (bitmap != null) {
            this.c.sendMessage(this.c.obtainMessage(0, bitmap));
        } else {
            r.a().a(this.f949b.getViewID(), this.f948a, this.c, this.d);
        }
    }
}
