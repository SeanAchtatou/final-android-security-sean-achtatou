package com.qihoo360.daily.c;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.h.bj;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class r {

    /* renamed from: a  reason: collision with root package name */
    private static r f958a = new r();

    /* renamed from: b  reason: collision with root package name */
    private ExecutorService f959b = Executors.newFixedThreadPool(5);
    @SuppressLint({"UseSparseArrays"})
    private final HashMap<Integer, s> c = new HashMap<>();

    private r() {
    }

    /* access modifiers changed from: private */
    public Bitmap a(String str, int i) {
        Bitmap decodeByteArray;
        try {
            byte[] b2 = b.b(bj.b() ? str.replace(".jpg", ".webp") : str);
            if (b2 == null || (decodeByteArray = BitmapFactory.decodeByteArray(b2, 0, b2.length, o.a(b2, i))) == null) {
                return null;
            }
            m.a(str, b2);
            return decodeByteArray;
        } catch (Exception e) {
            e.a().b();
            e.printStackTrace();
            return null;
        }
    }

    public static r a() {
        return f958a;
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        synchronized (this.c) {
            if (this.c.containsKey(Integer.valueOf(i))) {
                this.c.remove(Integer.valueOf(i));
            }
        }
    }

    private void a(int i, s sVar) {
        synchronized (this.c) {
            this.c.put(Integer.valueOf(i), sVar);
        }
    }

    private boolean b(int i) {
        boolean containsKey;
        synchronized (this.c) {
            containsKey = this.c.containsKey(Integer.valueOf(i));
        }
        return containsKey;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b(int r4, java.lang.String r5, android.os.Handler r6, int r7) {
        /*
            r3 = this;
            java.util.HashMap<java.lang.Integer, com.qihoo360.daily.c.s> r1 = r3.c
            monitor-enter(r1)
            java.util.HashMap<java.lang.Integer, com.qihoo360.daily.c.s> r0 = r3.c     // Catch:{ all -> 0x002b }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x002b }
            boolean r0 = r0.containsKey(r2)     // Catch:{ all -> 0x002b }
            if (r0 == 0) goto L_0x0028
            java.util.HashMap<java.lang.Integer, com.qihoo360.daily.c.s> r0 = r3.c     // Catch:{ all -> 0x002b }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x002b }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x002b }
            com.qihoo360.daily.c.s r0 = (com.qihoo360.daily.c.s) r0     // Catch:{ all -> 0x002b }
            if (r0 == 0) goto L_0x0028
            r0.f961b = r4     // Catch:{ all -> 0x002b }
            r0.f960a = r5     // Catch:{ all -> 0x002b }
            r0.c = r6     // Catch:{ all -> 0x002b }
            r0.d = r7     // Catch:{ all -> 0x002b }
            r0 = 1
            monitor-exit(r1)     // Catch:{ all -> 0x002b }
        L_0x0027:
            return r0
        L_0x0028:
            monitor-exit(r1)     // Catch:{ all -> 0x002b }
            r0 = 0
            goto L_0x0027
        L_0x002b:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x002b }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.c.r.b(int, java.lang.String, android.os.Handler, int):boolean");
    }

    public void a(int i, String str, Handler handler, int i2) {
        if (!b(i) || !b(i, str, handler, i2)) {
            s sVar = new s(this, str, i, handler, i2);
            a(i, sVar);
            this.f959b.execute(sVar);
        }
    }
}
