package com.qihoo360.daily.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.a.a.j;
import com.qihoo.miop.RetrievePushService;
import com.qihoo360.daily.h.u;
import com.qihoo360.daily.model.PushMeta;

public class TPayloadReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Bundle extras;
        if (intent != null && (extras = intent.getExtras()) != null) {
            j jVar = new j();
            try {
                String string = extras.getString("payload");
                u.a("push json: " + string);
                Intent intent2 = new Intent(context, RetrievePushService.class);
                intent2.setAction(RetrievePushService.RETRIEVE_PUSH_SERVICE);
                intent2.putExtra(RetrievePushService.PUSH_META, (PushMeta) jVar.a(string, PushMeta.class));
                context.startService(intent2);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
