package com.qihoo360.daily.receiver;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.qihoo.miop.PushService;
import com.qihoo360.daily.h.bj;
import java.util.Calendar;

public class WakeUpPushReceiver extends BroadcastReceiver {
    @TargetApi(19)
    private void a(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        PendingIntent b2 = b(context);
        alarmManager.cancel(b2);
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        instance.add(12, 3);
        long timeInMillis = instance.getTimeInMillis();
        if (bj.e()) {
            alarmManager.setExact(0, timeInMillis, b2);
        } else {
            alarmManager.set(0, timeInMillis, b2);
        }
    }

    private PendingIntent b(Context context) {
        Intent intent = new Intent(context, WakeUpPushReceiver.class);
        intent.setAction("AlarmManager");
        return PendingIntent.getBroadcast(context, 99, intent, 134217728);
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Intent intent2 = new Intent(context, PushService.class);
        intent2.setAction(action);
        context.startService(intent2);
        a(context);
    }
}
