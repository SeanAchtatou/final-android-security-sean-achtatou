package com.qihoo360.daily.wxapi;

public final class WXConfig {
    public static final String WX_ACCCESS_TOKEN = "access_token";
    public static final String WX_API_ACCESS_TOKEN = "oauth2/access_token";
    public static final String WX_API_AUTH = "auth";
    public static final String WX_API_REFRESH_TOKEN = "oauth2/refresh_token";
    public static final String WX_API_URL = "api.weixin.qq.com/sns";
    public static final String WX_API_USERINFO = "userinfo";
    public static final String WX_APPID = "appid";
    public static final String WX_APP_ID = "wx768117d6ac23353d";
    public static final String WX_APP_SCOPE = "snsapi_userinfo";
    public static final String WX_APP_SECRET = "7dee9130cc2f80fa887e272eb14aab49";
    public static final String WX_CODE = "code";
    public static final String WX_GRAND = "grant_type";
    public static final String WX_GRANT_AUTH_CODE = "authorization_code";
    public static final String WX_GRANT_REFRESH_TOKEN = "refresh_token";
    public static final String WX_OPENID = "openid";
    public static final String WX_REFRESH_TOKEN = "refresh_token";
    public static final String WX_SECRET = "secret";

    private WXConfig() {
        throw new AssertionError();
    }
}
