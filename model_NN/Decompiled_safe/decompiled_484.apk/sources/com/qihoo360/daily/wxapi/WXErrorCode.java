package com.qihoo360.daily.wxapi;

public class WXErrorCode {
    public static final int EXPIRED_ACCESS_TOKEN = 42001;
    public static final int EXPIRED_CODE = 42003;
    public static final int EXPIRED_REFRESH_TOKEN = 42002;
    public static final int INVALID_ACCESS_TOKEN = 40014;
    public static final int INVALID_CREDENTIAL = 40001;
    public static final int INVALID_OPENID = 40003;
    public static final int INVALID_REFRESH_TOKEN = 40030;
}
