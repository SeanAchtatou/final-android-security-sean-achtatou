package com.qihoo360.daily.wxapi;

import android.os.Parcel;
import android.os.Parcelable;

public class WXResult implements Parcelable {
    public static final Parcelable.Creator<WXResult> CREATOR = new Parcelable.Creator<WXResult>() {
        public WXResult createFromParcel(Parcel parcel) {
            return new WXResult(parcel);
        }

        public WXResult[] newArray(int i) {
            return new WXResult[i];
        }
    };
    private int errcode;
    private String errmsg;

    public WXResult() {
    }

    public WXResult(Parcel parcel) {
        this.errcode = parcel.readInt();
        this.errmsg = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public int getErrcode() {
        return this.errcode;
    }

    public String getErrmsg() {
        return this.errmsg;
    }

    public void setErrcode(int i) {
        this.errcode = i;
    }

    public void setErrmsg(String str) {
        this.errmsg = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.errcode);
        parcel.writeString(this.errmsg);
    }
}
