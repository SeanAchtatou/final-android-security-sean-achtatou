package com.qihoo360.daily.wxapi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.qihoo.dynamic.DotCount;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.g.ai;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.h.b;
import com.tencent.mm.sdk.d.a;
import com.tencent.mm.sdk.modelmsg.g;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

public class WXEntryActivity extends Activity implements IWXAPIEventHandler {
    private IWXAPI api = null;
    private c<Void, WXUser> mOnGetWeixinInfoListener = new c<Void, WXUser>() {
        public void onNetRequest(int i, WXUser wXUser) {
            if (wXUser != null) {
                ay.a(Application.getInstance()).a((int) R.string.navigation_drawer_login_success);
                b.b(Application.getInstance(), "Login_with_weixin");
                if (WXHelper.INSTANCE.getOnGetWeixinInfoCb() != null) {
                    WXHelper.INSTANCE.getOnGetWeixinInfoCb().OnWeixinLoginSuccess(wXUser);
                }
            } else {
                ay.a(Application.getInstance()).a("failed:" + wXUser);
                if (WXHelper.INSTANCE.getOnGetWeixinInfoCb() != null) {
                    WXHelper.INSTANCE.getOnGetWeixinInfoCb().OnWeixinLoginError(wXUser);
                }
            }
            WXEntryActivity.this.finish();
        }
    };

    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.api = WXAPIFactory.createWXAPI(this, WXConfig.WX_APP_ID, true);
        this.api.registerApp(WXConfig.WX_APP_ID);
        try {
            this.api.handleIntent(getIntent(), this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (WXHelper.INSTANCE.getOnGetWeixinInfoCb() != null) {
            WXHelper.INSTANCE.clearWeixinLoginCb();
        }
        if (this.api != null) {
            this.api.unregisterApp();
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        this.api.handleIntent(intent, this);
    }

    public void onReq(a aVar) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, boolean):void
     arg types: [com.qihoo360.daily.activity.Application, java.lang.String, int]
     candidates:
      com.qihoo360.daily.f.d.a(android.content.Context, long, java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, int):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, long):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String[], java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, boolean):void */
    public void onResp(com.tencent.mm.sdk.d.b bVar) {
        int i = R.string.share_cancel;
        switch (bVar.a()) {
            case 1:
                d.a((Context) Application.getInstance(), "login_status", false);
                switch (bVar.f1372a) {
                    case -2:
                        if (WXHelper.INSTANCE.getOnGetWeixinInfoCb() != null) {
                            WXHelper.INSTANCE.getOnGetWeixinInfoCb().OnWeixinLoginCancel();
                        }
                        finish();
                        return;
                    case -1:
                    default:
                        if (WXHelper.INSTANCE.getOnGetWeixinInfoCb() != null) {
                            WXHelper.INSTANCE.getOnGetWeixinInfoCb().OnWeixinLoginException(bVar.f1372a);
                        }
                        finish();
                        return;
                    case 0:
                        new ai(((g) bVar).e).a(this.mOnGetWeixinInfoListener, new Void[0]);
                        return;
                }
            case 2:
                switch (bVar.f1372a) {
                    case DotCount.VXERR_OUTOFMEMORY:
                        i = R.string.share_deny;
                        break;
                    case 0:
                        i = R.string.share_ok;
                        break;
                }
                ay.a(getApplicationContext()).a(i);
                finish();
                return;
            default:
                return;
        }
    }
}
