package com.qihoo360.daily.wxapi;

import android.content.Context;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.h.b;
import com.tencent.mm.sdk.modelmsg.f;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

public enum WXHelper {
    INSTANCE;
    
    private OnGetWeixinInfoCb mOnGetWeixinInfoBack;
    private IWXAPI mWXAPI;

    public interface OnGetWeixinInfoCb {
        void OnWeixinLoginCancel();

        void OnWeixinLoginError(WXUser wXUser);

        void OnWeixinLoginException(int i);

        void OnWeixinLoginSuccess(WXUser wXUser);
    }

    public void clearWeixinLoginCb() {
        this.mOnGetWeixinInfoBack = null;
    }

    public OnGetWeixinInfoCb getOnGetWeixinInfoCb() {
        return this.mOnGetWeixinInfoBack;
    }

    public void init(OnGetWeixinInfoCb onGetWeixinInfoCb) {
        this.mOnGetWeixinInfoBack = onGetWeixinInfoCb;
        this.mWXAPI = WXAPIFactory.createWXAPI(Application.getInstance(), WXConfig.WX_APP_ID, true);
        this.mWXAPI.registerApp(WXConfig.WX_APP_ID);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, boolean):void
     arg types: [com.qihoo360.daily.activity.Application, java.lang.String, int]
     candidates:
      com.qihoo360.daily.f.d.a(android.content.Context, long, java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, int):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, long):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String[], java.lang.String):void
      com.qihoo360.daily.f.d.a(android.content.Context, java.lang.String, boolean):void */
    public void loginWeixin(OnGetWeixinInfoCb onGetWeixinInfoCb) {
        init(onGetWeixinInfoCb);
        if (!b.d(Application.getInstance()) || !this.mWXAPI.isWXAppInstalled()) {
            d.a((Context) Application.getInstance(), "login_status", false);
            if (!this.mWXAPI.isWXAppInstalled()) {
                ay.a(Application.getInstance()).a((int) R.string.login_weixin_uninstalled);
                return;
            }
        }
        f fVar = new f();
        fVar.c = WXConfig.WX_APP_SCOPE;
        fVar.d = "kandian";
        this.mWXAPI.sendReq(fVar);
    }

    public void logoutWeixin(Context context) {
        WXInfoKeeper.clearWeiXinInfo(context);
    }
}
