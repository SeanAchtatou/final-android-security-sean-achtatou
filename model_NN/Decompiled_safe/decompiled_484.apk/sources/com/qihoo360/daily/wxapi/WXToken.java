package com.qihoo360.daily.wxapi;

import android.os.Parcel;
import android.os.Parcelable;

public class WXToken extends WXResult implements Parcelable {
    public static final Parcelable.Creator<WXToken> CREATOR = new Parcelable.Creator<WXToken>() {
        public WXToken createFromParcel(Parcel parcel) {
            return new WXToken(parcel);
        }

        public WXToken[] newArray(int i) {
            return new WXToken[i];
        }
    };
    private String access_token;
    private long expires_in;
    private String openid;
    private String refresh_token;
    private String scope;

    public WXToken() {
    }

    private WXToken(Parcel parcel) {
        super(parcel);
        this.access_token = parcel.readString();
        this.expires_in = parcel.readLong();
        this.refresh_token = parcel.readString();
        this.openid = parcel.readString();
        this.scope = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public String getAccess_token() {
        return this.access_token;
    }

    public long getExpires_in() {
        return this.expires_in;
    }

    public String getOpenid() {
        return this.openid;
    }

    public String getRefresh_token() {
        return this.refresh_token;
    }

    public String getScope() {
        return this.scope;
    }

    public void setAccess_token(String str) {
        this.access_token = str;
    }

    public void setExpires_in(long j) {
        this.expires_in = j;
    }

    public void setOpenid(String str) {
        this.openid = str;
    }

    public void setRefresh_token(String str) {
        this.refresh_token = str;
    }

    public void setScope(String str) {
        this.scope = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.access_token);
        parcel.writeLong(this.expires_in);
        parcel.writeString(this.refresh_token);
        parcel.writeString(this.openid);
        parcel.writeString(this.scope);
    }
}
