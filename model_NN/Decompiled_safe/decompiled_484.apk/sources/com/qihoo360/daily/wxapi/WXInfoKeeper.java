package com.qihoo360.daily.wxapi;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Base64;
import com.a.a.c.a;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.h.bj;

public class WXInfoKeeper {
    private static final String KEY_TOKEN_INFO_WX = "tokenJson4WX";
    private static final String KEY_USER_INFO_WX = "userJson4WX";
    private static final String PREFERENCES_NAME = "weixin_android";

    @SuppressLint({"InlinedApi"})
    public static void clearWeiXinInfo(Context context) {
        SharedPreferences.Editor edit = (bj.a() ? context.getSharedPreferences(PREFERENCES_NAME, 4) : context.getSharedPreferences(PREFERENCES_NAME, 0)).edit();
        edit.remove(KEY_USER_INFO_WX);
        edit.remove(KEY_TOKEN_INFO_WX);
        edit.commit();
    }

    public static WXToken getTokenInfo4WX(Context context) {
        WXToken wXToken;
        if (context != null) {
            try {
                String d = d.d(context, PREFERENCES_NAME, KEY_TOKEN_INFO_WX);
                if (TextUtils.isEmpty(d)) {
                    return null;
                }
                wXToken = (WXToken) Application.getGson().a(new String(Base64.decode(d, 0)), new a<WXToken>() {
                }.getType());
                return wXToken;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        wXToken = null;
        return wXToken;
    }

    public static WXUser getUserInfo4WX(Context context) {
        WXUser wXUser;
        if (context == null) {
            return null;
        }
        try {
            String d = d.d(context, PREFERENCES_NAME, KEY_USER_INFO_WX);
            if (TextUtils.isEmpty(d)) {
                return null;
            }
            wXUser = (WXUser) Application.getGson().a(new String(Base64.decode(d, 0)), new a<WXUser>() {
            }.getType());
            return wXUser;
        } catch (Exception e) {
            e.printStackTrace();
            wXUser = null;
        }
    }

    public static void saveTokenInfo4WX(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            str = Base64.encodeToString(str.getBytes(), 0);
        }
        d.b(context, PREFERENCES_NAME, KEY_TOKEN_INFO_WX, str);
    }

    public static void saveUserInfo4WX(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            str = Base64.encodeToString(str.getBytes(), 0);
        }
        d.b(context, PREFERENCES_NAME, KEY_USER_INFO_WX, str);
    }
}
