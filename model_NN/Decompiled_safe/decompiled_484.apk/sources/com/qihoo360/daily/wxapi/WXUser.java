package com.qihoo360.daily.wxapi;

import android.os.Parcel;
import android.os.Parcelable;

public class WXUser extends WXResult implements Parcelable {
    public static final Parcelable.Creator<WXUser> CREATOR = new Parcelable.Creator<WXUser>() {
        public WXUser createFromParcel(Parcel parcel) {
            return new WXUser(parcel);
        }

        public WXUser[] newArray(int i) {
            return new WXUser[i];
        }
    };
    private String city;
    private String country;
    private String headimgurl;
    private String nickname;
    private String openid;
    private String province;
    private int sex;
    private String unionid;

    public WXUser() {
    }

    private WXUser(Parcel parcel) {
        super(parcel);
        this.openid = parcel.readString();
        this.nickname = parcel.readString();
        this.sex = parcel.readInt();
        this.province = parcel.readString();
        this.city = parcel.readString();
        this.country = parcel.readString();
        this.unionid = parcel.readString();
        this.headimgurl = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public String getCity() {
        return this.city;
    }

    public String getCountry() {
        return this.country;
    }

    public String getHeadimgurl() {
        return this.headimgurl;
    }

    public String getNickname() {
        return this.nickname;
    }

    public String getOpenid() {
        return this.openid;
    }

    public String getProvince() {
        return this.province;
    }

    public int getSex() {
        return this.sex;
    }

    public String getUnionid() {
        return this.unionid;
    }

    public void setCity(String str) {
        this.city = str;
    }

    public void setCountry(String str) {
        this.country = str;
    }

    public void setHeadimgurl(String str) {
        this.headimgurl = str;
    }

    public void setNickname(String str) {
        this.nickname = str;
    }

    public void setOpenid(String str) {
        this.openid = str;
    }

    public void setProvince(String str) {
        this.province = str;
    }

    public void setSex(int i) {
        this.sex = i;
    }

    public void setUnionid(String str) {
        this.unionid = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.openid);
        parcel.writeString(this.nickname);
        parcel.writeInt(this.sex);
        parcel.writeString(this.province);
        parcel.writeString(this.city);
        parcel.writeString(this.country);
        parcel.writeString(this.unionid);
        parcel.writeString(this.headimgurl);
    }
}
