package com.qihoo360.daily.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.activity.FavouriteActivity;
import com.qihoo360.daily.activity.FeedbackActivity;
import com.qihoo360.daily.activity.IndexActivity;
import com.qihoo360.daily.activity.LoginActivity;
import com.qihoo360.daily.activity.PushListActivity;
import com.qihoo360.daily.activity.SettingsActivity;
import com.qihoo360.daily.g.af;
import com.qihoo360.daily.g.ah;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.User;
import com.qihoo360.daily.wxapi.WXInfoKeeper;
import com.qihoo360.daily.wxapi.WXUser;

public class NavigationDrawerFragment extends BaseFragment implements View.OnClickListener {
    private TextView accountName;
    private ImageView avata;
    int c;
    private View layAccount;
    private Activity mActivity;

    private void go2Activity(Class<? extends Activity> cls) {
        FragmentActivity activity = getActivity();
        if (activity != null && (activity instanceof IndexActivity)) {
            ((IndexActivity) activity).go2Activity(cls);
        }
    }

    /* access modifiers changed from: private */
    public void onUserLogout() {
        this.avata.setImageResource(R.drawable.default_avatar);
        this.accountName.setText((int) R.string.login_right_now);
        this.avata.setEnabled(true);
        this.layAccount.setEnabled(true);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = activity;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lay_account:
                startActivityForResult(new Intent(getActivity(), LoginActivity.class), 0);
                return;
            case R.id.account_avata:
            case R.id.tv_account_name:
            case R.id.drawer_list:
            case R.id.navi_drawer_mark:
            case R.id.navi_drawer_push:
            case R.id.navi_drawer_settings:
            case R.id.navi_drawer_feedback:
            default:
                return;
            case R.id.rl_navi_mark:
                go2Activity(FavouriteActivity.class);
                b.b(this.mActivity, "Sidebar_bottom_menu_bookmark");
                return;
            case R.id.rl_navi_push:
                go2Activity(PushListActivity.class);
                b.b(this.mActivity, "Sidebar_bottom_menu_push");
                return;
            case R.id.rl_navi_setting:
                go2Activity(SettingsActivity.class);
                b.b(this.mActivity, "Sidebar_bottom_menu_setting");
                return;
            case R.id.rl_navi_feedback:
                go2Activity(FeedbackActivity.class);
                b.b(this.mActivity, "Sidebar_bottom_menu_feedback");
                return;
            case R.id.ll_bottom_container:
                this.c++;
                if (this.c == 5) {
                    Application.DEBUG_SWITCHER = true;
                    ay.a(getActivity()).a("开启调试");
                    return;
                }
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.fragment_navigation_drawer, viewGroup, false);
        this.avata = (ImageView) inflate.findViewById(R.id.account_avata);
        this.accountName = (TextView) inflate.findViewById(R.id.tv_account_name);
        this.layAccount = inflate.findViewById(R.id.lay_account);
        this.layAccount.setOnClickListener(this);
        inflate.findViewById(R.id.rl_navi_mark).setOnClickListener(this);
        inflate.findViewById(R.id.rl_navi_setting).setOnClickListener(this);
        inflate.findViewById(R.id.rl_navi_push).setOnClickListener(this);
        inflate.findViewById(R.id.rl_navi_feedback).setOnClickListener(this);
        inflate.findViewById(R.id.ll_bottom_container).setOnClickListener(this);
        ((TextView) inflate.findViewById(R.id.navi_drawer_applogo)).setText("V" + a.b(Application.getInstance()));
        return inflate;
    }

    public void onResume() {
        super.onResume();
        af.a(this.mActivity, new ah() {
            public void onGetedUserInfo(User user) {
                if (user == null) {
                    WXUser userInfo4WX = WXInfoKeeper.getUserInfo4WX(Application.getInstance());
                    if (userInfo4WX == null) {
                        NavigationDrawerFragment.this.onUserLogout();
                    } else {
                        NavigationDrawerFragment.this.onUserLogin(userInfo4WX.getHeadimgurl(), userInfo4WX.getNickname());
                    }
                } else {
                    NavigationDrawerFragment.this.onUserLogin(user.getAvatar_large(), user.getName());
                }
            }
        });
    }

    public void onUserLogin(String str, String str2) {
        com.qihoo360.daily.h.af.b(null, this.avata, str, R.dimen.pc_avatar_size, R.dimen.pc_avatar_size, true, R.drawable.default_avatar);
        this.avata.setEnabled(false);
        this.accountName.setText(str2);
        this.layAccount.setEnabled(false);
    }
}
