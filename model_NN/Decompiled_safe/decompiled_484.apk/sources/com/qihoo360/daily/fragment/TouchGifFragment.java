package com.qihoo360.daily.fragment;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.activity.ImagesActivity;
import com.qihoo360.daily.activity.SearchActivity;
import com.qihoo360.daily.c.e;
import com.qihoo360.daily.c.k;
import pl.droidsonroids.gif.GifImageView;

public class TouchGifFragment extends BaseFragment implements View.OnClickListener, View.OnLongClickListener {
    private GifImageView gifView;
    private View layout;
    private Activity mActivity;
    /* access modifiers changed from: private */
    public boolean mIsLoadedFailed = false;
    /* access modifiers changed from: private */
    public View mLoading;
    private View.OnClickListener onClickListener;
    private String url;

    class ImageClallback extends k {
        private GifImageView imageView;

        public ImageClallback(GifImageView gifImageView) {
            super(gifImageView);
            this.imageView = gifImageView;
        }

        public void imageLoaded(Drawable drawable, String str, boolean z) {
            TouchGifFragment.this.mLoading.setVisibility(8);
            if (this.imageView != null && str != null && str.equals(this.imageView.getTag())) {
                if (drawable != null) {
                    this.imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    this.imageView.setImageDrawable(drawable);
                    boolean unused = TouchGifFragment.this.mIsLoadedFailed = false;
                    return;
                }
                this.imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                this.imageView.setImageResource(R.drawable.icon_img_fail);
                boolean unused2 = TouchGifFragment.this.mIsLoadedFailed = true;
            }
        }
    }

    public boolean isImageLoadedFail() {
        return this.mIsLoadedFailed;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = activity;
    }

    public void onClick(View view) {
        if (isImageLoadedFail()) {
            reloadImage();
        } else if (this.mActivity instanceof ImagesActivity) {
            ((ImagesActivity) this.mActivity).onPhotoViewClicked();
        } else {
            this.mActivity.finish();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.url = getArguments().getString(SearchActivity.TAG_URL);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ViewParent parent;
        if (this.layout == null || (parent = this.layout.getParent()) == null || !(parent instanceof ViewGroup)) {
            this.layout = layoutInflater.inflate((int) R.layout.fragment_touch_gif_img, viewGroup, false);
            this.gifView = (GifImageView) this.layout.findViewById(R.id.touch_iv);
            this.mLoading = this.layout.findViewById(R.id.loading_layout);
            this.mLoading.setVisibility(0);
            this.gifView.setOnLongClickListener(this);
            this.gifView.setOnClickListener(this);
            e.a().a(Application.getInstance(), this.url, new ImageClallback(this.gifView));
            return this.layout;
        }
        ((ViewGroup) parent).removeView(this.layout);
        return this.layout;
    }

    public boolean onLongClick(View view) {
        return false;
    }

    public void reloadImage() {
        this.mLoading.setVisibility(0);
        this.gifView.setImageResource(0);
        e.a().a(Application.getInstance(), (String) this.gifView.getTag(), new ImageClallback(this.gifView));
        this.mIsLoadedFailed = false;
    }

    public void setOnClickListener(View.OnClickListener onClickListener2) {
        this.onClickListener = onClickListener2;
    }
}
