package com.qihoo360.daily.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.e.b.al;
import com.e.b.m;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.activity.ImagesActivity;
import com.qihoo360.daily.activity.SearchActivity;
import com.qihoo360.daily.h.aw;
import com.qihoo360.daily.h.ay;
import java.io.IOException;
import java.util.List;
import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.l;

public class TouchImageFragment2 extends BaseFragment implements l {
    private Activity mActivity;
    /* access modifiers changed from: private */
    public String mCurrentUrl;
    /* access modifiers changed from: private */
    public boolean mIsLoadedFailed = false;
    /* access modifiers changed from: private */
    public View mLoadingView;
    /* access modifiers changed from: private */
    public boolean mNeedSaveToDisk = false;
    private View.OnClickListener mOnClickListener;
    /* access modifiers changed from: private */
    public View.OnLongClickListener mOnLongClickListener;
    /* access modifiers changed from: private */
    public PhotoView mPhotoView;
    private View mRootView;
    private List<String> mUrls;

    class PicassoCallback implements m {
        ImageView mImageView;

        public PicassoCallback(ImageView imageView) {
            this.mImageView = imageView;
        }

        public void onError() {
            TouchImageFragment2.this.mPhotoView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            this.mImageView.setImageResource(R.drawable.icon_img_fail);
            TouchImageFragment2.this.mLoadingView.setVisibility(8);
            TouchImageFragment2.this.mPhotoView.setOnLongClickListener(null);
            boolean unused = TouchImageFragment2.this.mIsLoadedFailed = true;
            ay.a(Application.getInstance()).a((int) R.string.error_image_load_failed);
        }

        public void onSuccess() {
            TouchImageFragment2.this.mLoadingView.setVisibility(8);
            boolean unused = TouchImageFragment2.this.mIsLoadedFailed = false;
            TouchImageFragment2.this.mPhotoView.setOnLongClickListener(TouchImageFragment2.this.mOnLongClickListener);
            if (TouchImageFragment2.this.mNeedSaveToDisk) {
                new Thread() {
                    public void run() {
                        Bitmap bitmap;
                        if (!com.qihoo360.daily.c.m.d(TouchImageFragment2.this.mCurrentUrl)) {
                            try {
                                bitmap = al.a((Context) Application.getInstance()).a(TouchImageFragment2.this.mCurrentUrl).c();
                            } catch (IOException e) {
                                e.printStackTrace();
                                bitmap = null;
                            }
                            if (bitmap != null && !bitmap.isRecycled()) {
                                com.qihoo360.daily.c.m.a(TouchImageFragment2.this.mCurrentUrl, aw.a(bitmap, false));
                            }
                        }
                    }
                }.start();
            }
        }
    }

    public boolean isImageLoadedFail() {
        return this.mIsLoadedFailed;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = activity;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        this.mCurrentUrl = arguments.getString(SearchActivity.TAG_URL);
        this.mNeedSaveToDisk = arguments.getBoolean("save2disk");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.mRootView = layoutInflater.inflate((int) R.layout.fragment_touch_img2, viewGroup, false);
        this.mPhotoView = (PhotoView) this.mRootView.findViewById(R.id.touch_iv);
        this.mLoadingView = this.mRootView.findViewById(R.id.loading_layout);
        this.mLoadingView.setVisibility(0);
        this.mPhotoView.setOnViewTapListener(this);
        if (!TextUtils.isEmpty(this.mCurrentUrl)) {
            al.a((Context) Application.getInstance()).a(this.mCurrentUrl).a(this.mPhotoView, new PicassoCallback(this.mPhotoView));
        }
        return this.mRootView;
    }

    public void onDestroy() {
        super.onDestroy();
        al.a((Context) Application.getInstance()).a((ImageView) this.mPhotoView);
    }

    public void onViewTap(View view, float f, float f2) {
        if (isImageLoadedFail()) {
            reloadImage();
        } else if (this.mActivity instanceof ImagesActivity) {
            ((ImagesActivity) this.mActivity).onPhotoViewClicked();
        } else {
            this.mActivity.finish();
        }
    }

    public void reloadImage() {
        this.mLoadingView.setVisibility(0);
        this.mPhotoView.setScaleType(null);
        if (!TextUtils.isEmpty(this.mCurrentUrl)) {
            al.a((Context) Application.getInstance()).a(this.mCurrentUrl).a(this.mPhotoView, new PicassoCallback(this.mPhotoView));
            this.mIsLoadedFailed = false;
        }
    }

    public void resetImage() {
        if (this.mPhotoView != null) {
            this.mPhotoView.setScale(1.0f);
        }
    }

    public void setImageUrls(List<String> list) {
        this.mUrls = list;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.mOnClickListener = onClickListener;
    }

    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.mOnLongClickListener = onLongClickListener;
    }
}
