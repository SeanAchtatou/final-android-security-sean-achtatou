package com.qihoo360.daily.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import com.e.b.al;
import com.qihoo360.daily.R;
import com.qihoo360.daily.a.s;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.activity.IndexActivity;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.g.l;
import com.qihoo360.daily.g.m;
import com.qihoo360.daily.h.ax;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.Result;
import java.util.Iterator;
import java.util.List;

public class DailyFragment extends BaseListFragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    public static final int REQUEST_CODE_GET_LIST = 0;
    public static final int REQUEST_CODE_GET_NUM = -1;
    public static final int REQUEST_CODE_PULL_UP = 1;
    public static final int REQUEST_CODE_STARTUP = -2;
    /* access modifiers changed from: private */
    public boolean bottomSuggestShowed;
    public int fackNewsCount;
    private boolean isFirstOnResume = true;
    /* access modifiers changed from: private */
    public IndexActivity mActivity;
    /* access modifiers changed from: private */
    public s mAdapter;
    private View mFragmentView;
    /* access modifiers changed from: private */
    public boolean mIsRequesting;
    /* access modifiers changed from: private */
    public View mLayoutError;
    /* access modifiers changed from: private */
    public View mLayoutLoading;
    /* access modifiers changed from: private */
    public String mOldPos;
    /* access modifiers changed from: private */
    public SwipeRefreshLayout mPullToRefreshView;
    /* access modifiers changed from: private */
    public RecyclerView mRecyclerView;
    /* access modifiers changed from: private */
    public boolean newsClicked;
    /* access modifiers changed from: private */
    public View none_update_layout;
    c<Result<List<Info>>, Result<List<Info>>> onNetRequestListener = new c<Result<List<Info>>, Result<List<Info>>>() {
        public void onNetRequest(int i, Result<List<Info>> result) {
        }

        public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
            onNetRequest(i, (Result<List<Info>>) ((Result) obj));
        }

        public void onProgressUpdate(int i, Result<List<Info>> result) {
            int i2;
            switch (i) {
                case -2:
                case 0:
                    DailyFragment.this.mPullToRefreshView.setRefreshing(false);
                    if (result == null || result.getStatus() != 0) {
                        if (DailyFragment.this.isAdded() && i == 0) {
                            ay.a(DailyFragment.this.mActivity).a((int) R.string.net_error);
                        }
                        if (DailyFragment.this.mAdapter.a() == null || DailyFragment.this.mAdapter.a().size() == 0) {
                            DailyFragment.this.mLayoutError.setVisibility(0);
                        } else if (DailyFragment.this.mAdapter.f()) {
                            DailyFragment.this.mAdapter.d();
                            DailyFragment.this.mAdapter.notifyDataSetChanged();
                        }
                    } else {
                        List data = result.getData();
                        if (data != null) {
                            if (data.size() > 0) {
                                i2 = DailyFragment.this.getUpdateCount(data, DailyFragment.this.mAdapter.a());
                                if (data.size() > 0) {
                                    String json = Application.toJson(result);
                                    if (DailyFragment.this.mActivity != null && !TextUtils.isEmpty(json)) {
                                        d.a(DailyFragment.this.mActivity.getApplicationContext(), "dailyinfolist", json, "DAILY_NEWS");
                                    }
                                }
                            } else {
                                i2 = 0;
                            }
                            DailyFragment.this.mAdapter.a(data);
                            DailyFragment.this.mAdapter.notifyDataSetChanged();
                            String unused = DailyFragment.this.mOldPos = DailyFragment.this.mAdapter.b();
                            Animation loadAnimation = AnimationUtils.loadAnimation(Application.getInstance(), R.anim.anim_alpha_in_out_delay);
                            if (DailyFragment.this.isAdded() && i == 0) {
                                if (i2 == 0) {
                                    DailyFragment.this.none_update_layout.startAnimation(loadAnimation);
                                } else if (i2 > 0) {
                                    DailyFragment.this.show_bar.setText(DailyFragment.this.getString(R.string.update_num, Integer.valueOf(i2)));
                                    DailyFragment.this.show_bar.startAnimation(loadAnimation);
                                }
                            }
                        }
                        if (DailyFragment.this.mAdapter.a() == null || DailyFragment.this.mAdapter.a().size() <= 0) {
                            DailyFragment.this.mLayoutError.setVisibility(0);
                        } else {
                            DailyFragment.this.mLayoutError.setVisibility(8);
                        }
                    }
                    DailyFragment.this.mLayoutLoading.setVisibility(8);
                    b.b(DailyFragment.this.mActivity, DailyFragment.this.channelAlias + "_refresh");
                    break;
                case 1:
                    DailyFragment.this.mLayoutLoading.setVisibility(8);
                    if (result == null || result.getStatus() != 0) {
                        ay.a(DailyFragment.this.mActivity).a((int) R.string.net_error);
                        DailyFragment.this.mAdapter.d();
                        DailyFragment.this.mAdapter.notifyDataSetChanged();
                    } else {
                        List data2 = result.getData();
                        if (data2 == null || data2.size() <= 0) {
                            DailyFragment.this.mAdapter.c();
                        } else {
                            DailyFragment.this.mAdapter.b(data2);
                            String unused2 = DailyFragment.this.mOldPos = DailyFragment.this.mAdapter.b();
                        }
                        DailyFragment.this.mAdapter.notifyDataSetChanged();
                    }
                    if (DailyFragment.this.mAdapter.a() == null || DailyFragment.this.mAdapter.a().size() == 0) {
                        DailyFragment.this.mLayoutError.setVisibility(0);
                        break;
                    }
                    break;
            }
            boolean unused3 = DailyFragment.this.mIsRequesting = false;
        }

        public /* bridge */ /* synthetic */ void onProgressUpdate(int i, Object obj) {
            onProgressUpdate(i, (Result<List<Info>>) ((Result) obj));
        }
    };
    /* access modifiers changed from: private */
    public TextView show_bar;

    /* access modifiers changed from: private */
    public int getUpdateCount(List<Info> list, List<Info> list2) {
        int size = list != null ? list.size() : 0;
        if (size <= 0 || list2 == null || list2.size() <= 0) {
            return size;
        }
        int size2 = list2.size();
        Iterator<Info> it = list.iterator();
        while (true) {
            int i = size;
            if (!it.hasNext()) {
                return i;
            }
            Info next = it.next();
            String nid = next.getNid();
            int i2 = 0;
            while (true) {
                if (i2 >= size2) {
                    break;
                }
                Info info = list2.get(i2);
                if (ax.a(nid) || nid.equals(info.getNid())) {
                    i--;
                    next.setRead(info.getRead());
                } else {
                    i2++;
                }
            }
            size = i;
        }
    }

    /* access modifiers changed from: private */
    public void hideBottomPop(boolean z) {
        final View findViewById = this.mFragmentView.findViewById(R.id.ll_read_more_suggest);
        if (findViewById.getVisibility() != 8) {
            if (!z) {
                findViewById.setVisibility(8);
                return;
            }
            Animation loadAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.abc_slide_out_bottom);
            findViewById.startAnimation(loadAnimation);
            loadAnimation.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation animation) {
                    findViewById.setVisibility(8);
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                }
            });
        }
    }

    private void initViews() {
        this.show_bar = (TextView) this.mFragmentView.findViewById(R.id.show_bar);
        this.none_update_layout = this.mFragmentView.findViewById(R.id.showbar_layout);
        this.mLayoutError = this.mFragmentView.findViewById(R.id.error_layout);
        this.mLayoutError.setOnClickListener(this);
        this.mLayoutLoading = this.mFragmentView.findViewById(R.id.loading_layout);
        this.mRecyclerView = (RecyclerView) this.mFragmentView.findViewById(R.id.recyclerView);
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(this.mRecyclerView.getContext()) {
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(-1, -2);
            }
        });
        this.mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                al a2 = al.a((Context) Application.getInstance());
                switch (i) {
                    case 0:
                        if (DailyFragment.this.mAdapter.getItemCount() > 0 && DailyFragment.this.mRecyclerView.getChildPosition(DailyFragment.this.mRecyclerView.getChildAt(DailyFragment.this.mRecyclerView.getChildCount() + -1)) >= DailyFragment.this.mAdapter.getItemCount() + -4) {
                            DailyFragment.this.onLoadMoreVisible();
                        }
                        a2.c(DailyFragment.this.channelAlias);
                        break;
                    case 1:
                        a2.c(DailyFragment.this.channelAlias);
                        break;
                    case 2:
                        a2.b((Object) DailyFragment.this.channelAlias);
                        break;
                }
                if (DailyFragment.this.mRecyclerView.getChildPosition(DailyFragment.this.mRecyclerView.getChildAt(DailyFragment.this.mRecyclerView.getChildCount() - 1)) - DailyFragment.this.fackNewsCount >= 20 && !DailyFragment.this.bottomSuggestShowed && !DailyFragment.this.newsClicked && (DailyFragment.this.getActivity() instanceof IndexActivity)) {
                    DailyFragment.this.showBottomPop(((IndexActivity) DailyFragment.this.getActivity()).getNewsSuggestText());
                    boolean unused = DailyFragment.this.bottomSuggestShowed = true;
                }
            }

            public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            }
        });
        this.mAdapter = new s(getActivity());
        this.mAdapter.a(this);
        Result<List<Info>> a2 = m.a(d.c(this.mActivity.getApplicationContext(), "dailyinfolist", "DAILY_NEWS"));
        if (!(a2 == null || a2.getData() == null || a2.getData().isEmpty())) {
            this.mLayoutLoading.setVisibility(8);
            this.mAdapter.a(a2.getData());
        }
        this.mRecyclerView.setAdapter(this.mAdapter);
        this.mPullToRefreshView = (SwipeRefreshLayout) this.mFragmentView.findViewById(R.id.pull_refresh_view);
        this.mPullToRefreshView.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark);
        this.mPullToRefreshView.setOnRefreshListener(this);
    }

    /* access modifiers changed from: private */
    public void onLoadMoreVisible() {
        if (!this.mIsRequesting && !this.mAdapter.h()) {
            loadMore();
        }
    }

    /* access modifiers changed from: private */
    public void showBottomPop(String str) {
        View findViewById = this.mFragmentView.findViewById(R.id.ll_read_more_suggest);
        if (findViewById.getVisibility() != 0) {
            findViewById.setVisibility(0);
            if (!TextUtils.isEmpty(str)) {
                ((TextView) findViewById.findViewById(R.id.tv_suggest_text)).setText(str);
            }
            findViewById.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    ((NavigationFragment) DailyFragment.this.getParentFragment()).onSelected(1);
                    b.b(view.getContext(), "kandian_to_channel_tips_onclick ");
                    DailyFragment.this.hideBottomPop(true);
                }
            });
            findViewById.findViewById(R.id.iv_close).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    DailyFragment.this.hideBottomPop(true);
                }
            });
            findViewById.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.abc_slide_in_bottom));
            findViewById.postDelayed(new Runnable() {
                public void run() {
                    DailyFragment.this.hideBottomPop(DailyFragment.this.isAdded());
                }
            }, 5000);
        }
    }

    public void backToTop(boolean z) {
        if (this.mRecyclerView != null) {
            this.mRecyclerView.post(new Runnable() {
                public void run() {
                    DailyFragment.this.mRecyclerView.scrollToPosition(0);
                }
            });
            if (z) {
                this.mPullToRefreshView.setRefreshing(true);
                this.mFragmentView.postDelayed(new Runnable() {
                    public void run() {
                        DailyFragment.this.refresh(0);
                    }
                }, 200);
            }
        }
    }

    public int getRequestCode() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public void lazyLoad() {
        if (this.mIsVisible && this.mIsPrepared) {
            if (this.mAdapter.a() == null || this.mAdapter.a().size() <= 0) {
                refresh(-2);
            }
        }
    }

    public void loadMore() {
        if (this.mAdapter.g()) {
            this.mAdapter.e();
            this.mAdapter.notifyDataSetChanged();
        }
        if (this.mOldPos == null) {
            this.mOldPos = this.mAdapter.b();
        }
        new m(Application.getInstance(), this.mOldPos).a(this.onNetRequestListener, 1, new String[0]);
        this.mIsRequesting = true;
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        Info info;
        int a2;
        super.onActivityResult(i, i2, intent);
        if (1 == i && intent != null && (info = (Info) intent.getParcelableExtra("Info")) != null && this.mAdapter != null && (a2 = this.mAdapter.a(info)) >= 0 && a2 < this.mAdapter.getItemCount()) {
            this.mAdapter.notifyItemChanged(a2);
        }
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = (IndexActivity) activity;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.error_layout:
                this.mLayoutError.setVisibility(8);
                this.mLayoutLoading.setVisibility(0);
                refresh(0);
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.channelAlias = getArguments().getString(BaseFragment.EXTRA_CHANNEL_ALIAS);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        b.b(this.mActivity, this.channelAlias + "_channel_onClick");
        new l(this.mActivity, this.channelAlias).a(null, new Void[0]);
        if (this.mFragmentView != null) {
            ViewParent parent = this.mFragmentView.getParent();
            if (parent != null && (parent instanceof ViewGroup)) {
                ((ViewGroup) parent).removeView(this.mFragmentView);
            }
            this.mIsPrepared = true;
            return this.mFragmentView;
        }
        this.mFragmentView = layoutInflater.inflate((int) R.layout.fragment_navigation_daily, viewGroup, false);
        initViews();
        this.mIsPrepared = true;
        refresh(-2);
        return this.mFragmentView;
    }

    public void onDestroy() {
        save();
        al.a((Context) Application.getInstance()).a((Object) this.channelAlias);
        super.onDestroy();
    }

    public void onRefresh() {
        refresh(0);
    }

    public void onResume() {
        super.onResume();
        if (!this.isFirstOnResume && b.h("daily")) {
            refresh(0);
        }
        this.isFirstOnResume = false;
    }

    public void refresh(int i) {
        new m(Application.getInstance(), "", this.mAdapter.a()).a(this.onNetRequestListener, i, new String[0]);
        this.mIsRequesting = true;
    }

    public void save() {
        if (this.mAdapter.a() != null) {
            Result result = new Result();
            result.setStatus(0);
            result.setData(this.mAdapter.a());
            String a2 = Application.getGson().a(result);
            if (this.mActivity != null) {
                d.a(this.mActivity.getApplicationContext(), "dailyinfolist", a2, "DAILY_NEWS");
            }
        }
    }

    public void setNewsClicked(boolean z) {
        this.newsClicked = z;
    }

    public void setUserVisibleHint(boolean z) {
        super.setUserVisibleHint(z);
    }
}
