package com.qihoo360.daily.fragment;

import android.support.v4.app.Fragment;
import com.f.a.g;

public class BaseFragment extends Fragment {
    public static final String EXTRA_CHANNEL_ALIAS = "extra_channel_alias";

    public void onPause() {
        super.onPause();
        g.a(getClass().getSimpleName());
    }

    public void onResume() {
        super.onResume();
        g.a(getClass().getSimpleName());
    }

    public void release() {
    }
}
