package com.qihoo360.daily.fragment;

public abstract class BaseListFragment extends LazyLoadFragment {
    protected String channelAlias;

    public abstract void backToTop(boolean z);

    public String getAlias() {
        return this.channelAlias;
    }

    public abstract int getRequestCode();
}
