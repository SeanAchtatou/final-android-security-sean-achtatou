package com.qihoo360.daily.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.qihoo360.daily.R;
import com.qihoo360.daily.a.a;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.activity.IndexActivity;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.Channel;
import com.qihoo360.daily.widget.ActionBar;
import com.qihoo360.daily.widget.LinearLayoutForListView;
import com.qihoo360.daily.widget.dragdropgrid.ChannelManage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class NavigationFragment extends BaseFragment implements View.OnClickListener, ActionBar.OnItemSelectedListener {
    private a actionBarAdapter;
    private LinearLayoutForListView actionBarList;
    private List<Channel> channels = new ArrayList();
    private boolean clearBadge;
    /* access modifiers changed from: private */
    public ActionBar discoveryTitleBar;
    private IndexActivity mActivity;
    private PagerAdapter mAdapter;
    private ChannelSubscribeFragment mChannelSubscribeFragment;
    /* access modifiers changed from: private */
    public int mCurrentSelectedPositon;
    /* access modifiers changed from: private */
    public DailyFragment mDailyFragment;
    private FragmentManager mFragManager;
    private ImageView mIvChannelEdit;
    private View mLayoutChannelManage;
    private View mLayoutSlidingPager;
    private ViewPager mPager;
    private TextView mTvChannelEdit;
    private View mView;

    public class PagerAdapter extends FragmentStatePagerAdapter {
        private SparseArray<Fragment> fragmentArray = new SparseArray<>();
        private HashMap<String, Fragment> fragments = new HashMap<>();
        private List<Channel> mChannels;

        public PagerAdapter(FragmentManager fragmentManager, List<Channel> list) {
            super(fragmentManager);
            this.mChannels = list;
        }

        public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
            super.destroyItem(viewGroup, i, obj);
            BaseListFragment baseListFragment = (BaseListFragment) obj;
            String alias = baseListFragment.getAlias();
            if (alias != null) {
                this.fragments.put(alias, baseListFragment);
            }
        }

        public int getCount() {
            if (this.mChannels != null) {
                return this.mChannels.size();
            }
            return 0;
        }

        public SparseArray<Fragment> getFragments() {
            return this.fragmentArray;
        }

        public Fragment getItem(int i) {
            Fragment remove = this.fragments.remove(this.mChannels.get(i).getAlias());
            if (remove == null || remove.isAdded()) {
                Bundle bundle = new Bundle();
                bundle.putString(BaseFragment.EXTRA_CHANNEL_ALIAS, this.mChannels.get(i).getAlias());
                int type = this.mChannels.get(i).getType();
                if (type == 2) {
                    DailyFragment unused = NavigationFragment.this.mDailyFragment = new DailyFragment();
                    NavigationFragment.this.mDailyFragment.setArguments(bundle);
                    this.fragmentArray.append(i, NavigationFragment.this.mDailyFragment);
                    return NavigationFragment.this.mDailyFragment;
                } else if (type == 1) {
                    SpecialChannelFragment specialChannelFragment = new SpecialChannelFragment();
                    specialChannelFragment.setArguments(bundle);
                    this.fragmentArray.append(i, specialChannelFragment);
                    return specialChannelFragment;
                } else {
                    CommonNewsFragment commonNewsFragment = new CommonNewsFragment();
                    commonNewsFragment.setArguments(bundle);
                    this.fragmentArray.append(i, commonNewsFragment);
                    return commonNewsFragment;
                }
            } else {
                this.fragmentArray.append(i, remove);
                return remove;
            }
        }

        public int getItemPosition(Object obj) {
            return -2;
        }

        public void setChannels(List<Channel> list) {
            this.mChannels = list;
            this.fragments.clear();
            this.fragmentArray.clear();
        }
    }

    private void initChannels() {
        this.channels = ChannelManage.getSubscribedChannels();
        this.actionBarAdapter = new a(getActivity(), this.channels);
        this.actionBarList.setAdapter(this.actionBarAdapter);
        this.discoveryTitleBar.setCurrentPosition(0);
    }

    private void initViews() {
        initChannels();
        this.mFragManager = getChildFragmentManager();
        this.mAdapter = new PagerAdapter(this.mFragManager, this.channels);
        this.mPager.setAdapter(this.mAdapter);
        this.mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int i) {
            }

            public void onPageScrolled(int i, float f, int i2) {
            }

            public void onPageSelected(int i) {
                int unused = NavigationFragment.this.mCurrentSelectedPositon = i;
                NavigationFragment.this.discoveryTitleBar.setCurrentPosition(i);
                NavigationFragment.this.releaseGif(i);
            }
        });
    }

    private void switchActionBar(boolean z) {
        this.mIvChannelEdit.clearAnimation();
        this.mLayoutChannelManage.clearAnimation();
        if (z) {
            this.mLayoutSlidingPager.setVisibility(4);
            this.mLayoutChannelManage.setVisibility(0);
            this.mLayoutChannelManage.startAnimation(AnimationUtils.loadAnimation(Application.getInstance(), R.anim.alpha_in));
            this.mIvChannelEdit.setImageResource(R.drawable.ic_arrow_up);
            this.mIvChannelEdit.startAnimation(AnimationUtils.loadAnimation(Application.getInstance(), R.anim.rotate_180_clockwise));
            return;
        }
        Animation loadAnimation = AnimationUtils.loadAnimation(Application.getInstance(), R.anim.alpha_out);
        this.mLayoutChannelManage.setVisibility(4);
        this.mLayoutSlidingPager.setVisibility(0);
        this.mLayoutChannelManage.startAnimation(loadAnimation);
        this.mIvChannelEdit.setImageResource(R.drawable.ic_arrow_down);
        this.mIvChannelEdit.startAnimation(AnimationUtils.loadAnimation(Application.getInstance(), R.anim.rotate_180_clockwise));
    }

    public void clearBadge() {
        this.actionBarList.getChildAt(1).findViewById(R.id.iv_badge).setVisibility(8);
        this.clearBadge = true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:7:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean exitSubscribeEditMode() {
        /*
            r3 = this;
            r0 = 1
            r1 = 0
            com.qihoo360.daily.fragment.ChannelSubscribeFragment r2 = r3.mChannelSubscribeFragment
            if (r2 == 0) goto L_0x002d
            r3.updateEditButtonStatus(r0)
            com.qihoo360.daily.fragment.ChannelSubscribeFragment r2 = r3.mChannelSubscribeFragment
            boolean r2 = r2.isEditing()
            if (r2 == 0) goto L_0x001e
            com.qihoo360.daily.fragment.ChannelSubscribeFragment r2 = r3.mChannelSubscribeFragment
            r2.setEditing(r1)
        L_0x0016:
            boolean r1 = r3.clearBadge
            if (r1 == 0) goto L_0x001d
            r3.clearBadge()
        L_0x001d:
            return r0
        L_0x001e:
            com.qihoo360.daily.fragment.ChannelSubscribeFragment r0 = r3.mChannelSubscribeFragment
            int r0 = r0.getCurrentIndex()
            r3.updateChannelData(r0)
            r3.switchActionBar(r1)
            r0 = 0
            r3.mChannelSubscribeFragment = r0
        L_0x002d:
            r0 = r1
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.fragment.NavigationFragment.exitSubscribeEditMode():boolean");
    }

    public ChannelSubscribeFragment getChannelSubscribeFragment() {
        return this.mChannelSubscribeFragment;
    }

    public int jumpToTargetChannelInActionbarListIfPossible(String str) {
        int i;
        if (TextUtils.isEmpty(str) || this.actionBarAdapter == null || this.actionBarAdapter.a() == null) {
            return -2;
        }
        int i2 = 0;
        Iterator<Channel> it = this.actionBarAdapter.a().iterator();
        while (true) {
            i = i2;
            if (it.hasNext() && !str.equals(it.next().getAlias())) {
                i2 = i + 1;
            }
        }
        if (i == this.actionBarAdapter.a().size()) {
            return -1;
        }
        this.mCurrentSelectedPositon = i;
        this.mPager.setCurrentItem(this.mCurrentSelectedPositon);
        this.discoveryTitleBar.setPosition(this.mCurrentSelectedPositon);
        return i;
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        List<Fragment> fragments;
        super.onActivityResult(i, i2, intent);
        if (this.mFragManager != null && (fragments = this.mFragManager.getFragments()) != null) {
            int size = fragments.size();
            for (int i3 = 0; i3 < size; i3++) {
                Fragment fragment = fragments.get(i3);
                if (fragment != null) {
                    fragment.onActivityResult(i, i2, intent);
                }
            }
        }
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = (IndexActivity) activity;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_channel_edit:
                if (this.mChannelSubscribeFragment == null) {
                    this.mChannelSubscribeFragment = new ChannelSubscribeFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("index", this.mPager.getCurrentItem());
                    this.mChannelSubscribeFragment.setArguments(bundle);
                    FragmentTransaction beginTransaction = this.mFragManager.beginTransaction();
                    beginTransaction.setCustomAnimations(R.anim.anim_top_in, R.anim.anim_top_out, R.anim.anim_top_in, R.anim.anim_top_out);
                    beginTransaction.replace(R.id.fl_channel_edit_container, this.mChannelSubscribeFragment).addToBackStack("ChannelSubcribeFragment").commitAllowingStateLoss();
                    switchActionBar(true);
                    b.b(this.mActivity, "channel_manage_onclick");
                    return;
                }
                this.mChannelSubscribeFragment.setEditing(false);
                this.mActivity.onBackPressed();
                return;
            case R.id.tv_channel_edit:
                if (this.mChannelSubscribeFragment != null) {
                    updateEditButtonStatus(this.mChannelSubscribeFragment.isEditing());
                    if (this.mChannelSubscribeFragment.isEditing()) {
                        this.mChannelSubscribeFragment.setEditing(false);
                        return;
                    } else {
                        this.mChannelSubscribeFragment.setEditing(true);
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ViewParent parent;
        if (this.mView == null || (parent = this.mView.getParent()) == null || !(parent instanceof ViewGroup)) {
            this.mView = layoutInflater.inflate((int) R.layout.fragment_navigation2, viewGroup, false);
            this.mPager = (ViewPager) this.mView.findViewById(R.id.pager);
            this.discoveryTitleBar = (ActionBar) this.mView.findViewById(R.id.discovery_title_bar);
            this.actionBarList = (LinearLayoutForListView) this.mView.findViewById(R.id.action_bar_list);
            this.discoveryTitleBar.setOnItemSelectedListener(this);
            this.mIvChannelEdit = (ImageView) this.mView.findViewById(R.id.iv_channel_edit);
            this.mIvChannelEdit.setOnClickListener(this);
            this.mTvChannelEdit = (TextView) this.mView.findViewById(R.id.tv_channel_edit);
            this.mTvChannelEdit.setOnClickListener(this);
            this.mLayoutSlidingPager = this.mView.findViewById(R.id.rl_sliding_page);
            this.mLayoutChannelManage = this.mView.findViewById(R.id.ll_channle_manage);
            return this.mView;
        }
        ((ViewGroup) parent).removeView(this.mView);
        return this.mView;
    }

    public void onReClick(int i) {
        scrollToTopAndRefresh(i);
    }

    public void onSelected(int i) {
        if (this.mPager.getCurrentItem() != i) {
            this.mPager.setCurrentItem(i, true);
        }
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        initViews();
    }

    public void refreshDaily() {
        if (this.mDailyFragment != null) {
            this.mDailyFragment.refresh(-2);
        }
    }

    public void releaseGif(int i) {
        SparseArray<Fragment> fragments;
        BaseFragment baseFragment;
        BaseFragment baseFragment2;
        if (this.mAdapter != null && (fragments = this.mAdapter.getFragments()) != null) {
            if (i == -1) {
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 < fragments.size()) {
                        BaseFragment baseFragment3 = (BaseFragment) fragments.get(i3);
                        if (baseFragment3 != null) {
                            baseFragment3.release();
                        }
                        i2 = i3 + 1;
                    } else {
                        return;
                    }
                }
            } else {
                if (i > 0 && (baseFragment2 = (BaseFragment) fragments.get(i - 1)) != null) {
                    baseFragment2.release();
                }
                if (i < this.channels.size() - 1 && (baseFragment = (BaseFragment) fragments.get(i + 1)) != null) {
                    baseFragment.release();
                }
            }
        }
    }

    public void scrollToTopAndRefresh() {
        Fragment fragment = this.mAdapter.getFragments().get(this.mCurrentSelectedPositon);
        if (fragment instanceof BaseListFragment) {
            ((BaseListFragment) fragment).backToTop(true);
        }
    }

    public void scrollToTopAndRefresh(int i) {
        this.mCurrentSelectedPositon = i;
        Fragment fragment = this.mAdapter.getFragments().get(this.mCurrentSelectedPositon);
        if (fragment instanceof BaseListFragment) {
            ((BaseListFragment) fragment).backToTop(true);
        }
    }

    public void updateChannelData(int i) {
        this.channels = ChannelManage.getSubscribedChannels();
        this.mAdapter.setChannels(this.channels);
        this.mAdapter.notifyDataSetChanged();
        this.mCurrentSelectedPositon = i;
        this.mPager.setCurrentItem(i);
        this.actionBarAdapter = new a(this.mActivity, this.channels);
        this.actionBarList.setAdapter(this.actionBarAdapter);
        this.discoveryTitleBar.setPosition(i);
    }

    public void updateEditButtonStatus(boolean z) {
        if (z) {
            this.mTvChannelEdit.setText((int) R.string.channel_edit);
        } else {
            this.mTvChannelEdit.setText((int) R.string.channel_edit_finish);
        }
    }
}
