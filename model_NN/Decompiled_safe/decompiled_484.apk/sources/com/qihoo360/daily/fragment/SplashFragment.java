package com.qihoo360.daily.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qihoo.download.base.AbsDownloadTask;
import com.qihoo.download.impl.so.SoDownloadManager;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.IndexActivity;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.widget.ProgressTextView;
import java.util.List;

public class SplashFragment extends BaseFragment {
    private static final int ANIMATION_GO = 6;
    private static final int ANIMATION_GONE = 7;
    private static final int ANIMATION_PROGRESS_GONE = 3;
    private static final int ANIMATION_PROGRESS_SHOW = 1;
    private static final int ANIMATION_PROGRESS_START = 2;
    private static final int ANIMATION_SEARCH = 4;
    private static final int ANIMATION_TRUN = 5;
    public static boolean isDownloadSo = false;
    /* access modifiers changed from: private */
    public Activity activity;
    /* access modifiers changed from: private */
    public FrameLayout contentLayout;
    private ImageView ivTurn;
    private ProgressTextView progressTextView;
    private RelativeLayout rlSearch;
    private int searchIndex = 0;
    private List<String> searchList;
    private TextView tvDate;
    private TextView tvSearch;
    private TextView tvSearchContent;

    /* access modifiers changed from: private */
    public void goAnimation() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(1200);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                SplashFragment.this.contentLayout.setVisibility(4);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        alphaAnimation.setFillAfter(true);
        this.contentLayout.startAnimation(alphaAnimation);
    }

    private void init() {
        if (b.b(this.activity) && SoDownloadManager.getInstance().needDownloadSo(this.activity)) {
            isDownloadSo = true;
            SoDownloadManager.getInstance().setListener(new SoDownloadManager.IDowloadSoListener() {
                public void onDownloadFailed(AbsDownloadTask absDownloadTask) {
                    SplashFragment.isDownloadSo = false;
                }

                public void onDownloadSizeChanged(AbsDownloadTask absDownloadTask) {
                }

                public void onDownloadSucess(AbsDownloadTask absDownloadTask) {
                    SplashFragment.isDownloadSo = false;
                }
            });
            SoDownloadManager.getInstance().downloadSo(this.activity.getApplicationContext());
        }
    }

    public void goneSelf() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(500);
        if (getView() != null) {
            getView().startAnimation(alphaAnimation);
        }
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                if (SplashFragment.this.activity instanceof IndexActivity) {
                    if (SplashFragment.this.getView() != null) {
                        SplashFragment.this.getView().setVisibility(8);
                    }
                    FragmentTransaction beginTransaction = ((IndexActivity) SplashFragment.this.activity).getSupportFragmentManager().beginTransaction();
                    beginTransaction.remove(SplashFragment.this);
                    beginTransaction.commit();
                }
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
    }

    public void onAttach(Activity activity2) {
        super.onAttach(activity2);
        this.activity = activity2;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        init();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.contentLayout = new FrameLayout(this.activity);
        this.contentLayout.setBackgroundResource(R.drawable.bg_splash);
        return this.contentLayout;
    }

    public void onResume() {
        super.onResume();
        new Handler(new Handler.Callback() {
            public boolean handleMessage(Message message) {
                SplashFragment.this.goAnimation();
                return false;
            }
        }).sendEmptyMessageDelayed(0, 1800);
    }
}
