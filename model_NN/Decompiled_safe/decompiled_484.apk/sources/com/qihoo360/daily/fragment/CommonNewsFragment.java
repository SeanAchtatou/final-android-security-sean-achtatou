package com.qihoo360.daily.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import com.e.b.al;
import com.mediav.ads.sdk.adcore.Config;
import com.qihoo360.daily.R;
import com.qihoo360.daily.a.ac;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.activity.IndexActivity;
import com.qihoo360.daily.b.d;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.e.n;
import com.qihoo360.daily.g.an;
import com.qihoo360.daily.g.ap;
import com.qihoo360.daily.g.l;
import com.qihoo360.daily.h.av;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.Result;
import com.qihoo360.daily.widget.RecyclerViewDivider.DividerItemDecoration;
import java.util.List;

public class CommonNewsFragment extends BaseListFragment implements View.OnClickListener {
    private static final int REQUEST_TAG_LOADMORE = 1;
    private static final int REQUEST_TAG_REFRESH = 0;
    /* access modifiers changed from: private */
    public boolean isRequesting;
    /* access modifiers changed from: private */
    public IndexActivity mActivity;
    /* access modifiers changed from: private */
    public ac mAdapter;
    private View mContentView;
    /* access modifiers changed from: private */
    public View mNetErrorLayout;
    /* access modifiers changed from: private */
    public View mProgressBar;
    /* access modifiers changed from: private */
    public SwipeRefreshLayout mPullToRefreshView;
    /* access modifiers changed from: private */
    public RecyclerView mRecyclerView;
    /* access modifiers changed from: private */
    public TextView mShowBar;
    /* access modifiers changed from: private */
    public Animation mShowBarAnimation;
    private c<Result<List<Info>>, Result<List<Info>>> onNetRequestListener = new c<Result<List<Info>>, Result<List<Info>>>() {
        public void onNetRequest(int i, Result<List<Info>> result) {
        }

        public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
            onNetRequest(i, (Result<List<Info>>) ((Result) obj));
        }

        public void onProgressUpdate(int i, Result<List<Info>> result) {
            int i2;
            int v_t;
            boolean z = CommonNewsFragment.this.mAdapter.a() == null || CommonNewsFragment.this.mAdapter.a().size() == 0;
            CommonNewsFragment.this.mPullToRefreshView.setRefreshing(false);
            CommonNewsFragment.this.mProgressBar.setVisibility(8);
            if (result == null || result.getStatus() != 0) {
                ay.a(CommonNewsFragment.this.mActivity).a((int) R.string.net_error);
                if (CommonNewsFragment.this.mAdapter.a() == null || CommonNewsFragment.this.mAdapter.a().size() == 0) {
                    CommonNewsFragment.this.mNetErrorLayout.setVisibility(0);
                }
                if (CommonNewsFragment.this.mAdapter.f()) {
                    CommonNewsFragment.this.mAdapter.c();
                    CommonNewsFragment.this.mAdapter.notifyDataSetChanged();
                }
            } else {
                List data = result.getData();
                if (data == null || data.size() == 0) {
                    CommonNewsFragment.this.mAdapter.b();
                    i2 = 0;
                } else {
                    int size = data.size();
                    boolean z2 = i != 0;
                    if (z2 && ((v_t = CommonNewsFragment.this.mAdapter.e().getV_t()) == 1 || v_t == 2 || (v_t == 3 && ((Info) data.get(0)).recommendCardHeadFlag))) {
                        ((Info) data.get(0)).recommendCardHeadFlag = false;
                    }
                    CommonNewsFragment.this.mAdapter.a(data, z2, false);
                    i2 = size;
                }
                CommonNewsFragment.this.mAdapter.notifyDataSetChanged();
                CommonNewsFragment.this.mNetErrorLayout.setVisibility(8);
                if (CommonNewsFragment.this.isAdded() && i == 0) {
                    switch (i2) {
                        case 0:
                            ay.a(CommonNewsFragment.this.mActivity).a((int) R.string.no_update);
                            break;
                        default:
                            CommonNewsFragment.this.mShowBar.setText(CommonNewsFragment.this.getString(R.string.update_num, Integer.valueOf(i2)));
                            CommonNewsFragment.this.mShowBar.startAnimation(CommonNewsFragment.this.mShowBarAnimation);
                            break;
                    }
                }
                if (CommonNewsFragment.this.getParentFragment() != null && (CommonNewsFragment.this.getParentFragment() instanceof NavigationFragment) && ChannelType.TYPE_CHANNEL_RECOMMEND.equals(CommonNewsFragment.this.channelAlias) && !z) {
                    ((NavigationFragment) CommonNewsFragment.this.getParentFragment()).clearBadge();
                }
            }
            boolean unused = CommonNewsFragment.this.isRequesting = false;
            b.b(CommonNewsFragment.this.mActivity, CommonNewsFragment.this.channelAlias + "_refresh");
        }

        public /* bridge */ /* synthetic */ void onProgressUpdate(int i, Object obj) {
            onProgressUpdate(i, (Result<List<Info>>) ((Result) obj));
        }
    };

    private void initView() {
        this.mShowBar = (TextView) this.mContentView.findViewById(R.id.show_bar);
        this.mRecyclerView = (RecyclerView) this.mContentView.findViewById(R.id.recyclerView);
        this.mNetErrorLayout = this.mContentView.findViewById(R.id.error_layout);
        this.mProgressBar = this.mContentView.findViewById(R.id.loading_layout);
        this.mNetErrorLayout.setOnClickListener(this);
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(this.mRecyclerView.getContext()) {
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(-1, -2);
            }
        });
        this.mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                al a2 = al.a((Context) Application.getInstance());
                switch (i) {
                    case 0:
                        if ((CommonNewsFragment.this.mAdapter.getItemCount() > 0 && CommonNewsFragment.this.mRecyclerView.getChildPosition(CommonNewsFragment.this.mRecyclerView.getChildAt(CommonNewsFragment.this.mRecyclerView.getChildCount() + -1)) >= CommonNewsFragment.this.mAdapter.getItemCount() + -1) && !CommonNewsFragment.this.mAdapter.g()) {
                            CommonNewsFragment.this.onLoadMoreVisible();
                        }
                        a2.c(CommonNewsFragment.this.channelAlias);
                        return;
                    case 1:
                        a2.c(CommonNewsFragment.this.channelAlias);
                        return;
                    case 2:
                        a2.b((Object) CommonNewsFragment.this.channelAlias);
                        return;
                    default:
                        return;
                }
            }

            public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            }
        });
        this.mAdapter = new ac(getActivity(), null, this.channelAlias, getRequestCode());
        this.mAdapter.a(this);
        this.mRecyclerView.setAdapter(this.mAdapter);
        this.mRecyclerView.addItemDecoration(new DividerItemDecoration(this.mRecyclerView.getContext(), 1));
        this.mPullToRefreshView = (SwipeRefreshLayout) this.mContentView.findViewById(R.id.pull_refresh_view);
        this.mPullToRefreshView.setColorScheme(R.color.colorPrimary, R.color.colorPrimaryDark);
        this.mPullToRefreshView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            public void onRefresh() {
                CommonNewsFragment.this.refresh(1);
            }
        });
        this.mShowBarAnimation = AnimationUtils.loadAnimation(Application.getInstance(), R.anim.anim_alpha_in_out_delay);
    }

    /* access modifiers changed from: private */
    public void onLoadMoreVisible() {
        if (!this.isRequesting && !this.mAdapter.g()) {
            loadMore();
        }
    }

    private void queryData() {
        this.isRequesting = true;
        new Thread(new Runnable() {
            public void run() {
                final List<Info> a2 = new d(CommonNewsFragment.this.mActivity).a(Info.class, "channelId", CommonNewsFragment.this.channelAlias, 0, 20, "timeOrder");
                if (ChannelType.TYPE_CHANNEL_RECOMMEND.equals(CommonNewsFragment.this.channelAlias)) {
                    a2 = av.a(a2);
                }
                CommonNewsFragment.this.mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        boolean unused = CommonNewsFragment.this.isRequesting = false;
                        if (a2 != null && a2.size() > 0) {
                            CommonNewsFragment.this.mAdapter.a(a2, false, false);
                            CommonNewsFragment.this.mAdapter.notifyDataSetChanged();
                            CommonNewsFragment.this.mProgressBar.setVisibility(8);
                            CommonNewsFragment.this.mNetErrorLayout.setVisibility(8);
                        } else if (CommonNewsFragment.this.mIsVisible) {
                            CommonNewsFragment.this.refresh(0);
                        }
                    }
                });
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void refresh(int i) {
        if (!this.isRequesting) {
            if (this.mRecyclerView != null) {
                this.mRecyclerView.getLayoutManager().scrollToPosition(0);
            }
            new ap(this.mActivity.getApplicationContext(), b.a(this.mAdapter, this.mActivity.getApplicationContext(), this.channelAlias), i).a(this.onNetRequestListener, 0, this.channelAlias);
            this.isRequesting = true;
        }
    }

    public void backToTop(boolean z) {
        if (this.mRecyclerView != null) {
            this.mRecyclerView.post(new Runnable() {
                public void run() {
                    CommonNewsFragment.this.mRecyclerView.scrollToPosition(0);
                }
            });
            if (z) {
                this.mPullToRefreshView.setRefreshing(true);
                this.mContentView.postDelayed(new Runnable() {
                    public void run() {
                        CommonNewsFragment.this.refresh(1);
                    }
                }, 200);
            }
        }
    }

    public int getRequestCode() {
        return 5;
    }

    /* access modifiers changed from: protected */
    public void lazyLoad() {
        if (this.mIsVisible && this.mIsPrepared) {
            if (this.mAdapter.a() == null || this.mAdapter.a().size() <= 0) {
                refresh(0);
            }
        }
    }

    public void loadMore() {
        Info e;
        if (this.mAdapter.h()) {
            this.mAdapter.d();
            this.mAdapter.notifyDataSetChanged();
        }
        List<Info> a2 = this.mAdapter.a();
        if (a2 != null && a2.size() > 1 && (e = this.mAdapter.e()) != null) {
            new an(this.mActivity, Config.CHANNEL_ID, e.getTimeOrder()).a(this.onNetRequestListener, 1, this.channelAlias);
            this.isRequesting = true;
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        Info info;
        super.onActivityResult(i, i2, intent);
        if (getRequestCode() == i && intent != null && (info = (Info) intent.getParcelableExtra("Info")) != null && this.mAdapter != null) {
            this.mAdapter.a(info);
            this.mAdapter.notifyDataSetChanged();
        }
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = (IndexActivity) activity;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.error_layout:
                this.mProgressBar.setVisibility(0);
                this.mNetErrorLayout.setVisibility(8);
                refresh(1);
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.channelAlias = getArguments().getString(BaseFragment.EXTRA_CHANNEL_ALIAS);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        b.b(this.mActivity, this.channelAlias + "_channel_onClick");
        new l(this.mActivity, this.channelAlias).a(null, new Void[0]);
        if (this.mContentView != null) {
            ViewParent parent = this.mContentView.getParent();
            if (parent != null && (parent instanceof ViewGroup)) {
                ((ViewGroup) parent).removeView(this.mContentView);
            }
            this.mIsPrepared = true;
            return this.mContentView;
        }
        this.mContentView = layoutInflater.inflate((int) R.layout.fragment_channel, viewGroup, false);
        initView();
        this.mIsPrepared = true;
        queryData();
        return this.mContentView;
    }

    public void onDestroy() {
        al.a((Context) Application.getInstance()).a((Object) this.channelAlias);
        super.onDestroy();
    }

    public void release() {
        super.release();
        if (this.mAdapter != null) {
            for (RecyclerView.ViewHolder next : this.mAdapter.f880a) {
                if (next != null) {
                    n.a(next);
                }
            }
        }
    }

    public void setUserVisibleHint(boolean z) {
        super.setUserVisibleHint(z);
    }
}
