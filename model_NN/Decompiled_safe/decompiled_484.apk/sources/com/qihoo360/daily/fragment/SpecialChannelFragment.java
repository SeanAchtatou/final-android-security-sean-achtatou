package com.qihoo360.daily.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import com.e.b.al;
import com.qihoo360.daily.R;
import com.qihoo360.daily.a.ac;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.activity.IndexActivity;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.e.n;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.g.l;
import com.qihoo360.daily.g.y;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.Result;
import com.qihoo360.daily.widget.RecyclerViewDivider.DividerItemDecoration;
import java.util.ArrayList;
import java.util.List;

public class SpecialChannelFragment extends BaseListFragment implements View.OnClickListener {
    public static final String ACTION_ONLOCATION_CHANGE = "ACTION_ONLOCATION_CHANGE";
    private static final int REQUEST_TAG_LOADMORE = 1;
    private static final int REQUEST_TAG_REFRESH = 0;
    /* access modifiers changed from: private */
    public boolean isRequesting;
    /* access modifiers changed from: private */
    public IndexActivity mActivity;
    /* access modifiers changed from: private */
    public ac mAdapter;
    private View mContentView;
    /* access modifiers changed from: private */
    public View mNetErrorLayout;
    /* access modifiers changed from: private */
    public View mProgressBar;
    /* access modifiers changed from: private */
    public SwipeRefreshLayout mPullToRefreshView;
    /* access modifiers changed from: private */
    public RecyclerView mRecyclerView;
    /* access modifiers changed from: private */
    public TextView mShowBar;
    /* access modifiers changed from: private */
    public Animation mShowBarAnimation;
    private BroadcastReceiver onBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (SpecialChannelFragment.ACTION_ONLOCATION_CHANGE.equals(intent.getAction()) && SpecialChannelFragment.this.isLocalChannel() && SpecialChannelFragment.this.mIsPrepared) {
                SpecialChannelFragment.this.refresh(true);
            }
        }
    };
    private c<Result<List<Info>>, Result<List<Info>>> onNetRequestListener = new c<Result<List<Info>>, Result<List<Info>>>() {
        public void onNetRequest(int i, Result<List<Info>> result) {
        }

        public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
            onNetRequest(i, (Result<List<Info>>) ((Result) obj));
        }

        public void onProgressUpdate(int i, Result<List<Info>> result) {
            int i2;
            SpecialChannelFragment.this.mPullToRefreshView.setRefreshing(false);
            SpecialChannelFragment.this.mProgressBar.setVisibility(8);
            if (result == null || result.getStatus() != 0) {
                ay.a(SpecialChannelFragment.this.mActivity).a((int) R.string.net_error);
                if (SpecialChannelFragment.this.mAdapter != null) {
                    List<Info> a2 = SpecialChannelFragment.this.mAdapter.a();
                    if (a2 == null || a2.size() == 0) {
                        SpecialChannelFragment.this.mNetErrorLayout.setVisibility(0);
                    }
                    if (SpecialChannelFragment.this.mAdapter.f()) {
                        SpecialChannelFragment.this.mAdapter.c();
                        SpecialChannelFragment.this.mAdapter.notifyDataSetChanged();
                    }
                } else {
                    return;
                }
            } else if (result.getStatus() == 0) {
                List<Info> data = result.getData();
                if (SpecialChannelFragment.this.isAdded() && i == 0) {
                    List<Info> a3 = SpecialChannelFragment.this.mAdapter.a();
                    if (a3 != null && a3.size() != 0) {
                        String url = a3.get(0).getUrl();
                        i2 = 0;
                        for (Info info : data) {
                            if (TextUtils.isEmpty(url) || url.equals(info.getUrl())) {
                                break;
                            }
                            i2++;
                        }
                    } else {
                        i2 = data != null ? data.size() : 0;
                    }
                    switch (i2) {
                        case 0:
                            ay.a(SpecialChannelFragment.this.mActivity).a((int) R.string.no_update);
                            break;
                        default:
                            SpecialChannelFragment.this.mShowBar.setText(SpecialChannelFragment.this.getString(R.string.update_num, Integer.valueOf(i2)));
                            SpecialChannelFragment.this.mShowBar.startAnimation(SpecialChannelFragment.this.mShowBarAnimation);
                            break;
                    }
                }
                if (data == null || data.size() == 0) {
                    SpecialChannelFragment.this.mAdapter.b();
                } else {
                    SpecialChannelFragment.this.mAdapter.a(data, i != 0, true);
                }
                SpecialChannelFragment.this.mAdapter.notifyDataSetChanged();
                SpecialChannelFragment.this.mNetErrorLayout.setVisibility(8);
            }
            boolean unused = SpecialChannelFragment.this.isRequesting = false;
            b.b(SpecialChannelFragment.this.mActivity, SpecialChannelFragment.this.channelAlias + "_refresh");
        }

        public /* bridge */ /* synthetic */ void onProgressUpdate(int i, Object obj) {
            onProgressUpdate(i, (Result<List<Info>>) ((Result) obj));
        }
    };

    public static void broadCastLocationChanged(Context context) {
        context.sendBroadcast(new Intent(ACTION_ONLOCATION_CHANGE));
    }

    private void getDataFromNet() {
        new y(this.mActivity, "0", this.channelAlias, this.mAdapter.a(), b.a(this.mAdapter)).a(this.onNetRequestListener, 0, new Void[0]);
        this.isRequesting = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.h.b.a(android.content.Context, java.lang.String):java.util.List<com.qihoo360.daily.model.Info>
     arg types: [com.qihoo360.daily.activity.IndexActivity, java.lang.String]
     candidates:
      com.qihoo360.daily.h.b.a(android.content.Context, float):int
      com.qihoo360.daily.h.b.a(java.lang.String, java.text.SimpleDateFormat):long
      com.qihoo360.daily.h.b.a(java.lang.String, java.lang.String):java.lang.String
      com.qihoo360.daily.h.b.a(long, long):boolean
      com.qihoo360.daily.h.b.a(android.app.Activity, java.lang.String):boolean
      com.qihoo360.daily.h.b.a(android.content.Context, java.lang.String):java.util.List<com.qihoo360.daily.model.Info> */
    private void initViews() {
        this.mShowBar = (TextView) this.mContentView.findViewById(R.id.show_bar);
        this.mRecyclerView = (RecyclerView) this.mContentView.findViewById(R.id.recyclerView);
        this.mNetErrorLayout = this.mContentView.findViewById(R.id.error_layout);
        this.mProgressBar = this.mContentView.findViewById(R.id.loading_layout);
        this.mNetErrorLayout.setOnClickListener(this);
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(this.mRecyclerView.getContext()) {
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(-1, -2);
            }
        });
        this.mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                al a2 = al.a((Context) Application.getInstance());
                switch (i) {
                    case 0:
                        if (SpecialChannelFragment.this.mAdapter.getItemCount() > 0 && SpecialChannelFragment.this.mRecyclerView.getChildPosition(SpecialChannelFragment.this.mRecyclerView.getChildAt(SpecialChannelFragment.this.mRecyclerView.getChildCount() + -1)) >= SpecialChannelFragment.this.mAdapter.getItemCount() + -1) {
                            SpecialChannelFragment.this.onLoadMoreVisible();
                        }
                        a2.c(SpecialChannelFragment.this.channelAlias);
                        return;
                    case 1:
                        a2.c(SpecialChannelFragment.this.channelAlias);
                        return;
                    case 2:
                        a2.b((Object) SpecialChannelFragment.this.channelAlias);
                        return;
                    default:
                        return;
                }
            }

            public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            }
        });
        this.mAdapter = new ac(getActivity(), null, this.channelAlias, getRequestCode());
        List<Info> a2 = b.a((Context) this.mActivity, this.channelAlias);
        if (a2 != null && a2.size() > 0) {
            this.mProgressBar.setVisibility(8);
        }
        this.mAdapter.a(a2, false, false);
        this.mAdapter.a(this);
        this.mRecyclerView.setAdapter(this.mAdapter);
        if (!isIMGChannel()) {
            this.mRecyclerView.addItemDecoration(new DividerItemDecoration(this.mRecyclerView.getContext(), 1));
        }
        this.mPullToRefreshView = (SwipeRefreshLayout) this.mContentView.findViewById(R.id.pull_refresh_view);
        this.mPullToRefreshView.setColorScheme(R.color.colorPrimary, R.color.colorPrimaryDark);
        this.mPullToRefreshView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            public void onRefresh() {
                SpecialChannelFragment.this.refresh(true);
            }
        });
        this.mShowBarAnimation = AnimationUtils.loadAnimation(Application.getInstance(), R.anim.anim_alpha_in_out_delay);
    }

    private boolean isIMGChannel() {
        return ChannelType.TYPE_CHANNEL_IMGS.equals(this.channelAlias);
    }

    /* access modifiers changed from: private */
    public boolean isLocalChannel() {
        return ChannelType.TYPE_CHANNEL_LOCAL.equals(this.channelAlias);
    }

    /* access modifiers changed from: private */
    public void onLoadMoreVisible() {
        if (!this.isRequesting && !this.mAdapter.g()) {
            loadMore();
        }
    }

    /* access modifiers changed from: private */
    public void refresh(boolean z) {
        if (isLocalChannel()) {
            refreshTianqi(false);
        }
        if (z && this.mRecyclerView != null) {
            this.mRecyclerView.getLayoutManager().scrollToPosition(0);
        }
        if (this.mAdapter.a() == null || this.mAdapter.a().size() == 0) {
            getDataFromNet();
        } else if (z) {
            getDataFromNet();
        }
    }

    private void refreshTianqi(boolean z) {
        d.f(this.mActivity, -1);
        if (z) {
            this.mAdapter.notifyDataSetChanged();
        }
    }

    public void backToTop(boolean z) {
        if (this.mRecyclerView != null) {
            this.mRecyclerView.post(new Runnable() {
                public void run() {
                    SpecialChannelFragment.this.mRecyclerView.scrollToPosition(0);
                }
            });
            if (z) {
                this.mPullToRefreshView.setRefreshing(true);
                this.mContentView.postDelayed(new Runnable() {
                    public void run() {
                        SpecialChannelFragment.this.refresh(true);
                    }
                }, 200);
            }
        }
    }

    public int getRequestCode() {
        if (ChannelType.TYPE_CHANNEL_GIF.equals(this.channelAlias)) {
            return 3;
        }
        if (ChannelType.TYPE_CHANNEL_JOKE.equals(this.channelAlias)) {
            return 4;
        }
        if (ChannelType.TYPE_CHANNEL_FUN_PIC.equals(this.channelAlias)) {
            return 2;
        }
        return ChannelType.TYPE_CHANNEL_LOCAL.equals(this.channelAlias) ? 5 : 3;
    }

    /* access modifiers changed from: protected */
    public void lazyLoad() {
        if (this.mIsVisible && this.mIsPrepared) {
            refresh(false);
        }
    }

    public void loadMore() {
        Info e;
        if (this.mAdapter.h()) {
            this.mAdapter.d();
            this.mAdapter.notifyDataSetChanged();
        }
        List<Info> a2 = this.mAdapter.a();
        if (a2 != null && a2.size() > 1 && (e = this.mAdapter.e()) != null) {
            new y(this.mActivity, e.getC_pos(), this.channelAlias, null, 20).a(this.onNetRequestListener, 1, new Void[0]);
            this.isRequesting = true;
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (getRequestCode() == i && intent != null) {
            if (getRequestCode() == 4) {
                ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("info_list");
                int intExtra = intent.getIntExtra("position", -1);
                if (!(parcelableArrayListExtra == null || intExtra < 0 || this.mAdapter == null)) {
                    this.mAdapter.a(parcelableArrayListExtra);
                    this.mAdapter.notifyDataSetChanged();
                    this.mRecyclerView.getLayoutManager().scrollToPosition(intExtra);
                }
            } else {
                Info info = (Info) intent.getParcelableExtra("Info");
                if (!(info == null || this.mAdapter == null)) {
                    this.mAdapter.a(info);
                    this.mAdapter.notifyDataSetChanged();
                }
            }
        }
        if (getRequestCode() == 5 && i == 7) {
            refresh(true);
        }
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = (IndexActivity) activity;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.error_layout:
                this.mNetErrorLayout.setVisibility(8);
                this.mProgressBar.setVisibility(0);
                refresh(true);
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.channelAlias = getArguments().getString(BaseFragment.EXTRA_CHANNEL_ALIAS);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_ONLOCATION_CHANGE);
        this.mActivity.registerReceiver(this.onBroadcastReceiver, intentFilter);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        b.b(this.mActivity, this.channelAlias + "_channel_onClick");
        new l(this.mActivity, this.channelAlias).a(null, new Void[0]);
        if (this.mContentView != null) {
            ViewParent parent = this.mContentView.getParent();
            if (parent != null && (parent instanceof ViewGroup)) {
                ((ViewGroup) parent).removeView(this.mContentView);
            }
            this.mIsPrepared = true;
            return this.mContentView;
        }
        this.mContentView = layoutInflater.inflate((int) R.layout.fragment_channel, viewGroup, false);
        initViews();
        this.mIsPrepared = true;
        lazyLoad();
        return this.mContentView;
    }

    public void onDestroy() {
        this.mActivity.unregisterReceiver(this.onBroadcastReceiver);
        al.a((Context) Application.getInstance()).a((Object) this.channelAlias);
        b.a(this.mActivity, this.channelAlias, this.mAdapter);
        super.onDestroy();
    }

    public void release() {
        super.release();
        if (this.mAdapter != null) {
            for (RecyclerView.ViewHolder next : this.mAdapter.f880a) {
                if (next != null) {
                    n.a(next);
                }
            }
        }
    }

    public void setUserVisibleHint(boolean z) {
        super.setUserVisibleHint(z);
    }
}
