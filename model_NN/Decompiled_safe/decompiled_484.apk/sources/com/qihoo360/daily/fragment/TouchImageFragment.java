package com.qihoo360.daily.fragment;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageView;
import com.e.b.al;
import com.e.b.m;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.activity.SearchActivity;
import com.qihoo360.daily.c.k;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.widget.TouchImageView;
import java.util.List;

public class TouchImageFragment extends BaseFragment {
    private View layout;
    /* access modifiers changed from: private */
    public boolean mIsLoadedFailed = false;
    /* access modifiers changed from: private */
    public View mLoading;
    /* access modifiers changed from: private */
    public TouchImageView mView;
    private View.OnClickListener onClickListener;
    /* access modifiers changed from: private */
    public View.OnLongClickListener onLongClickListener;
    private String url;
    private List<String> urls;

    class ImageClallback extends k {
        private ImageView imageView;
        private boolean isSmall;

        public ImageClallback(ImageView imageView2, boolean z) {
            super(imageView2);
            this.imageView = imageView2;
            this.isSmall = z;
        }

        public void imageLoaded(Drawable drawable, String str, boolean z) {
            if (!this.isSmall) {
                TouchImageFragment.this.mLoading.setVisibility(8);
            }
            TouchImageView touchImageView = (TouchImageView) getView();
            if (touchImageView != null && str != null && str.equals(touchImageView.getTag())) {
                if (drawable == null || !(drawable instanceof BitmapDrawable)) {
                    touchImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    touchImageView.setImageResource(R.drawable.icon_img_fail);
                    TouchImageFragment.this.mView.setOnLongClickListener(null);
                    boolean unused = TouchImageFragment.this.mIsLoadedFailed = true;
                    ay.a(Application.getInstance()).a((int) R.string.error_image_load_failed);
                    return;
                }
                this.imageView.setScaleType(ImageView.ScaleType.MATRIX);
                touchImageView.setImageBitmap(((BitmapDrawable) drawable).getBitmap());
                boolean unused2 = TouchImageFragment.this.mIsLoadedFailed = false;
                TouchImageFragment.this.mView.setOnLongClickListener(TouchImageFragment.this.onLongClickListener);
            }
        }
    }

    class PicassoCallback implements m {
        ImageView mImageView;

        public PicassoCallback(ImageView imageView) {
            this.mImageView = imageView;
        }

        public void onError() {
            TouchImageFragment.this.mLoading.setVisibility(8);
            this.mImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            this.mImageView.setImageResource(R.drawable.icon_img_fail);
            TouchImageFragment.this.mView.setOnLongClickListener(null);
            boolean unused = TouchImageFragment.this.mIsLoadedFailed = true;
            ay.a(Application.getInstance()).a((int) R.string.error_image_load_failed);
        }

        public void onSuccess() {
            TouchImageFragment.this.mLoading.setVisibility(8);
            this.mImageView.setScaleType(ImageView.ScaleType.MATRIX);
            boolean unused = TouchImageFragment.this.mIsLoadedFailed = false;
            TouchImageFragment.this.mView.setOnLongClickListener(TouchImageFragment.this.onLongClickListener);
        }
    }

    public boolean isImageLoadedFail() {
        return this.mIsLoadedFailed;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.url = getArguments().getString(SearchActivity.TAG_URL);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ViewParent parent;
        if (this.layout == null || (parent = this.layout.getParent()) == null || !(parent instanceof ViewGroup)) {
            this.layout = layoutInflater.inflate((int) R.layout.fragment_touch_img, viewGroup, false);
            this.mView = (TouchImageView) this.layout.findViewById(R.id.touch_iv);
            this.mLoading = this.layout.findViewById(R.id.loading_layout);
            this.mLoading.setVisibility(0);
            this.mView.setOnClickListener(this.onClickListener);
            this.mView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            if (getActivity() != null) {
                TouchImageView touchImageView = this.mView;
                if (!TextUtils.isEmpty(this.url)) {
                    al.a((Context) Application.getInstance()).a(this.url).a(touchImageView, new PicassoCallback(touchImageView));
                }
            }
            return this.layout;
        }
        ((ViewGroup) parent).removeView(this.layout);
        resetImage();
        return this.layout;
    }

    public void onDestroy() {
        super.onDestroy();
        al.a((Context) Application.getInstance()).a((ImageView) this.mView);
    }

    public void reloadImage() {
        this.mLoading.setVisibility(0);
        this.mView.setImageResource(0);
        if (!TextUtils.isEmpty(this.url)) {
            al.a((Context) Application.getInstance()).a(this.url).a((ImageView) this.mView);
            this.mIsLoadedFailed = false;
        }
    }

    public void resetImage() {
        if (this.mView != null && (this.mView instanceof TouchImageView)) {
            this.mView.reset();
        }
    }

    public void setImageUrils(List<String> list) {
        this.urls = list;
    }

    public void setOnClickListener(View.OnClickListener onClickListener2) {
        this.onClickListener = onClickListener2;
    }

    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener2) {
        this.onLongClickListener = onLongClickListener2;
    }
}
