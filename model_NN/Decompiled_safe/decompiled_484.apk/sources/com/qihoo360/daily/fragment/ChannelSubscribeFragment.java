package com.qihoo360.daily.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import com.qihoo360.daily.R;
import com.qihoo360.daily.model.Channel;
import com.qihoo360.daily.widget.dragdropgrid.ChannelManage;
import com.qihoo360.daily.widget.dragdropgrid.ChannelManageLayout;
import com.qihoo360.daily.widget.dragdropgrid.ChannelsAdapter;
import com.qihoo360.daily.widget.dragdropgrid.MovableGridView;
import java.util.ArrayList;
import java.util.Iterator;

public class ChannelSubscribeFragment extends BaseFragment implements AdapterView.OnItemLongClickListener, ChannelManageLayout.OnItemClickListener, ChannelManageLayout.OnSwichChangeListener {
    public static final String INDEX = "index";
    private Activity mActivity;
    private ChannelManageLayout mChannelManageLayout;
    private Channel mCurrentChannel = null;
    private int mCurrentChannelIndex;
    private boolean mIsEditing = false;
    private ChannelsAdapter mOtherAdapter;
    private MovableGridView mOtherGridView;
    ArrayList<Channel> mSubscribedChannelList;
    ArrayList<Channel> mUnSubscribedChannelList;
    private ChannelsAdapter mUserAdapter;
    private MovableGridView mUserGridView;

    private void initData() {
        this.mSubscribedChannelList = ChannelManage.getSubscribedChannels();
        this.mUnSubscribedChannelList = ChannelManage.getUnsubscribedChannels();
        if (this.mSubscribedChannelList != null && this.mSubscribedChannelList.size() > this.mCurrentChannelIndex) {
            this.mCurrentChannel = this.mSubscribedChannelList.get(this.mCurrentChannelIndex);
        }
        this.mUserAdapter = new ChannelsAdapter(this.mActivity, this.mSubscribedChannelList, this.mCurrentChannel, true);
        this.mUserGridView.setAdapter((ListAdapter) this.mUserAdapter);
        this.mUserGridView.setOnItemLongClickListener(this);
        this.mChannelManageLayout.setOnItemClickListener(this);
        this.mOtherAdapter = new ChannelsAdapter(this.mActivity, this.mUnSubscribedChannelList, null, false);
        this.mOtherGridView.setAdapter((ListAdapter) this.mOtherAdapter);
        this.mChannelManageLayout.setOnSwichChangeListener(this);
    }

    private void saveEditData() {
        ChannelManage.saveSubscribedChannels(this.mActivity.getBaseContext(), this.mSubscribedChannelList);
        ChannelManage.saveUnsubscribedChannels(this.mActivity.getBaseContext(), this.mUnSubscribedChannelList);
    }

    public void OnSwitch(int i, int i2) {
        this.mUserGridView.switchView(i, i2);
        this.mUserAdapter.switchItem(i, i2);
        if (this.mCurrentChannel != null && this.mCurrentChannel.getAlias() != null) {
            int i3 = 0;
            String alias = this.mCurrentChannel.getAlias();
            Iterator<Channel> it = this.mSubscribedChannelList.iterator();
            while (true) {
                int i4 = i3;
                if (!it.hasNext()) {
                    return;
                }
                if (alias.equals(it.next().getAlias())) {
                    this.mCurrentChannelIndex = i4;
                    return;
                }
                i3 = i4 + 1;
            }
        }
    }

    public int getCurrentIndex() {
        saveEditData();
        return this.mCurrentChannelIndex;
    }

    public boolean isEditing() {
        return this.mIsEditing;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = activity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.fragment_channel_subscribe, viewGroup, false);
        this.mChannelManageLayout = (ChannelManageLayout) inflate.findViewById(R.id.channelManageLayout);
        this.mUserGridView = this.mChannelManageLayout.getGridView();
        this.mOtherGridView = (MovableGridView) inflate.findViewById(R.id.gv_more_channels);
        this.mCurrentChannelIndex = getArguments().getInt("index");
        initData();
        return inflate;
    }

    public void onItemClick(AdapterView<?> adapterView, int i, Channel channel) {
        if (adapterView != this.mUserGridView) {
            return;
        }
        if (!isEditing()) {
            this.mCurrentChannel = this.mUserAdapter.getItem(i);
            this.mCurrentChannelIndex = i;
            this.mUserAdapter.setChannel(this.mCurrentChannel);
            this.mUserAdapter.notifyDataSetChanged();
            this.mActivity.onBackPressed();
        } else if (channel != null) {
            String alias = channel.getAlias();
            if (!(alias == null || this.mCurrentChannel == null || !alias.equals(this.mCurrentChannel.getAlias()))) {
                this.mCurrentChannelIndex = 0;
                this.mCurrentChannel = this.mSubscribedChannelList.get(0);
                this.mUserAdapter.setChannel(this.mCurrentChannel);
            }
            this.mCurrentChannelIndex = this.mUserAdapter.getIndex(this.mCurrentChannel);
        }
    }

    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long j) {
        setEditing(true);
        Fragment parentFragment = getParentFragment();
        if (parentFragment != null && (parentFragment instanceof NavigationFragment)) {
            ((NavigationFragment) parentFragment).updateEditButtonStatus(false);
        }
        return true;
    }

    public void setEditing(boolean z) {
        this.mIsEditing = z;
        this.mChannelManageLayout.setEdit(this.mIsEditing);
        this.mUserAdapter.setEditting(this.mIsEditing);
        if (this.mIsEditing) {
            this.mUserGridView.setLongClickable(false);
        } else {
            this.mUserGridView.setLongClickable(true);
        }
    }
}
