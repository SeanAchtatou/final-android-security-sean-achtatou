package com.qihoo360.daily.fragment;

public abstract class LazyLoadFragment extends BaseFragment {
    protected boolean mIsPrepared;
    protected boolean mIsVisible;

    /* access modifiers changed from: protected */
    public abstract void lazyLoad();

    /* access modifiers changed from: protected */
    public void onInvisible() {
    }

    /* access modifiers changed from: protected */
    public void onVisible() {
        lazyLoad();
    }

    public void setUserVisibleHint(boolean z) {
        super.setUserVisibleHint(z);
        if (getUserVisibleHint()) {
            this.mIsVisible = true;
            onVisible();
            return;
        }
        this.mIsVisible = false;
        onInvisible();
    }
}
