package com.qihoo360.daily.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.e.b.al;
import com.qihoo360.daily.R;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.activity.ImagesActivity;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.af;
import com.qihoo360.daily.h.ay;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.NewsRelated;
import com.qihoo360.daily.model.Related;
import com.qihoo360.daily.model.Result;
import java.util.ArrayList;

public class ImgRecFragment extends BaseFragment implements View.OnClickListener {
    /* access modifiers changed from: private */
    public View mRefreshView;
    private View mView;
    private String nid;
    private c<Void, Result<NewsRelated>> onNetRequestListener = new c<Void, Result<NewsRelated>>() {
        public void onNetRequest(int i, Result<NewsRelated> result) {
            if (ImgRecFragment.this.mRefreshView != null) {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        ImgRecFragment.this.mRefreshView.clearAnimation();
                        ImgRecFragment.this.mRefreshView.setClickable(true);
                    }
                }, 680);
            }
            if (result == null) {
                ay.a(Application.getInstance()).a((int) R.string.net_error);
            } else if (result.getStatus() == 0) {
                ArrayList unused = ImgRecFragment.this.relateds = result.getData().getRelated();
                if (ImgRecFragment.this.relateds != null && ImgRecFragment.this.relateds.size() != 0) {
                    ImgRecFragment.this.initData();
                }
            } else {
                ay.a(Application.getInstance()).a(result.getMsg());
            }
        }

        public /* bridge */ /* synthetic */ void onNetRequest(int i, Object obj) {
            onNetRequest(i, (Result<NewsRelated>) ((Result) obj));
        }
    };
    private int page;
    /* access modifiers changed from: private */
    public ArrayList<Related> relateds;
    private String url;
    private int v_t = 3;

    private int getImageWidth(String str) {
        String[] split;
        if (str != null) {
            int length = str.length();
            if (str.contains("?size=")) {
                try {
                    int lastIndexOf = str.lastIndexOf("=") + 1;
                    if (lastIndexOf < length) {
                        String substring = str.substring(lastIndexOf);
                        if (substring.contains("x") && (split = substring.split("x")) != null && split.length >= 2) {
                            return Integer.valueOf(split[0]).intValue();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return -1;
    }

    private String getUrlFromRelated(Related related) {
        if (related == null) {
            return null;
        }
        String imgurl = related.getImgurl();
        return (TextUtils.isEmpty(imgurl) || !imgurl.contains("|")) ? imgurl : imgurl.substring(0, imgurl.indexOf("|"));
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: private */
    public void initData() {
        if (this.mView != null) {
            View findViewById = this.mView.findViewById(R.id.atlas_rec_view0);
            View findViewById2 = this.mView.findViewById(R.id.atlas_rec_view1);
            View findViewById3 = this.mView.findViewById(R.id.atlas_rec_view2);
            View findViewById4 = this.mView.findViewById(R.id.atlas_rec_view3);
            View findViewById5 = this.mView.findViewById(R.id.atlas_rec_view4);
            View findViewById6 = this.mView.findViewById(R.id.atlas_rec_view5);
            View findViewById7 = this.mView.findViewById(R.id.atlas_rec_view6);
            View findViewById8 = this.mView.findViewById(R.id.atlas_rec_view7);
            ImageView imageView = (ImageView) this.mView.findViewById(R.id.atlas_rec_img0);
            ImageView imageView2 = (ImageView) this.mView.findViewById(R.id.atlas_rec_img1);
            ImageView imageView3 = (ImageView) this.mView.findViewById(R.id.atlas_rec_img2);
            ImageView imageView4 = (ImageView) this.mView.findViewById(R.id.atlas_rec_img3);
            ImageView imageView5 = (ImageView) this.mView.findViewById(R.id.atlas_rec_img4);
            ImageView imageView6 = (ImageView) this.mView.findViewById(R.id.atlas_rec_img5);
            ImageView imageView7 = (ImageView) this.mView.findViewById(R.id.atlas_rec_img6);
            ImageView imageView8 = (ImageView) this.mView.findViewById(R.id.atlas_rec_img7);
            TextView textView = (TextView) this.mView.findViewById(R.id.atlas_rec_txt0);
            TextView textView2 = (TextView) this.mView.findViewById(R.id.atlas_rec_txt1);
            TextView textView3 = (TextView) this.mView.findViewById(R.id.atlas_rec_txt2);
            TextView textView4 = (TextView) this.mView.findViewById(R.id.atlas_rec_txt3);
            TextView textView5 = (TextView) this.mView.findViewById(R.id.atlas_rec_txt4);
            TextView textView6 = (TextView) this.mView.findViewById(R.id.atlas_rec_txt5);
            TextView textView7 = (TextView) this.mView.findViewById(R.id.atlas_rec_txt6);
            TextView textView8 = (TextView) this.mView.findViewById(R.id.atlas_rec_txt7);
            if (this.relateds != null && this.relateds.size() > 0) {
                int size = this.relateds.size();
                if (size > 8) {
                    size = 8;
                }
                switch (size) {
                    case 2:
                        setImage(imageView2, 1, 1);
                        setText(textView2, 1);
                        setImage(imageView, 0, 0);
                        setText(textView, 0);
                        break;
                    case 3:
                        setImage(imageView3, 2, 2);
                        setText(textView3, 2);
                        setImage(imageView2, 1, 1);
                        setText(textView2, 1);
                        setImage(imageView, 0, 0);
                        setText(textView, 0);
                        break;
                    case 5:
                        setImage(imageView5, 4, 1);
                        setImage(imageView4, 3, 0);
                        setText(textView5, 4);
                        setText(textView4, 3);
                        setImage(imageView3, 2, 2);
                        setText(textView3, 2);
                        setImage(imageView2, 1, 1);
                        setText(textView2, 1);
                        setImage(imageView, 0, 0);
                        setText(textView, 0);
                        break;
                    case 8:
                        setImage(imageView8, 7, 0);
                        setImage(imageView7, 6, 0);
                        setImage(imageView6, 5, 0);
                        setText(textView8, 7);
                        setText(textView7, 6);
                        setText(textView6, 5);
                        setImage(imageView5, 4, 1);
                        setImage(imageView4, 3, 0);
                        setText(textView5, 4);
                        setText(textView4, 3);
                        setImage(imageView3, 2, 2);
                        setText(textView3, 2);
                        setImage(imageView2, 1, 1);
                        setText(textView2, 1);
                        setImage(imageView, 0, 0);
                        setText(textView, 0);
                        break;
                }
                switch (size) {
                    case 0:
                        findViewById.setVisibility(8);
                        findViewById2.setVisibility(8);
                        findViewById3.setVisibility(8);
                        findViewById4.setVisibility(8);
                        findViewById5.setVisibility(8);
                        break;
                    case 1:
                    default:
                        return;
                    case 2:
                        findViewById3.setVisibility(8);
                        findViewById4.setVisibility(8);
                        findViewById5.setVisibility(8);
                        break;
                    case 3:
                    case 4:
                        findViewById4.setVisibility(8);
                        findViewById5.setVisibility(8);
                        break;
                    case 5:
                    case 6:
                    case 7:
                        break;
                }
                findViewById8.setVisibility(8);
                findViewById7.setVisibility(8);
                findViewById6.setVisibility(8);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private View initView(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        if (layoutInflater == null) {
            return null;
        }
        this.mView = layoutInflater.inflate((int) R.layout.img_rec_views, viewGroup, false);
        this.mRefreshView = this.mView.findViewById(R.id.rec_refresh_btn);
        this.mRefreshView.setOnClickListener(this);
        this.mView.findViewById(R.id.atlas_rec_click0).setOnClickListener(this);
        this.mView.findViewById(R.id.atlas_rec_click1).setOnClickListener(this);
        this.mView.findViewById(R.id.atlas_rec_click2).setOnClickListener(this);
        this.mView.findViewById(R.id.atlas_rec_click3).setOnClickListener(this);
        this.mView.findViewById(R.id.atlas_rec_click4).setOnClickListener(this);
        this.mView.findViewById(R.id.atlas_rec_click5).setOnClickListener(this);
        this.mView.findViewById(R.id.atlas_rec_click6).setOnClickListener(this);
        this.mView.findViewById(R.id.atlas_rec_click7).setOnClickListener(this);
        return this.mView;
    }

    private void setImage(ImageView imageView, int i, int i2) {
        FragmentActivity activity = getActivity();
        if (activity != null && this.relateds != null && this.relateds.size() > i) {
            String urlFromRelated = getUrlFromRelated(this.relateds.get(i));
            int d = a.d(activity) / 3;
            switch (i2) {
                case 1:
                    d *= 2;
                    break;
                case 2:
                    d *= 3;
                    break;
            }
            af.a(this, imageView, urlFromRelated, d, getResources().getDimensionPixelSize(R.dimen.rec_height), false, R.drawable.img_rec_zhanwei);
        }
    }

    private void setText(TextView textView, int i) {
        if (this.relateds != null && this.relateds.size() > i && i > -1) {
            textView.setText(this.relateds.get(i).getTitle());
        }
    }

    public c<Void, Result<NewsRelated>> getOnNetRequestListener() {
        return this.onNetRequestListener;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        switch (view.getId()) {
            case R.id.rec_refresh_btn:
                this.page++;
                this.mRefreshView.setClickable(false);
                this.mRefreshView.startAnimation(AnimationUtils.loadAnimation(Application.getInstance(), R.anim.rotate_loading_anim));
                new com.qihoo360.daily.g.ay(Application.getInstance(), this.nid, this.url, this.v_t, this.page % 5, null).a(this.onNetRequestListener, new Void[0]);
                return;
            case R.id.atlas_rec_click0:
                i = 0;
                break;
            case R.id.atlas_rec_click1:
                i2 = 0;
                i = i2 + 1;
                break;
            case R.id.atlas_rec_click2:
                i3 = 0;
                i2 = i3 + 1;
                i = i2 + 1;
                break;
            case R.id.atlas_rec_click3:
                i4 = 0;
                i3 = i4 + 1;
                i2 = i3 + 1;
                i = i2 + 1;
                break;
            case R.id.atlas_rec_click4:
                i5 = 0;
                i4 = i5 + 1;
                i3 = i4 + 1;
                i2 = i3 + 1;
                i = i2 + 1;
                break;
            case R.id.atlas_rec_click5:
                i6 = 0;
                i5 = i6 + 1;
                i4 = i5 + 1;
                i3 = i4 + 1;
                i2 = i3 + 1;
                i = i2 + 1;
                break;
            case R.id.atlas_rec_click6:
                i7 = 0;
                i6 = i7 + 1;
                i5 = i6 + 1;
                i4 = i5 + 1;
                i3 = i4 + 1;
                i2 = i3 + 1;
                i = i2 + 1;
                break;
            case R.id.atlas_rec_click7:
                i7 = 1;
                i6 = i7 + 1;
                i5 = i6 + 1;
                i4 = i5 + 1;
                i3 = i4 + 1;
                i2 = i3 + 1;
                i = i2 + 1;
                break;
            default:
                return;
        }
        if (this.relateds != null && this.relateds.size() > i) {
            Intent intent = new Intent(view.getContext(), ImagesActivity.class);
            intent.putExtra("Info", this.relateds.get(i));
            intent.putExtra("from", ChannelType.TYPE_IMG_RELATED);
            intent.putExtra(ImagesActivity.HAS_MORE_IMG_REC, false);
            startActivity(intent);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Info info = (Info) getArguments().getParcelable("Info");
        if (info != null) {
            this.url = info.getUrl();
            this.nid = String.valueOf(info.getNid());
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ViewParent parent;
        if (this.mView == null || (parent = this.mView.getParent()) == null || !(parent instanceof ViewGroup)) {
            this.mView = initView(layoutInflater, viewGroup);
            if (this.relateds == null) {
                new com.qihoo360.daily.g.ay(Application.getInstance(), this.nid, this.url, this.v_t, this.page, null).a(this.onNetRequestListener, new Void[0]);
            } else {
                initData();
            }
            return this.mView;
        }
        ((ViewGroup) parent).removeView(this.mView);
        return this.mView;
    }

    public void onDestroy() {
        super.onDestroy();
        al.a((Context) Application.getInstance()).a(this);
    }
}
