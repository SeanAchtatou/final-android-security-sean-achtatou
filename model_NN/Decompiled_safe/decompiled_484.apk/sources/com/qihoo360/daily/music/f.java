package com.qihoo360.daily.music;

import com.qihoo360.daily.d.c;
import com.qihoo360.daily.h.ad;
import com.qihoo360.daily.model.Result;
import com.qihoo360.daily.model.Url;

class f extends c<Void, Result<Url>> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1164a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ PlayerService f1165b;

    f(PlayerService playerService, String str) {
        this.f1165b = playerService;
        this.f1164a = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.music.PlayerService.a(com.qihoo360.daily.music.PlayerService, boolean):boolean
     arg types: [com.qihoo360.daily.music.PlayerService, int]
     candidates:
      com.qihoo360.daily.music.PlayerService.a(android.content.Context, java.lang.String):java.lang.String
      com.qihoo360.daily.music.PlayerService.a(com.qihoo360.daily.music.PlayerService, android.media.MediaPlayer):void
      com.qihoo360.daily.music.PlayerService.a(com.qihoo360.daily.music.PlayerService, boolean):boolean */
    /* renamed from: a */
    public void onNetRequest(int i, Result<Url> result) {
        boolean unused = this.f1165b.f = false;
        if (result == null || result.getStatus() != 0 || result.getData() == null) {
            this.f1165b.g();
            this.f1165b.j();
            ad.a("GetSongUrlByIdTask 结果为空");
            return;
        }
        String url = result.getData().getUrl();
        if (url == null || "".equals(url.trim())) {
            ad.a("GetSongUrlByIdTask url为空");
            this.f1165b.j();
            return;
        }
        this.f1165b.a(this.f1164a, url.replaceAll("\\s", "%20"), false);
    }
}
