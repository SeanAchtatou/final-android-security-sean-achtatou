package com.qihoo360.daily.music;

import android.content.Context;
import android.os.Environment;
import com.qihoo360.daily.h.ac;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

class a {

    /* renamed from: a  reason: collision with root package name */
    private int f1158a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public ServerSocket f1159b = null;
    private String c;
    private URI d;
    /* access modifiers changed from: private */
    public String e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public Context g;
    private byte[] h = new byte[10240];
    private int i = 0;
    /* access modifiers changed from: private */
    public boolean j = false;

    public a(Context context, int i2) {
        this.g = context;
        this.f1158a = i2;
        a(this.f1158a);
        String externalStorageState = Environment.getExternalStorageState();
        if (externalStorageState == null || "".equals(externalStorageState.trim())) {
            this.j = false;
        } else if (externalStorageState.trim().equals("mounted")) {
            this.j = true;
        } else {
            this.j = false;
        }
    }

    /* access modifiers changed from: private */
    public String a(String str, int i2) {
        return str.replaceAll(ac.a(str, "Range: bytes=", "-") + "-", i2 + "-");
    }

    /* access modifiers changed from: private */
    public String a(Socket socket) {
        String replace;
        String str = "";
        if (socket == null) {
            return str;
        }
        try {
            InputStream inputStream = socket.getInputStream();
            byte[] bArr = new byte[1024];
            while (inputStream.read(bArr) != -1) {
                String str2 = str + new String(bArr);
                try {
                    if (!str2.contains("GET") || !str2.contains("\r\n\r\n") || this.c == null || "".equals(this.c.trim())) {
                        str = str2;
                    } else {
                        replace = str2.replace("127.0.0.1", this.c);
                        str = this.d.getPort() == -1 ? replace.replace(":" + this.f1158a, "") : replace.replace(":" + this.f1158a, ":" + this.d.getPort());
                        return !str.contains("RANGE: bytes=") ? str.replace("\r\n\r\n", "\r\nRange: bytes=0-\r\n\r\n") : str;
                    }
                } catch (IOException e2) {
                    IOException iOException = e2;
                    str = replace;
                    e = iOException;
                }
            }
            return str;
        } catch (IOException e3) {
            e = e3;
            e.printStackTrace();
            return str;
        }
    }

    /* access modifiers changed from: private */
    public Socket a(String str) {
        IOException e2;
        Socket socket;
        InetSocketAddress inetSocketAddress = this.d.getPort() != -1 ? new InetSocketAddress(this.c, this.d.getPort()) : new InetSocketAddress(this.c, 80);
        try {
            socket = new Socket();
            try {
                socket.connect(inetSocketAddress);
                OutputStream outputStream = socket.getOutputStream();
                outputStream.write(str.getBytes());
                outputStream.flush();
            } catch (IOException e3) {
                e2 = e3;
                e2.printStackTrace();
                return socket;
            }
        } catch (IOException e4) {
            IOException iOException = e4;
            socket = null;
            e2 = iOException;
            e2.printStackTrace();
            return socket;
        }
        return socket;
    }

    /* access modifiers changed from: private */
    public List<byte[]> a(String str, String str2, byte[] bArr, int i2) {
        if (this.i + i2 >= this.h.length) {
            b();
        }
        System.arraycopy(bArr, 0, this.h, this.i, i2);
        this.i += i2;
        String str3 = new String(this.h);
        if (!str3.contains(str) || !str3.contains(str2)) {
            return null;
        }
        int indexOf = str3.indexOf(str, 0);
        byte[] bArr2 = new byte[((str3.indexOf(str2, indexOf) + str2.length()) - indexOf)];
        System.arraycopy(this.h, indexOf, bArr2, 0, bArr2.length);
        ArrayList arrayList = new ArrayList();
        arrayList.add(new String(bArr2).replaceAll("\\s*", "").getBytes());
        if (this.i > bArr2.length) {
            byte[] bArr3 = new byte[(this.i - bArr2.length)];
            System.arraycopy(this.h, bArr2.length, bArr3, 0, bArr3.length);
            arrayList.add(new String(bArr3).replaceAll("\\s*", "").getBytes());
        }
        b();
        return arrayList;
    }

    private void a(int i2) {
        try {
            this.f1159b = new ServerSocket(i2, 1, InetAddress.getByName("127.0.0.1"));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void b() {
        this.h = new byte[10240];
        this.i = 0;
    }

    public String a(String str, String str2) {
        this.e = str;
        this.f = ac.f1089a + File.separator + this.e;
        this.d = URI.create(str2);
        this.c = this.d.getHost();
        return this.d.getPort() != -1 ? str2.replace(this.c + ":" + this.d.getPort(), "127.0.0.1:" + this.f1158a) : str2.replace(this.c, "127.0.0.1:" + this.f1158a);
    }

    public void a() {
        new b(this).start();
    }
}
