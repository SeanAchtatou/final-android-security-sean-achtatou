package com.qihoo360.daily.music;

import android.content.Context;
import android.content.Intent;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private static c f1161a = new c();

    private c() {
    }

    public static c a() {
        return f1161a;
    }

    public void a(Context context) {
        Intent intent = new Intent(context, PlayerService.class);
        intent.putExtra("action", "com.qihoo.pause");
        context.startService(intent);
    }

    public void a(Context context, String str) {
        Intent intent = new Intent(context, PlayerService.class);
        intent.putExtra("action", "com.qihoo.start");
        intent.putExtra("key_id_music", str);
        context.startService(intent);
    }

    public void b(Context context) {
        Intent intent = new Intent(context, PlayerService.class);
        intent.putExtra("action", "com.qihoo.stop");
        context.startService(intent);
    }
}
