package com.qihoo360.daily.music;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.text.TextUtils;
import com.qihoo360.daily.g.w;
import com.qihoo360.daily.h.ac;
import com.qihoo360.daily.h.ad;
import com.qihoo360.daily.h.bj;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class PlayerService extends Service {

    /* renamed from: a  reason: collision with root package name */
    private MediaPlayer f1156a;

    /* renamed from: b  reason: collision with root package name */
    private a f1157b;
    private boolean c = false;
    private boolean d = false;
    private String e = "";
    /* access modifiers changed from: private */
    public boolean f = false;

    private String a(Context context, String str) {
        if (!context.getSharedPreferences("cache", 1).getBoolean(str, false)) {
            return null;
        }
        File file = new File(ac.f1089a + File.separator + str);
        if (file.exists() && file.length() > 0) {
            return file.getAbsolutePath();
        }
        SharedPreferences.Editor edit = getSharedPreferences("cache", 2).edit();
        edit.putBoolean(str, false);
        edit.commit();
        return null;
    }

    private void a() {
        c();
        b();
    }

    private void a(Intent intent) {
        int i = this.c ? this.f1156a.isPlaying() ? 2 : 3 : 1;
        Intent intent2 = new Intent("com.qihoo.play.state");
        intent2.putExtra("key_play_state", i);
        intent2.putExtra("key_type", intent.getIntExtra("key_type", 1));
        sendBroadcast(intent2);
    }

    /* access modifiers changed from: private */
    public void a(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
        sendBroadcast(new Intent("com.qihoo.start"));
        this.c = true;
    }

    private void a(String str) {
        boolean z;
        if (!TextUtils.isEmpty(str)) {
            String a2 = a(this, str);
            if (a2 == null || "".equals(a2.trim())) {
                ad.a("没缓存，需联网");
                z = true;
            } else {
                File file = new File(a2);
                if (!file.exists() || file.length() == 0) {
                    SharedPreferences.Editor edit = getSharedPreferences("cache", 2).edit();
                    edit.putBoolean(str, false);
                    edit.commit();
                    z = true;
                } else {
                    ad.a("缓存存在");
                    a(str, a2, true);
                    z = false;
                }
            }
        } else {
            j();
            ad.a("id为空，不播放");
            z = false;
        }
        if (z && !this.f) {
            this.f = true;
            new w(str, this).a(new f(this, str), new Void[0]);
        }
        this.e = str;
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, boolean z) {
        if (this.f1156a == null) {
            c();
        }
        this.f1156a.reset();
        this.f1156a.setAudioStreamType(3);
        if (!z) {
            String a2 = this.f1157b.a(str, str2);
            if (!bj.f()) {
                str2 = a2;
            }
            this.f1156a.setDataSource(str2);
        } else if (str2 != null) {
            try {
                if (!"".equals(str2.trim())) {
                    FileInputStream fileInputStream = new FileInputStream(new File(str2));
                    this.f1156a.setDataSource(fileInputStream.getFD());
                    fileInputStream.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                return;
            }
        }
        this.f1156a.prepareAsync();
        this.d = true;
    }

    private void b() {
        this.f1157b = new a(this, 30360);
        try {
            this.f1157b.a();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private void b(Intent intent) {
        String stringExtra = intent.getStringExtra("key_id_music");
        if (!this.d) {
            i();
            a(stringExtra);
        } else if (stringExtra == null || "".equals(stringExtra.trim()) || this.e == null || "".equals(this.e.trim()) || !stringExtra.trim().equals(this.e.trim())) {
            i();
            a(stringExtra);
        } else if (!this.c) {
            ad.a("没有准备好,不做任何事，等待准备好继续播放");
        } else if (this.f1156a != null) {
            this.f1156a.seekTo(0);
            this.f1156a.start();
            sendBroadcast(new Intent("com.qihoo.start"));
        } else {
            this.c = false;
            this.d = false;
            this.e = "";
            a(stringExtra);
        }
    }

    private void c() {
        this.f1156a = new MediaPlayer();
        this.f1156a.setOnPreparedListener(new d(this));
        this.f1156a.setOnCompletionListener(new e(this));
    }

    private void d() {
        sendBroadcast(new Intent("com.qihoo.exception"));
    }

    private void e() {
        if (this.c && this.f1156a != null) {
            this.f1156a.start();
            sendBroadcast(new Intent("com.qihoo.start"));
        }
    }

    private void f() {
        if (this.f1156a == null) {
            return;
        }
        if (this.f1156a.isPlaying()) {
            this.f1156a.pause();
            sendBroadcast(new Intent("com.qihoo.pause"));
            return;
        }
        this.f1156a.start();
        sendBroadcast(new Intent("com.qihoo.start"));
    }

    /* access modifiers changed from: private */
    public void g() {
        ad.a("PlayerService dealStop");
        sendBroadcast(new Intent("com.qihoo.stop"));
        if (this.f1156a != null) {
            this.c = false;
            j();
        }
    }

    private void h() {
        if (this.f1156a != null && this.f1156a.isPlaying()) {
            this.f1156a.pause();
            sendBroadcast(new Intent("com.qihoo.pause"));
        }
    }

    private void i() {
        this.d = false;
        this.c = false;
        if (this.f1156a != null) {
            this.f1156a.stop();
            this.f1156a.release();
            this.f1156a = null;
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        ad.a("PlayerService release");
        this.e = "";
        i();
        stopSelf();
        System.exit(0);
    }

    /* access modifiers changed from: private */
    public void k() {
        ad.a("PlayerService dealCompletion");
        sendBroadcast(new Intent("com.qihoo.completion"));
        j();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        a();
    }

    public void onDestroy() {
        super.onDestroy();
        j();
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        String str = null;
        if (intent != null) {
            str = intent.getStringExtra("action");
        }
        if (str == null || "".equals(str)) {
            return 1;
        }
        ad.a("PlayerService action:" + str);
        if (str.equals("com.qihoo.start")) {
            b(intent);
            return 1;
        } else if (str.equals("com.qihoo.pause")) {
            h();
            return 1;
        } else if (str.equals("com.qihoo.stop")) {
            g();
            return 1;
        } else if (str.equals("com.qihoo.play.state")) {
            a(intent);
            return 1;
        } else if (str.equals("com.qihoo.toggle")) {
            f();
            return 1;
        } else if (str.equals("com.qihoo.play")) {
            e();
            return 1;
        } else if (!str.equals("com.qihoo.exception")) {
            return 1;
        } else {
            d();
            return 1;
        }
    }
}
