package com.qihoo360.daily.g;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import com.qihoo360.daily.d.c;
import com.qihoo360.daily.h.bj;

public abstract class a<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

    /* renamed from: a  reason: collision with root package name */
    protected int f1060a;

    /* renamed from: b  reason: collision with root package name */
    protected c<Progress, Result> f1061b;
    private boolean c;

    @TargetApi(11)
    public void a(c<Progress, Result> cVar, int i, Params... paramsArr) {
        this.f1061b = cVar;
        this.f1060a = i;
        if (bj.a()) {
            executeOnExecutor(be.f1074a, paramsArr);
        } else {
            execute(paramsArr);
        }
    }

    @TargetApi(11)
    public void a(c cVar, Object... objArr) {
        a(cVar, 0, objArr);
    }

    public void a(Object obj) {
        if (!this.c) {
            publishProgress(obj);
        }
        this.c = true;
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
        if (this.f1061b != null) {
            this.f1061b.onNetRequest(this.f1060a, null);
        }
    }

    /* access modifiers changed from: protected */
    @TargetApi(11)
    public void onCancelled(Result result) {
        super.onCancelled(result);
        if (this.f1061b != null) {
            this.f1061b.onNetRequest(this.f1060a, result);
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Result result) {
        super.onPostExecute(result);
        if (this.f1061b != null) {
            this.f1061b.onNetRequest(this.f1060a, result);
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Progress... progressArr) {
        super.onProgressUpdate(progressArr);
        if (this.f1061b != null && progressArr != null && progressArr.length > 0) {
            this.f1061b.onProgressUpdate(this.f1060a, progressArr[0]);
        }
    }
}
