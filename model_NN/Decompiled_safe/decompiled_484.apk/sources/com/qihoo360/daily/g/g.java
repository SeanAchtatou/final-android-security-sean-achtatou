package com.qihoo360.daily.g;

import android.content.Context;
import android.text.TextUtils;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.h.bi;
import java.io.IOException;
import org.apache.http.Header;

public class g extends a<String, Void, String> {
    private Context c;
    private int d;
    private String e;

    public g(Context context, int i, String str) {
        this.c = context;
        this.d = i;
        this.e = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(String... strArr) {
        String str;
        try {
            str = b.a(bi.a(this.c, this.d, this.e), new Header[0]);
        } catch (IOException e2) {
            e2.printStackTrace();
            str = null;
        }
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return str;
    }
}
