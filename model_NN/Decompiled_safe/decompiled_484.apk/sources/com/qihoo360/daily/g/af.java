package com.qihoo360.daily.g;

import android.content.Context;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.model.User;
import org.apache.http.Header;

public class af extends a<Void, Void, User> {
    private static final String c = af.class.getSimpleName();
    private String d;
    private String e;
    private Context f;

    public af(Context context, String str, String str2) {
        this.d = str;
        this.e = str2;
        this.f = context;
    }

    private static User a(Context context) {
        return d.a(context);
    }

    public static void a(Context context, ah ahVar) {
        User a2 = a(context);
        if (ahVar != null) {
            ahVar.onGetedUserInfo(a2);
        }
    }

    private void a(String str) {
        d.e(this.f, str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public User doInBackground(Void... voidArr) {
        try {
            String a2 = b.a(bi.a(this.d, this.e), new Header[0]);
            if (a2 != null) {
                a(a2);
            }
            User user = (User) Application.getGson().a(a2, new ag(this).getType());
            if (user != null) {
                return user;
            }
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
