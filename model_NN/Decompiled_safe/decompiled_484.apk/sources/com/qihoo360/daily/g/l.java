package com.qihoo360.daily.g;

import android.content.Context;
import com.qihoo.dynamic.DotCount;
import com.qihoo360.daily.R;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.h.ad;
import com.qihoo360.daily.h.bi;
import java.io.IOException;
import java.net.URI;
import org.apache.http.Header;

public class l extends a<Void, Void, Void> {
    private Context c;
    private String d;

    public l(Context context, String str) {
        this.c = context;
        this.d = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Void... voidArr) {
        URI e = bi.e(this.c, this.d);
        DotCount.setAppName(this.c.getString(R.string.dot_app_name));
        if (e != null) {
            String uri = e.toString();
            ad.a("sendCount url:" + uri);
            ad.a("sendCount code:" + DotCount.sendCount(this.c, uri));
        }
        try {
            b.a(e, new Header[0]);
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
