package com.qihoo360.daily.g;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

final class bf implements ThreadFactory {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicInteger f1076a = new AtomicInteger(1);

    bf() {
    }

    public Thread newThread(Runnable runnable) {
        return new Thread(runnable, "AsyncTask #" + this.f1076a.getAndIncrement());
    }
}
