package com.qihoo360.daily.g;

import android.app.Activity;
import android.app.ProgressDialog;
import android.text.TextUtils;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.model.NewComment;
import com.qihoo360.daily.model.Result;
import com.qihoo360.daily.model.User;
import com.qihoo360.daily.widget.AsyncProgressDialog;
import com.qihoo360.daily.wxapi.WXInfoKeeper;
import com.qihoo360.daily.wxapi.WXToken;
import com.qihoo360.daily.wxapi.WXUser;
import java.net.URI;
import java.util.ArrayList;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public class at extends a<Void, Void, Result<NewComment>> {
    private Activity c;
    private String d;
    private String e;
    private String f;
    private String g;
    private ProgressDialog h;

    public at(Activity activity, String str, String str2, String str3, String str4) {
        this.c = activity;
        this.g = str;
        this.d = str2;
        this.f = str3;
        this.e = str4;
    }

    private static String a(String str) {
        return TextUtils.isEmpty(str) ? "0" : str;
    }

    public static ArrayList<NameValuePair> a(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8) {
        ArrayList<NameValuePair> arrayList = new ArrayList<>();
        String a2 = a(str);
        arrayList.add(new BasicNameValuePair("key", a2));
        arrayList.add(new BasicNameValuePair("title", str8));
        String a3 = a(str2);
        arrayList.add(new BasicNameValuePair("comment", a3));
        String a4 = a(str4);
        arrayList.add(new BasicNameValuePair("comments_on", a4));
        String a5 = a(str3);
        arrayList.add(new BasicNameValuePair("uid", a5));
        String a6 = a(str6);
        arrayList.add(new BasicNameValuePair("uname", a6));
        String a7 = a(str7);
        arrayList.add(new BasicNameValuePair("uavatar", a7));
        String a8 = a(str5);
        arrayList.add(new BasicNameValuePair("uid_type", a8));
        arrayList.add(new BasicNameValuePair("verify", b.a("3DDDC1DC6A27A2EF649325B334109296", a8, "-", a5, a6, a7, a2, a3, a4)));
        return arrayList;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Result<NewComment> doInBackground(Void... voidArr) {
        try {
            URI c2 = bi.c(this.c);
            Result<NewComment> result = new Result<>();
            ArrayList arrayList = new ArrayList();
            String a2 = a.a(Application.getInstance());
            User a3 = d.a(Application.getInstance());
            String str = "";
            String str2 = "";
            String str3 = "";
            if (a3 != null) {
                com.sina.weibo.sdk.a.b a4 = com.qihoo360.daily.i.a.a(Application.getInstance());
                if (a4 != null) {
                    a2 = a4.b();
                }
                str = a3.getName();
                str2 = "weibo";
                str3 = a3.getAvatar_large();
            } else {
                WXUser userInfo4WX = WXInfoKeeper.getUserInfo4WX(Application.getInstance());
                if (userInfo4WX != null) {
                    a2 = userInfo4WX.getOpenid();
                    str = userInfo4WX.getNickname();
                    str2 = "weixin";
                    str3 = userInfo4WX.getHeadimgurl();
                    WXToken a5 = aw.a(WXInfoKeeper.getTokenInfo4WX(Application.getInstance()).getRefresh_token());
                    if (a5 == null || a5.getErrcode() != 0) {
                        result.setStatus(103);
                        return result;
                    }
                }
            }
            arrayList.addAll(a(this.d, this.f, a2, this.g, str2, str, str3, this.e));
            String a6 = com.qihoo360.daily.d.b.a(c2, arrayList, new Header[0]);
            if (TextUtils.isEmpty(a6)) {
                return null;
            }
            JSONObject jSONObject = new JSONObject(a6);
            if (jSONObject.getString("status").equals("109")) {
                result.setData(null);
                result.setStatus(109);
                return result;
            } else if (jSONObject.getString("status").equals("103")) {
                result.setStatus(103);
                return result;
            } else {
                Result<NewComment> result2 = (Result) Application.getGson().a(a6, new au(this).getType());
                b.a(result2);
                return result2;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void a(Result<NewComment> result) {
        super.onPostExecute(result);
        if (!this.c.isFinishing() && this.h != null && this.h.isShowing()) {
            this.h.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public /* synthetic */ void onPostExecute(Object obj) {
        a((Result<NewComment>) ((Result) obj));
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.c != null && !this.c.isFinishing()) {
            this.h = new AsyncProgressDialog(this.c);
            this.h.show();
        }
        super.onPreExecute();
    }
}
