package com.qihoo360.daily.g;

import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.wxapi.WXInfoKeeper;
import com.qihoo360.daily.wxapi.WXToken;
import org.apache.http.Header;

public class aw extends a<Void, Void, WXToken> {
    private String c;

    public static WXToken a(String str) {
        try {
            String a2 = b.a(bi.b(str), new Header[0]);
            WXToken wXToken = (WXToken) Application.getGson().a(a2, new ax().getType());
            if (wXToken != null && wXToken.getErrcode() == 0) {
                WXInfoKeeper.saveTokenInfo4WX(Application.getInstance(), a2);
                return wXToken;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public WXToken doInBackground(Void... voidArr) {
        return a(this.c);
    }
}
