package com.qihoo360.daily.g;

import android.content.Context;
import android.text.TextUtils;
import com.a.a.j;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.h.bl;
import com.qihoo360.daily.h.r;
import com.qihoo360.daily.model.Result;
import com.qihoo360.daily.model.Template;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.List;
import org.apache.http.Header;

public class bh extends a<Void, Void, Void> {
    private Context c;

    public bh(Context context) {
        this.c = context;
    }

    public static final String a(Context context, String str) {
        return context.getCacheDir() + File.separator + "template" + File.separator + str;
    }

    private boolean a(Template template) {
        File file;
        boolean z = false;
        if (template != null) {
            File file2 = null;
            try {
                InputStream a2 = b.a(template.getF_url());
                File file3 = new File(a(this.c, "download"));
                if (!file3.exists()) {
                    file3.mkdirs();
                }
                file = new File(file3, template.getF_name());
                try {
                    if (!file.exists()) {
                        file.createNewFile();
                    }
                    r.a(a2, file);
                    if (a.a(this.c, file.getPath())) {
                        String a3 = a(this.c, template.getT_name());
                        File file4 = new File(a3);
                        if (file4.exists()) {
                            r.b(file4);
                        }
                        z = bl.a(file, a3);
                        if (file != null && file.exists()) {
                            r.b(file);
                        }
                    } else if (file != null && file.exists()) {
                        r.b(file);
                    }
                } catch (Exception e) {
                    e = e;
                    file2 = file;
                    try {
                        e.printStackTrace();
                        if (file2 != null && file2.exists()) {
                            r.b(file2);
                        }
                        return z;
                    } catch (Throwable th) {
                        th = th;
                        file = file2;
                        r.b(file);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (file != null && file.exists()) {
                        r.b(file);
                    }
                    throw th;
                }
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                r.b(file2);
                return z;
            } catch (Throwable th3) {
                th = th3;
                file = null;
                r.b(file);
                throw th;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Void... voidArr) {
        List<Template> list;
        try {
            String a2 = b.a(bi.e(this.c), new Header[0]);
            j gson = Application.getGson();
            Type type = new bi(this).getType();
            Result result = (Result) gson.a(a2, type);
            if (!(result == null || result.getStatus() != 0 || (list = (List) result.getData()) == null || list.size() == 0)) {
                String a3 = d.a(this.c, "template");
                List list2 = !TextUtils.isEmpty(a3) ? (List) ((Result) gson.a(a3, type)).getData() : null;
                for (Template template : list) {
                    if (template == null || !"default_template".equals(template.getT_name()) || template.getT_code() > 10) {
                        if (list2 != null) {
                            Iterator it = list2.iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    break;
                                }
                                Template template2 = (Template) it.next();
                                if (!(template2 == null || template == null)) {
                                    String t_name = template.getT_name();
                                    String t_name2 = template2.getT_name();
                                    int t_code = template.getT_code();
                                    int t_code2 = template2.getT_code();
                                    boolean isLoaded = template2.isLoaded();
                                    if (t_name.equals(t_name2) && t_code == t_code2 && isLoaded) {
                                        if (new File(a(this.c, t_name), "index.html").exists()) {
                                            template.setLoaded(true);
                                        }
                                    }
                                }
                            }
                        }
                        if (!template.isLoaded() && a(template)) {
                            template.setLoaded(true);
                        }
                    } else {
                        template.setLoaded(true);
                    }
                }
                d.a(this.c, "template", gson.a(result));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
