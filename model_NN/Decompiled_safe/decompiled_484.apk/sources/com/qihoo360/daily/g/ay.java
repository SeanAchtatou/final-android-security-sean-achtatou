package com.qihoo360.daily.g;

import android.content.Context;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.NewsRelated;
import com.qihoo360.daily.model.Result;
import org.apache.http.Header;

public class ay extends a<Void, Void, Result<NewsRelated>> {
    private Context c;
    private String d;
    private String e;
    private int f;
    private int g;
    private int h;
    private String i;

    public ay(Context context, String str, String str2, int i2, int i3, int i4, String str3) {
        this.c = context;
        this.d = str;
        this.e = str2;
        this.f = i2;
        this.g = i3;
        this.h = i4;
        this.i = str3;
    }

    public ay(Context context, String str, String str2, int i2, int i3, String str3) {
        this.c = context;
        this.d = str;
        this.e = str2;
        this.f = i2;
        this.g = i3;
        this.i = str3;
    }

    private Result<NewsRelated> a(String str) {
        try {
            return (Result) Application.getGson().a(str, new az(this).getType());
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Result<NewsRelated> doInBackground(Void... voidArr) {
        try {
            String a2 = b.a(bi.a(this.c, this.d, this.e, this.f, this.g, this.h, this.i), new Header[0]);
            Result<NewsRelated> a3 = a(a2);
            String str = this.e;
            if (3 == this.f) {
                str = str + this.g;
            }
            if (a3 == null || a3.getStatus() != 0) {
                a2 = com.qihoo360.daily.h.b.a(str, ChannelType.TYPE_RELATED);
                a3 = a(a2);
            } else {
                com.qihoo360.daily.h.b.a(str, a2, ChannelType.TYPE_RELATED);
            }
            a3.setJson(a2);
            return a3;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
