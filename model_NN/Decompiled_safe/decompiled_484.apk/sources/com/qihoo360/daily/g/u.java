package com.qihoo360.daily.g;

import android.content.Context;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.Result;
import java.util.ArrayList;
import org.apache.http.Header;

public class u extends a<String, Void, Result<ArrayList<Info>>> {
    private Context c;

    public u(Context context) {
        this.c = context;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Result<ArrayList<Info>> doInBackground(String... strArr) {
        try {
            return (Result) Application.getGson().a(b.a(bi.g(this.c, (strArr == null || strArr.length < 1) ? null : strArr[0]), new Header[0]), new v(this).getType());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
