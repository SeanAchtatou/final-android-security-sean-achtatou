package com.qihoo360.daily.g;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.provider.MediaStore;
import com.e.b.al;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.c.m;
import java.io.File;
import java.io.FileOutputStream;

public class bb extends a<Void, Void, String> {
    private String c;

    public bb(String str) {
        this.c = str;
    }

    public static String a() {
        return Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + Environment.DIRECTORY_DCIM + File.separator + "daily" + File.separator;
    }

    public static String a(String str) {
        return m.b(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(Void... voidArr) {
        try {
            Bitmap c2 = al.a((Context) Application.getInstance()).a(this.c).c();
            if (c2 != null) {
                String a2 = a();
                File file = new File(a2);
                if (!file.exists()) {
                    file.mkdirs();
                }
                String a3 = a(this.c);
                String str = a2 + a3;
                File file2 = new File(str);
                if (file2.exists()) {
                    return str;
                }
                file2.createNewFile();
                if (!c2.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(str))) {
                    return str;
                }
                MediaStore.Images.Media.insertImage(Application.getInstance().getContentResolver(), c2, a3, a3);
                return str;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
