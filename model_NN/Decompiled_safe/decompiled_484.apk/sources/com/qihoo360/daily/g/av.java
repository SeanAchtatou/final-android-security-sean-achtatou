package com.qihoo360.daily.g;

import android.content.Context;
import com.qihoo.dynamic.DotCount;
import com.qihoo360.daily.R;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.h.ad;
import com.qihoo360.daily.h.bi;
import java.net.URI;
import org.apache.http.Header;

public class av extends a<Void, Void, Void> {
    private Context c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    private String k;

    public av(Context context, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8) {
        this.c = context;
        this.d = str;
        this.e = str2;
        this.f = str3;
        this.g = str4;
        this.h = str5;
        this.i = str6;
        this.j = str7;
        this.k = str8;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Void... voidArr) {
        try {
            URI a2 = bi.a(this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k);
            DotCount.setAppName(this.c.getString(R.string.dot_app_name));
            if (a2 != null) {
                String uri = a2.toString();
                ad.a("sendCount url:" + uri);
                ad.a("sendCount code:" + DotCount.sendCount(this.c, uri));
            }
            b.a(a2, new Header[0]);
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
