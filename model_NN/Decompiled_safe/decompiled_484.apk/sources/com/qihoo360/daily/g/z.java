package com.qihoo360.daily.g;

import android.content.Context;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.model.Result;
import com.qihoo360.daily.model.SpecialTopic;
import org.apache.http.Header;

public class z extends a<String, Void, Result<SpecialTopic>> {
    private Context c;
    private String d;
    private String e;

    public z(Context context, String str, String str2) {
        this.c = context;
        this.d = str;
        this.e = str2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Result<SpecialTopic> doInBackground(String... strArr) {
        try {
            return (Result) Application.getGson().a(b.a(bi.d(this.c, this.d, this.e), new Header[0]), new aa(this).getType());
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
