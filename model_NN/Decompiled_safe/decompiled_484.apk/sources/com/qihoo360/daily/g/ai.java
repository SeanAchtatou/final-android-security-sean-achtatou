package com.qihoo360.daily.g;

import com.a.a.j;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.wxapi.WXInfoKeeper;
import com.qihoo360.daily.wxapi.WXToken;
import com.qihoo360.daily.wxapi.WXUser;
import org.apache.http.Header;

public class ai extends a<Void, Void, WXUser> {
    private String c;

    public ai(String str) {
        this.c = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public WXUser doInBackground(Void... voidArr) {
        try {
            String a2 = b.a(bi.a(this.c), new Header[0]);
            j gson = Application.getGson();
            WXToken wXToken = (WXToken) gson.a(a2, new aj(this).getType());
            if (wXToken != null && wXToken.getErrcode() == 0) {
                WXInfoKeeper.saveTokenInfo4WX(Application.getInstance(), a2);
                String a3 = b.a(bi.b(wXToken.getAccess_token(), wXToken.getOpenid()), new Header[0]);
                WXUser wXUser = (WXUser) gson.a(a3, new ak(this).getType());
                if (wXUser == null || wXUser.getErrcode() != 0) {
                    return wXUser;
                }
                WXInfoKeeper.saveUserInfo4WX(Application.getInstance(), a3);
                return wXUser;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
