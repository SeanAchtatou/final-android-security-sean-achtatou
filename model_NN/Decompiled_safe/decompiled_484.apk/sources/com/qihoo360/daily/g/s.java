package com.qihoo360.daily.g;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import com.a.a.j;
import com.mediav.ads.sdk.adcore.Config;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.activity.SendCmtActivity;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.h.l;
import com.qihoo360.daily.model.NewsDetail;
import com.qihoo360.daily.model.ResponseBean;
import java.lang.reflect.Type;
import java.net.URI;
import org.apache.http.Header;
import org.json.JSONObject;

public class s extends a<Void, ResponseBean<NewsDetail>, ResponseBean<NewsDetail>> {
    private Context c;
    private String d;
    private String e;
    private String f;
    private String g;
    private boolean h;
    private String i;
    private String j;

    public s(Context context, String str, String str2, String str3, String str4, boolean z, String str5, String str6) {
        this.c = context;
        this.d = str;
        this.e = str2;
        this.f = str3;
        this.g = str4;
        this.h = z;
        this.i = str5;
        this.j = str6;
    }

    private ResponseBean<NewsDetail> a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            j gson = Application.getGson();
            Type type = new t(this).getType();
            ResponseBean responseBean = (ResponseBean) gson.a(str, type);
            return (ResponseBean) gson.a(str, type);
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private void a(ResponseBean<NewsDetail> responseBean, String str) {
        if (responseBean != null && !TextUtils.isEmpty(str)) {
            try {
                responseBean.setJson(new JSONObject(str).getJSONObject(SendCmtActivity.TAG_DATA).toString());
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ResponseBean<NewsDetail> doInBackground(Void... voidArr) {
        ResponseBean<NewsDetail> responseBean;
        Exception e2;
        try {
            String a2 = b.a(this.d, "detail");
            if (!TextUtils.isEmpty(a2)) {
                a2 = new String(Base64.decode(a2.getBytes(), 0));
            }
            ResponseBean<NewsDetail> a3 = a(a2);
            if (a3 != null) {
                try {
                    if (a3.getErrno() == 0) {
                        a(a3, a2);
                        a(a3);
                        return a3;
                    }
                } catch (Exception e3) {
                    e2 = e3;
                    responseBean = a3;
                    a(responseBean);
                    e2.printStackTrace();
                    return responseBean;
                }
            }
            String str = this.h ? Config.CHANNEL_ID : null;
            long currentTimeMillis = System.currentTimeMillis();
            URI a4 = bi.a(this.c, this.e, this.f, this.d, str, this.i, this.g, this.j);
            String a5 = com.qihoo360.daily.d.b.a(a4, new Header[0]);
            if (Application.DEBUG_SWITCHER) {
                l.a(System.currentTimeMillis() - currentTimeMillis, a4.toString(), a5);
            }
            responseBean = a(a5);
            try {
                a(responseBean, a5);
                a(responseBean);
                if (responseBean == null || responseBean.getErrno() != 0 || a5 == null) {
                    return responseBean;
                }
                b.a(this.d, new String(Base64.encode(a5.getBytes(), 0)), "detail");
                return responseBean;
            } catch (Exception e4) {
                e2 = e4;
                a(responseBean);
                e2.printStackTrace();
                return responseBean;
            }
        } catch (Exception e5) {
            Exception exc = e5;
            responseBean = null;
            e2 = exc;
            a(responseBean);
            e2.printStackTrace();
            return responseBean;
        }
    }
}
