package com.qihoo360.daily.g;

import android.content.Context;
import android.text.TextUtils;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.h.av;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.h.l;
import com.qihoo360.daily.i.a;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.Result;
import com.sina.weibo.sdk.a.b;
import java.net.URI;
import java.util.List;
import org.apache.http.message.BasicHeader;

public class ap extends a<String, Result<List<Info>>, Result<List<Info>>> {
    private Context c;
    private int d;
    private int e;

    public ap(Context context, int i, int i2) {
        this.c = context;
        this.d = i;
        this.e = i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qihoo360.daily.h.bi.a(android.content.Context, java.lang.String, java.lang.String, long, long, int, java.lang.String):java.net.URI
     arg types: [android.content.Context, java.lang.String, ?[OBJECT, ARRAY], int, int, int, java.lang.String]
     candidates:
      com.qihoo360.daily.h.bi.a(android.content.Context, java.lang.String, java.lang.String, int, int, int, java.lang.String):java.net.URI
      com.qihoo360.daily.h.bi.a(android.content.Context, java.lang.String, java.lang.String, long, long, int, java.lang.String):java.net.URI */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Result<List<Info>> doInBackground(String... strArr) {
        Exception e2;
        Result<List<Info>> result;
        Result<List<Info>> result2 = new Result<>();
        String str = strArr[0];
        try {
            long currentTimeMillis = System.currentTimeMillis();
            URI a2 = bi.a(this.c, str, (String) null, 0L, 0L, this.d, this.e == 0 ? "first" : "more");
            BasicHeader basicHeader = null;
            b a3 = a.a(this.c);
            if (a3 != null && !TextUtils.isEmpty(a3.b())) {
                basicHeader = new BasicHeader("WBTOKEN", a3.c());
            }
            String a4 = com.qihoo360.daily.d.b.a(a2, basicHeader);
            if (Application.DEBUG_SWITCHER) {
                l.a(System.currentTimeMillis() - currentTimeMillis, a2.toString(), a4);
            }
            result = (Result) Application.getGson().a(a4, new aq(this).getType());
            if (result != null) {
                try {
                    if (result.getStatus() == 0) {
                        List<Info> data = result.getData();
                        if (data != null) {
                            long i = d.i(this.c, str);
                            long currentTimeMillis2 = System.currentTimeMillis();
                            long j = currentTimeMillis2 - i;
                            if (j > 3600000) {
                                j = 3600000;
                            }
                            for (Info info : data) {
                                if (info != null) {
                                    currentTimeMillis2 = (long) ((((double) currentTimeMillis2) - (((Math.random() * ((double) j)) * 2.0d) / ((double) this.d))) - 1000.0d);
                                    String b2 = com.qihoo360.daily.h.b.b(currentTimeMillis2);
                                    info.setTimeOrder(currentTimeMillis2);
                                    info.setChannelId(str);
                                    info.setPdate(b2);
                                }
                            }
                            if (ChannelType.TYPE_CHANNEL_RECOMMEND.equals(str)) {
                                result.setData(av.a(data));
                            }
                            a(result);
                            if (!TextUtils.isEmpty(str)) {
                                new com.qihoo360.daily.b.d(this.c).a(data);
                            }
                        } else {
                            a(result);
                        }
                        d.a(this.c, System.currentTimeMillis(), str);
                        return result;
                    }
                } catch (Exception e3) {
                    e2 = e3;
                    e2.printStackTrace();
                    a(result);
                    return result;
                }
            }
            a(result);
        } catch (Exception e4) {
            e2 = e4;
            result = result2;
            e2.printStackTrace();
            a(result);
            return result;
        }
        return result;
    }
}
