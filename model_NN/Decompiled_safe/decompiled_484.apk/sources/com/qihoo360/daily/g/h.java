package com.qihoo360.daily.g;

import android.text.TextUtils;
import com.mediav.ads.sdk.adcore.Config;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.h.a;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.model.Result;
import com.qihoo360.daily.wxapi.WXInfoKeeper;
import com.qihoo360.daily.wxapi.WXToken;
import com.sina.weibo.sdk.a.b;
import java.util.ArrayList;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;

public class h extends a<Object, Void, Result<Object>> {
    private k c;
    private String d;

    public h(k kVar, String... strArr) {
        this.c = kVar;
        if (strArr == null) {
            return;
        }
        if (strArr.length == 1) {
            this.d = strArr[0];
            return;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < strArr.length - 1; i++) {
            stringBuffer.append(strArr[i]).append(",");
        }
        stringBuffer.append(strArr[strArr.length - 1]);
        this.d = stringBuffer.toString();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Result<Object> doInBackground(Object... objArr) {
        String str;
        BasicHeader basicHeader;
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("mo", "news"));
            arrayList.add(new BasicNameValuePair("ro", "collect"));
            arrayList.add(new BasicNameValuePair("nids", this.d));
            switch (j.f1080a[this.c.ordinal()]) {
                case 1:
                    str = Config.CHANNEL_ID;
                    break;
                case 2:
                    str = "2";
                    break;
                default:
                    str = "";
                    break;
            }
            String a2 = a.a(Application.getInstance());
            arrayList.add(new BasicNameValuePair("token", a2));
            b a3 = com.qihoo360.daily.i.a.a(Application.getInstance());
            String str2 = "0";
            Result<Object> result = new Result<>();
            if (a3 == null || TextUtils.isEmpty(a3.b())) {
                WXToken tokenInfo4WX = WXInfoKeeper.getTokenInfo4WX(Application.getInstance());
                if (tokenInfo4WX != null) {
                    str2 = "6";
                    a2 = tokenInfo4WX.getOpenid();
                    WXToken a4 = aw.a(tokenInfo4WX.getRefresh_token());
                    if (a4 == null || a4.getErrcode() != 0) {
                        result.setStatus(103);
                        return result;
                    }
                    basicHeader = new BasicHeader("WXTOKEN", a4.getAccess_token());
                } else {
                    basicHeader = null;
                }
            } else {
                str2 = Config.CHANNEL_ID;
                a2 = a3.b();
                basicHeader = new BasicHeader("WBTOKEN", a3.c());
            }
            arrayList.add(new BasicNameValuePair("uid", a2));
            arrayList.add(new BasicNameValuePair("tp", str2));
            arrayList.add(new BasicNameValuePair("s_code", com.qihoo360.daily.h.b.b(a2 + this.d + "_4!e@a#2$6^b&9*6_")));
            arrayList.add(new BasicNameValuePair("op", str));
            arrayList.add(new BasicNameValuePair("_cv", a.b(Application.getInstance())));
            String a5 = com.qihoo360.daily.d.b.a(bi.a(arrayList), basicHeader);
            if (!TextUtils.isEmpty(a5)) {
                return (Result) Application.getGson().a(a5, new i(this).getType());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
