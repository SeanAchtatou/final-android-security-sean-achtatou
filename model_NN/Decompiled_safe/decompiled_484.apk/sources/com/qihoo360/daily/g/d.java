package com.qihoo360.daily.g;

import com.qihoo360.daily.h.r;
import java.io.File;

public class d extends a<File, Void, Long> {
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Long doInBackground(File... fileArr) {
        long j = 0;
        if (fileArr != null) {
            if (fileArr.length == 1) {
                return Long.valueOf(r.a(fileArr[0]));
            }
            if (fileArr.length > 1) {
                for (File a2 : fileArr) {
                    j += r.a(a2);
                }
                return Long.valueOf(j);
            }
        }
        return 0L;
    }
}
