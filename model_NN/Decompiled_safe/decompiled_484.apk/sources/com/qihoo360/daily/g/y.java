package com.qihoo360.daily.g;

import android.content.Context;
import android.text.TextUtils;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.h.l;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.City;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.Result;
import java.net.URI;
import java.util.Iterator;
import java.util.List;
import org.apache.http.Header;

public class y extends a<Void, Result<List<Info>>, Result<List<Info>>> {
    private Context c;
    private String d;
    private String e;
    private List<Info> f;
    private int g;

    public y(Context context, String str, String str2, List<Info> list, int i) {
        this.c = context;
        this.d = str;
        this.e = str2;
        this.f = list;
        this.g = i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Result<List<Info>> doInBackground(Void... voidArr) {
        Exception exc;
        Result<List<Info>> result;
        URI a2;
        if (this.c == null || this.e == null) {
            a((Object) null);
            return null;
        }
        try {
            long currentTimeMillis = System.currentTimeMillis();
            if (ChannelType.TYPE_CHANNEL_LOCAL.equals(this.e)) {
                City q = d.q(this.c);
                a2 = bi.a(this.c, this.d, this.e, this.g, q.getCitySpell(), q.getProvinceSpell());
            } else {
                a2 = bi.a(this.c, this.d, this.e, this.g);
            }
            String a3 = b.a(a2, new Header[0]);
            if (Application.DEBUG_SWITCHER) {
                l.a(System.currentTimeMillis() - currentTimeMillis, a2.toString(), a3);
            }
            Result<List<Info>> e2 = com.qihoo360.daily.h.b.e(a3);
            try {
                String str = "channel_news_" + this.e;
                if (e2 == null || e2.getStatus() != 0) {
                    a(e2);
                } else {
                    List<Info> data = e2.getData();
                    if (!(this.f == null || data == null)) {
                        for (Info info : data) {
                            String url = info.getUrl();
                            Iterator<Info> it = this.f.iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    break;
                                }
                                Info next = it.next();
                                String url2 = next.getUrl();
                                if (!TextUtils.isEmpty(url2) && url2.equals(url)) {
                                    info.setDigg_type(next.getDigg_type());
                                    info.setRead(next.getRead());
                                    break;
                                }
                            }
                        }
                    }
                    a(e2);
                    if ("0".equals(this.d)) {
                        d.a(this.c, str, Application.getGson().a(data));
                    }
                    d.a(this.c, System.currentTimeMillis(), this.e);
                }
                return e2;
            } catch (Exception e3) {
                exc = e3;
                result = e2;
                exc.printStackTrace();
                a(result);
                return result;
            }
        } catch (Exception e4) {
            exc = e4;
            result = null;
        }
    }
}
