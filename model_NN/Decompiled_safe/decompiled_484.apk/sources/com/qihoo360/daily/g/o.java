package com.qihoo360.daily.g;

import android.content.Context;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.model.FavouriteInfo;
import com.qihoo360.daily.model.Result;
import java.util.ArrayList;
import org.apache.http.Header;

public class o extends a<String, Void, Result<ArrayList<FavouriteInfo>>> {
    private Context c;

    public o(Context context) {
        this.c = context;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Result<ArrayList<FavouriteInfo>> doInBackground(String... strArr) {
        try {
            return (Result) Application.getGson().a(b.a(bi.c(this.c, (strArr == null || strArr.length < 1) ? null : strArr[0]), new Header[0]), new p(this).getType());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
