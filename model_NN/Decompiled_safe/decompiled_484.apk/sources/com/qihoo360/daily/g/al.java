package com.qihoo360.daily.g;

import android.content.Context;
import android.text.TextUtils;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.model.Tianqi;
import org.apache.http.Header;
import org.json.JSONObject;

public class al extends a<Void, Void, Tianqi> {
    String c = "GetWeatherTask";
    private Context d;
    private String e;

    public al(Context context, String str) {
        this.d = context;
        this.e = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Tianqi doInBackground(Void... voidArr) {
        try {
            String a2 = b.a(bi.f(this.d, this.e), new Header[0]);
            if (!TextUtils.isEmpty(a2)) {
                Tianqi tianqi = (Tianqi) Application.getGson().a(a2, new am(this).getType());
                JSONObject optJSONObject = new JSONObject(a2).optJSONObject("pm25");
                if (optJSONObject != null) {
                    tianqi.setPm(optJSONObject.getJSONArray("pm25").optString(0));
                }
                if (TextUtils.isEmpty(this.e)) {
                    return tianqi;
                }
                d.f(this.d, System.currentTimeMillis());
                d.a(this.d, this.e, Application.toJson(tianqi));
                return tianqi;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }
}
