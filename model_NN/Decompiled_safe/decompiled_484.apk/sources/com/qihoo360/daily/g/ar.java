package com.qihoo360.daily.g;

import android.content.Context;
import android.text.TextUtils;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.model.NewComment;
import com.qihoo360.daily.model.NewCommentResponse;
import com.qihoo360.daily.model.Result;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.Header;

public class ar extends a<Void, Void, Result<NewCommentResponse>> {
    private Context c;
    private String d;
    private String e;

    public ar(Context context, String str, String str2) {
        this.c = context;
        this.d = str;
        this.e = str2;
    }

    private Result<NewCommentResponse> a(String str) {
        try {
            Result<NewCommentResponse> result = (Result) Application.getGson().a(str, new as(this).getType());
            b.a(result);
            NewCommentResponse data = result.getData();
            List<NewComment> hot_comments = data.getHot_comments();
            List<NewComment> comments = data.getComments();
            List<NewComment> chain_message = data.getChain_message();
            a(hot_comments, chain_message);
            a(comments, chain_message);
            return result;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private void a(List<NewComment> list, List<NewComment> list2) {
        if (list != null && list.size() != 0 && list2 != null && list2.size() != 0) {
            for (NewComment next : list) {
                List<String> chain = next.getChain();
                ArrayList arrayList = new ArrayList();
                if (!(chain == null || chain.size() == 0)) {
                    for (String next2 : chain) {
                        if (!TextUtils.isEmpty(next2)) {
                            for (NewComment next3 : list2) {
                                if (next3 != null && next2.equals(next3.getComment_id())) {
                                    arrayList.add(next3);
                                }
                            }
                        }
                    }
                    next.setChain_comments(arrayList);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Result<NewCommentResponse> doInBackground(Void... voidArr) {
        try {
            URI a2 = bi.a(this.c, this.d, this.e);
            String uri = a2.toString();
            String a3 = com.qihoo360.daily.d.b.a(a2, new Header[0]);
            Result<NewCommentResponse> a4 = a(a3);
            if (a4 == null || a4.getStatus() != 0) {
                return a(b.a(uri, "cmt"));
            }
            b.a(uri, a3, "cmt");
            return a4;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
