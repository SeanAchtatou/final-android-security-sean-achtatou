package com.qihoo360.daily.g;

import android.content.Context;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.model.Suggest;
import org.apache.http.Header;

public class ad extends a<Void, Void, Suggest> {
    private Context c;
    private String d;

    public ad(Context context, String str) {
        this.c = context;
        this.d = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Suggest doInBackground(Void... voidArr) {
        try {
            String a2 = b.a(bi.b(this.c, this.d), new Header[0]);
            Suggest suggest = (Suggest) Application.getGson().a(a2.substring(a2.indexOf("(") + 1, a2.indexOf(")")), new ae(this).getType());
            if (suggest != null) {
                return suggest;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
