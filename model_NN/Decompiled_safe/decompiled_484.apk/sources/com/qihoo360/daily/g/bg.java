package com.qihoo360.daily.g;

import android.content.Context;
import com.qihoo.dynamic.DotCount;
import com.qihoo360.daily.R;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.h.ad;
import com.qihoo360.daily.h.bi;
import java.io.IOException;
import java.net.URI;
import org.apache.http.Header;

public class bg extends a<Void, Void, Void> {
    private Context c;
    private long d;

    public bg(Context context, long j) {
        this.c = context;
        this.d = j;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Void... voidArr) {
        URI d2 = bi.d(this.c, String.valueOf(this.d));
        DotCount.setAppName(this.c.getString(R.string.dot_app_name));
        if (d2 != null) {
            String uri = d2.toString();
            ad.a("sendCount url:" + uri);
            ad.a("sendCount code:" + DotCount.sendCount(this.c, uri));
        }
        String str = "";
        try {
            str = b.a(d2, new Header[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ad.a("Tongji: " + str);
        return null;
    }
}
