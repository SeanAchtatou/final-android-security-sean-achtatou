package com.qihoo360.daily.g;

import android.content.Context;
import android.text.TextUtils;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.c.m;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.h.b;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.model.QdData;
import com.qihoo360.daily.model.Result;
import com.qihoo360.daily.model.Splash;
import java.io.IOException;
import org.apache.http.Header;

public class ab extends a<Void, Void, Result<QdData>> {
    private Context c;

    public ab(Context context) {
        this.c = context;
    }

    private void a(String str, boolean z) {
        if (!z && b.a()) {
            byte[] bArr = null;
            try {
                bArr = com.qihoo360.daily.d.b.b(str);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (bArr != null) {
                m.a(str, bArr);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Result<QdData> doInBackground(Void... voidArr) {
        try {
            String a2 = com.qihoo360.daily.d.b.a(bi.f(this.c), new Header[0]);
            if (TextUtils.isEmpty(a2)) {
                return null;
            }
            Result<QdData> result = (Result) Application.getGson().a(a2, new ac(this).getType());
            if (result != null) {
                QdData data = result.getData();
                if (data != null) {
                    d.a(this.c, "ad_JX_Enable", data.getAd_JX_Enable());
                }
                if (data == null || data.getQd() == null) {
                    return result;
                }
                QdData r = d.r(this.c);
                Splash qd = result.getData().getQd();
                String imgurl = (r == null || r.getQd() == null) ? null : r.getQd().getImgurl();
                String imgurl2 = qd.getImgurl();
                if (!TextUtils.isEmpty(imgurl2)) {
                    boolean a3 = b.a(this.c);
                    if (!imgurl2.equals(imgurl)) {
                        a(imgurl2, a3);
                    } else if (!m.d(imgurl2)) {
                        a(imgurl2, a3);
                    }
                }
                d.a(this.c, result.getData());
                return result;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
