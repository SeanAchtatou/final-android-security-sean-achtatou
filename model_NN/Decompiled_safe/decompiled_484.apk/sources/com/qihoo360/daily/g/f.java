package com.qihoo360.daily.g;

import android.content.Context;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.h.ad;
import com.qihoo360.daily.h.bi;
import org.apache.http.Header;

public class f extends a<Object, Object, String> {
    private static final String c = f.class.getSimpleName();
    private Context d;
    private String e;
    private String f;

    public f(Context context, String str, String str2) {
        this.d = context;
        this.f = str;
        this.e = str2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(Object... objArr) {
        try {
            String a2 = b.a(bi.b(this.d, this.e, this.f), new Header[0]);
            ad.a(a2, c);
            return a2;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
