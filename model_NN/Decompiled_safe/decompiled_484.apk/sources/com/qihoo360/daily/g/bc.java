package com.qihoo360.daily.g;

import android.content.Context;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.model.Result;
import java.net.URI;
import java.util.ArrayList;
import org.apache.http.Header;
import org.apache.http.message.BasicNameValuePair;

public class bc extends a<Void, Void, Result<String>> {
    private Context c;
    private String d;

    public bc(Context context, String str) {
        this.c = context;
        this.d = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Result<String> doInBackground(Void... voidArr) {
        try {
            URI b2 = bi.b(this.c);
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("log", this.d));
            b.a(b2, arrayList, new Header[0]);
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
