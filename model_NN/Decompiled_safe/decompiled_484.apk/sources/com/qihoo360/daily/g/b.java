package com.qihoo360.daily.g;

import android.content.Context;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.model.UpdateInfo;
import org.apache.http.Header;

public class b extends a<Object, Void, UpdateInfo> {
    private Context c;

    public b(Context context) {
        this.c = context;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public UpdateInfo doInBackground(Object... objArr) {
        try {
            return (UpdateInfo) Application.getGson().a(com.qihoo360.daily.d.b.a(bi.d(this.c), new Header[0]), new c(this).getType());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
