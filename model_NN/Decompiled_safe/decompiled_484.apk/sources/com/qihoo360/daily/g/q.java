package com.qihoo360.daily.g;

import android.content.Context;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.model.HotNews;
import java.util.List;
import org.apache.http.Header;

public class q extends a<Void, Void, List<HotNews>> {
    private Context c;

    public q(Context context) {
        this.c = context;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public List<HotNews> doInBackground(Void... voidArr) {
        try {
            return (List) Application.getGson().a(b.a(bi.a(this.c, "BC076E39784D7B27"), new Header[0]), new r(this).getType());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
