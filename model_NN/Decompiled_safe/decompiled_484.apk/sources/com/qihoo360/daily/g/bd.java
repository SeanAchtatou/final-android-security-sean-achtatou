package com.qihoo360.daily.g;

import android.app.Activity;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.model.Result;
import java.net.URI;
import java.util.ArrayList;
import org.apache.http.Header;
import org.apache.http.message.BasicNameValuePair;

public class bd extends a<Void, Void, Result> {
    private Activity c;
    private String d;
    private String e;
    private int f;

    public bd(Activity activity, String str, String str2) {
        this.c = activity;
        this.d = str;
        this.e = str2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Result doInBackground(Void... voidArr) {
        try {
            URI a2 = bi.a(this.c);
            ArrayList arrayList = new ArrayList();
            if (this.f != 0) {
                arrayList.add(new BasicNameValuePair("good", String.valueOf(this.f)));
            } else {
                arrayList.add(new BasicNameValuePair("content", this.d));
                arrayList.add(new BasicNameValuePair("mail", this.e));
            }
            return (Result) Application.getGson().a(b.a(a2, arrayList, new Header[0]), Result.class);
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
