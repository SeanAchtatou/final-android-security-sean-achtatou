package com.qihoo360.daily.g;

import android.content.Context;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.h.bi;
import org.apache.http.Header;

public class ba extends a<Object, Object, String> {
    private Context c;
    private String d;
    private String e;

    public ba(Context context, String str, String str2) {
        this.c = context;
        this.d = str;
        this.e = str2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(Object... objArr) {
        try {
            return b.a(bi.c(this.c, this.d, this.e), new Header[0]);
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
