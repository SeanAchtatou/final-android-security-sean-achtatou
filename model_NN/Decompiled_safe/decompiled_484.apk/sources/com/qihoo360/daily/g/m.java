package com.qihoo360.daily.g;

import android.content.Context;
import android.text.TextUtils;
import com.a.a.t;
import com.a.a.w;
import com.a.a.y;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.activity.SendCmtActivity;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.f.d;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.h.l;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.Result;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.Header;

public class m extends a<String, Result<List<Info>>, Result<List<Info>>> {
    private Context c;
    private String d;
    private List<Info> e;

    public m(Context context, String str) {
        this.c = context;
        this.d = str;
    }

    public m(Context context, String str, List<Info> list) {
        this.c = context;
        this.d = str;
        this.e = list;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.j.a(com.a.a.t, java.lang.reflect.Type):T
     arg types: [com.a.a.r, java.lang.reflect.Type]
     candidates:
      com.a.a.j.a(com.a.a.j, double):void
      com.a.a.j.a(java.lang.Object, com.a.a.d.a):void
      com.a.a.j.a(com.a.a.ag, com.a.a.c.a):com.a.a.af<T>
      com.a.a.j.a(com.a.a.d.a, java.lang.reflect.Type):T
      com.a.a.j.a(java.io.Reader, java.lang.reflect.Type):T
      com.a.a.j.a(java.lang.String, java.lang.Class):T
      com.a.a.j.a(java.lang.String, java.lang.reflect.Type):T
      com.a.a.j.a(java.lang.Object, java.lang.reflect.Type):java.lang.String
      com.a.a.j.a(com.a.a.t, com.a.a.d.d):void
      com.a.a.j.a(com.a.a.t, java.lang.Appendable):void
      com.a.a.j.a(com.a.a.t, java.lang.reflect.Type):T */
    public static Result<List<Info>> a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        Result<List<Info>> result = new Result<>();
        ArrayList arrayList = new ArrayList();
        result.setData(arrayList);
        w k = new y().a(str).k();
        int e2 = k.a("status").e();
        result.setStatus(e2);
        if (e2 == 0) {
            List list = (List) Application.getGson().a((t) k.b(SendCmtActivity.TAG_DATA), new n().getType());
            if (list == null) {
                return result;
            }
            for (int i = 0; i < list.size(); i++) {
                Info info = (Info) list.get(i);
                arrayList.add(info);
                if (info.getV_t() == 11) {
                    info.setV_t(-6);
                }
                if (info.getExtnews() != null && info.getExtnews().size() > 0) {
                    for (int i2 = 0; i2 < info.getExtnews().size(); i2++) {
                        Info info2 = info.getExtnews().get(i2);
                        info2.setC_pos(info.getC_pos());
                        info2.setSubNews(true);
                        arrayList.add(info2);
                        if (info2.getV_t() == 11) {
                            info2.setV_t(-7);
                        }
                        if (i2 == info.getExtnews().size() - 1) {
                            if (!TextUtils.isEmpty(info.getAndroid_channeltarget())) {
                                Info info3 = new Info();
                                info3.setV_t(-5);
                                info3.setAndroid_channeltarget(info.getAndroid_channeltarget());
                                info3.setAndroid_channeltargetname(info.getAndroid_channeltargetname());
                                info3.setC_pos(info.getC_pos());
                                arrayList.add(info3);
                            } else {
                                info2.setCardTail(true);
                            }
                        }
                    }
                    info.setExtnews(null);
                    info.setAndroid_channeltarget(null);
                    info.setAndroid_channeltargetname(null);
                } else if (!(TextUtils.isEmpty(info.getAndroid_channeltarget()) || info.getV_t() == -1 || info.getV_t() == -2 || info.getV_t() == -4 || info.getV_t() == -5 || info.isSeekMoreAttached())) {
                    info.setSeekMoreAttached(true);
                    Info info4 = new Info();
                    info4.setV_t(-5);
                    info4.setAndroid_channeltarget(info.getAndroid_channeltarget());
                    info4.setAndroid_channeltargetname(info.getAndroid_channeltargetname());
                    info4.setC_pos(info.getC_pos());
                    arrayList.add(info4);
                }
            }
        }
        return result;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0068 A[EDGE_INSN: B:38:0x0068->B:36:0x0068 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.util.List<com.qihoo360.daily.model.Info> r13, java.util.List<com.qihoo360.daily.model.Info> r14) {
        /*
            r12 = this;
            if (r13 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            if (r14 == 0) goto L_0x0002
            int r0 = r14.size()
            if (r0 <= 0) goto L_0x0002
            int r4 = r14.size()
            java.util.Iterator r5 = r13.iterator()
        L_0x0013:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0002
            java.lang.Object r0 = r5.next()
            r2 = r0
            com.qihoo360.daily.model.Info r2 = (com.qihoo360.daily.model.Info) r2
            java.lang.String r6 = r2.getNid()
            r0 = 0
            r3 = r0
        L_0x0026:
            if (r3 >= r4) goto L_0x0013
            java.lang.Object r0 = r14.get(r3)
            com.qihoo360.daily.model.Info r0 = (com.qihoo360.daily.model.Info) r0
            boolean r1 = com.qihoo360.daily.h.ax.a(r6)
            if (r1 != 0) goto L_0x003e
            java.lang.String r1 = r0.getNid()
            boolean r1 = r6.equals(r1)
            if (r1 == 0) goto L_0x004c
        L_0x003e:
            int r1 = r0.getRead()
            r2.setRead(r1)
            int r1 = r0.getDigg_type()
            r2.setDigg_type(r1)
        L_0x004c:
            java.util.List r1 = r2.getExtnews()
            java.util.List r7 = r0.getExtnews()
            if (r1 == 0) goto L_0x00a1
            int r0 = r1.size()
            if (r0 <= 0) goto L_0x00a1
            if (r7 == 0) goto L_0x00a1
            int r0 = r7.size()
            if (r0 <= 0) goto L_0x00a1
            java.util.Iterator r8 = r1.iterator()
        L_0x0068:
            boolean r0 = r8.hasNext()
            if (r0 == 0) goto L_0x00a1
            java.lang.Object r0 = r8.next()
            r1 = r0
            com.qihoo360.daily.model.Info r1 = (com.qihoo360.daily.model.Info) r1
            java.lang.String r9 = r1.getNid()
            java.util.Iterator r10 = r7.iterator()
        L_0x007d:
            boolean r0 = r10.hasNext()
            if (r0 == 0) goto L_0x0068
            java.lang.Object r0 = r10.next()
            com.qihoo360.daily.model.Info r0 = (com.qihoo360.daily.model.Info) r0
            boolean r11 = com.qihoo360.daily.h.ax.a(r9)
            if (r11 != 0) goto L_0x0099
            java.lang.String r11 = r0.getNid()
            boolean r11 = r9.equals(r11)
            if (r11 == 0) goto L_0x007d
        L_0x0099:
            int r0 = r0.getRead()
            r1.setRead(r0)
            goto L_0x0068
        L_0x00a1:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.g.m.a(java.util.List, java.util.List):void");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Result<List<Info>> doInBackground(String... strArr) {
        Result<List<Info>> result;
        Exception exc;
        try {
            long currentTimeMillis = System.currentTimeMillis();
            URI a2 = bi.a((String) null, this.d, this.c);
            String a3 = b.a(a2, new Header[0]);
            if (Application.DEBUG_SWITCHER) {
                l.a(System.currentTimeMillis() - currentTimeMillis, a2.toString(), a3);
            }
            Result<List<Info>> a4 = a(a3);
            if (a4 != null) {
                try {
                    if (a4.getStatus() == 0) {
                        if (TextUtils.isEmpty(this.d)) {
                            a(a4.getData(), this.e);
                            a(a4);
                            d.a(this.c, System.currentTimeMillis(), "daily");
                        } else {
                            a(a4);
                        }
                        return a4;
                    }
                } catch (Exception e2) {
                    Exception exc2 = e2;
                    result = a4;
                    exc = exc2;
                }
            }
            a(a4);
            return a4;
        } catch (Exception e3) {
            Exception exc3 = e3;
            result = null;
            exc = exc3;
            exc.printStackTrace();
            a(result);
            return result;
        }
    }
}
