package com.qihoo360.daily.g;

import android.content.Context;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.d.b;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.model.Result;
import com.qihoo360.daily.model.Url;
import org.apache.http.Header;

public class w extends a<Void, Void, Result<Url>> {
    private static final String c = w.class.getSimpleName();
    private String d;
    private Context e;

    public w(String str, Context context) {
        this.d = str;
        this.e = context;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Result<Url> doInBackground(Void... voidArr) {
        try {
            return (Result) Application.getGson().a(b.a(bi.a(this.d, this.e), new Header[0]), new x(this).getType());
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
