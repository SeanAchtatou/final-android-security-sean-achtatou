package com.qihoo360.daily.g;

import android.content.Context;
import android.text.TextUtils;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.b.d;
import com.qihoo360.daily.h.av;
import com.qihoo360.daily.h.bi;
import com.qihoo360.daily.h.l;
import com.qihoo360.daily.i.a;
import com.qihoo360.daily.model.ChannelType;
import com.qihoo360.daily.model.Info;
import com.qihoo360.daily.model.Result;
import com.sina.weibo.sdk.a.b;
import java.net.URI;
import java.util.List;
import org.apache.http.message.BasicHeader;

public class an extends a<String, Result<List<Info>>, Result<List<Info>>> {
    private Context c;
    private String d;
    private long e;

    public an(Context context, String str, long j) {
        this.c = context;
        this.d = str;
        this.e = j;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Result<List<Info>> doInBackground(String... strArr) {
        Exception e2;
        Result<List<Info>> result;
        List<Info> list;
        Result<List<Info>> result2 = new Result<>();
        try {
            long j = this.e;
            String str = strArr[0];
            List a2 = new d(this.c).a(Info.class, "channelId", str, 0, 20, "timeOrder", j, j - 7200000);
            result2.setData(a2);
            if (a2 == null || a2.size() == 0) {
                long currentTimeMillis = System.currentTimeMillis();
                URI a3 = bi.a(this.c, str, this.d, j - 14400000, this.e, 20, "history");
                BasicHeader basicHeader = null;
                b a4 = a.a(this.c);
                if (a4 != null && !TextUtils.isEmpty(a4.b())) {
                    basicHeader = new BasicHeader("WBTOKEN", a4.c());
                }
                String a5 = com.qihoo360.daily.d.b.a(a3, basicHeader);
                if (Application.DEBUG_SWITCHER) {
                    l.a(System.currentTimeMillis() - currentTimeMillis, a3.toString(), a5);
                }
                result = (Result) Application.getGson().a(a5, new ao(this).getType());
                if (result != null) {
                    try {
                        list = result.getData();
                    } catch (Exception e3) {
                        e2 = e3;
                        e2.printStackTrace();
                        a(result);
                        return result;
                    }
                } else {
                    list = null;
                }
                if (list != null && list.size() > 0) {
                    long j2 = this.e;
                    for (Info info : list) {
                        if (info != null) {
                            j2 = (long) ((((double) j2) - ((Math.random() * ((double) 7200000)) / ((double) 20))) - 1000.0d);
                            String b2 = com.qihoo360.daily.h.b.b(j2);
                            info.setTimeOrder(j2);
                            info.setPdate(b2);
                            info.setChannelId(str);
                        }
                    }
                    if (ChannelType.TYPE_CHANNEL_RECOMMEND.equals(str)) {
                        result.setData(av.a(list));
                    }
                    a(result);
                    new d(this.c).a(list);
                    return result;
                } else if (result != null) {
                    result.setData(new d(this.c).a(Info.class, "channelId", str, 0, 20, "timeOrder", j, 0));
                    a(result);
                    return result;
                } else {
                    a(result);
                    return result;
                }
            } else {
                if (ChannelType.TYPE_CHANNEL_RECOMMEND.equals(str)) {
                    result2.setData(av.a(a2));
                }
                a(result2);
                return result2;
            }
        } catch (Exception e4) {
            e2 = e4;
            result = result2;
            e2.printStackTrace();
            a(result);
            return result;
        }
    }
}
