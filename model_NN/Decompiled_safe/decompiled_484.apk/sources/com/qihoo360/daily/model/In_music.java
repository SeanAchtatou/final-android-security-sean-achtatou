package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class In_music implements Parcelable {
    public static final Parcelable.Creator<In_music> CREATOR = new Parcelable.Creator<In_music>() {
        public In_music createFromParcel(Parcel parcel) {
            return new In_music(parcel);
        }

        public In_music[] newArray(int i) {
            return new In_music[i];
        }
    };
    private int customSort;
    private long music_id;
    private String title;
    private String url;

    public In_music() {
    }

    public In_music(Parcel parcel) {
        this.url = parcel.readString();
        this.title = parcel.readString();
        this.music_id = (long) parcel.readInt();
        this.customSort = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public int getCustomSort() {
        return this.customSort;
    }

    public long getMusic_id() {
        return this.music_id;
    }

    public String getTitle() {
        return this.title;
    }

    public String getUrl() {
        return this.url;
    }

    public void setCustomSort(int i) {
        this.customSort = i;
    }

    public void setMusic_id(long j) {
        this.music_id = j;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public void setUrl(String str) {
        this.url = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.url);
        parcel.writeString(this.title);
        parcel.writeLong(this.music_id);
        parcel.writeInt(this.customSort);
    }
}
