package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Comment implements Parcelable {
    public static final Parcelable.Creator<Comment> CREATOR = new Parcelable.Creator<Comment>() {
        public Comment createFromParcel(Parcel parcel) {
            return new Comment(parcel);
        }

        public Comment[] newArray(int i) {
            return new Comment[i];
        }
    };
    private String cid;
    private String content;
    private String digg;
    private int diggok;
    private String head;
    private String id;
    private String name;
    private String pdate;
    private String pos;
    private String uid;

    public Comment() {
    }

    private Comment(Parcel parcel) {
        this.id = parcel.readString();
        this.pos = parcel.readString();
        this.cid = parcel.readString();
        this.uid = parcel.readString();
        this.name = parcel.readString();
        this.digg = parcel.readString();
        this.pdate = parcel.readString();
        this.content = parcel.readString();
        this.head = parcel.readString();
        this.diggok = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public String getCid() {
        return this.cid;
    }

    public String getContent() {
        return this.content;
    }

    public String getDigg() {
        return this.digg;
    }

    public int getDiggok() {
        return this.diggok;
    }

    public String getHead() {
        return this.head;
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getPdate() {
        return this.pdate;
    }

    public String getPos() {
        return this.pos;
    }

    public String getUid() {
        return this.uid;
    }

    public void setCid(String str) {
        this.cid = str;
    }

    public void setContent(String str) {
        this.content = str;
    }

    public void setDigg(String str) {
        this.digg = str;
    }

    public void setDiggok(int i) {
        this.diggok = i;
    }

    public void setHead(String str) {
        this.head = str;
    }

    public void setId(String str) {
        this.id = str;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setPdate(String str) {
        this.pdate = str;
    }

    public void setPos(String str) {
        this.pos = str;
    }

    public void setUid(String str) {
        this.uid = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.id);
        parcel.writeString(this.pos);
        parcel.writeString(this.cid);
        parcel.writeString(this.uid);
        parcel.writeString(this.name);
        parcel.writeString(this.digg);
        parcel.writeString(this.pdate);
        parcel.writeString(this.content);
        parcel.writeString(this.head);
        parcel.writeInt(this.diggok);
    }
}
