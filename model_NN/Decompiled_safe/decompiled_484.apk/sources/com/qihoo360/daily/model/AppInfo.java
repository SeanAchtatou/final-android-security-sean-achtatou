package com.qihoo360.daily.model;

import android.graphics.drawable.Drawable;

public class AppInfo {
    private Drawable appIcon;
    private String appLauncherClassName;
    private String appName;
    private String appPkgName;

    public AppInfo() {
    }

    public AppInfo(String str, String str2) {
        this.appLauncherClassName = str;
        this.appName = str2;
    }

    public Drawable getAppIcon() {
        return this.appIcon;
    }

    public String getAppLauncherClassName() {
        return this.appLauncherClassName;
    }

    public String getAppName() {
        return this.appName;
    }

    public String getAppPkgName() {
        return this.appPkgName;
    }

    public void setAppIcon(Drawable drawable) {
        this.appIcon = drawable;
    }

    public void setAppLauncherClassName(String str) {
        this.appLauncherClassName = str;
    }

    public void setAppName(String str) {
        this.appName = str;
    }

    public void setAppPkgName(String str) {
        this.appPkgName = str;
    }
}
