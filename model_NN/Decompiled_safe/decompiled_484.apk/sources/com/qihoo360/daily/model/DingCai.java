package com.qihoo360.daily.model;

public class DingCai {
    private String bury;
    private String digg;
    private int dingType;

    public String getBury() {
        return this.bury;
    }

    public String getDigg() {
        return this.digg;
    }

    public int getDingType() {
        return this.dingType;
    }

    public void setBury(String str) {
        this.bury = str;
    }

    public void setDigg(String str) {
        this.digg = str;
    }

    public void setDingType(int i) {
        this.dingType = i;
    }
}
