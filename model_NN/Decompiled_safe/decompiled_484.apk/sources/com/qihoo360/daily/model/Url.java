package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Url implements Parcelable {
    public static final Parcelable.Creator<Url> CREATOR = new Parcelable.Creator<Url>() {
        public Url createFromParcel(Parcel parcel) {
            return new Url(parcel);
        }

        public Url[] newArray(int i) {
            return new Url[i];
        }
    };
    private String url;

    public Url() {
    }

    public Url(Parcel parcel) {
        this.url = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String str) {
        this.url = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.url);
    }
}
