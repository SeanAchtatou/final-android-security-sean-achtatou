package com.qihoo360.daily.model;

import java.io.Serializable;

public class Tianqi implements Serializable {
    private static final long serialVersionUID = 1;
    private String[][] area;
    private WeatherHistory historyWeather;
    private Life life;
    private String pm;
    private Realtime realtime;
    private long time;
    private WeatherDay[] weather;

    public String[][] getArea() {
        return this.area;
    }

    public WeatherHistory getHistoryWeather() {
        return this.historyWeather;
    }

    public Life getLife() {
        return this.life;
    }

    public String getPm() {
        return this.pm;
    }

    public Realtime getRealtime() {
        return this.realtime;
    }

    public long getTime() {
        return this.time;
    }

    public WeatherDay[] getWeather() {
        return this.weather;
    }

    public void setArea(String[][] strArr) {
        this.area = strArr;
    }

    public void setHistoryWeather(WeatherHistory weatherHistory) {
        this.historyWeather = weatherHistory;
    }

    public void setLife(Life life2) {
        this.life = life2;
    }

    public void setPm(String str) {
        this.pm = str;
    }

    public void setRealtime(Realtime realtime2) {
        this.realtime = realtime2;
    }

    public void setTime(long j) {
        this.time = j;
    }

    public void setWeather(WeatherDay[] weatherDayArr) {
        this.weather = weatherDayArr;
    }
}
