package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Suggest implements Parcelable {
    public static final Parcelable.Creator<Suggest> CREATOR = new Parcelable.Creator<Suggest>() {
        public Suggest createFromParcel(Parcel parcel) {
            return new Suggest(parcel);
        }

        public Suggest[] newArray(int i) {
            return new Suggest[i];
        }
    };
    private String p;
    private String q;
    private String[] s;

    public Suggest(Parcel parcel) {
        this.q = parcel.readString();
        this.p = parcel.readString();
        parcel.readStringArray(this.s);
    }

    public int describeContents() {
        return 0;
    }

    public String getP() {
        return this.p;
    }

    public String getQ() {
        return this.q;
    }

    public String[] getS() {
        return this.s;
    }

    public void setP(String str) {
        this.p = str;
    }

    public void setQ(String str) {
        this.q = str;
    }

    public void setS(String[] strArr) {
        this.s = strArr;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.q);
        parcel.writeString(this.p);
        parcel.writeStringArray(this.s);
    }
}
