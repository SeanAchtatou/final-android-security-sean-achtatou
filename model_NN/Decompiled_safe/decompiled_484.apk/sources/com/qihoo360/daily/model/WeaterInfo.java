package com.qihoo360.daily.model;

import java.io.Serializable;

public class WeaterInfo implements Serializable {
    private static final long serialVersionUID = 1;
    private String[] dawn;
    private String[] day;
    private String[] night;

    public String[] getDawn() {
        return this.dawn;
    }

    public String[] getDay() {
        return this.day;
    }

    public String[] getNight() {
        return this.night;
    }

    public void setDawn(String[] strArr) {
        this.dawn = strArr;
    }

    public void setDay(String[] strArr) {
        this.day = strArr;
    }

    public void setNight(String[] strArr) {
        this.night = strArr;
    }
}
