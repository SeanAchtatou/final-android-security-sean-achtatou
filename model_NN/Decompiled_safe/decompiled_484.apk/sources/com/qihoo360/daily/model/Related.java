package com.qihoo360.daily.model;

import android.os.Parcelable;

public class Related extends Info implements Parcelable {
    private String ext;
    private String rank;

    public String getExt() {
        return this.ext;
    }

    public String getRank() {
        return this.rank;
    }

    public void setExt(String str) {
        this.ext = str;
    }

    public void setRank(String str) {
        this.rank = str;
    }
}
