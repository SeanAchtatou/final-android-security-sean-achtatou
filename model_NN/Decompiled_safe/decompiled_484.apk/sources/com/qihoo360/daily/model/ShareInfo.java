package com.qihoo360.daily.model;

public class ShareInfo {
    public static final int ISNOT_IMG_LOGO = 0;
    public static final int IS_IMG_LOGO = 1;
    private String content;
    private String imgPath;
    private boolean isDeepRead;
    private String title;
    private int type;
    private String url;

    public ShareInfo() {
    }

    public ShareInfo(String str, String str2, String str3, String str4, boolean z) {
        this.url = str;
        this.title = str2;
        this.content = str3;
        this.imgPath = str4;
        this.type = 0;
        this.isDeepRead = z;
    }

    public String getContent() {
        return this.content;
    }

    public String getImgPath() {
        return this.imgPath;
    }

    public String getTitle() {
        return this.title;
    }

    public int getType() {
        return this.type;
    }

    public String getUrl() {
        return this.url;
    }

    public boolean isDeepRead() {
        return this.isDeepRead;
    }

    public void setContent(String str) {
        this.content = str;
    }

    public void setDeepRead(boolean z) {
        this.isDeepRead = z;
    }

    public void setImgPath(String str) {
        this.imgPath = str;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public void setType(int i) {
        this.type = i;
    }

    public void setUrl(String str) {
        this.url = str;
    }
}
