package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

public class SearchResponse implements Parcelable {
    public static final Parcelable.Creator<SearchResponse> CREATOR = new Parcelable.Creator<SearchResponse>() {
        public SearchResponse createFromParcel(Parcel parcel) {
            return new SearchResponse(parcel);
        }

        public SearchResponse[] newArray(int i) {
            return new SearchResponse[i];
        }
    };
    private int code;
    private List<SearchNews> result;

    public SearchResponse() {
    }

    private SearchResponse(Parcel parcel) {
        this.code = parcel.readInt();
        parcel.readTypedList(this.result, SearchNews.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public int getCode() {
        return this.code;
    }

    public List<SearchNews> getResult() {
        return this.result;
    }

    public void setCode(int i) {
        this.code = i;
    }

    public void setResult(List<SearchNews> list) {
        this.result = list;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.code);
        parcel.writeTypedList(this.result);
    }
}
