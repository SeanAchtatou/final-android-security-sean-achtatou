package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class HotSearchItem implements Parcelable {
    public static final Parcelable.Creator<HotSearchItem> CREATOR = new Parcelable.Creator<HotSearchItem>() {
        public HotSearchItem createFromParcel(Parcel parcel) {
            return new HotSearchItem(parcel);
        }

        public HotSearchItem[] newArray(int i) {
            return new HotSearchItem[i];
        }
    };
    private int hot_label;
    private String hot_word;
    private String query_key;
    private String query_url;

    private HotSearchItem(Parcel parcel) {
        this.hot_word = parcel.readString();
        this.query_url = parcel.readString();
        this.query_key = parcel.readString();
        this.hot_label = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public int getHot_label() {
        return this.hot_label;
    }

    public String getHot_word() {
        return this.hot_word;
    }

    public String getQuery_key() {
        return this.query_key;
    }

    public String getQuery_url() {
        return this.query_url;
    }

    public void setHot_label(int i) {
        this.hot_label = i;
    }

    public void setHot_word(String str) {
        this.hot_word = str;
    }

    public void setQuery_key(String str) {
        this.query_key = str;
    }

    public void setQuery_url(String str) {
        this.query_url = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.hot_word);
        parcel.writeString(this.query_url);
        parcel.writeString(this.query_key);
        parcel.writeInt(this.hot_label);
    }
}
