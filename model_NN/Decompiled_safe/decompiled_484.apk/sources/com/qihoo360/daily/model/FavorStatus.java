package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class FavorStatus implements Parcelable {
    public static final Parcelable.Creator<FavorStatus> CREATOR = new Parcelable.Creator<FavorStatus>() {
        public FavorStatus createFromParcel(Parcel parcel) {
            return new FavorStatus(parcel);
        }

        public FavorStatus[] newArray(int i) {
            return new FavorStatus[i];
        }
    };
    private String md5;
    private int stat;

    public FavorStatus() {
    }

    private FavorStatus(Parcel parcel) {
        this.md5 = parcel.readString();
        this.stat = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public String getMd5() {
        return this.md5;
    }

    public int getStat() {
        return this.stat;
    }

    public void setMd5(String str) {
        this.md5 = str;
    }

    public void setStat(int i) {
        this.stat = i;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.md5);
        parcel.writeInt(this.stat);
    }
}
