package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CmtHot implements Parcelable {
    public static final Parcelable.Creator<CmtHot> CREATOR = new Parcelable.Creator<CmtHot>() {
        public CmtHot createFromParcel(Parcel parcel) {
            return new CmtHot(parcel);
        }

        public CmtHot[] newArray(int i) {
            return new CmtHot[i];
        }
    };
    private String cid;
    private String content;
    private String digg;
    private String head;
    private String name;
    private String pdate;
    private String tp;
    private String uid;

    public CmtHot() {
    }

    private CmtHot(Parcel parcel) {
        this.cid = parcel.readString();
        this.content = parcel.readString();
        this.digg = parcel.readString();
        this.head = parcel.readString();
        this.name = parcel.readString();
        this.pdate = parcel.readString();
        this.tp = parcel.readString();
        this.uid = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public String getCid() {
        return this.cid;
    }

    public String getContent() {
        return this.content;
    }

    public String getDigg() {
        return this.digg;
    }

    public String getHead() {
        return this.head;
    }

    public String getName() {
        return this.name;
    }

    public String getPdate() {
        return this.pdate;
    }

    public String getTp() {
        return this.tp;
    }

    public String getUid() {
        return this.uid;
    }

    public void setCid(String str) {
        this.cid = str;
    }

    public void setContent(String str) {
        this.content = str;
    }

    public void setDigg(String str) {
        this.digg = str;
    }

    public void setHead(String str) {
        this.head = str;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setPdate(String str) {
        this.pdate = str;
    }

    public void setTp(String str) {
        this.tp = str;
    }

    public void setUid(String str) {
        this.uid = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.cid);
        parcel.writeString(this.content);
        parcel.writeString(this.digg);
        parcel.writeString(this.head);
        parcel.writeString(this.name);
        parcel.writeString(this.pdate);
        parcel.writeString(this.tp);
        parcel.writeString(this.uid);
    }
}
