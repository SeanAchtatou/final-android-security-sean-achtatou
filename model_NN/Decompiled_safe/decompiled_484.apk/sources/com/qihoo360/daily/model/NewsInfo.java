package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class NewsInfo implements Parcelable {
    public static final Parcelable.Creator<NewsInfo> CREATOR = new Parcelable.Creator<NewsInfo>() {
        public NewsInfo createFromParcel(Parcel parcel) {
            return new NewsInfo(parcel);
        }

        public NewsInfo[] newArray(int i) {
            return new NewsInfo[i];
        }
    };
    private String bury;
    private String cmt_cnt;
    private String digg;
    private int dingType;
    private String nid;
    private String url;

    public NewsInfo() {
    }

    private NewsInfo(Parcel parcel) {
        this.nid = parcel.readString();
        this.url = parcel.readString();
        this.digg = parcel.readString();
        this.bury = parcel.readString();
        this.dingType = parcel.readInt();
        this.cmt_cnt = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public String getBury() {
        return this.bury;
    }

    public String getCmt_cnt() {
        return this.cmt_cnt;
    }

    public String getDigg() {
        return this.digg;
    }

    public int getDingType() {
        return this.dingType;
    }

    public String getNid() {
        return this.nid;
    }

    public String getUrl() {
        return this.url;
    }

    public void setBury(String str) {
        this.bury = str;
    }

    public void setCmt_cnt(String str) {
        this.cmt_cnt = str;
    }

    public void setDigg(String str) {
        this.digg = str;
    }

    public void setDingType(int i) {
        this.dingType = i;
    }

    public void setNid(String str) {
        this.nid = str;
    }

    public void setUrl(String str) {
        this.url = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.nid);
        parcel.writeString(this.url);
        parcel.writeString(this.digg);
        parcel.writeString(this.bury);
        parcel.writeInt(this.dingType);
        parcel.writeString(this.cmt_cnt);
    }
}
