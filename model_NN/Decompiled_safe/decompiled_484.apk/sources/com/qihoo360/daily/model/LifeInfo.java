package com.qihoo360.daily.model;

import java.io.Serializable;

public class LifeInfo implements Serializable {
    private static final long serialVersionUID = 1;
    private String[] bingqilin;
    private String[] chuanyi;
    private String[] ganmao;
    private String[] kongtiao;
    private String[] tiganwendu;
    private String[] wuran;
    private String[] xiche;
    private String[] yundong;
    private String[] ziwaixian;

    public String[] getBingqilin() {
        return this.bingqilin;
    }

    public String[] getChuanyi() {
        return this.chuanyi;
    }

    public String[] getGanmao() {
        return this.ganmao;
    }

    public String[] getKongtiao() {
        return this.kongtiao;
    }

    public String[] getTiganwendu() {
        return this.tiganwendu;
    }

    public String[] getWuran() {
        return this.wuran;
    }

    public String[] getXiche() {
        return this.xiche;
    }

    public String[] getYundong() {
        return this.yundong;
    }

    public String[] getZiwaixian() {
        return this.ziwaixian;
    }

    public void setBingqilin(String[] strArr) {
        this.bingqilin = strArr;
    }

    public void setChuanyi(String[] strArr) {
        this.chuanyi = strArr;
    }

    public void setGanmao(String[] strArr) {
        this.ganmao = strArr;
    }

    public void setKongtiao(String[] strArr) {
        this.kongtiao = strArr;
    }

    public void setTiganwendu(String[] strArr) {
        this.tiganwendu = strArr;
    }

    public void setWuran(String[] strArr) {
        this.wuran = strArr;
    }

    public void setXiche(String[] strArr) {
        this.xiche = strArr;
    }

    public void setYundong(String[] strArr) {
        this.yundong = strArr;
    }

    public void setZiwaixian(String[] strArr) {
        this.ziwaixian = strArr;
    }
}
