package com.qihoo360.daily.model;

import java.io.Serializable;

public class Result<T> implements Serializable {
    public static final int HTTP_DIGGED = 108;
    public static final int HTTP_OK = 0;
    private static final long serialVersionUID = 1;
    private T data;
    private String json;
    private String msg;
    private int status;

    public T getData() {
        return this.data;
    }

    public String getJson() {
        return this.json;
    }

    public String getMsg() {
        return this.msg;
    }

    public int getStatus() {
        return this.status;
    }

    public void setData(T t) {
        this.data = t;
    }

    public void setJson(String str) {
        this.json = str;
    }

    public void setMsg(String str) {
        this.msg = str;
    }

    public void setStatus(int i) {
        this.status = i;
    }
}
