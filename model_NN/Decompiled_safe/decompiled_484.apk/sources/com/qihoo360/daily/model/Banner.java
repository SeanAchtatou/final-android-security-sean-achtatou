package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Banner implements Parcelable {
    public static final Parcelable.Creator<Banner> CREATOR = new Parcelable.Creator<Banner>() {
        public Banner createFromParcel(Parcel parcel) {
            return new Banner(parcel);
        }

        public Banner[] newArray(int i) {
            return new Banner[i];
        }
    };
    private String desc;
    private String imgurl;

    public Banner(Parcel parcel) {
        this.imgurl = parcel.readString();
        this.desc = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public String getDesc() {
        return this.desc;
    }

    public String getImgurl() {
        return this.imgurl;
    }

    public void setDesc(String str) {
        this.desc = str;
    }

    public void setImgurl(String str) {
        this.imgurl = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.imgurl);
        parcel.writeString(this.desc);
    }
}
