package com.qihoo360.daily.model;

public class DailyUpdate {
    private Data data;
    private String msg;
    private int status;

    public class Data {
        private long seg;

        public Data() {
        }

        public long getSeg() {
            return this.seg;
        }

        public void setSeg(long j) {
            this.seg = j;
        }
    }

    public Data getData() {
        return this.data;
    }

    public String getMsg() {
        return this.msg;
    }

    public int getStatus() {
        return this.status;
    }

    public void setData(Data data2) {
        this.data = data2;
    }

    public void setMsg(String str) {
        this.msg = str;
    }

    public void setStatus(int i) {
        this.status = i;
    }
}
