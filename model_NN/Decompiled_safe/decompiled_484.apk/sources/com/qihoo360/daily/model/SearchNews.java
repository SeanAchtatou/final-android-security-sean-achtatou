package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SearchNews implements Parcelable {
    public static final Parcelable.Creator<SearchNews> CREATOR = new Parcelable.Creator<SearchNews>() {
        public SearchNews createFromParcel(Parcel parcel) {
            return new SearchNews(parcel);
        }

        public SearchNews[] newArray(int i) {
            return new SearchNews[i];
        }
    };
    private String domain;
    private String imgUrl;
    private String[] kws;
    private String m;
    private long pdate;
    private String summary;
    private String title;
    private String url;

    public SearchNews() {
    }

    private SearchNews(Parcel parcel) {
        this.domain = parcel.readString();
        this.imgUrl = parcel.readString();
        this.m = parcel.readString();
        this.pdate = parcel.readLong();
        this.summary = parcel.readString();
        this.title = parcel.readString();
        this.url = parcel.readString();
        this.kws = parcel.createStringArray();
    }

    public int describeContents() {
        return 0;
    }

    public String getDomain() {
        return this.domain;
    }

    public String getImgUrl() {
        return this.imgUrl;
    }

    public String[] getKws() {
        return this.kws;
    }

    public String getM() {
        return this.m;
    }

    public long getPdate() {
        return this.pdate;
    }

    public String getSummary() {
        return this.summary;
    }

    public String getTitle() {
        return this.title;
    }

    public String getUrl() {
        return this.url;
    }

    public void setDomain(String str) {
        this.domain = str;
    }

    public void setImgUrl(String str) {
        this.imgUrl = str;
    }

    public void setKws(String[] strArr) {
        this.kws = strArr;
    }

    public void setM(String str) {
        this.m = str;
    }

    public void setPdate(long j) {
        this.pdate = j;
    }

    public void setSummary(String str) {
        this.summary = str;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public void setUrl(String str) {
        this.url = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.domain);
        parcel.writeString(this.imgUrl);
        parcel.writeString(this.m);
        parcel.writeLong(this.pdate);
        parcel.writeString(this.summary);
        parcel.writeString(this.title);
        parcel.writeString(this.url);
        parcel.writeStringArray(this.kws);
    }
}
