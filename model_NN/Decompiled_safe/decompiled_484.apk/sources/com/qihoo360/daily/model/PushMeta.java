package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PushMeta implements Parcelable {
    public static final Parcelable.Creator<PushMeta> CREATOR = new Parcelable.Creator<PushMeta>() {
        public PushMeta createFromParcel(Parcel parcel) {
            return new PushMeta(parcel);
        }

        public PushMeta[] newArray(int i) {
            return new PushMeta[i];
        }
    };
    private String channelId;
    private String expire;
    private String important;
    private String msgID;
    private String pushUrl;
    private int type;

    public PushMeta(Parcel parcel) {
        this.msgID = parcel.readString();
        this.expire = parcel.readString();
        this.channelId = parcel.readString();
        this.important = parcel.readString();
        this.pushUrl = parcel.readString();
        this.type = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public String getChannelId() {
        return this.channelId;
    }

    public String getExpire() {
        return this.expire;
    }

    public String getImportant() {
        return this.important;
    }

    public String getMsgID() {
        return this.msgID;
    }

    public String getPushUrl() {
        return this.pushUrl;
    }

    public int getType() {
        return this.type;
    }

    public void setChannelId(String str) {
        this.channelId = str;
    }

    public void setExpire(String str) {
        this.expire = str;
    }

    public void setImportant(String str) {
        this.important = str;
    }

    public void setMsgID(String str) {
        this.msgID = str;
    }

    public void setPushUrl(String str) {
        this.pushUrl = str;
    }

    public void setType(int i) {
        this.type = i;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.msgID);
        parcel.writeString(this.expire);
        parcel.writeString(this.channelId);
        parcel.writeString(this.important);
        parcel.writeString(this.pushUrl);
        parcel.writeInt(this.type);
    }
}
