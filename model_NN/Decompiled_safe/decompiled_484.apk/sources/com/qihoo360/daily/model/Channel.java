package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.qihoo360.daily.activity.SendCmtActivity;
import java.util.LinkedList;
import org.json.JSONArray;
import org.json.JSONObject;

public class Channel implements Parcelable {
    public static final Parcelable.Creator<Channel> CREATOR = new Parcelable.Creator<Channel>() {
        public Channel createFromParcel(Parcel parcel) {
            return new Channel(parcel);
        }

        public Channel[] newArray(int i) {
            return new Channel[i];
        }
    };
    public static final int TYPE_COMMON = 0;
    public static final int TYPE_DAILY = 2;
    public static final int TYPE_SPECIAL = 1;
    private String alias;
    private String id;
    private String title;
    private int type;

    public Channel() {
    }

    public Channel(Parcel parcel) {
        this.id = parcel.readString();
        this.title = parcel.readString();
        this.type = parcel.readInt();
        parcel.readBooleanArray(new boolean[1]);
    }

    public Channel(String str, String str2, int i) {
        this.title = str;
        this.alias = str2;
        this.type = i;
    }

    public Channel(String str, String str2, int i, int i2) {
        this.title = str;
        this.type = i2;
    }

    public int describeContents() {
        return 0;
    }

    public String getAlias() {
        return this.alias;
    }

    public String getId() {
        return this.id;
    }

    public LinkedList<Channel> getListEntity(String str) {
        LinkedList<Channel> linkedList = new LinkedList<>();
        JSONArray jSONArray = new JSONObject(str).getJSONArray(SendCmtActivity.TAG_DATA);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= jSONArray.length()) {
                return linkedList;
            }
            JSONObject jSONObject = (JSONObject) jSONArray.get(i2);
            Channel channel = new Channel();
            channel.setTitle(jSONObject.optString("columnname"));
            channel.setId(jSONObject.optString("id"));
            linkedList.add(channel);
            i = i2 + 1;
        }
    }

    public String getTitle() {
        return this.title;
    }

    public int getType() {
        return this.type;
    }

    public void setAlias(String str) {
        this.alias = str;
    }

    public void setId(String str) {
        this.id = str;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public void setType(int i) {
        this.type = i;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.id);
        parcel.writeString(this.title);
        parcel.writeInt(this.type);
    }
}
