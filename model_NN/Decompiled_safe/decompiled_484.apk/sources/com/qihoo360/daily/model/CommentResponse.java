package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public class CommentResponse implements Parcelable {
    public static final Parcelable.Creator<CommentResponse> CREATOR = new Parcelable.Creator<CommentResponse>() {
        public CommentResponse createFromParcel(Parcel parcel) {
            return new CommentResponse(parcel);
        }

        public CommentResponse[] newArray(int i) {
            return new CommentResponse[i];
        }
    };
    private ArrayList<Comment> hot;
    private int hot_total;
    private ArrayList<Comment> time;
    private int time_total;

    public CommentResponse() {
    }

    private CommentResponse(Parcel parcel) {
        this.time = parcel.readArrayList(Comment.class.getClassLoader());
        this.hot = parcel.readArrayList(Comment.class.getClassLoader());
        this.hot_total = parcel.readInt();
        this.time_total = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public ArrayList<Comment> getHot() {
        return this.hot;
    }

    public int getHot_total() {
        return this.hot_total;
    }

    public ArrayList<Comment> getTime() {
        return this.time;
    }

    public int getTime_total() {
        return this.time_total;
    }

    public void setHot(ArrayList<Comment> arrayList) {
        this.hot = arrayList;
    }

    public void setHot_total(int i) {
        this.hot_total = i;
    }

    public void setTime(ArrayList<Comment> arrayList) {
        this.time = arrayList;
    }

    public void setTime_total(int i) {
        this.time_total = i;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(this.time);
        parcel.writeList(this.hot);
        parcel.writeInt(this.hot_total);
        parcel.writeInt(this.time_total);
    }
}
