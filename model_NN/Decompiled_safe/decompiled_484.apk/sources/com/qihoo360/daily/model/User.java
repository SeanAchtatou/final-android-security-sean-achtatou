package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {
    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel parcel) {
            return new User(parcel);
        }

        public User[] newArray(int i) {
            return new User[i];
        }
    };
    private boolean allow_all_act_msg;
    private boolean allow_all_comment;
    private String avatar_large;
    private int bi_followers_count;
    private String city;
    private String created_at;
    private String description;
    private String domain;
    private int favourites_count;
    private boolean follow_me;
    private int followers_count;
    private boolean following;
    private int friends_count;
    private String gender;
    private boolean geo_enabled;
    private long id;
    private String location;
    private String name;
    private int online_status;
    private String profile_image_url;
    private String province;
    private String screen_name;
    private Status status;
    private int statuses_count;
    private String url;
    private boolean verified;
    private String verified_reason;

    public User(Parcel parcel) {
        this.id = parcel.readLong();
        this.created_at = parcel.readString();
        this.screen_name = parcel.readString();
        this.name = parcel.readString();
        this.province = parcel.readString();
        this.city = parcel.readString();
        this.location = parcel.readString();
        this.description = parcel.readString();
        this.url = parcel.readString();
        this.profile_image_url = parcel.readString();
        this.domain = parcel.readString();
        this.gender = parcel.readString();
        this.avatar_large = parcel.readString();
        this.verified_reason = parcel.readString();
        this.followers_count = parcel.readInt();
        this.friends_count = parcel.readInt();
        this.statuses_count = parcel.readInt();
        this.favourites_count = parcel.readInt();
        this.online_status = parcel.readInt();
        this.bi_followers_count = parcel.readInt();
        boolean[] zArr = new boolean[6];
        parcel.readBooleanArray(zArr);
        this.following = zArr[0];
        this.allow_all_act_msg = zArr[1];
        this.geo_enabled = zArr[2];
        this.verified = zArr[3];
        this.allow_all_comment = zArr[4];
        this.follow_me = zArr[5];
        this.status = (Status) parcel.readParcelable(Status.class.getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    public String getAvatar_large() {
        return this.avatar_large;
    }

    public int getBi_followers_count() {
        return this.bi_followers_count;
    }

    public String getCity() {
        return this.city;
    }

    public String getCreated_at() {
        return this.created_at;
    }

    public String getDescription() {
        return this.description;
    }

    public String getDomain() {
        return this.domain;
    }

    public int getFavourites_count() {
        return this.favourites_count;
    }

    public int getFollowers_count() {
        return this.followers_count;
    }

    public int getFriends_count() {
        return this.friends_count;
    }

    public String getGender() {
        return this.gender;
    }

    public long getId() {
        return this.id;
    }

    public String getLocation() {
        return this.location;
    }

    public String getName() {
        return this.name;
    }

    public int getOnline_status() {
        return this.online_status;
    }

    public String getProfile_image_url() {
        return this.profile_image_url;
    }

    public String getProvince() {
        return this.province;
    }

    public String getScreen_name() {
        return this.screen_name;
    }

    public Status getStatus() {
        return this.status;
    }

    public int getStatuses_count() {
        return this.statuses_count;
    }

    public String getUrl() {
        return this.url;
    }

    public String getVerified_reason() {
        return this.verified_reason;
    }

    public boolean isAllow_all_act_msg() {
        return this.allow_all_act_msg;
    }

    public boolean isAllow_all_comment() {
        return this.allow_all_comment;
    }

    public boolean isFollow_me() {
        return this.follow_me;
    }

    public boolean isFollowing() {
        return this.following;
    }

    public boolean isGeo_enabled() {
        return this.geo_enabled;
    }

    public boolean isVerified() {
        return this.verified;
    }

    public void merge(User user) {
        if (user != null) {
            this.allow_all_act_msg = user.allow_all_act_msg;
            this.allow_all_comment = user.allow_all_comment;
            this.avatar_large = user.avatar_large;
            this.bi_followers_count = user.bi_followers_count;
            this.city = user.city;
            this.created_at = user.created_at;
            this.description = user.description;
            this.domain = user.domain;
            this.favourites_count = user.favourites_count;
            this.follow_me = user.follow_me;
            this.followers_count = user.followers_count;
            this.following = user.following;
            this.friends_count = user.friends_count;
            this.gender = user.gender;
            this.geo_enabled = user.geo_enabled;
            this.id = user.id;
            this.location = user.location;
            this.name = user.name;
            this.online_status = user.online_status;
            this.profile_image_url = user.profile_image_url;
            this.province = user.province;
            this.screen_name = user.screen_name;
            this.status = user.status;
            this.statuses_count = user.statuses_count;
            this.url = user.url;
            this.verified = user.verified;
            this.verified_reason = user.verified_reason;
        }
    }

    public void setAllow_all_act_msg(boolean z) {
        this.allow_all_act_msg = z;
    }

    public void setAllow_all_comment(boolean z) {
        this.allow_all_comment = z;
    }

    public void setAvatar_large(String str) {
        this.avatar_large = str;
    }

    public void setBi_followers_count(int i) {
        this.bi_followers_count = i;
    }

    public void setCity(String str) {
        this.city = str;
    }

    public void setCreated_at(String str) {
        this.created_at = str;
    }

    public void setDescription(String str) {
        this.description = str;
    }

    public void setDomain(String str) {
        this.domain = str;
    }

    public void setFavourites_count(int i) {
        this.favourites_count = i;
    }

    public void setFollow_me(boolean z) {
        this.follow_me = z;
    }

    public void setFollowers_count(int i) {
        this.followers_count = i;
    }

    public void setFollowing(boolean z) {
        this.following = z;
    }

    public void setFriends_count(int i) {
        this.friends_count = i;
    }

    public void setGender(String str) {
        this.gender = str;
    }

    public void setGeo_enabled(boolean z) {
        this.geo_enabled = z;
    }

    public void setId(long j) {
        this.id = j;
    }

    public void setLocation(String str) {
        this.location = str;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setOnline_status(int i) {
        this.online_status = i;
    }

    public void setProfile_image_url(String str) {
        this.profile_image_url = str;
    }

    public void setProvince(String str) {
        this.province = str;
    }

    public void setScreen_name(String str) {
        this.screen_name = str;
    }

    public void setStatus(Status status2) {
        this.status = status2;
    }

    public void setStatuses_count(int i) {
        this.statuses_count = i;
    }

    public void setUrl(String str) {
        this.url = str;
    }

    public void setVerified(boolean z) {
        this.verified = z;
    }

    public void setVerified_reason(String str) {
        this.verified_reason = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.id);
        parcel.writeString(this.created_at);
        parcel.writeString(this.screen_name);
        parcel.writeString(this.name);
        parcel.writeString(this.province);
        parcel.writeString(this.city);
        parcel.writeString(this.location);
        parcel.writeString(this.description);
        parcel.writeString(this.url);
        parcel.writeString(this.profile_image_url);
        parcel.writeString(this.domain);
        parcel.writeString(this.gender);
        parcel.writeString(this.avatar_large);
        parcel.writeString(this.verified_reason);
        parcel.writeInt(this.followers_count);
        parcel.writeInt(this.friends_count);
        parcel.writeInt(this.statuses_count);
        parcel.writeInt(this.favourites_count);
        parcel.writeInt(this.online_status);
        parcel.writeInt(this.bi_followers_count);
        parcel.writeBooleanArray(new boolean[]{this.following, this.allow_all_act_msg, this.geo_enabled, this.verified, this.allow_all_comment, this.follow_me});
        parcel.writeParcelable(this.status, i);
    }
}
