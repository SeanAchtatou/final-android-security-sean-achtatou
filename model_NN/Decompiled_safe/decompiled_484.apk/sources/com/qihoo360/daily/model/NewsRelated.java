package com.qihoo360.daily.model;

import java.util.ArrayList;

public class NewsRelated {
    private ArrayList<Related> editorNewsList;
    private String isCollected;
    private ArrayList<Related> related;
    private String summary;
    private DingCai supp;

    public ArrayList<Related> getEditorNewsList() {
        return this.editorNewsList;
    }

    public String getIsCollected() {
        return this.isCollected;
    }

    public ArrayList<Related> getRelated() {
        return this.related;
    }

    public String getSummary() {
        return this.summary;
    }

    public DingCai getSupp() {
        return this.supp;
    }

    public void setEditorNewsList(ArrayList<Related> arrayList) {
        this.editorNewsList = arrayList;
    }

    public void setIsCollected(String str) {
        this.isCollected = str;
    }

    public void setRelated(ArrayList<Related> arrayList) {
        this.related = arrayList;
    }

    public void setSummary(String str) {
        this.summary = str;
    }

    public void setSupp(DingCai dingCai) {
        this.supp = dingCai;
    }
}
