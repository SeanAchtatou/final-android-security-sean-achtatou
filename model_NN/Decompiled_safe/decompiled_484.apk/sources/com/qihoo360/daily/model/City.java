package com.qihoo360.daily.model;

import android.text.TextUtils;
import java.io.Serializable;

public class City implements Serializable {
    private static final long serialVersionUID = 1;
    private String city = "";
    private String citySpell = "";
    private String code = "";
    private String district = "";
    private boolean isAuto = false;
    private String province = "";
    private String provinceSpell = "";

    public String getCity() {
        return this.city;
    }

    public String getCitySpell() {
        return this.citySpell;
    }

    public String getCode() {
        return this.code;
    }

    public String getDistrict() {
        return this.district;
    }

    public String getName() {
        String str = this.district;
        if (TextUtils.isEmpty(str)) {
            str = this.city;
        }
        return TextUtils.isEmpty(str) ? this.province : str;
    }

    public String getProvince() {
        return this.province;
    }

    public String getProvinceSpell() {
        return this.provinceSpell;
    }

    public boolean isAuto() {
        return this.isAuto;
    }

    public void merge(City city2) {
        if (city2 != null) {
            this.city = city2.getCity();
            this.district = city2.getDistrict();
            this.province = city2.getProvince();
            this.code = city2.getCode();
            this.citySpell = city2.getCitySpell();
            this.provinceSpell = city2.getProvinceSpell();
            this.isAuto = city2.isAuto();
        }
    }

    public void setAuto(boolean z) {
        this.isAuto = z;
    }

    public void setCity(String str) {
        this.city = str;
    }

    public void setCitySpell(String str) {
        this.citySpell = str;
    }

    public void setCode(String str) {
        this.code = str;
    }

    public void setDistrict(String str) {
        this.district = str;
    }

    public void setProvince(String str) {
        this.province = str;
    }

    public void setProvinceSpell(String str) {
        this.provinceSpell = str;
    }
}
