package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class HotNews implements Parcelable {
    public static final Parcelable.Creator<HotNews> CREATOR = new Parcelable.Creator<HotNews>() {
        public HotNews createFromParcel(Parcel parcel) {
            return new HotNews(parcel);
        }

        public HotNews[] newArray(int i) {
            return new HotNews[i];
        }
    };
    private String cls;
    private String content;
    private String createtime;
    private String domain;
    private String flag;
    private String gk;
    private long id;
    private String idx;
    private String img;
    private String isnew;
    private String keyword;
    private String ltitle;
    private String m;
    private int n_t;
    private String pdate;
    private String show;
    private String site_name;
    private String title;
    private int updateType;
    private String uptime;
    private String url;

    public HotNews(Parcel parcel) {
        this.id = parcel.readLong();
        this.title = parcel.readString();
        this.ltitle = parcel.readString();
        this.keyword = parcel.readString();
        this.uptime = parcel.readString();
        this.createtime = parcel.readString();
        this.url = parcel.readString();
        this.isnew = parcel.readString();
        this.img = parcel.readString();
        this.cls = parcel.readString();
        this.idx = parcel.readString();
        this.pdate = parcel.readString();
        this.show = parcel.readString();
        this.domain = parcel.readString();
        this.site_name = parcel.readString();
        this.content = parcel.readString();
        this.m = parcel.readString();
        this.gk = parcel.readString();
        this.flag = parcel.readString();
        this.n_t = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public String getCls() {
        return this.cls;
    }

    public String getContent() {
        return this.content;
    }

    public String getCreatetime() {
        return this.createtime;
    }

    public String getDomain() {
        return this.domain;
    }

    public String getFlag() {
        return this.flag;
    }

    public String getGk() {
        return this.gk;
    }

    public long getId() {
        return this.id;
    }

    public String getIdx() {
        return this.idx;
    }

    public String getImg() {
        String[] split;
        return (!this.img.contains("|") || (split = this.img.split("\\|")) == null || split.length <= 0) ? this.img : split[0];
    }

    public String getIsnew() {
        return this.isnew;
    }

    public String getKeyword() {
        return this.keyword;
    }

    public String getLtitle() {
        return this.ltitle;
    }

    public String getM() {
        return this.m;
    }

    public int getN_t() {
        return this.n_t;
    }

    public String getPdate() {
        return this.pdate;
    }

    public String getShow() {
        return this.show;
    }

    public String getSite_name() {
        return this.site_name;
    }

    public String getTitle() {
        return this.title;
    }

    public int getUpdateType() {
        return this.updateType;
    }

    public String getUptime() {
        return this.uptime;
    }

    public String getUrl() {
        return this.url;
    }

    public void setCls(String str) {
        this.cls = str;
    }

    public void setContent(String str) {
        this.content = str;
    }

    public void setCreatetime(String str) {
        this.createtime = str;
    }

    public void setDomain(String str) {
        this.domain = str;
    }

    public void setFlag(String str) {
        this.flag = str;
    }

    public void setGk(String str) {
        this.gk = str;
    }

    public void setId(long j) {
        this.id = j;
    }

    public void setIdx(String str) {
        this.idx = str;
    }

    public void setImg(String str) {
        this.img = str;
    }

    public void setIsnew(String str) {
        this.isnew = str;
    }

    public void setKeyword(String str) {
        this.keyword = str;
    }

    public void setLtitle(String str) {
        this.ltitle = str;
    }

    public void setM(String str) {
        this.m = str;
    }

    public void setN_t(int i) {
        this.n_t = i;
    }

    public void setPdate(String str) {
        this.pdate = str;
    }

    public void setShow(String str) {
        this.show = str;
    }

    public void setSite_name(String str) {
        this.site_name = str;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public void setUpdateType(int i) {
        this.updateType = i;
    }

    public void setUptime(String str) {
        this.uptime = str;
    }

    public void setUrl(String str) {
        this.url = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.id);
        parcel.writeString(this.title);
        parcel.writeString(this.ltitle);
        parcel.writeString(this.keyword);
        parcel.writeString(this.uptime);
        parcel.writeString(this.createtime);
        parcel.writeString(this.url);
        parcel.writeString(this.isnew);
        parcel.writeString(this.img);
        parcel.writeString(this.cls);
        parcel.writeString(this.idx);
        parcel.writeString(this.pdate);
        parcel.writeString(this.show);
        parcel.writeString(this.domain);
        parcel.writeString(this.site_name);
        parcel.writeString(this.content);
        parcel.writeString(this.m);
        parcel.writeString(this.gk);
        parcel.writeString(this.flag);
        parcel.writeInt(this.n_t);
    }
}
