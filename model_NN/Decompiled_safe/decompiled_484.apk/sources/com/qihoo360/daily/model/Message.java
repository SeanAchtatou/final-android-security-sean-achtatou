package com.qihoo360.daily.model;

import java.io.Serializable;

public class Message implements Serializable {
    private String body;
    private String displayType;
    private String ts;

    public String getBody() {
        return this.body;
    }

    public String getDisplayType() {
        return this.displayType;
    }

    public String getTs() {
        return this.ts;
    }

    public void setBody(String str) {
        this.body = str;
    }

    public void setDisplayType(String str) {
        this.displayType = str;
    }

    public void setTs(String str) {
        this.ts = str;
    }
}
