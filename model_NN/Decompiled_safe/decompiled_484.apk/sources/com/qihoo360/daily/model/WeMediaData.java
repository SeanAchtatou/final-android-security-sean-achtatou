package com.qihoo360.daily.model;

import java.util.List;

public class WeMediaData {
    private String head;
    private String media_type;
    private List<WeMediaInfo> news;

    public String getHead() {
        return this.head;
    }

    public String getMedia_type() {
        return this.media_type;
    }

    public List<WeMediaInfo> getNews() {
        return this.news;
    }

    public void setHead(String str) {
        this.head = str;
    }

    public void setMedia_type(String str) {
        this.media_type = str;
    }

    public void setNews(List<WeMediaInfo> list) {
        this.news = list;
    }
}
