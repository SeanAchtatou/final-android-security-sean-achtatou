package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Template implements Parcelable {
    public static final Parcelable.Creator<Template> CREATOR = new Parcelable.Creator<Template>() {
        public Template createFromParcel(Parcel parcel) {
            return new Template(parcel);
        }

        public Template[] newArray(int i) {
            return new Template[i];
        }
    };
    private String f_name;
    private String f_url;
    private boolean isLoaded;
    private int t_code;
    private String t_name;

    public Template() {
    }

    private Template(Parcel parcel) {
        this.t_name = parcel.readString();
        this.t_code = parcel.readInt();
        this.f_name = parcel.readString();
        this.f_url = parcel.readString();
        this.isLoaded = Boolean.parseBoolean(parcel.readValue(Boolean.class.getClassLoader()).toString());
    }

    public int describeContents() {
        return 0;
    }

    public String getF_name() {
        return this.f_name;
    }

    public String getF_url() {
        return this.f_url;
    }

    public int getT_code() {
        return this.t_code;
    }

    public String getT_name() {
        return this.t_name;
    }

    public boolean isLoaded() {
        return this.isLoaded;
    }

    public void setF_name(String str) {
        this.f_name = str;
    }

    public void setF_url(String str) {
        this.f_url = str;
    }

    public void setLoaded(boolean z) {
        this.isLoaded = z;
    }

    public void setT_code(int i) {
        this.t_code = i;
    }

    public void setT_name(String str) {
        this.t_name = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.t_name);
        parcel.writeInt(this.t_code);
        parcel.writeString(this.f_name);
        parcel.writeString(this.f_url);
        parcel.writeValue(Boolean.valueOf(this.isLoaded));
    }
}
