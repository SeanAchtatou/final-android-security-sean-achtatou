package com.qihoo360.daily.model;

import java.io.Serializable;

public class Life implements Serializable {
    private static final long serialVersionUID = 1;
    private String date;
    private LifeInfo info;

    public String getDate() {
        return this.date;
    }

    public LifeInfo getInfo() {
        return this.info;
    }

    public void setDate(String str) {
        this.date = str;
    }

    public void setInfo(LifeInfo lifeInfo) {
        this.info = lifeInfo;
    }
}
