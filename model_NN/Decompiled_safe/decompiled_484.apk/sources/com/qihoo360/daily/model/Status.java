package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Status implements Parcelable {
    public static final Parcelable.Creator<Status> CREATOR = new Parcelable.Creator<Status>() {
        public Status createFromParcel(Parcel parcel) {
            return new Status(parcel);
        }

        public Status[] newArray(int i) {
            return new Status[i];
        }
    };
    private int comments_count;
    private String created_at;
    private boolean favorited;
    private long id;
    private String in_reply_to_screen_name;
    private String in_reply_to_status_id;
    private String in_reply_to_user_id;
    private String mid;
    private int reposts_count;
    private String source;
    private String text;
    private boolean truncated;

    public Status(Parcel parcel) {
        this.created_at = parcel.readString();
        this.id = parcel.readLong();
        this.text = parcel.readString();
        this.source = parcel.readString();
        boolean[] zArr = new boolean[2];
        parcel.readBooleanArray(zArr);
        this.favorited = zArr[0];
        this.truncated = zArr[1];
        this.in_reply_to_status_id = parcel.readString();
        this.in_reply_to_user_id = parcel.readString();
        this.in_reply_to_screen_name = parcel.readString();
        this.mid = parcel.readString();
        this.reposts_count = parcel.readInt();
        this.comments_count = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public int getComments_count() {
        return this.comments_count;
    }

    public String getCreated_at() {
        return this.created_at;
    }

    public long getId() {
        return this.id;
    }

    public String getIn_reply_to_screen_name() {
        return this.in_reply_to_screen_name;
    }

    public String getIn_reply_to_status_id() {
        return this.in_reply_to_status_id;
    }

    public String getIn_reply_to_user_id() {
        return this.in_reply_to_user_id;
    }

    public String getMid() {
        return this.mid;
    }

    public int getReposts_count() {
        return this.reposts_count;
    }

    public String getSource() {
        return this.source;
    }

    public String getText() {
        return this.text;
    }

    public boolean isFavorited() {
        return this.favorited;
    }

    public boolean isTruncated() {
        return this.truncated;
    }

    public void setComments_count(int i) {
        this.comments_count = i;
    }

    public void setCreated_at(String str) {
        this.created_at = str;
    }

    public void setFavorited(boolean z) {
        this.favorited = z;
    }

    public void setId(long j) {
        this.id = j;
    }

    public void setIn_reply_to_screen_name(String str) {
        this.in_reply_to_screen_name = str;
    }

    public void setIn_reply_to_status_id(String str) {
        this.in_reply_to_status_id = str;
    }

    public void setIn_reply_to_user_id(String str) {
        this.in_reply_to_user_id = str;
    }

    public void setMid(String str) {
        this.mid = str;
    }

    public void setReposts_count(int i) {
        this.reposts_count = i;
    }

    public void setSource(String str) {
        this.source = str;
    }

    public void setText(String str) {
        this.text = str;
    }

    public void setTruncated(boolean z) {
        this.truncated = z;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.created_at);
        parcel.writeLong(this.id);
        parcel.writeString(this.text);
        parcel.writeString(this.source);
        parcel.writeBooleanArray(new boolean[]{this.favorited, this.truncated});
        parcel.writeString(this.in_reply_to_status_id);
        parcel.writeString(this.in_reply_to_user_id);
        parcel.writeString(this.in_reply_to_screen_name);
        parcel.writeString(this.mid);
        parcel.writeInt(this.reposts_count);
        parcel.writeInt(this.comments_count);
    }
}
