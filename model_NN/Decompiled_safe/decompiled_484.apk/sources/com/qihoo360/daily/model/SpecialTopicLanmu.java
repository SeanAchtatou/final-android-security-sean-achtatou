package com.qihoo360.daily.model;

import java.util.List;

public class SpecialTopicLanmu extends Info {
    private String columnId;
    private String columnName;
    private List<Info> newsList;

    public String getColumnId() {
        return this.columnId;
    }

    public String getColumnName() {
        return this.columnName;
    }

    public List<Info> getNewsList() {
        return this.newsList;
    }
}
