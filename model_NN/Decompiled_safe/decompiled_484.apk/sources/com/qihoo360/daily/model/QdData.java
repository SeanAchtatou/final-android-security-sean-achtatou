package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class QdData implements Parcelable {
    public static final Parcelable.Creator<QdData> CREATOR = new Parcelable.Creator<QdData>() {
        public QdData createFromParcel(Parcel parcel) {
            return new QdData(parcel);
        }

        public QdData[] newArray(int i) {
            return new QdData[i];
        }
    };
    private String ad_JX_Enable;
    private NewsSuggest news_suggest;
    private Splash qd;

    public QdData() {
    }

    private QdData(Parcel parcel) {
        this.ad_JX_Enable = parcel.readString();
        this.qd = (Splash) parcel.readParcelable(Splash.class.getClassLoader());
        this.news_suggest = (NewsSuggest) parcel.readParcelable(NewsSuggest.class.getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    public String getAd_JX_Enable() {
        return this.ad_JX_Enable;
    }

    public NewsSuggest getNews_suggest() {
        return this.news_suggest;
    }

    public Splash getQd() {
        return this.qd;
    }

    public void setNews_suggest(NewsSuggest newsSuggest) {
        this.news_suggest = newsSuggest;
    }

    public void setQd(Splash splash) {
        this.qd = splash;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.ad_JX_Enable);
        parcel.writeParcelable(this.qd, 0);
        parcel.writeParcelable(this.news_suggest, 0);
    }
}
