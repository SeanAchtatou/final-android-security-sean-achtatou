package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Splash implements Parcelable {
    public static final Parcelable.Creator<Splash> CREATOR = new Parcelable.Creator<Splash>() {
        public Splash createFromParcel(Parcel parcel) {
            return new Splash(parcel);
        }

        public Splash[] newArray(int i) {
            return new Splash[i];
        }
    };
    private String imgurl;
    private String mzclick_url;
    private String mzshow_url;
    private int qd_type;
    private long servertime;
    private long showtime;
    private String url;

    public Splash() {
    }

    private Splash(Parcel parcel) {
        this.qd_type = parcel.readInt();
        this.imgurl = parcel.readString();
        this.url = parcel.readString();
        this.showtime = parcel.readLong();
        this.servertime = parcel.readLong();
        this.mzclick_url = parcel.readString();
        this.mzshow_url = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public String getImgurl() {
        return this.imgurl;
    }

    public String getMzclick_url() {
        return this.mzclick_url;
    }

    public String getMzshow_url() {
        return this.mzshow_url;
    }

    public int getQd_type() {
        return this.qd_type;
    }

    public long getServertime() {
        return this.servertime;
    }

    public long getShowtime() {
        return this.showtime;
    }

    public String getUrl() {
        return this.url;
    }

    public void setImgurl(String str) {
        this.imgurl = str;
    }

    public void setMzclick_url(String str) {
        this.mzclick_url = str;
    }

    public void setMzshow_url(String str) {
        this.mzshow_url = str;
    }

    public void setQd_type(int i) {
        this.qd_type = i;
    }

    public void setServertime(long j) {
        this.servertime = j;
    }

    public void setShowtime(long j) {
        this.showtime = j;
    }

    public void setUrl(String str) {
        this.url = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.qd_type);
        parcel.writeString(this.imgurl);
        parcel.writeString(this.url);
        parcel.writeLong(this.showtime);
        parcel.writeLong(this.servertime);
        parcel.writeString(this.mzclick_url);
        parcel.writeString(this.mzshow_url);
    }
}
