package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class NewsContent implements Parcelable {
    public static final Parcelable.Creator<NewsContent> CREATOR = new Parcelable.Creator<NewsContent>() {
        public NewsContent createFromParcel(Parcel parcel) {
            return new NewsContent(parcel);
        }

        public NewsContent[] newArray(int i) {
            return new NewsContent[i];
        }
    };
    private String subtype;
    private String type;
    private String value;

    public NewsContent() {
    }

    private NewsContent(Parcel parcel) {
        this.type = parcel.readString();
        this.subtype = parcel.readString();
        this.value = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public String getSubtype() {
        return this.subtype;
    }

    public String getType() {
        return this.type;
    }

    public String getValue() {
        return this.value;
    }

    public void setSubtype(String str) {
        this.subtype = str;
    }

    public void setType(String str) {
        this.type = str;
    }

    public void setValue(String str) {
        this.value = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.type);
        parcel.writeString(this.subtype);
        parcel.writeString(this.value);
    }
}
