package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class NewsSuggest implements Parcelable {
    public static final Parcelable.Creator<NewsSuggest> CREATOR = new Parcelable.Creator<NewsSuggest>() {
        public NewsSuggest createFromParcel(Parcel parcel) {
            return new NewsSuggest(parcel);
        }

        public NewsSuggest[] newArray(int i) {
            return new NewsSuggest[i];
        }
    };
    private String name;

    public NewsSuggest() {
    }

    private NewsSuggest(Parcel parcel) {
        this.name = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.name);
    }
}
