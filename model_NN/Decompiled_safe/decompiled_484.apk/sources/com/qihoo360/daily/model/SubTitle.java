package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.a.a.c.a;
import com.qihoo360.daily.activity.Application;
import com.qihoo360.daily.h.ax;

public class SubTitle implements Parcelable {
    public static final Parcelable.Creator<SubTitle> CREATOR = new Parcelable.Creator<SubTitle>() {
        public SubTitle createFromParcel(Parcel parcel) {
            return new SubTitle(parcel);
        }

        public SubTitle[] newArray(int i) {
            return new SubTitle[i];
        }
    };
    private int hot;
    private String title;

    public SubTitle() {
    }

    private SubTitle(Parcel parcel) {
        this.title = parcel.readString();
        this.hot = parcel.readInt();
    }

    public static SubTitle parseWiki(String str) {
        SubTitle subTitle;
        if (ax.a(str)) {
            return null;
        }
        try {
            subTitle = (SubTitle) Application.getGson().a(str, new a<SubTitle>() {
            }.getType());
        } catch (Exception e) {
            e.printStackTrace();
            subTitle = null;
        }
        return subTitle;
    }

    public int describeContents() {
        return 0;
    }

    public int getHot() {
        return this.hot;
    }

    public String getTitle() {
        return this.title;
    }

    public void setHot(int i) {
        this.hot = i;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public String toString() {
        return Application.getGson().a(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.title);
        parcel.writeInt(this.hot);
    }
}
