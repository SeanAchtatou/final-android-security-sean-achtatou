package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public class NewsDetail implements Parcelable {
    public static final Parcelable.Creator<NewsDetail> CREATOR = new Parcelable.Creator<NewsDetail>() {
        public NewsDetail createFromParcel(Parcel parcel) {
            return new NewsDetail(parcel);
        }

        public NewsDetail[] newArray(int i) {
            return new NewsDetail[i];
        }
    };
    private ArrayList<NewsContent> content;
    private String errno;
    private String lanmu;
    private String newtags;
    private String shorturl;
    private String slogan;
    private String source;
    private String subhead;
    private String time;
    private String title;
    private String url;
    private String wapurl;

    public NewsDetail() {
    }

    private NewsDetail(Parcel parcel) {
        this.title = parcel.readString();
        this.time = parcel.readString();
        this.wapurl = parcel.readString();
        this.errno = parcel.readString();
        this.url = parcel.readString();
        this.content = (ArrayList) parcel.readSerializable();
        this.source = parcel.readString();
        this.shorturl = parcel.readString();
        this.slogan = parcel.readString();
        this.subhead = parcel.readString();
        this.newtags = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public ArrayList<NewsContent> getContent() {
        return this.content;
    }

    public String getErrno() {
        return this.errno;
    }

    public String getLanmu() {
        return this.lanmu;
    }

    public String getNewtags() {
        return this.newtags;
    }

    public String getShorturl() {
        return this.shorturl;
    }

    public String getSlogan() {
        return this.slogan;
    }

    public String getSource() {
        return this.source;
    }

    public String getSubhead() {
        return this.subhead;
    }

    public String getTime() {
        return this.time;
    }

    public String getTitle() {
        return this.title;
    }

    public String getUrl() {
        return this.url;
    }

    public String getWapurl() {
        return this.wapurl;
    }

    public void setContent(ArrayList<NewsContent> arrayList) {
        this.content = arrayList;
    }

    public void setErrno(String str) {
        this.errno = str;
    }

    public void setLanmu(String str) {
        this.lanmu = str;
    }

    public void setNewtags(String str) {
        this.newtags = str;
    }

    public void setShorturl(String str) {
        this.shorturl = str;
    }

    public void setSlogan(String str) {
        this.slogan = str;
    }

    public void setSource(String str) {
        this.source = str;
    }

    public void setSubhead(String str) {
        this.subhead = str;
    }

    public void setTime(String str) {
        this.time = str;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public void setUrl(String str) {
        this.url = str;
    }

    public void setWapurl(String str) {
        this.wapurl = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.title);
        parcel.writeString(this.time);
        parcel.writeString(this.wapurl);
        parcel.writeString(this.errno);
        parcel.writeString(this.url);
        parcel.writeSerializable(this.content);
        parcel.writeString(this.source);
        parcel.writeString(this.shorturl);
        parcel.writeString(this.slogan);
        parcel.writeString(this.subhead);
        parcel.writeString(this.newtags);
    }
}
