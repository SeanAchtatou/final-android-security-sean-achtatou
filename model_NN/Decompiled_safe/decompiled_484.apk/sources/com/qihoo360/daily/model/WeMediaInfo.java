package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class WeMediaInfo extends Info {
    public static final Parcelable.Creator<WeMediaInfo> CREATOR = new Parcelable.Creator<WeMediaInfo>() {
        public WeMediaInfo createFromParcel(Parcel parcel) {
            return new WeMediaInfo(parcel);
        }

        public WeMediaInfo[] newArray(int i) {
            return new WeMediaInfo[i];
        }
    };
    private String m_type;
    private String slogen;
    private String user_img;
    private String user_name;

    public WeMediaInfo() {
    }

    public WeMediaInfo(Parcel parcel) {
        super(parcel);
        this.user_name = parcel.readString();
        this.user_img = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public String getM_type() {
        return this.m_type;
    }

    public String getSlogen() {
        return this.slogen;
    }

    public String getUser_img() {
        return this.user_img;
    }

    public String getUser_name() {
        return this.user_name;
    }

    public void setM_type(String str) {
        this.m_type = str;
    }

    public void setSlogen(String str) {
        this.slogen = str;
    }

    public void setUser_img(String str) {
        this.user_img = str;
    }

    public void setUser_name(String str) {
        this.user_name = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.user_name);
        parcel.writeString(this.user_img);
    }
}
