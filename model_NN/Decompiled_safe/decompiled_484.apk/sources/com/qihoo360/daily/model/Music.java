package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Music implements Parcelable {
    public static final Parcelable.Creator<Music> CREATOR = new Parcelable.Creator<Music>() {
        public Music createFromParcel(Parcel parcel) {
            return new Music(parcel);
        }

        public Music[] newArray(int i) {
            return new Music[i];
        }
    };
    private String head_songid;
    private String head_title;

    public Music() {
    }

    public Music(Parcel parcel) {
        this.head_songid = parcel.readString();
        this.head_title = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public String getHead_songid() {
        return this.head_songid;
    }

    public String getHead_title() {
        return this.head_title;
    }

    public void setHead_songid(String str) {
        this.head_songid = str;
    }

    public void setHead_title(String str) {
        this.head_title = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.head_songid);
        parcel.writeString(this.head_title);
    }
}
