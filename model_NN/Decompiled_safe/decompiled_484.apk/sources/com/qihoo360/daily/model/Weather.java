package com.qihoo360.daily.model;

import android.text.TextUtils;
import java.io.Serializable;

public class Weather implements Serializable {
    public static final int DEFAULT_WEATHER_BG_LEVEL = 0;
    private static final long serialVersionUID = 1;
    private long dataUptime;
    private String info;
    private String temperature;
    private String time;
    private int weatherBgLevel;

    public Weather() {
        this.info = "阴";
        this.temperature = "15";
        this.time = "";
        this.dataUptime = Long.MAX_VALUE;
    }

    public Weather(String str, String str2, String str3) {
        this.info = str;
        this.temperature = str2;
        this.time = str3;
        this.dataUptime = Long.MAX_VALUE;
    }

    public long getDataUptime() {
        return this.dataUptime;
    }

    public String getInfo() {
        return this.info;
    }

    public String getTemperature() {
        return this.temperature;
    }

    public String getTime() {
        return this.time;
    }

    public int getWeatherBgLevel() {
        if (TextUtils.isEmpty(this.info)) {
            this.weatherBgLevel = 0;
        }
        if (this.info.contains("晴")) {
            this.weatherBgLevel = 1;
        } else if (this.info.contains("阴")) {
            this.weatherBgLevel = 2;
        } else if (this.info.contains("云")) {
            this.weatherBgLevel = 3;
        } else if (this.info.contains("雾") || this.info.contains("霾")) {
            this.weatherBgLevel = 4;
        } else if (this.info.contains("沙") || this.info.contains("尘")) {
            this.weatherBgLevel = 6;
        } else if (this.info.contains("雨")) {
            this.weatherBgLevel = 8;
        } else if (this.info.contains("雪")) {
            this.weatherBgLevel = 9;
        }
        return this.weatherBgLevel;
    }

    public void merge(Weather weather) {
        if (weather != null) {
            this.info = weather.info;
            this.temperature = weather.temperature;
            this.time = weather.time;
            this.dataUptime = weather.dataUptime;
        }
    }

    public void setDataUptime(long j) {
        this.dataUptime = j;
    }

    public void setInfo(String str) {
        this.info = str;
    }

    public void setTemperature(String str) {
        this.temperature = str;
    }

    public void setTime(String str) {
        this.time = str;
    }
}
