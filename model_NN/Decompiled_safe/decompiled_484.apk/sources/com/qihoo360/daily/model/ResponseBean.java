package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ResponseBean<T extends Parcelable> implements Parcelable {
    public static final Parcelable.Creator<ResponseBean> CREATOR = new Parcelable.Creator<ResponseBean>() {
        public ResponseBean createFromParcel(Parcel parcel) {
            return new ResponseBean(parcel);
        }

        public ResponseBean[] newArray(int i) {
            return new ResponseBean[i];
        }
    };
    private T data;
    private int errno;
    private String json;
    private String wapurl;

    public ResponseBean(Parcel parcel) {
        this.errno = parcel.readInt();
        this.wapurl = parcel.readString();
        this.data = parcel.readParcelable(this.data.getClass().getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    public T getData() {
        return this.data;
    }

    public int getErrno() {
        return this.errno;
    }

    public String getJson() {
        return this.json;
    }

    public String getWapurl() {
        return this.wapurl;
    }

    public void setData(T t) {
        this.data = t;
    }

    public void setErrno(int i) {
        this.errno = i;
    }

    public void setJson(String str) {
        this.json = str;
    }

    public void setWapurl(String str) {
        this.wapurl = str;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void writeToParcel(android.os.Parcel r2, int r3) {
        /*
            r1 = this;
            int r0 = r1.errno
            r2.writeInt(r0)
            java.lang.String r0 = r1.wapurl
            r2.writeString(r0)
            T r0 = r1.data
            r0.writeToParcel(r2, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qihoo360.daily.model.ResponseBean.writeToParcel(android.os.Parcel, int):void");
    }
}
