package com.qihoo360.daily.model;

import java.io.Serializable;

public class WeatherDay implements Serializable {
    private static final long serialVersionUID = 1;
    private String date;
    private WeaterInfo info;

    public String getDate() {
        return this.date;
    }

    public WeaterInfo getInfo() {
        return this.info;
    }

    public void setDate(String str) {
        this.date = str;
    }

    public void setInfo(WeaterInfo weaterInfo) {
        this.info = weaterInfo;
    }
}
