package com.qihoo360.daily.model;

public class PushInfo extends Info {
    private String cmdType;
    private String content;
    private String entrance;
    private String getPushUrl;
    private int msgType;

    public String getCmdType() {
        return this.cmdType;
    }

    public String getContent() {
        return this.content;
    }

    public String getEntrance() {
        return this.entrance;
    }

    public String getGetPushUrl() {
        return this.getPushUrl;
    }

    public int getMsgType() {
        return this.msgType;
    }

    public void setCmdType(String str) {
        this.cmdType = str;
    }

    public void setContent(String str) {
        this.content = str;
    }

    public void setEntrance(String str) {
        this.entrance = str;
    }

    public void setGetPushUrl(String str) {
        this.getPushUrl = str;
    }

    public void setMsgType(int i) {
        this.msgType = i;
    }
}
