package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

public class NewComment implements Parcelable {
    public static final Parcelable.Creator<NewComment> CREATOR = new Parcelable.Creator<NewComment>() {
        public NewComment createFromParcel(Parcel parcel) {
            return new NewComment(parcel);
        }

        public NewComment[] newArray(int i) {
            return new NewComment[i];
        }
    };
    private String against;
    private List<String> chain;
    private List<NewComment> chain_comments;
    private String comment;
    private String comment_id;
    private String comments_on;
    private String create_time;
    private int diggok;
    private String key;
    private String layer;
    private String support;
    private String uavatar;
    private String uid;
    private String uid_type;
    private String uname;

    private NewComment(Parcel parcel) {
        this.comment = parcel.readString();
        this.comment_id = parcel.readString();
        this.comments_on = parcel.readString();
        this.key = parcel.readString();
        this.uid = parcel.readString();
        this.uid_type = parcel.readString();
        this.uname = parcel.readString();
        this.uavatar = parcel.readString();
        this.against = parcel.readString();
        this.create_time = parcel.readString();
        this.layer = parcel.readString();
        this.support = parcel.readString();
        this.diggok = parcel.readInt();
        this.chain = parcel.readArrayList(String.class.getClassLoader());
        this.chain_comments = parcel.readArrayList(NewComment.class.getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    public String getAgainst() {
        return this.against;
    }

    public List<String> getChain() {
        return this.chain;
    }

    public List<NewComment> getChain_comments() {
        return this.chain_comments;
    }

    public String getComment() {
        return this.comment;
    }

    public String getComment_id() {
        return this.comment_id;
    }

    public String getComments_on() {
        return this.comments_on;
    }

    public String getCreate_time() {
        return this.create_time;
    }

    public int getDiggok() {
        return this.diggok;
    }

    public String getKey() {
        return this.key;
    }

    public String getLayer() {
        return this.layer;
    }

    public String getSupport() {
        return this.support;
    }

    public String getUavatar() {
        return this.uavatar;
    }

    public String getUid() {
        return this.uid;
    }

    public String getUid_type() {
        return this.uid_type;
    }

    public String getUname() {
        return this.uname;
    }

    public void setAgainst(String str) {
        this.against = str;
    }

    public void setChain(List<String> list) {
        this.chain = list;
    }

    public void setChain_comments(List<NewComment> list) {
        this.chain_comments = list;
    }

    public void setComment(String str) {
        this.comment = str;
    }

    public void setComment_id(String str) {
        this.comment_id = str;
    }

    public void setComments_on(String str) {
        this.comments_on = str;
    }

    public void setCreate_time(String str) {
        this.create_time = str;
    }

    public void setDiggok(int i) {
        this.diggok = i;
    }

    public void setKey(String str) {
        this.key = str;
    }

    public void setLayer(String str) {
        this.layer = str;
    }

    public void setSupport(String str) {
        this.support = str;
    }

    public void setUavatar(String str) {
        this.uavatar = str;
    }

    public void setUid(String str) {
        this.uid = str;
    }

    public void setUid_type(String str) {
        this.uid_type = str;
    }

    public void setUname(String str) {
        this.uname = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.comment);
        parcel.writeString(this.comment_id);
        parcel.writeString(this.comments_on);
        parcel.writeString(this.key);
        parcel.writeString(this.uid);
        parcel.writeString(this.uid_type);
        parcel.writeString(this.uname);
        parcel.writeString(this.uavatar);
        parcel.writeString(this.against);
        parcel.writeString(this.create_time);
        parcel.writeString(this.layer);
        parcel.writeString(this.support);
        parcel.writeInt(this.diggok);
        parcel.writeList(this.chain);
        parcel.writeList(this.chain_comments);
    }
}
