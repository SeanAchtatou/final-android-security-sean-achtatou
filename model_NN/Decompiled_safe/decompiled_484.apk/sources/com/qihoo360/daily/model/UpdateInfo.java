package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public class UpdateInfo implements Parcelable {
    public static final Parcelable.Creator<UpdateInfo> CREATOR = new Parcelable.Creator<UpdateInfo>() {
        public UpdateInfo createFromParcel(Parcel parcel) {
            return new UpdateInfo(parcel);
        }

        public UpdateInfo[] newArray(int i) {
            return new UpdateInfo[i];
        }
    };
    private String apkURL;
    private String fileSize;
    private int forceUpdate;
    private String saveFileName;
    private String savePath;
    private List<String> updateContent = new ArrayList();
    private String updateMsg;
    private int versionCode;
    private String versionName;

    public UpdateInfo(Parcel parcel) {
        this.versionCode = parcel.readInt();
        this.versionName = parcel.readString();
        this.apkURL = parcel.readString();
        this.savePath = parcel.readString();
        this.saveFileName = parcel.readString();
        this.updateMsg = parcel.readString();
        this.fileSize = parcel.readString();
        parcel.readStringList(this.updateContent);
        this.forceUpdate = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public String getApkURL() {
        return this.apkURL;
    }

    public String getFileSize() {
        return this.fileSize;
    }

    public int getForceUpdate() {
        return this.forceUpdate;
    }

    public String getSaveFileName() {
        return this.saveFileName;
    }

    public String getSavePath() {
        return this.savePath;
    }

    public List<String> getUpdateContent() {
        return this.updateContent;
    }

    public String getUpdateMsg() {
        return this.updateMsg;
    }

    public int getVersionCode() {
        return this.versionCode;
    }

    public String getVersionName() {
        return this.versionName;
    }

    public void setApkURL(String str) {
        this.apkURL = str;
    }

    public void setFileSize(String str) {
        this.fileSize = str;
    }

    public void setForceUpdate(int i) {
        this.forceUpdate = i;
    }

    public void setSaveFileName(String str) {
        this.saveFileName = str;
    }

    public void setSavePath(String str) {
        this.savePath = str;
    }

    public void setUpdateContent(List<String> list) {
        this.updateContent = list;
    }

    public void setUpdateMsg(String str) {
        this.updateMsg = str;
    }

    public void setVersionCode(int i) {
        this.versionCode = i;
    }

    public void setVersionName(String str) {
        this.versionName = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.versionCode);
        parcel.writeString(this.versionName);
        parcel.writeString(this.apkURL);
        parcel.writeString(this.savePath);
        parcel.writeString(this.saveFileName);
        parcel.writeString(this.updateMsg);
        parcel.writeString(this.fileSize);
        parcel.writeStringList(this.updateContent);
        parcel.writeInt(this.forceUpdate);
    }
}
