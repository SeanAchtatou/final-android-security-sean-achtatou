package com.qihoo360.daily.model;

import java.util.List;

public class SpecialTopic extends Info {
    private Banner banner;
    private String shorturl;
    private List<SpecialTopicLanmu> topicdata;

    public Banner getBanner() {
        return this.banner;
    }

    public String getShorturl() {
        return this.shorturl;
    }

    public List<SpecialTopicLanmu> getTopicdata() {
        return this.topicdata;
    }

    public void setBanner(Banner banner2) {
        this.banner = banner2;
    }

    public void setShorturl(String str) {
        this.shorturl = str;
    }

    public void setTopicdata(List<SpecialTopicLanmu> list) {
        this.topicdata = list;
    }
}
