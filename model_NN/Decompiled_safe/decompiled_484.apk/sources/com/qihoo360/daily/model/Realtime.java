package com.qihoo360.daily.model;

import java.io.Serializable;

public class Realtime implements Serializable {
    private static final long serialVersionUID = 1;
    private String city_code;
    private String city_name;
    private long dataUptime;
    private String date;
    private String moon;
    private String time;
    private Weather weather;
    private String week;
    private Wind wind;

    public String getCity_code() {
        return this.city_code;
    }

    public String getCity_name() {
        return this.city_name;
    }

    public long getDataUptime() {
        return this.dataUptime;
    }

    public String getDate() {
        return this.date;
    }

    public String getMoon() {
        return this.moon;
    }

    public String getTime() {
        return this.time;
    }

    public Weather getWeather() {
        return this.weather;
    }

    public String getWeek() {
        return this.week;
    }

    public Wind getWind() {
        return this.wind;
    }

    public void setCity_code(String str) {
        this.city_code = str;
    }

    public void setCity_name(String str) {
        this.city_name = str;
    }

    public void setDataUptime(long j) {
        this.dataUptime = j;
    }

    public void setDate(String str) {
        this.date = str;
    }

    public void setMoon(String str) {
        this.moon = str;
    }

    public void setTime(String str) {
        this.time = str;
    }

    public void setWeather(Weather weather2) {
        this.weather = weather2;
    }

    public void setWeek(String str) {
        this.week = str;
    }

    public void setWind(Wind wind2) {
        this.wind = wind2;
    }
}
