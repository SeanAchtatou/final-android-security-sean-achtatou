package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.qihoo.video.DateUtil;
import com.qihoo360.daily.h.b;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FavouriteInfo extends Info implements Parcelable {
    public static final Parcelable.Creator<FavouriteInfo> CREATOR = new Parcelable.Creator<FavouriteInfo>() {
        public FavouriteInfo createFromParcel(Parcel parcel) {
            return new FavouriteInfo(parcel);
        }

        public FavouriteInfo[] newArray(int i) {
            return new FavouriteInfo[i];
        }
    };
    private String create_time;
    private String pos;

    public FavouriteInfo() {
    }

    public FavouriteInfo(Parcel parcel) {
        super(parcel);
        this.pos = parcel.readString();
        this.create_time = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public String getCreate_time() {
        return this.create_time;
    }

    public String getFavoTime() {
        if (TextUtils.isEmpty(this.create_time)) {
            return null;
        }
        if (b.g(this.create_time)) {
            return new SimpleDateFormat(DateUtil.FORMAT_DEFAULT, Locale.CHINA).format(new Date(Long.valueOf(this.create_time + "000").longValue()));
        }
        String trim = this.create_time.trim();
        String[] split = trim.split(" ");
        return split.length != 0 ? split[0] : trim;
    }

    public String getPos() {
        return this.pos;
    }

    public void setCreate_time(String str) {
        this.create_time = str;
    }

    public void setPos(String str) {
        this.pos = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.pos);
        parcel.writeString(this.create_time);
    }
}
