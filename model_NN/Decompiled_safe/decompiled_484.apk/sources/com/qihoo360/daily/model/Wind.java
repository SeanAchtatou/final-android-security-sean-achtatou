package com.qihoo360.daily.model;

import java.io.Serializable;

public class Wind implements Serializable {
    private static final long serialVersionUID = 1;
    private String direct;
    private String offset;
    private String power;
    private String windspeed;

    public String getDirect() {
        return this.direct;
    }

    public String getOffset() {
        return this.offset;
    }

    public String getPower() {
        return this.power;
    }

    public String getWindspeed() {
        return this.windspeed;
    }

    public void setDirect(String str) {
        this.direct = str;
    }

    public void setOffset(String str) {
        this.offset = str;
    }

    public void setPower(String str) {
        this.power = str;
    }

    public void setWindspeed(String str) {
        this.windspeed = str;
    }
}
