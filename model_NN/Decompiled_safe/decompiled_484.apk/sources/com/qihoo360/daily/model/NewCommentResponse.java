package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public class NewCommentResponse implements Parcelable {
    public static final Parcelable.Creator<NewCommentResponse> CREATOR = new Parcelable.Creator<NewCommentResponse>() {
        public NewCommentResponse createFromParcel(Parcel parcel) {
            return new NewCommentResponse(parcel);
        }

        public NewCommentResponse[] newArray(int i) {
            return new NewCommentResponse[i];
        }
    };
    private List<NewComment> chain_message;
    private int comment_num;
    private List<NewComment> comments;
    private List<NewComment> hot_comments;
    private String key;

    public NewCommentResponse() {
    }

    private NewCommentResponse(Parcel parcel) {
        this.chain_message = parcel.readArrayList(NewComment.class.getClassLoader());
        this.comments = parcel.readArrayList(NewComment.class.getClassLoader());
        this.hot_comments = parcel.readArrayList(NewComment.class.getClassLoader());
        this.key = parcel.readString();
        this.comment_num = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public List<NewComment> getChain_message() {
        return this.chain_message;
    }

    public int getComment_num() {
        return this.comment_num;
    }

    public List<NewComment> getComments() {
        return this.comments;
    }

    public List<NewComment> getHot_comments() {
        return this.hot_comments;
    }

    public String getKey() {
        return this.key;
    }

    public void setChain_message(List<NewComment> list) {
        this.chain_message = list;
    }

    public void setComment_num(int i) {
        this.comment_num = i;
    }

    public void setComments(ArrayList<NewComment> arrayList) {
        this.comments = arrayList;
    }

    public void setHot_comments(List<NewComment> list) {
        this.hot_comments = list;
    }

    public void setKey(String str) {
        this.key = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(this.chain_message);
        parcel.writeList(this.comments);
        parcel.writeList(this.hot_comments);
        parcel.writeString(this.key);
        parcel.writeInt(this.comment_num);
    }
}
