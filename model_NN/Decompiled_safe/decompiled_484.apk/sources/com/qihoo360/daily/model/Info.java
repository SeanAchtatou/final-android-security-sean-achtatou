package com.qihoo360.daily.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.mediav.ads.sdk.adcore.Config;
import com.qihoo360.daily.h.ae;
import java.util.ArrayList;
import java.util.List;

public class Info implements Parcelable {
    public static final Parcelable.Creator<Info> CREATOR = new Parcelable.Creator<Info>() {
        public Info createFromParcel(Parcel parcel) {
            return new Info(parcel);
        }

        public Info[] newArray(int i) {
            return new Info[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    protected String f1155a;
    private String android_channeltarget;
    private String android_channeltargetname;
    protected String bury;
    protected String c_pos;
    protected String channelId;
    protected String cmt_cnt;
    private String conhotwords;
    private String conhotwordstime;
    protected String digg;
    protected int digg_type;
    protected String duration;
    private String editor_desc;
    private String editor_id;
    private String editor_name;
    private String editor_pic;
    private List<Info> extnews;
    private List<HotSearchItem> hot_search;
    private String hotlabel;
    protected int imageSet = -1;
    protected String imgurl;
    @ae
    private boolean isCardTail;
    @ae
    private boolean isSubNews;
    protected String keywords;
    protected String m;
    protected String n_t;
    private String newstag;
    protected String newtags;
    protected String newtrans;
    protected String nid;
    protected String nocmt;
    protected String page_template;
    protected int paper;
    protected String pdate;
    protected int read;
    @ae
    public boolean recommendCardHeadFlag;
    @ae
    private boolean seekMoreAttached;
    protected String src;
    protected String status;
    protected String summary;
    protected long timeOrder;
    protected String title;
    protected String type;
    protected String url;
    protected int v_t;
    protected int view;

    public Info() {
    }

    public Info(Parcel parcel) {
        boolean z = true;
        this.type = parcel.readString();
        this.title = parcel.readString();
        this.url = parcel.readString();
        this.nid = parcel.readString();
        this.c_pos = parcel.readString();
        this.f1155a = parcel.readString();
        this.pdate = parcel.readString();
        this.src = parcel.readString();
        this.keywords = parcel.readString();
        this.n_t = parcel.readString();
        this.m = parcel.readString();
        this.imgurl = parcel.readString();
        this.digg = parcel.readString();
        this.bury = parcel.readString();
        this.digg_type = parcel.readInt();
        this.cmt_cnt = parcel.readString();
        this.v_t = parcel.readInt();
        this.summary = parcel.readString();
        this.status = parcel.readString();
        this.nocmt = parcel.readString();
        this.timeOrder = parcel.readLong();
        this.duration = parcel.readString();
        this.channelId = parcel.readString();
        this.read = parcel.readInt();
        this.paper = parcel.readInt();
        this.imageSet = parcel.readInt();
        this.newtags = parcel.readString();
        this.newtrans = parcel.readString();
        this.page_template = parcel.readString();
        this.view = parcel.readInt();
        this.editor_id = parcel.readString();
        this.editor_name = parcel.readString();
        this.editor_pic = parcel.readString();
        this.editor_desc = parcel.readString();
        this.hotlabel = parcel.readString();
        this.conhotwords = parcel.readString();
        this.isSubNews = parcel.readInt() == 1;
        this.isCardTail = parcel.readInt() != 1 ? false : z;
        this.hot_search = new ArrayList();
        parcel.readList(this.hot_search, HotSearchItem.class.getClassLoader());
        this.newstag = parcel.readString();
        this.conhotwordstime = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public String getA() {
        return this.f1155a;
    }

    public String getAndroid_channeltarget() {
        return this.android_channeltarget;
    }

    public String getAndroid_channeltargetname() {
        return this.android_channeltargetname;
    }

    public String getBury() {
        return this.bury;
    }

    public String getC_pos() {
        return this.c_pos;
    }

    public String getChannelId() {
        return this.channelId;
    }

    public String getCmt_cnt() {
        return this.cmt_cnt;
    }

    public String getConhotwords() {
        return this.conhotwords;
    }

    public String getConhotwordstime() {
        return this.conhotwordstime;
    }

    public String getDigg() {
        return this.digg;
    }

    public int getDigg_type() {
        return this.digg_type;
    }

    public String getDuration() {
        return this.duration;
    }

    public String getEditor_desc() {
        return this.editor_desc;
    }

    public String getEditor_id() {
        return this.editor_id;
    }

    public String getEditor_name() {
        return this.editor_name;
    }

    public String getEditor_pic() {
        return this.editor_pic;
    }

    public List<Info> getExtnews() {
        return this.extnews;
    }

    public List<HotSearchItem> getHot_search() {
        return this.hot_search;
    }

    public String getHotlabel() {
        return this.hotlabel;
    }

    public int getImageSet() {
        return this.imageSet;
    }

    public String getImgurl() {
        return this.imgurl;
    }

    public String getKeywords() {
        return this.keywords;
    }

    public String getM() {
        return this.m;
    }

    public String getN_t() {
        return this.n_t;
    }

    public String getNewstag() {
        return this.newstag;
    }

    public String getNewtags() {
        return this.newtags;
    }

    public String getNewtrans() {
        return this.newtrans;
    }

    public String getNid() {
        return this.nid;
    }

    public String getNocmt() {
        return this.nocmt;
    }

    public String getPage_template() {
        return this.page_template;
    }

    public int getPaper() {
        return this.paper;
    }

    public String getPdate() {
        return this.pdate;
    }

    public int getRead() {
        return this.read;
    }

    public String getSrc() {
        return this.src;
    }

    public String getStatus() {
        return this.status;
    }

    public String getSummary() {
        return this.summary;
    }

    public long getTimeOrder() {
        return this.timeOrder;
    }

    public String getTitle() {
        return this.title;
    }

    public String getType() {
        return this.type;
    }

    public String getUrl() {
        return this.url;
    }

    public int getV_t() {
        return this.v_t;
    }

    public int getView() {
        return this.view;
    }

    public boolean isCardTail() {
        return this.isCardTail;
    }

    public boolean isDailyInfo() {
        return Config.CHANNEL_ID.equals(this.newtrans);
    }

    public boolean isDeepRead() {
        return (!isDailyInfo() || 100 == this.paper || 101 == this.paper) ? false : true;
    }

    public boolean isFromFunAutomatic() {
        return 100 == this.paper && isDailyInfo();
    }

    public boolean isSeekMoreAttached() {
        return this.seekMoreAttached;
    }

    public boolean isSubNews() {
        return this.isSubNews;
    }

    public void setA(String str) {
        this.f1155a = str;
    }

    public void setAndroid_channeltarget(String str) {
        this.android_channeltarget = str;
    }

    public void setAndroid_channeltargetname(String str) {
        this.android_channeltargetname = str;
    }

    public void setBury(String str) {
        this.bury = str;
    }

    public void setC_pos(String str) {
        this.c_pos = str;
    }

    public void setCardTail(boolean z) {
        this.isCardTail = z;
    }

    public void setChannelId(String str) {
        this.channelId = str;
    }

    public void setCmt_cnt(String str) {
        this.cmt_cnt = str;
    }

    public void setConhotwords(String str) {
        this.conhotwords = str;
    }

    public void setConhotwordstime(String str) {
        this.conhotwordstime = str;
    }

    public void setDigg(String str) {
        this.digg = str;
    }

    public void setDigg_type(int i) {
        this.digg_type = i;
    }

    public void setDuration(String str) {
        this.duration = str;
    }

    public void setEditor_desc(String str) {
        this.editor_desc = str;
    }

    public void setEditor_id(String str) {
        this.editor_id = str;
    }

    public void setEditor_name(String str) {
        this.editor_name = str;
    }

    public void setEditor_pic(String str) {
        this.editor_pic = str;
    }

    public void setExtnews(List<Info> list) {
        this.extnews = list;
    }

    public void setHot_search(List<HotSearchItem> list) {
        this.hot_search = list;
    }

    public void setHotlabel(String str) {
        this.hotlabel = str;
    }

    public void setImageSet(int i) {
        this.imageSet = i;
    }

    public void setImgurl(String str) {
        this.imgurl = str;
    }

    public void setKeywords(String str) {
        this.keywords = str;
    }

    public void setM(String str) {
        this.m = str;
    }

    public void setN_t(String str) {
        this.n_t = str;
    }

    public void setNewstag(String str) {
        this.newstag = str;
    }

    public void setNewtags(String str) {
        this.newtags = str;
    }

    public void setNewtrans(String str) {
        this.newtrans = str;
    }

    public void setNid(String str) {
        this.nid = str;
    }

    public void setNocmt(String str) {
        this.nocmt = str;
    }

    public void setPage_template(String str) {
        this.page_template = str;
    }

    public void setPaper(int i) {
        this.paper = i;
    }

    public void setPdate(String str) {
        this.pdate = str;
    }

    public void setRead(int i) {
        this.read = i;
    }

    public void setSeekMoreAttached(boolean z) {
        this.seekMoreAttached = z;
    }

    public void setSrc(String str) {
        this.src = str;
    }

    public void setStatus(String str) {
        this.status = str;
    }

    public void setSubNews(boolean z) {
        this.isSubNews = z;
    }

    public void setSummary(String str) {
        this.summary = str;
    }

    public void setTimeOrder(long j) {
        this.timeOrder = j;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public void setType(String str) {
        this.type = str;
    }

    public void setUrl(String str) {
        this.url = str;
    }

    public void setV_t(int i) {
        this.v_t = i;
    }

    public void setView(int i) {
        this.view = i;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int i2 = 1;
        parcel.writeString(this.type);
        parcel.writeString(this.title);
        parcel.writeString(this.url);
        parcel.writeString(this.nid);
        parcel.writeString(this.c_pos);
        parcel.writeString(this.f1155a);
        parcel.writeString(this.pdate);
        parcel.writeString(this.src);
        parcel.writeString(this.keywords);
        parcel.writeString(this.n_t);
        parcel.writeString(this.m);
        parcel.writeString(this.imgurl);
        parcel.writeString(this.digg);
        parcel.writeString(this.bury);
        parcel.writeInt(this.digg_type);
        parcel.writeString(this.cmt_cnt);
        parcel.writeInt(this.v_t);
        parcel.writeString(this.summary);
        parcel.writeString(this.status);
        parcel.writeString(this.nocmt);
        parcel.writeLong(this.timeOrder);
        parcel.writeString(this.duration);
        parcel.writeString(this.channelId);
        parcel.writeInt(this.read);
        parcel.writeInt(this.paper);
        parcel.writeInt(this.imageSet);
        parcel.writeString(this.newtags);
        parcel.writeString(this.newtrans);
        parcel.writeString(this.page_template);
        parcel.writeInt(this.view);
        parcel.writeString(this.editor_id);
        parcel.writeString(this.editor_name);
        parcel.writeString(this.editor_pic);
        parcel.writeString(this.editor_desc);
        parcel.writeString(this.hotlabel);
        parcel.writeString(this.conhotwords);
        parcel.writeInt(this.isSubNews ? 1 : 0);
        if (!this.isCardTail) {
            i2 = 0;
        }
        parcel.writeInt(i2);
        parcel.writeList(this.hot_search);
        parcel.writeString(this.newstag);
        parcel.writeString(this.conhotwordstime);
    }
}
