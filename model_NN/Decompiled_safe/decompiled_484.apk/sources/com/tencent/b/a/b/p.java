package com.tencent.b.a.b;

import java.io.File;

final class p {

    /* renamed from: a  reason: collision with root package name */
    private static int f1316a = -1;

    public static boolean a() {
        if (f1316a == 1) {
            return true;
        }
        if (f1316a == 0) {
            return false;
        }
        String[] strArr = {"/bin", "/system/bin/", "/system/xbin/", "/system/sbin/", "/sbin/", "/vendor/bin/"};
        int i = 0;
        while (i < strArr.length) {
            try {
                if (new File(strArr[i] + "su").exists()) {
                    f1316a = 1;
                    return true;
                }
                i++;
            } catch (Exception e) {
            }
        }
        f1316a = 0;
        return false;
    }
}
