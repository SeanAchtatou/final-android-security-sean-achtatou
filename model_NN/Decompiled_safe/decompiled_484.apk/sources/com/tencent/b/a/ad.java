package com.tencent.b.a;

import android.content.Context;
import com.tencent.b.a.a.g;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;

final class ad implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private Context f1279a = null;

    /* renamed from: b  reason: collision with root package name */
    private Map<String, Integer> f1280b = null;
    private w c = null;

    public ad(Context context) {
        this.f1279a = context;
        this.c = null;
    }

    private static s a(String str, int i) {
        s sVar = new s();
        Socket socket = new Socket();
        int i2 = 0;
        try {
            sVar.a(str);
            sVar.b(i);
            long currentTimeMillis = System.currentTimeMillis();
            InetSocketAddress inetSocketAddress = new InetSocketAddress(str, i);
            socket.connect(inetSocketAddress, 30000);
            sVar.a(System.currentTimeMillis() - currentTimeMillis);
            sVar.b(inetSocketAddress.getAddress().getHostAddress());
            socket.close();
            try {
                socket.close();
            } catch (Throwable th) {
                v.q.b(th);
            }
        } catch (IOException e) {
            IOException iOException = e;
            i2 = -1;
            v.q.b((Throwable) iOException);
            socket.close();
        } catch (Throwable th2) {
            v.q.b(th2);
        }
        sVar.a(i2);
        return sVar;
    }

    private static Map<String, Integer> a() {
        String str;
        HashMap hashMap = new HashMap();
        String a2 = t.a("__MTA_TEST_SPEED__");
        if (!(a2 == null || a2.trim().length() == 0)) {
            for (String split : a2.split(";")) {
                String[] split2 = split.split(",");
                if (!(split2 == null || split2.length != 2 || (str = split2[0]) == null || str.trim().length() == 0)) {
                    try {
                        hashMap.put(str, Integer.valueOf(Integer.valueOf(split2[1]).intValue()));
                    } catch (NumberFormatException e) {
                        v.q.b((Throwable) e);
                    }
                }
            }
        }
        return hashMap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.b.a.v.a(android.content.Context, boolean, com.tencent.b.a.w):int
     arg types: [android.content.Context, int, com.tencent.b.a.w]
     candidates:
      com.tencent.b.a.v.a(android.content.Context, java.lang.String, java.lang.String):boolean
      com.tencent.b.a.v.a(android.content.Context, boolean, com.tencent.b.a.w):int */
    public final void run() {
        try {
            if (this.f1280b == null) {
                this.f1280b = a();
            }
            if (this.f1280b == null || this.f1280b.size() == 0) {
                v.q.a("empty domain list.");
                return;
            }
            JSONArray jSONArray = new JSONArray();
            for (Map.Entry next : this.f1280b.entrySet()) {
                String str = (String) next.getKey();
                if (str == null || str.length() == 0) {
                    v.q.c("empty domain name.");
                } else if (((Integer) next.getValue()) == null) {
                    v.q.c("port is null for " + str);
                } else {
                    jSONArray.put(a((String) next.getKey(), ((Integer) next.getValue()).intValue()).a());
                }
            }
            if (jSONArray.length() != 0) {
                g gVar = new g(this.f1279a, v.a(this.f1279a, false, this.c), this.c);
                gVar.a(jSONArray.toString());
                new ae(gVar).a();
            }
        } catch (Throwable th) {
            v.q.b(th);
        }
    }
}
