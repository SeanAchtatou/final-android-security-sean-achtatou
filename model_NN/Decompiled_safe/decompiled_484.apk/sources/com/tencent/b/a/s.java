package com.tencent.b.a;

import org.json.JSONException;
import org.json.JSONObject;

public final class s {

    /* renamed from: a  reason: collision with root package name */
    private long f1341a = 0;

    /* renamed from: b  reason: collision with root package name */
    private int f1342b = 0;
    private String c = "";
    private int d = 0;
    private String e = "";

    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("tm", this.f1341a);
            jSONObject.put("st", this.f1342b);
            if (this.c != null) {
                jSONObject.put("dm", this.c);
            }
            jSONObject.put("pt", this.d);
            if (this.e != null) {
                jSONObject.put("rip", this.e);
            }
            jSONObject.put("ts", System.currentTimeMillis() / 1000);
        } catch (JSONException e2) {
        }
        return jSONObject;
    }

    public final void a(int i) {
        this.f1342b = i;
    }

    public final void a(long j) {
        this.f1341a = j;
    }

    public final void a(String str) {
        this.c = str;
    }

    public final void b(int i) {
        this.d = i;
    }

    public final void b(String str) {
        this.e = str;
    }
}
