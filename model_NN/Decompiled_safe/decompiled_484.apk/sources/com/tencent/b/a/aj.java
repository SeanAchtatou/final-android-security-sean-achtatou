package com.tencent.b.a;

import java.util.List;

final class aj implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f1288a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ int f1289b = 1;
    final /* synthetic */ boolean c;
    final /* synthetic */ boolean d;
    final /* synthetic */ ai e;

    aj(ai aiVar, List list, boolean z) {
        this.e = aiVar;
        this.f1288a = list;
        this.c = z;
        this.d = true;
    }

    public final void run() {
        this.e.a(this.f1288a, this.f1289b, this.c);
        if (this.d) {
            this.f1288a.clear();
        }
    }
}
