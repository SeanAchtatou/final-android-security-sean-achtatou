package com.tencent.b.a;

import android.content.Context;
import com.qihoo.messenger.util.QDefine;
import com.tencent.b.a.b.l;
import java.util.Timer;

public class g {

    /* renamed from: b  reason: collision with root package name */
    private static volatile g f1326b = null;

    /* renamed from: a  reason: collision with root package name */
    private Timer f1327a = null;
    /* access modifiers changed from: private */
    public Context c = null;

    private g(Context context) {
        this.c = context.getApplicationContext();
        this.f1327a = new Timer(false);
    }

    public static g a(Context context) {
        if (f1326b == null) {
            synchronized (g.class) {
                if (f1326b == null) {
                    f1326b = new g(context);
                }
            }
        }
        return f1326b;
    }

    public final void a() {
        if (t.a() == u.PERIOD) {
            long l = (long) (t.l() * 60 * QDefine.ONE_SECOND);
            if (t.b()) {
                l.c().a("setupPeriodTimer delay:" + l);
            }
            h hVar = new h(this);
            if (this.f1327a != null) {
                if (t.b()) {
                    l.c().a("setupPeriodTimer schedule delay:" + l);
                }
                this.f1327a.schedule(hVar, l);
            } else if (t.b()) {
                l.c().c("setupPeriodTimer schedule timer == null");
            }
        }
    }
}
