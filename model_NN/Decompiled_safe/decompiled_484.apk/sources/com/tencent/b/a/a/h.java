package com.tencent.b.a.a;

import android.content.Context;
import com.tencent.b.a.b.d;
import com.tencent.b.a.b.l;
import com.tencent.b.a.w;
import org.json.JSONObject;

public final class h extends d {

    /* renamed from: a  reason: collision with root package name */
    private d f1275a;
    private JSONObject m = null;

    public h(Context context, int i, JSONObject jSONObject, w wVar) {
        super(context, i, wVar);
        this.f1275a = new d(context);
        this.m = jSONObject;
    }

    public final boolean a(JSONObject jSONObject) {
        if (this.e != null) {
            jSONObject.put("ut", this.e.d());
        }
        if (this.m != null) {
            jSONObject.put("cfg", this.m);
        }
        if (l.u(this.l)) {
            jSONObject.put("ncts", 1);
        }
        this.f1275a.a(jSONObject, null);
        return true;
    }

    public final e b() {
        return e.SESSION_ENV;
    }
}
