package com.tencent.b.a;

import android.content.Context;
import android.content.IntentFilter;
import com.tencent.b.a.b.b;
import com.tencent.b.a.b.f;
import com.tencent.b.a.b.l;
import com.tencent.b.a.b.r;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;
import org.apache.http.HttpHost;
import org.json.JSONObject;

public class x {
    private static x g = null;

    /* renamed from: a  reason: collision with root package name */
    private List<String> f1351a = null;

    /* renamed from: b  reason: collision with root package name */
    private volatile int f1352b = 2;
    private volatile String c = "";
    private volatile HttpHost d = null;
    /* access modifiers changed from: private */
    public f e = null;
    private int f = 0;
    private Context h = null;
    private b i = null;

    private x(Context context) {
        this.h = context.getApplicationContext();
        this.e = new f();
        l.a(context);
        this.i = l.c();
        j();
        this.f1351a = new ArrayList(10);
        this.f1351a.add("117.135.169.101");
        this.f1351a.add("140.207.54.125");
        this.f1351a.add("180.153.8.53");
        this.f1351a.add("120.198.203.175");
        this.f1351a.add("14.17.43.18");
        this.f1351a.add("163.177.71.186");
        this.f1351a.add("111.30.131.31");
        this.f1351a.add("123.126.121.167");
        this.f1351a.add("123.151.152.111");
        this.f1351a.add("113.142.45.79");
        this.f1351a.add("123.138.162.90");
        this.f1351a.add("103.7.30.94");
        g();
    }

    public static x a(Context context) {
        if (g == null) {
            synchronized (x.class) {
                if (g == null) {
                    g = new x(context);
                }
            }
        }
        return g;
    }

    private static boolean b(String str) {
        return Pattern.compile("(2[5][0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})").matcher(str).matches();
    }

    private String i() {
        try {
            if (!b("pingma.qq.com")) {
                return InetAddress.getByName("pingma.qq.com").getHostAddress();
            }
        } catch (Exception e2) {
            this.i.b((Throwable) e2);
        }
        return "";
    }

    private void j() {
        this.f1352b = 0;
        this.d = null;
        this.c = null;
    }

    public final HttpHost a() {
        return this.d;
    }

    public final void a(String str) {
        if (t.b()) {
            this.i.a("updateIpList " + str);
        }
        try {
            if (l.c(str)) {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.length() > 0) {
                    Iterator<String> keys = jSONObject.keys();
                    while (keys.hasNext()) {
                        String string = jSONObject.getString(keys.next());
                        if (l.c(string)) {
                            for (String str2 : string.split(";")) {
                                if (l.c(str2)) {
                                    String[] split = str2.split(":");
                                    if (split.length > 1) {
                                        String str3 = split[0];
                                        if (b(str3) && !this.f1351a.contains(str3)) {
                                            if (t.b()) {
                                                this.i.a("add new ip:" + str3);
                                            }
                                            this.f1351a.add(str3);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e2) {
            this.i.b((Throwable) e2);
        }
        this.f = new Random().nextInt(this.f1351a.size());
    }

    public final String b() {
        return this.c;
    }

    public final int c() {
        return this.f1352b;
    }

    public final void d() {
        this.f = (this.f + 1) % this.f1351a.size();
    }

    public final boolean e() {
        return this.f1352b == 1;
    }

    public final boolean f() {
        return this.f1352b != 0;
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        String str;
        if (r.e(this.h)) {
            if (t.g) {
                String i2 = i();
                if (t.b()) {
                    this.i.a("remoteIp ip is " + i2);
                }
                if (l.c(i2)) {
                    if (this.f1351a.contains(i2)) {
                        str = i2;
                    } else {
                        str = this.f1351a.get(this.f);
                        if (t.b()) {
                            this.i.c(i2 + " not in ip list, change to:" + str);
                        }
                    }
                    t.c("http://" + str + ":80/mstat/report");
                }
            }
            this.c = l.j(this.h);
            if (t.b()) {
                this.i.a("NETWORK name:" + this.c);
            }
            if (l.c(this.c)) {
                if ("WIFI".equalsIgnoreCase(this.c)) {
                    this.f1352b = 1;
                } else {
                    this.f1352b = 2;
                }
                this.d = l.a(this.h);
            }
            if (v.a()) {
                v.a(this.h);
                return;
            }
            return;
        }
        if (t.b()) {
            this.i.a("NETWORK TYPE: network is close.");
        }
        j();
    }

    public final void h() {
        this.h.getApplicationContext().registerReceiver(new ao(this), new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }
}
