package com.tencent.b.a.a;

import android.content.Context;
import com.tencent.b.a.b.l;
import com.tencent.b.a.b.r;
import com.tencent.b.a.w;
import com.tencent.b.a.x;
import org.json.JSONObject;

public final class g extends d {

    /* renamed from: a  reason: collision with root package name */
    private static String f1274a = null;
    private String m = null;
    private String n = null;

    public g(Context context, int i, w wVar) {
        super(context, i, wVar);
        this.m = x.a(context).b();
        if (f1274a == null) {
            f1274a = l.h(context);
        }
    }

    public final void a(String str) {
        this.n = str;
    }

    public final boolean a(JSONObject jSONObject) {
        r.a(jSONObject, "op", f1274a);
        r.a(jSONObject, "cn", this.m);
        jSONObject.put("sp", this.n);
        return true;
    }

    public final e b() {
        return e.NETWORK_MONITOR;
    }
}
