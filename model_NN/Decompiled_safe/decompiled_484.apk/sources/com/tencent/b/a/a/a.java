package com.tencent.b.a.a;

import android.content.Context;
import com.tencent.b.a.v;
import com.tencent.b.a.w;
import java.util.Map;
import java.util.Properties;
import org.json.JSONException;
import org.json.JSONObject;

public final class a extends d {

    /* renamed from: a  reason: collision with root package name */
    protected b f1265a = new b();
    private long m = -1;

    public a(Context context, int i, String str, w wVar) {
        super(context, i, wVar);
        this.f1265a.f1266a = str;
    }

    public final b a() {
        return this.f1265a;
    }

    public final boolean a(JSONObject jSONObject) {
        Properties a2;
        jSONObject.put("ei", this.f1265a.f1266a);
        if (this.m > 0) {
            jSONObject.put("du", this.m);
        }
        if (this.f1265a.f1267b == null) {
            if (!(this.f1265a.f1266a == null || (a2 = v.a(this.f1265a.f1266a)) == null || a2.size() <= 0)) {
                if (this.f1265a.c == null || this.f1265a.c.length() == 0) {
                    this.f1265a.c = new JSONObject(a2);
                } else {
                    for (Map.Entry entry : a2.entrySet()) {
                        try {
                            this.f1265a.c.put(entry.getKey().toString(), entry.getValue());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            jSONObject.put("kv", this.f1265a.c);
            return true;
        }
        jSONObject.put("ar", this.f1265a.f1267b);
        return true;
    }

    public final e b() {
        return e.CUSTOM;
    }
}
