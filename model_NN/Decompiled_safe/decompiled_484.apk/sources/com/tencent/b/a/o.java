package com.tencent.b.a;

import android.content.Context;
import com.tencent.b.a.b.l;
import java.lang.Thread;

final class o implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f1336a;

    o(Context context) {
        this.f1336a = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.b.a.b.l.a(android.content.Context, boolean):int
     arg types: [android.content.Context, int]
     candidates:
      com.tencent.b.a.b.l.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.b.a.b.l.a(android.content.Context, int):void
      com.tencent.b.a.b.l.a(android.content.Context, boolean):int */
    public final void run() {
        x.a(v.t).h();
        l.a(this.f1336a, true);
        ai.a(this.f1336a);
        l.b(this.f1336a);
        Thread.UncaughtExceptionHandler unused = v.r = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new ac());
        if (t.a() == u.APP_LAUNCH) {
            v.b(this.f1336a);
        }
        if (t.b()) {
            v.q.g("Init MTA StatService success.");
        }
    }
}
