package com.tencent.b.a.b;

final class k extends i {
    static final /* synthetic */ boolean g = (!h.class.desiredAssertionStatus());
    private static final byte[] h = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] i = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    int c = 0;
    public final boolean d = true;
    public final boolean e = true;
    public final boolean f = false;
    private final byte[] j = new byte[2];
    private int k;
    private final byte[] l = h;

    public k() {
        this.f1312a = null;
        this.k = this.e ? 19 : -1;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final boolean a(byte[] bArr, int i2) {
        byte b2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        byte b3;
        byte b4;
        int i8;
        byte b5;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13 = 0;
        byte[] bArr2 = this.l;
        byte[] bArr3 = this.f1312a;
        int i14 = this.k;
        int i15 = i2 + 0;
        switch (this.c) {
            case 0:
                b2 = -1;
                i3 = 0;
                break;
            case 1:
                if (2 <= i15) {
                    this.c = 0;
                    b2 = ((this.j[0] & 255) << 16) | ((bArr[0] & 255) << 8) | (bArr[1] & 255);
                    i3 = 2;
                    break;
                }
                b2 = -1;
                i3 = 0;
                break;
            case 2:
                if (i15 > 0) {
                    this.c = 0;
                    b2 = ((this.j[0] & 255) << 16) | ((this.j[1] & 255) << 8) | (bArr[0] & 255);
                    i3 = 1;
                    break;
                }
                b2 = -1;
                i3 = 0;
                break;
            default:
                b2 = -1;
                i3 = 0;
                break;
        }
        if (b2 != -1) {
            bArr3[0] = bArr2[(b2 >> 18) & 63];
            bArr3[1] = bArr2[(b2 >> 12) & 63];
            bArr3[2] = bArr2[(b2 >> 6) & 63];
            int i16 = 4;
            bArr3[3] = bArr2[b2 & 63];
            int i17 = i14 - 1;
            if (i17 == 0) {
                if (this.f) {
                    i16 = 5;
                    bArr3[4] = 13;
                }
                i6 = i16 + 1;
                bArr3[i16] = 10;
                i5 = 19;
            } else {
                i5 = i17;
                i6 = 4;
            }
        } else {
            i5 = i14;
            i6 = 0;
        }
        while (i4 + 3 <= i15) {
            byte b6 = ((bArr[i4] & 255) << 16) | ((bArr[i4 + 1] & 255) << 8) | (bArr[i4 + 2] & 255);
            bArr3[i6] = bArr2[(b6 >> 18) & 63];
            bArr3[i6 + 1] = bArr2[(b6 >> 12) & 63];
            bArr3[i6 + 2] = bArr2[(b6 >> 6) & 63];
            bArr3[i6 + 3] = bArr2[b6 & 63];
            int i18 = i4 + 3;
            int i19 = i6 + 4;
            int i20 = i5 - 1;
            if (i20 == 0) {
                if (this.f) {
                    i12 = i19 + 1;
                    bArr3[i19] = 13;
                } else {
                    i12 = i19;
                }
                i11 = i12 + 1;
                bArr3[i12] = 10;
                i4 = i18;
                i10 = 19;
            } else {
                i10 = i20;
                i11 = i19;
                i4 = i18;
            }
        }
        if (i4 - this.c == i15 - 1) {
            if (this.c > 0) {
                b5 = this.j[0];
                i9 = 1;
            } else {
                b5 = bArr[i4];
                i4++;
                i9 = 0;
            }
            int i21 = (b5 & 255) << 4;
            this.c -= i9;
            int i22 = i6 + 1;
            bArr3[i6] = bArr2[(i21 >> 6) & 63];
            int i23 = i22 + 1;
            bArr3[i22] = bArr2[i21 & 63];
            if (this.d) {
                int i24 = i23 + 1;
                bArr3[i23] = 61;
                i23 = i24 + 1;
                bArr3[i24] = 61;
            }
            if (this.e) {
                if (this.f) {
                    bArr3[i23] = 13;
                    i23++;
                }
                bArr3[i23] = 10;
                i23++;
            }
            i6 = i23;
        } else if (i4 - this.c == i15 - 2) {
            if (this.c > 1) {
                b3 = this.j[0];
                i13 = 1;
            } else {
                b3 = bArr[i4];
                i4++;
            }
            int i25 = (b3 & 255) << 10;
            if (this.c > 0) {
                b4 = this.j[i13];
                i13++;
            } else {
                b4 = bArr[i4];
                i4++;
            }
            int i26 = ((b4 & 255) << 2) | i25;
            this.c -= i13;
            int i27 = i6 + 1;
            bArr3[i6] = bArr2[(i26 >> 12) & 63];
            int i28 = i27 + 1;
            bArr3[i27] = bArr2[(i26 >> 6) & 63];
            int i29 = i28 + 1;
            bArr3[i28] = bArr2[i26 & 63];
            if (this.d) {
                i8 = i29 + 1;
                bArr3[i29] = 61;
            } else {
                i8 = i29;
            }
            if (this.e) {
                if (this.f) {
                    bArr3[i8] = 13;
                    i8++;
                }
                bArr3[i8] = 10;
                i8++;
            }
            i6 = i8;
        } else if (this.e && i6 > 0 && i5 != 19) {
            if (this.f) {
                i7 = i6 + 1;
                bArr3[i6] = 13;
            } else {
                i7 = i6;
            }
            i6 = i7 + 1;
            bArr3[i7] = 10;
        }
        if (!g && this.c != 0) {
            throw new AssertionError();
        } else if (g || i4 == i15) {
            this.f1313b = i6;
            this.k = i5;
            return true;
        } else {
            throw new AssertionError();
        }
    }
}
