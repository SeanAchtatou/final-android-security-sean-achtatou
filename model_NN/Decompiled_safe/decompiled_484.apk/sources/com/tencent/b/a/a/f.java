package com.tencent.b.a.a;

import android.content.Context;
import com.tencent.b.a.b.r;
import com.tencent.b.a.t;
import com.tencent.b.a.w;
import org.json.JSONObject;

public final class f extends d {

    /* renamed from: a  reason: collision with root package name */
    public static final w f1273a;

    static {
        w wVar = new w();
        f1273a = wVar;
        wVar.a("A9VH9B8L4GX4");
    }

    public f(Context context) {
        super(context, 0, f1273a);
    }

    public final boolean a(JSONObject jSONObject) {
        r.a(jSONObject, "actky", t.a(this.l));
        return true;
    }

    public final e b() {
        return e.NETWORK_DETECTOR;
    }
}
