package com.tencent.b.a.b;

import org.json.JSONException;
import org.json.JSONObject;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private String f1304a = null;

    /* renamed from: b  reason: collision with root package name */
    private String f1305b = null;
    private String c = null;
    private String d = "0";
    private int e;
    private int f = 0;
    private long g = 0;

    public c() {
    }

    public c(String str, String str2, int i) {
        this.f1304a = str;
        this.f1305b = str2;
        this.e = i;
    }

    private JSONObject e() {
        JSONObject jSONObject = new JSONObject();
        try {
            r.a(jSONObject, "ui", this.f1304a);
            r.a(jSONObject, "mc", this.f1305b);
            r.a(jSONObject, "mid", this.d);
            r.a(jSONObject, "aid", this.c);
            jSONObject.put("ts", this.g);
            jSONObject.put("ver", this.f);
        } catch (JSONException e2) {
        }
        return jSONObject;
    }

    public final String a() {
        return this.f1304a;
    }

    public final String b() {
        return this.f1305b;
    }

    public final void c() {
        this.e = 1;
    }

    public final int d() {
        return this.e;
    }

    public final String toString() {
        return e().toString();
    }
}
