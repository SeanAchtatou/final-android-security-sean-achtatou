package com.tencent.b.a;

import com.tencent.b.a.a.c;
import com.tencent.b.a.a.d;
import java.lang.Thread;

final class ac implements Thread.UncaughtExceptionHandler {
    ac() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.b.a.ai.a(com.tencent.b.a.a.d, com.tencent.b.a.k, boolean, boolean):void
     arg types: [com.tencent.b.a.a.c, ?[OBJECT, ARRAY], int, int]
     candidates:
      com.tencent.b.a.ai.a(com.tencent.b.a.ai, java.util.List, int, boolean):void
      com.tencent.b.a.ai.a(com.tencent.b.a.a.d, com.tencent.b.a.k, boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.b.a.v.a(android.content.Context, boolean, com.tencent.b.a.w):int
     arg types: [android.content.Context, int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.b.a.v.a(android.content.Context, java.lang.String, java.lang.String):boolean
      com.tencent.b.a.v.a(android.content.Context, boolean, com.tencent.b.a.w):int */
    public final void uncaughtException(Thread thread, Throwable th) {
        if (t.c() && v.t != null) {
            if (t.n()) {
                ai.a(v.t).a((d) new c(v.t, v.a(v.t, false, (w) null), th, thread), (k) null, false, true);
                v.q.f("MTA has caught the following uncaught exception:");
                v.q.a(th);
            }
            v.c(v.t);
            if (v.r != null) {
                v.q.g("Call the original uncaught exception handler.");
                if (!(v.r instanceof ac)) {
                    v.r.uncaughtException(thread, th);
                }
            }
        }
    }
}
