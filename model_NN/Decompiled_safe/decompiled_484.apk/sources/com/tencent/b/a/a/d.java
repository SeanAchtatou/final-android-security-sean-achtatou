package com.tencent.b.a.a;

import android.content.Context;
import com.tencent.a.a.a.a.h;
import com.tencent.b.a.ai;
import com.tencent.b.a.b.c;
import com.tencent.b.a.b.l;
import com.tencent.b.a.b.r;
import com.tencent.b.a.t;
import com.tencent.b.a.w;
import org.json.JSONObject;

public abstract class d {
    protected static String j = null;

    /* renamed from: a  reason: collision with root package name */
    private w f1269a = null;

    /* renamed from: b  reason: collision with root package name */
    protected String f1270b = null;
    protected long c;
    protected int d;
    protected c e = null;
    protected int f;
    protected String g = null;
    protected String h = null;
    protected String i = null;
    protected boolean k = false;
    protected Context l;

    d(Context context, int i2, w wVar) {
        this.l = context;
        this.c = System.currentTimeMillis() / 1000;
        this.d = i2;
        this.h = t.b(context);
        this.i = l.i(context);
        this.f1270b = t.a(context);
        if (wVar != null) {
            this.f1269a = wVar;
            if (l.c(wVar.c())) {
                this.f1270b = wVar.c();
            }
            if (l.c(wVar.d())) {
                this.h = wVar.d();
            }
            if (l.c(wVar.b())) {
                this.i = wVar.b();
            }
            this.k = wVar.e();
        }
        this.g = t.d(context);
        this.e = ai.a(context).b(context);
        if (b() != e.NETWORK_DETECTOR) {
            this.f = l.p(context).intValue();
        } else {
            this.f = -e.NETWORK_DETECTOR.a();
        }
        if (!h.b(j)) {
            String e2 = t.e(context);
            j = e2;
            if (!l.c(e2)) {
                j = "0";
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.b.a.b.l.a(android.content.Context, boolean):int
     arg types: [android.content.Context, int]
     candidates:
      com.tencent.b.a.b.l.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.b.a.b.l.a(android.content.Context, int):void
      com.tencent.b.a.b.l.a(android.content.Context, boolean):int */
    private boolean b(JSONObject jSONObject) {
        try {
            r.a(jSONObject, "ky", this.f1270b);
            jSONObject.put("et", b().a());
            if (this.e != null) {
                jSONObject.put("ui", this.e.a());
                r.a(jSONObject, "mc", this.e.b());
                int d2 = this.e.d();
                jSONObject.put("ut", d2);
                if (d2 == 0 && l.s(this.l) == 1) {
                    jSONObject.put("ia", 1);
                }
            }
            r.a(jSONObject, "cui", this.g);
            if (b() != e.SESSION_ENV) {
                r.a(jSONObject, "av", this.i);
                r.a(jSONObject, "ch", this.h);
            }
            if (this.k) {
                jSONObject.put("impt", 1);
            }
            r.a(jSONObject, "mid", j);
            jSONObject.put("idx", this.f);
            jSONObject.put("si", this.d);
            jSONObject.put("ts", this.c);
            jSONObject.put("dts", l.a(this.l, false));
            return a(jSONObject);
        } catch (Throwable th) {
            return false;
        }
    }

    public abstract boolean a(JSONObject jSONObject);

    public abstract e b();

    public final long c() {
        return this.c;
    }

    public final w d() {
        return this.f1269a;
    }

    public final Context e() {
        return this.l;
    }

    public final boolean f() {
        return this.k;
    }

    public final String g() {
        try {
            JSONObject jSONObject = new JSONObject();
            b(jSONObject);
            return jSONObject.toString();
        } catch (Throwable th) {
            return "";
        }
    }
}
