package com.tencent.b.a.b;

public class h {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f1311a = (!h.class.desiredAssertionStatus());

    private h() {
    }

    public static byte[] a(byte[] bArr) {
        int length = bArr.length;
        j jVar = new j(new byte[((length * 3) / 4)]);
        if (!jVar.a(bArr, length)) {
            throw new IllegalArgumentException("bad base-64");
        } else if (jVar.f1313b == jVar.f1312a.length) {
            return jVar.f1312a;
        } else {
            byte[] bArr2 = new byte[jVar.f1313b];
            System.arraycopy(jVar.f1312a, 0, bArr2, 0, jVar.f1313b);
            return bArr2;
        }
    }

    public static byte[] b(byte[] bArr) {
        int length = bArr.length;
        k kVar = new k();
        int i = (length / 3) * 4;
        if (!kVar.d) {
            switch (length % 3) {
                case 1:
                    i += 2;
                    break;
                case 2:
                    i += 3;
                    break;
            }
        } else if (length % 3 > 0) {
            i += 4;
        }
        if (kVar.e && length > 0) {
            i += (kVar.f ? 2 : 1) * (((length - 1) / 57) + 1);
        }
        kVar.f1312a = new byte[i];
        kVar.a(bArr, length);
        if (f1311a || kVar.f1313b == i) {
            return kVar.f1312a;
        }
        throw new AssertionError();
    }
}
