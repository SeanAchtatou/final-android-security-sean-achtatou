package com.tencent.b.a;

import android.content.Context;
import com.tencent.b.a.a.a;
import com.tencent.b.a.a.b;

final class r implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f1339a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ w f1340b = null;
    final /* synthetic */ b c;

    r(Context context, b bVar) {
        this.f1339a = context;
        this.c = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.b.a.v.a(android.content.Context, boolean, com.tencent.b.a.w):int
     arg types: [android.content.Context, int, com.tencent.b.a.w]
     candidates:
      com.tencent.b.a.v.a(android.content.Context, java.lang.String, java.lang.String):boolean
      com.tencent.b.a.v.a(android.content.Context, boolean, com.tencent.b.a.w):int */
    public final void run() {
        try {
            a aVar = new a(this.f1339a, v.a(this.f1339a, false, this.f1340b), this.c.f1266a, this.f1340b);
            aVar.a().c = this.c.c;
            new ae(aVar).a();
        } catch (Throwable th) {
            v.q.b(th);
            v.a(this.f1339a, th);
        }
    }
}
