package com.tencent.b.a;

public enum u {
    INSTANT(1),
    ONLY_WIFI(2),
    BATCH(3),
    APP_LAUNCH(4),
    DEVELOPER(5),
    PERIOD(6),
    ONLY_WIFI_NO_CACHE(7);
    
    int h;

    private u(int i2) {
        this.h = i2;
    }

    public static u a(int i2) {
        for (u uVar : values()) {
            if (i2 == uVar.h) {
                return uVar;
            }
        }
        return null;
    }
}
