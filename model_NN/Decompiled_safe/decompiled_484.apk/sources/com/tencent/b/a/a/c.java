package com.tencent.b.a.a;

import android.content.Context;
import com.tencent.b.a.b.d;
import com.tencent.b.a.b.r;
import com.tencent.b.a.w;
import java.io.PrintWriter;
import java.io.StringWriter;
import org.json.JSONObject;

public final class c extends d {

    /* renamed from: a  reason: collision with root package name */
    private String f1268a;
    private int m;
    private int n = 100;
    private Thread o = null;

    public c(Context context, int i, Throwable th, w wVar) {
        super(context, i, wVar);
        a(99, th);
    }

    public c(Context context, int i, Throwable th, Thread thread) {
        super(context, i, null);
        a(2, th);
        this.o = thread;
    }

    private void a(int i, Throwable th) {
        if (th != null) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            th.printStackTrace(printWriter);
            this.f1268a = stringWriter.toString();
            this.m = i;
            printWriter.close();
        }
    }

    public final boolean a(JSONObject jSONObject) {
        r.a(jSONObject, "er", this.f1268a);
        jSONObject.put("ea", this.m);
        if (this.m != 2 && this.m != 3) {
            return true;
        }
        new d(this.l).a(jSONObject, this.o);
        return true;
    }

    public final e b() {
        return e.ERROR;
    }
}
