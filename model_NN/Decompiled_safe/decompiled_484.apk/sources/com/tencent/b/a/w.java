package com.tencent.b.a;

public final class w {

    /* renamed from: a  reason: collision with root package name */
    private String f1349a = null;

    /* renamed from: b  reason: collision with root package name */
    private String f1350b = null;
    private String c = null;
    private boolean d = false;
    private boolean e = false;

    public final void a(String str) {
        this.f1349a = str;
    }

    public final boolean a() {
        return this.d;
    }

    public final String b() {
        return this.c;
    }

    public final String c() {
        return this.f1349a;
    }

    public final String d() {
        return this.f1350b;
    }

    public final boolean e() {
        return this.e;
    }

    public final String toString() {
        return "StatSpecifyReportedInfo [appKey=" + this.f1349a + ", installChannel=" + this.f1350b + ", version=" + this.c + ", sendImmediately=" + this.d + ", isImportant=" + this.e + "]";
    }
}
