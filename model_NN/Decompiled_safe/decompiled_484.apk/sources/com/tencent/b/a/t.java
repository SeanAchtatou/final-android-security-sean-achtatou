package com.tencent.b.a;

import android.content.Context;
import com.qihoo.messenger.util.QDefine;
import com.tencent.a.a.a.a.g;
import com.tencent.b.a.b.b;
import com.tencent.b.a.b.l;
import com.tencent.b.a.b.q;
import com.tencent.b.a.b.r;
import java.net.URI;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public final class t {
    private static int A = 1;
    private static String B = null;
    private static String C;
    private static String D;
    private static String E = "mta_channel";
    private static int F = 180;
    private static int G = 1024;
    private static long H = 0;
    private static long I = QDefine.WIFI_ACCESS_PERIOD;
    private static volatile String J = "http://pingma.qq.com:80/mstat/report";
    private static int K = 0;
    private static volatile int L = 0;
    private static int M = 20;
    private static int N = 0;
    private static boolean O = false;
    private static int P = 4096;
    private static boolean Q = false;
    private static String R = null;
    private static boolean S = false;
    private static j T = null;

    /* renamed from: a  reason: collision with root package name */
    static i f1343a = new i(2);

    /* renamed from: b  reason: collision with root package name */
    static i f1344b = new i(1);
    static String c = "__HIBERNATE__";
    static String d = "__HIBERNATE__TIME";
    static String e = "__MTA_KILL__";
    static String f = "";
    static boolean g = false;
    static int h = 100;
    static long i = 10000;
    static boolean j = true;
    public static boolean k = true;
    static volatile String l = "pingma.qq.com:80";
    static boolean m = true;
    static int n = 0;
    static long o = 10000;
    static int p = 512;
    private static b q = l.c();
    private static u r = u.APP_LAUNCH;
    private static boolean s = false;
    private static boolean t = true;
    private static int u = 30000;
    private static int v = 100000;
    private static int w = 30;
    private static int x = 10;
    private static int y = 100;
    private static int z = 30;

    public static u a() {
        return r;
    }

    public static synchronized String a(Context context) {
        String str;
        synchronized (t.class) {
            if (C != null) {
                str = C;
            } else {
                if (context != null) {
                    if (C == null) {
                        C = l.f(context);
                    }
                }
                if (C == null || C.trim().length() == 0) {
                    q.d("AppKey can not be null or empty, please read Developer's Guide first!");
                }
                str = C;
            }
        }
        return str;
    }

    static String a(String str) {
        try {
            String string = f1344b.f1330b.getString(str);
            if (string != null) {
                return string;
            }
            return null;
        } catch (Throwable th) {
            q.c("can't find custom key:" + str);
        }
    }

    private static void a(long j2) {
        q.a(l.a(), c, j2);
        a(false);
        q.b("MTA is disable for current SDK version");
    }

    static void a(Context context, i iVar) {
        if (iVar.f1329a == f1344b.f1329a) {
            f1344b = iVar;
            a(iVar.f1330b);
            if (!f1344b.f1330b.isNull("iplist")) {
                x.a(context).a(f1344b.f1330b.getString("iplist"));
            }
        } else if (iVar.f1329a == f1343a.f1329a) {
            f1343a = iVar;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:109:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0044, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0045, code lost:
        com.tencent.b.a.t.q.b((java.lang.Throwable) r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x005c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005d, code lost:
        com.tencent.b.a.t.q.b(r0);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x005c A[ExcHandler: Throwable (r0v0 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:1:0x0002] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(android.content.Context r10, com.tencent.b.a.i r11, org.json.JSONObject r12) {
        /*
            r2 = 0
            r1 = 1
            java.util.Iterator r4 = r12.keys()     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            r3 = r2
        L_0x0007:
            boolean r0 = r4.hasNext()     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            if (r0 == 0) goto L_0x0063
            java.lang.Object r0 = r4.next()     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            java.lang.String r5 = "v"
            boolean r5 = r0.equalsIgnoreCase(r5)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            if (r5 == 0) goto L_0x0028
            int r5 = r12.getInt(r0)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            int r0 = r11.d     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            if (r0 == r5) goto L_0x02a0
            r0 = r1
        L_0x0024:
            r11.d = r5     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            r3 = r0
            goto L_0x0007
        L_0x0028:
            java.lang.String r5 = "c"
            boolean r5 = r0.equalsIgnoreCase(r5)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            if (r5 == 0) goto L_0x004b
            java.lang.String r0 = "c"
            java.lang.String r0 = r12.getString(r0)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            int r5 = r0.length()     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            if (r5 <= 0) goto L_0x0007
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            r5.<init>(r0)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            r11.f1330b = r5     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            goto L_0x0007
        L_0x0044:
            r0 = move-exception
            com.tencent.b.a.b.b r1 = com.tencent.b.a.t.q
            r1.b(r0)
        L_0x004a:
            return
        L_0x004b:
            java.lang.String r5 = "m"
            boolean r0 = r0.equalsIgnoreCase(r5)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            if (r0 == 0) goto L_0x0007
            java.lang.String r0 = "m"
            java.lang.String r0 = r12.getString(r0)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            r11.c = r0     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            goto L_0x0007
        L_0x005c:
            r0 = move-exception
            com.tencent.b.a.b.b r1 = com.tencent.b.a.t.q
            r1.b(r0)
            goto L_0x004a
        L_0x0063:
            if (r3 != r1) goto L_0x0089
            android.content.Context r0 = com.tencent.b.a.l.a()     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            com.tencent.b.a.ai r0 = com.tencent.b.a.ai.a(r0)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            if (r0 == 0) goto L_0x0072
            r0.a(r11)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
        L_0x0072:
            int r0 = r11.f1329a     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            com.tencent.b.a.i r3 = com.tencent.b.a.t.f1344b     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            int r3 = r3.f1329a     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            if (r0 != r3) goto L_0x0089
            org.json.JSONObject r0 = r11.f1330b     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            a(r0)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            org.json.JSONObject r3 = r11.f1330b     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            if (r3 == 0) goto L_0x0089
            int r0 = r3.length()     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            if (r0 != 0) goto L_0x008d
        L_0x0089:
            a(r10, r11)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            goto L_0x004a
        L_0x008d:
            android.content.Context r4 = com.tencent.b.a.l.a()     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            java.lang.String r0 = com.tencent.b.a.t.e     // Catch:{ Exception -> 0x0282 }
            java.lang.String r0 = r3.optString(r0)     // Catch:{ Exception -> 0x0282 }
            boolean r5 = com.tencent.b.a.b.l.c(r0)     // Catch:{ Exception -> 0x0282 }
            if (r5 == 0) goto L_0x00a8
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Exception -> 0x0282 }
            r5.<init>(r0)     // Catch:{ Exception -> 0x0282 }
            int r0 = r5.length()     // Catch:{ Exception -> 0x0282 }
            if (r0 != 0) goto L_0x00e7
        L_0x00a8:
            java.lang.String r0 = com.tencent.b.a.t.c     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            java.lang.String r0 = r3.getString(r0)     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            boolean r1 = com.tencent.b.a.t.s     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            if (r1 == 0) goto L_0x00cc
            com.tencent.b.a.b.b r1 = com.tencent.b.a.t.q     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            java.lang.String r3 = "hibernateVer:"
            r2.<init>(r3)     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            java.lang.String r3 = ", current version:2.0.3"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            java.lang.String r2 = r2.toString()     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            r1.g(r2)     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
        L_0x00cc:
            long r0 = com.tencent.b.a.b.l.b(r0)     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            java.lang.String r2 = "2.0.3"
            long r2 = com.tencent.b.a.b.l.b(r2)     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            int r2 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r2 > 0) goto L_0x0089
            a(r0)     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            goto L_0x0089
        L_0x00de:
            r0 = move-exception
            com.tencent.b.a.b.b r0 = com.tencent.b.a.t.q     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            java.lang.String r1 = "__HIBERNATE__ not found."
            r0.g(r1)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            goto L_0x0089
        L_0x00e7:
            java.lang.String r0 = "sm"
            boolean r0 = r5.isNull(r0)     // Catch:{ Exception -> 0x0282 }
            if (r0 != 0) goto L_0x0139
            java.lang.String r0 = "sm"
            java.lang.Object r0 = r5.get(r0)     // Catch:{ Exception -> 0x0282 }
            boolean r6 = r0 instanceof java.lang.Integer     // Catch:{ Exception -> 0x0282 }
            if (r6 == 0) goto L_0x028a
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0282 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0282 }
        L_0x00ff:
            if (r0 <= 0) goto L_0x0139
            boolean r6 = com.tencent.b.a.t.s     // Catch:{ Exception -> 0x0282 }
            if (r6 == 0) goto L_0x011f
            com.tencent.b.a.b.b r6 = com.tencent.b.a.t.q     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            java.lang.String r8 = "match sleepTime:"
            r7.<init>(r8)     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r8 = " minutes"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x0282 }
            r6.a(r7)     // Catch:{ Exception -> 0x0282 }
        L_0x011f:
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0282 }
            int r0 = r0 * 60
            int r0 = r0 * 1000
            long r8 = (long) r0     // Catch:{ Exception -> 0x0282 }
            long r6 = r6 + r8
            java.lang.String r0 = com.tencent.b.a.t.d     // Catch:{ Exception -> 0x0282 }
            com.tencent.b.a.b.q.a(r4, r0, r6)     // Catch:{ Exception -> 0x0282 }
            r0 = 0
            a(r0)     // Catch:{ Exception -> 0x0282 }
            com.tencent.b.a.b.b r0 = com.tencent.b.a.t.q     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = "MTA is disable for current SDK version"
            r0.b(r6)     // Catch:{ Exception -> 0x0282 }
        L_0x0139:
            java.lang.String r0 = "sv"
            java.lang.String r6 = "2.0.3"
            boolean r0 = a(r5, r0, r6)     // Catch:{ Exception -> 0x0282 }
            if (r0 == 0) goto L_0x029a
            com.tencent.b.a.b.b r0 = com.tencent.b.a.t.q     // Catch:{ Exception -> 0x0282 }
            java.lang.String r2 = "match sdk version:2.0.3"
            r0.a(r2)     // Catch:{ Exception -> 0x0282 }
            r0 = r1
        L_0x014b:
            java.lang.String r2 = "md"
            java.lang.String r6 = android.os.Build.MODEL     // Catch:{ Exception -> 0x0282 }
            boolean r2 = a(r5, r2, r6)     // Catch:{ Exception -> 0x0282 }
            if (r2 == 0) goto L_0x016c
            com.tencent.b.a.b.b r0 = com.tencent.b.a.t.q     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = "match MODEL:"
            r2.<init>(r6)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = android.os.Build.MODEL     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0282 }
            r0.a(r2)     // Catch:{ Exception -> 0x0282 }
            r0 = r1
        L_0x016c:
            java.lang.String r2 = "av"
            java.lang.String r6 = com.tencent.b.a.b.l.i(r4)     // Catch:{ Exception -> 0x0282 }
            boolean r2 = a(r5, r2, r6)     // Catch:{ Exception -> 0x0282 }
            if (r2 == 0) goto L_0x0191
            com.tencent.b.a.b.b r0 = com.tencent.b.a.t.q     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = "match app version:"
            r2.<init>(r6)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = com.tencent.b.a.b.l.i(r4)     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0282 }
            r0.a(r2)     // Catch:{ Exception -> 0x0282 }
            r0 = r1
        L_0x0191:
            java.lang.String r2 = "mf"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            r6.<init>()     // Catch:{ Exception -> 0x0282 }
            java.lang.String r7 = android.os.Build.MANUFACTURER     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0282 }
            boolean r2 = a(r5, r2, r6)     // Catch:{ Exception -> 0x0282 }
            if (r2 == 0) goto L_0x01bf
            com.tencent.b.a.b.b r0 = com.tencent.b.a.t.q     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = "match MANUFACTURER:"
            r2.<init>(r6)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = android.os.Build.MANUFACTURER     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0282 }
            r0.a(r2)     // Catch:{ Exception -> 0x0282 }
            r0 = r1
        L_0x01bf:
            java.lang.String r2 = "osv"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            r6.<init>()     // Catch:{ Exception -> 0x0282 }
            int r7 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0282 }
            boolean r2 = a(r5, r2, r6)     // Catch:{ Exception -> 0x0282 }
            if (r2 == 0) goto L_0x01ed
            com.tencent.b.a.b.b r0 = com.tencent.b.a.t.q     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = "match android SDK version:"
            r2.<init>(r6)     // Catch:{ Exception -> 0x0282 }
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0282 }
            r0.a(r2)     // Catch:{ Exception -> 0x0282 }
            r0 = r1
        L_0x01ed:
            java.lang.String r2 = "ov"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            r6.<init>()     // Catch:{ Exception -> 0x0282 }
            int r7 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0282 }
            boolean r2 = a(r5, r2, r6)     // Catch:{ Exception -> 0x0282 }
            if (r2 == 0) goto L_0x021b
            com.tencent.b.a.b.b r0 = com.tencent.b.a.t.q     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = "match android SDK version:"
            r2.<init>(r6)     // Catch:{ Exception -> 0x0282 }
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0282 }
            r0.a(r2)     // Catch:{ Exception -> 0x0282 }
            r0 = r1
        L_0x021b:
            java.lang.String r2 = "ui"
            com.tencent.b.a.ai r6 = com.tencent.b.a.ai.a(r4)     // Catch:{ Exception -> 0x0282 }
            com.tencent.b.a.b.c r6 = r6.b(r4)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = r6.a()     // Catch:{ Exception -> 0x0282 }
            boolean r2 = a(r5, r2, r6)     // Catch:{ Exception -> 0x0282 }
            if (r2 == 0) goto L_0x0250
            com.tencent.b.a.b.b r0 = com.tencent.b.a.t.q     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = "match imei:"
            r2.<init>(r6)     // Catch:{ Exception -> 0x0282 }
            com.tencent.b.a.ai r6 = com.tencent.b.a.ai.a(r4)     // Catch:{ Exception -> 0x0282 }
            com.tencent.b.a.b.c r6 = r6.b(r4)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = r6.a()     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0282 }
            r0.a(r2)     // Catch:{ Exception -> 0x0282 }
            r0 = r1
        L_0x0250:
            java.lang.String r2 = "mid"
            java.lang.String r6 = e(r4)     // Catch:{ Exception -> 0x0282 }
            boolean r2 = a(r5, r2, r6)     // Catch:{ Exception -> 0x0282 }
            if (r2 == 0) goto L_0x0275
            com.tencent.b.a.b.b r0 = com.tencent.b.a.t.q     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            java.lang.String r5 = "match mid:"
            r2.<init>(r5)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r4 = e(r4)     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0282 }
            r0.a(r2)     // Catch:{ Exception -> 0x0282 }
            r0 = r1
        L_0x0275:
            if (r0 == 0) goto L_0x00a8
            java.lang.String r0 = "2.0.3"
            long r0 = com.tencent.b.a.b.l.b(r0)     // Catch:{ Exception -> 0x0282 }
            a(r0)     // Catch:{ Exception -> 0x0282 }
            goto L_0x00a8
        L_0x0282:
            r0 = move-exception
            com.tencent.b.a.b.b r1 = com.tencent.b.a.t.q     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            r1.b(r0)     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            goto L_0x00a8
        L_0x028a:
            boolean r6 = r0 instanceof java.lang.String     // Catch:{ Exception -> 0x0282 }
            if (r6 == 0) goto L_0x029d
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0282 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x0282 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0282 }
            goto L_0x00ff
        L_0x029a:
            r0 = r2
            goto L_0x014b
        L_0x029d:
            r0 = r2
            goto L_0x00ff
        L_0x02a0:
            r0 = r3
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.b.a.t.a(android.content.Context, com.tencent.b.a.i, org.json.JSONObject):void");
    }

    public static void a(Context context, String str) {
        String str2;
        if (context == null) {
            q.d("ctx in StatConfig.setAppKey() is null");
        } else if (str == null || str.length() > 256) {
            q.d("appkey in StatConfig.setAppKey() is null or exceed 256 bytes");
        } else {
            if (C == null) {
                C = r.a(q.a(context, "_mta_ky_tag_", (String) null));
            }
            if ((d(str) || d(l.f(context))) && (str2 = C) != null) {
                q.b(context, "_mta_ky_tag_", r.b(str2));
            }
        }
    }

    static void a(Context context, JSONObject jSONObject) {
        try {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                if (next.equalsIgnoreCase(Integer.toString(f1344b.f1329a))) {
                    a(context, f1344b, jSONObject.getJSONObject(next));
                } else if (next.equalsIgnoreCase(Integer.toString(f1343a.f1329a))) {
                    a(context, f1343a, jSONObject.getJSONObject(next));
                } else if (next.equalsIgnoreCase("rs")) {
                    u a2 = u.a(jSONObject.getInt(next));
                    if (a2 != null) {
                        r = a2;
                        if (s) {
                            q.g("Change to ReportStrategy:" + a2.name());
                        }
                    }
                } else {
                    return;
                }
            }
        } catch (JSONException e2) {
            q.b((Throwable) e2);
        }
    }

    public static void a(u uVar) {
        r = uVar;
        if (uVar != u.PERIOD) {
            v.c = 0;
        }
        if (s) {
            q.g("Change to statSendStrategy: " + uVar);
        }
    }

    private static void a(JSONObject jSONObject) {
        try {
            u a2 = u.a(jSONObject.getInt("rs"));
            if (a2 != null) {
                a(a2);
            }
        } catch (JSONException e2) {
            if (s) {
                q.a("rs not found.");
            }
        }
    }

    public static void a(boolean z2) {
        t = z2;
        if (!z2) {
            q.b("!!!!!!MTA StatService has been disabled!!!!!!");
        }
    }

    private static boolean a(JSONObject jSONObject, String str, String str2) {
        if (!jSONObject.isNull(str)) {
            String optString = jSONObject.optString(str);
            return l.c(str2) && l.c(optString) && str2.equalsIgnoreCase(optString);
        }
    }

    public static synchronized String b(Context context) {
        String str;
        synchronized (t.class) {
            if (D != null) {
                str = D;
            } else {
                String a2 = q.a(context, E, "");
                D = a2;
                if (a2 == null || D.trim().length() == 0) {
                    D = l.g(context);
                }
                if (D == null || D.trim().length() == 0) {
                    q.c("installChannel can not be null or empty, please read Developer's Guide first!");
                }
                str = D;
            }
        }
        return str;
    }

    public static void b(Context context, String str) {
        if (str.length() > 128) {
            q.d("the length of installChannel can not exceed the range of 128 bytes.");
            return;
        }
        D = str;
        q.b(context, E, str);
    }

    public static void b(String str) {
        if (str.length() > 128) {
            q.d("the length of installChannel can not exceed the range of 128 bytes.");
        } else {
            D = str;
        }
    }

    public static boolean b() {
        return s;
    }

    public static String c(Context context) {
        return q.a(context, "mta.acc.qq", f);
    }

    public static void c(String str) {
        if (str == null || str.length() == 0) {
            q.d("statReportUrl cannot be null or empty.");
            return;
        }
        J = str;
        try {
            l = new URI(J).getHost();
        } catch (Exception e2) {
            q.c(e2);
        }
        if (s) {
            q.a("url:" + J + ", domain:" + l);
        }
    }

    public static boolean c() {
        return t;
    }

    public static int d() {
        return u;
    }

    public static String d(Context context) {
        if (context == null) {
            q.d("Context for getCustomUid is null.");
            return null;
        }
        if (R == null) {
            R = q.a(context, "MTA_CUSTOM_UID", "");
        }
        return R;
    }

    private static boolean d(String str) {
        if (str == null) {
            return false;
        }
        if (C == null) {
            C = str;
            return true;
        } else if (C.contains(str)) {
            return false;
        } else {
            C += "|" + str;
            return true;
        }
    }

    public static int e() {
        return y;
    }

    public static String e(Context context) {
        return context != null ? g.a(context).a().a() : "0";
    }

    public static int f() {
        return z;
    }

    public static int g() {
        return x;
    }

    public static int h() {
        return A;
    }

    static int i() {
        return w;
    }

    public static int j() {
        return v;
    }

    public static void k() {
        F = 60;
    }

    public static int l() {
        return F;
    }

    public static void m() {
        j = true;
    }

    public static boolean n() {
        return k;
    }

    public static String o() {
        return J;
    }

    static synchronized void p() {
        synchronized (t.class) {
            L = 0;
        }
    }

    public static int q() {
        return M;
    }

    static void r() {
        N++;
    }

    static void s() {
        N = 0;
    }

    static int t() {
        return N;
    }

    public static boolean u() {
        return Q;
    }

    public static j v() {
        return T;
    }
}
