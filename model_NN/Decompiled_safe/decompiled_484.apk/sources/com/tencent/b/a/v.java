package com.tencent.b.a;

import android.content.Context;
import com.qihoo.messenger.util.QDefine;
import com.tencent.b.a.a.b;
import com.tencent.b.a.a.h;
import com.tencent.b.a.b.f;
import com.tencent.b.a.b.l;
import com.tencent.b.a.b.q;
import java.lang.Thread;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONException;
import org.json.JSONObject;

public class v {

    /* renamed from: a  reason: collision with root package name */
    static volatile int f1347a = 0;

    /* renamed from: b  reason: collision with root package name */
    static volatile long f1348b = 0;
    static volatile long c = 0;
    private static f d;
    private static volatile Map<b, Long> e = new ConcurrentHashMap();
    private static volatile Map<String, Properties> f = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public static volatile Map<Integer, Integer> g = new ConcurrentHashMap(10);
    /* access modifiers changed from: private */
    public static volatile long h = 0;
    private static volatile long i = 0;
    private static volatile long j = 0;
    private static String k = "";
    private static volatile int l = 0;
    private static volatile String m = "";
    private static volatile String n = "";
    private static Map<String, Long> o = new ConcurrentHashMap();
    private static Map<String, Long> p = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public static com.tencent.b.a.b.b q = l.c();
    /* access modifiers changed from: private */
    public static Thread.UncaughtExceptionHandler r = null;
    private static volatile boolean s = true;
    /* access modifiers changed from: private */
    public static Context t = null;

    static int a(Context context, boolean z, w wVar) {
        boolean z2 = true;
        long currentTimeMillis = System.currentTimeMillis();
        boolean z3 = z && currentTimeMillis - i >= ((long) t.d());
        i = currentTimeMillis;
        if (j == 0) {
            j = l.d();
        }
        if (currentTimeMillis >= j) {
            j = l.d();
            if (ai.a(context).b(context).d() != 1) {
                ai.a(context).b(context).c();
            }
            t.s();
            f1347a = 0;
            k = l.e();
            z3 = true;
        }
        String str = k;
        if (l.a(wVar)) {
            str = wVar.c() + k;
        }
        if (p.containsKey(str)) {
            z2 = z3;
        }
        if (z2) {
            if (l.a(wVar)) {
                a(context, wVar);
            } else if (t.t() < t.q()) {
                l.t(context);
                a(context, (w) null);
            } else {
                q.e("Exceed StatConfig.getMaxDaySessionNumbers().");
            }
            p.put(str, 1L);
        }
        if (s) {
            if (t.c()) {
                Context e2 = e(context);
                if (e2 == null) {
                    q.d("The Context of StatService.testSpeed() can not be null!");
                } else if (g(e2) != null) {
                    d.a(new z(e2));
                }
            }
            s = false;
        }
        return l;
    }

    public static Properties a(String str) {
        return f.get(str);
    }

    static void a(Context context) {
        if (t.c()) {
            Context e2 = e(context);
            if (e2 == null) {
                q.d("The Context of StatService.sendNetworkDetector() can not be null!");
                return;
            }
            try {
                l.b(e2).a(new com.tencent.b.a.a.f(e2), new q());
            } catch (Throwable th) {
                q.b(th);
            }
        }
    }

    private static void a(Context context, w wVar) {
        if (g(context) != null) {
            if (t.b()) {
                q.g("start new session.");
            }
            if (wVar == null || l == 0) {
                l = l.a();
            }
            t.p();
            t.r();
            new ae(new h(context, l, i(), wVar)).a();
        }
    }

    public static void a(Context context, String str) {
        if (t.c()) {
            Context e2 = e(context);
            if (e2 == null) {
                q.d("The Context of StatService.trackCustomEvent() can not be null!");
                return;
            }
            if (str == null || str.length() == 0) {
                q.d("The event_id of StatService.trackCustomEvent() can not be null or empty.");
                return;
            }
            b bVar = new b(str);
            if (g(e2) != null) {
                d.a(new r(e2, bVar));
            }
        }
    }

    static void a(Context context, Throwable th) {
        if (t.c()) {
            Context e2 = e(context);
            if (e2 == null) {
                q.d("The Context of StatService.reportSdkSelfException() can not be null!");
            } else if (g(e2) != null) {
                d.a(new p(e2, th));
            }
        }
    }

    static boolean a() {
        if (f1347a < 2) {
            return false;
        }
        f1348b = System.currentTimeMillis();
        return true;
    }

    public static boolean a(Context context, String str, String str2) {
        try {
            if (!t.c()) {
                q.d("MTA StatService is disable.");
                return false;
            }
            if (t.b()) {
                q.g("MTA SDK version, current: " + "2.0.3" + " ,required: " + str2);
            }
            if (context == null || str2 == null) {
                q.d("Context or mtaSdkVersion in StatService.startStatService() is null, please check it!");
                t.a(false);
                return false;
            } else if (l.b("2.0.3") < l.b(str2)) {
                q.d(("MTA SDK version conflicted, current: " + "2.0.3" + ",required: " + str2) + ". please delete the current SDK and download the latest one. official website: http://mta.qq.com/ or http://mta.oa.com/");
                t.a(false);
                return false;
            } else {
                String b2 = t.b(context);
                if (b2 == null || b2.length() == 0) {
                    t.b("-");
                }
                if (str != null) {
                    t.a(context, str);
                }
                if (g(context) != null) {
                    d.a(new ab(context));
                }
                return true;
            }
        } catch (Throwable th) {
            q.b(th);
            return false;
        }
    }

    static void b() {
        f1347a = 0;
        f1348b = 0;
    }

    public static void b(Context context) {
        if (t.c()) {
            if (t.b()) {
                q.a("commitEvents, maxNumber=-1");
            }
            Context e2 = e(context);
            if (e2 == null) {
                q.d("The Context of StatService.commitEvents() can not be null!");
            } else if (x.a(t).f() && g(e2) != null) {
                d.a(new y(e2));
            }
        }
    }

    static void c() {
        f1347a++;
        f1348b = System.currentTimeMillis();
        c(t);
    }

    public static void c(Context context) {
        if (t.c() && t.n > 0) {
            Context e2 = e(context);
            if (e2 == null) {
                q.d("The Context of StatService.testSpeed() can not be null!");
            } else {
                ai.a(e2).c();
            }
        }
    }

    static void d(Context context) {
        c = System.currentTimeMillis() + ((long) (QDefine.ONE_MINUTE * t.l()));
        q.a(context, "last_period_ts", c);
        b(context);
    }

    private static Context e(Context context) {
        return context != null ? context : t;
    }

    private static synchronized void f(Context context) {
        boolean z = false;
        synchronized (v.class) {
            if (context != null) {
                if (d == null) {
                    long a2 = q.a(context, t.c);
                    long b2 = l.b("2.0.3");
                    boolean z2 = true;
                    if (b2 <= a2) {
                        q.d("MTA is disable for current version:" + b2 + ",wakeup version:" + a2);
                        z2 = false;
                    }
                    long a3 = q.a(context, t.d);
                    if (a3 > System.currentTimeMillis()) {
                        q.d("MTA is disable for current time:" + System.currentTimeMillis() + ",wakeup time:" + a3);
                    } else {
                        z = z2;
                    }
                    t.a(z);
                    if (z) {
                        Context applicationContext = context.getApplicationContext();
                        t = applicationContext;
                        d = new f();
                        k = l.e();
                        h = System.currentTimeMillis() + t.i;
                        d.a(new o(applicationContext));
                    }
                }
            }
        }
    }

    private static f g(Context context) {
        if (d == null) {
            synchronized (v.class) {
                if (d == null) {
                    try {
                        f(context);
                    } catch (Throwable th) {
                        q.a(th);
                        t.a(false);
                    }
                }
            }
        }
        return d;
    }

    private static JSONObject i() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            if (t.f1344b.d != 0) {
                jSONObject2.put("v", t.f1344b.d);
            }
            jSONObject.put(Integer.toString(t.f1344b.f1329a), jSONObject2);
            JSONObject jSONObject3 = new JSONObject();
            if (t.f1343a.d != 0) {
                jSONObject3.put("v", t.f1343a.d);
            }
            jSONObject.put(Integer.toString(t.f1343a.f1329a), jSONObject3);
        } catch (JSONException e2) {
            q.b((Throwable) e2);
        }
        return jSONObject;
    }
}
