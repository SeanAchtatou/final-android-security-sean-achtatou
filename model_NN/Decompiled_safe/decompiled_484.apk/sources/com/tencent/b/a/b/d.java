package com.tencent.b.a.b;

import android.content.Context;
import com.tencent.b.a.x;
import org.json.JSONObject;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    static e f1306a;
    private static b d = l.c();
    private static JSONObject e = new JSONObject();

    /* renamed from: b  reason: collision with root package name */
    Integer f1307b = null;
    String c = null;

    public d(Context context) {
        try {
            a(context);
            this.f1307b = l.k(context.getApplicationContext());
            this.c = x.a(context).b();
        } catch (Throwable th) {
            d.b(th);
        }
    }

    private static synchronized e a(Context context) {
        e eVar;
        synchronized (d.class) {
            if (f1306a == null) {
                f1306a = new e(context.getApplicationContext(), (byte) 0);
            }
            eVar = f1306a;
        }
        return eVar;
    }

    public final void a(JSONObject jSONObject, Thread thread) {
        JSONObject jSONObject2 = new JSONObject();
        try {
            if (f1306a != null) {
                f1306a.a(jSONObject2, thread);
            }
            r.a(jSONObject2, "cn", this.c);
            if (this.f1307b != null) {
                jSONObject2.put("tn", this.f1307b);
            }
            if (thread == null) {
                jSONObject.put("ev", jSONObject2);
            } else {
                jSONObject.put("errkv", jSONObject2.toString());
            }
            if (e != null && e.length() > 0) {
                jSONObject.put("eva", e);
            }
        } catch (Throwable th) {
            d.b(th);
        }
    }
}
