package com.tencent.b.a;

import android.content.Context;
import com.tencent.b.a.a.c;
import com.tencent.b.a.a.f;

final class p implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f1337a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Throwable f1338b;

    p(Context context, Throwable th) {
        this.f1337a = context;
        this.f1338b = th;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.b.a.v.a(android.content.Context, boolean, com.tencent.b.a.w):int
     arg types: [android.content.Context, int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.b.a.v.a(android.content.Context, java.lang.String, java.lang.String):boolean
      com.tencent.b.a.v.a(android.content.Context, boolean, com.tencent.b.a.w):int */
    public final void run() {
        try {
            if (t.c()) {
                new ae(new c(this.f1337a, v.a(this.f1337a, false, (w) null), this.f1338b, f.f1273a)).a();
            }
        } catch (Throwable th) {
            v.q.e("reportSdkSelfException error: " + th);
        }
    }
}
