package com.tencent.b.a.a;

import com.qihoo.messenger.util.QDefine;

public enum e {
    PAGE_VIEW(1),
    SESSION_ENV(2),
    ERROR(3),
    CUSTOM(QDefine.ONE_SECOND),
    ADDITION(1001),
    MONITOR_STAT(1002),
    MTA_GAME_USER(1003),
    NETWORK_MONITOR(1004),
    NETWORK_DETECTOR(1005);
    
    private int j;

    private e(int i) {
        this.j = i;
    }

    public final int a() {
        return this.j;
    }
}
