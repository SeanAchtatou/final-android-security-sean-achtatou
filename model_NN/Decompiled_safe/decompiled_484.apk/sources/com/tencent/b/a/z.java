package com.tencent.b.a;

import android.content.Context;

final class z implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f1355a;

    z(Context context) {
        this.f1355a = context;
    }

    public final void run() {
        try {
            new Thread(new ad(this.f1355a), "NetworkMonitorTask").start();
        } catch (Throwable th) {
            v.q.b(th);
            v.a(this.f1355a, th);
        }
    }
}
