package com.tencent.b.a.a;

import org.json.JSONArray;
import org.json.JSONObject;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    public String f1266a;

    /* renamed from: b  reason: collision with root package name */
    public JSONArray f1267b;
    public JSONObject c = null;

    public b() {
    }

    public b(String str) {
        this.f1266a = str;
        this.c = new JSONObject();
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj instanceof b) {
            return toString().equals(((b) obj).toString());
        }
        return false;
    }

    public final int hashCode() {
        return toString().hashCode();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(32);
        sb.append(this.f1266a).append(",");
        if (this.f1267b != null) {
            sb.append(this.f1267b.toString());
        }
        if (this.c != null) {
            sb.append(this.c.toString());
        }
        return sb.toString();
    }
}
