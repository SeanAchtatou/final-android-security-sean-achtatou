package com.tencent.b.a.b;

import android.util.Log;
import com.tencent.b.a.t;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private String f1302a = "default";

    /* renamed from: b  reason: collision with root package name */
    private boolean f1303b = true;
    private int c = 2;

    public b() {
    }

    public b(String str) {
        this.f1302a = str;
    }

    private String b() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        if (stackTrace == null) {
            return null;
        }
        for (StackTraceElement stackTraceElement : stackTrace) {
            if (!stackTraceElement.isNativeMethod() && !stackTraceElement.getClassName().equals(Thread.class.getName()) && !stackTraceElement.getClassName().equals(getClass().getName())) {
                return "[" + Thread.currentThread().getName() + "(" + Thread.currentThread().getId() + "): " + stackTraceElement.getFileName() + ":" + stackTraceElement.getLineNumber() + "]";
            }
        }
        return null;
    }

    public final void a() {
        this.f1303b = false;
    }

    public final void a(Object obj) {
        if (this.f1303b && this.c <= 4) {
            String b2 = b();
            Log.i(this.f1302a, b2 == null ? obj.toString() : b2 + " - " + obj);
            t.v();
        }
    }

    public final void a(Throwable th) {
        if (this.c <= 6) {
            Log.e(this.f1302a, "", th);
            t.v();
        }
    }

    public final void b(Object obj) {
        if (this.c <= 5) {
            String b2 = b();
            Log.w(this.f1302a, b2 == null ? obj.toString() : b2 + " - " + obj);
            t.v();
        }
    }

    public final void b(Throwable th) {
        if (this.f1303b) {
            a(th);
        }
    }

    public final void c(Object obj) {
        if (this.f1303b) {
            b(obj);
        }
    }

    public final void d(Object obj) {
        if (this.c <= 6) {
            String b2 = b();
            Log.e(this.f1302a, b2 == null ? obj.toString() : b2 + " - " + obj);
            t.v();
        }
    }

    public final void e(Object obj) {
        if (this.f1303b) {
            d(obj);
        }
    }

    public final void f(Object obj) {
        if (this.c <= 3) {
            String b2 = b();
            Log.d(this.f1302a, b2 == null ? obj.toString() : b2 + " - " + obj);
            t.v();
        }
    }

    public final void g(Object obj) {
        if (this.f1303b) {
            f(obj);
        }
    }
}
