package com.tencent.b.a.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public final class q {

    /* renamed from: a  reason: collision with root package name */
    private static SharedPreferences f1317a = null;

    public static int a(Context context, String str, int i) {
        return a(context).getInt(l.a(context, "wxop_" + str), i);
    }

    public static long a(Context context, String str) {
        return a(context).getLong(l.a(context, "wxop_" + str), 0);
    }

    private static synchronized SharedPreferences a(Context context) {
        SharedPreferences sharedPreferences;
        synchronized (q.class) {
            SharedPreferences sharedPreferences2 = context.getSharedPreferences(".mta-wxop", 0);
            f1317a = sharedPreferences2;
            if (sharedPreferences2 == null) {
                f1317a = PreferenceManager.getDefaultSharedPreferences(context);
            }
            sharedPreferences = f1317a;
        }
        return sharedPreferences;
    }

    public static String a(Context context, String str, String str2) {
        return a(context).getString(l.a(context, "wxop_" + str), str2);
    }

    public static void a(Context context, String str, long j) {
        String a2 = l.a(context, "wxop_" + str);
        SharedPreferences.Editor edit = a(context).edit();
        edit.putLong(a2, j);
        edit.commit();
    }

    public static void b(Context context, String str, int i) {
        String a2 = l.a(context, "wxop_" + str);
        SharedPreferences.Editor edit = a(context).edit();
        edit.putInt(a2, i);
        edit.commit();
    }

    public static void b(Context context, String str, String str2) {
        String a2 = l.a(context, "wxop_" + str);
        SharedPreferences.Editor edit = a(context).edit();
        edit.putString(a2, str2);
        edit.commit();
    }
}
