package com.tencent.b.a;

import android.content.Context;
import com.tencent.b.a.a.d;

final class ae {
    private static volatile long f = 0;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public d f1281a;

    /* renamed from: b  reason: collision with root package name */
    private u f1282b = null;
    /* access modifiers changed from: private */
    public boolean c = false;
    /* access modifiers changed from: private */
    public Context d = null;
    private long e = System.currentTimeMillis();

    public ae(d dVar) {
        this.f1281a = dVar;
        this.f1282b = t.a();
        this.c = dVar.f();
        this.d = dVar.e();
    }

    private void a(k kVar) {
        l.b(v.t).a(this.f1281a, kVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.b.a.ai.a(com.tencent.b.a.a.d, com.tencent.b.a.k, boolean, boolean):void
     arg types: [com.tencent.b.a.a.d, ?[OBJECT, ARRAY], boolean, int]
     candidates:
      com.tencent.b.a.ai.a(com.tencent.b.a.ai, java.util.List, int, boolean):void
      com.tencent.b.a.ai.a(com.tencent.b.a.a.d, com.tencent.b.a.k, boolean, boolean):void */
    private void b() {
        if (ai.b().f1286a <= 0 || !t.m) {
            a(new ah(this));
            return;
        }
        ai.b().a(this.f1281a, (k) null, this.c, true);
        ai.b().a(-1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.b.a.ai.a(com.tencent.b.a.a.d, com.tencent.b.a.k, boolean, boolean):void
     arg types: [com.tencent.b.a.a.d, ?[OBJECT, ARRAY], boolean, int]
     candidates:
      com.tencent.b.a.ai.a(com.tencent.b.a.ai, java.util.List, int, boolean):void
      com.tencent.b.a.ai.a(com.tencent.b.a.a.d, com.tencent.b.a.k, boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.b.a.ai.a(com.tencent.b.a.a.d, com.tencent.b.a.k, boolean, boolean):void
     arg types: [com.tencent.b.a.a.d, com.tencent.b.a.af, boolean, int]
     candidates:
      com.tencent.b.a.ai.a(com.tencent.b.a.ai, java.util.List, int, boolean):void
      com.tencent.b.a.ai.a(com.tencent.b.a.a.d, com.tencent.b.a.k, boolean, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:80:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r9 = this;
            r1 = 1
            r8 = 0
            r2 = 0
            int r0 = com.tencent.b.a.t.h
            if (r0 <= 0) goto L_0x00cc
            long r4 = r9.e
            long r6 = com.tencent.b.a.v.h
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 <= 0) goto L_0x0040
            java.util.Map r0 = com.tencent.b.a.v.g
            r0.clear()
            long r4 = r9.e
            long r6 = com.tencent.b.a.t.i
            long r4 = r4 + r6
            long unused = com.tencent.b.a.v.h = r4
            boolean r0 = com.tencent.b.a.t.b()
            if (r0 == 0) goto L_0x0040
            com.tencent.b.a.b.b r0 = com.tencent.b.a.v.q
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "clear methodsCalledLimitMap, nextLimitCallClearTime="
            r3.<init>(r4)
            long r4 = com.tencent.b.a.v.h
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r0.a(r3)
        L_0x0040:
            com.tencent.b.a.a.d r0 = r9.f1281a
            com.tencent.b.a.a.e r0 = r0.b()
            int r0 = r0.a()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r0)
            java.util.Map r0 = com.tencent.b.a.v.g
            java.lang.Object r0 = r0.get(r3)
            java.lang.Integer r0 = (java.lang.Integer) r0
            if (r0 == 0) goto L_0x00c1
            java.util.Map r4 = com.tencent.b.a.v.g
            int r5 = r0.intValue()
            int r5 = r5 + 1
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r4.put(r3, r5)
            int r3 = r0.intValue()
            int r4 = com.tencent.b.a.t.h
            if (r3 <= r4) goto L_0x00cc
            boolean r3 = com.tencent.b.a.t.b()
            if (r3 == 0) goto L_0x00bd
            com.tencent.b.a.b.b r3 = com.tencent.b.a.v.q
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "event "
            r4.<init>(r5)
            com.tencent.b.a.a.d r5 = r9.f1281a
            java.lang.String r5 = r5.g()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = " was discard, cause of called limit, current:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r4 = ", limit:"
            java.lang.StringBuilder r0 = r0.append(r4)
            int r4 = com.tencent.b.a.t.h
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r4 = ", period:"
            java.lang.StringBuilder r0 = r0.append(r4)
            long r4 = com.tencent.b.a.t.i
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r4 = " ms"
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r0 = r0.toString()
            r3.e(r0)
        L_0x00bd:
            r0 = r1
        L_0x00be:
            if (r0 == 0) goto L_0x00ce
        L_0x00c0:
            return
        L_0x00c1:
            java.util.Map r0 = com.tencent.b.a.v.g
            java.lang.Integer r4 = java.lang.Integer.valueOf(r1)
            r0.put(r3, r4)
        L_0x00cc:
            r0 = r2
            goto L_0x00be
        L_0x00ce:
            int r0 = com.tencent.b.a.t.n
            if (r0 <= 0) goto L_0x0104
            long r4 = r9.e
            long r6 = com.tencent.b.a.ae.f
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 < 0) goto L_0x0104
            android.content.Context r0 = r9.d
            com.tencent.b.a.v.c(r0)
            long r4 = r9.e
            long r6 = com.tencent.b.a.t.o
            long r4 = r4 + r6
            com.tencent.b.a.ae.f = r4
            boolean r0 = com.tencent.b.a.t.b()
            if (r0 == 0) goto L_0x0104
            com.tencent.b.a.b.b r0 = com.tencent.b.a.v.q
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "nextFlushTime="
            r3.<init>(r4)
            long r4 = com.tencent.b.a.ae.f
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r0.a(r3)
        L_0x0104:
            android.content.Context r0 = r9.d
            com.tencent.b.a.x r0 = com.tencent.b.a.x.a(r0)
            boolean r0 = r0.f()
            if (r0 == 0) goto L_0x02f3
            boolean r0 = com.tencent.b.a.t.b()
            if (r0 == 0) goto L_0x012e
            com.tencent.b.a.b.b r0 = com.tencent.b.a.v.q
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "sendFailedCount="
            r3.<init>(r4)
            int r4 = com.tencent.b.a.v.f1347a
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r0.a(r3)
        L_0x012e:
            boolean r0 = com.tencent.b.a.v.a()
            if (r0 != 0) goto L_0x02d3
            com.tencent.b.a.a.d r0 = r9.f1281a
            com.tencent.b.a.w r0 = r0.d()
            if (r0 == 0) goto L_0x014c
            com.tencent.b.a.a.d r0 = r9.f1281a
            com.tencent.b.a.w r0 = r0.d()
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x014c
            com.tencent.b.a.u r0 = com.tencent.b.a.u.INSTANT
            r9.f1282b = r0
        L_0x014c:
            boolean r0 = com.tencent.b.a.t.j
            if (r0 == 0) goto L_0x0162
            android.content.Context r0 = com.tencent.b.a.v.t
            com.tencent.b.a.x r0 = com.tencent.b.a.x.a(r0)
            boolean r0 = r0.e()
            if (r0 == 0) goto L_0x0162
            com.tencent.b.a.u r0 = com.tencent.b.a.u.INSTANT
            r9.f1282b = r0
        L_0x0162:
            boolean r0 = com.tencent.b.a.t.b()
            if (r0 == 0) goto L_0x0184
            com.tencent.b.a.b.b r0 = com.tencent.b.a.v.q
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "strategy="
            r3.<init>(r4)
            com.tencent.b.a.u r4 = r9.f1282b
            java.lang.String r4 = r4.name()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r0.a(r3)
        L_0x0184:
            int[] r0 = com.tencent.b.a.aa.f1276a
            com.tencent.b.a.u r3 = r9.f1282b
            int r3 = r3.ordinal()
            r0 = r0[r3]
            switch(r0) {
                case 1: goto L_0x01ad;
                case 2: goto L_0x01b2;
                case 3: goto L_0x027c;
                case 4: goto L_0x027c;
                case 5: goto L_0x028b;
                case 6: goto L_0x029f;
                case 7: goto L_0x02c1;
                default: goto L_0x0191;
            }
        L_0x0191:
            com.tencent.b.a.b.b r0 = com.tencent.b.a.v.q
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Invalid stat strategy:"
            r1.<init>(r2)
            com.tencent.b.a.u r2 = com.tencent.b.a.t.a()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.d(r1)
            goto L_0x00c0
        L_0x01ad:
            r9.b()
            goto L_0x00c0
        L_0x01b2:
            android.content.Context r0 = r9.d
            com.tencent.b.a.ai r0 = com.tencent.b.a.ai.a(r0)
            com.tencent.b.a.a.d r1 = r9.f1281a
            boolean r3 = r9.c
            r0.a(r1, r8, r3, r2)
            boolean r0 = com.tencent.b.a.t.b()
            if (r0 == 0) goto L_0x01f8
            com.tencent.b.a.b.b r0 = com.tencent.b.a.v.q
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "PERIOD currTime="
            r1.<init>(r2)
            long r2 = r9.e
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = ",nextPeriodSendTs="
            java.lang.StringBuilder r1 = r1.append(r2)
            long r2 = com.tencent.b.a.v.c
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = ",difftime="
            java.lang.StringBuilder r1 = r1.append(r2)
            long r2 = com.tencent.b.a.v.c
            long r4 = r9.e
            long r2 = r2 - r4
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.a(r1)
        L_0x01f8:
            long r0 = com.tencent.b.a.v.c
            r2 = 0
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 != 0) goto L_0x0234
            android.content.Context r0 = r9.d
            java.lang.String r1 = "last_period_ts"
            long r0 = com.tencent.b.a.b.q.a(r0, r1)
            com.tencent.b.a.v.c = r0
            long r0 = r9.e
            long r2 = com.tencent.b.a.v.c
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x0217
            android.content.Context r0 = r9.d
            com.tencent.b.a.v.d(r0)
        L_0x0217:
            long r0 = r9.e
            int r2 = com.tencent.b.a.t.l()
            int r2 = r2 * 60
            int r2 = r2 * 1000
            long r2 = (long) r2
            long r0 = r0 + r2
            long r2 = com.tencent.b.a.v.c
            int r2 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r2 <= 0) goto L_0x022b
            com.tencent.b.a.v.c = r0
        L_0x022b:
            android.content.Context r0 = r9.d
            com.tencent.b.a.g r0 = com.tencent.b.a.g.a(r0)
            r0.a()
        L_0x0234:
            boolean r0 = com.tencent.b.a.t.b()
            if (r0 == 0) goto L_0x026d
            com.tencent.b.a.b.b r0 = com.tencent.b.a.v.q
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "PERIOD currTime="
            r1.<init>(r2)
            long r2 = r9.e
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = ",nextPeriodSendTs="
            java.lang.StringBuilder r1 = r1.append(r2)
            long r2 = com.tencent.b.a.v.c
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = ",difftime="
            java.lang.StringBuilder r1 = r1.append(r2)
            long r2 = com.tencent.b.a.v.c
            long r4 = r9.e
            long r2 = r2 - r4
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.a(r1)
        L_0x026d:
            long r0 = r9.e
            long r2 = com.tencent.b.a.v.c
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x00c0
            android.content.Context r0 = r9.d
            com.tencent.b.a.v.d(r0)
            goto L_0x00c0
        L_0x027c:
            android.content.Context r0 = r9.d
            com.tencent.b.a.ai r0 = com.tencent.b.a.ai.a(r0)
            com.tencent.b.a.a.d r1 = r9.f1281a
            boolean r3 = r9.c
            r0.a(r1, r8, r3, r2)
            goto L_0x00c0
        L_0x028b:
            android.content.Context r0 = r9.d
            com.tencent.b.a.ai r0 = com.tencent.b.a.ai.a(r0)
            com.tencent.b.a.a.d r2 = r9.f1281a
            com.tencent.b.a.af r3 = new com.tencent.b.a.af
            r3.<init>(r9)
            boolean r4 = r9.c
            r0.a(r2, r3, r4, r1)
            goto L_0x00c0
        L_0x029f:
            android.content.Context r0 = com.tencent.b.a.v.t
            com.tencent.b.a.x r0 = com.tencent.b.a.x.a(r0)
            int r0 = r0.c()
            if (r0 != r1) goto L_0x02b2
            r9.b()
            goto L_0x00c0
        L_0x02b2:
            android.content.Context r0 = r9.d
            com.tencent.b.a.ai r0 = com.tencent.b.a.ai.a(r0)
            com.tencent.b.a.a.d r1 = r9.f1281a
            boolean r3 = r9.c
            r0.a(r1, r8, r3, r2)
            goto L_0x00c0
        L_0x02c1:
            android.content.Context r0 = r9.d
            boolean r0 = com.tencent.b.a.b.l.e(r0)
            if (r0 == 0) goto L_0x00c0
            com.tencent.b.a.ag r0 = new com.tencent.b.a.ag
            r0.<init>(r9)
            r9.a(r0)
            goto L_0x00c0
        L_0x02d3:
            android.content.Context r0 = r9.d
            com.tencent.b.a.ai r0 = com.tencent.b.a.ai.a(r0)
            com.tencent.b.a.a.d r1 = r9.f1281a
            boolean r3 = r9.c
            r0.a(r1, r8, r3, r2)
            long r0 = r9.e
            long r2 = com.tencent.b.a.v.f1348b
            long r0 = r0 - r2
            r2 = 1800000(0x1b7740, double:8.89318E-318)
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x00c0
            android.content.Context r0 = r9.d
            com.tencent.b.a.v.a(r0)
            goto L_0x00c0
        L_0x02f3:
            android.content.Context r0 = r9.d
            com.tencent.b.a.ai r0 = com.tencent.b.a.ai.a(r0)
            com.tencent.b.a.a.d r1 = r9.f1281a
            boolean r3 = r9.c
            r0.a(r1, r8, r3, r2)
            goto L_0x00c0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.b.a.ae.a():void");
    }
}
