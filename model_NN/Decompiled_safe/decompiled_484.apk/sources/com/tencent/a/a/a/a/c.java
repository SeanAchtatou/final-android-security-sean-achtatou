package com.tencent.a.a.a.a;

import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    String f1260a = null;

    /* renamed from: b  reason: collision with root package name */
    String f1261b = null;
    String c = "0";
    long d = 0;

    static c a(String str) {
        c cVar = new c();
        if (h.a(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (!jSONObject.isNull("ui")) {
                    cVar.f1260a = jSONObject.getString("ui");
                }
                if (!jSONObject.isNull("mc")) {
                    cVar.f1261b = jSONObject.getString("mc");
                }
                if (!jSONObject.isNull("mid")) {
                    cVar.c = jSONObject.getString("mid");
                }
                if (!jSONObject.isNull("ts")) {
                    cVar.d = jSONObject.getLong("ts");
                }
            } catch (JSONException e) {
                Log.w("MID", e);
            }
        }
        return cVar;
    }

    private JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            h.a(jSONObject, "ui", this.f1260a);
            h.a(jSONObject, "mc", this.f1261b);
            h.a(jSONObject, "mid", this.c);
            jSONObject.put("ts", this.d);
        } catch (JSONException e) {
            Log.w("MID", e);
        }
        return jSONObject;
    }

    public final String a() {
        return this.c;
    }

    public final String toString() {
        return b().toString();
    }
}
