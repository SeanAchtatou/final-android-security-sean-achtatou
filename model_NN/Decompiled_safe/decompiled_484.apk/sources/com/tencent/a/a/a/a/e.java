package com.tencent.a.a.a.a;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;

public final class e extends f {
    public e(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        synchronized (this) {
            Log.i("MID", "write mid to Settings.System");
            Settings.System.putString(this.f1262a.getContentResolver(), h.c("4kU71lN96TJUomD1vOU9lgj9Tw=="), str);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a() {
        return h.a(this.f1262a, "android.permission.WRITE_SETTINGS");
    }

    /* access modifiers changed from: protected */
    public final String b() {
        String string;
        synchronized (this) {
            Log.i("MID", "read mid from Settings.System");
            string = Settings.System.getString(this.f1262a.getContentResolver(), h.c("4kU71lN96TJUomD1vOU9lgj9Tw=="));
        }
        return string;
    }
}
