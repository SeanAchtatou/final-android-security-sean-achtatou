package com.tencent.mm.sdk.modelmsg;

import android.os.Bundle;
import com.tencent.mm.sdk.d.a;

public class d extends a {
    public String c;
    public String d;
    public String e;
    public String f;

    public d() {
    }

    public d(Bundle bundle) {
        b(bundle);
    }

    public int a() {
        return 6;
    }

    public void a(Bundle bundle) {
        super.a(bundle);
        bundle.putString("_wxobject_message_action", this.c);
        bundle.putString("_wxobject_message_ext", this.d);
        bundle.putString("_wxapi_launch_req_lang", this.e);
        bundle.putString("_wxapi_launch_req_country", this.f);
    }

    public void b(Bundle bundle) {
        super.b(bundle);
        this.c = bundle.getString("_wxobject_message_action");
        this.d = bundle.getString("_wxobject_message_ext");
        this.e = bundle.getString("_wxapi_launch_req_lang");
        this.f = bundle.getString("_wxapi_launch_req_country");
    }

    public boolean b() {
        if (this.c != null && this.c.length() > 2048) {
            com.tencent.mm.sdk.b.a.a("MicroMsg.SDK.LaunchFromWX.Req", "checkArgs fail, messageAction is too long");
            return false;
        } else if (this.d == null || this.d.length() <= 2048) {
            return true;
        } else {
            com.tencent.mm.sdk.b.a.a("MicroMsg.SDK.LaunchFromWX.Req", "checkArgs fail, messageExt is too long");
            return false;
        }
    }
}
