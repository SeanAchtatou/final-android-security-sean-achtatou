package com.tencent.mm.sdk.modelmsg;

import android.os.Bundle;
import com.tencent.mm.sdk.d.a;

public class l extends a {
    public WXMediaMessage c;
    public String d;
    public String e;

    public l() {
    }

    public l(Bundle bundle) {
        b(bundle);
    }

    public int a() {
        return 4;
    }

    public void a(Bundle bundle) {
        Bundle a2 = m.a(this.c);
        super.a(a2);
        bundle.putString("_wxapi_showmessage_req_lang", this.d);
        bundle.putString("_wxapi_showmessage_req_country", this.e);
        bundle.putAll(a2);
    }

    public void b(Bundle bundle) {
        super.b(bundle);
        this.d = bundle.getString("_wxapi_showmessage_req_lang");
        this.e = bundle.getString("_wxapi_showmessage_req_country");
        this.c = m.a(bundle);
    }

    public boolean b() {
        if (this.c == null) {
            return false;
        }
        return this.c.checkArgs();
    }
}
