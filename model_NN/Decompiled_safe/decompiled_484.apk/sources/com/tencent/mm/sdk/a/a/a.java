package com.tencent.mm.sdk.a.a;

import android.content.Context;
import android.content.Intent;
import com.tencent.mm.sdk.b.f;

public final class a {
    public static boolean a(Context context, b bVar) {
        if (context == null || bVar == null) {
            com.tencent.mm.sdk.b.a.a("MicroMsg.SDK.MMessage", "send fail, invalid argument");
            return false;
        } else if (f.a(bVar.f1358b)) {
            com.tencent.mm.sdk.b.a.a("MicroMsg.SDK.MMessage", "send fail, action is null");
            return false;
        } else {
            String str = null;
            if (!f.a(bVar.f1357a)) {
                str = bVar.f1357a + ".permission.MM_MESSAGE";
            }
            Intent intent = new Intent(bVar.f1358b);
            if (bVar.d != null) {
                intent.putExtras(bVar.d);
            }
            String packageName = context.getPackageName();
            intent.putExtra("_mmessage_sdkVersion", 570490883);
            intent.putExtra("_mmessage_appPackage", packageName);
            intent.putExtra("_mmessage_content", bVar.c);
            intent.putExtra("_mmessage_checksum", c.a(bVar.c, 570490883, packageName));
            context.sendBroadcast(intent, str);
            com.tencent.mm.sdk.b.a.d("MicroMsg.SDK.MMessage", "send mm message, intent=" + intent + ", perm=" + str);
            return true;
        }
    }
}
