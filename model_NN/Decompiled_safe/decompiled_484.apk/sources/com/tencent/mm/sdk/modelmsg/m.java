package com.tencent.mm.sdk.modelmsg;

import android.os.Bundle;
import com.tencent.mm.sdk.b.a;

public class m {
    public static Bundle a(WXMediaMessage wXMediaMessage) {
        Bundle bundle = new Bundle();
        bundle.putInt("_wxobject_sdkVer", wXMediaMessage.sdkVer);
        bundle.putString("_wxobject_title", wXMediaMessage.title);
        bundle.putString("_wxobject_description", wXMediaMessage.description);
        bundle.putByteArray("_wxobject_thumbdata", wXMediaMessage.thumbData);
        if (wXMediaMessage.mediaObject != null) {
            bundle.putString("_wxobject_identifier_", a(wXMediaMessage.mediaObject.getClass().getName()));
            wXMediaMessage.mediaObject.serialize(bundle);
        }
        bundle.putString("_wxobject_mediatagname", wXMediaMessage.mediaTagName);
        bundle.putString("_wxobject_message_action", wXMediaMessage.messageAction);
        bundle.putString("_wxobject_message_ext", wXMediaMessage.messageExt);
        return bundle;
    }

    public static WXMediaMessage a(Bundle bundle) {
        WXMediaMessage wXMediaMessage = new WXMediaMessage();
        wXMediaMessage.sdkVer = bundle.getInt("_wxobject_sdkVer");
        wXMediaMessage.title = bundle.getString("_wxobject_title");
        wXMediaMessage.description = bundle.getString("_wxobject_description");
        wXMediaMessage.thumbData = bundle.getByteArray("_wxobject_thumbdata");
        wXMediaMessage.mediaTagName = bundle.getString("_wxobject_mediatagname");
        wXMediaMessage.messageAction = bundle.getString("_wxobject_message_action");
        wXMediaMessage.messageExt = bundle.getString("_wxobject_message_ext");
        String b2 = b(bundle.getString("_wxobject_identifier_"));
        if (b2 == null || b2.length() <= 0) {
            return wXMediaMessage;
        }
        try {
            wXMediaMessage.mediaObject = (n) Class.forName(b2).newInstance();
            wXMediaMessage.mediaObject.unserialize(bundle);
            return wXMediaMessage;
        } catch (Exception e) {
            e.printStackTrace();
            a.a("MicroMsg.SDK.WXMediaMessage", "get media object from bundle failed: unknown ident " + b2 + ", ex = " + e.getMessage());
            return wXMediaMessage;
        }
    }

    private static String a(String str) {
        if (str != null && str.length() != 0) {
            return str.replace("com.tencent.mm.sdk.modelmsg", "com.tencent.mm.sdk.openapi");
        }
        a.a("MicroMsg.SDK.WXMediaMessage", "pathNewToOld fail, newPath is null");
        return str;
    }

    private static String b(String str) {
        if (str != null && str.length() != 0) {
            return str.replace("com.tencent.mm.sdk.openapi", "com.tencent.mm.sdk.modelmsg");
        }
        a.a("MicroMsg.SDK.WXMediaMessage", "pathOldToNew fail, oldPath is null");
        return str;
    }
}
