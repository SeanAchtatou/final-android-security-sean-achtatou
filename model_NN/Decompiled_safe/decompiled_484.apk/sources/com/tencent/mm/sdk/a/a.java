package com.tencent.mm.sdk.a;

import android.content.Context;
import android.content.Intent;
import com.tencent.mm.sdk.a.a.c;
import com.tencent.mm.sdk.b.f;

public final class a {
    public static boolean a(Context context, b bVar) {
        if (context == null || bVar == null) {
            com.tencent.mm.sdk.b.a.a("MicroMsg.SDK.MMessageAct", "send fail, invalid argument");
            return false;
        } else if (f.a(bVar.f1359a)) {
            com.tencent.mm.sdk.b.a.a("MicroMsg.SDK.MMessageAct", "send fail, invalid targetPkgName, targetPkgName = " + bVar.f1359a);
            return false;
        } else {
            if (f.a(bVar.f1360b)) {
                bVar.f1360b = bVar.f1359a + ".wxapi.WXEntryActivity";
            }
            com.tencent.mm.sdk.b.a.d("MicroMsg.SDK.MMessageAct", "send, targetPkgName = " + bVar.f1359a + ", targetClassName = " + bVar.f1360b);
            Intent intent = new Intent();
            intent.setClassName(bVar.f1359a, bVar.f1360b);
            if (bVar.e != null) {
                intent.putExtras(bVar.e);
            }
            String packageName = context.getPackageName();
            intent.putExtra("_mmessage_sdkVersion", 570490883);
            intent.putExtra("_mmessage_appPackage", packageName);
            intent.putExtra("_mmessage_content", bVar.c);
            intent.putExtra("_mmessage_checksum", c.a(bVar.c, 570490883, packageName));
            if (bVar.d == -1) {
                intent.addFlags(268435456).addFlags(134217728);
            } else {
                intent.setFlags(bVar.d);
            }
            try {
                context.startActivity(intent);
                com.tencent.mm.sdk.b.a.d("MicroMsg.SDK.MMessageAct", "send mm message, intent=" + intent);
                return true;
            } catch (Exception e) {
                com.tencent.mm.sdk.b.a.a("MicroMsg.SDK.MMessageAct", "send fail, ex = %s", e.getMessage());
                return false;
            }
        }
    }
}
