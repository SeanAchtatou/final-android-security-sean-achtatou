package com.tencent.mm.sdk.modelmsg;

import android.os.Bundle;
import com.tencent.mm.sdk.d.a;

public class b extends a {
    public String c;
    public String d;

    public b() {
    }

    public b(Bundle bundle) {
        b(bundle);
    }

    public int a() {
        return 3;
    }

    public void a(Bundle bundle) {
        super.a(bundle);
        bundle.putString("_wxapi_getmessage_req_lang", this.c);
        bundle.putString("_wxapi_getmessage_req_country", this.d);
    }

    public void b(Bundle bundle) {
        super.b(bundle);
        this.c = bundle.getString("_wxapi_getmessage_req_lang");
        this.d = bundle.getString("_wxapi_getmessage_req_country");
    }

    public boolean b() {
        return true;
    }
}
