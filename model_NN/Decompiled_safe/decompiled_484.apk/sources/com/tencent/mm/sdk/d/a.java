package com.tencent.mm.sdk.d;

import android.os.Bundle;

public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    public String f1370a;

    /* renamed from: b  reason: collision with root package name */
    public String f1371b;

    public abstract int a();

    public void a(Bundle bundle) {
        bundle.putInt("_wxapi_command_type", a());
        bundle.putString("_wxapi_basereq_transaction", this.f1370a);
        bundle.putString("_wxapi_basereq_openid", this.f1371b);
    }

    public void b(Bundle bundle) {
        this.f1370a = bundle.getString("_wxapi_basereq_transaction");
        this.f1371b = bundle.getString("_wxapi_basereq_openid");
    }

    public abstract boolean b();
}
