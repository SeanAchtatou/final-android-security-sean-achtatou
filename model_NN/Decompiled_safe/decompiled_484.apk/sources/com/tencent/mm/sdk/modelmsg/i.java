package com.tencent.mm.sdk.modelmsg;

import android.os.Bundle;
import com.tencent.mm.sdk.d.a;

public class i extends a {
    public WXMediaMessage c;
    public int d;

    public int a() {
        return 2;
    }

    public void a(Bundle bundle) {
        super.a(bundle);
        bundle.putAll(m.a(this.c));
        bundle.putInt("_wxapi_sendmessagetowx_req_scene", this.d);
    }

    public void b(Bundle bundle) {
        super.b(bundle);
        this.c = m.a(bundle);
        this.d = bundle.getInt("_wxapi_sendmessagetowx_req_scene");
    }

    public boolean b() {
        if (this.c == null) {
            com.tencent.mm.sdk.b.a.a("MicroMsg.SDK.SendMessageToWX.Req", "checkArgs fail ,message is null");
            return false;
        }
        if (this.c.mediaObject.type() == 6 && this.d == 2) {
            ((WXFileObject) this.c.mediaObject).setContentLengthLimit(26214400);
        }
        return this.c.checkArgs();
    }
}
