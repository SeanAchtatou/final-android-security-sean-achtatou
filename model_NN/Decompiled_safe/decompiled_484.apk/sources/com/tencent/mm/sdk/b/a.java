package com.tencent.mm.sdk.b;

import android.os.Build;
import android.os.Looper;
import android.os.Process;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    public static e f1361a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static int f1362b = 6;
    private static b c;
    private static b d;
    private static final String e;

    static {
        c cVar = new c();
        c = cVar;
        d = cVar;
        StringBuilder sb = new StringBuilder();
        sb.append("VERSION.RELEASE:[" + Build.VERSION.RELEASE);
        sb.append("] VERSION.CODENAME:[" + Build.VERSION.CODENAME);
        sb.append("] VERSION.INCREMENTAL:[" + Build.VERSION.INCREMENTAL);
        sb.append("] BOARD:[" + Build.BOARD);
        sb.append("] DEVICE:[" + Build.DEVICE);
        sb.append("] DISPLAY:[" + Build.DISPLAY);
        sb.append("] FINGERPRINT:[" + Build.FINGERPRINT);
        sb.append("] HOST:[" + Build.HOST);
        sb.append("] MANUFACTURER:[" + Build.MANUFACTURER);
        sb.append("] MODEL:[" + Build.MODEL);
        sb.append("] PRODUCT:[" + Build.PRODUCT);
        sb.append("] TAGS:[" + Build.TAGS);
        sb.append("] TYPE:[" + Build.TYPE);
        sb.append("] USER:[" + Build.USER + "]");
        e = sb.toString();
    }

    private static String a(String str) {
        return f1361a != null ? f1361a.a(str) : str;
    }

    public static void a(String str, String str2) {
        a(str, str2, null);
    }

    public static void a(String str, String str2, Object... objArr) {
        if (d != null && d.a() <= 4) {
            String format = objArr == null ? str2 : String.format(str2, objArr);
            if (format == null) {
                format = "";
            }
            String a2 = a(str);
            b bVar = d;
            Process.myPid();
            Thread.currentThread().getId();
            Looper.getMainLooper().getThread().getId();
            bVar.d(a2, format);
        }
    }

    public static void b(String str, String str2) {
        if (d != null && d.a() <= 3) {
            if (str2 == null) {
                str2 = "";
            }
            String a2 = a(str);
            b bVar = d;
            Process.myPid();
            Thread.currentThread().getId();
            Looper.getMainLooper().getThread().getId();
            bVar.c(a2, str2);
        }
    }

    public static void c(String str, String str2) {
        if (d != null && d.a() <= 2) {
            if (str2 == null) {
                str2 = "";
            }
            String a2 = a(str);
            b bVar = d;
            Process.myPid();
            Thread.currentThread().getId();
            Looper.getMainLooper().getThread().getId();
            bVar.a(a2, str2);
        }
    }

    public static void d(String str, String str2) {
        if (d != null && d.a() <= 1) {
            if (str2 == null) {
                str2 = "";
            }
            String a2 = a(str);
            b bVar = d;
            Process.myPid();
            Thread.currentThread().getId();
            Looper.getMainLooper().getThread().getId();
            bVar.b(a2, str2);
        }
    }
}
