package com.tencent.mm.sdk.d;

import android.os.Bundle;

public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    public int f1372a;

    /* renamed from: b  reason: collision with root package name */
    public String f1373b;
    public String c;
    public String d;

    public abstract int a();

    public void a(Bundle bundle) {
        bundle.putInt("_wxapi_command_type", a());
        bundle.putInt("_wxapi_baseresp_errcode", this.f1372a);
        bundle.putString("_wxapi_baseresp_errstr", this.f1373b);
        bundle.putString("_wxapi_baseresp_transaction", this.c);
        bundle.putString("_wxapi_baseresp_openId", this.d);
    }

    public void b(Bundle bundle) {
        this.f1372a = bundle.getInt("_wxapi_baseresp_errcode");
        this.f1373b = bundle.getString("_wxapi_baseresp_errstr");
        this.c = bundle.getString("_wxapi_baseresp_transaction");
        this.d = bundle.getString("_wxapi_baseresp_openId");
    }

    public abstract boolean b();
}
