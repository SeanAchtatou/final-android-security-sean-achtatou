package com.tencent.mm.sdk.c;

import com.tencent.mm.sdk.b.a;

public final class b {
    public static Object a(int i, String str) {
        switch (i) {
            case 1:
                return Integer.valueOf(str);
            case 2:
                return Long.valueOf(str);
            case 3:
                return str;
            case 4:
                return Boolean.valueOf(str);
            case 5:
                return Float.valueOf(str);
            case 6:
                return Double.valueOf(str);
            default:
                try {
                    a.a("MicroMsg.SDK.PluginProvider.Resolver", "unknown type");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
        }
    }
}
