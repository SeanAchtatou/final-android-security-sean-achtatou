package com.tencent.mm.sdk.openapi;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.tencent.b.a.t;
import com.tencent.b.a.u;
import com.tencent.b.a.v;
import com.tencent.mm.sdk.a.a.c;
import com.tencent.mm.sdk.a.b;
import com.tencent.mm.sdk.b.a;
import com.tencent.mm.sdk.modelmsg.d;
import com.tencent.mm.sdk.modelmsg.g;
import com.tencent.mm.sdk.modelmsg.j;
import com.tencent.mm.sdk.modelmsg.l;

final class WXApiImplV10 implements IWXAPI {
    private static final String TAG = "MicroMsg.SDK.WXApiImplV10";
    /* access modifiers changed from: private */
    public static ActivityLifecycleCb activityCb = null;
    private static String wxappPayEntryClassname = null;
    private String appId;
    private boolean checkSignature = false;
    private Context context;
    private boolean detached = false;

    final class ActivityLifecycleCb implements Application.ActivityLifecycleCallbacks {
        private static final int DELAYED = 800;
        private static final String TAG = "MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb";
        /* access modifiers changed from: private */
        public Context context;
        private Handler handler;
        /* access modifiers changed from: private */
        public boolean isForeground;
        private Runnable onPausedRunnable;
        private Runnable onResumedRunnable;

        private ActivityLifecycleCb(Context context2) {
            this.isForeground = false;
            this.handler = new Handler(Looper.getMainLooper());
            this.onPausedRunnable = new Runnable() {
                public void run() {
                    if (WXApiImplV10.activityCb != null && ActivityLifecycleCb.this.isForeground) {
                        Log.v(ActivityLifecycleCb.TAG, "WXStat trigger onBackground");
                        v.a(ActivityLifecycleCb.this.context, "onBackground_WX");
                        boolean unused = ActivityLifecycleCb.this.isForeground = false;
                    }
                }
            };
            this.onResumedRunnable = new Runnable() {
                public void run() {
                    if (WXApiImplV10.activityCb != null && !ActivityLifecycleCb.this.isForeground) {
                        Log.v(ActivityLifecycleCb.TAG, "WXStat trigger onForeground");
                        v.a(ActivityLifecycleCb.this.context, "onForeground_WX");
                        boolean unused = ActivityLifecycleCb.this.isForeground = true;
                    }
                }
            };
            this.context = context2;
        }

        public final void detach() {
            this.handler.removeCallbacks(this.onResumedRunnable);
            this.handler.removeCallbacks(this.onPausedRunnable);
            this.context = null;
        }

        public final void onActivityCreated(Activity activity, Bundle bundle) {
        }

        public final void onActivityDestroyed(Activity activity) {
        }

        public final void onActivityPaused(Activity activity) {
            Log.v(TAG, activity.getComponentName().getClassName() + "  onActivityPaused");
            this.handler.removeCallbacks(this.onResumedRunnable);
            this.handler.postDelayed(this.onPausedRunnable, 800);
        }

        public final void onActivityResumed(Activity activity) {
            Log.v(TAG, activity.getComponentName().getClassName() + "  onActivityResumed");
            this.handler.removeCallbacks(this.onPausedRunnable);
            this.handler.postDelayed(this.onResumedRunnable, 800);
        }

        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public final void onActivityStarted(Activity activity) {
        }

        public final void onActivityStopped(Activity activity) {
        }
    }

    WXApiImplV10(Context context2, String str, boolean z) {
        a.d(TAG, "<init>, appId = " + str + ", checkSignature = " + z);
        this.context = context2;
        this.appId = str;
        this.checkSignature = z;
    }

    private boolean checkSumConsistent(byte[] bArr, byte[] bArr2) {
        if (bArr == null || bArr.length == 0 || bArr2 == null || bArr2.length == 0) {
            a.a(TAG, "checkSumConsistent fail, invalid arguments");
            return false;
        } else if (bArr.length != bArr2.length) {
            a.a(TAG, "checkSumConsistent fail, length is different");
            return false;
        } else {
            for (int i = 0; i < bArr.length; i++) {
                if (bArr[i] != bArr2[i]) {
                    return false;
                }
            }
            return true;
        }
    }

    private void initMta(Context context2, String str) {
        String str2 = "AWXOP" + str;
        t.a(context2, str2);
        t.m();
        t.a(u.PERIOD);
        t.k();
        t.b(context2, "Wechat_Sdk");
        try {
            v.a(context2, str2, "2.0.3");
        } catch (com.tencent.b.a.a e) {
            e.printStackTrace();
        }
    }

    private boolean sendAddCardToWX(Context context2, Bundle bundle) {
        Cursor query = context2.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/addCardToWX"), null, null, new String[]{this.appId, bundle.getString("_wxapi_add_card_to_wx_card_list"), bundle.getString("_wxapi_basereq_transaction")}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    private boolean sendJumpToBizProfileReq(Context context2, Bundle bundle) {
        Cursor query = context2.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/jumpToBizProfile"), null, null, new String[]{this.appId, bundle.getString("_wxapi_jump_to_biz_profile_req_to_user_name"), bundle.getString("_wxapi_jump_to_biz_profile_req_ext_msg"), new StringBuilder().append(bundle.getInt("_wxapi_jump_to_biz_profile_req_scene")).toString(), new StringBuilder().append(bundle.getInt("_wxapi_jump_to_biz_profile_req_profile_type")).toString()}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    private boolean sendJumpToBizWebviewReq(Context context2, Bundle bundle) {
        Cursor query = context2.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/jumpToBizProfile"), null, null, new String[]{this.appId, bundle.getString("_wxapi_jump_to_biz_webview_req_to_user_name"), bundle.getString("_wxapi_jump_to_biz_webview_req_ext_msg"), new StringBuilder().append(bundle.getInt("_wxapi_jump_to_biz_webview_req_scene")).toString()}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    private boolean sendPayReq(Context context2, Bundle bundle) {
        if (wxappPayEntryClassname == null) {
            wxappPayEntryClassname = new MMSharedPreferences(context2).getString("_wxapp_pay_entry_classname_", null);
            a.d(TAG, "pay, set wxappPayEntryClassname = " + wxappPayEntryClassname);
            if (wxappPayEntryClassname == null) {
                a.a(TAG, "pay fail, wxappPayEntryClassname is null");
                return false;
            }
        }
        b bVar = new b();
        bVar.e = bundle;
        bVar.f1359a = "com.tencent.mm";
        bVar.f1360b = wxappPayEntryClassname;
        return com.tencent.mm.sdk.a.a.a(context2, bVar);
    }

    public final void detach() {
        a.d(TAG, "detach");
        this.detached = true;
        if (activityCb != null && Build.VERSION.SDK_INT >= 14) {
            if (this.context instanceof Activity) {
                ((Activity) this.context).getApplication().unregisterActivityLifecycleCallbacks(activityCb);
            } else if (this.context instanceof Service) {
                ((Service) this.context).getApplication().unregisterActivityLifecycleCallbacks(activityCb);
            }
            activityCb.detach();
        }
        this.context = null;
    }

    public final int getWXAppSupportAPI() {
        if (this.detached) {
            throw new IllegalStateException("getWXAppSupportAPI fail, WXMsgImpl has been detached");
        } else if (isWXAppInstalled()) {
            return new MMSharedPreferences(this.context).getInt("_build_info_sdk_int_", 0);
        } else {
            a.a(TAG, "open wx app failed, not installed or signature check failed");
            return 0;
        }
    }

    public final boolean handleIntent(Intent intent, IWXAPIEventHandler iWXAPIEventHandler) {
        if (!WXApiImplComm.isIntentFromWx(intent, "com.tencent.mm.openapi.token")) {
            a.c(TAG, "handleIntent fail, intent not from weixin msg");
            return false;
        } else if (this.detached) {
            throw new IllegalStateException("handleIntent fail, WXMsgImpl has been detached");
        } else {
            String stringExtra = intent.getStringExtra("_mmessage_content");
            int intExtra = intent.getIntExtra("_mmessage_sdkVersion", 0);
            String stringExtra2 = intent.getStringExtra("_mmessage_appPackage");
            if (stringExtra2 == null || stringExtra2.length() == 0) {
                a.a(TAG, "invalid argument");
                return false;
            } else if (!checkSumConsistent(intent.getByteArrayExtra("_mmessage_checksum"), c.a(stringExtra, intExtra, stringExtra2))) {
                a.a(TAG, "checksum fail");
                return false;
            } else {
                int intExtra2 = intent.getIntExtra("_wxapi_command_type", 0);
                switch (intExtra2) {
                    case 1:
                        iWXAPIEventHandler.onResp(new g(intent.getExtras()));
                        return true;
                    case 2:
                        iWXAPIEventHandler.onResp(new j(intent.getExtras()));
                        return true;
                    case 3:
                        iWXAPIEventHandler.onReq(new com.tencent.mm.sdk.modelmsg.b(intent.getExtras()));
                        return true;
                    case 4:
                        iWXAPIEventHandler.onReq(new l(intent.getExtras()));
                        return true;
                    case 5:
                        iWXAPIEventHandler.onResp(new com.tencent.mm.sdk.f.a(intent.getExtras()));
                        return true;
                    case 6:
                        iWXAPIEventHandler.onReq(new d(intent.getExtras()));
                        return true;
                    case 7:
                    case 8:
                    default:
                        a.a(TAG, "unknown cmd = " + intExtra2);
                        return false;
                    case 9:
                        iWXAPIEventHandler.onResp(new com.tencent.mm.sdk.e.b(intent.getExtras()));
                        return true;
                }
            }
        }
    }

    public final boolean isWXAppInstalled() {
        if (this.detached) {
            throw new IllegalStateException("isWXAppInstalled fail, WXMsgImpl has been detached");
        }
        try {
            PackageInfo packageInfo = this.context.getPackageManager().getPackageInfo("com.tencent.mm", 64);
            if (packageInfo == null) {
                return false;
            }
            return WXApiImplComm.validateAppSignature(this.context, packageInfo.signatures, this.checkSignature);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public final boolean isWXAppSupportAPI() {
        if (!this.detached) {
            return getWXAppSupportAPI() >= 570490883;
        }
        throw new IllegalStateException("isWXAppSupportAPI fail, WXMsgImpl has been detached");
    }

    public final boolean openWXApp() {
        if (this.detached) {
            throw new IllegalStateException("openWXApp fail, WXMsgImpl has been detached");
        } else if (!isWXAppInstalled()) {
            a.a(TAG, "open wx app failed, not installed or signature check failed");
            return false;
        } else {
            try {
                this.context.startActivity(this.context.getPackageManager().getLaunchIntentForPackage("com.tencent.mm"));
                return true;
            } catch (Exception e) {
                a.a(TAG, "startActivity fail, exception = " + e.getMessage());
                return false;
            }
        }
    }

    public final boolean registerApp(String str) {
        if (this.detached) {
            throw new IllegalStateException("registerApp fail, WXMsgImpl has been detached");
        } else if (!WXApiImplComm.validateAppSignatureForPackage(this.context, "com.tencent.mm", this.checkSignature)) {
            a.a(TAG, "register app failed for wechat app signature check failed");
            return false;
        } else {
            if (activityCb == null && Build.VERSION.SDK_INT >= 14) {
                if (this.context instanceof Activity) {
                    initMta(this.context, str);
                    activityCb = new ActivityLifecycleCb(this.context);
                    ((Activity) this.context).getApplication().registerActivityLifecycleCallbacks(activityCb);
                } else if (this.context instanceof Service) {
                    initMta(this.context, str);
                    activityCb = new ActivityLifecycleCb(this.context);
                    ((Service) this.context).getApplication().registerActivityLifecycleCallbacks(activityCb);
                } else {
                    a.b(TAG, "context is not instanceof Activity or Service, disable WXStat");
                }
            }
            a.d(TAG, "registerApp, appId = " + str);
            if (str != null) {
                this.appId = str;
            }
            a.d(TAG, "register app " + this.context.getPackageName());
            com.tencent.mm.sdk.a.a.b bVar = new com.tencent.mm.sdk.a.a.b();
            bVar.f1357a = "com.tencent.mm";
            bVar.f1358b = "com.tencent.mm.plugin.openapi.Intent.ACTION_HANDLE_APP_REGISTER";
            bVar.c = "weixin://registerapp?appid=" + this.appId;
            return com.tencent.mm.sdk.a.a.a.a(this.context, bVar);
        }
    }

    public final boolean sendReq(com.tencent.mm.sdk.d.a aVar) {
        if (this.detached) {
            throw new IllegalStateException("sendReq fail, WXMsgImpl has been detached");
        } else if (!WXApiImplComm.validateAppSignatureForPackage(this.context, "com.tencent.mm", this.checkSignature)) {
            a.a(TAG, "sendReq failed for wechat app signature check failed");
            return false;
        } else if (!aVar.b()) {
            a.a(TAG, "sendReq checkArgs fail");
            return false;
        } else {
            a.d(TAG, "sendReq, req type = " + aVar.a());
            Bundle bundle = new Bundle();
            aVar.a(bundle);
            if (aVar.a() == 5) {
                return sendPayReq(this.context, bundle);
            }
            if (aVar.a() == 7) {
                return sendJumpToBizProfileReq(this.context, bundle);
            }
            if (aVar.a() == 8) {
                return sendJumpToBizWebviewReq(this.context, bundle);
            }
            if (aVar.a() == 9) {
                return sendAddCardToWX(this.context, bundle);
            }
            b bVar = new b();
            bVar.e = bundle;
            bVar.c = "weixin://sendreq?appid=" + this.appId;
            bVar.f1359a = "com.tencent.mm";
            bVar.f1360b = "com.tencent.mm.plugin.base.stub.WXEntryActivity";
            return com.tencent.mm.sdk.a.a.a(this.context, bVar);
        }
    }

    public final boolean sendResp(com.tencent.mm.sdk.d.b bVar) {
        if (this.detached) {
            throw new IllegalStateException("sendResp fail, WXMsgImpl has been detached");
        } else if (!WXApiImplComm.validateAppSignatureForPackage(this.context, "com.tencent.mm", this.checkSignature)) {
            a.a(TAG, "sendResp failed for wechat app signature check failed");
            return false;
        } else if (!bVar.b()) {
            a.a(TAG, "sendResp checkArgs fail");
            return false;
        } else {
            Bundle bundle = new Bundle();
            bVar.a(bundle);
            b bVar2 = new b();
            bVar2.e = bundle;
            bVar2.c = "weixin://sendresp?appid=" + this.appId;
            bVar2.f1359a = "com.tencent.mm";
            bVar2.f1360b = "com.tencent.mm.plugin.base.stub.WXEntryActivity";
            return com.tencent.mm.sdk.a.a.a(this.context, bVar2);
        }
    }

    public final void unregisterApp() {
        if (this.detached) {
            throw new IllegalStateException("unregisterApp fail, WXMsgImpl has been detached");
        } else if (!WXApiImplComm.validateAppSignatureForPackage(this.context, "com.tencent.mm", this.checkSignature)) {
            a.a(TAG, "unregister app failed for wechat app signature check failed");
        } else {
            a.d(TAG, "unregisterApp, appId = " + this.appId);
            if (this.appId == null || this.appId.length() == 0) {
                a.a(TAG, "unregisterApp fail, appId is empty");
                return;
            }
            a.d(TAG, "unregister app " + this.context.getPackageName());
            com.tencent.mm.sdk.a.a.b bVar = new com.tencent.mm.sdk.a.a.b();
            bVar.f1357a = "com.tencent.mm";
            bVar.f1358b = "com.tencent.mm.plugin.openapi.Intent.ACTION_HANDLE_APP_UNREGISTER";
            bVar.c = "weixin://unregisterapp?appid=" + this.appId;
            com.tencent.mm.sdk.a.a.a.a(this.context, bVar);
        }
    }
}
