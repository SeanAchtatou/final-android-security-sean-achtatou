package com.tencent.mm.sdk.b;

import java.util.TimeZone;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private static final long[] f1367a = {300, 200, 300, 200};

    /* renamed from: b  reason: collision with root package name */
    private static final long[] f1368b = {300, 50, 300, 50};
    private static final TimeZone c = TimeZone.getTimeZone("GMT");
    private static final char[] d = {'<', '>', '\"', '\'', '&', 13, 10, ' ', 9};
    private static final String[] e = {"&lt;", "&gt;", "&quot;", "&apos;", "&amp;", "&#x0D;", "&#x0A;", "&#x20;", "&#x09;"};

    public static boolean a(String str) {
        return str == null || str.length() <= 0;
    }
}
