package com.tencent.mm.sdk.f;

import android.os.Bundle;
import com.tencent.mm.sdk.d.b;

public class a extends b {
    public String e;
    public String f;
    public String g;

    public a() {
    }

    public a(Bundle bundle) {
        b(bundle);
    }

    public int a() {
        return 5;
    }

    public void a(Bundle bundle) {
        super.a(bundle);
        bundle.putString("_wxapi_payresp_prepayid", this.e);
        bundle.putString("_wxapi_payresp_returnkey", this.f);
        bundle.putString("_wxapi_payresp_extdata", this.g);
    }

    public void b(Bundle bundle) {
        super.b(bundle);
        this.e = bundle.getString("_wxapi_payresp_prepayid");
        this.f = bundle.getString("_wxapi_payresp_returnkey");
        this.g = bundle.getString("_wxapi_payresp_extdata");
    }

    public boolean b() {
        return true;
    }
}
