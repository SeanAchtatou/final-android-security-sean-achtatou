package com.tencent.mm.sdk.modelmsg;

import android.os.Bundle;
import com.tencent.mm.sdk.d.a;

public class f extends a {
    public String c;
    public String d;

    public int a() {
        return 1;
    }

    public void a(Bundle bundle) {
        super.a(bundle);
        bundle.putString("_wxapi_sendauth_req_scope", this.c);
        bundle.putString("_wxapi_sendauth_req_state", this.d);
    }

    public void b(Bundle bundle) {
        super.b(bundle);
        this.c = bundle.getString("_wxapi_sendauth_req_scope");
        this.d = bundle.getString("_wxapi_sendauth_req_state");
    }

    public boolean b() {
        if (this.c == null || this.c.length() == 0 || this.c.length() > 1024) {
            com.tencent.mm.sdk.b.a.a("MicroMsg.SDK.SendAuth.Req", "checkArgs fail, scope is invalid");
            return false;
        } else if (this.d == null || this.d.length() <= 1024) {
            return true;
        } else {
            com.tencent.mm.sdk.b.a.a("MicroMsg.SDK.SendAuth.Req", "checkArgs fail, state is invalid");
            return false;
        }
    }
}
