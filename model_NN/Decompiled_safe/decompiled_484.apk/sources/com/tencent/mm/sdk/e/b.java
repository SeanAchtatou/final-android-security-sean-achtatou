package com.tencent.mm.sdk.e;

import android.os.Bundle;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.json.JSONTokener;

public class b extends com.tencent.mm.sdk.d.b {
    public List<c> e;

    public b() {
    }

    public b(Bundle bundle) {
        b(bundle);
    }

    public int a() {
        return 9;
    }

    public void a(Bundle bundle) {
        super.a(bundle);
        JSONStringer jSONStringer = new JSONStringer();
        try {
            jSONStringer.object();
            jSONStringer.key("card_list");
            jSONStringer.array();
            for (c next : this.e) {
                jSONStringer.object();
                jSONStringer.key("card_id");
                jSONStringer.value(next.f1374a);
                jSONStringer.key("card_ext");
                jSONStringer.value(next.f1375b == null ? "" : next.f1375b);
                jSONStringer.key("is_succ");
                jSONStringer.value((long) next.c);
                jSONStringer.endObject();
            }
            jSONStringer.endArray();
            jSONStringer.endObject();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        bundle.putString("_wxapi_add_card_to_wx_card_list", jSONStringer.toString());
    }

    public void b(Bundle bundle) {
        super.b(bundle);
        if (this.e == null) {
            this.e = new LinkedList();
        }
        String string = bundle.getString("_wxapi_add_card_to_wx_card_list");
        if (string != null && string.length() > 0) {
            try {
                JSONArray jSONArray = ((JSONObject) new JSONTokener(string).nextValue()).getJSONArray("card_list");
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    c cVar = new c();
                    cVar.f1374a = jSONObject.optString("card_id");
                    cVar.f1375b = jSONObject.optString("card_ext");
                    cVar.c = jSONObject.optInt("is_succ");
                    this.e.add(cVar);
                }
            } catch (Exception e2) {
            }
        }
    }

    public boolean b() {
        return (this.e == null || this.e.size() == 0) ? false : true;
    }
}
