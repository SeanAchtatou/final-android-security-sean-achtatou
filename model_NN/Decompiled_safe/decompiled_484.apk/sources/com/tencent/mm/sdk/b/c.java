package com.tencent.mm.sdk.b;

import android.util.Log;

final class c implements b {
    c() {
    }

    public final int a() {
        return a.f1362b;
    }

    public final void a(String str, String str2) {
        if (a.f1362b <= 2) {
            Log.i(str, str2);
        }
    }

    public final void b(String str, String str2) {
        if (a.f1362b <= 1) {
            Log.d(str, str2);
        }
    }

    public final void c(String str, String str2) {
        if (a.f1362b <= 3) {
            Log.w(str, str2);
        }
    }

    public final void d(String str, String str2) {
        if (a.f1362b <= 4) {
            Log.e(str, str2);
        }
    }
}
