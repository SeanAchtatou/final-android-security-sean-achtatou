package com.tencent.mm.sdk.modelmsg;

import android.os.Bundle;

public interface n {
    boolean checkArgs();

    void serialize(Bundle bundle);

    int type();

    void unserialize(Bundle bundle);
}
