package com.tencent.mm.sdk.openapi;

import android.content.Intent;
import com.tencent.mm.sdk.d.a;
import com.tencent.mm.sdk.d.b;

public interface IWXAPI {
    void detach();

    int getWXAppSupportAPI();

    boolean handleIntent(Intent intent, IWXAPIEventHandler iWXAPIEventHandler);

    boolean isWXAppInstalled();

    boolean isWXAppSupportAPI();

    boolean openWXApp();

    boolean registerApp(String str);

    boolean sendReq(a aVar);

    boolean sendResp(b bVar);

    void unregisterApp();
}
