package com.sina.a;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

public abstract class b extends Binder implements a {
    public static a a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.sina.sso.RemoteSSO");
        return (queryLocalInterface == null || !(queryLocalInterface instanceof a)) ? new c(iBinder) : (a) queryLocalInterface;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 1:
                parcel.enforceInterface("com.sina.sso.RemoteSSO");
                String a2 = a();
                parcel2.writeNoException();
                parcel2.writeString(a2);
                return true;
            case 2:
                parcel.enforceInterface("com.sina.sso.RemoteSSO");
                String b2 = b();
                parcel2.writeNoException();
                parcel2.writeString(b2);
                return true;
            case 3:
                parcel.enforceInterface("com.sina.sso.RemoteSSO");
                String c = c();
                parcel2.writeNoException();
                parcel2.writeString(c);
                return true;
            case 1598968902:
                parcel2.writeString("com.sina.sso.RemoteSSO");
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }
}
