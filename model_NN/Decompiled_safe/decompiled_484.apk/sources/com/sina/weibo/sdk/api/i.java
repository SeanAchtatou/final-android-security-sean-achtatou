package com.sina.weibo.sdk.api;

import android.os.Bundle;
import com.sina.weibo.sdk.d.f;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    public TextObject f1191a;

    /* renamed from: b  reason: collision with root package name */
    public ImageObject f1192b;
    public BaseMediaObject c;

    public Bundle a(Bundle bundle) {
        if (this.f1191a != null) {
            bundle.putParcelable("_weibo_message_text", this.f1191a);
            bundle.putString("_weibo_message_text_extra", this.f1191a.b());
        }
        if (this.f1192b != null) {
            bundle.putParcelable("_weibo_message_image", this.f1192b);
            bundle.putString("_weibo_message_image_extra", this.f1192b.b());
        }
        if (this.c != null) {
            bundle.putParcelable("_weibo_message_media", this.c);
            bundle.putString("_weibo_message_media_extra", this.c.b());
        }
        return bundle;
    }

    public boolean a() {
        if (this.f1191a != null && !this.f1191a.a()) {
            f.c("WeiboMultiMessage", "checkArgs fail, textObject is invalid");
            return false;
        } else if (this.f1192b != null && !this.f1192b.a()) {
            f.c("WeiboMultiMessage", "checkArgs fail, imageObject is invalid");
            return false;
        } else if (this.c != null && !this.c.a()) {
            f.c("WeiboMultiMessage", "checkArgs fail, mediaObject is invalid");
            return false;
        } else if (this.f1191a != null || this.f1192b != null || this.c != null) {
            return true;
        } else {
            f.c("WeiboMultiMessage", "checkArgs fail, textObject and imageObject and mediaObject is null");
            return false;
        }
    }

    public i b(Bundle bundle) {
        this.f1191a = (TextObject) bundle.getParcelable("_weibo_message_text");
        if (this.f1191a != null) {
            this.f1191a.a(bundle.getString("_weibo_message_text_extra"));
        }
        this.f1192b = (ImageObject) bundle.getParcelable("_weibo_message_image");
        if (this.f1192b != null) {
            this.f1192b.a(bundle.getString("_weibo_message_image_extra"));
        }
        this.c = (BaseMediaObject) bundle.getParcelable("_weibo_message_media");
        if (this.c != null) {
            this.c.a(bundle.getString("_weibo_message_media_extra"));
        }
        return this;
    }
}
