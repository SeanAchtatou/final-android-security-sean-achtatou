package com.sina.weibo.sdk.api.a;

import android.content.Context;
import com.sina.weibo.sdk.api.CmdObject;
import com.sina.weibo.sdk.api.VoiceObject;
import com.sina.weibo.sdk.api.h;
import com.sina.weibo.sdk.api.i;
import com.sina.weibo.sdk.d.f;
import com.sina.weibo.sdk.g;

public class k {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1185a = k.class.getName();

    public boolean a(Context context, g gVar, h hVar) {
        if (gVar == null || !gVar.c()) {
            return false;
        }
        f.a(f1185a, "WeiboMessage WeiboInfo package : " + gVar.a());
        f.a(f1185a, "WeiboMessage WeiboInfo supportApi : " + gVar.b());
        if (gVar.b() < 10351 && hVar.f1190a != null && (hVar.f1190a instanceof VoiceObject)) {
            hVar.f1190a = null;
        }
        if (gVar.b() < 10352 && hVar.f1190a != null && (hVar.f1190a instanceof CmdObject)) {
            hVar.f1190a = null;
        }
        return true;
    }

    public boolean a(Context context, g gVar, i iVar) {
        if (gVar == null || !gVar.c()) {
            return false;
        }
        f.a(f1185a, "WeiboMultiMessage WeiboInfo package : " + gVar.a());
        f.a(f1185a, "WeiboMultiMessage WeiboInfo supportApi : " + gVar.b());
        if (gVar.b() < 10351) {
            return false;
        }
        if (gVar.b() < 10352 && iVar.c != null && (iVar.c instanceof CmdObject)) {
            iVar.c = null;
        }
        return true;
    }
}
