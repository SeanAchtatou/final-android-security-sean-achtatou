package com.sina.weibo.sdk.b.a;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.widget.TextView;

public class a extends TextView {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public int f1195a;

    /* renamed from: b  reason: collision with root package name */
    private int f1196b;
    private Paint c;
    private Handler d;
    private Runnable e = new b(this);

    public a(Context context) {
        super(context);
        a(context);
    }

    private void a(Context context) {
        this.d = new Handler();
        this.c = new Paint();
        a();
    }

    private Rect getRect() {
        int left = getLeft();
        int top = getTop();
        return new Rect(0, 0, (getLeft() + (((getRight() - getLeft()) * this.f1195a) / 100)) - left, getBottom() - top);
    }

    public void a() {
        this.f1196b = -11693826;
    }

    public void a(int i) {
        if (i < 7) {
            this.d.postDelayed(this.e, 70);
        } else {
            this.d.removeCallbacks(this.e);
            this.f1195a = i;
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.c.setColor(this.f1196b);
        canvas.drawRect(getRect(), this.c);
    }
}
