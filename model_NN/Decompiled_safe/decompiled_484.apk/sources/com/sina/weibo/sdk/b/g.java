package com.sina.weibo.sdk.b;

import android.text.TextUtils;
import com.qihoo360.daily.activity.SendCmtActivity;
import com.qihoo360.daily.wxapi.WXConfig;
import org.json.JSONException;
import org.json.JSONObject;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private int f1203a = -2;

    /* renamed from: b  reason: collision with root package name */
    private String f1204b;

    private g() {
    }

    public static g a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        g gVar = new g();
        try {
            JSONObject jSONObject = new JSONObject(str);
            gVar.f1203a = jSONObject.optInt(WXConfig.WX_CODE, -2);
            gVar.f1204b = jSONObject.optString(SendCmtActivity.TAG_DATA, "");
            return gVar;
        } catch (JSONException e) {
            return gVar;
        }
    }

    public int a() {
        return this.f1203a;
    }

    public String b() {
        return this.f1204b;
    }
}
