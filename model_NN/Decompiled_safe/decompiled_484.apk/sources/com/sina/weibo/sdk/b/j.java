package com.sina.weibo.sdk.b;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.sina.weibo.sdk.b.a.a;
import com.sina.weibo.sdk.d.f;
import com.sina.weibo.sdk.d.h;
import com.sina.weibo.sdk.d.m;
import com.sina.weibo.sdk.net.i;

public class j extends Activity implements d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f1208a = j.class.getName();

    /* renamed from: b  reason: collision with root package name */
    private String f1209b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public String e;
    /* access modifiers changed from: private */
    public boolean f;
    private TextView g;
    private TextView h;
    private WebView i;
    /* access modifiers changed from: private */
    public a j;
    private LinearLayout k;
    private Button l;
    /* access modifiers changed from: private */
    public e m;
    private o n;

    private e a(Bundle bundle) {
        c cVar = (c) bundle.getSerializable("key_launcher");
        if (cVar == c.AUTH) {
            a aVar = new a(this);
            aVar.c(bundle);
            a(aVar);
            return aVar;
        } else if (cVar == c.SHARE) {
            f fVar = new f(this);
            fVar.c(bundle);
            a(fVar);
            return fVar;
        } else if (cVar != c.WIDGET) {
            return null;
        } else {
            p pVar = new p(this);
            pVar.c(bundle);
            a(pVar);
            return pVar;
        }
    }

    public static void a(Activity activity, String str, String str2) {
        i a2 = i.a(activity.getApplicationContext());
        if (!TextUtils.isEmpty(str)) {
            a2.b(str);
            activity.finish();
        }
        if (!TextUtils.isEmpty(str2)) {
            a2.d(str2);
            activity.finish();
        }
    }

    private void a(a aVar) {
        this.n = new b(this, aVar);
        this.n.a(this);
    }

    private void a(f fVar) {
        h hVar = new h(this, fVar);
        hVar.a(this);
        this.n = hVar;
    }

    private void a(p pVar) {
        r rVar = new r(this, pVar);
        rVar.a(this);
        this.n = rVar;
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        this.i.loadUrl(str);
    }

    private boolean a(Intent intent) {
        this.m = a(intent.getExtras());
        if (this.m == null) {
            return false;
        }
        this.e = this.m.e();
        if (TextUtils.isEmpty(this.e)) {
            return false;
        }
        f.a(f1208a, "LOAD URL : " + this.e);
        this.f1209b = this.m.g();
        return true;
    }

    private boolean a(e eVar) {
        return eVar.f() == c.SHARE;
    }

    private void b(WebView webView, int i2, String str, String str2) {
        if (!str2.startsWith("sinaweibo")) {
            this.f = true;
            j();
        }
    }

    /* access modifiers changed from: private */
    public boolean b(String str) {
        return !TextUtils.isEmpty(str) && "sinaweibo".equalsIgnoreCase(Uri.parse(str).getAuthority());
    }

    private void c() {
        f.a(f1208a, "Enter startShare()............");
        f fVar = (f) this.m;
        if (fVar.a()) {
            f.a(f1208a, "loadUrl hasImage............");
            new com.sina.weibo.sdk.net.a(this).a("http://service.weibo.com/share/mobilesdk_uppic.php", fVar.a(new i(fVar.b())), "POST", new k(this, fVar));
            return;
        }
        a(this.e);
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    private void d() {
        this.i.getSettings().setJavaScriptEnabled(true);
        if (a(this.m)) {
            this.i.getSettings().setUserAgentString(m.b(this));
        }
        this.i.getSettings().setSavePassword(false);
        this.i.setWebViewClient(this.n);
        this.i.setWebChromeClient(new n(this, null));
        this.i.requestFocus();
        this.i.setScrollBarStyle(0);
    }

    private void e() {
        this.h.setText(this.f1209b);
        this.g.setOnClickListener(new l(this));
    }

    /* access modifiers changed from: private */
    public void f() {
        String str = "";
        if (!TextUtils.isEmpty(this.c)) {
            str = this.c;
        } else if (!TextUtils.isEmpty(this.f1209b)) {
            str = this.f1209b;
        }
        this.h.setText(str);
    }

    private void g() {
        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        relativeLayout.setBackgroundColor(-1);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setId(1);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        RelativeLayout relativeLayout2 = new RelativeLayout(this);
        relativeLayout2.setLayoutParams(new ViewGroup.LayoutParams(-1, com.sina.weibo.sdk.d.j.a(this, 45)));
        relativeLayout2.setBackgroundDrawable(com.sina.weibo.sdk.d.j.b(this, "weibosdk_navigationbar_background.9.png"));
        this.g = new TextView(this);
        this.g.setClickable(true);
        this.g.setTextSize(2, 17.0f);
        this.g.setTextColor(com.sina.weibo.sdk.d.j.a(-32256, 1728020992));
        this.g.setText(com.sina.weibo.sdk.d.j.a(this, "Close", "关闭", "关闭"));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(5);
        layoutParams.addRule(15);
        layoutParams.leftMargin = com.sina.weibo.sdk.d.j.a(this, 10);
        layoutParams.rightMargin = com.sina.weibo.sdk.d.j.a(this, 10);
        this.g.setLayoutParams(layoutParams);
        relativeLayout2.addView(this.g);
        this.h = new TextView(this);
        this.h.setTextSize(2, 18.0f);
        this.h.setTextColor(-11382190);
        this.h.setEllipsize(TextUtils.TruncateAt.END);
        this.h.setSingleLine(true);
        this.h.setGravity(17);
        this.h.setMaxWidth(com.sina.weibo.sdk.d.j.a(this, 160));
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(13);
        this.h.setLayoutParams(layoutParams2);
        relativeLayout2.addView(this.h);
        TextView textView = new TextView(this);
        textView.setLayoutParams(new LinearLayout.LayoutParams(-1, com.sina.weibo.sdk.d.j.a(this, 2)));
        textView.setBackgroundDrawable(com.sina.weibo.sdk.d.j.b(this, "weibosdk_common_shadow_top.9.png"));
        this.j = new a(this);
        this.j.setBackgroundColor(0);
        this.j.a(0);
        this.j.setLayoutParams(new LinearLayout.LayoutParams(-1, com.sina.weibo.sdk.d.j.a(this, 3)));
        linearLayout.addView(relativeLayout2);
        linearLayout.addView(textView);
        linearLayout.addView(this.j);
        this.i = new WebView(this);
        this.i.setBackgroundColor(-1);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams3.addRule(3, 1);
        this.i.setLayoutParams(layoutParams3);
        this.k = new LinearLayout(this);
        this.k.setVisibility(8);
        this.k.setOrientation(1);
        this.k.setGravity(17);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams4.addRule(3, 1);
        this.k.setLayoutParams(layoutParams4);
        ImageView imageView = new ImageView(this);
        imageView.setImageDrawable(com.sina.weibo.sdk.d.j.a(this, "weibosdk_empty_failed.png"));
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-2, -2);
        int a2 = com.sina.weibo.sdk.d.j.a(this, 8);
        layoutParams5.bottomMargin = a2;
        layoutParams5.rightMargin = a2;
        layoutParams5.topMargin = a2;
        layoutParams5.leftMargin = a2;
        imageView.setLayoutParams(layoutParams5);
        this.k.addView(imageView);
        TextView textView2 = new TextView(this);
        textView2.setGravity(1);
        textView2.setTextColor(-4342339);
        textView2.setTextSize(2, 14.0f);
        textView2.setText(com.sina.weibo.sdk.d.j.a(this, "A network error occurs, please tap the button to reload", "网络出错啦，请点击按钮重新加载", "網路出錯啦，請點擊按鈕重新載入"));
        textView2.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.k.addView(textView2);
        this.l = new Button(this);
        this.l.setGravity(17);
        this.l.setTextColor(-8882056);
        this.l.setTextSize(2, 16.0f);
        this.l.setText(com.sina.weibo.sdk.d.j.a(this, "channel_data_error", "重新加载", "重新載入"));
        this.l.setBackgroundDrawable(com.sina.weibo.sdk.d.j.a(this, "weibosdk_common_button_alpha.9.png", "weibosdk_common_button_alpha_highlighted.9.png"));
        LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams(com.sina.weibo.sdk.d.j.a(this, 142), com.sina.weibo.sdk.d.j.a(this, 46));
        layoutParams6.topMargin = com.sina.weibo.sdk.d.j.a(this, 10);
        this.l.setLayoutParams(layoutParams6);
        this.l.setOnClickListener(new m(this));
        this.k.addView(this.l);
        relativeLayout.addView(linearLayout);
        relativeLayout.addView(this.i);
        relativeLayout.addView(this.k);
        setContentView(relativeLayout);
        e();
    }

    private void h() {
        f();
        this.j.setVisibility(8);
    }

    private void i() {
        this.h.setText(com.sina.weibo.sdk.d.j.a(this, "Loading....", "加载中....", "載入中...."));
        this.j.setVisibility(0);
    }

    private void j() {
        this.k.setVisibility(0);
        this.i.setVisibility(8);
    }

    private void k() {
        this.k.setVisibility(8);
        this.i.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.d) {
            i();
        } else {
            h();
        }
    }

    public void a(WebView webView, int i2, String str, String str2) {
        f.a(f1208a, "onReceivedError: errorCode = " + i2 + ", description = " + str + ", failingUrl = " + str2);
        b(webView, i2, str, str2);
    }

    public void a(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        f.a(f1208a, "onReceivedSslErrorCallBack.........");
    }

    public void a(WebView webView, String str, Bitmap bitmap) {
        f.a(f1208a, "onPageStarted URL: " + str);
        this.e = str;
        if (!b(str)) {
            this.c = "";
        }
    }

    public boolean a(WebView webView, String str) {
        f.b(f1208a, "shouldOverrideUrlLoading URL: " + str);
        return false;
    }

    public void b(WebView webView, String str) {
        f.a(f1208a, "onPageFinished URL: " + str);
        if (this.f) {
            j();
            return;
        }
        this.f = false;
        k();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!a(getIntent())) {
            finish();
            return;
        }
        g();
        d();
        if (a(this.m)) {
            c();
        } else {
            a(this.e);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        h.d(this);
        super.onDestroy();
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyUp(i2, keyEvent);
        }
        this.m.a(this, 3);
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
