package com.sina.weibo.sdk.net;

import android.text.TextUtils;
import com.qihoo.dynamic.util.Md5Util;
import com.sina.weibo.sdk.d.f;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Set;

public class i {

    /* renamed from: a  reason: collision with root package name */
    private LinkedHashMap<String, Object> f1258a = new LinkedHashMap<>();

    /* renamed from: b  reason: collision with root package name */
    private String f1259b;

    public i(String str) {
        this.f1259b = str;
    }

    public Object a(String str) {
        return this.f1258a.get(str);
    }

    public String a() {
        return this.f1259b;
    }

    public void a(String str, String str2) {
        this.f1258a.put(str, str2);
    }

    public Set<String> b() {
        return this.f1258a.keySet();
    }

    public void b(String str) {
        if (this.f1258a.containsKey(str)) {
            this.f1258a.remove(str);
            this.f1258a.remove(this.f1258a.get(str));
        }
    }

    public String c() {
        boolean z;
        StringBuilder sb = new StringBuilder();
        boolean z2 = true;
        for (String next : this.f1258a.keySet()) {
            if (z2) {
                z = false;
            } else {
                sb.append("&");
                z = z2;
            }
            Object obj = this.f1258a.get(next);
            if (obj instanceof String) {
                String str = (String) obj;
                if (!TextUtils.isEmpty(str)) {
                    try {
                        sb.append(String.valueOf(URLEncoder.encode(next, Md5Util.DEFAULT_CHARSET)) + "=" + URLEncoder.encode(str, Md5Util.DEFAULT_CHARSET));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                f.b("encodeUrl", sb.toString());
            }
            z2 = z;
        }
        return sb.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x0012  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean d() {
        /*
            r3 = this;
            java.util.LinkedHashMap<java.lang.String, java.lang.Object> r0 = r3.f1258a
            java.util.Set r0 = r0.keySet()
            java.util.Iterator r1 = r0.iterator()
        L_0x000a:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x0012
            r0 = 0
        L_0x0011:
            return r0
        L_0x0012:
            java.lang.Object r0 = r1.next()
            java.lang.String r0 = (java.lang.String) r0
            java.util.LinkedHashMap<java.lang.String, java.lang.Object> r2 = r3.f1258a
            java.lang.Object r0 = r2.get(r0)
            boolean r2 = r0 instanceof java.io.ByteArrayOutputStream
            if (r2 != 0) goto L_0x0026
            boolean r0 = r0 instanceof android.graphics.Bitmap
            if (r0 == 0) goto L_0x000a
        L_0x0026:
            r0 = 1
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.net.i.d():boolean");
    }
}
