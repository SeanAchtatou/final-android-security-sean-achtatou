package com.sina.weibo.sdk.b;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import com.sina.weibo.sdk.a.c;
import com.sina.weibo.sdk.d.m;

class r extends o {

    /* renamed from: b  reason: collision with root package name */
    private Activity f1216b;
    private p c;
    private c d;
    private q e;

    public r(Activity activity, p pVar) {
        this.f1216b = activity;
        this.c = pVar;
        this.e = pVar.c();
        this.d = pVar.a();
    }

    public void onPageFinished(WebView webView, String str) {
        if (this.f1215a != null) {
            this.f1215a.b(webView, str);
        }
        super.onPageFinished(webView, str);
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        if (this.f1215a != null) {
            this.f1215a.a(webView, str, bitmap);
        }
        super.onPageStarted(webView, str, bitmap);
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        if (this.f1215a != null) {
            this.f1215a.a(webView, i, str, str2);
        }
        super.onReceivedError(webView, i, str, str2);
    }

    public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        if (this.f1215a != null) {
            this.f1215a.a(webView, sslErrorHandler, sslError);
        }
        super.onReceivedSslError(webView, sslErrorHandler, sslError);
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (this.f1215a != null) {
            this.f1215a.a(webView, str);
        }
        boolean startsWith = str.startsWith("sinaweibo://browser/close");
        if (!str.startsWith("sinaweibo://browser/close") && !str.startsWith("sinaweibo://browser/datatransfer")) {
            return super.shouldOverrideUrlLoading(webView, str);
        }
        Bundle b2 = m.b(str);
        if (!b2.isEmpty() && this.d != null) {
            this.d.a(b2);
        }
        if (this.e != null) {
            this.e.a(str);
        }
        if (startsWith) {
            j.a(this.f1216b, this.c.b(), this.c.h());
        }
        return true;
    }
}
