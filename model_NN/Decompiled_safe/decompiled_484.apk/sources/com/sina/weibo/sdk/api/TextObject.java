package com.sina.weibo.sdk.api;

import android.os.Parcel;
import android.os.Parcelable;
import com.sina.weibo.sdk.d.f;

public class TextObject extends BaseMediaObject {
    public static final Parcelable.Creator<TextObject> CREATOR = new d();
    public String g;

    public TextObject() {
    }

    public TextObject(Parcel parcel) {
        this.g = parcel.readString();
    }

    /* access modifiers changed from: protected */
    public BaseMediaObject a(String str) {
        return this;
    }

    public boolean a() {
        if (this.g != null && this.g.length() != 0 && this.g.length() <= 1024) {
            return true;
        }
        f.c("Weibo.TextObject", "checkArgs fail, text is invalid");
        return false;
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "";
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.g);
    }
}
