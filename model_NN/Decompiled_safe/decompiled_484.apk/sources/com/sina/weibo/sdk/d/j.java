package com.sina.weibo.sdk.d;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.text.TextUtils;
import android.util.StateSet;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

public class j {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1235a = j.class.getName();

    /* renamed from: b  reason: collision with root package name */
    private static final String[] f1236b = {"drawable-xxhdpi", "drawable-xhdpi", "drawable-hdpi", "drawable-mdpi", "drawable-ldpi", "drawable"};

    public static int a(Context context, int i) {
        return (int) (((double) (context.getResources().getDisplayMetrics().density * ((float) i))) + 0.5d);
    }

    public static ColorStateList a(int i, int i2) {
        int[] iArr = {i2, i2, i2, i};
        return new ColorStateList(new int[][]{new int[]{16842919}, new int[]{16842913}, new int[]{16842908}, StateSet.WILD_CARD}, iArr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sina.weibo.sdk.d.j.a(android.content.Context, java.lang.String, boolean):android.graphics.drawable.Drawable
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.sina.weibo.sdk.d.j.a(android.content.Context, java.lang.String, java.lang.String):android.graphics.drawable.StateListDrawable
      com.sina.weibo.sdk.d.j.a(android.content.Context, java.lang.String, boolean):android.graphics.drawable.Drawable */
    public static Drawable a(Context context, String str) {
        return a(context, c(context, str), false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0067 A[SYNTHETIC, Splitter:B:26:0x0067] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.graphics.drawable.Drawable a(android.content.Context r11, java.lang.String r12, boolean r13) {
        /*
            r6 = 0
            android.content.res.AssetManager r0 = r11.getAssets()
            java.io.InputStream r7 = r0.open(r12)     // Catch:{ IOException -> 0x007a, all -> 0x0063 }
            if (r7 == 0) goto L_0x007f
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeStream(r7)     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            android.content.res.Resources r0 = r11.getResources()     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            if (r13 == 0) goto L_0x0043
            android.content.res.Resources r1 = r11.getResources()     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            android.content.res.Configuration r3 = r1.getConfiguration()     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            android.content.res.Resources r1 = new android.content.res.Resources     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            android.content.res.AssetManager r4 = r11.getAssets()     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            r1.<init>(r4, r0, r3)     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            android.graphics.drawable.NinePatchDrawable r0 = new android.graphics.drawable.NinePatchDrawable     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            byte[] r3 = r2.getNinePatchChunk()     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            android.graphics.Rect r4 = new android.graphics.Rect     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            r5 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r4.<init>(r5, r8, r9, r10)     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            r5 = 0
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
        L_0x003d:
            if (r7 == 0) goto L_0x0042
            r7.close()     // Catch:{ IOException -> 0x0070 }
        L_0x0042:
            return r0
        L_0x0043:
            int r0 = r0.densityDpi     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            r2.setDensity(r0)     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            android.graphics.drawable.BitmapDrawable r0 = new android.graphics.drawable.BitmapDrawable     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            android.content.res.Resources r1 = r11.getResources()     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            r0.<init>(r1, r2)     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            goto L_0x003d
        L_0x0052:
            r0 = move-exception
            r1 = r7
        L_0x0054:
            r0.printStackTrace()     // Catch:{ all -> 0x0077 }
            if (r1 == 0) goto L_0x007d
            r1.close()     // Catch:{ IOException -> 0x005e }
        L_0x005c:
            r0 = r6
            goto L_0x0042
        L_0x005e:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x005c
        L_0x0063:
            r0 = move-exception
            r7 = r6
        L_0x0065:
            if (r7 == 0) goto L_0x006a
            r7.close()     // Catch:{ IOException -> 0x006b }
        L_0x006a:
            throw r0
        L_0x006b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006a
        L_0x0070:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0042
        L_0x0075:
            r0 = move-exception
            goto L_0x0065
        L_0x0077:
            r0 = move-exception
            r7 = r1
            goto L_0x0065
        L_0x007a:
            r0 = move-exception
            r1 = r6
            goto L_0x0054
        L_0x007d:
            r0 = r6
            goto L_0x0042
        L_0x007f:
            r0 = r6
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.d.j.a(android.content.Context, java.lang.String, boolean):android.graphics.drawable.Drawable");
    }

    public static StateListDrawable a(Context context, String str, String str2) {
        Drawable b2 = str.indexOf(".9") > -1 ? b(context, str) : a(context, str);
        Drawable b3 = str2.indexOf(".9") > -1 ? b(context, str2) : a(context, str2);
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842919}, b3);
        stateListDrawable.addState(new int[]{16842913}, b3);
        stateListDrawable.addState(new int[]{16842908}, b3);
        stateListDrawable.addState(StateSet.WILD_CARD, b2);
        return stateListDrawable;
    }

    private static String a(Context context) {
        int i = context.getResources().getDisplayMetrics().densityDpi;
        return i <= 120 ? "drawable-ldpi" : (i <= 120 || i > 160) ? (i <= 160 || i > 240) ? (i <= 240 || i > 320) ? "drawable-xxhdpi" : "drawable-xhdpi" : "drawable-hdpi" : "drawable-mdpi";
    }

    public static String a(Context context, String str, String str2, String str3) {
        Locale a2 = a();
        return Locale.SIMPLIFIED_CHINESE.equals(a2) ? str2 : Locale.TRADITIONAL_CHINESE.equals(a2) ? str3 : str;
    }

    public static Locale a() {
        Locale locale = Locale.getDefault();
        return (Locale.SIMPLIFIED_CHINESE.equals(locale) || Locale.TRADITIONAL_CHINESE.equals(locale)) ? locale : Locale.ENGLISH;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sina.weibo.sdk.d.j.a(android.content.Context, java.lang.String, boolean):android.graphics.drawable.Drawable
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.sina.weibo.sdk.d.j.a(android.content.Context, java.lang.String, java.lang.String):android.graphics.drawable.StateListDrawable
      com.sina.weibo.sdk.d.j.a(android.content.Context, java.lang.String, boolean):android.graphics.drawable.Drawable */
    public static Drawable b(Context context, String str) {
        return a(context, c(context, str), true);
    }

    private static String c(Context context, String str) {
        if (TextUtils.isEmpty(str)) {
            f.c(f1235a, "id is NOT correct!");
            return null;
        }
        String a2 = a(context);
        f.a(f1235a, "find Appropriate path...");
        int i = 0;
        int i2 = -1;
        int i3 = -1;
        while (true) {
            if (i >= f1236b.length) {
                i = -1;
                break;
            }
            if (f1236b[i].equals(a2)) {
                i2 = i;
            }
            String str2 = String.valueOf(f1236b[i]) + "/" + str;
            if (d(context, str2)) {
                if (i2 != i) {
                    if (i2 >= 0) {
                        break;
                    }
                    i3 = i;
                } else {
                    return str2;
                }
            }
            i++;
        }
        if (i3 <= 0 || i <= 0) {
            if (i3 <= 0 || i >= 0) {
                if (i3 >= 0 || i <= 0) {
                    f.c(f1235a, "Not find the appropriate path for drawable");
                    i3 = -1;
                } else {
                    i3 = i;
                }
            }
        } else if (Math.abs(i2 - i) <= Math.abs(i2 - i3)) {
            i3 = i;
        }
        if (i3 >= 0) {
            return String.valueOf(f1236b[i3]) + "/" + str;
        }
        f.c(f1235a, "Not find the appropriate path for drawable");
        return null;
    }

    private static boolean d(Context context, String str) {
        if (context == null || TextUtils.isEmpty(str)) {
            return false;
        }
        InputStream inputStream = null;
        try {
            inputStream = context.getAssets().open(str);
            f.a(f1235a, "file [" + str + "] existed");
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return true;
        } catch (IOException e2) {
            f.a(f1235a, "file [" + str + "] NOT existed");
            if (inputStream == null) {
                return false;
            }
            try {
                inputStream.close();
                return false;
            } catch (IOException e3) {
                e3.printStackTrace();
                return false;
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            }
            throw th;
        }
    }
}
