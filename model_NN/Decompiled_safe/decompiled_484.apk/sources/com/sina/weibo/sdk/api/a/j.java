package com.sina.weibo.sdk.api.a;

import android.content.Context;
import android.os.Bundle;
import com.sina.weibo.sdk.api.i;
import com.sina.weibo.sdk.g;

public class j extends b {

    /* renamed from: b  reason: collision with root package name */
    public i f1184b;

    public int a() {
        return 1;
    }

    public void a(Bundle bundle) {
        super.a(bundle);
        bundle.putAll(this.f1184b.a(bundle));
    }

    /* access modifiers changed from: package-private */
    public final boolean a(Context context, g gVar, k kVar) {
        if (this.f1184b == null || gVar == null || !gVar.c()) {
            return false;
        }
        if (kVar == null || kVar.a(context, gVar, this.f1184b)) {
            return this.f1184b.a();
        }
        return false;
    }
}
