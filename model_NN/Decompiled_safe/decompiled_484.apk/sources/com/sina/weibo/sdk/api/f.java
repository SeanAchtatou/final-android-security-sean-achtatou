package com.sina.weibo.sdk.api;

import android.os.Parcel;
import android.os.Parcelable;

class f implements Parcelable.Creator<VoiceObject> {
    f() {
    }

    /* renamed from: a */
    public VoiceObject createFromParcel(Parcel parcel) {
        return new VoiceObject(parcel);
    }

    /* renamed from: a */
    public VoiceObject[] newArray(int i) {
        return new VoiceObject[i];
    }
}
