package com.sina.weibo.sdk.d;

import android.text.TextUtils;

class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f1228a;

    c(a aVar) {
        this.f1228a = aVar;
    }

    public void run() {
        if (this.f1228a.d.tryLock()) {
            if (!TextUtils.isEmpty(this.f1228a.a())) {
                this.f1228a.d.unlock();
                return;
            }
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= 3) {
                    break;
                }
                try {
                    String b2 = this.f1228a.c();
                    d.a(b2);
                    this.f1228a.b(b2);
                    break;
                } catch (com.sina.weibo.sdk.c.c e) {
                    f.c("AidTask", "AidTaskInit WeiboException Msg : " + e.getMessage());
                    i = i2 + 1;
                }
            }
            this.f1228a.d.unlock();
        }
    }
}
