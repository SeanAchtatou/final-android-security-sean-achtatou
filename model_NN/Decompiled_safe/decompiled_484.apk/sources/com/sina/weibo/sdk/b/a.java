package com.sina.weibo.sdk.b;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.sina.weibo.sdk.a.c;

public class a extends e {
    private com.sina.weibo.sdk.a.a e;
    private c f;
    private String g;

    public a(Context context) {
        super(context);
        this.c = c.AUTH;
    }

    public com.sina.weibo.sdk.a.a a() {
        return this.e;
    }

    public void a(Activity activity, int i) {
        if (i == 3) {
            if (this.f != null) {
                this.f.a();
            }
            j.a(activity, this.g, (String) null);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Bundle bundle) {
        Bundle bundle2 = bundle.getBundle("key_authinfo");
        if (bundle2 != null) {
            this.e = com.sina.weibo.sdk.a.a.a(this.f1201a, bundle2);
        }
        this.g = bundle.getString("key_listener");
        if (!TextUtils.isEmpty(this.g)) {
            this.f = i.a(this.f1201a).a(this.g);
        }
    }

    public void a(com.sina.weibo.sdk.a.a aVar) {
        this.e = aVar;
    }

    public void a(c cVar) {
        this.f = cVar;
    }

    public c b() {
        return this.f;
    }

    public void b(Bundle bundle) {
        if (this.e != null) {
            bundle.putBundle("key_authinfo", this.e.f());
        }
        if (this.f != null) {
            i a2 = i.a(this.f1201a);
            this.g = a2.a();
            a2.a(this.g, this.f);
            bundle.putString("key_listener", this.g);
        }
    }

    public String c() {
        return this.g;
    }
}
