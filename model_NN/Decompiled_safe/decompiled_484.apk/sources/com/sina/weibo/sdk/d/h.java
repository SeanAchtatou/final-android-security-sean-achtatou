package com.sina.weibo.sdk.d;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.os.EnvironmentCompat;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import com.qihoo.messenger.util.QHeader;

public class h {
    public static boolean a(Context context) {
        return context == null || context.checkCallingOrSelfPermission("android.permission.INTERNET") == 0;
    }

    public static boolean b(Context context) {
        if (context == null) {
            return false;
        }
        NetworkInfo c = c(context);
        return c != null && 1 == c.getType() && c.isConnected();
    }

    public static NetworkInfo c(Context context) {
        return ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
    }

    public static void d(Context context) {
        CookieSyncManager.createInstance(context);
        CookieManager.getInstance().removeAllCookie();
        CookieSyncManager.getInstance().sync();
    }

    public static String e(Context context) {
        StringBuilder sb = new StringBuilder();
        sb.append(QHeader.os);
        sb.append("__");
        sb.append("weibo");
        sb.append("__");
        sb.append("sdk");
        sb.append("__");
        try {
            sb.append(context.getPackageManager().getPackageInfo(context.getPackageName(), 16).versionName.replaceAll("\\s+", "_"));
        } catch (Exception e) {
            sb.append(EnvironmentCompat.MEDIA_UNKNOWN);
        }
        return sb.toString();
    }
}
