package com.sina.weibo.sdk.net;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import org.apache.http.HttpHost;

public class f {

    /* renamed from: a  reason: collision with root package name */
    public static g f1254a = g.Mobile;

    /* renamed from: b  reason: collision with root package name */
    private static Context f1255b;

    public static HttpHost a() {
        HttpHost httpHost = null;
        Cursor query = f1255b != null ? f1255b.getContentResolver().query(Uri.parse("content://telephony/carriers/preferapn"), null, null, null, null) : null;
        if (query != null && query.moveToFirst()) {
            String string = query.getString(query.getColumnIndex("proxy"));
            if (string != null && string.trim().length() > 0) {
                httpHost = new HttpHost(string, 80);
            }
            query.close();
        }
        return httpHost;
    }
}
