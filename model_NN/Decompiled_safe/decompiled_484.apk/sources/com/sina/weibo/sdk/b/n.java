package com.sina.weibo.sdk.b;

import android.webkit.WebChromeClient;
import android.webkit.WebView;

class n extends WebChromeClient {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ j f1214a;

    private n(j jVar) {
        this.f1214a = jVar;
    }

    /* synthetic */ n(j jVar, n nVar) {
        this(jVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sina.weibo.sdk.b.j.a(com.sina.weibo.sdk.b.j, boolean):void
     arg types: [com.sina.weibo.sdk.b.j, int]
     candidates:
      com.sina.weibo.sdk.b.j.a(com.sina.weibo.sdk.b.j, java.lang.String):boolean
      com.sina.weibo.sdk.b.j.a(android.webkit.WebView, java.lang.String):boolean
      com.sina.weibo.sdk.b.d.a(android.webkit.WebView, java.lang.String):boolean
      com.sina.weibo.sdk.b.j.a(com.sina.weibo.sdk.b.j, boolean):void */
    public void onProgressChanged(WebView webView, int i) {
        this.f1214a.j.a(i);
        if (i == 100) {
            this.f1214a.d = false;
            this.f1214a.a();
        } else if (!this.f1214a.d) {
            this.f1214a.d = true;
            this.f1214a.a();
        }
    }

    public void onReceivedTitle(WebView webView, String str) {
        if (!this.f1214a.b(this.f1214a.e)) {
            this.f1214a.c = str;
            this.f1214a.f();
        }
    }
}
