package com.sina.weibo.sdk.b;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

public abstract class e {

    /* renamed from: a  reason: collision with root package name */
    protected Context f1201a;

    /* renamed from: b  reason: collision with root package name */
    protected String f1202b;
    protected c c;
    protected String d;

    public e(Context context) {
        this.f1201a = context.getApplicationContext();
    }

    public abstract void a(Activity activity, int i);

    /* access modifiers changed from: protected */
    public abstract void a(Bundle bundle);

    public void a(String str) {
        this.f1202b = str;
    }

    /* access modifiers changed from: protected */
    public abstract void b(Bundle bundle);

    public void b(String str) {
        this.d = str;
    }

    public void c(Bundle bundle) {
        this.f1202b = bundle.getString("key_url");
        this.c = (c) bundle.getSerializable("key_launcher");
        this.d = bundle.getString("key_specify_title");
        a(bundle);
    }

    public Bundle d() {
        Bundle bundle = new Bundle();
        if (!TextUtils.isEmpty(this.f1202b)) {
            bundle.putString("key_url", this.f1202b);
        }
        if (this.c != null) {
            bundle.putSerializable("key_launcher", this.c);
        }
        if (!TextUtils.isEmpty(this.d)) {
            bundle.putString("key_specify_title", this.d);
        }
        b(bundle);
        return bundle;
    }

    public String e() {
        return this.f1202b;
    }

    public c f() {
        return this.c;
    }

    public String g() {
        return this.d;
    }
}
