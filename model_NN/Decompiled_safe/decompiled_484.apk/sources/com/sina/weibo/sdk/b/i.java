package com.sina.weibo.sdk.b;

import android.content.Context;
import android.text.TextUtils;
import com.sina.weibo.sdk.a.c;
import java.util.HashMap;
import java.util.Map;

public class i {

    /* renamed from: a  reason: collision with root package name */
    private static i f1206a;

    /* renamed from: b  reason: collision with root package name */
    private Context f1207b;
    private Map<String, c> c = new HashMap();
    private Map<String, q> d = new HashMap();

    private i(Context context) {
        this.f1207b = context;
    }

    public static synchronized i a(Context context) {
        i iVar;
        synchronized (i.class) {
            if (f1206a == null) {
                f1206a = new i(context);
            }
            iVar = f1206a;
        }
        return iVar;
    }

    public synchronized c a(String str) {
        return TextUtils.isEmpty(str) ? null : this.c.get(str);
    }

    public String a() {
        return String.valueOf(System.currentTimeMillis());
    }

    public synchronized void a(String str, c cVar) {
        if (!TextUtils.isEmpty(str) && cVar != null) {
            this.c.put(str, cVar);
        }
    }

    public synchronized void a(String str, q qVar) {
        if (!TextUtils.isEmpty(str) && qVar != null) {
            this.d.put(str, qVar);
        }
    }

    public synchronized void b(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.c.remove(str);
        }
    }

    public synchronized q c(String str) {
        return TextUtils.isEmpty(str) ? null : this.d.get(str);
    }

    public synchronized void d(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.d.remove(str);
        }
    }
}
