package com.sina.weibo.sdk.b;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;

interface d {
    void a(WebView webView, int i, String str, String str2);

    void a(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError);

    void a(WebView webView, String str, Bitmap bitmap);

    boolean a(WebView webView, String str);

    void b(WebView webView, String str);
}
