package com.sina.weibo.sdk.api;

import android.os.Parcel;
import android.os.Parcelable;

class e implements Parcelable.Creator<VideoObject> {
    e() {
    }

    /* renamed from: a */
    public VideoObject createFromParcel(Parcel parcel) {
        return new VideoObject(parcel);
    }

    /* renamed from: a */
    public VideoObject[] newArray(int i) {
        return new VideoObject[i];
    }
}
