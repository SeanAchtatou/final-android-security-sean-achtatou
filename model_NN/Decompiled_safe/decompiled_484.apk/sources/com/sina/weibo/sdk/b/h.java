package com.sina.weibo.sdk.b;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import com.qihoo360.daily.wxapi.WXConfig;
import com.sina.weibo.sdk.a.c;
import com.sina.weibo.sdk.d.m;

class h extends o {

    /* renamed from: b  reason: collision with root package name */
    private Activity f1205b;
    private f c;
    private c d;

    public h(Activity activity, f fVar) {
        this.f1205b = activity;
        this.c = fVar;
        this.d = fVar.c();
    }

    public void onPageFinished(WebView webView, String str) {
        if (this.f1215a != null) {
            this.f1215a.b(webView, str);
        }
        super.onPageFinished(webView, str);
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        if (this.f1215a != null) {
            this.f1215a.a(webView, str, bitmap);
        }
        super.onPageStarted(webView, str, bitmap);
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        if (this.f1215a != null) {
            this.f1215a.a(webView, i, str, str2);
        }
        this.c.a(this.f1205b, str);
        j.a(this.f1205b, this.c.h(), (String) null);
    }

    public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        if (this.f1215a != null) {
            this.f1215a.a(webView, sslErrorHandler, sslError);
        }
        sslErrorHandler.cancel();
        this.c.a(this.f1205b, "ReceivedSslError");
        j.a(this.f1205b, this.c.h(), (String) null);
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (this.f1215a != null) {
            this.f1215a.a(webView, str);
        }
        if (!str.startsWith("sinaweibo://browser/close")) {
            return super.shouldOverrideUrlLoading(webView, str);
        }
        Bundle b2 = m.b(str);
        if (!b2.isEmpty() && this.d != null) {
            this.d.a(b2);
        }
        String string = b2.getString(WXConfig.WX_CODE);
        String string2 = b2.getString("msg");
        if (TextUtils.isEmpty(string)) {
            this.c.a(this.f1205b);
        } else if (!"0".equals(string)) {
            this.c.a(this.f1205b, string2);
        } else {
            this.c.b(this.f1205b);
        }
        j.a(this.f1205b, this.c.h(), (String) null);
        return true;
    }
}
