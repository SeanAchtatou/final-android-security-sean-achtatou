package com.sina.weibo.sdk.d;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private static char[] f1231a = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".toCharArray();

    /* renamed from: b  reason: collision with root package name */
    private static byte[] f1232b = new byte[256];

    static {
        for (int i = 0; i < 256; i++) {
            f1232b[i] = -1;
        }
        for (int i2 = 65; i2 <= 90; i2++) {
            f1232b[i2] = (byte) (i2 - 65);
        }
        for (int i3 = 97; i3 <= 122; i3++) {
            f1232b[i3] = (byte) ((i3 + 26) - 97);
        }
        for (int i4 = 48; i4 <= 57; i4++) {
            f1232b[i4] = (byte) ((i4 + 52) - 48);
        }
        f1232b[43] = 62;
        f1232b[47] = 63;
    }

    public static byte[] a(byte[] bArr) {
        int i = 0;
        int length = ((bArr.length + 3) / 4) * 3;
        if (bArr.length > 0 && bArr[bArr.length - 1] == 61) {
            length--;
        }
        if (bArr.length > 1 && bArr[bArr.length - 2] == 61) {
            length--;
        }
        byte[] bArr2 = new byte[length];
        byte b2 = 0;
        int i2 = 0;
        for (byte b3 : bArr) {
            byte b4 = f1232b[b3 & 255];
            if (b4 >= 0) {
                int i3 = b2 << 6;
                int i4 = i2 + 6;
                byte b5 = i3 | b4;
                if (i4 >= 8) {
                    int i5 = i4 - 8;
                    bArr2[i] = (byte) ((b5 >> i5) & 255);
                    i++;
                    b2 = b5;
                    i2 = i5;
                } else {
                    byte b6 = b5;
                    i2 = i4;
                    b2 = b6;
                }
            }
        }
        if (i == bArr2.length) {
            return bArr2;
        }
        throw new RuntimeException("miscalculated data length!");
    }

    public static byte[] b(byte[] bArr) {
        boolean z;
        boolean z2;
        byte[] bArr2 = new byte[(((bArr.length + 2) / 3) * 4)];
        int i = 0;
        int i2 = 0;
        while (i2 < bArr.length) {
            int i3 = (bArr[i2] & 255) << 8;
            if (i2 + 1 < bArr.length) {
                i3 |= bArr[i2 + 1] & 255;
                z = true;
            } else {
                z = false;
            }
            int i4 = i3 << 8;
            if (i2 + 2 < bArr.length) {
                i4 |= bArr[i2 + 2] & 255;
                z2 = true;
            } else {
                z2 = false;
            }
            bArr2[i + 3] = (byte) f1231a[z2 ? i4 & 63 : 64];
            int i5 = i4 >> 6;
            bArr2[i + 2] = (byte) f1231a[z ? i5 & 63 : 64];
            int i6 = i5 >> 6;
            bArr2[i + 1] = (byte) f1231a[i6 & 63];
            bArr2[i + 0] = (byte) f1231a[(i6 >> 6) & 63];
            i2 += 3;
            i += 4;
        }
        return bArr2;
    }
}
