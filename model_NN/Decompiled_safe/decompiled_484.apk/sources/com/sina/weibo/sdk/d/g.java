package com.sina.weibo.sdk.d;

import com.qihoo.dynamic.util.Md5Util;
import java.security.MessageDigest;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f1234a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public static String a(String str) {
        try {
            return a(str.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String a(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance(Md5Util.ALGORITHM);
            instance.update(bArr);
            byte[] digest = instance.digest();
            char[] cArr = new char[32];
            int i = 0;
            for (int i2 = 0; i2 < 16; i2++) {
                byte b2 = digest[i2];
                int i3 = i + 1;
                cArr[i] = f1234a[(b2 >>> 4) & 15];
                i = i3 + 1;
                cArr[i3] = f1234a[b2 & 15];
            }
            return new String(cArr);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
