package com.sina.weibo.sdk.api.a;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.sina.weibo.sdk.c.e;
import com.sina.weibo.sdk.d.a;
import com.sina.weibo.sdk.d.m;
import com.sina.weibo.sdk.f;
import com.sina.weibo.sdk.g;

class o implements g {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1188a = o.class.getName();

    /* renamed from: b  reason: collision with root package name */
    private Context f1189b;
    private String c;
    private g d = null;
    private boolean e = true;
    private d f;
    private Dialog g = null;

    public o(Context context, String str, boolean z) {
        this.f1189b = context;
        this.c = str;
        this.e = z;
        this.d = f.a(context).a();
        if (this.d != null) {
            com.sina.weibo.sdk.d.f.a(f1188a, this.d.toString());
        } else {
            com.sina.weibo.sdk.d.f.a(f1188a, "WeiboInfo is null");
        }
        a.a(context).a(str);
    }

    private void a(Context context, String str, String str2, String str3, Bundle bundle) {
        Intent intent = new Intent(str);
        String packageName = context.getPackageName();
        intent.putExtra("_weibo_sdkVersion", "0030105000");
        intent.putExtra("_weibo_appPackage", packageName);
        intent.putExtra("_weibo_appKey", str2);
        intent.putExtra("_weibo_flag", 538116905);
        intent.putExtra("_weibo_sign", com.sina.weibo.sdk.d.g.a(m.a(context, packageName)));
        if (!TextUtils.isEmpty(str3)) {
            intent.setPackage(str3);
        }
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        com.sina.weibo.sdk.d.f.a(f1188a, "intent=" + intent + ", extra=" + intent.getExtras());
        context.sendBroadcast(intent, "com.sina.weibo.permission.WEIBO_SDK_PERMISSION");
    }

    private boolean a(Activity activity, String str, String str2, String str3, Bundle bundle) {
        if (activity == null || TextUtils.isEmpty(str) || TextUtils.isEmpty(str2) || TextUtils.isEmpty(str3)) {
            com.sina.weibo.sdk.d.f.c(f1188a, "launchWeiboActivity fail, invalid arguments");
            return false;
        }
        Intent intent = new Intent();
        intent.setPackage(str2);
        intent.setAction(str);
        String packageName = activity.getPackageName();
        intent.putExtra("_weibo_sdkVersion", "0030105000");
        intent.putExtra("_weibo_appPackage", packageName);
        intent.putExtra("_weibo_appKey", str3);
        intent.putExtra("_weibo_flag", 538116905);
        intent.putExtra("_weibo_sign", com.sina.weibo.sdk.d.g.a(m.a(activity, packageName)));
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        try {
            com.sina.weibo.sdk.d.f.a(f1188a, "launchWeiboActivity intent=" + intent + ", extra=" + intent.getExtras());
            activity.startActivityForResult(intent, 765);
            return true;
        } catch (ActivityNotFoundException e2) {
            com.sina.weibo.sdk.d.f.c(f1188a, e2.getMessage());
            return false;
        }
    }

    private boolean a(boolean z) {
        if (!a()) {
            if (z) {
                if (this.g == null) {
                    this.g = l.a(this.f1189b, this.f);
                    this.g.show();
                } else if (!this.g.isShowing()) {
                    this.g.show();
                }
                return false;
            }
            throw new e("Weibo is not installed!");
        } else if (!d()) {
            throw new e("Weibo do not support share api!");
        } else if (com.sina.weibo.sdk.a.a(this.f1189b, this.d.a())) {
            return true;
        } else {
            throw new e("Weibo signature is incorrect!");
        }
    }

    public boolean a() {
        return this.d != null && this.d.c();
    }

    public boolean a(Activity activity, b bVar) {
        if (bVar == null) {
            com.sina.weibo.sdk.d.f.c(f1188a, "sendRequest faild request is null");
            return false;
        }
        try {
            if (!a(this.e)) {
                return false;
            }
            if (!bVar.a(this.f1189b, this.d, new k())) {
                com.sina.weibo.sdk.d.f.c(f1188a, "sendRequest faild request check faild");
                return false;
            }
            Bundle bundle = new Bundle();
            bVar.a(bundle);
            return a(activity, "com.sina.weibo.sdk.action.ACTION_WEIBO_ACTIVITY", this.d.a(), this.c, bundle);
        } catch (Exception e2) {
            com.sina.weibo.sdk.d.f.c(f1188a, e2.getMessage());
            return false;
        }
    }

    public boolean a(Intent intent, f fVar) {
        String stringExtra = intent.getStringExtra("_weibo_appPackage");
        String stringExtra2 = intent.getStringExtra("_weibo_transaction");
        if (TextUtils.isEmpty(stringExtra)) {
            com.sina.weibo.sdk.d.f.c(f1188a, "handleWeiboResponse faild appPackage is null");
            return false;
        } else if (!(fVar instanceof Activity)) {
            com.sina.weibo.sdk.d.f.c(f1188a, "handleWeiboResponse faild handler is not Activity");
            return false;
        } else {
            Activity activity = (Activity) fVar;
            com.sina.weibo.sdk.d.f.a(f1188a, "handleWeiboResponse getCallingPackage : " + activity.getCallingPackage());
            if (TextUtils.isEmpty(stringExtra2)) {
                com.sina.weibo.sdk.d.f.c(f1188a, "handleWeiboResponse faild intent _weibo_transaction is null");
                return false;
            } else if (com.sina.weibo.sdk.a.a(this.f1189b, stringExtra) || stringExtra.equals(activity.getPackageName())) {
                fVar.onResponse(new i(intent.getExtras()));
                return true;
            } else {
                com.sina.weibo.sdk.d.f.c(f1188a, "handleWeiboResponse faild appPackage validateSign faild");
                return false;
            }
        }
    }

    public int b() {
        if (this.d == null || !this.d.c()) {
            return -1;
        }
        return this.d.b();
    }

    public boolean c() {
        a(this.f1189b, "com.sina.weibo.sdk.Intent.ACTION_WEIBO_REGISTER", this.c, (String) null, (Bundle) null);
        return true;
    }

    public boolean d() {
        return b() >= 10350;
    }
}
