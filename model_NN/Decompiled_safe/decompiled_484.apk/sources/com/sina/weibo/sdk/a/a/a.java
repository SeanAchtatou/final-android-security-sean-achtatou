package com.sina.weibo.sdk.a.a;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.text.TextUtils;
import com.qihoo.video.httpservice.NetWorkError;
import com.sina.weibo.sdk.a.b;
import com.sina.weibo.sdk.a.c;
import com.sina.weibo.sdk.d.k;
import com.sina.weibo.sdk.d.m;
import com.sina.weibo.sdk.f;
import com.sina.weibo.sdk.g;

public class a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public d f1170a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public c f1171b;
    /* access modifiers changed from: private */
    public Activity c;
    private int d;
    private g e;
    private com.sina.weibo.sdk.a.a f;
    /* access modifiers changed from: private */
    public ServiceConnection g = new b(this);

    public a(Activity activity, com.sina.weibo.sdk.a.a aVar) {
        this.c = activity;
        this.f = aVar;
        this.f1170a = new d(activity, aVar);
        this.e = f.a(activity).a();
        com.sina.weibo.sdk.d.a.a(this.c).a(aVar.a());
    }

    private void a(int i, c cVar, c cVar2) {
        this.d = i;
        this.f1171b = cVar;
        boolean z = false;
        if (cVar2 == c.SsoOnly) {
            z = true;
        }
        if (cVar2 == c.WebOnly) {
            if (cVar != null) {
                this.f1170a.a(cVar);
            }
        } else if (a(this.c.getApplicationContext())) {
        } else {
            if (!z) {
                this.f1170a.a(this.f1171b);
            } else if (this.f1171b != null) {
                this.f1171b.a(new com.sina.weibo.sdk.c.c("not install weibo client!!!!!"));
            }
        }
    }

    private boolean a(Context context) {
        if (!a()) {
            return false;
        }
        String a2 = this.e.a();
        Intent intent = new Intent("com.sina.weibo.remotessoservice");
        intent.setPackage(a2);
        return context.bindService(intent, this.g, 1);
    }

    /* access modifiers changed from: private */
    public boolean a(String str, String str2) {
        boolean z = true;
        Intent intent = new Intent();
        intent.setClassName(str, str2);
        intent.putExtras(this.f1170a.a().f());
        intent.putExtra("_weibo_command_type", 3);
        intent.putExtra("_weibo_transaction", String.valueOf(System.currentTimeMillis()));
        intent.putExtra("aid", m.b(this.c, this.f.a()));
        if (!k.a(this.c, intent)) {
            return false;
        }
        String b2 = m.b(this.c, this.f.a());
        if (!TextUtils.isEmpty(b2)) {
            intent.putExtra("aid", b2);
        }
        try {
            this.c.startActivityForResult(intent, this.d);
        } catch (ActivityNotFoundException e2) {
            z = false;
        }
        return z;
    }

    public void a(int i, int i2, Intent intent) {
        com.sina.weibo.sdk.d.f.a("Weibo_SSO_login", "requestCode: " + i + ", resultCode: " + i2 + ", data: " + intent);
        if (i != this.d) {
            return;
        }
        if (i2 == -1) {
            if (k.a(this.c, this.e, intent)) {
                String stringExtra = intent.getStringExtra(NetWorkError.ErrorCode);
                if (stringExtra == null) {
                    stringExtra = intent.getStringExtra("error_type");
                }
                if (stringExtra == null) {
                    Bundle extras = intent.getExtras();
                    b a2 = b.a(extras);
                    if (a2 == null || !a2.a()) {
                        com.sina.weibo.sdk.d.f.a("Weibo_SSO_login", "Failed to receive access token by SSO");
                        this.f1170a.a(this.f1171b);
                        return;
                    }
                    com.sina.weibo.sdk.d.f.a("Weibo_SSO_login", "Login Success! " + a2.toString());
                    this.f1171b.a(extras);
                } else if (stringExtra.equals("access_denied") || stringExtra.equals("OAuthAccessDeniedException")) {
                    com.sina.weibo.sdk.d.f.a("Weibo_SSO_login", "Login canceled by user.");
                    this.f1171b.a();
                } else {
                    String stringExtra2 = intent.getStringExtra("error_description");
                    if (stringExtra2 != null) {
                        stringExtra = String.valueOf(stringExtra) + ":" + stringExtra2;
                    }
                    com.sina.weibo.sdk.d.f.a("Weibo_SSO_login", "Login failed: " + stringExtra);
                    this.f1171b.a(new com.sina.weibo.sdk.c.b(stringExtra, i2, stringExtra2));
                }
            }
        } else if (i2 != 0) {
        } else {
            if (intent != null) {
                com.sina.weibo.sdk.d.f.a("Weibo_SSO_login", "Login failed: " + intent.getStringExtra(NetWorkError.ErrorCode));
                this.f1171b.a(new com.sina.weibo.sdk.c.b(intent.getStringExtra(NetWorkError.ErrorCode), intent.getIntExtra("error_code", -1), intent.getStringExtra("failing_url")));
                return;
            }
            com.sina.weibo.sdk.d.f.a("Weibo_SSO_login", "Login canceled by user.");
            this.f1171b.a();
        }
    }

    public void a(c cVar) {
        a(32973, cVar, c.SsoOnly);
    }

    public boolean a() {
        return this.e != null && this.e.c();
    }
}
