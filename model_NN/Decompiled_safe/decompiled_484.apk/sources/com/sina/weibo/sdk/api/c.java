package com.sina.weibo.sdk.api;

import android.os.Parcel;
import android.os.Parcelable;

class c implements Parcelable.Creator<MusicObject> {
    c() {
    }

    /* renamed from: a */
    public MusicObject createFromParcel(Parcel parcel) {
        return new MusicObject(parcel);
    }

    /* renamed from: a */
    public MusicObject[] newArray(int i) {
        return new MusicObject[i];
    }
}
