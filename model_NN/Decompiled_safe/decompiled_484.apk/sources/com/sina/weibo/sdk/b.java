package com.sina.weibo.sdk;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Pair;
import com.sina.weibo.sdk.d.g;
import com.sina.weibo.sdk.d.m;
import com.sina.weibo.sdk.net.DownloadService;
import com.sina.weibo.sdk.net.a;
import com.sina.weibo.sdk.net.h;
import com.sina.weibo.sdk.net.i;
import java.io.File;
import java.util.concurrent.CountDownLatch;

public class b {

    /* renamed from: a  reason: collision with root package name */
    public static final String f1193a = (Environment.getExternalStorageDirectory() + "/Android/org_share_data/");
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final String f1194b = b.class.getName();
    private static b d;
    /* access modifiers changed from: private */
    public Context c;
    /* access modifiers changed from: private */
    public CountDownLatch e;
    /* access modifiers changed from: private */
    public e f;
    private String g;
    /* access modifiers changed from: private */
    public boolean h = true;

    private b(Context context, String str) {
        this.c = context.getApplicationContext();
        this.g = str;
    }

    public static synchronized b a(Context context, String str) {
        b bVar;
        synchronized (b.class) {
            if (d == null) {
                d = new b(context, str);
            }
            bVar = d;
        }
        return bVar;
    }

    private static void a(Context context, String str, h hVar) {
        String packageName = context.getPackageName();
        String a2 = m.a(context, packageName);
        i iVar = new i(str);
        iVar.a("appkey", str);
        iVar.a("packagename", packageName);
        iVar.a("key_hash", a2);
        new a(context).a("http://api.weibo.cn/2/client/common_config", iVar, "GET", hVar);
    }

    private static boolean a(PackageInfo packageInfo) {
        return b(packageInfo) && c(packageInfo);
    }

    private static boolean b(PackageInfo packageInfo) {
        if (packageInfo == null) {
            return false;
        }
        String str = packageInfo.packageName;
        return "com.sina.weibo".equals(str) || "com.sina.weibog3".equals(str);
    }

    /* access modifiers changed from: private */
    public static Pair<Integer, File> c(Context context, String str) {
        File[] listFiles;
        int i;
        File file = null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        File file2 = new File(str);
        if (!file2.exists() || !file2.isDirectory() || (listFiles = file2.listFiles()) == null) {
            return null;
        }
        int length = listFiles.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            File file3 = listFiles[i2];
            String name = file3.getName();
            if (file3.isFile() && name.endsWith(".apk")) {
                PackageInfo packageArchiveInfo = context.getPackageManager().getPackageArchiveInfo(file3.getAbsolutePath(), 64);
                if (!a(packageArchiveInfo)) {
                    i = i3;
                } else if (packageArchiveInfo.versionCode > i3) {
                    File file4 = file3;
                    i = packageArchiveInfo.versionCode;
                    file = file4;
                }
                i2++;
                i3 = i;
            }
            i = i3;
            i2++;
            i3 = i;
        }
        return new Pair<>(Integer.valueOf(i3), file);
    }

    private void c() {
        a(this.c, this.g, new d(this));
    }

    /* access modifiers changed from: private */
    public static void c(Context context, String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            com.sina.weibo.sdk.d.i.a(context, str, str2);
        }
    }

    private static boolean c(PackageInfo packageInfo) {
        if (packageInfo == null) {
            return false;
        }
        if (packageInfo.signatures == null) {
            return Build.VERSION.SDK_INT < 11;
        }
        String str = "";
        for (Signature byteArray : packageInfo.signatures) {
            byte[] byteArray2 = byteArray.toByteArray();
            if (byteArray2 != null) {
                str = g.a(byteArray2);
            }
        }
        return "18da2bf10352443a00a5e046d9fca6bd".equals(str);
    }

    /* access modifiers changed from: private */
    public static void d(Context context, String str, String str2) {
        Intent intent = new Intent(context, DownloadService.class);
        Bundle bundle = new Bundle();
        bundle.putString("notification_content", str);
        bundle.putString("download_url", str2);
        intent.putExtras(bundle);
        context.startService(intent);
    }

    public void a() {
        g a2 = f.a(this.c).a();
        if ((a2 == null || !a2.c()) && this.h) {
            this.h = false;
            this.e = new CountDownLatch(1);
            c();
            new Thread(new c(this, f1193a)).start();
        }
    }
}
