package com.sina.weibo.sdk.a.a;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import com.sina.a.a;

class b implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f1172a;

    b(a aVar) {
        this.f1172a = aVar;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        a a2 = com.sina.a.b.a(iBinder);
        try {
            String a3 = a2.a();
            String b2 = a2.b();
            this.f1172a.c.getApplicationContext().unbindService(this.f1172a.g);
            if (!this.f1172a.a(a3, b2)) {
                this.f1172a.f1170a.a(this.f1172a.f1171b);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        this.f1172a.f1170a.a(this.f1172a.f1171b);
    }
}
