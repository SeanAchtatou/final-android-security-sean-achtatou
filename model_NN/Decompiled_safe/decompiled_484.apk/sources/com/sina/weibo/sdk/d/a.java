package com.sina.weibo.sdk.d;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.Settings;
import android.support.v4.os.EnvironmentCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.mediav.ads.sdk.adcore.Config;
import com.qihoo.dynamic.util.Md5Util;
import com.sina.weibo.sdk.c.c;
import com.sina.weibo.sdk.net.i;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.concurrent.locks.ReentrantLock;
import org.json.JSONException;
import org.json.JSONObject;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static a f1225a;

    /* renamed from: b  reason: collision with root package name */
    private Context f1226b;
    private String c;
    /* access modifiers changed from: private */
    public volatile ReentrantLock d = new ReentrantLock(true);

    private a(Context context) {
        this.f1226b = context.getApplicationContext();
        new Thread(new b(this)).start();
    }

    private int a(byte[] bArr, int i, int i2) {
        if (i >= bArr.length) {
            return -1;
        }
        return Math.min(bArr.length - i, i2);
    }

    public static synchronized a a(Context context) {
        a aVar;
        synchronized (a.class) {
            if (f1225a == null) {
                f1225a = new a(context);
            }
            aVar = f1225a;
        }
        return aVar;
    }

    /* access modifiers changed from: private */
    public File a(int i) {
        return new File(this.f1226b.getFilesDir(), "weibo_sdk_aid" + i);
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x00c2 A[SYNTHETIC, Splitter:B:19:0x00c2] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(java.lang.String r10, java.lang.String r11) {
        /*
            r9 = this;
            java.lang.String r0 = "RSA/ECB/PKCS1Padding"
            javax.crypto.Cipher r3 = javax.crypto.Cipher.getInstance(r0)
            java.security.PublicKey r0 = r9.c(r11)
            r1 = 1
            r3.init(r1, r0)
            r2 = 0
            java.lang.String r0 = "UTF-8"
            byte[] r4 = r10.getBytes(r0)
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x00be }
            r1.<init>()     // Catch:{ all -> 0x00be }
            r0 = 0
        L_0x001b:
            r2 = 117(0x75, float:1.64E-43)
            int r2 = r9.a(r4, r0, r2)     // Catch:{ all -> 0x00ca }
            r5 = -1
            if (r2 != r5) goto L_0x008b
            r1.flush()     // Catch:{ all -> 0x00ca }
            byte[] r0 = r1.toByteArray()     // Catch:{ all -> 0x00ca }
            java.lang.String r2 = "AidTask"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ca }
            java.lang.String r4 = "encryptRsa total enBytes len = "
            r3.<init>(r4)     // Catch:{ all -> 0x00ca }
            int r4 = r0.length     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00ca }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00ca }
            com.sina.weibo.sdk.d.f.a(r2, r3)     // Catch:{ all -> 0x00ca }
            byte[] r0 = com.sina.weibo.sdk.d.e.b(r0)     // Catch:{ all -> 0x00ca }
            java.lang.String r2 = "AidTask"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ca }
            java.lang.String r4 = "encryptRsa total base64byte len = "
            r3.<init>(r4)     // Catch:{ all -> 0x00ca }
            int r4 = r0.length     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00ca }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00ca }
            com.sina.weibo.sdk.d.f.a(r2, r3)     // Catch:{ all -> 0x00ca }
            java.lang.String r2 = "01"
            java.lang.String r2 = new java.lang.String     // Catch:{ all -> 0x00ca }
            java.lang.String r3 = "UTF-8"
            r2.<init>(r0, r3)     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ca }
            java.lang.String r3 = "01"
            r0.<init>(r3)     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ all -> 0x00ca }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00ca }
            java.lang.String r2 = "AidTask"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ca }
            java.lang.String r4 = "encryptRsa total base64string : "
            r3.<init>(r4)     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ all -> 0x00ca }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00ca }
            com.sina.weibo.sdk.d.f.a(r2, r3)     // Catch:{ all -> 0x00ca }
            if (r1 == 0) goto L_0x008a
            r1.close()     // Catch:{ IOException -> 0x00c6 }
        L_0x008a:
            return r0
        L_0x008b:
            byte[] r5 = r3.doFinal(r4, r0, r2)     // Catch:{ all -> 0x00ca }
            r1.write(r5)     // Catch:{ all -> 0x00ca }
            java.lang.String r6 = "AidTask"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ca }
            java.lang.String r8 = "encryptRsa offset = "
            r7.<init>(r8)     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ all -> 0x00ca }
            java.lang.String r8 = "     len = "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r7 = r7.append(r2)     // Catch:{ all -> 0x00ca }
            java.lang.String r8 = "     enBytes len = "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x00ca }
            int r5 = r5.length     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r5 = r7.append(r5)     // Catch:{ all -> 0x00ca }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x00ca }
            com.sina.weibo.sdk.d.f.a(r6, r5)     // Catch:{ all -> 0x00ca }
            int r0 = r0 + r2
            goto L_0x001b
        L_0x00be:
            r0 = move-exception
            r1 = r2
        L_0x00c0:
            if (r1 == 0) goto L_0x00c5
            r1.close()     // Catch:{ IOException -> 0x00c8 }
        L_0x00c5:
            throw r0
        L_0x00c6:
            r1 = move-exception
            goto L_0x008a
        L_0x00c8:
            r1 = move-exception
            goto L_0x00c5
        L_0x00ca:
            r0 = move-exception
            goto L_0x00c0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.d.a.a(java.lang.String, java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0035 A[SYNTHETIC, Splitter:B:21:0x0035] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized com.sina.weibo.sdk.d.d b() {
        /*
            r5 = this;
            r0 = 0
            monitor-enter(r5)
            r1 = 1
            java.io.File r2 = r5.a(r1)     // Catch:{ Exception -> 0x0025, all -> 0x002f }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0025, all -> 0x002f }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0025, all -> 0x002f }
            int r2 = r1.available()     // Catch:{ Exception -> 0x0042, all -> 0x0040 }
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x0042, all -> 0x0040 }
            r1.read(r2)     // Catch:{ Exception -> 0x0042, all -> 0x0040 }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x0042, all -> 0x0040 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x0042, all -> 0x0040 }
            com.sina.weibo.sdk.d.d r0 = com.sina.weibo.sdk.d.d.a(r3)     // Catch:{ Exception -> 0x0042, all -> 0x0040 }
            if (r1 == 0) goto L_0x0023
            r1.close()     // Catch:{ IOException -> 0x003c }
        L_0x0023:
            monitor-exit(r5)
            return r0
        L_0x0025:
            r1 = move-exception
            r1 = r0
        L_0x0027:
            if (r1 == 0) goto L_0x0023
            r1.close()     // Catch:{ IOException -> 0x002d }
            goto L_0x0023
        L_0x002d:
            r1 = move-exception
            goto L_0x0023
        L_0x002f:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0033:
            if (r1 == 0) goto L_0x0038
            r1.close()     // Catch:{ IOException -> 0x003e }
        L_0x0038:
            throw r0     // Catch:{ all -> 0x0039 }
        L_0x0039:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x003c:
            r1 = move-exception
            goto L_0x0023
        L_0x003e:
            r1 = move-exception
            goto L_0x0038
        L_0x0040:
            r0 = move-exception
            goto L_0x0033
        L_0x0042:
            r2 = move-exception
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.d.a.b():com.sina.weibo.sdk.d.d");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0027 A[SYNTHETIC, Splitter:B:18:0x0027] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0030 A[SYNTHETIC, Splitter:B:23:0x0030] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void b(java.lang.String r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            boolean r0 = android.text.TextUtils.isEmpty(r5)     // Catch:{ all -> 0x0034 }
            if (r0 == 0) goto L_0x0009
        L_0x0007:
            monitor-exit(r4)
            return
        L_0x0009:
            r1 = 0
            r0 = 1
            java.io.File r2 = r4.a(r0)     // Catch:{ Exception -> 0x0023, all -> 0x002d }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0023, all -> 0x002d }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0023, all -> 0x002d }
            byte[] r1 = r5.getBytes()     // Catch:{ Exception -> 0x003e, all -> 0x0039 }
            r0.write(r1)     // Catch:{ Exception -> 0x003e, all -> 0x0039 }
            if (r0 == 0) goto L_0x0007
            r0.close()     // Catch:{ IOException -> 0x0021 }
            goto L_0x0007
        L_0x0021:
            r0 = move-exception
            goto L_0x0007
        L_0x0023:
            r0 = move-exception
            r0 = r1
        L_0x0025:
            if (r0 == 0) goto L_0x0007
            r0.close()     // Catch:{ IOException -> 0x002b }
            goto L_0x0007
        L_0x002b:
            r0 = move-exception
            goto L_0x0007
        L_0x002d:
            r0 = move-exception
        L_0x002e:
            if (r1 == 0) goto L_0x0033
            r1.close()     // Catch:{ IOException -> 0x0037 }
        L_0x0033:
            throw r0     // Catch:{ all -> 0x0034 }
        L_0x0034:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0037:
            r1 = move-exception
            goto L_0x0033
        L_0x0039:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x002e
        L_0x003e:
            r1 = move-exception
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.d.a.b(java.lang.String):void");
    }

    /* access modifiers changed from: private */
    public String c() {
        String packageName = this.f1226b.getPackageName();
        String a2 = m.a(this.f1226b, packageName);
        String d2 = d();
        i iVar = new i(this.c);
        iVar.a("appkey", this.c);
        iVar.a("mfp", d2);
        iVar.a("packagename", packageName);
        iVar.a("key_hash", a2);
        try {
            String a3 = new com.sina.weibo.sdk.net.a(this.f1226b).a("http://api.weibo.com/oauth2/getaid.json", iVar, "GET");
            f.a("AidTask", "loadAidFromNet response : " + a3);
            return a3;
        } catch (c e) {
            f.a("AidTask", "loadAidFromNet WeiboException Msg : " + e.getMessage());
            throw e;
        }
    }

    private PublicKey c(String str) {
        return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(e.a(str.getBytes())));
    }

    private String d() {
        String str;
        try {
            str = new String(e().getBytes(), Md5Util.DEFAULT_CHARSET);
        } catch (UnsupportedEncodingException e) {
            str = "";
        }
        f.a("AidTask", "genMfpString() utf-8 string : " + str);
        try {
            String a2 = a(str, "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDHHM0Fi2Z6+QYKXqFUX2Cy6AaWq3cPi+GSn9oeAwQbPZR75JB7Netm0HtBVVbtPhzT7UO2p1JhFUKWqrqoYuAjkgMVPmA0sFrQohns5EE44Y86XQopD4ZO+dE5KjUZFE6vrPO3rWW3np2BqlgKpjnYZri6TJApmIpGcQg9/G/3zQIDAQAB");
            f.a("AidTask", "encryptRsa() string : " + a2);
            return a2;
        } catch (Exception e2) {
            f.c("AidTask", e2.getMessage());
            return "";
        }
    }

    private String e() {
        JSONObject jSONObject = new JSONObject();
        try {
            String f = f();
            if (!TextUtils.isEmpty(f)) {
                jSONObject.put(Config.CHANNEL_ID, f);
            }
            String g = g();
            if (!TextUtils.isEmpty(g)) {
                jSONObject.put("2", g);
            }
            String h = h();
            if (!TextUtils.isEmpty(h)) {
                jSONObject.put("3", h);
            }
            String i = i();
            if (!TextUtils.isEmpty(i)) {
                jSONObject.put("4", i);
            }
            String j = j();
            if (!TextUtils.isEmpty(j)) {
                jSONObject.put("5", j);
            }
            String k = k();
            if (!TextUtils.isEmpty(k)) {
                jSONObject.put("6", k);
            }
            String l = l();
            if (!TextUtils.isEmpty(l)) {
                jSONObject.put("7", l);
            }
            String m = m();
            if (!TextUtils.isEmpty(m)) {
                jSONObject.put("10", m);
            }
            String n = n();
            if (!TextUtils.isEmpty(n)) {
                jSONObject.put("13", n);
            }
            String o = o();
            if (!TextUtils.isEmpty(o)) {
                jSONObject.put("14", o);
            }
            String p = p();
            if (!TextUtils.isEmpty(p)) {
                jSONObject.put("15", p);
            }
            String q = q();
            if (!TextUtils.isEmpty(q)) {
                jSONObject.put("16", q);
            }
            String r = r();
            if (!TextUtils.isEmpty(r)) {
                jSONObject.put("17", r);
            }
            String s = s();
            if (!TextUtils.isEmpty(s)) {
                jSONObject.put("18", s);
            }
            String t = t();
            if (!TextUtils.isEmpty(t)) {
                jSONObject.put("19", t);
            }
            return jSONObject.toString();
        } catch (JSONException e) {
            return "";
        }
    }

    private String f() {
        try {
            return "Android " + Build.VERSION.RELEASE;
        } catch (Exception e) {
            return "";
        }
    }

    private String g() {
        try {
            return ((TelephonyManager) this.f1226b.getSystemService("phone")).getDeviceId();
        } catch (Exception e) {
            return "";
        }
    }

    private String h() {
        try {
            return ((TelephonyManager) this.f1226b.getSystemService("phone")).getDeviceId();
        } catch (Exception e) {
            return "";
        }
    }

    private String i() {
        try {
            return ((TelephonyManager) this.f1226b.getSystemService("phone")).getSubscriberId();
        } catch (Exception e) {
            return "";
        }
    }

    private String j() {
        try {
            WifiManager wifiManager = (WifiManager) this.f1226b.getSystemService("wifi");
            if (wifiManager == null) {
                return "";
            }
            WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            return connectionInfo != null ? connectionInfo.getMacAddress() : "";
        } catch (Exception e) {
            return "";
        }
    }

    private String k() {
        try {
            return ((TelephonyManager) this.f1226b.getSystemService("phone")).getSimSerialNumber();
        } catch (Exception e) {
            return "";
        }
    }

    private String l() {
        try {
            Class<?> cls = Class.forName("android.os.SystemProperties");
            return (String) cls.getMethod("get", String.class, String.class).invoke(cls, "ro.serialno", EnvironmentCompat.MEDIA_UNKNOWN);
        } catch (Exception e) {
            return "";
        }
    }

    private String m() {
        try {
            return Settings.Secure.getString(this.f1226b.getContentResolver(), "android_id");
        } catch (Exception e) {
            return "";
        }
    }

    private String n() {
        try {
            return Build.CPU_ABI;
        } catch (Exception e) {
            return "";
        }
    }

    private String o() {
        try {
            return Build.MODEL;
        } catch (Exception e) {
            return "";
        }
    }

    private String p() {
        try {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            return Long.toString(((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize()));
        } catch (Exception e) {
            return "";
        }
    }

    private String q() {
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) this.f1226b.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            return String.valueOf(String.valueOf(displayMetrics.widthPixels)) + "*" + String.valueOf(displayMetrics.heightPixels);
        } catch (Exception e) {
            return "";
        }
    }

    private String r() {
        try {
            WifiInfo connectionInfo = ((WifiManager) this.f1226b.getSystemService("wifi")).getConnectionInfo();
            if (connectionInfo != null) {
                return connectionInfo.getSSID();
            }
        } catch (Exception e) {
        }
        return "";
    }

    private String s() {
        try {
            return Build.BRAND;
        } catch (Exception e) {
            return "";
        }
    }

    private String t() {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.f1226b.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                if (activeNetworkInfo.getType() == 0) {
                    switch (activeNetworkInfo.getSubtype()) {
                        case 1:
                        case 2:
                        case 4:
                        case 7:
                        case 11:
                            return "2G";
                        case 3:
                        case 5:
                        case 6:
                        case 8:
                        case 9:
                        case 10:
                        case 12:
                        case 14:
                        case 15:
                            return "3G";
                        case 13:
                            return "4G";
                        default:
                            return "none";
                    }
                } else if (activeNetworkInfo.getType() == 1) {
                    return "wifi";
                }
            }
            return "none";
        } catch (Exception e) {
            return "none";
        }
    }

    public synchronized String a() {
        d b2;
        b2 = b();
        return b2 != null ? b2.a() : "";
    }

    public void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.c = str;
            new Thread(new c(this)).start();
        }
    }
}
