package com.sina.weibo.sdk.b;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import com.qihoo.video.httpservice.NetWorkError;
import com.sina.weibo.sdk.a.c;
import com.sina.weibo.sdk.c.a;
import com.sina.weibo.sdk.d.m;

class b extends o {

    /* renamed from: b  reason: collision with root package name */
    private Activity f1198b;
    private a c;
    private c d;
    private boolean e = false;

    public b(Activity activity, a aVar) {
        this.f1198b = activity;
        this.c = aVar;
        this.d = this.c.b();
    }

    private void a(String str) {
        Bundle a2 = m.a(str);
        String string = a2.getString(NetWorkError.ErrorCode);
        String string2 = a2.getString("error_code");
        String string3 = a2.getString("error_description");
        if (string == null && string2 == null) {
            if (this.d != null) {
                this.d.a(a2);
            }
        } else if (this.d != null) {
            this.d.a(new a(string2, string, string3));
        }
    }

    public void onPageFinished(WebView webView, String str) {
        if (this.f1215a != null) {
            this.f1215a.b(webView, str);
        }
        super.onPageFinished(webView, str);
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        if (this.f1215a != null) {
            this.f1215a.a(webView, str, bitmap);
        }
        if (!str.startsWith(this.c.a().b()) || this.e) {
            super.onPageStarted(webView, str, bitmap);
            return;
        }
        this.e = true;
        a(str);
        webView.stopLoading();
        j.a(this.f1198b, this.c.c(), (String) null);
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        if (this.f1215a != null) {
            this.f1215a.a(webView, i, str, str2);
        }
        super.onReceivedError(webView, i, str, str2);
    }

    public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        if (this.f1215a != null) {
            this.f1215a.a(webView, sslErrorHandler, sslError);
        }
        super.onReceivedSslError(webView, sslErrorHandler, sslError);
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (this.f1215a != null) {
            this.f1215a.a(webView, str);
        }
        if (str.startsWith("sms:")) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.putExtra("address", str.replace("sms:", ""));
            intent.setType("vnd.android-dir/mms-sms");
            this.f1198b.startActivity(intent);
            return true;
        } else if (!str.startsWith("sinaweibo://browser/close")) {
            return super.shouldOverrideUrlLoading(webView, str);
        } else {
            if (this.d != null) {
                this.d.a();
            }
            j.a(this.f1198b, this.c.c(), (String) null);
            return true;
        }
    }
}
