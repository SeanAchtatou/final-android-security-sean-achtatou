package com.sina.weibo.sdk;

import android.text.TextUtils;
import android.util.Pair;
import com.sina.weibo.sdk.d.h;
import java.io.File;

class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f1217a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f1218b;

    c(b bVar, String str) {
        this.f1217a = bVar;
        this.f1218b = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.sina.weibo.sdk.b.a(com.sina.weibo.sdk.b, boolean):void
     arg types: [com.sina.weibo.sdk.b, int]
     candidates:
      com.sina.weibo.sdk.b.a(android.content.Context, java.lang.String):com.sina.weibo.sdk.b
      com.sina.weibo.sdk.b.a(com.sina.weibo.sdk.b, com.sina.weibo.sdk.e):void
      com.sina.weibo.sdk.b.a(com.sina.weibo.sdk.b, boolean):void */
    public void run() {
        Pair b2 = b.c(this.f1217a.c, this.f1218b);
        try {
            this.f1217a.e.await();
            if (this.f1217a.f != null && this.f1217a.f.a()) {
                String a2 = this.f1217a.f.f1238b;
                String b3 = this.f1217a.f.f1237a;
                if (b2 != null && b2.second != null && ((Integer) b2.first).intValue() >= this.f1217a.f.c) {
                    b.c(this.f1217a.c, b3, ((File) b2.second).getAbsolutePath());
                } else if (h.b(this.f1217a.c) && !TextUtils.isEmpty(a2)) {
                    b.d(this.f1217a.c, b3, a2);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            this.f1217a.h = true;
        }
    }
}
