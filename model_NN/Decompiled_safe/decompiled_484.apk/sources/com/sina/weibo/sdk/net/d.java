package com.sina.weibo.sdk.net;

import android.text.TextUtils;
import com.sina.weibo.sdk.d.f;
import java.net.URI;
import org.apache.http.HttpResponse;
import org.apache.http.client.RedirectHandler;
import org.apache.http.protocol.HttpContext;

public abstract class d implements RedirectHandler {
    private static final String c = d.class.getCanonicalName();

    /* renamed from: a  reason: collision with root package name */
    int f1252a;

    /* renamed from: b  reason: collision with root package name */
    String f1253b;
    private String d;

    public abstract void a();

    public abstract boolean a(String str);

    public String b() {
        return this.f1253b;
    }

    public URI getLocationURI(HttpResponse httpResponse, HttpContext httpContext) {
        f.a(c, "CustomRedirectHandler getLocationURI getRedirectUrl : " + this.d);
        if (!TextUtils.isEmpty(this.d)) {
            return URI.create(this.d);
        }
        return null;
    }

    public boolean isRedirectRequested(HttpResponse httpResponse, HttpContext httpContext) {
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        if (statusCode == 301 || statusCode == 302) {
            this.d = httpResponse.getFirstHeader("Location").getValue();
            if (!TextUtils.isEmpty(this.d) && this.f1252a < 15 && a(this.d)) {
                this.f1252a++;
                return true;
            }
        } else if (statusCode == 200) {
            this.f1253b = this.d;
        } else {
            a();
        }
        return false;
    }
}
