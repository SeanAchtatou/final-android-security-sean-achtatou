package com.sina.weibo.sdk.b;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import com.qihoo360.daily.wxapi.WXConfig;
import com.sina.weibo.sdk.a.c;
import com.sina.weibo.sdk.api.ImageObject;
import com.sina.weibo.sdk.api.MusicObject;
import com.sina.weibo.sdk.api.TextObject;
import com.sina.weibo.sdk.api.VideoObject;
import com.sina.weibo.sdk.api.VoiceObject;
import com.sina.weibo.sdk.api.WebpageObject;
import com.sina.weibo.sdk.api.a.b;
import com.sina.weibo.sdk.api.i;
import com.sina.weibo.sdk.d.g;
import com.sina.weibo.sdk.d.m;

public class f extends e {
    private c e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    private b k;
    private String l;
    private byte[] m;

    public f(Context context) {
        super(context);
        this.c = c.SHARE;
    }

    private void a(Activity activity, int i2, String str) {
        Bundle extras = activity.getIntent().getExtras();
        if (extras != null) {
            Intent intent = new Intent("com.sina.weibo.sdk.action.ACTION_SDK_REQ_ACTIVITY");
            intent.setFlags(131072);
            intent.setPackage(extras.getString("_weibo_appPackage"));
            intent.putExtras(extras);
            intent.putExtra("_weibo_appPackage", activity.getPackageName());
            intent.putExtra("_weibo_resp_errcode", i2);
            intent.putExtra("_weibo_resp_errstr", str);
            try {
                activity.startActivityForResult(intent, 765);
            } catch (ActivityNotFoundException e2) {
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0041 A[SYNTHETIC, Splitter:B:21:0x0041] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0053 A[SYNTHETIC, Splitter:B:29:0x0053] */
    /* JADX WARNING: Removed duplicated region for block: B:42:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.lang.String r8, byte[] r9) {
        /*
            r7 = this;
            boolean r0 = android.text.TextUtils.isEmpty(r8)     // Catch:{ SecurityException -> 0x0057 }
            if (r0 != 0) goto L_0x0044
            java.io.File r2 = new java.io.File     // Catch:{ SecurityException -> 0x0057 }
            r2.<init>(r8)     // Catch:{ SecurityException -> 0x0057 }
            boolean r0 = r2.exists()     // Catch:{ SecurityException -> 0x0057 }
            if (r0 == 0) goto L_0x0044
            boolean r0 = r2.canRead()     // Catch:{ SecurityException -> 0x0057 }
            if (r0 == 0) goto L_0x0044
            long r0 = r2.length()     // Catch:{ SecurityException -> 0x0057 }
            r4 = 0
            int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x0044
            long r0 = r2.length()     // Catch:{ SecurityException -> 0x0057 }
            int r0 = (int) r0     // Catch:{ SecurityException -> 0x0057 }
            byte[] r3 = new byte[r0]     // Catch:{ SecurityException -> 0x0057 }
            r1 = 0
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ IOException -> 0x003d, all -> 0x0050 }
            r0.<init>(r2)     // Catch:{ IOException -> 0x003d, all -> 0x0050 }
            r0.read(r3)     // Catch:{ IOException -> 0x0064, all -> 0x005f }
            byte[] r1 = com.sina.weibo.sdk.d.e.b(r3)     // Catch:{ IOException -> 0x0064, all -> 0x005f }
            r7.m = r1     // Catch:{ IOException -> 0x0064, all -> 0x005f }
            if (r0 == 0) goto L_0x003c
            r0.close()     // Catch:{ Exception -> 0x0059 }
        L_0x003c:
            return
        L_0x003d:
            r0 = move-exception
            r0 = r1
        L_0x003f:
            if (r0 == 0) goto L_0x0044
            r0.close()     // Catch:{ Exception -> 0x005b }
        L_0x0044:
            if (r9 == 0) goto L_0x003c
            int r0 = r9.length
            if (r0 <= 0) goto L_0x003c
            byte[] r0 = com.sina.weibo.sdk.d.e.b(r9)
            r7.m = r0
            goto L_0x003c
        L_0x0050:
            r0 = move-exception
        L_0x0051:
            if (r1 == 0) goto L_0x0056
            r1.close()     // Catch:{ Exception -> 0x005d }
        L_0x0056:
            throw r0     // Catch:{ SecurityException -> 0x0057 }
        L_0x0057:
            r0 = move-exception
            goto L_0x0044
        L_0x0059:
            r0 = move-exception
            goto L_0x003c
        L_0x005b:
            r0 = move-exception
            goto L_0x0044
        L_0x005d:
            r1 = move-exception
            goto L_0x0056
        L_0x005f:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0051
        L_0x0064:
            r1 = move-exception
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.b.f.a(java.lang.String, byte[]):void");
    }

    private void d(Bundle bundle) {
        i iVar = new i();
        iVar.b(bundle);
        StringBuilder sb = new StringBuilder();
        if (iVar.f1191a instanceof TextObject) {
            sb.append(iVar.f1191a.g);
        }
        if (iVar.f1192b instanceof ImageObject) {
            ImageObject imageObject = iVar.f1192b;
            a(imageObject.h, imageObject.g);
        }
        if (iVar.c instanceof TextObject) {
            sb.append(((TextObject) iVar.c).g);
        }
        if (iVar.c instanceof ImageObject) {
            ImageObject imageObject2 = (ImageObject) iVar.c;
            a(imageObject2.h, imageObject2.g);
        }
        if (iVar.c instanceof WebpageObject) {
            sb.append(" ").append(((WebpageObject) iVar.c).f1179a);
        }
        if (iVar.c instanceof MusicObject) {
            sb.append(" ").append(((MusicObject) iVar.c).f1179a);
        }
        if (iVar.c instanceof VideoObject) {
            sb.append(" ").append(((VideoObject) iVar.c).f1179a);
        }
        if (iVar.c instanceof VoiceObject) {
            sb.append(" ").append(((VoiceObject) iVar.c).f1179a);
        }
        this.l = sb.toString();
    }

    public com.sina.weibo.sdk.net.i a(com.sina.weibo.sdk.net.i iVar) {
        if (a()) {
            iVar.a("img", new String(this.m));
        }
        return iVar;
    }

    public void a(Activity activity) {
        a(activity, 1, "send cancel!!!");
    }

    public void a(Activity activity, int i2) {
        if (i2 == 3) {
            a(activity);
            j.a(activity, this.f, (String) null);
        }
    }

    public void a(Activity activity, String str) {
        a(activity, 2, str);
    }

    /* access modifiers changed from: protected */
    public void a(Bundle bundle) {
        this.i = bundle.getString("source");
        this.g = bundle.getString("packagename");
        this.j = bundle.getString("key_hash");
        this.h = bundle.getString(WXConfig.WX_ACCCESS_TOKEN);
        this.f = bundle.getString("key_listener");
        if (!TextUtils.isEmpty(this.f)) {
            this.e = i.a(this.f1201a).a(this.f);
        }
        d(bundle);
        this.f1202b = c("");
    }

    public boolean a() {
        return this.m != null && this.m.length > 0;
    }

    public String b() {
        return this.i;
    }

    public void b(Activity activity) {
        a(activity, 0, "send ok!!!");
    }

    public void b(Bundle bundle) {
        if (this.k != null) {
            this.k.a(bundle);
        }
        if (!TextUtils.isEmpty(this.g)) {
            this.j = g.a(m.a(this.f1201a, this.g));
        }
        bundle.putString(WXConfig.WX_ACCCESS_TOKEN, this.h);
        bundle.putString("source", this.i);
        bundle.putString("packagename", this.g);
        bundle.putString("key_hash", this.j);
        bundle.putString("_weibo_appPackage", this.g);
        bundle.putString("_weibo_appKey", this.i);
        bundle.putInt("_weibo_flag", 538116905);
        bundle.putString("_weibo_sign", this.j);
        if (this.e != null) {
            i a2 = i.a(this.f1201a);
            this.f = a2.a();
            a2.a(this.f, this.e);
            bundle.putString("key_listener", this.f);
        }
    }

    public c c() {
        return this.e;
    }

    public String c(String str) {
        Uri.Builder buildUpon = Uri.parse("http://service.weibo.com/share/mobilesdk.php").buildUpon();
        buildUpon.appendQueryParameter("title", this.l);
        buildUpon.appendQueryParameter("version", "0030105000");
        if (!TextUtils.isEmpty(this.i)) {
            buildUpon.appendQueryParameter("source", this.i);
        }
        if (!TextUtils.isEmpty(this.h)) {
            buildUpon.appendQueryParameter(WXConfig.WX_ACCCESS_TOKEN, this.h);
        }
        String b2 = m.b(this.f1201a, this.i);
        if (!TextUtils.isEmpty(b2)) {
            buildUpon.appendQueryParameter("aid", b2);
        }
        if (!TextUtils.isEmpty(this.g)) {
            buildUpon.appendQueryParameter("packagename", this.g);
        }
        if (!TextUtils.isEmpty(this.j)) {
            buildUpon.appendQueryParameter("key_hash", this.j);
        }
        if (!TextUtils.isEmpty(str)) {
            buildUpon.appendQueryParameter("picinfo", str);
        }
        return buildUpon.build().toString();
    }

    public String h() {
        return this.f;
    }
}
