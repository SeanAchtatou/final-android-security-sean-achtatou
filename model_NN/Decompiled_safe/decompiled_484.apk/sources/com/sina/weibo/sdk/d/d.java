package com.sina.weibo.sdk.d;

import com.qihoo.video.httpservice.NetWorkError;
import com.sina.weibo.sdk.c.c;
import org.json.JSONException;
import org.json.JSONObject;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private String f1229a;

    /* renamed from: b  reason: collision with root package name */
    private String f1230b;

    public static d a(String str) {
        d dVar = new d();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has(NetWorkError.ErrorCode) || jSONObject.has("error_code")) {
                f.a("AidTask", "loadAidFromNet has error !!!");
                throw new c("loadAidFromNet has error !!!");
            }
            dVar.f1229a = jSONObject.optString("aid", "");
            dVar.f1230b = jSONObject.optString("sub", "");
            return dVar;
        } catch (JSONException e) {
            f.a("AidTask", "loadAidFromNet JSONException Msg : " + e.getMessage());
            throw new c("loadAidFromNet has error !!!");
        }
    }

    public String a() {
        return this.f1229a;
    }
}
