package com.sina.weibo.sdk.net;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import com.sina.weibo.sdk.b;
import com.sina.weibo.sdk.c.c;
import com.sina.weibo.sdk.d.f;
import com.sina.weibo.sdk.d.i;
import java.io.File;

public class DownloadService extends IntentService {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1243a = DownloadService.class.getCanonicalName();

    /* renamed from: b  reason: collision with root package name */
    private static final String f1244b = b.f1193a;

    public DownloadService() {
        super(f1243a);
    }

    private static String a(String str) {
        int lastIndexOf = str.lastIndexOf("/");
        return lastIndexOf != -1 ? str.substring(lastIndexOf + 1, str.length()) : "";
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras == null) {
            stopSelf();
            return;
        }
        String string = extras.getString("download_url");
        String string2 = extras.getString("notification_content");
        f.c(f1243a, "onHandleIntent downLoadUrl:" + string + "!!!!!");
        if (TextUtils.isEmpty(string)) {
            f.c(f1243a, "downloadurl is null");
            stopSelf();
            return;
        }
        String str = "";
        try {
            String b2 = HttpManager.b(getApplicationContext(), string, "GET", new i(""));
            String a2 = a(b2);
            if (TextUtils.isEmpty(a2) || !a2.endsWith(".apk")) {
                f.c(f1243a, "redirectDownloadUrl is illeagle");
                stopSelf();
                return;
            }
            str = HttpManager.a(getApplicationContext(), b2, f1244b, a2);
            if (TextUtils.isEmpty(str)) {
                f.c(f1243a, "download failed!");
            } else if (new File(str).exists()) {
                f.c(f1243a, "download successed!");
                i.a(getApplicationContext(), string2, str);
            }
        } catch (c e) {
            e.printStackTrace();
        }
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        if (intent == null) {
        }
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        return super.onStartCommand(intent, i, i2);
    }
}
