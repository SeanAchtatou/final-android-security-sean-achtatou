package com.sina.weibo.sdk.a;

import android.content.Context;
import android.os.Bundle;
import com.sina.weibo.sdk.d.m;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private String f1168a = "";

    /* renamed from: b  reason: collision with root package name */
    private String f1169b = "";
    private String c = "";
    private String d = "";
    private String e = "";

    public a(Context context, String str, String str2, String str3) {
        this.f1168a = str;
        this.f1169b = str2;
        this.c = str3;
        this.d = context.getPackageName();
        this.e = m.a(context, this.d);
    }

    public static a a(Context context, Bundle bundle) {
        return new a(context, bundle.getString("appKey"), bundle.getString("redirectUri"), bundle.getString("scope"));
    }

    public String a() {
        return this.f1168a;
    }

    public String b() {
        return this.f1169b;
    }

    public String c() {
        return this.c;
    }

    public String d() {
        return this.d;
    }

    public String e() {
        return this.e;
    }

    public Bundle f() {
        Bundle bundle = new Bundle();
        bundle.putString("appKey", this.f1168a);
        bundle.putString("redirectUri", this.f1169b);
        bundle.putString("scope", this.c);
        bundle.putString("packagename", this.d);
        bundle.putString("key_hash", this.e);
        return bundle;
    }
}
