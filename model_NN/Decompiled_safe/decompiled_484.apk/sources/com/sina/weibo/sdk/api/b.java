package com.sina.weibo.sdk.api;

import android.os.Parcel;
import android.os.Parcelable;

class b implements Parcelable.Creator<ImageObject> {
    b() {
    }

    /* renamed from: a */
    public ImageObject createFromParcel(Parcel parcel) {
        return new ImageObject(parcel);
    }

    /* renamed from: a */
    public ImageObject[] newArray(int i) {
        return new ImageObject[i];
    }
}
