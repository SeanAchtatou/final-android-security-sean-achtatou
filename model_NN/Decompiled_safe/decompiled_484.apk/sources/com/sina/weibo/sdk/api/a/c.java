package com.sina.weibo.sdk.api.a;

import android.os.Bundle;

public abstract class c extends a {

    /* renamed from: b  reason: collision with root package name */
    public int f1182b;
    public String c;
    public String d;

    public void a(Bundle bundle) {
        this.f1182b = bundle.getInt("_weibo_resp_errcode");
        this.c = bundle.getString("_weibo_resp_errstr");
        this.f1181a = bundle.getString("_weibo_transaction");
        this.d = bundle.getString("_weibo_appPackage");
    }
}
