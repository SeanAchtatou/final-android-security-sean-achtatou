package com.sina.weibo.sdk.d;

import android.util.Log;

public class f {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f1233a = false;

    public static void a() {
        f1233a = false;
    }

    public static void a(String str, String str2) {
        if (f1233a) {
            StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];
            Log.d(str, String.valueOf(String.valueOf(stackTraceElement.getFileName()) + "(" + stackTraceElement.getLineNumber() + ") " + stackTraceElement.getMethodName()) + ": " + str2);
        }
    }

    public static void b(String str, String str2) {
        if (f1233a) {
            StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];
            Log.i(str, String.valueOf(String.valueOf(stackTraceElement.getFileName()) + "(" + stackTraceElement.getLineNumber() + ") " + stackTraceElement.getMethodName()) + ": " + str2);
        }
    }

    public static void c(String str, String str2) {
        if (f1233a) {
            StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];
            Log.e(str, String.valueOf(String.valueOf(stackTraceElement.getFileName()) + "(" + stackTraceElement.getLineNumber() + ") " + stackTraceElement.getMethodName()) + ": " + str2);
        }
    }
}
