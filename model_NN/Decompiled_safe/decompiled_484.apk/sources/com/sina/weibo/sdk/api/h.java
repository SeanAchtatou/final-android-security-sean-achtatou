package com.sina.weibo.sdk.api;

import android.os.Bundle;
import com.sina.weibo.sdk.d.f;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    public BaseMediaObject f1190a;

    public Bundle a(Bundle bundle) {
        if (this.f1190a != null) {
            bundle.putParcelable("_weibo_message_media", this.f1190a);
            bundle.putString("_weibo_message_media_extra", this.f1190a.b());
        }
        return bundle;
    }

    public boolean a() {
        if (this.f1190a == null) {
            f.c("Weibo.WeiboMessage", "checkArgs fail, mediaObject is null");
            return false;
        } else if (this.f1190a == null || this.f1190a.a()) {
            return true;
        } else {
            f.c("Weibo.WeiboMessage", "checkArgs fail, mediaObject is invalid");
            return false;
        }
    }
}
