package com.sina.weibo.sdk.api.a;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.sina.weibo.sdk.d.m;

public class l {
    public static Dialog a(Context context, d dVar) {
        String str = "提示";
        String str2 = "未安装微博客户端，是否现在去下载？";
        String str3 = "现在下载";
        String str4 = "以后再说";
        if (!m.a(context.getApplicationContext())) {
            str = "Notice";
            str2 = "Sina Weibo client is not installed, download now?";
            str3 = "Download Now";
            str4 = "Download Later";
        }
        return new AlertDialog.Builder(context).setMessage(str2).setTitle(str).setPositiveButton(str3, new m(context)).setNegativeButton(str4, new n(dVar)).create();
    }

    /* access modifiers changed from: private */
    public static void b(Context context) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setFlags(268435456);
        intent.setData(Uri.parse("http://app.sina.cn/appdetail.php?appID=84560"));
        try {
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
