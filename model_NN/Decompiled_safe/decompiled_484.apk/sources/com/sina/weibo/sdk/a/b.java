package com.sina.weibo.sdk.a;

import android.os.Bundle;
import android.text.TextUtils;
import com.qihoo360.daily.wxapi.WXConfig;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private String f1177a = "";

    /* renamed from: b  reason: collision with root package name */
    private String f1178b = "";
    private String c = "";
    private long d = 0;

    public static b a(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        b bVar = new b();
        bVar.a(a(bundle, "uid", ""));
        bVar.b(a(bundle, WXConfig.WX_ACCCESS_TOKEN, ""));
        bVar.d(a(bundle, "expires_in", ""));
        bVar.c(a(bundle, "refresh_token", ""));
        return bVar;
    }

    private static String a(Bundle bundle, String str, String str2) {
        String string;
        return (bundle == null || (string = bundle.getString(str)) == null) ? str2 : string;
    }

    public void a(long j) {
        this.d = j;
    }

    public void a(String str) {
        this.f1177a = str;
    }

    public boolean a() {
        return !TextUtils.isEmpty(this.f1178b);
    }

    public String b() {
        return this.f1177a;
    }

    public void b(String str) {
        this.f1178b = str;
    }

    public String c() {
        return this.f1178b;
    }

    public void c(String str) {
        this.c = str;
    }

    public long d() {
        return this.d;
    }

    public void d(String str) {
        if (!TextUtils.isEmpty(str) && !str.equals("0")) {
            a(System.currentTimeMillis() + (Long.parseLong(str) * 1000));
        }
    }

    public String toString() {
        return "uid: " + this.f1177a + ", " + WXConfig.WX_ACCCESS_TOKEN + ": " + this.f1178b + ", " + "refresh_token" + ": " + this.c + ", " + "expires_in" + ": " + Long.toString(this.d);
    }
}
