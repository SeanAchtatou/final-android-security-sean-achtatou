package com.sina.weibo.sdk.api;

import android.os.Parcel;
import android.os.Parcelable;

public class CmdObject extends BaseMediaObject {
    public static final Parcelable.Creator<CmdObject> CREATOR = new a();
    public String g;

    public CmdObject() {
    }

    public CmdObject(Parcel parcel) {
        this.g = parcel.readString();
    }

    /* access modifiers changed from: protected */
    public BaseMediaObject a(String str) {
        return this;
    }

    public boolean a() {
        return (this.g == null || this.g.length() == 0 || this.g.length() > 1024) ? false : true;
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "";
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.g);
    }
}
