package com.sina.weibo.sdk;

import android.text.TextUtils;
import com.qihoo.video.httpservice.NetWorkError;
import com.sina.weibo.sdk.d.f;
import org.json.JSONException;
import org.json.JSONObject;

public class e {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f1237a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public String f1238b;
    /* access modifiers changed from: private */
    public int c;

    public e(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has(NetWorkError.ErrorCode) || jSONObject.has("error_code")) {
                f.a(b.f1194b, "parse NotificationInfo error !!!");
                return;
            }
            this.f1238b = jSONObject.optString("sdk_url", "");
            this.f1237a = jSONObject.optString("sdk_push", "");
            this.c = jSONObject.optInt("version_code");
        } catch (JSONException e) {
            f.a(b.f1194b, "parse NotificationInfo error: " + e.getMessage());
        }
    }

    public boolean a() {
        return !TextUtils.isEmpty(this.f1237a);
    }
}
