package com.sina.weibo.sdk.d;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import com.qihoo.dynamic.util.Md5Util;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Locale;
import java.util.UUID;

public class m {
    public static Bundle a(String str) {
        try {
            URL url = new URL(str);
            Bundle c = c(url.getQuery());
            c.putAll(c(url.getRef()));
            return c;
        } catch (MalformedURLException e) {
            return new Bundle();
        }
    }

    public static String a() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    public static String a(Context context, String str) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(str, 64);
            for (Signature byteArray : packageInfo.signatures) {
                byte[] byteArray2 = byteArray.toByteArray();
                if (byteArray2 != null) {
                    return g.a(byteArray2);
                }
            }
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    public static boolean a(Context context) {
        try {
            Locale locale = context.getResources().getConfiguration().locale;
            return Locale.CHINA.equals(locale) || Locale.CHINESE.equals(locale) || Locale.SIMPLIFIED_CHINESE.equals(locale) || Locale.TAIWAN.equals(locale);
        } catch (Exception e) {
            return true;
        }
    }

    public static Bundle b(String str) {
        try {
            return c(new URI(str).getQuery());
        } catch (Exception e) {
            return new Bundle();
        }
    }

    public static String b(Context context) {
        StringBuilder sb = new StringBuilder();
        sb.append(Build.MANUFACTURER).append("-").append(Build.MODEL);
        sb.append("_");
        sb.append(Build.VERSION.RELEASE);
        sb.append("_");
        sb.append("weibosdk");
        sb.append("_");
        sb.append("0030105000");
        sb.append("_android");
        return sb.toString();
    }

    public static String b(Context context, String str) {
        a a2 = a.a(context);
        String a3 = a2.a();
        if (!TextUtils.isEmpty(a3)) {
            return a3;
        }
        a2.a(str);
        return "";
    }

    public static Bundle c(String str) {
        Bundle bundle = new Bundle();
        if (str != null) {
            for (String split : str.split("&")) {
                String[] split2 = split.split("=");
                try {
                    bundle.putString(URLDecoder.decode(split2[0], Md5Util.DEFAULT_CHARSET), URLDecoder.decode(split2[1], Md5Util.DEFAULT_CHARSET));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
        return bundle;
    }
}
