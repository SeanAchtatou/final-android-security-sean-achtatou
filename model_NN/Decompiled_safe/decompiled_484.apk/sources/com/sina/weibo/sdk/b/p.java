package com.sina.weibo.sdk.b;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import com.qihoo360.daily.wxapi.WXConfig;
import com.sina.weibo.sdk.a.c;
import com.sina.weibo.sdk.d.g;
import com.sina.weibo.sdk.d.m;

public class p extends e {
    private c e;
    private String f;
    private q g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    private String m;
    private String n;
    private String o;
    private String p;

    public p(Context context) {
        super(context);
        this.c = c.WIDGET;
    }

    private String c(String str) {
        Uri.Builder buildUpon = Uri.parse(str).buildUpon();
        buildUpon.appendQueryParameter("version", "0030105000");
        if (!TextUtils.isEmpty(this.k)) {
            buildUpon.appendQueryParameter("source", this.k);
        }
        if (!TextUtils.isEmpty(this.j)) {
            buildUpon.appendQueryParameter(WXConfig.WX_ACCCESS_TOKEN, this.j);
        }
        String b2 = m.b(this.f1201a, this.k);
        if (!TextUtils.isEmpty(b2)) {
            buildUpon.appendQueryParameter("aid", b2);
        }
        if (!TextUtils.isEmpty(this.i)) {
            buildUpon.appendQueryParameter("packagename", this.i);
        }
        if (!TextUtils.isEmpty(this.l)) {
            buildUpon.appendQueryParameter("key_hash", this.l);
        }
        if (!TextUtils.isEmpty(this.m)) {
            buildUpon.appendQueryParameter("fuid", this.m);
        }
        if (!TextUtils.isEmpty(this.o)) {
            buildUpon.appendQueryParameter("q", this.o);
        }
        if (!TextUtils.isEmpty(this.n)) {
            buildUpon.appendQueryParameter("content", this.n);
        }
        if (!TextUtils.isEmpty(this.p)) {
            buildUpon.appendQueryParameter("category", this.p);
        }
        return buildUpon.build().toString();
    }

    public c a() {
        return this.e;
    }

    public void a(Activity activity, int i2) {
        if (i2 == 3) {
            j.a(activity, this.f, this.h);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Bundle bundle) {
        this.k = bundle.getString("source");
        this.i = bundle.getString("packagename");
        this.l = bundle.getString("key_hash");
        this.j = bundle.getString(WXConfig.WX_ACCCESS_TOKEN);
        this.m = bundle.getString("fuid");
        this.o = bundle.getString("q");
        this.n = bundle.getString("content");
        this.p = bundle.getString("category");
        this.f = bundle.getString("key_listener");
        if (!TextUtils.isEmpty(this.f)) {
            this.e = i.a(this.f1201a).a(this.f);
        }
        this.h = bundle.getString("key_widget_callback");
        if (!TextUtils.isEmpty(this.h)) {
            this.g = i.a(this.f1201a).c(this.h);
        }
        this.f1202b = c(this.f1202b);
    }

    public String b() {
        return this.f;
    }

    public void b(Bundle bundle) {
        this.i = this.f1201a.getPackageName();
        if (!TextUtils.isEmpty(this.i)) {
            this.l = g.a(m.a(this.f1201a, this.i));
        }
        bundle.putString(WXConfig.WX_ACCCESS_TOKEN, this.j);
        bundle.putString("source", this.k);
        bundle.putString("packagename", this.i);
        bundle.putString("key_hash", this.l);
        bundle.putString("fuid", this.m);
        bundle.putString("q", this.o);
        bundle.putString("content", this.n);
        bundle.putString("category", this.p);
        i a2 = i.a(this.f1201a);
        if (this.e != null) {
            this.f = a2.a();
            a2.a(this.f, this.e);
            bundle.putString("key_listener", this.f);
        }
        if (this.g != null) {
            this.h = a2.a();
            a2.a(this.h, this.g);
            bundle.putString("key_widget_callback", this.h);
        }
    }

    public q c() {
        return this.g;
    }

    public String h() {
        return this.h;
    }
}
