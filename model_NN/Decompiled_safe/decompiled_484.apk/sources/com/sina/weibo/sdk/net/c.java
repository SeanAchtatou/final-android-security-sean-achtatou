package com.sina.weibo.sdk.net;

import android.content.Context;
import android.os.AsyncTask;

class c extends AsyncTask<Void, Void, b<String>> {

    /* renamed from: a  reason: collision with root package name */
    private final Context f1250a;

    /* renamed from: b  reason: collision with root package name */
    private final String f1251b;
    private final i c;
    private final String d;
    private final h e;

    public c(Context context, String str, i iVar, String str2, h hVar) {
        this.f1250a = context;
        this.f1251b = str;
        this.c = iVar;
        this.d = str2;
        this.e = hVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public b<String> doInBackground(Void... voidArr) {
        try {
            return new b<>(HttpManager.a(this.f1250a, this.f1251b, this.d, this.c));
        } catch (com.sina.weibo.sdk.c.c e2) {
            return new b<>(e2);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(b<String> bVar) {
        com.sina.weibo.sdk.c.c b2 = bVar.b();
        if (b2 != null) {
            this.e.a(b2);
        } else {
            this.e.a(bVar.a());
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
    }
}
