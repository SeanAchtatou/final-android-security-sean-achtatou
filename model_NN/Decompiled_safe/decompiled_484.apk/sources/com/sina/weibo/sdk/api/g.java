package com.sina.weibo.sdk.api;

import android.os.Parcel;
import android.os.Parcelable;

class g implements Parcelable.Creator<WebpageObject> {
    g() {
    }

    /* renamed from: a */
    public WebpageObject createFromParcel(Parcel parcel) {
        return new WebpageObject(parcel);
    }

    /* renamed from: a */
    public WebpageObject[] newArray(int i) {
        return new WebpageObject[i];
    }
}
