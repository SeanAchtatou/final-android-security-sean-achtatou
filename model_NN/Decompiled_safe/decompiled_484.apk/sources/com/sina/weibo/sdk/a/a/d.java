package com.sina.weibo.sdk.a.a;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.qihoo360.daily.wxapi.WXConfig;
import com.sina.weibo.sdk.a.a;
import com.sina.weibo.sdk.a.c;
import com.sina.weibo.sdk.b;
import com.sina.weibo.sdk.b.j;
import com.sina.weibo.sdk.d.h;
import com.sina.weibo.sdk.d.l;
import com.sina.weibo.sdk.d.m;
import com.sina.weibo.sdk.net.i;

class d {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1175a = d.class.getName();

    /* renamed from: b  reason: collision with root package name */
    private Context f1176b;
    private a c;

    public d(Context context, a aVar) {
        this.f1176b = context;
        this.c = aVar;
    }

    private void b(c cVar, int i) {
        if (cVar != null) {
            i iVar = new i(this.c.a());
            iVar.a("client_id", this.c.a());
            iVar.a("redirect_uri", this.c.b());
            iVar.a("scope", this.c.c());
            iVar.a("response_type", WXConfig.WX_CODE);
            iVar.a("version", "0030105000");
            String b2 = m.b(this.f1176b, this.c.a());
            if (!TextUtils.isEmpty(b2)) {
                iVar.a("aid", b2);
            }
            if (1 == i) {
                iVar.a("packagename", this.c.d());
                iVar.a("key_hash", this.c.e());
            }
            String str = "https://open.weibo.cn/oauth2/authorize?" + iVar.c();
            if (!h.a(this.f1176b)) {
                l.a(this.f1176b, "Error", "Application requires permission to access the Internet");
                return;
            }
            com.sina.weibo.sdk.b.a aVar = new com.sina.weibo.sdk.b.a(this.f1176b);
            aVar.a(this.c);
            aVar.a(cVar);
            aVar.a(str);
            aVar.b("微博登录");
            Bundle d = aVar.d();
            Intent intent = new Intent(this.f1176b, j.class);
            intent.putExtras(d);
            this.f1176b.startActivity(intent);
        }
    }

    public a a() {
        return this.c;
    }

    public void a(c cVar) {
        a(cVar, 1);
    }

    public void a(c cVar, int i) {
        b(cVar, i);
        b.a(this.f1176b, this.c.a()).a();
    }
}
