package com.sina.weibo.sdk.d;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import java.io.File;

public class i {
    private static int a(Context context) {
        int c = c(context, "com_sina_weibo_sdk_weibo_logo", "drawable");
        if (c > 0) {
            return c;
        }
        return 17301659;
    }

    private static PendingIntent a(Context context, String str) {
        if (TextUtils.isEmpty(str)) {
            return PendingIntent.getActivity(context, 0, new Intent(), 16);
        }
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(str)), "application/vnd.android.package-archive");
        return PendingIntent.getActivity(context, 0, intent, 16);
    }

    public static void a(Context context, String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            ((NotificationManager) context.getSystemService("notification")).notify(1, b(context, str, str2));
        }
    }

    private static Notification b(Context context, String str, String str2) {
        String a2 = j.a(context, "Weibo", "微博", "微博");
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setAutoCancel(true);
        builder.setWhen(System.currentTimeMillis());
        builder.setLargeIcon(((BitmapDrawable) j.a(context, "ic_com_sina_weibo_sdk_weibo_logo.png")).getBitmap());
        builder.setSmallIcon(a(context));
        builder.setContentTitle(a2);
        builder.setTicker(str);
        builder.setContentText(str);
        builder.setContentIntent(a(context, str2));
        return builder.build();
    }

    private static int c(Context context, String str, String str2) {
        String packageName = context.getApplicationContext().getPackageName();
        try {
            return context.getPackageManager().getResourcesForApplication(packageName).getIdentifier(str, str2, packageName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
