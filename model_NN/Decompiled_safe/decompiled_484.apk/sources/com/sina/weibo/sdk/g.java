package com.sina.weibo.sdk;

import android.text.TextUtils;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private String f1241a;

    /* renamed from: b  reason: collision with root package name */
    private int f1242b;

    /* access modifiers changed from: private */
    public void a(int i) {
        this.f1242b = i;
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        this.f1241a = str;
    }

    public String a() {
        return this.f1241a;
    }

    public int b() {
        return this.f1242b;
    }

    public boolean c() {
        return !TextUtils.isEmpty(this.f1241a) && this.f1242b > 0;
    }

    public String toString() {
        return "WeiboInfo: PackageName = " + this.f1241a + ", supportApi = " + this.f1242b;
    }
}
