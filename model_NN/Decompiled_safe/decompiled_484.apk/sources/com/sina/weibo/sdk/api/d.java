package com.sina.weibo.sdk.api;

import android.os.Parcel;
import android.os.Parcelable;

class d implements Parcelable.Creator<TextObject> {
    d() {
    }

    /* renamed from: a */
    public TextObject createFromParcel(Parcel parcel) {
        return new TextObject(parcel);
    }

    /* renamed from: a */
    public TextObject[] newArray(int i) {
        return new TextObject[i];
    }
}
