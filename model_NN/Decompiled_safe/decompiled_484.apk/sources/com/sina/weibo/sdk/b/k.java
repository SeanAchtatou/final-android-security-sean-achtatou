package com.sina.weibo.sdk.b;

import android.text.TextUtils;
import com.sina.weibo.sdk.c.c;
import com.sina.weibo.sdk.d.f;
import com.sina.weibo.sdk.net.h;

class k implements h {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ j f1210a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ f f1211b;

    k(j jVar, f fVar) {
        this.f1210a = jVar;
        this.f1211b = fVar;
    }

    public void a(c cVar) {
        f.a(j.f1208a, "post onWeiboException " + cVar.getMessage());
        this.f1211b.a(this.f1210a, cVar.getMessage());
        this.f1210a.finish();
    }

    public void a(String str) {
        f.a(j.f1208a, "post onComplete : " + str);
        g a2 = g.a(str);
        if (a2 == null || a2.a() != 1 || TextUtils.isEmpty(a2.b())) {
            this.f1211b.a(this.f1210a, "upload pic faild");
            this.f1210a.finish();
            return;
        }
        this.f1210a.a(this.f1211b.c(a2.b()));
    }
}
