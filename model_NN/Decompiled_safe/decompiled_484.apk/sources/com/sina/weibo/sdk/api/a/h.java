package com.sina.weibo.sdk.api.a;

import android.content.Context;
import android.os.Bundle;
import com.sina.weibo.sdk.g;

public class h extends b {

    /* renamed from: b  reason: collision with root package name */
    public com.sina.weibo.sdk.api.h f1183b;

    public int a() {
        return 1;
    }

    public void a(Bundle bundle) {
        super.a(bundle);
        bundle.putAll(this.f1183b.a(bundle));
    }

    /* access modifiers changed from: package-private */
    public final boolean a(Context context, g gVar, k kVar) {
        if (this.f1183b == null || gVar == null || !gVar.c()) {
            return false;
        }
        if (kVar == null || kVar.a(context, gVar, this.f1183b)) {
            return this.f1183b.a();
        }
        return false;
    }
}
