package a;

import java.io.IOException;
import java.io.InterruptedIOException;

public class a extends ac {

    /* renamed from: a  reason: collision with root package name */
    private static a f0a;
    private boolean c;
    private a d;
    private long e;

    private static synchronized void a(a aVar, long j, boolean z) {
        synchronized (a.class) {
            if (f0a == null) {
                f0a = new a();
                new d().start();
            }
            long nanoTime = System.nanoTime();
            if (j != 0 && z) {
                aVar.e = Math.min(j, aVar.d() - nanoTime) + nanoTime;
            } else if (j != 0) {
                aVar.e = nanoTime + j;
            } else if (z) {
                aVar.e = aVar.d();
            } else {
                throw new AssertionError();
            }
            long b2 = aVar.b(nanoTime);
            a aVar2 = f0a;
            while (aVar2.d != null && b2 >= aVar2.d.b(nanoTime)) {
                aVar2 = aVar2.d;
            }
            aVar.d = aVar2.d;
            aVar2.d = aVar;
            if (aVar2 == f0a) {
                a.class.notify();
            }
        }
    }

    private static synchronized boolean a(a aVar) {
        boolean z;
        synchronized (a.class) {
            a aVar2 = f0a;
            while (true) {
                if (aVar2 == null) {
                    z = true;
                    break;
                } else if (aVar2.d == aVar) {
                    aVar2.d = aVar.d;
                    aVar.d = null;
                    z = false;
                    break;
                } else {
                    aVar2 = aVar2.d;
                }
            }
        }
        return z;
    }

    private long b(long j) {
        return this.e - j;
    }

    /* access modifiers changed from: private */
    public static synchronized a h() {
        a aVar = null;
        synchronized (a.class) {
            a aVar2 = f0a.d;
            if (aVar2 == null) {
                a.class.wait();
            } else {
                long b2 = aVar2.b(System.nanoTime());
                if (b2 > 0) {
                    long j = b2 / 1000000;
                    a.class.wait(j, (int) (b2 - (1000000 * j)));
                } else {
                    f0a.d = aVar2.d;
                    aVar2.d = null;
                    aVar = aVar2;
                }
            }
        }
        return aVar;
    }

    public final aa a(aa aaVar) {
        return new b(this, aaVar);
    }

    public final ab a(ab abVar) {
        return new c(this, abVar);
    }

    /* access modifiers changed from: package-private */
    public final IOException a(IOException iOException) {
        if (!a_()) {
            return iOException;
        }
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        interruptedIOException.initCause(iOException);
        return interruptedIOException;
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        if (a_() && z) {
            throw new InterruptedIOException("timeout");
        }
    }

    public final boolean a_() {
        if (!this.c) {
            return false;
        }
        this.c = false;
        return a(this);
    }

    public final void c() {
        if (this.c) {
            throw new IllegalStateException("Unbalanced enter/exit");
        }
        long b_ = b_();
        boolean c_ = c_();
        if (b_ != 0 || c_) {
            this.c = true;
            a(this, b_, c_);
        }
    }
}
