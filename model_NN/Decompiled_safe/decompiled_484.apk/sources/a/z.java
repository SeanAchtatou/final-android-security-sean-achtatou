package a;

import java.util.Arrays;

final class z extends j {
    final transient byte[][] f;
    final transient int[] g;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    z(f fVar, int i) {
        super(null);
        int i2 = 0;
        ae.a(fVar.f11b, 0, (long) i);
        x xVar = fVar.f10a;
        int i3 = 0;
        int i4 = 0;
        while (i4 < i) {
            if (xVar.c == xVar.f36b) {
                throw new AssertionError("s.limit == s.pos");
            }
            i4 += xVar.c - xVar.f36b;
            i3++;
            xVar = xVar.f;
        }
        this.f = new byte[i3][];
        this.g = new int[(i3 * 2)];
        x xVar2 = fVar.f10a;
        int i5 = 0;
        while (i2 < i) {
            this.f[i5] = xVar2.f35a;
            i2 += xVar2.c - xVar2.f36b;
            this.g[i5] = i2;
            this.g[this.f.length + i5] = xVar2.f36b;
            xVar2.d = true;
            i5++;
            xVar2 = xVar2.f;
        }
    }

    private int b(int i) {
        int binarySearch = Arrays.binarySearch(this.g, 0, this.f.length, i + 1);
        return binarySearch >= 0 ? binarySearch : binarySearch ^ -1;
    }

    private j h() {
        return new j(g());
    }

    public byte a(int i) {
        ae.a((long) this.g[this.f.length - 1], (long) i, 1);
        int b2 = b(i);
        return this.f[b2][(i - (b2 == 0 ? 0 : this.g[b2 - 1])) + this.g[this.f.length + b2]];
    }

    public String a() {
        return h().a();
    }

    /* access modifiers changed from: package-private */
    public void a(f fVar) {
        int i = 0;
        int length = this.f.length;
        int i2 = 0;
        while (i < length) {
            int i3 = this.g[length + i];
            int i4 = this.g[i];
            x xVar = new x(this.f[i], i3, (i3 + i4) - i2);
            if (fVar.f10a == null) {
                xVar.g = xVar;
                xVar.f = xVar;
                fVar.f10a = xVar;
            } else {
                fVar.f10a.g.a(xVar);
            }
            i++;
            i2 = i4;
        }
        fVar.f11b = ((long) i2) + fVar.f11b;
    }

    public boolean a(int i, j jVar, int i2, int i3) {
        if (i > f() - i3) {
            return false;
        }
        int b2 = b(i);
        while (i3 > 0) {
            int i4 = b2 == 0 ? 0 : this.g[b2 - 1];
            int min = Math.min(i3, ((this.g[b2] - i4) + i4) - i);
            if (!jVar.a(i2, this.f[b2], (i - i4) + this.g[this.f.length + b2], min)) {
                return false;
            }
            i += min;
            i2 += min;
            i3 -= min;
            b2++;
        }
        return true;
    }

    public boolean a(int i, byte[] bArr, int i2, int i3) {
        if (i > f() - i3 || i2 > bArr.length - i3) {
            return false;
        }
        int b2 = b(i);
        while (i3 > 0) {
            int i4 = b2 == 0 ? 0 : this.g[b2 - 1];
            int min = Math.min(i3, ((this.g[b2] - i4) + i4) - i);
            if (!ae.a(this.f[b2], (i - i4) + this.g[this.f.length + b2], bArr, i2, min)) {
                return false;
            }
            i += min;
            i2 += min;
            i3 -= min;
            b2++;
        }
        return true;
    }

    public String b() {
        return h().b();
    }

    public j c() {
        return h().c();
    }

    public String d() {
        return h().d();
    }

    public j e() {
        return h().e();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof j) && ((j) obj).f() == f() && a(0, (j) obj, 0, f());
    }

    public int f() {
        return this.g[this.f.length - 1];
    }

    public byte[] g() {
        int i = 0;
        byte[] bArr = new byte[this.g[this.f.length - 1]];
        int length = this.f.length;
        int i2 = 0;
        while (i < length) {
            int i3 = this.g[length + i];
            int i4 = this.g[i];
            System.arraycopy(this.f[i], i3, bArr, i2, i4 - i2);
            i++;
            i2 = i4;
        }
        return bArr;
    }

    public int hashCode() {
        int i = this.d;
        if (i == 0) {
            i = 1;
            int length = this.f.length;
            int i2 = 0;
            int i3 = 0;
            while (i2 < length) {
                byte[] bArr = this.f[i2];
                int i4 = this.g[length + i2];
                int i5 = this.g[i2];
                int i6 = (i5 - i3) + i4;
                int i7 = i4;
                int i8 = i;
                for (int i9 = i7; i9 < i6; i9++) {
                    i8 = (i8 * 31) + bArr[i9];
                }
                i2++;
                i3 = i5;
                i = i8;
            }
            this.d = i;
        }
        return i;
    }

    public String toString() {
        return h().toString();
    }
}
