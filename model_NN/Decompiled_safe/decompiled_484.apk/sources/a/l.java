package a;

public abstract class l implements aa {

    /* renamed from: a  reason: collision with root package name */
    private final aa f17a;

    public l(aa aaVar) {
        if (aaVar == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f17a = aaVar;
    }

    public ac a() {
        return this.f17a.a();
    }

    public void a_(f fVar, long j) {
        this.f17a.a_(fVar, j);
    }

    public void close() {
        this.f17a.close();
    }

    public void flush() {
        this.f17a.flush();
    }

    public String toString() {
        return getClass().getSimpleName() + "(" + this.f17a.toString() + ")";
    }
}
