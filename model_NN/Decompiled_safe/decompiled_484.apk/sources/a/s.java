package a;

import java.io.InputStream;

final class s implements ab {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ac f27a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ InputStream f28b;

    s(ac acVar, InputStream inputStream) {
        this.f27a = acVar;
        this.f28b = inputStream;
    }

    public long a(f fVar, long j) {
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (j == 0) {
            return 0;
        } else {
            this.f27a.g();
            x e = fVar.e(1);
            int read = this.f28b.read(e.f35a, e.c, (int) Math.min(j, (long) (2048 - e.c)));
            if (read == -1) {
                return -1;
            }
            e.c += read;
            fVar.f11b += (long) read;
            return (long) read;
        }
    }

    public ac a() {
        return this.f27a;
    }

    public void close() {
        this.f28b.close();
    }

    public String toString() {
        return "source(" + this.f28b + ")";
    }
}
