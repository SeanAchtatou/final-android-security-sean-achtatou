package a;

import android.support.v4.media.session.PlaybackStateCompat;

final class y {

    /* renamed from: a  reason: collision with root package name */
    static x f37a;

    /* renamed from: b  reason: collision with root package name */
    static long f38b;

    private y() {
    }

    static x a() {
        synchronized (y.class) {
            if (f37a == null) {
                return new x();
            }
            x xVar = f37a;
            f37a = xVar.f;
            xVar.f = null;
            f38b -= PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH;
            return xVar;
        }
    }

    static void a(x xVar) {
        if (xVar.f != null || xVar.g != null) {
            throw new IllegalArgumentException();
        } else if (!xVar.d) {
            synchronized (y.class) {
                if (f38b + PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH <= 65536) {
                    f38b += PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH;
                    xVar.f = f37a;
                    xVar.c = 0;
                    xVar.f36b = 0;
                    f37a = xVar;
                }
            }
        }
    }
}
