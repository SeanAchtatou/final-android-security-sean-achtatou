package a;

import java.net.Socket;
import java.util.logging.Level;

final class t extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Socket f29a;

    t(Socket socket) {
        this.f29a = socket;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* access modifiers changed from: protected */
    public void a() {
        try {
            this.f29a.close();
        } catch (Exception e) {
            q.f24a.log(Level.WARNING, "Failed to close timed out socket " + this.f29a, (Throwable) e);
        }
    }
}
