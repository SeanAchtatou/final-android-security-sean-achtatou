package a;

import android.support.v4.media.session.PlaybackStateCompat;

final class u implements h {

    /* renamed from: a  reason: collision with root package name */
    public final f f30a;

    /* renamed from: b  reason: collision with root package name */
    public final aa f31b;
    private boolean c;

    public u(aa aaVar) {
        this(aaVar, new f());
    }

    public u(aa aaVar, f fVar) {
        if (aaVar == null) {
            throw new IllegalArgumentException("sink == null");
        }
        this.f30a = fVar;
        this.f31b = aaVar;
    }

    public long a(ab abVar) {
        if (abVar == null) {
            throw new IllegalArgumentException("source == null");
        }
        long j = 0;
        while (true) {
            long a2 = abVar.a(this.f30a, PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH);
            if (a2 == -1) {
                return j;
            }
            j += a2;
            w();
        }
    }

    public ac a() {
        return this.f31b.a();
    }

    public void a_(f fVar, long j) {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f30a.a_(fVar, j);
        w();
    }

    public h b(j jVar) {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f30a.b(jVar);
        return w();
    }

    public h b(String str) {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f30a.b(str);
        return w();
    }

    public f c() {
        return this.f30a;
    }

    public h c(byte[] bArr) {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f30a.c(bArr);
        return w();
    }

    public void close() {
        if (!this.c) {
            Throwable th = null;
            try {
                if (this.f30a.f11b > 0) {
                    this.f31b.a_(this.f30a, this.f30a.f11b);
                }
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.f31b.close();
            } catch (Throwable th3) {
                if (th == null) {
                    th = th3;
                }
            }
            this.c = true;
            if (th != null) {
                ae.a(th);
            }
        }
    }

    public h e() {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        long b2 = this.f30a.b();
        if (b2 > 0) {
            this.f31b.a_(this.f30a, b2);
        }
        return this;
    }

    public void flush() {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        if (this.f30a.f11b > 0) {
            this.f31b.a_(this.f30a, this.f30a.f11b);
        }
        this.f31b.flush();
    }

    public h g(int i) {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f30a.g(i);
        return w();
    }

    public h h(int i) {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f30a.h(i);
        return w();
    }

    public h i(int i) {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f30a.i(i);
        return w();
    }

    public h j(long j) {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f30a.j(j);
        return w();
    }

    public h k(long j) {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f30a.k(j);
        return w();
    }

    public String toString() {
        return "buffer(" + this.f31b + ")";
    }

    public h w() {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        long h = this.f30a.h();
        if (h > 0) {
            this.f31b.a_(this.f30a, h);
        }
        return this;
    }
}
