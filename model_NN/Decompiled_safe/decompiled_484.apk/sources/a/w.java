package a;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.IOException;
import java.io.InputStream;

class w extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ v f34a;

    w(v vVar) {
        this.f34a = vVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public int available() {
        if (!this.f34a.c) {
            return (int) Math.min(this.f34a.f32a.f11b, 2147483647L);
        }
        throw new IOException("closed");
    }

    public void close() {
        this.f34a.close();
    }

    public int read() {
        if (this.f34a.c) {
            throw new IOException("closed");
        } else if (this.f34a.f32a.f11b == 0 && this.f34a.f33b.a(this.f34a.f32a, PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH) == -1) {
            return -1;
        } else {
            return this.f34a.f32a.i() & 255;
        }
    }

    public int read(byte[] bArr, int i, int i2) {
        if (this.f34a.c) {
            throw new IOException("closed");
        }
        ae.a((long) bArr.length, (long) i, (long) i2);
        if (this.f34a.f32a.f11b == 0 && this.f34a.f33b.a(this.f34a.f32a, PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH) == -1) {
            return -1;
        }
        return this.f34a.f32a.a(bArr, i, i2);
    }

    public String toString() {
        return this.f34a + ".inputStream()";
    }
}
