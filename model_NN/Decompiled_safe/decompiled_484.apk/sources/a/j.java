package a;

import com.qihoo.dynamic.util.Md5Util;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class j implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    static final char[] f13a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /* renamed from: b  reason: collision with root package name */
    public static final j f14b = a(new byte[0]);
    final byte[] c;
    transient int d;
    transient String e;

    j(byte[] bArr) {
        this.c = bArr;
    }

    public static j a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("s == null");
        }
        j jVar = new j(str.getBytes(ae.f3a));
        jVar.e = str;
        return jVar;
    }

    public static j a(byte... bArr) {
        if (bArr != null) {
            return new j((byte[]) bArr.clone());
        }
        throw new IllegalArgumentException("data == null");
    }

    public static j b(String str) {
        if (str == null) {
            throw new IllegalArgumentException("base64 == null");
        }
        byte[] a2 = e.a(str);
        if (a2 != null) {
            return new j(a2);
        }
        return null;
    }

    private j c(String str) {
        try {
            return a(MessageDigest.getInstance(str).digest(this.c));
        } catch (NoSuchAlgorithmException e2) {
            throw new AssertionError(e2);
        }
    }

    public byte a(int i) {
        return this.c[i];
    }

    public String a() {
        String str = this.e;
        if (str != null) {
            return str;
        }
        String str2 = new String(this.c, ae.f3a);
        this.e = str2;
        return str2;
    }

    /* access modifiers changed from: package-private */
    public void a(f fVar) {
        fVar.b(this.c, 0, this.c.length);
    }

    public boolean a(int i, j jVar, int i2, int i3) {
        return jVar.a(i2, this.c, i, i3);
    }

    public boolean a(int i, byte[] bArr, int i2, int i3) {
        return i <= this.c.length - i3 && i2 <= bArr.length - i3 && ae.a(this.c, i, bArr, i2, i3);
    }

    public String b() {
        return e.a(this.c);
    }

    public j c() {
        return c(Md5Util.ALGORITHM);
    }

    public String d() {
        char[] cArr = new char[(this.c.length * 2)];
        int i = 0;
        for (byte b2 : this.c) {
            int i2 = i + 1;
            cArr[i] = f13a[(b2 >> 4) & 15];
            i = i2 + 1;
            cArr[i2] = f13a[b2 & 15];
        }
        return new String(cArr);
    }

    public j e() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.c.length) {
                return this;
            }
            byte b2 = this.c[i2];
            if (b2 < 65 || b2 > 90) {
                i = i2 + 1;
            } else {
                byte[] bArr = (byte[]) this.c.clone();
                bArr[i2] = (byte) (b2 + 32);
                for (int i3 = i2 + 1; i3 < bArr.length; i3++) {
                    byte b3 = bArr[i3];
                    if (b3 >= 65 && b3 <= 90) {
                        bArr[i3] = (byte) (b3 + 32);
                    }
                }
                return new j(bArr);
            }
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof j) && ((j) obj).f() == this.c.length && ((j) obj).a(0, this.c, 0, this.c.length);
    }

    public int f() {
        return this.c.length;
    }

    public byte[] g() {
        return (byte[]) this.c.clone();
    }

    public int hashCode() {
        int i = this.d;
        if (i != 0) {
            return i;
        }
        int hashCode = Arrays.hashCode(this.c);
        this.d = hashCode;
        return hashCode;
    }

    public String toString() {
        if (this.c.length == 0) {
            return "ByteString[size=0]";
        }
        if (this.c.length <= 16) {
            return String.format("ByteString[size=%s data=%s]", Integer.valueOf(this.c.length), d());
        }
        return String.format("ByteString[size=%s md5=%s]", Integer.valueOf(this.c.length), c().d());
    }
}
