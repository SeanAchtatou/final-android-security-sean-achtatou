package a;

import java.io.IOException;

class c implements ab {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ab f6a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ a f7b;

    c(a aVar, ab abVar) {
        this.f7b = aVar;
        this.f6a = abVar;
    }

    public long a(f fVar, long j) {
        this.f7b.c();
        try {
            long a2 = this.f6a.a(fVar, j);
            this.f7b.a(true);
            return a2;
        } catch (IOException e) {
            throw this.f7b.a(e);
        } catch (Throwable th) {
            this.f7b.a(false);
            throw th;
        }
    }

    public ac a() {
        return this.f7b;
    }

    public void close() {
        try {
            this.f6a.close();
            this.f7b.a(true);
        } catch (IOException e) {
            throw this.f7b.a(e);
        } catch (Throwable th) {
            this.f7b.a(false);
            throw th;
        }
    }

    public String toString() {
        return "AsyncTimeout.source(" + this.f6a + ")";
    }
}
