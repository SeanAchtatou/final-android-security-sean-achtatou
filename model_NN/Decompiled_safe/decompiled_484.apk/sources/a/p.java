package a;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

public final class p implements ab {

    /* renamed from: a  reason: collision with root package name */
    private final i f22a;

    /* renamed from: b  reason: collision with root package name */
    private final Inflater f23b;
    private int c;
    private boolean d;

    public p(ab abVar, Inflater inflater) {
        this(q.a(abVar), inflater);
    }

    p(i iVar, Inflater inflater) {
        if (iVar == null) {
            throw new IllegalArgumentException("source == null");
        } else if (inflater == null) {
            throw new IllegalArgumentException("inflater == null");
        } else {
            this.f22a = iVar;
            this.f23b = inflater;
        }
    }

    private void c() {
        if (this.c != 0) {
            int remaining = this.c - this.f23b.getRemaining();
            this.c -= remaining;
            this.f22a.g((long) remaining);
        }
    }

    public long a(f fVar, long j) {
        boolean b2;
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.d) {
            throw new IllegalStateException("closed");
        } else if (j == 0) {
            return 0;
        } else {
            do {
                b2 = b();
                try {
                    x e = fVar.e(1);
                    int inflate = this.f23b.inflate(e.f35a, e.c, 2048 - e.c);
                    if (inflate > 0) {
                        e.c += inflate;
                        fVar.f11b += (long) inflate;
                        return (long) inflate;
                    } else if (this.f23b.finished() || this.f23b.needsDictionary()) {
                        c();
                        if (e.f36b == e.c) {
                            fVar.f10a = e.a();
                            y.a(e);
                        }
                        return -1;
                    }
                } catch (DataFormatException e2) {
                    throw new IOException(e2);
                }
            } while (!b2);
            throw new EOFException("source exhausted prematurely");
        }
    }

    public ac a() {
        return this.f22a.a();
    }

    public boolean b() {
        if (!this.f23b.needsInput()) {
            return false;
        }
        c();
        if (this.f23b.getRemaining() != 0) {
            throw new IllegalStateException("?");
        } else if (this.f22a.f()) {
            return true;
        } else {
            x xVar = this.f22a.c().f10a;
            this.c = xVar.c - xVar.f36b;
            this.f23b.setInput(xVar.f35a, xVar.f36b, this.c);
            return false;
        }
    }

    public void close() {
        if (!this.d) {
            this.f23b.end();
            this.d = true;
            this.f22a.close();
        }
    }
}
