package a;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.EOFException;
import java.io.InputStream;

final class v implements i {

    /* renamed from: a  reason: collision with root package name */
    public final f f32a;

    /* renamed from: b  reason: collision with root package name */
    public final ab f33b;
    /* access modifiers changed from: private */
    public boolean c;

    public v(ab abVar) {
        this(abVar, new f());
    }

    public v(ab abVar, f fVar) {
        if (abVar == null) {
            throw new IllegalArgumentException("source == null");
        }
        this.f32a = fVar;
        this.f33b = abVar;
    }

    public long a(byte b2) {
        return a(b2, 0);
    }

    public long a(byte b2, long j) {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        while (j >= this.f32a.f11b) {
            if (this.f33b.a(this.f32a, PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH) == -1) {
                return -1;
            }
        }
        do {
            long a2 = this.f32a.a(b2, j);
            if (a2 != -1) {
                return a2;
            }
            j = this.f32a.f11b;
        } while (this.f33b.a(this.f32a, PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH) != -1);
        return -1;
    }

    public long a(f fVar, long j) {
        if (fVar == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.c) {
            throw new IllegalStateException("closed");
        } else if (this.f32a.f11b == 0 && this.f33b.a(this.f32a, PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH) == -1) {
            return -1;
        } else {
            return this.f32a.a(fVar, Math.min(j, this.f32a.f11b));
        }
    }

    public ac a() {
        return this.f33b.a();
    }

    public void a(long j) {
        if (!b(j)) {
            throw new EOFException();
        }
    }

    public boolean b(long j) {
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.c) {
            throw new IllegalStateException("closed");
        } else {
            while (this.f32a.f11b < j) {
                if (this.f33b.a(this.f32a, PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH) == -1) {
                    return false;
                }
            }
            return true;
        }
    }

    public f c() {
        return this.f32a;
    }

    public j c(long j) {
        a(j);
        return this.f32a.c(j);
    }

    public void close() {
        if (!this.c) {
            this.c = true;
            this.f33b.close();
            this.f32a.t();
        }
    }

    public boolean f() {
        if (!this.c) {
            return this.f32a.f() && this.f33b.a(this.f32a, PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH) == -1;
        }
        throw new IllegalStateException("closed");
    }

    public byte[] f(long j) {
        a(j);
        return this.f32a.f(j);
    }

    public InputStream g() {
        return new w(this);
    }

    public void g(long j) {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        while (j > 0) {
            if (this.f32a.f11b == 0 && this.f33b.a(this.f32a, PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH) == -1) {
                throw new EOFException();
            }
            long min = Math.min(j, this.f32a.b());
            this.f32a.g(min);
            j -= min;
        }
    }

    public byte i() {
        a(1);
        return this.f32a.i();
    }

    public short j() {
        a(2);
        return this.f32a.j();
    }

    public int k() {
        a(4);
        return this.f32a.k();
    }

    public short l() {
        a(2);
        return this.f32a.l();
    }

    public int m() {
        a(4);
        return this.f32a.m();
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0027  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long n() {
        /*
            r6 = this;
            r1 = 0
            r2 = 1
            r6.a(r2)
            r0 = r1
        L_0x0007:
            int r2 = r0 + 1
            long r2 = (long) r2
            boolean r2 = r6.b(r2)
            if (r2 == 0) goto L_0x003f
            a.f r2 = r6.f32a
            long r4 = (long) r0
            byte r2 = r2.b(r4)
            r3 = 48
            if (r2 < r3) goto L_0x001f
            r3 = 57
            if (r2 <= r3) goto L_0x003c
        L_0x001f:
            if (r0 != 0) goto L_0x0025
            r3 = 45
            if (r2 == r3) goto L_0x003c
        L_0x0025:
            if (r0 != 0) goto L_0x003f
            java.lang.NumberFormatException r0 = new java.lang.NumberFormatException
            java.lang.String r3 = "Expected leading [0-9] or '-' character but was %#x"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]
            java.lang.Byte r2 = java.lang.Byte.valueOf(r2)
            r4[r1] = r2
            java.lang.String r1 = java.lang.String.format(r3, r4)
            r0.<init>(r1)
            throw r0
        L_0x003c:
            int r0 = r0 + 1
            goto L_0x0007
        L_0x003f:
            a.f r0 = r6.f32a
            long r0 = r0.n()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: a.v.n():long");
    }

    public long o() {
        a(1);
        int i = 0;
        while (true) {
            if (!b((long) (i + 1))) {
                break;
            }
            byte b2 = this.f32a.b((long) i);
            if ((b2 >= 48 && b2 <= 57) || ((b2 >= 97 && b2 <= 102) || (b2 >= 65 && b2 <= 70))) {
                i++;
            } else if (i == 0) {
                throw new NumberFormatException(String.format("Expected leading [0-9a-fA-F] character but was %#x", Byte.valueOf(b2)));
            }
        }
        return this.f32a.o();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public String r() {
        long a2 = a((byte) 10);
        if (a2 != -1) {
            return this.f32a.e(a2);
        }
        f fVar = new f();
        this.f32a.a(fVar, 0, Math.min(32L, this.f32a.b()));
        throw new EOFException("\\n not found: size=" + this.f32a.b() + " content=" + fVar.p().d() + "...");
    }

    public byte[] s() {
        this.f32a.a(this.f33b);
        return this.f32a.s();
    }

    public String toString() {
        return "buffer(" + this.f33b + ")";
    }
}
