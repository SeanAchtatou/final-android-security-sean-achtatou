package a;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Logger;

public final class q {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final Logger f24a = Logger.getLogger(q.class.getName());

    private q() {
    }

    public static aa a(OutputStream outputStream) {
        return a(outputStream, new ac());
    }

    private static aa a(OutputStream outputStream, ac acVar) {
        if (outputStream == null) {
            throw new IllegalArgumentException("out == null");
        } else if (acVar != null) {
            return new r(acVar, outputStream);
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    public static aa a(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException("socket == null");
        }
        a c = c(socket);
        return c.a(a(socket.getOutputStream(), c));
    }

    public static ab a(File file) {
        if (file != null) {
            return a(new FileInputStream(file));
        }
        throw new IllegalArgumentException("file == null");
    }

    public static ab a(InputStream inputStream) {
        return a(inputStream, new ac());
    }

    private static ab a(InputStream inputStream, ac acVar) {
        if (inputStream == null) {
            throw new IllegalArgumentException("in == null");
        } else if (acVar != null) {
            return new s(acVar, inputStream);
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    public static h a(aa aaVar) {
        if (aaVar != null) {
            return new u(aaVar);
        }
        throw new IllegalArgumentException("sink == null");
    }

    public static i a(ab abVar) {
        if (abVar != null) {
            return new v(abVar);
        }
        throw new IllegalArgumentException("source == null");
    }

    public static aa b(File file) {
        if (file != null) {
            return a(new FileOutputStream(file));
        }
        throw new IllegalArgumentException("file == null");
    }

    public static ab b(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException("socket == null");
        }
        a c = c(socket);
        return c.a(a(socket.getInputStream(), c));
    }

    private static a c(Socket socket) {
        return new t(socket);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public static aa c(File file) {
        if (file != null) {
            return a(new FileOutputStream(file, true));
        }
        throw new IllegalArgumentException("file == null");
    }
}
