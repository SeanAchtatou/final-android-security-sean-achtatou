package a;

import android.support.v4.media.session.PlaybackStateCompat;
import com.qihoo.dynamic.util.Md5Util;
import java.io.EOFException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class f implements h, i, Cloneable {
    private static final byte[] c = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};

    /* renamed from: a  reason: collision with root package name */
    x f10a;

    /* renamed from: b  reason: collision with root package name */
    long f11b;

    public int a(byte[] bArr, int i, int i2) {
        ae.a((long) bArr.length, (long) i, (long) i2);
        x xVar = this.f10a;
        if (xVar == null) {
            return -1;
        }
        int min = Math.min(i2, xVar.c - xVar.f36b);
        System.arraycopy(xVar.f35a, xVar.f36b, bArr, i, min);
        xVar.f36b += min;
        this.f11b -= (long) min;
        if (xVar.f36b != xVar.c) {
            return min;
        }
        this.f10a = xVar.a();
        y.a(xVar);
        return min;
    }

    public long a(byte b2) {
        return a(b2, 0);
    }

    public long a(byte b2, long j) {
        if (j < 0) {
            throw new IllegalArgumentException("fromIndex < 0");
        }
        x xVar = this.f10a;
        if (xVar == null) {
            return -1;
        }
        long j2 = 0;
        do {
            int i = xVar.c - xVar.f36b;
            if (j >= ((long) i)) {
                j -= (long) i;
            } else {
                byte[] bArr = xVar.f35a;
                long j3 = (long) xVar.c;
                for (long j4 = ((long) xVar.f36b) + j; j4 < j3; j4++) {
                    if (bArr[(int) j4] == b2) {
                        return (j2 + j4) - ((long) xVar.f36b);
                    }
                }
                j = 0;
            }
            j2 += (long) i;
            xVar = xVar.f;
        } while (xVar != this.f10a);
        return -1;
    }

    public long a(ab abVar) {
        if (abVar == null) {
            throw new IllegalArgumentException("source == null");
        }
        long j = 0;
        while (true) {
            long a2 = abVar.a(this, PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH);
            if (a2 == -1) {
                return j;
            }
            j += a2;
        }
    }

    public long a(f fVar, long j) {
        if (fVar == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f11b == 0) {
            return -1;
        } else {
            if (j > this.f11b) {
                j = this.f11b;
            }
            fVar.a_(this, j);
            return j;
        }
    }

    public ac a() {
        return ac.f1b;
    }

    public f a(int i) {
        if (i < 128) {
            i(i);
        } else if (i < 2048) {
            i((i >> 6) | 192);
            i((i & 63) | 128);
        } else if (i < 65536) {
            if (i < 55296 || i > 57343) {
                i((i >> 12) | 224);
                i(((i >> 6) & 63) | 128);
                i((i & 63) | 128);
            } else {
                throw new IllegalArgumentException("Unexpected code point: " + Integer.toHexString(i));
            }
        } else if (i <= 1114111) {
            i((i >> 18) | 240);
            i(((i >> 12) & 63) | 128);
            i(((i >> 6) & 63) | 128);
            i((i & 63) | 128);
        } else {
            throw new IllegalArgumentException("Unexpected code point: " + Integer.toHexString(i));
        }
        return this;
    }

    public f a(f fVar, long j, long j2) {
        if (fVar == null) {
            throw new IllegalArgumentException("out == null");
        }
        ae.a(this.f11b, j, j2);
        if (j2 != 0) {
            fVar.f11b += j2;
            x xVar = this.f10a;
            while (j >= ((long) (xVar.c - xVar.f36b))) {
                j -= (long) (xVar.c - xVar.f36b);
                xVar = xVar.f;
            }
            while (j2 > 0) {
                x xVar2 = new x(xVar);
                xVar2.f36b = (int) (((long) xVar2.f36b) + j);
                xVar2.c = Math.min(xVar2.f36b + ((int) j2), xVar2.c);
                if (fVar.f10a == null) {
                    xVar2.g = xVar2;
                    xVar2.f = xVar2;
                    fVar.f10a = xVar2;
                } else {
                    fVar.f10a.g.a(xVar2);
                }
                j2 -= (long) (xVar2.c - xVar2.f36b);
                xVar = xVar.f;
                j = 0;
            }
        }
        return this;
    }

    /* renamed from: a */
    public f b(j jVar) {
        if (jVar == null) {
            throw new IllegalArgumentException("byteString == null");
        }
        jVar.a(this);
        return this;
    }

    /* renamed from: a */
    public f b(String str) {
        return a(str, 0, str.length());
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 152 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public a.f a(java.lang.String r10, int r11, int r12) {
        /*
            r9 = this;
            r8 = 57343(0xdfff, float:8.0355E-41)
            r7 = 128(0x80, float:1.794E-43)
            if (r10 != 0) goto L_0x000f
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "string == null"
            r0.<init>(r1)
            throw r0
        L_0x000f:
            if (r11 >= 0) goto L_0x002a
            java.lang.IllegalAccessError r0 = new java.lang.IllegalAccessError
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "beginIndex < 0: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x002a:
            if (r12 >= r11) goto L_0x004f
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "endIndex < beginIndex: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r12)
            java.lang.String r2 = " < "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x004f:
            int r0 = r10.length()
            if (r12 <= r0) goto L_0x0090
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "endIndex > string.length: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r12)
            java.lang.String r2 = " > "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r10.length()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x007c:
            r0 = 0
        L_0x007d:
            r2 = 56319(0xdbff, float:7.892E-41)
            if (r1 > r2) goto L_0x0089
            r2 = 56320(0xdc00, float:7.8921E-41)
            if (r0 < r2) goto L_0x0089
            if (r0 <= r8) goto L_0x0114
        L_0x0089:
            r0 = 63
            r9.i(r0)
            int r11 = r11 + 1
        L_0x0090:
            if (r11 >= r12) goto L_0x0145
            char r1 = r10.charAt(r11)
            if (r1 >= r7) goto L_0x00d2
            r0 = 1
            a.x r2 = r9.e(r0)
            byte[] r3 = r2.f35a
            int r0 = r2.c
            int r4 = r0 - r11
            int r0 = 2048 - r4
            int r5 = java.lang.Math.min(r12, r0)
            int r0 = r11 + 1
            int r6 = r4 + r11
            byte r1 = (byte) r1
            r3[r6] = r1
        L_0x00b0:
            if (r0 >= r5) goto L_0x00b8
            char r6 = r10.charAt(r0)
            if (r6 < r7) goto L_0x00ca
        L_0x00b8:
            int r1 = r0 + r4
            int r3 = r2.c
            int r1 = r1 - r3
            int r3 = r2.c
            int r3 = r3 + r1
            r2.c = r3
            long r2 = r9.f11b
            long r4 = (long) r1
            long r2 = r2 + r4
            r9.f11b = r2
        L_0x00c8:
            r11 = r0
            goto L_0x0090
        L_0x00ca:
            int r1 = r0 + 1
            int r0 = r0 + r4
            byte r6 = (byte) r6
            r3[r0] = r6
            r0 = r1
            goto L_0x00b0
        L_0x00d2:
            r0 = 2048(0x800, float:2.87E-42)
            if (r1 >= r0) goto L_0x00e7
            int r0 = r1 >> 6
            r0 = r0 | 192(0xc0, float:2.69E-43)
            r9.i(r0)
            r0 = r1 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            r9.i(r0)
            int r0 = r11 + 1
            goto L_0x00c8
        L_0x00e7:
            r0 = 55296(0xd800, float:7.7486E-41)
            if (r1 < r0) goto L_0x00ee
            if (r1 <= r8) goto L_0x0108
        L_0x00ee:
            int r0 = r1 >> 12
            r0 = r0 | 224(0xe0, float:3.14E-43)
            r9.i(r0)
            int r0 = r1 >> 6
            r0 = r0 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            r9.i(r0)
            r0 = r1 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            r9.i(r0)
            int r0 = r11 + 1
            goto L_0x00c8
        L_0x0108:
            int r0 = r11 + 1
            if (r0 >= r12) goto L_0x007c
            int r0 = r11 + 1
            char r0 = r10.charAt(r0)
            goto L_0x007d
        L_0x0114:
            r2 = 65536(0x10000, float:9.18355E-41)
            r3 = -55297(0xffffffffffff27ff, float:NaN)
            r1 = r1 & r3
            int r1 = r1 << 10
            r3 = -56321(0xffffffffffff23ff, float:NaN)
            r0 = r0 & r3
            r0 = r0 | r1
            int r0 = r0 + r2
            int r1 = r0 >> 18
            r1 = r1 | 240(0xf0, float:3.36E-43)
            r9.i(r1)
            int r1 = r0 >> 12
            r1 = r1 & 63
            r1 = r1 | 128(0x80, float:1.794E-43)
            r9.i(r1)
            int r1 = r0 >> 6
            r1 = r1 & 63
            r1 = r1 | 128(0x80, float:1.794E-43)
            r9.i(r1)
            r0 = r0 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            r9.i(r0)
            int r0 = r11 + 2
            goto L_0x00c8
        L_0x0145:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: a.f.a(java.lang.String, int, int):a.f");
    }

    public String a(long j, Charset charset) {
        ae.a(this.f11b, 0, j);
        if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        } else if (j > 2147483647L) {
            throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + j);
        } else if (j == 0) {
            return "";
        } else {
            x xVar = this.f10a;
            if (((long) xVar.f36b) + j > ((long) xVar.c)) {
                return new String(f(j), charset);
            }
            String str = new String(xVar.f35a, xVar.f36b, (int) j, charset);
            xVar.f36b = (int) (((long) xVar.f36b) + j);
            this.f11b -= j;
            if (xVar.f36b != xVar.c) {
                return str;
            }
            this.f10a = xVar.a();
            y.a(xVar);
            return str;
        }
    }

    public void a(long j) {
        if (this.f11b < j) {
            throw new EOFException();
        }
    }

    public void a(byte[] bArr) {
        int i = 0;
        while (i < bArr.length) {
            int a2 = a(bArr, i, bArr.length - i);
            if (a2 == -1) {
                throw new EOFException();
            }
            i += a2;
        }
    }

    public void a_(f fVar, long j) {
        if (fVar == null) {
            throw new IllegalArgumentException("source == null");
        } else if (fVar == this) {
            throw new IllegalArgumentException("source == this");
        } else {
            ae.a(fVar.f11b, 0, j);
            while (j > 0) {
                if (j < ((long) (fVar.f10a.c - fVar.f10a.f36b))) {
                    x xVar = this.f10a != null ? this.f10a.g : null;
                    if (xVar != null && xVar.e) {
                        if ((((long) xVar.c) + j) - ((long) (xVar.d ? 0 : xVar.f36b)) <= PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH) {
                            fVar.f10a.a(xVar, (int) j);
                            fVar.f11b -= j;
                            this.f11b += j;
                            return;
                        }
                    }
                    fVar.f10a = fVar.f10a.a((int) j);
                }
                x xVar2 = fVar.f10a;
                long j2 = (long) (xVar2.c - xVar2.f36b);
                fVar.f10a = xVar2.a();
                if (this.f10a == null) {
                    this.f10a = xVar2;
                    x xVar3 = this.f10a;
                    x xVar4 = this.f10a;
                    x xVar5 = this.f10a;
                    xVar4.g = xVar5;
                    xVar3.f = xVar5;
                } else {
                    this.f10a.g.a(xVar2).b();
                }
                fVar.f11b -= j2;
                this.f11b += j2;
                j -= j2;
            }
        }
    }

    public byte b(long j) {
        ae.a(this.f11b, j, 1);
        x xVar = this.f10a;
        while (true) {
            int i = xVar.c - xVar.f36b;
            if (j < ((long) i)) {
                return xVar.f35a[xVar.f36b + ((int) j)];
            }
            j -= (long) i;
            xVar = xVar.f;
        }
    }

    public long b() {
        return this.f11b;
    }

    /* renamed from: b */
    public f i(int i) {
        x e = e(1);
        byte[] bArr = e.f35a;
        int i2 = e.c;
        e.c = i2 + 1;
        bArr[i2] = (byte) i;
        this.f11b++;
        return this;
    }

    /* renamed from: b */
    public f c(byte[] bArr) {
        if (bArr != null) {
            return b(bArr, 0, bArr.length);
        }
        throw new IllegalArgumentException("source == null");
    }

    public f b(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new IllegalArgumentException("source == null");
        }
        ae.a((long) bArr.length, (long) i, (long) i2);
        int i3 = i + i2;
        while (i < i3) {
            x e = e(1);
            int min = Math.min(i3 - i, 2048 - e.c);
            System.arraycopy(bArr, i, e.f35a, e.c, min);
            i += min;
            e.c = min + e.c;
        }
        this.f11b += (long) i2;
        return this;
    }

    public f c() {
        return this;
    }

    /* renamed from: c */
    public f h(int i) {
        x e = e(2);
        byte[] bArr = e.f35a;
        int i2 = e.c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 8) & 255);
        bArr[i3] = (byte) (i & 255);
        e.c = i3 + 1;
        this.f11b += 2;
        return this;
    }

    public j c(long j) {
        return new j(f(j));
    }

    public void close() {
    }

    /* renamed from: d */
    public f w() {
        return this;
    }

    /* renamed from: d */
    public f g(int i) {
        x e = e(4);
        byte[] bArr = e.f35a;
        int i2 = e.c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 24) & 255);
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((i >>> 16) & 255);
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((i >>> 8) & 255);
        bArr[i5] = (byte) (i & 255);
        e.c = i5 + 1;
        this.f11b += 4;
        return this;
    }

    public String d(long j) {
        return a(j, ae.f3a);
    }

    public h e() {
        return this;
    }

    /* access modifiers changed from: package-private */
    public x e(int i) {
        if (i < 1 || i > 2048) {
            throw new IllegalArgumentException();
        } else if (this.f10a == null) {
            this.f10a = y.a();
            x xVar = this.f10a;
            x xVar2 = this.f10a;
            x xVar3 = this.f10a;
            xVar2.g = xVar3;
            xVar.f = xVar3;
            return xVar3;
        } else {
            x xVar4 = this.f10a.g;
            return (xVar4.c + i > 2048 || !xVar4.e) ? xVar4.a(y.a()) : xVar4;
        }
    }

    /* access modifiers changed from: package-private */
    public String e(long j) {
        if (j <= 0 || b(j - 1) != 13) {
            String d = d(j);
            g(1L);
            return d;
        }
        String d2 = d(j - 1);
        g(2L);
        return d2;
    }

    public boolean equals(Object obj) {
        long j = 0;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        if (this.f11b != fVar.f11b) {
            return false;
        }
        if (this.f11b == 0) {
            return true;
        }
        x xVar = this.f10a;
        x xVar2 = fVar.f10a;
        int i = xVar.f36b;
        int i2 = xVar2.f36b;
        while (j < this.f11b) {
            long min = (long) Math.min(xVar.c - i, xVar2.c - i2);
            int i3 = 0;
            while (((long) i3) < min) {
                int i4 = i + 1;
                byte b2 = xVar.f35a[i];
                int i5 = i2 + 1;
                if (b2 != xVar2.f35a[i2]) {
                    return false;
                }
                i3++;
                i2 = i5;
                i = i4;
            }
            if (i == xVar.c) {
                xVar = xVar.f;
                i = xVar.f36b;
            }
            if (i2 == xVar2.c) {
                xVar2 = xVar2.f;
                i2 = xVar2.f36b;
            }
            j += min;
        }
        return true;
    }

    public j f(int i) {
        return i == 0 ? j.f14b : new z(this, i);
    }

    public boolean f() {
        return this.f11b == 0;
    }

    public byte[] f(long j) {
        ae.a(this.f11b, 0, j);
        if (j > 2147483647L) {
            throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + j);
        }
        byte[] bArr = new byte[((int) j)];
        a(bArr);
        return bArr;
    }

    public void flush() {
    }

    public InputStream g() {
        return new g(this);
    }

    public void g(long j) {
        while (j > 0) {
            if (this.f10a == null) {
                throw new EOFException();
            }
            int min = (int) Math.min(j, (long) (this.f10a.c - this.f10a.f36b));
            this.f11b -= (long) min;
            j -= (long) min;
            x xVar = this.f10a;
            xVar.f36b = min + xVar.f36b;
            if (this.f10a.f36b == this.f10a.c) {
                x xVar2 = this.f10a;
                this.f10a = xVar2.a();
                y.a(xVar2);
            }
        }
    }

    public long h() {
        long j = this.f11b;
        if (j == 0) {
            return 0;
        }
        x xVar = this.f10a.g;
        return (xVar.c >= 2048 || !xVar.e) ? j : j - ((long) (xVar.c - xVar.f36b));
    }

    /* renamed from: h */
    public f k(long j) {
        boolean z;
        long j2;
        if (j == 0) {
            return i(48);
        }
        if (j < 0) {
            j2 = -j;
            if (j2 < 0) {
                return b("-9223372036854775808");
            }
            z = true;
        } else {
            z = false;
            j2 = j;
        }
        int i = j2 < 100000000 ? j2 < 10000 ? j2 < 100 ? j2 < 10 ? 1 : 2 : j2 < 1000 ? 3 : 4 : j2 < 1000000 ? j2 < 100000 ? 5 : 6 : j2 < 10000000 ? 7 : 8 : j2 < 1000000000000L ? j2 < 10000000000L ? j2 < 1000000000 ? 9 : 10 : j2 < 100000000000L ? 11 : 12 : j2 < 1000000000000000L ? j2 < 10000000000000L ? 13 : j2 < 100000000000000L ? 14 : 15 : j2 < 100000000000000000L ? j2 < 10000000000000000L ? 16 : 17 : j2 < 1000000000000000000L ? 18 : 19;
        if (z) {
            i++;
        }
        x e = e(i);
        byte[] bArr = e.f35a;
        int i2 = e.c + i;
        while (j2 != 0) {
            i2--;
            bArr[i2] = c[(int) (j2 % 10)];
            j2 /= 10;
        }
        if (z) {
            bArr[i2 - 1] = 45;
        }
        e.c += i;
        this.f11b = ((long) i) + this.f11b;
        return this;
    }

    public int hashCode() {
        x xVar = this.f10a;
        if (xVar == null) {
            return 0;
        }
        int i = 1;
        do {
            int i2 = xVar.f36b;
            int i3 = xVar.c;
            while (i2 < i3) {
                i2++;
                i = xVar.f35a[i2] + (i * 31);
            }
            xVar = xVar.f;
        } while (xVar != this.f10a);
        return i;
    }

    public byte i() {
        if (this.f11b == 0) {
            throw new IllegalStateException("size == 0");
        }
        x xVar = this.f10a;
        int i = xVar.f36b;
        int i2 = xVar.c;
        int i3 = i + 1;
        byte b2 = xVar.f35a[i];
        this.f11b--;
        if (i3 == i2) {
            this.f10a = xVar.a();
            y.a(xVar);
        } else {
            xVar.f36b = i3;
        }
        return b2;
    }

    /* renamed from: i */
    public f j(long j) {
        if (j == 0) {
            return i(48);
        }
        int numberOfTrailingZeros = (Long.numberOfTrailingZeros(Long.highestOneBit(j)) / 4) + 1;
        x e = e(numberOfTrailingZeros);
        byte[] bArr = e.f35a;
        int i = e.c;
        for (int i2 = (e.c + numberOfTrailingZeros) - 1; i2 >= i; i2--) {
            bArr[i2] = c[(int) (15 & j)];
            j >>>= 4;
        }
        e.c += numberOfTrailingZeros;
        this.f11b = ((long) numberOfTrailingZeros) + this.f11b;
        return this;
    }

    public short j() {
        if (this.f11b < 2) {
            throw new IllegalStateException("size < 2: " + this.f11b);
        }
        x xVar = this.f10a;
        int i = xVar.f36b;
        int i2 = xVar.c;
        if (i2 - i < 2) {
            return (short) (((i() & 255) << 8) | (i() & 255));
        }
        byte[] bArr = xVar.f35a;
        int i3 = i + 1;
        int i4 = i3 + 1;
        byte b2 = ((bArr[i] & 255) << 8) | (bArr[i3] & 255);
        this.f11b -= 2;
        if (i4 == i2) {
            this.f10a = xVar.a();
            y.a(xVar);
        } else {
            xVar.f36b = i4;
        }
        return (short) b2;
    }

    public int k() {
        if (this.f11b < 4) {
            throw new IllegalStateException("size < 4: " + this.f11b);
        }
        x xVar = this.f10a;
        int i = xVar.f36b;
        int i2 = xVar.c;
        if (i2 - i < 4) {
            return ((i() & 255) << 24) | ((i() & 255) << 16) | ((i() & 255) << 8) | (i() & 255);
        }
        byte[] bArr = xVar.f35a;
        int i3 = i + 1;
        int i4 = i3 + 1;
        byte b2 = ((bArr[i] & 255) << 24) | ((bArr[i3] & 255) << 16);
        int i5 = i4 + 1;
        byte b3 = b2 | ((bArr[i4] & 255) << 8);
        int i6 = i5 + 1;
        byte b4 = b3 | (bArr[i5] & 255);
        this.f11b -= 4;
        if (i6 == i2) {
            this.f10a = xVar.a();
            y.a(xVar);
            return b4;
        }
        xVar.f36b = i6;
        return b4;
    }

    public short l() {
        return ae.a(j());
    }

    public int m() {
        return ae.a(k());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00b8, code lost:
        if (r7 != r14) goto L_0x00da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00ba, code lost:
        r0.f10a = r12.a();
        a.y.a(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c5, code lost:
        if (r4 != false) goto L_0x00cd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00da, code lost:
        r12.f36b = r7;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long n() {
        /*
            r20 = this;
            r0 = r20
            long r2 = r0.f11b
            r4 = 0
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 != 0) goto L_0x0012
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r3 = "size == 0"
            r2.<init>(r3)
            throw r2
        L_0x0012:
            r8 = 0
            r6 = 0
            r5 = 0
            r4 = 0
            r10 = -922337203685477580(0xf333333333333334, double:-8.390303882365713E246)
            r2 = -7
        L_0x001e:
            r0 = r20
            a.x r12 = r0.f10a
            byte[] r13 = r12.f35a
            int r7 = r12.f36b
            int r14 = r12.c
        L_0x0028:
            if (r7 >= r14) goto L_0x00b8
            byte r15 = r13[r7]
            r16 = 48
            r0 = r16
            if (r15 < r0) goto L_0x008a
            r16 = 57
            r0 = r16
            if (r15 > r0) goto L_0x008a
            int r16 = 48 - r15
            int r17 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r17 < 0) goto L_0x004b
            int r17 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r17 != 0) goto L_0x007a
            r0 = r16
            long r0 = (long) r0
            r18 = r0
            int r17 = (r18 > r2 ? 1 : (r18 == r2 ? 0 : -1))
            if (r17 >= 0) goto L_0x007a
        L_0x004b:
            a.f r2 = new a.f
            r2.<init>()
            a.f r2 = r2.k(r8)
            a.f r2 = r2.i(r15)
            if (r5 != 0) goto L_0x005d
            r2.i()
        L_0x005d:
            java.lang.NumberFormatException r3 = new java.lang.NumberFormatException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Number too large: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r2 = r2.q()
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r2 = r2.toString()
            r3.<init>(r2)
            throw r3
        L_0x007a:
            r18 = 10
            long r8 = r8 * r18
            r0 = r16
            long r0 = (long) r0
            r16 = r0
            long r8 = r8 + r16
        L_0x0085:
            int r7 = r7 + 1
            int r6 = r6 + 1
            goto L_0x0028
        L_0x008a:
            r16 = 45
            r0 = r16
            if (r15 != r0) goto L_0x0098
            if (r6 != 0) goto L_0x0098
            r5 = 1
            r16 = 1
            long r2 = r2 - r16
            goto L_0x0085
        L_0x0098:
            if (r6 != 0) goto L_0x00b7
            java.lang.NumberFormatException r2 = new java.lang.NumberFormatException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Expected leading [0-9] or '-' character but was 0x"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = java.lang.Integer.toHexString(r15)
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.<init>(r3)
            throw r2
        L_0x00b7:
            r4 = 1
        L_0x00b8:
            if (r7 != r14) goto L_0x00da
            a.x r7 = r12.a()
            r0 = r20
            r0.f10a = r7
            a.y.a(r12)
        L_0x00c5:
            if (r4 != 0) goto L_0x00cd
            r0 = r20
            a.x r7 = r0.f10a
            if (r7 != 0) goto L_0x001e
        L_0x00cd:
            r0 = r20
            long r2 = r0.f11b
            long r6 = (long) r6
            long r2 = r2 - r6
            r0 = r20
            r0.f11b = r2
            if (r5 == 0) goto L_0x00dd
        L_0x00d9:
            return r8
        L_0x00da:
            r12.f36b = r7
            goto L_0x00c5
        L_0x00dd:
            long r8 = -r8
            goto L_0x00d9
        */
        throw new UnsupportedOperationException("Method not decompiled: a.f.n():long");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x009c, code lost:
        if (r7 != r12) goto L_0x00c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x009e, code lost:
        r0.f10a = r10.a();
        a.y.a(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00a9, code lost:
        if (r2 != false) goto L_0x00b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00c9, code lost:
        r10.f36b = r7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x007e A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long o() {
        /*
            r18 = this;
            r0 = r18
            long r2 = r0.f11b
            r4 = 0
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 != 0) goto L_0x0012
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r3 = "size == 0"
            r2.<init>(r3)
            throw r2
        L_0x0012:
            r4 = 0
            r3 = 0
            r2 = 0
        L_0x0016:
            r0 = r18
            a.x r10 = r0.f10a
            byte[] r11 = r10.f35a
            int r6 = r10.f36b
            int r12 = r10.c
            r7 = r6
        L_0x0021:
            if (r7 >= r12) goto L_0x009c
            byte r8 = r11[r7]
            r6 = 48
            if (r8 < r6) goto L_0x0062
            r6 = 57
            if (r8 > r6) goto L_0x0062
            int r6 = r8 + -48
        L_0x002f:
            r14 = -1152921504606846976(0xf000000000000000, double:-3.105036184601418E231)
            long r14 = r14 & r4
            r16 = 0
            int r9 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r9 == 0) goto L_0x00bd
            a.f r2 = new a.f
            r2.<init>()
            a.f r2 = r2.j(r4)
            a.f r2 = r2.i(r8)
            java.lang.NumberFormatException r3 = new java.lang.NumberFormatException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Number too large: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r2 = r2.q()
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r2 = r2.toString()
            r3.<init>(r2)
            throw r3
        L_0x0062:
            r6 = 97
            if (r8 < r6) goto L_0x006f
            r6 = 102(0x66, float:1.43E-43)
            if (r8 > r6) goto L_0x006f
            int r6 = r8 + -97
            int r6 = r6 + 10
            goto L_0x002f
        L_0x006f:
            r6 = 65
            if (r8 < r6) goto L_0x007c
            r6 = 70
            if (r8 > r6) goto L_0x007c
            int r6 = r8 + -65
            int r6 = r6 + 10
            goto L_0x002f
        L_0x007c:
            if (r3 != 0) goto L_0x009b
            java.lang.NumberFormatException r2 = new java.lang.NumberFormatException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Expected leading [0-9a-fA-F] character but was 0x"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = java.lang.Integer.toHexString(r8)
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.<init>(r3)
            throw r2
        L_0x009b:
            r2 = 1
        L_0x009c:
            if (r7 != r12) goto L_0x00c9
            a.x r6 = r10.a()
            r0 = r18
            r0.f10a = r6
            a.y.a(r10)
        L_0x00a9:
            if (r2 != 0) goto L_0x00b1
            r0 = r18
            a.x r6 = r0.f10a
            if (r6 != 0) goto L_0x0016
        L_0x00b1:
            r0 = r18
            long r6 = r0.f11b
            long r2 = (long) r3
            long r2 = r6 - r2
            r0 = r18
            r0.f11b = r2
            return r4
        L_0x00bd:
            r8 = 4
            long r4 = r4 << r8
            long r8 = (long) r6
            long r8 = r8 | r4
            int r4 = r7 + 1
            int r3 = r3 + 1
            r7 = r4
            r4 = r8
            goto L_0x0021
        L_0x00c9:
            r10.f36b = r7
            goto L_0x00a9
        */
        throw new UnsupportedOperationException("Method not decompiled: a.f.o():long");
    }

    public j p() {
        return new j(s());
    }

    public String q() {
        try {
            return a(this.f11b, ae.f3a);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public String r() {
        long a2 = a((byte) 10);
        if (a2 != -1) {
            return e(a2);
        }
        f fVar = new f();
        a(fVar, 0, Math.min(32L, this.f11b));
        throw new EOFException("\\n not found: size=" + b() + " content=" + fVar.p().d() + "...");
    }

    public byte[] s() {
        try {
            return f(this.f11b);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    public void t() {
        try {
            g(this.f11b);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    public String toString() {
        if (this.f11b == 0) {
            return "Buffer[size=0]";
        }
        if (this.f11b <= 16) {
            return String.format("Buffer[size=%s data=%s]", Long.valueOf(this.f11b), clone().p().d());
        }
        try {
            MessageDigest instance = MessageDigest.getInstance(Md5Util.ALGORITHM);
            instance.update(this.f10a.f35a, this.f10a.f36b, this.f10a.c - this.f10a.f36b);
            for (x xVar = this.f10a.f; xVar != this.f10a; xVar = xVar.f) {
                instance.update(xVar.f35a, xVar.f36b, xVar.c - xVar.f36b);
            }
            return String.format("Buffer[size=%s md5=%s]", Long.valueOf(this.f11b), j.a(instance.digest()).d());
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError();
        }
    }

    /* renamed from: u */
    public f clone() {
        f fVar = new f();
        if (this.f11b == 0) {
            return fVar;
        }
        fVar.f10a = new x(this.f10a);
        x xVar = fVar.f10a;
        x xVar2 = fVar.f10a;
        x xVar3 = fVar.f10a;
        xVar2.g = xVar3;
        xVar.f = xVar3;
        for (x xVar4 = this.f10a.f; xVar4 != this.f10a; xVar4 = xVar4.f) {
            fVar.f10a.g.a(new x(xVar4));
        }
        fVar.f11b = this.f11b;
        return fVar;
    }

    public j v() {
        if (this.f11b <= 2147483647L) {
            return f((int) this.f11b);
        }
        throw new IllegalArgumentException("size > Integer.MAX_VALUE: " + this.f11b);
    }
}
