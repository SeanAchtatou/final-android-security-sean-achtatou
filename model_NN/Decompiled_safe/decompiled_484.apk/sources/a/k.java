package a;

import java.util.zip.Deflater;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;

public final class k implements aa {

    /* renamed from: a  reason: collision with root package name */
    private final h f15a;

    /* renamed from: b  reason: collision with root package name */
    private final Deflater f16b;
    private boolean c;

    public k(aa aaVar, Deflater deflater) {
        this(q.a(aaVar), deflater);
    }

    k(h hVar, Deflater deflater) {
        if (hVar == null) {
            throw new IllegalArgumentException("source == null");
        } else if (deflater == null) {
            throw new IllegalArgumentException("inflater == null");
        } else {
            this.f15a = hVar;
            this.f16b = deflater;
        }
    }

    @IgnoreJRERequirement
    private void a(boolean z) {
        x e;
        f c2 = this.f15a.c();
        while (true) {
            e = c2.e(1);
            int deflate = z ? this.f16b.deflate(e.f35a, e.c, 2048 - e.c, 2) : this.f16b.deflate(e.f35a, e.c, 2048 - e.c);
            if (deflate > 0) {
                e.c += deflate;
                c2.f11b += (long) deflate;
                this.f15a.w();
            } else if (this.f16b.needsInput()) {
                break;
            }
        }
        if (e.f36b == e.c) {
            c2.f10a = e.a();
            y.a(e);
        }
    }

    public ac a() {
        return this.f15a.a();
    }

    public void a_(f fVar, long j) {
        ae.a(fVar.f11b, 0, j);
        while (j > 0) {
            x xVar = fVar.f10a;
            int min = (int) Math.min(j, (long) (xVar.c - xVar.f36b));
            this.f16b.setInput(xVar.f35a, xVar.f36b, min);
            a(false);
            fVar.f11b -= (long) min;
            xVar.f36b += min;
            if (xVar.f36b == xVar.c) {
                fVar.f10a = xVar.a();
                y.a(xVar);
            }
            j -= (long) min;
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.f16b.finish();
        a(false);
    }

    public void close() {
        if (!this.c) {
            Throwable th = null;
            try {
                b();
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.f16b.end();
                th = th;
            } catch (Throwable th3) {
                th = th3;
                if (th != null) {
                    th = th;
                }
            }
            try {
                this.f15a.close();
            } catch (Throwable th4) {
                if (th == null) {
                    th = th4;
                }
            }
            this.c = true;
            if (th != null) {
                ae.a(th);
            }
        }
    }

    public void flush() {
        a(true);
        this.f15a.flush();
    }

    public String toString() {
        return "DeflaterSink(" + this.f15a + ")";
    }
}
