package a;

import java.io.InputStream;

class g extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f12a;

    g(f fVar) {
        this.f12a = fVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public int available() {
        return (int) Math.min(this.f12a.f11b, 2147483647L);
    }

    public void close() {
    }

    public int read() {
        if (this.f12a.f11b > 0) {
            return this.f12a.i() & 255;
        }
        return -1;
    }

    public int read(byte[] bArr, int i, int i2) {
        return this.f12a.a(bArr, i, i2);
    }

    public String toString() {
        return this.f12a + ".inputStream()";
    }
}
