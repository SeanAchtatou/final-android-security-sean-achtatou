package a;

import java.util.concurrent.TimeUnit;

public class n extends ac {

    /* renamed from: a  reason: collision with root package name */
    private ac f19a;

    public n(ac acVar) {
        if (acVar == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f19a = acVar;
    }

    public final ac a() {
        return this.f19a;
    }

    public ac a(long j) {
        return this.f19a.a(j);
    }

    public ac a(long j, TimeUnit timeUnit) {
        return this.f19a.a(j, timeUnit);
    }

    public final n a(ac acVar) {
        if (acVar == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f19a = acVar;
        return this;
    }

    public long b_() {
        return this.f19a.b_();
    }

    public boolean c_() {
        return this.f19a.c_();
    }

    public long d() {
        return this.f19a.d();
    }

    public ac d_() {
        return this.f19a.d_();
    }

    public ac f() {
        return this.f19a.f();
    }

    public void g() {
        this.f19a.g();
    }
}
