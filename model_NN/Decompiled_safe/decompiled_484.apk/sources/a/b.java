package a;

import java.io.IOException;

class b implements aa {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ aa f4a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ a f5b;

    b(a aVar, aa aaVar) {
        this.f5b = aVar;
        this.f4a = aaVar;
    }

    public ac a() {
        return this.f5b;
    }

    public void a_(f fVar, long j) {
        this.f5b.c();
        try {
            this.f4a.a_(fVar, j);
            this.f5b.a(true);
        } catch (IOException e) {
            throw this.f5b.a(e);
        } catch (Throwable th) {
            this.f5b.a(false);
            throw th;
        }
    }

    public void close() {
        this.f5b.c();
        try {
            this.f4a.close();
            this.f5b.a(true);
        } catch (IOException e) {
            throw this.f5b.a(e);
        } catch (Throwable th) {
            this.f5b.a(false);
            throw th;
        }
    }

    public void flush() {
        this.f5b.c();
        try {
            this.f4a.flush();
            this.f5b.a(true);
        } catch (IOException e) {
            throw this.f5b.a(e);
        } catch (Throwable th) {
            this.f5b.a(false);
            throw th;
        }
    }

    public String toString() {
        return "AsyncTimeout.sink(" + this.f4a + ")";
    }
}
