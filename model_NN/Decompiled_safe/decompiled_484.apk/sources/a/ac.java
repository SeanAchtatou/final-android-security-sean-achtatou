package a;

import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;

public class ac {

    /* renamed from: b  reason: collision with root package name */
    public static final ac f1b = new ad();

    /* renamed from: a  reason: collision with root package name */
    private boolean f2a;
    private long c;
    private long d;

    public ac a(long j) {
        this.f2a = true;
        this.c = j;
        return this;
    }

    public ac a(long j, TimeUnit timeUnit) {
        if (j < 0) {
            throw new IllegalArgumentException("timeout < 0: " + j);
        } else if (timeUnit == null) {
            throw new IllegalArgumentException("unit == null");
        } else {
            this.d = timeUnit.toNanos(j);
            return this;
        }
    }

    public long b_() {
        return this.d;
    }

    public boolean c_() {
        return this.f2a;
    }

    public long d() {
        if (this.f2a) {
            return this.c;
        }
        throw new IllegalStateException("No deadline");
    }

    public ac d_() {
        this.d = 0;
        return this;
    }

    public ac f() {
        this.f2a = false;
        return this;
    }

    public void g() {
        if (Thread.interrupted()) {
            throw new InterruptedIOException("thread interrupted");
        } else if (this.f2a && this.c - System.nanoTime() <= 0) {
            throw new InterruptedIOException("deadline reached");
        }
    }
}
