package a;

import java.io.OutputStream;

final class r implements aa {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ac f25a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ OutputStream f26b;

    r(ac acVar, OutputStream outputStream) {
        this.f25a = acVar;
        this.f26b = outputStream;
    }

    public ac a() {
        return this.f25a;
    }

    public void a_(f fVar, long j) {
        ae.a(fVar.f11b, 0, j);
        while (j > 0) {
            this.f25a.g();
            x xVar = fVar.f10a;
            int min = (int) Math.min(j, (long) (xVar.c - xVar.f36b));
            this.f26b.write(xVar.f35a, xVar.f36b, min);
            xVar.f36b += min;
            j -= (long) min;
            fVar.f11b -= (long) min;
            if (xVar.f36b == xVar.c) {
                fVar.f10a = xVar.a();
                y.a(xVar);
            }
        }
    }

    public void close() {
        this.f26b.close();
    }

    public void flush() {
        this.f26b.flush();
    }

    public String toString() {
        return "sink(" + this.f26b + ")";
    }
}
