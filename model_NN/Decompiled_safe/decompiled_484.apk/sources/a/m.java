package a;

public abstract class m implements ab {

    /* renamed from: a  reason: collision with root package name */
    private final ab f18a;

    public m(ab abVar) {
        if (abVar == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f18a = abVar;
    }

    public long a(f fVar, long j) {
        return this.f18a.a(fVar, j);
    }

    public ac a() {
        return this.f18a.a();
    }

    public void close() {
        this.f18a.close();
    }

    public String toString() {
        return getClass().getSimpleName() + "(" + this.f18a.toString() + ")";
    }
}
