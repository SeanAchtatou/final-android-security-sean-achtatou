package a;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.Inflater;

public final class o implements ab {

    /* renamed from: a  reason: collision with root package name */
    private int f20a = 0;

    /* renamed from: b  reason: collision with root package name */
    private final i f21b;
    private final Inflater c;
    private final p d;
    private final CRC32 e = new CRC32();

    public o(ab abVar) {
        if (abVar == null) {
            throw new IllegalArgumentException("source == null");
        }
        this.c = new Inflater(true);
        this.f21b = q.a(abVar);
        this.d = new p(this.f21b, this.c);
    }

    private void a(f fVar, long j, long j2) {
        x xVar = fVar.f10a;
        while (j >= ((long) (xVar.c - xVar.f36b))) {
            j -= (long) (xVar.c - xVar.f36b);
            xVar = xVar.f;
        }
        while (j2 > 0) {
            int i = (int) (((long) xVar.f36b) + j);
            int min = (int) Math.min((long) (xVar.c - i), j2);
            this.e.update(xVar.f35a, i, min);
            j2 -= (long) min;
            xVar = xVar.f;
            j = 0;
        }
    }

    private void a(String str, int i, int i2) {
        if (i2 != i) {
            throw new IOException(String.format("%s: actual 0x%08x != expected 0x%08x", str, Integer.valueOf(i2), Integer.valueOf(i)));
        }
    }

    private void b() {
        this.f21b.a(10);
        byte b2 = this.f21b.c().b(3L);
        boolean z = ((b2 >> 1) & 1) == 1;
        if (z) {
            a(this.f21b.c(), 0, 10);
        }
        a("ID1ID2", 8075, this.f21b.j());
        this.f21b.g(8);
        if (((b2 >> 2) & 1) == 1) {
            this.f21b.a(2);
            if (z) {
                a(this.f21b.c(), 0, 2);
            }
            short l = this.f21b.c().l();
            this.f21b.a((long) l);
            if (z) {
                a(this.f21b.c(), 0, (long) l);
            }
            this.f21b.g((long) l);
        }
        if (((b2 >> 3) & 1) == 1) {
            long a2 = this.f21b.a((byte) 0);
            if (a2 == -1) {
                throw new EOFException();
            }
            if (z) {
                a(this.f21b.c(), 0, 1 + a2);
            }
            this.f21b.g(1 + a2);
        }
        if (((b2 >> 4) & 1) == 1) {
            long a3 = this.f21b.a((byte) 0);
            if (a3 == -1) {
                throw new EOFException();
            }
            if (z) {
                a(this.f21b.c(), 0, 1 + a3);
            }
            this.f21b.g(1 + a3);
        }
        if (z) {
            a("FHCRC", this.f21b.l(), (short) ((int) this.e.getValue()));
            this.e.reset();
        }
    }

    private void c() {
        a("CRC", this.f21b.m(), (int) this.e.getValue());
        a("ISIZE", this.f21b.m(), this.c.getTotalOut());
    }

    public long a(f fVar, long j) {
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (j == 0) {
            return 0;
        } else {
            if (this.f20a == 0) {
                b();
                this.f20a = 1;
            }
            if (this.f20a == 1) {
                long j2 = fVar.f11b;
                long a2 = this.d.a(fVar, j);
                if (a2 != -1) {
                    a(fVar, j2, a2);
                    return a2;
                }
                this.f20a = 2;
            }
            if (this.f20a == 2) {
                c();
                this.f20a = 3;
                if (!this.f21b.f()) {
                    throw new IOException("gzip finished without exhausting source");
                }
            }
            return -1;
        }
    }

    public ac a() {
        return this.f21b.a();
    }

    public void close() {
        this.d.close();
    }
}
