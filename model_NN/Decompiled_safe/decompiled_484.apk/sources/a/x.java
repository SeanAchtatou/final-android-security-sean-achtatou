package a;

final class x {

    /* renamed from: a  reason: collision with root package name */
    final byte[] f35a;

    /* renamed from: b  reason: collision with root package name */
    int f36b;
    int c;
    boolean d;
    boolean e;
    x f;
    x g;

    x() {
        this.f35a = new byte[2048];
        this.e = true;
        this.d = false;
    }

    x(x xVar) {
        this(xVar.f35a, xVar.f36b, xVar.c);
        xVar.d = true;
    }

    x(byte[] bArr, int i, int i2) {
        this.f35a = bArr;
        this.f36b = i;
        this.c = i2;
        this.e = false;
        this.d = true;
    }

    public x a() {
        x xVar = this.f != this ? this.f : null;
        this.g.f = this.f;
        this.f.g = this.g;
        this.f = null;
        this.g = null;
        return xVar;
    }

    public x a(int i) {
        if (i <= 0 || i > this.c - this.f36b) {
            throw new IllegalArgumentException();
        }
        x xVar = new x(this);
        xVar.c = xVar.f36b + i;
        this.f36b += i;
        this.g.a(xVar);
        return xVar;
    }

    public x a(x xVar) {
        xVar.g = this;
        xVar.f = this.f;
        this.f.g = xVar;
        this.f = xVar;
        return xVar;
    }

    public void a(x xVar, int i) {
        if (!xVar.e) {
            throw new IllegalArgumentException();
        }
        if (xVar.c + i > 2048) {
            if (xVar.d) {
                throw new IllegalArgumentException();
            } else if ((xVar.c + i) - xVar.f36b > 2048) {
                throw new IllegalArgumentException();
            } else {
                System.arraycopy(xVar.f35a, xVar.f36b, xVar.f35a, 0, xVar.c - xVar.f36b);
                xVar.c -= xVar.f36b;
                xVar.f36b = 0;
            }
        }
        System.arraycopy(this.f35a, this.f36b, xVar.f35a, xVar.c, i);
        xVar.c += i;
        this.f36b += i;
    }

    public void b() {
        if (this.g == this) {
            throw new IllegalStateException();
        } else if (this.g.e) {
            int i = this.c - this.f36b;
            if (i <= (this.g.d ? 0 : this.g.f36b) + (2048 - this.g.c)) {
                a(this.g, i);
                a();
                y.a(this);
            }
        }
    }
}
