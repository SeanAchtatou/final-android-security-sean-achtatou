package android.support.v7.app;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.v7.internal.view.SupportActionModeWrapper;
import android.support.v7.internal.widget.NativeActionModeAwareLayout;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

@TargetApi(11)
class AppCompatDelegateImplV11 extends AppCompatDelegateImplV7 implements NativeActionModeAwareLayout.OnActionModeForChildListener {
    private NativeActionModeAwareLayout mNativeActionModeAwareLayout;

    AppCompatDelegateImplV11(Context context, Window window, AppCompatCallback appCompatCallback) {
        super(context, window, appCompatCallback);
    }

    /* access modifiers changed from: package-private */
    public View callActivityOnCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        View callActivityOnCreateView = super.callActivityOnCreateView(view, str, context, attributeSet);
        if (callActivityOnCreateView != null) {
            return callActivityOnCreateView;
        }
        if (this.mOriginalWindowCallback instanceof LayoutInflater.Factory2) {
            return ((LayoutInflater.Factory2) this.mOriginalWindowCallback).onCreateView(view, str, context, attributeSet);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void onSubDecorInstalled(ViewGroup viewGroup) {
        this.mNativeActionModeAwareLayout = (NativeActionModeAwareLayout) viewGroup.findViewById(16908290);
        if (this.mNativeActionModeAwareLayout != null) {
            this.mNativeActionModeAwareLayout.setActionModeForChildListener(this);
        }
    }

    public ActionMode startActionModeForChild(View view, ActionMode.Callback callback) {
        android.support.v7.view.ActionMode startSupportActionMode = startSupportActionMode(new SupportActionModeWrapper.CallbackWrapper(view.getContext(), callback));
        if (startSupportActionMode != null) {
            return new SupportActionModeWrapper(this.mContext, startSupportActionMode);
        }
        return null;
    }
}
