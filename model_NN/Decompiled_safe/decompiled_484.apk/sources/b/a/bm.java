package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class bm implements fu<bm, bs>, Serializable, Cloneable {
    public static final Map<bs, gk> d;
    /* access modifiers changed from: private */
    public static final hc e = new hc("IdTracking");
    /* access modifiers changed from: private */
    public static final gt f = new gt("snapshots", (byte) 13, 1);
    /* access modifiers changed from: private */
    public static final gt g = new gt("journals", (byte) 15, 2);
    /* access modifiers changed from: private */
    public static final gt h = new gt("checksum", (byte) 11, 3);
    private static final Map<Class<? extends he>, hf> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public Map<String, bf> f71a;

    /* renamed from: b  reason: collision with root package name */
    public List<ay> f72b;
    public String c;
    private bs[] j = {bs.JOURNALS, bs.CHECKSUM};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.bs, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(hg.class, new bp());
        i.put(hh.class, new br());
        EnumMap enumMap = new EnumMap(bs.class);
        enumMap.put((Object) bs.SNAPSHOTS, (Object) new gk("snapshots", (byte) 1, new gn((byte) 13, new gl((byte) 11), new go((byte) 12, bf.class))));
        enumMap.put((Object) bs.JOURNALS, (Object) new gk("journals", (byte) 2, new gm((byte) 15, new go((byte) 12, ay.class))));
        enumMap.put((Object) bs.CHECKSUM, (Object) new gk("checksum", (byte) 2, new gl((byte) 11)));
        d = Collections.unmodifiableMap(enumMap);
        gk.a(bm.class, d);
    }

    public bm a(List<ay> list) {
        this.f72b = list;
        return this;
    }

    public bm a(Map<String, bf> map) {
        this.f71a = map;
        return this;
    }

    public Map<String, bf> a() {
        return this.f71a;
    }

    public void a(gw gwVar) {
        i.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z) {
        if (!z) {
            this.f71a = null;
        }
    }

    public List<ay> b() {
        return this.f72b;
    }

    public void b(gw gwVar) {
        i.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z) {
        if (!z) {
            this.f72b = null;
        }
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public boolean c() {
        return this.f72b != null;
    }

    public boolean d() {
        return this.c != null;
    }

    public void e() {
        if (this.f71a == null) {
            throw new gx("Required field 'snapshots' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("IdTracking(");
        sb.append("snapshots:");
        if (this.f71a == null) {
            sb.append("null");
        } else {
            sb.append(this.f71a);
        }
        if (c()) {
            sb.append(", ");
            sb.append("journals:");
            if (this.f72b == null) {
                sb.append("null");
            } else {
                sb.append(this.f72b);
            }
        }
        if (d()) {
            sb.append(", ");
            sb.append("checksum:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
