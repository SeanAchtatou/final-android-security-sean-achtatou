package b.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.qihoo.dynamic.util.Md5Util;
import java.security.MessageDigest;
import java.util.Locale;

public class fv {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f145a = {0, 0, 0, 0, 0, 0, 0, 0};

    /* renamed from: b  reason: collision with root package name */
    private String f146b = "1.0";
    private String c = null;
    private byte[] d = null;
    private byte[] e = null;
    private byte[] f = null;
    private int g = 0;
    private int h = 0;
    private int i = 0;
    private byte[] j = null;
    private byte[] k = null;

    private fv(byte[] bArr, String str, byte[] bArr2) {
        if (bArr == null || bArr.length == 0) {
            throw new Exception("entity is null or empty");
        }
        this.c = str;
        this.i = bArr.length;
        this.j = fq.a(bArr);
        this.h = (int) (System.currentTimeMillis() / 1000);
        this.k = bArr2;
    }

    public static fv a(Context context, String str, byte[] bArr) {
        try {
            String k2 = fl.k(context);
            String c2 = fl.c(context);
            SharedPreferences a2 = id.a(context);
            String string = a2.getString("signature", null);
            int i2 = a2.getInt("serial", 1);
            fv fvVar = new fv(bArr, str, (c2 + k2).getBytes());
            fvVar.a(string);
            fvVar.a(i2);
            fvVar.b();
            a2.edit().putInt("serial", i2 + 1).putString("signature", fvVar.a()).commit();
            return fvVar;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static byte[] a(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance(Md5Util.ALGORITHM);
            instance.reset();
            instance.update(bArr);
            return instance.digest();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private byte[] a(byte[] bArr, int i2) {
        byte[] a2 = a(this.k);
        byte[] a3 = a(this.j);
        int length = a2.length;
        byte[] bArr2 = new byte[(length * 2)];
        for (int i3 = 0; i3 < length; i3++) {
            bArr2[i3 * 2] = a3[i3];
            bArr2[(i3 * 2) + 1] = a2[i3];
        }
        for (int i4 = 0; i4 < 2; i4++) {
            bArr2[i4] = bArr[i4];
            bArr2[(bArr2.length - i4) - 1] = bArr[(bArr.length - i4) - 1];
        }
        byte[] bArr3 = {(byte) (i2 & 255), (byte) ((i2 >> 8) & 255), (byte) ((i2 >> 16) & 255), (byte) (i2 >>> 24)};
        for (int i5 = 0; i5 < bArr2.length; i5++) {
            bArr2[i5] = (byte) (bArr2[i5] ^ bArr3[i5 % 4]);
        }
        return bArr2;
    }

    public static String b(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < bArr.length; i2++) {
            stringBuffer.append(String.format("%02X", Byte.valueOf(bArr[i2])));
        }
        return stringBuffer.toString().toLowerCase(Locale.US);
    }

    public static byte[] b(String str) {
        byte[] bArr = null;
        if (str != null) {
            int length = str.length();
            if (length % 2 == 0) {
                bArr = new byte[(length / 2)];
                for (int i2 = 0; i2 < length; i2 += 2) {
                    bArr[i2 / 2] = (byte) Integer.valueOf(str.substring(i2, i2 + 2), 16).intValue();
                }
            }
        }
        return bArr;
    }

    private byte[] d() {
        return a(this.f145a, (int) (System.currentTimeMillis() / 1000));
    }

    private byte[] e() {
        return a((b(this.d) + this.g + this.h + this.i + b(this.e)).getBytes());
    }

    public String a() {
        return b(this.d);
    }

    public void a(int i2) {
        this.g = i2;
    }

    public void a(String str) {
        this.d = b(str);
    }

    public void b() {
        if (this.d == null) {
            this.d = d();
        }
        this.e = a(this.d, this.h);
        this.f = e();
    }

    public byte[] c() {
        fe feVar = new fe();
        feVar.a(this.f146b);
        feVar.b(this.c);
        feVar.c(b(this.d));
        feVar.a(this.g);
        feVar.b(this.h);
        feVar.c(this.i);
        feVar.a(this.j);
        feVar.d(b(this.e));
        feVar.e(b(this.f));
        try {
            return new gc().a(feVar);
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public String toString() {
        return String.format("version : %s\n", this.f146b) + String.format("address : %s\n", this.c) + String.format("signature : %s\n", b(this.d)) + String.format("serial : %s\n", Integer.valueOf(this.g)) + String.format("timestamp : %d\n", Integer.valueOf(this.h)) + String.format("length : %d\n", Integer.valueOf(this.i)) + String.format("guid : %s\n", b(this.e)) + String.format("checksum : %s ", b(this.f));
    }
}
