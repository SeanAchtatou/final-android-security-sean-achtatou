package b.a;

import android.content.Context;

public class hp {

    /* renamed from: a  reason: collision with root package name */
    private static gy f193a = null;

    /* renamed from: b  reason: collision with root package name */
    private static hn f194b = null;

    public static synchronized gy a(Context context) {
        gy gyVar;
        synchronized (hp.class) {
            if (f193a == null) {
                f193a = new gy(context);
                f193a.a(new hm(context));
                f193a.a(new ho(context));
                f193a.a(new dm(context));
                f193a.a(new hq(context));
                f193a.d();
            }
            gyVar = f193a;
        }
        return gyVar;
    }

    public static synchronized hn b(Context context) {
        hn hnVar;
        synchronized (hp.class) {
            if (f194b == null) {
                f194b = new hn(context);
                f194b.b();
            }
            hnVar = f194b;
        }
        return hnVar;
    }
}
