package b.a;

import android.support.v7.internal.widget.ActivityChooserView;

public class ha {

    /* renamed from: a  reason: collision with root package name */
    private static int f180a = ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;

    public static void a(gw gwVar, byte b2) {
        a(gwVar, b2, f180a);
    }

    public static void a(gw gwVar, byte b2, int i) {
        int i2 = 0;
        if (i <= 0) {
            throw new ga("Maximum skip depth exceeded");
        }
        switch (b2) {
            case 2:
                gwVar.p();
                return;
            case 3:
                gwVar.q();
                return;
            case 4:
                gwVar.u();
                return;
            case 5:
            case 7:
            case 9:
            default:
                return;
            case 6:
                gwVar.r();
                return;
            case 8:
                gwVar.s();
                return;
            case 10:
                gwVar.t();
                return;
            case 11:
                gwVar.w();
                return;
            case 12:
                gwVar.f();
                while (true) {
                    gt h = gwVar.h();
                    if (h.f172b == 0) {
                        gwVar.g();
                        return;
                    } else {
                        a(gwVar, h.f172b, i - 1);
                        gwVar.i();
                    }
                }
            case 13:
                gv j = gwVar.j();
                while (i2 < j.c) {
                    a(gwVar, j.f175a, i - 1);
                    a(gwVar, j.f176b, i - 1);
                    i2++;
                }
                gwVar.k();
                return;
            case 14:
                hb n = gwVar.n();
                while (i2 < n.f182b) {
                    a(gwVar, n.f181a, i - 1);
                    i2++;
                }
                gwVar.o();
                return;
            case 15:
                gu l = gwVar.l();
                while (i2 < l.f174b) {
                    a(gwVar, l.f173a, i - 1);
                    i2++;
                }
                gwVar.m();
                return;
        }
    }
}
