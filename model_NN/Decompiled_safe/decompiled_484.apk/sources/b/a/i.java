package b.a;

class i extends hg<g> {
    private i() {
    }

    /* renamed from: a */
    public void b(gw gwVar, g gVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                if (!gVar.a()) {
                    throw new gx("Required field 'ts' was not found in serialized data! Struct: " + toString());
                }
                gVar.b();
                return;
            }
            switch (h.c) {
                case 1:
                    if (h.f172b != 10) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        gVar.f151a = gwVar.t();
                        gVar.a(true);
                        break;
                    }
                default:
                    ha.a(gwVar, h.f172b);
                    break;
            }
            gwVar.i();
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, g gVar) {
        gVar.b();
        gwVar.a(g.c);
        gwVar.a(g.d);
        gwVar.a(gVar.f151a);
        gwVar.b();
        gwVar.c();
        gwVar.a();
    }
}
