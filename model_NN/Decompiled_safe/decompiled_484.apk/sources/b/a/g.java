package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class g implements fu<g, m>, Serializable, Cloneable {

    /* renamed from: b  reason: collision with root package name */
    public static final Map<m, gk> f150b;
    /* access modifiers changed from: private */
    public static final hc c = new hc("ActivateMsg");
    /* access modifiers changed from: private */
    public static final gt d = new gt("ts", (byte) 10, 1);
    private static final Map<Class<? extends he>, hf> e = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public long f151a;
    private byte f;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.m, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        e.put(hg.class, new j());
        e.put(hh.class, new l());
        EnumMap enumMap = new EnumMap(m.class);
        enumMap.put((Object) m.TS, (Object) new gk("ts", (byte) 1, new gl((byte) 10)));
        f150b = Collections.unmodifiableMap(enumMap);
        gk.a(g.class, f150b);
    }

    public g() {
        this.f = 0;
    }

    public g(long j) {
        this();
        this.f151a = j;
        a(true);
    }

    public void a(gw gwVar) {
        e.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z) {
        this.f = fs.a(this.f, 0, z);
    }

    public boolean a() {
        return fs.a(this.f, 0);
    }

    public void b() {
    }

    public void b(gw gwVar) {
        e.get(gwVar.y()).b().a(gwVar, this);
    }

    public String toString() {
        return "ActivateMsg(" + "ts:" + this.f151a + ")";
    }
}
