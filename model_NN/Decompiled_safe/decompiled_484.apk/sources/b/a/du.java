package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class du implements fu<du, ea>, Serializable, Cloneable {
    public static final Map<ea, gk> d;
    /* access modifiers changed from: private */
    public static final hc e = new hc("Response");
    /* access modifiers changed from: private */
    public static final gt f = new gt("resp_code", (byte) 8, 1);
    /* access modifiers changed from: private */
    public static final gt g = new gt("msg", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final gt h = new gt("imprint", (byte) 12, 3);
    private static final Map<Class<? extends he>, hf> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public int f108a;

    /* renamed from: b  reason: collision with root package name */
    public String f109b;
    public bt c;
    private byte j = 0;
    private ea[] k = {ea.MSG, ea.IMPRINT};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.ea, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(hg.class, new dx());
        i.put(hh.class, new dz());
        EnumMap enumMap = new EnumMap(ea.class);
        enumMap.put((Object) ea.RESP_CODE, (Object) new gk("resp_code", (byte) 1, new gl((byte) 8)));
        enumMap.put((Object) ea.MSG, (Object) new gk("msg", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) ea.IMPRINT, (Object) new gk("imprint", (byte) 2, new go((byte) 12, bt.class)));
        d = Collections.unmodifiableMap(enumMap);
        gk.a(du.class, d);
    }

    public void a(gw gwVar) {
        i.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z) {
        this.j = fs.a(this.j, 0, z);
    }

    public boolean a() {
        return fs.a(this.j, 0);
    }

    public String b() {
        return this.f109b;
    }

    public void b(gw gwVar) {
        i.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z) {
        if (!z) {
            this.f109b = null;
        }
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public boolean c() {
        return this.f109b != null;
    }

    public bt d() {
        return this.c;
    }

    public boolean e() {
        return this.c != null;
    }

    public void f() {
        if (this.c != null) {
            this.c.e();
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Response(");
        sb.append("resp_code:");
        sb.append(this.f108a);
        if (c()) {
            sb.append(", ");
            sb.append("msg:");
            if (this.f109b == null) {
                sb.append("null");
            } else {
                sb.append(this.f109b);
            }
        }
        if (e()) {
            sb.append(", ");
            sb.append("imprint:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
