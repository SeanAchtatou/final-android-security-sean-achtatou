package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class u implements fu<u, aa>, Serializable, Cloneable {
    public static final Map<aa, gk> d;
    /* access modifiers changed from: private */
    public static final hc e = new hc("ClientStats");
    /* access modifiers changed from: private */
    public static final gt f = new gt("successful_requests", (byte) 8, 1);
    /* access modifiers changed from: private */
    public static final gt g = new gt("failed_requests", (byte) 8, 2);
    /* access modifiers changed from: private */
    public static final gt h = new gt("last_request_spent_ms", (byte) 8, 3);
    private static final Map<Class<? extends he>, hf> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public int f228a = 0;

    /* renamed from: b  reason: collision with root package name */
    public int f229b = 0;
    public int c;
    private byte j = 0;
    private aa[] k = {aa.LAST_REQUEST_SPENT_MS};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.aa, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(hg.class, new x());
        i.put(hh.class, new z());
        EnumMap enumMap = new EnumMap(aa.class);
        enumMap.put((Object) aa.SUCCESSFUL_REQUESTS, (Object) new gk("successful_requests", (byte) 1, new gl((byte) 8)));
        enumMap.put((Object) aa.FAILED_REQUESTS, (Object) new gk("failed_requests", (byte) 1, new gl((byte) 8)));
        enumMap.put((Object) aa.LAST_REQUEST_SPENT_MS, (Object) new gk("last_request_spent_ms", (byte) 2, new gl((byte) 8)));
        d = Collections.unmodifiableMap(enumMap);
        gk.a(u.class, d);
    }

    public u a(int i2) {
        this.f228a = i2;
        a(true);
        return this;
    }

    public void a(gw gwVar) {
        i.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z) {
        this.j = fs.a(this.j, 0, z);
    }

    public boolean a() {
        return fs.a(this.j, 0);
    }

    public u b(int i2) {
        this.f229b = i2;
        b(true);
        return this;
    }

    public void b(gw gwVar) {
        i.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z) {
        this.j = fs.a(this.j, 1, z);
    }

    public boolean b() {
        return fs.a(this.j, 1);
    }

    public u c(int i2) {
        this.c = i2;
        c(true);
        return this;
    }

    public void c(boolean z) {
        this.j = fs.a(this.j, 2, z);
    }

    public boolean c() {
        return fs.a(this.j, 2);
    }

    public void d() {
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ClientStats(");
        sb.append("successful_requests:");
        sb.append(this.f228a);
        sb.append(", ");
        sb.append("failed_requests:");
        sb.append(this.f229b);
        if (c()) {
            sb.append(", ");
            sb.append("last_request_spent_ms:");
            sb.append(this.c);
        }
        sb.append(")");
        return sb.toString();
    }
}
