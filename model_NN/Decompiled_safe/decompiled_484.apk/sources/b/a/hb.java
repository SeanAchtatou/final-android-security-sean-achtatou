package b.a;

public final class hb {

    /* renamed from: a  reason: collision with root package name */
    public final byte f181a;

    /* renamed from: b  reason: collision with root package name */
    public final int f182b;

    public hb() {
        this((byte) 0, 0);
    }

    public hb(byte b2, int i) {
        this.f181a = b2;
        this.f182b = i;
    }

    public hb(gu guVar) {
        this(guVar.f173a, guVar.f174b);
    }
}
