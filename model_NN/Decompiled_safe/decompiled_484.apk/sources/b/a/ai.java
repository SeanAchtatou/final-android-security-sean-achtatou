package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class ai implements fu<ai, ao>, Serializable, Cloneable {
    public static final Map<ao, gk> d;
    /* access modifiers changed from: private */
    public static final hc e = new hc("Error");
    /* access modifiers changed from: private */
    public static final gt f = new gt("ts", (byte) 10, 1);
    /* access modifiers changed from: private */
    public static final gt g = new gt("context", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final gt h = new gt("source", (byte) 8, 3);
    private static final Map<Class<? extends he>, hf> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public long f51a;

    /* renamed from: b  reason: collision with root package name */
    public String f52b;
    public ap c;
    private byte j = 0;
    private ao[] k = {ao.SOURCE};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.ao, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(hg.class, new al());
        i.put(hh.class, new an());
        EnumMap enumMap = new EnumMap(ao.class);
        enumMap.put((Object) ao.TS, (Object) new gk("ts", (byte) 1, new gl((byte) 10)));
        enumMap.put((Object) ao.CONTEXT, (Object) new gk("context", (byte) 1, new gl((byte) 11)));
        enumMap.put((Object) ao.SOURCE, (Object) new gk("source", (byte) 2, new gj((byte) 16, ap.class)));
        d = Collections.unmodifiableMap(enumMap);
        gk.a(ai.class, d);
    }

    public ai a(long j2) {
        this.f51a = j2;
        a(true);
        return this;
    }

    public ai a(ap apVar) {
        this.c = apVar;
        return this;
    }

    public ai a(String str) {
        this.f52b = str;
        return this;
    }

    public void a(gw gwVar) {
        i.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z) {
        this.j = fs.a(this.j, 0, z);
    }

    public boolean a() {
        return fs.a(this.j, 0);
    }

    public void b(gw gwVar) {
        i.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z) {
        if (!z) {
            this.f52b = null;
        }
    }

    public boolean b() {
        return this.c != null;
    }

    public void c() {
        if (this.f52b == null) {
            throw new gx("Required field 'context' was not present! Struct: " + toString());
        }
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Error(");
        sb.append("ts:");
        sb.append(this.f51a);
        sb.append(", ");
        sb.append("context:");
        if (this.f52b == null) {
            sb.append("null");
        } else {
            sb.append(this.f52b);
        }
        if (b()) {
            sb.append(", ");
            sb.append("source:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
