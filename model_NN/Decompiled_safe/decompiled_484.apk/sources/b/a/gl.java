package b.a;

import java.io.Serializable;

public class gl implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f159a;

    /* renamed from: b  reason: collision with root package name */
    public final byte f160b;
    private final String c;
    private final boolean d;

    public gl(byte b2) {
        this(b2, false);
    }

    public gl(byte b2, boolean z) {
        this.f160b = b2;
        this.f159a = false;
        this.c = null;
        this.d = z;
    }
}
