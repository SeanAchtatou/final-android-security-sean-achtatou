package b.a;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

class bo extends hg<bm> {
    private bo() {
    }

    /* renamed from: a */
    public void b(gw gwVar, bm bmVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                bmVar.e();
                return;
            }
            switch (h.c) {
                case 1:
                    if (h.f172b != 13) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        gv j = gwVar.j();
                        bmVar.f71a = new HashMap(j.c * 2);
                        for (int i = 0; i < j.c; i++) {
                            String v = gwVar.v();
                            bf bfVar = new bf();
                            bfVar.a(gwVar);
                            bmVar.f71a.put(v, bfVar);
                        }
                        gwVar.k();
                        bmVar.a(true);
                        break;
                    }
                case 2:
                    if (h.f172b != 15) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        gu l = gwVar.l();
                        bmVar.f72b = new ArrayList(l.f174b);
                        for (int i2 = 0; i2 < l.f174b; i2++) {
                            ay ayVar = new ay();
                            ayVar.a(gwVar);
                            bmVar.f72b.add(ayVar);
                        }
                        gwVar.m();
                        bmVar.b(true);
                        break;
                    }
                case 3:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        bmVar.c = gwVar.v();
                        bmVar.c(true);
                        break;
                    }
                default:
                    ha.a(gwVar, h.f172b);
                    break;
            }
            gwVar.i();
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, bm bmVar) {
        bmVar.e();
        gwVar.a(bm.e);
        if (bmVar.f71a != null) {
            gwVar.a(bm.f);
            gwVar.a(new gv((byte) 11, (byte) 12, bmVar.f71a.size()));
            for (Map.Entry next : bmVar.f71a.entrySet()) {
                gwVar.a((String) next.getKey());
                ((bf) next.getValue()).b(gwVar);
            }
            gwVar.d();
            gwVar.b();
        }
        if (bmVar.f72b != null && bmVar.c()) {
            gwVar.a(bm.g);
            gwVar.a(new gu((byte) 12, bmVar.f72b.size()));
            for (ay b2 : bmVar.f72b) {
                b2.b(gwVar);
            }
            gwVar.e();
            gwVar.b();
        }
        if (bmVar.c != null && bmVar.d()) {
            gwVar.a(bm.h);
            gwVar.a(bmVar.c);
            gwVar.b();
        }
        gwVar.c();
        gwVar.a();
    }
}
