package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum bz implements gb {
    PROPERTY(1, "property"),
    VERSION(2, "version"),
    CHECKSUM(3, "checksum");
    
    private static final Map<String, bz> d = new HashMap();
    private final short e;
    private final String f;

    static {
        Iterator it = EnumSet.allOf(bz.class).iterator();
        while (it.hasNext()) {
            bz bzVar = (bz) it.next();
            d.put(bzVar.b(), bzVar);
        }
    }

    private bz(short s, String str) {
        this.e = s;
        this.f = str;
    }

    public short a() {
        return this.e;
    }

    public String b() {
        return this.f;
    }
}
