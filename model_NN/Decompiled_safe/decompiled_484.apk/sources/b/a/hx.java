package b.a;

import android.content.Context;
import com.f.a.a;
import java.util.HashMap;

public class hx {

    /* renamed from: a  reason: collision with root package name */
    private final int f208a = 128;

    /* renamed from: b  reason: collision with root package name */
    private final int f209b = 256;
    private hv c;
    private Context d;
    private hs e;

    public hx(Context context) {
        if (context == null) {
            throw new RuntimeException("Context is null, can't track event");
        }
        this.d = context.getApplicationContext();
        this.c = new hv(this.d);
        this.c.a(!a.l);
        this.e = hs.a(this.d);
    }

    private boolean a(String str) {
        int length;
        if (str != null && (length = str.trim().getBytes().length) > 0 && length <= 128) {
            return true;
        }
        fm.b("MobclickAgent", "Event id is empty or too long in tracking Event");
        return false;
    }

    private boolean b(String str) {
        if (str == null || str.trim().getBytes().length <= 256) {
            return true;
        }
        fm.b("MobclickAgent", "Event label or value is empty or too long in tracking Event");
        return false;
    }

    public void a(String str, String str2, long j, int i) {
        if (a(str) && b(str2)) {
            HashMap hashMap = new HashMap();
            if (str2 == null) {
                str2 = "";
            }
            hashMap.put(str, str2);
            this.e.a(new b(str, hashMap, j, i));
        }
    }
}
