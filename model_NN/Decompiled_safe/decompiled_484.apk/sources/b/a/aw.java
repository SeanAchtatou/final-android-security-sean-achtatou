package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum aw implements gb {
    NAME(1, "name"),
    PROPERTIES(2, "properties"),
    DURATION(3, "duration"),
    ACC(4, "acc"),
    TS(5, "ts");
    
    private static final Map<String, aw> f = new HashMap();
    private final short g;
    private final String h;

    static {
        Iterator it = EnumSet.allOf(aw.class).iterator();
        while (it.hasNext()) {
            aw awVar = (aw) it.next();
            f.put(awVar.b(), awVar);
        }
    }

    private aw(short s, String str) {
        this.g = s;
        this.h = str;
    }

    public short a() {
        return this.g;
    }

    public String b() {
        return this.h;
    }
}
