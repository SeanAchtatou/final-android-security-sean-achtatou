package b.a;

import android.util.Log;

public class fm {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f137a = false;

    public static void a(String str, String str2) {
        if (f137a) {
            Log.i(str, str2);
        }
    }

    public static void a(String str, String str2, Exception exc) {
        if (f137a) {
            Log.i(str, String.valueOf(exc.toString()) + ":  [" + str2 + "]");
        }
    }

    public static void b(String str, String str2) {
        if (f137a) {
            Log.e(str, str2);
        }
    }

    public static void b(String str, String str2, Exception exc) {
        if (f137a) {
            Log.e(str, String.valueOf(exc.toString()) + ":  [" + str2 + "]");
            StackTraceElement[] stackTrace = exc.getStackTrace();
            int length = stackTrace.length;
            for (int i = 0; i < length; i++) {
                Log.e(str, "        at\t " + stackTrace[i].toString());
            }
        }
    }

    public static void c(String str, String str2) {
        if (f137a) {
            Log.d(str, str2);
        }
    }

    public static void c(String str, String str2, Exception exc) {
        if (f137a) {
            Log.d(str, String.valueOf(exc.toString()) + ":  [" + str2 + "]");
        }
    }

    public static void d(String str, String str2) {
        if (f137a) {
            Log.w(str, str2);
        }
    }

    public static void d(String str, String str2, Exception exc) {
        if (f137a) {
            Log.w(str, String.valueOf(exc.toString()) + ":  [" + str2 + "]");
            StackTraceElement[] stackTrace = exc.getStackTrace();
            int length = stackTrace.length;
            for (int i = 0; i < length; i++) {
                Log.w(str, "        at\t " + stackTrace[i].toString());
            }
        }
    }
}
