package b.a;

import java.util.BitSet;

class af extends hh<ab> {
    private af() {
    }

    public void a(gw gwVar, ab abVar) {
        hd hdVar = (hd) gwVar;
        BitSet bitSet = new BitSet();
        if (abVar.a()) {
            bitSet.set(0);
        }
        if (abVar.b()) {
            bitSet.set(1);
        }
        if (abVar.c()) {
            bitSet.set(2);
        }
        if (abVar.d()) {
            bitSet.set(3);
        }
        if (abVar.e()) {
            bitSet.set(4);
        }
        if (abVar.f()) {
            bitSet.set(5);
        }
        if (abVar.g()) {
            bitSet.set(6);
        }
        if (abVar.h()) {
            bitSet.set(7);
        }
        if (abVar.i()) {
            bitSet.set(8);
        }
        if (abVar.j()) {
            bitSet.set(9);
        }
        if (abVar.k()) {
            bitSet.set(10);
        }
        if (abVar.l()) {
            bitSet.set(11);
        }
        if (abVar.m()) {
            bitSet.set(12);
        }
        if (abVar.n()) {
            bitSet.set(13);
        }
        if (abVar.o()) {
            bitSet.set(14);
        }
        if (abVar.p()) {
            bitSet.set(15);
        }
        if (abVar.q()) {
            bitSet.set(16);
        }
        hdVar.a(bitSet, 17);
        if (abVar.a()) {
            hdVar.a(abVar.f47a);
        }
        if (abVar.b()) {
            hdVar.a(abVar.f48b);
        }
        if (abVar.c()) {
            hdVar.a(abVar.c);
        }
        if (abVar.d()) {
            hdVar.a(abVar.d);
        }
        if (abVar.e()) {
            hdVar.a(abVar.e);
        }
        if (abVar.f()) {
            hdVar.a(abVar.f);
        }
        if (abVar.g()) {
            hdVar.a(abVar.g);
        }
        if (abVar.h()) {
            hdVar.a(abVar.h);
        }
        if (abVar.i()) {
            abVar.i.b(hdVar);
        }
        if (abVar.j()) {
            hdVar.a(abVar.j);
        }
        if (abVar.k()) {
            hdVar.a(abVar.k);
        }
        if (abVar.l()) {
            hdVar.a(abVar.l);
        }
        if (abVar.m()) {
            hdVar.a(abVar.m);
        }
        if (abVar.n()) {
            hdVar.a(abVar.n);
        }
        if (abVar.o()) {
            hdVar.a(abVar.o);
        }
        if (abVar.p()) {
            hdVar.a(abVar.p);
        }
        if (abVar.q()) {
            hdVar.a(abVar.q);
        }
    }

    public void b(gw gwVar, ab abVar) {
        hd hdVar = (hd) gwVar;
        BitSet b2 = hdVar.b(17);
        if (b2.get(0)) {
            abVar.f47a = hdVar.v();
            abVar.a(true);
        }
        if (b2.get(1)) {
            abVar.f48b = hdVar.v();
            abVar.b(true);
        }
        if (b2.get(2)) {
            abVar.c = hdVar.v();
            abVar.c(true);
        }
        if (b2.get(3)) {
            abVar.d = hdVar.v();
            abVar.d(true);
        }
        if (b2.get(4)) {
            abVar.e = hdVar.v();
            abVar.e(true);
        }
        if (b2.get(5)) {
            abVar.f = hdVar.v();
            abVar.f(true);
        }
        if (b2.get(6)) {
            abVar.g = hdVar.v();
            abVar.g(true);
        }
        if (b2.get(7)) {
            abVar.h = hdVar.v();
            abVar.h(true);
        }
        if (b2.get(8)) {
            abVar.i = new dn();
            abVar.i.a(hdVar);
            abVar.i(true);
        }
        if (b2.get(9)) {
            abVar.j = hdVar.p();
            abVar.j(true);
        }
        if (b2.get(10)) {
            abVar.k = hdVar.p();
            abVar.k(true);
        }
        if (b2.get(11)) {
            abVar.l = hdVar.v();
            abVar.l(true);
        }
        if (b2.get(12)) {
            abVar.m = hdVar.v();
            abVar.m(true);
        }
        if (b2.get(13)) {
            abVar.n = hdVar.t();
            abVar.n(true);
        }
        if (b2.get(14)) {
            abVar.o = hdVar.v();
            abVar.o(true);
        }
        if (b2.get(15)) {
            abVar.p = hdVar.v();
            abVar.p(true);
        }
        if (b2.get(16)) {
            abVar.q = hdVar.v();
            abVar.q(true);
        }
    }
}
