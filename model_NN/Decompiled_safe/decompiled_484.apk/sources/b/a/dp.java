package b.a;

class dp extends hg<dn> {
    private dp() {
    }

    /* renamed from: a */
    public void b(gw gwVar, dn dnVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                if (!dnVar.a()) {
                    throw new gx("Required field 'height' was not found in serialized data! Struct: " + toString());
                } else if (!dnVar.b()) {
                    throw new gx("Required field 'width' was not found in serialized data! Struct: " + toString());
                } else {
                    dnVar.c();
                    return;
                }
            } else {
                switch (h.c) {
                    case 1:
                        if (h.f172b != 8) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            dnVar.f104a = gwVar.s();
                            dnVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.f172b != 8) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            dnVar.f105b = gwVar.s();
                            dnVar.b(true);
                            break;
                        }
                    default:
                        ha.a(gwVar, h.f172b);
                        break;
                }
                gwVar.i();
            }
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, dn dnVar) {
        dnVar.c();
        gwVar.a(dn.d);
        gwVar.a(dn.e);
        gwVar.a(dnVar.f104a);
        gwVar.b();
        gwVar.a(dn.f);
        gwVar.a(dnVar.f105b);
        gwVar.b();
        gwVar.c();
        gwVar.a();
    }
}
