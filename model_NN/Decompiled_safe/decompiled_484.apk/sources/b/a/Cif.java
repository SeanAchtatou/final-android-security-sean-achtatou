package b.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.qihoo.messenger.util.QDefine;

/* renamed from: b.a.if  reason: invalid class name */
public class Cif {

    /* renamed from: a  reason: collision with root package name */
    public int f216a;

    /* renamed from: b  reason: collision with root package name */
    public int f217b;
    public long c;
    private final int d = QDefine.ONE_HOUR;
    private int e;
    private long f = 0;
    private long g = 0;
    private Context h;

    public Cif(Context context) {
        b(context);
    }

    public static u a(Context context) {
        SharedPreferences a2 = id.a(context);
        u uVar = new u();
        uVar.b(a2.getInt("failed_requests ", 0));
        uVar.c(a2.getInt("last_request_spent_ms", 0));
        uVar.a(a2.getInt("successful_request", 0));
        return uVar;
    }

    private void b(Context context) {
        this.h = context.getApplicationContext();
        SharedPreferences a2 = id.a(context);
        this.f216a = a2.getInt("successful_request", 0);
        this.f217b = a2.getInt("failed_requests ", 0);
        this.e = a2.getInt("last_request_spent_ms", 0);
        this.c = a2.getLong("last_request_time", 0);
    }

    public boolean a() {
        return this.c == 0;
    }

    public void b() {
        this.f216a++;
        this.c = this.f;
    }

    public void c() {
        this.f217b++;
    }

    public void d() {
        this.f = System.currentTimeMillis();
    }

    public void e() {
        this.e = (int) (System.currentTimeMillis() - this.f);
    }

    public void f() {
        id.a(this.h).edit().putInt("successful_request", this.f216a).putInt("failed_requests ", this.f217b).putInt("last_request_spent_ms", this.e).putLong("last_request_time", this.c).commit();
    }

    public void g() {
        id.a(this.h).edit().putLong("first_activate_time", System.currentTimeMillis()).commit();
    }

    public boolean h() {
        if (this.g == 0) {
            this.g = id.a(this.h).getLong("first_activate_time", 0);
        }
        return this.g == 0;
    }

    public long i() {
        return h() ? System.currentTimeMillis() : this.g;
    }
}
