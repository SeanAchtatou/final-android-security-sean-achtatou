package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum cn implements gb {
    ID(1, "id"),
    ERRORS(2, "errors"),
    EVENTS(3, "events"),
    GAME_EVENTS(4, "game_events");
    
    private static final Map<String, cn> e = new HashMap();
    private final short f;
    private final String g;

    static {
        Iterator it = EnumSet.allOf(cn.class).iterator();
        while (it.hasNext()) {
            cn cnVar = (cn) it.next();
            e.put(cnVar.b(), cnVar);
        }
    }

    private cn(short s, String str) {
        this.f = s;
        this.g = str;
    }

    public short a() {
        return this.f;
    }

    public String b() {
        return this.g;
    }
}
