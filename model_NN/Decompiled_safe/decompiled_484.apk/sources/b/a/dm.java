package b.a;

import android.content.Context;
import android.provider.Settings;

public class dm extends a {

    /* renamed from: a  reason: collision with root package name */
    private Context f103a;

    public dm(Context context) {
        super("android_id");
        this.f103a = context;
    }

    public String f() {
        try {
            return Settings.Secure.getString(this.f103a.getContentResolver(), "android_id");
        } catch (Exception e) {
            return null;
        }
    }
}
