package b.a;

class fg extends hg<fe> {
    private fg() {
    }

    /* renamed from: a */
    public void b(gw gwVar, fe feVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                if (!feVar.a()) {
                    throw new gx("Required field 'serial_num' was not found in serialized data! Struct: " + toString());
                } else if (!feVar.b()) {
                    throw new gx("Required field 'ts_secs' was not found in serialized data! Struct: " + toString());
                } else if (!feVar.c()) {
                    throw new gx("Required field 'length' was not found in serialized data! Struct: " + toString());
                } else {
                    feVar.d();
                    return;
                }
            } else {
                switch (h.c) {
                    case 1:
                        if (h.f172b != 11) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            feVar.f132a = gwVar.v();
                            feVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.f172b != 11) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            feVar.f133b = gwVar.v();
                            feVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.f172b != 11) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            feVar.c = gwVar.v();
                            feVar.c(true);
                            break;
                        }
                    case 4:
                        if (h.f172b != 8) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            feVar.d = gwVar.s();
                            feVar.d(true);
                            break;
                        }
                    case 5:
                        if (h.f172b != 8) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            feVar.e = gwVar.s();
                            feVar.e(true);
                            break;
                        }
                    case 6:
                        if (h.f172b != 8) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            feVar.f = gwVar.s();
                            feVar.f(true);
                            break;
                        }
                    case 7:
                        if (h.f172b != 11) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            feVar.g = gwVar.w();
                            feVar.g(true);
                            break;
                        }
                    case 8:
                        if (h.f172b != 11) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            feVar.h = gwVar.v();
                            feVar.h(true);
                            break;
                        }
                    case 9:
                        if (h.f172b != 11) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            feVar.i = gwVar.v();
                            feVar.i(true);
                            break;
                        }
                    default:
                        ha.a(gwVar, h.f172b);
                        break;
                }
                gwVar.i();
            }
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, fe feVar) {
        feVar.d();
        gwVar.a(fe.k);
        if (feVar.f132a != null) {
            gwVar.a(fe.l);
            gwVar.a(feVar.f132a);
            gwVar.b();
        }
        if (feVar.f133b != null) {
            gwVar.a(fe.m);
            gwVar.a(feVar.f133b);
            gwVar.b();
        }
        if (feVar.c != null) {
            gwVar.a(fe.n);
            gwVar.a(feVar.c);
            gwVar.b();
        }
        gwVar.a(fe.o);
        gwVar.a(feVar.d);
        gwVar.b();
        gwVar.a(fe.p);
        gwVar.a(feVar.e);
        gwVar.b();
        gwVar.a(fe.q);
        gwVar.a(feVar.f);
        gwVar.b();
        if (feVar.g != null) {
            gwVar.a(fe.r);
            gwVar.a(feVar.g);
            gwVar.b();
        }
        if (feVar.h != null) {
            gwVar.a(fe.s);
            gwVar.a(feVar.h);
            gwVar.b();
        }
        if (feVar.i != null) {
            gwVar.a(fe.t);
            gwVar.a(feVar.i);
            gwVar.b();
        }
        gwVar.c();
        gwVar.a();
    }
}
