package b.a;

public enum ax {
    MALE(0),
    FEMALE(1),
    UNKNOWN(2);
    
    private final int d;

    private ax(int i) {
        this.d = i;
    }

    public static ax a(int i) {
        switch (i) {
            case 0:
                return MALE;
            case 1:
                return FEMALE;
            case 2:
                return UNKNOWN;
            default:
                return null;
        }
    }

    public int a() {
        return this.d;
    }
}
