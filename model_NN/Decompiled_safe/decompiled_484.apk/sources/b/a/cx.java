package b.a;

class cx extends hg<cv> {
    private cx() {
    }

    /* renamed from: a */
    public void b(gw gwVar, cv cvVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                cvVar.l();
                return;
            }
            switch (h.c) {
                case 1:
                    if (h.f172b != 8) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        cvVar.f91a = gwVar.s();
                        cvVar.a(true);
                        break;
                    }
                case 2:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        cvVar.f92b = gwVar.v();
                        cvVar.b(true);
                        break;
                    }
                case 3:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        cvVar.c = gwVar.v();
                        cvVar.c(true);
                        break;
                    }
                case 4:
                    if (h.f172b != 4) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        cvVar.d = gwVar.u();
                        cvVar.d(true);
                        break;
                    }
                case 5:
                    if (h.f172b != 4) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        cvVar.e = gwVar.u();
                        cvVar.e(true);
                        break;
                    }
                case 6:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        cvVar.f = gwVar.v();
                        cvVar.f(true);
                        break;
                    }
                case 7:
                    if (h.f172b != 8) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        cvVar.g = gwVar.s();
                        cvVar.g(true);
                        break;
                    }
                case 8:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        cvVar.h = gwVar.v();
                        cvVar.h(true);
                        break;
                    }
                case 9:
                    if (h.f172b != 8) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        cvVar.i = f.a(gwVar.s());
                        cvVar.i(true);
                        break;
                    }
                case 10:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        cvVar.j = gwVar.v();
                        cvVar.j(true);
                        break;
                    }
                case 11:
                    if (h.f172b != 12) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        cvVar.k = new ex();
                        cvVar.k.a(gwVar);
                        cvVar.k(true);
                        break;
                    }
                default:
                    ha.a(gwVar, h.f172b);
                    break;
            }
            gwVar.i();
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, cv cvVar) {
        cvVar.l();
        gwVar.a(cv.m);
        if (cvVar.a()) {
            gwVar.a(cv.n);
            gwVar.a(cvVar.f91a);
            gwVar.b();
        }
        if (cvVar.f92b != null && cvVar.b()) {
            gwVar.a(cv.o);
            gwVar.a(cvVar.f92b);
            gwVar.b();
        }
        if (cvVar.c != null && cvVar.c()) {
            gwVar.a(cv.p);
            gwVar.a(cvVar.c);
            gwVar.b();
        }
        if (cvVar.d()) {
            gwVar.a(cv.q);
            gwVar.a(cvVar.d);
            gwVar.b();
        }
        if (cvVar.e()) {
            gwVar.a(cv.r);
            gwVar.a(cvVar.e);
            gwVar.b();
        }
        if (cvVar.f != null && cvVar.f()) {
            gwVar.a(cv.s);
            gwVar.a(cvVar.f);
            gwVar.b();
        }
        if (cvVar.g()) {
            gwVar.a(cv.t);
            gwVar.a(cvVar.g);
            gwVar.b();
        }
        if (cvVar.h != null && cvVar.h()) {
            gwVar.a(cv.u);
            gwVar.a(cvVar.h);
            gwVar.b();
        }
        if (cvVar.i != null && cvVar.i()) {
            gwVar.a(cv.v);
            gwVar.a(cvVar.i.a());
            gwVar.b();
        }
        if (cvVar.j != null && cvVar.j()) {
            gwVar.a(cv.w);
            gwVar.a(cvVar.j);
            gwVar.b();
        }
        if (cvVar.k != null && cvVar.k()) {
            gwVar.a(cv.x);
            cvVar.k.b(gwVar);
            gwVar.b();
        }
        gwVar.c();
        gwVar.a();
    }
}
