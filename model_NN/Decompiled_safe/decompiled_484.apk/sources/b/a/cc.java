package b.a;

class cc extends hg<ca> {
    private cc() {
    }

    /* renamed from: a */
    public void b(gw gwVar, ca caVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                if (!caVar.d()) {
                    throw new gx("Required field 'ts' was not found in serialized data! Struct: " + toString());
                }
                caVar.f();
                return;
            }
            switch (h.c) {
                case 1:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        caVar.f79a = gwVar.v();
                        caVar.a(true);
                        break;
                    }
                case 2:
                    if (h.f172b != 10) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        caVar.f80b = gwVar.t();
                        caVar.b(true);
                        break;
                    }
                case 3:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        caVar.c = gwVar.v();
                        caVar.c(true);
                        break;
                    }
                default:
                    ha.a(gwVar, h.f172b);
                    break;
            }
            gwVar.i();
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, ca caVar) {
        caVar.f();
        gwVar.a(ca.e);
        if (caVar.f79a != null && caVar.b()) {
            gwVar.a(ca.f);
            gwVar.a(caVar.f79a);
            gwVar.b();
        }
        gwVar.a(ca.g);
        gwVar.a(caVar.f80b);
        gwVar.b();
        if (caVar.c != null) {
            gwVar.a(ca.h);
            gwVar.a(caVar.c);
            gwVar.b();
        }
        gwVar.c();
        gwVar.a();
    }
}
