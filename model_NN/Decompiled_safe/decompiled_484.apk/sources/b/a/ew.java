package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum ew implements gb {
    CLIENT_STATS(1, "client_stats"),
    APP_INFO(2, "app_info"),
    DEVICE_INFO(3, "device_info"),
    MISC_INFO(4, "misc_info"),
    ACTIVATE_MSG(5, "activate_msg"),
    INSTANT_MSGS(6, "instant_msgs"),
    SESSIONS(7, "sessions"),
    IMPRINT(8, "imprint"),
    ID_TRACKING(9, "id_tracking");
    
    private static final Map<String, ew> j = new HashMap();
    private final short k;
    private final String l;

    static {
        Iterator it = EnumSet.allOf(ew.class).iterator();
        while (it.hasNext()) {
            ew ewVar = (ew) it.next();
            j.put(ewVar.b(), ewVar);
        }
    }

    private ew(short s, String str) {
        this.k = s;
        this.l = str;
    }

    public short a() {
        return this.k;
    }

    public String b() {
        return this.l;
    }
}
