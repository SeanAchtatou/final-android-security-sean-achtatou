package b.a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class eq implements fu<eq, ew>, Serializable, Cloneable {
    public static final Map<ew, gk> j;
    /* access modifiers changed from: private */
    public static final hc k = new hc("UALogEntry");
    /* access modifiers changed from: private */
    public static final gt l = new gt("client_stats", (byte) 12, 1);
    /* access modifiers changed from: private */
    public static final gt m = new gt("app_info", (byte) 12, 2);
    /* access modifiers changed from: private */
    public static final gt n = new gt("device_info", (byte) 12, 3);
    /* access modifiers changed from: private */
    public static final gt o = new gt("misc_info", (byte) 12, 4);
    /* access modifiers changed from: private */
    public static final gt p = new gt("activate_msg", (byte) 12, 5);
    /* access modifiers changed from: private */
    public static final gt q = new gt("instant_msgs", (byte) 15, 6);
    /* access modifiers changed from: private */
    public static final gt r = new gt("sessions", (byte) 15, 7);
    /* access modifiers changed from: private */
    public static final gt s = new gt("imprint", (byte) 12, 8);
    /* access modifiers changed from: private */
    public static final gt t = new gt("id_tracking", (byte) 12, 9);
    private static final Map<Class<? extends he>, hf> u = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public u f122a;

    /* renamed from: b  reason: collision with root package name */
    public n f123b;
    public ab c;
    public cv d;
    public g e;
    public List<ch> f;
    public List<ec> g;
    public bt h;
    public bm i;
    private ew[] v = {ew.ACTIVATE_MSG, ew.INSTANT_MSGS, ew.SESSIONS, ew.IMPRINT, ew.ID_TRACKING};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.ew, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        u.put(hg.class, new et());
        u.put(hh.class, new ev());
        EnumMap enumMap = new EnumMap(ew.class);
        enumMap.put((Object) ew.CLIENT_STATS, (Object) new gk("client_stats", (byte) 1, new go((byte) 12, u.class)));
        enumMap.put((Object) ew.APP_INFO, (Object) new gk("app_info", (byte) 1, new go((byte) 12, n.class)));
        enumMap.put((Object) ew.DEVICE_INFO, (Object) new gk("device_info", (byte) 1, new go((byte) 12, ab.class)));
        enumMap.put((Object) ew.MISC_INFO, (Object) new gk("misc_info", (byte) 1, new go((byte) 12, cv.class)));
        enumMap.put((Object) ew.ACTIVATE_MSG, (Object) new gk("activate_msg", (byte) 2, new go((byte) 12, g.class)));
        enumMap.put((Object) ew.INSTANT_MSGS, (Object) new gk("instant_msgs", (byte) 2, new gm((byte) 15, new go((byte) 12, ch.class))));
        enumMap.put((Object) ew.SESSIONS, (Object) new gk("sessions", (byte) 2, new gm((byte) 15, new go((byte) 12, ec.class))));
        enumMap.put((Object) ew.IMPRINT, (Object) new gk("imprint", (byte) 2, new go((byte) 12, bt.class)));
        enumMap.put((Object) ew.ID_TRACKING, (Object) new gk("id_tracking", (byte) 2, new go((byte) 12, bm.class)));
        j = Collections.unmodifiableMap(enumMap);
        gk.a(eq.class, j);
    }

    public eq a(ab abVar) {
        this.c = abVar;
        return this;
    }

    public eq a(bm bmVar) {
        this.i = bmVar;
        return this;
    }

    public eq a(bt btVar) {
        this.h = btVar;
        return this;
    }

    public eq a(cv cvVar) {
        this.d = cvVar;
        return this;
    }

    public eq a(g gVar) {
        this.e = gVar;
        return this;
    }

    public eq a(n nVar) {
        this.f123b = nVar;
        return this;
    }

    public eq a(u uVar) {
        this.f122a = uVar;
        return this;
    }

    public void a(ch chVar) {
        if (this.f == null) {
            this.f = new ArrayList();
        }
        this.f.add(chVar);
    }

    public void a(ec ecVar) {
        if (this.g == null) {
            this.g = new ArrayList();
        }
        this.g.add(ecVar);
    }

    public void a(gw gwVar) {
        u.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z) {
        if (!z) {
            this.f122a = null;
        }
    }

    public boolean a() {
        return this.e != null;
    }

    public int b() {
        if (this.f == null) {
            return 0;
        }
        return this.f.size();
    }

    public void b(gw gwVar) {
        u.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z) {
        if (!z) {
            this.f123b = null;
        }
    }

    public List<ch> c() {
        return this.f;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public void d(boolean z) {
        if (!z) {
            this.d = null;
        }
    }

    public boolean d() {
        return this.f != null;
    }

    public List<ec> e() {
        return this.g;
    }

    public void e(boolean z) {
        if (!z) {
            this.e = null;
        }
    }

    public void f(boolean z) {
        if (!z) {
            this.f = null;
        }
    }

    public boolean f() {
        return this.g != null;
    }

    public void g(boolean z) {
        if (!z) {
            this.g = null;
        }
    }

    public boolean g() {
        return this.h != null;
    }

    public void h(boolean z) {
        if (!z) {
            this.h = null;
        }
    }

    public boolean h() {
        return this.i != null;
    }

    public void i() {
        if (this.f122a == null) {
            throw new gx("Required field 'client_stats' was not present! Struct: " + toString());
        } else if (this.f123b == null) {
            throw new gx("Required field 'app_info' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new gx("Required field 'device_info' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new gx("Required field 'misc_info' was not present! Struct: " + toString());
        } else {
            if (this.f122a != null) {
                this.f122a.d();
            }
            if (this.f123b != null) {
                this.f123b.g();
            }
            if (this.c != null) {
                this.c.r();
            }
            if (this.d != null) {
                this.d.l();
            }
            if (this.e != null) {
                this.e.b();
            }
            if (this.h != null) {
                this.h.e();
            }
            if (this.i != null) {
                this.i.e();
            }
        }
    }

    public void i(boolean z) {
        if (!z) {
            this.i = null;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("UALogEntry(");
        sb.append("client_stats:");
        if (this.f122a == null) {
            sb.append("null");
        } else {
            sb.append(this.f122a);
        }
        sb.append(", ");
        sb.append("app_info:");
        if (this.f123b == null) {
            sb.append("null");
        } else {
            sb.append(this.f123b);
        }
        sb.append(", ");
        sb.append("device_info:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("misc_info:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        if (a()) {
            sb.append(", ");
            sb.append("activate_msg:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (d()) {
            sb.append(", ");
            sb.append("instant_msgs:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        if (f()) {
            sb.append(", ");
            sb.append("sessions:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        if (g()) {
            sb.append(", ");
            sb.append("imprint:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        if (h()) {
            sb.append(", ");
            sb.append("id_tracking:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
