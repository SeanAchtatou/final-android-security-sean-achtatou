package b.a;

import com.qihoo.dynamic.util.Md5Util;
import com.qihoo.messenger.util.QDefine;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONException;
import org.json.JSONObject;

public class fn {

    /* renamed from: a  reason: collision with root package name */
    private static final String f138a = fn.class.getName();

    /* renamed from: b  reason: collision with root package name */
    private Map<String, String> f139b;

    private static String a(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    try {
                        inputStream.close();
                        return sb.toString();
                    } catch (IOException e) {
                        fm.b(f138a, "Caught IOException in convertStreamToString()", e);
                        return null;
                    }
                } else {
                    sb.append(String.valueOf(readLine) + "\n");
                }
            } catch (IOException e2) {
                fm.b(f138a, "Caught IOException in convertStreamToString()", e2);
                try {
                    inputStream.close();
                    return null;
                } catch (IOException e3) {
                    fm.b(f138a, "Caught IOException in convertStreamToString()", e3);
                    return null;
                }
            } catch (Throwable th) {
                try {
                    inputStream.close();
                    throw th;
                } catch (IOException e4) {
                    fm.b(f138a, "Caught IOException in convertStreamToString()", e4);
                    return null;
                }
            }
        }
    }

    private JSONObject a(String str) {
        InputStream inputStream;
        int nextInt = new Random().nextInt(QDefine.ONE_SECOND);
        try {
            String property = System.getProperty("line.separator");
            if (str.length() <= 1) {
                fm.b(f138a, String.valueOf(nextInt) + ":\tInvalid baseUrl.");
                return null;
            }
            fm.a(f138a, String.valueOf(nextInt) + ":\tget: " + str);
            HttpGet httpGet = new HttpGet(str);
            if (this.f139b != null && this.f139b.size() > 0) {
                for (String next : this.f139b.keySet()) {
                    httpGet.addHeader(next, this.f139b.get(next));
                }
            }
            HttpResponse execute = new DefaultHttpClient(b()).execute(httpGet);
            if (execute.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = execute.getEntity();
                if (entity != null) {
                    InputStream content = entity.getContent();
                    Header firstHeader = execute.getFirstHeader("Content-Encoding");
                    if (firstHeader == null || !firstHeader.getValue().equalsIgnoreCase("gzip")) {
                        if (firstHeader != null) {
                            if (firstHeader.getValue().equalsIgnoreCase("deflate")) {
                                fm.a(f138a, String.valueOf(nextInt) + "  Use InflaterInputStream get data....");
                                inputStream = new InflaterInputStream(content);
                            }
                        }
                        inputStream = content;
                    } else {
                        fm.a(f138a, String.valueOf(nextInt) + "  Use GZIPInputStream get data....");
                        inputStream = new GZIPInputStream(content);
                    }
                    String a2 = a(inputStream);
                    fm.a(f138a, String.valueOf(nextInt) + ":\tresponse: " + property + a2);
                    if (a2 == null) {
                        return null;
                    }
                    return new JSONObject(a2);
                }
            } else {
                fm.c(f138a, String.valueOf(nextInt) + ":\tFailed to send message. StatusCode = " + execute.getStatusLine().getStatusCode() + fr.f142a + str);
            }
            return null;
        } catch (ClientProtocolException e) {
            fm.c(f138a, String.valueOf(nextInt) + ":\tClientProtocolException,Failed to send message." + str, e);
            return null;
        } catch (Exception e2) {
            fm.c(f138a, String.valueOf(nextInt) + ":\tIOException,Failed to send message." + str, e2);
            return null;
        }
    }

    private JSONObject a(String str, JSONObject jSONObject) {
        String jSONObject2 = jSONObject.toString();
        int nextInt = new Random().nextInt(QDefine.ONE_SECOND);
        fm.c(f138a, String.valueOf(nextInt) + ":\trequest: " + str + fr.f142a + jSONObject2);
        HttpPost httpPost = new HttpPost(str);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(b());
        try {
            if (a()) {
                byte[] a2 = fq.a("content=" + jSONObject2, Charset.defaultCharset().toString());
                httpPost.addHeader("Content-Encoding", "deflate");
                httpPost.setEntity(new InputStreamEntity(new ByteArrayInputStream(a2), (long) a2.length));
            } else {
                ArrayList arrayList = new ArrayList(1);
                arrayList.add(new BasicNameValuePair("content", jSONObject2));
                httpPost.setEntity(new UrlEncodedFormEntity(arrayList, Md5Util.DEFAULT_CHARSET));
            }
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            if (execute.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = execute.getEntity();
                if (entity == null) {
                    return null;
                }
                InputStream content = entity.getContent();
                Header firstHeader = execute.getFirstHeader("Content-Encoding");
                String a3 = a((firstHeader == null || !firstHeader.getValue().equalsIgnoreCase("deflate")) ? content : new InflaterInputStream(content));
                fm.a(f138a, String.valueOf(nextInt) + ":\tresponse: " + fr.f142a + a3);
                if (a3 == null) {
                    return null;
                }
                return new JSONObject(a3);
            }
            fm.c(f138a, String.valueOf(nextInt) + ":\tFailed to send message. StatusCode = " + execute.getStatusLine().getStatusCode() + fr.f142a + str);
            return null;
        } catch (ClientProtocolException e) {
            fm.c(f138a, String.valueOf(nextInt) + ":\tClientProtocolException,Failed to send message." + str, e);
            return null;
        } catch (IOException e2) {
            fm.c(f138a, String.valueOf(nextInt) + ":\tIOException,Failed to send message." + str, e2);
            return null;
        } catch (JSONException e3) {
            fm.c(f138a, String.valueOf(nextInt) + ":\tIOException,Failed to send message." + str, e3);
            return null;
        }
    }

    private HttpParams b() {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 20000);
        HttpProtocolParams.setUserAgent(basicHttpParams, System.getProperty("http.agent"));
        return basicHttpParams;
    }

    private void b(String str) {
        if (fr.c(str) || !(fo.c.equals(str.trim()) ^ fo.f140b.equals(str.trim()))) {
            throw new RuntimeException("验证请求方式失败[" + str + "]");
        }
    }

    public <T extends fp> T a(fo foVar, Class<T> cls) {
        String trim = foVar.c().trim();
        b(trim);
        JSONObject a2 = fo.c.equals(trim) ? a(foVar.b()) : fo.f140b.equals(trim) ? a(foVar.d, foVar.a()) : null;
        if (a2 == null) {
            return null;
        }
        try {
            return (fp) cls.getConstructor(JSONObject.class).newInstance(a2);
        } catch (SecurityException e) {
            fm.b(f138a, "SecurityException", e);
        } catch (NoSuchMethodException e2) {
            fm.b(f138a, "NoSuchMethodException", e2);
        } catch (IllegalArgumentException e3) {
            fm.b(f138a, "IllegalArgumentException", e3);
        } catch (InstantiationException e4) {
            fm.b(f138a, "InstantiationException", e4);
        } catch (IllegalAccessException e5) {
            fm.b(f138a, "IllegalAccessException", e5);
        } catch (InvocationTargetException e6) {
            fm.b(f138a, "InvocationTargetException", e6);
        }
        return null;
    }

    public boolean a() {
        return false;
    }
}
