package b.a;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;

class bq extends hh<bm> {
    private bq() {
    }

    public void a(gw gwVar, bm bmVar) {
        hd hdVar = (hd) gwVar;
        hdVar.a(bmVar.f71a.size());
        for (Map.Entry next : bmVar.f71a.entrySet()) {
            hdVar.a((String) next.getKey());
            ((bf) next.getValue()).b(hdVar);
        }
        BitSet bitSet = new BitSet();
        if (bmVar.c()) {
            bitSet.set(0);
        }
        if (bmVar.d()) {
            bitSet.set(1);
        }
        hdVar.a(bitSet, 2);
        if (bmVar.c()) {
            hdVar.a(bmVar.f72b.size());
            for (ay b2 : bmVar.f72b) {
                b2.b(hdVar);
            }
        }
        if (bmVar.d()) {
            hdVar.a(bmVar.c);
        }
    }

    public void b(gw gwVar, bm bmVar) {
        hd hdVar = (hd) gwVar;
        gv gvVar = new gv((byte) 11, (byte) 12, hdVar.s());
        bmVar.f71a = new HashMap(gvVar.c * 2);
        for (int i = 0; i < gvVar.c; i++) {
            String v = hdVar.v();
            bf bfVar = new bf();
            bfVar.a(hdVar);
            bmVar.f71a.put(v, bfVar);
        }
        bmVar.a(true);
        BitSet b2 = hdVar.b(2);
        if (b2.get(0)) {
            gu guVar = new gu((byte) 12, hdVar.s());
            bmVar.f72b = new ArrayList(guVar.f174b);
            for (int i2 = 0; i2 < guVar.f174b; i2++) {
                ay ayVar = new ay();
                ayVar.a(hdVar);
                bmVar.f72b.add(ayVar);
            }
            bmVar.b(true);
        }
        if (b2.get(1)) {
            bmVar.c = hdVar.v();
            bmVar.c(true);
        }
    }
}
