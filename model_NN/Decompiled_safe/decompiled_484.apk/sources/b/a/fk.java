package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum fk implements gb {
    VERSION(1, "version"),
    ADDRESS(2, "address"),
    SIGNATURE(3, "signature"),
    SERIAL_NUM(4, "serial_num"),
    TS_SECS(5, "ts_secs"),
    LENGTH(6, "length"),
    ENTITY(7, "entity"),
    GUID(8, "guid"),
    CHECKSUM(9, "checksum");
    
    private static final Map<String, fk> j = new HashMap();
    private final short k;
    private final String l;

    static {
        Iterator it = EnumSet.allOf(fk.class).iterator();
        while (it.hasNext()) {
            fk fkVar = (fk) it.next();
            j.put(fkVar.b(), fkVar);
        }
    }

    private fk(short s, String str) {
        this.k = s;
        this.l = str;
    }

    public short a() {
        return this.k;
    }

    public String b() {
        return this.l;
    }
}
