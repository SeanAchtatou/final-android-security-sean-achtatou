package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class ex implements fu<ex, fd>, Serializable, Cloneable {
    public static final Map<fd, gk> e;
    /* access modifiers changed from: private */
    public static final hc f = new hc("UserInfo");
    /* access modifiers changed from: private */
    public static final gt g = new gt("gender", (byte) 8, 1);
    /* access modifiers changed from: private */
    public static final gt h = new gt("age", (byte) 8, 2);
    /* access modifiers changed from: private */
    public static final gt i = new gt("id", (byte) 11, 3);
    /* access modifiers changed from: private */
    public static final gt j = new gt("source", (byte) 11, 4);
    private static final Map<Class<? extends he>, hf> k = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public ax f126a;

    /* renamed from: b  reason: collision with root package name */
    public int f127b;
    public String c;
    public String d;
    private byte l = 0;
    private fd[] m = {fd.GENDER, fd.AGE, fd.ID, fd.SOURCE};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.fd, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        k.put(hg.class, new fa());
        k.put(hh.class, new fc());
        EnumMap enumMap = new EnumMap(fd.class);
        enumMap.put((Object) fd.GENDER, (Object) new gk("gender", (byte) 2, new gj((byte) 16, ax.class)));
        enumMap.put((Object) fd.AGE, (Object) new gk("age", (byte) 2, new gl((byte) 8)));
        enumMap.put((Object) fd.ID, (Object) new gk("id", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) fd.SOURCE, (Object) new gk("source", (byte) 2, new gl((byte) 11)));
        e = Collections.unmodifiableMap(enumMap);
        gk.a(ex.class, e);
    }

    public ex a(int i2) {
        this.f127b = i2;
        b(true);
        return this;
    }

    public ex a(ax axVar) {
        this.f126a = axVar;
        return this;
    }

    public ex a(String str) {
        this.c = str;
        return this;
    }

    public void a(gw gwVar) {
        k.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z) {
        if (!z) {
            this.f126a = null;
        }
    }

    public boolean a() {
        return this.f126a != null;
    }

    public ex b(String str) {
        this.d = str;
        return this;
    }

    public void b(gw gwVar) {
        k.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z) {
        this.l = fs.a(this.l, 0, z);
    }

    public boolean b() {
        return fs.a(this.l, 0);
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public boolean c() {
        return this.c != null;
    }

    public void d(boolean z) {
        if (!z) {
            this.d = null;
        }
    }

    public boolean d() {
        return this.d != null;
    }

    public void e() {
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("UserInfo(");
        boolean z2 = true;
        if (a()) {
            sb.append("gender:");
            if (this.f126a == null) {
                sb.append("null");
            } else {
                sb.append(this.f126a);
            }
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("age:");
            sb.append(this.f127b);
            z2 = false;
        }
        if (c()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("id:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        } else {
            z = z2;
        }
        if (d()) {
            if (!z) {
                sb.append(", ");
            }
            sb.append("source:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
