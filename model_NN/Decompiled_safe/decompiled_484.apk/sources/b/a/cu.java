package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum cu implements gb {
    LAT(1, "lat"),
    LNG(2, "lng"),
    TS(3, "ts");
    
    private static final Map<String, cu> d = new HashMap();
    private final short e;
    private final String f;

    static {
        Iterator it = EnumSet.allOf(cu.class).iterator();
        while (it.hasNext()) {
            cu cuVar = (cu) it.next();
            d.put(cuVar.b(), cuVar);
        }
    }

    private cu(short s, String str) {
        this.e = s;
        this.f = str;
    }

    public short a() {
        return this.e;
    }

    public String b() {
        return this.f;
    }
}
