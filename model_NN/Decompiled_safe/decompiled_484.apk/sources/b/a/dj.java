package b.a;

import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;

public class dj extends gd<dj, dl> {

    /* renamed from: a  reason: collision with root package name */
    public static final Map<dl, gk> f99a;
    private static final hc d = new hc("PropertyValue");
    private static final gt e = new gt("string_value", (byte) 11, 1);
    private static final gt f = new gt("long_value", (byte) 10, 2);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.dl, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(dl.class);
        enumMap.put((Object) dl.STRING_VALUE, (Object) new gk("string_value", (byte) 3, new gl((byte) 11)));
        enumMap.put((Object) dl.LONG_VALUE, (Object) new gk("long_value", (byte) 3, new gl((byte) 10)));
        f99a = Collections.unmodifiableMap(enumMap);
        gk.a(dj.class, f99a);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public dl b(short s) {
        return dl.b(s);
    }

    /* access modifiers changed from: protected */
    public gt a(dl dlVar) {
        switch (dk.f100a[dlVar.ordinal()]) {
            case 1:
                return e;
            case 2:
                return f;
            default:
                throw new IllegalArgumentException("Unknown field id " + dlVar);
        }
    }

    /* access modifiers changed from: protected */
    public hc a() {
        return d;
    }

    /* access modifiers changed from: protected */
    public Object a(gw gwVar, gt gtVar) {
        dl a2 = dl.a(gtVar.c);
        if (a2 == null) {
            return null;
        }
        switch (dk.f100a[a2.ordinal()]) {
            case 1:
                if (gtVar.f172b == e.f172b) {
                    return gwVar.v();
                }
                ha.a(gwVar, gtVar.f172b);
                return null;
            case 2:
                if (gtVar.f172b == f.f172b) {
                    return Long.valueOf(gwVar.t());
                }
                ha.a(gwVar, gtVar.f172b);
                return null;
            default:
                throw new IllegalStateException("setField wasn't null, but didn't match any of the case statements!");
        }
    }

    /* access modifiers changed from: protected */
    public Object a(gw gwVar, short s) {
        dl a2 = dl.a(s);
        if (a2 != null) {
            switch (dk.f100a[a2.ordinal()]) {
                case 1:
                    return gwVar.v();
                case 2:
                    return Long.valueOf(gwVar.t());
                default:
                    throw new IllegalStateException("setField wasn't null, but didn't match any of the case statements!");
            }
        } else {
            throw new gx("Couldn't find a field with field id " + ((int) s));
        }
    }

    public void a(long j) {
        this.c = dl.LONG_VALUE;
        this.f155b = Long.valueOf(j);
    }

    public void a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.c = dl.STRING_VALUE;
        this.f155b = str;
    }

    public boolean a(dj djVar) {
        return djVar != null && b() == djVar.b() && c().equals(djVar.c());
    }

    /* access modifiers changed from: protected */
    public void c(gw gwVar) {
        switch (dk.f100a[((dl) this.c).ordinal()]) {
            case 1:
                gwVar.a((String) this.f155b);
                return;
            case 2:
                gwVar.a(((Long) this.f155b).longValue());
                return;
            default:
                throw new IllegalStateException("Cannot write union with unknown field " + this.c);
        }
    }

    /* access modifiers changed from: protected */
    public void d(gw gwVar) {
        switch (dk.f100a[((dl) this.c).ordinal()]) {
            case 1:
                gwVar.a((String) this.f155b);
                return;
            case 2:
                gwVar.a(((Long) this.f155b).longValue());
                return;
            default:
                throw new IllegalStateException("Cannot write union with unknown field " + this.c);
        }
    }

    public boolean equals(Object obj) {
        if (obj instanceof dj) {
            return a((dj) obj);
        }
        return false;
    }

    public int hashCode() {
        return 0;
    }
}
