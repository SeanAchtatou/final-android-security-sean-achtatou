package b.a;

import java.util.BitSet;

class ce extends hh<ca> {
    private ce() {
    }

    public void a(gw gwVar, ca caVar) {
        hd hdVar = (hd) gwVar;
        hdVar.a(caVar.f80b);
        hdVar.a(caVar.c);
        BitSet bitSet = new BitSet();
        if (caVar.b()) {
            bitSet.set(0);
        }
        hdVar.a(bitSet, 1);
        if (caVar.b()) {
            hdVar.a(caVar.f79a);
        }
    }

    public void b(gw gwVar, ca caVar) {
        hd hdVar = (hd) gwVar;
        caVar.f80b = hdVar.t();
        caVar.b(true);
        caVar.c = hdVar.v();
        caVar.c(true);
        if (hdVar.b(1).get(0)) {
            caVar.f79a = hdVar.v();
            caVar.a(true);
        }
    }
}
