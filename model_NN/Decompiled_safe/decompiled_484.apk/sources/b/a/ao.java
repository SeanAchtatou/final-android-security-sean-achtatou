package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum ao implements gb {
    TS(1, "ts"),
    CONTEXT(2, "context"),
    SOURCE(3, "source");
    
    private static final Map<String, ao> d = new HashMap();
    private final short e;
    private final String f;

    static {
        Iterator it = EnumSet.allOf(ao.class).iterator();
        while (it.hasNext()) {
            ao aoVar = (ao) it.next();
            d.put(aoVar.b(), aoVar);
        }
    }

    private ao(short s, String str) {
        this.e = s;
        this.f = str;
    }

    public short a() {
        return this.e;
    }

    public String b() {
        return this.f;
    }
}
