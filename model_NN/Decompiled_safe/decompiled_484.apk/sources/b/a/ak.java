package b.a;

class ak extends hg<ai> {
    private ak() {
    }

    /* renamed from: a */
    public void b(gw gwVar, ai aiVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                if (!aiVar.a()) {
                    throw new gx("Required field 'ts' was not found in serialized data! Struct: " + toString());
                }
                aiVar.c();
                return;
            }
            switch (h.c) {
                case 1:
                    if (h.f172b != 10) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        aiVar.f51a = gwVar.t();
                        aiVar.a(true);
                        break;
                    }
                case 2:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        aiVar.f52b = gwVar.v();
                        aiVar.b(true);
                        break;
                    }
                case 3:
                    if (h.f172b != 8) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        aiVar.c = ap.a(gwVar.s());
                        aiVar.c(true);
                        break;
                    }
                default:
                    ha.a(gwVar, h.f172b);
                    break;
            }
            gwVar.i();
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, ai aiVar) {
        aiVar.c();
        gwVar.a(ai.e);
        gwVar.a(ai.f);
        gwVar.a(aiVar.f51a);
        gwVar.b();
        if (aiVar.f52b != null) {
            gwVar.a(ai.g);
            gwVar.a(aiVar.f52b);
            gwVar.b();
        }
        if (aiVar.c != null && aiVar.b()) {
            gwVar.a(ai.h);
            gwVar.a(aiVar.c.a());
            gwVar.b();
        }
        gwVar.c();
        gwVar.a();
    }
}
