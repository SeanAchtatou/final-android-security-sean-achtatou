package b.a;

import java.util.BitSet;

class dy extends hh<du> {
    private dy() {
    }

    public void a(gw gwVar, du duVar) {
        hd hdVar = (hd) gwVar;
        hdVar.a(duVar.f108a);
        BitSet bitSet = new BitSet();
        if (duVar.c()) {
            bitSet.set(0);
        }
        if (duVar.e()) {
            bitSet.set(1);
        }
        hdVar.a(bitSet, 2);
        if (duVar.c()) {
            hdVar.a(duVar.f109b);
        }
        if (duVar.e()) {
            duVar.c.b(hdVar);
        }
    }

    public void b(gw gwVar, du duVar) {
        hd hdVar = (hd) gwVar;
        duVar.f108a = hdVar.s();
        duVar.a(true);
        BitSet b2 = hdVar.b(2);
        if (b2.get(0)) {
            duVar.f109b = hdVar.v();
            duVar.b(true);
        }
        if (b2.get(1)) {
            duVar.c = new bt();
            duVar.c.a(hdVar);
            duVar.c(true);
        }
    }
}
