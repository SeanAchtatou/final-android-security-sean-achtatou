package b.a;

import android.support.v4.os.EnvironmentCompat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    private final int f43a = 10;

    /* renamed from: b  reason: collision with root package name */
    private final int f44b = 20;
    private final String c;
    private List<ay> d;
    private bf e;

    public a(String str) {
        this.c = str;
    }

    private boolean g() {
        bf bfVar = this.e;
        String a2 = bfVar == null ? null : bfVar.a();
        int d2 = bfVar == null ? 0 : bfVar.d();
        String a3 = a(f());
        if (a3 == null || a3.equals(a2)) {
            return false;
        }
        if (bfVar == null) {
            bfVar = new bf();
        }
        bfVar.a(a3);
        bfVar.a(System.currentTimeMillis());
        bfVar.a(d2 + 1);
        ay ayVar = new ay();
        ayVar.a(this.c);
        ayVar.c(a3);
        ayVar.b(a2);
        ayVar.a(bfVar.b());
        if (this.d == null) {
            this.d = new ArrayList(2);
        }
        this.d.add(ayVar);
        if (this.d.size() > 10) {
            this.d.remove(0);
        }
        this.e = bfVar;
        return true;
    }

    public String a(String str) {
        if (str == null) {
            return null;
        }
        String trim = str.trim();
        if (trim.length() == 0 || "0".equals(trim) || EnvironmentCompat.MEDIA_UNKNOWN.equals(trim.toLowerCase(Locale.US))) {
            return null;
        }
        return trim;
    }

    public void a(bm bmVar) {
        this.e = bmVar.a().get("mName");
        List<ay> b2 = bmVar.b();
        if (b2 != null && b2.size() > 0) {
            if (this.d == null) {
                this.d = new ArrayList();
            }
            for (ay next : b2) {
                if (this.c.equals(next.f63a)) {
                    this.d.add(next);
                }
            }
        }
    }

    public void a(List<ay> list) {
        this.d = list;
    }

    public boolean a() {
        return g();
    }

    public String b() {
        return this.c;
    }

    public boolean c() {
        return this.e == null || this.e.d() <= 20;
    }

    public bf d() {
        return this.e;
    }

    public List<ay> e() {
        return this.d;
    }

    public abstract String f();
}
