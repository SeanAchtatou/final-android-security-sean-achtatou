package b.a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ch implements fu<ch, cn>, Serializable, Cloneable {
    public static final Map<cn, gk> e;
    /* access modifiers changed from: private */
    public static final hc f = new hc("InstantMsg");
    /* access modifiers changed from: private */
    public static final gt g = new gt("id", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final gt h = new gt("errors", (byte) 15, 2);
    /* access modifiers changed from: private */
    public static final gt i = new gt("events", (byte) 15, 3);
    /* access modifiers changed from: private */
    public static final gt j = new gt("game_events", (byte) 15, 4);
    private static final Map<Class<? extends he>, hf> k = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f83a;

    /* renamed from: b  reason: collision with root package name */
    public List<ai> f84b;
    public List<aq> c;
    public List<aq> d;
    private cn[] l = {cn.ERRORS, cn.EVENTS, cn.GAME_EVENTS};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.cn, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        k.put(hg.class, new ck());
        k.put(hh.class, new cm());
        EnumMap enumMap = new EnumMap(cn.class);
        enumMap.put((Object) cn.ID, (Object) new gk("id", (byte) 1, new gl((byte) 11)));
        enumMap.put((Object) cn.ERRORS, (Object) new gk("errors", (byte) 2, new gm((byte) 15, new go((byte) 12, ai.class))));
        enumMap.put((Object) cn.EVENTS, (Object) new gk("events", (byte) 2, new gm((byte) 15, new go((byte) 12, aq.class))));
        enumMap.put((Object) cn.GAME_EVENTS, (Object) new gk("game_events", (byte) 2, new gm((byte) 15, new go((byte) 12, aq.class))));
        e = Collections.unmodifiableMap(enumMap);
        gk.a(ch.class, e);
    }

    public ch a(String str) {
        this.f83a = str;
        return this;
    }

    public String a() {
        return this.f83a;
    }

    public void a(ai aiVar) {
        if (this.f84b == null) {
            this.f84b = new ArrayList();
        }
        this.f84b.add(aiVar);
    }

    public void a(aq aqVar) {
        if (this.c == null) {
            this.c = new ArrayList();
        }
        this.c.add(aqVar);
    }

    public void a(gw gwVar) {
        k.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z) {
        if (!z) {
            this.f83a = null;
        }
    }

    public void b(gw gwVar) {
        k.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z) {
        if (!z) {
            this.f84b = null;
        }
    }

    public boolean b() {
        return this.f84b != null;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public boolean c() {
        return this.c != null;
    }

    public void d(boolean z) {
        if (!z) {
            this.d = null;
        }
    }

    public boolean d() {
        return this.d != null;
    }

    public void e() {
        if (this.f83a == null) {
            throw new gx("Required field 'id' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("InstantMsg(");
        sb.append("id:");
        if (this.f83a == null) {
            sb.append("null");
        } else {
            sb.append(this.f83a);
        }
        if (b()) {
            sb.append(", ");
            sb.append("errors:");
            if (this.f84b == null) {
                sb.append("null");
            } else {
                sb.append(this.f84b);
            }
        }
        if (c()) {
            sb.append(", ");
            sb.append("events:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        if (d()) {
            sb.append(", ");
            sb.append("game_events:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
