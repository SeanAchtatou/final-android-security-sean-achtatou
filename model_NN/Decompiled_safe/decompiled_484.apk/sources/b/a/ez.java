package b.a;

class ez extends hg<ex> {
    private ez() {
    }

    /* renamed from: a */
    public void b(gw gwVar, ex exVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                exVar.e();
                return;
            }
            switch (h.c) {
                case 1:
                    if (h.f172b != 8) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        exVar.f126a = ax.a(gwVar.s());
                        exVar.a(true);
                        break;
                    }
                case 2:
                    if (h.f172b != 8) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        exVar.f127b = gwVar.s();
                        exVar.b(true);
                        break;
                    }
                case 3:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        exVar.c = gwVar.v();
                        exVar.c(true);
                        break;
                    }
                case 4:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        exVar.d = gwVar.v();
                        exVar.d(true);
                        break;
                    }
                default:
                    ha.a(gwVar, h.f172b);
                    break;
            }
            gwVar.i();
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, ex exVar) {
        exVar.e();
        gwVar.a(ex.f);
        if (exVar.f126a != null && exVar.a()) {
            gwVar.a(ex.g);
            gwVar.a(exVar.f126a.a());
            gwVar.b();
        }
        if (exVar.b()) {
            gwVar.a(ex.h);
            gwVar.a(exVar.f127b);
            gwVar.b();
        }
        if (exVar.c != null && exVar.c()) {
            gwVar.a(ex.i);
            gwVar.a(exVar.c);
            gwVar.b();
        }
        if (exVar.d != null && exVar.d()) {
            gwVar.a(ex.j);
            gwVar.a(exVar.d);
            gwVar.b();
        }
        gwVar.c();
        gwVar.a();
    }
}
