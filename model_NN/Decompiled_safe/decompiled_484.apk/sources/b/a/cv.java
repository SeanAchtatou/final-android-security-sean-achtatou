package b.a;

import com.qihoo360.daily.activity.LoginActivity;
import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class cv implements fu<cv, db>, Serializable, Cloneable {
    public static final Map<db, gk> l;
    /* access modifiers changed from: private */
    public static final hc m = new hc("MiscInfo");
    /* access modifiers changed from: private */
    public static final gt n = new gt("time_zone", (byte) 8, 1);
    /* access modifiers changed from: private */
    public static final gt o = new gt("language", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final gt p = new gt("country", (byte) 11, 3);
    /* access modifiers changed from: private */
    public static final gt q = new gt("latitude", (byte) 4, 4);
    /* access modifiers changed from: private */
    public static final gt r = new gt("longitude", (byte) 4, 5);
    /* access modifiers changed from: private */
    public static final gt s = new gt("carrier", (byte) 11, 6);
    /* access modifiers changed from: private */
    public static final gt t = new gt("latency", (byte) 8, 7);
    /* access modifiers changed from: private */
    public static final gt u = new gt("display_name", (byte) 11, 8);
    /* access modifiers changed from: private */
    public static final gt v = new gt("access_type", (byte) 8, 9);
    /* access modifiers changed from: private */
    public static final gt w = new gt("access_subtype", (byte) 11, 10);
    /* access modifiers changed from: private */
    public static final gt x = new gt(LoginActivity.KEY_USER_INFO, (byte) 12, 11);
    private static final Map<Class<? extends he>, hf> y = new HashMap();
    private db[] A = {db.TIME_ZONE, db.LANGUAGE, db.COUNTRY, db.LATITUDE, db.LONGITUDE, db.CARRIER, db.LATENCY, db.DISPLAY_NAME, db.ACCESS_TYPE, db.ACCESS_SUBTYPE, db.USER_INFO};

    /* renamed from: a  reason: collision with root package name */
    public int f91a;

    /* renamed from: b  reason: collision with root package name */
    public String f92b;
    public String c;
    public double d;
    public double e;
    public String f;
    public int g;
    public String h;
    public f i;
    public String j;
    public ex k;
    private byte z = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.db, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        y.put(hg.class, new cy());
        y.put(hh.class, new da());
        EnumMap enumMap = new EnumMap(db.class);
        enumMap.put((Object) db.TIME_ZONE, (Object) new gk("time_zone", (byte) 2, new gl((byte) 8)));
        enumMap.put((Object) db.LANGUAGE, (Object) new gk("language", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) db.COUNTRY, (Object) new gk("country", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) db.LATITUDE, (Object) new gk("latitude", (byte) 2, new gl((byte) 4)));
        enumMap.put((Object) db.LONGITUDE, (Object) new gk("longitude", (byte) 2, new gl((byte) 4)));
        enumMap.put((Object) db.CARRIER, (Object) new gk("carrier", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) db.LATENCY, (Object) new gk("latency", (byte) 2, new gl((byte) 8)));
        enumMap.put((Object) db.DISPLAY_NAME, (Object) new gk("display_name", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) db.ACCESS_TYPE, (Object) new gk("access_type", (byte) 2, new gj((byte) 16, f.class)));
        enumMap.put((Object) db.ACCESS_SUBTYPE, (Object) new gk("access_subtype", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) db.USER_INFO, (Object) new gk(LoginActivity.KEY_USER_INFO, (byte) 2, new go((byte) 12, ex.class)));
        l = Collections.unmodifiableMap(enumMap);
        gk.a(cv.class, l);
    }

    public cv a(int i2) {
        this.f91a = i2;
        a(true);
        return this;
    }

    public cv a(ex exVar) {
        this.k = exVar;
        return this;
    }

    public cv a(f fVar) {
        this.i = fVar;
        return this;
    }

    public cv a(String str) {
        this.f92b = str;
        return this;
    }

    public void a(gw gwVar) {
        y.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z2) {
        this.z = fs.a(this.z, 0, z2);
    }

    public boolean a() {
        return fs.a(this.z, 0);
    }

    public cv b(String str) {
        this.c = str;
        return this;
    }

    public void b(gw gwVar) {
        y.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z2) {
        if (!z2) {
            this.f92b = null;
        }
    }

    public boolean b() {
        return this.f92b != null;
    }

    public cv c(String str) {
        this.f = str;
        return this;
    }

    public void c(boolean z2) {
        if (!z2) {
            this.c = null;
        }
    }

    public boolean c() {
        return this.c != null;
    }

    public cv d(String str) {
        this.j = str;
        return this;
    }

    public void d(boolean z2) {
        this.z = fs.a(this.z, 1, z2);
    }

    public boolean d() {
        return fs.a(this.z, 1);
    }

    public void e(boolean z2) {
        this.z = fs.a(this.z, 2, z2);
    }

    public boolean e() {
        return fs.a(this.z, 2);
    }

    public void f(boolean z2) {
        if (!z2) {
            this.f = null;
        }
    }

    public boolean f() {
        return this.f != null;
    }

    public void g(boolean z2) {
        this.z = fs.a(this.z, 3, z2);
    }

    public boolean g() {
        return fs.a(this.z, 3);
    }

    public void h(boolean z2) {
        if (!z2) {
            this.h = null;
        }
    }

    public boolean h() {
        return this.h != null;
    }

    public void i(boolean z2) {
        if (!z2) {
            this.i = null;
        }
    }

    public boolean i() {
        return this.i != null;
    }

    public void j(boolean z2) {
        if (!z2) {
            this.j = null;
        }
    }

    public boolean j() {
        return this.j != null;
    }

    public void k(boolean z2) {
        if (!z2) {
            this.k = null;
        }
    }

    public boolean k() {
        return this.k != null;
    }

    public void l() {
        if (this.k != null) {
            this.k.e();
        }
    }

    public String toString() {
        boolean z2 = false;
        StringBuilder sb = new StringBuilder("MiscInfo(");
        boolean z3 = true;
        if (a()) {
            sb.append("time_zone:");
            sb.append(this.f91a);
            z3 = false;
        }
        if (b()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("language:");
            if (this.f92b == null) {
                sb.append("null");
            } else {
                sb.append(this.f92b);
            }
            z3 = false;
        }
        if (c()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("country:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
            z3 = false;
        }
        if (d()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("latitude:");
            sb.append(this.d);
            z3 = false;
        }
        if (e()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("longitude:");
            sb.append(this.e);
            z3 = false;
        }
        if (f()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("carrier:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
            z3 = false;
        }
        if (g()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("latency:");
            sb.append(this.g);
            z3 = false;
        }
        if (h()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("display_name:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
            z3 = false;
        }
        if (i()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("access_type:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
            z3 = false;
        }
        if (j()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("access_subtype:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        } else {
            z2 = z3;
        }
        if (k()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("user_info:");
            if (this.k == null) {
                sb.append("null");
            } else {
                sb.append(this.k);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
