package b.a;

import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;

class au extends hh<aq> {
    private au() {
    }

    public void a(gw gwVar, aq aqVar) {
        hd hdVar = (hd) gwVar;
        hdVar.a(aqVar.f57a);
        hdVar.a(aqVar.f58b.size());
        for (Map.Entry next : aqVar.f58b.entrySet()) {
            hdVar.a((String) next.getKey());
            ((dj) next.getValue()).b(hdVar);
        }
        hdVar.a(aqVar.e);
        BitSet bitSet = new BitSet();
        if (aqVar.a()) {
            bitSet.set(0);
        }
        if (aqVar.b()) {
            bitSet.set(1);
        }
        hdVar.a(bitSet, 2);
        if (aqVar.a()) {
            hdVar.a(aqVar.c);
        }
        if (aqVar.b()) {
            hdVar.a(aqVar.d);
        }
    }

    public void b(gw gwVar, aq aqVar) {
        hd hdVar = (hd) gwVar;
        aqVar.f57a = hdVar.v();
        aqVar.a(true);
        gv gvVar = new gv((byte) 11, (byte) 12, hdVar.s());
        aqVar.f58b = new HashMap(gvVar.c * 2);
        for (int i = 0; i < gvVar.c; i++) {
            String v = hdVar.v();
            dj djVar = new dj();
            djVar.a(hdVar);
            aqVar.f58b.put(v, djVar);
        }
        aqVar.b(true);
        aqVar.e = hdVar.t();
        aqVar.e(true);
        BitSet b2 = hdVar.b(2);
        if (b2.get(0)) {
            aqVar.c = hdVar.t();
            aqVar.c(true);
        }
        if (b2.get(1)) {
            aqVar.d = hdVar.s();
            aqVar.d(true);
        }
    }
}
