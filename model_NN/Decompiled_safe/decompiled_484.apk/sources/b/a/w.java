package b.a;

class w extends hg<u> {
    private w() {
    }

    /* renamed from: a */
    public void b(gw gwVar, u uVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                if (!uVar.a()) {
                    throw new gx("Required field 'successful_requests' was not found in serialized data! Struct: " + toString());
                } else if (!uVar.b()) {
                    throw new gx("Required field 'failed_requests' was not found in serialized data! Struct: " + toString());
                } else {
                    uVar.d();
                    return;
                }
            } else {
                switch (h.c) {
                    case 1:
                        if (h.f172b != 8) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            uVar.f228a = gwVar.s();
                            uVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.f172b != 8) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            uVar.f229b = gwVar.s();
                            uVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.f172b != 8) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            uVar.c = gwVar.s();
                            uVar.c(true);
                            break;
                        }
                    default:
                        ha.a(gwVar, h.f172b);
                        break;
                }
                gwVar.i();
            }
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, u uVar) {
        uVar.d();
        gwVar.a(u.e);
        gwVar.a(u.f);
        gwVar.a(uVar.f228a);
        gwVar.b();
        gwVar.a(u.g);
        gwVar.a(uVar.f229b);
        gwVar.b();
        if (uVar.c()) {
            gwVar.a(u.h);
            gwVar.a(uVar.c);
            gwVar.b();
        }
        gwVar.c();
        gwVar.a();
    }
}
