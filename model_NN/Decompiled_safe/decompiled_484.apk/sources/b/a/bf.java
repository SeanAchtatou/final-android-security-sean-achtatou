package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class bf implements fu<bf, bl>, Serializable, Cloneable {
    public static final Map<bl, gk> d;
    /* access modifiers changed from: private */
    public static final hc e = new hc("IdSnapshot");
    /* access modifiers changed from: private */
    public static final gt f = new gt("identity", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final gt g = new gt("ts", (byte) 10, 2);
    /* access modifiers changed from: private */
    public static final gt h = new gt("version", (byte) 8, 3);
    private static final Map<Class<? extends he>, hf> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f67a;

    /* renamed from: b  reason: collision with root package name */
    public long f68b;
    public int c;
    private byte j = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.bl, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(hg.class, new bi());
        i.put(hh.class, new bk());
        EnumMap enumMap = new EnumMap(bl.class);
        enumMap.put((Object) bl.IDENTITY, (Object) new gk("identity", (byte) 1, new gl((byte) 11)));
        enumMap.put((Object) bl.TS, (Object) new gk("ts", (byte) 1, new gl((byte) 10)));
        enumMap.put((Object) bl.VERSION, (Object) new gk("version", (byte) 1, new gl((byte) 8)));
        d = Collections.unmodifiableMap(enumMap);
        gk.a(bf.class, d);
    }

    public bf a(int i2) {
        this.c = i2;
        c(true);
        return this;
    }

    public bf a(long j2) {
        this.f68b = j2;
        b(true);
        return this;
    }

    public bf a(String str) {
        this.f67a = str;
        return this;
    }

    public String a() {
        return this.f67a;
    }

    public void a(gw gwVar) {
        i.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z) {
        if (!z) {
            this.f67a = null;
        }
    }

    public long b() {
        return this.f68b;
    }

    public void b(gw gwVar) {
        i.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z) {
        this.j = fs.a(this.j, 0, z);
    }

    public void c(boolean z) {
        this.j = fs.a(this.j, 1, z);
    }

    public boolean c() {
        return fs.a(this.j, 0);
    }

    public int d() {
        return this.c;
    }

    public boolean e() {
        return fs.a(this.j, 1);
    }

    public void f() {
        if (this.f67a == null) {
            throw new gx("Required field 'identity' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("IdSnapshot(");
        sb.append("identity:");
        if (this.f67a == null) {
            sb.append("null");
        } else {
            sb.append(this.f67a);
        }
        sb.append(", ");
        sb.append("ts:");
        sb.append(this.f68b);
        sb.append(", ");
        sb.append("version:");
        sb.append(this.c);
        sb.append(")");
        return sb.toString();
    }
}
