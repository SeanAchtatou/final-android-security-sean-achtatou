package b.a;

public class fs {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.fs.a(int, int, boolean):int
     arg types: [byte, int, boolean]
     candidates:
      b.a.fs.a(byte, int, boolean):byte
      b.a.fs.a(int, int, boolean):int */
    public static final byte a(byte b2, int i, boolean z) {
        return (byte) a((int) b2, i, z);
    }

    public static final int a(int i, int i2, boolean z) {
        return z ? (1 << i2) | i : b(i, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.fs.a(int, int):boolean
     arg types: [byte, int]
     candidates:
      b.a.fs.a(byte, int):boolean
      b.a.fs.a(int, int):boolean */
    public static final boolean a(byte b2, int i) {
        return a((int) b2, i);
    }

    public static final boolean a(int i, int i2) {
        return ((1 << i2) & i) != 0;
    }

    public static final int b(int i, int i2) {
        return ((1 << i2) ^ -1) & i;
    }
}
