package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class ej implements fu<ej, ep>, Serializable, Cloneable {
    public static final Map<ep, gk> c;
    /* access modifiers changed from: private */
    public static final hc d = new hc("Traffic");
    /* access modifiers changed from: private */
    public static final gt e = new gt("upload_traffic", (byte) 8, 1);
    /* access modifiers changed from: private */
    public static final gt f = new gt("download_traffic", (byte) 8, 2);
    private static final Map<Class<? extends he>, hf> g = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public int f118a;

    /* renamed from: b  reason: collision with root package name */
    public int f119b;
    private byte h = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.ep, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        g.put(hg.class, new em());
        g.put(hh.class, new eo());
        EnumMap enumMap = new EnumMap(ep.class);
        enumMap.put((Object) ep.UPLOAD_TRAFFIC, (Object) new gk("upload_traffic", (byte) 1, new gl((byte) 8)));
        enumMap.put((Object) ep.DOWNLOAD_TRAFFIC, (Object) new gk("download_traffic", (byte) 1, new gl((byte) 8)));
        c = Collections.unmodifiableMap(enumMap);
        gk.a(ej.class, c);
    }

    public ej a(int i) {
        this.f118a = i;
        a(true);
        return this;
    }

    public void a(gw gwVar) {
        g.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z) {
        this.h = fs.a(this.h, 0, z);
    }

    public boolean a() {
        return fs.a(this.h, 0);
    }

    public ej b(int i) {
        this.f119b = i;
        b(true);
        return this;
    }

    public void b(gw gwVar) {
        g.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z) {
        this.h = fs.a(this.h, 1, z);
    }

    public boolean b() {
        return fs.a(this.h, 1);
    }

    public void c() {
    }

    public String toString() {
        return "Traffic(" + "upload_traffic:" + this.f118a + ", " + "download_traffic:" + this.f119b + ")";
    }
}
