package b.a;

import java.util.BitSet;

class fb extends hh<ex> {
    private fb() {
    }

    public void a(gw gwVar, ex exVar) {
        hd hdVar = (hd) gwVar;
        BitSet bitSet = new BitSet();
        if (exVar.a()) {
            bitSet.set(0);
        }
        if (exVar.b()) {
            bitSet.set(1);
        }
        if (exVar.c()) {
            bitSet.set(2);
        }
        if (exVar.d()) {
            bitSet.set(3);
        }
        hdVar.a(bitSet, 4);
        if (exVar.a()) {
            hdVar.a(exVar.f126a.a());
        }
        if (exVar.b()) {
            hdVar.a(exVar.f127b);
        }
        if (exVar.c()) {
            hdVar.a(exVar.c);
        }
        if (exVar.d()) {
            hdVar.a(exVar.d);
        }
    }

    public void b(gw gwVar, ex exVar) {
        hd hdVar = (hd) gwVar;
        BitSet b2 = hdVar.b(4);
        if (b2.get(0)) {
            exVar.f126a = ax.a(hdVar.s());
            exVar.a(true);
        }
        if (b2.get(1)) {
            exVar.f127b = hdVar.s();
            exVar.b(true);
        }
        if (b2.get(2)) {
            exVar.c = hdVar.v();
            exVar.c(true);
        }
        if (b2.get(3)) {
            exVar.d = hdVar.v();
            exVar.d(true);
        }
    }
}
