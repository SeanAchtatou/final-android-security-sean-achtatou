package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class bt implements fu<bt, bz>, Serializable, Cloneable {
    public static final Map<bz, gk> d;
    /* access modifiers changed from: private */
    public static final hc e = new hc("Imprint");
    /* access modifiers changed from: private */
    public static final gt f = new gt("property", (byte) 13, 1);
    /* access modifiers changed from: private */
    public static final gt g = new gt("version", (byte) 8, 2);
    /* access modifiers changed from: private */
    public static final gt h = new gt("checksum", (byte) 11, 3);
    private static final Map<Class<? extends he>, hf> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public Map<String, ca> f75a;

    /* renamed from: b  reason: collision with root package name */
    public int f76b;
    public String c;
    private byte j = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.bz, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(hg.class, new bw());
        i.put(hh.class, new by());
        EnumMap enumMap = new EnumMap(bz.class);
        enumMap.put((Object) bz.PROPERTY, (Object) new gk("property", (byte) 1, new gn((byte) 13, new gl((byte) 11), new go((byte) 12, ca.class))));
        enumMap.put((Object) bz.VERSION, (Object) new gk("version", (byte) 1, new gl((byte) 8)));
        enumMap.put((Object) bz.CHECKSUM, (Object) new gk("checksum", (byte) 1, new gl((byte) 11)));
        d = Collections.unmodifiableMap(enumMap);
        gk.a(bt.class, d);
    }

    public bt a(int i2) {
        this.f76b = i2;
        b(true);
        return this;
    }

    public bt a(String str) {
        this.c = str;
        return this;
    }

    public Map<String, ca> a() {
        return this.f75a;
    }

    public void a(gw gwVar) {
        i.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z) {
        if (!z) {
            this.f75a = null;
        }
    }

    public int b() {
        return this.f76b;
    }

    public void b(gw gwVar) {
        i.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z) {
        this.j = fs.a(this.j, 0, z);
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public boolean c() {
        return fs.a(this.j, 0);
    }

    public String d() {
        return this.c;
    }

    public void e() {
        if (this.f75a == null) {
            throw new gx("Required field 'property' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new gx("Required field 'checksum' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Imprint(");
        sb.append("property:");
        if (this.f75a == null) {
            sb.append("null");
        } else {
            sb.append(this.f75a);
        }
        sb.append(", ");
        sb.append("version:");
        sb.append(this.f76b);
        sb.append(", ");
        sb.append("checksum:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(")");
        return sb.toString();
    }
}
