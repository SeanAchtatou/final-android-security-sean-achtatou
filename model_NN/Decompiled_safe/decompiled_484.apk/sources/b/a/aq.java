package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class aq implements fu<aq, aw>, Serializable, Cloneable {
    public static final Map<aw, gk> f;
    /* access modifiers changed from: private */
    public static final hc g = new hc("Event");
    /* access modifiers changed from: private */
    public static final gt h = new gt("name", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final gt i = new gt("properties", (byte) 13, 2);
    /* access modifiers changed from: private */
    public static final gt j = new gt("duration", (byte) 10, 3);
    /* access modifiers changed from: private */
    public static final gt k = new gt("acc", (byte) 8, 4);
    /* access modifiers changed from: private */
    public static final gt l = new gt("ts", (byte) 10, 5);
    private static final Map<Class<? extends he>, hf> m = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f57a;

    /* renamed from: b  reason: collision with root package name */
    public Map<String, dj> f58b;
    public long c;
    public int d;
    public long e;
    private byte n = 0;
    private aw[] o = {aw.DURATION, aw.ACC};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.aw, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        m.put(hg.class, new at());
        m.put(hh.class, new av());
        EnumMap enumMap = new EnumMap(aw.class);
        enumMap.put((Object) aw.NAME, (Object) new gk("name", (byte) 1, new gl((byte) 11)));
        enumMap.put((Object) aw.PROPERTIES, (Object) new gk("properties", (byte) 1, new gn((byte) 13, new gl((byte) 11), new go((byte) 12, dj.class))));
        enumMap.put((Object) aw.DURATION, (Object) new gk("duration", (byte) 2, new gl((byte) 10)));
        enumMap.put((Object) aw.ACC, (Object) new gk("acc", (byte) 2, new gl((byte) 8)));
        enumMap.put((Object) aw.TS, (Object) new gk("ts", (byte) 1, new gl((byte) 10)));
        f = Collections.unmodifiableMap(enumMap);
        gk.a(aq.class, f);
    }

    public aq a(int i2) {
        this.d = i2;
        d(true);
        return this;
    }

    public aq a(long j2) {
        this.c = j2;
        c(true);
        return this;
    }

    public aq a(String str) {
        this.f57a = str;
        return this;
    }

    public aq a(Map<String, dj> map) {
        this.f58b = map;
        return this;
    }

    public void a(gw gwVar) {
        m.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z) {
        if (!z) {
            this.f57a = null;
        }
    }

    public boolean a() {
        return fs.a(this.n, 0);
    }

    public aq b(long j2) {
        this.e = j2;
        e(true);
        return this;
    }

    public void b(gw gwVar) {
        m.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z) {
        if (!z) {
            this.f58b = null;
        }
    }

    public boolean b() {
        return fs.a(this.n, 1);
    }

    public void c(boolean z) {
        this.n = fs.a(this.n, 0, z);
    }

    public boolean c() {
        return fs.a(this.n, 2);
    }

    public void d() {
        if (this.f57a == null) {
            throw new gx("Required field 'name' was not present! Struct: " + toString());
        } else if (this.f58b == null) {
            throw new gx("Required field 'properties' was not present! Struct: " + toString());
        }
    }

    public void d(boolean z) {
        this.n = fs.a(this.n, 1, z);
    }

    public void e(boolean z) {
        this.n = fs.a(this.n, 2, z);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Event(");
        sb.append("name:");
        if (this.f57a == null) {
            sb.append("null");
        } else {
            sb.append(this.f57a);
        }
        sb.append(", ");
        sb.append("properties:");
        if (this.f58b == null) {
            sb.append("null");
        } else {
            sb.append(this.f58b);
        }
        if (a()) {
            sb.append(", ");
            sb.append("duration:");
            sb.append(this.c);
        }
        if (b()) {
            sb.append(", ");
            sb.append("acc:");
            sb.append(this.d);
        }
        sb.append(", ");
        sb.append("ts:");
        sb.append(this.e);
        sb.append(")");
        return sb.toString();
    }
}
