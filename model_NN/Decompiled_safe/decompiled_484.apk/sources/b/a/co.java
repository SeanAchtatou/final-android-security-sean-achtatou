package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class co implements fu<co, cu>, Serializable, Cloneable {
    public static final Map<cu, gk> d;
    /* access modifiers changed from: private */
    public static final hc e = new hc("Location");
    /* access modifiers changed from: private */
    public static final gt f = new gt("lat", (byte) 4, 1);
    /* access modifiers changed from: private */
    public static final gt g = new gt("lng", (byte) 4, 2);
    /* access modifiers changed from: private */
    public static final gt h = new gt("ts", (byte) 10, 3);
    private static final Map<Class<? extends he>, hf> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public double f87a;

    /* renamed from: b  reason: collision with root package name */
    public double f88b;
    public long c;
    private byte j;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.cu, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(hg.class, new cr());
        i.put(hh.class, new ct());
        EnumMap enumMap = new EnumMap(cu.class);
        enumMap.put((Object) cu.LAT, (Object) new gk("lat", (byte) 1, new gl((byte) 4)));
        enumMap.put((Object) cu.LNG, (Object) new gk("lng", (byte) 1, new gl((byte) 4)));
        enumMap.put((Object) cu.TS, (Object) new gk("ts", (byte) 1, new gl((byte) 10)));
        d = Collections.unmodifiableMap(enumMap);
        gk.a(co.class, d);
    }

    public co() {
        this.j = 0;
    }

    public co(double d2, double d3, long j2) {
        this();
        this.f87a = d2;
        a(true);
        this.f88b = d3;
        b(true);
        this.c = j2;
        c(true);
    }

    public void a(gw gwVar) {
        i.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z) {
        this.j = fs.a(this.j, 0, z);
    }

    public boolean a() {
        return fs.a(this.j, 0);
    }

    public void b(gw gwVar) {
        i.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z) {
        this.j = fs.a(this.j, 1, z);
    }

    public boolean b() {
        return fs.a(this.j, 1);
    }

    public void c(boolean z) {
        this.j = fs.a(this.j, 2, z);
    }

    public boolean c() {
        return fs.a(this.j, 2);
    }

    public void d() {
    }

    public String toString() {
        return "Location(" + "lat:" + this.f87a + ", " + "lng:" + this.f88b + ", " + "ts:" + this.c + ")";
    }
}
