package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum aa implements gb {
    SUCCESSFUL_REQUESTS(1, "successful_requests"),
    FAILED_REQUESTS(2, "failed_requests"),
    LAST_REQUEST_SPENT_MS(3, "last_request_spent_ms");
    
    private static final Map<String, aa> d = new HashMap();
    private final short e;
    private final String f;

    static {
        Iterator it = EnumSet.allOf(aa.class).iterator();
        while (it.hasNext()) {
            aa aaVar = (aa) it.next();
            d.put(aaVar.b(), aaVar);
        }
    }

    private aa(short s, String str) {
        this.e = s;
        this.f = str;
    }

    public short a() {
        return this.e;
    }

    public String b() {
        return this.f;
    }
}
