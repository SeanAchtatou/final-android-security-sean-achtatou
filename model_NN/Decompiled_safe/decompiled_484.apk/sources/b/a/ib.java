package b.a;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import com.f.a.a;
import com.f.a.o;
import com.qihoo.dynamic.util.Md5Util;
import com.qihoo.messenger.util.QHeader;
import java.net.URLEncoder;

public class ib {

    /* renamed from: a  reason: collision with root package name */
    private final int f212a = 1;

    /* renamed from: b  reason: collision with root package name */
    private String f213b;
    private String c = "10.0.0.172";
    private int d = 80;
    private Context e;
    private Cif f;
    private hn g;

    public ib(Context context) {
        this.e = context;
        this.g = hp.b(context);
        this.f213b = a(context);
    }

    private String a(Context context) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(QHeader.os);
        stringBuffer.append("/");
        stringBuffer.append("5.2.4");
        stringBuffer.append(" ");
        try {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append(fl.p(context));
            stringBuffer2.append("/");
            stringBuffer2.append(fl.b(context));
            stringBuffer2.append(" ");
            stringBuffer2.append(Build.MODEL);
            stringBuffer2.append("/");
            stringBuffer2.append(Build.VERSION.RELEASE);
            stringBuffer2.append(" ");
            stringBuffer2.append(fr.a(a.a(context)));
            stringBuffer.append(URLEncoder.encode(stringBuffer2.toString(), Md5Util.DEFAULT_CHARSET));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return stringBuffer.toString();
    }

    private boolean a() {
        String extraInfo;
        if (this.e.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", this.e.getPackageName()) != 0) {
            return false;
        }
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.e.getSystemService("connectivity")).getActiveNetworkInfo();
            if (!(activeNetworkInfo == null || activeNetworkInfo.getType() == 1 || (extraInfo = activeNetworkInfo.getExtraInfo()) == null || (!extraInfo.equals("cmwap") && !extraInfo.equals("3gwap") && !extraInfo.equals("uniwap")))) {
                return true;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return false;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] a(byte[] r9, java.lang.String r10) {
        /*
            r8 = this;
            r1 = 0
            org.apache.http.client.methods.HttpPost r0 = new org.apache.http.client.methods.HttpPost
            r0.<init>(r10)
            org.apache.http.params.BasicHttpParams r2 = new org.apache.http.params.BasicHttpParams
            r2.<init>()
            r3 = 10000(0x2710, float:1.4013E-41)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r2, r3)
            r3 = 30000(0x7530, float:4.2039E-41)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r2, r3)
            org.apache.http.impl.client.DefaultHttpClient r3 = new org.apache.http.impl.client.DefaultHttpClient
            r3.<init>(r2)
            java.lang.String r2 = "X-Umeng-Sdk"
            java.lang.String r4 = r8.f213b
            r0.addHeader(r2, r4)
            java.lang.String r2 = "Msg-Type"
            java.lang.String r4 = "envelope"
            r0.addHeader(r2, r4)
            boolean r2 = r8.a()     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            if (r2 == 0) goto L_0x0040
            org.apache.http.HttpHost r2 = new org.apache.http.HttpHost     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            java.lang.String r4 = r8.c     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            int r5 = r8.d     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            r2.<init>(r4, r5)     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            org.apache.http.params.HttpParams r4 = r3.getParams()     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            java.lang.String r5 = "http.route.default-proxy"
            r4.setParameter(r5, r2)     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
        L_0x0040:
            org.apache.http.entity.InputStreamEntity r2 = new org.apache.http.entity.InputStreamEntity     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            r4.<init>(r9)     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            int r5 = r9.length     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            long r6 = (long) r5     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            r2.<init>(r4, r6)     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            r0.setEntity(r2)     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            b.a.if r2 = r8.f     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            if (r2 == 0) goto L_0x0058
            b.a.if r2 = r8.f     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            r2.d()     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
        L_0x0058:
            org.apache.http.HttpResponse r0 = r3.execute(r0)     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            b.a.if r2 = r8.f     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            if (r2 == 0) goto L_0x0065
            b.a.if r2 = r8.f     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            r2.e()     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
        L_0x0065:
            org.apache.http.StatusLine r2 = r0.getStatusLine()     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            int r2 = r2.getStatusCode()     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            java.lang.String r3 = "MobclickAgent"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            r4.<init>()     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            java.lang.String r5 = "status code : "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            java.lang.String r2 = r2.toString()     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            b.a.fm.a(r3, r2)     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            org.apache.http.StatusLine r2 = r0.getStatusLine()     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            int r2 = r2.getStatusCode()     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            r3 = 200(0xc8, float:2.8E-43)
            if (r2 != r3) goto L_0x00cc
            java.lang.String r2 = "MobclickAgent"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            r3.<init>()     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            java.lang.String r4 = "Sent message to "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            java.lang.StringBuilder r3 = r3.append(r10)     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            java.lang.String r3 = r3.toString()     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            b.a.fm.a(r2, r3)     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            org.apache.http.HttpEntity r0 = r0.getEntity()     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            if (r0 == 0) goto L_0x00ca
            java.io.InputStream r2 = r0.getContent()     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            byte[] r0 = b.a.fr.b(r2)     // Catch:{ all -> 0x00bb }
            b.a.fr.c(r2)     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
        L_0x00ba:
            return r0
        L_0x00bb:
            r0 = move-exception
            b.a.fr.c(r2)     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
            throw r0     // Catch:{ ClientProtocolException -> 0x00c0, IOException -> 0x00ce }
        L_0x00c0:
            r0 = move-exception
            java.lang.String r2 = "MobclickAgent"
            java.lang.String r3 = "ClientProtocolException,Failed to send message."
            b.a.fm.b(r2, r3, r0)
            r0 = r1
            goto L_0x00ba
        L_0x00ca:
            r0 = r1
            goto L_0x00ba
        L_0x00cc:
            r0 = r1
            goto L_0x00ba
        L_0x00ce:
            r0 = move-exception
            java.lang.String r2 = "MobclickAgent"
            java.lang.String r3 = "IOException,Failed to send message."
            b.a.fm.b(r2, r3, r0)
            r0 = r1
            goto L_0x00ba
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.ib.a(byte[], java.lang.String):byte[]");
    }

    private int b(byte[] bArr) {
        du duVar = new du();
        try {
            new fz(new gq()).a(duVar, bArr);
            if (duVar.f108a == 1) {
                this.g.b(duVar.d());
                this.g.c();
            }
            fm.a("MobclickAgent", "send log:" + duVar.b());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return duVar.f108a == 1 ? 2 : 3;
    }

    public int a(byte[] bArr) {
        byte[] bArr2 = null;
        int i = 0;
        while (true) {
            if (i >= o.f863a.length) {
                break;
            }
            bArr2 = a(bArr, o.f863a[i]);
            if (bArr2 == null) {
                if (this.f != null) {
                    this.f.c();
                }
                i++;
            } else if (this.f != null) {
                this.f.b();
            }
        }
        if (bArr2 == null) {
            return 1;
        }
        return b(bArr2);
    }

    public void a(Cif ifVar) {
        this.f = ifVar;
    }
}
