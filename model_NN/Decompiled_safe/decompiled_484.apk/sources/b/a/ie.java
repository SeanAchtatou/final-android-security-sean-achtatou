package b.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.f.a.a;
import com.qihoo.messenger.util.QDefine;
import java.util.Arrays;
import java.util.List;

public class ie {

    /* renamed from: a  reason: collision with root package name */
    private final String f214a = "a_start_time";

    /* renamed from: b  reason: collision with root package name */
    private final String f215b = "a_end_time";

    private String a(Context context, SharedPreferences sharedPreferences) {
        hs a2 = hs.a(context);
        String b2 = b(context);
        e a3 = a(context);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("session_id", b2);
        edit.putLong("session_start_time", System.currentTimeMillis());
        edit.putLong("session_end_time", 0);
        edit.commit();
        if (a3 != null) {
            a2.a(a3);
        } else {
            a2.a((hz) null);
        }
        return b2;
    }

    private void a(SharedPreferences sharedPreferences) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.remove("session_start_time");
        edit.remove("session_end_time");
        edit.remove("session_id");
        edit.remove("a_start_time");
        edit.remove("a_end_time");
        edit.putString("activities", "");
        edit.commit();
    }

    private boolean b(SharedPreferences sharedPreferences) {
        long j = sharedPreferences.getLong("a_start_time", 0);
        long j2 = sharedPreferences.getLong("a_end_time", 0);
        long currentTimeMillis = System.currentTimeMillis();
        if (j == 0 || currentTimeMillis - j >= a.n) {
            return currentTimeMillis - j2 > a.n;
        }
        fm.b("MobclickAgent", "onResume called before onPause");
        return false;
    }

    public e a(Context context) {
        SharedPreferences a2 = id.a(context);
        String string = a2.getString("session_id", null);
        if (string == null) {
            return null;
        }
        long j = a2.getLong("session_start_time", 0);
        long j2 = a2.getLong("session_end_time", 0);
        long j3 = 0;
        if (j2 != 0) {
            j3 = j2 - j;
            if (Math.abs(j3) > QDefine.ONE_DAY) {
                j3 = 0;
            }
        }
        e eVar = new e();
        eVar.a(string);
        eVar.a(j);
        eVar.b(j2);
        eVar.c(j3);
        double[] a3 = a.a();
        if (a3 != null) {
            co coVar = new co(a3[0], a3[1], System.currentTimeMillis());
            if (eVar.f()) {
                eVar.a(coVar);
            } else {
                eVar.b(Arrays.asList(coVar));
            }
        }
        ej a4 = ih.a(context);
        if (a4 != null) {
            eVar.a(a4);
        }
        List<dc> a5 = ii.a(a2);
        if (a5 != null && a5.size() > 0) {
            eVar.a(a5);
        }
        a(a2);
        return eVar;
    }

    public String b(Context context) {
        String c = fl.c(context);
        String a2 = a.a(context);
        long currentTimeMillis = System.currentTimeMillis();
        if (a2 == null) {
            throw new RuntimeException("Appkey is null or empty, Please check AndroidManifest.xml");
        }
        StringBuilder sb = new StringBuilder();
        sb.append(currentTimeMillis).append(a2).append(c);
        return fr.a(sb.toString());
    }

    public void c(Context context) {
        SharedPreferences a2 = id.a(context);
        if (a2 != null) {
            if (b(a2)) {
                fm.a("MobclickAgent", "Start new session: " + a(context, a2));
            } else {
                fm.a("MobclickAgent", "Extend current session: " + a2.getString("session_id", null));
            }
            SharedPreferences.Editor edit = a2.edit();
            edit.putLong("a_start_time", System.currentTimeMillis());
            edit.putLong("a_end_time", 0);
            edit.commit();
        }
    }

    public void d(Context context) {
        SharedPreferences a2 = id.a(context);
        if (a2 != null) {
            if (a2.getLong("a_start_time", 0) != 0 || !a.j) {
                long currentTimeMillis = System.currentTimeMillis();
                SharedPreferences.Editor edit = a2.edit();
                edit.putLong("a_start_time", 0);
                edit.putLong("a_end_time", currentTimeMillis);
                edit.putLong("session_end_time", currentTimeMillis);
                edit.commit();
                return;
            }
            fm.b("MobclickAgent", "onPause called before onResume");
        }
    }
}
