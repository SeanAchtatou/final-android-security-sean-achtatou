package b.a;

import android.content.Context;
import android.os.Environment;
import java.io.File;
import java.io.FileInputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class hq extends a {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f195a = Pattern.compile("UTDID\">([^<]+)");

    /* renamed from: b  reason: collision with root package name */
    private Context f196b;

    public hq(Context context) {
        super("utdid");
        this.f196b = context;
    }

    private String b(String str) {
        if (str == null) {
            return null;
        }
        Matcher matcher = f195a.matcher(str);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    private String g() {
        FileInputStream fileInputStream;
        File h = h();
        if (h == null || !h.exists()) {
            return null;
        }
        try {
            fileInputStream = new FileInputStream(h);
            String b2 = b(fr.a(fileInputStream));
            fr.c(fileInputStream);
            return b2;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } catch (Throwable th) {
            fr.c(fileInputStream);
            throw th;
        }
    }

    private File h() {
        if (!fl.a(this.f196b, "android.permission.WRITE_EXTERNAL_STORAGE") || !Environment.getExternalStorageState().equals("mounted")) {
            return null;
        }
        try {
            return new File(Environment.getExternalStorageDirectory().getCanonicalPath(), ".UTSystemConfig/Global/Alvin2.xml");
        } catch (Exception e) {
            return null;
        }
    }

    public String f() {
        return g();
    }
}
