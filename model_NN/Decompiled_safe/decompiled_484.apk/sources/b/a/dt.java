package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum dt implements gb {
    HEIGHT(1, "height"),
    WIDTH(2, "width");
    
    private static final Map<String, dt> c = new HashMap();
    private final short d;
    private final String e;

    static {
        Iterator it = EnumSet.allOf(dt.class).iterator();
        while (it.hasNext()) {
            dt dtVar = (dt) it.next();
            c.put(dtVar.b(), dtVar);
        }
    }

    private dt(short s, String str) {
        this.d = s;
        this.e = str;
    }

    public short a() {
        return this.d;
    }

    public String b() {
        return this.e;
    }
}
