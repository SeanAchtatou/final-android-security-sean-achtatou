package b.a;

import java.util.BitSet;

class bc extends hh<ay> {
    private bc() {
    }

    public void a(gw gwVar, ay ayVar) {
        hd hdVar = (hd) gwVar;
        hdVar.a(ayVar.f63a);
        hdVar.a(ayVar.c);
        hdVar.a(ayVar.d);
        BitSet bitSet = new BitSet();
        if (ayVar.a()) {
            bitSet.set(0);
        }
        hdVar.a(bitSet, 1);
        if (ayVar.a()) {
            hdVar.a(ayVar.f64b);
        }
    }

    public void b(gw gwVar, ay ayVar) {
        hd hdVar = (hd) gwVar;
        ayVar.f63a = hdVar.v();
        ayVar.a(true);
        ayVar.c = hdVar.v();
        ayVar.c(true);
        ayVar.d = hdVar.t();
        ayVar.d(true);
        if (hdVar.b(1).get(0)) {
            ayVar.f64b = hdVar.v();
            ayVar.b(true);
        }
    }
}
