package b.a;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class b extends aq implements hz {
    public b(String str, Map<String, Object> map, long j, int i) {
        a(str);
        b(System.currentTimeMillis());
        if (map.size() > 0) {
            a(b(map));
        }
        a(i <= 0 ? 1 : i);
        if (j > 0) {
            a(j);
        }
    }

    private HashMap<String, dj> b(Map<String, Object> map) {
        Iterator<Map.Entry<String, Object>> it = map.entrySet().iterator();
        HashMap<String, dj> hashMap = new HashMap<>();
        int i = 0;
        while (i < 10 && it.hasNext()) {
            Map.Entry next = it.next();
            dj djVar = new dj();
            Object value = next.getValue();
            if (value instanceof String) {
                djVar.a((String) value);
            } else if (value instanceof Long) {
                djVar.a(((Long) value).longValue());
            } else if (value instanceof Integer) {
                djVar.a(((Integer) value).longValue());
            } else if (value instanceof Float) {
                djVar.a(((Float) value).longValue());
            } else if (value instanceof Double) {
                djVar.a(((Double) value).longValue());
            }
            if (djVar.d()) {
                hashMap.put(next.getKey(), djVar);
                i++;
            }
        }
        return hashMap;
    }

    public void a(eq eqVar, String str) {
        ch chVar;
        if (eqVar.b() > 0) {
            Iterator<ch> it = eqVar.c().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                chVar = it.next();
                if (str.equals(chVar.a())) {
                    break;
                }
            }
        }
        chVar = null;
        if (chVar == null) {
            chVar = new ch();
            chVar.a(str);
            eqVar.a(chVar);
        }
        chVar.a(this);
    }
}
