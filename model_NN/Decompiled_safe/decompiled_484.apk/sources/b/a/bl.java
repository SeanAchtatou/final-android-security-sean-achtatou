package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum bl implements gb {
    IDENTITY(1, "identity"),
    TS(2, "ts"),
    VERSION(3, "version");
    
    private static final Map<String, bl> d = new HashMap();
    private final short e;
    private final String f;

    static {
        Iterator it = EnumSet.allOf(bl.class).iterator();
        while (it.hasNext()) {
            bl blVar = (bl) it.next();
            d.put(blVar.b(), blVar);
        }
    }

    private bl(short s, String str) {
        this.e = s;
        this.f = str;
    }

    public short a() {
        return this.e;
    }

    public String b() {
        return this.f;
    }
}
