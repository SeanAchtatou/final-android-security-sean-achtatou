package b.a;

import java.util.ArrayList;

class es extends hg<eq> {
    private es() {
    }

    /* renamed from: a */
    public void b(gw gwVar, eq eqVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                eqVar.i();
                return;
            }
            switch (h.c) {
                case 1:
                    if (h.f172b != 12) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        eqVar.f122a = new u();
                        eqVar.f122a.a(gwVar);
                        eqVar.a(true);
                        break;
                    }
                case 2:
                    if (h.f172b != 12) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        eqVar.f123b = new n();
                        eqVar.f123b.a(gwVar);
                        eqVar.b(true);
                        break;
                    }
                case 3:
                    if (h.f172b != 12) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        eqVar.c = new ab();
                        eqVar.c.a(gwVar);
                        eqVar.c(true);
                        break;
                    }
                case 4:
                    if (h.f172b != 12) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        eqVar.d = new cv();
                        eqVar.d.a(gwVar);
                        eqVar.d(true);
                        break;
                    }
                case 5:
                    if (h.f172b != 12) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        eqVar.e = new g();
                        eqVar.e.a(gwVar);
                        eqVar.e(true);
                        break;
                    }
                case 6:
                    if (h.f172b != 15) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        gu l = gwVar.l();
                        eqVar.f = new ArrayList(l.f174b);
                        for (int i = 0; i < l.f174b; i++) {
                            ch chVar = new ch();
                            chVar.a(gwVar);
                            eqVar.f.add(chVar);
                        }
                        gwVar.m();
                        eqVar.f(true);
                        break;
                    }
                case 7:
                    if (h.f172b != 15) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        gu l2 = gwVar.l();
                        eqVar.g = new ArrayList(l2.f174b);
                        for (int i2 = 0; i2 < l2.f174b; i2++) {
                            ec ecVar = new ec();
                            ecVar.a(gwVar);
                            eqVar.g.add(ecVar);
                        }
                        gwVar.m();
                        eqVar.g(true);
                        break;
                    }
                case 8:
                    if (h.f172b != 12) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        eqVar.h = new bt();
                        eqVar.h.a(gwVar);
                        eqVar.h(true);
                        break;
                    }
                case 9:
                    if (h.f172b != 12) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        eqVar.i = new bm();
                        eqVar.i.a(gwVar);
                        eqVar.i(true);
                        break;
                    }
                default:
                    ha.a(gwVar, h.f172b);
                    break;
            }
            gwVar.i();
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, eq eqVar) {
        eqVar.i();
        gwVar.a(eq.k);
        if (eqVar.f122a != null) {
            gwVar.a(eq.l);
            eqVar.f122a.b(gwVar);
            gwVar.b();
        }
        if (eqVar.f123b != null) {
            gwVar.a(eq.m);
            eqVar.f123b.b(gwVar);
            gwVar.b();
        }
        if (eqVar.c != null) {
            gwVar.a(eq.n);
            eqVar.c.b(gwVar);
            gwVar.b();
        }
        if (eqVar.d != null) {
            gwVar.a(eq.o);
            eqVar.d.b(gwVar);
            gwVar.b();
        }
        if (eqVar.e != null && eqVar.a()) {
            gwVar.a(eq.p);
            eqVar.e.b(gwVar);
            gwVar.b();
        }
        if (eqVar.f != null && eqVar.d()) {
            gwVar.a(eq.q);
            gwVar.a(new gu((byte) 12, eqVar.f.size()));
            for (ch b2 : eqVar.f) {
                b2.b(gwVar);
            }
            gwVar.e();
            gwVar.b();
        }
        if (eqVar.g != null && eqVar.f()) {
            gwVar.a(eq.r);
            gwVar.a(new gu((byte) 12, eqVar.g.size()));
            for (ec b3 : eqVar.g) {
                b3.b(gwVar);
            }
            gwVar.e();
            gwVar.b();
        }
        if (eqVar.h != null && eqVar.g()) {
            gwVar.a(eq.s);
            eqVar.h.b(gwVar);
            gwVar.b();
        }
        if (eqVar.i != null && eqVar.h()) {
            gwVar.a(eq.t);
            eqVar.i.b(gwVar);
            gwVar.b();
        }
        gwVar.c();
        gwVar.a();
    }
}
