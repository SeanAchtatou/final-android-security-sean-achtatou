package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum ah implements gb {
    DEVICE_ID(1, "device_id"),
    IDMD5(2, "idmd5"),
    MAC_ADDRESS(3, "mac_address"),
    OPEN_UDID(4, "open_udid"),
    MODEL(5, "model"),
    CPU(6, "cpu"),
    OS(7, "os"),
    OS_VERSION(8, "os_version"),
    RESOLUTION(9, "resolution"),
    IS_JAILBROKEN(10, "is_jailbroken"),
    IS_PIRATED(11, "is_pirated"),
    DEVICE_BOARD(12, "device_board"),
    DEVICE_BRAND(13, "device_brand"),
    DEVICE_MANUTIME(14, "device_manutime"),
    DEVICE_MANUFACTURER(15, "device_manufacturer"),
    DEVICE_MANUID(16, "device_manuid"),
    DEVICE_NAME(17, "device_name");
    
    private static final Map<String, ah> r = new HashMap();
    private final short s;
    private final String t;

    static {
        Iterator it = EnumSet.allOf(ah.class).iterator();
        while (it.hasNext()) {
            ah ahVar = (ah) it.next();
            r.put(ahVar.b(), ahVar);
        }
    }

    private ah(short s2, String str) {
        this.s = s2;
        this.t = str;
    }

    public short a() {
        return this.s;
    }

    public String b() {
        return this.t;
    }
}
