package b.a;

public class gt {

    /* renamed from: a  reason: collision with root package name */
    public final String f171a;

    /* renamed from: b  reason: collision with root package name */
    public final byte f172b;
    public final short c;

    public gt() {
        this("", (byte) 0, 0);
    }

    public gt(String str, byte b2, short s) {
        this.f171a = str;
        this.f172b = b2;
        this.c = s;
    }

    public String toString() {
        return "<TField name:'" + this.f171a + "' type:" + ((int) this.f172b) + " field-id:" + ((int) this.c) + ">";
    }
}
