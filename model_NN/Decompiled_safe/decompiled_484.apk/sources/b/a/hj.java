package b.a;

public final class hj extends hk {

    /* renamed from: a  reason: collision with root package name */
    private byte[] f186a;

    /* renamed from: b  reason: collision with root package name */
    private int f187b;
    private int c;

    public int a(byte[] bArr, int i, int i2) {
        int d = d();
        if (i2 > d) {
            i2 = d;
        }
        if (i2 > 0) {
            System.arraycopy(this.f186a, this.f187b, bArr, i, i2);
            a(i2);
        }
        return i2;
    }

    public void a() {
        this.f186a = null;
    }

    public void a(int i) {
        this.f187b += i;
    }

    public void a(byte[] bArr) {
        c(bArr, 0, bArr.length);
    }

    public void b(byte[] bArr, int i, int i2) {
        throw new UnsupportedOperationException("No writing allowed!");
    }

    public byte[] b() {
        return this.f186a;
    }

    public int c() {
        return this.f187b;
    }

    public void c(byte[] bArr, int i, int i2) {
        this.f186a = bArr;
        this.f187b = i;
        this.c = i + i2;
    }

    public int d() {
        return this.c - this.f187b;
    }
}
