package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum m implements gb {
    TS(1, "ts");
    

    /* renamed from: b  reason: collision with root package name */
    private static final Map<String, m> f223b = new HashMap();
    private final short c;
    private final String d;

    static {
        Iterator it = EnumSet.allOf(m.class).iterator();
        while (it.hasNext()) {
            m mVar = (m) it.next();
            f223b.put(mVar.b(), mVar);
        }
    }

    private m(short s, String str) {
        this.c = s;
        this.d = str;
    }

    public short a() {
        return this.c;
    }

    public String b() {
        return this.d;
    }
}
