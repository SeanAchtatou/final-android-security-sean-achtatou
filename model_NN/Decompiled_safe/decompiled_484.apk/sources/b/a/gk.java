package b.a;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class gk implements Serializable {
    private static Map<Class<? extends fu>, Map<? extends gb, gk>> d = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public final String f157a;

    /* renamed from: b  reason: collision with root package name */
    public final byte f158b;
    public final gl c;

    public gk(String str, byte b2, gl glVar) {
        this.f157a = str;
        this.f158b = b2;
        this.c = glVar;
    }

    public static void a(Class<? extends fu> cls, Map<? extends gb, gk> map) {
        d.put(cls, map);
    }
}
