package b.a;

import android.content.Context;
import com.qihoo.messenger.util.QDefine;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class gy {

    /* renamed from: a  reason: collision with root package name */
    private final String f178a = "umeng_it.cache";

    /* renamed from: b  reason: collision with root package name */
    private File f179b;
    private bm c = null;
    private long d;
    private long e;
    private Set<a> f = new HashSet();

    public gy(Context context) {
        this.f179b = new File(context.getFilesDir(), "umeng_it.cache");
        this.e = QDefine.ONE_DAY;
    }

    private void a(bm bmVar) {
        byte[] a2;
        if (bmVar != null) {
            try {
                synchronized (this) {
                    a2 = new gc().a(bmVar);
                }
                if (a2 != null) {
                    fr.a(this.f179b, a2);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    private void f() {
        bm bmVar = new bm();
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        for (a next : this.f) {
            if (next.c()) {
                if (next.d() != null) {
                    hashMap.put(next.b(), next.d());
                }
                if (next.e() != null && !next.e().isEmpty()) {
                    arrayList.addAll(next.e());
                }
            }
        }
        bmVar.a(arrayList);
        bmVar.a(hashMap);
        synchronized (this) {
            this.c = bmVar;
        }
    }

    private bm g() {
        FileInputStream fileInputStream;
        Throwable th;
        if (!this.f179b.exists()) {
            return null;
        }
        try {
            fileInputStream = new FileInputStream(this.f179b);
            try {
                byte[] b2 = fr.b(fileInputStream);
                bm bmVar = new bm();
                new fz().a(bmVar, b2);
                fr.c(fileInputStream);
                return bmVar;
            } catch (Exception e2) {
                e = e2;
                try {
                    e.printStackTrace();
                    fr.c(fileInputStream);
                    return null;
                } catch (Throwable th2) {
                    th = th2;
                    fr.c(fileInputStream);
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            fileInputStream = null;
            e.printStackTrace();
            fr.c(fileInputStream);
            return null;
        } catch (Throwable th3) {
            fileInputStream = null;
            th = th3;
            fr.c(fileInputStream);
            throw th;
        }
    }

    public void a() {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.d >= this.e) {
            boolean z = false;
            for (a next : this.f) {
                if (!next.c()) {
                    z = true;
                } else {
                    z = next.a() ? true : z;
                }
            }
            if (z) {
                f();
                e();
            }
            this.d = currentTimeMillis;
        }
    }

    public void a(a aVar) {
        this.f.add(aVar);
    }

    public bm b() {
        return this.c;
    }

    public void c() {
        boolean z = false;
        for (a next : this.f) {
            if (next.c()) {
                if (next.e() != null && !next.e().isEmpty()) {
                    next.a((List<ay>) null);
                    z = true;
                }
                z = z;
            }
        }
        if (z) {
            this.c.b(false);
            e();
        }
    }

    public void d() {
        bm g = g();
        if (g != null) {
            ArrayList<a> arrayList = new ArrayList<>(this.f.size());
            synchronized (this) {
                this.c = g;
                for (a next : this.f) {
                    next.a(this.c);
                    if (!next.c()) {
                        arrayList.add(next);
                    }
                }
                for (a remove : arrayList) {
                    this.f.remove(remove);
                }
            }
            f();
        }
    }

    public void e() {
        if (this.c != null) {
            a(this.c);
        }
    }
}
