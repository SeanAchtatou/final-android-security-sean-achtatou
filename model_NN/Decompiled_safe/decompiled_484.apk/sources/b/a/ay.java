package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class ay implements fu<ay, be>, Serializable, Cloneable {
    public static final Map<be, gk> e;
    /* access modifiers changed from: private */
    public static final hc f = new hc("IdJournal");
    /* access modifiers changed from: private */
    public static final gt g = new gt("domain", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final gt h = new gt("old_id", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final gt i = new gt("new_id", (byte) 11, 3);
    /* access modifiers changed from: private */
    public static final gt j = new gt("ts", (byte) 10, 4);
    private static final Map<Class<? extends he>, hf> k = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f63a;

    /* renamed from: b  reason: collision with root package name */
    public String f64b;
    public String c;
    public long d;
    private byte l = 0;
    private be[] m = {be.OLD_ID};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.be, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        k.put(hg.class, new bb());
        k.put(hh.class, new bd());
        EnumMap enumMap = new EnumMap(be.class);
        enumMap.put((Object) be.DOMAIN, (Object) new gk("domain", (byte) 1, new gl((byte) 11)));
        enumMap.put((Object) be.OLD_ID, (Object) new gk("old_id", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) be.NEW_ID, (Object) new gk("new_id", (byte) 1, new gl((byte) 11)));
        enumMap.put((Object) be.TS, (Object) new gk("ts", (byte) 1, new gl((byte) 10)));
        e = Collections.unmodifiableMap(enumMap);
        gk.a(ay.class, e);
    }

    public ay a(long j2) {
        this.d = j2;
        d(true);
        return this;
    }

    public ay a(String str) {
        this.f63a = str;
        return this;
    }

    public void a(gw gwVar) {
        k.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z) {
        if (!z) {
            this.f63a = null;
        }
    }

    public boolean a() {
        return this.f64b != null;
    }

    public ay b(String str) {
        this.f64b = str;
        return this;
    }

    public void b(gw gwVar) {
        k.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z) {
        if (!z) {
            this.f64b = null;
        }
    }

    public boolean b() {
        return fs.a(this.l, 0);
    }

    public ay c(String str) {
        this.c = str;
        return this;
    }

    public void c() {
        if (this.f63a == null) {
            throw new gx("Required field 'domain' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new gx("Required field 'new_id' was not present! Struct: " + toString());
        }
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public void d(boolean z) {
        this.l = fs.a(this.l, 0, z);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("IdJournal(");
        sb.append("domain:");
        if (this.f63a == null) {
            sb.append("null");
        } else {
            sb.append(this.f63a);
        }
        if (a()) {
            sb.append(", ");
            sb.append("old_id:");
            if (this.f64b == null) {
                sb.append("null");
            } else {
                sb.append(this.f64b);
            }
        }
        sb.append(", ");
        sb.append("new_id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("ts:");
        sb.append(this.d);
        sb.append(")");
        return sb.toString();
    }
}
