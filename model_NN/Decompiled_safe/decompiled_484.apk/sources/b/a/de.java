package b.a;

class de extends hg<dc> {
    private de() {
    }

    /* renamed from: a */
    public void b(gw gwVar, dc dcVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                if (!dcVar.a()) {
                    throw new gx("Required field 'duration' was not found in serialized data! Struct: " + toString());
                }
                dcVar.b();
                return;
            }
            switch (h.c) {
                case 1:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        dcVar.f95a = gwVar.v();
                        dcVar.a(true);
                        break;
                    }
                case 2:
                    if (h.f172b != 10) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        dcVar.f96b = gwVar.t();
                        dcVar.b(true);
                        break;
                    }
                default:
                    ha.a(gwVar, h.f172b);
                    break;
            }
            gwVar.i();
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, dc dcVar) {
        dcVar.b();
        gwVar.a(dc.d);
        if (dcVar.f95a != null) {
            gwVar.a(dc.e);
            gwVar.a(dcVar.f95a);
            gwVar.b();
        }
        gwVar.a(dc.f);
        gwVar.a(dcVar.f96b);
        gwVar.b();
        gwVar.c();
        gwVar.a();
    }
}
