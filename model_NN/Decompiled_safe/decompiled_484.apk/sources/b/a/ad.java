package b.a;

class ad extends hg<ab> {
    private ad() {
    }

    /* renamed from: a */
    public void b(gw gwVar, ab abVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                abVar.r();
                return;
            }
            switch (h.c) {
                case 1:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        abVar.f47a = gwVar.v();
                        abVar.a(true);
                        break;
                    }
                case 2:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        abVar.f48b = gwVar.v();
                        abVar.b(true);
                        break;
                    }
                case 3:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        abVar.c = gwVar.v();
                        abVar.c(true);
                        break;
                    }
                case 4:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        abVar.d = gwVar.v();
                        abVar.d(true);
                        break;
                    }
                case 5:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        abVar.e = gwVar.v();
                        abVar.e(true);
                        break;
                    }
                case 6:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        abVar.f = gwVar.v();
                        abVar.f(true);
                        break;
                    }
                case 7:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        abVar.g = gwVar.v();
                        abVar.g(true);
                        break;
                    }
                case 8:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        abVar.h = gwVar.v();
                        abVar.h(true);
                        break;
                    }
                case 9:
                    if (h.f172b != 12) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        abVar.i = new dn();
                        abVar.i.a(gwVar);
                        abVar.i(true);
                        break;
                    }
                case 10:
                    if (h.f172b != 2) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        abVar.j = gwVar.p();
                        abVar.j(true);
                        break;
                    }
                case 11:
                    if (h.f172b != 2) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        abVar.k = gwVar.p();
                        abVar.k(true);
                        break;
                    }
                case 12:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        abVar.l = gwVar.v();
                        abVar.l(true);
                        break;
                    }
                case 13:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        abVar.m = gwVar.v();
                        abVar.m(true);
                        break;
                    }
                case 14:
                    if (h.f172b != 10) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        abVar.n = gwVar.t();
                        abVar.n(true);
                        break;
                    }
                case 15:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        abVar.o = gwVar.v();
                        abVar.o(true);
                        break;
                    }
                case 16:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        abVar.p = gwVar.v();
                        abVar.p(true);
                        break;
                    }
                case 17:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        abVar.q = gwVar.v();
                        abVar.q(true);
                        break;
                    }
                default:
                    ha.a(gwVar, h.f172b);
                    break;
            }
            gwVar.i();
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, ab abVar) {
        abVar.r();
        gwVar.a(ab.s);
        if (abVar.f47a != null && abVar.a()) {
            gwVar.a(ab.t);
            gwVar.a(abVar.f47a);
            gwVar.b();
        }
        if (abVar.f48b != null && abVar.b()) {
            gwVar.a(ab.u);
            gwVar.a(abVar.f48b);
            gwVar.b();
        }
        if (abVar.c != null && abVar.c()) {
            gwVar.a(ab.v);
            gwVar.a(abVar.c);
            gwVar.b();
        }
        if (abVar.d != null && abVar.d()) {
            gwVar.a(ab.w);
            gwVar.a(abVar.d);
            gwVar.b();
        }
        if (abVar.e != null && abVar.e()) {
            gwVar.a(ab.x);
            gwVar.a(abVar.e);
            gwVar.b();
        }
        if (abVar.f != null && abVar.f()) {
            gwVar.a(ab.y);
            gwVar.a(abVar.f);
            gwVar.b();
        }
        if (abVar.g != null && abVar.g()) {
            gwVar.a(ab.z);
            gwVar.a(abVar.g);
            gwVar.b();
        }
        if (abVar.h != null && abVar.h()) {
            gwVar.a(ab.A);
            gwVar.a(abVar.h);
            gwVar.b();
        }
        if (abVar.i != null && abVar.i()) {
            gwVar.a(ab.B);
            abVar.i.b(gwVar);
            gwVar.b();
        }
        if (abVar.j()) {
            gwVar.a(ab.C);
            gwVar.a(abVar.j);
            gwVar.b();
        }
        if (abVar.k()) {
            gwVar.a(ab.D);
            gwVar.a(abVar.k);
            gwVar.b();
        }
        if (abVar.l != null && abVar.l()) {
            gwVar.a(ab.E);
            gwVar.a(abVar.l);
            gwVar.b();
        }
        if (abVar.m != null && abVar.m()) {
            gwVar.a(ab.F);
            gwVar.a(abVar.m);
            gwVar.b();
        }
        if (abVar.n()) {
            gwVar.a(ab.G);
            gwVar.a(abVar.n);
            gwVar.b();
        }
        if (abVar.o != null && abVar.o()) {
            gwVar.a(ab.H);
            gwVar.a(abVar.o);
            gwVar.b();
        }
        if (abVar.p != null && abVar.p()) {
            gwVar.a(ab.I);
            gwVar.a(abVar.p);
            gwVar.b();
        }
        if (abVar.q != null && abVar.q()) {
            gwVar.a(ab.J);
            gwVar.a(abVar.q);
            gwVar.b();
        }
        gwVar.c();
        gwVar.a();
    }
}
