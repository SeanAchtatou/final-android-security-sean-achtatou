package b.a;

import android.content.Context;
import com.f.a.a.b;
import com.f.a.a.f;
import com.f.a.t;

public final class hs implements hy {
    private static hs c;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public hy f199a = new hr(this.f200b);

    /* renamed from: b  reason: collision with root package name */
    private Context f200b;

    private hs(Context context) {
        this.f200b = context.getApplicationContext();
    }

    public static synchronized hs a(Context context) {
        hs hsVar;
        synchronized (hs.class) {
            if (c == null && context != null) {
                c = new hs(context);
            }
            hsVar = c;
        }
        return hsVar;
    }

    public void a() {
        t.b(new hu(this));
    }

    public void a(hz hzVar) {
        t.b(new ht(this, hzVar));
    }

    public void a(b bVar) {
        if (bVar != null && this.f199a != null) {
            bVar.a((f) this.f199a);
        }
    }

    public void b(hz hzVar) {
        this.f199a.b(hzVar);
    }
}
