package b.a;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class hi extends hk {

    /* renamed from: a  reason: collision with root package name */
    protected InputStream f184a = null;

    /* renamed from: b  reason: collision with root package name */
    protected OutputStream f185b = null;

    protected hi() {
    }

    public hi(OutputStream outputStream) {
        this.f185b = outputStream;
    }

    public int a(byte[] bArr, int i, int i2) {
        if (this.f184a == null) {
            throw new hl(1, "Cannot read from null inputStream");
        }
        try {
            int read = this.f184a.read(bArr, i, i2);
            if (read >= 0) {
                return read;
            }
            throw new hl(4);
        } catch (IOException e) {
            throw new hl(0, e);
        }
    }

    public void b(byte[] bArr, int i, int i2) {
        if (this.f185b == null) {
            throw new hl(1, "Cannot write to null outputStream");
        }
        try {
            this.f185b.write(bArr, i, i2);
        } catch (IOException e) {
            throw new hl(0, e);
        }
    }
}
