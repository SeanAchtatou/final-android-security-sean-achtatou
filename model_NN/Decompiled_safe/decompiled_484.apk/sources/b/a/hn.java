package b.a;

import android.content.Context;
import java.io.File;
import java.nio.ByteBuffer;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

public class hn {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f190a = "pbl0".getBytes();

    /* renamed from: b  reason: collision with root package name */
    private bt f191b = null;
    private Context c;

    public hn(Context context) {
        this.c = context;
    }

    private bt a(bt btVar, bt btVar2) {
        if (btVar2 != null) {
            Map<String, ca> a2 = btVar.a();
            for (Map.Entry next : btVar2.a().entrySet()) {
                if (((ca) next.getValue()).b()) {
                    a2.put(next.getKey(), next.getValue());
                } else {
                    a2.remove(next.getKey());
                }
            }
            btVar.a(btVar2.b());
            btVar.a(a(btVar));
        }
        return btVar;
    }

    private boolean c(bt btVar) {
        if (!btVar.d().equals(a(btVar))) {
            return false;
        }
        for (ca next : btVar.a().values()) {
            byte[] b2 = fv.b(next.e());
            byte[] a2 = a(next);
            int i = 0;
            while (true) {
                if (i < 4) {
                    if (b2[i] != a2[i]) {
                        return false;
                    }
                    i++;
                }
            }
        }
        return true;
    }

    public synchronized bt a() {
        return this.f191b;
    }

    public String a(bt btVar) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : new TreeMap(btVar.a()).entrySet()) {
            sb.append((String) entry.getKey());
            sb.append(((ca) entry.getValue()).a());
            sb.append(((ca) entry.getValue()).c());
            sb.append(((ca) entry.getValue()).e());
        }
        sb.append(btVar.f76b);
        return fr.a(sb.toString()).toLowerCase(Locale.US);
    }

    public byte[] a(ca caVar) {
        ByteBuffer allocate = ByteBuffer.allocate(8);
        allocate.order(null);
        allocate.putLong(caVar.c());
        byte[] array = allocate.array();
        byte[] bArr = f190a;
        byte[] bArr2 = new byte[4];
        for (int i = 0; i < 4; i++) {
            bArr2[i] = (byte) (array[i] ^ bArr[i]);
        }
        return bArr2;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* JADX WARN: Type inference failed for: r2v4, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026 A[SYNTHETIC, Splitter:B:8:0x0026] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b() {
        /*
            r4 = this;
            r2 = 0
            java.io.File r0 = new java.io.File
            android.content.Context r1 = r4.c
            java.io.File r1 = r1.getFilesDir()
            java.lang.String r3 = ".imprint"
            r0.<init>(r1, r3)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x0015
        L_0x0014:
            return
        L_0x0015:
            android.content.Context r0 = r4.c     // Catch:{ Exception -> 0x003b, all -> 0x0044 }
            java.lang.String r1 = ".imprint"
            java.io.FileInputStream r1 = r0.openFileInput(r1)     // Catch:{ Exception -> 0x003b, all -> 0x0044 }
            byte[] r2 = b.a.fr.b(r1)     // Catch:{ Exception -> 0x004c }
            b.a.fr.c(r1)
        L_0x0024:
            if (r2 == 0) goto L_0x0014
            b.a.bt r0 = new b.a.bt     // Catch:{ Exception -> 0x0036 }
            r0.<init>()     // Catch:{ Exception -> 0x0036 }
            b.a.fz r1 = new b.a.fz     // Catch:{ Exception -> 0x0036 }
            r1.<init>()     // Catch:{ Exception -> 0x0036 }
            r1.a(r0, r2)     // Catch:{ Exception -> 0x0036 }
            r4.f191b = r0     // Catch:{ Exception -> 0x0036 }
            goto L_0x0014
        L_0x0036:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0014
        L_0x003b:
            r0 = move-exception
            r1 = r2
        L_0x003d:
            r0.printStackTrace()     // Catch:{ all -> 0x0049 }
            b.a.fr.c(r1)
            goto L_0x0024
        L_0x0044:
            r0 = move-exception
        L_0x0045:
            b.a.fr.c(r2)
            throw r0
        L_0x0049:
            r0 = move-exception
            r2 = r1
            goto L_0x0045
        L_0x004c:
            r0 = move-exception
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.hn.b():void");
    }

    public void b(bt btVar) {
        if (btVar != null && c(btVar)) {
            synchronized (this) {
                bt btVar2 = this.f191b;
                if (btVar2 != null) {
                    btVar = a(btVar2, btVar);
                }
                this.f191b = btVar;
            }
        }
    }

    public void c() {
        if (this.f191b != null) {
            try {
                fr.a(new File(this.c.getFilesDir(), ".imprint"), new gc().a(this.f191b));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
