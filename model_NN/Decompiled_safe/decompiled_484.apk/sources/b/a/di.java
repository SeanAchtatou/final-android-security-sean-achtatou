package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum di implements gb {
    PAGE_NAME(1, "page_name"),
    DURATION(2, "duration");
    
    private static final Map<String, di> c = new HashMap();
    private final short d;
    private final String e;

    static {
        Iterator it = EnumSet.allOf(di.class).iterator();
        while (it.hasNext()) {
            di diVar = (di) it.next();
            c.put(diVar.b(), diVar);
        }
    }

    private di(short s, String str) {
        this.d = s;
        this.e = str;
    }

    public short a() {
        return this.d;
    }

    public String b() {
        return this.e;
    }
}
