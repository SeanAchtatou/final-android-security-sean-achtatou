package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum bs implements gb {
    SNAPSHOTS(1, "snapshots"),
    JOURNALS(2, "journals"),
    CHECKSUM(3, "checksum");
    
    private static final Map<String, bs> d = new HashMap();
    private final short e;
    private final String f;

    static {
        Iterator it = EnumSet.allOf(bs.class).iterator();
        while (it.hasNext()) {
            bs bsVar = (bs) it.next();
            d.put(bsVar.b(), bsVar);
        }
    }

    private bs(short s, String str) {
        this.e = s;
        this.f = str;
    }

    public short a() {
        return this.e;
    }

    public String b() {
        return this.f;
    }
}
