package b.a;

class cq extends hg<co> {
    private cq() {
    }

    /* renamed from: a */
    public void b(gw gwVar, co coVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                if (!coVar.a()) {
                    throw new gx("Required field 'lat' was not found in serialized data! Struct: " + toString());
                } else if (!coVar.b()) {
                    throw new gx("Required field 'lng' was not found in serialized data! Struct: " + toString());
                } else if (!coVar.c()) {
                    throw new gx("Required field 'ts' was not found in serialized data! Struct: " + toString());
                } else {
                    coVar.d();
                    return;
                }
            } else {
                switch (h.c) {
                    case 1:
                        if (h.f172b != 4) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            coVar.f87a = gwVar.u();
                            coVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.f172b != 4) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            coVar.f88b = gwVar.u();
                            coVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.f172b != 10) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            coVar.c = gwVar.t();
                            coVar.c(true);
                            break;
                        }
                    default:
                        ha.a(gwVar, h.f172b);
                        break;
                }
                gwVar.i();
            }
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, co coVar) {
        coVar.d();
        gwVar.a(co.e);
        gwVar.a(co.f);
        gwVar.a(coVar.f87a);
        gwVar.b();
        gwVar.a(co.g);
        gwVar.a(coVar.f88b);
        gwVar.b();
        gwVar.a(co.h);
        gwVar.a(coVar.c);
        gwVar.b();
        gwVar.c();
        gwVar.a();
    }
}
