package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum ei implements gb {
    ID(1, "id"),
    START_TIME(2, "start_time"),
    END_TIME(3, "end_time"),
    DURATION(4, "duration"),
    PAGES(5, "pages"),
    LOCATIONS(6, "locations"),
    TRAFFIC(7, "traffic");
    
    private static final Map<String, ei> h = new HashMap();
    private final short i;
    private final String j;

    static {
        Iterator it = EnumSet.allOf(ei.class).iterator();
        while (it.hasNext()) {
            ei eiVar = (ei) it.next();
            h.put(eiVar.b(), eiVar);
        }
    }

    private ei(short s, String str) {
        this.i = s;
        this.j = str;
    }

    public short a() {
        return this.i;
    }

    public String b() {
        return this.j;
    }
}
