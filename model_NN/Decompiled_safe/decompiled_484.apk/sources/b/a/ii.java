package b.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ii {

    /* renamed from: a  reason: collision with root package name */
    private final Map<String, Long> f220a = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    private final ArrayList<ig> f221b = new ArrayList<>();

    public static List<dc> a(SharedPreferences sharedPreferences) {
        String string = sharedPreferences.getString("activities", "");
        if ("".equals(string)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        try {
            String[] split = string.split(";");
            for (String str : split) {
                if (!TextUtils.isEmpty(str)) {
                    arrayList.add(new d(str));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (arrayList.size() > 0) {
            return arrayList;
        }
        return null;
    }

    public void a() {
        long j;
        String str;
        String str2 = null;
        long j2 = 0;
        synchronized (this.f220a) {
            for (Map.Entry next : this.f220a.entrySet()) {
                if (((Long) next.getValue()).longValue() > j2) {
                    long longValue = ((Long) next.getValue()).longValue();
                    str = (String) next.getKey();
                    j = longValue;
                } else {
                    j = j2;
                    str = str2;
                }
                str2 = str;
                j2 = j;
            }
        }
        if (str2 != null) {
            b(str2);
        }
    }

    public void a(Context context) {
        SharedPreferences a2 = id.a(context);
        SharedPreferences.Editor edit = a2.edit();
        if (this.f221b.size() > 0) {
            String string = a2.getString("activities", "");
            StringBuilder sb = new StringBuilder();
            if (!TextUtils.isEmpty(string)) {
                sb.append(string);
                sb.append(";");
            }
            synchronized (this.f221b) {
                Iterator<ig> it = this.f221b.iterator();
                while (it.hasNext()) {
                    ig next = it.next();
                    sb.append(String.format("[\"%s\",%d]", next.f218a, Long.valueOf(next.f219b)));
                    sb.append(";");
                }
                this.f221b.clear();
            }
            sb.deleteCharAt(sb.length() - 1);
            edit.remove("activities");
            edit.putString("activities", sb.toString());
        }
        edit.commit();
    }

    public void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            synchronized (this.f220a) {
                this.f220a.put(str, Long.valueOf(System.currentTimeMillis()));
            }
        }
    }

    public void b(String str) {
        Long remove;
        if (!TextUtils.isEmpty(str)) {
            synchronized (this.f220a) {
                remove = this.f220a.remove(str);
            }
            if (remove == null) {
                fm.d("MobclickAgent", String.format("please call 'onPageStart(%s)' before onPageEnd", str));
                return;
            }
            long currentTimeMillis = System.currentTimeMillis() - remove.longValue();
            synchronized (this.f221b) {
                this.f221b.add(new ig(str, currentTimeMillis));
            }
        }
    }
}
