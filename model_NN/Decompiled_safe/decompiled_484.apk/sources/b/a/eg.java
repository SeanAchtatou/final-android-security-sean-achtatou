package b.a;

import java.util.ArrayList;
import java.util.BitSet;

class eg extends hh<ec> {
    private eg() {
    }

    public void a(gw gwVar, ec ecVar) {
        hd hdVar = (hd) gwVar;
        hdVar.a(ecVar.f114a);
        hdVar.a(ecVar.f115b);
        hdVar.a(ecVar.c);
        hdVar.a(ecVar.d);
        BitSet bitSet = new BitSet();
        if (ecVar.e()) {
            bitSet.set(0);
        }
        if (ecVar.f()) {
            bitSet.set(1);
        }
        if (ecVar.g()) {
            bitSet.set(2);
        }
        hdVar.a(bitSet, 3);
        if (ecVar.e()) {
            hdVar.a(ecVar.e.size());
            for (dc b2 : ecVar.e) {
                b2.b(hdVar);
            }
        }
        if (ecVar.f()) {
            hdVar.a(ecVar.f.size());
            for (co b3 : ecVar.f) {
                b3.b(hdVar);
            }
        }
        if (ecVar.g()) {
            ecVar.g.b(hdVar);
        }
    }

    public void b(gw gwVar, ec ecVar) {
        hd hdVar = (hd) gwVar;
        ecVar.f114a = hdVar.v();
        ecVar.a(true);
        ecVar.f115b = hdVar.t();
        ecVar.b(true);
        ecVar.c = hdVar.t();
        ecVar.c(true);
        ecVar.d = hdVar.t();
        ecVar.d(true);
        BitSet b2 = hdVar.b(3);
        if (b2.get(0)) {
            gu guVar = new gu((byte) 12, hdVar.s());
            ecVar.e = new ArrayList(guVar.f174b);
            for (int i = 0; i < guVar.f174b; i++) {
                dc dcVar = new dc();
                dcVar.a(hdVar);
                ecVar.e.add(dcVar);
            }
            ecVar.e(true);
        }
        if (b2.get(1)) {
            gu guVar2 = new gu((byte) 12, hdVar.s());
            ecVar.f = new ArrayList(guVar2.f174b);
            for (int i2 = 0; i2 < guVar2.f174b; i2++) {
                co coVar = new co();
                coVar.a(hdVar);
                ecVar.f.add(coVar);
            }
            ecVar.f(true);
        }
        if (b2.get(2)) {
            ecVar.g = new ej();
            ecVar.g.a(hdVar);
            ecVar.g(true);
        }
    }
}
