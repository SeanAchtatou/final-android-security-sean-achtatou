package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class n implements fu<n, t>, Serializable, Cloneable {
    public static final Map<t, gk> k;
    /* access modifiers changed from: private */
    public static final hc l = new hc("AppInfo");
    /* access modifiers changed from: private */
    public static final gt m = new gt("key", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final gt n = new gt("version", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final gt o = new gt("version_index", (byte) 8, 3);
    /* access modifiers changed from: private */
    public static final gt p = new gt("package_name", (byte) 11, 4);
    /* access modifiers changed from: private */
    public static final gt q = new gt("sdk_type", (byte) 8, 5);
    /* access modifiers changed from: private */
    public static final gt r = new gt("sdk_version", (byte) 11, 6);
    /* access modifiers changed from: private */
    public static final gt s = new gt("channel", (byte) 11, 7);
    /* access modifiers changed from: private */
    public static final gt t = new gt("wrapper_type", (byte) 11, 8);
    /* access modifiers changed from: private */
    public static final gt u = new gt("wrapper_version", (byte) 11, 9);
    /* access modifiers changed from: private */
    public static final gt v = new gt("vertical_type", (byte) 8, 10);
    private static final Map<Class<? extends he>, hf> w = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f224a;

    /* renamed from: b  reason: collision with root package name */
    public String f225b;
    public int c;
    public String d;
    public eb e;
    public String f;
    public String g;
    public String h;
    public String i;
    public int j;
    private byte x = 0;
    private t[] y = {t.VERSION, t.VERSION_INDEX, t.PACKAGE_NAME, t.WRAPPER_TYPE, t.WRAPPER_VERSION, t.VERTICAL_TYPE};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.t, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        w.put(hg.class, new q());
        w.put(hh.class, new s());
        EnumMap enumMap = new EnumMap(t.class);
        enumMap.put((Object) t.KEY, (Object) new gk("key", (byte) 1, new gl((byte) 11)));
        enumMap.put((Object) t.VERSION, (Object) new gk("version", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) t.VERSION_INDEX, (Object) new gk("version_index", (byte) 2, new gl((byte) 8)));
        enumMap.put((Object) t.PACKAGE_NAME, (Object) new gk("package_name", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) t.SDK_TYPE, (Object) new gk("sdk_type", (byte) 1, new gj((byte) 16, eb.class)));
        enumMap.put((Object) t.SDK_VERSION, (Object) new gk("sdk_version", (byte) 1, new gl((byte) 11)));
        enumMap.put((Object) t.CHANNEL, (Object) new gk("channel", (byte) 1, new gl((byte) 11)));
        enumMap.put((Object) t.WRAPPER_TYPE, (Object) new gk("wrapper_type", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) t.WRAPPER_VERSION, (Object) new gk("wrapper_version", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) t.VERTICAL_TYPE, (Object) new gk("vertical_type", (byte) 2, new gl((byte) 8)));
        k = Collections.unmodifiableMap(enumMap);
        gk.a(n.class, k);
    }

    public n a(int i2) {
        this.c = i2;
        c(true);
        return this;
    }

    public n a(eb ebVar) {
        this.e = ebVar;
        return this;
    }

    public n a(String str) {
        this.f224a = str;
        return this;
    }

    public void a(gw gwVar) {
        w.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z) {
        if (!z) {
            this.f224a = null;
        }
    }

    public boolean a() {
        return this.f225b != null;
    }

    public n b(int i2) {
        this.j = i2;
        j(true);
        return this;
    }

    public n b(String str) {
        this.f225b = str;
        return this;
    }

    public void b(gw gwVar) {
        w.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z) {
        if (!z) {
            this.f225b = null;
        }
    }

    public boolean b() {
        return fs.a(this.x, 0);
    }

    public n c(String str) {
        this.d = str;
        return this;
    }

    public void c(boolean z) {
        this.x = fs.a(this.x, 0, z);
    }

    public boolean c() {
        return this.d != null;
    }

    public n d(String str) {
        this.f = str;
        return this;
    }

    public void d(boolean z) {
        if (!z) {
            this.d = null;
        }
    }

    public boolean d() {
        return this.h != null;
    }

    public n e(String str) {
        this.g = str;
        return this;
    }

    public void e(boolean z) {
        if (!z) {
            this.e = null;
        }
    }

    public boolean e() {
        return this.i != null;
    }

    public n f(String str) {
        this.h = str;
        return this;
    }

    public void f(boolean z) {
        if (!z) {
            this.f = null;
        }
    }

    public boolean f() {
        return fs.a(this.x, 1);
    }

    public n g(String str) {
        this.i = str;
        return this;
    }

    public void g() {
        if (this.f224a == null) {
            throw new gx("Required field 'key' was not present! Struct: " + toString());
        } else if (this.e == null) {
            throw new gx("Required field 'sdk_type' was not present! Struct: " + toString());
        } else if (this.f == null) {
            throw new gx("Required field 'sdk_version' was not present! Struct: " + toString());
        } else if (this.g == null) {
            throw new gx("Required field 'channel' was not present! Struct: " + toString());
        }
    }

    public void g(boolean z) {
        if (!z) {
            this.g = null;
        }
    }

    public void h(boolean z) {
        if (!z) {
            this.h = null;
        }
    }

    public void i(boolean z) {
        if (!z) {
            this.i = null;
        }
    }

    public void j(boolean z) {
        this.x = fs.a(this.x, 1, z);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("AppInfo(");
        sb.append("key:");
        if (this.f224a == null) {
            sb.append("null");
        } else {
            sb.append(this.f224a);
        }
        if (a()) {
            sb.append(", ");
            sb.append("version:");
            if (this.f225b == null) {
                sb.append("null");
            } else {
                sb.append(this.f225b);
            }
        }
        if (b()) {
            sb.append(", ");
            sb.append("version_index:");
            sb.append(this.c);
        }
        if (c()) {
            sb.append(", ");
            sb.append("package_name:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
        }
        sb.append(", ");
        sb.append("sdk_type:");
        if (this.e == null) {
            sb.append("null");
        } else {
            sb.append(this.e);
        }
        sb.append(", ");
        sb.append("sdk_version:");
        if (this.f == null) {
            sb.append("null");
        } else {
            sb.append(this.f);
        }
        sb.append(", ");
        sb.append("channel:");
        if (this.g == null) {
            sb.append("null");
        } else {
            sb.append(this.g);
        }
        if (d()) {
            sb.append(", ");
            sb.append("wrapper_type:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        if (e()) {
            sb.append(", ");
            sb.append("wrapper_version:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        }
        if (f()) {
            sb.append(", ");
            sb.append("vertical_type:");
            sb.append(this.j);
        }
        sb.append(")");
        return sb.toString();
    }
}
