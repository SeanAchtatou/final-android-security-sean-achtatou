package b.a;

class gf extends hg<gd> {
    private gf() {
    }

    /* renamed from: a */
    public void b(gw gwVar, gd gdVar) {
        gdVar.c = null;
        gdVar.f155b = null;
        gwVar.f();
        gt h = gwVar.h();
        gdVar.f155b = gdVar.a(gwVar, h);
        if (gdVar.f155b != null) {
            gdVar.c = gdVar.b(h.c);
        }
        gwVar.i();
        gwVar.h();
        gwVar.g();
    }

    /* renamed from: b */
    public void a(gw gwVar, gd gdVar) {
        if (gdVar.b() == null || gdVar.c() == null) {
            throw new gx("Cannot write a TUnion with no set value!");
        }
        gwVar.a(gdVar.a());
        gwVar.a(gdVar.a((gb) gdVar.c));
        gdVar.c(gwVar);
        gwVar.b();
        gwVar.c();
        gwVar.a();
    }
}
