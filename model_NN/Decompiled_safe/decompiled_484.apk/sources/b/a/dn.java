package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class dn implements fu<dn, dt>, Serializable, Cloneable {
    public static final Map<dt, gk> c;
    /* access modifiers changed from: private */
    public static final hc d = new hc("Resolution");
    /* access modifiers changed from: private */
    public static final gt e = new gt("height", (byte) 8, 1);
    /* access modifiers changed from: private */
    public static final gt f = new gt("width", (byte) 8, 2);
    private static final Map<Class<? extends he>, hf> g = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public int f104a;

    /* renamed from: b  reason: collision with root package name */
    public int f105b;
    private byte h;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.dt, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        g.put(hg.class, new dq());
        g.put(hh.class, new ds());
        EnumMap enumMap = new EnumMap(dt.class);
        enumMap.put((Object) dt.HEIGHT, (Object) new gk("height", (byte) 1, new gl((byte) 8)));
        enumMap.put((Object) dt.WIDTH, (Object) new gk("width", (byte) 1, new gl((byte) 8)));
        c = Collections.unmodifiableMap(enumMap);
        gk.a(dn.class, c);
    }

    public dn() {
        this.h = 0;
    }

    public dn(int i, int i2) {
        this();
        this.f104a = i;
        a(true);
        this.f105b = i2;
        b(true);
    }

    public void a(gw gwVar) {
        g.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z) {
        this.h = fs.a(this.h, 0, z);
    }

    public boolean a() {
        return fs.a(this.h, 0);
    }

    public void b(gw gwVar) {
        g.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z) {
        this.h = fs.a(this.h, 1, z);
    }

    public boolean b() {
        return fs.a(this.h, 1);
    }

    public void c() {
    }

    public String toString() {
        return "Resolution(" + "height:" + this.f104a + ", " + "width:" + this.f105b + ")";
    }
}
