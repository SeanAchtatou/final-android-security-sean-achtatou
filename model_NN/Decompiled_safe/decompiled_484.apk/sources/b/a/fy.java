package b.a;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

class fy implements Comparator {
    private fy() {
    }

    public int compare(Object obj, Object obj2) {
        if (obj == null && obj2 == null) {
            return 0;
        }
        if (obj == null) {
            return -1;
        }
        if (obj2 == null) {
            return 1;
        }
        return obj instanceof List ? fw.a((List) obj, (List) obj2) : obj instanceof Set ? fw.a((Set) obj, (Set) obj2) : obj instanceof Map ? fw.a((Map) obj, (Map) obj2) : obj instanceof byte[] ? fw.a((byte[]) obj, (byte[]) obj2) : fw.a((Comparable) obj, (Comparable) obj2);
    }
}
