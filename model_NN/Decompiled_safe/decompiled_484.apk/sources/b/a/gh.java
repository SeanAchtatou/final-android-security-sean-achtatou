package b.a;

class gh extends hh<gd> {
    private gh() {
    }

    /* renamed from: a */
    public void b(gw gwVar, gd gdVar) {
        gdVar.c = null;
        gdVar.f155b = null;
        short r = gwVar.r();
        gdVar.f155b = gdVar.a(gwVar, r);
        if (gdVar.f155b != null) {
            gdVar.c = gdVar.b(r);
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, gd gdVar) {
        if (gdVar.b() == null || gdVar.c() == null) {
            throw new gx("Cannot write a TUnion with no set value!");
        }
        gwVar.a(gdVar.c.a());
        gdVar.d(gwVar);
    }
}
