package b.a;

import android.content.Context;
import android.text.TextUtils;
import com.f.a.a;
import com.f.a.a.f;
import com.f.a.i;
import com.f.a.j;
import com.f.a.k;
import com.f.a.l;
import com.f.a.m;
import com.f.a.n;
import com.f.a.v;

public final class hr implements hy, f {

    /* renamed from: a  reason: collision with root package name */
    private ia f197a = null;

    /* renamed from: b  reason: collision with root package name */
    private ib f198b = null;
    private m c = null;
    private v d = null;
    private Cif e = null;
    private gy f = null;
    private int g = 10;
    private Context h;

    public hr(Context context) {
        this.h = context;
        this.f197a = new ia(context);
        this.f = hp.a(context);
        this.e = new Cif(context);
        this.f198b = new ib(context);
        this.f198b.a(this.e);
        this.d = v.a(context);
        int[] c2 = a.c(this.h);
        a(c2[0], c2[1]);
    }

    private eq a(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        try {
            eq eqVar = new eq();
            new fz().a(eqVar, bArr);
            return eqVar;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private void a(int i, int i2) {
        switch (i) {
            case 0:
                this.c = new m();
                break;
            case 1:
                this.c = new i();
                break;
            case 2:
            case 3:
            default:
                this.c = new i();
                break;
            case 4:
                this.c = new l(this.e);
                break;
            case 5:
                this.c = new n(this.h);
                break;
            case 6:
                this.c = new j(this.e, (long) i2);
                break;
            case 7:
                this.c = new k(this.f197a, i2);
                break;
        }
        fm.c("MobclickAgent", "report policy:" + i + " interval:" + i2);
    }

    private boolean a(boolean z) {
        if (!fl.g(this.h)) {
            if (fm.f137a) {
                fm.c("MobclickAgent", "network is unavailable");
            }
            return false;
        } else if (this.e.a()) {
            return true;
        } else {
            if (!fm.f137a || !fl.q(this.h)) {
                return this.c.a(z);
            }
            return true;
        }
    }

    private byte[] a(eq eqVar) {
        try {
            return new gc().a(eqVar);
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private boolean c() {
        return this.f197a.a() > this.g;
    }

    private void d() {
        try {
            if (this.e.a()) {
                this.f197a.a(new g(this.e.i()));
            }
            e();
        } catch (Throwable th) {
            if (th instanceof OutOfMemoryError) {
            }
            if (th != null) {
                th.printStackTrace();
            }
        }
    }

    private void e() {
        byte[] c2;
        v a2 = v.a(this.h);
        boolean f2 = a2.f();
        if (f2) {
            c2 = a2.d();
        } else {
            this.f.a();
            byte[] b2 = b();
            if (b2 == null) {
                fm.d("MobclickAgent", "message is null");
                return;
            } else {
                c2 = fv.a(this.h, a.a(this.h), b2).c();
                a2.c();
            }
        }
        switch (this.f198b.a(c2)) {
            case 1:
                if (!f2) {
                    a2.b(c2);
                }
                fm.b("MobclickAgent", "connection error");
                return;
            case 2:
                if (this.e.h()) {
                    this.e.g();
                }
                this.f.c();
                this.e.f();
                if (f2) {
                    a2.e();
                    return;
                }
                return;
            case 3:
                this.e.f();
                if (f2) {
                    a2.e();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void a() {
        if (this.f197a.a() > 0) {
            try {
                byte[] b2 = b();
                if (b2 != null) {
                    this.d.a(b2);
                }
            } catch (Throwable th) {
                if (th instanceof OutOfMemoryError) {
                    this.d.c();
                }
                if (th != null) {
                    th.printStackTrace();
                }
            }
        }
    }

    public void a(int i, long j) {
        a.a(i, (int) j);
        a(i, (int) j);
    }

    public void a(hz hzVar) {
        if (hzVar != null) {
            this.f197a.a(hzVar);
        }
        if (a(hzVar instanceof ec)) {
            d();
        } else if (c()) {
            a();
        }
    }

    public void b(hz hzVar) {
        this.f197a.a(hzVar);
    }

    /* access modifiers changed from: protected */
    public byte[] b() {
        byte[] bArr;
        try {
            if (TextUtils.isEmpty(a.a(this.h))) {
                fm.b("MobclickAgent", "Appkey is missing ,Please check AndroidManifest.xml");
                return null;
            }
            byte[] b2 = v.a(this.h).b();
            eq a2 = b2 == null ? null : a(b2);
            if (a2 == null && this.f197a.a() == 0) {
                return null;
            }
            eq eqVar = a2 == null ? new eq() : a2;
            this.f197a.a(eqVar);
            if (fm.f137a && eqVar.f()) {
                boolean z = false;
                for (ec d2 : eqVar.e()) {
                    z = d2.d() > 0 ? true : z;
                }
                if (!z) {
                    fm.d("MobclickAgent", "missing Activities or PageViews");
                }
            }
            try {
                bArr = a(eqVar);
                try {
                    if (!fm.f137a) {
                        return bArr;
                    }
                    fm.c("MobclickAgent", eqVar.toString());
                    return bArr;
                } catch (Exception e2) {
                    fm.b("MobclickAgent", "Fail to serialize log ...");
                    return bArr;
                }
            } catch (Exception e3) {
                bArr = null;
            }
        } catch (Exception e4) {
            fm.b("MobclickAgent", "Fail to construct message ...", e4);
            v.a(this.h).c();
            return null;
        }
    }
}
