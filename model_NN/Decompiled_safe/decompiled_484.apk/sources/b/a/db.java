package b.a;

import com.qihoo360.daily.activity.LoginActivity;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum db implements gb {
    TIME_ZONE(1, "time_zone"),
    LANGUAGE(2, "language"),
    COUNTRY(3, "country"),
    LATITUDE(4, "latitude"),
    LONGITUDE(5, "longitude"),
    CARRIER(6, "carrier"),
    LATENCY(7, "latency"),
    DISPLAY_NAME(8, "display_name"),
    ACCESS_TYPE(9, "access_type"),
    ACCESS_SUBTYPE(10, "access_subtype"),
    USER_INFO(11, LoginActivity.KEY_USER_INFO);
    
    private static final Map<String, db> l = new HashMap();
    private final short m;
    private final String n;

    static {
        Iterator it = EnumSet.allOf(db.class).iterator();
        while (it.hasNext()) {
            db dbVar = (db) it.next();
            l.put(dbVar.b(), dbVar);
        }
    }

    private db(short s, String str) {
        this.m = s;
        this.n = str;
    }

    public short a() {
        return this.m;
    }

    public String b() {
        return this.n;
    }
}
