package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum t implements gb {
    KEY(1, "key"),
    VERSION(2, "version"),
    VERSION_INDEX(3, "version_index"),
    PACKAGE_NAME(4, "package_name"),
    SDK_TYPE(5, "sdk_type"),
    SDK_VERSION(6, "sdk_version"),
    CHANNEL(7, "channel"),
    WRAPPER_TYPE(8, "wrapper_type"),
    WRAPPER_VERSION(9, "wrapper_version"),
    VERTICAL_TYPE(10, "vertical_type");
    
    private static final Map<String, t> k = new HashMap();
    private final short l;
    private final String m;

    static {
        Iterator it = EnumSet.allOf(t.class).iterator();
        while (it.hasNext()) {
            t tVar = (t) it.next();
            k.put(tVar.b(), tVar);
        }
    }

    private t(short s, String str) {
        this.l = s;
        this.m = str;
    }

    public short a() {
        return this.l;
    }

    public String b() {
        return this.m;
    }
}
