package b.a;

class bh extends hg<bf> {
    private bh() {
    }

    /* renamed from: a */
    public void b(gw gwVar, bf bfVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                if (!bfVar.c()) {
                    throw new gx("Required field 'ts' was not found in serialized data! Struct: " + toString());
                } else if (!bfVar.e()) {
                    throw new gx("Required field 'version' was not found in serialized data! Struct: " + toString());
                } else {
                    bfVar.f();
                    return;
                }
            } else {
                switch (h.c) {
                    case 1:
                        if (h.f172b != 11) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            bfVar.f67a = gwVar.v();
                            bfVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.f172b != 10) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            bfVar.f68b = gwVar.t();
                            bfVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.f172b != 8) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            bfVar.c = gwVar.s();
                            bfVar.c(true);
                            break;
                        }
                    default:
                        ha.a(gwVar, h.f172b);
                        break;
                }
                gwVar.i();
            }
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, bf bfVar) {
        bfVar.f();
        gwVar.a(bf.e);
        if (bfVar.f67a != null) {
            gwVar.a(bf.f);
            gwVar.a(bfVar.f67a);
            gwVar.b();
        }
        gwVar.a(bf.g);
        gwVar.a(bfVar.f68b);
        gwVar.b();
        gwVar.a(bf.h);
        gwVar.a(bfVar.c);
        gwVar.b();
        gwVar.c();
        gwVar.a();
    }
}
