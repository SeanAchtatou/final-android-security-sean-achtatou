package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum be implements gb {
    DOMAIN(1, "domain"),
    OLD_ID(2, "old_id"),
    NEW_ID(3, "new_id"),
    TS(4, "ts");
    
    private static final Map<String, be> e = new HashMap();
    private final short f;
    private final String g;

    static {
        Iterator it = EnumSet.allOf(be.class).iterator();
        while (it.hasNext()) {
            be beVar = (be) it.next();
            e.put(beVar.b(), beVar);
        }
    }

    private be(short s, String str) {
        this.f = s;
        this.g = str;
    }

    public short a() {
        return this.f;
    }

    public String b() {
        return this.g;
    }
}
