package b.a;

class el extends hg<ej> {
    private el() {
    }

    /* renamed from: a */
    public void b(gw gwVar, ej ejVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                if (!ejVar.a()) {
                    throw new gx("Required field 'upload_traffic' was not found in serialized data! Struct: " + toString());
                } else if (!ejVar.b()) {
                    throw new gx("Required field 'download_traffic' was not found in serialized data! Struct: " + toString());
                } else {
                    ejVar.c();
                    return;
                }
            } else {
                switch (h.c) {
                    case 1:
                        if (h.f172b != 8) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            ejVar.f118a = gwVar.s();
                            ejVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.f172b != 8) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            ejVar.f119b = gwVar.s();
                            ejVar.b(true);
                            break;
                        }
                    default:
                        ha.a(gwVar, h.f172b);
                        break;
                }
                gwVar.i();
            }
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, ej ejVar) {
        ejVar.c();
        gwVar.a(ej.d);
        gwVar.a(ej.e);
        gwVar.a(ejVar.f118a);
        gwVar.b();
        gwVar.a(ej.f);
        gwVar.a(ejVar.f119b);
        gwVar.b();
        gwVar.c();
        gwVar.a();
    }
}
