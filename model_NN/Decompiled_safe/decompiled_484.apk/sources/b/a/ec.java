package b.a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ec implements fu<ec, ei>, Serializable, Cloneable {
    public static final Map<ei, gk> h;
    /* access modifiers changed from: private */
    public static final hc i = new hc("Session");
    /* access modifiers changed from: private */
    public static final gt j = new gt("id", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final gt k = new gt("start_time", (byte) 10, 2);
    /* access modifiers changed from: private */
    public static final gt l = new gt("end_time", (byte) 10, 3);
    /* access modifiers changed from: private */
    public static final gt m = new gt("duration", (byte) 10, 4);
    /* access modifiers changed from: private */
    public static final gt n = new gt("pages", (byte) 15, 5);
    /* access modifiers changed from: private */
    public static final gt o = new gt("locations", (byte) 15, 6);
    /* access modifiers changed from: private */
    public static final gt p = new gt("traffic", (byte) 12, 7);
    private static final Map<Class<? extends he>, hf> q = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f114a;

    /* renamed from: b  reason: collision with root package name */
    public long f115b;
    public long c;
    public long d;
    public List<dc> e;
    public List<co> f;
    public ej g;
    private byte r = 0;
    private ei[] s = {ei.PAGES, ei.LOCATIONS, ei.TRAFFIC};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.ei, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        q.put(hg.class, new ef());
        q.put(hh.class, new eh());
        EnumMap enumMap = new EnumMap(ei.class);
        enumMap.put((Object) ei.ID, (Object) new gk("id", (byte) 1, new gl((byte) 11)));
        enumMap.put((Object) ei.START_TIME, (Object) new gk("start_time", (byte) 1, new gl((byte) 10)));
        enumMap.put((Object) ei.END_TIME, (Object) new gk("end_time", (byte) 1, new gl((byte) 10)));
        enumMap.put((Object) ei.DURATION, (Object) new gk("duration", (byte) 1, new gl((byte) 10)));
        enumMap.put((Object) ei.PAGES, (Object) new gk("pages", (byte) 2, new gm((byte) 15, new go((byte) 12, dc.class))));
        enumMap.put((Object) ei.LOCATIONS, (Object) new gk("locations", (byte) 2, new gm((byte) 15, new go((byte) 12, co.class))));
        enumMap.put((Object) ei.TRAFFIC, (Object) new gk("traffic", (byte) 2, new go((byte) 12, ej.class)));
        h = Collections.unmodifiableMap(enumMap);
        gk.a(ec.class, h);
    }

    public ec a(long j2) {
        this.f115b = j2;
        b(true);
        return this;
    }

    public ec a(ej ejVar) {
        this.g = ejVar;
        return this;
    }

    public ec a(String str) {
        this.f114a = str;
        return this;
    }

    public ec a(List<dc> list) {
        this.e = list;
        return this;
    }

    public void a(co coVar) {
        if (this.f == null) {
            this.f = new ArrayList();
        }
        this.f.add(coVar);
    }

    public void a(gw gwVar) {
        q.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z) {
        if (!z) {
            this.f114a = null;
        }
    }

    public boolean a() {
        return fs.a(this.r, 0);
    }

    public ec b(long j2) {
        this.c = j2;
        c(true);
        return this;
    }

    public ec b(List<co> list) {
        this.f = list;
        return this;
    }

    public void b(gw gwVar) {
        q.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z) {
        this.r = fs.a(this.r, 0, z);
    }

    public boolean b() {
        return fs.a(this.r, 1);
    }

    public ec c(long j2) {
        this.d = j2;
        d(true);
        return this;
    }

    public void c(boolean z) {
        this.r = fs.a(this.r, 1, z);
    }

    public boolean c() {
        return fs.a(this.r, 2);
    }

    public int d() {
        if (this.e == null) {
            return 0;
        }
        return this.e.size();
    }

    public void d(boolean z) {
        this.r = fs.a(this.r, 2, z);
    }

    public void e(boolean z) {
        if (!z) {
            this.e = null;
        }
    }

    public boolean e() {
        return this.e != null;
    }

    public void f(boolean z) {
        if (!z) {
            this.f = null;
        }
    }

    public boolean f() {
        return this.f != null;
    }

    public void g(boolean z) {
        if (!z) {
            this.g = null;
        }
    }

    public boolean g() {
        return this.g != null;
    }

    public void h() {
        if (this.f114a == null) {
            throw new gx("Required field 'id' was not present! Struct: " + toString());
        } else if (this.g != null) {
            this.g.c();
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Session(");
        sb.append("id:");
        if (this.f114a == null) {
            sb.append("null");
        } else {
            sb.append(this.f114a);
        }
        sb.append(", ");
        sb.append("start_time:");
        sb.append(this.f115b);
        sb.append(", ");
        sb.append("end_time:");
        sb.append(this.c);
        sb.append(", ");
        sb.append("duration:");
        sb.append(this.d);
        if (e()) {
            sb.append(", ");
            sb.append("pages:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (f()) {
            sb.append(", ");
            sb.append("locations:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        if (g()) {
            sb.append(", ");
            sb.append("traffic:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
