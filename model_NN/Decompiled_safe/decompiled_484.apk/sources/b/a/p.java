package b.a;

class p extends hg<n> {
    private p() {
    }

    /* renamed from: a */
    public void b(gw gwVar, n nVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                nVar.g();
                return;
            }
            switch (h.c) {
                case 1:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        nVar.f224a = gwVar.v();
                        nVar.a(true);
                        break;
                    }
                case 2:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        nVar.f225b = gwVar.v();
                        nVar.b(true);
                        break;
                    }
                case 3:
                    if (h.f172b != 8) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        nVar.c = gwVar.s();
                        nVar.c(true);
                        break;
                    }
                case 4:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        nVar.d = gwVar.v();
                        nVar.d(true);
                        break;
                    }
                case 5:
                    if (h.f172b != 8) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        nVar.e = eb.a(gwVar.s());
                        nVar.e(true);
                        break;
                    }
                case 6:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        nVar.f = gwVar.v();
                        nVar.f(true);
                        break;
                    }
                case 7:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        nVar.g = gwVar.v();
                        nVar.g(true);
                        break;
                    }
                case 8:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        nVar.h = gwVar.v();
                        nVar.h(true);
                        break;
                    }
                case 9:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        nVar.i = gwVar.v();
                        nVar.i(true);
                        break;
                    }
                case 10:
                    if (h.f172b != 8) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        nVar.j = gwVar.s();
                        nVar.j(true);
                        break;
                    }
                default:
                    ha.a(gwVar, h.f172b);
                    break;
            }
            gwVar.i();
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, n nVar) {
        nVar.g();
        gwVar.a(n.l);
        if (nVar.f224a != null) {
            gwVar.a(n.m);
            gwVar.a(nVar.f224a);
            gwVar.b();
        }
        if (nVar.f225b != null && nVar.a()) {
            gwVar.a(n.n);
            gwVar.a(nVar.f225b);
            gwVar.b();
        }
        if (nVar.b()) {
            gwVar.a(n.o);
            gwVar.a(nVar.c);
            gwVar.b();
        }
        if (nVar.d != null && nVar.c()) {
            gwVar.a(n.p);
            gwVar.a(nVar.d);
            gwVar.b();
        }
        if (nVar.e != null) {
            gwVar.a(n.q);
            gwVar.a(nVar.e.a());
            gwVar.b();
        }
        if (nVar.f != null) {
            gwVar.a(n.r);
            gwVar.a(nVar.f);
            gwVar.b();
        }
        if (nVar.g != null) {
            gwVar.a(n.s);
            gwVar.a(nVar.g);
            gwVar.b();
        }
        if (nVar.h != null && nVar.d()) {
            gwVar.a(n.t);
            gwVar.a(nVar.h);
            gwVar.b();
        }
        if (nVar.i != null && nVar.e()) {
            gwVar.a(n.u);
            gwVar.a(nVar.i);
            gwVar.b();
        }
        if (nVar.f()) {
            gwVar.a(n.v);
            gwVar.a(nVar.j);
            gwVar.b();
        }
        gwVar.c();
        gwVar.a();
    }
}
