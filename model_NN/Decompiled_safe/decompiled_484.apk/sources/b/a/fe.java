package b.a;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class fe implements fu<fe, fk>, Serializable, Cloneable {
    public static final Map<fk, gk> j;
    /* access modifiers changed from: private */
    public static final hc k = new hc("UMEnvelope");
    /* access modifiers changed from: private */
    public static final gt l = new gt("version", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final gt m = new gt("address", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final gt n = new gt("signature", (byte) 11, 3);
    /* access modifiers changed from: private */
    public static final gt o = new gt("serial_num", (byte) 8, 4);
    /* access modifiers changed from: private */
    public static final gt p = new gt("ts_secs", (byte) 8, 5);
    /* access modifiers changed from: private */
    public static final gt q = new gt("length", (byte) 8, 6);
    /* access modifiers changed from: private */
    public static final gt r = new gt("entity", (byte) 11, 7);
    /* access modifiers changed from: private */
    public static final gt s = new gt("guid", (byte) 11, 8);
    /* access modifiers changed from: private */
    public static final gt t = new gt("checksum", (byte) 11, 9);
    private static final Map<Class<? extends he>, hf> u = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f132a;

    /* renamed from: b  reason: collision with root package name */
    public String f133b;
    public String c;
    public int d;
    public int e;
    public int f;
    public ByteBuffer g;
    public String h;
    public String i;
    private byte v = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.fk, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        u.put(hg.class, new fh());
        u.put(hh.class, new fj());
        EnumMap enumMap = new EnumMap(fk.class);
        enumMap.put((Object) fk.VERSION, (Object) new gk("version", (byte) 1, new gl((byte) 11)));
        enumMap.put((Object) fk.ADDRESS, (Object) new gk("address", (byte) 1, new gl((byte) 11)));
        enumMap.put((Object) fk.SIGNATURE, (Object) new gk("signature", (byte) 1, new gl((byte) 11)));
        enumMap.put((Object) fk.SERIAL_NUM, (Object) new gk("serial_num", (byte) 1, new gl((byte) 8)));
        enumMap.put((Object) fk.TS_SECS, (Object) new gk("ts_secs", (byte) 1, new gl((byte) 8)));
        enumMap.put((Object) fk.LENGTH, (Object) new gk("length", (byte) 1, new gl((byte) 8)));
        enumMap.put((Object) fk.ENTITY, (Object) new gk("entity", (byte) 1, new gl((byte) 11, true)));
        enumMap.put((Object) fk.GUID, (Object) new gk("guid", (byte) 1, new gl((byte) 11)));
        enumMap.put((Object) fk.CHECKSUM, (Object) new gk("checksum", (byte) 1, new gl((byte) 11)));
        j = Collections.unmodifiableMap(enumMap);
        gk.a(fe.class, j);
    }

    public fe a(int i2) {
        this.d = i2;
        d(true);
        return this;
    }

    public fe a(String str) {
        this.f132a = str;
        return this;
    }

    public fe a(ByteBuffer byteBuffer) {
        this.g = byteBuffer;
        return this;
    }

    public fe a(byte[] bArr) {
        a(bArr == null ? null : ByteBuffer.wrap(bArr));
        return this;
    }

    public void a(gw gwVar) {
        u.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z) {
        if (!z) {
            this.f132a = null;
        }
    }

    public boolean a() {
        return fs.a(this.v, 0);
    }

    public fe b(int i2) {
        this.e = i2;
        e(true);
        return this;
    }

    public fe b(String str) {
        this.f133b = str;
        return this;
    }

    public void b(gw gwVar) {
        u.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z) {
        if (!z) {
            this.f133b = null;
        }
    }

    public boolean b() {
        return fs.a(this.v, 1);
    }

    public fe c(int i2) {
        this.f = i2;
        f(true);
        return this;
    }

    public fe c(String str) {
        this.c = str;
        return this;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public boolean c() {
        return fs.a(this.v, 2);
    }

    public fe d(String str) {
        this.h = str;
        return this;
    }

    public void d() {
        if (this.f132a == null) {
            throw new gx("Required field 'version' was not present! Struct: " + toString());
        } else if (this.f133b == null) {
            throw new gx("Required field 'address' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new gx("Required field 'signature' was not present! Struct: " + toString());
        } else if (this.g == null) {
            throw new gx("Required field 'entity' was not present! Struct: " + toString());
        } else if (this.h == null) {
            throw new gx("Required field 'guid' was not present! Struct: " + toString());
        } else if (this.i == null) {
            throw new gx("Required field 'checksum' was not present! Struct: " + toString());
        }
    }

    public void d(boolean z) {
        this.v = fs.a(this.v, 0, z);
    }

    public fe e(String str) {
        this.i = str;
        return this;
    }

    public void e(boolean z) {
        this.v = fs.a(this.v, 1, z);
    }

    public void f(boolean z) {
        this.v = fs.a(this.v, 2, z);
    }

    public void g(boolean z) {
        if (!z) {
            this.g = null;
        }
    }

    public void h(boolean z) {
        if (!z) {
            this.h = null;
        }
    }

    public void i(boolean z) {
        if (!z) {
            this.i = null;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("UMEnvelope(");
        sb.append("version:");
        if (this.f132a == null) {
            sb.append("null");
        } else {
            sb.append(this.f132a);
        }
        sb.append(", ");
        sb.append("address:");
        if (this.f133b == null) {
            sb.append("null");
        } else {
            sb.append(this.f133b);
        }
        sb.append(", ");
        sb.append("signature:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("serial_num:");
        sb.append(this.d);
        sb.append(", ");
        sb.append("ts_secs:");
        sb.append(this.e);
        sb.append(", ");
        sb.append("length:");
        sb.append(this.f);
        sb.append(", ");
        sb.append("entity:");
        if (this.g == null) {
            sb.append("null");
        } else {
            fw.a(this.g, sb);
        }
        sb.append(", ");
        sb.append("guid:");
        if (this.h == null) {
            sb.append("null");
        } else {
            sb.append(this.h);
        }
        sb.append(", ");
        sb.append("checksum:");
        if (this.i == null) {
            sb.append("null");
        } else {
            sb.append(this.i);
        }
        sb.append(")");
        return sb.toString();
    }
}
