package b.a;

import java.nio.ByteBuffer;

public abstract class gw {
    protected hk e;

    protected gw(hk hkVar) {
        this.e = hkVar;
    }

    public abstract void a();

    public abstract void a(double d);

    public abstract void a(int i);

    public abstract void a(long j);

    public abstract void a(gt gtVar);

    public abstract void a(gu guVar);

    public abstract void a(gv gvVar);

    public abstract void a(hc hcVar);

    public abstract void a(String str);

    public abstract void a(ByteBuffer byteBuffer);

    public abstract void a(short s);

    public abstract void a(boolean z);

    public abstract void b();

    public abstract void c();

    public abstract void d();

    public abstract void e();

    public abstract hc f();

    public abstract void g();

    public abstract gt h();

    public abstract void i();

    public abstract gv j();

    public abstract void k();

    public abstract gu l();

    public abstract void m();

    public abstract hb n();

    public abstract void o();

    public abstract boolean p();

    public abstract byte q();

    public abstract short r();

    public abstract int s();

    public abstract long t();

    public abstract double u();

    public abstract String v();

    public abstract ByteBuffer w();

    public void x() {
    }

    public Class<? extends he> y() {
        return hg.class;
    }
}
