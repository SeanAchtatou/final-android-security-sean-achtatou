package b.a;

import java.util.ArrayList;
import java.util.BitSet;

class cl extends hh<ch> {
    private cl() {
    }

    public void a(gw gwVar, ch chVar) {
        hd hdVar = (hd) gwVar;
        hdVar.a(chVar.f83a);
        BitSet bitSet = new BitSet();
        if (chVar.b()) {
            bitSet.set(0);
        }
        if (chVar.c()) {
            bitSet.set(1);
        }
        if (chVar.d()) {
            bitSet.set(2);
        }
        hdVar.a(bitSet, 3);
        if (chVar.b()) {
            hdVar.a(chVar.f84b.size());
            for (ai b2 : chVar.f84b) {
                b2.b(hdVar);
            }
        }
        if (chVar.c()) {
            hdVar.a(chVar.c.size());
            for (aq b3 : chVar.c) {
                b3.b(hdVar);
            }
        }
        if (chVar.d()) {
            hdVar.a(chVar.d.size());
            for (aq b4 : chVar.d) {
                b4.b(hdVar);
            }
        }
    }

    public void b(gw gwVar, ch chVar) {
        hd hdVar = (hd) gwVar;
        chVar.f83a = hdVar.v();
        chVar.a(true);
        BitSet b2 = hdVar.b(3);
        if (b2.get(0)) {
            gu guVar = new gu((byte) 12, hdVar.s());
            chVar.f84b = new ArrayList(guVar.f174b);
            for (int i = 0; i < guVar.f174b; i++) {
                ai aiVar = new ai();
                aiVar.a(hdVar);
                chVar.f84b.add(aiVar);
            }
            chVar.b(true);
        }
        if (b2.get(1)) {
            gu guVar2 = new gu((byte) 12, hdVar.s());
            chVar.c = new ArrayList(guVar2.f174b);
            for (int i2 = 0; i2 < guVar2.f174b; i2++) {
                aq aqVar = new aq();
                aqVar.a(hdVar);
                chVar.c.add(aqVar);
            }
            chVar.c(true);
        }
        if (b2.get(2)) {
            gu guVar3 = new gu((byte) 12, hdVar.s());
            chVar.d = new ArrayList(guVar3.f174b);
            for (int i3 = 0; i3 < guVar3.f174b; i3++) {
                aq aqVar2 = new aq();
                aqVar2.a(hdVar);
                chVar.d.add(aqVar2);
            }
            chVar.d(true);
        }
    }
}
