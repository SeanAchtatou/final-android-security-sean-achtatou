package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum ea implements gb {
    RESP_CODE(1, "resp_code"),
    MSG(2, "msg"),
    IMPRINT(3, "imprint");
    
    private static final Map<String, ea> d = new HashMap();
    private final short e;
    private final String f;

    static {
        Iterator it = EnumSet.allOf(ea.class).iterator();
        while (it.hasNext()) {
            ea eaVar = (ea) it.next();
            d.put(eaVar.b(), eaVar);
        }
    }

    private ea(short s, String str) {
        this.e = s;
        this.f = str;
    }

    public short a() {
        return this.e;
    }

    public String b() {
        return this.f;
    }
}
