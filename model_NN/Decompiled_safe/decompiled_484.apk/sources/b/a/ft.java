package b.a;

public class ft {

    /* renamed from: a  reason: collision with root package name */
    private short[] f143a;

    /* renamed from: b  reason: collision with root package name */
    private int f144b = -1;

    public ft(int i) {
        this.f143a = new short[i];
    }

    private void c() {
        short[] sArr = new short[(this.f143a.length * 2)];
        System.arraycopy(this.f143a, 0, sArr, 0, this.f143a.length);
        this.f143a = sArr;
    }

    public short a() {
        short[] sArr = this.f143a;
        int i = this.f144b;
        this.f144b = i - 1;
        return sArr[i];
    }

    public void a(short s) {
        if (this.f143a.length == this.f144b + 1) {
            c();
        }
        short[] sArr = this.f143a;
        int i = this.f144b + 1;
        this.f144b = i;
        sArr[i] = s;
    }

    public void b() {
        this.f144b = -1;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<ShortStack vector:[");
        for (int i = 0; i < this.f143a.length; i++) {
            if (i != 0) {
                sb.append(" ");
            }
            if (i == this.f144b) {
                sb.append(">>");
            }
            sb.append((int) this.f143a[i]);
            if (i == this.f144b) {
                sb.append("<<");
            }
        }
        sb.append("]>");
        return sb.toString();
    }
}
