package b.a;

import java.util.ArrayList;
import java.util.BitSet;

class eu extends hh<eq> {
    private eu() {
    }

    public void a(gw gwVar, eq eqVar) {
        hd hdVar = (hd) gwVar;
        eqVar.f122a.b(hdVar);
        eqVar.f123b.b(hdVar);
        eqVar.c.b(hdVar);
        eqVar.d.b(hdVar);
        BitSet bitSet = new BitSet();
        if (eqVar.a()) {
            bitSet.set(0);
        }
        if (eqVar.d()) {
            bitSet.set(1);
        }
        if (eqVar.f()) {
            bitSet.set(2);
        }
        if (eqVar.g()) {
            bitSet.set(3);
        }
        if (eqVar.h()) {
            bitSet.set(4);
        }
        hdVar.a(bitSet, 5);
        if (eqVar.a()) {
            eqVar.e.b(hdVar);
        }
        if (eqVar.d()) {
            hdVar.a(eqVar.f.size());
            for (ch b2 : eqVar.f) {
                b2.b(hdVar);
            }
        }
        if (eqVar.f()) {
            hdVar.a(eqVar.g.size());
            for (ec b3 : eqVar.g) {
                b3.b(hdVar);
            }
        }
        if (eqVar.g()) {
            eqVar.h.b(hdVar);
        }
        if (eqVar.h()) {
            eqVar.i.b(hdVar);
        }
    }

    public void b(gw gwVar, eq eqVar) {
        hd hdVar = (hd) gwVar;
        eqVar.f122a = new u();
        eqVar.f122a.a(hdVar);
        eqVar.a(true);
        eqVar.f123b = new n();
        eqVar.f123b.a(hdVar);
        eqVar.b(true);
        eqVar.c = new ab();
        eqVar.c.a(hdVar);
        eqVar.c(true);
        eqVar.d = new cv();
        eqVar.d.a(hdVar);
        eqVar.d(true);
        BitSet b2 = hdVar.b(5);
        if (b2.get(0)) {
            eqVar.e = new g();
            eqVar.e.a(hdVar);
            eqVar.e(true);
        }
        if (b2.get(1)) {
            gu guVar = new gu((byte) 12, hdVar.s());
            eqVar.f = new ArrayList(guVar.f174b);
            for (int i = 0; i < guVar.f174b; i++) {
                ch chVar = new ch();
                chVar.a(hdVar);
                eqVar.f.add(chVar);
            }
            eqVar.f(true);
        }
        if (b2.get(2)) {
            gu guVar2 = new gu((byte) 12, hdVar.s());
            eqVar.g = new ArrayList(guVar2.f174b);
            for (int i2 = 0; i2 < guVar2.f174b; i2++) {
                ec ecVar = new ec();
                ecVar.a(hdVar);
                eqVar.g.add(ecVar);
            }
            eqVar.g(true);
        }
        if (b2.get(3)) {
            eqVar.h = new bt();
            eqVar.h.a(hdVar);
            eqVar.h(true);
        }
        if (b2.get(4)) {
            eqVar.i = new bm();
            eqVar.i.a(hdVar);
            eqVar.i(true);
        }
    }
}
