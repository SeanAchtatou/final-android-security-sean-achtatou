package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum ep implements gb {
    UPLOAD_TRAFFIC(1, "upload_traffic"),
    DOWNLOAD_TRAFFIC(2, "download_traffic");
    
    private static final Map<String, ep> c = new HashMap();
    private final short d;
    private final String e;

    static {
        Iterator it = EnumSet.allOf(ep.class).iterator();
        while (it.hasNext()) {
            ep epVar = (ep) it.next();
            c.put(epVar.b(), epVar);
        }
    }

    private ep(short s, String str) {
        this.d = s;
        this.e = str;
    }

    public short a() {
        return this.d;
    }

    public String b() {
        return this.e;
    }
}
