package b.a;

import com.qihoo.dynamic.util.Md5Util;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

public class gp extends gw {
    private static final hc f = new hc();

    /* renamed from: a  reason: collision with root package name */
    protected boolean f164a = false;

    /* renamed from: b  reason: collision with root package name */
    protected boolean f165b = true;
    protected int c;
    protected boolean d = false;
    private byte[] g = new byte[1];
    private byte[] h = new byte[2];
    private byte[] i = new byte[4];
    private byte[] j = new byte[8];
    private byte[] k = new byte[1];
    private byte[] l = new byte[2];
    private byte[] m = new byte[4];
    private byte[] n = new byte[8];

    public gp(hk hkVar, boolean z, boolean z2) {
        super(hkVar);
        this.f164a = z;
        this.f165b = z2;
    }

    private int a(byte[] bArr, int i2, int i3) {
        d(i3);
        return this.e.d(bArr, i2, i3);
    }

    public void a() {
    }

    public void a(byte b2) {
        this.g[0] = b2;
        this.e.b(this.g, 0, 1);
    }

    public void a(double d2) {
        a(Double.doubleToLongBits(d2));
    }

    public void a(int i2) {
        this.i[0] = (byte) ((i2 >> 24) & 255);
        this.i[1] = (byte) ((i2 >> 16) & 255);
        this.i[2] = (byte) ((i2 >> 8) & 255);
        this.i[3] = (byte) (i2 & 255);
        this.e.b(this.i, 0, 4);
    }

    public void a(long j2) {
        this.j[0] = (byte) ((int) ((j2 >> 56) & 255));
        this.j[1] = (byte) ((int) ((j2 >> 48) & 255));
        this.j[2] = (byte) ((int) ((j2 >> 40) & 255));
        this.j[3] = (byte) ((int) ((j2 >> 32) & 255));
        this.j[4] = (byte) ((int) ((j2 >> 24) & 255));
        this.j[5] = (byte) ((int) ((j2 >> 16) & 255));
        this.j[6] = (byte) ((int) ((j2 >> 8) & 255));
        this.j[7] = (byte) ((int) (255 & j2));
        this.e.b(this.j, 0, 8);
    }

    public void a(gt gtVar) {
        a(gtVar.f172b);
        a(gtVar.c);
    }

    public void a(gu guVar) {
        a(guVar.f173a);
        a(guVar.f174b);
    }

    public void a(gv gvVar) {
        a(gvVar.f175a);
        a(gvVar.f176b);
        a(gvVar.c);
    }

    public void a(hc hcVar) {
    }

    public void a(String str) {
        try {
            byte[] bytes = str.getBytes(Md5Util.DEFAULT_CHARSET);
            a(bytes.length);
            this.e.b(bytes, 0, bytes.length);
        } catch (UnsupportedEncodingException e) {
            throw new ga("JVM DOES NOT SUPPORT UTF-8");
        }
    }

    public void a(ByteBuffer byteBuffer) {
        int limit = byteBuffer.limit() - byteBuffer.position();
        a(limit);
        this.e.b(byteBuffer.array(), byteBuffer.position() + byteBuffer.arrayOffset(), limit);
    }

    public void a(short s) {
        this.h[0] = (byte) ((s >> 8) & 255);
        this.h[1] = (byte) (s & 255);
        this.e.b(this.h, 0, 2);
    }

    public void a(boolean z) {
        a(z ? (byte) 1 : 0);
    }

    public String b(int i2) {
        try {
            d(i2);
            byte[] bArr = new byte[i2];
            this.e.d(bArr, 0, i2);
            return new String(bArr, Md5Util.DEFAULT_CHARSET);
        } catch (UnsupportedEncodingException e) {
            throw new ga("JVM DOES NOT SUPPORT UTF-8");
        }
    }

    public void b() {
    }

    public void c() {
        a((byte) 0);
    }

    public void c(int i2) {
        this.c = i2;
        this.d = true;
    }

    public void d() {
    }

    /* access modifiers changed from: protected */
    public void d(int i2) {
        if (i2 < 0) {
            throw new gx("Negative length: " + i2);
        } else if (this.d) {
            this.c -= i2;
            if (this.c < 0) {
                throw new gx("Message length exceeded: " + i2);
            }
        }
    }

    public void e() {
    }

    public hc f() {
        return f;
    }

    public void g() {
    }

    public gt h() {
        byte q = q();
        return new gt("", q, q == 0 ? 0 : r());
    }

    public void i() {
    }

    public gv j() {
        return new gv(q(), q(), s());
    }

    public void k() {
    }

    public gu l() {
        return new gu(q(), s());
    }

    public void m() {
    }

    public hb n() {
        return new hb(q(), s());
    }

    public void o() {
    }

    public boolean p() {
        return q() == 1;
    }

    public byte q() {
        if (this.e.d() >= 1) {
            byte b2 = this.e.b()[this.e.c()];
            this.e.a(1);
            return b2;
        }
        a(this.k, 0, 1);
        return this.k[0];
    }

    public short r() {
        int i2 = 0;
        byte[] bArr = this.l;
        if (this.e.d() >= 2) {
            bArr = this.e.b();
            i2 = this.e.c();
            this.e.a(2);
        } else {
            a(this.l, 0, 2);
        }
        return (short) ((bArr[i2 + 1] & 255) | ((bArr[i2] & 255) << 8));
    }

    public int s() {
        int i2 = 0;
        byte[] bArr = this.m;
        if (this.e.d() >= 4) {
            bArr = this.e.b();
            i2 = this.e.c();
            this.e.a(4);
        } else {
            a(this.m, 0, 4);
        }
        return (bArr[i2 + 3] & 255) | ((bArr[i2] & 255) << 24) | ((bArr[i2 + 1] & 255) << 16) | ((bArr[i2 + 2] & 255) << 8);
    }

    public long t() {
        int i2 = 0;
        byte[] bArr = this.n;
        if (this.e.d() >= 8) {
            bArr = this.e.b();
            i2 = this.e.c();
            this.e.a(8);
        } else {
            a(this.n, 0, 8);
        }
        return ((long) (bArr[i2 + 7] & 255)) | (((long) (bArr[i2] & 255)) << 56) | (((long) (bArr[i2 + 1] & 255)) << 48) | (((long) (bArr[i2 + 2] & 255)) << 40) | (((long) (bArr[i2 + 3] & 255)) << 32) | (((long) (bArr[i2 + 4] & 255)) << 24) | (((long) (bArr[i2 + 5] & 255)) << 16) | (((long) (bArr[i2 + 6] & 255)) << 8);
    }

    public double u() {
        return Double.longBitsToDouble(t());
    }

    public String v() {
        int s = s();
        if (this.e.d() < s) {
            return b(s);
        }
        try {
            String str = new String(this.e.b(), this.e.c(), s, Md5Util.DEFAULT_CHARSET);
            this.e.a(s);
            return str;
        } catch (UnsupportedEncodingException e) {
            throw new ga("JVM DOES NOT SUPPORT UTF-8");
        }
    }

    public ByteBuffer w() {
        int s = s();
        d(s);
        if (this.e.d() >= s) {
            ByteBuffer wrap = ByteBuffer.wrap(this.e.b(), this.e.c(), s);
            this.e.a(s);
            return wrap;
        }
        byte[] bArr = new byte[s];
        this.e.d(bArr, 0, s);
        return ByteBuffer.wrap(bArr);
    }
}
