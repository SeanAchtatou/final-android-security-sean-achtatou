package b.a;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Iterator;

public class c extends ai implements hz {
    public c() {
        a(System.currentTimeMillis());
        a(ap.LEGIT);
    }

    public c(Throwable th) {
        this();
        a(a(th));
    }

    private String a(Throwable th) {
        if (th == null) {
            return null;
        }
        try {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            th.printStackTrace(printWriter);
            for (Throwable cause = th.getCause(); cause != null; cause = cause.getCause()) {
                cause.printStackTrace(printWriter);
            }
            String obj = stringWriter.toString();
            printWriter.close();
            stringWriter.close();
            return obj;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void a(eq eqVar, String str) {
        ch chVar;
        if (eqVar.b() > 0) {
            Iterator<ch> it = eqVar.c().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                chVar = it.next();
                if (str.equals(chVar.a())) {
                    break;
                }
            }
        }
        chVar = null;
        if (chVar == null) {
            chVar = new ch();
            chVar.a(str);
            eqVar.a(chVar);
        }
        chVar.a(this);
    }
}
