package b.a;

public final class gv {

    /* renamed from: a  reason: collision with root package name */
    public final byte f175a;

    /* renamed from: b  reason: collision with root package name */
    public final byte f176b;
    public final int c;

    public gv() {
        this((byte) 0, (byte) 0, 0);
    }

    public gv(byte b2, byte b3, int i) {
        this.f175a = b2;
        this.f176b = b3;
        this.c = i;
    }
}
