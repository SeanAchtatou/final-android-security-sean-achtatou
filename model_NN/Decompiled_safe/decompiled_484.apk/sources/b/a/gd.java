package b.a;

import b.a.gb;
import b.a.gd;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public abstract class gd<T extends gd<?, ?>, F extends gb> implements fu<T, F> {

    /* renamed from: a  reason: collision with root package name */
    private static final Map<Class<? extends he>, hf> f154a = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    protected Object f155b = null;
    protected F c = null;

    static {
        f154a.put(hg.class, new gg());
        f154a.put(hh.class, new gi());
    }

    protected gd() {
    }

    /* access modifiers changed from: protected */
    public abstract gt a(gb gbVar);

    /* access modifiers changed from: protected */
    public abstract hc a();

    /* access modifiers changed from: protected */
    public abstract Object a(gw gwVar, gt gtVar);

    /* access modifiers changed from: protected */
    public abstract Object a(gw gwVar, short s);

    public void a(gw gwVar) {
        f154a.get(gwVar.y()).b().b(gwVar, this);
    }

    public F b() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public abstract F b(short s);

    public void b(gw gwVar) {
        f154a.get(gwVar.y()).b().a(gwVar, this);
    }

    public Object c() {
        return this.f155b;
    }

    /* access modifiers changed from: protected */
    public abstract void c(gw gwVar);

    /* access modifiers changed from: protected */
    public abstract void d(gw gwVar);

    public boolean d() {
        return this.c != null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<");
        sb.append(getClass().getSimpleName());
        sb.append(" ");
        if (b() != null) {
            Object c2 = c();
            sb.append(a(b()).f171a);
            sb.append(":");
            if (c2 instanceof ByteBuffer) {
                fw.a((ByteBuffer) c2, sb);
            } else {
                sb.append(c2.toString());
            }
        }
        sb.append(">");
        return sb.toString();
    }
}
