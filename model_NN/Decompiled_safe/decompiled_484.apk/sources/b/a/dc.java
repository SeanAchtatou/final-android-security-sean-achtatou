package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class dc implements fu<dc, di>, Serializable, Cloneable {
    public static final Map<di, gk> c;
    /* access modifiers changed from: private */
    public static final hc d = new hc("Page");
    /* access modifiers changed from: private */
    public static final gt e = new gt("page_name", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final gt f = new gt("duration", (byte) 10, 2);
    private static final Map<Class<? extends he>, hf> g = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f95a;

    /* renamed from: b  reason: collision with root package name */
    public long f96b;
    private byte h = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.di, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        g.put(hg.class, new df());
        g.put(hh.class, new dh());
        EnumMap enumMap = new EnumMap(di.class);
        enumMap.put((Object) di.PAGE_NAME, (Object) new gk("page_name", (byte) 1, new gl((byte) 11)));
        enumMap.put((Object) di.DURATION, (Object) new gk("duration", (byte) 1, new gl((byte) 10)));
        c = Collections.unmodifiableMap(enumMap);
        gk.a(dc.class, c);
    }

    public dc a(long j) {
        this.f96b = j;
        b(true);
        return this;
    }

    public dc a(String str) {
        this.f95a = str;
        return this;
    }

    public void a(gw gwVar) {
        g.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z) {
        if (!z) {
            this.f95a = null;
        }
    }

    public boolean a() {
        return fs.a(this.h, 0);
    }

    public void b() {
        if (this.f95a == null) {
            throw new gx("Required field 'page_name' was not present! Struct: " + toString());
        }
    }

    public void b(gw gwVar) {
        g.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z) {
        this.h = fs.a(this.h, 0, z);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Page(");
        sb.append("page_name:");
        if (this.f95a == null) {
            sb.append("null");
        } else {
            sb.append(this.f95a);
        }
        sb.append(", ");
        sb.append("duration:");
        sb.append(this.f96b);
        sb.append(")");
        return sb.toString();
    }
}
