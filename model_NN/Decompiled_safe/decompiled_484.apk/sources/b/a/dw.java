package b.a;

class dw extends hg<du> {
    private dw() {
    }

    /* renamed from: a */
    public void b(gw gwVar, du duVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                if (!duVar.a()) {
                    throw new gx("Required field 'resp_code' was not found in serialized data! Struct: " + toString());
                }
                duVar.f();
                return;
            }
            switch (h.c) {
                case 1:
                    if (h.f172b != 8) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        duVar.f108a = gwVar.s();
                        duVar.a(true);
                        break;
                    }
                case 2:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        duVar.f109b = gwVar.v();
                        duVar.b(true);
                        break;
                    }
                case 3:
                    if (h.f172b != 12) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        duVar.c = new bt();
                        duVar.c.a(gwVar);
                        duVar.c(true);
                        break;
                    }
                default:
                    ha.a(gwVar, h.f172b);
                    break;
            }
            gwVar.i();
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, du duVar) {
        duVar.f();
        gwVar.a(du.e);
        gwVar.a(du.f);
        gwVar.a(duVar.f108a);
        gwVar.b();
        if (duVar.f109b != null && duVar.c()) {
            gwVar.a(du.g);
            gwVar.a(duVar.f109b);
            gwVar.b();
        }
        if (duVar.c != null && duVar.e()) {
            gwVar.a(du.h);
            duVar.c.b(gwVar);
            gwVar.b();
        }
        gwVar.c();
        gwVar.a();
    }
}
