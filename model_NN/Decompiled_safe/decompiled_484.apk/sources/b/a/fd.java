package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum fd implements gb {
    GENDER(1, "gender"),
    AGE(2, "age"),
    ID(3, "id"),
    SOURCE(4, "source");
    
    private static final Map<String, fd> e = new HashMap();
    private final short f;
    private final String g;

    static {
        Iterator it = EnumSet.allOf(fd.class).iterator();
        while (it.hasNext()) {
            fd fdVar = (fd) it.next();
            e.put(fdVar.b(), fdVar);
        }
    }

    private fd(short s, String str) {
        this.f = s;
        this.g = str;
    }

    public short a() {
        return this.f;
    }

    public String b() {
        return this.g;
    }
}
