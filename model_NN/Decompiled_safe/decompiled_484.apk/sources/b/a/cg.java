package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum cg implements gb {
    VALUE(1, "value"),
    TS(2, "ts"),
    GUID(3, "guid");
    
    private static final Map<String, cg> d = new HashMap();
    private final short e;
    private final String f;

    static {
        Iterator it = EnumSet.allOf(cg.class).iterator();
        while (it.hasNext()) {
            cg cgVar = (cg) it.next();
            d.put(cgVar.b(), cgVar);
        }
    }

    private cg(short s, String str) {
        this.e = s;
        this.f = str;
    }

    public short a() {
        return this.e;
    }

    public String b() {
        return this.f;
    }
}
