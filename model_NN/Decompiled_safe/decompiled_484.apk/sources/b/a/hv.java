package b.a;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class hv {

    /* renamed from: a  reason: collision with root package name */
    private final String f204a = "umeng_event_snapshot";

    /* renamed from: b  reason: collision with root package name */
    private boolean f205b = false;
    private SharedPreferences c;
    private Map<String, ArrayList<Object>> d = new HashMap();

    public hv(Context context) {
        this.c = id.a(context, "umeng_event_snapshot");
    }

    public void a(boolean z) {
        this.f205b = z;
    }
}
