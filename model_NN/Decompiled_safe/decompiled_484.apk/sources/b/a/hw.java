package b.a;

import com.f.a.a;
import java.lang.Thread;

public class hw implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    private Thread.UncaughtExceptionHandler f206a;

    /* renamed from: b  reason: collision with root package name */
    private ic f207b;

    public hw() {
        if (Thread.getDefaultUncaughtExceptionHandler() != this) {
            this.f206a = Thread.getDefaultUncaughtExceptionHandler();
            Thread.setDefaultUncaughtExceptionHandler(this);
        }
    }

    private void a(Throwable th) {
        if (a.m) {
            this.f207b.a(th);
        } else {
            this.f207b.a(null);
        }
    }

    public void a(ic icVar) {
        this.f207b = icVar;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        a(th);
        if (this.f206a != null && this.f206a != Thread.getDefaultUncaughtExceptionHandler()) {
            this.f206a.uncaughtException(thread, th);
        }
    }
}
