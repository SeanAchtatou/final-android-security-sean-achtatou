package b.a;

import java.io.ByteArrayOutputStream;

public class gc {

    /* renamed from: a  reason: collision with root package name */
    private final ByteArrayOutputStream f152a;

    /* renamed from: b  reason: collision with root package name */
    private final hi f153b;
    private gw c;

    public gc() {
        this(new gs());
    }

    public gc(gz gzVar) {
        this.f152a = new ByteArrayOutputStream();
        this.f153b = new hi(this.f152a);
        this.c = gzVar.a(this.f153b);
    }

    public byte[] a(fu fuVar) {
        this.f152a.reset();
        fuVar.b(this.c);
        return this.f152a.toByteArray();
    }
}
