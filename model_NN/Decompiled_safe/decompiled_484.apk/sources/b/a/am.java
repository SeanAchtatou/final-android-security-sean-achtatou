package b.a;

import java.util.BitSet;

class am extends hh<ai> {
    private am() {
    }

    public void a(gw gwVar, ai aiVar) {
        hd hdVar = (hd) gwVar;
        hdVar.a(aiVar.f51a);
        hdVar.a(aiVar.f52b);
        BitSet bitSet = new BitSet();
        if (aiVar.b()) {
            bitSet.set(0);
        }
        hdVar.a(bitSet, 1);
        if (aiVar.b()) {
            hdVar.a(aiVar.c.a());
        }
    }

    public void b(gw gwVar, ai aiVar) {
        hd hdVar = (hd) gwVar;
        aiVar.f51a = hdVar.t();
        aiVar.a(true);
        aiVar.f52b = hdVar.v();
        aiVar.b(true);
        if (hdVar.b(1).get(0)) {
            aiVar.c = ap.a(hdVar.s());
            aiVar.c(true);
        }
    }
}
