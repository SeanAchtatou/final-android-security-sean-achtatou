package b.a;

import java.util.HashMap;
import java.util.Map;

class as extends hg<aq> {
    private as() {
    }

    /* renamed from: a */
    public void b(gw gwVar, aq aqVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                if (!aqVar.c()) {
                    throw new gx("Required field 'ts' was not found in serialized data! Struct: " + toString());
                }
                aqVar.d();
                return;
            }
            switch (h.c) {
                case 1:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        aqVar.f57a = gwVar.v();
                        aqVar.a(true);
                        break;
                    }
                case 2:
                    if (h.f172b != 13) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        gv j = gwVar.j();
                        aqVar.f58b = new HashMap(j.c * 2);
                        for (int i = 0; i < j.c; i++) {
                            String v = gwVar.v();
                            dj djVar = new dj();
                            djVar.a(gwVar);
                            aqVar.f58b.put(v, djVar);
                        }
                        gwVar.k();
                        aqVar.b(true);
                        break;
                    }
                case 3:
                    if (h.f172b != 10) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        aqVar.c = gwVar.t();
                        aqVar.c(true);
                        break;
                    }
                case 4:
                    if (h.f172b != 8) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        aqVar.d = gwVar.s();
                        aqVar.d(true);
                        break;
                    }
                case 5:
                    if (h.f172b != 10) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        aqVar.e = gwVar.t();
                        aqVar.e(true);
                        break;
                    }
                default:
                    ha.a(gwVar, h.f172b);
                    break;
            }
            gwVar.i();
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, aq aqVar) {
        aqVar.d();
        gwVar.a(aq.g);
        if (aqVar.f57a != null) {
            gwVar.a(aq.h);
            gwVar.a(aqVar.f57a);
            gwVar.b();
        }
        if (aqVar.f58b != null) {
            gwVar.a(aq.i);
            gwVar.a(new gv((byte) 11, (byte) 12, aqVar.f58b.size()));
            for (Map.Entry next : aqVar.f58b.entrySet()) {
                gwVar.a((String) next.getKey());
                ((dj) next.getValue()).b(gwVar);
            }
            gwVar.d();
            gwVar.b();
        }
        if (aqVar.a()) {
            gwVar.a(aq.j);
            gwVar.a(aqVar.c);
            gwVar.b();
        }
        if (aqVar.b()) {
            gwVar.a(aq.k);
            gwVar.a(aqVar.d);
            gwVar.b();
        }
        gwVar.a(aq.l);
        gwVar.a(aqVar.e);
        gwVar.b();
        gwVar.c();
        gwVar.a();
    }
}
