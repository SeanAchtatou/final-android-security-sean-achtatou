package b.a;

import android.content.Context;
import android.os.Build;
import com.f.a.a;
import com.f.a.b;
import com.qihoo.messenger.util.QHeader;
import java.util.ArrayList;
import java.util.List;

public class ia {

    /* renamed from: a  reason: collision with root package name */
    private List<hz> f210a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    private g f211b = null;
    private n c = null;
    private ab d = null;
    private cv e = null;
    private Context f = null;

    public ia(Context context) {
        this.f = context;
    }

    private void a(Context context) {
        try {
            this.c.a(a.a(context));
            this.c.e(a.b(context));
            if (!(a.f843a == null || a.f844b == null)) {
                this.c.f(a.f843a);
                this.c.g(a.f844b);
            }
            this.c.c(fl.o(context));
            this.c.a(eb.ANDROID);
            this.c.d("5.2.4");
            this.c.b(fl.b(context));
            this.c.a(Integer.parseInt(fl.a(context)));
            if (a.c == 1) {
                this.c.b(a.c);
                this.c.d("5.2.4.1");
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void b(Context context) {
        try {
            this.d.e(fl.a());
            this.d.a(fl.c(context));
            this.d.b(fl.d(context));
            this.d.c(fl.k(context));
            this.d.d(Build.MODEL);
            this.d.f(QHeader.os);
            this.d.g(Build.VERSION.RELEASE);
            int[] l = fl.l(context);
            if (l != null) {
                this.d.a(new dn(l[1], l[0]));
            }
            if (a.e == null || a.d != null) {
            }
            this.d.h(Build.BOARD);
            this.d.i(Build.BRAND);
            this.d.a(Build.TIME);
            this.d.j(Build.MANUFACTURER);
            this.d.k(Build.ID);
            this.d.l(Build.DEVICE);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void c(Context context) {
        try {
            String[] e2 = fl.e(context);
            if ("Wi-Fi".equals(e2[0])) {
                this.e.a(f.ACCESS_TYPE_WIFI);
            } else if ("2G/3G".equals(e2[0])) {
                this.e.a(f.ACCESS_TYPE_2G_3G);
            } else {
                this.e.a(f.ACCESS_TYPE_UNKNOWN);
            }
            if (!"".equals(e2[1])) {
                this.e.d(e2[1]);
            }
            this.e.c(fl.m(context));
            String[] i = fl.i(context);
            this.e.b(i[0]);
            this.e.a(i[1]);
            this.e.a(fl.h(context));
            if (a.g != 0 || a.f != null || a.h != null || a.i != null) {
                ex exVar = new ex();
                exVar.a(a.g);
                exVar.a(b.a(a.f));
                exVar.a(a.h);
                exVar.b(a.i);
                this.e.a(exVar);
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    private String h() {
        return id.a(this.f).getString("session_id", null);
    }

    public synchronized int a() {
        int size;
        size = this.f210a.size();
        if (this.f211b != null) {
            size++;
        }
        return size;
    }

    public void a(eq eqVar) {
        String h = h();
        if (h != null) {
            synchronized (this) {
                for (hz a2 : this.f210a) {
                    a2.a(eqVar, h);
                }
                this.f210a.clear();
                if (this.f211b != null) {
                    eqVar.a(this.f211b);
                    this.f211b = null;
                }
            }
            eqVar.a(b());
            eqVar.a(c());
            eqVar.a(d());
            eqVar.a(g());
            eqVar.a(e());
            eqVar.a(f());
        }
    }

    public synchronized void a(g gVar) {
        this.f211b = gVar;
    }

    public synchronized void a(hz hzVar) {
        this.f210a.add(hzVar);
    }

    public synchronized n b() {
        if (this.c == null) {
            this.c = new n();
            a(this.f);
        }
        return this.c;
    }

    public synchronized ab c() {
        if (this.d == null) {
            this.d = new ab();
            b(this.f);
        }
        return this.d;
    }

    public synchronized cv d() {
        if (this.e == null) {
            this.e = new cv();
            c(this.f);
        }
        return this.e;
    }

    public bt e() {
        try {
            return hp.b(this.f).a();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public bm f() {
        try {
            return hp.a(this.f).b();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public u g() {
        try {
            return Cif.a(this.f);
        } catch (Exception e2) {
            e2.printStackTrace();
            return new u();
        }
    }
}
