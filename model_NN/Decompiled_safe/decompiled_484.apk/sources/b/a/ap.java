package b.a;

public enum ap {
    LEGIT(1),
    ALIEN(2);
    
    private final int c;

    private ap(int i) {
        this.c = i;
    }

    public static ap a(int i) {
        switch (i) {
            case 1:
                return LEGIT;
            case 2:
                return ALIEN;
            default:
                return null;
        }
    }

    public int a() {
        return this.c;
    }
}
