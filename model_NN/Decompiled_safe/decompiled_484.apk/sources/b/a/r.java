package b.a;

import java.util.BitSet;

class r extends hh<n> {
    private r() {
    }

    public void a(gw gwVar, n nVar) {
        hd hdVar = (hd) gwVar;
        hdVar.a(nVar.f224a);
        hdVar.a(nVar.e.a());
        hdVar.a(nVar.f);
        hdVar.a(nVar.g);
        BitSet bitSet = new BitSet();
        if (nVar.a()) {
            bitSet.set(0);
        }
        if (nVar.b()) {
            bitSet.set(1);
        }
        if (nVar.c()) {
            bitSet.set(2);
        }
        if (nVar.d()) {
            bitSet.set(3);
        }
        if (nVar.e()) {
            bitSet.set(4);
        }
        if (nVar.f()) {
            bitSet.set(5);
        }
        hdVar.a(bitSet, 6);
        if (nVar.a()) {
            hdVar.a(nVar.f225b);
        }
        if (nVar.b()) {
            hdVar.a(nVar.c);
        }
        if (nVar.c()) {
            hdVar.a(nVar.d);
        }
        if (nVar.d()) {
            hdVar.a(nVar.h);
        }
        if (nVar.e()) {
            hdVar.a(nVar.i);
        }
        if (nVar.f()) {
            hdVar.a(nVar.j);
        }
    }

    public void b(gw gwVar, n nVar) {
        hd hdVar = (hd) gwVar;
        nVar.f224a = hdVar.v();
        nVar.a(true);
        nVar.e = eb.a(hdVar.s());
        nVar.e(true);
        nVar.f = hdVar.v();
        nVar.f(true);
        nVar.g = hdVar.v();
        nVar.g(true);
        BitSet b2 = hdVar.b(6);
        if (b2.get(0)) {
            nVar.f225b = hdVar.v();
            nVar.b(true);
        }
        if (b2.get(1)) {
            nVar.c = hdVar.s();
            nVar.c(true);
        }
        if (b2.get(2)) {
            nVar.d = hdVar.v();
            nVar.d(true);
        }
        if (b2.get(3)) {
            nVar.h = hdVar.v();
            nVar.h(true);
        }
        if (b2.get(4)) {
            nVar.i = hdVar.v();
            nVar.i(true);
        }
        if (b2.get(5)) {
            nVar.j = hdVar.s();
            nVar.j(true);
        }
    }
}
