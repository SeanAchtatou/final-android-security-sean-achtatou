package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class ca implements fu<ca, cg>, Serializable, Cloneable {
    public static final Map<cg, gk> d;
    /* access modifiers changed from: private */
    public static final hc e = new hc("ImprintValue");
    /* access modifiers changed from: private */
    public static final gt f = new gt("value", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final gt g = new gt("ts", (byte) 10, 2);
    /* access modifiers changed from: private */
    public static final gt h = new gt("guid", (byte) 11, 3);
    private static final Map<Class<? extends he>, hf> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f79a;

    /* renamed from: b  reason: collision with root package name */
    public long f80b;
    public String c;
    private byte j = 0;
    private cg[] k = {cg.VALUE};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.cg, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(hg.class, new cd());
        i.put(hh.class, new cf());
        EnumMap enumMap = new EnumMap(cg.class);
        enumMap.put((Object) cg.VALUE, (Object) new gk("value", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) cg.TS, (Object) new gk("ts", (byte) 1, new gl((byte) 10)));
        enumMap.put((Object) cg.GUID, (Object) new gk("guid", (byte) 1, new gl((byte) 11)));
        d = Collections.unmodifiableMap(enumMap);
        gk.a(ca.class, d);
    }

    public String a() {
        return this.f79a;
    }

    public void a(gw gwVar) {
        i.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z) {
        if (!z) {
            this.f79a = null;
        }
    }

    public void b(gw gwVar) {
        i.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z) {
        this.j = fs.a(this.j, 0, z);
    }

    public boolean b() {
        return this.f79a != null;
    }

    public long c() {
        return this.f80b;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public boolean d() {
        return fs.a(this.j, 0);
    }

    public String e() {
        return this.c;
    }

    public void f() {
        if (this.c == null) {
            throw new gx("Required field 'guid' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ImprintValue(");
        boolean z = true;
        if (b()) {
            sb.append("value:");
            if (this.f79a == null) {
                sb.append("null");
            } else {
                sb.append(this.f79a);
            }
            z = false;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("ts:");
        sb.append(this.f80b);
        sb.append(", ");
        sb.append("guid:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(")");
        return sb.toString();
    }
}
