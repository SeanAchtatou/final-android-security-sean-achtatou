package b.a;

import java.util.HashMap;
import java.util.Map;

class bv extends hg<bt> {
    private bv() {
    }

    /* renamed from: a */
    public void b(gw gwVar, bt btVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                if (!btVar.c()) {
                    throw new gx("Required field 'version' was not found in serialized data! Struct: " + toString());
                }
                btVar.e();
                return;
            }
            switch (h.c) {
                case 1:
                    if (h.f172b != 13) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        gv j = gwVar.j();
                        btVar.f75a = new HashMap(j.c * 2);
                        for (int i = 0; i < j.c; i++) {
                            String v = gwVar.v();
                            ca caVar = new ca();
                            caVar.a(gwVar);
                            btVar.f75a.put(v, caVar);
                        }
                        gwVar.k();
                        btVar.a(true);
                        break;
                    }
                case 2:
                    if (h.f172b != 8) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        btVar.f76b = gwVar.s();
                        btVar.b(true);
                        break;
                    }
                case 3:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        btVar.c = gwVar.v();
                        btVar.c(true);
                        break;
                    }
                default:
                    ha.a(gwVar, h.f172b);
                    break;
            }
            gwVar.i();
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, bt btVar) {
        btVar.e();
        gwVar.a(bt.e);
        if (btVar.f75a != null) {
            gwVar.a(bt.f);
            gwVar.a(new gv((byte) 11, (byte) 12, btVar.f75a.size()));
            for (Map.Entry next : btVar.f75a.entrySet()) {
                gwVar.a((String) next.getKey());
                ((ca) next.getValue()).b(gwVar);
            }
            gwVar.d();
            gwVar.b();
        }
        gwVar.a(bt.g);
        gwVar.a(btVar.f76b);
        gwVar.b();
        if (btVar.c != null) {
            gwVar.a(bt.h);
            gwVar.a(btVar.c);
            gwVar.b();
        }
        gwVar.c();
        gwVar.a();
    }
}
