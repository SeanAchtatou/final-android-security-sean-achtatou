package b.a;

import java.util.BitSet;

class cz extends hh<cv> {
    private cz() {
    }

    public void a(gw gwVar, cv cvVar) {
        hd hdVar = (hd) gwVar;
        BitSet bitSet = new BitSet();
        if (cvVar.a()) {
            bitSet.set(0);
        }
        if (cvVar.b()) {
            bitSet.set(1);
        }
        if (cvVar.c()) {
            bitSet.set(2);
        }
        if (cvVar.d()) {
            bitSet.set(3);
        }
        if (cvVar.e()) {
            bitSet.set(4);
        }
        if (cvVar.f()) {
            bitSet.set(5);
        }
        if (cvVar.g()) {
            bitSet.set(6);
        }
        if (cvVar.h()) {
            bitSet.set(7);
        }
        if (cvVar.i()) {
            bitSet.set(8);
        }
        if (cvVar.j()) {
            bitSet.set(9);
        }
        if (cvVar.k()) {
            bitSet.set(10);
        }
        hdVar.a(bitSet, 11);
        if (cvVar.a()) {
            hdVar.a(cvVar.f91a);
        }
        if (cvVar.b()) {
            hdVar.a(cvVar.f92b);
        }
        if (cvVar.c()) {
            hdVar.a(cvVar.c);
        }
        if (cvVar.d()) {
            hdVar.a(cvVar.d);
        }
        if (cvVar.e()) {
            hdVar.a(cvVar.e);
        }
        if (cvVar.f()) {
            hdVar.a(cvVar.f);
        }
        if (cvVar.g()) {
            hdVar.a(cvVar.g);
        }
        if (cvVar.h()) {
            hdVar.a(cvVar.h);
        }
        if (cvVar.i()) {
            hdVar.a(cvVar.i.a());
        }
        if (cvVar.j()) {
            hdVar.a(cvVar.j);
        }
        if (cvVar.k()) {
            cvVar.k.b(hdVar);
        }
    }

    public void b(gw gwVar, cv cvVar) {
        hd hdVar = (hd) gwVar;
        BitSet b2 = hdVar.b(11);
        if (b2.get(0)) {
            cvVar.f91a = hdVar.s();
            cvVar.a(true);
        }
        if (b2.get(1)) {
            cvVar.f92b = hdVar.v();
            cvVar.b(true);
        }
        if (b2.get(2)) {
            cvVar.c = hdVar.v();
            cvVar.c(true);
        }
        if (b2.get(3)) {
            cvVar.d = hdVar.u();
            cvVar.d(true);
        }
        if (b2.get(4)) {
            cvVar.e = hdVar.u();
            cvVar.e(true);
        }
        if (b2.get(5)) {
            cvVar.f = hdVar.v();
            cvVar.f(true);
        }
        if (b2.get(6)) {
            cvVar.g = hdVar.s();
            cvVar.g(true);
        }
        if (b2.get(7)) {
            cvVar.h = hdVar.v();
            cvVar.h(true);
        }
        if (b2.get(8)) {
            cvVar.i = f.a(hdVar.s());
            cvVar.i(true);
        }
        if (b2.get(9)) {
            cvVar.j = hdVar.v();
            cvVar.j(true);
        }
        if (b2.get(10)) {
            cvVar.k = new ex();
            cvVar.k.a(hdVar);
            cvVar.k(true);
        }
    }
}
