package b.a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum dl implements gb {
    STRING_VALUE(1, "string_value"),
    LONG_VALUE(2, "long_value");
    
    private static final Map<String, dl> c = new HashMap();
    private final short d;
    private final String e;

    static {
        Iterator it = EnumSet.allOf(dl.class).iterator();
        while (it.hasNext()) {
            dl dlVar = (dl) it.next();
            c.put(dlVar.b(), dlVar);
        }
    }

    private dl(short s, String str) {
        this.d = s;
        this.e = str;
    }

    public static dl a(int i) {
        switch (i) {
            case 1:
                return STRING_VALUE;
            case 2:
                return LONG_VALUE;
            default:
                return null;
        }
    }

    public static dl b(int i) {
        dl a2 = a(i);
        if (a2 != null) {
            return a2;
        }
        throw new IllegalArgumentException("Field " + i + " doesn't exist!");
    }

    public short a() {
        return this.d;
    }

    public String b() {
        return this.e;
    }
}
