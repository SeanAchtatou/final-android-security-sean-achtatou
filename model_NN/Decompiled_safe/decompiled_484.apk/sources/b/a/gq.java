package b.a;

public class gq implements gz {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f166a;

    /* renamed from: b  reason: collision with root package name */
    protected boolean f167b;
    protected int c;

    public gq() {
        this(false, true);
    }

    public gq(boolean z, boolean z2) {
        this(z, z2, 0);
    }

    public gq(boolean z, boolean z2, int i) {
        this.f166a = false;
        this.f167b = true;
        this.f166a = z;
        this.f167b = z2;
        this.c = i;
    }

    public gw a(hk hkVar) {
        gp gpVar = new gp(hkVar, this.f166a, this.f167b);
        if (this.c != 0) {
            gpVar.c(this.c);
        }
        return gpVar;
    }
}
