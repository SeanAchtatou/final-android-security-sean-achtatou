package b.a;

public class fz {

    /* renamed from: a  reason: collision with root package name */
    private final gw f148a;

    /* renamed from: b  reason: collision with root package name */
    private final hj f149b;

    public fz() {
        this(new gs());
    }

    public fz(gz gzVar) {
        this.f149b = new hj();
        this.f148a = gzVar.a(this.f149b);
    }

    public void a(fu fuVar, byte[] bArr) {
        try {
            this.f149b.a(bArr);
            fuVar.a(this.f148a);
        } finally {
            this.f149b.a();
            this.f148a.x();
        }
    }
}
