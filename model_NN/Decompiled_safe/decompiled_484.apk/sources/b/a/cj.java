package b.a;

import java.util.ArrayList;

class cj extends hg<ch> {
    private cj() {
    }

    /* renamed from: a */
    public void b(gw gwVar, ch chVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                chVar.e();
                return;
            }
            switch (h.c) {
                case 1:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        chVar.f83a = gwVar.v();
                        chVar.a(true);
                        break;
                    }
                case 2:
                    if (h.f172b != 15) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        gu l = gwVar.l();
                        chVar.f84b = new ArrayList(l.f174b);
                        for (int i = 0; i < l.f174b; i++) {
                            ai aiVar = new ai();
                            aiVar.a(gwVar);
                            chVar.f84b.add(aiVar);
                        }
                        gwVar.m();
                        chVar.b(true);
                        break;
                    }
                case 3:
                    if (h.f172b != 15) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        gu l2 = gwVar.l();
                        chVar.c = new ArrayList(l2.f174b);
                        for (int i2 = 0; i2 < l2.f174b; i2++) {
                            aq aqVar = new aq();
                            aqVar.a(gwVar);
                            chVar.c.add(aqVar);
                        }
                        gwVar.m();
                        chVar.c(true);
                        break;
                    }
                case 4:
                    if (h.f172b != 15) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        gu l3 = gwVar.l();
                        chVar.d = new ArrayList(l3.f174b);
                        for (int i3 = 0; i3 < l3.f174b; i3++) {
                            aq aqVar2 = new aq();
                            aqVar2.a(gwVar);
                            chVar.d.add(aqVar2);
                        }
                        gwVar.m();
                        chVar.d(true);
                        break;
                    }
                default:
                    ha.a(gwVar, h.f172b);
                    break;
            }
            gwVar.i();
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, ch chVar) {
        chVar.e();
        gwVar.a(ch.f);
        if (chVar.f83a != null) {
            gwVar.a(ch.g);
            gwVar.a(chVar.f83a);
            gwVar.b();
        }
        if (chVar.f84b != null && chVar.b()) {
            gwVar.a(ch.h);
            gwVar.a(new gu((byte) 12, chVar.f84b.size()));
            for (ai b2 : chVar.f84b) {
                b2.b(gwVar);
            }
            gwVar.e();
            gwVar.b();
        }
        if (chVar.c != null && chVar.c()) {
            gwVar.a(ch.i);
            gwVar.a(new gu((byte) 12, chVar.c.size()));
            for (aq b3 : chVar.c) {
                b3.b(gwVar);
            }
            gwVar.e();
            gwVar.b();
        }
        if (chVar.d != null && chVar.d()) {
            gwVar.a(ch.j);
            gwVar.a(new gu((byte) 12, chVar.d.size()));
            for (aq b4 : chVar.d) {
                b4.b(gwVar);
            }
            gwVar.e();
            gwVar.b();
        }
        gwVar.c();
        gwVar.a();
    }
}
