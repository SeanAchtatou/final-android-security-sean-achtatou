package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class ab implements fu<ab, ah>, Serializable, Cloneable {
    /* access modifiers changed from: private */
    public static final gt A = new gt("os_version", (byte) 11, 8);
    /* access modifiers changed from: private */
    public static final gt B = new gt("resolution", (byte) 12, 9);
    /* access modifiers changed from: private */
    public static final gt C = new gt("is_jailbroken", (byte) 2, 10);
    /* access modifiers changed from: private */
    public static final gt D = new gt("is_pirated", (byte) 2, 11);
    /* access modifiers changed from: private */
    public static final gt E = new gt("device_board", (byte) 11, 12);
    /* access modifiers changed from: private */
    public static final gt F = new gt("device_brand", (byte) 11, 13);
    /* access modifiers changed from: private */
    public static final gt G = new gt("device_manutime", (byte) 10, 14);
    /* access modifiers changed from: private */
    public static final gt H = new gt("device_manufacturer", (byte) 11, 15);
    /* access modifiers changed from: private */
    public static final gt I = new gt("device_manuid", (byte) 11, 16);
    /* access modifiers changed from: private */
    public static final gt J = new gt("device_name", (byte) 11, 17);
    private static final Map<Class<? extends he>, hf> K = new HashMap();
    public static final Map<ah, gk> r;
    /* access modifiers changed from: private */
    public static final hc s = new hc("DeviceInfo");
    /* access modifiers changed from: private */
    public static final gt t = new gt("device_id", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final gt u = new gt("idmd5", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final gt v = new gt("mac_address", (byte) 11, 3);
    /* access modifiers changed from: private */
    public static final gt w = new gt("open_udid", (byte) 11, 4);
    /* access modifiers changed from: private */
    public static final gt x = new gt("model", (byte) 11, 5);
    /* access modifiers changed from: private */
    public static final gt y = new gt("cpu", (byte) 11, 6);
    /* access modifiers changed from: private */
    public static final gt z = new gt("os", (byte) 11, 7);
    private byte L = 0;
    private ah[] M = {ah.DEVICE_ID, ah.IDMD5, ah.MAC_ADDRESS, ah.OPEN_UDID, ah.MODEL, ah.CPU, ah.OS, ah.OS_VERSION, ah.RESOLUTION, ah.IS_JAILBROKEN, ah.IS_PIRATED, ah.DEVICE_BOARD, ah.DEVICE_BRAND, ah.DEVICE_MANUTIME, ah.DEVICE_MANUFACTURER, ah.DEVICE_MANUID, ah.DEVICE_NAME};

    /* renamed from: a  reason: collision with root package name */
    public String f47a;

    /* renamed from: b  reason: collision with root package name */
    public String f48b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public String h;
    public dn i;
    public boolean j;
    public boolean k;
    public String l;
    public String m;
    public long n;
    public String o;
    public String p;
    public String q;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.ah, b.a.gk]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        K.put(hg.class, new ae());
        K.put(hh.class, new ag());
        EnumMap enumMap = new EnumMap(ah.class);
        enumMap.put((Object) ah.DEVICE_ID, (Object) new gk("device_id", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) ah.IDMD5, (Object) new gk("idmd5", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) ah.MAC_ADDRESS, (Object) new gk("mac_address", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) ah.OPEN_UDID, (Object) new gk("open_udid", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) ah.MODEL, (Object) new gk("model", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) ah.CPU, (Object) new gk("cpu", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) ah.OS, (Object) new gk("os", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) ah.OS_VERSION, (Object) new gk("os_version", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) ah.RESOLUTION, (Object) new gk("resolution", (byte) 2, new go((byte) 12, dn.class)));
        enumMap.put((Object) ah.IS_JAILBROKEN, (Object) new gk("is_jailbroken", (byte) 2, new gl((byte) 2)));
        enumMap.put((Object) ah.IS_PIRATED, (Object) new gk("is_pirated", (byte) 2, new gl((byte) 2)));
        enumMap.put((Object) ah.DEVICE_BOARD, (Object) new gk("device_board", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) ah.DEVICE_BRAND, (Object) new gk("device_brand", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) ah.DEVICE_MANUTIME, (Object) new gk("device_manutime", (byte) 2, new gl((byte) 10)));
        enumMap.put((Object) ah.DEVICE_MANUFACTURER, (Object) new gk("device_manufacturer", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) ah.DEVICE_MANUID, (Object) new gk("device_manuid", (byte) 2, new gl((byte) 11)));
        enumMap.put((Object) ah.DEVICE_NAME, (Object) new gk("device_name", (byte) 2, new gl((byte) 11)));
        r = Collections.unmodifiableMap(enumMap);
        gk.a(ab.class, r);
    }

    public ab a(long j2) {
        this.n = j2;
        n(true);
        return this;
    }

    public ab a(dn dnVar) {
        this.i = dnVar;
        return this;
    }

    public ab a(String str) {
        this.f47a = str;
        return this;
    }

    public void a(gw gwVar) {
        K.get(gwVar.y()).b().b(gwVar, this);
    }

    public void a(boolean z2) {
        if (!z2) {
            this.f47a = null;
        }
    }

    public boolean a() {
        return this.f47a != null;
    }

    public ab b(String str) {
        this.f48b = str;
        return this;
    }

    public void b(gw gwVar) {
        K.get(gwVar.y()).b().a(gwVar, this);
    }

    public void b(boolean z2) {
        if (!z2) {
            this.f48b = null;
        }
    }

    public boolean b() {
        return this.f48b != null;
    }

    public ab c(String str) {
        this.c = str;
        return this;
    }

    public void c(boolean z2) {
        if (!z2) {
            this.c = null;
        }
    }

    public boolean c() {
        return this.c != null;
    }

    public ab d(String str) {
        this.e = str;
        return this;
    }

    public void d(boolean z2) {
        if (!z2) {
            this.d = null;
        }
    }

    public boolean d() {
        return this.d != null;
    }

    public ab e(String str) {
        this.f = str;
        return this;
    }

    public void e(boolean z2) {
        if (!z2) {
            this.e = null;
        }
    }

    public boolean e() {
        return this.e != null;
    }

    public ab f(String str) {
        this.g = str;
        return this;
    }

    public void f(boolean z2) {
        if (!z2) {
            this.f = null;
        }
    }

    public boolean f() {
        return this.f != null;
    }

    public ab g(String str) {
        this.h = str;
        return this;
    }

    public void g(boolean z2) {
        if (!z2) {
            this.g = null;
        }
    }

    public boolean g() {
        return this.g != null;
    }

    public ab h(String str) {
        this.l = str;
        return this;
    }

    public void h(boolean z2) {
        if (!z2) {
            this.h = null;
        }
    }

    public boolean h() {
        return this.h != null;
    }

    public ab i(String str) {
        this.m = str;
        return this;
    }

    public void i(boolean z2) {
        if (!z2) {
            this.i = null;
        }
    }

    public boolean i() {
        return this.i != null;
    }

    public ab j(String str) {
        this.o = str;
        return this;
    }

    public void j(boolean z2) {
        this.L = fs.a(this.L, 0, z2);
    }

    public boolean j() {
        return fs.a(this.L, 0);
    }

    public ab k(String str) {
        this.p = str;
        return this;
    }

    public void k(boolean z2) {
        this.L = fs.a(this.L, 1, z2);
    }

    public boolean k() {
        return fs.a(this.L, 1);
    }

    public ab l(String str) {
        this.q = str;
        return this;
    }

    public void l(boolean z2) {
        if (!z2) {
            this.l = null;
        }
    }

    public boolean l() {
        return this.l != null;
    }

    public void m(boolean z2) {
        if (!z2) {
            this.m = null;
        }
    }

    public boolean m() {
        return this.m != null;
    }

    public void n(boolean z2) {
        this.L = fs.a(this.L, 2, z2);
    }

    public boolean n() {
        return fs.a(this.L, 2);
    }

    public void o(boolean z2) {
        if (!z2) {
            this.o = null;
        }
    }

    public boolean o() {
        return this.o != null;
    }

    public void p(boolean z2) {
        if (!z2) {
            this.p = null;
        }
    }

    public boolean p() {
        return this.p != null;
    }

    public void q(boolean z2) {
        if (!z2) {
            this.q = null;
        }
    }

    public boolean q() {
        return this.q != null;
    }

    public void r() {
        if (this.i != null) {
            this.i.c();
        }
    }

    public String toString() {
        boolean z2 = false;
        StringBuilder sb = new StringBuilder("DeviceInfo(");
        boolean z3 = true;
        if (a()) {
            sb.append("device_id:");
            if (this.f47a == null) {
                sb.append("null");
            } else {
                sb.append(this.f47a);
            }
            z3 = false;
        }
        if (b()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("idmd5:");
            if (this.f48b == null) {
                sb.append("null");
            } else {
                sb.append(this.f48b);
            }
            z3 = false;
        }
        if (c()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("mac_address:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
            z3 = false;
        }
        if (d()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("open_udid:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
            z3 = false;
        }
        if (e()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("model:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
            z3 = false;
        }
        if (f()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("cpu:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
            z3 = false;
        }
        if (g()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("os:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
            z3 = false;
        }
        if (h()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("os_version:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
            z3 = false;
        }
        if (i()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("resolution:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
            z3 = false;
        }
        if (j()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("is_jailbroken:");
            sb.append(this.j);
            z3 = false;
        }
        if (k()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("is_pirated:");
            sb.append(this.k);
            z3 = false;
        }
        if (l()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("device_board:");
            if (this.l == null) {
                sb.append("null");
            } else {
                sb.append(this.l);
            }
            z3 = false;
        }
        if (m()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("device_brand:");
            if (this.m == null) {
                sb.append("null");
            } else {
                sb.append(this.m);
            }
            z3 = false;
        }
        if (n()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("device_manutime:");
            sb.append(this.n);
            z3 = false;
        }
        if (o()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("device_manufacturer:");
            if (this.o == null) {
                sb.append("null");
            } else {
                sb.append(this.o);
            }
            z3 = false;
        }
        if (p()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("device_manuid:");
            if (this.p == null) {
                sb.append("null");
            } else {
                sb.append(this.p);
            }
        } else {
            z2 = z3;
        }
        if (q()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("device_name:");
            if (this.q == null) {
                sb.append("null");
            } else {
                sb.append(this.q);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
