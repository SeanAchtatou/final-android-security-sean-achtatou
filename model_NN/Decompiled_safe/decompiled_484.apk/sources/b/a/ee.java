package b.a;

import java.util.ArrayList;

class ee extends hg<ec> {
    private ee() {
    }

    /* renamed from: a */
    public void b(gw gwVar, ec ecVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                if (!ecVar.a()) {
                    throw new gx("Required field 'start_time' was not found in serialized data! Struct: " + toString());
                } else if (!ecVar.b()) {
                    throw new gx("Required field 'end_time' was not found in serialized data! Struct: " + toString());
                } else if (!ecVar.c()) {
                    throw new gx("Required field 'duration' was not found in serialized data! Struct: " + toString());
                } else {
                    ecVar.h();
                    return;
                }
            } else {
                switch (h.c) {
                    case 1:
                        if (h.f172b != 11) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            ecVar.f114a = gwVar.v();
                            ecVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.f172b != 10) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            ecVar.f115b = gwVar.t();
                            ecVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.f172b != 10) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            ecVar.c = gwVar.t();
                            ecVar.c(true);
                            break;
                        }
                    case 4:
                        if (h.f172b != 10) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            ecVar.d = gwVar.t();
                            ecVar.d(true);
                            break;
                        }
                    case 5:
                        if (h.f172b != 15) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            gu l = gwVar.l();
                            ecVar.e = new ArrayList(l.f174b);
                            for (int i = 0; i < l.f174b; i++) {
                                dc dcVar = new dc();
                                dcVar.a(gwVar);
                                ecVar.e.add(dcVar);
                            }
                            gwVar.m();
                            ecVar.e(true);
                            break;
                        }
                    case 6:
                        if (h.f172b != 15) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            gu l2 = gwVar.l();
                            ecVar.f = new ArrayList(l2.f174b);
                            for (int i2 = 0; i2 < l2.f174b; i2++) {
                                co coVar = new co();
                                coVar.a(gwVar);
                                ecVar.f.add(coVar);
                            }
                            gwVar.m();
                            ecVar.f(true);
                            break;
                        }
                    case 7:
                        if (h.f172b != 12) {
                            ha.a(gwVar, h.f172b);
                            break;
                        } else {
                            ecVar.g = new ej();
                            ecVar.g.a(gwVar);
                            ecVar.g(true);
                            break;
                        }
                    default:
                        ha.a(gwVar, h.f172b);
                        break;
                }
                gwVar.i();
            }
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, ec ecVar) {
        ecVar.h();
        gwVar.a(ec.i);
        if (ecVar.f114a != null) {
            gwVar.a(ec.j);
            gwVar.a(ecVar.f114a);
            gwVar.b();
        }
        gwVar.a(ec.k);
        gwVar.a(ecVar.f115b);
        gwVar.b();
        gwVar.a(ec.l);
        gwVar.a(ecVar.c);
        gwVar.b();
        gwVar.a(ec.m);
        gwVar.a(ecVar.d);
        gwVar.b();
        if (ecVar.e != null && ecVar.e()) {
            gwVar.a(ec.n);
            gwVar.a(new gu((byte) 12, ecVar.e.size()));
            for (dc b2 : ecVar.e) {
                b2.b(gwVar);
            }
            gwVar.e();
            gwVar.b();
        }
        if (ecVar.f != null && ecVar.f()) {
            gwVar.a(ec.o);
            gwVar.a(new gu((byte) 12, ecVar.f.size()));
            for (co b3 : ecVar.f) {
                b3.b(gwVar);
            }
            gwVar.e();
            gwVar.b();
        }
        if (ecVar.g != null && ecVar.g()) {
            gwVar.a(ec.p);
            ecVar.g.b(gwVar);
            gwVar.b();
        }
        gwVar.c();
        gwVar.a();
    }
}
