package b.a;

import android.support.v4.media.TransportMediator;
import com.qihoo.dynamic.util.Md5Util;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

public class gr extends gw {
    private static final hc d = new hc("");
    private static final gt f = new gt("", (byte) 0, 0);
    private static final byte[] g = new byte[16];

    /* renamed from: a  reason: collision with root package name */
    byte[] f168a = new byte[5];

    /* renamed from: b  reason: collision with root package name */
    byte[] f169b = new byte[10];
    byte[] c = new byte[1];
    private ft h = new ft(15);
    private short i = 0;
    private gt j = null;
    private Boolean k = null;
    private final long l;
    private byte[] m = new byte[1];

    static {
        g[0] = 0;
        g[2] = 1;
        g[3] = 3;
        g[6] = 4;
        g[8] = 5;
        g[10] = 6;
        g[4] = 7;
        g[11] = 8;
        g[15] = 9;
        g[14] = 10;
        g[13] = 11;
        g[12] = 12;
    }

    public gr(hk hkVar, long j2) {
        super(hkVar);
        this.l = j2;
    }

    private long A() {
        int i2 = 0;
        long j2 = 0;
        if (this.e.d() >= 10) {
            byte[] b2 = this.e.b();
            int c2 = this.e.c();
            int i3 = 0;
            while (true) {
                byte b3 = b2[c2 + i2];
                j2 |= ((long) (b3 & Byte.MAX_VALUE)) << i3;
                if ((b3 & 128) != 128) {
                    break;
                }
                i3 += 7;
                i2++;
            }
            this.e.a(i2 + 1);
        } else {
            while (true) {
                byte q = q();
                j2 |= ((long) (q & Byte.MAX_VALUE)) << i2;
                if ((q & 128) != 128) {
                    break;
                }
                i2 += 7;
            }
        }
        return j2;
    }

    private long a(byte[] bArr) {
        return ((((long) bArr[7]) & 255) << 56) | ((((long) bArr[6]) & 255) << 48) | ((((long) bArr[5]) & 255) << 40) | ((((long) bArr[4]) & 255) << 32) | ((((long) bArr[3]) & 255) << 24) | ((((long) bArr[2]) & 255) << 16) | ((((long) bArr[1]) & 255) << 8) | (((long) bArr[0]) & 255);
    }

    private void a(long j2, byte[] bArr, int i2) {
        bArr[i2 + 0] = (byte) ((int) (j2 & 255));
        bArr[i2 + 1] = (byte) ((int) ((j2 >> 8) & 255));
        bArr[i2 + 2] = (byte) ((int) ((j2 >> 16) & 255));
        bArr[i2 + 3] = (byte) ((int) ((j2 >> 24) & 255));
        bArr[i2 + 4] = (byte) ((int) ((j2 >> 32) & 255));
        bArr[i2 + 5] = (byte) ((int) ((j2 >> 40) & 255));
        bArr[i2 + 6] = (byte) ((int) ((j2 >> 48) & 255));
        bArr[i2 + 7] = (byte) ((int) ((j2 >> 56) & 255));
    }

    private void a(gt gtVar, byte b2) {
        if (b2 == -1) {
            b2 = e(gtVar.f172b);
        }
        if (gtVar.c <= this.i || gtVar.c - this.i > 15) {
            b(b2);
            a(gtVar.c);
        } else {
            d((int) (((gtVar.c - this.i) << 4) | b2));
        }
        this.i = gtVar.c;
    }

    private void a(byte[] bArr, int i2, int i3) {
        b(i3);
        this.e.b(bArr, i2, i3);
    }

    private void b(byte b2) {
        this.m[0] = b2;
        this.e.b(this.m);
    }

    private void b(int i2) {
        int i3 = 0;
        while ((i2 & -128) != 0) {
            this.f168a[i3] = (byte) ((i2 & TransportMediator.KEYCODE_MEDIA_PAUSE) | 128);
            i2 >>>= 7;
            i3++;
        }
        this.f168a[i3] = (byte) i2;
        this.e.b(this.f168a, 0, i3 + 1);
    }

    private void b(long j2) {
        int i2 = 0;
        while ((-128 & j2) != 0) {
            this.f169b[i2] = (byte) ((int) ((127 & j2) | 128));
            j2 >>>= 7;
            i2++;
        }
        this.f169b[i2] = (byte) ((int) j2);
        this.e.b(this.f169b, 0, i2 + 1);
    }

    private int c(int i2) {
        return (i2 << 1) ^ (i2 >> 31);
    }

    private long c(long j2) {
        return (j2 << 1) ^ (j2 >> 63);
    }

    private boolean c(byte b2) {
        byte b3 = b2 & 15;
        return b3 == 1 || b3 == 2;
    }

    private byte d(byte b2) {
        switch ((byte) (b2 & 15)) {
            case 0:
                return 0;
            case 1:
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 6;
            case 5:
                return 8;
            case 6:
                return 10;
            case 7:
                return 4;
            case 8:
                return 11;
            case 9:
                return 15;
            case 10:
                return 14;
            case 11:
                return 13;
            case 12:
                return 12;
            default:
                throw new gx("don't know what type: " + ((int) ((byte) (b2 & 15))));
        }
    }

    private long d(long j2) {
        return (j2 >>> 1) ^ (-(1 & j2));
    }

    private void d(int i2) {
        b((byte) i2);
    }

    private byte e(byte b2) {
        return g[b2];
    }

    private byte[] e(int i2) {
        if (i2 == 0) {
            return new byte[0];
        }
        byte[] bArr = new byte[i2];
        this.e.d(bArr, 0, i2);
        return bArr;
    }

    private void f(int i2) {
        if (i2 < 0) {
            throw new gx("Negative length: " + i2);
        } else if (this.l != -1 && ((long) i2) > this.l) {
            throw new gx("Length exceeded max allowed: " + i2);
        }
    }

    private int g(int i2) {
        return (i2 >>> 1) ^ (-(i2 & 1));
    }

    private int z() {
        int i2 = 0;
        if (this.e.d() >= 5) {
            byte[] b2 = this.e.b();
            int c2 = this.e.c();
            int i3 = 0;
            int i4 = 0;
            while (true) {
                byte b3 = b2[c2 + i2];
                i4 |= (b3 & Byte.MAX_VALUE) << i3;
                if ((b3 & 128) != 128) {
                    this.e.a(i2 + 1);
                    return i4;
                }
                i3 += 7;
                i2++;
            }
        } else {
            int i5 = 0;
            while (true) {
                byte q = q();
                i5 |= (q & Byte.MAX_VALUE) << i2;
                if ((q & 128) != 128) {
                    return i5;
                }
                i2 += 7;
            }
        }
    }

    public void a() {
        this.i = this.h.a();
    }

    public void a(byte b2) {
        b(b2);
    }

    /* access modifiers changed from: protected */
    public void a(byte b2, int i2) {
        if (i2 <= 14) {
            d((int) ((i2 << 4) | e(b2)));
            return;
        }
        d((int) (e(b2) | 240));
        b(i2);
    }

    public void a(double d2) {
        byte[] bArr = {0, 0, 0, 0, 0, 0, 0, 0};
        a(Double.doubleToLongBits(d2), bArr, 0);
        this.e.b(bArr);
    }

    public void a(int i2) {
        b(c(i2));
    }

    public void a(long j2) {
        b(c(j2));
    }

    public void a(gt gtVar) {
        if (gtVar.f172b == 2) {
            this.j = gtVar;
        } else {
            a(gtVar, (byte) -1);
        }
    }

    public void a(gu guVar) {
        a(guVar.f173a, guVar.f174b);
    }

    public void a(gv gvVar) {
        if (gvVar.c == 0) {
            d(0);
            return;
        }
        b(gvVar.c);
        d((int) ((e(gvVar.f175a) << 4) | e(gvVar.f176b)));
    }

    public void a(hc hcVar) {
        this.h.a(this.i);
        this.i = 0;
    }

    public void a(String str) {
        try {
            byte[] bytes = str.getBytes(Md5Util.DEFAULT_CHARSET);
            a(bytes, 0, bytes.length);
        } catch (UnsupportedEncodingException e) {
            throw new ga("UTF-8 not supported!");
        }
    }

    public void a(ByteBuffer byteBuffer) {
        a(byteBuffer.array(), byteBuffer.position() + byteBuffer.arrayOffset(), byteBuffer.limit() - byteBuffer.position());
    }

    public void a(short s) {
        b(c((int) s));
    }

    public void a(boolean z) {
        byte b2 = 1;
        if (this.j != null) {
            gt gtVar = this.j;
            if (!z) {
                b2 = 2;
            }
            a(gtVar, b2);
            this.j = null;
            return;
        }
        if (!z) {
            b2 = 2;
        }
        b(b2);
    }

    public void b() {
    }

    public void c() {
        b((byte) 0);
    }

    public void d() {
    }

    public void e() {
    }

    public hc f() {
        this.h.a(this.i);
        this.i = 0;
        return d;
    }

    public void g() {
        this.i = this.h.a();
    }

    public gt h() {
        byte q = q();
        if (q == 0) {
            return f;
        }
        short s = (short) ((q & 240) >> 4);
        gt gtVar = new gt("", d((byte) (q & 15)), s == 0 ? r() : (short) (s + this.i));
        if (c(q)) {
            this.k = ((byte) (q & 15)) == 1 ? Boolean.TRUE : Boolean.FALSE;
        }
        this.i = gtVar.c;
        return gtVar;
    }

    public void i() {
    }

    public gv j() {
        int z = z();
        byte q = z == 0 ? 0 : q();
        return new gv(d((byte) (q >> 4)), d((byte) (q & 15)), z);
    }

    public void k() {
    }

    public gu l() {
        byte q = q();
        int i2 = (q >> 4) & 15;
        if (i2 == 15) {
            i2 = z();
        }
        return new gu(d(q), i2);
    }

    public void m() {
    }

    public hb n() {
        return new hb(l());
    }

    public void o() {
    }

    public boolean p() {
        if (this.k == null) {
            return q() == 1;
        }
        boolean booleanValue = this.k.booleanValue();
        this.k = null;
        return booleanValue;
    }

    public byte q() {
        if (this.e.d() > 0) {
            byte b2 = this.e.b()[this.e.c()];
            this.e.a(1);
            return b2;
        }
        this.e.d(this.c, 0, 1);
        return this.c[0];
    }

    public short r() {
        return (short) g(z());
    }

    public int s() {
        return g(z());
    }

    public long t() {
        return d(A());
    }

    public double u() {
        byte[] bArr = new byte[8];
        this.e.d(bArr, 0, 8);
        return Double.longBitsToDouble(a(bArr));
    }

    public String v() {
        int z = z();
        f(z);
        if (z == 0) {
            return "";
        }
        try {
            if (this.e.d() < z) {
                return new String(e(z), Md5Util.DEFAULT_CHARSET);
            }
            String str = new String(this.e.b(), this.e.c(), z, Md5Util.DEFAULT_CHARSET);
            this.e.a(z);
            return str;
        } catch (UnsupportedEncodingException e) {
            throw new ga("UTF-8 not supported!");
        }
    }

    public ByteBuffer w() {
        int z = z();
        f(z);
        if (z == 0) {
            return ByteBuffer.wrap(new byte[0]);
        }
        byte[] bArr = new byte[z];
        this.e.d(bArr, 0, z);
        return ByteBuffer.wrap(bArr);
    }

    public void x() {
        this.h.b();
        this.i = 0;
    }
}
