package b.a;

class ba extends hg<ay> {
    private ba() {
    }

    /* renamed from: a */
    public void b(gw gwVar, ay ayVar) {
        gwVar.f();
        while (true) {
            gt h = gwVar.h();
            if (h.f172b == 0) {
                gwVar.g();
                if (!ayVar.b()) {
                    throw new gx("Required field 'ts' was not found in serialized data! Struct: " + toString());
                }
                ayVar.c();
                return;
            }
            switch (h.c) {
                case 1:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        ayVar.f63a = gwVar.v();
                        ayVar.a(true);
                        break;
                    }
                case 2:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        ayVar.f64b = gwVar.v();
                        ayVar.b(true);
                        break;
                    }
                case 3:
                    if (h.f172b != 11) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        ayVar.c = gwVar.v();
                        ayVar.c(true);
                        break;
                    }
                case 4:
                    if (h.f172b != 10) {
                        ha.a(gwVar, h.f172b);
                        break;
                    } else {
                        ayVar.d = gwVar.t();
                        ayVar.d(true);
                        break;
                    }
                default:
                    ha.a(gwVar, h.f172b);
                    break;
            }
            gwVar.i();
        }
    }

    /* renamed from: b */
    public void a(gw gwVar, ay ayVar) {
        ayVar.c();
        gwVar.a(ay.f);
        if (ayVar.f63a != null) {
            gwVar.a(ay.g);
            gwVar.a(ayVar.f63a);
            gwVar.b();
        }
        if (ayVar.f64b != null && ayVar.a()) {
            gwVar.a(ay.h);
            gwVar.a(ayVar.f64b);
            gwVar.b();
        }
        if (ayVar.c != null) {
            gwVar.a(ay.i);
            gwVar.a(ayVar.c);
            gwVar.b();
        }
        gwVar.a(ay.j);
        gwVar.a(ayVar.d);
        gwVar.b();
        gwVar.c();
        gwVar.a();
    }
}
