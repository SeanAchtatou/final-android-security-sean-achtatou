package defpackage;

import android.content.Context;
import com.tencent.connect.common.Constants;
import com.tencent.securemodule.jni.SecureEngine;
import defpackage.ae;
import defpackage.ag;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: af  reason: default package */
public final class af {

    /* renamed from: a  reason: collision with root package name */
    public static ag.a[] f8a = {new ag.a(0, "info", "getGuid"), new ag.a(1, "info", "getUpdatesV2"), new ag.a(2, "info", "reportTipsRes"), new ag.a(3, "cloudcheck", "tinyCloudCheck"), new ag.a(4, "cloudcheck", "cloudCheck"), new ag.a(5, "report", "reportSoftUsageInfo")};
    private static String b = null;
    private String c = null;
    private String d = null;
    private Context e;
    private l f;
    private u g;
    private h h;
    private p i;

    public af(Context context) {
        this.e = context;
        b = as.a(this.e, 20001, (String) null);
        this.c = ay.a(at.a(this.e));
        this.d = ay.a(at.c(this.e));
    }

    public static String a(int i2) {
        return f8a[i2].b;
    }

    public static String b(int i2) {
        return f8a[i2].c;
    }

    private h e() {
        if (this.h == null) {
            this.h = new h();
            this.h.a(this.c);
            this.h.b(ay.a(at.b(this.e)));
            this.h.c(this.d);
            this.h.d(ay.a(at.d(this.e)));
            this.h.e(ay.a(at.d()));
            this.h.a(at.c());
            this.h.f(ay.a(at.a()));
            this.h.g(ay.a(at.b()));
            this.h.h(ay.a(au.a(this.e)));
            this.h.i(b);
        } else {
            this.h.i(b);
            this.h.a(this.c);
            this.h.c(this.d);
        }
        return this.h;
    }

    private int f() {
        j jVar;
        h e2 = e();
        AtomicReference atomicReference = new AtomicReference();
        int a2 = ag.a(this.e).a(e2, atomicReference);
        if (a2 == 0 && (jVar = (j) atomicReference.get()) != null) {
            b = jVar.a();
            if (b == null || b.equals(Constants.STR_EMPTY)) {
                return -2001;
            }
        }
        return a2;
    }

    private boolean g() {
        String a2 = ay.a(at.a(this.e));
        String a3 = ay.a(at.c(this.e));
        this.c = as.a(this.e, 20002, a2);
        this.d = as.a(this.e, 20003, a3);
        if (a2.equals(this.c) && a3.equals(this.d)) {
            return false;
        }
        this.c = a2;
        this.d = a3;
        return true;
    }

    public l a() {
        if (this.f == null) {
            this.f = new l();
            this.f.a(2);
            this.f.b(as.a(this.e, 30005, (int) com.tencent.securemodule.service.Constants.PLATFROM_ANDROID_PHONE));
        }
        return this.f;
    }

    public p b() {
        int i2 = 1;
        if (this.i == null) {
            this.i = new p();
            this.i.n(b);
            this.i.a("0000000000000000");
            this.i.b(i.a(1).toString());
            this.i.c(as.a(this.e, 30001, "0.0.0"));
            this.i.d(this.c);
            this.i.e(ay.a(at.b(this.e)));
            this.i.a(2);
            this.i.h(Constants.STR_EMPTY + at.c());
            this.i.b((int) com.tencent.securemodule.service.Constants.PLATFROM_ANDROID_PHONE);
            this.i.i(ay.a(at.a()));
            this.i.l(as.a(this.e, 30002, Constants.STR_EMPTY));
            p pVar = this.i;
            if (!ar.a(this.e)) {
                i2 = 0;
            }
            pVar.c(i2);
        } else {
            this.i.d(this.c);
        }
        return this.i;
    }

    public u c() {
        int i2 = 2;
        int i3 = 0;
        int i4 = 1;
        if (this.g == null) {
            this.g = new u();
            this.g.a(this.c);
            this.g.e("0000000000000000");
            this.g.f(as.a(this.e, 30002, Constants.STR_EMPTY));
            this.g.g(ay.a(at.a()));
            this.g.b(as.a(this.e, 30003, 6));
            String[] split = as.a(this.e, 30001, "0.0.0").trim().split("[\\.]");
            if (split.length >= 3) {
                this.g.a(new n(Integer.parseInt(split[0]), Integer.parseInt(split[1]), Integer.parseInt(split[2])));
            }
            this.g.h(b);
            this.g.i(ay.a(at.b(this.e)));
            u uVar = this.g;
            if (ae.c(this.e) != ae.a.CONN_WIFI) {
                i2 = 1;
            }
            uVar.a(i2);
            this.g.c(ar.a(this.e) ? 1 : 0);
            u uVar2 = this.g;
            if (!SecureEngine.checkSecureStatus(this.e)) {
                i4 = 0;
            }
            uVar2.d(i4);
            this.g.e(at.c());
            this.g.f(as.a(this.e, 30004, 0));
            this.g.b(as.a(this.e, 30006, Constants.STR_EMPTY));
        } else {
            this.g.h(b);
            this.g.a(this.c);
            u uVar3 = this.g;
            if (SecureEngine.checkSecureStatus(this.e)) {
                i3 = 1;
            }
            uVar3.d(i3);
            u uVar4 = this.g;
            if (ae.c(this.e) != ae.a.CONN_WIFI) {
                i2 = 1;
            }
            uVar4.a(i2);
            this.g.b(as.a(this.e, 30006, Constants.STR_EMPTY));
        }
        return this.g;
    }

    public int d() {
        if (b == null || b.equals(Constants.STR_EMPTY) || g()) {
            int f2 = f();
            if (f2 != 0) {
                return f2;
            }
            as.b(this.e, 20002, this.c);
            as.b(this.e, 20003, this.d);
            as.b(this.e, 20001, b);
        }
        return 0;
    }
}
