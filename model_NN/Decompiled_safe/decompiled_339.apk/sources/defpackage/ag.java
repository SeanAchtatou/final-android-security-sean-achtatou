package defpackage;

import android.content.Context;
import com.qq.jce.wup.UniPacket;
import com.tencent.securemodule.impl.ErrorCode;
import com.tencent.securemodule.jni.TccCryptor;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: ag  reason: default package */
public class ag {

    /* renamed from: a  reason: collision with root package name */
    private static ag f9a;
    private Context b;
    private af c;

    /* renamed from: ag$a */
    static class a {

        /* renamed from: a  reason: collision with root package name */
        int f10a;
        String b;
        String c;

        public a(int i, String str, String str2) {
            this.f10a = i;
            this.b = str;
            this.c = str2;
        }
    }

    private ag(Context context) {
        this.b = context;
        this.c = new af(context);
    }

    private int a(int i, UniPacket uniPacket, UniPacket uniPacket2) {
        uniPacket.setRequestId(i);
        uniPacket.setServantName(af.a(i));
        uniPacket.setFuncName(af.b(i));
        uniPacket.setEncodeName("UTF-8");
        uniPacket2.setEncodeName("UTF-8");
        return this.c.d();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ae.a(boolean, java.util.concurrent.atomic.AtomicReference<byte[]>):int
     arg types: [int, java.util.concurrent.atomic.AtomicReference]
     candidates:
      ae.a(android.content.Context, java.lang.String):int
      ae.a(boolean, java.util.concurrent.atomic.AtomicReference<byte[]>):int */
    private int a(UniPacket uniPacket, UniPacket uniPacket2, boolean z) {
        try {
            byte[] encrypt = TccCryptor.encrypt(this.b, uniPacket.encode(), (byte[]) null);
            ae aeVar = new ae(this.b);
            int a2 = aeVar.a(this.b, "http://pmir.3g.qq.com", encrypt);
            if (a2 != 0) {
                return a2;
            }
            AtomicReference atomicReference = new AtomicReference();
            int a3 = aeVar.a(false, (AtomicReference<byte[]>) atomicReference);
            if (a3 != 0) {
                return a3;
            }
            byte[] bArr = (byte[]) atomicReference.get();
            if (bArr != null && bArr.length > 0) {
                uniPacket2.decode(TccCryptor.decrypt(this.b, bArr, null));
            }
            return 0;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return -6057;
        } catch (Exception e2) {
            e2.printStackTrace();
            return ErrorCode.ERR_WUP;
        }
    }

    public static ag a(Context context) {
        if (f9a == null) {
            f9a = new ag(context);
        }
        return f9a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ag.a(com.qq.jce.wup.UniPacket, com.qq.jce.wup.UniPacket, boolean):int
     arg types: [com.qq.jce.wup.UniPacket, com.qq.jce.wup.UniPacket, int]
     candidates:
      ag.a(int, com.qq.jce.wup.UniPacket, com.qq.jce.wup.UniPacket):int
      ag.a(com.qq.jce.wup.UniPacket, com.qq.jce.wup.UniPacket, boolean):int */
    public int a(h hVar, AtomicReference<j> atomicReference) {
        UniPacket uniPacket = new UniPacket(true);
        UniPacket uniPacket2 = new UniPacket(true);
        uniPacket.setRequestId(0);
        uniPacket.setServantName(af.a(0));
        uniPacket.setFuncName(af.b(0));
        uniPacket.setEncodeName("UTF-8");
        uniPacket2.setEncodeName("UTF-8");
        uniPacket.put("phonetype", this.c.a());
        uniPacket.put("userinfo", this.c.c());
        uniPacket.put("deviceinfo", hVar);
        int a2 = a(uniPacket, uniPacket2, false);
        if (a2 != 0) {
            return a2;
        }
        j jVar = (j) uniPacket2.getByClass("guidinfo", new j());
        if (jVar != null) {
            atomicReference.set(jVar);
        }
        return 0;
    }

    public int a(String str) {
        UniPacket uniPacket = new UniPacket(true);
        int a2 = a(5, uniPacket, new UniPacket(true));
        if (a2 != 0) {
            return a2;
        }
        ArrayList arrayList = new ArrayList();
        o oVar = new o();
        oVar.f3938a = 28868;
        oVar.c = "1";
        oVar.b = (int) (System.currentTimeMillis() / 1000);
        oVar.e = str;
        arrayList.add(oVar);
        uniPacket.put("suikey", this.c.b());
        uniPacket.put("vecsui", arrayList);
        return a(arrayList);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ag.a(com.qq.jce.wup.UniPacket, com.qq.jce.wup.UniPacket, boolean):int
     arg types: [com.qq.jce.wup.UniPacket, com.qq.jce.wup.UniPacket, int]
     candidates:
      ag.a(int, com.qq.jce.wup.UniPacket, com.qq.jce.wup.UniPacket):int
      ag.a(com.qq.jce.wup.UniPacket, com.qq.jce.wup.UniPacket, boolean):int */
    public int a(ArrayList<o> arrayList) {
        UniPacket uniPacket = new UniPacket(true);
        UniPacket uniPacket2 = new UniPacket(true);
        int a2 = a(5, uniPacket, uniPacket2);
        if (a2 != 0) {
            return a2;
        }
        uniPacket.put("suikey", this.c.b());
        uniPacket.put("vecsui", arrayList);
        return a(uniPacket, uniPacket2, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ag.a(com.qq.jce.wup.UniPacket, com.qq.jce.wup.UniPacket, boolean):int
     arg types: [com.qq.jce.wup.UniPacket, com.qq.jce.wup.UniPacket, int]
     candidates:
      ag.a(int, com.qq.jce.wup.UniPacket, com.qq.jce.wup.UniPacket):int
      ag.a(com.qq.jce.wup.UniPacket, com.qq.jce.wup.UniPacket, boolean):int */
    public int a(ArrayList<d> arrayList, ArrayList<f> arrayList2) {
        UniPacket uniPacket = new UniPacket(true);
        UniPacket uniPacket2 = new UniPacket(true);
        int a2 = a(3, uniPacket, uniPacket2);
        if (a2 != 0) {
            return a2;
        }
        uniPacket.put("phonetype", this.c.a());
        uniPacket.put("userinfo", this.c.c());
        uniPacket.put("vecCloudFeature", arrayList);
        int a3 = a(uniPacket, uniPacket2, false);
        if (a3 != 0) {
            return a3;
        }
        ArrayList arrayList3 = new ArrayList();
        arrayList3.add(new f());
        ArrayList arrayList4 = (ArrayList) uniPacket2.getByClass("vecCloudResult", arrayList3);
        if (arrayList4 != null) {
            arrayList2.clear();
            arrayList2.addAll(arrayList4);
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ag.a(com.qq.jce.wup.UniPacket, com.qq.jce.wup.UniPacket, boolean):int
     arg types: [com.qq.jce.wup.UniPacket, com.qq.jce.wup.UniPacket, int]
     candidates:
      ag.a(int, com.qq.jce.wup.UniPacket, com.qq.jce.wup.UniPacket):int
      ag.a(com.qq.jce.wup.UniPacket, com.qq.jce.wup.UniPacket, boolean):int */
    public int a(List<b> list, AtomicReference<q> atomicReference) {
        UniPacket uniPacket = new UniPacket(true);
        UniPacket uniPacket2 = new UniPacket(true);
        int a2 = a(1, uniPacket, uniPacket2);
        if (a2 != 0) {
            return a2;
        }
        uniPacket.put("phonetype", this.c.a());
        uniPacket.put("userinfo", this.c.c());
        uniPacket.put("vecclient", list);
        int a3 = a(uniPacket, uniPacket2, false);
        if (a3 != 0) {
            return a3;
        }
        q qVar = (q) uniPacket2.getByClass("cmdinfo", new q());
        if (qVar != null) {
            atomicReference.set(qVar);
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ag.a(com.qq.jce.wup.UniPacket, com.qq.jce.wup.UniPacket, boolean):int
     arg types: [com.qq.jce.wup.UniPacket, com.qq.jce.wup.UniPacket, int]
     candidates:
      ag.a(int, com.qq.jce.wup.UniPacket, com.qq.jce.wup.UniPacket):int
      ag.a(com.qq.jce.wup.UniPacket, com.qq.jce.wup.UniPacket, boolean):int */
    public int b(ArrayList<g> arrayList, ArrayList<f> arrayList2) {
        UniPacket uniPacket = new UniPacket(true);
        UniPacket uniPacket2 = new UniPacket(true);
        int a2 = a(4, uniPacket, uniPacket2);
        if (a2 != 0) {
            return a2;
        }
        uniPacket.put("phonetype", this.c.a());
        uniPacket.put("userinfo", this.c.c());
        uniPacket.put("vecDetailCloudFeature", arrayList);
        int a3 = a(uniPacket, uniPacket2, false);
        if (a3 != 0) {
            return a3;
        }
        ArrayList arrayList3 = new ArrayList();
        arrayList3.add(new f());
        ArrayList arrayList4 = (ArrayList) uniPacket2.getByClass("vecCloudResult", arrayList3);
        if (arrayList4 != null) {
            arrayList2.clear();
            arrayList2.addAll(arrayList4);
        }
        return 0;
    }
}
