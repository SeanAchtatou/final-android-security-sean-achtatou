package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.view.a.g;
import android.view.View;

/* compiled from: ProGuard */
class bc extends bb {
    bc() {
    }

    public boolean b(View view) {
        return ViewCompatJB.hasTransientState(view);
    }

    public void a(View view, boolean z) {
        ViewCompatJB.setHasTransientState(view, z);
    }

    public void c(View view) {
        ViewCompatJB.postInvalidateOnAnimation(view);
    }

    public void a(View view, int i, int i2, int i3, int i4) {
        ViewCompatJB.postInvalidateOnAnimation(view, i, i2, i3, i4);
    }

    public void a(View view, Runnable runnable) {
        ViewCompatJB.postOnAnimation(view, runnable);
    }

    public void a(View view, Runnable runnable, long j) {
        ViewCompatJB.postOnAnimationDelayed(view, runnable, j);
    }

    public int d(View view) {
        return ViewCompatJB.getImportantForAccessibility(view);
    }

    public void d(View view, int i) {
        ViewCompatJB.setImportantForAccessibility(view, i);
    }

    public boolean a(View view, int i, Bundle bundle) {
        return ViewCompatJB.performAccessibilityAction(view, i, bundle);
    }

    public g e(View view) {
        Object accessibilityNodeProvider = ViewCompatJB.getAccessibilityNodeProvider(view);
        if (accessibilityNodeProvider != null) {
            return new g(accessibilityNodeProvider);
        }
        return null;
    }
}
