package com.tencent.beacon.event;

import android.content.Context;
import android.net.TrafficStats;
import android.os.Process;
import com.tencent.assistant.component.txscrollview.TXTabBarLayout;
import com.tencent.beacon.a.a.d;
import com.tencent.beacon.a.b.b;
import com.tencent.beacon.a.b.c;
import com.tencent.beacon.a.b.h;
import com.tencent.beacon.a.b.i;
import com.tencent.beacon.a.b.j;
import com.tencent.beacon.a.f;
import com.tencent.beacon.a.g;
import com.tencent.beacon.f.a;
import com.tencent.beacon.f.k;
import com.tencent.connect.common.Constants;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/* compiled from: ProGuard */
public final class t implements b, i, j {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f3443a;
    private static t c = null;
    private static int i = 100;
    public final Context b;
    private g d = null;
    private l e = null;
    private l f = null;
    private boolean g = true;
    private boolean h = false;
    private com.tencent.beacon.f.j j;
    private boolean k;
    private int l = 0;

    static /* synthetic */ void a(t tVar) {
        d c2 = com.tencent.beacon.a.i.c(tVar.b);
        if (c2 != null) {
            HashMap hashMap = new HashMap();
            hashMap.put("A70", new StringBuilder().append(c2.m - c2.i).toString());
            hashMap.put("A71", new StringBuilder().append(c2.l - c2.h).toString());
            hashMap.put("A72", new StringBuilder().append(c2.d - c2.f).toString());
            hashMap.put("A73", new StringBuilder().append(c2.e - c2.g).toString());
            if (a("rqd_app_net_consumed", true, 0, 0, hashMap, true)) {
                com.tencent.beacon.a.i.a(tVar.b, c2);
            }
        }
    }

    public static synchronized t a(Context context, boolean z, com.tencent.beacon.f.j jVar, a aVar, g gVar) {
        t tVar;
        synchronized (t.class) {
            if (c == null) {
                boolean a2 = a(context);
                f3443a = a2;
                if (!a2) {
                    com.tencent.beacon.d.a.e(" ua create instance ", new Object[0]);
                    t tVar2 = new t(context, true, jVar, aVar, null);
                    c = tVar2;
                    tVar2.a(true);
                }
            }
            tVar = c;
        }
        return tVar;
    }

    private static boolean a(Context context) {
        boolean z;
        try {
            int parseInt = Integer.parseInt(com.tencent.beacon.a.a.b(context, "key_initsdktimes", "0"));
            if (!com.tencent.beacon.b.a.g().equals(com.tencent.beacon.a.a.b(context, "key_initsdkdate", Constants.STR_EMPTY))) {
                com.tencent.beacon.a.a.b(context, com.tencent.beacon.b.a.g());
                parseInt = 0;
            }
            if (parseInt <= i) {
                com.tencent.beacon.a.a.a(context, new StringBuilder().append(parseInt + 1).toString());
                return false;
            }
            z = true;
            try {
                com.tencent.beacon.d.a.e(" sdk init max times", new Object[0]);
                return true;
            } catch (Exception e2) {
                com.tencent.beacon.d.a.c(" set init times failed! ", new Object[0]);
                return z;
            }
        } catch (Exception e3) {
            z = false;
            com.tencent.beacon.d.a.c(" set init times failed! ", new Object[0]);
            return z;
        }
    }

    public static synchronized t d() {
        t tVar;
        synchronized (t.class) {
            tVar = c;
        }
        return tVar;
    }

    public static synchronized com.tencent.beacon.f.j a(Context context, boolean z) {
        k a2;
        synchronized (t.class) {
            a2 = k.a(context, z);
        }
        return a2;
    }

    public final void a(boolean z) {
        h b2;
        c a2 = c.a(this.b);
        if (a2 != null && (b2 = a2.b().b(1)) != null && b2.a() != z) {
            b2.a(z);
            if (z != e()) {
                e(z);
            }
        }
    }

    public static boolean a(String str, boolean z, long j2, long j3, Map<String, String> map, boolean z2) {
        com.tencent.beacon.d.a.e(" onUA: %s,%b,%d,%d,%b", str, Boolean.valueOf(z), Long.valueOf(j2), Long.valueOf(j3), Boolean.valueOf(z2));
        if (!i()) {
            return false;
        }
        t d2 = d();
        if (d2.d.a(str)) {
            com.tencent.beacon.d.a.c("onUserAction return false, because eventName:[%s] is not allowed in server strategy!", str);
            return false;
        } else if (!z || (z && d2.d.b(str))) {
            l k2 = z2 ? d2.k() : d2.j();
            if (k2 != null) {
                return k2.a(b.a(d2.b, str, z, j2, j3, map));
            }
            return false;
        } else {
            com.tencent.beacon.d.a.c("onUserAction return false, because eventName:[%s] is sampled by svr rate!", str);
            return false;
        }
    }

    public static boolean b(boolean z) {
        t d2 = d();
        if (d2 == null) {
            com.tencent.beacon.d.a.c(" ua module not ready!", new Object[0]);
            return false;
        } else if (i()) {
            return d2.c(z);
        } else {
            return false;
        }
    }

    private static boolean i() {
        g gVar;
        t d2 = d();
        if (d2 == null) {
            com.tencent.beacon.d.a.d("isModuleAble:not init ua", new Object[0]);
            return false;
        }
        boolean e2 = d2.e();
        if (e2 && d2.l()) {
            e2 = d2.m();
        }
        if (e2 && (gVar = d2.d) != null) {
            d e3 = com.tencent.beacon.a.i.e(d2.b);
            if (e3.k + e3.j >= ((long) gVar.f())) {
                com.tencent.beacon.d.a.c(" reach daily consume limited! %d ", Integer.valueOf(gVar.f()));
                return false;
            }
        }
        return e2;
    }

    public final synchronized boolean e() {
        return this.k;
    }

    private t(Context context, boolean z, com.tencent.beacon.f.j jVar, a aVar, g gVar) {
        if (context == null) {
            com.tencent.beacon.d.a.c(" the context is null! init UserActionRecord failed!", new Object[0]);
            this.b = null;
            return;
        }
        Context applicationContext = context.getApplicationContext();
        if (applicationContext != null) {
            this.b = applicationContext;
        } else {
            this.b = context;
        }
        if (g.m() == null) {
            g.a(this.b);
        }
        this.j = jVar;
        if (jVar != null) {
            jVar.a(0, new x(context));
            jVar.a(aVar);
        }
        c a2 = c.a(this.b);
        a2.a((b) this);
        a2.a((i) this);
        a2.a(1, this);
        a2.a(0, jVar);
        a2.a(1, jVar);
        if (gVar != null) {
            this.d = gVar;
        } else {
            this.d = new g();
        }
        this.e = new b(context);
        this.f = new m(context);
        this.g = z;
    }

    private synchronized l j() {
        return this.e;
    }

    private synchronized l k() {
        return this.f;
    }

    private synchronized boolean l() {
        return this.g;
    }

    private synchronized boolean m() {
        return this.h;
    }

    private synchronized void d(boolean z) {
        this.h = true;
    }

    public final boolean c(boolean z) {
        if ((e() ? com.tencent.beacon.a.h.p(this.b) : -1) > 0) {
            try {
                if (this.j == null) {
                    return true;
                }
                e eVar = new e(this.b);
                eVar.a(z);
                this.j.a(eVar);
                return true;
            } catch (Throwable th) {
                com.tencent.beacon.d.a.c(" up common error: %s", th.toString());
                th.printStackTrace();
            }
        }
        return false;
    }

    public final void c() {
        Context context = this.b;
        com.tencent.beacon.d.a.a(" RecordDAO.deleteRecords() start", new Object[0]);
        com.tencent.beacon.d.a.e(" ua first clean :%d", Integer.valueOf(com.tencent.beacon.a.a.a.a(context, new int[]{1, 2, 3, 4}, -1, Long.MAX_VALUE)));
        com.tencent.beacon.d.a.e(" ua remove strategy :%d", Integer.valueOf(com.tencent.beacon.a.h.b(this.b, (int) TXTabBarLayout.TABITEM_TIPS_TEXT_ID)));
    }

    private synchronized void e(boolean z) {
        if (z != e()) {
            l j2 = j();
            if (j2 != null) {
                j2.a(z);
            }
            l k2 = k();
            if (k2 != null) {
                k2.a(z);
            }
            this.k = z;
        }
    }

    public final com.tencent.beacon.f.j f() {
        return this.j;
    }

    public final g g() {
        return this.d;
    }

    public final void b() {
        d(true);
        try {
            com.tencent.beacon.a.b.g b2 = c.a(this.b).b();
            if (b2 != null) {
                h b3 = b2.b(1);
                if (!(b3 == null || this.d == null)) {
                    Set<String> d2 = b3.d();
                    if (d2 != null && d2.size() > 0) {
                        this.d.a(d2);
                    }
                    Set<String> g2 = b3.g();
                    if (g2 != null && g2.size() > 0) {
                        this.d.b(g2);
                    }
                }
                if (!e() || b3 == null) {
                    com.tencent.beacon.d.a.b("event module is disable", new Object[0]);
                } else {
                    if ((e() ? com.tencent.beacon.a.h.p(this.b) : -1) > 0) {
                        com.tencent.beacon.d.a.e(" asyn up module %d", 1);
                        com.tencent.beacon.a.d.a().a(new u(this));
                    }
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
            com.tencent.beacon.d.a.d(" common query end error %s", th.toString());
        }
        if (h() < 2 && k.a(this.b).b() && com.tencent.beacon.a.j.a(this.b).a("sig_1")) {
            com.tencent.beacon.d.a.e(" get lock %s do singleton!", "sig_1");
            if (this.d.i() || this.d.p()) {
                int h2 = this.d.h();
                int g3 = this.d.g();
                if (h2 > 0 && g3 > 0) {
                    com.tencent.beacon.a.d.a().a(104, new com.tencent.beacon.a.c(this.b, h2, g3, new s(this.b), this.d.i(), this.d.p(), this.d.o()), (long) (h2 * 1000), (long) (h2 * 1000));
                }
            }
            if (k.a(this.b).c()) {
                n();
                o();
                d b4 = com.tencent.beacon.a.i.b(this.b);
                if (b4 != null) {
                    long j2 = b4.d + b4.e;
                    long j3 = b4.e;
                    HashMap hashMap = new HashMap();
                    hashMap.put("A64", new StringBuilder().append(j2).toString());
                    hashMap.put("A65", new StringBuilder().append(j3).toString());
                    if (a("rqd_sdk_net_consumed", true, 0, 0, hashMap, false)) {
                        a.b(this.b);
                    }
                }
                try {
                    if (!com.tencent.beacon.a.h.m().equals(com.tencent.beacon.a.a.b(this.b, "rqd_model", Constants.STR_EMPTY))) {
                        com.tencent.beacon.a.d.a().a(new v(this), 50000);
                        try {
                            com.tencent.beacon.a.a.a(this.b, "rqd_model", com.tencent.beacon.a.h.m());
                        } catch (Exception e2) {
                            com.tencent.beacon.d.a.c(" save modelEvent upload flag failed! ", new Object[0]);
                            e2.printStackTrace();
                        }
                    }
                } catch (Exception e3) {
                    com.tencent.beacon.d.a.c(" save modelEvent upload flag failed! ", new Object[0]);
                }
                if (this.d.m()) {
                    int myUid = Process.myUid();
                    if ((TrafficStats.getUidRxBytes(myUid) == -1 || TrafficStats.getUidTxBytes(myUid) == -1) ? false : true) {
                        com.tencent.beacon.a.d.a().a(new w(this), 50000);
                        int n = this.d.n();
                        if (n > 0) {
                            com.tencent.beacon.a.d.a().a(107, new com.tencent.beacon.a.b(this.b), 0, (long) (n * 1000));
                        }
                        new f().a(this.b);
                    }
                }
            }
        }
    }

    private void n() {
        c a2;
        h b2;
        String b3;
        try {
            if ((e() || this.d.l()) && (a2 = c.a(this.b)) != null && (b2 = a2.b().b(1)) != null && (b3 = b2.b()) != null && !Constants.STR_EMPTY.equals(b3.trim())) {
                new j(this.b).a(false);
            }
        } catch (Exception e2) {
            com.tencent.beacon.d.a.c(" startHeart failed! " + e2.getMessage(), new Object[0]);
        }
    }

    private void o() {
        if (com.tencent.beacon.a.h.a(this.b) == null) {
            com.tencent.beacon.d.a.c(" DeviceInfo == null?,return", new Object[0]);
            return;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("A33", com.tencent.beacon.a.h.k(this.b));
        hashMap.put("A63", "Y");
        if (com.tencent.beacon.a.a.c(this.b)) {
            hashMap.put("A21", "Y");
        } else {
            hashMap.put("A21", "N");
        }
        if (c.a(this.b).d()) {
            hashMap.put("A45", "Y");
        } else {
            hashMap.put("A45", "N");
        }
        if (com.tencent.beacon.a.a.g(this.b)) {
            hashMap.put("A66", "F");
        } else {
            hashMap.put("A66", "B");
        }
        hashMap.put("A68", new StringBuilder().append(com.tencent.beacon.a.a.h(this.b)).toString());
        a("rqd_applaunched", true, 0, 0, hashMap, true);
        try {
            if (this.d.k()) {
                int j2 = this.d.j();
                com.tencent.beacon.a.d.a().a(106, new h(this.b, j2), (long) (j2 * 1000), (long) (j2 * 1000));
            }
        } catch (Exception e2) {
            com.tencent.beacon.d.a.c(" startAutoLaunchEvent failed! ", new Object[0]);
            com.tencent.beacon.d.a.b(" startAutoLaunchEvent failed! " + e2.getMessage(), new Object[0]);
        }
    }

    public final void a(com.tencent.beacon.a.b.g gVar) {
        h b2;
        boolean a2;
        if (gVar != null && (b2 = gVar.b(1)) != null && e() != (a2 = b2.a())) {
            com.tencent.beacon.d.a.f("UAR onCommonStrategyChange setUsable:%b ", Boolean.valueOf(a2));
            e(a2);
        }
    }

    public final void a() {
        a(h() + 1);
    }

    public final synchronized int h() {
        return this.l;
    }

    private synchronized void a(int i2) {
        this.l = i2;
    }

    public final void a(Map<String, String> map) {
        if (map != null && map.size() > 0 && this.d != null) {
            this.d.a(map);
        }
    }
}
