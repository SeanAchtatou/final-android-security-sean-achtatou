package com.tencent.beacon.event;

import com.tencent.beacon.a.g;
import com.tencent.beacon.a.h;
import com.tencent.beacon.d.a;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: ProGuard */
final class v implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ t f3445a;

    v(t tVar) {
        this.f3445a = tVar;
    }

    public final void run() {
        if (g.m() == null) {
            a.c(" model even common info == null?,return", new Object[0]);
            return;
        }
        q a2 = q.a(this.f3445a.b);
        if (a2 == null) {
            a.c(" UADeviceInfo == null?,return", new Object[0]);
            return;
        }
        h a3 = h.a(this.f3445a.b);
        HashMap hashMap = new HashMap();
        hashMap.put("A9", a2.j());
        hashMap.put("A10", a2.a());
        hashMap.put("A11", a2.g());
        hashMap.put("A12", a2.h());
        hashMap.put("A13", a2.i());
        hashMap.put("A14", a2.e());
        hashMap.put("A15", a2.f());
        hashMap.put("A16", a2.c());
        hashMap.put("A17", a2.b());
        hashMap.put("A18", a2.d());
        hashMap.put("A20", h.f(this.f3445a.b));
        hashMap.put("A22", q.b(this.f3445a.b));
        hashMap.put("A30", a3.i() + "m");
        hashMap.put("A33", h.k(this.f3445a.b));
        hashMap.put("A52", a2.k());
        hashMap.put("A53", a2.l());
        hashMap.put("A54", a2.m());
        hashMap.put("A55", a2.n());
        hashMap.put("A56", a2.o());
        hashMap.put("A57", a2.p());
        hashMap.put("A58", a2.q());
        StringBuilder sb = new StringBuilder();
        String str = "0";
        long i = h.i(this.f3445a.b);
        if (i > 0) {
            str = new StringBuilder().append((i / 1024) / 1024).toString();
        }
        hashMap.put("A59", sb.append(str).append("m").toString());
        hashMap.put("A69", h.g(this.f3445a.b));
        ArrayList<String> a4 = com.tencent.beacon.b.a.a(new String[]{"/system/bin/sh", "-c", "getprop ro.build.fingerprint"});
        hashMap.put("A82", (a4 == null || a4.size() <= 0) ? Constants.STR_EMPTY : a4.get(0));
        t.a("rqd_model", true, 0, 0, hashMap, true);
    }
}
