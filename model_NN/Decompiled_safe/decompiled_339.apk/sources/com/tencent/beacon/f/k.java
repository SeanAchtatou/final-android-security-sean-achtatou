package com.tencent.beacon.f;

import android.content.Context;
import android.util.SparseArray;
import com.tencent.beacon.a.b.g;
import com.tencent.beacon.a.h;
import com.tencent.beacon.a.i;
import com.tencent.beacon.c.a.c;
import com.tencent.beacon.d.a;
import com.tencent.beacon.d.b;
import com.tencent.beacon.e.f;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* compiled from: ProGuard */
public final class k implements j {

    /* renamed from: a  reason: collision with root package name */
    private static k f3451a = null;
    private SparseArray<i> b = new SparseArray<>(5);
    private List<a> c = new ArrayList(5);
    private f d;
    private Context e = null;
    private boolean f = true;
    private int g = 0;
    private boolean h = true;

    public static synchronized k a(Context context) {
        k kVar;
        synchronized (k.class) {
            if (f3451a == null) {
                f3451a = new k(context, true);
                a.h(" create uphandler up:true", new Object[0]);
            }
            kVar = f3451a;
        }
        return kVar;
    }

    public static synchronized k a(Context context, boolean z) {
        k kVar;
        synchronized (k.class) {
            if (f3451a == null) {
                f3451a = new k(context, z);
                a.h(" create uphandler up: %b", Boolean.valueOf(z));
            }
            if (f3451a.b() != z) {
                f3451a.a(z);
                a.h(" change uphandler up: %b", Boolean.valueOf(z));
            }
            kVar = f3451a;
        }
        return kVar;
    }

    private k(Context context, boolean z) {
        Context context2 = null;
        context2 = context != null ? context.getApplicationContext() : context2;
        if (context2 != null) {
            this.e = context2;
        } else {
            this.e = context;
        }
        this.f = z;
        this.d = f.a(this.e);
    }

    public final synchronized boolean a(int i, i iVar) {
        boolean z;
        if (iVar == null) {
            z = false;
        } else {
            this.b.append(i, iVar);
            z = true;
        }
        return z;
    }

    public final synchronized boolean a(a aVar) {
        boolean z;
        if (aVar == null) {
            z = false;
        } else {
            if (!this.c.contains(aVar)) {
                this.c.add(aVar);
            }
            z = true;
        }
        return z;
    }

    public final synchronized int a() {
        return this.g;
    }

    public final synchronized void a(int i) {
        this.g = i;
    }

    public final void a(b bVar) {
        boolean z;
        byte[] bArr;
        g b2;
        if (!b() || !c()) {
            if (bVar.c() == 2) {
                a.h("  Not UpProc real event sync 2 DB done false", new Object[0]);
                bVar.b(false);
            }
            if (bVar.b != 0) {
                a.h("  Not UpProc not req: %d", Integer.valueOf(bVar.c()));
                return;
            }
            a.h("  NotUpProc com req start ", new Object[0]);
        }
        if (!b.b(this.e)) {
            a.c(" doUpload network is disabled!", new Object[0]);
            if (bVar.c() == 2) {
                bVar.b(false);
                return;
            }
            return;
        }
        a(i.a(this.e));
        if (bVar == null) {
            a.d(" upData == null ", new Object[0]);
            return;
        }
        int c2 = bVar.c();
        long j = 0;
        long j2 = 0;
        boolean z2 = false;
        int i = -1;
        String e2 = bVar.e();
        if (e2 == null || Constants.STR_EMPTY.equals(e2.trim())) {
            a.d(" url error", new Object[0]);
            if (bVar.c() == 2) {
                bVar.b(false);
            }
            a(c2, -1, 0, 0, false, "url error");
            return;
        }
        byte[] b3 = b(bVar);
        String d2 = bVar.d();
        if (d2 != null) {
            e2 = e2 + "?rid=" + d2;
        }
        a.h(" start upload cmd: %d  url:%s  datas:%s", Integer.valueOf(c2), e2, bVar.getClass().toString());
        if (b3 == null) {
            a.d(" sData error", new Object[0]);
            a(c2, -1, 0, 0, false, "sData error");
            return;
        }
        f e3 = e();
        if (e3 == null) {
            a.d(" reqH error", new Object[0]);
            a(c2, -1, 0, 0, false, "reqH error");
            return;
        }
        try {
            e eVar = new e();
            byte[] a2 = e3.a(e2, b3, eVar, bVar);
            if (a2 == null) {
                if (c2 != 100 || "http://strategy.beacon.qq.com/analytics/upload?mType=beacon".equals(e2)) {
                    bArr = a2;
                } else {
                    bArr = e3.a("http://strategy.beacon.qq.com/analytics/upload?mType=beacon", b3, eVar, bVar);
                }
                a(a() + 1);
            } else {
                a(0);
                bArr = a2;
            }
            j = eVar.a();
            j2 = eVar.b();
            c a3 = a(bArr);
            if (a3 != null) {
                i = a3.b;
                z2 = a3.f3410a == 0;
                a.b("response.cmd:%d response.result:%d", Integer.valueOf(a3.b), Byte.valueOf(a3.f3410a));
            }
            if (!(bVar == null || a3 == null)) {
                com.tencent.beacon.a.g m = com.tencent.beacon.a.g.m();
                if (m != null) {
                    if (a3.d != null) {
                        m.a(a3.d.trim());
                    }
                    m.a(a3.g - new Date().getTime());
                    a.h(" fix ip:%s  tmgap: %d", m.g(), Long.valueOf(m.h()));
                }
                if ((a3.b == 101 || a3.b == 103 || a3.b == 105) && (b2 = com.tencent.beacon.a.b.c.a(this.e).b()) != null) {
                    if (b2.j() != a3.f) {
                        b2.b(a3.f);
                    }
                    if (b2.i() != a3.e) {
                        b2.a(a3.e);
                    }
                    if (b2.k() == null || (a3.h != null && !b2.k().equals(a3.h))) {
                        b2.b(a3.h);
                    }
                }
                byte[] bArr2 = a3.c;
                if (bArr2 != null) {
                    byte[] b4 = com.tencent.beacon.b.a.b(bArr2, a3.f, a3.e, a3.i);
                    SparseArray<i> f2 = f();
                    if (f2 != null && f2.size() > 0) {
                        int c3 = bVar.c();
                        int i2 = a3.b;
                        if (a3.f3410a == 0 && c3 > 0 && c3 <= 5) {
                            try {
                                if (Constants.STR_EMPTY.equals(com.tencent.beacon.a.a.b(this.e, "QIMEI_DENGTA", Constants.STR_EMPTY)) && !g.a().h()) {
                                    com.tencent.beacon.a.b.c.a().a(new d(this.e));
                                    com.tencent.beacon.a.a.a(this.e, "GEN_QIMEI", com.tencent.beacon.b.a.g());
                                }
                            } catch (Exception e4) {
                            }
                        }
                        if (i2 != 0) {
                            switch (c3) {
                                case 4:
                                    if (i2 != 105) {
                                        a.c(" UNMATCH req: %d , rep: %d", Integer.valueOf(c3), Integer.valueOf(i2));
                                        break;
                                    }
                                    a(f2, i2, b4);
                                    break;
                                case 100:
                                    if (i2 != 101) {
                                        a.c(" UNMATCH req: %d , rep: %d", Integer.valueOf(c3), Integer.valueOf(i2));
                                        break;
                                    }
                                    a(f2, i2, b4);
                                    break;
                                case 102:
                                    if (i2 != 103) {
                                        a.c(" UNMATCH req: %d  , rep: %d", Integer.valueOf(c3), Integer.valueOf(i2));
                                        break;
                                    }
                                    a(f2, i2, b4);
                                    break;
                                default:
                                    a.c(" unknown req: %d ", Integer.valueOf(c3));
                                    break;
                            }
                        } else {
                            a.h(" response no datas ", new Object[0]);
                        }
                    } else {
                        a.h(" no handler! ", new Object[0]);
                    }
                } else {
                    a.h(" no response! ", new Object[0]);
                }
            }
            a(c2, i, j, j2, z2, null);
            bVar.b(z2);
        } catch (Throwable th) {
            th = th;
            z2 = z;
            bVar.b(z2);
            throw th;
        }
    }

    private static byte[] b(b bVar) {
        if (bVar != null) {
            try {
                com.tencent.beacon.c.a.b a2 = bVar.a();
                if (a2 != null) {
                    a.b(" RequestPackage info appkey:%s sdkid:%s appVersion:%s cmd: %d", a2.b, a2.d, a2.c, Integer.valueOf(a2.f));
                    f fVar = new f();
                    fVar.a(1);
                    fVar.b("test");
                    fVar.a("test");
                    fVar.a("detail", a2);
                    return h.a(2, fVar.a());
                }
            } catch (Throwable th) {
                th.printStackTrace();
                bVar.b();
            }
        }
        return null;
    }

    private static c a(byte[] bArr) {
        if (bArr != null) {
            try {
                byte[] b2 = h.b(2, bArr);
                f fVar = new f();
                fVar.a(b2);
                c cVar = new c();
                a.b(" covert to ResponsePackage ", new Object[0]);
                return (c) fVar.b("detail", cVar);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return null;
    }

    private void a(int i, int i2, long j, long j2, boolean z, String str) {
        a[] d2 = d();
        if (d2 != null) {
            for (a a2 : d2) {
                a2.a(i, i2, j, j2, z, str);
            }
        }
    }

    private synchronized a[] d() {
        a[] aVarArr;
        if (this.c == null || this.c.size() <= 0) {
            aVarArr = null;
        } else {
            aVarArr = (a[]) this.c.toArray(new a[0]);
        }
        return aVarArr;
    }

    private synchronized f e() {
        return this.d;
    }

    private synchronized SparseArray<i> f() {
        SparseArray<i> sparseArray;
        if (this.b == null || this.b.size() <= 0) {
            sparseArray = null;
        } else {
            new com.tencent.beacon.d.c();
            sparseArray = com.tencent.beacon.d.c.a(this.b);
        }
        return sparseArray;
    }

    private boolean a(SparseArray<i> sparseArray, int i, byte[] bArr) {
        if (sparseArray == null || bArr == null) {
            return true;
        }
        switch (i) {
            case 103:
                try {
                    a.a(" process CMD_RESPONSE_GEN_QIMEI", new Object[0]);
                    com.tencent.beacon.e.a aVar = new com.tencent.beacon.e.a(bArr);
                    com.tencent.beacon.c.c.a aVar2 = new com.tencent.beacon.c.c.a();
                    aVar2.a(aVar);
                    if (aVar2.f3413a != null) {
                        com.tencent.beacon.b.a.a(this.e).a(aVar2.f3413a);
                        com.tencent.beacon.a.a.a(this.e, "QIMEI_DENGTA", aVar2.f3413a);
                    }
                    a.h(" Qimei:%s  imei:%s  imsi:%s  aid:%s  mac:%s ", aVar2.f3413a, aVar2.b, aVar2.d, aVar2.e, aVar2.c);
                } catch (Throwable th) {
                    th.printStackTrace();
                }
                return true;
            default:
                i iVar = sparseArray.get(i);
                if (iVar == null) {
                    a.c(" no handler key:%d", Integer.valueOf(i));
                    return false;
                }
                try {
                    a.b(" key:%d  handler: %s", Integer.valueOf(i), iVar.getClass().toString());
                    iVar.a(i, bArr, true);
                    return true;
                } catch (Throwable th2) {
                    th2.printStackTrace();
                    a.d(" handle error key:%d", Integer.valueOf(i));
                    return false;
                }
        }
    }

    public final synchronized boolean b() {
        return this.f;
    }

    private synchronized void a(boolean z) {
        this.f = z;
    }

    public final synchronized boolean c() {
        boolean z;
        if (b.a(this.e)) {
            z = true;
        } else {
            z = this.h;
        }
        return z;
    }
}
