package com.tencent.beacon.a;

import android.content.Context;
import com.tencent.beacon.a.a.e;
import com.tencent.beacon.d.a;
import com.tencent.beacon.event.t;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/* compiled from: ProGuard */
public final class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private Context f3395a;
    private int b;
    private int c;
    private Runnable d;
    private boolean e;
    private boolean f;
    private boolean g;
    private int h = 0;

    public c(Context context, int i, int i2, Runnable runnable, boolean z, boolean z2, boolean z3) {
        this.f3395a = context;
        this.b = i;
        this.c = i2;
        this.d = runnable;
        this.e = z;
        this.f = z2;
        this.g = z3;
    }

    public final void run() {
        e eVar;
        String str;
        String str2;
        boolean g2 = a.g(this.f3395a);
        String str3 = g2 ? "F" : "B";
        h a2 = h.a(this.f3395a);
        if (this.b > 0 && this.f) {
            if (this.f) {
                String f2 = a2.f();
                str = (f2 == null || f2.equals(Constants.STR_EMPTY)) ? f2 : str3 + "," + f2;
            } else {
                str = null;
            }
            if (this.g) {
                String g3 = h.g();
                str2 = (g3 == null || g3.equals(Constants.STR_EMPTY)) ? g3 : str3 + "," + g3;
            } else {
                str2 = null;
            }
            try {
                String b2 = a.b(this.f3395a, "app_mem_info", Constants.STR_EMPTY);
                String b3 = a.b(this.f3395a, "app_cpu_info", Constants.STR_EMPTY);
                if ((this.h != 0 || Constants.STR_EMPTY.equals(b2)) && this.h < this.c / this.b) {
                    if (Constants.STR_EMPTY.equals(b2)) {
                        a.a(this.f3395a, "app_mem_info", str);
                    } else {
                        a.a(this.f3395a, "app_mem_info", b2 + ";" + str);
                    }
                    if (str2 != null) {
                        if (Constants.STR_EMPTY.equals(b3)) {
                            a.a(this.f3395a, "app_cpu_info", str2);
                        } else {
                            a.a(this.f3395a, "app_cpu_info", b3 + ";" + str2);
                        }
                    }
                    this.h++;
                } else {
                    HashMap hashMap = new HashMap();
                    hashMap.put("A78", b2 + ";" + str);
                    if (str2 != null) {
                        hashMap.put("A77", b3 + ";" + str2);
                    }
                    if (t.a("rqd_res_occ", true, 0, 0, hashMap, true)) {
                        a.a(this.f3395a, "app_mem_info", Constants.STR_EMPTY);
                        if (str2 != null) {
                            a.a(this.f3395a, "app_cpu_info", Constants.STR_EMPTY);
                        }
                        this.h = 0;
                    } else {
                        a.a(this.f3395a, "app_mem_info", b2 + ";" + str);
                        if (str2 != null) {
                            a.a(this.f3395a, "app_cpu_info", b3 + ";" + str2);
                        }
                        this.h++;
                    }
                }
            } catch (Exception e2) {
                a.c("get resinfo from sp failed! ", new Object[0]);
            }
        }
        if (this.b > 0 && this.e) {
            long time = new Date().getTime();
            e o = h.o(this.f3395a);
            if (o == null) {
                e eVar2 = new e();
                eVar2.c(time);
                eVar2.d(time);
                eVar2.b(0);
                eVar2.a(0);
                eVar = eVar2;
            } else {
                eVar = o;
            }
            eVar.a(eVar.a() + ((long) (this.b / 60)));
            if (g2) {
                eVar.b(eVar.b() + ((long) (this.b / 60)));
            }
            eVar.d(time);
            Context context = this.f3395a;
            if (!(context == null || eVar == null)) {
                ArrayList arrayList = new ArrayList();
                com.tencent.beacon.a.a.a aVar = new com.tencent.beacon.a.a.a(8, 0, eVar.d(), com.tencent.beacon.b.a.a(eVar));
                aVar.a(eVar.e());
                arrayList.add(aVar);
                com.tencent.beacon.a.a.a.b(context, arrayList);
            }
            a.e(" used:%d  seen:%d  next:%d", Long.valueOf(eVar.a()), Long.valueOf(eVar.b()), Integer.valueOf(this.b));
            if (eVar.a() >= ((long) this.c)) {
                d.a().a(this.d);
            }
        }
    }
}
