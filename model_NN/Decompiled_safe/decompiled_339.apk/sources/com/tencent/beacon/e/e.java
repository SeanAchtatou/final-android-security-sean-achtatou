package com.tencent.beacon.e;

import com.tencent.connect.common.Constants;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public final class e extends c {
    private static byte[] k = null;
    private static Map<String, String> l = null;

    /* renamed from: a  reason: collision with root package name */
    public short f3428a = 3;
    public int b = 0;
    public String c = null;
    public String d = null;
    public byte[] e;
    private byte f = 0;
    private int g = 0;
    private int h = 0;
    private Map<String, String> i;
    private Map<String, String> j;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.d.a(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.tencent.beacon.e.d.a(byte, int):void
      com.tencent.beacon.e.d.a(int, int):void
      com.tencent.beacon.e.d.a(long, int):void
      com.tencent.beacon.e.d.a(com.tencent.beacon.e.c, int):void
      com.tencent.beacon.e.d.a(java.lang.Object, int):void
      com.tencent.beacon.e.d.a(java.lang.String, int):void
      com.tencent.beacon.e.d.a(java.util.Collection, int):void
      com.tencent.beacon.e.d.a(short, int):void
      com.tencent.beacon.e.d.a(boolean, int):void
      com.tencent.beacon.e.d.a(byte[], int):void
      com.tencent.beacon.e.d.a(java.util.Map, int):void */
    public final void a(d dVar) {
        dVar.a(this.f3428a, 1);
        dVar.a(this.f, 2);
        dVar.a(this.g, 3);
        dVar.a(this.b, 4);
        dVar.a(this.c, 5);
        dVar.a(this.d, 6);
        dVar.a(this.e, 7);
        dVar.a(this.h, 8);
        dVar.a((Map) this.i, 9);
        dVar.a((Map) this.j, 10);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(short, int, boolean):short
     arg types: [short, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object */
    public final void a(a aVar) {
        try {
            this.f3428a = aVar.a(this.f3428a, 1, true);
            this.f = aVar.a(this.f, 2, true);
            this.g = aVar.a(this.g, 3, true);
            this.b = aVar.a(this.b, 4, true);
            this.c = aVar.b(5, true);
            this.d = aVar.b(6, true);
            if (k == null) {
                k = new byte[1];
            }
            byte[] bArr = k;
            this.e = aVar.c(7, true);
            this.h = aVar.a(this.h, 8, true);
            if (l == null) {
                HashMap hashMap = new HashMap();
                l = hashMap;
                hashMap.put(Constants.STR_EMPTY, Constants.STR_EMPTY);
            }
            this.i = (Map) aVar.a((Object) l, 9, true);
            if (l == null) {
                HashMap hashMap2 = new HashMap();
                l = hashMap2;
                hashMap2.put(Constants.STR_EMPTY, Constants.STR_EMPTY);
            }
            this.j = (Map) aVar.a((Object) l, 10, true);
        } catch (Exception e2) {
            e2.printStackTrace();
            throw new RuntimeException(e2);
        }
    }
}
