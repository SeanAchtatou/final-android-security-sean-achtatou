package com.tencent.open;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import com.tencent.open.a.n;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
class a implements IUiListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SocialApiIml f3749a;
    private IUiListener b;
    private String c;
    private String d;
    private Bundle e;
    private Activity f;

    a(SocialApiIml socialApiIml, Activity activity, IUiListener iUiListener, String str, String str2, Bundle bundle) {
        this.f3749a = socialApiIml;
        this.b = iUiListener;
        this.c = str;
        this.d = str2;
        this.e = bundle;
    }

    public void onComplete(Object obj) {
        String str;
        try {
            str = ((JSONObject) obj).getString(SocialConstants.PARAM_ENCRY_EOKEN);
        } catch (JSONException e2) {
            e2.printStackTrace();
            n.b(n.d, "OpenApi, EncrytokenListener() onComplete error", e2);
            str = null;
        }
        this.e.putString("encrytoken", str);
        this.f3749a.a(this.f3749a.b, this.c, this.e, this.d, this.b);
        if (TextUtils.isEmpty(str)) {
            n.b("miles", "The token get from qq or qzone is empty. Write temp token to localstorage.");
            this.f3749a.writeEncryToken(this.f);
        }
    }

    public void onError(UiError uiError) {
        n.b(n.d, "OpenApi, EncryptTokenListener() onError" + uiError.errorMessage);
        this.b.onError(uiError);
    }

    public void onCancel() {
        this.b.onCancel();
    }
}
