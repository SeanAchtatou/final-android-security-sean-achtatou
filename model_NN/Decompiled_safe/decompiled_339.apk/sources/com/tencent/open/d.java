package com.tencent.open;

import android.webkit.WebView;
import java.lang.ref.WeakReference;

/* compiled from: ProGuard */
public class d {

    /* renamed from: a  reason: collision with root package name */
    protected WeakReference<WebView> f3776a;
    protected long b;
    protected String c;

    public d(WebView webView, long j, String str) {
        this.f3776a = new WeakReference<>(webView);
        this.b = j;
        this.c = str;
    }

    public void a(Object obj) {
        WebView webView = this.f3776a.get();
        if (webView != null) {
            String str = "'undefined'";
            if (obj instanceof String) {
                str = "'" + ((Object) ((String) obj).replace("\\", "\\\\").replace("'", "\\'")) + "'";
            } else if ((obj instanceof Number) || (obj instanceof Long) || (obj instanceof Integer) || (obj instanceof Double) || (obj instanceof Float)) {
                str = obj.toString();
            } else if (obj instanceof Boolean) {
                str = obj.toString();
            }
            webView.loadUrl("javascript:window.JsBridge&&JsBridge.callback(" + this.b + ",{'r':0,'result':" + str + "});");
        }
    }

    public void a() {
        WebView webView = this.f3776a.get();
        if (webView != null) {
            webView.loadUrl("javascript:window.JsBridge&&JsBridge.callback(" + this.b + ",{'r':1,'result':'no such method'})");
        }
    }

    public void a(String str) {
        WebView webView = this.f3776a.get();
        if (webView != null) {
            webView.loadUrl("javascript:" + str);
        }
    }
}
