package com.tencent.open.b;

import android.os.Bundle;
import com.tencent.open.a.n;
import com.tencent.open.utils.Global;
import com.tencent.open.utils.HttpUtils;
import com.tencent.open.utils.OpenConfig;
import com.tencent.open.utils.ServerSetting;
import java.net.SocketTimeoutException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.ByteArrayEntity;

/* compiled from: ProGuard */
class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f3770a;

    k(g gVar) {
        this.f3770a = gVar;
    }

    /*  JADX ERROR: IF instruction can be used only in fallback mode
        jadx.core.utils.exceptions.CodegenException: IF instruction can be used only in fallback mode
        	at jadx.core.codegen.InsnGen.fallbackOnlyInsn(InsnGen.java:580)
        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:486)
        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:249)
        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:220)
        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:110)
        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:56)
        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:99)
        	at jadx.core.codegen.RegionGen.makeLoop(RegionGen.java:206)
        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:67)
        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:99)
        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:143)
        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:63)
        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:99)
        	at jadx.core.codegen.RegionGen.makeTryCatch(RegionGen.java:318)
        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:69)
        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:215)
        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:208)
        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:322)
        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:275)
        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$3(ClassGen.java:244)
        	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
        	at java.base/java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
        	at java.base/java.util.stream.Sink$ChainedReference.end(Sink.java:258)
        	at java.base/java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:485)
        	at java.base/java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:474)
        	at java.base/java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
        	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
        	at java.base/java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
        	at java.base/java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:497)
        	at jadx.core.codegen.ClassGen.addInnerClsAndMethods(ClassGen.java:240)
        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:231)
        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:115)
        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:81)
        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:45)
        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:34)
        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:22)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:61)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        */
    public void run() {
        /*
            r8 = this;
            r1 = 0
            com.tencent.open.b.g r0 = r8.f3770a     // Catch:{ Exception -> 0x00b2 }
            android.os.Bundle r4 = r0.c()     // Catch:{ Exception -> 0x00b2 }
            if (r4 != 0) goto L_0x000a
        L_0x0009:
            return
        L_0x000a:
            android.content.Context r0 = com.tencent.open.utils.Global.getContext()     // Catch:{ Exception -> 0x00b2 }
            r2 = 0
            com.tencent.open.utils.OpenConfig r0 = com.tencent.open.utils.OpenConfig.getInstance(r0, r2)     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r2 = "Common_HttpRetryCount"
            int r0 = r0.getInt(r2)     // Catch:{ Exception -> 0x00b2 }
            if (r0 != 0) goto L_0x00bc
            r0 = 3
            r3 = r0
        L_0x001d:
            java.lang.String r0 = com.tencent.open.b.g.f3766a     // Catch:{ Exception -> 0x00b2 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b2 }
            r2.<init>()     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r5 = "-->doReportCgi, retryCount: "
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x00b2 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00b2 }
            com.tencent.open.a.n.b(r0, r2)     // Catch:{ Exception -> 0x00b2 }
            r0 = r1
        L_0x0036:
            int r0 = r0 + 1
            android.content.Context r2 = com.tencent.open.utils.Global.getContext()     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            r5 = 0
            java.lang.String r6 = "http://wspeed.qq.com/w.cgi"
            org.apache.http.client.HttpClient r2 = com.tencent.open.utils.HttpUtils.getHttpClient(r2, r5, r6)     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            org.apache.http.client.methods.HttpPost r5 = new org.apache.http.client.methods.HttpPost     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            java.lang.String r6 = "http://wspeed.qq.com/w.cgi"
            r5.<init>(r6)     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            java.lang.String r6 = "Accept-Encoding"
            java.lang.String r7 = "gzip"
            r5.addHeader(r6, r7)     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            java.lang.String r6 = "Content-Type"
            java.lang.String r7 = "application/x-www-form-urlencoded"
            r5.setHeader(r6, r7)     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            java.lang.String r6 = com.tencent.open.utils.HttpUtils.encodeUrl(r4)     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            byte[] r6 = r6.getBytes()     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            org.apache.http.entity.ByteArrayEntity r7 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            r7.<init>(r6)     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            r5.setEntity(r7)     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            org.apache.http.HttpResponse r2 = r2.execute(r5)     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            org.apache.http.StatusLine r2 = r2.getStatusLine()     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            int r2 = r2.getStatusCode()     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            java.lang.String r5 = com.tencent.open.b.g.f3766a     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            r6.<init>()     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            java.lang.String r7 = "-->doReportCgi, statusCode: "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            java.lang.StringBuilder r6 = r6.append(r2)     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            java.lang.String r6 = r6.toString()     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            com.tencent.open.a.n.b(r5, r6)     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            r5 = 200(0xc8, float:2.8E-43)
            if (r2 != r5) goto L_0x009a
            com.tencent.open.b.f r2 = com.tencent.open.b.f.a()     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            java.lang.String r5 = "report_cgi"
            r2.b(r5)     // Catch:{ ConnectTimeoutException -> 0x00bf, SocketTimeoutException -> 0x00ca, Exception -> 0x00d3 }
            r1 = 1
        L_0x009a:
            if (r1 != 0) goto L_0x00a9
            com.tencent.open.b.f r0 = com.tencent.open.b.f.a()     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r1 = "report_cgi"
            com.tencent.open.b.g r2 = r8.f3770a     // Catch:{ Exception -> 0x00b2 }
            java.util.List<java.io.Serializable> r2 = r2.d     // Catch:{ Exception -> 0x00b2 }
            r0.a(r1, r2)     // Catch:{ Exception -> 0x00b2 }
        L_0x00a9:
            com.tencent.open.b.g r0 = r8.f3770a     // Catch:{ Exception -> 0x00b2 }
            java.util.List<java.io.Serializable> r0 = r0.d     // Catch:{ Exception -> 0x00b2 }
            r0.clear()     // Catch:{ Exception -> 0x00b2 }
            goto L_0x0009
        L_0x00b2:
            r0 = move-exception
            java.lang.String r1 = com.tencent.open.b.g.f3766a
            java.lang.String r2 = "-->doReportCgi, doupload exception out."
            com.tencent.open.a.n.a(r1, r2, r0)
            goto L_0x0009
        L_0x00bc:
            r3 = r0
            goto L_0x001d
        L_0x00bf:
            r2 = move-exception
            java.lang.String r5 = com.tencent.open.b.g.f3766a     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r6 = "-->doReportCgi, doupload exception"
            com.tencent.open.a.n.a(r5, r6, r2)     // Catch:{ Exception -> 0x00b2 }
        L_0x00c7:
            if (r0 < r3) goto L_0x0036
            goto L_0x009a
        L_0x00ca:
            r2 = move-exception
            java.lang.String r5 = com.tencent.open.b.g.f3766a     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r6 = "-->doReportCgi, doupload exception"
            com.tencent.open.a.n.a(r5, r6, r2)     // Catch:{ Exception -> 0x00b2 }
            goto L_0x00c7
        L_0x00d3:
            r0 = move-exception
            java.lang.String r2 = com.tencent.open.b.g.f3766a     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r3 = "-->doReportCgi, doupload exception"
            com.tencent.open.a.n.a(r2, r3, r0)     // Catch:{ Exception -> 0x00b2 }
            goto L_0x009a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.b.k.run():void");
    }
}
