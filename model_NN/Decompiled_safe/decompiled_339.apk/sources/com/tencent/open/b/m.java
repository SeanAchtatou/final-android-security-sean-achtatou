package com.tencent.open.b;

import android.os.Bundle;

/* compiled from: ProGuard */
class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Bundle f3772a;
    final /* synthetic */ String b;
    final /* synthetic */ boolean c;
    final /* synthetic */ String d;
    final /* synthetic */ g e;

    m(g gVar, Bundle bundle, String str, boolean z, String str2) {
        this.e = gVar;
        this.f3772a = bundle;
        this.b = str;
        this.c = z;
        this.d = str2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x00ac A[SYNTHETIC, Splitter:B:22:0x00ac] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0114 A[Catch:{ Exception -> 0x00b5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0101 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r10 = this;
            r2 = 1
            r0 = 0
            android.os.Bundle r1 = r10.f3772a     // Catch:{ Exception -> 0x00b5 }
            if (r1 != 0) goto L_0x000e
            java.lang.String r0 = com.tencent.open.b.g.f3766a     // Catch:{ Exception -> 0x00b5 }
            java.lang.String r1 = "-->httpRequest, params is null!"
            com.tencent.open.a.n.e(r0, r1)     // Catch:{ Exception -> 0x00b5 }
        L_0x000d:
            return
        L_0x000e:
            int r1 = com.tencent.open.b.e.a()     // Catch:{ Exception -> 0x00b5 }
            if (r1 != 0) goto L_0x00bf
            r1 = 3
            r4 = r1
        L_0x0016:
            java.lang.String r1 = com.tencent.open.b.g.f3766a     // Catch:{ Exception -> 0x00b5 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b5 }
            r3.<init>()     // Catch:{ Exception -> 0x00b5 }
            java.lang.String r5 = "-->httpRequest, retryCount: "
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ Exception -> 0x00b5 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00b5 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00b5 }
            com.tencent.open.a.n.b(r1, r3)     // Catch:{ Exception -> 0x00b5 }
            android.content.Context r1 = com.tencent.open.utils.Global.getContext()     // Catch:{ Exception -> 0x00b5 }
            r3 = 0
            java.lang.String r5 = r10.b     // Catch:{ Exception -> 0x00b5 }
            org.apache.http.client.HttpClient r5 = com.tencent.open.utils.HttpUtils.getHttpClient(r1, r3, r5)     // Catch:{ Exception -> 0x00b5 }
            android.os.Bundle r1 = r10.f3772a     // Catch:{ Exception -> 0x00b5 }
            java.lang.String r1 = com.tencent.open.utils.HttpUtils.encodeUrl(r1)     // Catch:{ Exception -> 0x00b5 }
            boolean r3 = r10.c     // Catch:{ Exception -> 0x00b5 }
            if (r3 == 0) goto L_0x0126
            java.lang.String r1 = java.net.URLEncoder.encode(r1)     // Catch:{ Exception -> 0x00b5 }
            r3 = r1
        L_0x0048:
            java.lang.String r1 = r10.d     // Catch:{ Exception -> 0x00b5 }
            java.lang.String r1 = r1.toUpperCase()     // Catch:{ Exception -> 0x00b5 }
            java.lang.String r6 = "GET"
            boolean r1 = r1.equals(r6)     // Catch:{ Exception -> 0x00b5 }
            if (r1 == 0) goto L_0x00c2
            java.lang.StringBuffer r6 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x00b5 }
            java.lang.String r1 = r10.b     // Catch:{ Exception -> 0x00b5 }
            r6.<init>(r1)     // Catch:{ Exception -> 0x00b5 }
            r6.append(r3)     // Catch:{ Exception -> 0x00b5 }
            org.apache.http.client.methods.HttpGet r1 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x00b5 }
            java.lang.String r3 = r6.toString()     // Catch:{ Exception -> 0x00b5 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x00b5 }
            r3 = r1
        L_0x006a:
            java.lang.String r1 = "Accept-Encoding"
            java.lang.String r6 = "gzip"
            r3.addHeader(r1, r6)     // Catch:{ Exception -> 0x00b5 }
            java.lang.String r1 = "Content-Type"
            java.lang.String r6 = "application/x-www-form-urlencoded"
            r3.addHeader(r1, r6)     // Catch:{ Exception -> 0x00b5 }
            r1 = r0
        L_0x0079:
            int r1 = r1 + 1
            org.apache.http.HttpResponse r6 = r5.execute(r3)     // Catch:{ ConnectTimeoutException -> 0x00f7, SocketTimeoutException -> 0x0102, Exception -> 0x010b }
            org.apache.http.StatusLine r6 = r6.getStatusLine()     // Catch:{ ConnectTimeoutException -> 0x00f7, SocketTimeoutException -> 0x0102, Exception -> 0x010b }
            int r6 = r6.getStatusCode()     // Catch:{ ConnectTimeoutException -> 0x00f7, SocketTimeoutException -> 0x0102, Exception -> 0x010b }
            java.lang.String r7 = com.tencent.open.b.g.f3766a     // Catch:{ ConnectTimeoutException -> 0x00f7, SocketTimeoutException -> 0x0102, Exception -> 0x010b }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ ConnectTimeoutException -> 0x00f7, SocketTimeoutException -> 0x0102, Exception -> 0x010b }
            r8.<init>()     // Catch:{ ConnectTimeoutException -> 0x00f7, SocketTimeoutException -> 0x0102, Exception -> 0x010b }
            java.lang.String r9 = "-->httpRequest, statusCode: "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ ConnectTimeoutException -> 0x00f7, SocketTimeoutException -> 0x0102, Exception -> 0x010b }
            java.lang.StringBuilder r8 = r8.append(r6)     // Catch:{ ConnectTimeoutException -> 0x00f7, SocketTimeoutException -> 0x0102, Exception -> 0x010b }
            java.lang.String r8 = r8.toString()     // Catch:{ ConnectTimeoutException -> 0x00f7, SocketTimeoutException -> 0x0102, Exception -> 0x010b }
            com.tencent.open.a.n.b(r7, r8)     // Catch:{ ConnectTimeoutException -> 0x00f7, SocketTimeoutException -> 0x0102, Exception -> 0x010b }
            r7 = 200(0xc8, float:2.8E-43)
            if (r6 == r7) goto L_0x00ee
            java.lang.String r6 = com.tencent.open.b.g.f3766a     // Catch:{ ConnectTimeoutException -> 0x00f7, SocketTimeoutException -> 0x0102, Exception -> 0x010b }
            java.lang.String r7 = "-->ReportCenter httpRequest : HttpStatuscode != 200"
            com.tencent.open.a.n.b(r6, r7)     // Catch:{ ConnectTimeoutException -> 0x00f7, SocketTimeoutException -> 0x0102, Exception -> 0x010b }
        L_0x00aa:
            if (r0 != r2) goto L_0x0114
            java.lang.String r0 = com.tencent.open.b.g.f3766a     // Catch:{ Exception -> 0x00b5 }
            java.lang.String r1 = "-->ReportCenter httpRequest Thread request success"
            com.tencent.open.a.n.b(r0, r1)     // Catch:{ Exception -> 0x00b5 }
            goto L_0x000d
        L_0x00b5:
            r0 = move-exception
            java.lang.String r0 = com.tencent.open.b.g.f3766a
            java.lang.String r1 = "-->httpRequest, exception in serial executor."
            com.tencent.open.a.n.b(r0, r1)
            goto L_0x000d
        L_0x00bf:
            r4 = r1
            goto L_0x0016
        L_0x00c2:
            java.lang.String r1 = r10.d     // Catch:{ Exception -> 0x00b5 }
            java.lang.String r1 = r1.toUpperCase()     // Catch:{ Exception -> 0x00b5 }
            java.lang.String r6 = "POST"
            boolean r1 = r1.equals(r6)     // Catch:{ Exception -> 0x00b5 }
            if (r1 == 0) goto L_0x00e5
            org.apache.http.client.methods.HttpPost r1 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x00b5 }
            java.lang.String r6 = r10.b     // Catch:{ Exception -> 0x00b5 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x00b5 }
            byte[] r3 = r3.getBytes()     // Catch:{ Exception -> 0x00b5 }
            org.apache.http.entity.ByteArrayEntity r6 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ Exception -> 0x00b5 }
            r6.<init>(r3)     // Catch:{ Exception -> 0x00b5 }
            r1.setEntity(r6)     // Catch:{ Exception -> 0x00b5 }
            r3 = r1
            goto L_0x006a
        L_0x00e5:
            java.lang.String r0 = com.tencent.open.b.g.f3766a     // Catch:{ Exception -> 0x00b5 }
            java.lang.String r1 = "-->httpRequest unkonw request method return."
            com.tencent.open.a.n.e(r0, r1)     // Catch:{ Exception -> 0x00b5 }
            goto L_0x000d
        L_0x00ee:
            java.lang.String r0 = com.tencent.open.b.g.f3766a     // Catch:{ ConnectTimeoutException -> 0x0123, SocketTimeoutException -> 0x0120, Exception -> 0x011d }
            java.lang.String r6 = "-->ReportCenter httpRequest Thread success"
            com.tencent.open.a.n.b(r0, r6)     // Catch:{ ConnectTimeoutException -> 0x0123, SocketTimeoutException -> 0x0120, Exception -> 0x011d }
            r0 = r2
            goto L_0x00aa
        L_0x00f7:
            r6 = move-exception
        L_0x00f8:
            java.lang.String r6 = com.tencent.open.b.g.f3766a     // Catch:{ Exception -> 0x00b5 }
            java.lang.String r7 = "-->ReportCenter httpRequest ConnectTimeoutException"
            com.tencent.open.a.n.b(r6, r7)     // Catch:{ Exception -> 0x00b5 }
        L_0x00ff:
            if (r1 < r4) goto L_0x0079
            goto L_0x00aa
        L_0x0102:
            r6 = move-exception
        L_0x0103:
            java.lang.String r6 = com.tencent.open.b.g.f3766a     // Catch:{ Exception -> 0x00b5 }
            java.lang.String r7 = "-->ReportCenter httpRequest SocketTimeoutException"
            com.tencent.open.a.n.b(r6, r7)     // Catch:{ Exception -> 0x00b5 }
            goto L_0x00ff
        L_0x010b:
            r1 = move-exception
        L_0x010c:
            java.lang.String r1 = com.tencent.open.b.g.f3766a     // Catch:{ Exception -> 0x00b5 }
            java.lang.String r3 = "-->ReportCenter httpRequest Exception"
            com.tencent.open.a.n.b(r1, r3)     // Catch:{ Exception -> 0x00b5 }
            goto L_0x00aa
        L_0x0114:
            java.lang.String r0 = com.tencent.open.b.g.f3766a     // Catch:{ Exception -> 0x00b5 }
            java.lang.String r1 = "-->ReportCenter httpRequest Thread request failed"
            com.tencent.open.a.n.b(r0, r1)     // Catch:{ Exception -> 0x00b5 }
            goto L_0x000d
        L_0x011d:
            r0 = move-exception
            r0 = r2
            goto L_0x010c
        L_0x0120:
            r0 = move-exception
            r0 = r2
            goto L_0x0103
        L_0x0123:
            r0 = move-exception
            r0 = r2
            goto L_0x00f8
        L_0x0126:
            r3 = r1
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.b.m.run():void");
    }
}
