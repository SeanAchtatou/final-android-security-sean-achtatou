package com.tencent.open.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.text.TextUtils;
import com.tencent.connect.a.a;
import com.tencent.connect.auth.QQToken;
import com.tencent.connect.common.Constants;
import com.tencent.open.a.n;
import com.tencent.open.utils.Util;
import com.tencent.tauth.IRequestListener;
import java.io.ByteArrayOutputStream;
import java.io.CharConversionException;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InvalidClassException;
import java.io.InvalidObjectException;
import java.io.NotActiveException;
import java.io.NotSerializableException;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.io.SyncFailedException;
import java.io.UTFDataFormatException;
import java.io.UnsupportedEncodingException;
import java.io.WriteAbortedException;
import java.net.BindException;
import java.net.ConnectException;
import java.net.HttpRetryException;
import java.net.MalformedURLException;
import java.net.NoRouteToHostException;
import java.net.PortUnreachableException;
import java.net.ProtocolException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.net.UnknownServiceException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.FileLockInterruptionException;
import java.nio.charset.MalformedInputException;
import java.nio.charset.UnmappableCharacterException;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.InvalidPropertiesFormatException;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLKeyException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLProtocolException;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import org.apache.http.ConnectionClosedException;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.MalformedChunkCodingException;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class HttpUtils {

    /* renamed from: a  reason: collision with root package name */
    private static final String f3784a = HttpUtils.class.getName();

    private HttpUtils() {
    }

    /* compiled from: ProGuard */
    public class HttpStatusException extends Exception {
        public static final String ERROR_INFO = "http status code error:";

        public HttpStatusException(String str) {
            super(str);
        }
    }

    /* compiled from: ProGuard */
    public class NetworkUnavailableException extends Exception {
        public static final String ERROR_INFO = "network unavailable";

        public NetworkUnavailableException(String str) {
            super(str);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0102, code lost:
        r11 = -4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0104, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0105, code lost:
        r15 = r14;
        r14 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x011c, code lost:
        r12 = r7;
        r7 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0127, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0128, code lost:
        r15 = r14;
        r14 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x012a, code lost:
        r14.printStackTrace();
        r11 = -8;
        r9 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0132, code lost:
        if (r13 >= r12) goto L_0x013e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0134, code lost:
        r5 = android.os.SystemClock.elapsedRealtime();
        r7 = r15;
        r14 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x013e, code lost:
        com.tencent.open.b.g.a().a(r4, r5, 0, 0, -8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0145, code lost:
        throw r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0146, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0147, code lost:
        r12 = r3;
        r12.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r11 = java.lang.Integer.parseInt(r12.getMessage().replace(com.tencent.open.utils.HttpUtils.HttpStatusException.ERROR_INFO, com.tencent.connect.common.Constants.STR_EMPTY));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0167, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0168, code lost:
        r3.printStackTrace();
        r11 = -9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x016e, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x016f, code lost:
        r3.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0172, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0173, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0174, code lost:
        r12 = r3;
        r12.printStackTrace();
        com.tencent.open.b.g.a().a(r4, r5, 0, 0, -3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0184, code lost:
        throw r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0185, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0186, code lost:
        r12 = r3;
        r12.printStackTrace();
        com.tencent.open.b.g.a().a(r4, r5, 0, 0, getErrorCodeFromException(r12));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0199, code lost:
        throw r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x019a, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x019b, code lost:
        r12 = r3;
        r12.printStackTrace();
        com.tencent.open.b.g.a().a(r4, r5, 0, 0, -4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01ab, code lost:
        throw r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x01ac, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x01ad, code lost:
        r14 = r7;
        r15 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x01b1, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01b2, code lost:
        r14 = r7;
        r15 = r8;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0104 A[ExcHandler: ConnectTimeoutException (r7v21 'e' org.apache.http.conn.ConnectTimeoutException A[CUSTOM_DECLARE]), Splitter:B:11:0x00ee] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0127 A[ExcHandler: SocketTimeoutException (r7v20 'e' java.net.SocketTimeoutException A[CUSTOM_DECLARE]), Splitter:B:11:0x00ee] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0134  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0146 A[ExcHandler: HttpStatusException (r3v12 'e' com.tencent.open.utils.HttpUtils$HttpStatusException A[CUSTOM_DECLARE]), Splitter:B:8:0x00e2] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x016e A[ExcHandler: NetworkUnavailableException (r3v11 'e' com.tencent.open.utils.HttpUtils$NetworkUnavailableException A[CUSTOM_DECLARE]), Splitter:B:8:0x00e2] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0173 A[ExcHandler: MalformedURLException (r3v9 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:8:0x00e2] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0185 A[ExcHandler: IOException (r3v7 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:8:0x00e2] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x019a A[ExcHandler: JSONException (r3v5 'e' org.json.JSONException A[CUSTOM_DECLARE]), Splitter:B:8:0x00e2] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x01b6 A[LOOP:0: B:7:0x00da->B:61:0x01b6, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x011c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x013e A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.json.JSONObject request(com.tencent.connect.auth.QQToken r18, android.content.Context r19, java.lang.String r20, android.os.Bundle r21, java.lang.String r22) {
        /*
            java.lang.String r3 = com.tencent.open.a.n.d
            java.lang.String r4 = "OpenApi request"
            com.tencent.open.a.n.a(r3, r4)
            java.lang.String r3 = r20.toLowerCase()
            java.lang.String r4 = "http"
            boolean r3 = r3.startsWith(r4)
            if (r3 != 0) goto L_0x01ba
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            com.tencent.open.utils.ServerSetting r4 = com.tencent.open.utils.ServerSetting.getInstance()
            java.lang.String r5 = "https://openmobile.qq.com/"
            r0 = r19
            java.lang.String r4 = r4.getEnvUrl(r0, r5)
            java.lang.StringBuilder r3 = r3.append(r4)
            r0 = r20
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            com.tencent.open.utils.ServerSetting r5 = com.tencent.open.utils.ServerSetting.getInstance()
            java.lang.String r6 = "https://openmobile.qq.com/"
            r0 = r19
            java.lang.String r5 = r5.getEnvUrl(r0, r6)
            java.lang.StringBuilder r4 = r4.append(r5)
            r0 = r20
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r4 = r4.toString()
        L_0x0051:
            r0 = r19
            r1 = r18
            r2 = r20
            a(r0, r1, r2)
            r9 = 0
            long r7 = android.os.SystemClock.elapsedRealtime()
            r6 = 0
            java.lang.String r5 = r18.getAppId()
            r0 = r19
            com.tencent.open.utils.OpenConfig r5 = com.tencent.open.utils.OpenConfig.getInstance(r0, r5)
            java.lang.String r10 = "Common_HttpRetryCount"
            int r5 = r5.getInt(r10)
            java.lang.String r10 = "OpenConfig_test"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "config 1:Common_HttpRetryCount            config_value:"
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r5)
            java.lang.String r12 = "   appid:"
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.String r12 = r18.getAppId()
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.String r12 = "     url:"
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r4)
            java.lang.String r11 = r11.toString()
            com.tencent.open.a.n.b(r10, r11)
            if (r5 != 0) goto L_0x00ff
            r5 = 3
            r12 = r5
        L_0x00a4:
            java.lang.String r5 = "OpenConfig_test"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "config 1:Common_HttpRetryCount            result_value:"
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r12)
            java.lang.String r11 = "   appid:"
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r11 = r18.getAppId()
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r11 = "     url:"
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r4)
            java.lang.String r10 = r10.toString()
            com.tencent.open.a.n.b(r5, r10)
            r16 = r6
            r5 = r7
            r7 = r16
            r8 = r9
        L_0x00da:
            int r13 = r7 + 1
            r0 = r19
            r1 = r22
            r2 = r21
            com.tencent.open.utils.Util$Statistic r9 = openUrl2(r0, r3, r1, r2)     // Catch:{ ConnectTimeoutException -> 0x01b1, SocketTimeoutException -> 0x01ac, HttpStatusException -> 0x0146, NetworkUnavailableException -> 0x016e, MalformedURLException -> 0x0173, IOException -> 0x0185, JSONException -> 0x019a }
            java.lang.String r7 = r9.response     // Catch:{ ConnectTimeoutException -> 0x01b1, SocketTimeoutException -> 0x01ac, HttpStatusException -> 0x0146, NetworkUnavailableException -> 0x016e, MalformedURLException -> 0x0173, IOException -> 0x0185, JSONException -> 0x019a }
            org.json.JSONObject r14 = com.tencent.open.utils.Util.parseJson(r7)     // Catch:{ ConnectTimeoutException -> 0x01b1, SocketTimeoutException -> 0x01ac, HttpStatusException -> 0x0146, NetworkUnavailableException -> 0x016e, MalformedURLException -> 0x0173, IOException -> 0x0185, JSONException -> 0x019a }
            java.lang.String r7 = "ret"
            int r11 = r14.getInt(r7)     // Catch:{ JSONException -> 0x0101, ConnectTimeoutException -> 0x0104, SocketTimeoutException -> 0x0127, HttpStatusException -> 0x0146, NetworkUnavailableException -> 0x016e, MalformedURLException -> 0x0173, IOException -> 0x0185 }
        L_0x00f2:
            long r7 = r9.reqSize     // Catch:{ ConnectTimeoutException -> 0x0104, SocketTimeoutException -> 0x0127, HttpStatusException -> 0x0146, NetworkUnavailableException -> 0x016e, MalformedURLException -> 0x0173, IOException -> 0x0185, JSONException -> 0x019a }
            long r9 = r9.rspSize     // Catch:{ ConnectTimeoutException -> 0x0104, SocketTimeoutException -> 0x0127, HttpStatusException -> 0x0146, NetworkUnavailableException -> 0x016e, MalformedURLException -> 0x0173, IOException -> 0x0185, JSONException -> 0x019a }
            r12 = r14
        L_0x00f7:
            com.tencent.open.b.g r3 = com.tencent.open.b.g.a()
            r3.a(r4, r5, r7, r9, r11)
            return r12
        L_0x00ff:
            r12 = r5
            goto L_0x00a4
        L_0x0101:
            r7 = move-exception
            r11 = -4
            goto L_0x00f2
        L_0x0104:
            r7 = move-exception
            r15 = r14
            r14 = r7
        L_0x0107:
            r14.printStackTrace()
            r11 = -7
            r7 = 0
            r9 = 0
            if (r13 >= r12) goto L_0x011f
            long r5 = android.os.SystemClock.elapsedRealtime()
            r16 = r7
            r7 = r15
            r14 = r16
        L_0x011a:
            if (r13 < r12) goto L_0x01b6
            r12 = r7
            r7 = r14
            goto L_0x00f7
        L_0x011f:
            com.tencent.open.b.g r3 = com.tencent.open.b.g.a()
            r3.a(r4, r5, r7, r9, r11)
            throw r14
        L_0x0127:
            r7 = move-exception
            r15 = r14
            r14 = r7
        L_0x012a:
            r14.printStackTrace()
            r11 = -8
            r7 = 0
            r9 = 0
            if (r13 >= r12) goto L_0x013e
            long r5 = android.os.SystemClock.elapsedRealtime()
            r16 = r7
            r7 = r15
            r14 = r16
            goto L_0x011a
        L_0x013e:
            com.tencent.open.b.g r3 = com.tencent.open.b.g.a()
            r3.a(r4, r5, r7, r9, r11)
            throw r14
        L_0x0146:
            r3 = move-exception
            r12 = r3
            r12.printStackTrace()
            java.lang.String r3 = r12.getMessage()
            java.lang.String r7 = "http status code error:"
            java.lang.String r8 = ""
            java.lang.String r3 = r3.replace(r7, r8)     // Catch:{ Exception -> 0x0167 }
            int r11 = java.lang.Integer.parseInt(r3)     // Catch:{ Exception -> 0x0167 }
        L_0x015b:
            r7 = 0
            r9 = 0
            com.tencent.open.b.g r3 = com.tencent.open.b.g.a()
            r3.a(r4, r5, r7, r9, r11)
            throw r12
        L_0x0167:
            r3 = move-exception
            r3.printStackTrace()
            r11 = -9
            goto L_0x015b
        L_0x016e:
            r3 = move-exception
            r3.printStackTrace()
            throw r3
        L_0x0173:
            r3 = move-exception
            r12 = r3
            r12.printStackTrace()
            r11 = -3
            r7 = 0
            r9 = 0
            com.tencent.open.b.g r3 = com.tencent.open.b.g.a()
            r3.a(r4, r5, r7, r9, r11)
            throw r12
        L_0x0185:
            r3 = move-exception
            r12 = r3
            r12.printStackTrace()
            int r11 = getErrorCodeFromException(r12)
            r7 = 0
            r9 = 0
            com.tencent.open.b.g r3 = com.tencent.open.b.g.a()
            r3.a(r4, r5, r7, r9, r11)
            throw r12
        L_0x019a:
            r3 = move-exception
            r12 = r3
            r12.printStackTrace()
            r11 = -4
            r7 = 0
            r9 = 0
            com.tencent.open.b.g r3 = com.tencent.open.b.g.a()
            r3.a(r4, r5, r7, r9, r11)
            throw r12
        L_0x01ac:
            r7 = move-exception
            r14 = r7
            r15 = r8
            goto L_0x012a
        L_0x01b1:
            r7 = move-exception
            r14 = r7
            r15 = r8
            goto L_0x0107
        L_0x01b6:
            r8 = r7
            r7 = r13
            goto L_0x00da
        L_0x01ba:
            r4 = r20
            r3 = r20
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.utils.HttpUtils.request(com.tencent.connect.auth.QQToken, android.content.Context, java.lang.String, android.os.Bundle, java.lang.String):org.json.JSONObject");
    }

    public static void requestAsync(QQToken qQToken, Context context, String str, Bundle bundle, String str2, IRequestListener iRequestListener) {
        n.a(n.d, "OpenApi requestAsync");
        final QQToken qQToken2 = qQToken;
        final Context context2 = context;
        final String str3 = str;
        final Bundle bundle2 = bundle;
        final String str4 = str2;
        final IRequestListener iRequestListener2 = iRequestListener;
        new Thread() {
            public void run() {
                try {
                    JSONObject request = HttpUtils.request(QQToken.this, context2, str3, bundle2, str4);
                    if (iRequestListener2 != null) {
                        iRequestListener2.onComplete(request);
                        n.b(n.d, "OpenApi onComplete");
                    }
                } catch (MalformedURLException e2) {
                    if (iRequestListener2 != null) {
                        iRequestListener2.onMalformedURLException(e2);
                        n.b(n.d, "OpenApi requestAsync MalformedURLException", e2);
                    }
                } catch (ConnectTimeoutException e3) {
                    if (iRequestListener2 != null) {
                        iRequestListener2.onConnectTimeoutException(e3);
                        n.b(n.d, "OpenApi requestAsync onConnectTimeoutException", e3);
                    }
                } catch (SocketTimeoutException e4) {
                    if (iRequestListener2 != null) {
                        iRequestListener2.onSocketTimeoutException(e4);
                        n.b(n.d, "OpenApi requestAsync onSocketTimeoutException", e4);
                    }
                } catch (NetworkUnavailableException e5) {
                    if (iRequestListener2 != null) {
                        iRequestListener2.onNetworkUnavailableException(e5);
                        n.b(n.d, "OpenApi requestAsync onNetworkUnavailableException", e5);
                    }
                } catch (HttpStatusException e6) {
                    if (iRequestListener2 != null) {
                        iRequestListener2.onHttpStatusException(e6);
                        n.b(n.d, "OpenApi requestAsync onHttpStatusException", e6);
                    }
                } catch (IOException e7) {
                    if (iRequestListener2 != null) {
                        iRequestListener2.onIOException(e7);
                        n.b(n.d, "OpenApi requestAsync IOException", e7);
                    }
                } catch (JSONException e8) {
                    if (iRequestListener2 != null) {
                        iRequestListener2.onJSONException(e8);
                        n.b(n.d, "OpenApi requestAsync JSONException", e8);
                    }
                } catch (Exception e9) {
                    if (iRequestListener2 != null) {
                        iRequestListener2.onUnknowException(e9);
                        n.b(n.d, "OpenApi requestAsync onUnknowException", e9);
                    }
                }
            }
        }.start();
    }

    private static void a(Context context, QQToken qQToken, String str) {
        if (str.indexOf("add_share") > -1 || str.indexOf("upload_pic") > -1 || str.indexOf("add_topic") > -1 || str.indexOf("set_user_face") > -1 || str.indexOf("add_t") > -1 || str.indexOf("add_pic_t") > -1 || str.indexOf("add_pic_url") > -1 || str.indexOf("add_video") > -1) {
            a.a(context, qQToken, "requireApi", str);
        }
    }

    public static int getErrorCodeFromException(IOException iOException) {
        if (iOException instanceof CharConversionException) {
            return -20;
        }
        if (iOException instanceof MalformedInputException) {
            return -21;
        }
        if (iOException instanceof UnmappableCharacterException) {
            return -22;
        }
        if (iOException instanceof HttpResponseException) {
            return -23;
        }
        if (iOException instanceof ClosedChannelException) {
            return -24;
        }
        if (iOException instanceof ConnectionClosedException) {
            return -25;
        }
        if (iOException instanceof EOFException) {
            return -26;
        }
        if (iOException instanceof FileLockInterruptionException) {
            return -27;
        }
        if (iOException instanceof FileNotFoundException) {
            return -28;
        }
        if (iOException instanceof HttpRetryException) {
            return -29;
        }
        if (iOException instanceof ConnectTimeoutException) {
            return -7;
        }
        if (iOException instanceof SocketTimeoutException) {
            return -8;
        }
        if (iOException instanceof InvalidPropertiesFormatException) {
            return -30;
        }
        if (iOException instanceof MalformedChunkCodingException) {
            return -31;
        }
        if (iOException instanceof MalformedURLException) {
            return -3;
        }
        if (iOException instanceof NoHttpResponseException) {
            return -32;
        }
        if (iOException instanceof InvalidClassException) {
            return -33;
        }
        if (iOException instanceof InvalidObjectException) {
            return -34;
        }
        if (iOException instanceof NotActiveException) {
            return -35;
        }
        if (iOException instanceof NotSerializableException) {
            return -36;
        }
        if (iOException instanceof OptionalDataException) {
            return -37;
        }
        if (iOException instanceof StreamCorruptedException) {
            return -38;
        }
        if (iOException instanceof WriteAbortedException) {
            return -39;
        }
        if (iOException instanceof ProtocolException) {
            return -40;
        }
        if (iOException instanceof SSLHandshakeException) {
            return -41;
        }
        if (iOException instanceof SSLKeyException) {
            return -42;
        }
        if (iOException instanceof SSLPeerUnverifiedException) {
            return -43;
        }
        if (iOException instanceof SSLProtocolException) {
            return -44;
        }
        if (iOException instanceof BindException) {
            return -45;
        }
        if (iOException instanceof ConnectException) {
            return -46;
        }
        if (iOException instanceof NoRouteToHostException) {
            return -47;
        }
        if (iOException instanceof PortUnreachableException) {
            return -48;
        }
        if (iOException instanceof SyncFailedException) {
            return -49;
        }
        if (iOException instanceof UTFDataFormatException) {
            return -50;
        }
        if (iOException instanceof UnknownHostException) {
            return -51;
        }
        if (iOException instanceof UnknownServiceException) {
            return -52;
        }
        if (iOException instanceof UnsupportedEncodingException) {
            return -53;
        }
        if (iOException instanceof ZipException) {
            return -54;
        }
        return -2;
    }

    public static Util.Statistic openUrl2(Context context, String str, String str2, Bundle bundle) {
        Bundle bundle2;
        HttpUriRequest httpUriRequest;
        int i;
        String str3;
        ConnectivityManager connectivityManager;
        NetworkInfo activeNetworkInfo;
        if (context == null || (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) == null || ((activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) != null && activeNetworkInfo.isAvailable())) {
            if (bundle != null) {
                bundle2 = new Bundle(bundle);
            } else {
                bundle2 = new Bundle();
            }
            String string = bundle2.getString("appid_for_getting_config");
            bundle2.remove("appid_for_getting_config");
            HttpClient httpClient = getHttpClient(context, string, str);
            if (str2.equals(Constants.HTTP_GET)) {
                String encodeUrl = encodeUrl(bundle2);
                int length = 0 + encodeUrl.length();
                n.b(f3784a, "-->openUrl2 before url =" + str);
                if (str.indexOf("?") == -1) {
                    str3 = str + "?";
                } else {
                    str3 = str + "&";
                }
                n.b(f3784a, "-->openUrl2 encodedParam =" + encodeUrl + " -- url = " + str3);
                HttpUriRequest httpGet = new HttpGet(str3 + encodeUrl);
                httpGet.addHeader("Accept-Encoding", "gzip");
                int i2 = length;
                httpUriRequest = httpGet;
                i = i2;
            } else if (str2.equals(Constants.HTTP_POST)) {
                HttpUriRequest httpPost = new HttpPost(str);
                httpPost.addHeader("Accept-Encoding", "gzip");
                Bundle bundle3 = new Bundle();
                for (String next : bundle2.keySet()) {
                    Object obj = bundle2.get(next);
                    if (obj instanceof byte[]) {
                        bundle3.putByteArray(next, (byte[]) obj);
                    }
                }
                if (!bundle2.containsKey("method")) {
                    bundle2.putString("method", str2);
                }
                httpPost.setHeader("Content-Type", "multipart/form-data; boundary=3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f");
                httpPost.setHeader("Connection", "Keep-Alive");
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byteArrayOutputStream.write(Util.getBytesUTF8("--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n"));
                byteArrayOutputStream.write(Util.getBytesUTF8(encodePostBody(bundle2, "3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f")));
                if (!bundle3.isEmpty()) {
                    int size = bundle3.size();
                    byteArrayOutputStream.write(Util.getBytesUTF8("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n"));
                    int i3 = -1;
                    for (String next2 : bundle3.keySet()) {
                        i3++;
                        byteArrayOutputStream.write(Util.getBytesUTF8("Content-Disposition: form-data; name=\"" + next2 + "\"; filename=\"" + next2 + "\"" + "\r\n"));
                        byteArrayOutputStream.write(Util.getBytesUTF8("Content-Type: content/unknown\r\n\r\n"));
                        byte[] byteArray = bundle3.getByteArray(next2);
                        if (byteArray != null) {
                            byteArrayOutputStream.write(byteArray);
                        }
                        if (i3 < size - 1) {
                            byteArrayOutputStream.write(Util.getBytesUTF8("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n"));
                        }
                    }
                }
                byteArrayOutputStream.write(Util.getBytesUTF8("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f--\r\n"));
                byte[] byteArray2 = byteArrayOutputStream.toByteArray();
                i = byteArray2.length + 0;
                byteArrayOutputStream.close();
                httpPost.setEntity(new ByteArrayEntity(byteArray2));
                httpUriRequest = httpPost;
            } else {
                httpUriRequest = null;
                i = 0;
            }
            HttpResponse execute = httpClient.execute(httpUriRequest);
            int statusCode = execute.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                return new Util.Statistic(a(execute), i);
            }
            throw new HttpStatusException(HttpStatusException.ERROR_INFO + statusCode);
        }
        throw new NetworkUnavailableException(NetworkUnavailableException.ERROR_INFO);
    }

    private static String a(HttpResponse httpResponse) {
        InputStream inputStream;
        InputStream content = httpResponse.getEntity().getContent();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Header firstHeader = httpResponse.getFirstHeader("Content-Encoding");
        if (firstHeader == null || firstHeader.getValue().toLowerCase().indexOf("gzip") <= -1) {
            inputStream = content;
        } else {
            inputStream = new GZIPInputStream(content);
        }
        byte[] bArr = new byte[Process.PROC_PARENS];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return new String(byteArrayOutputStream.toByteArray(), "UTF-8");
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    public static HttpClient getHttpClient(Context context, String str, String str2) {
        OpenConfig openConfig;
        int i;
        int i2 = 0;
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        if (Build.VERSION.SDK_INT < 16) {
            try {
                KeyStore instance = KeyStore.getInstance(KeyStore.getDefaultType());
                instance.load(null, null);
                CustomSSLSocketFactory customSSLSocketFactory = new CustomSSLSocketFactory(instance);
                customSSLSocketFactory.setHostnameVerifier(SSLSocketFactory.STRICT_HOSTNAME_VERIFIER);
                schemeRegistry.register(new Scheme("https", customSSLSocketFactory, 443));
            } catch (Exception e) {
                schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
            }
        } else {
            schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
        }
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        if (context != null) {
            openConfig = OpenConfig.getInstance(context, str);
        } else {
            openConfig = null;
        }
        if (openConfig != null) {
            i = openConfig.getInt("Common_HttpConnectionTimeout");
            i2 = openConfig.getInt("Common_SocketConnectionTimeout");
        } else {
            i = 0;
        }
        if (i == 0) {
            i = 15000;
        }
        if (i2 == 0) {
            i2 = 30000;
        }
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, i);
        HttpConnectionParams.setSoTimeout(basicHttpParams, i2);
        HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(basicHttpParams, "UTF-8");
        HttpProtocolParams.setUserAgent(basicHttpParams, "AndroidSDK_" + Build.VERSION.SDK + "_" + Build.DEVICE + "_" + Build.VERSION.RELEASE);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
        NetworkProxy proxy = getProxy(context);
        if (proxy != null) {
            defaultHttpClient.getParams().setParameter("http.route.default-proxy", new HttpHost(proxy.host, proxy.port));
        }
        return defaultHttpClient;
    }

    public static String encodeUrl(Bundle bundle) {
        if (bundle == null) {
            return Constants.STR_EMPTY;
        }
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (String next : bundle.keySet()) {
            Object obj = bundle.get(next);
            if ((obj instanceof String) || (obj instanceof String[])) {
                if (obj instanceof String[]) {
                    if (z) {
                        z = false;
                    } else {
                        sb.append("&");
                    }
                    sb.append(URLEncoder.encode(next) + "=");
                    String[] stringArray = bundle.getStringArray(next);
                    if (stringArray != null) {
                        for (int i = 0; i < stringArray.length; i++) {
                            if (i == 0) {
                                sb.append(URLEncoder.encode(stringArray[i]));
                            } else {
                                sb.append(URLEncoder.encode("," + stringArray[i]));
                            }
                        }
                    }
                } else {
                    if (z) {
                        z = false;
                    } else {
                        sb.append("&");
                    }
                    sb.append(URLEncoder.encode(next) + "=" + URLEncoder.encode(bundle.getString(next)));
                }
                z = z;
            }
        }
        return sb.toString();
    }

    public static String encodePostBody(Bundle bundle, String str) {
        if (bundle == null) {
            return Constants.STR_EMPTY;
        }
        StringBuilder sb = new StringBuilder();
        int size = bundle.size();
        int i = -1;
        for (String next : bundle.keySet()) {
            int i2 = i + 1;
            Object obj = bundle.get(next);
            if (!(obj instanceof String)) {
                i = i2;
            } else {
                sb.append("Content-Disposition: form-data; name=\"" + next + "\"" + "\r\n" + "\r\n" + ((String) obj));
                if (i2 < size - 1) {
                    sb.append("\r\n--" + str + "\r\n");
                }
                i = i2;
            }
        }
        return sb.toString();
    }

    public static NetworkProxy getProxy(Context context) {
        if (context == null) {
            return null;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return null;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return null;
        }
        if (activeNetworkInfo.getType() == 0) {
            String b = b(context);
            int a2 = a(context);
            if (!TextUtils.isEmpty(b) && a2 >= 0) {
                return new NetworkProxy(b, a2);
            }
        }
        return null;
    }

    /* compiled from: ProGuard */
    public class NetworkProxy {
        public final String host;
        public final int port;

        private NetworkProxy(String str, int i) {
            this.host = str;
            this.port = i;
        }
    }

    private static int a(Context context) {
        if (Build.VERSION.SDK_INT >= 11) {
            String property = System.getProperty("http.proxyPort");
            if (TextUtils.isEmpty(property)) {
                return -1;
            }
            try {
                return Integer.parseInt(property);
            } catch (NumberFormatException e) {
                return -1;
            }
        } else if (context == null) {
            return Proxy.getDefaultPort();
        } else {
            int port = Proxy.getPort(context);
            if (port < 0) {
                return Proxy.getDefaultPort();
            }
            return port;
        }
    }

    private static String b(Context context) {
        if (Build.VERSION.SDK_INT >= 11) {
            return System.getProperty("http.proxyHost");
        }
        if (context == null) {
            return Proxy.getDefaultHost();
        }
        String host = Proxy.getHost(context);
        if (TextUtils.isEmpty(host)) {
            return Proxy.getDefaultHost();
        }
        return host;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00fb, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00fc, code lost:
        r15 = r14;
        r14 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0113, code lost:
        r12 = r7;
        r7 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x011e, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x011f, code lost:
        r15 = r14;
        r14 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0121, code lost:
        r14.printStackTrace();
        r11 = -8;
        r9 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0129, code lost:
        if (r13 >= r12) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x012b, code lost:
        r5 = android.os.SystemClock.elapsedRealtime();
        r7 = r15;
        r14 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0135, code lost:
        com.tencent.open.b.g.a().a(r4, r5, 0, 0, -8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x013c, code lost:
        throw r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x013d, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x013e, code lost:
        r12 = r3;
        r12.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r11 = java.lang.Integer.parseInt(r12.getMessage().replace(com.tencent.open.utils.HttpUtils.HttpStatusException.ERROR_INFO, com.tencent.connect.common.Constants.STR_EMPTY));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x015e, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x015f, code lost:
        r3.printStackTrace();
        r11 = -9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0165, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0166, code lost:
        r3.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0169, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x016a, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x016b, code lost:
        r12 = r3;
        r12.printStackTrace();
        com.tencent.open.b.g.a().a(r4, r5, 0, 0, -3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x017b, code lost:
        throw r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x017c, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x017d, code lost:
        r12 = r3;
        r12.printStackTrace();
        com.tencent.open.b.g.a().a(r4, r5, 0, 0, getErrorCodeFromException(r12));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0190, code lost:
        throw r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0191, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0192, code lost:
        r12 = r3;
        r12.printStackTrace();
        com.tencent.open.b.g.a().a(r4, r5, 0, 0, -4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01a2, code lost:
        throw r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x01a3, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x01a4, code lost:
        r14 = r7;
        r15 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x01a8, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01a9, code lost:
        r14 = r7;
        r15 = r8;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00fb A[ExcHandler: ConnectTimeoutException (r7v21 'e' org.apache.http.conn.ConnectTimeoutException A[CUSTOM_DECLARE]), Splitter:B:11:0x00e5] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x011e A[ExcHandler: SocketTimeoutException (r7v20 'e' java.net.SocketTimeoutException A[CUSTOM_DECLARE]), Splitter:B:11:0x00e5] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x012b  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x013d A[ExcHandler: HttpStatusException (r3v11 'e' com.tencent.open.utils.HttpUtils$HttpStatusException A[CUSTOM_DECLARE]), Splitter:B:8:0x00d9] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0165 A[ExcHandler: NetworkUnavailableException (r3v10 'e' com.tencent.open.utils.HttpUtils$NetworkUnavailableException A[CUSTOM_DECLARE]), Splitter:B:8:0x00d9] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x016a A[ExcHandler: MalformedURLException (r3v8 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:8:0x00d9] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x017c A[ExcHandler: IOException (r3v6 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:8:0x00d9] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0191 A[ExcHandler: JSONException (r3v4 'e' org.json.JSONException A[CUSTOM_DECLARE]), Splitter:B:8:0x00d9] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x01ad A[LOOP:0: B:7:0x00d3->B:61:0x01ad, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0113 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0135 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.json.JSONObject upload(com.tencent.connect.auth.QQToken r18, android.content.Context r19, java.lang.String r20, android.os.Bundle r21) {
        /*
            java.lang.String r3 = r20.toLowerCase()
            java.lang.String r4 = "http"
            boolean r3 = r3.startsWith(r4)
            if (r3 != 0) goto L_0x01b1
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            com.tencent.open.utils.ServerSetting r4 = com.tencent.open.utils.ServerSetting.getInstance()
            java.lang.String r5 = "https://openmobile.qq.com/"
            r0 = r19
            java.lang.String r4 = r4.getEnvUrl(r0, r5)
            java.lang.StringBuilder r3 = r3.append(r4)
            r0 = r20
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            com.tencent.open.utils.ServerSetting r5 = com.tencent.open.utils.ServerSetting.getInstance()
            java.lang.String r6 = "https://openmobile.qq.com/"
            r0 = r19
            java.lang.String r5 = r5.getEnvUrl(r0, r6)
            java.lang.StringBuilder r4 = r4.append(r5)
            r0 = r20
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r4 = r4.toString()
        L_0x004a:
            r0 = r19
            r1 = r18
            r2 = r20
            a(r0, r1, r2)
            r9 = 0
            long r7 = android.os.SystemClock.elapsedRealtime()
            r6 = 0
            java.lang.String r5 = r18.getAppId()
            r0 = r19
            com.tencent.open.utils.OpenConfig r5 = com.tencent.open.utils.OpenConfig.getInstance(r0, r5)
            java.lang.String r10 = "Common_HttpRetryCount"
            int r5 = r5.getInt(r10)
            java.lang.String r10 = "OpenConfig_test"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "config 1:Common_HttpRetryCount            config_value:"
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r5)
            java.lang.String r12 = "   appid:"
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.String r12 = r18.getAppId()
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.String r12 = "     url:"
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r4)
            java.lang.String r11 = r11.toString()
            com.tencent.open.a.n.b(r10, r11)
            if (r5 != 0) goto L_0x00f6
            r5 = 3
            r12 = r5
        L_0x009d:
            java.lang.String r5 = "OpenConfig_test"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "config 1:Common_HttpRetryCount            result_value:"
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r12)
            java.lang.String r11 = "   appid:"
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r11 = r18.getAppId()
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r11 = "     url:"
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r4)
            java.lang.String r10 = r10.toString()
            com.tencent.open.a.n.b(r5, r10)
            r16 = r6
            r5 = r7
            r7 = r16
            r8 = r9
        L_0x00d3:
            int r13 = r7 + 1
            r0 = r19
            r1 = r21
            com.tencent.open.utils.Util$Statistic r9 = com.tencent.open.utils.Util.upload(r0, r3, r1)     // Catch:{ ConnectTimeoutException -> 0x01a8, SocketTimeoutException -> 0x01a3, HttpStatusException -> 0x013d, NetworkUnavailableException -> 0x0165, MalformedURLException -> 0x016a, IOException -> 0x017c, JSONException -> 0x0191 }
            java.lang.String r7 = r9.response     // Catch:{ ConnectTimeoutException -> 0x01a8, SocketTimeoutException -> 0x01a3, HttpStatusException -> 0x013d, NetworkUnavailableException -> 0x0165, MalformedURLException -> 0x016a, IOException -> 0x017c, JSONException -> 0x0191 }
            org.json.JSONObject r14 = com.tencent.open.utils.Util.parseJson(r7)     // Catch:{ ConnectTimeoutException -> 0x01a8, SocketTimeoutException -> 0x01a3, HttpStatusException -> 0x013d, NetworkUnavailableException -> 0x0165, MalformedURLException -> 0x016a, IOException -> 0x017c, JSONException -> 0x0191 }
            java.lang.String r7 = "ret"
            int r11 = r14.getInt(r7)     // Catch:{ JSONException -> 0x00f8, ConnectTimeoutException -> 0x00fb, SocketTimeoutException -> 0x011e, HttpStatusException -> 0x013d, NetworkUnavailableException -> 0x0165, MalformedURLException -> 0x016a, IOException -> 0x017c }
        L_0x00e9:
            long r7 = r9.reqSize     // Catch:{ ConnectTimeoutException -> 0x00fb, SocketTimeoutException -> 0x011e, HttpStatusException -> 0x013d, NetworkUnavailableException -> 0x0165, MalformedURLException -> 0x016a, IOException -> 0x017c, JSONException -> 0x0191 }
            long r9 = r9.rspSize     // Catch:{ ConnectTimeoutException -> 0x00fb, SocketTimeoutException -> 0x011e, HttpStatusException -> 0x013d, NetworkUnavailableException -> 0x0165, MalformedURLException -> 0x016a, IOException -> 0x017c, JSONException -> 0x0191 }
            r12 = r14
        L_0x00ee:
            com.tencent.open.b.g r3 = com.tencent.open.b.g.a()
            r3.a(r4, r5, r7, r9, r11)
            return r12
        L_0x00f6:
            r12 = r5
            goto L_0x009d
        L_0x00f8:
            r7 = move-exception
            r11 = -4
            goto L_0x00e9
        L_0x00fb:
            r7 = move-exception
            r15 = r14
            r14 = r7
        L_0x00fe:
            r14.printStackTrace()
            r11 = -7
            r7 = 0
            r9 = 0
            if (r13 >= r12) goto L_0x0116
            long r5 = android.os.SystemClock.elapsedRealtime()
            r16 = r7
            r7 = r15
            r14 = r16
        L_0x0111:
            if (r13 < r12) goto L_0x01ad
            r12 = r7
            r7 = r14
            goto L_0x00ee
        L_0x0116:
            com.tencent.open.b.g r3 = com.tencent.open.b.g.a()
            r3.a(r4, r5, r7, r9, r11)
            throw r14
        L_0x011e:
            r7 = move-exception
            r15 = r14
            r14 = r7
        L_0x0121:
            r14.printStackTrace()
            r11 = -8
            r7 = 0
            r9 = 0
            if (r13 >= r12) goto L_0x0135
            long r5 = android.os.SystemClock.elapsedRealtime()
            r16 = r7
            r7 = r15
            r14 = r16
            goto L_0x0111
        L_0x0135:
            com.tencent.open.b.g r3 = com.tencent.open.b.g.a()
            r3.a(r4, r5, r7, r9, r11)
            throw r14
        L_0x013d:
            r3 = move-exception
            r12 = r3
            r12.printStackTrace()
            java.lang.String r3 = r12.getMessage()
            java.lang.String r7 = "http status code error:"
            java.lang.String r8 = ""
            java.lang.String r3 = r3.replace(r7, r8)     // Catch:{ Exception -> 0x015e }
            int r11 = java.lang.Integer.parseInt(r3)     // Catch:{ Exception -> 0x015e }
        L_0x0152:
            r7 = 0
            r9 = 0
            com.tencent.open.b.g r3 = com.tencent.open.b.g.a()
            r3.a(r4, r5, r7, r9, r11)
            throw r12
        L_0x015e:
            r3 = move-exception
            r3.printStackTrace()
            r11 = -9
            goto L_0x0152
        L_0x0165:
            r3 = move-exception
            r3.printStackTrace()
            throw r3
        L_0x016a:
            r3 = move-exception
            r12 = r3
            r12.printStackTrace()
            r11 = -3
            r7 = 0
            r9 = 0
            com.tencent.open.b.g r3 = com.tencent.open.b.g.a()
            r3.a(r4, r5, r7, r9, r11)
            throw r12
        L_0x017c:
            r3 = move-exception
            r12 = r3
            r12.printStackTrace()
            int r11 = getErrorCodeFromException(r12)
            r7 = 0
            r9 = 0
            com.tencent.open.b.g r3 = com.tencent.open.b.g.a()
            r3.a(r4, r5, r7, r9, r11)
            throw r12
        L_0x0191:
            r3 = move-exception
            r12 = r3
            r12.printStackTrace()
            r11 = -4
            r7 = 0
            r9 = 0
            com.tencent.open.b.g r3 = com.tencent.open.b.g.a()
            r3.a(r4, r5, r7, r9, r11)
            throw r12
        L_0x01a3:
            r7 = move-exception
            r14 = r7
            r15 = r8
            goto L_0x0121
        L_0x01a8:
            r7 = move-exception
            r14 = r7
            r15 = r8
            goto L_0x00fe
        L_0x01ad:
            r8 = r7
            r7 = r13
            goto L_0x00d3
        L_0x01b1:
            r4 = r20
            r3 = r20
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.utils.HttpUtils.upload(com.tencent.connect.auth.QQToken, android.content.Context, java.lang.String, android.os.Bundle):org.json.JSONObject");
    }

    /* compiled from: ProGuard */
    public class CustomSSLSocketFactory extends SSLSocketFactory {

        /* renamed from: a  reason: collision with root package name */
        private final SSLContext f3786a = SSLContext.getInstance("TLS");

        public CustomSSLSocketFactory(KeyStore keyStore) {
            super(keyStore);
            MyX509TrustManager myX509TrustManager;
            try {
                myX509TrustManager = new MyX509TrustManager();
            } catch (Exception e) {
                myX509TrustManager = null;
            }
            this.f3786a.init(null, new TrustManager[]{myX509TrustManager}, null);
        }

        public Socket createSocket(Socket socket, String str, int i, boolean z) {
            return this.f3786a.getSocketFactory().createSocket(socket, str, i, z);
        }

        public Socket createSocket() {
            return this.f3786a.getSocketFactory().createSocket();
        }
    }

    /* compiled from: ProGuard */
    public class MyX509TrustManager implements X509TrustManager {

        /* renamed from: a  reason: collision with root package name */
        X509TrustManager f3787a;

        MyX509TrustManager() {
            KeyStore keyStore;
            TrustManager[] trustManagers;
            FileInputStream fileInputStream;
            Throwable th;
            try {
                keyStore = KeyStore.getInstance("JKS");
            } catch (Exception e) {
                keyStore = null;
            }
            TrustManager[] trustManagerArr = new TrustManager[0];
            if (keyStore != null) {
                try {
                    FileInputStream fileInputStream2 = new FileInputStream("trustedCerts");
                    try {
                        keyStore.load(fileInputStream2, "passphrase".toCharArray());
                        TrustManagerFactory instance = TrustManagerFactory.getInstance("SunX509", "SunJSSE");
                        instance.init(keyStore);
                        trustManagers = instance.getTrustManagers();
                        if (fileInputStream2 != null) {
                            fileInputStream2.close();
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        fileInputStream = fileInputStream2;
                    }
                } catch (Throwable th3) {
                    Throwable th4 = th3;
                    fileInputStream = null;
                    th = th4;
                    if (fileInputStream != null) {
                        fileInputStream.close();
                    }
                    throw th;
                }
            } else {
                TrustManagerFactory instance2 = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                instance2.init((KeyStore) null);
                trustManagers = instance2.getTrustManagers();
            }
            for (int i = 0; i < trustManagers.length; i++) {
                if (trustManagers[i] instanceof X509TrustManager) {
                    this.f3787a = (X509TrustManager) trustManagers[i];
                    return;
                }
            }
            throw new Exception("Couldn't initialize");
        }

        public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
            try {
                this.f3787a.checkClientTrusted(x509CertificateArr, str);
            } catch (CertificateException e) {
            }
        }

        public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) {
            try {
                this.f3787a.checkServerTrusted(x509CertificateArr, str);
            } catch (CertificateException e) {
            }
        }

        public X509Certificate[] getAcceptedIssuers() {
            return this.f3787a.getAcceptedIssuers();
        }
    }
}
