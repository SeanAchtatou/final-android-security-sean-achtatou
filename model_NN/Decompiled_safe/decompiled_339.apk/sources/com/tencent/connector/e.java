package com.tencent.connector;

import android.os.Handler;
import android.os.Message;
import com.qq.AppService.AppService;
import com.qq.l.p;
import com.tencent.assistant.model.OnlinePCListItemModel;
import com.tencent.connector.ConnectionActivity;
import com.tencent.wcs.c.b;

/* compiled from: ProGuard */
class e extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ConnectionActivity f3544a;

    e(ConnectionActivity connectionActivity) {
        this.f3544a = connectionActivity;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 10:
                if (this.f3544a.f3509a == ConnectionActivity.Status.WAIT_CONNECTION) {
                    AppService.BusinessConnectionType businessConnectionType = (AppService.BusinessConnectionType) message.obj;
                    if (businessConnectionType == AppService.BusinessConnectionType.QQ) {
                        this.f3544a.E();
                        return;
                    } else if (businessConnectionType == AppService.BusinessConnectionType.QRCODE) {
                        this.f3544a.b(5);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 11:
                if (this.f3544a.f3509a == ConnectionActivity.Status.WAIT_CONNECTION) {
                    AppService.BusinessConnectionType businessConnectionType2 = (AppService.BusinessConnectionType) message.obj;
                    if (businessConnectionType2 == AppService.BusinessConnectionType.QQ) {
                        this.f3544a.E();
                        return;
                    } else if (businessConnectionType2 == AppService.BusinessConnectionType.QRCODE) {
                        this.f3544a.b(5);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 12:
                p.m().b(p.m().p().b(), 0, -1);
                this.f3544a.a((AppService.BusinessConnectionType) message.obj);
                return;
            case 20:
                if (this.f3544a.f3509a == ConnectionActivity.Status.WAIT_CONNECTION) {
                    AppService.BusinessConnectionType businessConnectionType3 = (AppService.BusinessConnectionType) message.obj;
                    if (businessConnectionType3 == AppService.BusinessConnectionType.QQ) {
                        this.f3544a.E();
                        return;
                    } else if (businessConnectionType3 == AppService.BusinessConnectionType.QRCODE) {
                        this.f3544a.b(5);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 30:
                this.f3544a.E();
                return;
            case 31:
                i unused = this.f3544a.q = (i) null;
                OnlinePCListItemModel unused2 = this.f3544a.p = (OnlinePCListItemModel) message.obj;
                this.f3544a.b(this.f3544a.p.b, AppService.K(), AppService.L());
                if (AppService.w()) {
                    AppService.R();
                    if (AppService.x()) {
                        this.f3544a.a(this.f3544a.p.f1629a, AppService.M());
                        return;
                    } else if (AppService.y()) {
                        b.a("invite qq, but long connection disable, reconnect,waiting...");
                        return;
                    } else {
                        b.a("invite qq, but long connection disable, not reconnect...");
                        this.f3544a.y();
                        return;
                    }
                } else {
                    AppService.z();
                    return;
                }
            default:
                return;
        }
    }
}
