package com.tencent.connector;

import com.qq.l.p;
import com.qq.util.k;
import com.tencent.assistant.st.a;
import java.net.URLDecoder;

/* compiled from: ProGuard */
public class m {
    public static int a(String str) {
        if (str == null || str.length() == 0) {
            return 61166;
        }
        if (!str.startsWith("http://ws.sj.qq.com")) {
            return 61167;
        }
        if (c(str) == null) {
            return 61168;
        }
        return 0;
    }

    public static String b(String str) {
        int indexOf;
        int indexOf2;
        if (str != null && (indexOf = str.indexOf("pcname=")) > 0 && (indexOf2 = str.indexOf(38, indexOf)) > 0) {
            return str.substring(indexOf + 7, indexOf2);
        }
        return null;
    }

    public static String c(String str) {
        int indexOf;
        int indexOf2;
        if (str != null && (indexOf = str.indexOf("pcid=")) > 0 && (indexOf2 = str.indexOf(38, indexOf)) > 0) {
            return str.substring(indexOf + 5, indexOf2);
        }
        return null;
    }

    public static int a(int i) {
        if (i == 61169) {
            a.a().b((byte) 5);
            p.m().c(505, 2, 4);
            return 4;
        } else if (i == 61168) {
            a.a().b((byte) 6);
            p.m().c(505, 2, 3);
            return 3;
        } else if (i == 61166) {
            a.a().b((byte) 6);
            p.m().c(505, 2, 1);
            return 3;
        } else if (i == 61167) {
            a.a().b((byte) 6);
            p.m().c(505, 2, 2);
            return 3;
        } else {
            a.a().b((byte) 6);
            p.m().c(505, 2, 1);
            return 3;
        }
    }

    public static String d(String str) {
        String f;
        if (str == null || (f = f(str)) == null) {
            return null;
        }
        return e(f);
    }

    public static String e(String str) {
        String str2 = null;
        if (str == null) {
            return null;
        }
        try {
            str2 = new String(k.b("9O58/AGBy2Zx41%3A53^3D4B".getBytes("UTF-8"), com.qq.util.a.b(str.getBytes("UTF-8"))));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (str2 == null) {
            return str;
        }
        int indexOf = str2.indexOf(" ");
        if (indexOf > 0) {
            return str2.substring(indexOf + 1);
        }
        return str2;
    }

    public static String f(String str) {
        String str2;
        int indexOf;
        int indexOf2;
        if (str == null) {
            return null;
        }
        try {
            str2 = new String(k.b("9O58/AGBy2Zx41%3A53^3D4B".getBytes("UTF-8"), com.qq.util.a.a(URLDecoder.decode(str, "UTF-8"))));
        } catch (Exception e) {
            e.printStackTrace();
            str2 = null;
        }
        if (str2 == null || !str2.startsWith("pcid=") || (indexOf2 = str2.indexOf(38, (indexOf = str2.indexOf("pcid=")))) <= 0) {
            return null;
        }
        return str2.substring(indexOf + 5, indexOf2);
    }
}
