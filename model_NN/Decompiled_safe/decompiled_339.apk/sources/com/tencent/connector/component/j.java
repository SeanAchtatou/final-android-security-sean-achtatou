package com.tencent.connector.component;

import android.view.View;

/* compiled from: ProGuard */
class j implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ContentQQConnectionRequest f3539a;

    j(ContentQQConnectionRequest contentQQConnectionRequest) {
        this.f3539a = contentQQConnectionRequest;
    }

    public void onClick(View view) {
        this.f3539a.denyRequest();
        if (this.f3539a.b != null) {
            this.f3539a.b.finish();
        }
    }
}
