package com.tencent.connector.component;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.connector.ConnectionActivity;

/* compiled from: ProGuard */
public class ContentVersionLowTip extends LinearLayout {
    public static final int PAGE_MOBILE_LOW = 1;
    public static final int PAGE_PC_LOW = 0;

    /* renamed from: a  reason: collision with root package name */
    private Context f3527a;
    /* access modifiers changed from: private */
    public ConnectionActivity b;
    private ImageView c;
    private TextView d;
    private TextView e;
    private Button f;
    private View.OnClickListener g;
    private String h;
    /* access modifiers changed from: private */
    public int i = -1;
    public String pcName;
    public String qrcode;

    public ContentVersionLowTip(Context context) {
        super(context);
        this.f3527a = context;
    }

    public ContentVersionLowTip(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3527a = context;
    }

    public void setHostActivity(ConnectionActivity connectionActivity) {
        this.b = connectionActivity;
    }

    public void setChannelID(String str) {
        this.h = str;
    }

    public void switchPage(int i2) {
        if (i2 != this.i) {
            if (i2 == 0) {
                if (this.c != null) {
                    this.c.setImageResource(R.drawable.low_pc);
                }
                if (this.d != null) {
                    this.d.setText((int) R.string.label_low_pc);
                }
                if (this.e != null) {
                    this.e.setText((int) R.string.label_low_pc_tip);
                }
                if (this.f != null) {
                    this.f.setText((int) R.string.label_low_pc_btn);
                }
            } else if (i2 == 1) {
                if (this.c != null) {
                    this.c.setImageResource(R.drawable.low_mobile);
                }
                if (this.d != null) {
                    this.d.setText((int) R.string.label_low_mobile);
                }
                if (this.e != null) {
                    this.e.setText((int) R.string.label_low_mobile_tip);
                }
                if (this.f != null) {
                    this.f.setText((int) R.string.label_low_mobile_btn);
                }
            }
            this.i = i2;
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        if (this.b != null) {
            Uri parse = Uri.parse("tmast://download?pname=" + "com.tencent.android.qqdownloader" + "&channelid=" + this.h + "&oplist=\"1;2\"");
            Intent intent = new Intent();
            intent.setData(parse);
            intent.addFlags(268435456);
            intent.setAction("android.intent.action.VIEW");
            this.b.startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.c = (ImageView) findViewById(R.id.image_low_pic);
        this.d = (TextView) findViewById(R.id.label_low_version_t1);
        this.e = (TextView) findViewById(R.id.label_low_version_t2);
        this.f = (Button) findViewById(R.id.btn_low_version);
        this.g = new m(this);
        this.f.setOnClickListener(this.g);
    }
}
