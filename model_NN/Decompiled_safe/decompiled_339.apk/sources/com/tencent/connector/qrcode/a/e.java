package com.tencent.connector.qrcode.a;

/* compiled from: ProGuard */
public final class e extends com.google.zxing.e {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f3561a;
    private final int b;
    private final int c;
    private final int d;
    private final int e;

    public e(byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6) {
        super(i5, i6);
        if (i3 + i5 > i || i4 + i6 > i2) {
            throw new IllegalArgumentException("Crop rectangle does not fit within image data.");
        }
        this.f3561a = bArr;
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.e = i4;
    }

    public byte[] a(int i, byte[] bArr) {
        if (i < 0 || i >= c()) {
            throw new IllegalArgumentException("Requested row is outside the image: " + i);
        }
        int b2 = b();
        if (bArr == null || bArr.length < b2) {
            bArr = new byte[b2];
        }
        System.arraycopy(this.f3561a, ((this.e + i) * this.b) + this.d, bArr, 0, b2);
        return bArr;
    }

    public byte[] a() {
        int b2 = b();
        int c2 = c();
        if (b2 == this.b && c2 == this.c) {
            return this.f3561a;
        }
        int i = b2 * c2;
        byte[] bArr = new byte[i];
        int i2 = (this.e * this.b) + this.d;
        if (b2 == this.b) {
            System.arraycopy(this.f3561a, i2, bArr, 0, i);
            return bArr;
        }
        byte[] bArr2 = this.f3561a;
        for (int i3 = 0; i3 < c2; i3++) {
            int i4 = i3 * b2;
            if (bArr2 != null && bArr2.length >= i2 + b2 && bArr != null && bArr.length >= i4 + b2) {
                System.arraycopy(bArr2, i2, bArr, i4, b2);
            }
            i2 += this.b;
        }
        return bArr;
    }
}
