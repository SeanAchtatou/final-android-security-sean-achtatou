package com.tencent.connector.qrcode.decoder;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.google.zxing.ReaderException;
import com.google.zxing.c;
import com.google.zxing.common.m;
import com.google.zxing.d;
import com.google.zxing.f;
import com.google.zxing.h;
import com.tencent.android.qqdownloader.R;
import com.tencent.connector.CaptureActivity;
import java.util.Hashtable;

/* compiled from: ProGuard */
public final class b extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private static final String f3567a = b.class.getSimpleName();
    private final CaptureActivity b;
    private final f c = new f();
    private final Hashtable<d, Object> d;

    b(CaptureActivity captureActivity, Hashtable<d, Object> hashtable) {
        this.c.a(hashtable);
        this.d = hashtable;
        this.b = captureActivity;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case R.id.decode /*2131165241*/:
                a((byte[]) message.obj, message.arg1, message.arg2);
                return;
            case R.id.decode_failed /*2131165242*/:
            case R.id.decode_succeeded /*2131165243*/:
            default:
                return;
            case R.id.quit /*2131165244*/:
                Looper.myLooper().quit();
                return;
        }
    }

    private void a(byte[] bArr, int i, int i2) {
        f fVar;
        byte[] bArr2 = new byte[bArr.length];
        for (int i3 = 0; i3 < i2; i3++) {
            for (int i4 = 0; i4 < i; i4++) {
                int i5 = (((i4 * i2) + i2) - i3) - 1;
                int i6 = (i3 * i) + i4;
                if (i5 < bArr.length && i6 < bArr.length) {
                    bArr2[i5] = bArr[i6];
                }
            }
        }
        h hVar = null;
        try {
            hVar = this.c.a(new c(new m(com.tencent.connector.qrcode.a.d.a().a(bArr2, i2, i))));
            fVar = this.c;
        } catch (ReaderException e) {
            fVar = this.c;
        } catch (Throwable th) {
            this.c.a();
            throw th;
        }
        fVar.a();
        if (this.b.b() == null) {
            return;
        }
        if (hVar != null) {
            Message obtain = Message.obtain(this.b.b(), R.id.decode_succeeded, hVar);
            if (Build.MODEL.equals("HTC T528t")) {
                this.b.b().sendMessageDelayed(obtain, 750);
            } else {
                obtain.sendToTarget();
            }
        } else {
            Message.obtain(this.b.b(), (int) R.id.decode_failed).sendToTarget();
        }
    }
}
