package com.tencent.tmsecurelite.c;

import android.os.IInterface;
import com.tencent.tmsecurelite.commom.b;

/* compiled from: ProGuard */
public interface a extends IInterface {
    @Deprecated
    int a();

    @Deprecated
    void a(int i);

    void a(b bVar);

    @Deprecated
    void a(String str);

    @Deprecated
    void a(boolean z);

    int b(int i);

    int b(String str);

    int b(boolean z);

    @Deprecated
    String b();

    void b(b bVar);

    @Deprecated
    int c();

    void c(b bVar);

    @Deprecated
    void c(String str);

    int d(String str);

    @Deprecated
    String d();

    void d(b bVar);

    @Deprecated
    String e();

    void e(b bVar);

    @Deprecated
    void e(String str);

    int f(String str);

    void f(b bVar);

    @Deprecated
    boolean f();
}
