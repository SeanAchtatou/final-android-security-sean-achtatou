package com.tencent.cloud.adapter;

import android.widget.ImageView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.component.y;

/* compiled from: ProGuard */
class b extends y {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f3461a;
    final /* synthetic */ UpdateRecOneMoreAdapter b;

    b(UpdateRecOneMoreAdapter updateRecOneMoreAdapter, SimpleAppModel simpleAppModel) {
        this.b = updateRecOneMoreAdapter;
        this.f3461a = simpleAppModel;
    }

    public void a(DownloadInfo downloadInfo) {
        ImageView imageView = (ImageView) this.b.f.findViewWithTag(downloadInfo.downloadTicket);
        if (downloadInfo != null && downloadInfo.needReCreateInfo(this.f3461a)) {
            DownloadProxy.a().b(downloadInfo.downloadTicket);
            downloadInfo = DownloadInfo.createDownloadInfo(this.f3461a, null);
        }
        a.a().a(downloadInfo);
        com.tencent.assistant.utils.a.a(imageView);
    }
}
