package com.tencent.cloud.smartcard.view;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.protocol.jce.SmartCardPicNode;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class b extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SmartCardPicNode f3467a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ NormalSmartCardPicItemPicNode c;

    b(NormalSmartCardPicItemPicNode normalSmartCardPicItemPicNode, SmartCardPicNode smartCardPicNode, STInfoV2 sTInfoV2) {
        this.c = normalSmartCardPicItemPicNode;
        this.f3467a = smartCardPicNode;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        com.tencent.assistant.link.b.b(this.c.getContext(), this.f3467a.b);
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            return this.b;
        }
        return null;
    }
}
