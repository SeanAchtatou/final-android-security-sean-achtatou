package com.tencent.downloadsdk.network;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.text.TextUtils;
import com.tencent.connect.common.Constants;
import com.tencent.downloadsdk.DownloadManager;
import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;

public class HttpUtils {

    public enum HttpMethod {
        f3617a(0),
        b(1);
        
        private int c = 0;

        private HttpMethod(int i) {
            this.c = i;
        }

        public static HttpMethod a(int i) {
            switch (i) {
                case 0:
                    return f3617a;
                case 1:
                    return b;
                default:
                    return null;
            }
        }
    }

    public static String a(long j, long j2) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("bytes=");
        stringBuffer.append(j);
        stringBuffer.append("-");
        stringBuffer.append((j + j2) - 1);
        return stringBuffer.toString();
    }

    public static String a(String str) {
        return str.replace("\r", Constants.STR_EMPTY).replace("\n", Constants.STR_EMPTY).replace(" ", "%20").trim();
    }

    public static void a(HttpClient httpClient) {
        int i;
        int i2;
        try {
            i = DownloadManager.a().b().checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE");
        } catch (Exception e) {
            e.printStackTrace();
            i = -1;
        }
        if (i == 0) {
            try {
                NetworkInfo activeNetworkInfo = ((ConnectivityManager) DownloadManager.a().b().getSystemService("connectivity")).getActiveNetworkInfo();
                i2 = activeNetworkInfo == null ? 0 : activeNetworkInfo.getType();
            } catch (Throwable th) {
                i2 = 0;
            }
            if (i2 == 0) {
                String defaultHost = Proxy.getDefaultHost();
                int defaultPort = Proxy.getDefaultPort();
                if (!(defaultHost == null || defaultPort == -1)) {
                    httpClient.getParams().setParameter("http.route.default-proxy", new HttpHost(defaultHost, defaultPort));
                    return;
                }
            }
            httpClient.getParams().setParameter("http.route.default-proxy", null);
        }
    }

    public static long b(String str) {
        String[] split = str.split("/");
        if (split == null || split.length != 2) {
            return 0;
        }
        try {
            return Long.valueOf(split[1]).longValue();
        } catch (Exception e) {
            return 0;
        }
    }

    public static String c(String str) {
        if (!TextUtils.isEmpty(str)) {
            int lastIndexOf = str.lastIndexOf("&host=");
            int i = lastIndexOf + 6;
            if (lastIndexOf > -1 && i < str.length() - 1) {
                return str.substring(i);
            }
        }
        return null;
    }

    public static boolean d(String str) {
        return str != null && str.startsWith("text");
    }
}
