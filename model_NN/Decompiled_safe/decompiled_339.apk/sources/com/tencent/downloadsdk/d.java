package com.tencent.downloadsdk;

import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import com.tencent.downloadsdk.a.c;
import com.tencent.downloadsdk.b.b;
import com.tencent.downloadsdk.utils.f;
import com.tencent.downloadsdk.utils.i;
import java.util.HashMap;

class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f3604a;

    d(c cVar) {
        this.f3604a = cVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.downloadsdk.c.a(com.tencent.downloadsdk.c, boolean):boolean
     arg types: [com.tencent.downloadsdk.c, int]
     candidates:
      com.tencent.downloadsdk.c.a(com.tencent.downloadsdk.c, int):int
      com.tencent.downloadsdk.c.a(com.tencent.downloadsdk.network.a, java.lang.String):com.tencent.downloadsdk.h
      com.tencent.downloadsdk.c.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.downloadsdk.c.a(com.tencent.downloadsdk.c, boolean):boolean */
    public void run() {
        if (!this.f3604a.x && this.f3604a.o != null && this.f3604a.B.size() <= 0) {
            if (this.f3604a.y == null || !this.f3604a.y.f3590a) {
                f.d("DownloadScheduler", "checkIfDownloadComplete ok id:" + this.f3604a.q + " mErrCode: " + this.f3604a.v);
                if (this.f3604a.o != null) {
                    String str = Constants.STR_EMPTY;
                    String str2 = Constants.STR_EMPTY;
                    if (this.f3604a.I != null) {
                        str = this.f3604a.I.m + Constants.STR_EMPTY;
                        str2 = this.f3604a.I.l + Constants.STR_EMPTY;
                    }
                    try {
                        this.f3604a.o.a(("2,reportKey=" + this.f3604a.h + ",mErrCode=" + this.f3604a.v + ",mResult=" + str + ",taskResult=" + str2 + ",detect=" + this.f3604a.c + "+" + this.f3604a.d + "+" + this.f3604a.g + "+" + this.f3604a.e + "+" + this.f3604a.f) + this.f3604a.i);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (this.f3604a.v == 0) {
                    if (this.f3604a.y == null || this.f3604a.y.c() != this.f3604a.y.b() || this.f3604a.y.b() == 0) {
                        HashMap hashMap = new HashMap();
                        hashMap.put("B110", this.f3604a.h);
                        hashMap.put("B120", this.f3604a.v + Constants.STR_EMPTY);
                        if (this.f3604a.y == null) {
                            hashMap.put("B121", "mDownloadWriteFile is null");
                        } else if (this.f3604a.y.c() != this.f3604a.y.b()) {
                            hashMap.put("B121", "SavedLength :" + this.f3604a.y.c() + ",TotalLength:" + this.f3604a.y.b());
                        }
                        a.a("TMDownloadSDK", true, 0, 0, hashMap, true);
                    } else {
                        this.f3604a.z.a(this.f3604a.j.get());
                        b.a(this.f3604a.h, this.f3604a.I, this.f3604a.p.b, this.f3604a.n, this.f3604a.f3603a, this.f3604a.f3603a, this.f3604a.z.d, this.f3604a.z.b, this.f3604a.z.c, null, this.f3604a.v, this.f3604a.v);
                        if (this.f3604a.o != null) {
                            try {
                                this.f3604a.o.b();
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }
                        }
                        if (this.f3604a.u != null) {
                            this.f3604a.u.shutdownNow();
                        }
                    }
                } else if (this.f3604a.v == 2 || this.f3604a.G) {
                    this.f3604a.z.a(this.f3604a.j.get());
                    b.a(this.f3604a.h, this.f3604a.I, this.f3604a.p.b, this.f3604a.n, this.f3604a.f3603a, this.f3604a.f3603a, this.f3604a.z.d, this.f3604a.z.b, this.f3604a.z.c, null, 2, 2);
                    this.f3604a.z.a(this.f3604a.j.get());
                    if (this.f3604a.o != null) {
                        try {
                            this.f3604a.o.c();
                        } catch (Exception e3) {
                            e3.printStackTrace();
                        }
                    }
                    if (this.f3604a.u != null) {
                        this.f3604a.u.shutdownNow();
                    }
                } else if (this.f3604a.v == 1 || this.f3604a.F) {
                    b.a(this.f3604a.h, this.f3604a.I, this.f3604a.p.b, this.f3604a.n, this.f3604a.f3603a, this.f3604a.f3603a, this.f3604a.z.d, this.f3604a.z.b, this.f3604a.z.c, null, 1, 2);
                    this.f3604a.z.a(this.f3604a.j.get());
                    if (this.f3604a.o != null) {
                        try {
                            this.f3604a.o.d();
                        } catch (Exception e4) {
                            e4.printStackTrace();
                        }
                    }
                    if (this.f3604a.u != null) {
                        this.f3604a.u.shutdownNow();
                    }
                    if (this.f3604a.y != null) {
                        this.f3604a.y.b(this.f3604a.q);
                    }
                    i.a(this.f3604a.n + ".yyb");
                    i.a(this.f3604a.n);
                } else if (this.f3604a.v < 0) {
                    this.f3604a.z.a(this.f3604a.j.get());
                    b.a(this.f3604a.h, this.f3604a.I, this.f3604a.p.b, this.f3604a.n, this.f3604a.f3603a, this.f3604a.f3603a, this.f3604a.z.d, this.f3604a.z.b, this.f3604a.z.c, this.f3604a.w, this.f3604a.v, 1);
                    if (this.f3604a.o != null) {
                        try {
                            this.f3604a.o.a(this.f3604a.v, this.f3604a.w);
                        } catch (Exception e5) {
                            e5.printStackTrace();
                        }
                    }
                    if (this.f3604a.u != null) {
                        this.f3604a.u.shutdownNow();
                    }
                    if (this.f3604a.v == -19 || this.f3604a.v == -32) {
                        if (this.f3604a.y != null) {
                            this.f3604a.y.b(this.f3604a.q);
                        }
                        i.a(this.f3604a.n + ".yyb");
                        i.a(this.f3604a.n);
                    }
                } else {
                    HashMap hashMap2 = new HashMap();
                    hashMap2.put("B110", this.f3604a.h);
                    hashMap2.put("B120", this.f3604a.v + Constants.STR_EMPTY);
                    a.a("TMDownloadSDK", true, 0, 0, hashMap2, true);
                }
                f.b("DownloadScheduler", "aveSpeed: " + c.b(this.f3604a.z.d()));
                f.b("DownloadScheduler", "costTime: " + this.f3604a.z.d);
                f.b("DownloadScheduler", "mIsDownloadFinished = true");
                boolean unused = this.f3604a.x = true;
            }
        }
    }
}
