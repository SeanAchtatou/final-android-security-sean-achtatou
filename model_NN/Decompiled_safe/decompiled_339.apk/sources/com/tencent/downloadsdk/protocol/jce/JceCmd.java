package com.tencent.downloadsdk.protocol.jce;

import java.io.Serializable;

public final class JceCmd implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final JceCmd f3626a = new JceCmd(0, 0, "Empty");
    public static final JceCmd b = new JceCmd(1, 1, "GetDownloadSetting");
    static final /* synthetic */ boolean c = (!JceCmd.class.desiredAssertionStatus());
    private static JceCmd[] d = new JceCmd[2];
    private int e;
    private String f = new String();

    private JceCmd(int i, int i2, String str) {
        this.f = str;
        this.e = i2;
        d[i] = this;
    }

    public String toString() {
        return this.f;
    }
}
