package com.tencent.downloadsdk.monitor;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

public class d {

    /* renamed from: a  reason: collision with root package name */
    protected ReferenceQueue<e> f3614a = new ReferenceQueue<>();
    protected ConcurrentLinkedQueue<WeakReference<e>> b = new ConcurrentLinkedQueue<>();
    private SdCardEventReceiver c = new SdCardEventReceiver();

    protected d() {
    }

    /* access modifiers changed from: protected */
    public void a() {
        Iterator<WeakReference<e>> it = this.b.iterator();
        while (it.hasNext()) {
            e eVar = (e) it.next().get();
            if (eVar != null) {
                eVar.a();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        Iterator<WeakReference<e>> it = this.b.iterator();
        while (it.hasNext()) {
            e eVar = (e) it.next().get();
            if (eVar != null) {
                eVar.b();
            }
        }
    }
}
