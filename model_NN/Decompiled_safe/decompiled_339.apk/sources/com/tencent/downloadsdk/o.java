package com.tencent.downloadsdk;

import com.qq.taf.jce.JceStruct;
import com.tencent.downloadsdk.protocol.jce.DownloadSetting;
import com.tencent.downloadsdk.protocol.jce.GetSettingsRequest;
import com.tencent.downloadsdk.protocol.jce.GetSettingsResponse;
import com.tencent.downloadsdk.protocol.jce.Response;
import com.tencent.downloadsdk.protocol.jce.SettingsCfg;
import com.tencent.downloadsdk.storage.a.a;
import com.tencent.downloadsdk.storage.a.c;
import com.tencent.downloadsdk.utils.f;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public class o {
    private static o g = null;

    /* renamed from: a  reason: collision with root package name */
    private final String f3621a = "DownloadSettingManager";
    private final long b = 172800000;
    private final String c = "http://masdk.3g.qq.com/";
    private CopyOnWriteArrayList<DownloadSettingInfo> d = new CopyOnWriteArrayList<>();
    private a e;
    private boolean f = false;

    private o() {
    }

    public static synchronized o a() {
        o oVar;
        synchronized (o.class) {
            if (g == null) {
                g = new o();
            }
            oVar = g;
        }
        return oVar;
    }

    private boolean e() {
        boolean z = false;
        if (this.e == null) {
            this.e = new a();
        }
        ArrayList<DownloadSettingInfo> a2 = this.e.a();
        if (a2 != null && a2.size() > 0) {
            this.d.addAll(a2);
            z = true;
        }
        this.f = z;
        return z;
    }

    /* access modifiers changed from: private */
    public boolean f() {
        boolean a2;
        synchronized (this.d) {
            if (this.e == null) {
                this.e = new a();
            }
            a2 = this.e.a(this.d);
        }
        return a2;
    }

    public DownloadSettingInfo b() {
        DownloadSettingInfo downloadSettingInfo;
        synchronized (this.d) {
            int a2 = DownloadSettingInfo.a();
            f.a("DownloadSettingManager", "networkType: " + DownloadSettingInfo.a(a2));
            Iterator<DownloadSettingInfo> it = this.d.iterator();
            while (true) {
                if (!it.hasNext()) {
                    downloadSettingInfo = null;
                    break;
                }
                downloadSettingInfo = it.next();
                if (downloadSettingInfo.b == a2) {
                    break;
                }
            }
            if (downloadSettingInfo == null) {
                downloadSettingInfo = new DownloadSettingInfo(a2);
                this.d.add(downloadSettingInfo);
            }
        }
        return downloadSettingInfo;
    }

    /* access modifiers changed from: protected */
    public void c() {
        if (!this.f) {
            if (!e()) {
                for (int i = 0; i < 5; i++) {
                    this.d.add(new DownloadSettingInfo(i));
                }
                this.f = true;
                f();
            }
            new Thread(new p(this)).start();
        }
    }

    public boolean d() {
        Response a2;
        ArrayList<SettingsCfg> arrayList;
        int size;
        DownloadSetting downloadSetting;
        boolean z = false;
        c cVar = new c();
        if (cVar.a() <= System.currentTimeMillis()) {
            HashMap hashMap = new HashMap();
            hashMap.put("User-Agent", "AssistantDownloader");
            GetSettingsRequest getSettingsRequest = new GetSettingsRequest();
            getSettingsRequest.b = 2;
            if (this.e == null) {
                this.e = new a();
            }
            getSettingsRequest.c = this.e.a(1);
            byte[] bArr = (byte[]) new com.tencent.downloadsdk.network.a().a("http://masdk.3g.qq.com/", new ByteArrayInputStream(com.tencent.downloadsdk.protocol.a.a(com.tencent.downloadsdk.protocol.a.b(getSettingsRequest))), hashMap, false, new q(this)).i;
            if (!(bArr == null || bArr.length <= 4 || (a2 = com.tencent.downloadsdk.protocol.a.a(bArr)) == null || a2.b == null)) {
                JceStruct a3 = com.tencent.downloadsdk.protocol.a.a(getSettingsRequest, a2.b);
                if (a3 instanceof GetSettingsResponse) {
                    GetSettingsResponse getSettingsResponse = (GetSettingsResponse) a3;
                    if (getSettingsResponse.f3625a == 0 && (size = (arrayList = getSettingsResponse.b).size()) > 0) {
                        ArrayList arrayList2 = new ArrayList(size);
                        Iterator<SettingsCfg> it = arrayList.iterator();
                        while (it.hasNext()) {
                            SettingsCfg next = it.next();
                            if (next.f3633a == 2 && (downloadSetting = (DownloadSetting) com.tencent.downloadsdk.protocol.a.a(next.b, DownloadSetting.class)) != null) {
                                arrayList2.add(new DownloadSettingInfo(next.c, downloadSetting));
                            }
                        }
                        if (arrayList2.size() > 0) {
                            z = true;
                        }
                        if (z) {
                            f.b("DownloadSettingManager", "get new settings:" + arrayList2.toString());
                            synchronized (this.d) {
                                this.d.clear();
                                this.d.addAll(arrayList2);
                                this.f = true;
                            }
                        }
                    }
                }
            }
            if (z) {
                cVar.a(System.currentTimeMillis() + 172800000);
            }
        }
        return z;
    }
}
