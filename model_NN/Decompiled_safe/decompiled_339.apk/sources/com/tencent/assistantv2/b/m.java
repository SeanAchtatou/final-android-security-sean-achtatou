package com.tencent.assistantv2.b;

import android.text.TextUtils;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistant.protocol.jce.GetAppHotFriendsRequest;
import com.tencent.assistant.protocol.jce.GetAppHotFriendsResponse;
import com.tencent.assistant.protocol.jce.GetMyFriendCommentCountRequest;
import com.tencent.assistant.protocol.jce.GetMyFriendCommentCountResponse;
import com.tencent.assistant.protocol.jce.GetMyFriendsUsingRequest;
import com.tencent.assistant.protocol.jce.GetMyFriendsUsingResponse;
import com.tencent.assistant.protocol.jce.GetSelectedCommentListRequest;
import com.tencent.assistant.protocol.jce.GetSelectedCommentListResponse;
import com.tencent.assistant.protocol.scu.RequestResponePair;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.model.a.d;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class m extends BaseEngine<d> {

    /* renamed from: a  reason: collision with root package name */
    GetMyFriendsUsingResponse f2963a = null;
    GetAppHotFriendsResponse b = null;
    GetMyFriendCommentCountResponse c = null;
    GetSelectedCommentListResponse d = null;
    public ArrayList<CommentDetail> e = new ArrayList<>();
    private final int f = 4;
    private final int g = 5;
    /* access modifiers changed from: private */
    public int h = 0;

    public int a(SimpleAppModel simpleAppModel, boolean z, long j, String str) {
        ArrayList arrayList;
        GetMyFriendsUsingRequest getMyFriendsUsingRequest = new GetMyFriendsUsingRequest(simpleAppModel.f1634a, simpleAppModel.c);
        GetSelectedCommentListRequest getSelectedCommentListRequest = new GetSelectedCommentListRequest(simpleAppModel.f1634a, simpleAppModel.b, simpleAppModel.c, simpleAppModel.g);
        GetMyFriendCommentCountRequest getMyFriendCommentCountRequest = new GetMyFriendCommentCountRequest(simpleAppModel.f1634a, 4);
        if (z) {
            ArrayList arrayList2 = new ArrayList(4);
            GetAppHotFriendsRequest getAppHotFriendsRequest = new GetAppHotFriendsRequest();
            getAppHotFriendsRequest.b = j;
            getAppHotFriendsRequest.f2081a = str;
            arrayList2.add(getMyFriendsUsingRequest);
            arrayList2.add(getSelectedCommentListRequest);
            arrayList2.add(getMyFriendCommentCountRequest);
            arrayList2.add(getAppHotFriendsRequest);
            arrayList = arrayList2;
        } else {
            ArrayList arrayList3 = new ArrayList(3);
            arrayList3.add(getMyFriendsUsingRequest);
            arrayList3.add(getSelectedCommentListRequest);
            arrayList3.add(getMyFriendCommentCountRequest);
            arrayList = arrayList3;
        }
        return send(arrayList);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, List<RequestResponePair> list) {
        for (RequestResponePair next : list) {
            if (next.response instanceof GetMyFriendsUsingResponse) {
                this.f2963a = (GetMyFriendsUsingResponse) next.response;
            } else if (next.response instanceof GetAppHotFriendsResponse) {
                this.b = (GetAppHotFriendsResponse) next.response;
            } else if (next.response instanceof GetMyFriendCommentCountResponse) {
                this.c = (GetMyFriendCommentCountResponse) next.response;
            } else if (next.response instanceof GetSelectedCommentListResponse) {
                this.d = (GetSelectedCommentListResponse) next.response;
            }
        }
        if (!(this.f2963a == null && this.b == null)) {
            if (this.f2963a == null) {
                String[] split = this.b.b.split("位");
                if (split != null && split.length >= 2) {
                    this.h = Integer.parseInt(split[0]);
                }
                notifyDataChangedInMainThread(new n(this, i));
            } else if (this.b == null) {
                this.h = this.f2963a.b;
                notifyDataChangedInMainThread(new o(this, i));
            } else {
                this.h = this.f2963a.b;
                String[] split2 = this.b.b.split("位");
                if (split2 != null && split2.length >= 2) {
                    if (this.h <= 3) {
                        split2[0] = "这些";
                        this.b.b = split2[0];
                        for (int i2 = 1; i2 < split2.length; i2++) {
                            StringBuilder sb = new StringBuilder();
                            GetAppHotFriendsResponse getAppHotFriendsResponse = this.b;
                            getAppHotFriendsResponse.b = sb.append(getAppHotFriendsResponse.b).append(split2[i2]).toString();
                        }
                    } else {
                        split2[0] = Constants.STR_EMPTY + this.h;
                        this.b.b = split2[0] + "位";
                        for (int i3 = 1; i3 < split2.length; i3++) {
                            StringBuilder sb2 = new StringBuilder();
                            GetAppHotFriendsResponse getAppHotFriendsResponse2 = this.b;
                            getAppHotFriendsResponse2.b = sb2.append(getAppHotFriendsResponse2.b).append(split2[i3]).toString();
                        }
                    }
                }
                notifyDataChangedInMainThread(new p(this, i));
            }
        }
        if (this.d != null) {
            if (this.d.b.size() > 2) {
                int size = this.d.b.size();
                for (int i4 = 2; i4 < size; i4++) {
                    this.d.b.remove(this.d.b.size() - 1);
                }
            }
            Iterator<CommentDetail> it = this.d.b.iterator();
            while (it.hasNext()) {
                CommentDetail next2 = it.next();
                if (TextUtils.isEmpty(next2.c.trim())) {
                    next2.c = MainActivity.i().getResources().getString(R.string.comment_default_nick_name);
                }
                this.e.add(next2);
            }
        }
        if (this.e.size() > 0) {
            ArrayList arrayList = new ArrayList();
            if (this.d.a() != null && this.d.a().size() > 0) {
                Iterator<CommentTagInfo> it2 = this.d.a().iterator();
                while (it2.hasNext()) {
                    CommentTagInfo next3 = it2.next();
                    if (next3.b() == 2) {
                        arrayList.add(next3);
                    }
                }
            }
            notifyDataChangedInMainThread(new q(this, i, arrayList));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, List<RequestResponePair> list) {
    }
}
