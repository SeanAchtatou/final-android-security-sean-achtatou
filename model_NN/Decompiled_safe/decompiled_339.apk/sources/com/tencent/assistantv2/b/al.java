package com.tencent.assistantv2.b;

import com.tencent.assistantv2.manager.MainTabType;

/* compiled from: ProGuard */
public class al {

    /* renamed from: a  reason: collision with root package name */
    public String f2950a;
    public int b;
    public int c;
    public String d;
    public long e;
    public int f;
    public int g;
    public byte h;

    public al() {
    }

    public al(String str, int i, int i2, String str2) {
        this.f2950a = str;
        this.b = i;
        this.c = i2;
        this.d = str2;
    }

    public al(String str, int i, int i2, String str2, long j, int i3, int i4, byte b2) {
        this.f2950a = str;
        this.b = i;
        this.c = i2;
        this.d = str2;
        this.e = j;
        this.f = i3;
        this.g = i4;
        this.h = b2;
    }

    public MainTabType a() {
        if (this.b < 0 || this.b >= MainTabType.values().length) {
            return MainTabType.WEBVIEW;
        }
        return MainTabType.values()[this.b];
    }
}
