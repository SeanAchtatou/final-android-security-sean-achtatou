package com.tencent.assistantv2.manager;

import android.net.Uri;
import android.text.TextUtils;
import com.tencent.assistant.m;

/* compiled from: ProGuard */
public class j {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ i f3279a;
    private int b = 0;
    private boolean c = false;
    private boolean d = false;
    private String e;

    public j(i iVar) {
        this.f3279a = iVar;
        f();
    }

    private void f() {
        a();
        g();
    }

    public void a() {
        this.e = m.a().T();
    }

    private void g() {
        if (!TextUtils.isEmpty(this.e)) {
            Uri parse = Uri.parse(this.e);
            if (parse.getQueryParameter("showTimes") != null) {
                this.b = Integer.valueOf(parse.getQueryParameter("showTimes")).intValue();
            }
            if (parse.getQueryParameter("filtered") != null) {
                this.c = Boolean.valueOf(parse.getQueryParameter("filtered")).booleanValue();
            }
            if (parse.getQueryParameter("switchChanged") != null) {
                this.d = Boolean.valueOf(parse.getQueryParameter("switchChanged")).booleanValue();
            }
        }
    }

    private void h() {
        m.a().f(i());
    }

    private String i() {
        return "hotTab://filterInfo?" + "showTimes" + "=" + this.b + "&" + "filtered" + "=" + this.c + "&" + "switchChanged" + "=" + this.d;
    }

    public int b() {
        return this.b;
    }

    public void a(boolean z) {
        this.c = z;
        this.d = true;
        h();
    }

    public boolean c() {
        return this.c;
    }

    public boolean d() {
        return this.d;
    }

    public boolean e() {
        return this.f3279a.c;
    }
}
