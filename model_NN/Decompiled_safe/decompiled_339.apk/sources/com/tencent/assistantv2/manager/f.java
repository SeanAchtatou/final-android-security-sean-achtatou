package com.tencent.assistantv2.manager;

import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.x;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.manager.smartcard.o;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.e;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.Banner;
import com.tencent.assistant.protocol.jce.EntranceTemplate;
import com.tencent.assistant.protocol.jce.TopBanner;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.b.ad;
import com.tencent.assistantv2.b.r;
import com.tencent.assistantv2.component.banner.h;
import com.tencent.assistantv2.component.banner.n;
import com.tencent.assistantv2.component.topbanner.b;
import com.tencent.assistantv2.model.a.j;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class f extends x<j> implements NetworkMonitor.ConnectivityChangeListener, UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private static f f3275a;
    private r b = new r();
    private h c = new h(this, null);
    /* access modifiers changed from: private */
    public long d = 0;
    /* access modifiers changed from: private */
    public int e;
    private volatile boolean f = false;
    private volatile boolean g = false;
    private List<Long> h = new ArrayList();
    /* access modifiers changed from: private */
    public com.tencent.assistant.model.r i = null;
    private APN j = APN.NO_NETWORK;

    public static synchronized f a() {
        f fVar;
        synchronized (f.class) {
            if (f3275a == null) {
                f3275a = new f();
            }
            fVar = f3275a;
        }
        return fVar;
    }

    private f() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_GOFRONT, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        cq.a().a(this);
    }

    public void b() {
        if (!this.g) {
            this.g = true;
            if (!this.b.c()) {
                this.b.a();
            }
        }
        if (!this.f) {
            this.f = true;
            o.a();
            this.b.register(this.c);
        }
    }

    public void a(boolean z, ad adVar) {
        if (!z) {
            this.b.a();
        } else {
            this.b.a(adVar);
        }
    }

    public int c() {
        return this.b.b();
    }

    public ad d() {
        return this.b.d();
    }

    public boolean e() {
        return this.b.d().c;
    }

    public List<e> f() {
        return a(this.b.e(), this.b.f(), 0);
    }

    public boolean g() {
        ArrayList<SimpleAppModel> f2 = this.b.f();
        return f2 != null && f2.size() > 0;
    }

    public List<com.tencent.assistantv2.component.banner.f> h() {
        return a(this.b.g());
    }

    public com.tencent.assistant.model.r i() {
        return this.i;
    }

    public EntranceTemplate j() {
        return this.b.j();
    }

    public long k() {
        return this.b.k();
    }

    /* access modifiers changed from: private */
    public void a(TopBanner topBanner, long j2) {
        if (topBanner != null) {
            this.i = new com.tencent.assistant.model.r();
            this.i.f1671a = topBanner.f2406a;
            this.i.i = topBanner.i;
            this.i.g = topBanner.g;
            this.i.f = topBanner.f;
            this.i.e = topBanner.e;
            this.i.h = topBanner.h;
            this.i.c = topBanner.c;
            this.i.j = topBanner.j;
            this.i.d = topBanner.d;
            this.i.b = topBanner.b;
            this.i.m = j2;
            this.i.k = b.a().e(this.i);
        }
    }

    public com.tencent.assistant.model.b l() {
        return this.b.h();
    }

    public void onConnected(APN apn) {
        if (!this.b.c()) {
            this.b.a();
        } else if (this.j != APN.WIFI && apn == APN.WIFI && !c.k()) {
            this.b.a();
        }
    }

    public void onDisconnected(APN apn) {
        this.j = apn;
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        this.j = apn2;
        if (apn != APN.WIFI && apn2 == APN.WIFI && !c.k()) {
            this.b.a();
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                this.b.a();
                return;
            default:
                return;
        }
    }

    public void m() {
        if (this.d > 0 && System.currentTimeMillis() - this.d > m.a().ah()) {
            this.b.a();
        }
    }

    public long n() {
        return this.b.i();
    }

    /* access modifiers changed from: private */
    public List<com.tencent.assistantv2.component.banner.f> a(List<Banner> list) {
        com.tencent.assistantv2.component.banner.f jVar;
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(6);
        try {
            for (Banner next : list) {
                if (next != null) {
                    switch (next.a()) {
                        case 1:
                            jVar = new h(next);
                            break;
                        case 2:
                            jVar = new n(next);
                            break;
                        case 3:
                            jVar = new com.tencent.assistantv2.component.banner.j(next);
                            break;
                        default:
                            jVar = null;
                            break;
                    }
                    if (jVar != null) {
                        arrayList.add(jVar);
                    }
                }
            }
            return arrayList;
        } catch (Throwable th) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3, boolean z, ad adVar, boolean z2, List<com.tencent.assistantv2.component.banner.f> list, com.tencent.assistant.model.r rVar, List<e> list2) {
        a(new g(this, i2, i3, z, adVar, z2, list, rVar, list2));
    }

    /* access modifiers changed from: private */
    public synchronized List<e> a(com.tencent.assistant.h.c cVar, List<SimpleAppModel> list, int i2) {
        ArrayList arrayList;
        int i3;
        if (cVar == null || list == null) {
            arrayList = null;
        } else {
            if (i2 == 0) {
                this.h.clear();
            }
            ArrayList arrayList2 = new ArrayList();
            for (Long next : this.h) {
                for (SimpleAppModel next2 : list) {
                    if (next2.f1634a == next.longValue()) {
                        arrayList2.add(next2);
                    }
                }
            }
            ArrayList arrayList3 = new ArrayList(list);
            arrayList3.removeAll(arrayList2);
            if (arrayList3 == null || arrayList3.size() <= 0) {
                arrayList = null;
            } else {
                ArrayList arrayList4 = new ArrayList();
                int i4 = 0;
                int i5 = i2;
                while (i4 < arrayList3.size()) {
                    e eVar = new e();
                    XLog.d("SmartCard", "start.....assemble position:" + i5);
                    List<i> a2 = cVar.a(i5);
                    if (a2 == null || a2.size() <= 0) {
                        XLog.d("SmartCard", "finish..... position:" + i5 + ",null value.");
                    } else {
                        eVar.g = o.a().a(a2, this.h, i5);
                        if (eVar.g != null) {
                            o.a().a(eVar.g);
                            eVar.b = 2;
                            List<Long> h2 = eVar.g.h();
                            if (h2 != null) {
                                this.h.addAll(h2);
                            }
                        }
                    }
                    if (eVar.g == null) {
                        do {
                            int i6 = i4;
                            SimpleAppModel simpleAppModel = (SimpleAppModel) arrayList3.get(i6);
                            if (!this.h.contains(Long.valueOf(simpleAppModel.f1634a))) {
                                eVar.c = simpleAppModel;
                                eVar.b = 1;
                                this.h.add(Long.valueOf(simpleAppModel.f1634a));
                            }
                            i4 = i6 + 1;
                            if (eVar.c != null) {
                                break;
                            }
                        } while (i4 < arrayList3.size());
                    }
                    int i7 = i4;
                    if (eVar.c == null && eVar.g == null) {
                        i3 = i5;
                    } else {
                        i3 = i5 + 1;
                        arrayList4.add(eVar);
                    }
                    i5 = i3;
                    i4 = i7;
                }
                if (arrayList4 != null) {
                    this.e += arrayList4.size();
                }
                arrayList = arrayList4;
            }
        }
        return arrayList;
    }
}
