package com.tencent.assistantv2.manager;

import com.tencent.assistant.h.c;
import com.tencent.assistant.localres.x;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.manager.smartcard.o;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.b;
import com.tencent.assistant.model.e;
import com.tencent.assistant.module.ce;
import com.tencent.assistant.module.u;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.protocol.jce.CardItemWrapper;
import com.tencent.assistantv2.model.a.f;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class a extends x<f> implements NetworkMonitor.ConnectivityChangeListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public long f3270a = 0;
    private ce b = ce.a();
    private e c = new e(this, null);
    /* access modifiers changed from: private */
    public volatile boolean d = false;
    private int e;
    /* access modifiers changed from: private */
    public List<Long> f = new ArrayList();
    private List<e> g = new ArrayList();
    private APN h = APN.NO_NETWORK;

    public a() {
        cq.a().a(this);
    }

    public void a() {
        this.b.register(this.c);
        this.b.e();
    }

    public int b() {
        return this.b.g();
    }

    public int c() {
        return this.b.f();
    }

    public b d() {
        b b2 = this.b.b();
        b2.c(this.g);
        b2.b((List<SimpleAppModel>) null);
        return b2;
    }

    public void e() {
        if (this.f3270a > 0 && System.currentTimeMillis() - this.f3270a > m.a().ah()) {
            c();
        }
    }

    public void a(d dVar, c cVar) {
        if (dVar != null) {
            int i = this.e;
            if (dVar.e) {
                i = 0;
            }
            List<e> a2 = a(dVar.h, i, cVar);
            if (dVar.e) {
                this.g.clear();
                this.g.addAll(a2);
            }
            a(new b(this, dVar, a2));
        }
    }

    public synchronized List<e> a(List<CardItemWrapper> list, int i, c cVar) {
        ArrayList arrayList;
        int i2;
        ArrayList arrayList2 = new ArrayList();
        if (list == null || list.size() <= 0) {
            arrayList = arrayList2;
        } else {
            if (i == 0) {
                this.f.clear();
            }
            ArrayList arrayList3 = new ArrayList(list);
            int i3 = 0;
            int i4 = i;
            while (i3 < arrayList3.size()) {
                e eVar = new e();
                List<i> a2 = cVar.a(i4);
                if (a2 != null && a2.size() > 0) {
                    eVar.g = o.a().a(a2, this.f, i4);
                    if (eVar.g != null) {
                        o.a().a(eVar.g);
                        eVar.b = 2;
                        List<Long> h2 = eVar.g.h();
                        if (h2 != null) {
                            this.f.addAll(h2);
                        }
                    }
                }
                if (eVar.g == null) {
                    do {
                        int i5 = i3;
                        CardItemWrapper cardItemWrapper = (CardItemWrapper) arrayList3.get(i5);
                        if (cardItemWrapper.f2028a == 0) {
                            SimpleAppModel a3 = u.a(cardItemWrapper.b);
                            if (!this.f.contains(Long.valueOf(a3.f1634a)) && !this.f.contains(Long.valueOf(a3.f1634a))) {
                                eVar.c = a3;
                                eVar.b = 1;
                                this.f.add(Long.valueOf(a3.f1634a));
                            }
                        } else if (cardItemWrapper.f2028a > 0) {
                            eVar.d = u.a(cardItemWrapper.b(), new c(this), 3);
                            if (eVar.d == null || eVar.d.size() == 0) {
                                eVar.d = null;
                            } else {
                                eVar.b = 5;
                                eVar.f1662a = cardItemWrapper.a();
                                for (SimpleAppModel simpleAppModel : eVar.d) {
                                    this.f.add(Long.valueOf(simpleAppModel.f1634a));
                                }
                            }
                        }
                        i3 = i5 + 1;
                        if (eVar.c != null || eVar.d != null) {
                            break;
                        }
                    } while (i3 < arrayList3.size());
                }
                int i6 = i3;
                if (eVar != null) {
                    i2 = i4 + 1;
                    arrayList2.add(eVar);
                } else {
                    i2 = i4;
                }
                i4 = i2;
                i3 = i6;
            }
            if (arrayList2 != null) {
                this.e += arrayList2.size();
            }
            arrayList = arrayList2;
        }
        return arrayList;
    }

    public void onConnected(APN apn) {
        if (!this.d) {
            this.b.e();
        }
    }

    public void onDisconnected(APN apn) {
        this.h = apn;
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        this.h = apn2;
    }
}
