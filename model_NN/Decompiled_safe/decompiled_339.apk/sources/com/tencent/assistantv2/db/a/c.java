package com.tencent.assistantv2.db.a;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.db.table.IBaseTable;

/* compiled from: ProGuard */
public class c implements IBaseTable {
    public boolean a(int i, int i2) {
        b(i);
        SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
        ContentValues contentValues = new ContentValues();
        contentValues.put("event_id", Integer.valueOf(i));
        contentValues.put("show_time", Integer.valueOf(i2));
        return writableDatabaseWrapper.insert("treasure_box_entry_record_table", null, contentValues) > 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int a(int r11) {
        /*
            r10 = this;
            r8 = 0
            r9 = 0
            com.tencent.assistant.db.helper.SqliteHelper r0 = r10.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()
            java.lang.String r1 = "treasure_box_entry_record_table"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0046, all -> 0x003f }
            r3 = 0
            java.lang.String r4 = "show_time"
            r2[r3] = r4     // Catch:{ Exception -> 0x0046, all -> 0x003f }
            java.lang.String r3 = "event_id=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0046, all -> 0x003f }
            r5 = 0
            java.lang.String r6 = java.lang.String.valueOf(r11)     // Catch:{ Exception -> 0x0046, all -> 0x003f }
            r4[r5] = r6     // Catch:{ Exception -> 0x0046, all -> 0x003f }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0046, all -> 0x003f }
            if (r1 == 0) goto L_0x004f
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0058, all -> 0x0055 }
            if (r0 == 0) goto L_0x004f
            java.lang.String r0 = "show_time"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x0058, all -> 0x0055 }
            int r0 = r1.getInt(r0)     // Catch:{ Exception -> 0x0058, all -> 0x0055 }
            if (r1 == 0) goto L_0x003e
            r1.close()
        L_0x003e:
            return r0
        L_0x003f:
            r0 = move-exception
        L_0x0040:
            if (r9 == 0) goto L_0x0045
            r9.close()
        L_0x0045:
            throw r0
        L_0x0046:
            r0 = move-exception
            r0 = r9
        L_0x0048:
            if (r0 == 0) goto L_0x004d
            r0.close()
        L_0x004d:
            r0 = r8
            goto L_0x003e
        L_0x004f:
            if (r1 == 0) goto L_0x004d
            r1.close()
            goto L_0x004d
        L_0x0055:
            r0 = move-exception
            r9 = r1
            goto L_0x0040
        L_0x0058:
            r0 = move-exception
            r0 = r1
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistantv2.db.a.c.a(int):int");
    }

    public boolean b(int i) {
        if (getHelper().getWritableDatabaseWrapper().delete("treasure_box_entry_record_table", "event_id=?", new String[]{String.valueOf(i)}) > 0) {
            return true;
        }
        return false;
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "treasure_box_entry_record_table";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists treasure_box_entry_record_table( _id INTEGER PRIMARY KEY AUTOINCREMENT, event_id INTEGER, show_time INTEGER); ";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i2 != 10) {
            return null;
        }
        return new String[]{"CREATE TABLE if not exists treasure_box_entry_record_table( _id INTEGER PRIMARY KEY AUTOINCREMENT, event_id INTEGER, show_time INTEGER); "};
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }
}
