package com.tencent.assistantv2.db.a;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.db.table.IBaseTable;
import com.tencent.assistantv2.db.helper.MediaDbHelper;
import com.tencent.assistantv2.model.AbstractDownloadInfo;
import com.tencent.assistantv2.model.h;
import com.tencent.open.SocialConstants;

/* compiled from: ProGuard */
public class d implements IBaseTable {
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: com.tencent.assistant.db.helper.SQLiteDatabaseWrapper} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: com.tencent.assistant.db.helper.SQLiteDatabaseWrapper} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: com.tencent.assistant.db.helper.SQLiteDatabaseWrapper} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: com.tencent.assistant.db.helper.SQLiteDatabaseWrapper} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v5, resolved type: com.tencent.assistant.db.helper.SQLiteDatabaseWrapper} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v6, resolved type: com.tencent.assistant.db.helper.SQLiteDatabaseWrapper} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002d A[SYNTHETIC, Splitter:B:12:0x002d] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0032 A[Catch:{ Exception -> 0x0036 }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0042 A[SYNTHETIC, Splitter:B:24:0x0042] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0047 A[Catch:{ Exception -> 0x004b }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0054 A[SYNTHETIC, Splitter:B:33:0x0054] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0059 A[Catch:{ Exception -> 0x005d }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.tencent.assistantv2.model.h> a() {
        /*
            r5 = this;
            r1 = 0
            java.util.ArrayList r3 = new java.util.ArrayList
            r0 = 5
            r3.<init>(r0)
            com.tencent.assistant.db.helper.SqliteHelper r0 = r5.getHelper()     // Catch:{ Exception -> 0x003b, all -> 0x0050 }
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r2 = r0.getReadableDatabaseWrapper()     // Catch:{ Exception -> 0x003b, all -> 0x0050 }
            java.lang.String r0 = "select * from video_down_info order by _id desc"
            r4 = 0
            android.database.Cursor r1 = r2.rawQuery(r0, r4)     // Catch:{ Exception -> 0x0064 }
            if (r1 == 0) goto L_0x002b
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0064 }
            if (r0 == 0) goto L_0x002b
        L_0x001e:
            com.tencent.assistantv2.model.h r0 = r5.a(r1)     // Catch:{ Exception -> 0x0064 }
            r3.add(r0)     // Catch:{ Exception -> 0x0064 }
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x0064 }
            if (r0 != 0) goto L_0x001e
        L_0x002b:
            if (r1 == 0) goto L_0x0030
            r1.close()     // Catch:{ Exception -> 0x0036 }
        L_0x0030:
            if (r2 == 0) goto L_0x0035
            r2.close()     // Catch:{ Exception -> 0x0036 }
        L_0x0035:
            return r3
        L_0x0036:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0035
        L_0x003b:
            r0 = move-exception
            r2 = r1
        L_0x003d:
            r0.printStackTrace()     // Catch:{ all -> 0x0062 }
            if (r1 == 0) goto L_0x0045
            r1.close()     // Catch:{ Exception -> 0x004b }
        L_0x0045:
            if (r2 == 0) goto L_0x0035
            r2.close()     // Catch:{ Exception -> 0x004b }
            goto L_0x0035
        L_0x004b:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0035
        L_0x0050:
            r0 = move-exception
            r2 = r1
        L_0x0052:
            if (r1 == 0) goto L_0x0057
            r1.close()     // Catch:{ Exception -> 0x005d }
        L_0x0057:
            if (r2 == 0) goto L_0x005c
            r2.close()     // Catch:{ Exception -> 0x005d }
        L_0x005c:
            throw r0
        L_0x005d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005c
        L_0x0062:
            r0 = move-exception
            goto L_0x0052
        L_0x0064:
            r0 = move-exception
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistantv2.db.a.d.a():java.util.List");
    }

    private h a(Cursor cursor) {
        h hVar = new h();
        hVar.n = cursor.getString(cursor.getColumnIndexOrThrow("name"));
        hVar.o = cursor.getString(cursor.getColumnIndexOrThrow(SocialConstants.PARAM_APP_DESC));
        hVar.b = cursor.getString(cursor.getColumnIndexOrThrow("down_url"));
        hVar.c = cursor.getString(cursor.getColumnIndexOrThrow("video_id"));
        hVar.p = cursor.getString(cursor.getColumnIndexOrThrow("cover_url"));
        hVar.g = cursor.getString(cursor.getColumnIndexOrThrow("downloading_path"));
        hVar.h = cursor.getString(cursor.getColumnIndexOrThrow("save_path"));
        hVar.q = cursor.getString(cursor.getColumnIndexOrThrow("cp_name"));
        hVar.r = cursor.getString(cursor.getColumnIndexOrThrow("open_package_name"));
        hVar.s = cursor.getString(cursor.getColumnIndexOrThrow("open_activity"));
        hVar.t = cursor.getString(cursor.getColumnIndexOrThrow("open_uri"));
        hVar.w = cursor.getString(cursor.getColumnIndexOrThrow("extra_params"));
        hVar.d = cursor.getLong(cursor.getColumnIndexOrThrow("file_size"));
        hVar.i = AbstractDownloadInfo.DownState.values()[cursor.getInt(cursor.getColumnIndexOrThrow("down_state"))];
        hVar.e = cursor.getLong(cursor.getColumnIndexOrThrow("create_time"));
        hVar.f = cursor.getLong(cursor.getColumnIndexOrThrow("finish_time"));
        hVar.u = cursor.getInt(cursor.getColumnIndexOrThrow("min_version_code"));
        hVar.v = cursor.getString(cursor.getColumnIndexOrThrow("player_name"));
        hVar.f3307a = cursor.getString(cursor.getColumnIndexOrThrow("video_save_name"));
        return hVar;
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "video_down_info";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists video_down_info (\n[_id] integer PRIMARY KEY AUTOINCREMENT,\n[name] text,\n[desc] text,\n[down_url] text,\n[video_id] text,\n[cover_url] text,\n[file_size] integer,\n[down_state] integer,\n[downloading_path] text,\n[save_path] text,\n[cp_name] text,\n[create_time] integer,\n[finish_time] integer,\n[open_package_name] text,\n[open_activity] text,\n[open_uri] text,\n[min_version_code] integer,\n[extra_params] text,\n[video_save_name] text,\n[player_name] text\n);\n";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i2 != 1) {
            return null;
        }
        return new String[]{"CREATE TABLE if not exists video_down_info (\n[_id] integer PRIMARY KEY AUTOINCREMENT,\n[name] text,\n[desc] text,\n[down_url] text,\n[video_id] text,\n[cover_url] text,\n[file_size] integer,\n[down_state] integer,\n[downloading_path] text,\n[save_path] text,\n[cp_name] text,\n[create_time] integer,\n[finish_time] integer,\n[open_package_name] text,\n[open_activity] text,\n[open_uri] text,\n[min_version_code] integer,\n[extra_params] text,\n[video_save_name] text,\n[player_name] text\n);\n"};
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return MediaDbHelper.get(AstApp.i());
    }

    public int a(String str) {
        if (TextUtils.isEmpty(str)) {
            return 0;
        }
        return getHelper().getReadableDatabaseWrapper().delete("video_down_info", "video_id = ?", new String[]{str});
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x004d A[SYNTHETIC, Splitter:B:31:0x004d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long a(com.tencent.assistantv2.model.h r5) {
        /*
            r4 = this;
            r1 = 0
            if (r5 == 0) goto L_0x0041
            com.tencent.assistant.db.helper.SqliteHelper r0 = r4.getHelper()     // Catch:{ Exception -> 0x0038, all -> 0x0049 }
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r2 = r0.getWritableDatabaseWrapper()     // Catch:{ Exception -> 0x0038, all -> 0x0049 }
            int r0 = r4.a(r5, r2)     // Catch:{ Exception -> 0x005b, all -> 0x0056 }
            if (r0 > 0) goto L_0x002b
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ Exception -> 0x005b, all -> 0x0056 }
            r0.<init>()     // Catch:{ Exception -> 0x005b, all -> 0x0056 }
            r4.a(r0, r5)     // Catch:{ Exception -> 0x005b, all -> 0x0056 }
            java.lang.String r1 = "video_down_info"
            r3 = 0
            long r0 = r2.insert(r1, r3, r0)     // Catch:{ Exception -> 0x005b, all -> 0x0056 }
            if (r2 == 0) goto L_0x0025
            r2.close()     // Catch:{ Exception -> 0x0026 }
        L_0x0025:
            return r0
        L_0x0026:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0025
        L_0x002b:
            r0 = 0
            if (r2 == 0) goto L_0x0025
            r2.close()     // Catch:{ Exception -> 0x0033 }
            goto L_0x0025
        L_0x0033:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0025
        L_0x0038:
            r0 = move-exception
        L_0x0039:
            r0.printStackTrace()     // Catch:{ all -> 0x0058 }
            if (r1 == 0) goto L_0x0041
            r1.close()     // Catch:{ Exception -> 0x0044 }
        L_0x0041:
            r0 = -1
            goto L_0x0025
        L_0x0044:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0041
        L_0x0049:
            r0 = move-exception
            r2 = r1
        L_0x004b:
            if (r2 == 0) goto L_0x0050
            r2.close()     // Catch:{ Exception -> 0x0051 }
        L_0x0050:
            throw r0
        L_0x0051:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0050
        L_0x0056:
            r0 = move-exception
            goto L_0x004b
        L_0x0058:
            r0 = move-exception
            r2 = r1
            goto L_0x004b
        L_0x005b:
            r0 = move-exception
            r1 = r2
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistantv2.db.a.d.a(com.tencent.assistantv2.model.h):long");
    }

    private int a(h hVar, SQLiteDatabaseWrapper sQLiteDatabaseWrapper) {
        if (hVar == null) {
            return -1;
        }
        try {
            ContentValues contentValues = new ContentValues();
            a(contentValues, hVar);
            int update = sQLiteDatabaseWrapper.update("video_down_info", contentValues, "video_id = ? ", new String[]{hVar.c});
            if (update <= 0) {
                return 0;
            }
            return update;
        } catch (Exception e) {
            e.printStackTrace();
            return -2;
        }
    }

    private void a(ContentValues contentValues, h hVar) {
        if (hVar != null) {
            contentValues.put("name", hVar.n);
            contentValues.put(SocialConstants.PARAM_APP_DESC, hVar.o);
            contentValues.put("down_url", hVar.b);
            contentValues.put("video_id", hVar.c);
            contentValues.put("cover_url", hVar.p);
            contentValues.put("file_size", Long.valueOf(hVar.d));
            contentValues.put("downloading_path", hVar.g);
            contentValues.put("save_path", hVar.h);
            contentValues.put("cp_name", hVar.q);
            contentValues.put("create_time", Long.valueOf(hVar.e));
            contentValues.put("finish_time", Long.valueOf(hVar.f));
            contentValues.put("open_package_name", hVar.r);
            contentValues.put("open_activity", hVar.s);
            contentValues.put("open_uri", hVar.t);
            contentValues.put("min_version_code", Integer.valueOf(hVar.u));
            contentValues.put("extra_params", hVar.w);
            contentValues.put("player_name", hVar.v);
            contentValues.put("down_state", Integer.valueOf(hVar.i.ordinal()));
            contentValues.put("video_save_name", hVar.f3307a);
        }
    }
}
