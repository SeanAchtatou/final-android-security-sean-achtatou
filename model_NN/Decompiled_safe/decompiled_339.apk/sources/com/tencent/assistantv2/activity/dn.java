package com.tencent.assistantv2.activity;

import android.os.Handler;
import android.os.Message;
import com.tencent.assistant.event.EventDispatcherEnum;

/* compiled from: ProGuard */
class dn extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f2837a;

    dn(SearchActivity searchActivity) {
        this.f2837a = searchActivity;
    }

    public void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 0:
                String obj = message.obj.toString();
                this.f2837a.w.setVisibility(8);
                this.f2837a.v.setVisibility(8);
                int unused = this.f2837a.F = this.f2837a.D.a(obj.toString().trim());
                return;
            case EventDispatcherEnum.PLUGIN_EVENT_LOGIN_START:
                this.f2837a.B();
                return;
            default:
                return;
        }
    }
}
