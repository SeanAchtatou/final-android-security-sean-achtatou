package com.tencent.assistantv2.activity;

import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class dr extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f2841a;

    dr(SearchActivity searchActivity) {
        this.f2841a = searchActivity;
    }

    public void onTMAClick(View view) {
        if (!TextUtils.isEmpty(this.f2841a.M) && !TextUtils.isEmpty(this.f2841a.N)) {
            this.f2841a.u.b(this.f2841a.N);
            String unused = this.f2841a.M = (String) null;
            String unused2 = this.f2841a.N = (String) null;
            int unused3 = this.f2841a.K = 200705;
        } else if (this.f2841a.y != null) {
            int unused4 = this.f2841a.K = this.f2841a.y.b();
        } else {
            int unused5 = this.f2841a.K = (int) STConst.ST_PAGE_SEARCH;
        }
        String b = this.f2841a.u.b();
        if (TextUtils.isEmpty(b) || TextUtils.isEmpty(b.trim())) {
            Toast.makeText(this.f2841a, (int) R.string.must_input_keyword, 0).show();
            return;
        }
        this.f2841a.H.removeMessages(0);
        this.f2841a.D.a(this.f2841a.F);
        this.f2841a.z();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f2841a, 200);
        buildSTInfo.slotId = a.a("03", "004");
        return buildSTInfo;
    }
}
