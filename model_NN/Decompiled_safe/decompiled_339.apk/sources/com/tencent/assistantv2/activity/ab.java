package com.tencent.assistantv2.activity;

import android.view.animation.Animation;

/* compiled from: ProGuard */
class ab implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AssistantTabActivity f2749a;

    ab(AssistantTabActivity assistantTabActivity) {
        this.f2749a = assistantTabActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.AssistantTabActivity.a(com.tencent.assistantv2.activity.AssistantTabActivity, boolean):boolean
     arg types: [com.tencent.assistantv2.activity.AssistantTabActivity, int]
     candidates:
      com.tencent.assistantv2.activity.AssistantTabActivity.a(com.tencent.assistantv2.activity.AssistantTabActivity, java.lang.Class):java.lang.Class
      com.tencent.assistantv2.activity.AssistantTabActivity.a(com.tencent.assistantv2.activity.AssistantTabActivity, float):void
      com.tencent.assistantv2.activity.AssistantTabActivity.a(com.tencent.assistantv2.activity.AssistantTabActivity, int):void
      com.tencent.assistantv2.activity.AssistantTabActivity.a(com.tencent.assistantv2.activity.AssistantTabActivity, java.lang.String):void
      com.tencent.assistantv2.activity.AssistantTabActivity.a(boolean, long):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistantv2.activity.AssistantTabActivity.a(com.tencent.assistantv2.activity.AssistantTabActivity, boolean):boolean */
    public void onAnimationStart(Animation animation) {
        boolean unused = this.f2749a.aU = false;
    }

    public void onAnimationRepeat(Animation animation) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.AssistantTabActivity.a(com.tencent.assistantv2.activity.AssistantTabActivity, boolean):boolean
     arg types: [com.tencent.assistantv2.activity.AssistantTabActivity, int]
     candidates:
      com.tencent.assistantv2.activity.AssistantTabActivity.a(com.tencent.assistantv2.activity.AssistantTabActivity, java.lang.Class):java.lang.Class
      com.tencent.assistantv2.activity.AssistantTabActivity.a(com.tencent.assistantv2.activity.AssistantTabActivity, float):void
      com.tencent.assistantv2.activity.AssistantTabActivity.a(com.tencent.assistantv2.activity.AssistantTabActivity, int):void
      com.tencent.assistantv2.activity.AssistantTabActivity.a(com.tencent.assistantv2.activity.AssistantTabActivity, java.lang.String):void
      com.tencent.assistantv2.activity.AssistantTabActivity.a(boolean, long):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistantv2.activity.AssistantTabActivity.a(com.tencent.assistantv2.activity.AssistantTabActivity, boolean):boolean */
    public void onAnimationEnd(Animation animation) {
        boolean unused = this.f2749a.aU = true;
    }
}
