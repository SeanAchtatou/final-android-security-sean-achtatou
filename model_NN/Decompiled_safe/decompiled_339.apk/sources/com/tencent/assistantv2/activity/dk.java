package com.tencent.assistantv2.activity;

import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.st.STConst;

/* compiled from: ProGuard */
class dk implements View.OnKeyListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f2834a;

    dk(SearchActivity searchActivity) {
        this.f2834a = searchActivity;
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i == 66 && 1 == keyEvent.getAction()) {
            if (!TextUtils.isEmpty(this.f2834a.M) && !TextUtils.isEmpty(this.f2834a.N)) {
                this.f2834a.u.b(this.f2834a.N);
                String unused = this.f2834a.M = (String) null;
                String unused2 = this.f2834a.N = (String) null;
                int unused3 = this.f2834a.K = 200705;
            } else if (this.f2834a.y != null) {
                int unused4 = this.f2834a.K = this.f2834a.y.b();
            } else {
                int unused5 = this.f2834a.K = (int) STConst.ST_PAGE_SEARCH;
            }
            String b = this.f2834a.u.b();
            if (TextUtils.isEmpty(b) || TextUtils.isEmpty(b.trim())) {
                Toast.makeText(this.f2834a, (int) R.string.must_input_keyword, 0).show();
                return true;
            }
            this.f2834a.H.removeMessages(0);
            this.f2834a.D.a(this.f2834a.F);
            this.f2834a.a("004", 200);
            this.f2834a.z();
        }
        return false;
    }
}
