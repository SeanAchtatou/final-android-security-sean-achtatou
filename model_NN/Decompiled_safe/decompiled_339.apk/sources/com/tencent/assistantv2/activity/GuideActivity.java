package com.tencent.assistantv2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ExViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.ShareBaseActivity;
import com.tencent.assistant.activity.SplashActivity;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.component.txscrollview.TXTabBarLayout;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.login.d;
import com.tencent.assistant.manager.b;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.protocol.jce.WelcomePageItem;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.r;
import com.tencent.assistantv2.adapter.p;
import com.tencent.assistantv2.b.am;
import com.tencent.assistantv2.component.dh;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.business.StartUpCostTimeSTManager;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.STPageInfo;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class GuideActivity extends ShareBaseActivity implements Handler.Callback, View.OnTouchListener, UIEventListener {
    private TextView A;
    /* access modifiers changed from: private */
    public CheckBox B;
    /* access modifiers changed from: private */
    public CheckBox C;
    private View D;
    private LoadingView E;
    private ArrayList<WelcomePageItem> F = new ArrayList<>();
    private boolean G = false;
    private boolean H = false;
    private Handler I;
    private final int J = TXTabBarLayout.TABITEM_TIPS_TEXT_ID;
    private final int K = 102;
    private final int L = 103;
    private final int M = 104;
    /* access modifiers changed from: private */
    public boolean N = false;
    /* access modifiers changed from: private */
    public boolean O = false;
    private int P = 3;
    private int Q = 50;
    private long R = 0;
    /* access modifiers changed from: private */
    public GetPhoneUserAppListResponse S;
    /* access modifiers changed from: private */
    public int T = 0;
    /* access modifiers changed from: private */
    public TextView U;
    /* access modifiers changed from: private */
    public boolean V = false;
    private CompoundButton.OnCheckedChangeListener W = new bw(this);
    private OnTMAParamExClickListener X = new bx(this);
    ca n = new ca(this);
    am t;
    /* access modifiers changed from: private */
    public String u = "GuideActivity";
    private ExViewPager v;
    private p w;
    private List<View> x = new ArrayList();
    private LayoutInflater y;
    /* access modifiers changed from: private */
    public Button z;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        k.a(StartUpCostTimeSTManager.TIMETYPE.B11_TIME, System.currentTimeMillis(), 0);
        this.R = System.currentTimeMillis();
        if (b.a().c()) {
            startActivity(new Intent(this, MainActivity.class));
            TemporaryThreadManager.get().start(new bv(this));
            finish();
            overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
            return;
        }
        A();
        if (SplashActivity.f384a) {
            i();
        }
    }

    /* access modifiers changed from: package-private */
    public void i() {
        this.t = new am();
        this.t.register(this.n);
        this.t.a(false);
        this.t.a();
    }

    public int f() {
        return STConst.ST_PAGE_GUIDE_RESULT_HUANJI_NO;
    }

    private void v() {
        this.I = new Handler(this);
        z();
    }

    public STPageInfo q() {
        this.o.f3358a = STConst.ST_PAGE_GUIDE_COVER;
        return this.o;
    }

    /* access modifiers changed from: protected */
    public boolean l() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean k() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_SHARE_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_SHARE_FAIL, this);
        this.I.sendEmptyMessageDelayed(103, 1500);
        if (this.V) {
            A();
        }
    }

    private void w() {
        this.y = LayoutInflater.from(this);
        x();
        this.v = (ExViewPager) findViewById(R.id.vPager);
        this.w = new p();
        this.w.a(this.x);
        this.v.setAdapter(this.w);
        this.v.setOnTouchListener(this);
        this.v.setOnPageChangeListener(new cb(this));
    }

    private void x() {
        this.D = this.y.inflate((int) R.layout.guide_view_3, (ViewGroup) null);
        j();
        this.x.add(this.D);
        this.z = (Button) this.D.findViewById(R.id.btn_login);
        this.z.setVisibility(8);
        this.A = (TextView) this.D.findViewById(R.id.btn_skip);
        this.E = (LoadingView) this.D.findViewById(R.id.loadingView);
        this.U = (TextView) this.D.findViewById(R.id.showtips);
        this.z.setOnClickListener(this.X);
        this.z.setTag(R.id.tma_st_slot_tag, "001");
        this.A.setOnClickListener(this.X);
        this.A.setTag(R.id.tma_st_slot_tag, "002");
    }

    /* access modifiers changed from: private */
    public String y() {
        if (this.F.size() > 0) {
            return this.F.get(0).c;
        }
        return Constants.STR_EMPTY;
    }

    private void z() {
        XLog.i(this.u, ">>userAppCount=" + 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void A() {
        TemporaryThreadManager.get().start(new by(this));
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("action_key_from_guide", true);
        startActivity(intent);
        TemporaryThreadManager.get().start(new bz(this));
        finish();
    }

    public void finish() {
        super.finish();
        if (SplashActivity.f384a) {
            if (this.t != null) {
                this.t.unregister(this.n);
            }
            if (this.S == null) {
                r.a(this, System.currentTimeMillis());
            }
        }
    }

    private void B() {
        if (this.H) {
            this.E.setVisibility(0);
        }
        this.I.sendEmptyMessageDelayed(TXTabBarLayout.TABITEM_TIPS_TEXT_ID, 10000);
    }

    private void C() {
        if (d.a().j()) {
            this.I.removeMessages(102);
            B();
            return;
        }
        XLog.i(this.u, "这小子还真跑进这个分支了");
        if (this.H) {
            this.E.setVisibility(0);
        }
        this.I.sendEmptyMessageDelayed(102, 200);
    }

    public void handleUIEvent(Message message) {
        boolean z2 = true;
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                XLog.i(this.u, "login success" + message.arg2);
                if (message.arg2 == 1 && !this.O) {
                    z2 = false;
                }
                this.H = z2;
                B();
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_FAIL:
                XLog.i(this.u, "login fail");
                return;
            case EventDispatcherEnum.UI_EVENT_SHARE_SUCCESS:
                if (this.N) {
                    dh.a(getApplicationContext(), (int) R.string.share_success, 0);
                    A();
                    return;
                } else if ((message.obj instanceof Integer) && 4 == ((Integer) message.obj).intValue()) {
                    if (message.what == 1089) {
                        dh.a(getApplicationContext(), getString(R.string.share_success), 0);
                    }
                    A();
                    return;
                } else {
                    return;
                }
            case EventDispatcherEnum.UI_EVENT_SHARE_FAIL:
                if (this.N) {
                    dh.a(getApplicationContext(), (int) R.string.share_fail, 0);
                    A();
                    return;
                } else if ((message.obj instanceof Integer) && 4 == ((Integer) message.obj).intValue()) {
                    A();
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        XLog.i(this.u, ">>onStop>>");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        XLog.i(this.u, ">>onDestory>>");
        super.onDestroy();
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_SHARE_SUCCESS, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_SHARE_FAIL, this);
        if (this.I != null) {
            this.I.removeCallbacksAndMessages(null);
        }
        k.a((int) STConst.ST_PAGE_COMPETITIVE, CostTimeSTManager.TIMETYPE.CORRECTION_MINUS, System.currentTimeMillis() - this.R);
    }

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case TXTabBarLayout.TABITEM_TIPS_TEXT_ID:
                if (this.H) {
                    try {
                        dh.a(getApplicationContext(), getString(R.string.guide_toast_fail), 0);
                        break;
                    } catch (Exception e) {
                        break;
                    }
                }
                break;
            case 102:
                C();
                break;
            case 103:
                if (this.v == null || this.v.getCurrentItem() == 0) {
                }
            case 104:
                if (this.v != null && this.v.getCurrentItem() == 0) {
                    this.v.smoothScrollTo(0, 0, 100);
                    break;
                }
        }
        return false;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (4 != i) {
            return true;
        }
        A();
        k.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_NO, "04_002", 0, STConst.ST_DEFAULT_SLOT, 200));
        return true;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        this.I.removeMessages(103);
        this.I.removeMessages(104);
        return false;
    }

    /* access modifiers changed from: package-private */
    public void j() {
        k.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_NO, STConst.ST_DEFAULT_SLOT, 0, STConst.ST_DEFAULT_SLOT, 100));
    }
}
