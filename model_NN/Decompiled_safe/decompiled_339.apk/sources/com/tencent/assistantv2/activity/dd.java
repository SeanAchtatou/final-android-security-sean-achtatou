package com.tencent.assistantv2.activity;

import android.support.v4.view.ViewPager;
import android.view.View;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class dd implements ViewPager.OnPageChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MainActivity f2827a;

    public dd(MainActivity mainActivity) {
        this.f2827a = mainActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.MainActivity.b(com.tencent.assistantv2.activity.MainActivity, boolean):boolean
     arg types: [com.tencent.assistantv2.activity.MainActivity, int]
     candidates:
      com.tencent.assistantv2.activity.MainActivity.b(com.tencent.assistantv2.activity.MainActivity, int):void
      com.tencent.assistantv2.activity.MainActivity.b(com.tencent.assistantv2.activity.MainActivity, java.util.ArrayList):void
      com.tencent.assistantv2.activity.MainActivity.b(com.tencent.assistantv2.activity.MainActivity, boolean):boolean */
    public void onPageSelected(int i) {
        for (int i2 = 0; i2 < this.f2827a.F.getCount(); i2++) {
            if (i2 == i) {
                this.f2827a.G.b(i).setSelected(true);
            } else {
                View b = this.f2827a.G.b(i2);
                if (b != null) {
                    b.setSelected(false);
                }
            }
        }
        if (!(this.f2827a.I == i || this.f2827a.I >= this.f2827a.F.getCount() || this.f2827a.F.a(this.f2827a.I) == null)) {
            ((ay) this.f2827a.F.a(this.f2827a.I)).C();
        }
        String b2 = this.f2827a.b(i);
        if (i == 5) {
            this.f2827a.a(this.f2827a.I, i + 2, b2);
        } else {
            this.f2827a.a(this.f2827a.I, i, b2);
        }
        ((ay) this.f2827a.F.a(i)).I();
        int unused = this.f2827a.I = i;
        if (this.f2827a.L) {
            this.f2827a.H.a(this.f2827a.I, this.f2827a.P);
            boolean unused2 = this.f2827a.L = false;
        }
        this.f2827a.h(i);
        this.f2827a.c(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.MainActivity.b(com.tencent.assistantv2.activity.MainActivity, boolean):boolean
     arg types: [com.tencent.assistantv2.activity.MainActivity, int]
     candidates:
      com.tencent.assistantv2.activity.MainActivity.b(com.tencent.assistantv2.activity.MainActivity, int):void
      com.tencent.assistantv2.activity.MainActivity.b(com.tencent.assistantv2.activity.MainActivity, java.util.ArrayList):void
      com.tencent.assistantv2.activity.MainActivity.b(com.tencent.assistantv2.activity.MainActivity, boolean):boolean */
    public void onPageScrolled(int i, float f, int i2) {
        boolean unused = this.f2827a.L = false;
        this.f2827a.H.a((int) (((float) this.f2827a.getResources().getDimensionPixelSize(R.dimen.navigation_curcor_padding_left)) + ((((float) i) + f) * this.f2827a.P)));
    }

    public void onPageScrollStateChanged(int i) {
    }
}
