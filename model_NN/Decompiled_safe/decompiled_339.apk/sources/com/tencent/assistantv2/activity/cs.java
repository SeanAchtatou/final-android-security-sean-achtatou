package com.tencent.assistantv2.activity;

import com.tencent.assistant.m;
import com.tencent.assistant.module.timer.RecoverAppListReceiver;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bh;
import com.tencent.assistant.utils.bx;

/* compiled from: ProGuard */
class cs implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MainActivity f2815a;

    cs(MainActivity mainActivity) {
        this.f2815a = mainActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, boolean):void
     arg types: [com.tencent.assistantv2.activity.MainActivity, int]
     candidates:
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, int):int
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.db.table.z):com.tencent.assistant.db.table.z
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, java.util.ArrayList):java.util.ArrayList
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, android.content.Intent):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.protocol.jce.DesktopShortCut):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistant.protocol.jce.DesktopShortCut, com.tencent.assistant.protocol.jce.DesktopShortCut):boolean
      com.tencent.assistantv2.activity.MainActivity.a(int, boolean):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, boolean):void */
    public void run() {
        MainActivity.x = m.a().a("key_re_app_list_state", 0);
        XLog.d("KEY_RECOVER_APP_LIST_STATE", "KEY_RECOVER_APP_LIST_STATE:" + MainActivity.x);
        if (MainActivity.x == RecoverAppListReceiver.RecoverAppListState.FIRST_SHORT.ordinal()) {
            byte[] e = m.a().e("key_re_app_list_first_response");
            this.f2815a.z = (GetPhoneUserAppListResponse) bh.b(e, GetPhoneUserAppListResponse.class);
            if (this.f2815a.z != null) {
                switch (this.f2815a.z.b) {
                    case 0:
                    case 3:
                    case 4:
                        this.f2815a.d(this.f2815a.u);
                        return;
                    case 1:
                    case 2:
                        bx.a(this.f2815a, false, this.f2815a.f(), this.f2815a.z, false);
                        return;
                    default:
                        return;
                }
            }
        } else if (MainActivity.x == RecoverAppListReceiver.RecoverAppListState.SECONDLY_SHORT.ordinal()) {
            GetPhoneUserAppListResponse getPhoneUserAppListResponse = (GetPhoneUserAppListResponse) bh.b(m.a().e("key_re_app_list_second_response"), GetPhoneUserAppListResponse.class);
            if (getPhoneUserAppListResponse != null) {
                bx.a(this.f2815a, false, this.f2815a.f(), getPhoneUserAppListResponse, false);
            } else {
                this.f2815a.b(false);
            }
        }
    }
}
