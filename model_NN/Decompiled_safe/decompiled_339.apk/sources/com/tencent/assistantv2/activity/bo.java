package com.tencent.assistantv2.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXRefreshGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.model.e;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistantv2.a.b;
import com.tencent.assistantv2.adapter.smartlist.GameListPageAdapter;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.component.banner.floatheader.FloatBannerViewSwitcher;
import com.tencent.assistantv2.manager.a;
import com.tencent.assistantv2.model.a.f;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.k;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class bo extends ay implements ITXRefreshListViewListener, f {
    b P;
    boolean S = true;
    private final String T = "GameTabActivity:";
    private LinearLayout U;
    private TXRefreshGetMoreListView V;
    private ViewStub W;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage X = null;
    /* access modifiers changed from: private */
    public LoadingView Y;
    /* access modifiers changed from: private */
    public a Z = new a();
    /* access modifiers changed from: private */
    public GameListPageAdapter aa = null;
    /* access modifiers changed from: private */
    public SmartListAdapter.BannerType ab = SmartListAdapter.BannerType.None;
    /* access modifiers changed from: private */
    public FloatBannerViewSwitcher ac;
    /* access modifiers changed from: private */
    public ViewStub ad;
    /* access modifiers changed from: private */
    public boolean ae = false;

    public void d(Bundle bundle) {
        super.d(bundle);
        Log.d("YYB5_0", "GameTabActivity:onCreate------------3");
        this.U = new LinearLayout(this.Q);
        this.U.addView(new ImageView(this.Q), new LinearLayout.LayoutParams(-1, -1));
        a(this.U);
        this.Z.a(this);
    }

    private void b(View view) {
        this.V = (TXRefreshGetMoreListView) view.findViewById(R.id.list);
        this.aa = new GameListPageAdapter(this.Q, this.V, this.Z.d());
        this.aa.a(true);
        this.V.setAdapter(this.aa);
        this.V.setRefreshListViewListener(this);
        this.V.setListSelector(17170445);
        this.V.setVisibility(8);
        this.V.setDivider(null);
        bu buVar = new bu(this, null);
        this.V.setIScrollerListener(buVar);
        this.aa.a(buVar);
        this.W = (ViewStub) view.findViewById(R.id.error_stub);
        this.Y = (LoadingView) view.findViewById(R.id.loading);
        this.ac = (FloatBannerViewSwitcher) view.findViewById(R.id.float_banner_switcher);
        this.ac.a(this.V.getListView(), SmartListAdapter.SmartListType.GamePage.ordinal());
        this.ad = (ViewStub) view.findViewById(R.id.treasurebox_stub);
        this.P = new b();
    }

    private void A() {
        XLog.d("leobi", "initFirstPage=====================");
        k.a((int) STConst.ST_PAGE_GAME_POPULAR, CostTimeSTManager.TIMETYPE.START, System.currentTimeMillis());
        this.Z.a();
        F();
    }

    private void b(int i) {
        if (this.X == null) {
            B();
        }
        this.X.setErrorType(i);
        if (this.V != null) {
            this.V.setVisibility(8);
        }
        this.X.setVisibility(0);
    }

    private void B() {
        this.W.inflate();
        this.X = (NormalErrorRecommendPage) h().findViewById(R.id.error);
        this.X.setButtonClickListener(new bp(this));
        this.X.setIsAutoLoading(true);
    }

    public void d(boolean z) {
        if (this.S) {
            this.S = false;
            this.U.removeAllViews();
            View inflate = this.R.inflate((int) R.layout.act4, (ViewGroup) null);
            this.U.addView(inflate);
            this.U.requestLayout();
            this.U.forceLayout();
            this.U.invalidate();
            b(inflate);
            A();
        } else {
            this.Z.e();
        }
        if (!this.ae) {
            ba.a().post(new bq(this));
        }
        if (this.aa != null) {
            this.aa.l();
            this.aa.notifyDataSetChanged();
        }
        if (this.ac != null) {
            this.ac.f();
        }
        if (this.ae) {
            ba.a().post(new br(this));
        } else {
            ba.a().postDelayed(new bs(this), 2500);
        }
        Log.d("YYB5_0", "GameTabActivity:onResume------------3");
    }

    private void F() {
        TemporaryThreadManager.get().start(new bt(this));
    }

    public void k() {
        super.k();
        if (this.aa != null) {
            this.aa.k();
        }
        if (this.ac != null) {
            this.ac.g();
        }
        Log.d("YYB5_0", "GameTabActivity:onPause------------3");
    }

    public int J() {
        return STConst.ST_PAGE_GAME_POPULAR;
    }

    public bo() {
        super(MainActivity.i());
    }

    public void C() {
        if (this.aa != null) {
            this.aa.k();
        }
        Log.e("YYB5_0", "GameTabActivity:onPageTurnBackground------------3:::");
    }

    public int D() {
        return 0;
    }

    public int E() {
        return 4;
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (TXScrollViewBase.ScrollState.ScrollState_FromEnd == scrollState) {
            this.Z.b();
        }
        if (TXScrollViewBase.ScrollState.ScrollState_FromStart == scrollState) {
            this.Z.c();
        }
    }

    public void a(int i, int i2, boolean z, byte[] bArr, boolean z2, ArrayList<ColorCardItem> arrayList, List<e> list) {
        if (this.Y != null) {
            this.Y.setVisibility(8);
        }
        if (i2 == 0 || i == -1) {
            if (this.X != null) {
                this.X.setVisibility(8);
            }
            this.V.setVisibility(0);
            if (list == null || list.size() == 0) {
                k.a((int) STConst.ST_PAGE_GAME_POPULAR, CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
                b(10);
                return;
            }
            if (arrayList != null) {
                Iterator<ColorCardItem> it = arrayList.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    ColorCardItem next = it.next();
                    if (next.c.equals("礼包")) {
                        next.setTag(new Object());
                        break;
                    }
                }
            }
            this.aa.a(z2, list, (List<com.tencent.assistantv2.component.banner.f>) null, arrayList);
            if (z2) {
                this.ac.a(arrayList);
                if (i == -1) {
                    k.a((int) STConst.ST_PAGE_GAME_POPULAR, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                    this.V.onRefreshComplete(false, z, a((int) R.string.refresh_fail));
                    return;
                }
                k.a((int) STConst.ST_PAGE_GAME_POPULAR, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                this.V.onRefreshComplete(true, z, null);
                return;
            }
            this.V.onRefreshComplete(z, true);
        } else if (z2) {
            k.a((int) STConst.ST_PAGE_GAME_POPULAR, CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
            if (-800 == i2) {
                b(30);
            } else {
                b(20);
            }
        } else {
            this.V.onRefreshComplete(z, false);
        }
    }

    public void I() {
        M();
    }
}
