package com.tencent.assistantv2.activity;

import com.tencent.assistantv2.component.appdetail.b;

/* compiled from: ProGuard */
class r implements b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f2860a;

    r(AppDetailActivityV5 appDetailActivityV5) {
        this.f2860a = appDetailActivityV5;
    }

    public void a(int i) {
        this.f2860a.L.setPagerHeight(i);
        this.f2860a.t = i;
    }

    public void b(int i) {
        if (this.f2860a.Q()) {
            this.f2860a.A.getInnerScrollView().scrollDeltaY(-i);
            return;
        }
        this.f2860a.bi.sendMessageDelayed(this.f2860a.bi.obtainMessage(2, i, 0), 50);
    }
}
