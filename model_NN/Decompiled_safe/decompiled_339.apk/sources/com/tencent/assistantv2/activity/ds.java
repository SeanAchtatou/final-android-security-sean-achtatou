package com.tencent.assistantv2.activity;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.component.search.SearchResultTabPagesBase;

/* compiled from: ProGuard */
class ds implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchResultTabPagesBase.TabType f2842a;
    final /* synthetic */ String b;
    final /* synthetic */ SimpleAppModel c;
    final /* synthetic */ SearchActivity d;

    ds(SearchActivity searchActivity, SearchResultTabPagesBase.TabType tabType, String str, SimpleAppModel simpleAppModel) {
        this.d = searchActivity;
        this.f2842a = tabType;
        this.b = str;
        this.c = simpleAppModel;
    }

    public void run() {
        try {
            if (this.f2842a != SearchResultTabPagesBase.TabType.NONE) {
                this.d.y.a(this.f2842a, this.b, this.d.K, this.c);
            } else {
                this.d.y.a(this.b, this.d.K, this.c);
            }
        } catch (Throwable th) {
        }
    }
}
