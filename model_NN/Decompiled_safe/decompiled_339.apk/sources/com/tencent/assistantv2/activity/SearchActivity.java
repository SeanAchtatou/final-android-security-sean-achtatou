package com.tencent.assistantv2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.ShareBaseActivity;
import com.tencent.assistant.adapter.SearchMatchAdapter;
import com.tencent.assistant.b.a;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.link.b;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.aa;
import com.tencent.assistant.module.ai;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.AdvancedHotWord;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistantv2.component.TabBarView;
import com.tencent.assistantv2.component.search.SearchBarView;
import com.tencent.assistantv2.component.search.SearchHotwordsView;
import com.tencent.assistantv2.component.search.SearchResultTabPagesBase;
import com.tencent.assistantv2.component.search.g;
import com.tencent.assistantv2.component.search.m;
import com.tencent.assistantv2.model.b.f;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class SearchActivity extends ShareBaseActivity implements NetworkMonitor.ConnectivityChangeListener {
    /* access modifiers changed from: private */
    public static int I = 30;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage A;
    /* access modifiers changed from: private */
    public LoadingView B;
    private aa C;
    /* access modifiers changed from: private */
    public ai D;
    private dv E = new dv(this, null);
    /* access modifiers changed from: private */
    public int F = 0;
    /* access modifiers changed from: private */
    public boolean G = true;
    /* access modifiers changed from: private */
    public Handler H = null;
    private final int J = EventDispatcherEnum.PLUGIN_EVENT_LOGIN_START;
    /* access modifiers changed from: private */
    public int K = STConst.ST_PAGE_SEARCH;
    /* access modifiers changed from: private */
    public int L = 0;
    /* access modifiers changed from: private */
    public String M = null;
    /* access modifiers changed from: private */
    public String N = null;
    private ApkResCallback.Stub O = new df(this);
    private boolean P = false;
    /* access modifiers changed from: private */
    public boolean Q = false;
    /* access modifiers changed from: private */
    public int R = 0;
    private TextWatcher S = new dq(this);
    private View.OnClickListener T = new dr(this);
    private View.OnClickListener U = new dt(this);
    private View.OnClickListener V = new du(this);
    private g W = new dh(this);
    private View.OnClickListener X = new di(this);
    private View.OnClickListener Y = new dj(this);
    private View.OnKeyListener Z = new dk(this);
    private View.OnClickListener aa = new dm(this);
    private APN ab = APN.NO_NETWORK;
    boolean n = false;
    private final String t = "SearchActivity";
    /* access modifiers changed from: private */
    public SearchBarView u;
    /* access modifiers changed from: private */
    public SearchHotwordsView v;
    /* access modifiers changed from: private */
    public View w;
    private ListView x;
    /* access modifiers changed from: private */
    public m y;
    /* access modifiers changed from: private */
    public SearchMatchAdapter z;

    /* compiled from: ProGuard */
    enum Layer {
        Search,
        Result
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            setContentView((int) R.layout.act_search_layout);
            this.H = new dn(this);
            v();
            w();
            cq.a().a((NetworkMonitor.ConnectivityChangeListener) this);
        } catch (Throwable th) {
            this.P = true;
            finish();
            cq.a().b();
        }
    }

    private void v() {
        this.D = new ai();
        this.C = aa.a();
    }

    private void w() {
        this.A = (NormalErrorRecommendPage) findViewById(R.id.network_error);
        if (this.A != null) {
            this.A.setButtonClickListener(this.aa);
        }
        this.B = (LoadingView) findViewById(R.id.loading_view);
        this.u = (SearchBarView) findViewById(R.id.search_bar);
        this.u.a(this.S);
        this.u.a(this.T);
        this.u.b(this.U);
        this.u.c(this.Y);
        this.u.setOnKeyListener(this.Z);
        this.w = findViewById(R.id.search_scroll_view);
        this.v = (SearchHotwordsView) findViewById(R.id.search_hot_view);
        this.v.a(this.W);
        this.v.a(this.X);
        this.v.b(this.V);
        this.v.setVisibility(8);
        this.w.setVisibility(8);
        this.v.setOnClickListener(new Cdo(this));
        this.x = (ListView) findViewById(R.id.match_list);
        this.z = new SearchMatchAdapter(this, this.x, this.V);
        this.z.a((int) STConst.ST_PAGE_SEARCH_SUGGEST);
        this.x.setAdapter((ListAdapter) this.z);
        this.x.setDivider(null);
        this.y = new m(this, (TabBarView) findViewById(R.id.search_result_tab_bar), (ViewPager) findViewById(R.id.search_result_tab_content));
        this.B.setVisibility(0);
        String stringExtra = getIntent().getStringExtra("com.tencent.assistant.KEYWORD");
        this.K = getIntent().getIntExtra("com.tencent.assistant.SOURCESCENE", this.K);
        this.M = getIntent().getStringExtra(a.Y);
        this.N = getIntent().getStringExtra(a.Z);
        this.y.d(0);
        this.L = getIntent().getIntExtra(a.aa, 0);
        if (!TextUtils.isEmpty(this.M)) {
            this.u.a(this.M);
        }
        if (stringExtra == null || stringExtra.length() <= 0 || TextUtils.isEmpty(stringExtra.trim())) {
            C();
        } else {
            this.n = true;
            this.G = false;
            String b = b(stringExtra);
            if (!TextUtils.isEmpty(b)) {
                stringExtra = b;
            }
            if (stringExtra.length() > I) {
                stringExtra = stringExtra.substring(0, I);
            }
            this.u.a().setText(stringExtra);
            this.u.a().setSelection(stringExtra.length());
            F();
            this.y.a(b, this.K, (SimpleAppModel) null);
        }
        y();
        x();
    }

    private void x() {
        View findViewById = getWindow().getDecorView().findViewById(R.id.search_layout);
        findViewById.getViewTreeObserver().addOnGlobalLayoutListener(new dp(this, findViewById));
    }

    private String b(String str) {
        int indexOf;
        if (!TextUtils.isEmpty(str) && (indexOf = str.indexOf("：")) > 0 && indexOf < str.length()) {
            return str.substring(indexOf + 1);
        }
        return str;
    }

    /* access modifiers changed from: private */
    public void y() {
        XLog.d("SearchActivity", "loadHotWords:" + this.L);
        f b = this.C.b(this.L);
        if (b != null) {
            XLog.d("SearchActivity", "loadHotWords:" + this.L + ":" + b.b());
            this.v.a(b);
            return;
        }
        XLog.d("SearchActivity", "loadHotWords:model == null");
        this.C.b();
    }

    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (!this.P) {
            setIntent(intent);
            int intExtra = getIntent().getIntExtra(a.ab, -1);
            if (intExtra != -1) {
                this.y.b(intExtra);
                return;
            }
            v();
            w();
        }
    }

    public void onResume() {
        super.onResume();
        if (!this.P) {
            XLog.d("SearchActivity", "onResume");
            this.D.register(this.E);
            this.C.register(this.E);
            this.y.h();
            this.z.a();
            this.z.notifyDataSetChanged();
            this.v.a();
            ApkResourceManager.getInstance().registerApkResCallback(this.O);
            if (this.x.getVisibility() == 0 || this.v.getVisibility() == 0) {
                B();
            } else {
                A();
            }
        }
    }

    public void onPause() {
        XLog.d("SearchActivity", "onPause");
        if (this.P) {
            super.onPause();
            return;
        }
        A();
        this.z.b();
        this.y.g();
        ApkResourceManager.getInstance().unRegisterApkResCallback(this.O);
        super.onPause();
    }

    public void onDestroy() {
        if (this.P) {
            super.onDestroy();
            return;
        }
        this.C.unregister(this.E);
        this.D.a();
        this.D.unregister(this.E);
        this.y.i();
        this.z.c();
        this.H.removeMessages(0);
        this.H = null;
        this.A.destory();
        this.A = null;
        try {
            Class.forName("android.view.inputmethod.InputMethodManager").getMethod("wind‌​owDismissed", IBinder.class).invoke(null, this.u.a().getWindowToken());
        } catch (Exception e) {
        }
        this.u = null;
        cq.a().b(this);
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void z() {
        a(SearchResultTabPagesBase.TabType.NONE, (SimpleAppModel) null);
    }

    /* access modifiers changed from: private */
    public void a(SimpleAppModel simpleAppModel) {
        a(SearchResultTabPagesBase.TabType.b, simpleAppModel);
    }

    private void a(SearchResultTabPagesBase.TabType tabType, SimpleAppModel simpleAppModel) {
        A();
        String obj = this.u.a().getText().toString();
        if (!TextUtils.isEmpty(obj)) {
            if (f() != this.y.b()) {
                a(q(), (String) null, (String) null);
            }
            F();
            ba.a().postDelayed(new ds(this, tabType, obj, simpleAppModel), 300);
        }
    }

    /* access modifiers changed from: private */
    public void a(AdvancedHotWord advancedHotWord) {
        this.K = STConst.ST_PAGE_SEARCH_HOTWORDS;
        A();
        this.n = true;
        String str = advancedHotWord.f1971a;
        if (advancedHotWord.b == 2 && advancedHotWord.d != null) {
            str = advancedHotWord.d.f.b;
        }
        if (!TextUtils.isEmpty(str)) {
            this.u.a().setText(str);
            this.u.a().setSelection(str.length() > I ? I : str.length());
            this.w.setVisibility(8);
            this.v.setVisibility(8);
            a(SearchResultTabPagesBase.TabType.b, (SimpleAppModel) null);
        }
    }

    /* access modifiers changed from: private */
    public void b(AdvancedHotWord advancedHotWord) {
        if (!TextUtils.isEmpty(advancedHotWord.f)) {
            Bundle bundle = new Bundle();
            bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
            b.a(this, advancedHotWord.f, bundle);
        }
    }

    /* access modifiers changed from: private */
    public void c(AdvancedHotWord advancedHotWord) {
        if (!TextUtils.isEmpty(advancedHotWord.f)) {
            Bundle bundle = new Bundle();
            bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
            b.a(this, advancedHotWord.f, bundle);
        }
    }

    /* access modifiers changed from: private */
    public void A() {
        if (this.u != null && this.u.a() != null) {
            EditText a2 = this.u.a();
            a2.clearFocus();
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(a2.getWindowToken(), 2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void B() {
        if (this.u != null) {
            this.u.a().requestFocus();
            ba.a().postDelayed(new dl(this), 500);
        }
    }

    /* access modifiers changed from: private */
    public void c(int i) {
        this.x.setVisibility(i);
        this.B.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void d(int i) {
        this.B.setVisibility(8);
        this.y.e(8);
        c(8);
        this.w.setVisibility(8);
        this.v.setVisibility(8);
        this.A.setVisibility(0);
        this.A.setErrorType(i);
        getWindow().setSoftInputMode(32);
    }

    /* access modifiers changed from: private */
    public void C() {
        this.B.setVisibility(8);
        this.y.e(8);
        c(8);
        this.w.setVisibility(0);
        this.v.setVisibility(0);
        this.A.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void D() {
        i();
        this.B.setVisibility(8);
        this.y.e(8);
        c(0);
        this.w.setVisibility(8);
        this.v.setVisibility(8);
        this.A.setVisibility(8);
        h();
    }

    private void F() {
        this.B.setVisibility(8);
        this.y.e(0);
        c(8);
        this.w.setVisibility(8);
        this.v.setVisibility(8);
        this.A.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void G() {
        this.B.setVisibility(8);
        this.y.e(8);
        c(8);
        this.w.setVisibility(8);
        this.v.setVisibility(8);
        this.A.setVisibility(8);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || TextUtils.isEmpty(this.u.a().getText().toString())) {
            a("001", 200);
            return super.onKeyDown(i, keyEvent);
        }
        this.u.a().setText(Constants.STR_EMPTY);
        this.u.a().setSelection(0);
        return true;
    }

    /* access modifiers changed from: private */
    public Layer H() {
        if (this.y == null || this.y.j() != 0) {
            return Layer.Search;
        }
        return Layer.Result;
    }

    public int f() {
        if (this.x != null && this.x.getVisibility() == 0) {
            return STConst.ST_PAGE_SEARCH_SUGGEST;
        }
        if (this.y == null || this.y.j() != 0) {
            return STConst.ST_PAGE_SEARCH;
        }
        return this.y.b();
    }

    public boolean g() {
        return true;
    }

    public void i() {
        a(q(), (String) null, (String) null);
    }

    /* access modifiers changed from: private */
    public void a(String str, int i) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, i);
        buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a("03", str);
        buildSTInfo.extraData = Constants.STR_EMPTY;
        k.a(buildSTInfo);
    }

    public void onConnected(APN apn) {
        String str = null;
        if (this.u != null) {
            str = this.u.b();
        }
        if (H() == Layer.Search) {
            XLog.d("SearchActivity", "onConnected:Layer.Search");
            if (str == null || TextUtils.isEmpty(str.trim())) {
                if (this.C != null) {
                    f b = this.C.b(this.L);
                    if (b == null || b.b() <= 0) {
                        this.C.b();
                    }
                }
            } else if ((this.z == null || this.z.getCount() <= 0) && this.D != null) {
                this.D.a();
                this.D.a(str);
            }
        } else if (H() == Layer.Result && this.y != null) {
            this.y.a(str, this.K);
        }
    }

    public void onDisconnected(APN apn) {
        this.ab = apn;
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        this.ab = apn2;
    }
}
