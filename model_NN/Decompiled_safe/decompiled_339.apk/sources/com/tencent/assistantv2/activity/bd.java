package com.tencent.assistantv2.activity;

import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.c.b;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.invalidater.TXRefreshGetMoreListViewScrollListener;
import com.tencent.assistant.component.treasurebox.AppTreasureEntryBlinkEyesView;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXRefreshGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.model.e;
import com.tencent.assistant.model.r;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistantv2.adapter.smartlist.DiscoverPageListAdapter;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.b.ad;
import com.tencent.assistantv2.component.topbanner.a;
import com.tencent.assistantv2.manager.f;
import com.tencent.assistantv2.model.a.j;
import com.tencent.assistantv2.st.business.StartUpCostTimeSTManager;
import com.tencent.assistantv2.st.k;
import java.util.List;

/* compiled from: ProGuard */
public class bd extends ay implements ITXRefreshListViewListener, UIEventListener, j {
    b P = new b();
    a S = new bk(this);
    private final String T = "FoundTabActivity";
    /* access modifiers changed from: private */
    public TXRefreshGetMoreListView U;
    private ViewStub V;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage W = null;
    /* access modifiers changed from: private */
    public LoadingView X;
    /* access modifiers changed from: private */
    public f Y = f.a();
    /* access modifiers changed from: private */
    public SmartListAdapter Z = null;
    /* access modifiers changed from: private */
    public ad aa = null;
    private boolean ab = false;
    private boolean ac = false;
    private TXRefreshGetMoreListViewScrollListener ad = new TXRefreshGetMoreListViewScrollListener();
    private LinearLayout ae;
    /* access modifiers changed from: private */
    public boolean af = false;
    /* access modifiers changed from: private */
    public ViewStub ag;
    private AppTreasureEntryBlinkEyesView ah;
    private TextView ai;

    public void d(Bundle bundle) {
        super.d(bundle);
        k.a(StartUpCostTimeSTManager.TIMETYPE.B14_TIME, System.currentTimeMillis(), 0);
        System.currentTimeMillis();
        this.Y.a(this);
        this.ae = new LinearLayout(this.Q);
        ImageView imageView = new ImageView(this.Q);
        imageView.setBackgroundColor(this.Q.getResources().getColor(R.color.rank_navigation_bg));
        this.ae.addView(imageView, new LinearLayout.LayoutParams(-1, -1));
        a(this.ae);
    }

    public void b(View view) {
        this.U = (TXRefreshGetMoreListView) view.findViewById(R.id.list);
        this.U.setRefreshListViewListener(this);
        this.U.setVisibility(8);
        this.U.setDivider(null);
        this.U.setListSelector(17170445);
        this.V = (ViewStub) view.findViewById(R.id.error_stub);
        this.X = (LoadingView) view.findViewById(R.id.loading);
        this.U.setIScrollerListener(this.ad);
        this.ag = (ViewStub) view.findViewById(R.id.treasurebox_stub);
    }

    private void A() {
        k.a(StartUpCostTimeSTManager.TIMETYPE.B15_TIME, System.currentTimeMillis(), 0);
        this.Z = new DiscoverPageListAdapter(this.Q, this.U, this.Y.l());
        this.U.setAdapter(this.Z);
        this.Z.a(true);
        this.Z.a(this.ad);
        this.Z.a(this.S);
        boolean g = this.Y.g();
        int c = this.Y.c();
        if (g && this.Z != null) {
            this.ab = true;
            ba.a().postDelayed(new be(this), 300);
        } else if (c != 0) {
            b(30);
        } else {
            this.Y.b();
        }
    }

    private void b(int i) {
        if (this.W == null) {
            B();
        }
        this.W.setErrorType(i);
        if (this.U != null) {
            this.U.setVisibility(8);
        }
        this.X.setVisibility(8);
        this.W.setVisibility(0);
        e(false);
    }

    private void B() {
        this.V.inflate();
        this.W = (NormalErrorRecommendPage) this.ae.findViewById(R.id.error);
        this.W.setButtonClickListener(new bf(this));
    }

    public void d(boolean z) {
        XLog.d("YYB5_0", "found onResume");
        if (!this.ac) {
            this.ac = true;
            this.ae.removeAllViews();
            View inflate = this.R.inflate((int) R.layout.act2, (ViewGroup) null);
            this.ae.addView(inflate);
            b(inflate);
            A();
            ba.a().postDelayed(new bg(this), 2000);
        } else if (this.Y != null) {
            this.Y.m();
        }
        if (this.Z != null) {
            if (this.Z.n()) {
                this.Z.b(true);
            }
            this.Z.notifyDataSetChanged();
        }
        if (this.Z != null && !this.Z.i() && this.Z.j()) {
            if (this.X != null) {
                this.X.setVisibility(8);
            }
            b(30);
        }
        if (this.P.a() == null) {
            return;
        }
        if (this.af) {
            ba.a().post(new bh(this));
        } else {
            ba.a().postDelayed(new bi(this), 2500);
        }
    }

    public void k() {
        super.k();
    }

    public void n() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_SHOW_TREASURE_BOX_ENTRY, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_DISMISS_TREASURE_BOX_ENTRY, this);
        super.n();
    }

    public void i() {
        super.i();
    }

    public bd() {
        super(MainActivity.i());
    }

    public void C() {
    }

    public int D() {
        return 0;
    }

    public int E() {
        return 1;
    }

    public int J() {
        return STConst.ST_PAGE_COMPETITIVE;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.manager.f.a(boolean, com.tencent.assistantv2.b.ad):void
     arg types: [int, com.tencent.assistantv2.b.ad]
     candidates:
      com.tencent.assistantv2.manager.f.a(com.tencent.assistantv2.manager.f, int):int
      com.tencent.assistantv2.manager.f.a(com.tencent.assistantv2.manager.f, long):long
      com.tencent.assistantv2.manager.f.a(com.tencent.assistantv2.manager.f, java.util.List):java.util.List
      com.tencent.assistantv2.manager.f.a(com.tencent.assistant.protocol.jce.TopBanner, long):void
      com.tencent.assistantv2.manager.f.a(boolean, com.tencent.assistantv2.b.ad):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.manager.f.a(boolean, com.tencent.assistantv2.b.ad):void
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.assistantv2.manager.f.a(com.tencent.assistantv2.manager.f, int):int
      com.tencent.assistantv2.manager.f.a(com.tencent.assistantv2.manager.f, long):long
      com.tencent.assistantv2.manager.f.a(com.tencent.assistantv2.manager.f, java.util.List):java.util.List
      com.tencent.assistantv2.manager.f.a(com.tencent.assistant.protocol.jce.TopBanner, long):void
      com.tencent.assistantv2.manager.f.a(boolean, com.tencent.assistantv2.b.ad):void */
    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (TXScrollViewBase.ScrollState.ScrollState_FromEnd == scrollState) {
            this.Y.a(true, this.aa);
        }
        if (TXScrollViewBase.ScrollState.ScrollState_FromStart == scrollState) {
            this.Y.a(false, (ad) null);
            this.ab = false;
        }
    }

    public void a(int i, int i2, boolean z, ad adVar, boolean z2, List<com.tencent.assistantv2.component.banner.f> list, r rVar, List<e> list2) {
        k.a(StartUpCostTimeSTManager.TIMETYPE.B10_TIME, System.currentTimeMillis(), 0);
        if (this.X != null) {
            this.X.setVisibility(8);
        }
        XLog.d("icerao", "on page dataload.errorcode:" + i2 + ",data size:" + (list2 != null ? list2.size() : 0) + ",is firstpage:" + z2);
        if (i == -1 || i2 == 0) {
            if (this.W != null) {
                this.W.setVisibility(8);
            }
            this.U.setVisibility(0);
            this.aa = adVar;
            e(true);
            if (list2 == null || list2.size() == 0) {
                if (!z2) {
                    this.U.onRefreshComplete(z, false);
                } else if (!this.Z.i()) {
                    b(30);
                } else {
                    this.U.onRefreshComplete(false, z, a((int) R.string.refresh_fail));
                }
            } else if (!z2) {
                this.U.onRefreshComplete(z, true);
                this.Z.a(false, list2, list, rVar, null, this.Y.n());
            } else if (this.ab) {
                this.ab = false;
            } else {
                this.Z.a(true, list2, list, rVar, null, this.Y.n());
                k.a(StartUpCostTimeSTManager.TIMETYPE.END, System.currentTimeMillis(), 0);
                XLog.d("qt4atest", "TIMETYPE.END");
                this.U.setSelection(0);
                if (i != -1) {
                    this.U.onRefreshComplete(true, z, null);
                } else if (i2 == -800) {
                    this.U.onRefreshComplete(false, z, a((int) R.string.refresh_fail_network));
                } else {
                    this.U.onRefreshComplete(false, z, a((int) R.string.refresh_fail));
                }
            }
        } else if (z2) {
            k.a(StartUpCostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis(), 0);
            if (!this.Z.i()) {
                if (i2 == -800) {
                    b(30);
                } else {
                    b(20);
                }
            } else if (i2 == -800) {
                this.U.onRefreshComplete(false, z, a((int) R.string.refresh_fail_network));
            } else {
                this.U.onRefreshComplete(false, z, a((int) R.string.refresh_fail));
            }
        } else {
            this.U.onRefreshComplete(z, false);
        }
    }

    /* access modifiers changed from: private */
    public boolean F() {
        MainActivity mainActivity = null;
        if (this.Q instanceof MainActivity) {
            mainActivity = (MainActivity) this.Q;
        }
        if (mainActivity == null) {
            return false;
        }
        Bundle j = mainActivity.j();
        return j != null ? j.getInt("param_competitive_tab_show_treasure_box_entry") == 1 : false;
    }

    /* access modifiers changed from: private */
    public void e(boolean z) {
        try {
            if (this.ag == null) {
                return;
            }
            if (z) {
                this.ag.setVisibility(0);
            } else {
                this.ag.setVisibility(8);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_SHOW_TREASURE_BOX_ENTRY:
                this.ah.setVisibility(0);
                this.ai.setVisibility(0);
                return;
            case EventDispatcherEnum.UI_EVENT_DISMISS_TREASURE_BOX_ENTRY:
                this.ah.setVisibility(8);
                this.ai.setVisibility(8);
                TemporaryThreadManager.get().start(new bj(this));
                return;
            default:
                return;
        }
    }

    public void I() {
        M();
    }
}
