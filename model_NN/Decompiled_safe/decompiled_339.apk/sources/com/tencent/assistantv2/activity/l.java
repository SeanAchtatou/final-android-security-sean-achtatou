package com.tencent.assistantv2.activity;

import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistantv2.component.appdetail.AppDetailViewV5;
import com.tencent.connect.common.Constants;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
class l extends ViewPageScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f2854a;

    l(AppDetailActivityV5 appDetailActivityV5) {
        this.f2854a = appDetailActivityV5;
    }

    public void onPageSelected(int i) {
        this.f2854a.P.selectTab(i, true);
        int unused = this.f2854a.R = i;
        this.f2854a.c(i + 1);
        if (!this.f2854a.M && this.f2854a.R == 1) {
            this.f2854a.M();
        }
        if (this.f2854a.R == 0) {
            this.f2854a.bb.sendEmptyMessage(3);
        }
        if (this.f2854a.R == 2) {
            if (this.f2854a.L != null) {
                this.f2854a.L.hideBar();
            }
            if (!(this.f2854a.n == null || this.f2854a.ae == null || this.f2854a.ae.f1660a == null)) {
                this.f2854a.n.setPageHeight(this.f2854a.t);
                if (this.f2854a.n.loadUrl(this.f2854a.ae.f1660a.g)) {
                }
            }
        }
        this.f2854a.b(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.AppDetailActivityV5.a(com.tencent.assistantv2.activity.AppDetailActivityV5, boolean):boolean
     arg types: [com.tencent.assistantv2.activity.AppDetailActivityV5, int]
     candidates:
      com.tencent.assistantv2.activity.AppDetailActivityV5.a(com.tencent.assistantv2.activity.AppDetailActivityV5, int):int
      com.tencent.assistantv2.activity.AppDetailActivityV5.a(com.tencent.assistantv2.activity.AppDetailActivityV5, com.tencent.assistant.localres.model.LocalApkInfo):com.tencent.assistant.localres.model.LocalApkInfo
      com.tencent.assistantv2.activity.AppDetailActivityV5.a(com.tencent.assistant.model.c, com.tencent.assistant.model.SimpleAppModel):com.tencent.assistant.model.ShareAppModel
      com.tencent.assistantv2.activity.AppDetailActivityV5.a(com.tencent.assistantv2.activity.AppDetailActivityV5, com.tencent.assistant.model.c):com.tencent.assistant.model.c
      com.tencent.assistantv2.activity.AppDetailActivityV5.a(int, com.tencent.assistant.protocol.jce.GetRecommendAppListResponse):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistantv2.activity.AppDetailActivityV5.a(com.tencent.assistantv2.activity.AppDetailActivityV5, boolean):boolean */
    public void handleMessage(ViewInvalidateMessage viewInvalidateMessage) {
        String str;
        super.handleMessage(viewInvalidateMessage);
        switch (viewInvalidateMessage.what) {
            case 1:
                if (!this.f2854a.aW) {
                    this.f2854a.u[1] = viewInvalidateMessage;
                    return;
                }
                this.f2854a.f(viewInvalidateMessage.arg1);
                this.f2854a.u[1] = null;
                return;
            case 2:
                if (!this.f2854a.aW) {
                    this.f2854a.u[2] = viewInvalidateMessage;
                    return;
                }
                int i = viewInvalidateMessage.arg1;
                HashMap hashMap = (HashMap) viewInvalidateMessage.params;
                boolean booleanValue = ((Boolean) hashMap.get("hasNext")).booleanValue();
                byte[] bArr = (byte[]) hashMap.get("pageContext");
                if (i == 0 && (this.f2854a.aV & 2) == 0) {
                    this.f2854a.a(i, (List) hashMap.get("authorModel"), booleanValue, bArr);
                } else {
                    this.f2854a.a(i, (List) null, booleanValue, (byte[]) null);
                }
                this.f2854a.u[2] = null;
                return;
            case 3:
                if (this.f2854a.H != null && this.f2854a.ar) {
                    this.f2854a.H.d();
                    this.f2854a.H.requestLayout();
                    boolean unused = this.f2854a.ar = false;
                    return;
                }
                return;
            case 4:
                this.f2854a.H.a(((HashMap) this.f2854a.u[4].params).get("hasNext").toString(), (ArrayList) ((HashMap) this.f2854a.u[4].params).get("pageContext"));
                this.f2854a.u[4] = null;
                return;
            case 5:
                HashMap hashMap2 = (HashMap) this.f2854a.u[5].params;
                this.f2854a.H.b().a((ArrayList) hashMap2.get("selectedCommentList"), (ArrayList) hashMap2.get("goodTagList"));
                this.f2854a.u[5] = null;
                return;
            case 6:
                ArrayList arrayList = (ArrayList) ((HashMap) this.f2854a.u[6].params).get("pageContext");
                this.f2854a.H.b().a((List) ((HashMap) this.f2854a.u[6].params).get("hasNext"), this.f2854a.ac.f1634a, this.f2854a.ac.c);
                String obj = ((HashMap) this.f2854a.u[6].params).get("pageText").toString();
                if (arrayList.size() >= 2) {
                    this.f2854a.I.a(obj, arrayList, this.f2854a.aV);
                }
                this.f2854a.u[6] = null;
                return;
            case 7:
                int intValue = Integer.decode(this.f2854a.u[7].params.toString()).intValue();
                if (AppDetailActivityV5.a(this.f2854a.ae) && AppDetailViewV5.a(this.f2854a.ae, this.f2854a.ac) == AppDetailViewV5.APPDETAIL_MODE.NEED_UPDATE && this.f2854a.ae.f1660a.f1993a.b.get(0).F > 10000) {
                    BigDecimal scale = new BigDecimal(((double) this.f2854a.ae.f1660a.f1993a.b.get(0).F) / 10000.0d).setScale(1, 4);
                    str = String.format(this.f2854a.getResources().getString(R.string.appdetail_friends_update), scale);
                } else if (intValue > 0) {
                    str = String.format(this.f2854a.getResources().getString(R.string.appdetail_friends_using), Integer.valueOf(intValue));
                } else {
                    str = Constants.STR_EMPTY;
                }
                this.f2854a.F.a(str);
                this.f2854a.u[7] = null;
                return;
            default:
                return;
        }
    }
}
