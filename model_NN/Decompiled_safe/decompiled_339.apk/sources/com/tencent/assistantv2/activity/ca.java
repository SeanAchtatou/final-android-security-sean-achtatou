package com.tencent.assistantv2.activity;

import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.m;
import com.tencent.assistant.module.timer.RecoverAppListReceiver;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.bh;
import com.tencent.assistantv2.model.a.i;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class ca implements i {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GuideActivity f2798a;

    public ca(GuideActivity guideActivity) {
        this.f2798a = guideActivity;
    }

    public void a(int i, int i2, GetPhoneUserAppListResponse getPhoneUserAppListResponse) {
        GetPhoneUserAppListResponse unused = this.f2798a.S = getPhoneUserAppListResponse;
        if (i2 == 0 && this.f2798a.S != null) {
            m.a().b(Constants.STR_EMPTY, "key_re_app_list_state", Integer.valueOf(RecoverAppListReceiver.RecoverAppListState.FIRST_SHORT.ordinal()));
            MainActivity.x = RecoverAppListReceiver.RecoverAppListState.FIRST_SHORT.ordinal();
            m.a().a("key_re_app_list_first_response", bh.a(getPhoneUserAppListResponse));
            if (this.f2798a.S.b == 1 || this.f2798a.S.b == 3) {
                boolean unused2 = this.f2798a.V = false;
                this.f2798a.U.setText((int) R.string.guide_btn_des_switchphone);
                this.f2798a.z.setText((int) R.string.guide_btn_go_switchphone);
                this.f2798a.z.setVisibility(0);
                k.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_NO, "04_001", 0, STConst.ST_DEFAULT_SLOT, 100));
                return;
            }
            boolean unused3 = this.f2798a.V = true;
            this.f2798a.U.setText((int) R.string.guide_btn_des_switchphone_skip);
            this.f2798a.z.setText((int) R.string.guide_btn_go_skip);
            this.f2798a.z.setVisibility(0);
            k.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_NO, "04_003", 0, STConst.ST_DEFAULT_SLOT, 100));
        }
    }
}
