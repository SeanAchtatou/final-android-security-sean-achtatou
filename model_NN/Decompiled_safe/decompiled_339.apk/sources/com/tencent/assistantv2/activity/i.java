package com.tencent.assistantv2.activity;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.widget.ImageView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.ShareAppDialog;
import com.tencent.assistant.utils.a;

/* compiled from: ProGuard */
class i implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f2851a;

    i(AppDetailActivityV5 appDetailActivityV5) {
        this.f2851a = appDetailActivityV5;
    }

    public void onCancel(DialogInterface dialogInterface) {
        if (dialogInterface != null) {
            Bitmap animBitmap = ((ShareAppDialog) dialogInterface).getAnimBitmap();
            ImageView imageView = new ImageView(this.f2851a);
            imageView.setImageBitmap(animBitmap);
            a.a(this.f2851a.E, this.f2851a.G.findViewById(R.id.show_more), imageView);
        }
    }
}
