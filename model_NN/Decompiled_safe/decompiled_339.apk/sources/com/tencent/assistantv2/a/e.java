package com.tencent.assistantv2.a;

import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BaseActivity f2741a;
    final /* synthetic */ c b;

    e(c cVar, BaseActivity baseActivity) {
        this.b = cVar;
        this.f2741a = baseActivity;
    }

    public void onClick(View view) {
        if (this.b.c != null) {
            this.b.h();
            this.b.a(true);
            this.b.d();
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f2741a, 200);
            if (buildSTInfo != null) {
                buildSTInfo.scene = STConst.ST_PAGE_GAME_NPC;
                buildSTInfo.slotId = "05_001";
                k.a(buildSTInfo);
            }
        }
    }
}
