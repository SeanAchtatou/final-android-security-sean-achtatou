package com.tencent.assistantv2.st.business;

import com.tencent.assistant.db.table.x;
import com.tencent.assistant.utils.TemporaryThreadManager;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public abstract class e extends BaseSTManagerV2 {

    /* renamed from: a  reason: collision with root package name */
    protected List<byte[]> f3332a;
    /* access modifiers changed from: private */
    public x c;

    protected e() {
        this.c = null;
        this.f3332a = null;
        this.f3332a = new ArrayList(5);
        this.c = x.a();
    }

    public void flush() {
        if (this.f3332a != null && this.f3332a.size() > 0) {
            this.c.a(getSTType(), this.f3332a);
            this.f3332a.clear();
        }
    }

    public void a(byte[] bArr) {
        TemporaryThreadManager.get().start(new f(this, bArr));
    }
}
