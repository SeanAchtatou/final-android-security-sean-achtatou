package com.tencent.assistantv2.st.business;

import android.text.TextUtils;
import com.tencent.assistant.Global;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.model.k;
import com.tencent.assistant.module.wisedownload.m;
import com.tencent.assistant.net.b;
import com.tencent.assistant.protocol.jce.AppChunkDownlaod;
import com.tencent.assistant.protocol.jce.Speed;
import com.tencent.assistant.protocol.jce.StatAppDownlaodWithChunk;
import com.tencent.assistant.protocol.jce.StatWifi;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.f;
import com.tencent.assistant.st.h;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.downloadsdk.ae;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class c extends BaseSTManagerV2 implements ae {

    /* renamed from: a  reason: collision with root package name */
    private static c f3330a = null;
    private ConcurrentHashMap<String, StatAppDownlaodWithChunk> c = new ConcurrentHashMap<>();
    private ArrayList<k> d = new ArrayList<>();

    public static synchronized c a() {
        c cVar;
        synchronized (c.class) {
            if (f3330a == null) {
                f3330a = new c();
            }
            cVar = f3330a;
        }
        return cVar;
    }

    private c() {
    }

    public void a(String str, long j, long j2, byte b, StatInfo statInfo, SimpleDownloadInfo.UIType uIType, SimpleDownloadInfo.DownloadType downloadType) {
        XLog.d("NewDownload", "******** downlaod start scene=" + statInfo.scene + " sourceScene=" + statInfo.sourceScene + " action=" + statInfo.actionId + " slotId=" + statInfo.getFinalSlotId() + " sourceslotId=" + statInfo.sourceSceneSlotId + " extraData=" + statInfo.extraData + " via=" + statInfo.callerVia + " traceId=" + statInfo.traceId + " fileType=" + downloadType + " downloadTicket =" + str + " actionFlag=" + statInfo.d);
        StatAppDownlaodWithChunk statAppDownlaodWithChunk = null;
        if (this.c != null && this.c.containsKey(str)) {
            statAppDownlaodWithChunk = this.c.get(str);
            statAppDownlaodWithChunk.a();
        } else if (this.c != null && !this.c.containsKey(str)) {
            statAppDownlaodWithChunk = a(str, j, j2, b, statInfo, uIType, downloadType, new ArrayList());
        }
        if (statAppDownlaodWithChunk != null && this.c != null) {
            this.c.put(str, statAppDownlaodWithChunk);
        }
    }

    public StatAppDownlaodWithChunk a(String str, long j, long j2, byte b, StatInfo statInfo, SimpleDownloadInfo.UIType uIType, SimpleDownloadInfo.DownloadType downloadType, ArrayList<AppChunkDownlaod> arrayList) {
        StatAppDownlaodWithChunk statAppDownlaodWithChunk = new StatAppDownlaodWithChunk();
        statAppDownlaodWithChunk.f2351a = j;
        statAppDownlaodWithChunk.b = j2;
        statAppDownlaodWithChunk.m = b;
        if (statInfo != null) {
            statAppDownlaodWithChunk.j = statInfo.scene;
            statAppDownlaodWithChunk.c = statInfo.f3356a;
            statAppDownlaodWithChunk.k = statInfo.sourceScene;
            statAppDownlaodWithChunk.l = statInfo.extraData;
            statAppDownlaodWithChunk.s = statInfo.callerVia;
            try {
                statAppDownlaodWithChunk.r = Long.valueOf(statInfo.callerUin).longValue();
                statAppDownlaodWithChunk.M = Integer.valueOf(statInfo.callerVersionCode).intValue();
            } catch (Throwable th) {
            }
            statAppDownlaodWithChunk.t = statInfo.b;
            statAppDownlaodWithChunk.u = statInfo.c;
            statAppDownlaodWithChunk.v = statInfo.d;
            statAppDownlaodWithChunk.w = statInfo.getFinalSlotId();
            statAppDownlaodWithChunk.O = statInfo.sourceSceneSlotId;
            statAppDownlaodWithChunk.A = statInfo.recommendId;
            statAppDownlaodWithChunk.P = statInfo.contentId;
            statAppDownlaodWithChunk.n = statInfo.searchId;
            statAppDownlaodWithChunk.L = statInfo.expatiation;
            statAppDownlaodWithChunk.K = statInfo.searchPreId + "_" + statInfo.searchId;
            statAppDownlaodWithChunk.N = statInfo.rankGroupId;
            statAppDownlaodWithChunk.E = statInfo.pushInfo;
            statAppDownlaodWithChunk.S = statInfo.traceId;
        }
        if (statInfo == null || statInfo.pushId <= 0) {
            statAppDownlaodWithChunk.D = f.b();
        } else {
            statAppDownlaodWithChunk.D = statInfo.pushId;
        }
        statAppDownlaodWithChunk.J = f.a();
        b h = com.tencent.assistant.net.c.h();
        statAppDownlaodWithChunk.F = new StatWifi(h.e, h.f, h.a());
        statAppDownlaodWithChunk.y = f();
        statAppDownlaodWithChunk.d = Global.getClientIp();
        statAppDownlaodWithChunk.h = arrayList;
        statAppDownlaodWithChunk.H = uIType.ordinal();
        if (DownloadInfo.isUiTypeWiseDownload(uIType)) {
            int a2 = m.a(uIType);
            statAppDownlaodWithChunk.k = a2;
            statAppDownlaodWithChunk.j = a2;
        }
        if (downloadType == SimpleDownloadInfo.DownloadType.PLUGIN) {
            statAppDownlaodWithChunk.G = 6;
            statAppDownlaodWithChunk.j = STConst.ST_PAGE_PLUGIN;
            statAppDownlaodWithChunk.k = STConst.ST_PAGE_PLUGIN;
        } else if (downloadType == SimpleDownloadInfo.DownloadType.VIDEO) {
            statAppDownlaodWithChunk.G = 10;
            statAppDownlaodWithChunk.T = str;
        } else if (downloadType == SimpleDownloadInfo.DownloadType.FILE) {
            statAppDownlaodWithChunk.G = 11;
            statAppDownlaodWithChunk.T = str;
        }
        statAppDownlaodWithChunk.Q = Global.getAppVersion() + "_" + Global.getBuildNo();
        statAppDownlaodWithChunk.R = h.a();
        return statAppDownlaodWithChunk;
    }

    private ArrayList<Speed> a(ArrayList<Speed> arrayList, k kVar) {
        Speed speed;
        if (arrayList == null) {
            arrayList = new ArrayList<>();
        }
        if (!(kVar == null || kVar.b() == null)) {
            Iterator<Speed> it = arrayList.iterator();
            while (true) {
                if (!it.hasNext()) {
                    speed = null;
                    break;
                }
                speed = it.next();
                if (a(speed, kVar)) {
                    break;
                }
            }
            if (speed != null) {
                speed.f2346a = kVar.b().c;
                speed.b += kVar.c();
                speed.c += kVar.b().g;
            } else {
                Speed speed2 = new Speed();
                speed2.f2346a = kVar.b().c;
                speed2.b = kVar.c();
                speed2.c = kVar.b().g;
                speed2.d = kVar.b().k;
                speed2.e = kVar.b().l;
                speed2.f = kVar.b().a();
                speed2.g = kVar.b().b;
                arrayList.add(speed2);
            }
        }
        return arrayList;
    }

    private boolean a(Speed speed, k kVar) {
        if (speed == null || kVar == null || kVar.b() == null || speed.f2346a != kVar.b().c || !speed.d.equals(kVar.b().k) || speed.e != kVar.b().l || !speed.g.equals(kVar.b().b)) {
            return false;
        }
        return true;
    }

    private void a(StatAppDownlaodWithChunk statAppDownlaodWithChunk, byte b) {
        ArrayList<Speed> arrayList;
        if (statAppDownlaodWithChunk != null) {
            statAppDownlaodWithChunk.f = h.a() - statAppDownlaodWithChunk.e;
            statAppDownlaodWithChunk.i = b;
            ArrayList arrayList2 = new ArrayList();
            ArrayList<Speed> arrayList3 = new ArrayList<>();
            if (this.d != null) {
                Iterator<k> it = this.d.iterator();
                while (true) {
                    arrayList = arrayList3;
                    if (!it.hasNext()) {
                        break;
                    }
                    k next = it.next();
                    if (next.a() == statAppDownlaodWithChunk.f2351a) {
                        arrayList2.add(next.b());
                        arrayList3 = a(arrayList, next);
                    } else {
                        arrayList3 = arrayList;
                    }
                }
            } else {
                arrayList = arrayList3;
            }
            statAppDownlaodWithChunk.b(arrayList);
            statAppDownlaodWithChunk.a(arrayList2);
        }
    }

    private void a(StatAppDownlaodWithChunk statAppDownlaodWithChunk, String str) {
        if (statAppDownlaodWithChunk != null) {
            XLog.d("NewDownload", "******** downlaod end scene=" + statAppDownlaodWithChunk.j + " sourceScene=" + statAppDownlaodWithChunk.k + " slotId=" + statAppDownlaodWithChunk.w + " sourceslotId=" + statAppDownlaodWithChunk.O + " extraData=" + statAppDownlaodWithChunk.l + " idType=" + statAppDownlaodWithChunk.G + " resourceid=" + statAppDownlaodWithChunk.T);
            byte[] a2 = h.a(statAppDownlaodWithChunk);
            if (this.b != null) {
                this.b.a(getSTType(), a2);
            }
            this.c.remove(str);
            ArrayList arrayList = new ArrayList();
            if (this.d != null) {
                Iterator<k> it = this.d.iterator();
                while (it.hasNext()) {
                    k next = it.next();
                    if (next.a() == statAppDownlaodWithChunk.f2351a) {
                        arrayList.add(next);
                    }
                }
                if (arrayList.size() > 0) {
                    this.d.removeAll(arrayList);
                }
            }
        }
    }

    public void a(int i, String str, String str2) {
    }

    public void a(int i, String str) {
        StatAppDownlaodWithChunk statAppDownlaodWithChunk;
        if (this.c != null && (statAppDownlaodWithChunk = this.c.get(str)) != null) {
            statAppDownlaodWithChunk.e = h.a();
        }
    }

    public void a(int i, String str, long j, String str2, String str3) {
    }

    public void a(int i, String str, long j, long j2, double d2) {
        StatAppDownlaodWithChunk statAppDownlaodWithChunk = this.c.get(str);
        if (statAppDownlaodWithChunk != null) {
            statAppDownlaodWithChunk.g = j;
        }
    }

    public void a(int i, String str, int i2, byte[] bArr, String str2) {
        StatAppDownlaodWithChunk statAppDownlaodWithChunk = this.c.get(str);
        if (statAppDownlaodWithChunk != null) {
            a(statAppDownlaodWithChunk, (byte) 1);
            statAppDownlaodWithChunk.p = i2;
            statAppDownlaodWithChunk.q = bArr;
            a(statAppDownlaodWithChunk, str);
        }
    }

    public void b(int i, String str) {
        StatAppDownlaodWithChunk statAppDownlaodWithChunk = this.c.get(str);
        if (statAppDownlaodWithChunk != null) {
            a(statAppDownlaodWithChunk, (byte) 2);
            a(statAppDownlaodWithChunk, str);
        }
    }

    public void a(int i, String str, String str2, String str3) {
        StatAppDownlaodWithChunk statAppDownlaodWithChunk = this.c.get(str);
        if (i == 100) {
            String[] split = str2.split("\\.");
            if (split.length > 1) {
                statAppDownlaodWithChunk.l = a(statAppDownlaodWithChunk.l, "ext", split[split.length - 1]);
            }
        }
        if (statAppDownlaodWithChunk != null) {
            a(statAppDownlaodWithChunk, (byte) 0);
            a(statAppDownlaodWithChunk, str);
        }
    }

    public byte getSTType() {
        return 14;
    }

    public void flush() {
    }

    public void b(int i, String str, String str2) {
        AppChunkDownlaod appChunkDownlaod;
        StatAppDownlaodWithChunk statAppDownlaodWithChunk = this.c.get(str);
        if (statAppDownlaodWithChunk != null) {
            Iterator<k> it = this.d.iterator();
            while (true) {
                if (!it.hasNext()) {
                    appChunkDownlaod = null;
                    break;
                }
                k next = it.next();
                if (next.a() == statAppDownlaodWithChunk.f2351a) {
                    appChunkDownlaod = next.b();
                    if (appChunkDownlaod != null) {
                        appChunkDownlaod.o = str2;
                    }
                }
            }
            if (appChunkDownlaod == null) {
                AppChunkDownlaod appChunkDownlaod2 = new AppChunkDownlaod();
                appChunkDownlaod2.o = str2;
                k kVar = new k();
                kVar.a(statAppDownlaodWithChunk.f2351a);
                kVar.a(appChunkDownlaod2);
                this.d.add(kVar);
            }
        }
    }

    private String a(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(str) && str.contains("=")) {
            sb.append(str).append("&");
        }
        sb.append(str2).append("=").append(str3);
        return sb.toString();
    }
}
