package com.tencent.assistantv2.st.business;

import android.os.Handler;
import android.util.Log;
import com.tencent.assistant.Global;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.StatCostTime;
import com.tencent.assistant.utils.ba;
import com.tencent.beacon.event.a;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class CostTimeSTManager extends BaseSTManagerV2 {

    /* renamed from: a  reason: collision with root package name */
    private static CostTimeSTManager f3324a = null;
    private static Handler d;
    private ConcurrentHashMap<Integer, Long> c = new ConcurrentHashMap<>();

    /* compiled from: ProGuard */
    public enum TIMETYPE {
        START,
        END,
        CORRECTION_SUB,
        CORRECTION_MINUS,
        CANCEL
    }

    public static synchronized CostTimeSTManager a() {
        CostTimeSTManager costTimeSTManager;
        synchronized (CostTimeSTManager.class) {
            if (f3324a == null) {
                f3324a = new CostTimeSTManager();
            }
            costTimeSTManager = f3324a;
        }
        return costTimeSTManager;
    }

    private CostTimeSTManager() {
        d = ba.a("TimeCostLogHandler");
    }

    public byte getSTType() {
        return 20;
    }

    public void flush() {
    }

    public void a(int i, TIMETYPE timetype, long j) {
        if (this.c.containsKey(Integer.valueOf(i))) {
            if (timetype == TIMETYPE.CORRECTION_SUB || timetype == TIMETYPE.CORRECTION_MINUS) {
                long longValue = this.c.get(Integer.valueOf(i)).longValue();
                if (timetype == TIMETYPE.CORRECTION_MINUS) {
                    this.c.put(Integer.valueOf(i), Long.valueOf(longValue + j));
                } else {
                    this.c.put(Integer.valueOf(i), Long.valueOf(longValue - j));
                }
            }
            if (timetype == TIMETYPE.CANCEL) {
                if (this.c.containsKey(Integer.valueOf(i))) {
                    this.c.remove(Integer.valueOf(i));
                }
            } else if (timetype == TIMETYPE.END && this.c.containsKey(Integer.valueOf(i))) {
                byte intValue = c.h().f1925a.getIntValue();
                long longValue2 = j - this.c.get(Integer.valueOf(i)).longValue();
                StatCostTime statCostTime = new StatCostTime();
                statCostTime.f2359a = i;
                statCostTime.b = longValue2;
                statCostTime.c = intValue;
                Log.d("bao_cost", " sceneId = " + i + " cost " + longValue2);
                this.c.remove(Integer.valueOf(i));
                if (longValue2 > 0) {
                    d.postDelayed(new j(this, statCostTime), 50);
                }
            }
        } else if (timetype == TIMETYPE.START) {
            this.c.put(Integer.valueOf(i), Long.valueOf(j));
        }
    }

    /* access modifiers changed from: private */
    public void a(StatCostTime statCostTime) {
        Map<String, String> b = b(statCostTime);
        boolean z = false;
        if (statCostTime != null) {
            z = true;
        }
        a.a(StatCostTime.class.getSimpleName(), z, 0, 0, b, true);
    }

    private Map<String, String> b(StatCostTime statCostTime) {
        if (statCostTime == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("B1", URLEncoder.encode(Global.getQUA()));
        hashMap.put("B2", Global.getPhoneGuid());
        hashMap.put("B3", String.valueOf(statCostTime.f2359a));
        hashMap.put("B4", String.valueOf(statCostTime.b));
        hashMap.put("B5", String.valueOf(statCostTime.c));
        return hashMap;
    }
}
