package com.tencent.assistantv2.st.b;

import android.text.TextUtils;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public class b {
    private static b c;

    /* renamed from: a  reason: collision with root package name */
    Map<String, STInfoV2> f3322a = new HashMap();
    private final String b = "ExposureStrategy";

    public static synchronized b getInstance() {
        b bVar;
        synchronized (b.class) {
            if (c == null) {
                c = new b();
            }
            bVar = c;
        }
        return bVar;
    }

    public STInfoV2 getExposureSTInfoV2(byte[] bArr, long j, int i, int i2, String str) {
        STInfoV2 sTInfoV2;
        String a2 = a(j, i, str);
        if (!TextUtils.isEmpty(a2)) {
            sTInfoV2 = this.f3322a.get(a2);
            if (sTInfoV2 != null && Arrays.equals(sTInfoV2.recommendId, bArr)) {
                long currentTimeMillis = System.currentTimeMillis() - sTInfoV2.latestExposureTime;
                if (sTInfoV2.hasExposure && currentTimeMillis < 1200000) {
                    return null;
                }
                sTInfoV2.hasExposure = true;
                sTInfoV2.latestExposureTime = System.currentTimeMillis();
            }
        } else {
            sTInfoV2 = null;
        }
        return sTInfoV2;
    }

    public void exposure(STInfoV2 sTInfoV2) {
        if (sTInfoV2 != null) {
            String a2 = a(sTInfoV2.appId, sTInfoV2.scene, sTInfoV2.slotId);
            if (!TextUtils.isEmpty(a2)) {
                STInfoV2 sTInfoV22 = this.f3322a.get(a2);
                if (sTInfoV22 == null || !Arrays.equals(sTInfoV22.recommendId, sTInfoV2.recommendId)) {
                    sTInfoV2.hasExposure = true;
                    sTInfoV2.latestExposureTime = System.currentTimeMillis();
                } else {
                    long currentTimeMillis = System.currentTimeMillis() - sTInfoV22.latestExposureTime;
                    if (!sTInfoV22.hasExposure || currentTimeMillis >= 1200000) {
                        sTInfoV2.hasExposure = true;
                        sTInfoV2.latestExposureTime = System.currentTimeMillis();
                    } else {
                        return;
                    }
                }
                this.f3322a.put(a2, sTInfoV2);
                k.a(sTInfoV2);
            }
        }
    }

    public void exposureHotWord(STInfoV2 sTInfoV2) {
        if (sTInfoV2 != null) {
            String a2 = a(sTInfoV2.scene, sTInfoV2.slotId, sTInfoV2.extraData);
            if (!TextUtils.isEmpty(a2)) {
                STInfoV2 sTInfoV22 = this.f3322a.get(a2);
                if (sTInfoV22 == null || !Arrays.equals(sTInfoV22.recommendId, sTInfoV2.recommendId)) {
                    sTInfoV2.hasExposure = true;
                    sTInfoV2.latestExposureTime = System.currentTimeMillis();
                } else {
                    long currentTimeMillis = System.currentTimeMillis() - sTInfoV22.latestExposureTime;
                    if (!sTInfoV22.hasExposure || currentTimeMillis >= 60000) {
                        sTInfoV2.hasExposure = true;
                        sTInfoV2.latestExposureTime = System.currentTimeMillis();
                    } else {
                        return;
                    }
                }
                this.f3322a.put(a2, sTInfoV2);
                k.a(sTInfoV2);
            }
        }
    }

    public void exposureSpecialTopic(STInfoV2 sTInfoV2) {
    }

    private String a(long j, int i, String str) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(j).append("_");
        stringBuffer.append(i).append("_");
        stringBuffer.append(str);
        return stringBuffer.toString();
    }

    private String a(int i, String str, String str2) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(i).append("_");
        stringBuffer.append(str).append("_");
        stringBuffer.append(str2);
        return stringBuffer.toString();
    }

    public void reset() {
        if (this.f3322a != null) {
            this.f3322a.clear();
        }
    }
}
