package com.tencent.assistantv2.model.b;

/* compiled from: ProGuard */
public class c {
    public static int a(int i) {
        switch (i) {
            case 1:
            case 2:
            case 3:
            case 4:
            default:
                return 0;
            case 5:
                return 3;
            case 6:
                return 4;
            case 7:
                return 5;
        }
    }

    public static int[] b(int i) {
        switch (i) {
            case 0:
                return new int[]{1, 2};
            case 1:
                return new int[]{1, 2, 4};
            case 2:
                return new int[]{1, 2, 3};
            case 3:
                return new int[]{1, 2, 5};
            case 4:
                return new int[]{1, 2, 6};
            case 5:
                return new int[]{1, 2, 7};
            case 6:
                return new int[]{1, 2};
            case 7:
                return new int[]{1, 2};
            case 8:
                return new int[]{1, 2};
            default:
                return null;
        }
    }
}
