package com.tencent.assistantv2.model;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public String f3315a;
    public String b;
    public String c;
    public String d;
    public String e;
    public int f;
    public int g;
    public int h;
    public long i;
    public long j;
    public String k;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        b bVar = (b) obj;
        if (this.c != null) {
            if (this.c.equals(bVar.c)) {
                return true;
            }
        } else if (bVar.c == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.c != null) {
            return this.c.hashCode();
        }
        return 0;
    }
}
