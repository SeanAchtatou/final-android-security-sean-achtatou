package com.tencent.assistantv2.model.a;

import com.tencent.assistant.module.callback.ActionCallback;
import com.tencent.assistant.protocol.jce.AppHotFriend;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import java.util.ArrayList;

/* compiled from: ProGuard */
public interface d extends ActionCallback {
    void a(int i, int i2, int i3, int i4);

    void a(int i, int i2, String str, ArrayList<AppHotFriend> arrayList);

    void a(int i, int i2, ArrayList<CommentDetail> arrayList, ArrayList<CommentTagInfo> arrayList2, ArrayList<CommentTagInfo> arrayList3);
}
