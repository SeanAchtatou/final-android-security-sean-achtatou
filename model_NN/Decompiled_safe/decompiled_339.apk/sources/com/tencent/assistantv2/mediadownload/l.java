package com.tencent.assistantv2.mediadownload;

import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistantv2.model.AbstractDownloadInfo;
import com.tencent.assistantv2.model.d;
import com.tencent.downloadsdk.DownloadManager;

/* compiled from: ProGuard */
class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f3294a;

    l(c cVar) {
        this.f3294a = cVar;
    }

    public void run() {
        for (d next : this.f3294a.a()) {
            DownloadManager.a().a(100, next.c);
            if (next.i == AbstractDownloadInfo.DownState.DOWNLOADING || next.i == AbstractDownloadInfo.DownState.QUEUING) {
                next.i = AbstractDownloadInfo.DownState.FAIL;
                this.f3294a.c.sendMessage(this.f3294a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FAIL, next));
                this.f3294a.b.a(next);
            }
        }
    }
}
