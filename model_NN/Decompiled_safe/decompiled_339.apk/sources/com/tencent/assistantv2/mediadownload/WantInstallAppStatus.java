package com.tencent.assistantv2.mediadownload;

/* compiled from: ProGuard */
public enum WantInstallAppStatus {
    INSTALLED,
    NOT_INSTALL,
    NOT_MATCH_VERSION,
    DOWNLOADING,
    UNKNOWN
}
