package com.tencent.assistantv2.mediadownload;

import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcher;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistantv2.db.a.a;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class b {
    private static b d;

    /* renamed from: a  reason: collision with root package name */
    private EventDispatcher f3284a = AstApp.i().j();
    private a b = new a();
    private Map<String, com.tencent.assistantv2.model.b> c = new ConcurrentHashMap(5);

    public static synchronized b a() {
        b bVar;
        synchronized (b.class) {
            if (d == null) {
                d = new b();
            }
            bVar = d;
        }
        return bVar;
    }

    private b() {
        List<com.tencent.assistantv2.model.b> a2 = this.b.a();
        if (a2 != null && a2.size() > 0) {
            for (com.tencent.assistantv2.model.b next : a2) {
                this.c.put(next.c, next);
            }
        }
    }

    public List<com.tencent.assistantv2.model.b> b() {
        ArrayList arrayList = new ArrayList(this.c.size());
        for (Map.Entry<String, com.tencent.assistantv2.model.b> value : this.c.entrySet()) {
            arrayList.add(value.getValue());
        }
        return arrayList;
    }

    public com.tencent.assistantv2.model.b a(String str) {
        com.tencent.assistantv2.model.b remove = this.c.remove(str);
        this.f3284a.sendMessage(this.f3284a.obtainMessage(EventDispatcherEnum.UI_EVENT_QREADER_DELETE, remove));
        this.b.a(str);
        return remove;
    }

    public void a(com.tencent.assistantv2.model.b bVar) {
        if (bVar != null) {
            com.tencent.assistantv2.model.b bVar2 = this.c.get(bVar.c);
            if (bVar2 == null) {
                bVar.i = System.currentTimeMillis();
                bVar.j = System.currentTimeMillis();
                this.c.put(bVar.c, bVar);
                this.b.a(bVar);
                this.f3284a.sendMessage(this.f3284a.obtainMessage(EventDispatcherEnum.UI_EVENT_QREADER_ADD, bVar));
            } else if (bVar.g == 1) {
                if (bVar2 != bVar) {
                    bVar2.g = bVar.g;
                    bVar2.d = bVar.d;
                    bVar2.f = bVar.f;
                    bVar2.f3315a = bVar.f3315a;
                    bVar2.e = bVar.e;
                    bVar2.j = System.currentTimeMillis();
                    if (bVar.f == -1) {
                        bVar.h = 1;
                    }
                }
                this.b.a(bVar2);
                this.f3284a.sendMessage(this.f3284a.obtainMessage(EventDispatcherEnum.UI_EVENT_QREADER_UPDATE, bVar2));
            }
        }
    }
}
