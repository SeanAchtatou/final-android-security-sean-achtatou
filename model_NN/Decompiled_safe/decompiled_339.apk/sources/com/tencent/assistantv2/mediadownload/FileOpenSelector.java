package com.tencent.assistantv2.mediadownload;

import android.content.Intent;
import android.net.Uri;
import com.tencent.connect.common.Constants;
import java.io.File;

/* compiled from: ProGuard */
public class FileOpenSelector {

    /* compiled from: ProGuard */
    public enum FileType {
        music,
        video,
        txt,
        zip,
        office,
        other,
        apk,
        img
    }

    public static String a(String str) {
        try {
            return str.substring(str.lastIndexOf(".") + 1, str.length()).toLowerCase();
        } catch (Exception e) {
            e.printStackTrace();
            return Constants.STR_EMPTY;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.mediadownload.FileOpenSelector.a(java.lang.String, boolean):android.content.Intent
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistantv2.mediadownload.FileOpenSelector.a(java.lang.String, java.lang.String):android.content.Intent
      com.tencent.assistantv2.mediadownload.FileOpenSelector.a(java.lang.String, boolean):android.content.Intent */
    public static m b(String str) {
        Intent intent;
        m mVar = new m();
        mVar.c = true;
        try {
            String a2 = a(new File(str).getName());
            mVar.b = a2;
            if (a2.equals("m4a") || a2.equals("mp3") || a2.equals("mid") || a2.equals("aac") || a2.equals("amr") || a2.equals("ra") || a2.equals("wma") || a2.equals("mpga") || a2.equals("ape") || a2.equals("flac") || a2.equals("ogg") || a2.equals("wav")) {
                intent = f(str);
                mVar.d = FileType.music;
            } else if (a2.equals("3gp") || a2.equals("mp4") || a2.equals("rtsp") || a2.equals("rtmp") || a2.equals("flv") || a2.equals("avi") || a2.equals("3gpp") || a2.equals("webm") || a2.equals("ts") || a2.equals("ogv") || a2.equals("m3u8") || a2.equals("asf") || a2.equals("wmv") || a2.equals("rmvb") || a2.equals("rm") || a2.equals("f4v") || a2.equals("dat") || a2.equals("mov") || a2.equals("mpg") || a2.equals("mkv") || a2.equals("mpeg") || a2.equals("xvid") || a2.equals("dvd") || a2.equals("vcd") || a2.equals("vob") || a2.equals("divx")) {
                intent = e(str);
                mVar.d = FileType.video;
            } else if (a2.equals("jpg") || a2.equals("gif") || a2.equals("png") || a2.equals("jpeg") || a2.equals("bmp") || a2.equals("webp")) {
                intent = h(str);
                mVar.d = FileType.img;
            } else if (a2.equals("apk")) {
                intent = d(str);
                mVar.d = FileType.apk;
            } else if (a2.equals("ppt") || a2.equals("pptx")) {
                intent = i(str);
                mVar.d = FileType.office;
            } else if (a2.equals("xls") || a2.equals("xlsx")) {
                intent = j(str);
                mVar.d = FileType.office;
            } else if (a2.equals("doc") || a2.equals("docx")) {
                intent = k(str);
                mVar.d = FileType.office;
            } else if (a2.equals("pdf")) {
                intent = m(str);
            } else if (a2.equals("html")) {
                intent = g(str);
            } else if (a2.equals("chm")) {
                intent = l(str);
            } else if (a2.equals("txt") || a2.equals("xml") || a2.equals("log") || a2.equals("bat") || a2.equals("php") || a2.equals("js") || a2.equals("lrc") || a2.equals("ini")) {
                intent = a(str, false);
                mVar.d = FileType.txt;
            } else if (a2.equals("zip") || a2.equals("rar") || a2.equals("7z")) {
                intent = a(str, a2);
                mVar.d = FileType.zip;
            } else {
                mVar.c = false;
                intent = c(str);
            }
            mVar.f3295a = intent;
            return mVar;
        } catch (Exception e) {
            e.printStackTrace();
            return mVar;
        }
    }

    public static Intent c(String str) {
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(str)), "*/*");
        return intent;
    }

    public static Intent d(String str) {
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(str)), "application/vnd.android.package-archive");
        return intent;
    }

    public static Intent e(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(67108864);
        intent.putExtra("oneshot", 0);
        intent.putExtra("configchange", 0);
        intent.setDataAndType(Uri.fromFile(new File(str)), "video/*");
        return intent;
    }

    public static Intent f(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(67108864);
        intent.putExtra("oneshot", 0);
        intent.putExtra("configchange", 0);
        intent.setDataAndType(Uri.fromFile(new File(str)), "audio/*");
        return intent;
    }

    public static Intent g(String str) {
        Uri build = Uri.parse(str).buildUpon().encodedAuthority("com.android.htmlfileprovider").scheme("content").encodedPath(str).build();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(build, "text/html");
        return intent;
    }

    public static Intent h(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.fromFile(new File(str)), "image/*");
        return intent;
    }

    public static Intent i(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.fromFile(new File(str)), "application/vnd.ms-powerpoint");
        return intent;
    }

    public static Intent j(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.fromFile(new File(str)), "application/vnd.ms-excel");
        return intent;
    }

    public static Intent a(String str, String str2) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.fromFile(new File(str)), "application/" + str2);
        return intent;
    }

    public static Intent k(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.fromFile(new File(str)), "application/msword");
        return intent;
    }

    public static Intent l(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.fromFile(new File(str)), "application/x-chm");
        return intent;
    }

    public static Intent a(String str, boolean z) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(268435456);
        if (z) {
            intent.setDataAndType(Uri.parse(str), "text/plain");
        } else {
            intent.setDataAndType(Uri.fromFile(new File(str)), "text/plain");
        }
        return intent;
    }

    public static Intent m(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.fromFile(new File(str)), "application/pdf");
        return intent;
    }
}
