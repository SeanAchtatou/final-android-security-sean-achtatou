package com.tencent.assistantv2.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class da extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SecondNavigationTitleViewV5 f3170a;

    da(SecondNavigationTitleViewV5 secondNavigationTitleViewV5) {
        this.f3170a = secondNavigationTitleViewV5;
    }

    public void onTMAClick(View view) {
        if (this.f3170a.k != null) {
            this.f3170a.k.finish();
        }
    }

    public STInfoV2 getStInfo() {
        return this.f3170a.e(a.a(STConst.ST_STATUS_DEFAULT, "001"));
    }
}
