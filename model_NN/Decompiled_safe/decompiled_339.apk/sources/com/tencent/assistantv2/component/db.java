package com.tencent.assistantv2.component;

import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.b.a;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class db extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SecondNavigationTitleViewV5 f3171a;

    db(SecondNavigationTitleViewV5 secondNavigationTitleViewV5) {
        this.f3171a = secondNavigationTitleViewV5;
    }

    public void onTMAClick(View view) {
        b.b(this.f3171a.getContext(), ("tpmast://search?" + "&" + PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME + "=" + ((BaseActivity) this.f3171a.b).f()) + "&" + a.X + "=" + this.f3171a.q());
    }

    public STInfoV2 getStInfo() {
        return this.f3171a.e(com.tencent.assistantv2.st.page.a.a("01", "001"));
    }
}
