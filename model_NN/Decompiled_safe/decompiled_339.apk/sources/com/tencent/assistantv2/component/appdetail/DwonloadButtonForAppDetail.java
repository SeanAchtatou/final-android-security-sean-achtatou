package com.tencent.assistantv2.component.appdetail;

import android.content.Context;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.component.DownloadButton;

/* compiled from: ProGuard */
public class DwonloadButtonForAppDetail extends RelativeLayout implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    DownloadButton f3053a;
    ImageView b;
    SimpleAppModel c;

    public DwonloadButtonForAppDetail(Context context) {
        this(context, null);
        e();
    }

    public DwonloadButtonForAppDetail(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        e();
    }

    public void a(SimpleAppModel simpleAppModel) {
        this.c = simpleAppModel;
        this.f3053a.a(simpleAppModel);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
    }

    public void a() {
        this.b.setVisibility(0);
        this.b.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.circle));
        this.f3053a.setEnabled(false);
    }

    public DownloadButton b() {
        return this.f3053a;
    }

    public void c() {
        this.b.setAnimation(null);
        this.b.setVisibility(8);
        this.f3053a.setEnabled(true);
    }

    private void e() {
        View inflate = inflate(getContext(), R.layout.appdetail_download_appbutton_, this);
        this.f3053a = (DownloadButton) inflate.findViewById(R.id.appdownload_button);
        this.b = (ImageView) inflate.findViewById(R.id.download_load_view);
    }

    public void d() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
                if (message.obj instanceof DownloadInfo) {
                    DownloadInfo downloadInfo = (DownloadInfo) message.obj;
                    if (this.f3053a != null && downloadInfo != null && this.c != null && downloadInfo.downloadTicket != null && this.c.q().equals(downloadInfo.downloadTicket)) {
                        this.f3053a.a(this.c);
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }
}
