package com.tencent.assistantv2.component;

import android.content.Context;
import android.text.TextUtils;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistantv2.mediadownload.a;
import com.tencent.assistantv2.model.h;

/* compiled from: ProGuard */
class er extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    private h f3205a;
    private es b;

    public er(Context context, h hVar, int i, es esVar) {
        this.f3205a = hVar;
        this.b = esVar;
        this.titleRes = context.getResources().getString(R.string.video_down_tips);
        String string = i == -3 ? context.getResources().getString(R.string.video_down_tips_older) : context.getResources().getString(R.string.video_down_tips_content);
        Object[] objArr = new Object[1];
        objArr[0] = TextUtils.isEmpty(hVar.v) ? context.getResources().getString(R.string.video_down_tips_unknow_player) : hVar.v;
        this.contentRes = String.format(string, objArr);
        this.lBtnTxtRes = context.getResources().getString(R.string.video_down_cancel);
        this.rBtnTxtRes = context.getResources().getString(R.string.video_down_install);
    }

    public void onLeftBtnClick() {
    }

    public void onRightBtnClick() {
        a.a().a(this.f3205a.r, this.f3205a.u, true);
        if (this.b != null) {
            this.b.a();
        }
    }

    public void onCancell() {
    }
}
