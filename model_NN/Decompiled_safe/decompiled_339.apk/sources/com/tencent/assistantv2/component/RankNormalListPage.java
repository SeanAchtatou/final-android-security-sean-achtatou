package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.module.k;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.activity.ay;
import com.tencent.assistantv2.adapter.RankNormalListAdapter;

/* compiled from: ProGuard */
public class RankNormalListPage extends RelativeLayout implements NetworkMonitor.ConnectivityChangeListener, cl {
    protected LoadingView loadingView;
    protected Context mContext = null;
    protected NormalErrorRecommendPage mErrorPage;
    protected LayoutInflater mInflater = null;
    private APN mLastApn = APN.NO_NETWORK;
    protected RankNormalListView mListView;
    protected View.OnClickListener refreshClick = new cg(this);

    public RankNormalListPage(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mContext = context;
        initView(context);
    }

    public RankNormalListPage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mContext = context;
        initView(context);
    }

    public RankNormalListPage(Context context, TXScrollViewBase.ScrollMode scrollMode, k kVar) {
        super(context);
        this.mContext = context;
        initView(context);
        this.mListView.setAppEngine(kVar);
        this.mListView.setListPage(this);
        this.mListView.registerRefreshListener(this);
        this.mListView.setDivider(null);
    }

    /* access modifiers changed from: protected */
    public void initView(Context context) {
        this.mInflater = LayoutInflater.from(context);
        View inflate = this.mInflater.inflate((int) R.layout.ranknormallist_component_view, this);
        this.mListView = (RankNormalListView) inflate.findViewById(R.id.applist);
        this.mListView.setVisibility(8);
        this.loadingView = (LoadingView) inflate.findViewById(R.id.loading_view);
        this.loadingView.setVisibility(0);
        this.mErrorPage = (NormalErrorRecommendPage) inflate.findViewById(R.id.error_page);
        this.mErrorPage.setButtonClickListener(this.refreshClick);
        this.mErrorPage.setIsAutoLoading(true);
        cq.a().a(this);
    }

    public void onErrorHappened(int i) {
        this.loadingView.setVisibility(8);
        this.mListView.setVisibility(8);
        this.mErrorPage.setVisibility(0);
        this.mErrorPage.setErrorType(i);
    }

    public void onNetworkLoading() {
        this.loadingView.setVisibility(0);
        this.mListView.setVisibility(8);
        this.mErrorPage.setVisibility(8);
    }

    public void setAdapter(RankNormalListAdapter rankNormalListAdapter, ListViewScrollListener listViewScrollListener) {
        this.mListView.addHeaderView();
        this.mListView.setAdapter(rankNormalListAdapter);
        this.mListView.setScrollListener(listViewScrollListener);
        rankNormalListAdapter.a(listViewScrollListener);
    }

    public void loadFirstPage() {
        onNetworkLoading();
        this.mListView.loadFirstPage(getNeedRefresh());
    }

    public void recycleData() {
        this.mListView.recycleData();
    }

    public void onNetworkNoError() {
        this.mListView.setVisibility(0);
        this.mErrorPage.setVisibility(8);
        this.loadingView.setVisibility(8);
    }

    public void onNextPageLoadFailed() {
    }

    public void onPause() {
    }

    public void onResume() {
        if (this.mListView != null) {
            this.mListView.onResume();
        }
    }

    public int getFirstVisiblePosition() {
        return this.mListView.getFirstVisiblePosition();
    }

    public ListView getListView() {
        return this.mListView.getListView();
    }

    public int getPageId() {
        k appEngine = this.mListView.getAppEngine();
        if (appEngine == null || appEngine.f1840a != 0) {
            return 2000;
        }
        if (appEngine.b == 6) {
            return STConst.ST_PAGE_RANK_HOT;
        }
        if (appEngine.b == 5) {
            return STConst.ST_PAGE_RANK_CLASSIC;
        }
        if (appEngine.b == 2) {
            return STConst.ST_PAGE_RANK_RECOMMEND;
        }
        return 2000;
    }

    public void onConnected(APN apn) {
        if (this.mListView.getmAdapter() == null || this.mListView.getmAdapter().getCount() <= 0) {
        }
    }

    public void onDisconnected(APN apn) {
        this.mLastApn = apn;
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        this.mLastApn = apn2;
    }

    public boolean getNeedRefresh() {
        return this.mListView == null || this.mListView.getNeedRefresh();
    }

    public void showHideInstalledAppArea(boolean z) {
        if (this.mListView != null) {
            this.mListView.showHideInstalledAppArea(z, false);
        }
    }

    public void setBaseFragment(ay ayVar) {
        this.mListView.setBaseFragment(ayVar);
    }
}
