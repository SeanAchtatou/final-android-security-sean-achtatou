package com.tencent.assistantv2.component;

/* compiled from: ProGuard */
class cw implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ long f3165a;
    final /* synthetic */ ScaningProgressView b;

    cw(ScaningProgressView scaningProgressView, long j) {
        this.b = scaningProgressView;
        this.f3165a = j;
    }

    public void run() {
        this.b.i.setVisibility(4);
        this.b.h.setVisibility(0);
        this.b.e.setValue((((double) this.f3165a) * 1.0d) / 1024.0d);
    }
}
