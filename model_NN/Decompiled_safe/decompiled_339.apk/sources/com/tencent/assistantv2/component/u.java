package com.tencent.assistantv2.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.manager.g;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class u extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f3264a;
    final /* synthetic */ STCommonInfo b;
    final /* synthetic */ x c;
    final /* synthetic */ g[] d;
    final /* synthetic */ DownloadButton e;

    u(DownloadButton downloadButton, SimpleAppModel simpleAppModel, STCommonInfo sTCommonInfo, x xVar, g[] gVarArr) {
        this.e = downloadButton;
        this.f3264a = simpleAppModel;
        this.b = sTCommonInfo;
        this.c = xVar;
        this.d = gVarArr;
    }

    public void onTMAClick(View view) {
        this.e.a(this.f3264a, this.b, this.c, this.d);
    }

    public STInfoV2 getStInfo() {
        if (this.b == null || !(this.b instanceof STInfoV2)) {
            return null;
        }
        this.b.updateStatus(this.f3264a);
        return (STInfoV2) this.b;
    }
}
