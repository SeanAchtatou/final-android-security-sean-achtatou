package com.tencent.assistantv2.component.categorydetail;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.manager.ao;
import com.tencent.assistant.module.callback.c;
import com.tencent.assistant.protocol.jce.TagGroup;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.List;

/* compiled from: ProGuard */
public class GameCategoryDetailListView extends TXGetMoreListView implements ITXRefreshListViewListener {

    /* renamed from: a  reason: collision with root package name */
    private ao f3140a = null;
    /* access modifiers changed from: private */
    public SmartListAdapter b = null;
    private j c;
    private int d = 1;
    /* access modifiers changed from: private */
    public FloatTagHeader e;
    /* access modifiers changed from: private */
    public long f = 0;
    private float g;
    private float h;
    private float i;
    private float j;
    private final int k = df.a(getContext(), 10.0f);
    private boolean l = false;
    /* access modifiers changed from: private */
    public int m = 0;
    /* access modifiers changed from: private */
    public int n = 0;
    private c o = new h(this);
    private k p = new i(this);

    public GameCategoryDetailListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        e();
    }

    public ListView getListView() {
        return (ListView) this.mScrollContentView;
    }

    /* access modifiers changed from: private */
    public void e() {
        ListAdapter adapter = ((ListView) this.mScrollContentView).getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            this.b = (SmartListAdapter) ((HeaderViewListAdapter) adapter).getWrappedAdapter();
        } else {
            this.b = (SmartListAdapter) ((ListView) this.mScrollContentView).getAdapter();
        }
        if (this.b == null) {
        }
    }

    public void a(View.OnClickListener onClickListener) {
        this.e.b(onClickListener);
    }

    public void b(View.OnClickListener onClickListener) {
        this.e.a(onClickListener);
    }

    public void a(j jVar) {
        this.c = jVar;
    }

    public void a(ao aoVar) {
        this.f3140a = aoVar;
        this.f3140a.a(this.o);
        setRefreshListViewListener(this);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.g = motionEvent.getRawX();
                this.h = motionEvent.getRawY();
                break;
            case 1:
                this.i = motionEvent.getRawX();
                this.j = motionEvent.getRawY();
                if (this.l) {
                    a(true);
                    break;
                }
                break;
            case 2:
                this.i = motionEvent.getRawX();
                this.j = motionEvent.getRawY();
                break;
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3, boolean z) {
        if (i3 == 0) {
            if (this.b.getCount() == 0) {
                this.c.b(10);
                return;
            }
            this.c.f();
            this.mFooterLoadingView.setVisibility(0);
            this.b.notifyDataSetChanged();
            onRefreshComplete(this.f3140a.b(), true);
        } else if (!z) {
            this.mFooterLoadingView.setVisibility(0);
            onRefreshComplete(this.f3140a.b(), false);
            this.c.c();
        } else if (-800 == i3) {
            this.c.b(30);
        } else if (this.d <= 0) {
            this.c.b(20);
        } else {
            this.d--;
            this.f3140a.a(this.f);
        }
    }

    /* access modifiers changed from: private */
    public void b(int i2, int i3, boolean z) {
        if (i3 == 0) {
            if (this.b.getCount() == 0) {
                this.c.a(10);
                return;
            }
            this.c.b();
            this.mFooterLoadingView.setVisibility(0);
            this.b.notifyDataSetChanged();
            onRefreshComplete(this.f3140a.b());
        } else if (!z) {
            this.mFooterLoadingView.setVisibility(0);
            onRefreshComplete(this.f3140a.b());
            this.c.c();
        } else if (-800 == i3) {
            this.c.a(30);
        } else if (this.d <= 0) {
            this.c.a(20);
        } else {
            this.d--;
            this.f3140a.c();
        }
    }

    public void a(long j2) {
        if (this.b == null) {
            e();
        }
        if (this.b != null) {
        }
        if (this.e.a()) {
            this.mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.RESET;
            this.mFooterLoadingView.setVisibility(8);
            reset();
            this.b.d();
            this.c.e();
        } else {
            this.c.d();
        }
        this.f = j2;
        this.f3140a.a(j2);
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        this.f3140a.a();
    }

    public void a() {
        if (this.b != null) {
            this.b.k();
        }
    }

    public void b() {
        if (this.b != null) {
            this.b.l();
            this.b.notifyDataSetChanged();
        }
    }

    public void recycleData() {
        super.recycleData();
        this.f3140a.b(this.o);
        if (this.b != null) {
            this.b.m();
        }
    }

    public void a(FloatTagHeader floatTagHeader, boolean z) {
        this.e = floatTagHeader;
        if (z) {
            c();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.categorydetail.GameCategoryDetailListView.a(boolean, int):void
     arg types: [int, int]
     candidates:
      com.tencent.assistantv2.component.categorydetail.GameCategoryDetailListView.a(com.tencent.assistantv2.component.categorydetail.GameCategoryDetailListView, java.util.List):void
      com.tencent.assistantv2.component.categorydetail.GameCategoryDetailListView.a(com.tencent.assistantv2.component.categorydetail.FloatTagHeader, boolean):void
      com.tencent.assistantv2.component.categorydetail.GameCategoryDetailListView.a(boolean, int):void */
    public void a(boolean z) {
        if (!z) {
            a(true, 0);
        } else if (this.j - this.h >= 0.0f) {
            a(false, 0);
        } else {
            a(true, -((int) (this.j - this.h)));
        }
    }

    public void a(boolean z, int i2) {
        if (this.e != null && this.e.c() > 0) {
            this.m = getListView().getFirstVisiblePosition();
            View childAt = getListView().getChildAt(this.m);
            if (childAt != null) {
                this.n = childAt.getTop();
                this.e.a(z, i2, this.p);
            }
        }
        this.l = false;
    }

    public void c() {
        if (this.e != null && this.e.b() != null) {
            if (getAdapter() != null) {
                removeHeaderView(this.e.b());
            }
            addHeaderView(this.e.b());
        }
    }

    public void d() {
        if (this.e != null) {
            removeHeaderView(this.e.b());
        }
    }

    /* access modifiers changed from: private */
    public void a(List<TagGroup> list) {
        if (list != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < list.size()) {
                    if (list.get(i3) != null) {
                        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(getContext(), 100);
                        buildSTInfo.slotId = a.a("05", i3 + 1);
                        k.a(buildSTInfo);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
