package com.tencent.assistantv2.component;

import com.qq.AppService.AstApp;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.b;
import com.tencent.assistant.protocol.jce.TagGroup;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.manager.i;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
class ch implements b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankNormalListView f3154a;

    ch(RankNormalListView rankNormalListView) {
        this.f3154a = rankNormalListView;
    }

    public void a(int i, int i2, boolean z, List<SimpleAppModel> list, List<TagGroup> list2) {
        if (this.f3154a.scrolllistener != null) {
            ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(1, null, this.f3154a.pageMessageHandler);
            viewInvalidateMessage.arg1 = i2;
            viewInvalidateMessage.arg2 = i;
            HashMap hashMap = new HashMap();
            hashMap.put("isFirstPage", Boolean.valueOf(z));
            viewInvalidateMessage.params = hashMap;
            hashMap.put("key_data", list);
            boolean c = i.a().b().c();
            XLog.d("voken", "filter = " + c);
            if (!c && z && i.a().b().b() < 2 && this.f3154a.meetInstalledCount(list) && !i.a().b().e()) {
                AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.UI_EVENT_HOTTAB_DOWNLOAD_FILTER_SHOW);
            }
            this.f3154a.scrolllistener.sendMessage(viewInvalidateMessage);
        }
    }
}
