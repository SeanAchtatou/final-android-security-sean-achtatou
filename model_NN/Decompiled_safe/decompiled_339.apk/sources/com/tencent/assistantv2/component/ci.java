package com.tencent.assistantv2.component;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.k;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
class ci extends ViewInvalidateMessageHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankNormalListView f3155a;

    ci(RankNormalListView rankNormalListView) {
        this.f3155a = rankNormalListView;
    }

    public void handleMessage(ViewInvalidateMessage viewInvalidateMessage) {
        int i;
        if (viewInvalidateMessage.what == 1) {
            int i2 = viewInvalidateMessage.arg1;
            int i3 = viewInvalidateMessage.arg2;
            Map map = (Map) viewInvalidateMessage.params;
            boolean booleanValue = ((Boolean) map.get("isFirstPage")).booleanValue();
            Object obj = map.get("key_data");
            if (obj != null && this.f3155a.mAdapter != null) {
                List<SimpleAppModel> list = (List) obj;
                this.f3155a.mAdapter.a(booleanValue, list);
                if (booleanValue) {
                    k.a((int) STConst.ST_PAGE_RANK_CLASSIC, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                }
                this.f3155a.onAppListLoadedFinishedHandler(i3, i2, booleanValue, ((List) obj).size());
                if (booleanValue) {
                    int i4 = 0;
                    int i5 = 0;
                    for (SimpleAppModel simpleAppModel : list) {
                        i4++;
                        if (i4 > 20) {
                            break;
                        }
                        if (simpleAppModel.t() == AppConst.AppState.INSTALLED) {
                            i = i5 + 1;
                        } else {
                            i = i5;
                        }
                        i5 = i;
                    }
                    if ((i5 < 5 || !m.a().ai()) && ((!this.f3155a.isGameRank && !m.a().ak()) || (this.f3155a.isGameRank && !m.a().al()))) {
                        this.f3155a.isHideInstalledAppAreaAdded = false;
                        this.f3155a.mHideInstalledAppArea.setVisibility(8);
                    } else {
                        this.f3155a.isHideInstalledAppAreaAdded = true;
                        this.f3155a.buildSTInfoAndReport();
                        m.a().w(false);
                        if (this.f3155a.isGameRank) {
                            m.a().z(false);
                        } else {
                            m.a().y(false);
                        }
                    }
                    this.f3155a.mAdapter.d();
                }
            } else if (this.f3155a.mAdapter != null) {
                this.f3155a.onAppListLoadedFinishedHandler(i3, i2, booleanValue, 0);
            }
        } else {
            this.f3155a.mListener.onNetworkNoError();
            if (this.f3155a.mAdapter != null) {
                this.f3155a.mAdapter.notifyDataSetChanged();
            }
        }
    }
}
