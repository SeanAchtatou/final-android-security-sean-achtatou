package com.tencent.assistantv2.component;

import android.view.animation.Animation;
import android.view.animation.Transformation;

/* compiled from: ProGuard */
public class bn extends Animation {

    /* renamed from: a  reason: collision with root package name */
    private float f3124a;
    private float b;
    private bo c;

    public bn(float f, float f2, bo boVar) {
        this.f3124a = f;
        this.b = f2;
        this.c = boVar;
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f, Transformation transformation) {
        super.applyTransformation(f, transformation);
        if (this.c != null) {
            this.c.a(this.f3124a + ((this.b - this.f3124a) * f));
            if (f == 1.0f) {
                this.c.a();
            }
        }
    }

    public void a(float f, float f2) {
        this.f3124a = f;
        this.b = f2;
    }
}
