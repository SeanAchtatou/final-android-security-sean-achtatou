package com.tencent.assistantv2.component.appdetail;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.listener.OnTMAItemClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.AppExCfg;
import com.tencent.assistant.utils.ct;

/* compiled from: ProGuard */
class f extends OnTMAItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailViewV5 f3070a;

    f(AppDetailViewV5 appDetailViewV5) {
        this.f3070a = appDetailViewV5;
    }

    public void onTMAItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        AppExCfg appExCfg = (AppExCfg) this.f3070a.j.getItem(i);
        if (appExCfg != null && appExCfg.d != null) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("com.tencent.assistant.ACTION_URL", appExCfg.d);
            if (this.f3070a.m instanceof BaseActivity) {
                bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.f3070a.m).f());
            }
            b.b(this.f3070a.m, appExCfg.d.f1970a, bundle);
        }
    }

    public String getSTSlotId() {
        return "09_" + ct.a(getPosition() + 1);
    }
}
