package com.tencent.assistantv2.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.login.d;
import com.tencent.assistant.protocol.jce.ExplicitHotWord;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class bf extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MainActionHeaderView f3116a;

    bf(MainActionHeaderView mainActionHeaderView) {
        this.f3116a = mainActionHeaderView;
    }

    public void onTMAClick(View view) {
        this.f3116a.a(view);
    }

    public STInfoV2 getStInfo(View view) {
        ExplicitHotWord explicitHotWord;
        if (view == null) {
            return null;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3116a.getContext(), 200);
        if (buildSTInfo != null) {
            buildSTInfo.slotId = this.f3116a.b(view);
            if (view.getId() == this.f3116a.e.getId()) {
                if (d.a().j()) {
                    buildSTInfo.status = "01";
                } else {
                    buildSTInfo.status = "02";
                }
            }
        }
        if (view.getId() == this.f3116a.f.getId() && (explicitHotWord = (ExplicitHotWord) this.f3116a.g.e()) != null) {
            buildSTInfo.extraData = explicitHotWord.b + ";" + explicitHotWord.f2067a + ";" + (this.f3116a.g.d() + 1);
            buildSTInfo.searchPreId = this.f3116a.g.j();
        }
        return buildSTInfo;
    }
}
