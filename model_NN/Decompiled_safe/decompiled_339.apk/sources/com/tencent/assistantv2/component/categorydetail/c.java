package com.tencent.assistantv2.component.categorydetail;

import android.view.View;
import com.tencent.assistant.utils.df;

/* compiled from: ProGuard */
class c implements k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CategoryDetailListView f3143a;

    c(CategoryDetailListView categoryDetailListView) {
        this.f3143a = categoryDetailListView;
    }

    public void a() {
        int f;
        int a2 = df.a(this.f3143a.getContext(), 4.0f);
        int i = 0;
        for (int i2 = 0; i2 < this.f3143a.m; i2++) {
            View childAt = this.f3143a.getListView().getChildAt(i2);
            if (childAt != null) {
                i += childAt.getHeight();
            }
        }
        if (this.f3143a.m <= 1) {
            f = a2 + 0;
        } else {
            f = this.f3143a.n - i;
        }
        if (this.f3143a.e != null) {
            this.f3143a.e.d();
        }
        this.f3143a.getListView().setSelectionFromTop(this.f3143a.m, f);
        this.f3143a.getListView().postInvalidate();
    }

    public void b() {
    }

    public void a(int i) {
    }
}
