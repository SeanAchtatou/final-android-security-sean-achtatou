package com.tencent.assistantv2.component;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class be implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MainActionHeaderView f3115a;

    be(MainActionHeaderView mainActionHeaderView) {
        this.f3115a = mainActionHeaderView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.tencent.assistantv2.component.MainActionHeaderView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void run() {
        try {
            ((LayoutInflater) this.f3115a.b.getSystemService("layout_inflater")).inflate((int) R.layout.v2_main_action_header, (ViewGroup) this.f3115a, true);
        } catch (Throwable th) {
        }
        this.f3115a.h();
        this.f3115a.j.register(this.f3115a.p);
    }
}
