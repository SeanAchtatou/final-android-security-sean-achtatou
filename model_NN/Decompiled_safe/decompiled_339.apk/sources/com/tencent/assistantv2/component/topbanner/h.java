package com.tencent.assistantv2.component.topbanner;

import android.graphics.Bitmap;
import com.tencent.assistant.manager.notification.a.a.e;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistantv2.component.topbanner.TopBannerView;

/* compiled from: ProGuard */
class h implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TopBannerView f3258a;

    h(TopBannerView topBannerView) {
        this.f3258a = topBannerView;
    }

    public void a(Bitmap bitmap) {
        if (bitmap == null || bitmap.isRecycled()) {
            XLog.d("topbanner", "fail");
            ba.a().post(new j(this));
            return;
        }
        XLog.d("topbanner", "finish");
        this.f3258a.a(bitmap, TopBannerView.ImageType.BANNER);
        Bitmap unused = this.f3258a.g = this.f3258a.b(bitmap, TopBannerView.ImageType.BANNER);
        ba.a().post(new i(this));
    }
}
