package com.tencent.assistantv2.component;

import android.os.Parcel;
import android.os.Parcelable;
import com.tencent.assistantv2.component.CirclePageIndicator;

/* compiled from: ProGuard */
final class q implements Parcelable.Creator<CirclePageIndicator.SavedState> {
    q() {
    }

    /* renamed from: a */
    public CirclePageIndicator.SavedState createFromParcel(Parcel parcel) {
        return new CirclePageIndicator.SavedState(parcel);
    }

    /* renamed from: a */
    public CirclePageIndicator.SavedState[] newArray(int i) {
        return new CirclePageIndicator.SavedState[i];
    }
}
