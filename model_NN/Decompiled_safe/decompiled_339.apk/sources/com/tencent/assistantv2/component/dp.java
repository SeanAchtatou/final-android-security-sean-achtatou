package com.tencent.assistantv2.component;

import android.view.animation.Animation;

/* compiled from: ProGuard */
class dp implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TxManagerCommContainView f3181a;

    dp(TxManagerCommContainView txManagerCommContainView) {
        this.f3181a = txManagerCommContainView;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f3181a.j.setVisibility(0);
        int childCount = this.f3181a.j.getChildCount();
        if (childCount != 0) {
            for (int i = 0; i < childCount; i++) {
                this.f3181a.j.getChildAt(i).setVisibility(0);
            }
        }
    }
}
