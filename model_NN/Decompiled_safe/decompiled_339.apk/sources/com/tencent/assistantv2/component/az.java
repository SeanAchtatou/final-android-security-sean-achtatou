package com.tencent.assistantv2.component;

import android.content.Context;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.model.SimpleAppModel;

/* compiled from: ProGuard */
class az extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ListItemRelateNewsView f3097a;
    private Context b;
    private SimpleAppModel c;

    public az(ListItemRelateNewsView listItemRelateNewsView, Context context, SimpleAppModel simpleAppModel) {
        this.f3097a = listItemRelateNewsView;
        this.b = context;
        this.c = simpleAppModel;
    }

    public void onTMAClick(View view) {
        if (this.c != null && this.c.aw != null) {
            b.a(this.f3097a.getContext(), this.c.aw.b);
        }
    }
}
