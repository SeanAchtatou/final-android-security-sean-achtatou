package com.tencent.assistantv2.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import com.tencent.assistantv2.component.search.k;
import java.util.List;

/* compiled from: ProGuard */
public class al extends PagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private List<k> f2886a = null;

    public al(List<k> list) {
        this.f2886a = list;
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        if (this.f2886a != null && this.f2886a.get(i) != null) {
            viewGroup.removeView(this.f2886a.get(i).a());
        }
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        if (this.f2886a == null || this.f2886a.get(i) == null) {
            return null;
        }
        viewGroup.addView(this.f2886a.get(i).a());
        return this.f2886a.get(i).a();
    }

    public int getCount() {
        if (this.f2886a == null) {
            return 0;
        }
        return this.f2886a.size();
    }

    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }
}
