package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.e;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.model.SimpleEbookModel;

/* compiled from: ProGuard */
public class k extends g {
    private IViewInvalidater c;
    private ab d;

    public k(Context context, ab abVar, IViewInvalidater iViewInvalidater) {
        super(context, abVar, iViewInvalidater);
        this.c = iViewInvalidater;
        this.d = abVar;
    }

    public Pair<View, Object> a() {
        View inflate = LayoutInflater.from(this.f2908a).inflate((int) R.layout.ebook_rich_universal_item, (ViewGroup) null);
        m mVar = new m();
        mVar.f2923a = (TextView) inflate.findViewById(R.id.ebook_match_result);
        mVar.b = (TXAppIconView) inflate.findViewById(R.id.ebook_icon);
        mVar.b.setInvalidater(this.c);
        mVar.c = (TextView) inflate.findViewById(R.id.ebook_title);
        mVar.d = (TextView) inflate.findViewById(R.id.ebook_update_info);
        mVar.e = (TextView) inflate.findViewById(R.id.ebook_view_count);
        mVar.f = (TextView) inflate.findViewById(R.id.ebook_author);
        mVar.g = (TextView) inflate.findViewById(R.id.ebook_label);
        mVar.h = (TextView) inflate.findViewById(R.id.ebook_words);
        mVar.i = (TextView) inflate.findViewById(R.id.ebook_desc);
        return Pair.create(inflate, mVar);
    }

    public void a(View view, Object obj, int i, e eVar) {
        SimpleEbookModel simpleEbookModel;
        m mVar = (m) obj;
        if (eVar != null) {
            simpleEbookModel = eVar.e();
        } else {
            simpleEbookModel = null;
        }
        view.setOnClickListener(new j(this, this.f2908a, i, simpleEbookModel, this.b));
        a(mVar, simpleEbookModel, i);
    }

    private void a(m mVar, SimpleEbookModel simpleEbookModel, int i) {
        if (simpleEbookModel != null && mVar != null) {
            mVar.f2923a.setText(simpleEbookModel.k);
            mVar.b.updateImageView(simpleEbookModel.c, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            mVar.c.setText(simpleEbookModel.b);
            if (!TextUtils.isEmpty(ct.f(simpleEbookModel.i))) {
                mVar.d.setPadding(0, 0, df.a(this.f2908a, 10.0f), 0);
                mVar.d.setText(ct.f(simpleEbookModel.i));
            } else {
                mVar.d.setPadding(0, 0, 0, 0);
            }
            if (!TextUtils.isEmpty(ct.f(simpleEbookModel.d))) {
                mVar.f.setPadding(0, 0, df.a(this.f2908a, 10.0f), 0);
                mVar.f.setText(ct.f(simpleEbookModel.d));
            } else {
                mVar.f.setPadding(0, 0, 0, 0);
            }
            mVar.g.setText(simpleEbookModel.f);
            mVar.h.setText(simpleEbookModel.g);
            mVar.e.setText(simpleEbookModel.h);
            mVar.i.setText(ct.e(simpleEbookModel.e));
        }
    }
}
