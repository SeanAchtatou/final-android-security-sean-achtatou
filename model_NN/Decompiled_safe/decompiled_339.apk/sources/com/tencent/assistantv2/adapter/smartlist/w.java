package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class w extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    private Context f2929a;
    private int b;
    private SimpleAppModel c;
    private ab d;

    public w(Context context, int i, SimpleAppModel simpleAppModel, ab abVar) {
        this.f2929a = context;
        this.b = i;
        this.c = simpleAppModel;
        this.d = abVar;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.f2929a, AppDetailActivityV5.class);
        intent.putExtra("st_common_data", this.d.e());
        intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, this.c);
        this.f2929a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.d == null || this.d.e() == null) {
            return null;
        }
        this.d.e().actionId = 200;
        this.d.e().status = "01";
        return this.d.e();
    }
}
