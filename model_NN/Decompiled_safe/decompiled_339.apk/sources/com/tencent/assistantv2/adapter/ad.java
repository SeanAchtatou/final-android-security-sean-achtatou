package com.tencent.assistantv2.adapter;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistant.utils.e;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class ad {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2878a = false;
    private boolean b = true;
    private ArrayList<SimpleAppModel> c = new ArrayList<>(30);
    private ArrayList<SimpleAppModel> d = new ArrayList<>(30);

    public ad(boolean z) {
        this.f2878a = z;
        if (d()) {
            this.d = new ArrayList<>(30);
        }
    }

    private boolean d() {
        return this.f2878a && this.b;
    }

    public int a() {
        return d() ? this.d.size() : this.c.size();
    }

    public SimpleAppModel a(int i) {
        return d() ? this.d.get(i) : this.c.get(i);
    }

    public int a(SimpleAppModel simpleAppModel) {
        return this.c.indexOf(simpleAppModel) + 1;
    }

    public void a(List<SimpleAppModel> list) {
        if (d()) {
            this.d.addAll(b(list));
        }
        this.c.addAll(list);
    }

    public void b() {
        this.c.clear();
        if (this.d != null) {
            this.d.clear();
        }
    }

    public boolean a(boolean z) {
        if (z == this.f2878a) {
            return false;
        }
        this.f2878a = z;
        if (d()) {
            if (this.c.size() > 0) {
                if (this.d == null) {
                    this.d = new ArrayList<>(30);
                }
                this.d.addAll(b(this.c));
            }
        } else if (this.d != null) {
            this.d.clear();
        }
        return true;
    }

    private List<SimpleAppModel> b(List<SimpleAppModel> list) {
        ArrayList arrayList = new ArrayList();
        for (SimpleAppModel next : list) {
            if (!e.a(next.c, next.g)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public void c() {
        u.g(this.c);
    }

    public boolean a(String str) {
        ArrayList<SimpleAppModel> arrayList = this.c;
        if (d()) {
            arrayList = this.d;
        }
        if (arrayList == null || arrayList.size() == 0) {
            return false;
        }
        for (SimpleAppModel q : arrayList) {
            if (q.q().equals(str)) {
                return true;
            }
        }
        return false;
    }
}
