package com.tencent.assistantv2.adapter;

import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.assistant.component.AppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;

/* compiled from: ProGuard */
class ai {

    /* renamed from: a  reason: collision with root package name */
    TextView f2883a;
    TXImageView b;
    ImageView c;
    ImageView d;
    AppIconView e;
    DownloadButton f;
    ListItemInfoView g;
    TextView h;
    TextView i;
    final /* synthetic */ RankNormalListAdapter j;

    private ai(RankNormalListAdapter rankNormalListAdapter) {
        this.j = rankNormalListAdapter;
    }

    /* synthetic */ ai(RankNormalListAdapter rankNormalListAdapter, ae aeVar) {
        this(rankNormalListAdapter);
    }
}
