package com.tencent.wcs.jce;

import java.io.Serializable;

/* compiled from: ProGuard */
public final class DeviceType implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final DeviceType f3863a = new DeviceType(0, 0, "DEVICE_TYPE_UNKNOWN");
    public static final DeviceType b = new DeviceType(1, 1, "DEVICE_TYPE_PC");
    public static final DeviceType c = new DeviceType(2, 2, "DEVICE_TYPE_ANDROID");
    public static final DeviceType d = new DeviceType(3, 3, "DEVICE_TYPE_WEB");
    static final /* synthetic */ boolean e = (!DeviceType.class.desiredAssertionStatus());
    private static DeviceType[] f = new DeviceType[4];
    private int g;
    private String h = new String();

    public String toString() {
        return this.h;
    }

    private DeviceType(int i, int i2, String str) {
        this.h = str;
        this.g = i2;
        f[i] = this;
    }
}
