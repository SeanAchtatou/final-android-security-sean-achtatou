package com.tencent.wcs.proxy.b;

import com.qq.taf.jce.JceStruct;
import com.tencent.wcs.jce.TransDataMessage;
import com.tencent.wcs.proxy.c.e;

/* compiled from: ProGuard */
public class n extends a<e, Void> {
    public static n b() {
        return p.f3903a;
    }

    private n() {
    }

    /* access modifiers changed from: protected */
    public e a(Void... voidArr) {
        return new e();
    }

    /* access modifiers changed from: protected */
    public boolean b(Void... voidArr) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean a(e eVar) {
        return eVar != null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void c(e eVar) {
        if (eVar != null) {
            if (eVar.b() == 1997) {
                t.b().b((Object) ((TransDataMessage) eVar.c()));
            }
            eVar.a(0);
            eVar.a((JceStruct) null);
        }
    }
}
