package com.tencent.wcs.b;

import com.tencent.assistant.utils.FileUtil;
import com.tencent.connect.common.Constants;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

/* compiled from: ProGuard */
public class a {
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0022 A[SYNTHETIC, Splitter:B:20:0x0022] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r3) {
        /*
            r0 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0010, all -> 0x001d }
            r2.<init>(r3)     // Catch:{ FileNotFoundException -> 0x0010, all -> 0x001d }
            java.lang.String r0 = a(r2)     // Catch:{ FileNotFoundException -> 0x002a }
            if (r2 == 0) goto L_0x000f
            r2.close()     // Catch:{ IOException -> 0x001b }
        L_0x000f:
            return r0
        L_0x0010:
            r1 = move-exception
            r2 = r0
        L_0x0012:
            r1.printStackTrace()     // Catch:{ all -> 0x0028 }
            if (r2 == 0) goto L_0x000f
            r2.close()     // Catch:{ IOException -> 0x001b }
            goto L_0x000f
        L_0x001b:
            r1 = move-exception
            goto L_0x000f
        L_0x001d:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0020:
            if (r2 == 0) goto L_0x0025
            r2.close()     // Catch:{ IOException -> 0x0026 }
        L_0x0025:
            throw r0
        L_0x0026:
            r1 = move-exception
            goto L_0x0025
        L_0x0028:
            r0 = move-exception
            goto L_0x0020
        L_0x002a:
            r1 = move-exception
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wcs.b.a.a(java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002f A[SYNTHETIC, Splitter:B:14:0x002f] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003d A[SYNTHETIC, Splitter:B:21:0x003d] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0046 A[SYNTHETIC, Splitter:B:26:0x0046] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.io.InputStream r5) {
        /*
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            if (r5 != 0) goto L_0x000c
            java.lang.String r0 = r3.toString()
        L_0x000b:
            return r0
        L_0x000c:
            r2 = 0
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ UnsupportedEncodingException -> 0x0058, IOException -> 0x0037 }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ UnsupportedEncodingException -> 0x0058, IOException -> 0x0037 }
            java.lang.String r4 = "UTF-8"
            r0.<init>(r5, r4)     // Catch:{ UnsupportedEncodingException -> 0x0058, IOException -> 0x0037 }
            r1.<init>(r0)     // Catch:{ UnsupportedEncodingException -> 0x0058, IOException -> 0x0037 }
        L_0x0019:
            java.lang.String r0 = r1.readLine()     // Catch:{ UnsupportedEncodingException -> 0x0029, IOException -> 0x0055 }
            if (r0 == 0) goto L_0x004c
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x0029, IOException -> 0x0055 }
            java.lang.String r2 = "\n"
            r0.append(r2)     // Catch:{ UnsupportedEncodingException -> 0x0029, IOException -> 0x0055 }
            goto L_0x0019
        L_0x0029:
            r0 = move-exception
        L_0x002a:
            r0.printStackTrace()     // Catch:{ all -> 0x0052 }
            if (r1 == 0) goto L_0x0032
            r1.close()     // Catch:{ IOException -> 0x0041 }
        L_0x0032:
            java.lang.String r0 = r3.toString()
            goto L_0x000b
        L_0x0037:
            r0 = move-exception
        L_0x0038:
            r0.printStackTrace()     // Catch:{ all -> 0x0043 }
            if (r2 == 0) goto L_0x0032
            r2.close()     // Catch:{ IOException -> 0x0041 }
            goto L_0x0032
        L_0x0041:
            r0 = move-exception
            goto L_0x0032
        L_0x0043:
            r0 = move-exception
        L_0x0044:
            if (r2 == 0) goto L_0x0049
            r2.close()     // Catch:{ IOException -> 0x004a }
        L_0x0049:
            throw r0
        L_0x004a:
            r1 = move-exception
            goto L_0x0049
        L_0x004c:
            if (r1 == 0) goto L_0x0032
            r1.close()     // Catch:{ IOException -> 0x0041 }
            goto L_0x0032
        L_0x0052:
            r0 = move-exception
            r2 = r1
            goto L_0x0044
        L_0x0055:
            r0 = move-exception
            r2 = r1
            goto L_0x0038
        L_0x0058:
            r0 = move-exception
            r1 = r2
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wcs.b.a.a(java.io.InputStream):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0025 A[SYNTHETIC, Splitter:B:20:0x0025] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(android.content.Context r3, java.lang.String r4) {
        /*
            r0 = 0
            android.content.res.AssetManager r1 = r3.getAssets()     // Catch:{ IOException -> 0x0013, all -> 0x0020 }
            java.io.InputStream r2 = r1.open(r4)     // Catch:{ IOException -> 0x0013, all -> 0x0020 }
            java.lang.String r0 = a(r2)     // Catch:{ IOException -> 0x002d }
            if (r2 == 0) goto L_0x0012
            r2.close()     // Catch:{ IOException -> 0x001e }
        L_0x0012:
            return r0
        L_0x0013:
            r1 = move-exception
            r2 = r0
        L_0x0015:
            r1.printStackTrace()     // Catch:{ all -> 0x002b }
            if (r2 == 0) goto L_0x0012
            r2.close()     // Catch:{ IOException -> 0x001e }
            goto L_0x0012
        L_0x001e:
            r1 = move-exception
            goto L_0x0012
        L_0x0020:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0023:
            if (r2 == 0) goto L_0x0028
            r2.close()     // Catch:{ IOException -> 0x0029 }
        L_0x0028:
            throw r0
        L_0x0029:
            r1 = move-exception
            goto L_0x0028
        L_0x002b:
            r0 = move-exception
            goto L_0x0023
        L_0x002d:
            r1 = move-exception
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wcs.b.a.a(android.content.Context, java.lang.String):java.lang.String");
    }

    public static void b(String str) {
        if (str != null) {
            a(str.getBytes());
        }
    }

    public static void a(byte[] bArr) {
        if (bArr != null) {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            DataInputStream dataInputStream = new DataInputStream(byteArrayInputStream);
            while (true) {
                try {
                    String readLine = dataInputStream.readLine();
                    if (readLine == null) {
                        byteArrayInputStream.close();
                        dataInputStream.close();
                        return;
                    } else if (!readLine.startsWith("#") && !readLine.equals(Constants.STR_EMPTY) && !readLine.trim().equals(Constants.STR_EMPTY) && readLine.indexOf("=") != -1) {
                        String[] split = readLine.split("=");
                        if (split.length >= 2) {
                            c.a(split[0].trim(), split[1].trim());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    try {
                        byteArrayInputStream.close();
                        dataInputStream.close();
                        return;
                    } catch (IOException e2) {
                        return;
                    }
                } catch (Throwable th) {
                    try {
                        byteArrayInputStream.close();
                        dataInputStream.close();
                    } catch (IOException e3) {
                    }
                    throw th;
                }
            }
        }
    }

    public static void a(byte[] bArr, String str) {
        FileUtil.write2File(bArr, str);
    }
}
