package com.tencent.assistant.kapalaiadapter.a;

import android.content.Context;
import android.telephony.TelephonyManager;
import com.tencent.assistant.kapalaiadapter.g;

/* compiled from: ProGuard */
public class i implements j {

    /* renamed from: a  reason: collision with root package name */
    private TelephonyManager[] f1387a = null;

    public Object a(int i, Context context) {
        char c;
        if (this.f1387a == null) {
            try {
                this.f1387a = new TelephonyManager[2];
                this.f1387a[0] = (TelephonyManager) g.a("android.telephony.TelephonyManager", "getDefault");
                this.f1387a[1] = (TelephonyManager) g.a("android.telephony.TelephonyManager", "getSecondary");
            } catch (Exception e) {
            }
        }
        if (this.f1387a == null || this.f1387a.length <= i) {
            return null;
        }
        TelephonyManager[] telephonyManagerArr = this.f1387a;
        if (i <= 0) {
            c = 0;
        } else {
            c = 1;
        }
        return telephonyManagerArr[c];
    }

    public String b(int i, Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) a(i, context);
            if (telephonyManager != null) {
                return telephonyManager.getSubscriberId();
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public String c(int i, Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) a(i, context);
            if (telephonyManager != null) {
                return telephonyManager.getDeviceId();
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
