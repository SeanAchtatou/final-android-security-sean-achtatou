package com.tencent.assistant.backgroundscan;

import com.tencent.assistant.backgroundscan.BackgroundScanManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.securemodule.service.ISecureModuleService;

/* compiled from: ProGuard */
class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ISecureModuleService f861a;
    final /* synthetic */ BackgroundScanManager b;

    l(BackgroundScanManager backgroundScanManager, ISecureModuleService iSecureModuleService) {
        this.b = backgroundScanManager;
        this.f861a = iSecureModuleService;
    }

    public void run() {
        this.f861a.unregisterCloudScanListener(this.b.e, this.b.f847a);
        this.b.i.put((byte) 6, BackgroundScanManager.SStatus.none);
        XLog.e("BackgroundScan", "<scan> virus scan timeout !!!");
    }
}
