package com.tencent.assistant.backgroundscan;

import com.tencent.assistant.backgroundscan.BackgroundScanManager;
import java.util.Map;

/* compiled from: ProGuard */
class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BackgroundScanManager f855a;

    f(BackgroundScanManager backgroundScanManager) {
        this.f855a = backgroundScanManager;
    }

    public void run() {
        if (this.f855a.k()) {
            n.a().a("b_new_scan_push_begin2", (byte) 0);
        }
        for (Map.Entry entry : this.f855a.i.entrySet()) {
            if (((BackgroundScanManager.SStatus) entry.getValue()) == BackgroundScanManager.SStatus.prepare) {
                switch (((Byte) entry.getKey()).byteValue()) {
                    case 1:
                        this.f855a.l();
                        break;
                    case 2:
                        this.f855a.m();
                        break;
                    case 3:
                        this.f855a.o();
                        break;
                    case 4:
                        this.f855a.p();
                        break;
                    case 5:
                        this.f855a.n();
                        break;
                    case 6:
                        if (this.f855a.g()) {
                            this.f855a.i();
                            break;
                        }
                        break;
                }
                if (this.f855a.g() || ((Byte) entry.getKey()).byteValue() != 6) {
                    n.a().a("b_new_scan_push_start", ((Byte) entry.getKey()).byteValue());
                } else {
                    return;
                }
            }
        }
    }
}
