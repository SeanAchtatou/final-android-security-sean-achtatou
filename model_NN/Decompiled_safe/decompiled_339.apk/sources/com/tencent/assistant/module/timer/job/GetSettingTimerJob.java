package com.tencent.assistant.module.timer.job;

import com.tencent.assistant.m;
import com.tencent.assistant.module.dr;
import com.tencent.assistant.module.timer.SimpleBaseScheduleJob;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class GetSettingTimerJob extends SimpleBaseScheduleJob {

    /* renamed from: a  reason: collision with root package name */
    private static GetSettingTimerJob f1869a;

    public static synchronized GetSettingTimerJob e() {
        GetSettingTimerJob getSettingTimerJob;
        synchronized (GetSettingTimerJob.class) {
            if (f1869a == null) {
                f1869a = new GetSettingTimerJob();
            }
            getSettingTimerJob = f1869a;
        }
        return getSettingTimerJob;
    }

    public void d_() {
        XLog.v("testtest", "getSetting begin works-----");
        dr.a().b();
    }

    public int h() {
        return m.a().a("setting_sync_interval", 10800);
    }
}
