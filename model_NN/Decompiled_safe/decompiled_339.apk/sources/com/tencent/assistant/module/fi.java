package com.tencent.assistant.module;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.protocol.jce.AutoDownloadNewAppUserProfile;
import com.tencent.assistant.protocol.jce.AutoDownloadUserProfile;
import com.tencent.assistant.protocol.jce.SetUserProfileRequest;
import com.tencent.assistant.protocol.jce.UserProfile;
import com.tencent.assistant.utils.bh;
import java.util.ArrayList;

/* compiled from: ProGuard */
class fi implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.WISE_DOWNLOAD_SWITCH_TYPE f1821a;
    final /* synthetic */ boolean b;
    final /* synthetic */ fh c;

    fi(fh fhVar, AppConst.WISE_DOWNLOAD_SWITCH_TYPE wise_download_switch_type, boolean z) {
        this.c = fhVar;
        this.f1821a = wise_download_switch_type;
        this.b = z;
    }

    public void run() {
        UserProfile userProfile;
        int i = 0;
        int i2 = 1;
        if (this.f1821a == AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE) {
            if (this.b) {
                i = 1;
            }
            userProfile = new UserProfile((byte) 1, bh.a(new AutoDownloadUserProfile(i)), 0);
        } else if (this.f1821a == AppConst.WISE_DOWNLOAD_SWITCH_TYPE.NEW_DOWNLOAD) {
            if (!this.b) {
                i2 = 0;
            }
            userProfile = new UserProfile((byte) 3, bh.a(new AutoDownloadNewAppUserProfile(i2)), 0);
            i2 = 3;
        } else {
            return;
        }
        ArrayList<UserProfile> arrayList = new ArrayList<>();
        arrayList.add(userProfile);
        SetUserProfileRequest setUserProfileRequest = new SetUserProfileRequest();
        setUserProfileRequest.f2305a = arrayList;
        this.c.b.put(Integer.valueOf(this.c.send(setUserProfileRequest)), Integer.valueOf(i2));
    }
}
