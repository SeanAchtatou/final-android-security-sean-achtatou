package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.q;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/* compiled from: ProGuard */
class df implements CallbackHelper.Caller<q> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1772a;
    final /* synthetic */ boolean b;
    final /* synthetic */ ArrayList c;
    final /* synthetic */ boolean d;
    final /* synthetic */ de e;

    df(de deVar, int i, boolean z, ArrayList arrayList, boolean z2) {
        this.e = deVar;
        this.f1772a = i;
        this.b = z;
        this.c = arrayList;
        this.d = z2;
    }

    /* renamed from: a */
    public void call(q qVar) {
        q qVar2 = qVar;
        qVar2.a(this.f1772a, 0, this.b, new LinkedHashMap(), this.c, this.d);
    }
}
