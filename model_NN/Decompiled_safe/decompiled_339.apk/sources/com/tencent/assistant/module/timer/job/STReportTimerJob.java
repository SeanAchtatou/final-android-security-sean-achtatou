package com.tencent.assistant.module.timer.job;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.SystemClock;
import com.qq.AppService.AstApp;
import com.tencent.assistant.m;
import com.tencent.assistant.module.timer.SimpleBaseScheduleJob;
import com.tencent.assistantv2.st.b;

/* compiled from: ProGuard */
public class STReportTimerJob extends SimpleBaseScheduleJob {

    /* renamed from: a  reason: collision with root package name */
    private static STReportTimerJob f1872a;

    public static synchronized STReportTimerJob e() {
        STReportTimerJob sTReportTimerJob;
        synchronized (STReportTimerJob.class) {
            if (f1872a == null) {
                f1872a = new STReportTimerJob();
            }
            sTReportTimerJob = f1872a;
        }
        return sTReportTimerJob;
    }

    public int h() {
        return m.a().w();
    }

    public void d_() {
        b.a().b();
    }

    public void d() {
        long j;
        Intent intent = new Intent("com.tencent.android.qqdownloader.action.SCHEDULE_JOB");
        intent.putExtra("com.tencent.android.qqdownloader.key.SCHEDULE_JOB", getClass().getName());
        PendingIntent broadcast = PendingIntent.getBroadcast(AstApp.i(), b(), intent, 134217728);
        int h = h() * 1000;
        long j2 = (long) h;
        if (h > 0) {
            j = ((long) h) - (SystemClock.elapsedRealtime() % ((long) h));
        } else {
            j = j2;
        }
        try {
            ((AlarmManager) AstApp.i().getSystemService("alarm")).setRepeating(3, j + SystemClock.elapsedRealtime(), (long) h, broadcast);
        } catch (Throwable th) {
        }
    }
}
