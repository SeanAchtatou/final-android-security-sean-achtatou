package com.tencent.assistant.module.update;

import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ k f1890a;

    m(k kVar) {
        this.f1890a = kVar;
    }

    public void run() {
        Map<Integer, ArrayList<AppUpdateInfo>> h = as.w().h();
        boolean z = false;
        if (h != null && !h.isEmpty()) {
            Iterator<Integer> it = h.keySet().iterator();
            while (it.hasNext()) {
                List list = h.get(it.next());
                if (list != null && !list.isEmpty()) {
                    Iterator it2 = list.iterator();
                    while (it2.hasNext()) {
                        AppUpdateInfo appUpdateInfo = (AppUpdateInfo) it2.next();
                        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(appUpdateInfo.f2006a);
                        if (localApkInfo == null || localApkInfo.mVersionCode >= appUpdateInfo.d) {
                            it2.remove();
                            z = true;
                        }
                    }
                }
                if (list == null || list.isEmpty()) {
                    it.remove();
                }
            }
        }
        if (z) {
            as.w().a(h);
            this.f1890a.g();
        }
    }
}
