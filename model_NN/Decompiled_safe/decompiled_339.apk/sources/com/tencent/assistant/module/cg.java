package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistantv2.model.a.e;
import java.util.ArrayList;

/* compiled from: ProGuard */
class cg implements CallbackHelper.Caller<e> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f1748a;
    final /* synthetic */ byte[] b;
    final /* synthetic */ ArrayList c;
    final /* synthetic */ ce d;

    cg(ce ceVar, boolean z, byte[] bArr, ArrayList arrayList) {
        this.d = ceVar;
        this.f1748a = z;
        this.b = bArr;
        this.c = arrayList;
    }

    /* renamed from: a */
    public void call(e eVar) {
        eVar.a(-1, 0, this.f1748a, this.b, true, this.c, this.d.c, this.d.l);
    }
}
