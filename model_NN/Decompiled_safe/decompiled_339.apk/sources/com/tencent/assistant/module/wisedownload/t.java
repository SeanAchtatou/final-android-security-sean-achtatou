package com.tencent.assistant.module.wisedownload;

import android.text.TextUtils;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.module.update.u;
import com.tencent.assistant.protocol.jce.AutoDownloadCfg;
import com.tencent.assistant.protocol.jce.AutoDownloadInfo;
import com.tencent.assistant.utils.cv;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class t {
    public static List<DownloadInfo> a(boolean z, boolean z2) {
        ArrayList arrayList = new ArrayList();
        ArrayList<DownloadInfo> d = DownloadProxy.a().d();
        if (d == null || d.isEmpty()) {
            return null;
        }
        Iterator<DownloadInfo> it = d.iterator();
        while (it.hasNext()) {
            DownloadInfo next = it.next();
            if (next.uiType == SimpleDownloadInfo.UIType.WISE_NEW_DOWNLOAD) {
                arrayList.add(next);
            }
        }
        if (z) {
            if (arrayList.isEmpty()) {
                return null;
            }
            d.clear();
            d.addAll(arrayList);
            arrayList.clear();
            Iterator<DownloadInfo> it2 = d.iterator();
            while (it2.hasNext()) {
                DownloadInfo next2 = it2.next();
                if (next2.downloadState == SimpleDownloadInfo.DownloadState.SUCC) {
                    arrayList.add(next2);
                }
            }
        }
        if (z2) {
            if (arrayList.isEmpty()) {
                return null;
            }
            d.clear();
            d.addAll(arrayList);
            arrayList.clear();
            Iterator<DownloadInfo> it3 = d.iterator();
            while (it3.hasNext()) {
                DownloadInfo next3 = it3.next();
                String filePath = next3.getFilePath();
                if (!TextUtils.isEmpty(filePath) && new File(filePath).exists()) {
                    arrayList.add(next3);
                }
            }
        }
        return arrayList;
    }

    public static List<AutoDownloadInfo> a() {
        List<AutoDownloadInfo> f;
        List<DownloadInfo> a2;
        long P = m.a().P();
        if (cv.b(P) || (f = u.a().f()) == null || f.isEmpty() || (a2 = a(true)) == null || a2.isEmpty()) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (AutoDownloadInfo next : f) {
            Iterator<DownloadInfo> it = a2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                DownloadInfo next2 = it.next();
                if (next2.packageName.endsWith(next.f2010a) && next2.versionCode == next.d) {
                    if (P < next2.downloadEndTime) {
                        arrayList.add(next);
                    }
                }
            }
        }
        return arrayList;
    }

    public static boolean b() {
        List<AutoDownloadInfo> a2 = a();
        return a2 != null && !a2.isEmpty();
    }

    public static List<DownloadInfo> a(boolean z) {
        ArrayList arrayList = new ArrayList();
        List<DownloadInfo> a2 = a(true, true);
        long a3 = a(c());
        if (a2 == null || a2.isEmpty()) {
            return null;
        }
        for (DownloadInfo next : a2) {
            if (next != null) {
                if (z) {
                    if (next.downloadEndTime < a3) {
                    }
                } else if (next.downloadEndTime >= a3) {
                }
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public static long a(int i) {
        long currentTimeMillis = System.currentTimeMillis();
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(currentTimeMillis);
        instance.set(6, instance.get(6) - i);
        return instance.getTimeInMillis();
    }

    public static int c() {
        AutoDownloadCfg i = as.w().i();
        if (i != null) {
            return i.j;
        }
        return 0;
    }

    public static void d() {
        List<DownloadInfo> a2 = a(false);
        if (a2 != null && !a2.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            List<AutoDownloadInfo> f = u.a().f();
            if (f != null && !f.isEmpty()) {
                for (DownloadInfo next : a2) {
                    for (AutoDownloadInfo next2 : f) {
                        if (next.packageName.endsWith(next2.f2010a) && next.versionCode == next2.d) {
                            arrayList.add(next2);
                        }
                    }
                }
                a(arrayList);
            }
        }
    }

    public static void e() {
        List<AutoDownloadInfo> j = u.a().j();
        if (j != null && !j.isEmpty()) {
            a(j);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.DownloadProxy.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.manager.DownloadProxy.b(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>):void
      com.tencent.assistant.manager.DownloadProxy.b(java.lang.String, boolean):void */
    public static void a(List<AutoDownloadInfo> list) {
        if (list != null && !list.isEmpty()) {
            boolean z = false;
            for (AutoDownloadInfo next : list) {
                z |= u.a().a(next);
                DownloadInfo a2 = DownloadProxy.a().a(next.f2010a, next.d);
                if (a2 != null) {
                    DownloadProxy.a().b(a2.downloadTicket, true);
                }
            }
            if (z) {
                u.a().k();
            }
        }
    }
}
