package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.k;
import com.tencent.assistant.protocol.jce.ModifyAppCommentRequest;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class em implements CallbackHelper.Caller<k> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1799a;
    final /* synthetic */ int b;
    final /* synthetic */ ModifyAppCommentRequest c;
    final /* synthetic */ ek d;

    em(ek ekVar, int i, int i2, ModifyAppCommentRequest modifyAppCommentRequest) {
        this.d = ekVar;
        this.f1799a = i;
        this.b = i2;
        this.c = modifyAppCommentRequest;
    }

    /* renamed from: a */
    public void call(k kVar) {
        kVar.a(this.f1799a, this.b, this.c.f2235a, Constants.STR_EMPTY, -1, -1);
    }
}
