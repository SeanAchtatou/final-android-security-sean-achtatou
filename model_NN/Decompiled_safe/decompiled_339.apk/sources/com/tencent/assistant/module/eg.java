package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.u;
import com.tencent.assistant.protocol.jce.GetUserInfoRequest;
import com.tencent.assistant.protocol.jce.GetUserInfoResponse;

/* compiled from: ProGuard */
class eg implements CallbackHelper.Caller<u> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1794a;
    final /* synthetic */ int b;
    final /* synthetic */ JceStruct c;
    final /* synthetic */ JceStruct d;
    final /* synthetic */ ee e;

    eg(ee eeVar, int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        this.e = eeVar;
        this.f1794a = i;
        this.b = i2;
        this.c = jceStruct;
        this.d = jceStruct2;
    }

    /* renamed from: a */
    public void call(u uVar) {
        uVar.a(this.f1794a, this.b, (GetUserInfoRequest) this.c, (GetUserInfoResponse) this.d);
    }
}
