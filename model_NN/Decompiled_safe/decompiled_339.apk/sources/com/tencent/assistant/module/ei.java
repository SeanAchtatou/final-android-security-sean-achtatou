package com.tencent.assistant.module;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.tencent.assistant.module.callback.v;
import com.tencent.assistant.utils.be;

/* compiled from: ProGuard */
public class ei extends AsyncTask<Bundle, Void, be> {

    /* renamed from: a  reason: collision with root package name */
    private Handler f1796a = new Handler(Looper.getMainLooper());
    private Context b;
    private String c;
    private String d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public v f;

    public ei(Context context, String str, String str2, int i, v vVar) {
        this.b = context;
        this.c = str;
        this.d = str2;
        this.f = vVar;
        this.e = i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(be beVar) {
        if (!isCancelled()) {
            this.f1796a.post(new ej(this, beVar));
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002d, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002e, code lost:
        r8 = r2;
        r2 = r1;
        r1 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0055, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0056, code lost:
        r0.printStackTrace();
        r1.d = -1;
        r1.e = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x005e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005f, code lost:
        r0.printStackTrace();
        r1.d = -1;
        r1.e = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0067, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0068, code lost:
        r8 = r2;
        r2 = r1;
        r1 = r8;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0055 A[ExcHandler: IOException (r0v4 'e' java.io.IOException A[CUSTOM_DECLARE]), PHI: r1 
      PHI: (r1v5 com.tencent.assistant.utils.be) = (r1v1 com.tencent.assistant.utils.be), (r1v14 com.tencent.assistant.utils.be), (r1v14 com.tencent.assistant.utils.be), (r1v1 com.tencent.assistant.utils.be) binds: [B:19:0x003c, B:13:0x002a, B:14:?, B:8:0x0019] A[DONT_GENERATE, DONT_INLINE], Splitter:B:8:0x0019] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x005e A[ExcHandler: Exception (r0v3 'e' java.lang.Exception A[CUSTOM_DECLARE]), PHI: r1 
      PHI: (r1v4 com.tencent.assistant.utils.be) = (r1v1 com.tencent.assistant.utils.be), (r1v14 com.tencent.assistant.utils.be), (r1v14 com.tencent.assistant.utils.be), (r1v1 com.tencent.assistant.utils.be) binds: [B:19:0x003c, B:13:0x002a, B:14:?, B:8:0x0019] A[DONT_GENERATE, DONT_INLINE], Splitter:B:8:0x0019] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x000d A[EDGE_INSN: B:34:0x000d->B:2:0x000d ?: BREAK  , SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.tencent.assistant.utils.be doInBackground(android.os.Bundle... r10) {
        /*
            r9 = this;
            r0 = 0
            r7 = -1
            com.tencent.assistant.utils.be r1 = new com.tencent.assistant.utils.be
            r1.<init>()
            boolean r2 = r9.isCancelled()
            if (r2 == 0) goto L_0x000e
        L_0x000d:
            return r1
        L_0x000e:
            r3 = 3
        L_0x000f:
            boolean r2 = r9.isCancelled()
            if (r2 != 0) goto L_0x000d
            int r0 = r0 + 1
            if (r10 == 0) goto L_0x003c
            int r2 = r10.length     // Catch:{ ConnectTimeoutException -> 0x006c, SocketTimeoutException -> 0x0048, IOException -> 0x0055, Exception -> 0x005e }
            if (r2 <= 0) goto L_0x003c
            android.content.Context r2 = r9.b     // Catch:{ ConnectTimeoutException -> 0x006c, SocketTimeoutException -> 0x0048, IOException -> 0x0055, Exception -> 0x005e }
            java.lang.String r4 = r9.c     // Catch:{ ConnectTimeoutException -> 0x006c, SocketTimeoutException -> 0x0048, IOException -> 0x0055, Exception -> 0x005e }
            java.lang.String r5 = r9.d     // Catch:{ ConnectTimeoutException -> 0x006c, SocketTimeoutException -> 0x0048, IOException -> 0x0055, Exception -> 0x005e }
            r6 = 0
            r6 = r10[r6]     // Catch:{ ConnectTimeoutException -> 0x006c, SocketTimeoutException -> 0x0048, IOException -> 0x0055, Exception -> 0x005e }
            com.tencent.assistant.utils.be r1 = com.tencent.assistant.utils.bd.a(r2, r4, r5, r6)     // Catch:{ ConnectTimeoutException -> 0x006c, SocketTimeoutException -> 0x0048, IOException -> 0x0055, Exception -> 0x005e }
        L_0x0029:
            r2 = 0
            r1.d = r2     // Catch:{ ConnectTimeoutException -> 0x002d, SocketTimeoutException -> 0x0067, IOException -> 0x0055, Exception -> 0x005e }
            goto L_0x000d
        L_0x002d:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
        L_0x0031:
            r1.printStackTrace()
            r2.d = r7
            r2.e = r1
            r1 = r2
        L_0x0039:
            if (r0 < r3) goto L_0x000f
            goto L_0x000d
        L_0x003c:
            android.content.Context r2 = r9.b     // Catch:{ ConnectTimeoutException -> 0x006c, SocketTimeoutException -> 0x0048, IOException -> 0x0055, Exception -> 0x005e }
            java.lang.String r4 = r9.c     // Catch:{ ConnectTimeoutException -> 0x006c, SocketTimeoutException -> 0x0048, IOException -> 0x0055, Exception -> 0x005e }
            java.lang.String r5 = r9.d     // Catch:{ ConnectTimeoutException -> 0x006c, SocketTimeoutException -> 0x0048, IOException -> 0x0055, Exception -> 0x005e }
            r6 = 0
            com.tencent.assistant.utils.be r1 = com.tencent.assistant.utils.bd.a(r2, r4, r5, r6)     // Catch:{ ConnectTimeoutException -> 0x006c, SocketTimeoutException -> 0x0048, IOException -> 0x0055, Exception -> 0x005e }
            goto L_0x0029
        L_0x0048:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
        L_0x004c:
            r1.printStackTrace()
            r2.d = r7
            r2.e = r1
            r1 = r2
            goto L_0x0039
        L_0x0055:
            r0 = move-exception
            r0.printStackTrace()
            r1.d = r7
            r1.e = r0
            goto L_0x000d
        L_0x005e:
            r0 = move-exception
            r0.printStackTrace()
            r1.d = r7
            r1.e = r0
            goto L_0x000d
        L_0x0067:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x004c
        L_0x006c:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.module.ei.doInBackground(android.os.Bundle[]):com.tencent.assistant.utils.be");
    }
}
