package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistant.model.QuickEntranceNotify;
import com.tencent.assistant.module.callback.q;
import com.tencent.assistant.protocol.jce.AppGroup;
import com.tencent.assistant.protocol.jce.GetGroupAppsRequest;
import com.tencent.assistant.protocol.jce.GetGroupAppsResponse;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/* compiled from: ProGuard */
public class de extends BaseEngine<q> implements eb {
    private static int d = 5;

    /* renamed from: a  reason: collision with root package name */
    private int f1771a;
    private long b;
    private byte[] c;
    private int e;
    private int f;
    private boolean g;

    public de() {
        this.f1771a = -1;
        this.b = -1;
        this.e = 0;
        this.f = 0;
        this.g = false;
        this.c = new byte[0];
        dy.a().a(this);
    }

    public de(boolean z) {
        this.f1771a = -1;
        this.b = -1;
        this.e = 0;
        this.f = 0;
        this.g = false;
        this.g = z;
        this.c = new byte[0];
        dy.a().a(this);
    }

    public void a() {
        this.c = null;
        this.f1771a = -1;
        this.b = -1;
    }

    public void b() {
        this.c = new byte[0];
    }

    public int a(int i) {
        return a(i, 0);
    }

    public int a(int i, int i2) {
        XLog.d("million", "GetGroupAppsEngine refreshData(is first page)...");
        this.e = i;
        this.f = i2;
        if (a(true)) {
            return -1;
        }
        if (this.f1771a != -1) {
            cancel(this.f1771a);
        }
        b();
        this.f1771a = f();
        return this.f1771a;
    }

    public long c() {
        return this.b;
    }

    public int e() {
        XLog.d("million", "GetGroupAppsEngine refreshNextPageData...");
        if (this.c == null || this.c.length == 0 || a(false)) {
            return -1;
        }
        XLog.d("million", "refreshNextPageData... contextData = " + this.c);
        this.f1771a = f();
        return this.f1771a;
    }

    private boolean a(boolean z) {
        GetGroupAppsResponse a2;
        boolean z2 = false;
        if (this.g || (a2 = as.w().a(g(), this.c)) == null || a2.a() == null || a2.a().size() <= 0) {
            return false;
        }
        this.b = a2.c;
        if (this.b != m.a().a((byte) 3)) {
            return false;
        }
        if (a2.e == 1) {
            z2 = true;
        }
        this.c = a2.d;
        ArrayList<Long> b2 = a2.b();
        if (a2.a() != null) {
            a(-1, z, z2, b2, a2.a());
        }
        return true;
    }

    private int f() {
        if (this.e == 0) {
            return -1;
        }
        GetGroupAppsRequest getGroupAppsRequest = new GetGroupAppsRequest();
        getGroupAppsRequest.f2129a = this.e;
        getGroupAppsRequest.c = d;
        getGroupAppsRequest.b = this.c;
        getGroupAppsRequest.d = this.f;
        XLog.d("million", "GetGroupAppsEngine sendRequest groupId=" + getGroupAppsRequest.d);
        return send(getGroupAppsRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        boolean z;
        if (this.f1771a == i) {
            GetGroupAppsResponse getGroupAppsResponse = (GetGroupAppsResponse) jceStruct2;
            GetGroupAppsRequest getGroupAppsRequest = (GetGroupAppsRequest) jceStruct;
            this.c = getGroupAppsResponse.d;
            XLog.d("million", "onRequestSuccessed... contextData = " + this.c);
            XLog.d("million", "onRequestSuccessed... revision = " + getGroupAppsResponse.c + " mVersion = " + this.b);
            boolean z2 = getGroupAppsResponse.c != this.b || getGroupAppsRequest.b == null || getGroupAppsRequest.b.length == 0;
            XLog.d("million", "onRequestSuccessed... isFirst = " + z2);
            this.b = getGroupAppsResponse.c;
            if (getGroupAppsResponse.e == 1) {
                z = true;
            } else {
                z = false;
            }
            ArrayList<AppGroup> a2 = getGroupAppsResponse.a();
            ArrayList<Long> b2 = getGroupAppsResponse.b();
            if (a2 == null || a2.size() == 0) {
                notifyDataChangedInMainThread(new df(this, i, z2, b2, z));
                return;
            }
            a(i, z2, z, b2, a2);
            as.w().a(g(), getGroupAppsRequest.b, getGroupAppsResponse);
        }
    }

    private void a(int i, boolean z, boolean z2, ArrayList<Long> arrayList, List<AppGroup> list) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (AppGroup next : list) {
            AppGroupInfo appGroupInfo = new AppGroupInfo();
            appGroupInfo.a(next.f1995a);
            appGroupInfo.b(next.c);
            appGroupInfo.c(next.d);
            appGroupInfo.a(next.b);
            appGroupInfo.a(next.f);
            appGroupInfo.b(next.g);
            if (next.h != null) {
                appGroupInfo.a(next.h);
            }
            linkedHashMap.put(appGroupInfo, u.b(next.e));
        }
        notifyDataChangedInMainThread(new dg(this, i, z, linkedHashMap, arrayList, z2));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        GetGroupAppsRequest getGroupAppsRequest = (GetGroupAppsRequest) jceStruct;
        notifyDataChangedInMainThread(new dh(this, i, i2, getGroupAppsRequest.b == null || getGroupAppsRequest.b.length == 0));
    }

    public void d() {
        if (this.b != m.a().a((byte) 3)) {
            a(this.e, this.f);
        }
    }

    public void a(ArrayList<QuickEntranceNotify> arrayList) {
    }

    private String g() {
        return this.e + "_" + this.f;
    }
}
