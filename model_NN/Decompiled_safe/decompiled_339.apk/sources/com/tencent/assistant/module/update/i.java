package com.tencent.assistant.module.update;

import android.os.Bundle;
import android.os.Message;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.module.update.AppUpdateConst;
import com.tencent.assistant.utils.XLog;
import java.util.List;

/* compiled from: ProGuard */
class i extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f1886a;

    i(c cVar) {
        this.f1886a = cVar;
    }

    public void onLoadInstalledApkSuccess(List<LocalApkInfo> list) {
    }

    public void onLoadInstalledApkExtraSuccess(List<LocalApkInfo> list) {
        XLog.d("AppUpdateEngine", "onLoadInstalledApkExtraSuccess 扫描成功 ...... triggerType=" + this.f1886a.b + ",apkinfo size:" + (list != null ? list.size() : 0));
        if (this.f1886a.b != AppUpdateConst.RequestLaunchType.TYPE_DEFAULT && list != null && list.size() > 0) {
            Message message = new Message();
            message.what = 2;
            if (this.f1886a.j.hasMessages(2)) {
                this.f1886a.j.removeMessages(2);
                this.f1886a.j.sendMessageDelayed(message, 30000);
                return;
            }
            this.f1886a.j.sendMessage(message);
        }
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i, boolean z) {
        if (localApkInfo != null) {
            XLog.d("AppUpdateEngine", "onInstalledApkDataChanged  changedType=" + i);
            Message message = new Message();
            message.obj = localApkInfo;
            message.what = 1;
            message.arg1 = i;
            Bundle bundle = new Bundle();
            bundle.putBoolean("isReplacing", z);
            message.setData(bundle);
            this.f1886a.j.sendMessage(message);
        }
    }

    public boolean onInstallApkChangedNeedReplacedEvent() {
        return true;
    }
}
