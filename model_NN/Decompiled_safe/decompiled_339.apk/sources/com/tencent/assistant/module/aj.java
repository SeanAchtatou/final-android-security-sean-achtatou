package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.ab;
import com.tencent.assistant.protocol.jce.SuggestResponse;
import java.util.ArrayList;

/* compiled from: ProGuard */
class aj implements CallbackHelper.Caller<ab> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ArrayList f1694a;
    final /* synthetic */ SuggestResponse b;
    final /* synthetic */ ai c;

    aj(ai aiVar, ArrayList arrayList, SuggestResponse suggestResponse) {
        this.c = aiVar;
        this.f1694a = arrayList;
        this.b = suggestResponse;
    }

    /* renamed from: a */
    public void call(ab abVar) {
        abVar.a(this.f1694a, this.b.b, 0);
    }
}
