package com.tencent.assistant.module;

import com.tencent.assistant.protocol.jce.AppCategory;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class d {

    /* renamed from: a  reason: collision with root package name */
    protected ArrayList<d> f1766a = null;
    protected d b;
    protected AppCategory c;

    public d(AppCategory appCategory) {
        this.c = appCategory;
    }

    public void a(d dVar) {
        if (dVar != null) {
            dVar.b = this;
            if (this.f1766a == null) {
                this.f1766a = new ArrayList<>();
            }
            this.f1766a.add(dVar);
        }
    }
}
