package com.tencent.assistant.module;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;

/* compiled from: ProGuard */
class bw implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bp f1735a;
    private int b = 0;
    /* access modifiers changed from: private */
    public byte[] c;
    private long d;
    /* access modifiers changed from: private */
    public boolean e = true;
    private ArrayList<SimpleAppModel> f;
    private byte[] g;
    /* access modifiers changed from: private */
    public int h = -1;

    public bw(bp bpVar) {
        this.f1735a = bpVar;
    }

    public void a(byte[] bArr) {
        if (bArr != null && bArr.length != 0) {
            b();
            this.b = 1;
            this.c = bArr;
            if (this.f1735a.e) {
                this.h = this.f1735a.getUniqueId();
                TemporaryThreadManager.get().start(new bx(this));
                return;
            }
            XLog.d("voken", "next exec pageContext = " + bArr);
            this.h = this.f1735a.a(this.c);
        }
    }

    public int a() {
        return this.h;
    }

    public void b() {
        this.h = -1;
        this.c = null;
        this.e = true;
        this.g = null;
        this.f = null;
        this.b = 0;
    }

    public boolean c() {
        return this.b == 1;
    }

    public byte[] d() {
        return this.c;
    }

    public ArrayList<SimpleAppModel> e() {
        return this.f;
    }

    public boolean f() {
        return this.e;
    }

    public byte[] g() {
        return this.g;
    }

    public long h() {
        return this.d;
    }

    public void a(long j, ArrayList<SimpleAppModel> arrayList, boolean z, byte[] bArr) {
        this.d = j;
        this.f = arrayList;
        this.e = z;
        this.g = bArr;
        this.b = 2;
    }

    public void i() {
        this.b = 2;
    }

    /* renamed from: j */
    public bw clone() {
        try {
            bw bwVar = (bw) super.clone();
            if (this.f == null) {
                return bwVar;
            }
            bwVar.f = (ArrayList) this.f.clone();
            return bwVar;
        } catch (CloneNotSupportedException e2) {
            return this;
        }
    }
}
