package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.k;
import com.tencent.assistant.protocol.jce.AnswerAppCommentRequest;

/* compiled from: ProGuard */
class b implements CallbackHelper.Caller<k> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1709a;
    final /* synthetic */ AnswerAppCommentRequest b;
    final /* synthetic */ a c;

    b(a aVar, int i, AnswerAppCommentRequest answerAppCommentRequest) {
        this.c = aVar;
        this.f1709a = i;
        this.b = answerAppCommentRequest;
    }

    /* renamed from: a */
    public void call(k kVar) {
        kVar.a(this.f1709a, 0, this.b.f1974a, this.b.e, this.b.d, System.currentTimeMillis());
    }
}
