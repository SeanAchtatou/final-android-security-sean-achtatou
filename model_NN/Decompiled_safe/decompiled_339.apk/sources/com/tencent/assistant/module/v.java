package com.tencent.assistant.module;

import com.tencent.assistant.download.SimpleDownloadInfo;

/* compiled from: ProGuard */
/* synthetic */ class v {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f1904a = new int[SimpleDownloadInfo.DownloadState.values().length];

    static {
        try {
            f1904a[SimpleDownloadInfo.DownloadState.QUEUING.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f1904a[SimpleDownloadInfo.DownloadState.DOWNLOADING.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f1904a[SimpleDownloadInfo.DownloadState.PAUSED.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f1904a[SimpleDownloadInfo.DownloadState.WAITTING_FOR_WIFI.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f1904a[SimpleDownloadInfo.DownloadState.FAIL.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f1904a[SimpleDownloadInfo.DownloadState.COMPLETE.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f1904a[SimpleDownloadInfo.DownloadState.INIT.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f1904a[SimpleDownloadInfo.DownloadState.SUCC.ordinal()] = 8;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f1904a[SimpleDownloadInfo.DownloadState.INSTALLED.ordinal()] = 9;
        } catch (NoSuchFieldError e9) {
        }
    }
}
