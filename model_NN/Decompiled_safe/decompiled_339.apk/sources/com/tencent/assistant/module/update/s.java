package com.tencent.assistant.module.update;

import android.text.TextUtils;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class s {

    /* renamed from: a  reason: collision with root package name */
    public String f1896a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    public int c = 0;
    public boolean d = false;

    public s() {
    }

    public s(String str, String str2, int i, boolean z) {
        a(str, str2, i, z);
    }

    public void a(String str, String str2, int i, boolean z) {
        if (!TextUtils.isEmpty(str)) {
            this.f1896a = str;
        }
        if (!TextUtils.isEmpty(str2)) {
            this.b = str2;
        }
        this.c = i;
        this.d = z;
    }

    public boolean equals(Object obj) {
        s sVar;
        if (obj == null || (sVar = (s) obj) == null || !this.f1896a.equals(sVar.f1896a)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.f1896a.hashCode();
    }
}
