package com.tencent.assistant.module.timer.job;

import com.tencent.assistant.m;
import com.tencent.assistant.module.dy;
import com.tencent.assistant.module.timer.SimpleBaseScheduleJob;

/* compiled from: ProGuard */
public class GetUnionUpdateInfoTimerJob extends SimpleBaseScheduleJob {

    /* renamed from: a  reason: collision with root package name */
    private static GetUnionUpdateInfoTimerJob f1870a;

    public static synchronized GetUnionUpdateInfoTimerJob e() {
        GetUnionUpdateInfoTimerJob getUnionUpdateInfoTimerJob;
        synchronized (GetUnionUpdateInfoTimerJob.class) {
            if (f1870a == null) {
                f1870a = new GetUnionUpdateInfoTimerJob();
            }
            getUnionUpdateInfoTimerJob = f1870a;
        }
        return getUnionUpdateInfoTimerJob;
    }

    public void d_() {
        dy.a().c();
    }

    public int h() {
        return m.a().a("union_update_interval", 10800);
    }
}
