package com.tencent.assistant.module;

import com.tencent.assistant.protocol.jce.CardItemWrapper;
import com.tencent.assistant.protocol.jce.SmartCardWrapper;
import com.tencent.assistant.utils.TemporaryThreadManager;
import java.util.ArrayList;

/* compiled from: ProGuard */
class co implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ce f1756a;
    private int b = 0;
    /* access modifiers changed from: private */
    public cn c;
    private long d;
    private ArrayList<CardItemWrapper> e;
    private ArrayList<SmartCardWrapper> f;
    /* access modifiers changed from: private */
    public int g = -1;

    public co(ce ceVar) {
        this.f1756a = ceVar;
        this.c = new cn(ceVar);
    }

    public void a(cn cnVar) {
        if (cnVar != null && cnVar.f1755a.length != 0) {
            b();
            this.b = 1;
            this.c.a(cnVar);
            this.g = this.f1756a.getUniqueId();
            TemporaryThreadManager.get().start(new cp(this));
        }
    }

    public int a() {
        return this.g;
    }

    public void b() {
        this.g = -1;
        this.c.f1755a = null;
        this.c.b = 0;
        this.c.c = false;
        this.e = null;
        this.b = 0;
    }

    public boolean c() {
        return this.b == 1;
    }

    public cn d() {
        return this.c;
    }

    public ArrayList<CardItemWrapper> e() {
        return this.e;
    }

    public ArrayList<SmartCardWrapper> f() {
        return this.f;
    }

    public boolean g() {
        return this.c.c;
    }

    public long h() {
        return this.d;
    }

    public void a(long j, ArrayList<CardItemWrapper> arrayList, cn cnVar, ArrayList<SmartCardWrapper> arrayList2) {
        this.d = j;
        this.e = arrayList;
        this.c.a(cnVar);
        this.f = arrayList2;
        this.b = 2;
    }

    public void i() {
        this.b = 2;
    }

    /* renamed from: j */
    public co clone() {
        try {
            co coVar = (co) super.clone();
            if (this.e == null) {
                return coVar;
            }
            coVar.e = (ArrayList) this.e.clone();
            return coVar;
        } catch (CloneNotSupportedException e2) {
            return this;
        }
    }
}
