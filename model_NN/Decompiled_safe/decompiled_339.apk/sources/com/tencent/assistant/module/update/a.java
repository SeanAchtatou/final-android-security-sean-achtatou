package com.tencent.assistant.module.update;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.appbakcup.m;
import com.tencent.assistant.module.aw;
import com.tencent.assistant.protocol.jce.AppInfoForUpdate;
import com.tencent.assistant.protocol.jce.LbsData;
import com.tencent.assistant.protocol.jce.SetBatchAppUploadRequest;
import com.tencent.assistant.protocol.jce.SetBatchAppUploadResponse;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class a extends aw {

    /* renamed from: a  reason: collision with root package name */
    private static a f1877a = null;
    private LbsData b;
    private int c;
    private int d = -1;
    private byte e = 0;
    private boolean f = false;
    private long g = -1;
    private ArrayList<AppInfoForUpdate> h = null;
    private int i = 0;
    private int j = 0;
    private int k = 0;

    private a() {
    }

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (f1877a == null) {
                f1877a = new a();
            }
            aVar = f1877a;
        }
        return aVar;
    }

    public void a(LbsData lbsData, int i2, ArrayList<AppInfoForUpdate> arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            if (!this.f || this.g <= 0 || System.currentTimeMillis() - this.g >= 60000) {
                this.b = lbsData;
                this.c = i2;
                this.h = arrayList;
                this.g = System.currentTimeMillis();
                this.f = true;
                this.k = 0;
                if (this.e >= Byte.MAX_VALUE) {
                    this.e = 0;
                }
                this.e = (byte) (this.e + 1);
                this.i = Math.round((((float) arrayList.size()) * 1.0f) / 50.0f);
                if (this.i <= 0) {
                    this.i = 1;
                } else if (this.i > 4) {
                    this.i = 4;
                }
                this.j = arrayList.size() / this.i;
                a(1);
            }
        }
    }

    private void a(int i2) {
        int i3;
        byte b2 = 1;
        this.g = System.currentTimeMillis();
        if (i2 < 1 || this.h == null || this.h.size() <= 0) {
            b();
            return;
        }
        int size = this.h.size();
        int i4 = this.j * (i2 - 1);
        if (i4 < 0 || i4 >= size) {
            b();
            return;
        }
        if (i2 < this.i && (i3 = this.j * i2) <= size) {
            size = i3;
        }
        if (i4 >= size) {
            b();
            return;
        }
        ArrayList<AppInfoForUpdate> arrayList = new ArrayList<>();
        while (i4 < size) {
            arrayList.add(this.h.get(i4));
            i4++;
        }
        SetBatchAppUploadRequest setBatchAppUploadRequest = new SetBatchAppUploadRequest();
        setBatchAppUploadRequest.f2301a = arrayList;
        setBatchAppUploadRequest.b = (byte) i2;
        setBatchAppUploadRequest.c = (byte) this.i;
        setBatchAppUploadRequest.d = (long) this.e;
        if (!m.a()) {
            b2 = 0;
        }
        setBatchAppUploadRequest.e = b2;
        this.d = send(setBatchAppUploadRequest);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.update.k.a(com.tencent.assistant.protocol.jce.LbsData, int, boolean):void
     arg types: [com.tencent.assistant.protocol.jce.LbsData, int, int]
     candidates:
      com.tencent.assistant.module.update.k.a(com.tencent.assistant.module.update.k, com.tencent.assistant.protocol.jce.LbsData, int):int
      com.tencent.assistant.module.update.k.a(com.tencent.assistant.protocol.jce.LbsData, int, java.util.ArrayList<com.tencent.assistant.protocol.jce.AppInfoForUpdate>):int
      com.tencent.assistant.module.update.k.a(com.tencent.assistant.module.update.AppUpdateConst$RequestLaunchType, com.tencent.assistant.protocol.jce.LbsData, java.util.Map<java.lang.String, java.lang.String>):void
      com.tencent.assistant.module.update.k.a(com.tencent.assistant.protocol.jce.LbsData, int, boolean):void */
    private boolean a(byte b2) {
        if (this.k >= 1) {
            k.b().a(this.b, this.c, false);
            b();
            return false;
        }
        a((int) b2);
        this.k++;
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.update.k.a(com.tencent.assistant.protocol.jce.LbsData, int, boolean):void
     arg types: [com.tencent.assistant.protocol.jce.LbsData, int, int]
     candidates:
      com.tencent.assistant.module.update.k.a(com.tencent.assistant.module.update.k, com.tencent.assistant.protocol.jce.LbsData, int):int
      com.tencent.assistant.module.update.k.a(com.tencent.assistant.protocol.jce.LbsData, int, java.util.ArrayList<com.tencent.assistant.protocol.jce.AppInfoForUpdate>):int
      com.tencent.assistant.module.update.k.a(com.tencent.assistant.module.update.AppUpdateConst$RequestLaunchType, com.tencent.assistant.protocol.jce.LbsData, java.util.Map<java.lang.String, java.lang.String>):void
      com.tencent.assistant.module.update.k.a(com.tencent.assistant.protocol.jce.LbsData, int, boolean):void */
    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        SetBatchAppUploadResponse setBatchAppUploadResponse;
        if (this.d == i2 && jceStruct != null && (jceStruct instanceof SetBatchAppUploadRequest)) {
            SetBatchAppUploadRequest setBatchAppUploadRequest = (SetBatchAppUploadRequest) jceStruct;
            if (jceStruct2 == null || !(jceStruct2 instanceof SetBatchAppUploadResponse)) {
                setBatchAppUploadResponse = null;
            } else {
                setBatchAppUploadResponse = (SetBatchAppUploadResponse) jceStruct2;
            }
            if (setBatchAppUploadRequest == null || setBatchAppUploadResponse == null || setBatchAppUploadRequest.b != setBatchAppUploadResponse.b || setBatchAppUploadRequest.c != setBatchAppUploadResponse.c) {
                a(setBatchAppUploadRequest.b);
            } else if (setBatchAppUploadResponse.f2302a == 0) {
                this.k = 0;
                if (setBatchAppUploadResponse.b >= setBatchAppUploadResponse.c) {
                    k.b().a(this.b, this.c, true);
                    b();
                    return;
                }
                a(setBatchAppUploadResponse.b + 1);
            } else {
                a(setBatchAppUploadRequest.b);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.update.k.a(com.tencent.assistant.protocol.jce.LbsData, int, boolean):void
     arg types: [com.tencent.assistant.protocol.jce.LbsData, int, int]
     candidates:
      com.tencent.assistant.module.update.k.a(com.tencent.assistant.module.update.k, com.tencent.assistant.protocol.jce.LbsData, int):int
      com.tencent.assistant.module.update.k.a(com.tencent.assistant.protocol.jce.LbsData, int, java.util.ArrayList<com.tencent.assistant.protocol.jce.AppInfoForUpdate>):int
      com.tencent.assistant.module.update.k.a(com.tencent.assistant.module.update.AppUpdateConst$RequestLaunchType, com.tencent.assistant.protocol.jce.LbsData, java.util.Map<java.lang.String, java.lang.String>):void
      com.tencent.assistant.module.update.k.a(com.tencent.assistant.protocol.jce.LbsData, int, boolean):void */
    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
        SetBatchAppUploadResponse setBatchAppUploadResponse;
        if (this.d == i2 && jceStruct != null && (jceStruct instanceof SetBatchAppUploadRequest)) {
            SetBatchAppUploadRequest setBatchAppUploadRequest = (SetBatchAppUploadRequest) jceStruct;
            if (jceStruct2 == null || !(jceStruct2 instanceof SetBatchAppUploadResponse)) {
                setBatchAppUploadResponse = null;
            } else {
                setBatchAppUploadResponse = (SetBatchAppUploadResponse) jceStruct2;
            }
            if (setBatchAppUploadRequest == null || setBatchAppUploadResponse == null || setBatchAppUploadRequest.b != setBatchAppUploadResponse.b || setBatchAppUploadRequest.c != setBatchAppUploadResponse.c) {
                a(setBatchAppUploadRequest.b);
            } else if (setBatchAppUploadResponse.f2302a == -1) {
                a(setBatchAppUploadRequest.b);
            } else if (setBatchAppUploadResponse.f2302a == -2) {
                k.b().a(this.b, this.c, false);
                b();
            } else if (setBatchAppUploadResponse.f2302a == -3) {
                k.b().a(this.b, this.c, false);
                b();
            }
        }
    }

    private void b() {
        this.f = false;
        this.g = -1;
        this.k = 0;
    }
}
