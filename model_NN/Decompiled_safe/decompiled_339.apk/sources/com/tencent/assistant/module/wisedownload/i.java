package com.tencent.assistant.module.wisedownload;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.module.b.a;
import com.tencent.assistant.module.b.b;
import com.tencent.assistant.module.timer.job.AutoDownloadTimerJob;
import com.tencent.assistant.module.wisedownload.condition.ThresholdCondition;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.receiver.PhoneStatusReceiver;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class i implements UIEventListener, NetworkMonitor.ConnectivityChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private static i f1913a = null;
    private PhoneStatusReceiver b = null;
    private b c = null;
    private b d = null;
    private b e = null;
    private b f = null;
    private b g = null;
    private b h = null;

    public static synchronized i a() {
        i iVar;
        synchronized (i.class) {
            if (f1913a == null) {
                f1913a = new i();
            }
            iVar = f1913a;
        }
        return iVar;
    }

    public i() {
        q();
    }

    private void q() {
        this.c = new g();
        this.d = new a();
        this.e = new f();
        this.f = new d();
        this.g = new h();
        if (b.d()) {
            this.h = new a();
        }
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_AUTO_UPDATE_SWITCH_VALUE_CHANGE, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_AUTO_NEW_DOWNLOAD_SWITCH_VALUE_CHANGE, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_CLOSE_DIFF_RELATED_FILE_UPLOAD, this);
        c();
    }

    public void b() {
        d();
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_AUTO_UPDATE_SWITCH_VALUE_CHANGE, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_AUTO_NEW_DOWNLOAD_SWITCH_VALUE_CHANGE, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_CLOSE_DIFF_RELATED_FILE_UPLOAD, this);
    }

    public void c() {
        cq.a().a(this);
        if (this.b == null) {
            this.b = new PhoneStatusReceiver();
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.BATTERY_CHANGED");
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        intentFilter.addAction("android.intent.action.USER_PRESENT");
        AstApp.i().registerReceiver(this.b, intentFilter);
        AutoDownloadTimerJob.d().e();
    }

    public void d() {
        if (this.b != null) {
            try {
                AstApp.i().unregisterReceiver(this.b);
            } catch (Exception e2) {
            }
        }
        AutoDownloadTimerJob.d().f();
    }

    public void e() {
        if (this.f != null) {
            this.f.g();
        }
        if (this.g != null) {
            this.g.g();
        }
        if (this.d != null) {
            this.d.g();
        }
        if (this.e != null) {
            this.e.g();
        }
        AutoDownloadTimerJob.d().e();
    }

    public void f() {
        if (this.f != null) {
            this.f.a(ThresholdCondition.CONDITION_TYPE.CONDITION_PRIMARY);
        }
        if (this.g != null) {
            this.g.a(ThresholdCondition.CONDITION_TYPE.CONDITION_PRIMARY);
        }
        if (this.d != null) {
            this.d.a(ThresholdCondition.CONDITION_TYPE.CONDITION_PRIMARY);
        }
        if (this.e != null) {
            this.e.a(ThresholdCondition.CONDITION_TYPE.CONDITION_PRIMARY);
        }
    }

    public void g() {
        a(1);
        a(2);
        a(3);
        a(5);
        a(6);
    }

    public void a(int i) {
        if (i == 1) {
            if (this.c != null) {
                this.c.a(ThresholdCondition.CONDITION_TYPE.CONDITION_SWITCH);
            }
        } else if (i == 5) {
            if (this.f != null) {
                this.f.a(ThresholdCondition.CONDITION_TYPE.CONDITION_SWITCH);
            }
        } else if (i == 6) {
            if (this.g != null) {
                this.g.a(ThresholdCondition.CONDITION_TYPE.CONDITION_SWITCH);
            }
        } else if (i == 2) {
            if (this.d != null) {
                this.d.a(ThresholdCondition.CONDITION_TYPE.CONDITION_SWITCH);
            }
        } else if (i == 3 && this.e != null) {
            this.e.a(ThresholdCondition.CONDITION_TYPE.CONDITION_SWITCH);
        }
    }

    public void a(Intent intent) {
        boolean z = true;
        Bundle bundle = new Bundle();
        if (intent.getIntExtra("status", 1) != 2) {
            z = false;
        }
        bundle.putBoolean("battery_charging_status", z);
        bundle.putInt("battery_level", intent.getIntExtra("level", 0));
        a(ThresholdCondition.CONDITION_TRIGGER_ACTION.BATTERY_CHANGED, bundle);
    }

    public void b(Intent intent) {
        if (p.f(as.w().i())) {
            a(ThresholdCondition.CONDITION_TRIGGER_ACTION.SCREEN_ON);
        } else {
            a(ThresholdCondition.CONDITION_TRIGGER_ACTION.SCREEN_ON, null);
        }
    }

    public void c(Intent intent) {
        a(ThresholdCondition.CONDITION_TRIGGER_ACTION.SCREEN_OFF);
    }

    public void d(Intent intent) {
        a(ThresholdCondition.CONDITION_TRIGGER_ACTION.USER_PRESENT, null);
    }

    public void h() {
        a(ThresholdCondition.CONDITION_TRIGGER_ACTION.TIME_POINT);
    }

    public void i() {
        a(ThresholdCondition.CONDITION_TRIGGER_ACTION.DOWNLOAD_SUCC);
    }

    public void j() {
        a(ThresholdCondition.CONDITION_TRIGGER_ACTION.DOWNLOAD_FAIL);
    }

    public void k() {
        if (!a(ThresholdCondition.CONDITION_TRIGGER_ACTION.CONNECT, null)) {
            a(ThresholdCondition.CONDITION_TRIGGER_ACTION.CONNECT);
        }
    }

    public void l() {
        a(ThresholdCondition.CONDITION_TRIGGER_ACTION.DISCONNECT, null);
    }

    public void m() {
        if (!a(ThresholdCondition.CONDITION_TRIGGER_ACTION.CONNECT_CHANGED, null)) {
            a(ThresholdCondition.CONDITION_TRIGGER_ACTION.CONNECT_CHANGED);
        }
    }

    public void n() {
        if (this.b != null) {
            this.b.a(new Intent("ACTION_ON_TIME_POINT"));
        }
    }

    public void o() {
        if (this.b != null) {
            this.b.a(new Intent("ACTION_ON_DOWNLOAD_SUCC"));
        }
    }

    public void p() {
        if (this.b != null) {
            this.b.a(new Intent("ACTION_ON_DOWNLOAD_FAIL"));
        }
    }

    public void onConnected(APN apn) {
        if (this.b != null) {
            this.b.a(new Intent("ACTION_ON_CONNECTED"));
        }
    }

    public void onDisconnected(APN apn) {
        if (this.b != null) {
            this.b.a(new Intent("ACTION_ON_DISCONNECTED"));
        }
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        if (this.b != null) {
            this.b.a(new Intent("ACTION_ON_CONNECTIVITY_CHANGE"));
        }
    }

    public void handleUIEvent(Message message) {
        if (message.what == 1102) {
            a(2);
        } else if (message.what == 1103) {
            a(3);
        } else if (message.what == 1132) {
            this.h = null;
        }
    }

    public void a(ThresholdCondition.CONDITION_TRIGGER_ACTION condition_trigger_action) {
        if (!j.a().d()) {
            XLog.d("WiseDownload", "checkDownloadAllConditions action = " + condition_trigger_action);
            ThresholdCondition.CONDITION_RESULT_CODE condition_result_code = null;
            if (this.c == null || (condition_result_code = this.c.f()) != ThresholdCondition.CONDITION_RESULT_CODE.OK) {
                if (this.c != null) {
                    XLog.d("WiseDownload", "checkDownloadAllConditions groupSelf fail retCode = " + condition_result_code);
                    m.a(1, condition_result_code, condition_trigger_action.ordinal());
                }
                if (this.f == null || (condition_result_code = this.f.f()) != ThresholdCondition.CONDITION_RESULT_CODE.OK) {
                    if (this.f != null) {
                        XLog.d("WiseDownload", "checkDownloadAllConditions groupBooking fail retCode = " + condition_result_code);
                        m.a(5, condition_result_code, condition_trigger_action.ordinal());
                    }
                    if (this.g == null || (condition_result_code = this.g.f()) != ThresholdCondition.CONDITION_RESULT_CODE.OK) {
                        if (this.g != null) {
                            XLog.d("WiseDownload", "checkDownloadAllConditions groupSubscription fail retCode = " + condition_result_code);
                            m.a(6, condition_result_code, condition_trigger_action.ordinal());
                        }
                        if (this.d == null || (condition_result_code = this.d.f()) != ThresholdCondition.CONDITION_RESULT_CODE.OK) {
                            if (this.d != null) {
                                XLog.d("WiseDownload", "checkDownloadAllConditions groupUpdate fail retCode = " + condition_result_code);
                                m.a(2, condition_result_code, condition_trigger_action.ordinal());
                            }
                            if (this.e == null || (condition_result_code = this.e.f()) != ThresholdCondition.CONDITION_RESULT_CODE.OK) {
                                if (this.e != null) {
                                    XLog.d("WiseDownload", "checkDownloadAllConditions groupNewApp fail retCode = " + condition_result_code);
                                    m.a(3, condition_result_code, condition_trigger_action.ordinal());
                                }
                                if (this.h != null && this.h.f() == ThresholdCondition.CONDITION_RESULT_CODE.OK) {
                                    a(EventDispatcherEnum.UI_EVENT_DIFF_MERGE_FAIL_RELATED_FILE_UPLOAD, -1, condition_trigger_action.ordinal());
                                    return;
                                }
                                return;
                            }
                            m.a(3, condition_result_code, condition_trigger_action.ordinal());
                            a(EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_START, 3, condition_trigger_action.ordinal());
                            return;
                        }
                        m.a(2, condition_result_code, condition_trigger_action.ordinal());
                        a(EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_START, 2, condition_trigger_action.ordinal());
                        return;
                    }
                    m.a(6, condition_result_code, condition_trigger_action.ordinal());
                    a(EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_START, 6, condition_trigger_action.ordinal());
                    return;
                }
                m.a(5, condition_result_code, condition_trigger_action.ordinal());
                a(EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_START, 5, condition_trigger_action.ordinal());
                return;
            }
            m.a(1, condition_result_code, condition_trigger_action.ordinal());
            a(EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_START, 1, condition_trigger_action.ordinal());
        }
    }

    public boolean a(ThresholdCondition.CONDITION_TRIGGER_ACTION condition_trigger_action, Bundle bundle) {
        boolean z;
        XLog.d("WiseDownload", "checkPauseCondition action = " + condition_trigger_action);
        if (this.c == null) {
            z = true;
        } else if (this.c.a(condition_trigger_action, bundle)) {
            if (j.c() == 1) {
                m.a(1, ThresholdCondition.CONDITION_RESULT_CODE.FAIL_NEED_PAUSE, condition_trigger_action.ordinal());
            }
            a(EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_PAUSE, 1, condition_trigger_action.ordinal());
            z = true;
        } else {
            z = false;
        }
        if (this.f != null) {
            if (this.f.a(condition_trigger_action, bundle)) {
                if (j.c() == 1) {
                    m.a(5, ThresholdCondition.CONDITION_RESULT_CODE.FAIL_NEED_PAUSE, condition_trigger_action.ordinal());
                }
                a(EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_PAUSE, 5, condition_trigger_action.ordinal());
            } else {
                z = false;
            }
        }
        if (this.g != null) {
            if (this.g.a(condition_trigger_action, bundle)) {
                if (j.c() == 6) {
                    m.a(6, ThresholdCondition.CONDITION_RESULT_CODE.FAIL_NEED_PAUSE, condition_trigger_action.ordinal());
                }
                a(EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_PAUSE, 6, condition_trigger_action.ordinal());
            } else {
                z = false;
            }
        }
        if (this.d != null) {
            if (this.d.a(condition_trigger_action, bundle)) {
                if (j.c() == 2) {
                    m.a(2, ThresholdCondition.CONDITION_RESULT_CODE.FAIL_NEED_PAUSE, condition_trigger_action.ordinal());
                }
                a(EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_PAUSE, 2, condition_trigger_action.ordinal());
            } else {
                z = false;
            }
        }
        if (this.e == null) {
            return z;
        }
        if (!this.e.a(condition_trigger_action, bundle)) {
            return false;
        }
        if (j.c() == 3) {
            m.a(3, ThresholdCondition.CONDITION_RESULT_CODE.FAIL_NEED_PAUSE, condition_trigger_action.ordinal());
        }
        a(EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_PAUSE, 3, condition_trigger_action.ordinal());
        return z;
    }

    public void a(int i, int i2, int i3) {
        if (i != 0) {
            j.a();
            if (i == 1128) {
                b.a();
            }
            Message obtainMessage = AstApp.i().j().obtainMessage(i);
            obtainMessage.arg1 = i2;
            obtainMessage.arg2 = i3;
            AstApp.i().j().sendMessage(obtainMessage);
        }
    }

    public void a(o oVar) {
        if (oVar != null) {
            if (this.c != null) {
                this.c.b(oVar);
            }
            if (this.f != null) {
                this.f.b(oVar);
            }
            if (this.g != null) {
                this.g.b(oVar);
            }
            if (this.d != null) {
                this.d.b(oVar);
            }
            if (this.e != null) {
                this.e.b(oVar);
            }
        }
    }
}
