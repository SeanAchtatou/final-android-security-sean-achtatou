package com.tencent.assistant.floatingwindow;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

/* compiled from: ProGuard */
class Solid extends View {

    /* renamed from: a  reason: collision with root package name */
    private Paint f1283a;
    private Paint b;

    public Solid(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public Solid(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.weight = 1.0f;
        setLayoutParams(layoutParams);
    }

    public void a(Paint paint) {
        this.f1283a = paint;
    }

    public void b(Paint paint) {
        this.b = paint;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect((float) getLeft(), 0.0f, (float) getRight(), (float) getBottom(), this.b);
        canvas.drawRect((float) getLeft(), 0.0f, (float) getRight(), (float) getBottom(), this.f1283a);
    }
}
