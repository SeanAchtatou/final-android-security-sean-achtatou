package com.tencent.assistant.usagestats;

import android.text.TextUtils;
import com.tencent.assistant.usagestats.UsagestatsSTManager;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class p implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ o f2604a;

    p(o oVar) {
        this.f2604a = oVar;
    }

    public void run() {
        if (!TextUtils.isEmpty(this.f2604a.m)) {
            XLog.e("usagestats", "getStatOtherAppList execute timeout !!!, " + this.f2604a.m);
            UsagestatsSTManager.a().a("app_usage_r_report_timely", this.f2604a.m + "_block", null, UsagestatsSTManager.ReportType.timely);
        }
    }
}
