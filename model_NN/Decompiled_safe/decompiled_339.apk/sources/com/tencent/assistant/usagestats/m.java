package com.tencent.assistant.usagestats;

import android.os.Parcel;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class m {

    /* renamed from: a  reason: collision with root package name */
    final c<String, n> f2601a;
    int b;
    long c;

    m() {
        this.f2601a = new c<>();
        this.b = 0;
        this.c = 0;
    }

    m(Parcel parcel) {
        this.f2601a = new c<>();
        this.b = parcel.readInt();
        this.c = parcel.readLong();
        XLog.v("usagestats", "Launch count: " + this.b + ", Usage time:" + this.c);
        if (this.b != 0 && this.b <= 1000) {
            int readInt = parcel.readInt();
            XLog.v("usagestats", "Reading launch times: " + readInt);
            if (readInt != 0 && readInt <= 1000) {
                this.f2601a.a(readInt);
                for (int i = 0; i < readInt; i++) {
                    this.f2601a.put(parcel.readString(), new n(parcel));
                }
            }
        }
    }
}
