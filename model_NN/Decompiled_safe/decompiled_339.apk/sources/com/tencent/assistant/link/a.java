package com.tencent.assistant.link;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.ApkMgrActivity;
import com.tencent.assistant.activity.AppBackupActivity;
import com.tencent.assistant.activity.BrowserActivity;
import com.tencent.assistant.activity.DownloadActivity;
import com.tencent.assistant.activity.ExternalCallActivity;
import com.tencent.assistant.activity.GameRankActivity;
import com.tencent.assistant.activity.GroupListActivity;
import com.tencent.assistant.activity.HelperFeedbackActivity;
import com.tencent.assistant.activity.InstalledAppManagerActivity;
import com.tencent.assistant.activity.PhotoBackupNewActivity;
import com.tencent.assistant.activity.PluginDetailActivity;
import com.tencent.assistant.activity.SettingActivity;
import com.tencent.assistant.activity.SpaceCleanActivity;
import com.tencent.assistant.activity.SpecailTopicActivity;
import com.tencent.assistant.activity.SpecailTopicDetailActivity;
import com.tencent.assistant.activity.TencentAppListActivity;
import com.tencent.assistant.activity.UpdateListActivity;
import com.tencent.assistant.activity.VideoActivityV2;
import com.tencent.assistant.adapter.AppCategoryListAdapter;
import com.tencent.assistant.guessfavor.GuessFavorActivity;
import com.tencent.assistant.login.LoginRichDialog;
import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistant.model.PluginStartEntry;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.d;
import com.tencent.assistant.plugin.proxy.PluginProxyActivity;
import com.tencent.assistant.sdk.h;
import com.tencent.assistant.securescan.StartScanActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.tagpage.TagPageActivity;
import com.tencent.assistant.usercenter.UserCenterActivityV2;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.activity.AppCategoryActivity;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.activity.AppRankActivity;
import com.tencent.assistantv2.activity.AssistantTabActivity;
import com.tencent.assistantv2.activity.CategoryDetailActivity;
import com.tencent.assistantv2.activity.GameCategoryActivity;
import com.tencent.assistantv2.activity.GameCategoryDetailActivity;
import com.tencent.assistantv2.activity.GuideActivity;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.activity.SearchActivity;
import com.tencent.assistantv2.manager.MainTabType;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static final String f1394a = ("&" + com.tencent.assistant.b.a.D + "=1");
    public static final String b = ("?" + com.tencent.assistant.b.a.D + "=1");
    public static final String c = ("&" + com.tencent.assistant.b.a.E + "=1");
    public static final String d = ("?" + com.tencent.assistant.b.a.E + "=1");

    /* JADX WARNING: Removed duplicated region for block: B:46:0x00bc A[Catch:{ Exception -> 0x047a }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00f3 A[Catch:{ Exception -> 0x047a }] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x013f A[Catch:{ Exception -> 0x047a }] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0141 A[Catch:{ Exception -> 0x047a }] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0147 A[Catch:{ Exception -> 0x047a }] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0169 A[Catch:{ Exception -> 0x047a }] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x017f A[Catch:{ Exception -> 0x047a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static boolean a(android.content.Context r13, android.net.Uri r14, android.os.Bundle r15) {
        /*
            r1 = 0
            r7 = 0
            r6 = 1
            java.lang.String r0 = "ext"
            java.lang.String r0 = r14.getQueryParameter(r0)     // Catch:{ Exception -> 0x047a }
            boolean r2 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x047a }
            if (r2 != 0) goto L_0x0014
            com.tencent.assistant.link.b.a(r13, r0)     // Catch:{ Exception -> 0x047a }
            r0 = r6
        L_0x0013:
            return r0
        L_0x0014:
            java.lang.String r10 = r14.getHost()     // Catch:{ Exception -> 0x047a }
            java.lang.String r0 = "encrypt"
            boolean r0 = r0.equals(r10)     // Catch:{ Exception -> 0x047a }
            if (r0 != 0) goto L_0x0024
            android.os.Bundle r15 = com.tencent.assistantv2.st.a.a.a(r14, r15)     // Catch:{ Exception -> 0x047a }
        L_0x0024:
            java.lang.String r0 = "appdetails"
            boolean r0 = r0.equals(r10)     // Catch:{ Exception -> 0x047a }
            if (r0 != 0) goto L_0x0034
            java.lang.String r0 = "download"
            boolean r0 = r0.equals(r10)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x00eb
        L_0x0034:
            java.lang.String r0 = com.tencent.assistant.b.a.c     // Catch:{ Exception -> 0x012d }
            java.lang.String r0 = r14.getQueryParameter(r0)     // Catch:{ Exception -> 0x012d }
            if (r0 == 0) goto L_0x00f9
            java.lang.String r0 = com.tencent.assistant.b.a.c     // Catch:{ Exception -> 0x012d }
            java.lang.String r3 = r14.getQueryParameter(r0)     // Catch:{ Exception -> 0x012d }
        L_0x0042:
            java.lang.String r0 = com.tencent.assistant.b.a.t     // Catch:{ Exception -> 0x0481 }
            java.lang.String r0 = r14.getQueryParameter(r0)     // Catch:{ Exception -> 0x0481 }
            if (r0 == 0) goto L_0x0101
            java.lang.String r0 = com.tencent.assistant.b.a.t     // Catch:{ Exception -> 0x0481 }
            java.lang.String r0 = r14.getQueryParameter(r0)     // Catch:{ Exception -> 0x0481 }
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ Exception -> 0x0481 }
            boolean r5 = r0.booleanValue()     // Catch:{ Exception -> 0x0481 }
        L_0x0058:
            java.lang.String r0 = com.tencent.assistant.b.a.D     // Catch:{ Exception -> 0x0489 }
            java.lang.String r0 = r14.getQueryParameter(r0)     // Catch:{ Exception -> 0x0489 }
            if (r0 == 0) goto L_0x0109
            java.lang.String r0 = com.tencent.assistant.b.a.D     // Catch:{ Exception -> 0x0489 }
            java.lang.String r4 = r14.getQueryParameter(r0)     // Catch:{ Exception -> 0x0489 }
        L_0x0066:
            java.lang.String r0 = com.tencent.assistant.b.a.g     // Catch:{ Exception -> 0x0490 }
            java.lang.String r0 = r14.getQueryParameter(r0)     // Catch:{ Exception -> 0x0490 }
            if (r0 == 0) goto L_0x0111
            java.lang.String r0 = com.tencent.assistant.b.a.g     // Catch:{ Exception -> 0x0490 }
            java.lang.String r0 = r14.getQueryParameter(r0)     // Catch:{ Exception -> 0x0490 }
            java.lang.Byte r0 = java.lang.Byte.valueOf(r0)     // Catch:{ Exception -> 0x0490 }
            byte r9 = r0.byteValue()     // Catch:{ Exception -> 0x0490 }
        L_0x007c:
            java.lang.String r0 = com.tencent.assistant.b.a.s     // Catch:{ Exception -> 0x0496 }
            java.lang.String r0 = r14.getQueryParameter(r0)     // Catch:{ Exception -> 0x0496 }
            if (r0 == 0) goto L_0x0114
            java.lang.String r0 = com.tencent.assistant.b.a.s     // Catch:{ Exception -> 0x0496 }
            java.lang.String r0 = r14.getQueryParameter(r0)     // Catch:{ Exception -> 0x0496 }
            java.lang.Byte r0 = java.lang.Byte.valueOf(r0)     // Catch:{ Exception -> 0x0496 }
            byte r8 = r0.byteValue()     // Catch:{ Exception -> 0x0496 }
        L_0x0092:
            java.lang.String r0 = com.tencent.assistant.b.a.u     // Catch:{ Exception -> 0x049b }
            java.lang.String r2 = r14.getQueryParameter(r0)     // Catch:{ Exception -> 0x049b }
            java.lang.String r0 = com.tencent.assistant.b.a.n     // Catch:{ Exception -> 0x049f }
            java.lang.String r0 = r14.getQueryParameter(r0)     // Catch:{ Exception -> 0x049f }
            if (r0 == 0) goto L_0x0117
            java.lang.String r0 = com.tencent.assistant.b.a.n     // Catch:{ Exception -> 0x049f }
            java.lang.String r1 = r14.getQueryParameter(r0)     // Catch:{ Exception -> 0x049f }
        L_0x00a6:
            java.lang.String r0 = com.tencent.assistant.b.a.o     // Catch:{ Exception -> 0x04a5 }
            int r0 = r15.getInt(r0)     // Catch:{ Exception -> 0x04a5 }
            if (r0 <= 0) goto L_0x011e
            java.lang.String r0 = com.tencent.assistant.b.a.o     // Catch:{ Exception -> 0x04a5 }
            int r0 = r15.getInt(r0)     // Catch:{ Exception -> 0x04a5 }
        L_0x00b4:
            java.lang.String r11 = "com.tencent.mm"
            boolean r11 = r11.equalsIgnoreCase(r1)     // Catch:{ Exception -> 0x047a }
            if (r11 == 0) goto L_0x013f
            java.lang.String r11 = "com.tencent.mm"
            boolean r1 = r11.equalsIgnoreCase(r1)     // Catch:{ Exception -> 0x047a }
            if (r1 == 0) goto L_0x013d
            r1 = 543(0x21f, float:7.61E-43)
            if (r1 >= r0) goto L_0x013d
            r0 = r6
        L_0x00c9:
            boolean r1 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x047a }
            if (r1 != 0) goto L_0x00eb
            if (r5 == 0) goto L_0x00eb
            boolean r1 = android.text.TextUtils.isEmpty(r4)     // Catch:{ Exception -> 0x047a }
            if (r1 == 0) goto L_0x00eb
            if (r9 > 0) goto L_0x00eb
            if (r8 > 0) goto L_0x00eb
            if (r0 == 0) goto L_0x00eb
            com.tencent.assistant.m r0 = com.tencent.assistant.m.a()     // Catch:{ Exception -> 0x047a }
            java.lang.String r1 = "key_external_call_yyb_flag"
            r3 = 0
            int r0 = r0.a(r1, r3)     // Catch:{ Exception -> 0x047a }
            switch(r0) {
                case 1: goto L_0x0169;
                case 2: goto L_0x0147;
                case 3: goto L_0x0141;
                default: goto L_0x00eb;
            }     // Catch:{ Exception -> 0x047a }
        L_0x00eb:
            java.lang.String r0 = "app"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x017f
            e(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x00f9:
            java.lang.String r0 = com.tencent.assistant.b.a.c     // Catch:{ Exception -> 0x012d }
            java.lang.String r3 = r15.getString(r0)     // Catch:{ Exception -> 0x012d }
            goto L_0x0042
        L_0x0101:
            java.lang.String r0 = com.tencent.assistant.b.a.t     // Catch:{ Exception -> 0x0481 }
            boolean r5 = r15.getBoolean(r0)     // Catch:{ Exception -> 0x0481 }
            goto L_0x0058
        L_0x0109:
            java.lang.String r0 = com.tencent.assistant.b.a.D     // Catch:{ Exception -> 0x0489 }
            java.lang.String r4 = r15.getString(r0)     // Catch:{ Exception -> 0x0489 }
            goto L_0x0066
        L_0x0111:
            r9 = r7
            goto L_0x007c
        L_0x0114:
            r8 = r7
            goto L_0x0092
        L_0x0117:
            java.lang.String r0 = com.tencent.assistant.b.a.n     // Catch:{ Exception -> 0x049f }
            java.lang.String r1 = r15.getString(r0)     // Catch:{ Exception -> 0x049f }
            goto L_0x00a6
        L_0x011e:
            java.lang.String r0 = com.tencent.assistant.b.a.o     // Catch:{ Exception -> 0x04a5 }
            java.lang.String r0 = r14.getQueryParameter(r0)     // Catch:{ Exception -> 0x04a5 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x04a5 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x04a5 }
            goto L_0x00b4
        L_0x012d:
            r0 = move-exception
            r2 = r1
            r3 = r1
            r4 = r1
            r5 = r7
            r8 = r7
            r9 = r7
        L_0x0134:
            r0.printStackTrace()     // Catch:{ Exception -> 0x047a }
            r0 = r7
            r12 = r2
            r2 = r1
            r1 = r12
            goto L_0x00b4
        L_0x013d:
            r0 = r7
            goto L_0x00c9
        L_0x013f:
            r0 = r6
            goto L_0x00c9
        L_0x0141:
            O(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x0147:
            boolean r0 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Exception -> 0x047a }
            if (r0 != 0) goto L_0x0157
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r2)     // Catch:{ Exception -> 0x047a }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x047a }
            if (r0 != 0) goto L_0x0163
        L_0x0157:
            java.lang.String r0 = r14.getScheme()     // Catch:{ Exception -> 0x047a }
            java.lang.String r1 = "tpmast"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x00eb
        L_0x0163:
            O(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x0169:
            boolean r0 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Exception -> 0x047a }
            if (r0 != 0) goto L_0x00eb
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r2)     // Catch:{ Exception -> 0x047a }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x00eb
            O(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x017f:
            java.lang.String r0 = "found"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 != 0) goto L_0x018f
            java.lang.String r0 = "competitive"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x0197
        L_0x018f:
            com.tencent.assistantv2.manager.MainTabType r0 = com.tencent.assistantv2.manager.MainTabType.DISCOVER     // Catch:{ Exception -> 0x047a }
            a(r13, r14, r0, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x0197:
            java.lang.String r0 = "rank"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x01a7
            com.tencent.assistantv2.manager.MainTabType r0 = com.tencent.assistantv2.manager.MainTabType.HOT     // Catch:{ Exception -> 0x047a }
            a(r13, r14, r0, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x01a7:
            java.lang.String r0 = "video"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x01b7
            com.tencent.assistantv2.manager.MainTabType r0 = com.tencent.assistantv2.manager.MainTabType.VIDEO     // Catch:{ Exception -> 0x047a }
            a(r13, r14, r0, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x01b7:
            java.lang.String r0 = "ebook"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x01c7
            com.tencent.assistantv2.manager.MainTabType r0 = com.tencent.assistantv2.manager.MainTabType.EBOOK     // Catch:{ Exception -> 0x047a }
            a(r13, r14, r0, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x01c7:
            java.lang.String r0 = "game"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x01d5
            f(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x01d5:
            java.lang.String r0 = "assistant"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x01e3
            b(r13, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x01e3:
            java.lang.String r0 = "appcategoryentry"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x01f1
            g(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x01f1:
            java.lang.String r0 = "gamecategoryentry"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x01ff
            h(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x01ff:
            java.lang.String r0 = "appcategorydetail"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x020d
            i(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x020d:
            java.lang.String r0 = "gamecategorydetail"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x021b
            j(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x021b:
            java.lang.String r0 = "appdetails"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x0229
            l(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x0229:
            java.lang.String r0 = "appcategory"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x0237
            k(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x0237:
            java.lang.String r0 = "necessity"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x0245
            m(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x0245:
            java.lang.String r0 = "hot"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x0253
            n(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x0253:
            java.lang.String r0 = "specialtopic"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x0261
            o(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x0261:
            java.lang.String r0 = "appmanagement"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x026f
            c(r13, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x026f:
            java.lang.String r0 = "search"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x027d
            p(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x027d:
            java.lang.String r0 = "transmit"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x028b
            d(r13, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x028b:
            java.lang.String r0 = "update"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x0299
            s(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x0299:
            java.lang.String r0 = "download"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x02a7
            t(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x02a7:
            java.lang.String r0 = "optimize"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x02b5
            e(r13, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x02b5:
            java.lang.String r0 = "publisher"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x02c3
            w(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x02c3:
            java.lang.String r0 = "webview"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x02d1
            r(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x02d1:
            java.lang.String r0 = "devsetting"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x02df
            h(r13, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x02df:
            java.lang.String r0 = "wifisetting"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x02ed
            g(r13, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x02ed:
            java.lang.String r0 = "topic"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x02fb
            x(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x02fb:
            java.lang.String r0 = "feedback"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x0309
            i(r13, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x0309:
            java.lang.String r0 = "encrypt"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x0317
            A(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x0317:
            java.lang.String r0 = "setting"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x0325
            f(r13, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x0325:
            java.lang.String r0 = "selfupdatecheck"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x0333
            a(r13)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x0333:
            java.lang.String r0 = "updatedownload"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x0341
            u(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x0341:
            java.lang.String r0 = "topiclist"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x034f
            y(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x034f:
            java.lang.String r0 = "apkmanagement"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x035d
            z(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x035d:
            java.lang.String r0 = "mobilemanage"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x037e
            com.tencent.assistantv2.st.page.STInfoV2 r0 = new com.tencent.assistantv2.st.page.STInfoV2     // Catch:{ Exception -> 0x047a }
            r1 = 204006(0x31ce6, float:2.85873E-40)
            java.lang.String r2 = "03_001"
            r3 = 2000(0x7d0, float:2.803E-42)
            java.lang.String r4 = "-1"
            r5 = 100
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x047a }
            com.tencent.assistantv2.st.k.a(r0)     // Catch:{ Exception -> 0x047a }
            b(r13, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x037e:
            java.lang.String r0 = "mobilemanagefromdock"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x038c
            b(r13, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x038c:
            java.lang.String r0 = "plugindetail"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x039a
            B(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x039a:
            java.lang.String r0 = "usercenter"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x03a8
            C(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x03a8:
            java.lang.String r0 = "guessfavor"
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x03b6
            L(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x03b6:
            java.lang.String r0 = "loginrichdialog"
            boolean r0 = r0.equals(r10)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x03c4
            K(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x03c4:
            java.lang.String r0 = "apprank"
            boolean r0 = r0.equals(r10)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x03d2
            M(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x03d2:
            java.lang.String r0 = "gamerank"
            boolean r0 = r0.equals(r10)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x03e0
            N(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x03e0:
            java.lang.String r0 = "videoplay"
            boolean r0 = r0.equals(r10)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x03ee
            v(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x03ee:
            java.lang.String r0 = "picbackup"
            boolean r0 = r0.equals(r10)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x03fc
            a(r13, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x03fc:
            java.lang.String r0 = "safescan"
            boolean r0 = r0.equals(r10)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x040a
            J(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x040a:
            java.lang.String r0 = "appbackup"
            boolean r0 = r0.equals(r10)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x0418
            E(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x0418:
            java.lang.String r0 = "mobileaccel"
            boolean r0 = r0.equals(r10)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x0426
            q(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x0426:
            java.lang.String r0 = "spaceclean"
            boolean r0 = r0.equals(r10)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x0434
            F(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x0434:
            java.lang.String r0 = "login"
            boolean r0 = r0.equals(r10)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x0442
            G(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x0442:
            java.lang.String r0 = "guide"
            boolean r0 = r0.equals(r10)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x0450
            H(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x0450:
            java.lang.String r0 = "tag_detail"
            boolean r0 = r0.equals(r10)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x045e
            I(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x045e:
            java.lang.String r0 = "tagpage"
            boolean r0 = r0.equals(r10)     // Catch:{ Exception -> 0x047a }
            if (r0 == 0) goto L_0x046c
            D(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            r0 = r6
            goto L_0x0013
        L_0x046c:
            boolean r0 = com.tencent.assistant.link.b.b(r13, r14, r15)     // Catch:{ Exception -> 0x047a }
            if (r0 != 0) goto L_0x0477
            com.tencent.assistantv2.manager.MainTabType r0 = com.tencent.assistantv2.manager.MainTabType.DISCOVER     // Catch:{ Exception -> 0x047a }
            a(r13, r14, r0, r15)     // Catch:{ Exception -> 0x047a }
        L_0x0477:
            r0 = r6
            goto L_0x0013
        L_0x047a:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r7
            goto L_0x0013
        L_0x0481:
            r0 = move-exception
            r2 = r1
            r4 = r1
            r5 = r7
            r8 = r7
            r9 = r7
            goto L_0x0134
        L_0x0489:
            r0 = move-exception
            r2 = r1
            r4 = r1
            r8 = r7
            r9 = r7
            goto L_0x0134
        L_0x0490:
            r0 = move-exception
            r2 = r1
            r8 = r7
            r9 = r7
            goto L_0x0134
        L_0x0496:
            r0 = move-exception
            r2 = r1
            r8 = r7
            goto L_0x0134
        L_0x049b:
            r0 = move-exception
            r2 = r1
            goto L_0x0134
        L_0x049f:
            r0 = move-exception
            r12 = r2
            r2 = r1
            r1 = r12
            goto L_0x0134
        L_0x04a5:
            r0 = move-exception
            r12 = r2
            r2 = r1
            r1 = r12
            goto L_0x0134
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.link.a.a(android.content.Context, android.net.Uri, android.os.Bundle):boolean");
    }

    protected static Uri a(String str, String str2, HashMap<String, String> hashMap) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(str);
        stringBuffer.append("://");
        stringBuffer.append(str2);
        if (hashMap != null && hashMap.size() > 0) {
            stringBuffer.append("?");
            for (Map.Entry next : hashMap.entrySet()) {
                stringBuffer.append((String) next.getKey());
                stringBuffer.append("=");
                stringBuffer.append(URLEncoder.encode((String) next.getValue()));
                stringBuffer.append("&");
            }
        }
        if (stringBuffer.toString().endsWith("&")) {
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        }
        return Uri.parse(stringBuffer.toString());
    }

    protected static boolean b(Context context, Uri uri, Bundle bundle) {
        HashMap hashMap = new HashMap();
        hashMap.put(SocialConstants.PARAM_URL, uri.toString());
        return r(context, a("tmast", "webview", hashMap), bundle);
    }

    protected static boolean c(Context context, Uri uri, Bundle bundle) {
        try {
            String host = uri.getHost();
            if (host.equals("details")) {
                String queryParameter = uri.getQueryParameter("id");
                if (TextUtils.isEmpty(queryParameter)) {
                    return false;
                }
                HashMap hashMap = new HashMap();
                hashMap.put("pname", queryParameter);
                return l(context, a("tmast", "appdetails", hashMap), bundle);
            } else if (!host.equals("search")) {
                return false;
            } else {
                String queryParameter2 = uri.getQueryParameter("q");
                if (TextUtils.isEmpty(queryParameter2)) {
                    return false;
                }
                if (queryParameter2.startsWith("pname:")) {
                    String substring = queryParameter2.substring(queryParameter2.indexOf("pname:") + 6);
                    HashMap hashMap2 = new HashMap();
                    hashMap2.put("pname", substring);
                    return l(context, a("tmast", "appdetails", hashMap2), bundle);
                } else if (queryParameter2.startsWith("pub:")) {
                    String substring2 = queryParameter2.substring(queryParameter2.indexOf("pub:") + 4);
                    HashMap hashMap3 = new HashMap();
                    hashMap3.put("cpname", substring2);
                    return w(context, a("tmast", "publisher", hashMap3), bundle);
                } else {
                    HashMap hashMap4 = new HashMap();
                    hashMap4.put("key", queryParameter2);
                    return p(context, a("tmast", "publisher", hashMap4), bundle);
                }
            }
        } catch (Exception e) {
            return false;
        }
    }

    protected static boolean d(Context context, Uri uri, Bundle bundle) {
        boolean a2 = a(context, uri, bundle);
        if (a2 || !"updatedownload".equals(uri.getHost())) {
            return a2;
        }
        u(context, uri, bundle);
        return true;
    }

    private static void a(Context context, MainTabType mainTabType, Bundle bundle) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setAction("android.intent.action.VIEW");
        intent.addFlags(67108864);
        intent.putExtra("com.tencent.assistantv2.TAB_TYPE", mainTabType.ordinal());
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void e(Context context, Uri uri, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        a(context, MainTabType.APP, bundle);
    }

    private static void a(Context context, Uri uri, MainTabType mainTabType, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        try {
            String queryParameter = uri.getQueryParameter("column");
            String queryParameter2 = uri.getQueryParameter("groupid");
            if (!TextUtils.isEmpty(queryParameter)) {
                bundle.putInt("colnum", Integer.valueOf(queryParameter).intValue());
            }
            if (!TextUtils.isEmpty(queryParameter2)) {
                bundle.putInt("groupid", Integer.valueOf(queryParameter2).intValue());
            }
            String queryParameter3 = uri.getQueryParameter("showAppTreasureboxEntry");
            if (!TextUtils.isEmpty(queryParameter3)) {
                bundle.putInt("param_competitive_tab_show_treasure_box_entry", ct.d(queryParameter3));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        a(context, mainTabType, bundle);
    }

    private static void f(Context context, Uri uri, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        a(context, MainTabType.GAME, bundle);
    }

    private static void b(Context context, Bundle bundle) {
        Intent intent = new Intent(context, AssistantTabActivity.class);
        intent.setAction("android.intent.action.VIEW");
        intent.addFlags(67108864);
        intent.putExtras(bundle);
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void g(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, AppCategoryActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void h(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, GameCategoryActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void i(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, CategoryDetailActivity.class);
        try {
            intent.putExtra("com.tencent.assistant.CATATORY_ID", Long.valueOf(uri.getQueryParameter("category_detail_category_id")).longValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            intent.putExtra("com.tencent.assistant.TAG_ID", Long.valueOf(uri.getQueryParameter("category_detail_tag_id")).longValue());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        intent.putExtra("activityTitleName", uri.getQueryParameter("category_detail_category_name"));
        intent.putExtra("com.tencent.assistant.CATATORY_TYPE", AppCategoryListAdapter.CategoryType.CATEGORYTYPESOFTWARE.ordinal());
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void j(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, GameCategoryDetailActivity.class);
        try {
            intent.putExtra("com.tencent.assistant.CATATORY_ID", Long.valueOf(uri.getQueryParameter("category_detail_category_id")).longValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            intent.putExtra("com.tencent.assistant.TAG_ID", Long.valueOf(uri.getQueryParameter("category_detail_tag_id")).longValue());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        intent.putExtra("activityTitleName", uri.getQueryParameter("category_detail_category_name"));
        intent.putExtra("com.tencent.assistant.CATATORY_TYPE", AppCategoryListAdapter.CategoryType.CATEGORYTYPEGAME.ordinal());
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void k(Context context, Uri uri, Bundle bundle) {
        long longValue = Long.valueOf(uri.getQueryParameter("categoryid")).longValue();
        String queryParameter = uri.getQueryParameter("categoryname");
        Intent intent = null;
        if (-10 == longValue) {
            intent = new Intent(context, TencentAppListActivity.class);
        }
        if (intent != null) {
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            intent.putExtra("com.tencent.assistant.CATATORY_ID", longValue);
            intent.putExtra("activityTitleName", queryParameter);
            intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
            if (!(context instanceof Activity)) {
                intent.setFlags(268435456);
            }
            context.startActivity(intent);
        }
    }

    @SuppressLint({"DefaultLocale"})
    private static boolean l(Context context, Uri uri, Bundle bundle) {
        if (context == null) {
            context = AstApp.i().getApplicationContext();
        }
        Intent intent = new Intent(context, AppDetailActivityV5.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        String queryParameter = uri.getQueryParameter(com.tencent.assistant.b.a.f845a);
        String queryParameter2 = uri.getQueryParameter(com.tencent.assistant.b.a.b);
        String queryParameter3 = uri.getQueryParameter(com.tencent.assistant.b.a.c);
        String queryParameter4 = uri.getQueryParameter(com.tencent.assistant.b.a.d);
        String queryParameter5 = uri.getQueryParameter(com.tencent.assistant.b.a.h);
        String queryParameter6 = uri.getQueryParameter(com.tencent.assistant.b.a.C);
        String queryParameter7 = uri.getQueryParameter(com.tencent.assistant.b.a.e);
        String queryParameter8 = uri.getQueryParameter(com.tencent.assistant.b.a.f);
        String queryParameter9 = uri.getQueryParameter(com.tencent.assistant.b.a.D);
        String queryParameter10 = uri.getQueryParameter(com.tencent.assistant.b.a.s);
        String queryParameter11 = uri.getQueryParameter(com.tencent.assistant.b.a.g);
        String queryParameter12 = uri.getQueryParameter(com.tencent.assistant.b.a.x);
        String queryParameter13 = uri.getQueryParameter(com.tencent.assistant.b.a.i);
        String queryParameter14 = uri.getQueryParameter(com.tencent.assistant.b.a.l);
        String queryParameter15 = uri.getQueryParameter(com.tencent.assistant.b.a.k);
        String queryParameter16 = uri.getQueryParameter(com.tencent.assistant.b.a.I);
        String queryParameter17 = uri.getQueryParameter(com.tencent.assistant.b.a.J);
        String queryParameter18 = uri.getQueryParameter(com.tencent.assistant.b.a.U);
        String queryParameter19 = uri.getQueryParameter(com.tencent.assistant.b.a.V);
        String queryParameter20 = uri.getQueryParameter(com.tencent.assistant.b.a.W);
        if (!TextUtils.isEmpty(queryParameter)) {
            intent.putExtra("com.tencent.assistant.APP_ID", Long.valueOf(queryParameter));
        }
        if (!TextUtils.isEmpty(queryParameter2)) {
            intent.putExtra("com.tencent.assistant.APK_ID", queryParameter2);
        }
        if (!TextUtils.isEmpty(queryParameter3)) {
            intent.putExtra("com.tencent.assistant.PACKAGE_NAME", queryParameter3);
        }
        if (!TextUtils.isEmpty(queryParameter4)) {
            intent.putExtra("activityTitleName", queryParameter4);
        }
        if (!TextUtils.isEmpty(queryParameter5)) {
            intent.putExtra(com.tencent.assistant.b.a.h, queryParameter5);
        }
        if (!TextUtils.isEmpty(queryParameter6)) {
            intent.putExtra(com.tencent.assistant.b.a.C, queryParameter6);
        }
        if (!TextUtils.isEmpty(queryParameter7)) {
            intent.putExtra(com.tencent.assistant.b.a.e, queryParameter7);
        }
        if (!TextUtils.isEmpty(queryParameter8)) {
            intent.putExtra(com.tencent.assistant.b.a.f, queryParameter8);
        }
        if (!TextUtils.isEmpty(queryParameter9)) {
            intent.putExtra(com.tencent.assistant.b.a.D, queryParameter9);
        }
        if (!TextUtils.isEmpty(queryParameter10)) {
            intent.putExtra(com.tencent.assistant.b.a.s, queryParameter10);
        }
        if (!TextUtils.isEmpty(queryParameter11)) {
            intent.putExtra(com.tencent.assistant.b.a.g, queryParameter11);
        }
        if (!TextUtils.isEmpty(queryParameter13)) {
            intent.putExtra(com.tencent.assistant.b.a.i, queryParameter13);
        }
        if (!TextUtils.isEmpty(queryParameter14)) {
            intent.putExtra(com.tencent.assistant.b.a.l, queryParameter14);
        }
        if (!TextUtils.isEmpty(queryParameter15)) {
            intent.putExtra(com.tencent.assistant.b.a.k, queryParameter15);
        }
        if (!TextUtils.isEmpty(queryParameter12)) {
            intent.putExtra(com.tencent.assistant.b.a.x, queryParameter12);
        }
        if (!TextUtils.isEmpty(queryParameter16)) {
            XLog.d("channelid", "onAppDetail sdkId is: " + queryParameter16);
            intent.putExtra(com.tencent.assistant.b.a.I, queryParameter16);
        }
        if (!TextUtils.isEmpty(queryParameter17)) {
            intent.putExtra(com.tencent.assistant.b.a.J, queryParameter17);
        }
        if (!TextUtils.isEmpty(queryParameter18)) {
            intent.putExtra(com.tencent.assistant.b.a.U, queryParameter18);
        }
        if (!TextUtils.isEmpty(queryParameter19)) {
            intent.putExtra(com.tencent.assistant.b.a.V, queryParameter19);
        }
        if (!TextUtils.isEmpty(queryParameter20)) {
            intent.putExtra(com.tencent.assistant.b.a.W, queryParameter20);
        }
        if ("tpmast".equals(uri.getScheme())) {
            String queryParameter21 = uri.getQueryParameter(com.tencent.assistant.b.a.q);
            String queryParameter22 = uri.getQueryParameter(com.tencent.assistant.b.a.r);
            String queryParameter23 = uri.getQueryParameter(com.tencent.assistant.b.a.z);
            String queryParameter24 = uri.getQueryParameter(com.tencent.assistant.b.a.A);
            String queryParameter25 = uri.getQueryParameter(com.tencent.assistant.b.a.B);
            String uri2 = uri.toString();
            if (!TextUtils.isEmpty(uri2)) {
                intent.putExtra(com.tencent.assistant.b.a.p, uri2);
            }
            if (!TextUtils.isEmpty(queryParameter21)) {
                intent.putExtra(com.tencent.assistant.b.a.q, queryParameter21);
            }
            if (!TextUtils.isEmpty(queryParameter22)) {
                intent.putExtra(com.tencent.assistant.b.a.r, queryParameter22);
            }
            if (!TextUtils.isEmpty(queryParameter23)) {
                intent.putExtra(com.tencent.assistant.b.a.z, queryParameter23);
            }
            if (!TextUtils.isEmpty(queryParameter24)) {
                intent.putExtra(com.tencent.assistant.b.a.A, queryParameter24);
            }
            if (!TextUtils.isEmpty(queryParameter25)) {
                intent.putExtra(com.tencent.assistant.b.a.B, queryParameter25);
            }
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (TextUtils.isEmpty(queryParameter9)) {
            intent.setFlags(268468224);
        } else if (!(context instanceof Activity)) {
            intent.setFlags(335544320);
        }
        context.startActivity(intent);
        return true;
    }

    private static void m(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, GroupListActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.assistant.APP_GROUP_TYPE", 1);
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void n(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, GroupListActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.assistant.APP_GROUP_TYPE", 2);
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void o(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, SpecailTopicActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void c(Context context, Bundle bundle) {
        Intent intent = new Intent(context, InstalledAppManagerActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static boolean p(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, SearchActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        String queryParameter = uri.getQueryParameter("key");
        if (!TextUtils.isEmpty(queryParameter)) {
            intent.putExtra("com.tencent.assistant.KEYWORD", queryParameter);
        }
        String queryParameter2 = uri.getQueryParameter(com.tencent.assistant.b.a.X);
        if (!TextUtils.isEmpty(queryParameter2)) {
            try {
                intent.putExtra(com.tencent.assistant.b.a.X, Integer.valueOf(queryParameter2));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        String queryParameter3 = uri.getQueryParameter(com.tencent.assistant.b.a.Y);
        if (!TextUtils.isEmpty(queryParameter3)) {
            intent.putExtra(com.tencent.assistant.b.a.Y, queryParameter3);
        }
        String queryParameter4 = uri.getQueryParameter(com.tencent.assistant.b.a.Z);
        if (!TextUtils.isEmpty(queryParameter4)) {
            intent.putExtra(com.tencent.assistant.b.a.Z, queryParameter4);
        }
        String queryParameter5 = uri.getQueryParameter(com.tencent.assistant.b.a.aa);
        if (!TextUtils.isEmpty(queryParameter5)) {
            try {
                intent.putExtra(com.tencent.assistant.b.a.aa, Integer.valueOf(queryParameter5));
            } catch (NumberFormatException e2) {
                e2.printStackTrace();
            }
        }
        String queryParameter6 = uri.getQueryParameter(com.tencent.assistant.b.a.ab);
        if (!TextUtils.isEmpty(queryParameter6)) {
            try {
                intent.putExtra(com.tencent.assistant.b.a.ab, Integer.valueOf(queryParameter6));
            } catch (NumberFormatException e3) {
                e3.printStackTrace();
            }
        }
        intent.putExtra("com.tencent.assistant.SOURCESCENE", (int) STConst.ST_PAGE_SEARCH_RESULT_SOURCE_PUSH);
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
        return false;
    }

    private static void d(Context context, Bundle bundle) {
        Log.d("Donald", "onPageTransmit ");
        a(context, "com.tencent.mobileassistant_wifitransfer", "com.tencent.assistant.activity.WifiTransferActivity");
    }

    private static void q(Context context, Uri uri, Bundle bundle) {
        a(context, "com.assistant.accelerate", "com.assistant.accelerate.MobileAccelerateActivity");
    }

    protected static void a(Context context, Bundle bundle) {
        Intent intent = new Intent(context, PhotoBackupNewActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void a(Context context, String str, String str2) {
        PluginInfo.PluginEntry pluginEntryByStartActivity;
        PluginInfo a2 = d.b().a(str);
        if (a2 != null && (pluginEntryByStartActivity = a2.getPluginEntryByStartActivity(str2)) != null) {
            PluginProxyActivity.a(context, pluginEntryByStartActivity.getHostPlugInfo().getPackageName(), pluginEntryByStartActivity.getHostPlugInfo().getVersion(), pluginEntryByStartActivity.getStartActivity(), pluginEntryByStartActivity.getHostPlugInfo().getInProcess(), null, pluginEntryByStartActivity.getHostPlugInfo().getLaunchApplication());
        }
    }

    private static boolean r(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, BrowserActivity.class);
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        String queryParameter = uri.getQueryParameter(SocialConstants.PARAM_URL);
        XLog.i("xjp", "onWebView ---> url : " + queryParameter);
        if (TextUtils.isEmpty(queryParameter)) {
            return false;
        }
        intent.putExtra("com.tencent.assistant.BROWSER_URL", queryParameter);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        String queryParameter2 = uri.getQueryParameter("mode");
        if (!TextUtils.isEmpty(queryParameter2) && (queryParameter2.equals("0") || queryParameter2.equals("1"))) {
            intent.putExtra("com.tencent.assistant.activity.BROWSER_TYPE", queryParameter2);
        }
        String queryParameter3 = uri.getQueryParameter("goback");
        if (!TextUtils.isEmpty(queryParameter3)) {
            intent.putExtra("goback", queryParameter3);
        }
        String queryParameter4 = uri.getQueryParameter("pkgName");
        if (queryParameter4 != null && !TextUtils.isEmpty(queryParameter4)) {
            intent.putExtra("com.tencent.assistant.activity.PKGNAME_APPBAR", queryParameter4);
        }
        String queryParameter5 = uri.getQueryParameter("accelerate");
        if (!TextUtils.isEmpty(queryParameter5) && (queryParameter5.equals("0") || queryParameter5.equals("1"))) {
            intent.putExtra("com.tencent.assistant.activity.BROWSER_ACCELERATE", queryParameter5);
        }
        String queryParameter6 = uri.getQueryParameter("supportZoom");
        if (!TextUtils.isEmpty(queryParameter6) && (queryParameter6.equals("0") || queryParameter6.equals("1"))) {
            intent.putExtra("suport.zoom", queryParameter6);
        }
        intent.addFlags(872415232);
        context.startActivity(intent);
        return true;
    }

    private static void s(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, UpdateListActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        String queryParameter = uri.getQueryParameter(STConst.ST_PUSH_TO_UPDATE_KEY);
        if (!TextUtils.isEmpty(queryParameter)) {
            intent.putExtra(STConst.ST_PUSH_TO_UPDATE_KEY, queryParameter);
        }
        String queryParameter2 = uri.getQueryParameter(com.tencent.assistant.b.a.m);
        if (!TextUtils.isEmpty(queryParameter2)) {
            intent.putExtra(com.tencent.assistant.b.a.m, queryParameter2);
        }
        String queryParameter3 = uri.getQueryParameter("sort_list");
        if (!TextUtils.isEmpty(queryParameter3)) {
            intent.putExtra("sort_list", queryParameter3);
        }
        String queryParameter4 = uri.getQueryParameter(com.tencent.assistant.b.a.z);
        if (!TextUtils.isEmpty(queryParameter4)) {
            intent.putExtra(com.tencent.assistant.b.a.z, queryParameter4);
        }
        String queryParameter5 = uri.getQueryParameter(com.tencent.assistant.b.a.S);
        if (!TextUtils.isEmpty(queryParameter5)) {
            intent.putExtra(com.tencent.assistant.b.a.S, Integer.valueOf(queryParameter5));
        }
        String queryParameter6 = uri.getQueryParameter(com.tencent.assistant.b.a.T);
        if (!TextUtils.isEmpty(queryParameter6)) {
            intent.putExtra(com.tencent.assistant.b.a.T, Integer.valueOf(queryParameter6));
        }
        String queryParameter7 = uri.getQueryParameter(com.tencent.assistant.b.a.D);
        if (!TextUtils.isEmpty(queryParameter7)) {
            intent.putExtra(com.tencent.assistant.b.a.D, queryParameter7);
        }
        String queryParameter8 = uri.getQueryParameter(com.tencent.assistant.b.a.ae);
        if (!TextUtils.isEmpty(queryParameter8)) {
            intent.putExtra(com.tencent.assistant.b.a.ae, queryParameter8);
        }
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void t(Context context, Uri uri, Bundle bundle) {
        if (context == null) {
            context = AstApp.i().getApplicationContext();
        }
        XLog.d("voken", "download url = " + uri);
        Intent intent = new Intent(context, DownloadActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        String queryParameter = uri.getQueryParameter(com.tencent.assistant.b.a.f845a);
        String queryParameter2 = uri.getQueryParameter(com.tencent.assistant.b.a.c);
        String queryParameter3 = uri.getQueryParameter(com.tencent.assistant.b.a.d);
        String queryParameter4 = uri.getQueryParameter(com.tencent.assistant.b.a.e);
        String queryParameter5 = uri.getQueryParameter(com.tencent.assistant.b.a.h);
        String queryParameter6 = uri.getQueryParameter(com.tencent.assistant.b.a.C);
        String queryParameter7 = uri.getQueryParameter(com.tencent.assistant.b.a.i);
        String queryParameter8 = uri.getQueryParameter(com.tencent.assistant.b.a.l);
        String queryParameter9 = uri.getQueryParameter(com.tencent.assistant.b.a.k);
        String queryParameter10 = uri.getQueryParameter(com.tencent.assistant.b.a.b);
        String queryParameter11 = uri.getQueryParameter(com.tencent.assistant.b.a.x);
        String queryParameter12 = uri.getQueryParameter(com.tencent.assistant.b.a.w);
        String queryParameter13 = uri.getQueryParameter(com.tencent.assistant.b.a.D);
        String queryParameter14 = uri.getQueryParameter(com.tencent.assistant.b.a.s);
        String queryParameter15 = uri.getQueryParameter(com.tencent.assistant.b.a.J);
        String queryParameter16 = uri.getQueryParameter(com.tencent.assistant.b.a.K);
        String queryParameter17 = uri.getQueryParameter(com.tencent.assistant.b.a.L);
        String queryParameter18 = uri.getQueryParameter(com.tencent.assistant.b.a.M);
        String queryParameter19 = uri.getQueryParameter(com.tencent.assistant.b.a.H);
        String queryParameter20 = uri.getQueryParameter(com.tencent.assistant.b.a.I);
        String uri2 = uri.toString();
        if (!TextUtils.isEmpty(queryParameter)) {
            intent.putExtra(com.tencent.assistant.b.a.f845a, queryParameter);
        }
        if (!TextUtils.isEmpty(queryParameter2)) {
            intent.putExtra(com.tencent.assistant.b.a.c, queryParameter2);
        }
        if (!TextUtils.isEmpty(queryParameter3)) {
            intent.putExtra(com.tencent.assistant.b.a.d, queryParameter3);
        }
        if (!TextUtils.isEmpty(queryParameter4)) {
            intent.putExtra(com.tencent.assistant.b.a.e, queryParameter4);
        }
        if (!TextUtils.isEmpty(uri2)) {
            intent.putExtra(com.tencent.assistant.b.a.p, uri2);
        }
        if (!TextUtils.isEmpty(queryParameter5)) {
            intent.putExtra(com.tencent.assistant.b.a.h, queryParameter5);
        }
        if (!TextUtils.isEmpty(queryParameter6)) {
            intent.putExtra(com.tencent.assistant.b.a.C, queryParameter6);
        }
        if (!TextUtils.isEmpty(queryParameter7)) {
            intent.putExtra(com.tencent.assistant.b.a.i, queryParameter7);
        }
        if (!TextUtils.isEmpty(queryParameter8)) {
            intent.putExtra(com.tencent.assistant.b.a.l, queryParameter8);
        }
        if (!TextUtils.isEmpty(queryParameter9)) {
            intent.putExtra(com.tencent.assistant.b.a.k, queryParameter9);
        }
        if (!TextUtils.isEmpty(queryParameter10)) {
            intent.putExtra(com.tencent.assistant.b.a.b, queryParameter10);
        }
        if (!TextUtils.isEmpty(queryParameter11)) {
            intent.putExtra(com.tencent.assistant.b.a.x, queryParameter11);
        }
        if (!TextUtils.isEmpty(queryParameter12)) {
            intent.putExtra(com.tencent.assistant.b.a.w, queryParameter12);
        }
        if (!TextUtils.isEmpty(queryParameter13)) {
            intent.putExtra(com.tencent.assistant.b.a.D, queryParameter13);
        }
        if (!TextUtils.isEmpty(queryParameter14)) {
            intent.putExtra(com.tencent.assistant.b.a.s, queryParameter14);
        }
        if (!TextUtils.isEmpty(queryParameter19)) {
            intent.putExtra(com.tencent.assistant.b.a.H, queryParameter19);
        }
        if (!TextUtils.isEmpty(queryParameter20)) {
            XLog.d("channelid", "onPageDownload sdkId is: " + queryParameter20);
            intent.putExtra(com.tencent.assistant.b.a.I, queryParameter20);
        }
        if (!TextUtils.isEmpty(queryParameter15)) {
            intent.putExtra(com.tencent.assistant.b.a.J, queryParameter15);
        }
        if (!TextUtils.isEmpty(queryParameter16)) {
            intent.putExtra(com.tencent.assistant.b.a.K, queryParameter16);
        }
        if (!TextUtils.isEmpty(queryParameter17)) {
            intent.putExtra(com.tencent.assistant.b.a.L, queryParameter17);
        }
        if (!TextUtils.isEmpty(queryParameter18)) {
            intent.putExtra(com.tencent.assistant.b.a.M, queryParameter18);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(335544320);
        }
        context.startActivity(intent);
    }

    private static void u(Context context, Uri uri, Bundle bundle) {
        int i;
        byte b2 = 0;
        String queryParameter = uri.getQueryParameter(com.tencent.assistant.b.a.b);
        String queryParameter2 = uri.getQueryParameter(com.tencent.assistant.b.a.c);
        String queryParameter3 = uri.getQueryParameter(com.tencent.assistant.b.a.h);
        String queryParameter4 = uri.getQueryParameter(com.tencent.assistant.b.a.s);
        String queryParameter5 = uri.getQueryParameter(com.tencent.assistant.b.a.i);
        String queryParameter6 = uri.getQueryParameter(com.tencent.assistant.b.a.n);
        String queryParameter7 = uri.getQueryParameter(com.tencent.assistant.b.a.o);
        String queryParameter8 = uri.getQueryParameter(com.tencent.assistant.b.a.l);
        try {
            i = Integer.parseInt(queryParameter3);
            try {
                b2 = Byte.parseByte(queryParameter4);
            } catch (Exception e) {
            }
        } catch (Exception e2) {
            i = 0;
        }
        h.a().a(queryParameter, queryParameter2, i, b2, queryParameter5, queryParameter6, queryParameter7, queryParameter8);
    }

    private static boolean v(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, VideoActivityV2.class);
        String queryParameter = uri.getQueryParameter(SocialConstants.PARAM_URL);
        String queryParameter2 = uri.getQueryParameter("vid");
        String queryParameter3 = uri.getQueryParameter("from_detail");
        XLog.i("xjp", "onVideoPlay ---> url : " + queryParameter);
        XLog.i("xjp", "onVideoPlay ---> vid : " + queryParameter2);
        if (queryParameter != null && !TextUtils.isEmpty(queryParameter)) {
            intent.putExtra("com.tencent.assistant.VIDEO_URL", queryParameter);
        }
        if (queryParameter2 != null && !TextUtils.isEmpty(queryParameter2)) {
            intent.putExtra("com.tencent.assistant.VID", queryParameter2);
        }
        if (queryParameter != null && !TextUtils.isEmpty(queryParameter3)) {
            intent.putExtra("from_detail", queryParameter3);
        }
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.setFlags(268435456);
        context.startActivity(intent);
        return true;
    }

    private static void e(Context context, Bundle bundle) {
        Toast.makeText(context, "一键优化  暂不支持", 0).show();
    }

    private static boolean w(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, SearchActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        String queryParameter = uri.getQueryParameter("cpname");
        if (!TextUtils.isEmpty(queryParameter)) {
            intent.putExtra("com.tencent.assistant.KEYWORD", queryParameter);
        } else {
            String queryParameter2 = uri.getQueryParameter("cpid");
            if (!TextUtils.isEmpty(queryParameter2)) {
                intent.putExtra("com.tencent.assistant.KEYWORD", queryParameter2);
            }
        }
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
        return true;
    }

    private static void f(Context context, Bundle bundle) {
        Intent intent = new Intent(context, SettingActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void g(Context context, Bundle bundle) {
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.net.wifi.PICK_WIFI_NETWORK");
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void h(Context context, Bundle bundle) {
        Intent intent = new Intent();
        ComponentName componentName = new ComponentName("com.android.settings", "com.android.settings.DevelopmentSettings");
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.setComponent(componentName);
        intent.setAction("android.intent.action.VIEW");
        intent.setFlags(268435456);
        try {
            if (!(context instanceof Activity)) {
                intent.setFlags(268435456);
            }
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void x(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, SpecailTopicDetailActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        String queryParameter = uri.getQueryParameter("topicid");
        if (!TextUtils.isEmpty(queryParameter)) {
            intent.putExtra("com.tencent.assistant.TOPICID", queryParameter);
        }
        String queryParameter2 = uri.getQueryParameter("topicname");
        if (!TextUtils.isEmpty(queryParameter2)) {
            intent.putExtra("com.tencent.assistant.TOPICNAME", queryParameter2);
        }
        String queryParameter3 = uri.getQueryParameter("topicstyle");
        if (!TextUtils.isEmpty(queryParameter3)) {
            intent.putExtra("com.tencent.assistant.TOPICSTYLE", queryParameter3);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void i(Context context, Bundle bundle) {
        Intent intent = new Intent(context, HelperFeedbackActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void y(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, SpecailTopicActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void z(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, ApkMgrActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void A(Context context, Uri uri, Bundle bundle) {
        String a2 = com.tencent.e.a.b.a.a(uri.getQueryParameter(com.tencent.assistant.b.a.F));
        if (uri.getQueryParameter(com.tencent.assistant.b.a.D) != null && !TextUtils.isEmpty(a2)) {
            a2 = !a2.contains("?") ? a2 + b : a2 + f1394a;
        }
        if (uri.getQueryParameter(com.tencent.assistant.b.a.E) != null && !TextUtils.isEmpty(a2)) {
            if (!a2.contains("?")) {
                a2 = a2 + d;
            } else {
                a2 = a2 + c;
            }
        }
        if (!TextUtils.isEmpty(a2)) {
            b.a(context, Uri.parse(a2), bundle);
        }
    }

    private static void a(Context context) {
        SelfUpdateManager.a().a(true);
    }

    private static void B(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, PluginDetailActivity.class);
        intent.putExtra("plugin_start_entry", new PluginStartEntry(ct.a(uri.getQueryParameter("pluginid"), 0), null, uri.getQueryParameter("pname"), ct.a(uri.getQueryParameter("versioncode"), 0), uri.getQueryParameter("activity"), Constants.STR_EMPTY));
        context.startActivity(intent);
    }

    private static void C(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, UserCenterActivityV2.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static boolean D(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, TagPageActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        String queryParameter = uri.getQueryParameter("tagID");
        String queryParameter2 = uri.getQueryParameter("tagName");
        String queryParameter3 = uri.getQueryParameter("appID");
        String queryParameter4 = uri.getQueryParameter("pkgName");
        String queryParameter5 = uri.getQueryParameter("tagSubTitle");
        String queryParameter6 = uri.getQueryParameter("firstIconUrl");
        if (!TextUtils.isEmpty(queryParameter)) {
            intent.putExtra("tagID", queryParameter);
        }
        if (!TextUtils.isEmpty(queryParameter2)) {
            intent.putExtra("tagName", queryParameter2);
        }
        if (!TextUtils.isEmpty(queryParameter3)) {
            intent.putExtra("appID", queryParameter3);
        }
        if (!TextUtils.isEmpty(queryParameter4)) {
            intent.putExtra("pkgName", queryParameter4);
        }
        if (!TextUtils.isEmpty(queryParameter5)) {
            intent.putExtra("tagSubTitle", queryParameter5);
        }
        if (!TextUtils.isEmpty(queryParameter6)) {
            intent.putExtra("firstIconUrl", queryParameter6);
        }
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
        return true;
    }

    private static void E(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, AppBackupActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void F(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, SpaceCleanActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void G(Context context, Uri uri, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        int a2 = ct.a(uri.getQueryParameter("logintype"), 2);
        int a3 = ct.a(uri.getQueryParameter("loginfrom"), 0);
        bundle.putInt(AppConst.KEY_LOGIN_TYPE, a2);
        bundle.putInt(AppConst.KEY_FROM_TYPE, a3);
        com.tencent.assistant.login.d.a().a(a2 == 0 ? AppConst.IdentityType.WX : AppConst.IdentityType.MOBILEQ, bundle);
    }

    private static void H(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, GuideActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void I(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, TagPageActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        String queryParameter = uri.getQueryParameter("tag_id");
        String queryParameter2 = uri.getQueryParameter("tag_name");
        String queryParameter3 = uri.getQueryParameter("appID");
        String queryParameter4 = uri.getQueryParameter("pkgName");
        String queryParameter5 = uri.getQueryParameter("tagSubTitle");
        String queryParameter6 = uri.getQueryParameter("firstIconUrl");
        if (!TextUtils.isEmpty(queryParameter)) {
            intent.putExtra("tagID", queryParameter);
        }
        if (!TextUtils.isEmpty(queryParameter2)) {
            intent.putExtra("tagName", queryParameter2);
        }
        if (!TextUtils.isEmpty(queryParameter3)) {
            intent.putExtra("appID", queryParameter3);
        }
        if (!TextUtils.isEmpty(queryParameter4)) {
            intent.putExtra("pkgName", queryParameter4);
        }
        if (!TextUtils.isEmpty(queryParameter5)) {
            intent.putExtra("tagSubTitle", queryParameter5);
        }
        if (!TextUtils.isEmpty(queryParameter6)) {
            intent.putExtra("firstIconUrl", queryParameter6);
        }
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void J(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, StartScanActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void K(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, LoginRichDialog.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(536870912);
        }
        context.startActivity(intent);
    }

    private static void L(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, GuessFavorActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void M(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, AppRankActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void N(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, GameRankActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static void O(Context context, Uri uri, Bundle bundle) {
        Intent intent = new Intent(context, ExternalCallActivity.class);
        if (bundle != null) {
            if (uri != null) {
                HashSet<String> hashSet = new HashSet<>();
                try {
                    if (Build.VERSION.SDK_INT >= 11) {
                        hashSet.addAll(uri.getQueryParameterNames());
                    } else {
                        hashSet.addAll(a(uri));
                    }
                } catch (Throwable th) {
                    th.printStackTrace();
                }
                for (String str : hashSet) {
                    bundle.putString(str, uri.getQueryParameter(str));
                }
                bundle.putString("to_page", uri.getHost());
            }
            intent.putExtras(bundle);
        }
        intent.putExtra("com.tencent.android.qqdownloader.key.PAGE_FROM", context.getClass().getSimpleName());
        if (!(context instanceof Activity)) {
            intent.setFlags(268468224);
        }
        context.startActivity(intent);
    }

    protected static boolean a(Context context, Intent intent) {
        String scheme;
        Uri data = intent.getData();
        if (data != null && (scheme = data.getScheme()) != null && scheme.equals("tmast")) {
            return a(data.getHost());
        }
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 128);
        return queryIntentActivities != null && queryIntentActivities.size() > 0;
    }

    protected static boolean a(String str) {
        if (str.equals("competitive") || str.equals("rank") || str.equals("app") || str.equals("game") || str.equals("assistant") || str.equals("appdetails") || str.equals("appcategory") || str.equals("gamecategory") || str.equals("appcategoryentry") || str.equals("gamecategoryentry") || str.equals("appcategorydetail") || str.equals("gamecategorydetail") || str.equals("necessity") || str.equals("hot") || str.equals("specialtopic") || str.equals("appmanagement") || str.equals("search") || str.equals("transmit") || str.equals("update") || str.equals("download") || str.equals("optimize") || str.equals("publisher") || str.equals("webview") || str.equals("devsetting") || str.equals("wifisetting") || str.equals("topic") || str.equals("feedback") || str.equals("encrypt") || str.equals("setting") || str.equals("selfupdatecheck") || str.equals("updatedownload") || str.equals("mobilemanage") || str.equals("mobilemanagefromdock") || str.equals("found") || str.equals("video") || str.equals("ebook") || str.equals("apkmanagement") || str.equals("plugindetail") || str.equals("usercenter") || str.equals("guessfavor") || str.equals("apprank") || str.equals("videoplay") || str.equals("spaceclean") || str.equals("picbackup") || str.equals("safescan") || str.equals("appbackup") || str.equals("mobileaccel") || str.equals("login") || str.equals("tag_detail")) {
            return true;
        }
        return false;
    }

    public static Set<String> a(Uri uri) {
        if (uri == null) {
            return Collections.emptySet();
        }
        if (uri.isOpaque()) {
            throw new UnsupportedOperationException("This isn't a hierarchical URI.");
        }
        String encodedQuery = uri.getEncodedQuery();
        if (encodedQuery == null) {
            return Collections.emptySet();
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        int i = 0;
        do {
            int indexOf = encodedQuery.indexOf(38, i);
            if (indexOf == -1) {
                indexOf = encodedQuery.length();
            }
            int indexOf2 = encodedQuery.indexOf(61, i);
            if (indexOf2 > indexOf || indexOf2 == -1) {
                indexOf2 = indexOf;
            }
            linkedHashSet.add(Uri.decode(encodedQuery.substring(i, indexOf2)));
            i = indexOf + 1;
        } while (i < encodedQuery.length());
        return Collections.unmodifiableSet(linkedHashSet);
    }
}
