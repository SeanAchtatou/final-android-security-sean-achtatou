package com.tencent.assistant.link.sdk.c;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

/* compiled from: ProGuard */
public class a implements c {

    /* renamed from: a  reason: collision with root package name */
    private Context f1405a = null;

    public a() {
    }

    public a(Context context) {
        this.f1405a = context;
    }

    public synchronized int a(com.tencent.assistant.link.sdk.b.a aVar) {
        int a2;
        if (aVar == null) {
            a2 = -1;
        } else {
            a(aVar.f1404a, aVar.b);
            d c = c().c();
            ContentValues contentValues = new ContentValues();
            contentValues.put("packageName", aVar.f1404a);
            contentValues.put("version", aVar.b);
            contentValues.put("dataItem", aVar.a());
            contentValues.put("state", Integer.valueOf(aVar.d));
            a2 = (int) (c.a("applink_action_task_table", (String) null, contentValues) + ((long) 0));
        }
        return a2;
    }

    public boolean a(String str, String str2, int i) {
        d c = c().c();
        ContentValues contentValues = new ContentValues();
        contentValues.put("state", Integer.valueOf(i));
        if (c.a("applink_action_task_table", contentValues, "packageName=?", new String[]{str}) + 0 > 0) {
            return true;
        }
        return false;
    }

    public boolean a(String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        if (c().c().a("applink_action_task_table", "packageName=?", new String[]{str}) > 0) {
            return true;
        }
        return false;
    }

    public String a() {
        return "applink_action_task_table";
    }

    public String b() {
        return "CREATE TABLE if not exists applink_action_task_table( _id INTEGER PRIMARY KEY AUTOINCREMENT, packageName TEXT , version TEXT,dataItem BLOB,state INTEGER)";
    }

    public String[] a(int i, int i2) {
        return null;
    }

    public e c() {
        return b.a(this.f1405a);
    }

    public void a(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void b(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }
}
