package com.tencent.assistant.login.model;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
public class b extends a {
    private static b d;

    public static synchronized b b() {
        b bVar;
        synchronized (b.class) {
            if (d == null) {
                d = new b();
            }
            bVar = d;
        }
        return bVar;
    }

    private b() {
        super(AppConst.IdentityType.NONE);
    }

    /* access modifiers changed from: protected */
    public JceStruct a() {
        return null;
    }

    /* access modifiers changed from: protected */
    public byte[] getKey() {
        return "ji*9^&43U0X-~./(".getBytes();
    }
}
