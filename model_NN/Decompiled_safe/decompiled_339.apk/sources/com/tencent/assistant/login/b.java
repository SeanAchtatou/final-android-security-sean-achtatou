package com.tencent.assistant.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.plugin.PluginHelper;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.d;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class b extends a {
    public static String b = "com.tencent.mobileassistant_login";
    private static b c;

    public static synchronized b h() {
        b bVar;
        synchronized (b.class) {
            if (c == null) {
                c = new b();
            }
            bVar = c;
        }
        return bVar;
    }

    private b() {
    }

    public void d() {
        if (this.f1454a == null || !this.f1454a.containsKey(AppConst.KEY_FROM_TYPE) || this.f1454a.getInt(AppConst.KEY_FROM_TYPE) != 5) {
            int requireInstall = PluginHelper.requireInstall(b);
            if (requireInstall == 1) {
                PluginInfo a2 = d.b().a(b);
                if (this.f1454a != null) {
                    this.f1454a.putInt("pkgVersion", a2.getVersion());
                }
                a("login", AstApp.m(), this.f1454a);
            } else if (requireInstall == 0) {
                j();
            } else if (requireInstall == -1) {
                j();
            }
        } else {
            k.a(new STInfoV2(STConst.ST_PAGE_LOGIN_FROM_QQ, "03_001", STConst.ST_PAGE_LOGIN_FROM_QQ, STConst.ST_DEFAULT_SLOT, 100));
            a("loginWithA1", this.f1454a);
        }
    }

    private void j() {
        Intent intent = new Intent();
        Context m = AstApp.m() != null ? AstApp.m() : AstApp.i();
        intent.setClass(m, PluginLoadingActivity.class);
        if (!(m instanceof Activity)) {
            intent.setFlags(268435456);
        }
        m.startActivity(intent);
    }

    public AppConst.LoginEgnineType e() {
        return AppConst.LoginEgnineType.ENGINE_MOBILE_QQ;
    }

    public void f() {
        a("quit", new Object[0]);
    }

    public void g() {
        a("loadTicket", new Object[0]);
    }

    public void i() {
        d();
    }

    private void a(String str, Object... objArr) {
        TemporaryThreadManager.get().start(new c(this, str, objArr));
    }
}
