package com.tencent.assistant.login.a;

import com.tencent.d.a.a;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;

/* compiled from: ProGuard */
public class c {
    private static PublicKey a(String str) {
        return KeyFactory.getInstance("RSA", "BC").generatePublic(new X509EncodedKeySpec(a.a(str, 0)));
    }

    private static PrivateKey b(String str) {
        return KeyFactory.getInstance("RSA", "BC").generatePrivate(new PKCS8EncodedKeySpec(a.a(str, 0)));
    }

    private static byte[] a(byte[] bArr, Cipher cipher) {
        int blockSize = cipher.getBlockSize();
        int outputSize = cipher.getOutputSize(blockSize);
        int length = bArr.length;
        byte[] bArr2 = new byte[((length % blockSize == 0 ? length / blockSize : (length / blockSize) + 1) * outputSize)];
        int i = 0;
        int i2 = length;
        while (i2 >= blockSize) {
            cipher.doFinal(bArr, i * blockSize, blockSize, bArr2, i * outputSize);
            i++;
            i2 -= blockSize;
        }
        if (i2 > 0) {
            cipher.doFinal(bArr, i * blockSize, i2, bArr2, i * outputSize);
        }
        return bArr2;
    }

    public static String a(byte[] bArr, String str, boolean z) {
        if (bArr == null) {
            return null;
        }
        try {
            Cipher instance = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            if (z) {
                instance.init(2, a(str));
            } else {
                instance.init(2, b(str));
            }
            return new String(a(bArr, instance)).trim();
        } catch (Exception e) {
            return null;
        }
    }

    public static byte[] a(String str, String str2, boolean z) {
        try {
            Cipher instance = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            if (z) {
                instance.init(1, a(str2));
            } else {
                instance.init(1, b(str2));
            }
            return a(str.getBytes("UTF-8"), instance);
        } catch (Exception e) {
            return null;
        }
    }
}
