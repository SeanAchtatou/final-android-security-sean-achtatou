package com.tencent.assistant.login;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.event.EventController;
import com.tencent.assistant.event.EventDispatcher;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.js.m;
import com.tencent.assistant.login.a.a;
import com.tencent.assistant.login.model.MoblieQIdentityInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.bh;
import com.tencent.assistant.utils.e;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.Map;

/* compiled from: ProGuard */
public class PluginLoginIn {
    public static void onGetMobileQIdentity(String str, long j, byte[] bArr, byte[] bArr2, byte[] bArr3, String str2, boolean z) {
        b.h().a(str, j, bArr, bArr2, bArr3, str2, false);
    }

    public static void onGetMobileQIdentity(MoblieQIdentityInfo moblieQIdentityInfo, boolean z) {
        b.h().a(moblieQIdentityInfo, z);
    }

    public static void onLoginCancel() {
        b.h().c();
    }

    public static void onLoginFail() {
        b.h().b();
    }

    public static int getMobileQVersionCode() {
        return d.a().w();
    }

    public static boolean isWXInstalled() {
        return d.a().v();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.login.d.a(com.tencent.assistant.AppConst$IdentityType, android.os.Bundle, boolean):void
     arg types: [com.tencent.assistant.AppConst$IdentityType, android.os.Bundle, int]
     candidates:
      com.tencent.assistant.login.d.a(java.lang.String, java.lang.String, java.lang.String):void
      com.tencent.assistant.login.d.a(com.tencent.assistant.AppConst$IdentityType, android.os.Bundle, boolean):void */
    public static void login(AppConst.IdentityType identityType, Bundle bundle) {
        d.a().a(identityType, bundle, false);
    }

    public static void initEngine(AppConst.IdentityType identityType) {
        d.a().a(identityType);
    }

    public static void logReport(int i, int i2, String str, int i3, byte b, Map<String, String> map) {
        k.a(new STInfoV2(i, str, i2, STConst.ST_DEFAULT_SLOT, i3), map);
    }

    public static void logReportV2(int i, String str, int i2, int i3, String str2) {
        STInfoV2 sTInfoV2 = new STInfoV2(i, str, i2, STConst.ST_DEFAULT_SLOT, i3);
        if (sTInfoV2 != null) {
            sTInfoV2.extraData = str2;
            XLog.i("PluginLoginIn", "[logReportV2] --> pageId = " + i + ", slotId = " + str + ", prePageId = " + i2 + ", actionId = " + i3 + ", extraData = " + str2);
            k.a(sTInfoV2);
        }
    }

    public static boolean isAppInstalledFromSystem(String str, int i) {
        return e.b(str, i);
    }

    public static String getUserId() {
        return a.a();
    }

    public static long getUin() {
        return a.b();
    }

    public static AstApp getAstApp() {
        return AstApp.i();
    }

    public static Handler getMainHandler() {
        return ba.a();
    }

    public static void saveTicket(String str, String str2, String str3) {
        a.a(str, str2, str3);
    }

    public static void saveTicket(String str, long j, String str2, String str3) {
        a.a(str, j, str2, str3);
    }

    public static byte[] encrypt(byte[] bArr, byte[] bArr2) {
        return bh.a(bArr, bArr2);
    }

    public static void saveOpenId(long j, String str) {
        m.a(j, str);
    }

    public static EventController getEventController() {
        return AstApp.i().k();
    }

    public static EventDispatcher getEventDispatcher() {
        return AstApp.i().j();
    }

    public static int getActivityPageId(Context context) {
        if (context instanceof BaseActivity) {
            return ((BaseActivity) context).f();
        }
        return 2000;
    }

    public static void closeByKeybackOrTouchOutside() {
        Message obtainMessage = AstApp.i().j().obtainMessage();
        obtainMessage.what = EventDispatcherEnum.UI_EVENT_SETTING_LOGIN_CANCEL;
        AstApp.i().j().sendMessage(obtainMessage);
    }
}
