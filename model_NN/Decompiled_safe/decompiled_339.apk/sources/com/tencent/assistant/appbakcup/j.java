package com.tencent.assistant.appbakcup;

import android.view.View;
import android.widget.ProgressBar;
import com.tencent.assistant.protocol.jce.BackupDevice;

/* compiled from: ProGuard */
class j implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BackupDevice f837a;
    final /* synthetic */ ProgressBar b;
    final /* synthetic */ int c;
    final /* synthetic */ BackupDeviceAdapter d;

    j(BackupDeviceAdapter backupDeviceAdapter, BackupDevice backupDevice, ProgressBar progressBar, int i) {
        this.d = backupDeviceAdapter;
        this.f837a = backupDevice;
        this.b = progressBar;
        this.c = i;
    }

    public void onClick(View view) {
        if (this.d.c == -1) {
            this.d.d.a(this.f837a);
            this.b.setVisibility(0);
            int unused = this.d.c = this.c;
        }
    }
}
