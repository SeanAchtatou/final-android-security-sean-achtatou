package com.tencent.assistant.plugin;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.db.table.e;
import com.tencent.assistant.db.table.x;
import com.tencent.assistant.event.EventDispatcher;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.manager.cr;
import com.tencent.assistant.plugin.mgr.d;
import com.tencent.assistant.protocol.jce.AutoStartCfg;
import com.tencent.assistant.protocol.l;
import com.tencent.assistant.usagestats.UsagestatsSTManager;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.installuninstall.InstallUninstallUtil;
import com.tencent.assistant.utils.t;
import com.tencent.beacon.event.a;
import java.util.Map;

/* compiled from: ProGuard */
public class PluginProxyManager {
    public static final String KEY_DOCK_DISABLED_PACKAGES = "key_dock_disabled_packages";
    public static byte ST_MOBILE_EXCHANGE = 8;
    public static final String ServerEncoding = "utf-8";

    public static boolean getShowWifiTipsBeforeDownload() {
        return m.a().m();
    }

    public static void setShowWifiTipsBeforeDownload(boolean z) {
        m.a().e(z);
    }

    public static void registerNetWorkListener(NetworkMonitor.ConnectivityChangeListener connectivityChangeListener) {
        cq.a().a(connectivityChangeListener);
    }

    public static void unregisterNetWorkListener(NetworkMonitor.ConnectivityChangeListener connectivityChangeListener) {
        cq.a().b(connectivityChangeListener);
    }

    public static void registerUIEventListener(int i, UIEventListener uIEventListener) {
        AstApp.i().k().addUIEventListener(i, uIEventListener);
    }

    public static void unregisterUIEventListener(int i, UIEventListener uIEventListener) {
        AstApp.i().k().removeUIEventListener(i, uIEventListener);
    }

    public static void dispatchUIEvent(int i) {
        EventDispatcher j = AstApp.i().j();
        j.sendMessage(j.obtainMessage(i));
    }

    public static int getDownloadingAppInfoSize() {
        return DownloadProxy.a().g();
    }

    public static byte[] jceStructToUTF8Byte(JceStruct jceStruct) {
        return l.b(jceStruct);
    }

    public static void saveSTTable(byte b, byte[] bArr) {
        x.a().a(b, bArr);
    }

    public static void setToSetting(String str, Object obj) {
        m.a().b(str, obj);
    }

    public static String getFromSetting(String str, String str2) {
        return m.a().a(str, str2);
    }

    public static long getLongFromSettings(String str, long j) {
        return m.a().a(str, j);
    }

    public static boolean getBooleanFromSettings(String str, Boolean bool) {
        return m.a().a(str, bool.booleanValue());
    }

    public static boolean hasTempRoot() {
        return cr.a().c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public static boolean isRootAuthorized() {
        return m.a().a("key_permanent_root_avaliable", false);
    }

    public static boolean isDeviceForeverRooted() {
        return m.a().n() == AppConst.ROOT_STATUS.ROOTED;
    }

    public static boolean requestRootPermission() {
        return InstallUninstallUtil.a();
    }

    public static void savePermRootPermission() {
        cr.a().b();
    }

    public static String runTempRootCommand(String str) {
        return cr.a().b(str);
    }

    public static AutoStartCfg getAutoStartCfg() {
        return new e().a();
    }

    public static String formatSizeM(long j, int i) {
        return bt.a(j, i);
    }

    public static void addShortCut(Context context, Bitmap bitmap, String str, Intent intent) {
        com.tencent.assistant.utils.e.a(context, bitmap, str, intent);
    }

    public static void delShortCut(Context context, String str, Intent intent) {
        com.tencent.assistant.utils.e.a(context, str, intent);
    }

    public static final String getPhoneGuid() {
        return Global.getPhoneGuidAndGen();
    }

    public static final String getLinuxKernalInfo() {
        return t.v();
    }

    public static String getImei() {
        return t.g();
    }

    public static final String getQUA() {
        return Global.getQUAForBeacon();
    }

    public static Context getAstAppContext() {
        return AstApp.i();
    }

    public static Handler getMainHandler() {
        return ba.a();
    }

    public static int getPluginVersion(String str) {
        PluginInfo a2 = d.b().a(str);
        if (a2 != null) {
            return a2.getVersion();
        }
        return -1;
    }

    public static void reTriggerTempRoot() {
        TemporaryThreadManager.get().start(new b());
    }

    public static void triggerAppUsageReport() {
        UsagestatsSTManager.a().a(UsagestatsSTManager.ReportScene.temproot);
    }

    public static final void doBeaconReoprt(String str, boolean z, long j, long j2, Map<String, String> map, boolean z2) {
        a.a(str, z, j, j2, map, z2);
    }

    public static LocalApkInfo getLocalApkInfo(String str) {
        return ApkResourceManager.getInstance().getLocalApkInfo(str);
    }
}
