package com.tencent.assistant.plugin.a;

import android.os.Bundle;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.login.a.a;
import com.tencent.assistant.login.d;
import com.tencent.assistant.login.model.MoblieQIdentityInfo;
import com.tencent.assistant.plugin.PluginIPCClient;
import com.tencent.assistant.plugin.UserLoginInfo;
import com.tencent.assistant.plugin.UserStateInfo;
import com.tencent.assistant.sdk.SDKIPCBroadcaster;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.bh;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static d f1942a = new d(null);

    public static UserLoginInfo a(String str) {
        UserLoginInfo userLoginInfo = new UserLoginInfo();
        if (PluginIPCClient.LOGIN_ADDTION_INFO_GET_USER_INFO.equals(str)) {
            d a2 = d.a();
            if (a2.k()) {
                userLoginInfo.setState(0);
                a(a2, userLoginInfo);
            }
        } else if (PluginIPCClient.LOGIN_ADDTION_INFO_LOGIN.equals(str)) {
            AstApp.i().k();
            Bundle bundle = new Bundle();
            bundle.putInt(AppConst.KEY_LOGIN_TYPE, 6);
            bundle.putInt(AppConst.KEY_FROM_TYPE, 7);
            ba.a().post(new c(bundle));
        }
        return userLoginInfo;
    }

    private static void a(d dVar, UserLoginInfo userLoginInfo) {
        MoblieQIdentityInfo moblieQIdentityInfo = (MoblieQIdentityInfo) dVar.c();
        com.tencent.assistant.login.a.b f = a.f();
        if (moblieQIdentityInfo != null) {
            int i = dVar.w() > 83 ? 2 : 3;
            userLoginInfo.setA2(bh.b(moblieQIdentityInfo.getGTKey_ST_A2(), moblieQIdentityInfo.getKey()));
            userLoginInfo.setState(i);
            userLoginInfo.setUin(moblieQIdentityInfo.getUin());
            if (f != null) {
                userLoginInfo.setNickName(f.b);
                userLoginInfo.setPic(f.f1456a);
                return;
            }
            return;
        }
        userLoginInfo.setState(0);
    }

    public static void a(UserStateInfo userStateInfo) {
        SDKIPCBroadcaster.a().a(userStateInfo);
    }

    public static void a() {
        a(new UserStateInfo(4));
    }
}
