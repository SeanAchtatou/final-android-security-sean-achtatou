package com.tencent.assistant.g;

import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.model.ShareAppModel;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.h;
import com.tencent.assistant.protocol.jce.AppDetailWithComment;
import com.tencent.assistant.protocol.jce.GetAppDetailResponse;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;

/* compiled from: ProGuard */
class r extends h {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ p f1324a;

    private r(p pVar) {
        this.f1324a = pVar;
    }

    /* synthetic */ r(p pVar, q qVar) {
        this(pVar);
    }

    public void a() {
        SimpleAppModel simpleAppModel = new SimpleAppModel();
        simpleAppModel.c = AstApp.i().getPackageName();
        a(simpleAppModel, (byte) 0);
        XLog.i("YYBShareOganizer", "YYBShareInfoLoadEngine refreshShareData");
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        super.onRequestSuccessed(i, jceStruct, jceStruct2);
        if (jceStruct2 != null) {
            as.w().a((GetAppDetailResponse) jceStruct2);
        }
    }

    public ShareAppModel b() {
        ArrayList<AppDetailWithComment> a2;
        GetAppDetailResponse s = as.w().s();
        if (s == null || (a2 = s.a()) == null || a2.size() <= 0) {
            return null;
        }
        return ShareAppModel.a(new SimpleAppModel(a2.get(0)));
    }
}
