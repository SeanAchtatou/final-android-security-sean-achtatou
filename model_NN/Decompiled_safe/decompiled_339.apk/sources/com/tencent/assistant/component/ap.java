package com.tencent.assistant.component;

import android.view.View;
import com.tencent.assistant.component.HorizonScrollLayout;

/* compiled from: ProGuard */
class ap implements HorizonScrollLayout.OnTouchScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HomePageBanner f894a;

    ap(HomePageBanner homePageBanner) {
        this.f894a = homePageBanner;
    }

    public void onScrollStateChanged(int i, int i2) {
        if (1 == i) {
            this.f894a.stopPlay();
        } else if (i == 0) {
            this.f894a.startPlay();
        }
    }

    public void onScroll(View view, float f, float f2) {
    }

    public void onScreenChange(int i) {
        if (this.f894a.cards.size() > 0) {
            this.f894a.updateDots(i);
        }
    }
}
