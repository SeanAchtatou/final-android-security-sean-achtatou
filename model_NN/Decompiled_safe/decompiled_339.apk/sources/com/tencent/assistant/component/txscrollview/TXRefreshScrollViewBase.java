package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
public abstract class TXRefreshScrollViewBase<T extends View> extends TXScrollViewBase<T> {
    protected static int headerLoadingLayoutContentHeight = 0;
    protected TXLoadingLayoutBase mFooterLayout;
    protected TXLoadingLayoutBase mHeaderLayout;
    protected boolean mLayoutVisibilityChangesEnabled = true;
    protected ITXRefreshListViewListener mRefreshListViewListener = null;
    protected RefreshState mRefreshState = RefreshState.RESET;

    /* compiled from: ProGuard */
    public enum RefreshState {
        RESET,
        PULL_TO_REFRESH,
        RELEASE_TO_REFRESH,
        REFRESHING,
        REFRESH_LOAD_FINISH
    }

    /* access modifiers changed from: protected */
    public abstract TXLoadingLayoutBase createLoadingLayout(Context context, TXScrollViewBase.ScrollMode scrollMode);

    public TXRefreshScrollViewBase(Context context, TXScrollViewBase.ScrollDirection scrollDirection, TXScrollViewBase.ScrollMode scrollMode) {
        super(context, scrollDirection, scrollMode);
    }

    public TXRefreshScrollViewBase(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void onRefreshComplete(boolean z) {
        if (this.mRefreshState != RefreshState.REFRESHING) {
            return;
        }
        if (!z) {
            setState(RefreshState.REFRESH_LOAD_FINISH);
        } else {
            setState(RefreshState.RESET);
        }
    }

    /* access modifiers changed from: protected */
    public void initView(Context context) {
        if (this.mScrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_START) {
            this.mHeaderLayout = createLoadingLayout(context, TXScrollViewBase.ScrollMode.PULL_FROM_START);
        } else if (this.mScrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_END) {
            this.mFooterLayout = createLoadingLayout(context, TXScrollViewBase.ScrollMode.PULL_FROM_END);
        } else if (this.mScrollMode == TXScrollViewBase.ScrollMode.BOTH) {
            this.mHeaderLayout = createLoadingLayout(context, TXScrollViewBase.ScrollMode.PULL_FROM_START);
            this.mFooterLayout = createLoadingLayout(context, TXScrollViewBase.ScrollMode.PULL_FROM_END);
        }
        super.initView(context);
        updateUIFroMode();
        initLoadingLayoutHeight();
    }

    /* access modifiers changed from: protected */
    public void onScrollViewSizeChange(int i, int i2) {
        refreshLoadingLayoutSize();
        super.onScrollViewSizeChange(i, i2);
    }

    private void initLoadingLayoutHeight() {
        try {
            headerLoadingLayoutContentHeight = (int) getContext().getResources().getDimension(R.dimen.refresh_header_loading_layout_content_height);
        } catch (Resources.NotFoundException e) {
        }
    }

    /* access modifiers changed from: protected */
    public boolean onTouchEventCancelAndUp() {
        if (!this.mIsBeingDragged) {
            return false;
        }
        this.mIsBeingDragged = false;
        if (this.mRefreshState == RefreshState.RELEASE_TO_REFRESH) {
            setState(RefreshState.REFRESHING);
            setVerticalFadingEdgeEnabled(false);
            return true;
        } else if (this.mRefreshState == RefreshState.REFRESHING) {
            if (!(this instanceof RankRefreshGetMoreListView) || this.mCurScrollState != TXScrollViewBase.ScrollState.ScrollState_FromStart) {
                smoothScrollTo(0);
                return true;
            }
            setState(RefreshState.REFRESHING);
            setVerticalFadingEdgeEnabled(false);
            return true;
        } else if (this.mRefreshState == RefreshState.REFRESH_LOAD_FINISH) {
            smoothScrollTo(0);
            return true;
        } else {
            setState(RefreshState.RESET);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public int scrollMoveEvent() {
        int scrollMoveEvent = super.scrollMoveEvent();
        int i = 0;
        if (!(scrollMoveEvent == 0 || this.mRefreshState == RefreshState.REFRESHING)) {
            if (this.mCurScrollState == TXScrollViewBase.ScrollState.ScrollState_FromStart && this.mHeaderLayout != null) {
                i = this.mHeaderLayout.getTriggerSize();
                this.mHeaderLayout.onPull(scrollMoveEvent);
            } else if (this.mCurScrollState == TXScrollViewBase.ScrollState.ScrollState_FromEnd && this.mFooterLayout != null) {
                i = this.mFooterLayout.getContentSize();
                this.mFooterLayout.onPull(scrollMoveEvent);
            }
            if (this.mCurScrollState == TXScrollViewBase.ScrollState.ScrollState_FromEnd && this.mRefreshState == RefreshState.REFRESH_LOAD_FINISH) {
                return scrollMoveEvent;
            }
            if (this.mRefreshState != RefreshState.PULL_TO_REFRESH && i >= Math.abs(scrollMoveEvent)) {
                setState(RefreshState.PULL_TO_REFRESH);
            } else if (this.mRefreshState == RefreshState.PULL_TO_REFRESH && i < Math.abs(scrollMoveEvent)) {
                setState(RefreshState.RELEASE_TO_REFRESH);
            }
        }
        return scrollMoveEvent;
    }

    /* access modifiers changed from: protected */
    public void contentViewScrollTo(int i) {
        super.contentViewScrollTo(i);
        if (!this.mLayoutVisibilityChangesEnabled) {
            return;
        }
        if (i < 0 && this.mHeaderLayout != null) {
            this.mHeaderLayout.setVisibility(0);
        } else if (i <= 0 || this.mFooterLayout == null) {
            if (this.mHeaderLayout != null) {
                this.mHeaderLayout.setVisibility(4);
            }
            if (this.mFooterLayout != null) {
                this.mFooterLayout.setVisibility(4);
            }
        } else {
            this.mFooterLayout.setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public LinearLayout.LayoutParams getLoadingLayoutLayoutParams() {
        if (this.mScrollDirection == TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_HORIZONTAL) {
            return new LinearLayout.LayoutParams(-2, -1);
        }
        return new LinearLayout.LayoutParams(-1, -2);
    }

    /* access modifiers changed from: protected */
    public void updateUIFroMode() {
        LinearLayout.LayoutParams loadingLayoutLayoutParams = getLoadingLayoutLayoutParams();
        if (this.mHeaderLayout != null) {
            if (this.mHeaderLayout.getParent() == this) {
                removeView(this.mHeaderLayout);
            }
            addViewInternal(this.mHeaderLayout, 0, loadingLayoutLayoutParams);
        }
        if (this.mFooterLayout != null) {
            if (this.mFooterLayout.getParent() == this) {
                removeView(this.mFooterLayout);
            }
            addViewInternal(this.mFooterLayout, -1, loadingLayoutLayoutParams);
        }
        refreshLoadingLayoutSize();
    }

    /* access modifiers changed from: protected */
    public final void refreshLoadingLayoutSize() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6 = 0;
        int maximumScrollOffset = (int) (((float) getMaximumScrollOffset()) * 1.2f);
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        if (this.mScrollDirection == TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_HORIZONTAL) {
            if ((this.mScrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_START || this.mScrollMode == TXScrollViewBase.ScrollMode.BOTH) && this.mHeaderLayout != null) {
                this.mHeaderLayout.setWidth(maximumScrollOffset);
                i5 = -maximumScrollOffset;
            } else {
                i5 = 0;
            }
            if ((this.mScrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_END || this.mScrollMode == TXScrollViewBase.ScrollMode.BOTH) && this.mFooterLayout != null) {
                this.mFooterLayout.setWidth(maximumScrollOffset);
                i4 = i5;
                i3 = -maximumScrollOffset;
                i6 = paddingBottom;
                i2 = paddingTop;
            } else {
                i4 = i5;
                i3 = 0;
                i6 = paddingBottom;
                i2 = paddingTop;
            }
        } else {
            if ((this.mScrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_START || this.mScrollMode == TXScrollViewBase.ScrollMode.BOTH) && this.mHeaderLayout != null) {
                this.mHeaderLayout.setHeight(maximumScrollOffset);
                i = -maximumScrollOffset;
            } else {
                i = 0;
            }
            if ((this.mScrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_END || this.mScrollMode == TXScrollViewBase.ScrollMode.BOTH) && this.mFooterLayout != null) {
                this.mFooterLayout.setHeight(maximumScrollOffset);
                i6 = -maximumScrollOffset;
                i2 = i;
                i3 = paddingRight;
                i4 = paddingLeft;
            } else {
                i2 = i;
                i3 = paddingRight;
                i4 = paddingLeft;
            }
        }
        setPadding(i4, i2, i3, i6);
    }

    /* access modifiers changed from: protected */
    public final void addViewInternal(View view, int i, ViewGroup.LayoutParams layoutParams) {
        super.addViewInternal(view, i, layoutParams);
    }

    /* access modifiers changed from: protected */
    public void setState(RefreshState refreshState) {
        this.mRefreshState = refreshState;
        switch (r.f1214a[refreshState.ordinal()]) {
            case 1:
                onReset();
                return;
            case 2:
                onPullToRefresh();
                return;
            case 3:
                onReleaseToRefresh();
                return;
            case 4:
                onRefreshing();
                return;
            case 5:
                onLoadFinish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onReset() {
        this.mIsBeingDragged = false;
        this.mLayoutVisibilityChangesEnabled = true;
        if (this.mHeaderLayout != null) {
            this.mHeaderLayout.reset();
        }
        if (this.mFooterLayout != null) {
            this.mFooterLayout.reset();
        }
        smoothScrollTo(0);
    }

    /* access modifiers changed from: protected */
    public void onPullToRefresh() {
        if (this.mCurScrollState == TXScrollViewBase.ScrollState.ScrollState_FromStart && this.mHeaderLayout != null) {
            this.mHeaderLayout.setVisibility(0);
            this.mHeaderLayout.pullToRefresh();
        } else if (this.mCurScrollState == TXScrollViewBase.ScrollState.ScrollState_FromEnd && this.mFooterLayout != null) {
            this.mFooterLayout.setVisibility(0);
            this.mFooterLayout.pullToRefresh();
        }
    }

    /* access modifiers changed from: protected */
    public void onReleaseToRefresh() {
        if (this.mCurScrollState == TXScrollViewBase.ScrollState.ScrollState_FromStart && this.mHeaderLayout != null) {
            this.mHeaderLayout.releaseToRefresh();
        } else if (this.mCurScrollState == TXScrollViewBase.ScrollState.ScrollState_FromEnd && this.mFooterLayout != null) {
            this.mFooterLayout.releaseToRefresh();
        }
    }

    /* access modifiers changed from: protected */
    public void onRefreshing() {
        if ((this.mScrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_START || this.mScrollMode == TXScrollViewBase.ScrollMode.BOTH) && this.mHeaderLayout != null) {
            this.mHeaderLayout.refreshing();
        }
        if ((this.mScrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_END || this.mScrollMode == TXScrollViewBase.ScrollMode.BOTH) && this.mFooterLayout != null) {
            this.mFooterLayout.refreshing();
        }
        q qVar = new q(this);
        if (this.mCurScrollState == TXScrollViewBase.ScrollState.ScrollState_FromStart && this.mHeaderLayout != null) {
            smoothScrollTo(-this.mHeaderLayout.getContentSize(), qVar);
        } else if (this.mCurScrollState == TXScrollViewBase.ScrollState.ScrollState_FromEnd && this.mFooterLayout != null) {
            smoothScrollTo(this.mFooterLayout.getContentSize(), qVar);
        }
    }

    /* access modifiers changed from: protected */
    public void onLoadFinish() {
        onReset();
    }

    public void setRefreshListViewListener(ITXRefreshListViewListener iTXRefreshListViewListener) {
        this.mRefreshListViewListener = iTXRefreshListViewListener;
    }
}
