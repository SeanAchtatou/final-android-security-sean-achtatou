package com.tencent.assistant.component.appdetail;

import com.tencent.assistant.component.appdetail.AppdetailViewPager;

/* compiled from: ProGuard */
class u implements AppdetailViewPager.IHorizonScrollPicViewer {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HorizonScrollPicViewer f944a;

    u(HorizonScrollPicViewer horizonScrollPicViewer) {
        this.f944a = horizonScrollPicViewer;
    }

    public boolean canScrollToLeft() {
        return true;
    }

    public boolean canScrollToRight() {
        if (this.f944a.e != null) {
            return this.f944a.e.getScrollFlag();
        }
        return false;
    }

    public void scrollTo(int i) {
        if (this.f944a.e != null) {
            int scrollX = this.f944a.e.getScrollX();
            int scrollY = this.f944a.e.getScrollY();
            this.f944a.e.scrollTo(scrollX + i, scrollY);
        }
    }

    public void fling(int i) {
        if (this.f944a.e != null) {
            this.f944a.e.fling(i);
        }
    }
}
