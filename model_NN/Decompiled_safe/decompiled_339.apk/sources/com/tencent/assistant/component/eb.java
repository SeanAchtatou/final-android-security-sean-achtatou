package com.tencent.assistant.component;

import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.UpdateListView;
import com.tencent.assistant.manager.cd;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class eb extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ cd f1037a;
    final /* synthetic */ UpdateListView.UpdateAllType b;
    final /* synthetic */ ea c;

    eb(ea eaVar, cd cdVar, UpdateListView.UpdateAllType updateAllType) {
        this.c = eaVar;
        this.f1037a = cdVar;
        this.b = updateAllType;
    }

    public void onRightBtnClick() {
        TemporaryThreadManager.get().start(new ec(this));
        if (this.b == UpdateListView.UpdateAllType.NEEDSTARTDOWNLOAD) {
            Toast.makeText(this.c.f1036a.mContext, this.c.f1036a.mContext.getResources().getString(R.string.down_add_tips), 0).show();
        }
    }

    public void onLeftBtnClick() {
        String str;
        if (this.extraMsgView == null || this.c.f1036a.extraMsgViewCheckBox == null) {
            str = null;
        } else {
            str = this.c.f1036a.extraMsgViewCheckBox.isChecked() ? "1" : "0";
        }
        STInfoV2 sTInfoV2 = new STInfoV2(STConst.ST_PAGE_UPDATE_UPDATEALL, "03_002", STConst.ST_PAGE_UPDATE, STConst.ST_DEFAULT_SLOT, 200);
        sTInfoV2.status = str;
        sTInfoV2.pushInfo = this.c.f1036a.pushInfo;
        k.a(sTInfoV2);
    }

    public void onCancell() {
    }
}
