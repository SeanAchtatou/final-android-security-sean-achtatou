package com.tencent.assistant.component;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.adapter.ac;
import com.tencent.assistant.b.a;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.component.txscrollview.TXExpandableListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.s;
import com.tencent.assistant.module.u;
import com.tencent.assistant.module.update.k;
import com.tencent.assistant.module.wisedownload.r;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.protocol.jce.StatUpdateManageAction;
import com.tencent.assistant.sdk.param.entity.BatchDownloadParam;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STExternalInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class UpdateListView extends RelativeLayout implements s {
    private static final String TMA_ST_UPDATE_ALL_BTN_LEFT_TAG = "03_002";
    private static final String TMA_ST_UPDATE_ALL_BTN_RIGHT_TAG = "03_001";
    private static final String TMA_ST_UPDATE_ALL_BTN_TAG = "07_001";
    private static final String TMA_ST_UPDATE_LIST_IGNORE_TAG = "06_001";
    private final int INIT_DATA;
    private final int REFRESH_DATA;
    /* access modifiers changed from: private */
    public int callFrom;
    /* access modifiers changed from: private */
    public int expandCount;
    private STExternalInfo externalInfo;
    /* access modifiers changed from: private */
    public View extraMsgView;
    /* access modifiers changed from: private */
    public CheckBox extraMsgViewCheckBox;
    /* access modifiers changed from: private */
    public int groupExpandPostion;
    private String hostAppId;
    private String hostPackageName;
    private String hostVersionCode;
    private int indicatorGroupHeight;
    /* access modifiers changed from: private */
    public RelativeLayout layoutPopBar;
    private AstApp mApp;
    /* access modifiers changed from: private */
    public List<String> mAutoStartAppList;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public LayoutInflater mInflater;
    private View mRootView;
    private TextView mSaveSizeText;
    private List<BatchDownloadParam> mSpecifyAppList;
    private TextView mSumSizeText;
    private View mUpdateAllView;
    public ac mUpdateListAdapter;
    /* access modifiers changed from: private */
    public TXExpandableListView mUpdateListView;
    /* access modifiers changed from: private */
    public SimpleAppModel mobileManagerAppModelFromServer;
    private Handler myHandler;
    /* access modifiers changed from: private */
    public String pushInfo;
    private ListViewScrollListener scrollListener;
    private TextView textPopBar;
    private TextView textPopBarNum;
    private StatUpdateManageAction updateAction;
    private UpdateListFooterView updateListFooterView;
    private View updateLy;
    /* access modifiers changed from: private */
    public int userMode;
    private Map<Long, Integer> wantSortAheadAppAndPositionMap;

    /* compiled from: ProGuard */
    enum UpdateAllType {
        ALLDOWNLOADING,
        ALLUPDATED,
        NEEDUPDATE,
        NEEDSTARTDOWNLOAD
    }

    public UpdateListView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, null);
    }

    public UpdateListView(Context context, StatUpdateManageAction statUpdateManageAction) {
        this(context, null, statUpdateManageAction);
    }

    public UpdateListView(Context context, AttributeSet attributeSet, StatUpdateManageAction statUpdateManageAction) {
        super(context, attributeSet);
        this.layoutPopBar = null;
        this.textPopBar = null;
        this.textPopBarNum = null;
        this.extraMsgView = null;
        this.extraMsgViewCheckBox = null;
        this.mobileManagerAppModelFromServer = null;
        this.expandCount = 0;
        this.groupExpandPostion = -1;
        this.INIT_DATA = 1;
        this.REFRESH_DATA = 2;
        this.hostAppId = Constants.STR_EMPTY;
        this.hostPackageName = Constants.STR_EMPTY;
        this.hostVersionCode = Constants.STR_EMPTY;
        this.userMode = 0;
        this.callFrom = 0;
        this.pushInfo = Constants.STR_EMPTY;
        this.mSpecifyAppList = null;
        this.mAutoStartAppList = null;
        this.wantSortAheadAppAndPositionMap = null;
        this.scrollListener = new ed(this);
        this.myHandler = new ee(this);
        this.updateAction = statUpdateManageAction;
        this.userMode = 0;
        this.callFrom = 0;
        this.mApp = AstApp.i();
        this.mContext = context;
        this.mApp.j();
        this.mInflater = (LayoutInflater) this.mContext.getSystemService("layout_inflater");
        initView();
    }

    private void initView() {
        this.mRootView = this.mInflater.inflate((int) R.layout.updatelist_component, this);
        this.mUpdateListView = (TXExpandableListView) this.mRootView.findViewById(R.id.updatelist);
        this.mUpdateListView.setGroupIndicator(null);
        this.mUpdateListView.setDivider(null);
        this.mUpdateListView.setChildDivider(null);
        this.mUpdateListView.setSelector(R.drawable.transparent_selector);
        this.mUpdateListView.setOnScrollListener(this.scrollListener);
        this.mUpdateListView.setOnGroupClickListener(new dx(this));
        this.updateListFooterView = new UpdateListFooterView(this.mContext, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, TXScrollViewBase.ScrollMode.NONE);
        this.updateListFooterView.setFooterViewOnclickListner(new dy(this));
        this.mUpdateListView.addFooterView(this.updateListFooterView);
        this.mUpdateListAdapter = new ac(this.mContext, this.mUpdateListView, this.updateAction);
        this.mUpdateListView.setAdapter(this.mUpdateListAdapter);
        this.layoutPopBar = (RelativeLayout) findViewById(R.id.pop_bar);
        this.textPopBar = (TextView) findViewById(R.id.group_title);
        this.textPopBarNum = (TextView) findViewById(R.id.group_title_num);
        this.layoutPopBar.setOnClickListener(new dz(this));
        this.updateLy = this.mRootView.findViewById(R.id.btnlayout);
        this.mUpdateAllView = this.mRootView.findViewById(R.id.sizeLayout);
        this.mSumSizeText = (TextView) this.mRootView.findViewById(R.id.update_sumsize);
        this.mSaveSizeText = (TextView) this.mRootView.findViewById(R.id.update_savesize);
        this.mUpdateAllView.setOnClickListener(new ea(this));
        updateSizeLayout();
    }

    /* access modifiers changed from: private */
    public void freshPopBar() {
        long j;
        String[] a2;
        int pointToPosition = this.mUpdateListView.pointToPosition(0, getShowHeight() - 10);
        int pointToPosition2 = this.mUpdateListView.pointToPosition(0, 0);
        if (pointToPosition2 != -1) {
            long expandableListPosition = this.mUpdateListView.getExpandableListPosition(pointToPosition2);
            int packedPositionChild = ExpandableListView.getPackedPositionChild(expandableListPosition);
            int packedPositionGroup = ExpandableListView.getPackedPositionGroup(expandableListPosition);
            if (packedPositionChild == -1) {
                View expandChildAt = this.mUpdateListView.getExpandChildAt(pointToPosition2 - this.mUpdateListView.getFirstVisiblePosition());
                if (expandChildAt == null) {
                    this.indicatorGroupHeight = 100;
                } else {
                    this.indicatorGroupHeight = expandChildAt.getHeight();
                }
            }
            if (this.indicatorGroupHeight != 0) {
                if (this.expandCount > 0) {
                    this.groupExpandPostion = packedPositionGroup;
                    Integer num = (Integer) this.mUpdateListAdapter.getGroup(this.groupExpandPostion);
                    if (!(num == null || (a2 = this.mUpdateListAdapter.a(num.intValue())) == null)) {
                        this.textPopBar.setText(a2[0]);
                        this.textPopBarNum.setText(" " + a2[1]);
                        this.layoutPopBar.setVisibility(0);
                    }
                }
                if (this.expandCount == 0) {
                    this.layoutPopBar.setVisibility(8);
                }
                j = expandableListPosition;
            } else {
                return;
            }
        } else {
            j = 0;
        }
        if (this.groupExpandPostion == -1) {
            return;
        }
        if (j == 0 && pointToPosition == 0) {
            this.layoutPopBar.setVisibility(8);
            return;
        }
        int showHeight = getShowHeight();
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.layoutPopBar.getLayoutParams();
        marginLayoutParams.topMargin = -(this.indicatorGroupHeight - showHeight);
        this.layoutPopBar.setLayoutParams(marginLayoutParams);
    }

    private int getShowHeight() {
        int i = this.indicatorGroupHeight;
        int pointToPosition = this.mUpdateListView.pointToPosition(0, this.indicatorGroupHeight);
        if (pointToPosition == -1 || ExpandableListView.getPackedPositionGroup(this.mUpdateListView.getExpandableListPosition(pointToPosition)) == this.groupExpandPostion) {
            return i;
        }
        View expandChildAt = this.mUpdateListView.getExpandChildAt(pointToPosition - this.mUpdateListView.getFirstVisiblePosition());
        if (expandChildAt != null) {
            return expandChildAt.getTop();
        }
        return 100;
    }

    public void notifyChange() {
        if (this.mUpdateListAdapter != null) {
            this.mUpdateListAdapter.notifyDataSetChanged();
        }
    }

    private void initData(String str) {
        this.mUpdateListAdapter.a(this.wantSortAheadAppAndPositionMap);
        Message obtainMessage = this.myHandler.obtainMessage();
        obtainMessage.what = 1;
        this.myHandler.removeMessages(1);
        this.myHandler.sendMessage(obtainMessage);
    }

    public void initData(String str, Intent intent) {
        String[] b;
        if (intent != null) {
            this.hostAppId = intent.getStringExtra(a.m);
            this.hostPackageName = intent.getStringExtra(a.n);
            this.hostVersionCode = intent.getStringExtra(a.o);
            this.userMode = intent.getIntExtra(a.S, 0);
            this.callFrom = intent.getIntExtra(a.T, 0);
            Bundle extras = intent.getExtras();
            if (extras != null && extras.containsKey(a.Q)) {
                this.mSpecifyAppList = extras.getParcelableArrayList(a.Q);
            }
            if (extras != null && extras.containsKey(a.R)) {
                this.mAutoStartAppList = extras.getStringArrayList(a.R);
            }
            String stringExtra = intent.getStringExtra("sort_list");
            if (!TextUtils.isEmpty(stringExtra) && (b = ct.b(stringExtra, ",")) != null && b.length > 0) {
                this.wantSortAheadAppAndPositionMap = new HashMap(b.length);
                for (int i = 0; i < b.length; i++) {
                    this.wantSortAheadAppAndPositionMap.put(Long.valueOf(ct.c(b[i])), Integer.valueOf(i));
                }
            }
        }
        initData(str);
    }

    public void initSTParam(STExternalInfo sTExternalInfo, String str) {
        this.pushInfo = str;
        this.externalInfo = sTExternalInfo;
        if (this.mUpdateListAdapter != null) {
            this.mUpdateListAdapter.a(sTExternalInfo, str);
        }
    }

    public void refreshData() {
        Message obtainMessage = this.myHandler.obtainMessage();
        obtainMessage.what = 2;
        this.myHandler.removeMessages(2);
        this.myHandler.sendMessage(obtainMessage);
    }

    /* access modifiers changed from: private */
    public void updateIgnoreListNum() {
        int i = u.i();
        int b = u.b(this.mUpdateListAdapter.b);
        this.updateListFooterView.freshState(b > 0 ? i > 0 ? 1 : 2 : 3);
        if (b > 0) {
            this.updateListFooterView.setFooterViewText(String.format(getResources().getString(R.string.view_ignorelist), Integer.valueOf(k.b().e().size())));
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00aa A[EDGE_INSN: B:37:0x00aa->B:31:0x00aa ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void updateSizeLayout() {
        /*
            r7 = this;
            r1 = 1
            r2 = 0
            r6 = 8
            com.tencent.assistant.adapter.ac r0 = r7.mUpdateListAdapter
            boolean r0 = r0.b
            java.util.List r3 = com.tencent.assistant.module.u.a(r0)
            if (r3 == 0) goto L_0x00aa
            int r0 = r3.size()
            if (r0 <= 0) goto L_0x00aa
            java.util.Iterator r4 = r3.iterator()
        L_0x0018:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x00aa
            java.lang.Object r0 = r4.next()
            com.tencent.assistant.model.SimpleAppModel r0 = (com.tencent.assistant.model.SimpleAppModel) r0
            com.tencent.assistant.AppConst$AppState r0 = com.tencent.assistant.module.u.d(r0)
            com.tencent.assistant.AppConst$AppState r5 = com.tencent.assistant.AppConst.AppState.DOWNLOAD
            if (r0 == r5) goto L_0x0040
            com.tencent.assistant.AppConst$AppState r5 = com.tencent.assistant.AppConst.AppState.DOWNLOADING
            if (r0 == r5) goto L_0x0040
            com.tencent.assistant.AppConst$AppState r5 = com.tencent.assistant.AppConst.AppState.UPDATE
            if (r0 == r5) goto L_0x0040
            com.tencent.assistant.AppConst$AppState r5 = com.tencent.assistant.AppConst.AppState.PAUSED
            if (r0 == r5) goto L_0x0040
            com.tencent.assistant.AppConst$AppState r5 = com.tencent.assistant.AppConst.AppState.FAIL
            if (r0 == r5) goto L_0x0040
            com.tencent.assistant.AppConst$AppState r5 = com.tencent.assistant.AppConst.AppState.QUEUING
            if (r0 != r5) goto L_0x0018
        L_0x0040:
            r0 = r1
        L_0x0041:
            if (r3 == 0) goto L_0x0095
            int r4 = r3.size()
            if (r4 <= 0) goto L_0x0095
            java.lang.String[] r0 = r7.getSumSize(r3, r0)
            if (r0 == 0) goto L_0x0080
            android.view.View r3 = r7.updateLy
            r3.setVisibility(r2)
            android.view.View r3 = r7.mUpdateAllView
            r3.setVisibility(r2)
            android.widget.TextView r3 = r7.mSumSizeText
            r3.setVisibility(r2)
            android.widget.TextView r3 = r7.mSumSizeText
            r4 = r0[r2]
            r3.setText(r4)
            r3 = r0[r1]
            boolean r3 = android.text.TextUtils.isEmpty(r3)
            if (r3 != 0) goto L_0x007a
            android.widget.TextView r3 = r7.mSaveSizeText
            r3.setVisibility(r2)
            android.widget.TextView r2 = r7.mSaveSizeText
            r0 = r0[r1]
            r2.setText(r0)
        L_0x0079:
            return
        L_0x007a:
            android.widget.TextView r0 = r7.mSaveSizeText
            r0.setVisibility(r6)
            goto L_0x0079
        L_0x0080:
            android.view.View r0 = r7.updateLy
            r0.setVisibility(r6)
            android.view.View r0 = r7.mUpdateAllView
            r0.setVisibility(r6)
            android.widget.TextView r0 = r7.mSumSizeText
            r0.setVisibility(r6)
            android.widget.TextView r0 = r7.mSaveSizeText
            r0.setVisibility(r6)
            goto L_0x0079
        L_0x0095:
            android.view.View r0 = r7.updateLy
            r0.setVisibility(r6)
            android.widget.TextView r0 = r7.mSumSizeText
            r0.setVisibility(r6)
            android.widget.TextView r0 = r7.mSaveSizeText
            r0.setVisibility(r6)
            android.view.View r0 = r7.mUpdateAllView
            r0.setVisibility(r6)
            goto L_0x0079
        L_0x00aa:
            r0 = r2
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.component.UpdateListView.updateSizeLayout():void");
    }

    private String[] getSumSize(List<SimpleAppModel> list, boolean z) {
        String[] strArr = new String[2];
        if (!z) {
            strArr[0] = Constants.STR_EMPTY;
            return strArr;
        } else if (list == null || list.size() <= 0) {
            return null;
        } else {
            long j = 0;
            long j2 = 0;
            for (SimpleAppModel next : list) {
                j += next.k;
                if (next.a()) {
                    if (!r.b(next)) {
                        j2 += next.v;
                    }
                } else if (!r.b(next) && !r.a(next)) {
                    j2 += next.k;
                }
                j2 = j2;
            }
            long j3 = j - j2;
            if (j3 > 10240) {
                strArr[0] = bt.a(j2, 0);
                strArr[1] = " (" + getResources().getString(R.string.totalsave) + bt.a(j3, 0) + ")";
            } else {
                strArr[0] = " (" + bt.a(j2) + ")";
                strArr[1] = Constants.STR_EMPTY;
            }
            return strArr;
        }
    }

    /* access modifiers changed from: private */
    public UpdateAllType getUpdateAllType() {
        List<SimpleAppModel> c = this.mUpdateListAdapter.c();
        int size = c.size();
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        for (SimpleAppModel d : c) {
            AppConst.AppState d2 = u.d(d);
            if (d2 == AppConst.AppState.DOWNLOADING || d2 == AppConst.AppState.QUEUING) {
                i2++;
            } else if (d2 == AppConst.AppState.INSTALLED) {
                i++;
            } else if (d2 == AppConst.AppState.UPDATE || d2 == AppConst.AppState.DOWNLOAD) {
                i3++;
            }
            i3 = i3;
            i2 = i2;
            i = i;
        }
        if (i == size) {
            return UpdateAllType.ALLUPDATED;
        }
        if (i2 + i >= size) {
            return UpdateAllType.ALLDOWNLOADING;
        }
        if (i3 > 0) {
            return UpdateAllType.NEEDSTARTDOWNLOAD;
        }
        return UpdateAllType.NEEDUPDATE;
    }

    public int getUpdateListPageDataSize() {
        List<SimpleAppModel> c;
        if (this.mUpdateListAdapter == null || (c = this.mUpdateListAdapter.c()) == null) {
            return 0;
        }
        return c.size();
    }

    public void setActivity(Activity activity) {
        if (this.mUpdateListAdapter != null) {
            this.mUpdateListAdapter.a(activity);
        }
    }

    /* access modifiers changed from: private */
    public void updateAll() {
        updateAppList(getUpdateModelList());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.download.a.a(java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>, boolean):void
     arg types: [java.util.ArrayList, int]
     candidates:
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.a, java.util.List):java.util.List
      com.tencent.assistant.download.a.a(long, long):boolean
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.a, boolean):boolean
      com.tencent.assistant.download.a.a(java.lang.String, int):boolean
      com.tencent.assistant.download.a.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, com.tencent.assistantv2.st.model.StatInfo):int
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.download.SimpleDownloadInfo$UIType):void
      com.tencent.assistant.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo):void
      com.tencent.assistant.download.a.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, boolean):boolean
      com.tencent.assistant.download.a.a(java.lang.String, boolean):boolean
      com.tencent.assistant.download.a.a(java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>, boolean):void */
    public void updateAppList(List<SimpleAppModel> list) {
        if (list != null && list.size() != 0) {
            this.updateAction.b = true;
            ArrayList arrayList = new ArrayList();
            StatInfo a2 = com.tencent.assistantv2.st.page.a.a(buildUpdateStatInfo());
            for (SimpleAppModel next : list) {
                if (!TextUtils.isEmpty(next.q())) {
                    DownloadInfo a3 = DownloadProxy.a().a(next);
                    a2.recommendId = next.y;
                    if (a3 != null && a3.needReCreateInfo(next)) {
                        DownloadProxy.a().b(a3.downloadTicket);
                        a3 = null;
                    }
                    if (this.callFrom == 1 && a3 != null) {
                        a3.hostAppId = this.hostAppId;
                        a3.hostPackageName = this.hostPackageName;
                        a3.hostVersionCode = this.hostVersionCode;
                    }
                    if (a3 == null) {
                        a3 = DownloadInfo.createDownloadInfo(next, a2);
                        a3.isUpdate = 1;
                    } else {
                        a3.updateDownloadInfoStatInfo(a2);
                    }
                    if (u.d(next) != AppConst.AppState.DOWNLOADED || !a3.isSuccApkFileExist()) {
                        com.tencent.assistant.download.a.a().a(a3);
                    } else {
                        arrayList.add(a3);
                    }
                    this.mUpdateListAdapter.c.put(next.c.hashCode(), next.g);
                }
            }
            if (arrayList.size() > 0) {
                com.tencent.assistant.download.a.a((ArrayList<DownloadInfo>) arrayList, false);
            }
        }
    }

    /* access modifiers changed from: private */
    public void expandAllListView() {
        for (int i = 0; i < this.mUpdateListAdapter.getGroupCount(); i++) {
            this.mUpdateListView.expandGroup(i);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return true;
    }

    public void onGetAppInfoSuccess(int i, int i2, AppSimpleDetail appSimpleDetail) {
        if (appSimpleDetail != null) {
            this.mobileManagerAppModelFromServer = u.a(appSimpleDetail);
            downloadTencentMobileManager();
            return;
        }
        Toast.makeText(AstApp.i(), this.mContext.getString(R.string.rec_create_fail), 0).show();
    }

    public void onGetAppInfoFail(int i, int i2) {
        XLog.w("million", getClass().getSimpleName() + " onGetAppInfoFail...." + i2);
        Toast.makeText(AstApp.i(), this.mContext.getString(R.string.rec_create_fail), 0).show();
    }

    /* access modifiers changed from: private */
    public void downloadTencentMobileManager() {
        DownloadInfo downloadInfo;
        DownloadInfo downloadInfo2;
        if (this.mobileManagerAppModelFromServer != null) {
            DownloadInfo a2 = DownloadProxy.a().a(this.mobileManagerAppModelFromServer);
            StatInfo statInfo = new StatInfo(this.mobileManagerAppModelFromServer.b, STConst.ST_PAGE_UPDATE_UPDATEALL, 0, null, 0);
            if (a2 == null || !a2.needReCreateInfo(this.mobileManagerAppModelFromServer)) {
                downloadInfo = a2;
            } else {
                DownloadProxy.a().b(a2.downloadTicket);
                downloadInfo = null;
            }
            if (downloadInfo == null) {
                downloadInfo2 = DownloadInfo.createDownloadInfo(this.mobileManagerAppModelFromServer, statInfo);
            } else {
                downloadInfo.updateDownloadInfoStatInfo(statInfo);
                downloadInfo2 = downloadInfo;
            }
            downloadInfo2.autoInstall = false;
            switch (eg.f1042a[u.d(this.mobileManagerAppModelFromServer).ordinal()]) {
                case 1:
                case 2:
                case 3:
                case 4:
                    com.tencent.assistant.download.a.a().a(downloadInfo2);
                    break;
                case 5:
                    com.tencent.assistant.download.a.a().b(downloadInfo2);
                    break;
                case 6:
                    com.tencent.assistant.download.a.a().d(downloadInfo2);
                    break;
            }
            XLog.d("million", "UpdateListView after updateall download TencentMobileManager sucess");
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private List<SimpleAppModel> getUpdateModelList() {
        switch (this.userMode) {
            case 0:
                break;
            case 1:
                u.a(this.mUpdateListAdapter.b);
                break;
            case 2:
                return u.i(this.mSpecifyAppList);
            default:
                return u.a(this.mUpdateListAdapter.b);
        }
        return u.a(this.mUpdateListAdapter.b);
    }

    public List<SimpleAppModel> getUncheckSimpleAppModels() {
        int i;
        ArrayList arrayList = new ArrayList();
        if (this.callFrom == 1 && (this.userMode == 1 || this.userMode == 0)) {
            return arrayList;
        }
        if (this.callFrom == 0) {
            return arrayList;
        }
        if (this.mSpecifyAppList == null || this.mSpecifyAppList.size() <= 0) {
            return arrayList;
        }
        List<SimpleAppModel> updateModelList = getUpdateModelList();
        if (!(updateModelList == null || u.f() == null)) {
            updateModelList.addAll(u.f());
        }
        if (updateModelList == null || updateModelList.size() <= 0) {
            for (BatchDownloadParam next : this.mSpecifyAppList) {
                SimpleAppModel simpleAppModel = new SimpleAppModel();
                try {
                    simpleAppModel.f1634a = Long.valueOf(next.b).longValue();
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                simpleAppModel.c = next.d;
                simpleAppModel.D = Integer.valueOf(next.c).intValue();
                arrayList.add(simpleAppModel);
            }
            return arrayList;
        }
        for (BatchDownloadParam next2 : this.mSpecifyAppList) {
            if (next2 != null && !TextUtils.isEmpty(next2.d)) {
                int i2 = 0;
                while (true) {
                    i = i2;
                    if (i < updateModelList.size() && (updateModelList.get(i) == null || !next2.d.equals(updateModelList.get(i).c))) {
                        i2 = i + 1;
                    }
                }
                if (i >= updateModelList.size()) {
                    SimpleAppModel simpleAppModel2 = new SimpleAppModel();
                    try {
                        simpleAppModel2.f1634a = Long.valueOf(next2.b).longValue();
                        simpleAppModel2.c = next2.d;
                        simpleAppModel2.D = Integer.valueOf(next2.c).intValue();
                        simpleAppModel2.ac = next2.h;
                    } catch (NumberFormatException e2) {
                        e2.printStackTrace();
                    }
                    arrayList.add(simpleAppModel2);
                }
            }
        }
        return arrayList;
    }

    public STInfoV2 buildUpdateStatInfo() {
        SimpleAppModel simpleAppModel;
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.mContext, 200);
        if (buildSTInfo != null) {
            buildSTInfo.pushInfo = this.pushInfo;
            buildSTInfo.updateWithExternalPara(this.externalInfo);
            List<SimpleAppModel> a2 = u.a(this.mUpdateListAdapter.b);
            if (!(a2 == null || a2.size() <= 0 || (simpleAppModel = a2.get(0)) == null)) {
                buildSTInfo.recommendId = simpleAppModel.y;
            }
        }
        return buildSTInfo;
    }

    public void setGuanJiaStyle() {
        this.mUpdateListAdapter.d();
        this.mUpdateAllView.setBackgroundResource(R.drawable.guanjia_style_btn_backround_big_selector);
    }

    public void setNormalStyle() {
        this.mUpdateListAdapter.e();
        this.mUpdateAllView.setBackgroundResource(R.drawable.btn_green_selector);
    }
}
