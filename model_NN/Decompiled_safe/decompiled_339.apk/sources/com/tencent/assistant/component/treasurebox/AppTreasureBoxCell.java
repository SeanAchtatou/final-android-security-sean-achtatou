package com.tencent.assistant.component.treasurebox;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import com.tencent.assistant.model.s;

/* compiled from: ProGuard */
public class AppTreasureBoxCell extends RelativeLayout implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f1175a = false;
    protected int b = 0;
    protected AppTreasureBoxItemClickListener c;

    /* compiled from: ProGuard */
    public interface AppTreasureBoxItemClickListener {
        void onActionBtnClick(int i, int i2);

        void onAppExposure(int i);

        void onCloseBtnClick();

        void onTouchOpenedPosition(int i);
    }

    public AppTreasureBoxCell(Context context) {
        super(context);
    }

    public AppTreasureBoxCell(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void onClick(View view) {
    }

    public void onLeaveScreen() {
    }

    public void onGoinScreen() {
    }

    public void setGetPosListener(AppTreasureBoxItemClickListener appTreasureBoxItemClickListener) {
        this.c = appTreasureBoxItemClickListener;
    }

    public void onTouchCellOpen() {
        if (this.c != null) {
            this.c.onTouchOpenedPosition(this.b);
        }
    }

    public void onAppExposure(int i) {
        if (this.c != null) {
            this.c.onAppExposure(this.b);
        }
    }

    public void setPosition(int i) {
        this.b = i;
    }

    public int getPosition() {
        return this.b;
    }

    public void refreshData(s sVar) {
    }

    public static AppTreasureBoxCell createCell(boolean z, s sVar, Context context, AppTreasureBoxItemClickListener appTreasureBoxItemClickListener, int i) {
        AppTreasureBoxCell appTreasureBoxCell;
        if (z) {
            appTreasureBoxCell = new AppTreasureBoxCell1(context);
            if (sVar == null) {
                return null;
            }
        } else {
            appTreasureBoxCell = new AppTreasureBoxCell2(context);
        }
        appTreasureBoxCell.setPosition(i);
        if (appTreasureBoxCell != null) {
            appTreasureBoxCell.refreshData(sVar);
        }
        if (appTreasureBoxItemClickListener != null) {
            appTreasureBoxCell.setGetPosListener(appTreasureBoxItemClickListener);
        }
        appTreasureBoxCell.onAppExposure(i);
        return appTreasureBoxCell;
    }
}
