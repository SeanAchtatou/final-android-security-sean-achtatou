package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.model.a.i;
import com.tencent.assistantv2.st.b.d;

/* compiled from: ProGuard */
public abstract class ISmartcard extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    protected Context f1115a;
    protected LayoutInflater b;
    protected View c;
    protected IViewInvalidater d;
    protected d e = null;
    public boolean hasInit = false;
    public SmartcardListener smartcardListener = null;
    public i smartcardModel = null;

    /* access modifiers changed from: protected */
    public abstract void a();

    /* access modifiers changed from: protected */
    public abstract void b();

    public ISmartcard(Context context) {
        super(context);
    }

    public ISmartcard(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ISmartcard(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public ISmartcard(Context context, i iVar, SmartcardListener smartcardListener2, IViewInvalidater iViewInvalidater) {
        super(context, null);
        this.smartcardModel = iVar;
        this.f1115a = context;
        this.smartcardListener = smartcardListener2;
        this.b = (LayoutInflater) context.getSystemService("layout_inflater");
        setWillNotDraw(false);
        try {
            setBackgroundResource(R.drawable.bg_card_selector_padding);
        } catch (Throwable th) {
        }
        this.d = iViewInvalidater;
    }

    public void resetSmartcard(i iVar) {
        this.smartcardModel = iVar;
        b();
    }

    public void setSmartcardListener(SmartcardListener smartcardListener2) {
        this.smartcardListener = smartcardListener2;
    }
}
