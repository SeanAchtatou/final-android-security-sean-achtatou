package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.m;
import com.tencent.assistant.model.d;
import com.tencent.assistant.module.u;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class NormalSmartCardPlayerSHowItem extends NormalSmartcardBaseItem {
    private TXAppIconView f;
    private DownloadButton i;
    private TextView j;
    private ListItemInfoView k;
    private TextView l;
    private RelativeLayout m;
    private TXImageView n;

    public NormalSmartCardPlayerSHowItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartCardPlayerSHowItem(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        super(context, iVar, smartcardListener, iViewInvalidater);
    }

    public NormalSmartCardPlayerSHowItem(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b.inflate((int) R.layout.smartcard_playershow, this);
        this.f = (TXAppIconView) findViewById(R.id.app_icon_img);
        this.i = (DownloadButton) findViewById(R.id.state_app_btn);
        this.j = (TextView) findViewById(R.id.title);
        this.k = (ListItemInfoView) findViewById(R.id.download_info);
        this.l = (TextView) findViewById(R.id.desc);
        this.m = (RelativeLayout) findViewById(R.id.video_img_layout);
        this.n = (TXImageView) findViewById(R.id.video_img);
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    private void f() {
        m mVar;
        if (this.smartcardModel instanceof m) {
            mVar = (m) this.smartcardModel;
        } else {
            mVar = null;
        }
        if (mVar != null && mVar.d != null) {
            d e = u.e(mVar.d);
            this.f.updateImageView(mVar.d.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.j.setText(mVar.d.d);
            if (1 == (((int) (mVar.d.B >> 2)) & 3)) {
                Drawable drawable = this.f1115a.getResources().getDrawable(R.drawable.appdownload_icon_original);
                drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                this.j.setCompoundDrawablePadding(df.b(6.0f));
                this.j.setCompoundDrawables(null, null, drawable, null);
            } else {
                this.j.setCompoundDrawables(null, null, null, null);
            }
            this.k.a(mVar.d, e);
            this.i.a(mVar.d, e);
            STInfoV2 a2 = a("03_001", 200);
            if (a2 != null) {
                a2.updateWithSimpleAppModel(mVar.d);
            }
            if (s.a(mVar.d)) {
                this.i.setClickable(false);
            } else {
                this.i.setClickable(true);
                this.i.a(a2);
            }
            this.l.setText(mVar.d.X);
            this.n.updateImageView(mVar.f1646a, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
            setOnClickListener(new e(this, mVar, a2));
            this.m.setOnClickListener(new f(this, mVar, a2));
        }
    }

    /* access modifiers changed from: protected */
    public SimpleAppModel c() {
        if (this.smartcardModel instanceof m) {
            return ((m) this.smartcardModel).d;
        }
        return super.c();
    }
}
