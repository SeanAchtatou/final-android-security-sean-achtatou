package com.tencent.assistant.component.appdetail;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.appdetail.AppdetailViewPager;
import com.tencent.assistant.component.appdetail.HorizonImageListView;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.ApkInfo;
import com.tencent.assistant.protocol.jce.SnapshotsPic;
import com.tencent.assistant.protocol.jce.Video;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class HorizonScrollPicViewer extends LinearLayout {
    public static final String VIDEO_PLAY_SLOT_ID = "16";
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f908a;
    private LayoutInflater b;
    /* access modifiers changed from: private */
    public boolean c;
    private int d;
    /* access modifiers changed from: private */
    public HorizonImageListView e;
    /* access modifiers changed from: private */
    public List<SnapshotsPic> f = new ArrayList();
    /* access modifiers changed from: private */
    public List<Video> g = new ArrayList();
    private IPicViewerListener h;
    /* access modifiers changed from: private */
    public IShowPictureListener i;
    private HorizonImageListView.IHorizonImageListViewListener j = new s(this);
    private float k;
    private AppdetailViewPager.IHorizonScrollPicViewer l = new u(this);

    /* compiled from: ProGuard */
    public interface IPicViewerListener {
        void onTouchPicViewer();

        void requestEventHandle(boolean z);
    }

    /* compiled from: ProGuard */
    public interface IShowPictureListener {
        void showPicture(boolean z);
    }

    public HorizonScrollPicViewer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f908a = context;
        this.b = LayoutInflater.from(context);
        b();
    }

    public HorizonScrollPicViewer(Context context) {
        super(context);
        this.f908a = context;
        this.b = LayoutInflater.from(context);
        b();
    }

    public void refreshData(ApkInfo apkInfo) {
        if (apkInfo != null && this.f.size() <= 0) {
            ArrayList<SnapshotsPic> arrayList = apkInfo.g;
            ArrayList<Video> arrayList2 = apkInfo.t;
            if (arrayList != null || arrayList2 != null) {
                if (arrayList2 != null) {
                    for (Video next : arrayList2) {
                        next.b += "&url=" + next.b;
                    }
                    this.g.clear();
                    this.g.addAll(arrayList2);
                }
                if (arrayList != null) {
                    this.f.clear();
                    this.f.addAll(arrayList);
                }
                a();
                requestLayout();
            }
        }
    }

    private void a() {
        ArrayList arrayList = new ArrayList();
        for (Video next : this.g) {
            if (!TextUtils.isEmpty(next.f2420a)) {
                arrayList.add(next.f2420a);
            }
        }
        for (SnapshotsPic next2 : this.f) {
            if (this.c) {
                arrayList.add(next2.b);
            } else {
                arrayList.add(next2.c);
            }
        }
        if (arrayList == null || arrayList.size() == 0) {
            this.e.setVisibility(8);
            return;
        }
        try {
            this.e.setImageView(this.g.size(), arrayList, (int) getResources().getDimension(R.dimen.app_detail_pic_gap), R.drawable.pic_default_big);
        } catch (Throwable th) {
            cq.a().b();
        }
        if (this.g.size() > 0 && (this.f908a instanceof AppDetailActivityV5)) {
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f908a, 100);
            buildSTInfo.slotId = a.a("16", 0);
            k.a(buildSTInfo);
        }
    }

    private void b() {
        if (c.d() || c.f()) {
            this.c = true;
            this.d = (int) this.f908a.getResources().getDimension(R.dimen.app_detail_pic_height_wifi3g);
        } else {
            this.c = false;
            this.d = (int) this.f908a.getResources().getDimension(R.dimen.app_detail_pic_height_wifi3g);
        }
        this.e = (HorizonImageListView) this.b.inflate((int) R.layout.horizon_image_scroll_listview, this).findViewById(R.id.imageListView);
        ((RelativeLayout.LayoutParams) this.e.getLayoutParams()).height = this.d;
        this.e.setPicHeight(this.d);
        this.e.setlistener(this.j);
    }

    public void onResume() {
        if (this.e != null) {
            this.e.onResume();
        }
    }

    public void onPause() {
        if (this.e != null) {
            this.e.onPause();
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                if (this.h != null && this.e.canScroll()) {
                    this.h.onTouchPicViewer();
                }
                this.k = motionEvent.getX();
                break;
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return super.onTouchEvent(motionEvent);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        return super.dispatchTouchEvent(motionEvent);
    }

    public void setPicViewerListener(IPicViewerListener iPicViewerListener) {
        this.h = iPicViewerListener;
        this.e.setPicViewerListener(iPicViewerListener);
    }

    public void setShowPictureListener(IShowPictureListener iShowPictureListener) {
        this.i = iShowPictureListener;
    }

    public AppdetailViewPager.IHorizonScrollPicViewer getScrollPicViewerListener() {
        return this.l;
    }
}
