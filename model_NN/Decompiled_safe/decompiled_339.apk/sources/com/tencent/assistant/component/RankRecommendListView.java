package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.HeaderViewListAdapter;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.cw;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXExpandableListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.module.callback.q;
import com.tencent.assistant.module.de;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
public class RankRecommendListView extends TXExpandableListView implements ITXRefreshListViewListener {
    private final String KEY_APPID = "key_Appid";
    private final String KEY_DATA = "key_data";
    private final String KEY_HASNEST = "hasNext";
    private final String KEY_ISFIRSTPAGE = "isFirstPage";
    private final int MSG_LOAD_LOCAL_APPLIST = 2;
    private final int MSG_REFRESH_APPLIST = 1;
    /* access modifiers changed from: private */
    public de engine = null;
    q getGroupAppsCallback = new cq(this);
    /* access modifiers changed from: private */
    public cw mAdapter = null;
    /* access modifiers changed from: private */
    public AppListRefreshListener mListener;
    /* access modifiers changed from: private */
    public ViewInvalidateMessageHandler pageMessageHandler = new ct(this);
    private int rankRecommendTopGroupId = 0;
    private int retryCount = 1;
    /* access modifiers changed from: private */
    public ViewPageScrollListener viewPagerlistener;

    /* compiled from: ProGuard */
    public interface AppListRefreshListener {
        void onErrorHappened(int i);

        void onNetworkLoading();

        void onNetworkNoError();

        void onNextPageLoadFailed();
    }

    public void setViewPageListener(ViewPageScrollListener viewPageScrollListener) {
        this.viewPagerlistener = viewPageScrollListener;
    }

    public void registerRefreshListener(AppListRefreshListener appListRefreshListener) {
        this.mListener = appListRefreshListener;
    }

    public void setEngine(de deVar) {
        this.engine = deVar;
        this.engine.register(this.getGroupAppsCallback);
        setRefreshListViewListener(this);
    }

    public RankRecommendListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setGroupIndicator(null);
        setDivider(null);
        setChildDivider(null);
        setSelector(R.drawable.transparent_selector);
        setRefreshListViewListener(this);
        setOnGroupClickListener(new cr(this));
        initAdapter();
    }

    public ExpandableListView getListView() {
        return (ExpandableListView) this.mScrollContentView;
    }

    private void initAdapter() {
        ExpandableListAdapter expandableListAdapter = ((ExpandableListView) this.mScrollContentView).getExpandableListAdapter();
        if (expandableListAdapter instanceof HeaderViewListAdapter) {
            this.mAdapter = (cw) ((HeaderViewListAdapter) expandableListAdapter).getWrappedAdapter();
        } else {
            this.mAdapter = (cw) ((ExpandableListView) this.mScrollContentView).getExpandableListAdapter();
        }
        if (this.mAdapter == null) {
        }
    }

    public void loadFirstPage(int i) {
        if (this.mAdapter == null) {
            initAdapter();
        }
        if (this.mAdapter.getGroupCount() <= 0 || this.rankRecommendTopGroupId != i) {
            this.mListener.onNetworkLoading();
            this.engine.b();
            refreshEngineData(7, i);
        } else {
            this.viewPagerlistener.sendMessage(new ViewInvalidateMessage(2, null, this.pageMessageHandler));
            if (i > 0) {
                getListView().setSelection(0);
            }
        }
        this.rankRecommendTopGroupId = i;
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        TemporaryThreadManager.get().start(new cs(this));
    }

    public void onPause() {
    }

    public void onResume() {
    }

    public void recycleData() {
        super.recycleData();
        this.engine.unregister(this.getGroupAppsCallback);
    }

    /* access modifiers changed from: private */
    public void onAppListLoadedFinishedHandler(int i, int i2, boolean z, boolean z2) {
        if (i2 == 0) {
            this.mListener.onNetworkNoError();
            if (this.mAdapter == null) {
                initAdapter();
            }
            if (this.mAdapter.getGroupCount() == 0) {
                this.mListener.onErrorHappened(10);
                return;
            }
            this.mAdapter.notifyDataSetChanged();
            if (z) {
                getListView().setSelection(0);
            }
            onRefreshComplete(z2);
        } else if (!z) {
            onRefreshComplete(z2);
            this.mListener.onNextPageLoadFailed();
        } else if (-800 == i2) {
            this.mListener.onErrorHappened(30);
        } else if (this.retryCount <= 0) {
            this.mListener.onErrorHappened(20);
        } else {
            this.retryCount--;
            refreshEngineData(7, this.rankRecommendTopGroupId);
        }
    }

    public void refreshEngineData(int i, int i2) {
        if (this.engine != null) {
            TemporaryThreadManager.get().start(new cu(this, i, i2));
        }
    }
}
