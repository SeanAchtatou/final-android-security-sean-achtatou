package com.tencent.assistant.component;

import android.os.Message;
import android.view.animation.Animation;

/* compiled from: ProGuard */
class ds implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TotalTabLayout f1027a;

    ds(TotalTabLayout totalTabLayout) {
        this.f1027a = totalTabLayout;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f1027a.mIsAniming = false;
        if (this.f1027a.mCurrentTabIndex != this.f1027a.mLastAnimIndex) {
            Message obtain = Message.obtain();
            obtain.arg1 = this.f1027a.mCurrentTabIndex;
            obtain.arg2 = this.f1027a.mLastAnimIndex;
            this.f1027a.animationHandler.sendMessage(obtain);
        }
    }
}
