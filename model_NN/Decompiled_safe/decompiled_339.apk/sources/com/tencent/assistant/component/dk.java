package com.tencent.assistant.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAClickListener;

/* compiled from: ProGuard */
class dk extends OnTMAClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ShareAppBar f1020a;

    dk(ShareAppBar shareAppBar) {
        this.f1020a = shareAppBar;
    }

    public void onTMAClick(View view) {
        switch (view.getId()) {
            case R.id.tv_share_qq /*2131165663*/:
                this.f1020a.shareToQQ();
                return;
            case R.id.tv_share_qz /*2131165664*/:
                this.f1020a.shareToQZ();
                return;
            case R.id.tv_share_wx /*2131165665*/:
                this.f1020a.shareToWX();
                return;
            case R.id.iv_divide /*2131165666*/:
            case R.id.layout_down /*2131165667*/:
            default:
                return;
            case R.id.tv_share_timeline /*2131165668*/:
                this.f1020a.shareToTimeLine();
                return;
        }
    }
}
