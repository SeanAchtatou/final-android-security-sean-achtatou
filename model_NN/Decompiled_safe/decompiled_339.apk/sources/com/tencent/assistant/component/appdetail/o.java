package com.tencent.assistant.component.appdetail;

import com.tencent.assistant.component.appdetail.HorizonScrollPicViewer;

/* compiled from: ProGuard */
class o implements HorizonScrollPicViewer.IPicViewerListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppdetailViewPager f924a;

    o(AppdetailViewPager appdetailViewPager) {
        this.f924a = appdetailViewPager;
    }

    public void onTouchPicViewer() {
        boolean unused = this.f924a.f900a = true;
    }

    public void requestEventHandle(boolean z) {
        this.f924a.requestDisallowInterceptTouchEvent(z);
    }
}
