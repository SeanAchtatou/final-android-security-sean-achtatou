package com.tencent.assistant.component.slidingdrawer;

/* compiled from: ProGuard */
public interface ISlidingDrawerView {
    int getDrawerViewHeight();

    void onCloseGroup();

    void onDestroy();

    void onExpandGroup();

    void onPause();

    void onResume();

    void setFrameLayout(SlidingDrawerFrameLayout slidingDrawerFrameLayout);
}
