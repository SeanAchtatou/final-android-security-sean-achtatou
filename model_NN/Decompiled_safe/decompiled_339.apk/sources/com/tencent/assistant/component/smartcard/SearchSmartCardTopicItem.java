package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.graphics.Canvas;
import android.text.Html;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.v;
import com.tencent.assistantv2.adapter.smartlist.aa;
import com.tencent.assistantv2.st.page.a;
import com.tencent.assistantv2.st.page.d;

/* compiled from: ProGuard */
public class SearchSmartCardTopicItem extends SearchSmartCardBaseItem {
    private ImageView i;
    private TXImageView j;
    private TextView k;
    private aa l;
    private final String m = "16";

    public SearchSmartCardTopicItem(Context context) {
        super(context);
    }

    public SearchSmartCardTopicItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SearchSmartCardTopicItem(Context context, i iVar, SmartcardListener smartcardListener, aa aaVar) {
        super(context, iVar, smartcardListener);
        this.l = aaVar;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.hasInit) {
            this.hasInit = true;
            h();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = this.b.inflate((int) R.layout.smartcard_topic, this);
        this.j = (TXImageView) this.c.findViewById(R.id.topic_pic);
        setOnClickListener(this.h);
        this.k = (TextView) this.c.findViewById(R.id.topic_title);
        this.i = (ImageView) this.c.findViewById(R.id.close);
        this.i.setVisibility(8);
        i();
    }

    /* access modifiers changed from: protected */
    public void b() {
        i();
    }

    private void i() {
        v vVar = null;
        if (this.smartcardModel instanceof v) {
            vVar = (v) this.smartcardModel;
        }
        if (vVar != null) {
            this.j.updateImageView(vVar.f1655a, -1, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
            this.k.setText(Html.fromHtml(vVar.o));
        }
    }

    /* access modifiers changed from: protected */
    public String c() {
        return a.a("16", this.l == null ? 0 : this.l.a());
    }

    /* access modifiers changed from: protected */
    public int d() {
        return d.f3360a;
    }

    /* access modifiers changed from: protected */
    public String e() {
        if (this.l == null) {
            return null;
        }
        return this.l.d();
    }

    /* access modifiers changed from: protected */
    public long f() {
        if (this.l == null) {
            return 0;
        }
        return this.l.c();
    }

    /* access modifiers changed from: protected */
    public int g() {
        if (this.l == null) {
            return 2000;
        }
        return this.l.b();
    }
}
