package com.tencent.assistant.component.slidingdrawer;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

/* compiled from: ProGuard */
class b extends Animation {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SlidingDrawerFrameLayout f1111a;
    private LinearLayout b;
    private int c = 0;
    private LinearLayout.LayoutParams d;

    public b(SlidingDrawerFrameLayout slidingDrawerFrameLayout, LinearLayout linearLayout, int i) {
        this.f1111a = slidingDrawerFrameLayout;
        setDuration((long) i);
        this.b = linearLayout;
        this.d = (LinearLayout.LayoutParams) this.b.getLayoutParams();
        this.c = this.d.height;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.component.slidingdrawer.SlidingDrawerFrameLayout.a(com.tencent.assistant.component.slidingdrawer.SlidingDrawerFrameLayout, boolean):boolean
     arg types: [com.tencent.assistant.component.slidingdrawer.SlidingDrawerFrameLayout, int]
     candidates:
      com.tencent.assistant.component.slidingdrawer.SlidingDrawerFrameLayout.a(com.tencent.assistant.component.slidingdrawer.SlidingDrawerFrameLayout, int):int
      com.tencent.assistant.component.slidingdrawer.SlidingDrawerFrameLayout.a(com.tencent.assistant.component.slidingdrawer.SlidingDrawerFrameLayout, android.widget.LinearLayout):android.widget.LinearLayout
      com.tencent.assistant.component.slidingdrawer.SlidingDrawerFrameLayout.a(com.tencent.assistant.component.slidingdrawer.SlidingDrawerFrameLayout, boolean):boolean */
    /* access modifiers changed from: protected */
    public void applyTransformation(float f, Transformation transformation) {
        super.applyTransformation(f, transformation);
        if (f < 1.0f) {
            this.d.height = (int) (((float) this.c) - (((float) this.c) * f));
            this.b.requestLayout();
            return;
        }
        this.d.height = 0;
        View childAt = this.b.getChildAt(0);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) childAt.getLayoutParams();
        layoutParams.width = this.b.getWidth();
        layoutParams.height = this.b.getHeight();
        childAt.setVisibility(8);
        this.b.setVisibility(8);
        this.b.requestLayout();
        LinearLayout unused = this.f1111a.j = (LinearLayout) null;
        int unused2 = this.f1111a.k = -1;
        boolean unused3 = this.f1111a.p = false;
        this.f1111a.a(false);
        this.f1111a.c.scrollTo(0, 0);
        this.f1111a.q.requestLayout();
    }
}
