package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.model.a.t;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.List;

/* compiled from: ProGuard */
public class SmartSquareNode extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private View f1122a;
    private ImageView b;
    private ImageView c;
    private ImageView d;
    private ImageView e;
    private SmartSquareAppItem f;
    private SmartSquareAppItem g;
    private SmartSquareAppItem h;
    private SmartSquareAppItem i;

    /* compiled from: ProGuard */
    public interface StatExposureBuilder {
        STInfoV2 build(t tVar, int i);
    }

    public SmartSquareNode(Context context) {
        this(context, null);
    }

    public SmartSquareNode(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    /* access modifiers changed from: protected */
    public void a() {
        LayoutInflater.from(getContext()).inflate((int) R.layout.smartcard_square_node, this);
        this.f1122a = findViewById(R.id.bottomPart);
        this.b = (ImageView) findViewById(R.id.line1_split);
        this.c = (ImageView) findViewById(R.id.line2_split);
        this.d = (ImageView) findViewById(R.id.line_split);
        this.e = (ImageView) findViewById(R.id.line1_single_split);
        this.f = (SmartSquareAppItem) findViewById(R.id.topleft_anchor);
        this.g = (SmartSquareAppItem) findViewById(R.id.topright_anchor);
        this.h = (SmartSquareAppItem) findViewById(R.id.bottomleft_anchor);
        this.i = (SmartSquareAppItem) findViewById(R.id.bottomright_anchor);
    }

    public void setDataSource(List<t> list, IViewInvalidater iViewInvalidater, StatExposureBuilder statExposureBuilder) {
        STInfoV2 sTInfoV2;
        STInfoV2 sTInfoV22;
        if (list == null || list.size() < 2) {
            setVisibility(8);
            setMinimumHeight(0);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        setVisibility(0);
        for (int i2 = 0; i2 < 2; i2++) {
            t tVar = list.get(i2);
            if (statExposureBuilder != null) {
                sTInfoV22 = statExposureBuilder.build(tVar, i2);
            } else {
                sTInfoV22 = null;
            }
            if (!(tVar == null || tVar.f1653a == null)) {
                if (i2 == 0) {
                    this.f.setViewInvalidater(iViewInvalidater);
                    this.f.setData(tVar, sTInfoV22);
                } else {
                    this.g.setViewInvalidater(iViewInvalidater);
                    this.g.setData(tVar, sTInfoV22);
                }
            }
        }
        if (list.size() >= 4) {
            this.f1122a.setVisibility(0);
            this.c.setVisibility(0);
            this.d.setVisibility(0);
            this.e.setVisibility(8);
            this.b.setVisibility(0);
            for (int i3 = 2; i3 < 4; i3++) {
                t tVar2 = list.get(i3);
                if (statExposureBuilder != null) {
                    sTInfoV2 = statExposureBuilder.build(tVar2, i3);
                } else {
                    sTInfoV2 = null;
                }
                if (!(tVar2 == null || tVar2.f1653a == null)) {
                    if (i3 == 2) {
                        this.h.setViewInvalidater(iViewInvalidater);
                        this.h.setData(tVar2, sTInfoV2);
                    } else {
                        this.i.setViewInvalidater(iViewInvalidater);
                        this.i.setData(tVar2, sTInfoV2);
                    }
                }
            }
            return;
        }
        this.f1122a.setVisibility(8);
        this.d.setVisibility(8);
        this.b.setVisibility(8);
        this.c.setVisibility(8);
        this.e.setVisibility(0);
    }
}
