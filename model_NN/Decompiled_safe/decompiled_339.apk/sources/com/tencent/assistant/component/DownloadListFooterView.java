package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.protocol.jce.HotWordItem;
import com.tencent.assistantv2.component.HotwordsCardView;
import com.tencent.assistantv2.component.appdetail.RecommendAppViewV5;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class DownloadListFooterView extends TXLoadingLayoutBase {
    public static final String TMA_ST_DOWNLOAD_MANAGER_RELATE_USER_SLOT_TAG = "03";
    private Context context;
    private RelativeLayout hotwordLayout;
    private TextView hotwordTitleView;
    private HotwordsCardView hotwordsCardView;
    private LinearLayout layout;
    private RecommendAppViewV5 recommendAppView;

    public DownloadListFooterView(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
    }

    public DownloadListFooterView(Context context2, TXScrollViewBase.ScrollDirection scrollDirection, TXScrollViewBase.ScrollMode scrollMode) {
        super(context2, scrollDirection, scrollMode);
        this.context = context2;
        initLayout(context2);
    }

    private void initLayout(Context context2) {
        View inflate;
        LayoutInflater from = LayoutInflater.from(context2);
        try {
            inflate = from.inflate((int) R.layout.downloadlist_footer_layout, this);
        } catch (Throwable th) {
            cq.a().b();
            inflate = from.inflate((int) R.layout.downloadlist_footer_layout, this);
        }
        this.layout = (LinearLayout) inflate.findViewById(R.id.layout_id);
        this.recommendAppView = (RecommendAppViewV5) inflate.findViewById(R.id.recommendAppView);
        this.recommendAppView.a("03");
        this.recommendAppView.b(2);
        this.hotwordTitleView = (TextView) findViewById(R.id.hot_title);
        this.hotwordsCardView = (HotwordsCardView) inflate.findViewById(R.id.recommendHotwordsView);
        this.hotwordLayout = (RelativeLayout) inflate.findViewById(R.id.recommendHotWordsLayout);
        reset();
    }

    public RecommendAppViewV5 getRecommendAppView() {
        return this.recommendAppView;
    }

    public void reset() {
    }

    public void pullToRefresh() {
    }

    public void releaseToRefresh() {
    }

    public void refreshing() {
    }

    public void loadFinish(String str) {
    }

    public void onPull(int i) {
    }

    public void hideAllSubViews() {
    }

    public void showAllSubViews() {
    }

    public void setWidth(int i) {
        getLayoutParams().width = i;
        requestLayout();
    }

    public void setHeight(int i) {
        getLayoutParams().height = i;
        requestLayout();
    }

    public int getContentSize() {
        return this.layout.getHeight();
    }

    public int getTriggerSize() {
        return getResources().getDimensionPixelSize(R.dimen.tx_pull_to_refresh_trigger_size);
    }

    public void refreshSuc() {
    }

    public void refreshFail(String str) {
    }

    public void loadSuc() {
    }

    public void loadFail() {
    }

    public void freshHotwordCardData(String str, String str2, String str3, ArrayList<HotWordItem> arrayList) {
        if (this.hotwordsCardView != null) {
            this.hotwordTitleView.setText(str);
            this.hotwordsCardView.a(1, str2, str3, arrayList);
        }
    }

    public void setHotwordCardVisibility(int i) {
        if (this.hotwordLayout != null) {
            this.hotwordLayout.setVisibility(i);
        }
    }
}
