package com.tencent.assistant.component;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.f;
import com.tencent.assistant.manager.g;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;

/* compiled from: ProGuard */
public class AppStateButton extends RelativeLayout implements g {
    private Context context;
    private DownloadInfo downloadInfo;
    private boolean ignoreFileNotExist;
    private boolean isFromDownloadPage;
    private SimpleAppModel mAppModel;
    private Button mButton;
    private LayoutInflater mInflater;
    private ProgressBar mProgressBar;
    private TextView mTextView;

    public AppStateButton(Context context2) {
        this(context2, null);
    }

    public AppStateButton(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.ignoreFileNotExist = false;
        this.isFromDownloadPage = false;
        this.context = context2;
        this.mInflater = LayoutInflater.from(context2);
        initButtonView();
    }

    private void initButtonView() {
        this.mInflater.inflate((int) R.layout.component_appbutton, this);
        this.mButton = (Button) findViewById(R.id.state_btn);
        this.mProgressBar = (ProgressBar) findViewById(R.id.appdownload_progress_bar);
        this.mTextView = (TextView) findViewById(R.id.app_floating_txt);
    }

    public void initButtonWidthToFillParent(int i) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, i);
        this.mButton.setLayoutParams(layoutParams);
        this.mProgressBar.setLayoutParams(layoutParams);
        this.mTextView.setLayoutParams(layoutParams);
    }

    public void initButtonText(CharSequence charSequence) {
        this.mTextView.setText(charSequence);
    }

    public void initButtonTextSize(int i) {
        this.mTextView.setTextSize(2, (float) i);
    }

    public void setSimpleAppModel(SimpleAppModel simpleAppModel) {
        this.mAppModel = simpleAppModel;
        initButtonState();
        f.a().a(this.mAppModel.q(), this);
    }

    public void setDownloadInfo(DownloadInfo downloadInfo2) {
        this.downloadInfo = downloadInfo2;
        initButtonState();
        f.a().a(downloadInfo2.downloadTicket, this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.assistant.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.model.j, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.u.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.w, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    private void initButtonState() {
        AppConst.AppState appState = null;
        DownloadInfo d = DownloadProxy.a().d(getDownloadTicket());
        if (d != null && d.ignoreState()) {
            appState = u.a(d, false, false);
        }
        if (appState == null) {
            if (this.mAppModel != null) {
                appState = u.d(this.mAppModel);
            } else {
                appState = u.a(this.downloadInfo, this.ignoreFileNotExist, this.isFromDownloadPage);
            }
        }
        updateStateBtn(getDownloadTicket(), appState);
    }

    public void onAppStateChange(String str, AppConst.AppState appState) {
        String downloadTicket;
        if (!TextUtils.isEmpty(str) && (downloadTicket = getDownloadTicket()) != null && downloadTicket.equals(str)) {
            ba.a().post(new l(this, str, appState));
        }
    }

    private void setDownloadBarVisibility(int i) {
        if (this.mProgressBar != null) {
            this.mProgressBar.setVisibility(i);
        }
    }

    private void setDownloadButtonVisibility(int i) {
        if (this.mButton != null) {
            this.mButton.setVisibility(i);
        }
    }

    private boolean isProgressShowFake(DownloadInfo downloadInfo2, AppConst.AppState appState) {
        return (appState == AppConst.AppState.DOWNLOADING || (downloadInfo2.response.b > 0 && (appState == AppConst.AppState.PAUSED || appState == AppConst.AppState.FAIL))) && downloadInfo2.response.f > SimpleDownloadInfo.getPercent(downloadInfo2);
    }

    private void setProgressAndDownloading(int i, int i2) {
        if (this.mProgressBar != null) {
            this.mProgressBar.setProgress(i);
            this.mProgressBar.setSecondaryProgress(i2);
            invalidate();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.assistant.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.model.j, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.u.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.w, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.assistant.download.DownloadInfo, int, boolean]
     candidates:
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.model.j, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.u.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.w, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    /* access modifiers changed from: private */
    public void updateStateBtn(String str, AppConst.AppState appState) {
        if (appState == null || appState == AppConst.AppState.ILLEGAL) {
            if (this.mAppModel != null) {
                appState = u.d(this.mAppModel);
            } else {
                appState = u.a(this.downloadInfo, false, this.isFromDownloadPage);
            }
        }
        DownloadInfo d = DownloadProxy.a().d(str);
        if (d != null && d.ignoreState()) {
            appState = u.a(d, false, false);
        }
        XLog.d("Wise", "updateStateBtn : appState = " + appState);
        updateBtnTextColor(appState);
        updateBtnText(str, appState);
        updateBtnBg(appState);
        updateBtnVisible(appState);
    }

    private void updateBtnTextColor(AppConst.AppState appState) {
        if (s.b(this.mAppModel, appState) || s.c(this.mAppModel, appState)) {
            this.mTextView.setTextColor(this.context.getResources().getColor(R.color.list_vie_number_txt_color));
        } else if (appState == AppConst.AppState.INSTALLED) {
            this.mTextView.setTextColor(this.context.getResources().getColor(R.color.blue_color_down));
        } else {
            this.mTextView.setTextColor(this.context.getResources().getColor(17170443));
        }
    }

    private void updateBtnText(String str, AppConst.AppState appState) {
        int i;
        DownloadInfo d;
        if (appState != null) {
            if (s.b(this.mAppModel, appState)) {
                this.mTextView.setText((int) R.string.jionbeta);
            } else if (s.c(this.mAppModel, appState)) {
                this.mTextView.setText((int) R.string.jionfirstrelease);
            } else {
                if ((appState != AppConst.AppState.DOWNLOADING && appState != AppConst.AppState.PAUSED) || (d = DownloadProxy.a().d(str)) == null || d.response == null) {
                    i = 0;
                } else if (!d.isUiTypeWiseAppUpdateDownload()) {
                    if (isProgressShowFake(d, appState)) {
                        i = d.response.f;
                    } else {
                        i = SimpleDownloadInfo.getPercent(d);
                    }
                    setProgressAndDownloading(i, 0);
                } else {
                    return;
                }
                switch (m.f1098a[appState.ordinal()]) {
                    case 1:
                        if (this.mAppModel != null && this.mAppModel.c()) {
                            this.mTextView.setText((int) R.string.jionfirstrelease);
                            return;
                        } else if (this.mAppModel == null || !this.mAppModel.h()) {
                            this.mTextView.setText((int) R.string.appbutton_download);
                            return;
                        } else {
                            this.mTextView.setText((int) R.string.jionbeta);
                            return;
                        }
                    case 2:
                        this.mTextView.setText((int) R.string.appbutton_update);
                        return;
                    case 3:
                        this.mTextView.setText(this.downloadInfo != null ? this.context.getResources().getString(R.string.downloading_display_pause) : i + "%");
                        return;
                    case 4:
                        this.mTextView.setText((int) R.string.queuing);
                        return;
                    case 5:
                    case 6:
                        this.mTextView.setText((int) R.string.appbutton_continuing);
                        return;
                    case 7:
                        this.mTextView.setText((int) R.string.appbutton_install);
                        return;
                    case 8:
                        this.mTextView.setText((int) R.string.appbutton_open);
                        return;
                    case 9:
                        this.mTextView.setText((int) R.string.not_support);
                        return;
                    case 10:
                        return;
                    case 11:
                        this.mTextView.setText((int) R.string.installing);
                        return;
                    case 12:
                        this.mTextView.setText((int) R.string.uninstalling);
                        return;
                    default:
                        this.mTextView.setText((int) R.string.appbutton_unknown);
                        return;
                }
            }
        }
    }

    public void updateBtnBg(AppConst.AppState appState) {
        if (appState != null) {
            if (s.b(this.mAppModel, appState) || s.c(this.mAppModel, appState)) {
                this.mButton.setBackgroundColor(this.context.getResources().getColor(17170445));
                this.mButton.setPadding(0, 0, 0, 0);
                return;
            }
            switch (m.f1098a[appState.ordinal()]) {
                case 1:
                case 2:
                    this.mButton.setBackgroundResource(R.drawable.btn_green_selector);
                    return;
                case 3:
                case 4:
                case 5:
                case 6:
                    this.mButton.setBackgroundResource(R.drawable.btn_blue_selector);
                    return;
                case 7:
                    this.mButton.setBackgroundResource(R.drawable.btn_green_selector);
                    return;
                case 8:
                    this.mButton.setBackgroundResource(R.drawable.btn_gray_selector);
                    return;
                case 9:
                default:
                    this.mButton.setBackgroundResource(R.drawable.btn_green_selector);
                    return;
                case 10:
                case 12:
                    return;
                case 11:
                    this.mButton.setBackgroundResource(R.drawable.btn_blue_selector);
                    return;
            }
        }
    }

    public void updateBtnVisible(AppConst.AppState appState) {
        switch (m.f1098a[appState.ordinal()]) {
            case 3:
            case 5:
            case 6:
                if (this.downloadInfo == null) {
                    setDownloadBarVisibility(0);
                    setDownloadButtonVisibility(8);
                    return;
                }
                setDownloadBarVisibility(8);
                setDownloadButtonVisibility(0);
                return;
            case 4:
            default:
                setDownloadBarVisibility(8);
                setDownloadButtonVisibility(0);
                return;
        }
    }

    /* access modifiers changed from: private */
    public String getDownloadTicket() {
        if (this.mAppModel != null) {
            return this.mAppModel.q();
        }
        if (this.downloadInfo != null) {
            return this.downloadInfo.downloadTicket;
        }
        return null;
    }

    public boolean isIgnoreFileNotExist() {
        return this.ignoreFileNotExist;
    }

    public void setIgnoreFileNotExist(boolean z) {
        this.ignoreFileNotExist = z;
    }

    public void setFromDownloadPage(boolean z) {
        this.isFromDownloadPage = z;
    }
}
