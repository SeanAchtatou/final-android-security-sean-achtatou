package com.tencent.assistant.component.listview;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;

/* compiled from: ProGuard */
class p extends ListViewScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RubbishResultListView f1096a;

    p(RubbishResultListView rubbishResultListView) {
        this.f1096a = rubbishResultListView;
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        super.onScrollStateChanged(absListView, i);
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.f1096a.i == null || this.f1096a.h == null) {
            RelativeLayout unused = this.f1096a.i = (RelativeLayout) this.f1096a.g.findViewById(R.id.pop_bar);
            this.f1096a.i.setOnClickListener(new q(this));
            return;
        }
        if (i == 0) {
            this.f1096a.i.setVisibility(8);
        }
        int pointToPosition = this.f1096a.h.pointToPosition(0, this.f1096a.b() - 10);
        int pointToPosition2 = this.f1096a.h.pointToPosition(0, 0);
        long j = 0;
        if (pointToPosition2 != -1) {
            long expandableListPosition = this.f1096a.h.getExpandableListPosition(pointToPosition2);
            int packedPositionChild = ExpandableListView.getPackedPositionChild(expandableListPosition);
            int packedPositionGroup = ExpandableListView.getPackedPositionGroup(expandableListPosition);
            if (packedPositionChild == -1) {
                View expandChildAt = this.f1096a.h.getExpandChildAt(pointToPosition2 - this.f1096a.h.getFirstVisiblePosition());
                if (expandChildAt == null) {
                    int unused2 = this.f1096a.d = 100;
                } else {
                    int unused3 = this.f1096a.d = expandChildAt.getHeight();
                }
            }
            if (this.f1096a.d != 0) {
                if (this.f1096a.e > 0) {
                    int unused4 = this.f1096a.c = packedPositionGroup;
                    String str = (String) this.f1096a.f.getGroup(packedPositionGroup);
                    if (!TextUtils.isEmpty(str)) {
                        this.f1096a.j.setText(str);
                        this.f1096a.i.setVisibility(0);
                    }
                }
                if (this.f1096a.e == 0) {
                    this.f1096a.i.setVisibility(8);
                }
                j = expandableListPosition;
            } else {
                return;
            }
        }
        if (this.f1096a.c == -1) {
            return;
        }
        if (j == 0 && (pointToPosition == 0 || pointToPosition == -1)) {
            this.f1096a.i.setVisibility(8);
            return;
        }
        int d = this.f1096a.b();
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.f1096a.i.getLayoutParams();
        marginLayoutParams.topMargin = -(this.f1096a.d - d);
        this.f1096a.i.setLayoutParams(marginLayoutParams);
    }
}
