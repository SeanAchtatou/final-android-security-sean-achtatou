package com.tencent.assistant.component.smartcard;

import android.content.Context;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.model.a.i;
import com.tencent.assistantv2.adapter.smartlist.aa;
import java.util.HashMap;

/* compiled from: ProGuard */
public class SmartcardFactory {
    private static SmartcardFactory b;

    /* renamed from: a  reason: collision with root package name */
    private HashMap<SmartCardShowKey, Integer> f1123a = new HashMap<>();

    public static synchronized SmartcardFactory getInstance() {
        SmartcardFactory smartcardFactory;
        synchronized (SmartcardFactory.class) {
            if (b == null) {
                b = new SmartcardFactory();
            }
            smartcardFactory = b;
        }
        return smartcardFactory;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0013  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.tencent.assistant.component.smartcard.ISmartcard createSmartcard(android.content.Context r5, com.tencent.assistant.model.a.i r6, com.tencent.assistant.component.smartcard.SmartcardListener r7, int r8, com.tencent.assistantv2.adapter.smartlist.aa r9, com.tencent.assistant.component.invalidater.IViewInvalidater r10) {
        /*
            r4 = this;
            r1 = 0
            if (r6 == 0) goto L_0x0005
            if (r9 != 0) goto L_0x0006
        L_0x0005:
            return r1
        L_0x0006:
            int r0 = r6.i
            switch(r0) {
                case 0: goto L_0x0021;
                case 1: goto L_0x0027;
                case 2: goto L_0x001b;
                case 3: goto L_0x001b;
                case 4: goto L_0x001b;
                case 5: goto L_0x002d;
                case 6: goto L_0x0033;
                case 7: goto L_0x0039;
                case 8: goto L_0x003f;
                case 9: goto L_0x0079;
                case 10: goto L_0x0079;
                case 11: goto L_0x0079;
                case 12: goto L_0x0079;
                case 13: goto L_0x0079;
                case 14: goto L_0x0079;
                case 15: goto L_0x0079;
                case 16: goto L_0x00a2;
                case 17: goto L_0x00a9;
                case 22: goto L_0x00be;
                case 23: goto L_0x00b0;
                case 24: goto L_0x00b7;
                case 27: goto L_0x0079;
                case 2001: goto L_0x0045;
                case 2002: goto L_0x0055;
                case 2003: goto L_0x005b;
                case 2004: goto L_0x0061;
                case 2005: goto L_0x0067;
                case 2006: goto L_0x006d;
                case 2007: goto L_0x0073;
                default: goto L_0x000b;
            }
        L_0x000b:
            int r0 = r6.i
            com.tencent.assistant.manager.smartcard.c r0 = com.tencent.assistant.manager.smartcard.b.a(r0)
            if (r0 == 0) goto L_0x0017
            com.tencent.assistant.component.smartcard.NormalSmartcardBaseItem r1 = r0.a(r5, r6, r7, r10)
        L_0x0017:
            r4.recordJudge(r6, r1, r8, r9)
            goto L_0x0005
        L_0x001b:
            com.tencent.assistant.component.smartcard.NormalSmartcardAppListItem r1 = new com.tencent.assistant.component.smartcard.NormalSmartcardAppListItem
            r1.<init>(r5, r6, r7, r10)
            goto L_0x0017
        L_0x0021:
            com.tencent.assistant.component.smartcard.NormalSmartcardTopicItem r1 = new com.tencent.assistant.component.smartcard.NormalSmartcardTopicItem
            r1.<init>(r5, r6, r7, r10)
            goto L_0x0017
        L_0x0027:
            com.tencent.assistant.component.smartcard.NormalSmartcardLibaoItem r1 = new com.tencent.assistant.component.smartcard.NormalSmartcardLibaoItem
            r1.<init>(r5, r6, r7, r10)
            goto L_0x0017
        L_0x002d:
            com.tencent.assistant.component.smartcard.NormalSmartcardPersonalizedItem r1 = new com.tencent.assistant.component.smartcard.NormalSmartcardPersonalizedItem
            r1.<init>(r5, r6, r7, r10)
            goto L_0x0017
        L_0x0033:
            com.tencent.assistant.component.smartcard.NormalSmartCardSelfUpdateItem r1 = new com.tencent.assistant.component.smartcard.NormalSmartCardSelfUpdateItem
            r1.<init>(r5, r6, r7, r10)
            goto L_0x0017
        L_0x0039:
            com.tencent.assistant.component.smartcard.NormalSmartcardPersonalizedItem r1 = new com.tencent.assistant.component.smartcard.NormalSmartcardPersonalizedItem
            r1.<init>(r5, r6, r7, r10)
            goto L_0x0017
        L_0x003f:
            com.tencent.assistant.component.smartcard.NormalSmartcardPersonalizedItem r1 = new com.tencent.assistant.component.smartcard.NormalSmartcardPersonalizedItem
            r1.<init>(r5, r6, r7, r10)
            goto L_0x0017
        L_0x0045:
            com.tencent.assistant.component.smartcard.SearchSmartCardTopicItem r1 = new com.tencent.assistant.component.smartcard.SearchSmartCardTopicItem
            r1.<init>(r5, r6, r7, r9)
            r0 = r1
            com.tencent.assistant.component.smartcard.SearchSmartCardTopicItem r0 = (com.tencent.assistant.component.smartcard.SearchSmartCardTopicItem) r0
            long r2 = r9.c()
            r0.setSearchId(r2)
            goto L_0x0017
        L_0x0055:
            com.tencent.assistant.component.smartcard.SearchSmartCardAppListItem r1 = new com.tencent.assistant.component.smartcard.SearchSmartCardAppListItem
            r1.<init>(r5, r6, r7, r9)
            goto L_0x0017
        L_0x005b:
            com.tencent.assistant.component.smartcard.SearchSmartCardVideoItem r1 = new com.tencent.assistant.component.smartcard.SearchSmartCardVideoItem
            r1.<init>(r5, r6, r7, r9)
            goto L_0x0017
        L_0x0061:
            com.tencent.assistant.component.smartcard.SearchSmartCardFengyunItem r1 = new com.tencent.assistant.component.smartcard.SearchSmartCardFengyunItem
            r1.<init>(r5, r6, r7, r9)
            goto L_0x0017
        L_0x0067:
            com.tencent.assistant.component.smartcard.SearchSmartCardVideoRelateItem r1 = new com.tencent.assistant.component.smartcard.SearchSmartCardVideoRelateItem
            r1.<init>(r5, r6, r7, r9)
            goto L_0x0017
        L_0x006d:
            com.tencent.assistant.component.smartcard.SearchSmartCardEbookRelateItem r1 = new com.tencent.assistant.component.smartcard.SearchSmartCardEbookRelateItem
            r1.<init>(r5, r6, r7, r9)
            goto L_0x0017
        L_0x0073:
            com.tencent.assistant.component.smartcard.SearchSmartCardYuyiTagItem r1 = new com.tencent.assistant.component.smartcard.SearchSmartCardYuyiTagItem
            r1.<init>(r5, r6, r7, r9)
            goto L_0x0017
        L_0x0079:
            boolean r0 = r6 instanceof com.tencent.assistant.model.a.u
            if (r0 == 0) goto L_0x0017
            r0 = r6
            com.tencent.assistant.model.a.u r0 = (com.tencent.assistant.model.a.u) r0
            int r0 = r0.c()
            switch(r0) {
                case 1: goto L_0x008a;
                case 2: goto L_0x0090;
                case 3: goto L_0x0096;
                case 4: goto L_0x0087;
                case 5: goto L_0x009c;
                default: goto L_0x0087;
            }
        L_0x0087:
            r0 = r1
        L_0x0088:
            r1 = r0
            goto L_0x0017
        L_0x008a:
            com.tencent.assistant.component.smartcard.NormalSmartCardAppVerticalItem r0 = new com.tencent.assistant.component.smartcard.NormalSmartCardAppVerticalItem
            r0.<init>(r5, r6, r7, r10)
            goto L_0x0088
        L_0x0090:
            com.tencent.assistant.component.smartcard.SmartSquareCard r0 = new com.tencent.assistant.component.smartcard.SmartSquareCard
            r0.<init>(r5, r6, r7, r10)
            goto L_0x0088
        L_0x0096:
            com.tencent.assistant.component.smartcard.SmartHorizontalCard r0 = new com.tencent.assistant.component.smartcard.SmartHorizontalCard
            r0.<init>(r5, r6, r7, r10)
            goto L_0x0088
        L_0x009c:
            com.tencent.assistant.component.smartcard.SmartSquareCardWithUser r0 = new com.tencent.assistant.component.smartcard.SmartSquareCardWithUser
            r0.<init>(r5, r6, r7, r10)
            goto L_0x0088
        L_0x00a2:
            com.tencent.assistant.component.smartcard.NormalSmartcardBookingItem r1 = new com.tencent.assistant.component.smartcard.NormalSmartcardBookingItem
            r1.<init>(r5, r6, r7, r10)
            goto L_0x0017
        L_0x00a9:
            com.tencent.assistant.component.smartcard.NormalSmartCardQianghaoItem r1 = new com.tencent.assistant.component.smartcard.NormalSmartCardQianghaoItem
            r1.<init>(r5, r6, r7, r10)
            goto L_0x0017
        L_0x00b0:
            com.tencent.assistant.component.smartcard.NormalSmartCardPlayerSHowItem r1 = new com.tencent.assistant.component.smartcard.NormalSmartCardPlayerSHowItem
            r1.<init>(r5, r6, r7, r10)
            goto L_0x0017
        L_0x00b7:
            com.tencent.assistant.component.smartcard.NormalSmartcardCpaAdvertiseItem r1 = new com.tencent.assistant.component.smartcard.NormalSmartcardCpaAdvertiseItem
            r1.<init>(r5, r6, r7, r10)
            goto L_0x0017
        L_0x00be:
            com.tencent.assistant.component.smartcard.NormalSmartCardHotwordsItem r1 = new com.tencent.assistant.component.smartcard.NormalSmartCardHotwordsItem
            r1.<init>(r5, r6, r7, r10)
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.component.smartcard.SmartcardFactory.createSmartcard(android.content.Context, com.tencent.assistant.model.a.i, com.tencent.assistant.component.smartcard.SmartcardListener, int, com.tencent.assistantv2.adapter.smartlist.aa, com.tencent.assistant.component.invalidater.IViewInvalidater):com.tencent.assistant.component.smartcard.ISmartcard");
    }

    public ISmartcard resetSmartcard(ISmartcard iSmartcard, Context context, i iVar, SmartcardListener smartcardListener, int i, aa aaVar, IViewInvalidater iViewInvalidater) {
        if (iSmartcard == null || iVar == null || aaVar == null) {
            return null;
        }
        if (iSmartcard.smartcardModel.i == iVar.i) {
            iSmartcard.resetSmartcard(iVar);
        } else {
            iSmartcard = createSmartcard(context, iVar, smartcardListener, i, aaVar, iViewInvalidater);
        }
        recordJudge(iVar, iSmartcard, i, aaVar);
        return iSmartcard;
    }

    public void recordJudge(i iVar, ISmartcard iSmartcard, int i, aa aaVar) {
        if (iVar != null && iSmartcard != null && aaVar != null) {
            SmartCardShowKey smartCardShowKey = new SmartCardShowKey(aaVar.b(), iVar.i, iVar.j);
            if (!this.f1123a.containsKey(smartCardShowKey) || this.f1123a.get(smartCardShowKey).intValue() != i) {
                iSmartcard.hasInit = false;
                this.f1123a.put(smartCardShowKey, Integer.valueOf(i));
            }
        }
    }

    /* compiled from: ProGuard */
    public class SmartCardShowKey {

        /* renamed from: a  reason: collision with root package name */
        int f1124a;
        int b;
        int c;

        public SmartCardShowKey(int i, int i2, int i3) {
            this.f1124a = i;
            this.b = i2;
            this.c = i3;
        }

        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof SmartCardShowKey)) {
                return false;
            }
            SmartCardShowKey smartCardShowKey = (SmartCardShowKey) obj;
            if (this.f1124a == smartCardShowKey.f1124a && this.b == smartCardShowKey.b && this.c == smartCardShowKey.c) {
                return true;
            }
            return false;
        }

        public String toString() {
            return "SmartCardShowKey{scene=" + this.f1124a + ", cardType=" + this.b + ", cardId=" + this.c + '}';
        }

        public int hashCode() {
            return ("|" + this.f1124a + "|" + this.b + "|" + this.c).hashCode();
        }
    }
}
