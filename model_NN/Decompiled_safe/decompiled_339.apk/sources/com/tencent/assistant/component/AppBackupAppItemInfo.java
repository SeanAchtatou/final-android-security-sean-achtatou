package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.manager.f;
import com.tencent.assistant.manager.g;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.ba;

/* compiled from: ProGuard */
public class AppBackupAppItemInfo extends LinearLayout implements g {
    /* access modifiers changed from: private */
    public SimpleAppModel downloadableModel;

    public AppBackupAppItemInfo(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setDownloadModel(SimpleAppModel simpleAppModel) {
        this.downloadableModel = simpleAppModel;
        initViewVisibility(this.downloadableModel.t());
        f.a().a(this.downloadableModel.q(), this);
    }

    /* access modifiers changed from: private */
    public void initViewVisibility(AppConst.AppState appState) {
        switch (d.f1009a[appState.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                setVisibility(8);
                return;
            default:
                setVisibility(0);
                return;
        }
    }

    public void onAppStateChange(String str, AppConst.AppState appState) {
        if (str != null && this.downloadableModel != null && str.equals(this.downloadableModel.q())) {
            ba.a().post(new c(this, str, appState));
        }
    }
}
