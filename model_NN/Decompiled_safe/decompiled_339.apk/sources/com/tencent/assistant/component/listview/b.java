package com.tencent.assistant.component.listview;

import android.view.View;
import android.view.animation.Animation;
import com.tencent.assistant.component.listview.AnimationExpandableListView;

/* compiled from: ProGuard */
class b implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f1082a;
    final /* synthetic */ boolean b;
    final /* synthetic */ int c;
    final /* synthetic */ AnimationExpandableListView.ItemAnimatorLisenter d;
    final /* synthetic */ AnimationExpandableListView e;

    b(AnimationExpandableListView animationExpandableListView, View view, boolean z, int i, AnimationExpandableListView.ItemAnimatorLisenter itemAnimatorLisenter) {
        this.e = animationExpandableListView;
        this.f1082a = view;
        this.b = z;
        this.c = i;
        this.d = itemAnimatorLisenter;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f1082a.setVisibility(4);
        if (this.b) {
            this.e.a(this.f1082a, this.c, this.d);
        } else if (this.d != null) {
            this.d.onAnimatorEnd();
        }
    }
}
