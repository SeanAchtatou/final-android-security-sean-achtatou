package com.tencent.assistant.component.popwindow;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.download.a;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.PopUpInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.bt;
import com.tencent.assistantv2.adapter.BackupRecoveryListAdapter;
import com.tencent.assistantv2.st.model.StatInfo;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class BackupRecoveryPopWindowView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    BackupRecoveryListAdapter f1101a;
    /* access modifiers changed from: private */
    public Context b;
    private TextView c;
    private TextView d;
    private ImageView e;
    private TextView f;
    private CheckBox g;
    private TextView h;
    private TextView i;
    private ListView j;
    private PopUpInfo k;
    /* access modifiers changed from: private */
    public List<SimpleAppModel> l;

    public BackupRecoveryPopWindowView(Context context) {
        this(context, null);
    }

    public BackupRecoveryPopWindowView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public BackupRecoveryPopWindowView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.k = null;
        this.l = null;
        this.b = context;
        a();
    }

    private void a() {
        LayoutInflater.from(getContext()).inflate((int) R.layout.popwindow_backuprecovery, this);
        this.c = (TextView) findViewById(R.id.title);
        this.d = (TextView) findViewById(R.id.backup_other);
        this.e = (ImageView) findViewById(R.id.backup_more);
        this.f = (TextView) findViewById(R.id.choseAll_size);
        this.g = (CheckBox) findViewById(R.id.choseAll_checkbox);
        this.h = (TextView) findViewById(R.id.cancelbtn);
        this.i = (TextView) findViewById(R.id.downloadbtn);
        this.i.setOnClickListener(new a(this));
        this.j = (ListView) findViewById(R.id.list_view);
    }

    public void setPopInfo(PopUpInfo popUpInfo) {
        if (popUpInfo != null) {
            this.k = popUpInfo;
            b();
        }
    }

    private void b() {
        if (this.k != null) {
            this.l = u.b(this.k.k);
            this.f1101a.a(this.l, this.l.size(), (long) this.l.size());
            this.j.setAdapter((ListAdapter) this.f1101a);
            this.j.setOnItemClickListener(new b(this));
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        boolean z = true;
        this.i.setText(String.format(this.b.getString(R.string.popwindow_downlaodsize), Integer.valueOf(this.f1101a.a()), bt.a(this.f1101a.b())));
        TextView textView = this.i;
        if (this.f1101a.a() == 0) {
            z = false;
        }
        textView.setEnabled(z);
    }

    /* access modifiers changed from: private */
    public byte[] a(int i2) {
        byte[] bArr = new byte[0];
        if (this.l == null || this.l.size() <= i2) {
            return bArr;
        }
        return this.l.get(i2).y;
    }

    /* access modifiers changed from: private */
    public void d() {
        ArrayList<Boolean> c2 = this.f1101a.c();
        ArrayList arrayList = new ArrayList();
        int i2 = -1;
        for (int i3 = 0; i3 < this.l.size(); i3++) {
            if (c2.get(i3).booleanValue()) {
                arrayList.add(this.l.get(i3));
                i2 = i3;
            }
        }
        if (i2 > -1) {
            a.a().a(arrayList, new StatInfo(0, STConst.ST_PAGE_NECESSARY_POP, 0, null, 0));
        }
    }
}
