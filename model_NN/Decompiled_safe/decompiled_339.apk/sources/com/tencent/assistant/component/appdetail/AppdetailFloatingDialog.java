package com.tencent.assistant.component.appdetail;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.login.d;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ap;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

/* compiled from: ProGuard */
public class AppdetailFloatingDialog extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    private TextView f898a;
    private TextView b;
    private TextView c;
    private TextView d;
    private TextView e;
    private TextView f;
    /* access modifiers changed from: private */
    public IOnFloatViewListener g;
    private Context h;
    private boolean i;
    private boolean j;
    private boolean k;
    private LinearLayout l;
    private ImageView m;
    /* access modifiers changed from: private */
    public String n = "03";
    private View.OnClickListener o = new k(this);

    /* compiled from: ProGuard */
    public interface IOnFloatViewListener {
        void doCollectioon();

        void shareToQQ();

        void shareToQZ();

        void shareToTimeLine();

        void shareToWX();

        void showPermission();

        void showReport();
    }

    public AppdetailFloatingDialog(Context context, int i2) {
        super(context, i2);
        this.h = context;
    }

    public AppdetailFloatingDialog(Context context) {
        super(context);
        this.h = context;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.appdetail_permission_report_float);
        this.f898a = (TextView) findViewById(R.id.tv_permission);
        this.f898a.setOnClickListener(this.o);
        this.b = (TextView) findViewById(R.id.tv_report);
        this.b.setOnClickListener(this.o);
        this.c = (TextView) findViewById(R.id.tv_share_qq);
        this.d = (TextView) findViewById(R.id.tv_share_qz);
        this.e = (TextView) findViewById(R.id.tv_share_wx);
        this.f = (TextView) findViewById(R.id.tv_share_timeline);
        this.c.setOnClickListener(this.o);
        this.d.setOnClickListener(this.o);
        this.e.setOnClickListener(this.o);
        this.f.setOnClickListener(this.o);
        this.l = (LinearLayout) findViewById(R.id.layout_down);
        this.m = (ImageView) findViewById(R.id.iv_divide);
    }

    /* access modifiers changed from: private */
    public STInfoV2 a() {
        int i2 = 2000;
        if (!(this.h instanceof AppDetailActivityV5)) {
            return new STInfoV2(STConst.ST_PAGE_APP_DETAIL_MORE, STConst.ST_DEFAULT_SLOT, 2000, STConst.ST_DEFAULT_SLOT, 100);
        }
        STInfoV2 i3 = ((AppDetailActivityV5) this.h).i();
        if (i3 != null) {
            i2 = i3.scene;
        }
        return new STInfoV2(STConst.ST_PAGE_APP_DETAIL_MORE, STConst.ST_DEFAULT_SLOT, i2, a.b(i3 != null ? i3.slotId : STConst.ST_DEFAULT_SLOT, STConst.ST_STATUS_DEFAULT), 100);
    }

    public void shwoUserCare(boolean z) {
        this.i = z;
    }

    public void setShowCareFirstTime(boolean z) {
        this.j = z;
    }

    public boolean isShowUserCare() {
        return this.i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.component.appdetail.AppdetailFloatingDialog.a(android.widget.TextView, int, boolean):void
     arg types: [android.widget.TextView, ?, int]
     candidates:
      com.tencent.assistant.component.appdetail.AppdetailFloatingDialog.a(android.widget.TextView, int, int):void
      com.tencent.assistant.component.appdetail.AppdetailFloatingDialog.a(android.widget.TextView, int, boolean):void */
    public void disableShareArea() {
        this.k = true;
        if (isShowing()) {
            a(this.d, (int) R.drawable.qzone, false);
            a(this.c, (int) R.drawable.qq, false);
            a(this.e, (int) R.drawable.weixin, false);
            a(this.f, (int) R.drawable.friends, false);
        }
    }

    public void refreshState(boolean z, long j2) {
        refreshShareState();
    }

    public void refreshShareState() {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4 = true;
        IWXAPI createWXAPI = WXAPIFactory.createWXAPI(AstApp.i().getApplicationContext(), "wx3909f6add1206543", false);
        TextView textView = this.d;
        if (f() || this.k) {
            z = false;
        } else {
            z = true;
        }
        a(textView, (int) R.drawable.qzone, z);
        TextView textView2 = this.c;
        if (!d.a().u() || !ap.b() || this.k) {
            z2 = false;
        } else {
            z2 = true;
        }
        a(textView2, (int) R.drawable.qq, z2);
        TextView textView3 = this.e;
        if (createWXAPI.getWXAppSupportAPI() <= 553779201 || this.k) {
            z3 = false;
        } else {
            z3 = true;
        }
        a(textView3, (int) R.drawable.weixin, z3);
        TextView textView4 = this.f;
        if (createWXAPI.getWXAppSupportAPI() <= 553779201 || this.k) {
            z4 = false;
        }
        a(textView4, (int) R.drawable.friends, z4);
    }

    /* access modifiers changed from: private */
    public boolean b() {
        boolean z = !f();
        a(this.d, (int) R.drawable.qzone, z);
        return z;
    }

    /* access modifiers changed from: private */
    public boolean c() {
        boolean z = d.a().u() && ap.b();
        a(this.c, (int) R.drawable.qq, z);
        return z;
    }

    /* access modifiers changed from: private */
    public boolean d() {
        boolean v = d.a().v();
        a(this.e, (int) R.drawable.weixin, v);
        return v;
    }

    /* access modifiers changed from: private */
    public boolean e() {
        boolean z = false;
        if (WXAPIFactory.createWXAPI(AstApp.i().getApplicationContext(), "wx3909f6add1206543", false).getWXAppSupportAPI() > 553779201) {
            z = true;
        }
        a(this.f, (int) R.drawable.friends, z);
        return z;
    }

    private boolean f() {
        return d.a().l();
    }

    public void setListener(IOnFloatViewListener iOnFloatViewListener) {
        this.g = iOnFloatViewListener;
    }

    private Drawable a(int i2, int i3) {
        Drawable drawable = this.h.getResources().getDrawable(i2);
        drawable.mutate();
        drawable.setAlpha(i3);
        return drawable;
    }

    private void a(TextView textView, int i2, int i3) {
        if (textView != null) {
            if (textView.getTextColors() != null) {
                textView.setTextColor(textView.getTextColors().withAlpha(i3));
            }
            Drawable a2 = a(i2, i3);
            a2.setBounds(0, 0, a2.getMinimumWidth(), a2.getMinimumHeight());
            textView.setCompoundDrawables(null, a2, null, null);
        }
    }

    private void a(TextView textView, int i2, boolean z) {
        a(textView, i2, z ? 255 : 120);
        if (textView != null) {
            textView.setEnabled(z);
        }
    }

    public void hideLayoutBesidesShare() {
        this.f898a.setVisibility(4);
        this.b.setVisibility(4);
    }
}
