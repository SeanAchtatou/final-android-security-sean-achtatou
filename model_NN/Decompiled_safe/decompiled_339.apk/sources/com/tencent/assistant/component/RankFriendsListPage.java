package com.tencent.assistant.component;

import android.content.Context;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.login.d;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.module.k;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.net.c;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.component.RankFriendsLoginErrorView;
import com.tencent.assistantv2.component.RankNormalListPage;

/* compiled from: ProGuard */
public class RankFriendsListPage extends RankNormalListPage implements UIEventListener, NetworkMonitor.ConnectivityChangeListener {
    private APN mLastApn = APN.NO_NETWORK;
    private RankFriendsLoginErrorView mLoginErrorView;

    public RankFriendsListPage(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public RankFriendsListPage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public RankFriendsListPage(Context context, TXScrollViewBase.ScrollMode scrollMode, k kVar) {
        super(context, scrollMode, kVar);
    }

    public int getPageId() {
        if (this.mLoginErrorView == null || this.mLoginErrorView.getVisibility() != 0) {
            return STConst.ST_PAGE_RANK_FRIENDS;
        }
        return STConst.ST_PAGE_RANK_FRIENDS_LOGIN;
    }

    public void initView(Context context) {
        this.mInflater = LayoutInflater.from(context);
        View inflate = this.mInflater.inflate((int) R.layout.rankfriendslist_component_view, this);
        this.mListView = (RankFriendsListView) inflate.findViewById(R.id.friends_applist);
        this.mListView.setVisibility(8);
        this.mListView.setDivider(null);
        this.mListView.setTopPaddingSize(0);
        this.loadingView = (LoadingView) inflate.findViewById(R.id.loading_view);
        this.loadingView.setVisibility(0);
        this.mErrorPage = (NormalErrorRecommendPage) inflate.findViewById(R.id.friends_error_page);
        this.mErrorPage.setActivityPageId(STConst.ST_PAGE_RANK_FRIENDS);
        this.mErrorPage.setErrorText(this.mContext.getString(R.string.rank_friends_empty_text));
        this.mErrorPage.setErrorImage(R.drawable.emptypage_pic_06);
        this.mErrorPage.setVisibility(8);
        this.mErrorPage.setButtonClickListener(this.refreshClick);
        this.mLoginErrorView = (RankFriendsLoginErrorView) inflate.findViewById(R.id.loginErrorPage);
        cq.a().a(this);
    }

    public void loadFirstPage() {
        onNetworkLoading();
        refreshView();
    }

    private void showLoginErrorView() {
        this.mErrorPage.setVisibility(8);
        this.loadingView.setVisibility(8);
        this.mListView.setVisibility(8);
        this.mLoginErrorView.setVisibility(0);
        this.mLoginErrorView.a();
    }

    public void onPause() {
        super.onPause();
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGOUT, this);
    }

    public void onResume() {
        super.onResume();
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGOUT, this);
    }

    public void onErrorHappened(int i) {
        if (i == 10) {
            super.onErrorHappened(90);
        } else {
            super.onErrorHappened(i);
        }
        this.mLoginErrorView.setVisibility(8);
    }

    public void onNetworkLoading() {
        super.onNetworkLoading();
        this.mLoginErrorView.setVisibility(8);
    }

    public void onNetworkNoError() {
        super.onNetworkNoError();
        this.mLoginErrorView.setVisibility(8);
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS /*1081*/:
                refreshView();
                return;
            case EventDispatcherEnum.UI_EVENT_LOGOUT /*1085*/:
                showLoginErrorView();
                return;
            default:
                return;
        }
    }

    private void refreshView() {
        if (!c.a()) {
            onErrorHappened(30);
        } else if (d.a().j()) {
            this.mLoginErrorView.setVisibility(8);
            this.mListView.loadFirstPage(true);
        } else {
            this.mListView.clearData();
            showLoginErrorView();
        }
    }

    public void onConnected(APN apn) {
        XLog.d("leobi", "app onConnected" + this.mListView.getmAdapter());
        if (this.mListView.getmAdapter() == null || this.mListView.getmAdapter().getCount() <= 0) {
            loadFirstPage();
        }
    }

    public void onDisconnected(APN apn) {
        this.mLastApn = apn;
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        this.mLastApn = apn2;
    }
}
