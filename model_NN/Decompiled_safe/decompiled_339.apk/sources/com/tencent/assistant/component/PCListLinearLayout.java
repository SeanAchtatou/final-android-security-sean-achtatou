package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.model.OnlinePCListItemModel;
import com.tencent.connect.common.Constants;
import com.tencent.connector.ipc.a;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/* compiled from: ProGuard */
public class PCListLinearLayout extends LinearLayout {
    private static final String TAG = "PCListLinearLayout";
    private static final long TIME_OUT = 15000;
    private PCListClickCallback callback;
    private View.OnClickListener clickListener = new bk(this);
    private int connectOtherPC = R.string.photo_backup_connect_new_pc;
    private Context context;
    private ImageView[] imageViews = new ImageView[4];
    /* access modifiers changed from: private */
    public List<OnlinePCListItemModel> objList = new ArrayList();
    private TimerTask task;
    private TextView[] textViewStatus = new TextView[4];
    private TextView[] textViews = new TextView[4];
    private Timer timer;
    private View[] views = new View[4];

    /* compiled from: ProGuard */
    public interface PCListClickCallback {
        void call(OnlinePCListItemModel onlinePCListItemModel);
    }

    public void setPCListClickCallback(PCListClickCallback pCListClickCallback) {
        this.callback = pCListClickCallback;
    }

    public PCListLinearLayout(Context context2) {
        super(context2);
        this.context = context2;
        init();
    }

    public PCListLinearLayout(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.context = context2;
        init();
    }

    /* access modifiers changed from: private */
    public void printInfo(OnlinePCListItemModel onlinePCListItemModel) {
    }

    /* access modifiers changed from: private */
    public void wantToConnectNewPC(OnlinePCListItemModel onlinePCListItemModel) {
        if ((onlinePCListItemModel == null || !(onlinePCListItemModel.c == 69904 || onlinePCListItemModel.c == 69906)) && this.callback != null) {
            this.callback.call(onlinePCListItemModel);
        }
    }

    public void startTimer() {
        this.task = new bi(this);
        this.timer = new Timer(true);
        this.timer.schedule(this.task, (long) TIME_OUT);
    }

    public void stopTimer() {
        if (this.task != null) {
            this.task.cancel();
            this.task = null;
        }
        if (this.timer != null) {
            this.timer.cancel();
            this.timer = null;
        }
    }

    public synchronized void updateItem(OnlinePCListItemModel onlinePCListItemModel) {
        if (onlinePCListItemModel != null) {
            int size = this.objList.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    break;
                }
                OnlinePCListItemModel onlinePCListItemModel2 = this.objList.get(i);
                if (onlinePCListItemModel2.f1629a.equals(onlinePCListItemModel.f1629a)) {
                    onlinePCListItemModel2.c = 69907;
                    updatePCStatus(onlinePCListItemModel, this.textViews[i], this.textViewStatus[i]);
                    break;
                }
                i++;
            }
        }
    }

    public void updateItemToFailedStatus() {
        int size = this.objList.size();
        for (int i = 0; i < size; i++) {
            OnlinePCListItemModel onlinePCListItemModel = this.objList.get(i);
            if (onlinePCListItemModel.c == 69904) {
                onlinePCListItemModel.c = 69906;
                updateToFaileStatus(onlinePCListItemModel, this.imageViews[i], this.textViews[i], this.textViewStatus[i]);
            }
        }
    }

    private void updateToFaileStatus(OnlinePCListItemModel onlinePCListItemModel, ImageView imageView, TextView textView, TextView textView2) {
        imageView.setImageResource(R.drawable.linkpc_takenicon);
        textView.setTextColor(this.context.getResources().getColor(R.color.default_connect_text_color));
        textView2.setText(this.context.getString(R.string.photo_backup_ping_failed));
    }

    private void updatePCStatus(OnlinePCListItemModel onlinePCListItemModel, TextView textView, TextView textView2) {
        textView.setTextColor(this.context.getResources().getColor(R.color.default_connected_text_color));
        textView2.setText(Constants.STR_EMPTY);
    }

    private void init() {
        int i = 0;
        setOrientation(1);
        View inflate = LayoutInflater.from(this.context).inflate((int) R.layout.backup_pc_list, this);
        this.views[0] = inflate.findViewById(R.id.pc1);
        this.views[1] = inflate.findViewById(R.id.pc2);
        this.views[2] = inflate.findViewById(R.id.pc3);
        this.views[3] = inflate.findViewById(R.id.pc4);
        while (true) {
            int i2 = i;
            if (i2 < this.views.length) {
                this.views[i2].setOnClickListener(this.clickListener);
                this.imageViews[i2] = (ImageView) this.views[i2].findViewById(R.id.connect_icon);
                this.textViews[i2] = (TextView) this.views[i2].findViewById(R.id.connect_pc_name);
                this.textViewStatus[i2] = (TextView) this.views[i2].findViewById(R.id.connect_status);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void updateList(List<OnlinePCListItemModel> list) {
        if (list != null && list.size() != 0) {
            startTimer();
            if (list.size() > 3) {
                list = list.subList(0, 3);
            }
            this.objList.clear();
            this.objList.addAll(list);
            sortList();
            refreshViews();
        }
    }

    private void sortList() {
        OnlinePCListItemModel onlinePCListItemModel;
        int size = this.objList.size();
        if (size != 1) {
            String g = a.a().g();
            int i = 0;
            while (true) {
                if (i < size) {
                    if (g != null && g.equals(this.objList.get(i).f1629a)) {
                        onlinePCListItemModel = this.objList.get(i);
                        break;
                    }
                    i++;
                } else {
                    onlinePCListItemModel = null;
                    break;
                }
            }
            if (onlinePCListItemModel != null) {
                this.objList.remove(onlinePCListItemModel);
                this.objList.add(0, onlinePCListItemModel);
            }
        }
    }

    private void refreshViews() {
        int size = this.objList.size();
        for (int i = 0; i < size; i++) {
            this.views[i].setVisibility(0);
            OnlinePCListItemModel onlinePCListItemModel = this.objList.get(i);
            String g = a.a().g();
            if (g == null || !g.equals(onlinePCListItemModel.f1629a)) {
                onlinePCListItemModel.c = 69904;
                this.imageViews[i].setImageResource(R.drawable.linkpc_othericon);
                this.textViews[i].setText(onlinePCListItemModel.b);
                this.textViews[i].setTextColor(this.context.getResources().getColor(R.color.default_connect_text_color));
                this.textViewStatus[i].setText(this.context.getString(R.string.photo_backup_connecting));
                this.textViewStatus[i].setTextColor(getResources().getColor(R.color.default_connect_text_color));
            } else {
                onlinePCListItemModel.c = 69905;
                this.imageViews[i].setImageResource(R.drawable.linkpc_connecticon_n);
                this.textViews[i].setText(onlinePCListItemModel.b);
                this.textViews[i].setTextColor(this.context.getResources().getColor(R.color.cur_connect_text_color));
                this.textViewStatus[i].setText(this.context.getString(R.string.photo_backup_connect_current));
                this.textViewStatus[i].setTextColor(getResources().getColor(R.color.cur_connect_text_color));
            }
        }
        for (int i2 = size; i2 < this.views.length; i2++) {
            if (i2 == size) {
                this.views[i2].setVisibility(0);
                this.imageViews[i2].setImageResource(R.drawable.logo_other_pc);
                this.textViews[i2].setText(this.connectOtherPC);
                this.textViews[i2].setTextColor(getResources().getColor(R.color.default_connected_text_color));
                this.textViewStatus[i2].setVisibility(8);
            } else {
                this.views[i2].setVisibility(8);
            }
        }
    }
}
