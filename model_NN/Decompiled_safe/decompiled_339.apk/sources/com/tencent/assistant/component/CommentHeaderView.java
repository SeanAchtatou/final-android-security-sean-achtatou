package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class CommentHeaderView extends RelativeLayout {
    private TextView averageCount;
    private CommentProgressBar bar;
    private Context mContext;
    private LayoutInflater mInflater;
    private RatingView ratingView;
    private TextView totalUsersCount;

    private void initLayout() {
        this.mInflater = LayoutInflater.from(this.mContext);
        View inflate = this.mInflater.inflate((int) R.layout.comment_detail_header_layout, this);
        this.averageCount = (TextView) inflate.findViewById(R.id.comment_average_numbers);
        this.ratingView = (RatingView) inflate.findViewById(R.id.comment_score);
        this.totalUsersCount = (TextView) inflate.findViewById(R.id.comment_average_text_totol_custom);
        this.bar = (CommentProgressBar) inflate.findViewById(R.id.commentBar);
    }

    public TextView getAverageCount() {
        return this.averageCount;
    }

    public void setAverageCount(TextView textView) {
        this.averageCount = textView;
    }

    public RatingView getRatingView() {
        return this.ratingView;
    }

    public void setRatingView(RatingView ratingView2) {
        this.ratingView = ratingView2;
    }

    public TextView getTotalUsersCount() {
        return this.totalUsersCount;
    }

    public void setTotalUsersCount(TextView textView) {
        this.totalUsersCount = textView;
    }

    public CommentProgressBar getBar() {
        return this.bar;
    }

    public void setBar(CommentProgressBar commentProgressBar) {
        this.bar = commentProgressBar;
    }

    public CommentHeaderView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mContext = context;
        initLayout();
    }

    public CommentHeaderView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mContext = context;
        initLayout();
    }

    public CommentHeaderView(Context context) {
        super(context);
        this.mContext = context;
        initLayout();
    }

    public float getBarTotalWidth() {
        return 151.0f;
    }
}
