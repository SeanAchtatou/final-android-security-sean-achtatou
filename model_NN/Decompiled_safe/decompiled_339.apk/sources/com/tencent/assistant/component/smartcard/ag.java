package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.view.View;
import com.tencent.assistant.b.a;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.model.a.x;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class ag extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchSmartCardVideoRelateItem f1132a;
    private Context b;
    private x c;

    public ag(SearchSmartCardVideoRelateItem searchSmartCardVideoRelateItem, Context context, x xVar) {
        this.f1132a = searchSmartCardVideoRelateItem;
        this.b = context;
        this.c = xVar;
    }

    public void onTMAClick(View view) {
        String str;
        if (this.c != null) {
            if (this.f1132a.m != null) {
                str = "tpmast://search?" + PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME + "=" + this.f1132a.m.b();
            } else {
                str = "tpmast://search?" + PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME + "=" + 2000;
            }
            b.a(this.f1132a.getContext(), str + "&" + a.ab + "=" + 2);
        }
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f1132a.getContext(), 200);
        if (this.f1132a.m != null) {
            buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a(Constants.VIA_REPORT_TYPE_SHARE_TO_QQ, this.f1132a.m.a());
        } else {
            buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a(Constants.VIA_REPORT_TYPE_SHARE_TO_QQ, 0);
        }
        if (this.f1132a.m != null) {
            buildSTInfo.extraData = this.f1132a.m.d() + ";" + this.c.j;
        }
        buildSTInfo.status = "01";
        return buildSTInfo;
    }
}
