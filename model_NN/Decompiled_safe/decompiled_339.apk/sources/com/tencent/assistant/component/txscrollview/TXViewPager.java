package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
public class TXViewPager extends TXScrollViewBase<ViewPager> {
    public TXViewPager(Context context) {
        super(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_HORIZONTAL, TXScrollViewBase.ScrollMode.NONE);
    }

    public TXViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setAdapter(PagerAdapter pagerAdapter) {
        if (pagerAdapter != null) {
            ((ViewPager) this.mScrollContentView).setAdapter(pagerAdapter);
        }
    }

    public void setOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
        ((ViewPager) this.mScrollContentView).setOnPageChangeListener(onPageChangeListener);
    }

    public void setCurrentItem(int i) {
        ((ViewPager) this.mScrollContentView).setCurrentItem(i);
    }

    public int getCurrentItem() {
        return ((ViewPager) this.mScrollContentView).getCurrentItem();
    }

    public void removeAllViews() {
        ((ViewPager) this.mScrollContentView).removeAllViews();
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForScrollStart() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForScrollEnd() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean isContentFullScreen() {
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ViewPager createScrollContentView(Context context) {
        return new ViewPager(context);
    }
}
