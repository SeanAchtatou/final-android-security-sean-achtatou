package com.tencent.assistant.component.appdetail.process;

import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
/* synthetic */ class r {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f939a = new int[AppConst.AppState.values().length];

    static {
        try {
            f939a[AppConst.AppState.DOWNLOADED.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f939a[AppConst.AppState.ILLEGAL.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f939a[AppConst.AppState.INSTALLED.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f939a[AppConst.AppState.PAUSED.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f939a[AppConst.AppState.FAIL.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f939a[AppConst.AppState.DOWNLOADING.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f939a[AppConst.AppState.QUEUING.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f939a[AppConst.AppState.DOWNLOAD.ordinal()] = 8;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f939a[AppConst.AppState.UPDATE.ordinal()] = 9;
        } catch (NoSuchFieldError e9) {
        }
        try {
            f939a[AppConst.AppState.INSTALLING.ordinal()] = 10;
        } catch (NoSuchFieldError e10) {
        }
        try {
            f939a[AppConst.AppState.UNINSTALLING.ordinal()] = 11;
        } catch (NoSuchFieldError e11) {
        }
    }
}
