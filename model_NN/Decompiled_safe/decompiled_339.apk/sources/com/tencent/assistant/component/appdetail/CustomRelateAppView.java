package com.tencent.assistant.component.appdetail;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.RatingView;
import com.tencent.assistant.component.appdetail.CustomTextView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class CustomRelateAppView extends LinearLayout implements CustomTextView.CustomTextViewInterface {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f903a;
    private LinearLayout[] b = new LinearLayout[3];
    private TXImageView[] c = new TXImageView[3];
    private TextView[] d = new TextView[3];
    private RatingView[] e = new RatingView[3];
    /* access modifiers changed from: private */
    public List<SimpleAppModel> f = new ArrayList();
    private boolean g = false;
    private View.OnClickListener h = new q(this);
    public AstApp mApp;
    public View mParentView;
    public int position = 0;
    public String slotColumn = Constants.STR_EMPTY;

    public CustomRelateAppView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f903a = context;
        a();
    }

    public CustomRelateAppView(Context context) {
        super(context);
        this.f903a = context;
        a();
    }

    private void a() {
        this.mApp = AstApp.i();
        View inflate = LayoutInflater.from(this.f903a).inflate((int) R.layout.appdetail_custom_relate_layout, this);
        this.mParentView = this;
        this.b[0] = (LinearLayout) inflate.findViewById(R.id.ralate_app_layout1);
        this.b[1] = (LinearLayout) inflate.findViewById(R.id.ralate_app_layout2);
        this.b[2] = (LinearLayout) inflate.findViewById(R.id.ralate_app_layout3);
        for (LinearLayout onClickListener : this.b) {
            onClickListener.setOnClickListener(this.h);
        }
        this.c[0] = (TXImageView) inflate.findViewById(R.id.soft_icon_img1);
        this.c[1] = (TXImageView) inflate.findViewById(R.id.soft_icon_img2);
        this.c[2] = (TXImageView) inflate.findViewById(R.id.soft_icon_img3);
        this.d[0] = (TextView) inflate.findViewById(R.id.soft_name_txt1);
        this.d[1] = (TextView) inflate.findViewById(R.id.soft_name_txt2);
        this.d[2] = (TextView) inflate.findViewById(R.id.soft_name_txt3);
        this.e[0] = (RatingView) inflate.findViewById(R.id.app_ratingview1);
        this.e[1] = (RatingView) inflate.findViewById(R.id.app_ratingview2);
        this.e[2] = (RatingView) inflate.findViewById(R.id.app_ratingview3);
    }

    public void freshData(List<SimpleAppModel> list) {
        if (list != null) {
            if (list.size() > 3) {
                list = list.subList(0, 3);
            }
            this.f.clear();
            this.f.addAll(list);
            b();
        }
    }

    public void reset() {
        for (TXImageView imageResource : this.c) {
            imageResource.setImageResource(R.drawable.ic_launcher);
        }
        for (TextView text : this.d) {
            text.setText(getResources().getString(R.string.unknown));
        }
    }

    private void b() {
        for (int i = 0; i < this.f.size(); i++) {
            this.b[i].setVisibility(0);
            SimpleAppModel simpleAppModel = this.f.get(i);
            this.c[i].updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.d[i].setText(simpleAppModel.d);
            this.e[i].setRating(simpleAppModel.q);
        }
        for (int size = this.f.size(); size < this.b.length; size++) {
            this.b[size].setVisibility(4);
        }
    }

    /* access modifiers changed from: private */
    public int a(int i) {
        switch (i) {
            case R.id.ralate_app_layout1 /*2131165582*/:
            default:
                return 0;
            case R.id.ralate_app_layout2 /*2131165586*/:
                return 1;
            case R.id.ralate_app_layout3 /*2131165590*/:
                return 2;
        }
    }

    /* access modifiers changed from: private */
    public String a(int i, int i2) {
        int i3 = (i * 3) + i2 + 1;
        return this.slotColumn + "_" + ct.a(i3) + "|" + (i3 % 4);
    }

    /* access modifiers changed from: private */
    public void a(SimpleAppModel simpleAppModel) {
        if (simpleAppModel != null && !this.g) {
            this.g = true;
            Intent intent = new Intent(this.f903a, AppDetailActivityV5.class);
            int i = STConst.ST_PAGE_APP_DETAIL;
            if (this.f903a instanceof BaseActivity) {
                i = ((BaseActivity) this.f903a).f();
                intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i);
            }
            intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, simpleAppModel);
            intent.putExtra("statInfo", new StatInfo(simpleAppModel.b, i, 0, null, 0));
            this.f903a.startActivity(intent);
            this.g = false;
        }
    }

    public List<SimpleAppModel> getApps() {
        return this.f;
    }

    public void viewExposureST() {
        int i;
        int i2 = 2000;
        if (this.f903a instanceof BaseActivity) {
            BaseActivity baseActivity = (BaseActivity) this.f903a;
            i = baseActivity.f();
            i2 = baseActivity.p();
        } else {
            i = 2000;
        }
        k.a(new STInfoV2(i, this.slotColumn, i2, STConst.ST_DEFAULT_SLOT, 100));
    }
}
