package com.tencent.assistant.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.model.OnlinePCListItemModel;

/* compiled from: ProGuard */
class bk implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PCListLinearLayout f966a;

    bk(PCListLinearLayout pCListLinearLayout) {
        this.f966a = pCListLinearLayout;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.pc1 /*2131165733*/:
                if (this.f966a.objList.size() > 0) {
                    OnlinePCListItemModel onlinePCListItemModel = (OnlinePCListItemModel) this.f966a.objList.get(0);
                    this.f966a.printInfo(onlinePCListItemModel);
                    this.f966a.wantToConnectNewPC(onlinePCListItemModel);
                    return;
                }
                this.f966a.wantToConnectNewPC(null);
                return;
            case R.id.pc2 /*2131165734*/:
                if (this.f966a.objList.size() > 1) {
                    OnlinePCListItemModel onlinePCListItemModel2 = (OnlinePCListItemModel) this.f966a.objList.get(1);
                    this.f966a.printInfo(onlinePCListItemModel2);
                    this.f966a.wantToConnectNewPC(onlinePCListItemModel2);
                    return;
                }
                this.f966a.wantToConnectNewPC(null);
                return;
            case R.id.pc3 /*2131165735*/:
                if (this.f966a.objList.size() > 2) {
                    OnlinePCListItemModel onlinePCListItemModel3 = (OnlinePCListItemModel) this.f966a.objList.get(2);
                    this.f966a.printInfo(onlinePCListItemModel3);
                    this.f966a.wantToConnectNewPC(onlinePCListItemModel3);
                    return;
                }
                this.f966a.wantToConnectNewPC(null);
                return;
            case R.id.pc4 /*2131165736*/:
                this.f966a.wantToConnectNewPC(null);
                return;
            default:
                return;
        }
    }
}
