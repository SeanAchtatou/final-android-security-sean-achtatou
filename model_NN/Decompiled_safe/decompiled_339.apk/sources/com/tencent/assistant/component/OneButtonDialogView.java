package com.tencent.assistant.component;

import android.content.Context;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class OneButtonDialogView extends RelativeLayout {
    private LayoutInflater mInflater;
    private TextView mInfoText;
    private Button mPositiveBtn;
    private TextView mTitleText;

    public OneButtonDialogView(Context context) {
        this(context, null);
    }

    public OneButtonDialogView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mInflater = LayoutInflater.from(context);
        initView();
    }

    private void initView() {
        this.mInflater.inflate((int) R.layout.dialog_1button, this);
        this.mTitleText = (TextView) findViewById(R.id.title);
        this.mInfoText = (TextView) findViewById(R.id.msg);
        this.mInfoText.setMovementMethod(ScrollingMovementMethod.getInstance());
        this.mPositiveBtn = (Button) findViewById(R.id.positive_btn);
    }

    public void setTitleAndMsg(boolean z, String str, String str2) {
        if (z) {
            this.mTitleText.setVisibility(0);
            if (!TextUtils.isEmpty(str)) {
                this.mTitleText.setText(str);
            }
        } else {
            this.mTitleText.setVisibility(8);
        }
        if (!TextUtils.isEmpty(str2)) {
            this.mInfoText.setText(str2);
        }
    }

    public void setButton(String str, View.OnClickListener onClickListener) {
        if (!TextUtils.isEmpty(str)) {
            this.mPositiveBtn.setText(str);
        }
        if (onClickListener != null) {
            this.mPositiveBtn.setOnClickListener(onClickListener);
        }
    }
}
