package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;

/* compiled from: ProGuard */
public class TXAppIconView extends TXImageView {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1188a = true;

    public TXAppIconView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public TXAppIconView(Context context) {
        super(context);
    }

    public void setImageBitmap(Bitmap bitmap) {
        this.f1188a = false;
        super.setImageBitmap(bitmap);
        this.f1188a = true;
    }

    public void setImageResource(int i) {
        this.f1188a = false;
        super.setImageResource(i);
        this.f1188a = true;
    }

    public void requestLayout() {
        if (this.f1188a) {
            super.requestLayout();
        }
    }
}
