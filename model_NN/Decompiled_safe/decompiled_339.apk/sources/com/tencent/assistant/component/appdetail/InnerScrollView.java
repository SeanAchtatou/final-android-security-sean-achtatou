package com.tencent.assistant.component.appdetail;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

/* compiled from: ProGuard */
public class InnerScrollView extends ScrollView implements IInnerScrollListener {

    /* renamed from: a  reason: collision with root package name */
    private float f909a = 10.0f;
    private IOnScrolledToPageBottom b;
    private IOnScrolledToPageTop c;

    /* compiled from: ProGuard */
    public interface IOnScrolledToPageBottom {
        void onNotifyScrollToBottom();
    }

    /* compiled from: ProGuard */
    public interface IOnScrolledToPageTop {
        void onNotifyScrollToTop(boolean z);
    }

    public IOnScrolledToPageBottom getmListener() {
        return this.b;
    }

    public void setOnScrolledToPageBottom(IOnScrolledToPageBottom iOnScrolledToPageBottom) {
        this.b = iOnScrolledToPageBottom;
    }

    public void setOnScrolledToTop(IOnScrolledToPageTop iOnScrolledToPageTop) {
        this.c = iOnScrolledToPageTop;
    }

    public InnerScrollView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context);
    }

    public InnerScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public InnerScrollView(Context context) {
        super(context);
        a(context);
    }

    private void a(Context context) {
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent == null) {
            return false;
        }
        return super.onTouchEvent(motionEvent);
    }

    public boolean canScrollDown() {
        return getScrollY() != 0;
    }

    public boolean canScrollUp() {
        return false;
    }

    public void scrollDeltaY(int i) {
        scrollTo(getScrollX(), getScrollY() - i);
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i, int i2, int i3, int i4) {
        if (getChildAt(getChildCount() - 1).getBottom() - (getHeight() + getScrollY()) > 0 || this.b == null) {
            super.onScrollChanged(i, i2, i3, i4);
        } else {
            this.b.onNotifyScrollToBottom();
        }
    }

    public void setScrolledToBottomOffset(float f) {
        this.f909a = f;
    }

    public void scrollTopFinish(boolean z) {
        if (this.c != null) {
            this.c.onNotifyScrollToTop(z);
        }
    }

    public void startScrollToBottom(int i) {
        post(new v(this, i));
    }
}
