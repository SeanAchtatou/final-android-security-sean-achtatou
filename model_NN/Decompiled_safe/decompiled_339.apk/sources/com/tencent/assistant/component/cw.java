package com.tencent.assistant.component;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.utils.ck;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class cw extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SecondNavigationTitleView f1005a;

    cw(SecondNavigationTitleView secondNavigationTitleView) {
        this.f1005a = secondNavigationTitleView;
    }

    public void onTMAClick(View view) {
        this.f1005a.dialog.setShowCareFirstTime(!ck.b());
        ck.a(this.f1005a.dialog.isShowUserCare());
        if (this.f1005a.iv_red_dot != null) {
            this.f1005a.iv_red_dot.setVisibility(8);
        }
        if (this.f1005a.firstShowFloatFlag) {
            boolean unused = this.f1005a.firstShowFloatFlag = false;
        }
        if (this.f1005a.hostActivity != null && !this.f1005a.hostActivity.isFinishing()) {
            this.f1005a.dialog.show();
        }
        if (!(this.f1005a.appModel == null || this.f1005a.simpleAppModel == null)) {
            this.f1005a.dialog.refreshState(this.f1005a.appModel.f1660a.f, this.f1005a.simpleAppModel.f1634a);
        }
        Window window = this.f1005a.dialog.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.gravity = 49;
        attributes.y = this.f1005a.showMore.getTop() + this.f1005a.showMore.getHeight();
        attributes.width = (int) (((double) this.f1005a.dialog.getWindow().getWindowManager().getDefaultDisplay().getWidth()) * 0.95d);
        window.setAttributes(attributes);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f1005a.context, 200);
        if (buildSTInfo != null) {
            buildSTInfo.slotId = "03_001";
            buildSTInfo.updateWithSimpleAppModel(this.f1005a.simpleAppModel);
        }
        return buildSTInfo;
    }
}
