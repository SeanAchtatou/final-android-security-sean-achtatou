package com.tencent.assistant.component.appdetail;

import com.tencent.assistant.adapter.an;

/* compiled from: ProGuard */
class n implements IInnerScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppdetailViewPager f923a;

    n(AppdetailViewPager appdetailViewPager) {
        this.f923a = appdetailViewPager;
    }

    public boolean canScrollDown() {
        IAppdetailView iAppdetailView = (IAppdetailView) ((an) this.f923a.getAdapter()).a(this.f923a.getCurrentItem());
        if (iAppdetailView == null) {
            return false;
        }
        IInnerScrollListener innerScrollView = iAppdetailView.getInnerScrollView();
        if (innerScrollView != null) {
            return innerScrollView.canScrollDown();
        }
        return false;
    }

    public boolean canScrollUp() {
        IAppdetailView iAppdetailView = (IAppdetailView) ((an) this.f923a.getAdapter()).a(this.f923a.getCurrentItem());
        if (iAppdetailView == null) {
            return false;
        }
        IInnerScrollListener innerScrollView = iAppdetailView.getInnerScrollView();
        if (innerScrollView != null) {
            return innerScrollView.canScrollUp();
        }
        return false;
    }

    public void scrollDeltaY(int i) {
        IInnerScrollListener innerScrollView;
        IAppdetailView iAppdetailView = (IAppdetailView) ((an) this.f923a.getAdapter()).a(this.f923a.getCurrentItem());
        if (iAppdetailView != null && (innerScrollView = iAppdetailView.getInnerScrollView()) != null) {
            innerScrollView.scrollDeltaY(i);
        }
    }

    public void fling(int i) {
        IInnerScrollListener innerScrollView;
        IAppdetailView iAppdetailView = (IAppdetailView) ((an) this.f923a.getAdapter()).a(this.f923a.getCurrentItem());
        if (iAppdetailView != null && (innerScrollView = iAppdetailView.getInnerScrollView()) != null) {
            innerScrollView.fling(i);
        }
    }

    public void scrollTopFinish(boolean z) {
        IInnerScrollListener innerScrollView;
        IAppdetailView iAppdetailView = (IAppdetailView) ((an) this.f923a.getAdapter()).a(this.f923a.getCurrentItem());
        if (iAppdetailView != null && (innerScrollView = iAppdetailView.getInnerScrollView()) != null) {
            innerScrollView.scrollTopFinish(z);
        }
    }
}
