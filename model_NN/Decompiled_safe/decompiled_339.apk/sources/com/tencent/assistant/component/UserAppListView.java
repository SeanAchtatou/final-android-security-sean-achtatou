package com.tencent.assistant.component;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import android.view.ViewStub;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.adapter.UserInstalledAppListAdapter;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXRefreshListView;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.bo;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.installuninstall.p;
import com.tencent.assistant.utils.t;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* compiled from: ProGuard */
public class UserAppListView extends FrameLayout {
    private ViewStub errorStub = null;
    private NormalErrorPage errorView = null;
    private LoadingView loadingView;
    /* access modifiers changed from: private */
    public UserInstalledAppListAdapter mAdapter;
    private Context mContext;
    /* access modifiers changed from: private */
    public FooterView mFooterView;
    private ek mInvalidater;
    /* access modifiers changed from: private */
    public boolean mIsUserStartUninstall = false;
    /* access modifiers changed from: private */
    public TXRefreshListView mListView;
    private SideBar mSideBar;
    private int mSortType;
    private TextView mTvChoosedLetter = null;
    /* access modifiers changed from: private */
    public List<LocalApkInfo> mUninstallAppList = Collections.synchronizedList(new ArrayList());
    /* access modifiers changed from: private */
    public Set<String> mUninstallFailSet = new HashSet();
    /* access modifiers changed from: private */
    public boolean mUninstalling = false;

    public UserAppListView(Context context) {
        super(context);
        this.mContext = context;
        initView();
    }

    public UserAppListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mContext = context;
        initView();
    }

    private void initView() {
        this.mInvalidater = new ek(this);
        LayoutInflater.from(this.mContext).inflate((int) R.layout.installed_applist_layout, this);
        this.mListView = (TXRefreshListView) findViewById(R.id.user_applist);
        this.mListView.setOnScrollListener(this.mInvalidater);
        this.mListView.setDivider(null);
        this.mSideBar = (SideBar) findViewById(R.id.sidrbar);
        this.mFooterView = (FooterView) findViewById(R.id.foot_view);
        this.mSideBar.setTextView(this.mTvChoosedLetter);
        this.mSideBar.setOnTouchingLetterChangedListener(new eh(this));
        this.mFooterView.updateContent(this.mContext.getString(R.string.app_admin_one_key_uninstall));
        this.mFooterView.setFooterViewEnable(false);
        this.mFooterView.setOnFooterViewClickListener(new ej(this));
        this.loadingView = (LoadingView) findViewById(R.id.loading);
        this.errorStub = (ViewStub) findViewById(R.id.error_stub);
        this.mAdapter = new UserInstalledAppListAdapter(this.mContext);
        this.mAdapter.a(this.mInvalidater);
        this.mListView.setAdapter(this.mAdapter);
        this.loadingView.setVisibility(0);
        this.mListView.setVisibility(8);
        this.mSideBar.setVisibility(8);
        this.mFooterView.setVisibility(8);
    }

    public void refreshData(List<LocalApkInfo> list, int i) {
        this.mSortType = i;
        if (list == null || list.isEmpty()) {
            showEmptyPage();
            return;
        }
        if (i == 2) {
            this.mSideBar.setVisibility(0);
        }
        this.mListView.setVisibility(0);
        this.mFooterView.setVisibility(0);
        this.loadingView.setVisibility(8);
        this.mAdapter.a(list, i);
        if (this.errorView != null) {
            this.errorView.setVisibility(8);
        }
    }

    private void showEmptyPage() {
        if (this.errorView == null) {
            this.errorStub.inflate();
            this.errorView = (NormalErrorPage) findViewById(R.id.error);
        }
        this.errorView.setErrorType(1);
        this.errorView.setErrorHint(getResources().getString(R.string.no_user_apps));
        this.errorView.setErrorImage(R.drawable.emptypage_pic_03);
        this.errorView.setErrorHintTextColor(getResources().getColor(R.color.common_listiteminfo));
        this.errorView.setErrorHintTextSize(getResources().getDimension(R.dimen.appadmin_empty_page_text_size));
        this.errorView.setErrorTextVisibility(8);
        this.errorView.setErrorHintVisibility(0);
        this.errorView.setFreshButtonVisibility(8);
        this.errorView.setVisibility(0);
        this.loadingView.setVisibility(8);
        this.mListView.setVisibility(8);
        this.mSideBar.setVisibility(8);
        this.mFooterView.setVisibility(8);
    }

    public void hideSideBar() {
        this.mSideBar.setVisibility(8);
        this.mAdapter.a(true);
    }

    public void showSideBar() {
        this.mSideBar.setVisibility(0);
        this.mAdapter.a(false);
    }

    public void setTextChooser(TextView textView) {
        this.mTvChoosedLetter = textView;
        if (this.mSideBar != null) {
            this.mSideBar.setTextView(this.mTvChoosedLetter);
        }
    }

    public void setInvalidater(IViewInvalidater iViewInvalidater) {
        this.mAdapter.a(iViewInvalidater);
    }

    public void setOnScrollListener(AbsListView.OnScrollListener onScrollListener) {
        this.mListView.setOnScrollListener(onScrollListener);
    }

    public void refreshUi() {
        if (this.mAdapter != null) {
            this.mAdapter.notifyDataSetChanged();
        }
    }

    public void setSelection(int i) {
        this.mListView.setSelection(i);
    }

    public List<LocalApkInfo> getUninstallAppList() {
        return this.mUninstallAppList;
    }

    public void updateFootView(LocalApkInfo localApkInfo) {
        boolean contains = this.mUninstallAppList.contains(localApkInfo);
        if (localApkInfo.mIsSelect && !contains) {
            this.mUninstallAppList.add(localApkInfo);
        } else if (!localApkInfo.mIsSelect && contains) {
            this.mUninstallAppList.remove(localApkInfo);
        }
        updateFootView();
    }

    public void setHandlerToAdaper(Handler handler) {
        if (this.mAdapter != null) {
            this.mAdapter.a(handler);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.adapter.UserInstalledAppListAdapter.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.tencent.assistant.adapter.UserInstalledAppListAdapter.a(int, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistant.adapter.UserInstalledAppListAdapter.a(android.widget.TextView, long):void
      com.tencent.assistant.adapter.UserInstalledAppListAdapter.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, int):void
      com.tencent.assistant.adapter.UserInstalledAppListAdapter.a(boolean, boolean):void */
    public boolean onAppUninstallFinish(LocalApkInfo localApkInfo) {
        this.mUninstallAppList.remove(localApkInfo);
        this.mUninstallFailSet.remove(localApkInfo.mPackageName);
        LocalApkInfo nextUninstallApp = getNextUninstallApp();
        if (nextUninstallApp == null || !this.mIsUserStartUninstall) {
            this.mUninstalling = false;
            this.mIsUserStartUninstall = false;
            if (this.mAdapter != null) {
                this.mAdapter.a(this.mUninstalling, false);
            }
            updateFootView();
        } else {
            this.mUninstalling = true;
            if (this.mAdapter != null) {
                this.mAdapter.a(this.mUninstalling, true);
            }
            updateFootView();
            startUninstallApp(nextUninstallApp);
        }
        return this.mUninstalling;
    }

    public void onAppUninstallFailed(String str) {
        this.mUninstallFailSet.add(str);
    }

    /* access modifiers changed from: private */
    public void startUninstallApp(LocalApkInfo localApkInfo) {
        if (!p.a().a(localApkInfo.mPackageName)) {
            bo.a().c(localApkInfo.mAppName);
            bo.a().d(localApkInfo.mPackageName);
            p.a().a(this.mContext, String.valueOf(localApkInfo.mAppid), localApkInfo.mPackageName, (long) localApkInfo.mVersionCode, localApkInfo.mAppName, localApkInfo.mLocalFilePath, true, true);
        }
    }

    /* access modifiers changed from: private */
    public void updateFootView() {
        long j;
        if (this.mUninstalling) {
            this.mFooterView.updateContent(this.mContext.getString(R.string.uninstalling));
            this.mFooterView.setFooterViewEnable(false);
            return;
        }
        int size = this.mUninstallAppList.size();
        long j2 = 0;
        Iterator<LocalApkInfo> it = this.mUninstallAppList.iterator();
        while (true) {
            j = j2;
            if (!it.hasNext()) {
                break;
            }
            j2 = j + it.next().occupySize;
        }
        if (this.mFooterView != null) {
            String c = bt.c(j);
            String string = this.mContext.getString(R.string.app_admin_one_key_uninstall);
            if (size > 0) {
                this.mFooterView.setFooterViewEnable(true);
                this.mFooterView.updateContent(string, " " + String.format(this.mContext.getString(R.string.apkmgr_delete_format), Integer.valueOf(size), c));
                return;
            }
            this.mFooterView.setFooterViewEnable(false);
            this.mFooterView.updateContent(string);
        }
    }

    public LocalApkInfo getNextUninstallApp() {
        if (this.mUninstallAppList.size() > 0) {
            for (LocalApkInfo next : this.mUninstallAppList) {
                if (!this.mUninstallFailSet.contains(next.mPackageName)) {
                    return next;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void reportBatchUninstall(int i) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", i + Constants.STR_EMPTY);
        hashMap.put("B2", this.mSortType + Constants.STR_EMPTY);
        hashMap.put("B3", Global.getPhoneGuidAndGen());
        hashMap.put("B4", Global.getQUAForBeacon());
        hashMap.put("B5", t.g());
        a.a("batchUninstallUserApp", true, -1, -1, hashMap, true);
        XLog.d("beacon", "beacon report >> event: batchUninstallUserApp, params : " + hashMap.toString());
    }

    /* access modifiers changed from: private */
    public float getYVelocity() {
        try {
            Field declaredField = AbsListView.class.getDeclaredField("mVelocityTracker");
            declaredField.setAccessible(true);
            try {
                VelocityTracker velocityTracker = (VelocityTracker) declaredField.get(this.mListView.getListView());
                velocityTracker.computeCurrentVelocity(1000);
                return velocityTracker.getYVelocity();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                return 0.0f;
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
                return 0.0f;
            } catch (Exception e3) {
                e3.printStackTrace();
                return 0.0f;
            }
        } catch (NoSuchFieldException e4) {
            e4.printStackTrace();
            return 0.0f;
        }
    }

    /* access modifiers changed from: private */
    public boolean isSlowFling() {
        return Math.abs(getYVelocity()) < ((float) (ViewConfiguration.get(getContext()).getScaledMinimumFlingVelocity() * 50));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.adapter.UserInstalledAppListAdapter.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.tencent.assistant.adapter.UserInstalledAppListAdapter.a(int, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistant.adapter.UserInstalledAppListAdapter.a(android.widget.TextView, long):void
      com.tencent.assistant.adapter.UserInstalledAppListAdapter.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, int):void
      com.tencent.assistant.adapter.UserInstalledAppListAdapter.a(boolean, boolean):void */
    public void onResume() {
        this.mUninstalling = false;
        if (this.mAdapter != null) {
            this.mAdapter.a(this.mUninstalling, true);
        }
        updateFootView();
    }
}
