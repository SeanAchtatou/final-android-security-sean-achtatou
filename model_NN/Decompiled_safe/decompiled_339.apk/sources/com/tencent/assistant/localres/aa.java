package com.tencent.assistant.localres;

import android.text.TextUtils;
import com.tencent.assistant.localres.callback.b;
import com.tencent.assistant.localres.localapk.a;
import com.tencent.assistant.localres.localapk.j;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class aa extends x<b> {

    /* renamed from: a  reason: collision with root package name */
    a f1413a;
    aj b;
    /* access modifiers changed from: private */
    public List<LocalApkInfo> c = Collections.synchronizedList(new ArrayList());
    private j d = new ab(this);
    /* access modifiers changed from: private */
    public int e = 1;

    public aa() {
        h();
    }

    private void h() {
        this.f1413a = new a();
        this.f1413a.a(this.d);
        this.b = new aj();
    }

    public void a() {
        if (!this.f1413a.f1437a) {
            this.c.clear();
            this.b.a();
            this.f1413a.a(3);
        }
    }

    public void b() {
        this.f1413a.a(1);
    }

    public void c() {
        this.f1413a.a();
    }

    public Map<Integer, ArrayList<LocalApkInfo>> d() {
        return this.b.d();
    }

    public void a(LocalApkInfo localApkInfo) {
        if (localApkInfo != null) {
            this.b.f();
            int i = 0;
            long j = 0;
            for (LocalApkInfo next : this.c) {
                if (next != null && next.mIsSelect) {
                    i++;
                    j += next.occupySize;
                }
            }
            a(new ac(this, this.c.size(), i, j));
        }
    }

    public boolean e() {
        if (this.c == null) {
            return false;
        }
        for (LocalApkInfo next : this.c) {
            if (next != null && !next.mIsSelect) {
                return false;
            }
        }
        return true;
    }

    public LocalApkInfo a(String str, int i, int i2) {
        LocalApkInfo a2 = this.f1413a.a(str, i, i2, 1);
        if (a2 == null || !this.f1413a.a(a2.mLocalFilePath)) {
            return null;
        }
        return a2;
    }

    public ArrayList<LocalApkInfo> f() {
        return new ArrayList<>(this.c);
    }

    public void g() {
        ArrayList<LocalApkInfo> arrayList = new ArrayList<>();
        long j = 0;
        for (LocalApkInfo localApkInfo : new ArrayList(this.c)) {
            if (localApkInfo.mIsSelect) {
                boolean d2 = d(localApkInfo);
                if (d2) {
                    arrayList.add(localApkInfo);
                    j += localApkInfo.occupySize;
                }
                a(new ad(this, localApkInfo, d2));
            }
        }
        int size = arrayList.size();
        for (LocalApkInfo e2 : arrayList) {
            e(e2);
        }
        a(new ae(this, new ArrayList(this.c), size, j));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.localres.aa.a(com.tencent.assistant.localres.model.LocalApkInfo, boolean):void
     arg types: [com.tencent.assistant.localres.model.LocalApkInfo, int]
     candidates:
      com.tencent.assistant.localres.aa.a(com.tencent.assistant.localres.aa, java.util.List):void
      com.tencent.assistant.localres.aa.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, java.lang.String):void
      com.tencent.assistant.localres.aa.a(boolean, int):void
      com.tencent.assistant.localres.aa.a(java.lang.String, int):void
      com.tencent.assistant.localres.aa.a(com.tencent.assistant.localres.model.LocalApkInfo, boolean):void */
    public void b(LocalApkInfo localApkInfo) {
        a(localApkInfo, true);
    }

    public void a(LocalApkInfo localApkInfo, boolean z) {
        if (localApkInfo != null) {
            boolean d2 = d(localApkInfo);
            e(localApkInfo);
            ArrayList arrayList = new ArrayList(this.c);
            if (z) {
                a(new af(this, arrayList, localApkInfo, d2));
            }
        }
    }

    public void a(String str, int i) {
        if (!TextUtils.isEmpty(str)) {
            LocalApkInfo localApkInfo = null;
            int i2 = 0;
            for (LocalApkInfo next : this.c) {
                if (next.getLocalApkInfoKey().equals(str)) {
                    if (i == 1) {
                        next.mInstall = true;
                        next.mIsHistory = false;
                        this.b.c(next);
                    } else {
                        next.mInstall = false;
                    }
                    if (!c(next) || m.a().l()) {
                        if (next.mIsSelect) {
                            i2++;
                        }
                        localApkInfo = next;
                    }
                }
                next = localApkInfo;
                localApkInfo = next;
            }
            long j = 0;
            if (localApkInfo != null) {
                j = 0 + localApkInfo.occupySize;
                e(localApkInfo);
                d(localApkInfo);
            }
            this.b.b();
            this.b.c();
            a(new ag(this, new ArrayList(this.c), i2, j));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.localres.localapk.a.a(int, java.lang.String, boolean):void
     arg types: [int, java.lang.String, int]
     candidates:
      com.tencent.assistant.localres.localapk.a.a(java.lang.String, int, int):java.lang.String
      com.tencent.assistant.localres.localapk.a.a(int, java.lang.String, com.tencent.assistant.localres.model.LocalApkInfo):void
      com.tencent.assistant.localres.localapk.a.a(int, java.lang.String, boolean):void */
    public void a(String str) {
        this.f1413a.a(this.e, str, true);
    }

    /* access modifiers changed from: private */
    public void a(boolean z, int i) {
        a(new ah(this, new ArrayList(this.c), this.b.e(), z, i));
    }

    /* access modifiers changed from: private */
    public void a(List<LocalApkInfo> list) {
        a(new ai(this, list));
    }

    public LocalApkInfo b(String str) {
        return this.f1413a.a(str, 1);
    }

    private boolean c(LocalApkInfo localApkInfo) {
        if (localApkInfo == null) {
            return false;
        }
        return this.f1413a.a(localApkInfo.mLocalFilePath);
    }

    private boolean d(LocalApkInfo localApkInfo) {
        return localApkInfo != null && this.f1413a.b(localApkInfo);
    }

    private void e(LocalApkInfo localApkInfo) {
        if (localApkInfo != null) {
            a(this.c, localApkInfo.mLocalFilePath);
            this.b.b(localApkInfo);
            this.f1413a.c(localApkInfo);
        }
    }

    private void a(List<LocalApkInfo> list, String str) {
        int i;
        if (list != null && !TextUtils.isEmpty(str)) {
            int i2 = 0;
            while (true) {
                i = i2;
                if (i >= list.size()) {
                    i = -1;
                    break;
                }
                LocalApkInfo localApkInfo = list.get(i);
                if (localApkInfo != null && localApkInfo.mLocalFilePath.equals(str)) {
                    break;
                }
                i2 = i + 1;
            }
            if (i >= 0 && i < list.size()) {
                try {
                    list.remove(i);
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        }
    }
}
