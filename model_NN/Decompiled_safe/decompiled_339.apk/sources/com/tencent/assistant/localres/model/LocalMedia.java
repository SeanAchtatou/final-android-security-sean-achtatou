package com.tencent.assistant.localres.model;

import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class LocalMedia {
    public static final byte MEDIA_TYPE_APP = 4;
    public static final byte MEDIA_TYPE_AUDIO = 1;
    public static final byte MEDIA_TYPE_AVI = 5;
    public static final byte MEDIA_TYPE_IMAGE = 2;
    public static final byte MEDIA_TYPE_VIDEO = 3;
    public static final byte MEDIA_TYPE_XML = 6;
    public long createDate;
    public String description = Constants.STR_EMPTY;
    public int id;
    public String mimeType;
    public String name;
    public String path;
    public long size;
    public String softKey;
    public String thumbnailPath;
    public long updateDate;
}
