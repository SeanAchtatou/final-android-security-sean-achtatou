package com.tencent.assistant.localres;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import java.lang.ref.WeakReference;
import java.util.Iterator;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1431a;
    final /* synthetic */ boolean b;
    final /* synthetic */ ApkResourceManager c;

    g(ApkResourceManager apkResourceManager, String str, boolean z) {
        this.c = apkResourceManager;
        this.f1431a = str;
        this.b = z;
    }

    public void run() {
        LocalApkInfo localApkInfo = (LocalApkInfo) this.c.h.remove(this.f1431a);
        Iterator it = this.c.e.iterator();
        while (it.hasNext()) {
            ApkResCallback apkResCallback = (ApkResCallback) ((WeakReference) it.next()).get();
            if (apkResCallback != null) {
                if (apkResCallback.onInstallApkChangedNeedReplacedEvent()) {
                    apkResCallback.onInstalledApkDataChanged(localApkInfo, 2, this.b);
                } else {
                    apkResCallback.onInstalledApkDataChanged(localApkInfo, 2);
                }
            }
        }
        this.c.c.a(localApkInfo);
        if (this.c.g != null) {
            this.c.g.a(localApkInfo.getLocalApkInfoKey(), 2);
        }
    }
}
