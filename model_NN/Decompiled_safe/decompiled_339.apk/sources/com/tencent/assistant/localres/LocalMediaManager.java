package com.tencent.assistant.localres;

import android.content.Context;
import com.qq.AppService.AstApp;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class LocalMediaManager {
    private static LocalMediaManager b = null;

    /* renamed from: a  reason: collision with root package name */
    private Context f1411a;
    private ArrayList<LocalMediaLoader<?>> c;

    protected LocalMediaManager() {
        this.f1411a = null;
        this.f1411a = AstApp.i().getBaseContext();
        this.c = new ArrayList<>();
    }

    public static LocalMediaManager getInstance() {
        if (b == null) {
            b = new LocalMediaManager();
        }
        return b;
    }

    public LocalMediaLoader<?> getLoader(int i) {
        LocalMediaLoader<?> localMediaLoader;
        Iterator<LocalMediaLoader<?>> it = this.c.iterator();
        while (true) {
            if (it.hasNext()) {
                localMediaLoader = it.next();
                if (localMediaLoader.getMediaType() == i) {
                    break;
                }
            } else {
                localMediaLoader = null;
                switch (i) {
                    case 1:
                        localMediaLoader = new LocalImageLoader(this.f1411a);
                        break;
                    case 2:
                        localMediaLoader = new LocalVideoLoader(this.f1411a);
                        break;
                    case 3:
                        localMediaLoader = new LocalAudioLoader(this.f1411a);
                        break;
                    case 5:
                        localMediaLoader = new an(this.f1411a);
                        break;
                    case 6:
                        localMediaLoader = new LocalAVLoader(this.f1411a);
                        break;
                    case 7:
                        localMediaLoader = new WiFiReceiveLoader(this.f1411a);
                        break;
                }
                if (localMediaLoader != null) {
                    this.c.add(localMediaLoader);
                }
            }
        }
        return localMediaLoader;
    }
}
