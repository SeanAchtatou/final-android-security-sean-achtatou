package com.tencent.assistant.localres;

import com.tencent.assistant.localres.LocalMediaLoader;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
class al implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LocalMediaLoader f1424a;
    final /* synthetic */ ArrayList b;
    final /* synthetic */ boolean c;
    final /* synthetic */ LocalMediaLoader d;

    al(LocalMediaLoader localMediaLoader, LocalMediaLoader localMediaLoader2, ArrayList arrayList, boolean z) {
        this.d = localMediaLoader;
        this.f1424a = localMediaLoader2;
        this.b = arrayList;
        this.c = z;
    }

    public void run() {
        synchronized (this.d.e) {
            Iterator<WeakReference<LocalMediaLoader.ILocalMediaLoaderListener<T>>> it = this.d.e.iterator();
            while (it.hasNext()) {
                LocalMediaLoader.ILocalMediaLoaderListener iLocalMediaLoaderListener = (LocalMediaLoader.ILocalMediaLoaderListener) it.next().get();
                if (iLocalMediaLoaderListener != null) {
                    iLocalMediaLoaderListener.onLocalMediaLoaderFinish(this.f1424a, this.b, this.c);
                }
            }
        }
    }
}
