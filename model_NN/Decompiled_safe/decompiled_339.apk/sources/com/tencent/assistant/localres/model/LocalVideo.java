package com.tencent.assistant.localres.model;

/* compiled from: ProGuard */
public class LocalVideo extends LocalMedia {
    public String album;
    public String artist;
    public String category;
    public int datetaken;
    public String description;
    public int duration;
    public String language;
    public double latitude;
    public double longitude;
    public boolean privateFlag;
    public String resolution;
    public String tags;
}
