package com.tencent.assistant.download;

import android.os.Bundle;
import android.text.TextUtils;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.f;
import com.tencent.assistant.manager.g;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.j;
import com.tencent.assistant.module.update.k;
import com.tencent.assistant.module.wisedownload.u;
import com.tencent.assistant.plugin.mgr.e;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.a.d;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.connect.common.Constants;
import com.tencent.downloadsdk.DownloadTask;
import com.tencent.f.a.a.a;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/* compiled from: ProGuard */
public class DownloadInfo extends SimpleDownloadInfo implements Cloneable, Comparable {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1245a = false;
    public byte actionFlag = 0;
    public long apkId = 0;
    public long appId = 0;
    public String appLinkActionUrl = Constants.STR_EMPTY;
    public boolean autoInstall = true;
    private Object b = new Object();
    public String channelId = Constants.STR_EMPTY;
    public boolean fromOutCall = false;
    public int grayVersionCode = 0;
    public String hostAppId = Constants.STR_EMPTY;
    public String hostPackageName = Constants.STR_EMPTY;
    public String hostVersionCode = Constants.STR_EMPTY;
    public int installType = 2;
    public byte isAutoOpen = 0;
    public int isUpdate = 0;
    public long localFlag = 0;
    public int localVersionCode;
    public int maxQLauncherVersionCode = 0;
    public int minQLauncherVersionCode = 0;
    public byte overWriteChannelId;
    public String packageName = Constants.STR_EMPTY;
    public String sdkId = Constants.STR_EMPTY;
    public String signatrue = null;
    public List<String> sllApkUrlList = new ArrayList();
    public String sllFileMd5 = Constants.STR_EMPTY;
    public long sllFileSize = 0;
    public String sllLocalManifestMd5;
    public int sllLocalVersionCode;
    public int sllUpdate = 0;
    public String taskId = Constants.STR_EMPTY;
    public int themeVersionCode = 0;
    public SimpleDownloadInfo.UIType uiType = SimpleDownloadInfo.UIType.NORMAL;
    public String uin = Constants.STR_EMPTY;
    public String uinType = Constants.STR_EMPTY;
    public int versionCode;
    public String versionName = null;
    public String via = Constants.STR_EMPTY;

    public String getTmpSaveName() {
        switch (l.f1262a[this.fileType.ordinal()]) {
            case 1:
                if (this.grayVersionCode <= 0) {
                    if (isSllUpdate()) {
                        return this.packageName + "_" + this.versionCode + ".patch" + Constants.STR_EMPTY;
                    }
                    return this.packageName + "_" + this.versionCode + ".apk" + Constants.STR_EMPTY;
                } else if (isSllUpdate()) {
                    return this.packageName + "_" + this.versionCode + "_" + this.grayVersionCode + ".patch" + Constants.STR_EMPTY;
                } else {
                    return this.packageName + "_" + this.versionCode + "_" + this.grayVersionCode + ".apk" + Constants.STR_EMPTY;
                }
            case 2:
                return e.c(this.packageName, this.versionCode);
            default:
                return this.packageName;
        }
    }

    public static String getDownloadingDir(SimpleDownloadInfo.DownloadType downloadType) {
        if (downloadType == SimpleDownloadInfo.DownloadType.PLUGIN) {
            return FileUtil.getDynamicAPKDir();
        }
        return FileUtil.getDynamicAPKDir();
    }

    private String a() {
        if (this.grayVersionCode <= 0) {
            return this.packageName + "_" + this.versionCode + "_2.apk";
        }
        return this.packageName + "_" + this.versionCode + "_" + this.grayVersionCode + "_2.apk";
    }

    public static DownloadTask.PRIORITY getPriority(SimpleDownloadInfo.DownloadType downloadType, SimpleDownloadInfo.UIType uIType) {
        switch (l.f1262a[downloadType.ordinal()]) {
            case 1:
                if (isUiTypeWiseDownload(uIType)) {
                    return DownloadTask.PRIORITY.NORMAL;
                }
                return DownloadTask.PRIORITY.HIGH;
            case 2:
                return DownloadTask.PRIORITY.URGENT;
            default:
                return DownloadTask.PRIORITY.NORMAL;
        }
    }

    public boolean isSuccApkFileExist() {
        String str = this.filePath;
        if (!TextUtils.isEmpty(str)) {
            return new File(str).exists();
        }
        return false;
    }

    @Deprecated
    public boolean isApkFileExist() {
        return isDownloadFileExist();
    }

    public boolean isDownloadFileExist() {
        if (isDownloadInfoNotFinish()) {
            return true;
        }
        String str = this.filePath;
        if (str == null) {
            return false;
        }
        return new File(str).exists();
    }

    public static DownloadInfo createDownloadInfo(SimpleAppModel simpleAppModel, StatInfo statInfo) {
        return createDownloadInfo(simpleAppModel, statInfo, SimpleDownloadInfo.UIType.NORMAL);
    }

    public static DownloadInfo createDownloadInfo(SimpleAppModel simpleAppModel, StatInfo statInfo, g... gVarArr) {
        if (!(simpleAppModel == null || gVarArr == null)) {
            for (g a2 : gVarArr) {
                f.a().a(simpleAppModel.q(), a2);
            }
        }
        return createDownloadInfo(simpleAppModel, statInfo, SimpleDownloadInfo.UIType.NORMAL);
    }

    public static DownloadInfo createDownloadInfo(SimpleAppModel simpleAppModel, StatInfo statInfo, SimpleDownloadInfo.UIType uIType) {
        if (simpleAppModel == null) {
            return null;
        }
        DownloadInfo downloadInfo = new DownloadInfo();
        downloadInfo.uiType = uIType;
        downloadInfo.fileType = SimpleDownloadInfo.DownloadType.APK;
        downloadInfo.downloadTicket = simpleAppModel.q();
        if (TextUtils.isEmpty(downloadInfo.downloadTicket)) {
            downloadInfo.downloadTicket = simpleAppModel.c;
        }
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(simpleAppModel.c);
        if (k.b().b(simpleAppModel.c) || (localApkInfo != null && localApkInfo.mVersionCode < simpleAppModel.g)) {
            downloadInfo.isUpdate = 1;
            if (simpleAppModel.a()) {
                downloadInfo.sllUpdate = 1;
            } else {
                downloadInfo.sllUpdate = 0;
            }
        } else {
            downloadInfo.isUpdate = 0;
        }
        downloadInfo.appId = simpleAppModel.f1634a;
        downloadInfo.apkId = simpleAppModel.b;
        downloadInfo.packageName = simpleAppModel.c;
        downloadInfo.name = simpleAppModel.d;
        downloadInfo.iconUrl = simpleAppModel.e;
        downloadInfo.versionName = simpleAppModel.f;
        downloadInfo.versionCode = simpleAppModel.g;
        if (ct.b(simpleAppModel.j)) {
            downloadInfo.apkUrlList.addAll(simpleAppModel.j);
        } else {
            downloadInfo.apkUrlList.add(simpleAppModel.i);
        }
        downloadInfo.fileSize = simpleAppModel.k;
        downloadInfo.fileMd5 = simpleAppModel.l;
        downloadInfo.signatrue = simpleAppModel.m;
        if (ct.b(simpleAppModel.u)) {
            downloadInfo.sllApkUrlList.addAll(simpleAppModel.u);
        } else {
            downloadInfo.sllApkUrlList.add(simpleAppModel.t);
        }
        downloadInfo.sllFileSize = simpleAppModel.v;
        downloadInfo.sllFileMd5 = simpleAppModel.w;
        downloadInfo.sllLocalManifestMd5 = simpleAppModel.z;
        downloadInfo.sllLocalVersionCode = simpleAppModel.A;
        if (statInfo != null) {
            downloadInfo.statInfo.callerVia = statInfo.callerVia;
            downloadInfo.statInfo.callerUin = statInfo.callerUin;
            downloadInfo.updateDownloadInfoStatInfo(statInfo);
        }
        downloadInfo.localVersionCode = simpleAppModel.D;
        downloadInfo.localFlag = simpleAppModel.K;
        downloadInfo.response.d = DownloadTask.PRIORITY.NORMAL;
        downloadInfo.response.f = getRandomPercent(downloadInfo);
        downloadInfo.channelId = simpleAppModel.ac;
        downloadInfo.grayVersionCode = simpleAppModel.ad;
        downloadInfo.actionFlag = simpleAppModel.Q;
        downloadInfo.sdkId = simpleAppModel.af;
        downloadInfo.categoryId = simpleAppModel.R;
        downloadInfo.minQLauncherVersionCode = simpleAppModel.ar;
        downloadInfo.maxQLauncherVersionCode = simpleAppModel.as;
        downloadInfo.themeVersionCode = simpleAppModel.at;
        downloadInfo.overWriteChannelId = simpleAppModel.ag;
        downloadInfo.isAutoOpen = simpleAppModel.aB;
        return downloadInfo;
    }

    public DownloadInfo createDownloadInfo(SimpleDownloadInfo.DownloadType downloadType) {
        DownloadInfo downloadInfo = new DownloadInfo();
        downloadInfo.fileType = downloadType;
        return downloadInfo;
    }

    public static DownloadInfo createDownloadInfo(j jVar) {
        DownloadInfo downloadInfo = new DownloadInfo();
        downloadInfo.fileType = SimpleDownloadInfo.DownloadType.PLUGIN;
        downloadInfo.appId = (long) jVar.f1666a;
        downloadInfo.packageName = jVar.c;
        downloadInfo.versionCode = jVar.d;
        downloadInfo.name = jVar.e;
        downloadInfo.fileSize = jVar.j;
        downloadInfo.apkUrlList.add(jVar.o);
        downloadInfo.downloadState = SimpleDownloadInfo.DownloadState.INIT;
        downloadInfo.downloadTicket = jVar.a();
        downloadInfo.uiType = SimpleDownloadInfo.UIType.PLUGIN_PREDOWNLOAD;
        return downloadInfo;
    }

    public static int getRandomPercent(DownloadInfo downloadInfo) {
        Random random = new Random();
        if (downloadInfo != null) {
            return random.nextInt(3) + 1;
        }
        return 0;
    }

    public DownloadInfo clone() {
        DownloadInfo downloadInfo;
        try {
            downloadInfo = (DownloadInfo) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.toString());
            downloadInfo = null;
        }
        if (this.response != null) {
            downloadInfo.response = this.response.clone();
        }
        return downloadInfo;
    }

    public void updateDownloadingApkSavePath(String str) {
        this.downloadingPath = str;
    }

    public synchronized boolean makeFinalFile() {
        boolean z = false;
        synchronized (this) {
            try {
                if (b()) {
                    LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(this.packageName);
                    String str = localApkInfo != null ? localApkInfo.mLocalFilePath : null;
                    if (this.downloadState == SimpleDownloadInfo.DownloadState.COMPLETE) {
                        this.filePath = getDownloadingDir(this.fileType) + File.separator + a();
                        XLog.d("jimluo", "overChannelId flag is: " + ((int) this.overWriteChannelId));
                        if (this.sllUpdate != 1) {
                            this.filePath = this.downloadingPath;
                            a(Byte.valueOf(this.overWriteChannelId), str, this.filePath);
                            this.downloadState = SimpleDownloadInfo.DownloadState.SUCC;
                            this.downloadEndTime = System.currentTimeMillis();
                            z = true;
                        } else if (d.a(this, this.filePath, this.downloadingPath) == 0) {
                            this.downloadState = SimpleDownloadInfo.DownloadState.SUCC;
                            this.downloadEndTime = System.currentTimeMillis();
                            a(Byte.valueOf(this.overWriteChannelId), str, this.filePath);
                            z = true;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return z;
    }

    private boolean b() {
        if (!TextUtils.isEmpty(this.downloadingPath)) {
            return new File(this.downloadingPath).exists();
        }
        return false;
    }

    public int checkCurrentDownloadSucc() {
        if (this.downloadState != SimpleDownloadInfo.DownloadState.SUCC || TextUtils.isEmpty(this.filePath)) {
            if (this.fileType != SimpleDownloadInfo.DownloadType.APK) {
                return 0;
            }
            String isApkFileValid = ApkResourceManager.getInstance().isApkFileValid(this.packageName, this.versionCode, this.grayVersionCode);
            if (TextUtils.isEmpty(isApkFileValid)) {
                return 0;
            }
            this.errorCode = 0;
            this.sllUpdate = 0;
            this.filePath = isApkFileValid;
            this.downloadEndTime = System.currentTimeMillis();
            if (!(this.downloadState == SimpleDownloadInfo.DownloadState.INSTALLED || this.downloadState == SimpleDownloadInfo.DownloadState.INSTALLING)) {
                this.downloadState = SimpleDownloadInfo.DownloadState.SUCC;
            }
            return 2;
        } else if (new File(this.filePath).exists()) {
            return 1;
        } else {
            return 0;
        }
    }

    public void initStartDownload() {
        if (this.createTime == 0) {
            this.createTime = System.currentTimeMillis();
        }
        if (this.response == null) {
            this.response = new m();
        }
        this.response.f = getRandomPercent(this);
    }

    private static void a(Byte b2, String str, String str2) {
        if (b2.byteValue() == 1 && !TextUtils.isEmpty(str)) {
            try {
                File file = new File(str);
                XLog.d("jimluo", "old Apk Channel Id is: " + a.a(file));
                if (file.exists()) {
                    a.a(str, str2);
                    XLog.d("jimluo", "new Channel Id is: " + a.a(new File(str2)));
                }
            } catch (Exception e) {
                XLog.d("DownloadTag", "rewrite msdk channel id fail:" + e.getMessage());
            }
        }
    }

    public boolean isUpdateApk() {
        return this.fileType == SimpleDownloadInfo.DownloadType.APK && this.isUpdate == 1;
    }

    public boolean isSllUpdateApk() {
        return this.fileType == SimpleDownloadInfo.DownloadType.APK && this.sllUpdate == 1;
    }

    public void updateToFullUpdate() {
        this.sllUpdate = 0;
    }

    public int getDownloadSubType() {
        if (this.isUpdate != 1) {
            return 2;
        }
        if (isSllUpdate()) {
            return 3;
        }
        return 1;
    }

    public String getCurrentValidPath() {
        if (isDownloadInfoNotFinish()) {
            return this.downloadingPath;
        }
        return this.filePath;
    }

    public int compareTo(Object obj) {
        if (obj == null || !(obj instanceof DownloadInfo) || this.createTime - ((DownloadInfo) obj).createTime <= 0) {
            return 1;
        }
        return -1;
    }

    public void initCallParamsFromActionBundle(Bundle bundle) {
        if (bundle != null) {
            this.hostAppId = bundle.getString(com.tencent.assistant.b.a.i);
            this.hostPackageName = bundle.getString(com.tencent.assistant.b.a.n);
            this.hostVersionCode = bundle.getString(com.tencent.assistant.b.a.o);
            this.via = bundle.getString(com.tencent.assistant.b.a.j);
            this.taskId = bundle.getString(com.tencent.assistant.b.a.l);
            this.uin = bundle.getString(com.tencent.assistant.b.a.y);
            this.uinType = bundle.getString(com.tencent.assistant.b.a.z);
            this.fromOutCall = true;
        }
    }

    public void updateDownloadInfoStatInfo(StatInfo statInfo) {
        this.statInfo.c = this.channelId;
        this.statInfo.d = this.actionFlag;
        if (statInfo != null) {
            this.statInfo.scene = statInfo.scene;
            this.statInfo.sourceScene = statInfo.sourceScene;
            this.statInfo.f3356a = statInfo.f3356a;
            this.statInfo.extraData = statInfo.extraData;
            this.statInfo.searchId = statInfo.searchId;
            this.statInfo.searchPreId = statInfo.searchPreId;
            this.statInfo.expatiation = statInfo.expatiation;
            this.statInfo.b = statInfo.b;
            this.statInfo.d = statInfo.d;
            this.statInfo.slotId = statInfo.slotId;
            this.statInfo.sourceSceneSlotId = statInfo.sourceSceneSlotId;
            this.statInfo.recommendId = statInfo.recommendId;
            this.statInfo.callerVersionCode = statInfo.callerVersionCode;
            this.statInfo.rankGroupId = statInfo.rankGroupId;
            this.statInfo.pushInfo = statInfo.pushInfo;
            this.statInfo.contentId = statInfo.contentId;
            if (!TextUtils.isEmpty(statInfo.traceId)) {
                this.statInfo.traceId = statInfo.traceId;
                return;
            }
            this.statInfo.traceId = com.tencent.assistant.st.f.f();
        }
    }

    public boolean ignoreState() {
        if (this.fileType == SimpleDownloadInfo.DownloadType.PLUGIN || (isUiTypeWiseDownload(this.uiType) && (this.downloadState == SimpleDownloadInfo.DownloadState.QUEUING || this.downloadState == SimpleDownloadInfo.DownloadState.DOWNLOADING || this.downloadState == SimpleDownloadInfo.DownloadState.PAUSED || this.downloadState == SimpleDownloadInfo.DownloadState.WAITTING_FOR_WIFI || this.downloadState == SimpleDownloadInfo.DownloadState.COMPLETE || this.downloadState == SimpleDownloadInfo.DownloadState.FAIL))) {
            return true;
        }
        return false;
    }

    public boolean isDownloadInfoNotFinish() {
        return (this.downloadState == SimpleDownloadInfo.DownloadState.SUCC || this.downloadState == SimpleDownloadInfo.DownloadState.INSTALLING || this.downloadState == SimpleDownloadInfo.DownloadState.INSTALLED) ? false : true;
    }

    public boolean isDownloaded() {
        if (this.downloadState == SimpleDownloadInfo.DownloadState.INSTALLING || this.downloadState == SimpleDownloadInfo.DownloadState.INSTALLED) {
            return true;
        }
        if (this.downloadState != SimpleDownloadInfo.DownloadState.SUCC) {
            return false;
        }
        String str = this.filePath;
        if (TextUtils.isEmpty(str) || !new File(str).exists()) {
            return false;
        }
        return true;
    }

    public boolean isDownloadedFile() {
        if (this.downloadState != SimpleDownloadInfo.DownloadState.SUCC && this.downloadState != SimpleDownloadInfo.DownloadState.INSTALLING && this.downloadState != SimpleDownloadInfo.DownloadState.INSTALLED) {
            return false;
        }
        String str = this.filePath;
        if (!TextUtils.isEmpty(str) && new File(str).exists()) {
            return true;
        }
        return false;
    }

    public boolean isUiTypeWiseDownload() {
        return this.uiType == SimpleDownloadInfo.UIType.WISE_APP_UPDATE || this.uiType == SimpleDownloadInfo.UIType.WISE_NEW_DOWNLOAD || this.uiType == SimpleDownloadInfo.UIType.WISE_SELF_UPDAET || this.uiType == SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD || this.uiType == SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD;
    }

    public static boolean isUiTypeWiseDownload(SimpleDownloadInfo.UIType uIType) {
        return uIType == SimpleDownloadInfo.UIType.WISE_APP_UPDATE || uIType == SimpleDownloadInfo.UIType.WISE_NEW_DOWNLOAD || uIType == SimpleDownloadInfo.UIType.WISE_SELF_UPDAET || uIType == SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD || uIType == SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD;
    }

    public boolean isUiTypeWiseUpdateDownload() {
        return this.uiType == SimpleDownloadInfo.UIType.WISE_APP_UPDATE || this.uiType == SimpleDownloadInfo.UIType.WISE_SELF_UPDAET;
    }

    public boolean isUiTypeWiseBookingDownload() {
        return this.uiType == SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD;
    }

    public boolean isUiTypeWiseSubscribtionDownloadAutoInstall() {
        return u.a(this);
    }

    public boolean isSllUpdate() {
        return this.sllUpdate == 1;
    }

    public boolean needReCreateInfo(SimpleAppModel simpleAppModel) {
        if (simpleAppModel == null || simpleAppModel.g < this.versionCode) {
            return false;
        }
        if (isDownloadFileExist() || ((TextUtils.isEmpty(simpleAppModel.ac) && TextUtils.isEmpty(this.channelId)) || !(simpleAppModel.ac == null || this.channelId == null || !simpleAppModel.ac.equals(this.channelId)))) {
            if (this.downloadState != SimpleDownloadInfo.DownloadState.FAIL) {
                return false;
            }
            if (this.response == null || this.response.f1263a == 0) {
                return true;
            }
            return false;
        }
        return true;
    }

    public boolean needReCreateInfo() {
        return this.response == null || this.response.f1263a == 0;
    }

    public boolean isUiTypeWiseAppUpdateDownload() {
        return this.uiType == SimpleDownloadInfo.UIType.WISE_APP_UPDATE;
    }

    public boolean isSslUpdate() {
        return this.sllUpdate == 1;
    }

    public void setSslUpdate(boolean z) {
        this.sllUpdate = z ? 1 : 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.assistant.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.model.j, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.u.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.w, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    public int getProgress() {
        if (SimpleDownloadInfo.isProgressShowFake(this, com.tencent.assistant.module.u.a(this, true, true))) {
            return this.response.f;
        }
        return getPercent(this);
    }

    public String toString() {
        return "DownloadInfo{appId=" + this.appId + ", apkId=" + this.apkId + ", packageName='" + this.packageName + '\'' + ", versionCode=" + this.versionCode + ", versionName='" + this.versionName + '\'' + ", sllApkUrlList=" + this.sllApkUrlList + ", sllFileSize=" + this.sllFileSize + ", sllFileMd5='" + this.sllFileMd5 + '\'' + ", sllUpdate=" + this.sllUpdate + ", localFlag=" + this.localFlag + ", isUpdate=" + this.isUpdate + ", signatrue='" + this.signatrue + '\'' + ", autoInstall=" + this.autoInstall + ", hostAppId='" + this.hostAppId + '\'' + ", hostPackageName='" + this.hostPackageName + '\'' + ", hostVersionCode='" + this.hostVersionCode + '\'' + ", via='" + this.via + '\'' + ", taskId='" + this.taskId + '\'' + ", uin='" + this.uin + '\'' + ", uinType='" + this.uinType + '\'' + ", channelId='" + this.channelId + '\'' + ", actionFlag=" + ((int) this.actionFlag) + ", uiType=" + this.uiType + ", sdkId='" + this.sdkId + '\'' + ", minQLauncherVersionCode=" + this.minQLauncherVersionCode + ", maxQLauncherVersionCode=" + this.maxQLauncherVersionCode + ", themeVersionCode=" + this.themeVersionCode + ", grayVersionCode=" + this.grayVersionCode + '}';
    }
}
