package com.tencent.assistant.download;

import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.be;

/* compiled from: ProGuard */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f1255a;
    final /* synthetic */ SimpleDownloadInfo.UIType b;
    final /* synthetic */ a c;

    e(a aVar, DownloadInfo downloadInfo, SimpleDownloadInfo.UIType uIType) {
        this.c = aVar;
        this.f1255a = downloadInfo;
        this.b = uIType;
    }

    public void run() {
        if (this.f1255a.uiType != this.b) {
            this.f1255a.uiType = this.b;
            DownloadProxy.a().d(this.f1255a);
        }
        if (be.a().b(this.f1255a)) {
            be.a().d(this.f1255a);
            return;
        }
        if (this.f1255a.downloadState == SimpleDownloadInfo.DownloadState.WAITTING_FOR_WIFI) {
            this.f1255a.downloadState = SimpleDownloadInfo.DownloadState.INIT;
        }
        DownloadProxy.a().c(this.f1255a);
    }
}
