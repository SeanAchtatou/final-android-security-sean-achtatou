package com.tencent.assistant.download;

import android.text.TextUtils;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.utils.ao;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1257a;
    final /* synthetic */ a b;

    g(a aVar, String str) {
        this.b = aVar;
        this.f1257a = str;
    }

    public void run() {
        DownloadInfo d;
        if (!TextUtils.isEmpty(this.f1257a) && (d = DownloadProxy.a().d(this.f1257a)) != null) {
            boolean z = false;
            try {
                z = d.makeFinalFile();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (z) {
                DownloadProxy.a().d(d);
                a.d.sendMessage(a.d.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this.f1257a));
                ao.a().a(d.packageName);
                return;
            }
            a.a().a(this.f1257a);
        }
    }
}
