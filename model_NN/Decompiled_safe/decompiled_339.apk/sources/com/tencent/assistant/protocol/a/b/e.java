package com.tencent.assistant.protocol.a.b;

import com.tencent.assistant.protocol.a.b;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class e implements b {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<Integer> f1962a = new ArrayList<>();
    private int[] b = {57, 3, 31};

    public e() {
        for (int valueOf : this.b) {
            this.f1962a.add(Integer.valueOf(valueOf));
        }
    }

    public boolean a(int i) {
        return this.f1962a.contains(Integer.valueOf(i));
    }
}
