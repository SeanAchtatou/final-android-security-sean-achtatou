package com.tencent.assistant.protocol.jce;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import com.tencent.tauth.AuthActivity;

/* compiled from: ProGuard */
public final class StatItemForCommon extends JceStruct implements Cloneable {
    static byte[] K;
    static final /* synthetic */ boolean L = (!StatItemForCommon.class.desiredAssertionStatus());
    public String A = Constants.STR_EMPTY;
    public String B = Constants.STR_EMPTY;
    public String C = Constants.STR_EMPTY;
    public String D = Constants.STR_EMPTY;
    public String E = Constants.STR_EMPTY;
    public String F = Constants.STR_EMPTY;
    public String G = Constants.STR_EMPTY;
    public String H = Constants.STR_EMPTY;
    public String I = Constants.STR_EMPTY;
    public String J = Constants.STR_EMPTY;

    /* renamed from: a  reason: collision with root package name */
    public int f2365a = 0;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    public String d = Constants.STR_EMPTY;
    public String e = Constants.STR_EMPTY;
    public String f = Constants.STR_EMPTY;
    public String g = Constants.STR_EMPTY;
    public String h = Constants.STR_EMPTY;
    public String i = Constants.STR_EMPTY;
    public String j = Constants.STR_EMPTY;
    public String k = Constants.STR_EMPTY;
    public String l = Constants.STR_EMPTY;
    public String m = Constants.STR_EMPTY;
    public String n = Constants.STR_EMPTY;
    public String o = Constants.STR_EMPTY;
    public String p = Constants.STR_EMPTY;
    public String q = Constants.STR_EMPTY;
    public String r = Constants.STR_EMPTY;
    public String s = Constants.STR_EMPTY;
    public String t = Constants.STR_EMPTY;
    public byte[] u = null;
    public String v = Constants.STR_EMPTY;
    public String w = Constants.STR_EMPTY;
    public String x = Constants.STR_EMPTY;
    public String y = Constants.STR_EMPTY;
    public String z = Constants.STR_EMPTY;

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        StatItemForCommon statItemForCommon = (StatItemForCommon) obj;
        if (!JceUtil.equals(this.f2365a, statItemForCommon.f2365a) || !JceUtil.equals(this.b, statItemForCommon.b) || !JceUtil.equals(this.c, statItemForCommon.c) || !JceUtil.equals(this.d, statItemForCommon.d) || !JceUtil.equals(this.e, statItemForCommon.e) || !JceUtil.equals(this.f, statItemForCommon.f) || !JceUtil.equals(this.g, statItemForCommon.g) || !JceUtil.equals(this.h, statItemForCommon.h) || !JceUtil.equals(this.i, statItemForCommon.i) || !JceUtil.equals(this.j, statItemForCommon.j) || !JceUtil.equals(this.k, statItemForCommon.k) || !JceUtil.equals(this.l, statItemForCommon.l) || !JceUtil.equals(this.m, statItemForCommon.m) || !JceUtil.equals(this.n, statItemForCommon.n) || !JceUtil.equals(this.o, statItemForCommon.o) || !JceUtil.equals(this.p, statItemForCommon.p) || !JceUtil.equals(this.q, statItemForCommon.q) || !JceUtil.equals(this.r, statItemForCommon.r) || !JceUtil.equals(this.s, statItemForCommon.s) || !JceUtil.equals(this.t, statItemForCommon.t) || !JceUtil.equals(this.u, statItemForCommon.u) || !JceUtil.equals(this.v, statItemForCommon.v) || !JceUtil.equals(this.w, statItemForCommon.w) || !JceUtil.equals(this.x, statItemForCommon.x) || !JceUtil.equals(this.y, statItemForCommon.y) || !JceUtil.equals(this.z, statItemForCommon.z) || !JceUtil.equals(this.A, statItemForCommon.A) || !JceUtil.equals(this.B, statItemForCommon.B) || !JceUtil.equals(this.C, statItemForCommon.C) || !JceUtil.equals(this.D, statItemForCommon.D) || !JceUtil.equals(this.E, statItemForCommon.E) || !JceUtil.equals(this.F, statItemForCommon.F) || !JceUtil.equals(this.G, statItemForCommon.G) || !JceUtil.equals(this.H, statItemForCommon.H) || !JceUtil.equals(this.I, statItemForCommon.I) || !JceUtil.equals(this.J, statItemForCommon.J)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            if (L) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.f2365a, 0);
        if (this.b != null) {
            jceOutputStream.write(this.b, 1);
        }
        if (this.c != null) {
            jceOutputStream.write(this.c, 2);
        }
        if (this.d != null) {
            jceOutputStream.write(this.d, 3);
        }
        if (this.e != null) {
            jceOutputStream.write(this.e, 4);
        }
        if (this.f != null) {
            jceOutputStream.write(this.f, 5);
        }
        if (this.g != null) {
            jceOutputStream.write(this.g, 6);
        }
        if (this.h != null) {
            jceOutputStream.write(this.h, 7);
        }
        if (this.i != null) {
            jceOutputStream.write(this.i, 8);
        }
        if (this.j != null) {
            jceOutputStream.write(this.j, 9);
        }
        if (this.k != null) {
            jceOutputStream.write(this.k, 10);
        }
        if (this.l != null) {
            jceOutputStream.write(this.l, 11);
        }
        if (this.m != null) {
            jceOutputStream.write(this.m, 12);
        }
        if (this.n != null) {
            jceOutputStream.write(this.n, 13);
        }
        if (this.o != null) {
            jceOutputStream.write(this.o, 14);
        }
        if (this.p != null) {
            jceOutputStream.write(this.p, 15);
        }
        if (this.q != null) {
            jceOutputStream.write(this.q, 16);
        }
        if (this.r != null) {
            jceOutputStream.write(this.r, 17);
        }
        if (this.s != null) {
            jceOutputStream.write(this.s, 18);
        }
        if (this.t != null) {
            jceOutputStream.write(this.t, 19);
        }
        if (this.u != null) {
            jceOutputStream.write(this.u, 20);
        }
        if (this.v != null) {
            jceOutputStream.write(this.v, 21);
        }
        if (this.w != null) {
            jceOutputStream.write(this.w, 22);
        }
        if (this.x != null) {
            jceOutputStream.write(this.x, 23);
        }
        if (this.y != null) {
            jceOutputStream.write(this.y, 24);
        }
        if (this.z != null) {
            jceOutputStream.write(this.z, 25);
        }
        if (this.A != null) {
            jceOutputStream.write(this.A, 26);
        }
        if (this.B != null) {
            jceOutputStream.write(this.B, 27);
        }
        if (this.C != null) {
            jceOutputStream.write(this.C, 28);
        }
        if (this.D != null) {
            jceOutputStream.write(this.D, 29);
        }
        if (this.E != null) {
            jceOutputStream.write(this.E, 30);
        }
        if (this.F != null) {
            jceOutputStream.write(this.F, 31);
        }
        if (this.G != null) {
            jceOutputStream.write(this.G, 32);
        }
        if (this.H != null) {
            jceOutputStream.write(this.H, 33);
        }
        if (this.I != null) {
            jceOutputStream.write(this.I, 34);
        }
        if (this.J != null) {
            jceOutputStream.write(this.J, 35);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
     arg types: [byte[], int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[] */
    public void readFrom(JceInputStream jceInputStream) {
        this.f2365a = jceInputStream.read(this.f2365a, 0, true);
        this.b = jceInputStream.readString(1, false);
        this.c = jceInputStream.readString(2, false);
        this.d = jceInputStream.readString(3, false);
        this.e = jceInputStream.readString(4, false);
        this.f = jceInputStream.readString(5, false);
        this.g = jceInputStream.readString(6, false);
        this.h = jceInputStream.readString(7, false);
        this.i = jceInputStream.readString(8, false);
        this.j = jceInputStream.readString(9, false);
        this.k = jceInputStream.readString(10, false);
        this.l = jceInputStream.readString(11, false);
        this.m = jceInputStream.readString(12, false);
        this.n = jceInputStream.readString(13, false);
        this.o = jceInputStream.readString(14, false);
        this.p = jceInputStream.readString(15, false);
        this.q = jceInputStream.readString(16, false);
        this.r = jceInputStream.readString(17, false);
        this.s = jceInputStream.readString(18, false);
        this.t = jceInputStream.readString(19, false);
        if (K == null) {
            K = new byte[1];
            K[0] = 0;
        }
        this.u = jceInputStream.read(K, 20, false);
        this.v = jceInputStream.readString(21, false);
        this.w = jceInputStream.readString(22, false);
        this.x = jceInputStream.readString(23, false);
        this.y = jceInputStream.readString(24, false);
        this.z = jceInputStream.readString(25, false);
        this.A = jceInputStream.readString(26, false);
        this.B = jceInputStream.readString(27, false);
        this.C = jceInputStream.readString(28, false);
        this.D = jceInputStream.readString(29, false);
        this.E = jceInputStream.readString(30, false);
        this.F = jceInputStream.readString(31, false);
        this.G = jceInputStream.readString(32, false);
        this.H = jceInputStream.readString(33, false);
        this.I = jceInputStream.readString(34, false);
        this.J = jceInputStream.readString(35, false);
    }

    public void display(StringBuilder sb, int i2) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i2);
        jceDisplayer.display(this.f2365a, SocialConstants.PARAM_TYPE);
        jceDisplayer.display(this.b, "phoneGuid");
        jceDisplayer.display(this.c, "clientIp");
        jceDisplayer.display(this.d, "imei");
        jceDisplayer.display(this.e, "macAddress");
        jceDisplayer.display(this.f, "androidId");
        jceDisplayer.display(this.g, "androidIdSdCard");
        jceDisplayer.display(this.h, "imsi");
        jceDisplayer.display(this.i, "qua");
        jceDisplayer.display(this.j, "netType");
        jceDisplayer.display(this.k, "extNetworkOperator");
        jceDisplayer.display(this.l, "extNetworkType");
        jceDisplayer.display(this.m, "uin");
        jceDisplayer.display(this.n, "appId");
        jceDisplayer.display(this.o, CommentDetailTabView.PARAMS_APK_ID);
        jceDisplayer.display(this.p, "pkgName");
        jceDisplayer.display(this.q, AuthActivity.ACTION_KEY);
        jceDisplayer.display(this.r, "scene");
        jceDisplayer.display(this.s, "sourceScene");
        jceDisplayer.display(this.t, "via");
        jceDisplayer.display(this.u, "recommendData");
        jceDisplayer.display(this.v, "slot");
        jceDisplayer.display(this.w, "oper_time");
        jceDisplayer.display(this.x, "speeds");
        jceDisplayer.display(this.y, "viewId");
        jceDisplayer.display(this.z, "retCode");
        jceDisplayer.display(this.A, "clientTime");
        jceDisplayer.display(this.B, "savePackage");
        jceDisplayer.display(this.C, AppConst.KEY_OPENID);
        jceDisplayer.display(this.D, "token");
        jceDisplayer.display(this.E, "verifyType");
        jceDisplayer.display(this.F, "verifyResult");
        jceDisplayer.display(this.G, "actionFlag");
        jceDisplayer.display(this.H, "authPlatform");
        jceDisplayer.display(this.I, "authResult");
        jceDisplayer.display(this.J, "caller");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [int, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [java.lang.String, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
     arg types: [byte[], int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer */
    public void displaySimple(StringBuilder sb, int i2) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i2);
        jceDisplayer.displaySimple(this.f2365a, true);
        jceDisplayer.displaySimple(this.b, true);
        jceDisplayer.displaySimple(this.c, true);
        jceDisplayer.displaySimple(this.d, true);
        jceDisplayer.displaySimple(this.e, true);
        jceDisplayer.displaySimple(this.f, true);
        jceDisplayer.displaySimple(this.g, true);
        jceDisplayer.displaySimple(this.h, true);
        jceDisplayer.displaySimple(this.i, true);
        jceDisplayer.displaySimple(this.j, true);
        jceDisplayer.displaySimple(this.k, true);
        jceDisplayer.displaySimple(this.l, true);
        jceDisplayer.displaySimple(this.m, true);
        jceDisplayer.displaySimple(this.n, true);
        jceDisplayer.displaySimple(this.o, true);
        jceDisplayer.displaySimple(this.p, true);
        jceDisplayer.displaySimple(this.q, true);
        jceDisplayer.displaySimple(this.r, true);
        jceDisplayer.displaySimple(this.s, true);
        jceDisplayer.displaySimple(this.t, true);
        jceDisplayer.displaySimple(this.u, true);
        jceDisplayer.displaySimple(this.v, true);
        jceDisplayer.displaySimple(this.w, true);
        jceDisplayer.displaySimple(this.x, true);
        jceDisplayer.displaySimple(this.y, true);
        jceDisplayer.displaySimple(this.z, true);
        jceDisplayer.displaySimple(this.A, true);
        jceDisplayer.displaySimple(this.B, true);
        jceDisplayer.displaySimple(this.C, true);
        jceDisplayer.displaySimple(this.D, true);
        jceDisplayer.displaySimple(this.E, true);
        jceDisplayer.displaySimple(this.F, true);
        jceDisplayer.displaySimple(this.G, true);
        jceDisplayer.displaySimple(this.H, true);
        jceDisplayer.displaySimple(this.I, true);
        jceDisplayer.displaySimple(this.J, false);
    }
}
