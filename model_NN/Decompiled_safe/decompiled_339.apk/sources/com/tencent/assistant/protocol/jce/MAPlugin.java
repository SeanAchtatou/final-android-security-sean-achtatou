package com.tencent.assistant.protocol.jce;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public final class MAPlugin extends JceStruct implements Cloneable {
    static ActionUrl B;
    static Map<Integer, Integer> C;
    static Map<String, Integer> D;
    static Map<String, Integer> E;
    static final /* synthetic */ boolean F = (!MAPlugin.class.desiredAssertionStatus());
    public Map<String, Integer> A = null;

    /* renamed from: a  reason: collision with root package name */
    public int f2233a = 0;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    public String d = Constants.STR_EMPTY;
    public String e = Constants.STR_EMPTY;
    public long f = 0;
    public int g = 0;
    public int h = 0;
    public String i = Constants.STR_EMPTY;
    public byte j = 0;
    public String k = Constants.STR_EMPTY;
    public String l = Constants.STR_EMPTY;
    public String m = Constants.STR_EMPTY;
    public int n = 0;
    public int o = 0;
    public int p = 0;
    public int q = 0;
    public String r = Constants.STR_EMPTY;
    public long s = 0;
    public byte t = 0;
    public ActionUrl u = null;
    public int v = 0;
    public int w = 0;
    public int x = 0;
    public Map<Integer, Integer> y = null;
    public Map<String, Integer> z = null;

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        MAPlugin mAPlugin = (MAPlugin) obj;
        if (!JceUtil.equals(this.f2233a, mAPlugin.f2233a) || !JceUtil.equals(this.b, mAPlugin.b) || !JceUtil.equals(this.c, mAPlugin.c) || !JceUtil.equals(this.d, mAPlugin.d) || !JceUtil.equals(this.e, mAPlugin.e) || !JceUtil.equals(this.f, mAPlugin.f) || !JceUtil.equals(this.g, mAPlugin.g) || !JceUtil.equals(this.h, mAPlugin.h) || !JceUtil.equals(this.i, mAPlugin.i) || !JceUtil.equals(this.j, mAPlugin.j) || !JceUtil.equals(this.k, mAPlugin.k) || !JceUtil.equals(this.l, mAPlugin.l) || !JceUtil.equals(this.m, mAPlugin.m) || !JceUtil.equals(this.n, mAPlugin.n) || !JceUtil.equals(this.o, mAPlugin.o) || !JceUtil.equals(this.p, mAPlugin.p) || !JceUtil.equals(this.q, mAPlugin.q) || !JceUtil.equals(this.r, mAPlugin.r) || !JceUtil.equals(this.s, mAPlugin.s) || !JceUtil.equals(this.t, mAPlugin.t) || !JceUtil.equals(this.u, mAPlugin.u) || !JceUtil.equals(this.v, mAPlugin.v) || !JceUtil.equals(this.w, mAPlugin.w) || !JceUtil.equals(this.x, mAPlugin.x) || !JceUtil.equals(this.y, mAPlugin.y) || !JceUtil.equals(this.z, mAPlugin.z) || !JceUtil.equals(this.A, mAPlugin.A)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            if (F) {
                return null;
            }
            throw new AssertionError();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
     arg types: [com.tencent.assistant.protocol.jce.ActionUrl, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.Integer, java.lang.Integer>, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.Integer>, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void */
    public void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.f2233a, 0);
        jceOutputStream.write(this.b, 1);
        jceOutputStream.write(this.c, 2);
        jceOutputStream.write(this.d, 3);
        jceOutputStream.write(this.e, 4);
        jceOutputStream.write(this.f, 6);
        jceOutputStream.write(this.g, 7);
        jceOutputStream.write(this.h, 8);
        jceOutputStream.write(this.i, 9);
        jceOutputStream.write(this.j, 10);
        jceOutputStream.write(this.k, 11);
        jceOutputStream.write(this.l, 12);
        jceOutputStream.write(this.m, 13);
        jceOutputStream.write(this.n, 14);
        jceOutputStream.write(this.o, 15);
        jceOutputStream.write(this.p, 16);
        jceOutputStream.write(this.q, 17);
        if (this.r != null) {
            jceOutputStream.write(this.r, 18);
        }
        jceOutputStream.write(this.s, 19);
        jceOutputStream.write(this.t, 20);
        if (this.u != null) {
            jceOutputStream.write((JceStruct) this.u, 21);
        }
        jceOutputStream.write(this.v, 22);
        jceOutputStream.write(this.w, 23);
        jceOutputStream.write(this.x, 24);
        if (this.y != null) {
            jceOutputStream.write((Map) this.y, 25);
        }
        if (this.z != null) {
            jceOutputStream.write((Map) this.z, 26);
        }
        if (this.A != null) {
            jceOutputStream.write((Map) this.A, 27);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
     arg types: [com.tencent.assistant.protocol.jce.ActionUrl, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.Integer, java.lang.Integer>, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, java.lang.Integer>, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object */
    public void readFrom(JceInputStream jceInputStream) {
        this.f2233a = jceInputStream.read(this.f2233a, 0, true);
        this.b = jceInputStream.readString(1, true);
        this.c = jceInputStream.readString(2, true);
        this.d = jceInputStream.readString(3, true);
        this.e = jceInputStream.readString(4, true);
        this.f = jceInputStream.read(this.f, 6, true);
        this.g = jceInputStream.read(this.g, 7, true);
        this.h = jceInputStream.read(this.h, 8, true);
        this.i = jceInputStream.readString(9, true);
        this.j = jceInputStream.read(this.j, 10, true);
        this.k = jceInputStream.readString(11, true);
        this.l = jceInputStream.readString(12, true);
        this.m = jceInputStream.readString(13, true);
        this.n = jceInputStream.read(this.n, 14, false);
        this.o = jceInputStream.read(this.o, 15, false);
        this.p = jceInputStream.read(this.p, 16, false);
        this.q = jceInputStream.read(this.q, 17, false);
        this.r = jceInputStream.readString(18, false);
        this.s = jceInputStream.read(this.s, 19, false);
        this.t = jceInputStream.read(this.t, 20, false);
        if (B == null) {
            B = new ActionUrl();
        }
        this.u = (ActionUrl) jceInputStream.read((JceStruct) B, 21, false);
        this.v = jceInputStream.read(this.v, 22, false);
        this.w = jceInputStream.read(this.w, 23, false);
        this.x = jceInputStream.read(this.x, 24, false);
        if (C == null) {
            C = new HashMap();
            C.put(0, 0);
        }
        this.y = (Map) jceInputStream.read((Object) C, 25, false);
        if (D == null) {
            D = new HashMap();
            D.put(Constants.STR_EMPTY, 0);
        }
        this.z = (Map) jceInputStream.read((Object) D, 26, false);
        if (E == null) {
            E = new HashMap();
            E.put(Constants.STR_EMPTY, 0);
        }
        this.A = (Map) jceInputStream.read((Object) E, 27, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.display(com.qq.taf.jce.JceStruct, java.lang.String):com.qq.taf.jce.JceDisplayer
     arg types: [com.tencent.assistant.protocol.jce.ActionUrl, java.lang.String]
     candidates:
      com.qq.taf.jce.JceDisplayer.display(byte, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.String, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Collection, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Map, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(boolean, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(byte[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(com.qq.taf.jce.JceStruct, java.lang.String):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.display(java.util.Map, java.lang.String):com.qq.taf.jce.JceDisplayer
     arg types: [java.util.Map<java.lang.Integer, java.lang.Integer>, java.lang.String]
     candidates:
      com.qq.taf.jce.JceDisplayer.display(byte, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(com.qq.taf.jce.JceStruct, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.String, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Collection, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(boolean, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(byte[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Map, java.lang.String):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.display(java.util.Map, java.lang.String):com.qq.taf.jce.JceDisplayer
     arg types: [java.util.Map<java.lang.String, java.lang.Integer>, java.lang.String]
     candidates:
      com.qq.taf.jce.JceDisplayer.display(byte, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(com.qq.taf.jce.JceStruct, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.String, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Collection, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(boolean, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(byte[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Map, java.lang.String):com.qq.taf.jce.JceDisplayer */
    public void display(StringBuilder sb, int i2) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i2);
        jceDisplayer.display(this.f2233a, "pluginId");
        jceDisplayer.display(this.b, "iconUrl");
        jceDisplayer.display(this.c, "name");
        jceDisplayer.display(this.d, SocialConstants.PARAM_APP_DESC);
        jceDisplayer.display(this.e, "md5");
        jceDisplayer.display(this.f, "filesize");
        jceDisplayer.display(this.g, "version");
        jceDisplayer.display(this.h, "minFrameworkVersion");
        jceDisplayer.display(this.i, "downloadUrl");
        jceDisplayer.display(this.j, SocialConstants.PARAM_TYPE);
        jceDisplayer.display(this.k, "versionName");
        jceDisplayer.display(this.l, "lanchActivity");
        jceDisplayer.display(this.m, "packageName");
        jceDisplayer.display(this.n, "displayOrder");
        jceDisplayer.display(this.o, "minApiLevel");
        jceDisplayer.display(this.p, "minBaoVersion");
        jceDisplayer.display(this.q, "needPreDownload");
        jceDisplayer.display(this.r, "imgUrl");
        jceDisplayer.display(this.s, "id");
        jceDisplayer.display(this.t, "pluginForBaoType");
        jceDisplayer.display((JceStruct) this.u, "actionUrl");
        jceDisplayer.display(this.v, "maxBaoVersion");
        jceDisplayer.display(this.w, "grayLimitPerDay");
        jceDisplayer.display(this.x, "grayPolicySwitch");
        jceDisplayer.display((Map) this.y, "appVersionGrayMap");
        jceDisplayer.display((Map) this.z, "buildNumberGrayMap");
        jceDisplayer.display((Map) this.A, "channelIdGrayMap");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [int, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [java.lang.String, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [long, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [byte, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [com.tencent.assistant.protocol.jce.ActionUrl, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [java.util.Map<java.lang.Integer, java.lang.Integer>, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [java.util.Map<java.lang.String, java.lang.Integer>, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer */
    public void displaySimple(StringBuilder sb, int i2) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i2);
        jceDisplayer.displaySimple(this.f2233a, true);
        jceDisplayer.displaySimple(this.b, true);
        jceDisplayer.displaySimple(this.c, true);
        jceDisplayer.displaySimple(this.d, true);
        jceDisplayer.displaySimple(this.e, true);
        jceDisplayer.displaySimple(this.f, true);
        jceDisplayer.displaySimple(this.g, true);
        jceDisplayer.displaySimple(this.h, true);
        jceDisplayer.displaySimple(this.i, true);
        jceDisplayer.displaySimple(this.j, true);
        jceDisplayer.displaySimple(this.k, true);
        jceDisplayer.displaySimple(this.l, true);
        jceDisplayer.displaySimple(this.m, true);
        jceDisplayer.displaySimple(this.n, true);
        jceDisplayer.displaySimple(this.o, true);
        jceDisplayer.displaySimple(this.p, true);
        jceDisplayer.displaySimple(this.q, true);
        jceDisplayer.displaySimple(this.r, true);
        jceDisplayer.displaySimple(this.s, true);
        jceDisplayer.displaySimple(this.t, true);
        jceDisplayer.displaySimple((JceStruct) this.u, true);
        jceDisplayer.displaySimple(this.v, true);
        jceDisplayer.displaySimple(this.w, true);
        jceDisplayer.displaySimple(this.x, true);
        jceDisplayer.displaySimple((Map) this.y, true);
        jceDisplayer.displaySimple((Map) this.z, true);
        jceDisplayer.displaySimple((Map) this.A, false);
    }
}
