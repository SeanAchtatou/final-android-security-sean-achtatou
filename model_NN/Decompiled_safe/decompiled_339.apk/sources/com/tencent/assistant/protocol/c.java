package com.tencent.assistant.protocol;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import com.tencent.assistant.protocol.a.e;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;

/* compiled from: ProGuard */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private static String f1965a = "HttpClientUtils";
    /* access modifiers changed from: private */
    public static int b = 3;

    public static DefaultHttpClient a() {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        ConnManagerParams.setMaxTotalConnections(basicHttpParams, 100);
        ConnManagerParams.setMaxConnectionsPerRoute(basicHttpParams, new ConnPerRouteBean(50));
        ConnManagerParams.setTimeout(basicHttpParams, 1000);
        HttpProtocolParams.setUseExpectContinue(basicHttpParams, false);
        HttpConnectionParams.setStaleCheckingEnabled(basicHttpParams, false);
        HttpConnectionParams.setSocketBufferSize(basicHttpParams, 8192);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schemeRegistry.register(new Scheme("https", PlainSocketFactory.getSocketFactory(), 80));
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
        a((HttpClient) defaultHttpClient);
        b((HttpClient) defaultHttpClient);
        a(defaultHttpClient);
        try {
            b(defaultHttpClient);
        } catch (Throwable th) {
        }
        return defaultHttpClient;
    }

    public static void a(HttpClient httpClient) {
        int i;
        int i2;
        Context g = e.a().g();
        if (g != null) {
            try {
                i = g.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE");
            } catch (Exception e) {
                e.printStackTrace();
                i = -1;
            }
            if (i == 0) {
                try {
                    NetworkInfo activeNetworkInfo = ((ConnectivityManager) g.getSystemService("connectivity")).getActiveNetworkInfo();
                    i2 = activeNetworkInfo == null ? 0 : activeNetworkInfo.getType();
                } catch (Throwable th) {
                    i2 = 0;
                }
                if (i2 == 0) {
                    String defaultHost = Proxy.getDefaultHost();
                    int defaultPort = Proxy.getDefaultPort();
                    if (!(defaultHost == null || defaultPort == -1)) {
                        httpClient.getParams().setParameter("http.route.default-proxy", new HttpHost(defaultHost, defaultPort));
                        return;
                    }
                }
                httpClient.getParams().setParameter("http.route.default-proxy", null);
            }
        }
    }

    public static void b(HttpClient httpClient) {
        int i = 30;
        int i2 = 10;
        if (com.tencent.assistant.net.c.d()) {
            i2 = 5;
        } else if (com.tencent.assistant.net.c.f()) {
            i = 40;
        } else if (com.tencent.assistant.net.c.e()) {
            i2 = 15;
            i = 45;
        }
        httpClient.getParams().setParameter("http.connection.timeout", Integer.valueOf(i2 * 1000));
        httpClient.getParams().setParameter("http.socket.timeout", Integer.valueOf(i * 1000));
    }

    public static boolean a(HttpResponse httpResponse) {
        String a2 = a(httpResponse, "Content-Type");
        return a2 != null && a2.startsWith("text");
    }

    public static String a(HttpResponse httpResponse, String str) {
        Header[] headers = httpResponse.getHeaders(str);
        if (headers != null && headers.length > 0) {
            for (Header header : headers) {
                if (header.getName().equalsIgnoreCase(str)) {
                    return header.getValue();
                }
            }
        }
        return null;
    }

    private static void a(DefaultHttpClient defaultHttpClient) {
        defaultHttpClient.setHttpRequestRetryHandler(new d());
    }

    private static void b(DefaultHttpClient defaultHttpClient) {
        defaultHttpClient.setKeepAliveStrategy(new e());
    }
}
