package com.tencent.assistant.activity;

import android.content.Context;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.StartPopWindowGridViewAdapter;
import com.tencent.assistant.b.a;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.timer.RecoverAppListReceiver;
import com.tencent.assistant.module.u;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.protocol.jce.PopUpInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.bx;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.df;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class StartPopWindowActivity extends BaseActivity {
    private TextView A;
    /* access modifiers changed from: private */
    public TextView B;
    private TextView C;
    private TextView D = null;
    /* access modifiers changed from: private */
    public StartPopWindowGridViewAdapter E = null;
    private PopUpInfo F = null;
    private boolean G = false;
    private GetPhoneUserAppListResponse H;
    /* access modifiers changed from: private */
    public List<SimpleAppModel> I = null;
    /* access modifiers changed from: private */
    public Context J;
    private boolean K = false;
    private boolean L = false;
    private boolean M = false;
    private long N = -1;
    gx n = new gx(this, null);
    private View t;
    /* access modifiers changed from: private */
    public View u;
    private View v;
    private View w;
    private TextView x = null;
    /* access modifiers changed from: private */
    public GridView y = null;
    private TextView z;

    public int f() {
        if (this.G && this.H != null) {
            switch (this.H.b) {
                case 1:
                    return STConst.ST_PAGE_GUIDE_RESULT_BIBEI_OLD_CHANGE;
                case 2:
                    return STConst.ST_PAGE_GUIDE_RESULT_BIBEI_OLD_POP;
                case 3:
                    return STConst.ST_PAGE_GUIDE_RESULT_BIBEI_NEW_CHANGE;
                case 4:
                    return STConst.ST_PAGE_GUIDE_RESULT_BIBEI_NEW_POP;
            }
        }
        return STConst.ST_PAGE_NECESSARY_NORMAL;
    }

    public boolean g() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        x();
        super.onCreate(bundle);
        this.J = this;
        try {
            setContentView((int) R.layout.start_pop_window_layout);
            getWindow().setLayout(-1, -1);
            y();
            v();
            XLog.v("recommendId", "startPop--onCreate--");
            a(new gm(this));
        } catch (Exception e) {
            finish();
            this.K = true;
        }
    }

    /* access modifiers changed from: protected */
    public void h() {
        STInfoV2 s = s();
        c.j();
        s.status = c.d() ? "01" : "02";
        k.a(s);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.K) {
            z();
            j();
        }
    }

    private void v() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.wifi.STATE_CHANGE");
        registerReceiver(this.n, intentFilter);
    }

    /* access modifiers changed from: private */
    public void w() {
        XLog.v("recommendId", "startPop--activityExposureST--");
        if (this.I != null && this.I.size() > 0) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.I.size()) {
                    SimpleAppModel simpleAppModel = this.I.get(i2);
                    STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.J, 100);
                    if (buildSTInfo != null) {
                        if (simpleAppModel != null) {
                            buildSTInfo.updateWithSimpleAppModel(simpleAppModel);
                            buildSTInfo.extraData = simpleAppModel.c + "|" + simpleAppModel.g;
                        }
                        buildSTInfo.slotId = b(i2);
                        k.a(buildSTInfo);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    private String b(int i) {
        return "03_" + ct.a(i + 1);
    }

    private void x() {
        try {
            this.G = getIntent().getBooleanExtra("extra_is_recover", false);
            if (this.G) {
                this.H = (GetPhoneUserAppListResponse) getIntent().getSerializableExtra("extra_pop_info");
                if (getIntent().getBooleanExtra(a.G, false)) {
                    bx.a();
                    com.tencent.assistantv2.st.page.c.a(122, 14, false);
                }
                this.L = m.a().a("key_re_app_list_state", 0) == RecoverAppListReceiver.RecoverAppListState.SECONDLY_SHORT.ordinal();
                this.M = getIntent().getBooleanExtra("extra_special_title", false);
                m.a().b(Constants.STR_EMPTY, "key_re_app_list_state", Integer.valueOf(RecoverAppListReceiver.RecoverAppListState.NOMAL.ordinal()));
                MainActivity.x = RecoverAppListReceiver.RecoverAppListState.NOMAL.ordinal();
                return;
            }
            this.F = (PopUpInfo) getIntent().getSerializableExtra("extra_pop_info");
        } catch (Throwable th) {
            finish();
        }
    }

    private void y() {
        String str;
        boolean z2;
        boolean z3;
        this.t = findViewById(R.id.page);
        this.t.setBackgroundColor(getResources().getColor(R.color.start_pop_window_bg_color));
        this.x = (TextView) findViewById(R.id.title);
        this.u = findViewById(R.id.addtion_view);
        this.v = findViewById(R.id.addtion_view_2);
        this.B = (TextView) findViewById(R.id.all_select_txt);
        this.C = (TextView) findViewById(R.id.more_bibei_txt);
        this.w = findViewById(R.id.btnLayout);
        if (this.G) {
            this.I = u.b(this.H.c);
            if (this.H != null) {
                switch (this.H.b) {
                    case 0:
                        this.x.setText((int) R.string.pop_title_download_new_apps);
                        this.B.setVisibility(8);
                        break;
                    case 1:
                        if ((this.H.e == 0 || (System.currentTimeMillis() / 1000) - this.H.e <= 2592000) && !TextUtils.isEmpty(this.H.d)) {
                            this.x.setText(getString(R.string.pop_title_recover_old_phone_apps, new Object[]{this.H.d}));
                        } else {
                            this.x.setText((int) R.string.pop_title_download_old_time_apps);
                        }
                        this.C.setVisibility(8);
                        break;
                    case 2:
                        this.x.setText((int) R.string.pop_title_download_old_apps);
                        this.C.setVisibility(8);
                        break;
                    default:
                        this.x.setText((int) R.string.pop_title_download_new_apps);
                        this.B.setVisibility(8);
                        break;
                }
            }
            if (this.M || !(!this.L || this.H.b == 2 || this.H.b == 1)) {
                this.x.setText((int) R.string.pop_title_download_new_apps_x);
            }
        } else {
            this.B.setVisibility(8);
            TextView textView = this.x;
            if (this.F == null) {
                str = Constants.STR_EMPTY;
            } else {
                str = this.F.b;
            }
            textView.setText(str);
            if (this.F != null) {
                this.I = u.b(this.F.k);
            }
        }
        ImageView imageView = (ImageView) findViewById(R.id.cancel_btn);
        imageView.setTag(R.id.tma_st_slot_tag, "10_002");
        imageView.setOnClickListener(new go(this));
        this.y = (GridView) findViewById(R.id.grid_view);
        this.y.setNumColumns(3);
        this.y.setSelector(new ColorDrawable(0));
        this.E = new StartPopWindowGridViewAdapter(this);
        if (!this.G || this.H == null || !(this.H.b == 2 || this.H.b == 1)) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (z2) {
            if (df.c() <= 480) {
                if (this.I != null && this.I.size() > 6) {
                    this.I = this.I.subList(0, 6);
                }
            } else if (this.I != null && this.I.size() > 9) {
                this.I = this.I.subList(0, 9);
            }
        }
        if (this.I == null || this.I.size() <= 9) {
            z3 = false;
        } else {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.y.getLayoutParams();
            layoutParams.height = getResources().getDimensionPixelSize(R.dimen.pop_view_grideview_height);
            this.y.setLayoutParams(layoutParams);
            z3 = true;
        }
        if (this.I != null && this.I.size() > 6 && (((float) t.c) < df.a(546.0f) || (t.c == 854 && t.b == 480))) {
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.y.getLayoutParams();
            layoutParams2.height = ((getResources().getDimensionPixelSize(R.dimen.pop_view_grideview_height) / 3) * 2) + getResources().getDimensionPixelSize(R.dimen.curcor_add);
            this.y.setLayoutParams(layoutParams2);
            z3 = true;
        }
        if (z3) {
            gw gwVar = new gw(this);
            if (this.y != null) {
                this.y.post(gwVar);
            }
        }
        if (this.I != null) {
            int size = this.I.size();
            long j = 0;
            for (int i = 0; i < size; i++) {
                j += this.I.get(i).k;
            }
            this.E.a(this.I, size, j);
        }
        this.y.setAdapter((ListAdapter) this.E);
        this.y.setOnItemClickListener(new gp(this));
        this.z = (TextView) findViewById(R.id.download_btn);
        this.A = (TextView) findViewById(R.id.download_while_wifi_btn);
        this.z.setTag(R.id.tma_st_slot_tag, "10_003");
        this.z.setOnClickListener(new gq(this));
        this.A.setOnClickListener(new gr(this));
        this.D = (TextView) findViewById(R.id.count_text);
        this.B.setSelected(true);
        this.B.setOnClickListener(new gs(this));
        this.C.setOnClickListener(new gt(this));
    }

    /* access modifiers changed from: package-private */
    public void i() {
        if (this.G && this.H != null) {
            if (this.H.b == 1 || this.H.b == 3) {
                bx.a(this, false);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void j() {
        if (this.E != null && this.E.getCount() != 0) {
            SpannableString spannableString = new SpannableString("已选" + this.E.a() + "/" + this.E.getCount() + " , 共" + bt.a(this.E.b()));
            spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.about_last_version_color)), 2, new String(this.E.a() + Constants.STR_EMPTY).length() + 2, 33);
            this.D.setText(spannableString);
            if (this.E.a() == 0) {
                this.A.setEnabled(false);
                this.z.setEnabled(false);
                this.z.setTextColor(getResources().getColor(R.color.state_disable));
                this.A.setTextColor(getResources().getColor(R.color.state_disable));
                return;
            }
            this.A.setEnabled(true);
            this.z.setEnabled(true);
            this.z.setTextColor(getResources().getColor(R.color.state_normal));
            this.A.setTextColor(getResources().getColor(R.color.state_normal));
        }
    }

    /* access modifiers changed from: private */
    public void z() {
        c.j();
        if (c.d()) {
            ((ImageView) this.u.findViewById(R.id.wifi_image)).setImageResource(R.drawable.pop_wifi);
            ((TextView) this.u.findViewById(R.id.wifi_text)).setText((int) R.string.pop_tip_wifi);
            A();
            return;
        }
        ((ImageView) this.u.findViewById(R.id.wifi_image)).setImageResource(R.drawable.pop_gms);
        ((TextView) this.u.findViewById(R.id.wifi_text)).setText((int) R.string.pop_tip_net);
        B();
    }

    private void A() {
        this.A.setVisibility(8);
        findViewById(R.id.bland).setVisibility(8);
        this.w.setPadding(getResources().getDimensionPixelSize(R.dimen.pop_btn_one_padding_l_r), 0, getResources().getDimensionPixelSize(R.dimen.pop_btn_one_padding_l_r), getResources().getDimensionPixelSize(R.dimen.loading_progress_bar_size));
    }

    private void B() {
        this.A.setVisibility(0);
        findViewById(R.id.bland).setVisibility(0);
        this.w.setPadding(getResources().getDimensionPixelSize(R.dimen.app_down_item_icon_top_margin), 0, getResources().getDimensionPixelSize(R.dimen.app_down_item_icon_top_margin), getResources().getDimensionPixelSize(R.dimen.loading_progress_bar_size));
    }

    /* access modifiers changed from: private */
    public void C() {
        int i;
        View childAt;
        ArrayList<Boolean> c = this.E.c();
        ArrayList arrayList = new ArrayList();
        if (this.I != null) {
            i = -1;
            for (int i2 = 0; i2 < this.I.size(); i2++) {
                if (c.get(i2).booleanValue()) {
                    arrayList.add(this.I.get(i2));
                    a(this.I.get(i2), i2);
                    i = i2;
                }
            }
        } else {
            i = -1;
        }
        if (i > -1) {
            StatInfo buildDownloadSTInfo = STInfoBuilder.buildDownloadSTInfo(this, null);
            if (this.y != null) {
                this.y.postDelayed(new gu(this, arrayList, buildDownloadSTInfo), 500);
            }
            if (!(this.y == null || (childAt = this.y.getChildAt(i)) == null)) {
                com.tencent.assistant.utils.a.a((ImageView) childAt.findViewById(R.id.icon));
            }
            E();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(SimpleAppModel simpleAppModel, int i) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.J, 900);
        if (buildSTInfo != null) {
            buildSTInfo.extraData = simpleAppModel != null ? simpleAppModel.c + "|" + simpleAppModel.g : Constants.STR_EMPTY;
            buildSTInfo.slotId = c(i);
            buildSTInfo.updateWithSimpleAppModel(simpleAppModel);
            k.a(buildSTInfo);
        }
    }

    private String c(int i) {
        return "03_" + ct.a(i + 1);
    }

    /* access modifiers changed from: private */
    public void D() {
        ArrayList<Boolean> c = this.E.c();
        StatInfo buildDownloadSTInfo = STInfoBuilder.buildDownloadSTInfo(this, null);
        if (this.I != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.I.size()) {
                    break;
                }
                if (c.get(i2).booleanValue()) {
                    if (this.y != null) {
                        this.y.postDelayed(new gv(this, i2, buildDownloadSTInfo), 500);
                    }
                    a(this.I.get(i2), i2);
                }
                i = i2 + 1;
            }
        }
        E();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!this.K) {
            unregisterReceiver(this.n);
        }
    }

    /* access modifiers changed from: private */
    public void E() {
        finish();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        if (this.N > 0 && System.currentTimeMillis() - this.N < 800) {
            return false;
        }
        E();
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.J, 200);
        buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a("06", "000");
        buildSTInfo.status = "01";
        k.a(buildSTInfo);
        return true;
    }

    public boolean n() {
        return false;
    }

    public void overridePendingTransition(int i, int i2) {
    }

    /* access modifiers changed from: protected */
    public boolean l() {
        return false;
    }

    public void a(Runnable runnable) {
        ba.a().postDelayed(new gn(this, runnable), 2000);
    }
}
