package com.tencent.assistant.activity;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.net.c;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class gq extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartPopWindowActivity f613a;

    gq(StartPopWindowActivity startPopWindowActivity) {
        this.f613a = startPopWindowActivity;
    }

    public void onTMAClick(View view) {
        this.f613a.C();
        this.f613a.i();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f613a.J, 200);
        c.j();
        if (c.d()) {
            buildSTInfo.slotId = a.a("04", "001");
        } else {
            buildSTInfo.slotId = a.a("04", "002");
        }
        if (this.f613a.I != null && this.f613a.E.a() == this.f613a.I.size()) {
            buildSTInfo.actionId = 200;
            buildSTInfo.status = "02";
        }
        return buildSTInfo;
    }
}
