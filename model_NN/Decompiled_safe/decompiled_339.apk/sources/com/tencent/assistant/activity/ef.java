package com.tencent.assistant.activity;

import android.os.Handler;
import android.os.Message;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import java.util.List;

/* compiled from: ProGuard */
class ef extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstalledAppManagerActivity f548a;

    ef(InstalledAppManagerActivity installedAppManagerActivity) {
        this.f548a = installedAppManagerActivity;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 10701:
                List list = (List) message.obj;
                if (list != null) {
                    this.f548a.a(list);
                }
                XLog.d("miles", "MSG_LOAG_APK_SUCCESS received");
                this.f548a.A();
                return;
            case 10702:
                int i = message.arg1;
                this.f548a.a((LocalApkInfo) message.obj, i);
                this.f548a.A();
                return;
            case 10703:
                this.f548a.A();
                return;
            case 10704:
            default:
                return;
            case 10705:
                if (message.obj != null && (message.obj instanceof LocalApkInfo)) {
                    this.f548a.a((LocalApkInfo) message.obj);
                    return;
                }
                return;
            case 10706:
                if (this.f548a.G != null) {
                    this.f548a.G.onResume();
                }
                if (this.f548a.F != null) {
                    this.f548a.F.onResume();
                    return;
                }
                return;
        }
    }
}
