package com.tencent.assistant.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import com.qq.AppService.AppService;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.component.ConnectSuccessTopView;
import com.tencent.assistant.component.CustomBackupRelativeLayout;
import com.tencent.assistant.component.PCListLinearLayout;
import com.tencent.assistant.component.PhotoBackupBottomView;
import com.tencent.assistant.component.PhotoBackupMiddleGridView;
import com.tencent.assistant.component.WaitingPCConnectView;
import com.tencent.assistant.component.WaitingPCConnectViewNew;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.event.listener.b;
import com.tencent.assistant.f.a;
import com.tencent.assistant.model.OnlinePCListItemModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.activity.AssistantTabActivity;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.business.t;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connector.ConnectionActivity;
import com.tencent.connector.ipc.ConnectionType;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/* compiled from: ProGuard */
public class PhotoBackupNewActivity extends BaseActivity implements UIEventListener, b {
    private boolean A = false;
    private TextView B;
    private TextView C;
    private TextView D;
    private TextView E;
    private int F = 0;
    private CustomBackupRelativeLayout G;
    private View H;
    private View I;
    private WaitingPCConnectViewNew J;
    private WaitingPCConnectView K;
    private View L;
    private t M = new t();
    private Timer N = null;
    private TimerTask O = null;
    private View.OnClickListener P = new eq(this);
    /* access modifiers changed from: private */
    public boolean Q = false;
    private a R = new es(this);
    private PCListLinearLayout.PCListClickCallback S = new et(this);
    public boolean n = true;
    private SecondNavigationTitleViewV5 t;
    private ConnectSuccessTopView u;
    private PhotoBackupMiddleGridView v;
    private PhotoBackupBottomView w;
    private TextView x;
    /* access modifiers changed from: private */
    public PCListLinearLayout y;
    private TextView z;

    public int f() {
        return STConst.ST_PAGE_PHOTO_BACKUP;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_photo_backup_newlayout);
        E();
        B();
        A();
        overridePendingTransition(R.anim.activity_right_in, R.anim.activity_left_out);
        b(getIntent().getIntExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, 2000));
        z();
    }

    private void z() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", com.tencent.assistant.utils.t.g());
        XLog.d("beacon", "beacon report >> expose_photobackup. " + hashMap.toString());
        com.tencent.beacon.event.a.a("expose_photobackup", true, -1, -1, hashMap, true);
    }

    public void b(int i) {
        k.a(new STInfoV2(f(), STConst.ST_DEFAULT_SLOT, i, STConst.ST_DEFAULT_SLOT, 100));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void A() {
        if (!com.tencent.connector.ipc.a.a().c()) {
            Context applicationContext = getApplicationContext();
            Intent intent = new Intent();
            intent.setClass(applicationContext, AppService.class);
            intent.putExtra("isWifi", true);
            applicationContext.startService(intent);
        }
    }

    private void B() {
        if (this.z != null) {
            this.z.setOnClickListener(this.P);
        }
        if (this.B != null) {
            this.B.setOnClickListener(this.P);
        }
        if (this.C != null) {
            this.C.setOnClickListener(this.P);
        }
        if (this.D != null) {
            this.D.setOnClickListener(this.P);
        }
        if (this.E != null) {
            this.E.setOnClickListener(this.P);
        }
    }

    public void c(int i) {
        switch (i) {
            case 1:
                this.H.setVisibility(0);
                this.I.setVisibility(8);
                this.J.setVisibility(8);
                this.K.setVisibility(8);
                this.L.setVisibility(8);
                break;
            case 2:
                this.H.setVisibility(8);
                this.I.setVisibility(0);
                this.J.setVisibility(8);
                this.K.setVisibility(8);
                this.L.setVisibility(8);
                break;
            case 3:
                this.H.setVisibility(8);
                this.I.setVisibility(8);
                this.J.setVisibility(0);
                this.K.setVisibility(8);
                this.L.setVisibility(8);
                break;
            case 4:
                this.H.setVisibility(8);
                this.I.setVisibility(8);
                this.J.setVisibility(8);
                this.K.setVisibility(0);
                this.L.setVisibility(8);
                break;
            case 5:
                this.H.setVisibility(8);
                this.I.setVisibility(8);
                this.J.setVisibility(8);
                this.K.setVisibility(8);
                this.L.setVisibility(0);
                break;
        }
        this.F = i;
    }

    public void d(int i) {
        switch (i) {
            case 1:
                this.H.setVisibility(8);
                this.y.stopTimer();
                break;
            case 2:
                this.I.setVisibility(8);
                break;
            case 3:
                this.J.setVisibility(8);
                j();
                break;
            case 4:
                this.K.setVisibility(8);
                break;
            case 5:
                this.L.setVisibility(8);
                break;
        }
        this.F = 0;
    }

    public void i() {
        if (this.n) {
            this.z.setText((int) R.string.photo_backup_cancel);
        } else {
            this.z.setText((int) R.string.photo_select_cancel);
        }
        c(4);
        this.K.getPcListStart();
        com.tencent.assistant.f.b.a().c();
    }

    public void b(String str, String str2) {
        if (str != null && !TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            c(3);
            this.J.updatePc(str);
            com.tencent.assistant.f.b.a().a(str2);
            C();
        }
    }

    private void C() {
        this.O = new eo(this);
        this.N = new Timer(true);
        this.N.schedule(this.O, 15000);
    }

    /* access modifiers changed from: private */
    public void D() {
        com.tencent.connector.ipc.a.a().h();
        d(3);
        e(0);
        i();
    }

    public void j() {
        if (this.O != null) {
            this.O.cancel();
            this.O = null;
        }
        if (this.N != null) {
            this.N.cancel();
            this.N = null;
        }
    }

    private void E() {
        H();
        K();
        J();
        I();
        F();
    }

    private void F() {
        this.D = (TextView) findViewById(R.id.ok);
        this.E = (TextView) findViewById(R.id.cancel);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        boolean z2 = false;
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        switch (this.F) {
            case 1:
                d(1);
                if (com.tencent.connector.ipc.a.a().d()) {
                    z2 = true;
                    break;
                }
                break;
            case 2:
                d(2);
                break;
            case 3:
                d(3);
                break;
            case 4:
                d(4);
                if (com.tencent.connector.ipc.a.a().d()) {
                    z2 = true;
                    break;
                }
                break;
            case 5:
                d(5);
                if (com.tencent.connector.ipc.a.a().d()) {
                    z2 = true;
                    break;
                }
                break;
        }
        if (z2) {
            return true;
        }
        G();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void G() {
        if (!this.r) {
            finish();
            return;
        }
        Intent intent = new Intent(this, AssistantTabActivity.class);
        intent.setFlags(67108864);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
        intent.putExtra(com.tencent.assistant.b.a.G, true);
        startActivity(intent);
        this.r = false;
        finish();
        XLog.i("PhotoBackupNewActivity", "PhotoBackupNewActivity >> key back finish");
    }

    /* access modifiers changed from: protected */
    public void v() {
        Intent intent = new Intent();
        intent.setClass(this, ConnectionActivity.class);
        startActivity(intent);
    }

    private void H() {
        this.G = (CustomBackupRelativeLayout) findViewById(R.id.backup_layout);
        this.H = findViewById(R.id.backupPcListLayout);
        this.I = findViewById(R.id.wifiDisbaleLayout);
        this.J = (WaitingPCConnectViewNew) findViewById(R.id.waitingPCLayout);
        this.K = (WaitingPCConnectView) findViewById(R.id.getPCLoading);
        this.L = findViewById(R.id.tipsDisconnectLayout);
    }

    private void I() {
        this.B = (TextView) findViewById(R.id.no_backup);
        this.C = (TextView) findViewById(R.id.connect_pc);
    }

    private void J() {
        this.x = (TextView) findViewById(R.id.title);
        this.y = (PCListLinearLayout) findViewById(R.id.pcList);
        this.y.setPCListClickCallback(this.S);
        this.z = (TextView) findViewById(R.id.cancel_backup);
    }

    private void K() {
        this.t = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.u = (ConnectSuccessTopView) findViewById(R.id.topRightView);
        this.v = (PhotoBackupMiddleGridView) findViewById(R.id.middleView);
        this.w = (PhotoBackupBottomView) findViewById(R.id.bottom_view);
        this.t.a(this);
        this.t.b(getString(R.string.photo_backup_manager));
        this.t.d();
        this.t.c(new er(this));
        this.u.setActivity(this);
        this.v.setActivity(this);
        this.w.setActivity(this);
        this.w.setBacupEnable(false);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AstApp.i().k().addConnectionEventListener(5002, this);
        AstApp.i().k().addConnectionEventListener(5001, this);
        AstApp.i().k().addConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_PC_PING, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FAILED, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FINISH, this);
        if (!com.tencent.connector.ipc.a.f3550a && this.v.getCount() > 0) {
            Q();
        }
        L();
        N();
        com.tencent.assistant.f.b.a().b();
        if (this.v != null) {
            this.v.onResume();
        }
        if (this.t != null) {
            this.t.l();
        }
    }

    private void L() {
        if (this.v != null) {
            this.v.checkIDs(com.tencent.connector.ipc.a.b);
        }
        if (com.tencent.connector.ipc.a.f3550a) {
            if (this.G != null) {
                this.G.setClickEnable(false);
            }
            if (this.w != null) {
                this.w.setBackupStart();
            }
        }
    }

    private void M() {
        com.tencent.assistant.f.b.a().b(this.R);
    }

    private void N() {
        com.tencent.assistant.f.b.a().a(this.R);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.F != 0) {
            d(this.F);
        }
        T();
        AstApp.i().k().removeConnectionEventListener(5002, this);
        AstApp.i().k().removeConnectionEventListener(5001, this);
        AstApp.i().k().removeConnectionEventListener(EventDispatcherEnum.CONNECTION_EVENT_PC_PING, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_START, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FAILED, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FINISH, this);
        M();
        if (this.v != null) {
            this.v.onPause();
        }
        if (this.t != null) {
            this.t.m();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        byte b;
        super.onDestroy();
        if (this.v != null) {
            this.v.onDestroy();
        }
        if (!this.A) {
            ConnectionType e = com.tencent.connector.ipc.a.a().e();
            if (e == ConnectionType.USB) {
                b = 1;
            } else {
                b = e == ConnectionType.WIFI ? (byte) 2 : 0;
            }
            if (this.M != null) {
                this.M.a(b, false, 0);
            }
        }
    }

    public void e(int i) {
        if (i == 1) {
            this.x.setBackgroundResource(R.drawable.apptoast_bg_top);
            this.x.setText((int) R.string.photo_backup_please_select_backup_to_pc);
            this.x.setTextColor(getResources().getColor(R.color.report_reason));
            return;
        }
        this.x.setBackgroundResource(R.drawable.apptoast_bg_top_warn);
        this.x.setText((int) R.string.photo_backup_connect_failed);
        this.x.setTextColor(getResources().getColor(R.color.white));
    }

    public void w() {
        int[] checkedItemIds;
        if (this.v != null && com.tencent.connector.ipc.a.a().d() && (checkedItemIds = this.v.getCheckedItemIds()) != null && checkedItemIds.length > 0) {
            this.A = true;
            com.tencent.connector.ipc.a.b = checkedItemIds;
            com.tencent.connector.ipc.a.a().a(checkedItemIds);
            ConnectionType e = com.tencent.connector.ipc.a.a().e();
            byte b = 0;
            if (e == ConnectionType.USB) {
                b = 1;
            } else if (e == ConnectionType.WIFI) {
                b = 2;
            }
            if (this.M != null) {
                this.M.a(b, true, (short) checkedItemIds.length);
            }
        }
    }

    private void O() {
        if (this.w != null) {
            this.w.setBackupStart();
        }
        if (this.G != null) {
            this.G.setClickEnable(false);
        }
    }

    private void P() {
        if (this.w != null) {
            this.w.setBackupFailed();
        }
        if (this.v != null) {
            this.v.clearChoices();
        }
        if (this.G != null) {
            this.G.setClickEnable(true);
        }
    }

    private void Q() {
        if (this.w != null) {
            this.w.setBackupFinish();
        }
        if (this.v != null) {
            this.v.clearChoices();
        }
        if (this.G != null) {
            this.G.setClickEnable(true);
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_START /*1035*/:
                Log.d("PhotoBackupNewActivity", "backup start");
                O();
                return;
            case EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FAILED /*1036*/:
                Log.d("PhotoBackupNewActivity", "backup failed");
                P();
                return;
            case EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FINISH /*1037*/:
                Log.d("PhotoBackupNewActivity", "backup finish");
                Q();
                return;
            default:
                return;
        }
    }

    public void a(Message message) {
        switch (message.what) {
            case 5001:
                Log.d("PhotoBackupNewActivity", "event_disconnect");
                R();
                return;
            case 5002:
                Log.d("PhotoBackupNewActivity", "event_connect");
                if (this.F != 0) {
                    d(this.F);
                }
                ConnectionType connectionType = (ConnectionType) message.obj;
                if (connectionType == ConnectionType.USB) {
                    x();
                    return;
                } else if (connectionType == ConnectionType.WIFI) {
                    y();
                    return;
                } else {
                    R();
                    return;
                }
            case EventDispatcherEnum.CONNECTION_EVENT_PC_PING /*5008*/:
                Log.d("PhotoBackupNewActivity", "event_pc_ping");
                OnlinePCListItemModel onlinePCListItemModel = (OnlinePCListItemModel) message.obj;
                if (this.y != null) {
                    this.y.updateItem(onlinePCListItemModel);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void R() {
        if (this.F != 0) {
            Log.d("PhotoBackupNewActivity", "hide: " + this.F);
            d(this.F);
        }
        S();
        T();
        com.tencent.assistant.f.b.a().b();
    }

    private void S() {
        if (this.G != null) {
            this.G.setClickEnable(true);
        }
        if (this.w != null) {
            this.w.reset();
        }
    }

    private void T() {
        if (this.u != null && this.u.getVisibility() == 0) {
            this.u.setVisibility(8);
        }
        if (this.v != null) {
            this.v.clearChoices();
            f(0);
        }
    }

    public void f(int i) {
        if (this.w != null) {
            this.w.updateBackupCount(i);
        }
    }

    /* access modifiers changed from: protected */
    public void x() {
        if (this.u != null) {
            this.u.setVisibility(0);
            String f = com.tencent.connector.ipc.a.a().f();
            if (TextUtils.isEmpty(f)) {
                f = "usb";
            }
            this.u.switchConnectModel((byte) 0, f);
        }
        com.qq.k.b.a(AstApp.i(), System.currentTimeMillis());
    }

    /* access modifiers changed from: protected */
    public void y() {
        if (this.u != null) {
            this.u.setVisibility(0);
            String f = com.tencent.connector.ipc.a.a().f();
            if (TextUtils.isEmpty(f)) {
                f = "wifi";
            }
            this.u.switchConnectModel((byte) 1, f);
        }
        com.qq.k.b.a(AstApp.i(), System.currentTimeMillis());
    }
}
