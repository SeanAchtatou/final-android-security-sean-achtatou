package com.tencent.assistant.activity;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class go extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartPopWindowActivity f611a;

    go(StartPopWindowActivity startPopWindowActivity) {
        this.f611a = startPopWindowActivity;
    }

    public void onTMAClick(View view) {
        this.f611a.E();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f611a.J, 200);
        buildSTInfo.slotId = a.a("06", "000");
        buildSTInfo.status = "01";
        return buildSTInfo;
    }
}
