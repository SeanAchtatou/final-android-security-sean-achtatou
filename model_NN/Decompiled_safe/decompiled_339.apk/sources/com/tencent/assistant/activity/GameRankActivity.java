package com.tencent.assistant.activity;

import android.os.Bundle;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ba;
import com.tencent.assistantv2.activity.ay;
import com.tencent.assistantv2.b.e;
import com.tencent.assistantv2.b.h;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.component.TXViewPager;
import com.tencent.assistantv2.component.TabBarView;
import com.tencent.assistantv2.component.search.c;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
public class GameRankActivity extends BaseActivity {
    private SecondNavigationTitleViewV5 n;
    /* access modifiers changed from: private */
    public TXViewPager t;
    /* access modifiers changed from: private */
    public cw u;
    /* access modifiers changed from: private */
    public int v = 0;
    /* access modifiers changed from: private */
    public TabBarView w;
    private h x;
    private c y = new cv(this);

    public int f() {
        if (this.u == null || this.u.a(this.v) == null) {
            return STConst.ST_PAGE_GAME_RANKING;
        }
        return ((ay) this.u.a(this.v)).J();
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        return false;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_game_band);
        this.n = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.n.d(false);
        this.n.a(this);
        this.n.i();
        this.n.b(getString(R.string.gamerank_title));
        this.x = e.a().b();
        i();
        c(this.v);
        h();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.n != null) {
            this.n.l();
        }
        if (this.v < this.u.getCount()) {
            ba.a().post(new cu(this, (ay) this.u.a(this.v)));
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.n.m();
    }

    private void i() {
        String[] strArr = new String[this.x.b.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.x.b.size()) {
                strArr[i2] = this.x.b.get(i2).f2960a;
                i = i2 + 1;
            } else {
                this.w = (TabBarView) findViewById(R.id.tab_view);
                this.w.a(strArr);
                this.w.a(this.v);
                this.w.a(this.y);
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(int i) {
        STInfoV2 s = s();
        if (s != null) {
            s.actionId = 200;
            s.slotId = a.a("03", i);
        }
        k.a(s);
    }

    private void c(int i) {
        this.t = (TXViewPager) findViewById(R.id.vPager);
        if (this.u == null) {
            this.u = new cw(this, e(), this, this.x.b);
        }
        this.t.setAdapter(this.u);
        this.t.setCurrentItem(i);
        d(i);
        this.t.setOnPageChangeListener(new cx(this));
    }

    /* access modifiers changed from: private */
    public void d(int i) {
        if (this.u.a(i) != null) {
            ay ayVar = (ay) this.u.a(i);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
    }
}
