package com.tencent.assistant.activity;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class au extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppTreasureBoxActivity f409a;

    au(AppTreasureBoxActivity appTreasureBoxActivity) {
        this.f409a = appTreasureBoxActivity;
    }

    public void onTMAClick(View view) {
        this.f409a.onCloseBtnClick();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f409a.n, 200);
        buildSTInfo.slotId = a.a("06", "001");
        return buildSTInfo;
    }
}
