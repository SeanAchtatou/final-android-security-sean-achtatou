package com.tencent.assistant.activity;

import android.view.View;
import com.tencent.assistant.component.appdetail.HorizonImageListView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class gt extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartPopWindowActivity f616a;

    gt(StartPopWindowActivity startPopWindowActivity) {
        this.f616a = startPopWindowActivity;
    }

    public void onTMAClick(View view) {
        b.b(this.f616a.J, "tmast://necessity?selflink=1");
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f616a.J, 200);
        buildSTInfo.slotId = a.a(HorizonImageListView.TMA_ST_HORIZON_IMAGE_TAG, "001");
        return buildSTInfo;
    }
}
