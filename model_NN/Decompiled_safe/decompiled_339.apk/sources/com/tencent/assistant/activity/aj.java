package com.tencent.assistant.activity;

import android.view.animation.Animation;

/* compiled from: ProGuard */
class aj implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppBackupActivity f398a;

    aj(AppBackupActivity appBackupActivity) {
        this.f398a = appBackupActivity;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        long j;
        this.f398a.M.setVisibility(8);
        if (this.f398a.ac) {
            this.f398a.ab.setVisibility(8);
            this.f398a.aa.setVisibility(0);
        } else {
            this.f398a.ab.setVisibility(0);
            this.f398a.aa.setVisibility(8);
        }
        this.f398a.a(this.f398a.Z, 90.0f, 0.0f, false, new ak(this));
        if (this.f398a.ac) {
            j = 5000;
        } else {
            j = 3000;
        }
        if (this.f398a.am.hasMessages(11901)) {
            this.f398a.am.removeMessages(11901);
        }
        this.f398a.am.sendEmptyMessageDelayed(11901, j);
    }
}
