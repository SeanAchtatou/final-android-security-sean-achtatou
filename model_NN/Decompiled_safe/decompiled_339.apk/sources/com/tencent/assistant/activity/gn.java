package com.tencent.assistant.activity;

import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class gn implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Runnable f610a;
    final /* synthetic */ StartPopWindowActivity b;

    gn(StartPopWindowActivity startPopWindowActivity, Runnable runnable) {
        this.b = startPopWindowActivity;
        this.f610a = runnable;
    }

    public void run() {
        TemporaryThreadManager.get().start(this.f610a);
    }
}
