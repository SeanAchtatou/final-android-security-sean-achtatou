package com.tencent.assistant.activity;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.SpecailTopicDetailAdapter;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.invalidater.TXRefreshGetMoreListViewScrollListener;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.t;
import com.tencent.assistant.module.dv;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.List;

/* compiled from: ProGuard */
public class SpecailTopicDetailActivity extends BaseActivity implements ITXRefreshListViewListener, t {
    private String A;
    private int B = 0;
    /* access modifiers changed from: private */
    public dv C;
    private int D = 3;
    private View.OnClickListener E = new gf(this);
    private Context n;
    private AppGroupInfo t;
    private TXGetMoreListView u;
    private SecondNavigationTitleViewV5 v;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage w;
    private LoadingView x;
    private SpecailTopicDetailAdapter y;
    private int z = 0;

    public int f() {
        j();
        if (this.z == 5) {
            return STConst.ST_PAGE_SPECIAL_DETAIL_REDFLOWER;
        }
        return STConst.ST_PAGE_SPECIAL_DETAIL;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.n = this;
        setContentView((int) R.layout.activity_specail_topic_detail_layout);
        v();
        i();
        h();
    }

    public boolean g() {
        return false;
    }

    public void h() {
        STInfoV2 s = s();
        if (s != null) {
            s.updateContentId(STCommonInfo.ContentIdType.SPECIAL, String.valueOf(this.z));
        }
        k.a(s);
    }

    private void i() {
        j();
        this.y = new SpecailTopicDetailAdapter(this, this.u, this.t, null);
        this.y.a(this.z);
        this.y.b(this.B);
        this.u.setAdapter(this.y);
        if (TextUtils.isEmpty(this.A)) {
            this.v.b(getString(R.string.special_topic_detail));
            this.A = Constants.STR_EMPTY;
        } else {
            this.v.b(this.A);
        }
        TXRefreshGetMoreListViewScrollListener tXRefreshGetMoreListViewScrollListener = new TXRefreshGetMoreListViewScrollListener();
        this.u.setIScrollerListener(tXRefreshGetMoreListViewScrollListener);
        this.y.a(tXRefreshGetMoreListViewScrollListener);
        this.C = new dv(this.z, this.A);
        this.C.register(this);
        this.C.a();
    }

    private void j() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.t = (AppGroupInfo) extras.get("com.tencent.assistant.APP_GROUP_INFO");
            if (this.t != null) {
                this.A = this.t.b;
                this.z = this.t.f1628a;
                return;
            }
            try {
                this.z = Integer.parseInt(extras.getString("com.tencent.assistant.TOPICID"));
            } catch (Exception e) {
            }
            try {
                this.B = Integer.parseInt(extras.getString("com.tencent.assistant.TOPICSTYLE"));
            } catch (Exception e2) {
            }
            this.A = extras.getString("com.tencent.assistant.TOPICNAME");
        }
    }

    private void v() {
        this.x = (LoadingView) findViewById(R.id.loading_view);
        this.w = (NormalErrorRecommendPage) findViewById(R.id.topic_network_error);
        this.w.setOnClickListener(this.E);
        this.w.setButtonClickListener(this.E);
        this.v = (SecondNavigationTitleViewV5) findViewById(R.id.topic_title_view);
        this.v.a(this);
        this.v.d(false);
        this.v.i();
        this.u = (TXGetMoreListView) findViewById(R.id.topic_list);
        this.u.setDivider(null);
        this.u.setSelector(getResources().getDrawable(R.drawable.transparent_selector));
        this.u.setRefreshListViewListener(this);
    }

    private void b(int i) {
        this.u.setVisibility(8);
        this.w.setVisibility(0);
        this.x.setVisibility(8);
        this.w.setErrorType(i);
    }

    private void w() {
        this.u.setVisibility(0);
        this.x.setVisibility(8);
        this.w.setVisibility(8);
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (scrollState == TXScrollViewBase.ScrollState.ScrollState_FromEnd) {
            this.C.b();
        }
    }

    public void a(int i, int i2, boolean z2, List<SimpleAppModel> list, AppGroupInfo appGroupInfo) {
        if (i2 == 0) {
            this.D = 3;
            if (this.t == null) {
                this.t = appGroupInfo;
                this.y.a(this.t);
            }
            this.y.a(z2, list);
            this.u.onRefreshComplete(this.C.c(), true);
            if (!z2) {
                return;
            }
            if (list == null || list.size() == 0) {
                b(50);
            } else {
                w();
            }
        } else if (-800 == i2) {
            if (z2 || this.y == null || this.y.getCount() == 0) {
                b(30);
            } else {
                this.u.onRefreshComplete(true, false);
            }
        } else if (this.D > 0) {
            if (this.y == null || this.y.a() == 0) {
                this.C.a();
            } else {
                this.C.b();
            }
            this.D--;
        } else if (this.y == null || this.y.a() == 0) {
            b(20);
        } else {
            this.u.onRefreshComplete(true, false);
        }
    }

    /* access modifiers changed from: private */
    public void x() {
        this.u.setVisibility(8);
        this.x.setVisibility(0);
        this.w.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.y != null) {
            this.y.notifyDataSetChanged();
            this.y.c();
        }
        this.v.l();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.v.m();
        if (this.y != null) {
            this.y.b();
        }
    }
}
