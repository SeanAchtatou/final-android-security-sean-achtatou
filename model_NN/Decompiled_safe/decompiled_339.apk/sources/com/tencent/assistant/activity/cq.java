package com.tencent.assistant.activity;

import android.content.Intent;
import android.view.View;
import com.tencent.assistantv2.activity.MainActivity;

/* compiled from: ProGuard */
class cq implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ExternalCallActivity f458a;

    cq(ExternalCallActivity externalCallActivity) {
        this.f458a = externalCallActivity;
    }

    public void onClick(View view) {
        this.f458a.n.startActivity(new Intent(this.f458a.n, MainActivity.class));
        if (view.equals(this.f458a.A)) {
            this.f458a.a((String) null, 200, 1);
        } else {
            this.f458a.a((String) null, 200, 2);
        }
    }
}
