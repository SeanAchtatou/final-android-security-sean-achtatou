package com.tencent.assistant.activity;

import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.cv;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkMgrActivity f642a;

    k(ApkMgrActivity apkMgrActivity) {
        this.f642a = apkMgrActivity;
    }

    public void run() {
        List<LocalApkInfo> localApkInfos = ApkResourceManager.getInstance().getLocalApkInfos();
        if (localApkInfos != null && localApkInfos.size() > 0) {
            long d = cv.d();
            Iterator<LocalApkInfo> it = localApkInfos.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                LocalApkInfo next = it.next();
                if (next.mLastLaunchTime != 0 && cv.a(next.mLastLaunchTime, d) >= 30) {
                    boolean unused = this.f642a.L = true;
                    break;
                }
            }
        }
        this.f642a.R.sendMessage(this.f642a.R.obtainMessage(110010));
        XLog.i("ApkMgrActivity", "isSkipToInstalledAppManager in ApkMgrActivity");
    }
}
