package com.tencent.assistant.activity;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.component.spaceclean.RubbishItemView;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class fu extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceCleanActivity f590a;

    fu(SpaceCleanActivity spaceCleanActivity) {
        this.f590a = spaceCleanActivity;
    }

    public void onTMAClick(View view) {
        if (this.f590a.z.getFooterViewEnable()) {
            this.f590a.B.b();
            this.f590a.z();
            this.f590a.A();
            RubbishItemView.isDeleting = true;
            this.f590a.z.updateContent(this.f590a.getString(R.string.apkmgr_is_deleting));
            this.f590a.z.setFooterViewEnable(false);
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f590a.v, 200);
        buildSTInfo.scene = STConst.ST_PAGE_GARBAGE_INSTALL_STEWARD;
        buildSTInfo.slotId = "05_001";
        return buildSTInfo;
    }
}
