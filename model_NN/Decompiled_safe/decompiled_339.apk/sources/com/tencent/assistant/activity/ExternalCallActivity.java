package com.tencent.assistant.activity;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.b.a;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.m;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.net.c;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.df;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class ExternalCallActivity extends BrowserActivity {
    private static final String z = ExternalCallActivity.class.getSimpleName();
    /* access modifiers changed from: private */
    public LinearLayout A;
    private RelativeLayout B;
    /* access modifiers changed from: private */
    public boolean C = true;
    private String D = null;
    private int E = STConst.ST_PAGE_EXTERNAL_CALL;
    private b F = null;
    private long G = 0;
    private long H = 0;
    private View.OnClickListener I = new cq(this);
    public View.OnClickListener y = new cr(this);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.G = System.currentTimeMillis();
        XLog.d(z, "Activity onCreate time = " + this.G);
        F();
        G();
        a((String) null, 100, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(boolean, int):void
     arg types: [int, int]
     candidates:
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(com.tencent.assistantv2.component.SecondNavigationTitleViewV5, java.lang.String):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(com.tencent.assistantv2.component.SecondNavigationTitleViewV5, boolean):boolean
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(java.lang.String, int):void
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(java.lang.String, android.text.TextUtils$TruncateAt):void
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(boolean, int):void */
    private void F() {
        super.c(true);
        if (this.u != null) {
            this.u.a(this);
            this.u.setVisibility(0);
            this.u.i();
            this.u.a(this.n.getResources().getString(R.string.down_page_title));
            this.u.a(false, 0);
            this.u.b((int) R.drawable.common_icon_myapplogo_color);
            this.u.b(this.I);
            this.u.e(this.y);
        }
        this.B = (RelativeLayout) findViewById(R.id.browser_root);
        this.A = new LinearLayout(this.n);
        this.A.setId(R.id.external_call_topbar);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(3, R.id.browser_header_view);
        this.A.setOrientation(0);
        this.A.setGravity(16);
        this.A.setBackgroundColor(getResources().getColor(R.color.white));
        this.A.setOnClickListener(this.I);
        TXImageView tXImageView = new TXImageView(this.n);
        tXImageView.setImageResource(R.drawable.icon_about_logo);
        tXImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(df.a(this.n, 24.0f), df.a(this.n, 24.0f));
        layoutParams2.topMargin = df.a(this.n, 10.0f);
        layoutParams2.bottomMargin = df.a(this.n, 10.0f);
        layoutParams2.rightMargin = df.a(this.n, 10.0f);
        this.A.addView(tXImageView, layoutParams2);
        TextView textView = new TextView(this.n);
        textView.setText((int) R.string.external_call_see_more_apps);
        textView.setTextSize(2, 14.0f);
        this.A.addView(textView);
        this.A.setPadding(df.a(this.n, 16.0f), 0, df.a(this.n, 16.0f), 0);
        this.B.addView(this.A, layoutParams);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams3.addRule(3, this.A.getId());
        this.t.setLayoutParams(layoutParams3);
        this.t.a(new cp(this));
    }

    private void G() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.D = extras.getString(a.y);
            Set<String> keySet = extras.keySet();
            JSONObject jSONObject = new JSONObject();
            try {
                for (String next : keySet) {
                    jSONObject.put(next, extras.get(next));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String a2 = m.a().a("key_external_call_yyb_url", (String) null);
            if (!TextUtils.isEmpty(a2)) {
                StringBuilder sb = new StringBuilder();
                try {
                    sb.append(a2).append("downloadinfo").append("=").append(URLEncoder.encode(jSONObject.toString(), "utf-8")).append("&").append("mobileinfo").append("=").append(URLEncoder.encode(H(), "utf-8"));
                    this.H = System.currentTimeMillis();
                    this.t.a(sb.toString());
                    XLog.d(z, "url:" + a2 + " ,webview loadUrl time = " + this.H);
                } catch (UnsupportedEncodingException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    private String H() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("androidId", t.l());
            jSONObject.put("qua", Global.getQUA());
            jSONObject.put("osVer", Build.VERSION.SDK_INT);
            com.tencent.assistant.net.b h = c.h();
            if (h.f1925a == APN.UN_DETECT) {
                c.j();
            }
            jSONObject.put("apn", h.f1925a);
            jSONObject.put("isWap", h.d ? 1 : 0);
            jSONObject.put("wifiBssid", h.e);
            jSONObject.put("resolution", t.b + "x" + t.c);
            jSONObject.put("macAdress", t.k());
            jSONObject.put("imsi", t.h());
            jSONObject.put("imei", t.g());
            jSONObject.put(CommentDetailTabView.PARAMS_VERSION_CODE, Global.getAppVersionCode());
            jSONObject.put("versionName", Global.getAppVersionName());
            jSONObject.put("phoneGuid", Global.getPhoneGuid());
            jSONObject.put("model", t.t());
            jSONObject.put("deviceName", t.u());
            jSONObject.put("freeMemory", t.r());
            jSONObject.put("totalMemory", t.q());
            jSONObject.put("activityCreateTime", this.G);
            jSONObject.put("webviewLoadTime", this.H);
        } catch (JSONException e) {
            XLog.e(z, "JSONException>>>", e);
        }
        return jSONObject.toString();
    }

    public String D() {
        return this.D;
    }

    /* access modifiers changed from: private */
    public void a(String str, int i, int i2) {
        STInfoV2 buildSTInfo;
        if ((this.n instanceof BaseActivity) && (buildSTInfo = STInfoBuilder.buildSTInfo(this.n, i)) != null) {
            buildSTInfo.scene = this.E;
            switch (i2) {
                case 0:
                    buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a(STConst.ST_DEFAULT_SLOT, Constants.STR_EMPTY);
                    break;
                case 1:
                    buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a("05", "001");
                    break;
                case 2:
                    buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a("05", "002");
                    break;
                case 3:
                    buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a(STConst.ST_STATUS_DEFAULT, "001");
                    break;
            }
            if (str != null) {
                buildSTInfo.status = str;
            }
            if (i == 100) {
                if (this.F == null) {
                    this.F = new b();
                }
                this.F.exposure(buildSTInfo);
                return;
            }
            k.a(buildSTInfo);
        }
    }

    public int f() {
        return this.E;
    }
}
