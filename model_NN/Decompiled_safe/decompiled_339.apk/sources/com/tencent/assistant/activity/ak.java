package com.tencent.assistant.activity;

import android.view.animation.Animation;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class ak implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ aj f399a;

    ak(aj ajVar) {
        this.f399a = ajVar;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        if (this.f399a.f398a.ac) {
            this.f399a.f398a.O.setText(this.f399a.f398a.getString(R.string.backup_success));
        } else {
            this.f399a.f398a.O.setText(this.f399a.f398a.getString(R.string.backup_failed));
        }
        this.f399a.f398a.n.setEnabled(true);
        this.f399a.f398a.n.setText(this.f399a.f398a.getResources().getString(R.string.app_backup_list));
    }
}
