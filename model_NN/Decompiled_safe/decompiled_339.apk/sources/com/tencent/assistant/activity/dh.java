package com.tencent.assistant.activity;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import java.util.List;

/* compiled from: ProGuard */
class dh extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GroupListActivity f523a;

    dh(GroupListActivity groupListActivity) {
        this.f523a = groupListActivity;
    }

    public void onLoadInstalledApkSuccess(List<LocalApkInfo> list) {
        this.f523a.runOnUiThread(new di(this));
    }
}
