package com.tencent.assistant.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.ChildSettingAdapter;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.model.ItemElement;
import java.util.List;

/* compiled from: ProGuard */
public class ChildSettingActivity extends BaseActivity {
    private static List<ItemElement> x;
    private Context n;
    private SecondNavigationTitleViewV5 t;
    private ListView u;
    private ChildSettingAdapter v;
    private String w;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_child_setting);
        this.n = this;
        this.w = getIntent().getStringExtra("child_setting_title");
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            x = (List) extras.getSerializable("child_setting_page_key");
        }
        i();
        j();
    }

    private void i() {
        this.v = new ChildSettingAdapter(this.n);
        if (x != null) {
            this.v.a(x);
        }
    }

    private void j() {
        this.t = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.t.a(this);
        this.t.b(this.w);
        this.t.d();
        this.t.c(new bx(this));
        this.u = (ListView) findViewById(R.id.list_view);
        this.u.setAdapter((ListAdapter) this.v);
        this.u.setDivider(null);
    }

    public int f() {
        return STConst.ST_PAGE_SETTING_MESSAGE;
    }

    public boolean onMenuOpened(int i, Menu menu) {
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
}
