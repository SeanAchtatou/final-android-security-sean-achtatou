package com.tencent.assistant.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.view.KeyEvent;
import android.view.ViewStub;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.component.FooterView;
import com.tencent.assistant.component.NormalErrorPage;
import com.tencent.assistant.component.listview.RubbishResultListView;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.manager.spaceclean.SpaceScanManager;
import com.tencent.assistant.model.PluginStartEntry;
import com.tencent.assistant.model.spaceclean.SubRubbishInfo;
import com.tencent.assistant.model.spaceclean.a;
import com.tencent.assistant.module.callback.ak;
import com.tencent.assistant.module.callback.g;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.e;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.activity.AssistantTabActivity;
import com.tencent.assistantv2.component.ScaningProgressView;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.component.TxManagerCommContainView;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.tmsecurelite.commom.b;
import com.tencent.tmsecurelite.optimize.f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;

/* compiled from: ProGuard */
public class SpaceCleanActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public TxManagerCommContainView A;
    /* access modifiers changed from: private */
    public ScaningProgressView B;
    private RubbishResultListView C;
    private ViewStub D = null;
    private NormalErrorPage E = null;
    /* access modifiers changed from: private */
    public Map<Integer, ArrayList<a>> F = new HashMap();
    /* access modifiers changed from: private */
    public int G = 0;
    /* access modifiers changed from: private */
    public volatile long H = 0;
    /* access modifiers changed from: private */
    public volatile long I = 0;
    private long J = 0;
    /* access modifiers changed from: private */
    public long K = 0;
    /* access modifiers changed from: private */
    public boolean L = true;
    /* access modifiers changed from: private */
    public boolean M = false;
    /* access modifiers changed from: private */
    public boolean N = false;
    /* access modifiers changed from: private */
    public boolean O = false;
    /* access modifiers changed from: private */
    public PluginStartEntry P = null;
    /* access modifiers changed from: private */
    public long Q = 0;
    /* access modifiers changed from: private */
    public long R = 0;
    /* access modifiers changed from: private */
    public long S = 0;
    private long T = 0;
    /* access modifiers changed from: private */
    public long U = 0;
    private boolean V = false;
    /* access modifiers changed from: private */
    public Handler W = new fv(this);
    /* access modifiers changed from: private */
    public ak X = new fz(this);
    private g Y = new gb(this);
    /* access modifiers changed from: private */
    public f Z = new gc(this);
    /* access modifiers changed from: private */
    public b aa = new fp(this);
    private final int n = 1;
    private final int t = -1;
    private final int u = -2;
    /* access modifiers changed from: private */
    public Context v;
    private boolean w = false;
    /* access modifiers changed from: private */
    public boolean x = false;
    private SecondNavigationTitleViewV5 y;
    /* access modifiers changed from: private */
    public FooterView z = null;

    static /* synthetic */ long h(SpaceCleanActivity spaceCleanActivity, long j) {
        long j2 = spaceCleanActivity.H + j;
        spaceCleanActivity.H = j2;
        return j2;
    }

    static /* synthetic */ long i(SpaceCleanActivity spaceCleanActivity, long j) {
        long j2 = spaceCleanActivity.I + j;
        spaceCleanActivity.I = j2;
        return j2;
    }

    static /* synthetic */ int w(SpaceCleanActivity spaceCleanActivity) {
        int i = spaceCleanActivity.G;
        spaceCleanActivity.G = i + 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            setContentView((int) R.layout.activity_rubbish_clear);
            this.v = this;
            x();
            v();
            i();
            y();
            j();
        } catch (Throwable th) {
            this.V = true;
            finish();
            cq.a().b();
        }
    }

    private void j() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", t.g());
        XLog.d("beacon", "beacon report >> expose_spaceclean. " + hashMap.toString());
        com.tencent.beacon.event.a.a("expose_spaceclean", true, -1, -1, hashMap, true);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.V) {
            super.onResume();
            return;
        }
        if (this.y != null) {
            this.y.l();
        }
        super.onResume();
        this.L = true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.V) {
            super.onPause();
            return;
        }
        this.L = false;
        if (this.y != null) {
            this.y.m();
        }
        super.onPause();
    }

    private void v() {
        SpaceScanManager.a().a(this.Y);
    }

    private void w() {
        SpaceScanManager.a().b(this.Y);
    }

    private void x() {
        this.y = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.y.a(this);
        this.y.b(getString(R.string.rubbish_clear_title));
        this.y.d();
        this.y.c(new fo(this));
        this.A = (TxManagerCommContainView) findViewById(R.id.contentView);
        this.B = new ScaningProgressView(this.v);
        this.C = new RubbishResultListView(this.v);
        this.C.setHandleToAdapter(this.W);
        this.A.a(this.B);
        this.A.b(this.C);
        this.z = new FooterView(this.v);
        this.A.a(this.z, new RelativeLayout.LayoutParams(-1, -2));
        this.z.updateContent(getString(R.string.rubbish_clear_one_key_delete));
        this.z.setFooterViewEnable(false);
        this.z.setOnFooterViewClickListener(new fu(this));
        this.D = (ViewStub) findViewById(R.id.error_stub);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    public void i() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.w = extras.getBoolean(com.tencent.assistant.b.a.O);
            if (this.r) {
                m.a().b("key_space_clean_last_push_clicked", (Object) true);
            }
            this.P = (PluginStartEntry) extras.getSerializable("dock_plugin");
        }
    }

    /* access modifiers changed from: private */
    public void y() {
        XLog.d("miles", "initData called...");
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME);
        boolean z2 = false;
        if (localApkInfo == null) {
            z2 = e.a(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME);
        }
        if (localApkInfo == null && !z2) {
            b(Constants.STR_EMPTY);
        } else if (!SpaceScanManager.a().e()) {
            b(Constants.STR_EMPTY);
        } else {
            this.W.sendEmptyMessage(1);
        }
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        Intent intent = new Intent(this, MobileManagerInstallActivity.class);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
        intent.putExtra("toast_message", str);
        startActivityForResult(intent, 1000);
    }

    /* access modifiers changed from: private */
    public void z() {
        this.x = true;
        this.T = System.currentTimeMillis();
        this.W.sendEmptyMessageDelayed(30, 90000);
        TemporaryThreadManager.get().start(new fx(this));
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", ((this.K / 1024) / 1024) + Constants.STR_EMPTY);
        hashMap.put("B2", this.K + Constants.STR_EMPTY);
        hashMap.put("B3", str);
        hashMap.put("B4", Global.getPhoneGuidAndGen());
        hashMap.put("B5", Global.getQUAForBeacon());
        XLog.d("beacon", "beacon report >> RubbishCleanTimeout. " + hashMap.toString());
        com.tencent.beacon.event.a.a("RubbishCleanTimeout", true, -1, -1, hashMap, true);
    }

    /* access modifiers changed from: private */
    public void A() {
        TemporaryThreadManager.get().start(new fy(this));
    }

    /* access modifiers changed from: private */
    public boolean a(PackageManager packageManager, String str) {
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 0);
            if ((applicationInfo.flags & 1) == 0 || (applicationInfo.flags & 128) != 0) {
                return true;
            }
            return false;
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: private */
    public long a(a aVar) {
        long j = 0;
        if (this.F == null || this.F.size() <= 0) {
            return 0;
        }
        ArrayList arrayList = this.F.get(1);
        ArrayList arrayList2 = this.F.get(0);
        if (arrayList != null && arrayList.size() > 0) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                a aVar2 = (a) it.next();
                if (aVar != null && aVar.e.equals(aVar2.e)) {
                    aVar2.c = aVar.c;
                    aVar2.a(aVar.f);
                }
                j += aVar2.c;
            }
        }
        if (arrayList2 == null || arrayList2.size() <= 0) {
            return j;
        }
        Iterator it2 = arrayList2.iterator();
        long j2 = j;
        while (it2.hasNext()) {
            a aVar3 = (a) it2.next();
            if (aVar != null && aVar.b.equals(aVar3.b)) {
                aVar3.c = aVar.c;
                aVar3.a(aVar.f);
            }
            j2 += aVar3.c;
        }
        return j2;
    }

    /* access modifiers changed from: private */
    public ArrayList<String> B() {
        this.K = 0;
        ArrayList<String> arrayList = new ArrayList<>();
        if (this.F != null && this.F.size() > 0) {
            ArrayList arrayList2 = this.F.get(1);
            ArrayList arrayList3 = this.F.get(0);
            if (arrayList2 != null && arrayList2.size() > 0) {
                Iterator it = arrayList2.iterator();
                while (it.hasNext()) {
                    Iterator<SubRubbishInfo> it2 = ((a) it.next()).f.iterator();
                    while (it2.hasNext()) {
                        SubRubbishInfo next = it2.next();
                        if (next.d) {
                            arrayList.addAll(next.f);
                            this.K += next.c;
                        }
                    }
                }
            }
            if (arrayList3 != null && arrayList3.size() > 0) {
                Iterator it3 = arrayList3.iterator();
                while (it3.hasNext()) {
                    Iterator<SubRubbishInfo> it4 = ((a) it3.next()).f.iterator();
                    while (it4.hasNext()) {
                        SubRubbishInfo next2 = it4.next();
                        if (next2.d) {
                            arrayList.addAll(next2.f);
                            this.K += next2.c;
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void a(long j, boolean z2) {
        if (z2) {
            d(bt.c(j));
        } else {
            b(1);
        }
    }

    private void d(String str) {
        STInfoV2 C2 = C();
        String string = getString(R.string.apkmgr_clear_success);
        String format = String.format(getString(R.string.rubbish_clear_delete_tips), str);
        String string2 = getString(R.string.rubbish_clear_delete_guide);
        SpannableString spannableString = new SpannableString(string2);
        spannableString.setSpan(new gd(this, new ga(this)), 0, string2.length(), 33);
        this.A.a(string, new SpannableString(format), spannableString);
        float c = (float) t.c();
        float d = (float) t.d();
        if (!(d == 0.0f || c == 0.0f)) {
            float f = c / d;
            XLog.d("miles", "SpaceCleanActivity >> internal avaliable memory percent is " + f);
            if (f < 0.2f) {
                this.A.a(R.drawable.icon_space_clean_xiaobao, getString(R.string.rubbish_skip_to_app_uninstall_tip), 20, 20, 4);
                this.A.a(this, InstalledAppManagerActivity.class, (PluginStartEntry) null);
                C2.slotId = "03_001";
                k.a(C2);
                e(getString(R.string.soft_admin));
                return;
            }
        }
        if (this.M && this.P != null) {
            this.A.a(R.drawable.icon_space_clean_xiaobao, getString(R.string.rubbish_skip_to_accelerate_plugin_tip), 20, 20, 3);
            this.A.a(this, this.P);
            C2.slotId = "03_001";
            k.a(C2);
            e(getString(R.string.mobile_accelerate_title));
        }
    }

    private void e(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", t.g());
        hashMap.put("B4", getString(R.string.rubbish_clear_title));
        hashMap.put("B5", str);
        XLog.d("beacon", "beacon report >> featureLinkExp. " + hashMap.toString());
        com.tencent.beacon.event.a.a("featureLinkExp", true, -1, -1, hashMap, true);
    }

    private STInfoV2 C() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, 100);
        buildSTInfo.scene = STConst.ST_PAGE_RUBBISH_CLEAR_FINISH_STEWARD;
        com.tencent.assistantv2.st.b.b.getInstance().exposure(buildSTInfo);
        return buildSTInfo;
    }

    private void b(int i) {
        if (this.E == null) {
            this.D.inflate();
            this.E = (NormalErrorPage) findViewById(R.id.error);
        }
        this.E.setErrorType(i);
        if (i == 1) {
            this.E.setErrorHint(getResources().getString(R.string.rubbish_clear_empty_tips));
            this.E.setErrorImage(R.drawable.emptypage_pic_02);
            this.E.setErrorHintTextColor(getResources().getColor(R.color.common_listiteminfo));
            this.E.setErrorHintTextSize(getResources().getDimension(R.dimen.appadmin_empty_page_text_size));
            this.E.setErrorTextVisibility(8);
            this.E.setErrorHintVisibility(0);
            this.E.setFreshButtonVisibility(8);
        }
        this.A.setVisibility(8);
        this.z.setVisibility(8);
        this.E.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void D() {
        long j = 0;
        HashMap hashMap = new HashMap();
        hashMap.put("B1", String.valueOf(this.S - this.R));
        hashMap.put("B2", ((this.H / 1024) / 1024) + Constants.STR_EMPTY);
        hashMap.put("B3", this.H + Constants.STR_EMPTY);
        hashMap.put("B4", Global.getPhoneGuidAndGen());
        hashMap.put("B5", Global.getQUAForBeacon());
        hashMap.put("B6", t.g());
        if (this.Q != 0) {
            j = this.S - this.Q;
        }
        hashMap.put("B7", String.valueOf(j));
        com.tencent.beacon.event.a.a("SpaceCleanScan", true, j, -1, hashMap, true);
        XLog.d("beacon", "beacon report >> event: SpaceCleanScan, totalTime : " + (this.S - this.Q) + ", params : " + hashMap.toString());
    }

    /* access modifiers changed from: private */
    public void a(String str, boolean z2) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", ((this.K / 1024) / 1024) + Constants.STR_EMPTY);
        hashMap.put("B2", this.K + Constants.STR_EMPTY);
        hashMap.put("B3", Global.getPhoneGuidAndGen());
        hashMap.put("B4", Global.getQUAForBeacon());
        hashMap.put("B5", t.g());
        hashMap.put("B6", String.valueOf(this.U - this.T));
        com.tencent.beacon.event.a.a(str, z2, this.U - this.T, -1, hashMap, true);
        XLog.d("beacon", "beacon report >> event: " + str + ", totalTime : " + (this.U - this.T) + ", params : " + hashMap.toString());
    }

    /* access modifiers changed from: private */
    public void a(boolean z2, String str, long j, String str2, JSONArray jSONArray) {
        ArrayList arrayList;
        a aVar;
        a aVar2;
        SubRubbishInfo subRubbishInfo;
        ArrayList arrayList2 = this.F.get(1);
        if (arrayList2 == null) {
            ArrayList arrayList3 = new ArrayList();
            this.F.put(1, arrayList3);
            arrayList = arrayList3;
        } else {
            arrayList = arrayList2;
        }
        Iterator it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                aVar = null;
                break;
            }
            aVar = (a) it.next();
            if (str.equals(aVar.e)) {
                break;
            }
        }
        if (aVar == null) {
            a aVar3 = new a();
            aVar3.f1675a = 1;
            aVar3.e = str;
            arrayList.add(aVar3);
            aVar2 = aVar3;
        } else {
            aVar2 = aVar;
        }
        Iterator<SubRubbishInfo> it2 = aVar2.f.iterator();
        while (true) {
            if (!it2.hasNext()) {
                subRubbishInfo = null;
                break;
            }
            subRubbishInfo = it2.next();
            if (str2.equals(subRubbishInfo.b)) {
                break;
            }
        }
        if (subRubbishInfo == null) {
            aVar2.a(new SubRubbishInfo(str2, j, z2, jSONArray));
        } else {
            subRubbishInfo.c += j;
            subRubbishInfo.a(jSONArray);
        }
        aVar2.d += j;
        if (z2) {
            aVar2.c += j;
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z2, String str, long j, String str2, JSONArray jSONArray) {
        ArrayList arrayList;
        a aVar;
        a aVar2;
        SubRubbishInfo subRubbishInfo;
        ArrayList arrayList2 = this.F.get(0);
        if (arrayList2 == null) {
            ArrayList arrayList3 = new ArrayList();
            this.F.put(0, arrayList3);
            arrayList = arrayList3;
        } else {
            arrayList = arrayList2;
        }
        Iterator it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                aVar = null;
                break;
            }
            aVar = (a) it.next();
            if (str.equals(aVar.b)) {
                break;
            }
        }
        if (aVar == null) {
            a aVar3 = new a();
            aVar3.f1675a = 0;
            aVar3.b = str;
            arrayList.add(aVar3);
            aVar2 = aVar3;
        } else {
            aVar2 = aVar;
        }
        Iterator<SubRubbishInfo> it2 = aVar2.f.iterator();
        while (true) {
            if (!it2.hasNext()) {
                subRubbishInfo = null;
                break;
            }
            subRubbishInfo = it2.next();
            if (str2.equals(subRubbishInfo.b)) {
                break;
            }
        }
        if (subRubbishInfo == null) {
            aVar2.a(new SubRubbishInfo(str2, j, z2, jSONArray));
        } else {
            subRubbishInfo.c += j;
            subRubbishInfo.a(jSONArray);
        }
        aVar2.d += j;
        if (z2) {
            aVar2.c += j;
        }
    }

    /* access modifiers changed from: private */
    public void a(long j, boolean z2, int i) {
        if (z2) {
            this.B.a(j);
        } else if (i == 1) {
            this.B.b(j);
            this.B.c();
            this.W.post(new fq(this, i));
        } else if (i == 2) {
            this.B.a(j);
            this.W.postDelayed(new fs(this), 1000);
        } else {
            this.B.b(j);
            this.W.post(new ft(this));
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        if (z2) {
            this.C.setVisibility(8);
        } else if (this.F != null && !this.F.isEmpty()) {
            this.C.refreshData(this.F);
            this.C.setVisibility(0);
            if (this.E != null) {
                this.E.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(long j) {
        String c = bt.c(j);
        String string = getString(R.string.rubbish_clear_one_key_delete);
        if (j > 0) {
            this.z.setFooterViewEnable(true);
            this.z.updateContent(string, " " + String.format(getString(R.string.rubbish_clear_one_key_delete_extra), c));
            return;
        }
        this.z.setFooterViewEnable(false);
        this.z.updateContent(string);
    }

    public int f() {
        return STConst.ST_PAGE_GARBAGE_INSTALL_STEWARD;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.V) {
            super.onDestroy();
            return;
        }
        w();
        com.tencent.assistant.utils.b.a();
        if (this.w && this.x) {
            m.a().b("key_space_clean_has_run_clean", (Object) true);
        }
        if (this.W != null) {
            this.W.removeMessages(30);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        XLog.d("miles", "onActivityResult called. requestCode = " + i + ", resultCode = " + i2);
        if (i != 1000) {
            return;
        }
        if (i2 == 0) {
            finish();
        } else if (i2 == -1) {
            y();
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (4 != i) {
            return super.onKeyDown(i, keyEvent);
        }
        E();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void E() {
        if (!this.r) {
            finish();
            return;
        }
        Intent intent = new Intent(this, AssistantTabActivity.class);
        intent.setFlags(67108864);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
        intent.putExtra(com.tencent.assistant.b.a.G, true);
        startActivity(intent);
        this.r = false;
        finish();
        XLog.i("miles", "SpaceCleanActivity >> key back finish");
    }
}
