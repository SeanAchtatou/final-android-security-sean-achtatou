package com.tencent.assistant.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.SpecialTopicAdapter;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.q;
import com.tencent.assistant.module.de;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: ProGuard */
public class SpecailTopicActivity extends BaseActivity implements ITXRefreshListViewListener, q {
    private View.OnClickListener A = new ge(this);
    private Context n;
    private TXGetMoreListView t;
    private SecondNavigationTitleViewV5 u;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage v;
    private LoadingView w;
    /* access modifiers changed from: private */
    public de x;
    private int y = 3;
    private SpecialTopicAdapter z;

    public int f() {
        return STConst.ST_PAGE_SPECIAL;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.n = this;
        setContentView((int) R.layout.activity_specail_topic_layout);
        j();
        i();
    }

    private void i() {
        this.x = new de();
        this.x.register(this);
        this.x.a(3);
        this.z = new SpecialTopicAdapter(this.n, null);
        this.t.setAdapter(this.z);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.u.l();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.u.m();
    }

    private void j() {
        this.w = (LoadingView) findViewById(R.id.loading_view);
        this.v = (NormalErrorRecommendPage) findViewById(R.id.topic_network_error);
        this.v.setOnClickListener(this.A);
        this.v.setButtonClickListener(this.A);
        this.u = (SecondNavigationTitleViewV5) findViewById(R.id.topic_title_view);
        this.u.a(this);
        this.u.b(getResources().getString(R.string.special_topic));
        this.u.i();
        this.t = (TXGetMoreListView) findViewById(R.id.topic_list);
        this.t.setDivider(null);
        this.t.setSelector(getResources().getDrawable(R.drawable.transparent_selector));
        this.t.setRefreshListViewListener(this);
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (TXScrollViewBase.ScrollState.ScrollState_FromEnd == scrollState) {
            this.x.e();
        }
    }

    public void a(int i, int i2, boolean z2, Map<AppGroupInfo, ArrayList<SimpleAppModel>> map, ArrayList<Long> arrayList, boolean z3) {
        if (i2 == 0) {
            if (map != null && map.size() > 0) {
                v();
                this.z.a(map, z2);
                this.t.onRefreshComplete(z3, true);
            } else if (z2) {
                b(10);
            } else {
                Toast.makeText(this.n, (int) R.string.is_next_page_error_happen, 0).show();
            }
        } else if (-800 == i2) {
            if (z2 || this.z == null || this.z.getCount() == 0) {
                b(30);
                return;
            }
            Toast.makeText(this.n, (int) R.string.is_next_page_error_happen, 0).show();
            this.t.onRefreshComplete(true, false);
        } else if (this.y > 0) {
            if (this.z == null || this.z.a() == 0) {
                this.x.a(3);
            } else {
                this.x.e();
            }
            this.y--;
        } else if (this.z == null || this.z.a() == 0) {
            b(20);
        } else {
            Toast.makeText(this.n, (int) R.string.is_next_page_error_happen, 0).show();
            this.t.onRefreshComplete(true, false);
        }
    }

    private void b(int i) {
        this.t.setVisibility(8);
        this.v.setVisibility(0);
        this.w.setVisibility(8);
        this.v.setErrorType(i);
    }

    private void v() {
        this.t.setVisibility(0);
        this.w.setVisibility(8);
        this.v.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void w() {
        this.t.setVisibility(8);
        this.w.setVisibility(0);
        this.v.setVisibility(8);
    }
}
