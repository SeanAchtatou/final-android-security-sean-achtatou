package com.tencent.assistant.activity.debug;

import android.view.View;
import android.widget.Toast;
import com.tencent.assistant.component.CommentDetailView;

/* compiled from: ProGuard */
class h implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f504a;

    h(DActivity dActivity) {
        this.f504a = dActivity;
    }

    public void onClick(View view) {
        CommentDetailView.isTestMode = !CommentDetailView.isTestMode;
        if (CommentDetailView.isTestMode) {
            Toast.makeText(this.f504a.getApplicationContext(), "评论主人态打开", 1).show();
        } else {
            Toast.makeText(this.f504a.getApplicationContext(), "评论主人态关闭", 1).show();
        }
    }
}
