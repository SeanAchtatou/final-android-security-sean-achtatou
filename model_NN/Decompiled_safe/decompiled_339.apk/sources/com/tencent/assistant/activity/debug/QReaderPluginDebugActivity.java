package com.tencent.assistant.activity.debug;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.tencent.assistant.component.LineBreakLayout;

/* compiled from: ProGuard */
public class QReaderPluginDebugActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private ViewGroup f472a;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        LineBreakLayout lineBreakLayout = new LineBreakLayout(this);
        setContentView(lineBreakLayout, new ViewGroup.LayoutParams(-1, -1));
        this.f472a = lineBreakLayout;
        a("下载电子书", new am(this));
        a("下载免费全本电子书", new an(this));
        a("批量下载电子书", new ao(this));
        a("阅读电子书", new ap(this));
        a("删除电子书", new aq(this));
        a("书籍详情页", new ar(this));
        a("充值（2）", new as(this));
        a("充值（3）", new at(this));
        a("触发QQ阅读客户端下载", new au(this));
    }

    private void a(String str, View.OnClickListener onClickListener) {
        Button button = new Button(this);
        button.setText(str);
        button.setOnClickListener(onClickListener);
        this.f472a.addView(button);
    }
}
