package com.tencent.assistant.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.ca;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXExpandableListView;
import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.q;
import com.tencent.assistant.module.de;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: ProGuard */
public class GroupListActivity extends BaseActivity implements ITXRefreshListViewListener, q {
    private int A = 3;
    /* access modifiers changed from: private */
    public int B = 0;
    /* access modifiers changed from: private */
    public int C;
    /* access modifiers changed from: private */
    public int D = 0;
    /* access modifiers changed from: private */
    public int E = 0;
    private ApkResCallback F = new dh(this);
    private View.OnClickListener G = new dl(this);
    private AbsListView.OnScrollListener H = new dm(this);
    /* access modifiers changed from: private */
    public TXExpandableListView n;
    private SecondNavigationTitleViewV5 t;
    private NormalErrorRecommendPage u;
    private LoadingView v;
    /* access modifiers changed from: private */
    public ca w;
    /* access modifiers changed from: private */
    public de x;
    /* access modifiers changed from: private */
    public RelativeLayout y = null;
    /* access modifiers changed from: private */
    public TextView z = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.group_applist_layout);
        i();
        v();
        j();
        h();
    }

    public int f() {
        if (this.E == 1) {
            return STConst.ST_PAGE_NECESSARY;
        }
        if (this.E == 2) {
            return STConst.ST_PAGE_HOT;
        }
        return 2000;
    }

    public boolean g() {
        return false;
    }

    private void i() {
        this.E = getIntent().getIntExtra("com.tencent.assistant.APP_GROUP_TYPE", 1);
    }

    private void j() {
        this.u = (NormalErrorRecommendPage) findViewById(R.id.network_error);
        this.u.setButtonClickListener(this.G);
        this.v = (LoadingView) findViewById(R.id.loading_view);
        this.v.setVisibility(0);
        this.n = (TXExpandableListView) findViewById(R.id.group_list);
        this.n.setGroupIndicator(null);
        this.n.setDivider(null);
        this.n.setChildDivider(null);
        this.n.setSelector(R.drawable.transparent_selector);
        this.n.setRefreshListViewListener(this);
        this.n.setOnScrollListener(this.H);
        this.n.setOnGroupClickListener(new dj(this));
        this.w = new ca(this, this.n, f());
        this.x = new de(true);
        TemporaryThreadManager.get().start(new dk(this));
        this.n.setAdapter(this.w);
        this.D = this.w.getGroupCount();
        this.z = (TextView) findViewById(R.id.group_title);
    }

    private void v() {
        String string;
        this.t = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.t.a(this);
        if (2 == this.E) {
            string = getResources().getString(R.string.hot);
        } else {
            string = getResources().getString(R.string.necessary);
        }
        this.t.b(string);
        this.t.i();
        this.t.bringToFront();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.n = null;
        this.x.a();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.w.b();
        this.x.unregister(this);
        super.onPause();
        ApkResourceManager.getInstance().unRegisterApkResCallback(this.F);
        this.t.m();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.w.a();
        this.x.register(this);
        ApkResourceManager.getInstance().registerApkResCallback(this.F);
        this.t.l();
    }

    private void b(int i) {
        this.n.setVisibility(8);
        this.u.setVisibility(0);
        this.v.setVisibility(8);
        this.u.setErrorType(i);
    }

    private void w() {
        this.n.setVisibility(0);
        this.v.setVisibility(8);
        this.u.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void x() {
        this.n.setVisibility(8);
        this.v.setVisibility(0);
        this.u.setVisibility(8);
    }

    public void a(int i, int i2, boolean z2, Map<AppGroupInfo, ArrayList<SimpleAppModel>> map, ArrayList<Long> arrayList, boolean z3) {
        if (i2 == 0) {
            if (map.size() > 0) {
                w();
                this.w.a(z2, map, this.x.c(), z3);
                for (int i3 = 0; i3 < this.w.getGroupCount(); i3++) {
                    this.n.expandGroup(i3);
                }
                this.D = this.w.getGroupCount();
            } else if (z2 && this.w != null && this.w.getGroupCount() == 0) {
                b(10);
            }
            this.n.onRefreshComplete(z3);
        } else if (!z2) {
            this.n.onRefreshComplete(z3);
        } else if (-800 == i2) {
            b(30);
        } else if (this.A <= 0) {
            b(20);
        } else {
            this.x.a(this.E);
            this.A--;
        }
    }

    /* access modifiers changed from: private */
    public int y() {
        int i = this.C;
        int pointToPosition = this.n.pointToPosition(0, this.C);
        if (pointToPosition == -1 || ExpandableListView.getPackedPositionGroup(this.n.getExpandableListPosition(pointToPosition)) == this.B) {
            return i;
        }
        View expandChildAt = this.n.getExpandChildAt(pointToPosition - this.n.getFirstVisiblePosition());
        if (expandChildAt != null) {
            return expandChildAt.getTop();
        }
        return 100;
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (TXRefreshScrollViewBase.RefreshState.RESET == this.n.getMoreRefreshState() && TXScrollViewBase.ScrollState.ScrollState_FromEnd == scrollState) {
            this.x.e();
        }
    }
}
