package com.tencent.assistant.activity;

import com.tencent.assistant.appbakcup.i;
import com.tencent.assistant.protocol.jce.BackupApp;
import com.tencent.assistant.utils.TemporaryThreadManager;
import java.util.ArrayList;

/* compiled from: ProGuard */
class aq implements i {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppBackupActivity f405a;

    private aq(AppBackupActivity appBackupActivity) {
        this.f405a = appBackupActivity;
    }

    /* synthetic */ aq(AppBackupActivity appBackupActivity, ab abVar) {
        this(appBackupActivity);
    }

    public void a(ArrayList<BackupApp> arrayList) {
        TemporaryThreadManager.get().start(new ar(this, arrayList));
    }
}
