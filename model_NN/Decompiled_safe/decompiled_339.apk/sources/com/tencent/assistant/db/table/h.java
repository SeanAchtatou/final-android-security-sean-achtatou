package com.tencent.assistant.db.table;

import java.util.List;

/* compiled from: ProGuard */
public class h extends g {

    /* renamed from: a  reason: collision with root package name */
    private static h f1238a = null;

    public static synchronized h a() {
        h hVar;
        synchronized (h.class) {
            if (f1238a == null) {
                f1238a = new h();
            }
            hVar = f1238a;
        }
        return hVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005e, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        r3.endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0065, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0066, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x007f, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0080, code lost:
        r1 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x005e A[ExcHandler: all (th java.lang.Throwable), Splitter:B:6:0x000d] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0061 A[SYNTHETIC, Splitter:B:30:0x0061] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.util.List<com.qq.l.d> r9) {
        /*
            r8 = this;
            r1 = 1
            r2 = 0
            if (r9 == 0) goto L_0x000a
            boolean r0 = r9.isEmpty()
            if (r0 == 0) goto L_0x000c
        L_0x000a:
            r0 = r2
        L_0x000b:
            return r0
        L_0x000c:
            r3 = 0
            com.tencent.assistant.db.helper.SqliteHelper r0 = r8.getHelper()     // Catch:{ Throwable -> 0x007f, all -> 0x005e }
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r3 = r0.getWritableDatabaseWrapper()     // Catch:{ Throwable -> 0x007f, all -> 0x005e }
            if (r3 == 0) goto L_0x0074
            r3.beginTransaction()     // Catch:{ Throwable -> 0x0048, all -> 0x005e }
            java.lang.String r0 = "INSERT INTO st_connection_data (aid, content) values (?, ?)"
            android.database.sqlite.SQLiteStatement r4 = r3.compileStatement(r0)     // Catch:{ Throwable -> 0x0048, all -> 0x005e }
            java.util.Iterator r5 = r9.iterator()     // Catch:{ Throwable -> 0x0048, all -> 0x005e }
        L_0x0024:
            boolean r0 = r5.hasNext()     // Catch:{ Throwable -> 0x0048, all -> 0x005e }
            if (r0 == 0) goto L_0x0054
            java.lang.Object r0 = r5.next()     // Catch:{ Throwable -> 0x0048, all -> 0x005e }
            com.qq.l.d r0 = (com.qq.l.d) r0     // Catch:{ Throwable -> 0x0048, all -> 0x005e }
            r6 = 1
            java.lang.String r7 = r0.b()     // Catch:{ Throwable -> 0x0048, all -> 0x005e }
            r4.bindString(r6, r7)     // Catch:{ Throwable -> 0x0048, all -> 0x005e }
            r6 = 2
            java.lang.String r0 = r0.c()     // Catch:{ Throwable -> 0x0048, all -> 0x005e }
            byte[] r0 = r0.getBytes()     // Catch:{ Throwable -> 0x0048, all -> 0x005e }
            r4.bindBlob(r6, r0)     // Catch:{ Throwable -> 0x0048, all -> 0x005e }
            r4.executeInsert()     // Catch:{ Throwable -> 0x0048, all -> 0x005e }
            goto L_0x0024
        L_0x0048:
            r0 = move-exception
            r1 = r3
        L_0x004a:
            r0.printStackTrace()     // Catch:{ all -> 0x007c }
            if (r1 == 0) goto L_0x0052
            r1.endTransaction()     // Catch:{ Throwable -> 0x006a }
        L_0x0052:
            r0 = r2
            goto L_0x000b
        L_0x0054:
            r3.setTransactionSuccessful()     // Catch:{ Throwable -> 0x0048, all -> 0x005e }
            if (r3 == 0) goto L_0x005c
            r3.endTransaction()     // Catch:{ Throwable -> 0x006f }
        L_0x005c:
            r0 = r1
            goto L_0x000b
        L_0x005e:
            r0 = move-exception
        L_0x005f:
            if (r3 == 0) goto L_0x0064
            r3.endTransaction()     // Catch:{ Throwable -> 0x0065 }
        L_0x0064:
            throw r0
        L_0x0065:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0064
        L_0x006a:
            r0 = move-exception
        L_0x006b:
            r0.printStackTrace()
            goto L_0x0052
        L_0x006f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x005c
        L_0x0074:
            if (r3 == 0) goto L_0x0052
            r3.endTransaction()     // Catch:{ Throwable -> 0x007a }
            goto L_0x0052
        L_0x007a:
            r0 = move-exception
            goto L_0x006b
        L_0x007c:
            r0 = move-exception
            r3 = r1
            goto L_0x005f
        L_0x007f:
            r0 = move-exception
            r1 = r3
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.h.a(java.util.List):boolean");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v0, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v0, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v1, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v3, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x006b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.qq.l.d> a(int r12) {
        /*
            r11 = this;
            r9 = 0
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            if (r12 <= 0) goto L_0x0082
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "0,"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r12)
            java.lang.String r8 = r0.toString()
        L_0x001b:
            com.tencent.assistant.db.helper.SqliteHelper r0 = r11.getHelper()     // Catch:{ Exception -> 0x0070 }
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()     // Catch:{ Exception -> 0x0070 }
            java.lang.String r1 = "st_connection_data"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            java.lang.String r7 = "_id asc"
            android.database.Cursor r9 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0070 }
            if (r9 == 0) goto L_0x0069
            boolean r0 = r9.moveToFirst()     // Catch:{ Exception -> 0x0070 }
            if (r0 == 0) goto L_0x0069
        L_0x0038:
            java.lang.String r0 = "_id"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0070 }
            long r0 = r9.getLong(r0)     // Catch:{ Exception -> 0x0070 }
            java.lang.String r2 = "aid"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x0070 }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x0070 }
            java.lang.String r3 = "content"
            int r3 = r9.getColumnIndex(r3)     // Catch:{ Exception -> 0x0070 }
            byte[] r3 = r9.getBlob(r3)     // Catch:{ Exception -> 0x0070 }
            com.qq.l.d r4 = new com.qq.l.d     // Catch:{ Exception -> 0x0070 }
            java.lang.String r5 = new java.lang.String     // Catch:{ Exception -> 0x0070 }
            r5.<init>(r3)     // Catch:{ Exception -> 0x0070 }
            r4.<init>(r0, r2, r5)     // Catch:{ Exception -> 0x0070 }
            r10.add(r4)     // Catch:{ Exception -> 0x0070 }
            boolean r0 = r9.moveToNext()     // Catch:{ Exception -> 0x0070 }
            if (r0 != 0) goto L_0x0038
        L_0x0069:
            if (r9 == 0) goto L_0x006e
            r9.close()
        L_0x006e:
            r0 = r10
        L_0x006f:
            return r0
        L_0x0070:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x007b }
            if (r9 == 0) goto L_0x0079
            r9.close()
        L_0x0079:
            r0 = r10
            goto L_0x006f
        L_0x007b:
            r0 = move-exception
            if (r9 == 0) goto L_0x0081
            r9.close()
        L_0x0081:
            throw r0
        L_0x0082:
            r8 = r9
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.h.a(int):java.util.List");
    }

    public boolean b(List<Long> list) {
        if (list == null || list.size() == 0) {
            return false;
        }
        StringBuffer stringBuffer = new StringBuffer("(");
        for (Long append : list) {
            stringBuffer.append(append);
            stringBuffer.append(",");
        }
        if (stringBuffer.length() > 1) {
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        }
        stringBuffer.append(")");
        getHelper().getWritableDatabaseWrapper().delete("st_connection_data", "_id in " + stringBuffer.toString(), null);
        return true;
    }
}
