package com.tencent.assistant.db.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.tencent.assistant.db.table.r;

/* compiled from: ProGuard */
public class PluginDbHelper extends SqliteHelper {
    private static final String DB_NAME = "mobile_plugin.db";
    private static final int DB_VERSION = 2;
    private static final Class<?>[] TABLESS = {r.class};
    private static volatile SqliteHelper instance;

    public static synchronized SqliteHelper get(Context context) {
        SqliteHelper sqliteHelper;
        synchronized (PluginDbHelper.class) {
            if (instance == null) {
                instance = new PluginDbHelper(context, DB_NAME, null, 2);
            }
            sqliteHelper = instance;
        }
        return sqliteHelper;
    }

    public PluginDbHelper(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, DB_NAME, null, i);
    }

    public Class<?>[] getTables() {
        return TABLESS;
    }

    public int getDBVersion() {
        return 2;
    }
}
