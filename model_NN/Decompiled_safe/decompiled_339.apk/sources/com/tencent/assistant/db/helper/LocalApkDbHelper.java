package com.tencent.assistant.db.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.tencent.assistant.db.table.m;

/* compiled from: ProGuard */
public class LocalApkDbHelper extends SqliteHelper {
    private static final String DB_NAME = "getapkinfo_service.db";
    private static final int DB_VERSION = 1;
    private static final Class<?>[] TABLESS = {m.class};
    private static volatile LocalApkDbHelper instance;

    public static synchronized LocalApkDbHelper get(Context context) {
        LocalApkDbHelper localApkDbHelper;
        synchronized (LocalApkDbHelper.class) {
            if (instance == null) {
                instance = new LocalApkDbHelper(context, DB_NAME, null, 1);
            }
            localApkDbHelper = instance;
        }
        return localApkDbHelper;
    }

    public LocalApkDbHelper(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, DB_NAME, null, i);
    }

    public Class<?>[] getTables() {
        return TABLESS;
    }

    public int getDBVersion() {
        return 1;
    }
}
