package com.tencent.assistant.db.contentprovider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import com.tencent.assistant.db.helper.ExternalDbHelper;
import com.tencent.assistant.db.helper.SqliteHelper;

/* compiled from: ProGuard */
public class ExternalProvider extends ContentProvider {

    /* renamed from: a  reason: collision with root package name */
    private SqliteHelper f1231a;

    public boolean onCreate() {
        this.f1231a = ExternalDbHelper.get(getContext());
        return true;
    }

    public String getType(Uri uri) {
        b bVar = new b(uri, null, null);
        if (TextUtils.isEmpty(bVar.b)) {
            return "vnd.android.cursor.dir/" + bVar.f1233a;
        }
        return "vnd.android.cursor.item/" + bVar.f1233a;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        b bVar = new b(uri, str, strArr2);
        if ("qube_theme_download_status".equals(bVar.f1233a) || "qube_app_update_info".equals(bVar.f1233a)) {
            SQLiteDatabase readableDatabase = this.f1231a.getReadableDatabase();
            SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
            sQLiteQueryBuilder.setTables(bVar.f1233a);
            return sQLiteQueryBuilder.query(readableDatabase, strArr, bVar.b, bVar.c, null, null, str2);
        }
        try {
            throw new NotSupportOperationException("query is not supported");
        } catch (NotSupportOperationException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        try {
            throw new NotSupportOperationException("insert is not supported");
        } catch (NotSupportOperationException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int delete(Uri uri, String str, String[] strArr) {
        try {
            throw new NotSupportOperationException("delete is not supported");
        } catch (NotSupportOperationException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        try {
            throw new NotSupportOperationException("update is not supported");
        } catch (NotSupportOperationException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
