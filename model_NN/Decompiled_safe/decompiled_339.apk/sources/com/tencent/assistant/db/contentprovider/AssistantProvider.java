package com.tencent.assistant.db.contentprovider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.net.Uri;
import android.text.TextUtils;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.downloadsdk.storage.helper.SDKDBHelper;
import com.tencent.downloadsdk.storage.helper.SqliteHelper;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class AssistantProvider extends ContentProvider {
    private static boolean b = true;
    private static final UriMatcher c = new UriMatcher(-1);

    /* renamed from: a  reason: collision with root package name */
    private SqliteHelper f1230a;

    static {
        c.addURI("com.tencent.android.qqdownloader.provider", "downloadsinfo", 1001);
        c.addURI("com.tencent.android.qqdownloader.provider", "downloadsinfo/#", 1001);
    }

    public boolean onCreate() {
        this.f1230a = SDKDBHelper.getDBHelper(getContext());
        return true;
    }

    public String getType(Uri uri) {
        a aVar = new a(uri, null, null);
        if (TextUtils.isEmpty(aVar.b)) {
            return "vnd.android.cursor.dir/" + aVar.f1232a;
        }
        return "vnd.android.cursor.item/" + aVar.f1232a;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00d1  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00d9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.database.Cursor query(android.net.Uri r15, java.lang.String[] r16, java.lang.String r17, java.lang.String[] r18, java.lang.String r19) {
        /*
            r14 = this;
            r12 = 0
            com.tencent.downloadsdk.storage.helper.SqliteHelper r2 = r14.f1230a
            if (r2 != 0) goto L_0x000f
            android.content.Context r2 = r14.getContext()
            com.tencent.downloadsdk.storage.helper.SqliteHelper r2 = com.tencent.downloadsdk.storage.helper.SDKDBHelper.getDBHelper(r2)
            r14.f1230a = r2
        L_0x000f:
            com.tencent.assistant.db.contentprovider.a r13 = new com.tencent.assistant.db.contentprovider.a
            r0 = r17
            r1 = r18
            r13.<init>(r15, r0, r1)
            android.content.UriMatcher r2 = com.tencent.assistant.db.contentprovider.AssistantProvider.c
            int r3 = r2.match(r15)
            r10 = 0
            com.tencent.downloadsdk.storage.helper.SqliteHelper r2 = r14.f1230a     // Catch:{ Exception -> 0x00c8, all -> 0x00d6 }
            android.database.sqlite.SQLiteDatabase r2 = r2.getReadableDatabase()     // Catch:{ Exception -> 0x00c8, all -> 0x00d6 }
            switch(r3) {
                case 1001: goto L_0x0048;
                default: goto L_0x0028;
            }
        L_0x0028:
            r11 = r10
        L_0x0029:
            android.database.sqlite.SQLiteQueryBuilder r3 = new android.database.sqlite.SQLiteQueryBuilder     // Catch:{ Exception -> 0x00e9, all -> 0x00e0 }
            r3.<init>()     // Catch:{ Exception -> 0x00e9, all -> 0x00e0 }
            java.lang.String r4 = r13.f1232a     // Catch:{ Exception -> 0x00e9, all -> 0x00e0 }
            r3.setTables(r4)     // Catch:{ Exception -> 0x00e9, all -> 0x00e0 }
            java.lang.String r6 = r13.b     // Catch:{ Exception -> 0x00e9, all -> 0x00e0 }
            java.lang.String[] r7 = r13.c     // Catch:{ Exception -> 0x00e9, all -> 0x00e0 }
            r8 = 0
            r9 = 0
            r4 = r2
            r5 = r16
            r10 = r19
            android.database.Cursor r2 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x00e9, all -> 0x00e0 }
            if (r11 == 0) goto L_0x0047
            r11.close()
        L_0x0047:
            return r2
        L_0x0048:
            java.lang.String r3 = "downloadsinfo"
            java.lang.String r4 = r13.f1232a     // Catch:{ Exception -> 0x00c8, all -> 0x00d6 }
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x00c8, all -> 0x00d6 }
            if (r3 == 0) goto L_0x0028
            boolean r3 = android.text.TextUtils.isEmpty(r17)     // Catch:{ Exception -> 0x00c8, all -> 0x00d6 }
            if (r3 != 0) goto L_0x00b9
            java.lang.String r3 = "channelId"
            r0 = r17
            r1 = r18
            java.lang.String[][] r3 = r14.a(r0, r1, r3)     // Catch:{ Exception -> 0x00c8, all -> 0x00d6 }
            r4 = 0
            r4 = r3[r4]     // Catch:{ Exception -> 0x00c8, all -> 0x00d6 }
            r5 = 0
            r4 = r4[r5]     // Catch:{ Exception -> 0x00c8, all -> 0x00d6 }
            r5 = 1
            r3 = r3[r5]     // Catch:{ Exception -> 0x00c8, all -> 0x00d6 }
            java.lang.String[][] r3 = r14.a(r4, r3)     // Catch:{ Exception -> 0x00c8, all -> 0x00d6 }
            r4 = 0
            r4 = r3[r4]     // Catch:{ Exception -> 0x00c8, all -> 0x00d6 }
            r5 = 0
            r5 = r4[r5]     // Catch:{ Exception -> 0x00c8, all -> 0x00d6 }
            r4 = 1
            r6 = r3[r4]     // Catch:{ Exception -> 0x00c8, all -> 0x00d6 }
            java.lang.String r3 = "downloadsinfo"
            r7 = 0
            r8 = 0
            r4 = r16
            r9 = r19
            android.database.Cursor r4 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x00c8, all -> 0x00d6 }
        L_0x0084:
            if (r4 == 0) goto L_0x00ee
            boolean r3 = r4.moveToFirst()     // Catch:{ Exception -> 0x00e3 }
            if (r3 == 0) goto L_0x00ee
            java.lang.String r3 = "downloadTicket"
            int r3 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r3 = r4.getString(r3)     // Catch:{ Exception -> 0x00e3 }
            boolean r5 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x00e3 }
            if (r5 != 0) goto L_0x00ee
            java.lang.String r5 = "select * from downloadsinfo as a left outer join tbl_download as b on a.downloadTicket = b.id where a.downloadTicket = ?"
            r6 = 1
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ Exception -> 0x00e3 }
            r7 = 0
            r6[r7] = r3     // Catch:{ Exception -> 0x00e3 }
            android.database.Cursor r2 = r2.rawQuery(r5, r6)     // Catch:{ Exception -> 0x00e3 }
            android.content.Context r3 = r14.getContext()     // Catch:{ Exception -> 0x00e7 }
            android.content.ContentResolver r3 = r3.getContentResolver()     // Catch:{ Exception -> 0x00e7 }
            r2.setNotificationUri(r3, r15)     // Catch:{ Exception -> 0x00e7 }
            if (r4 == 0) goto L_0x0047
            r4.close()
            goto L_0x0047
        L_0x00b9:
            java.lang.String r3 = "downloadsinfo"
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r4 = r16
            r9 = r19
            android.database.Cursor r4 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x00c8, all -> 0x00d6 }
            goto L_0x0084
        L_0x00c8:
            r2 = move-exception
            r3 = r2
            r4 = r10
            r2 = r12
        L_0x00cc:
            r3.printStackTrace()     // Catch:{ all -> 0x00dd }
            if (r4 == 0) goto L_0x0047
            r4.close()
            goto L_0x0047
        L_0x00d6:
            r2 = move-exception
        L_0x00d7:
            if (r10 == 0) goto L_0x00dc
            r10.close()
        L_0x00dc:
            throw r2
        L_0x00dd:
            r2 = move-exception
            r10 = r4
            goto L_0x00d7
        L_0x00e0:
            r2 = move-exception
            r10 = r11
            goto L_0x00d7
        L_0x00e3:
            r2 = move-exception
            r3 = r2
            r2 = r12
            goto L_0x00cc
        L_0x00e7:
            r3 = move-exception
            goto L_0x00cc
        L_0x00e9:
            r2 = move-exception
            r3 = r2
            r4 = r11
            r2 = r12
            goto L_0x00cc
        L_0x00ee:
            r11 = r4
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.contentprovider.AssistantProvider.query(android.net.Uri, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String):android.database.Cursor");
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        try {
            throw new NotSupportOperationException("insert is not supported");
        } catch (NotSupportOperationException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int delete(Uri uri, String str, String[] strArr) {
        try {
            throw new NotSupportOperationException("delete is not supported");
        } catch (NotSupportOperationException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        try {
            throw new NotSupportOperationException("update is not supported");
        } catch (NotSupportOperationException e) {
            e.printStackTrace();
            return 0;
        }
    }

    private String[][] a(String str, String[] strArr, String str2) {
        String str3;
        String[] strArr2;
        String[] strArr3 = new String[strArr.length];
        String copyValueOf = String.copyValueOf(str.toCharArray());
        for (int i = 0; i < strArr.length; i++) {
            strArr3[i] = strArr[i];
        }
        if (!b || !copyValueOf.contains(str2)) {
            str3 = copyValueOf;
            strArr2 = strArr3;
        } else {
            String[] split = copyValueOf.split("and");
            for (int i2 = 0; i2 < split.length; i2++) {
                if (split[i2].contains(str2)) {
                    split[i2] = null;
                    if (strArr3.length >= i2) {
                        strArr3[i2] = null;
                    }
                }
            }
            StringBuilder sb = new StringBuilder();
            for (int i3 = 0; i3 < split.length; i3++) {
                if (!TextUtils.isEmpty(split[i3])) {
                    if (i3 == 0) {
                        sb.append(split[i3]);
                    } else {
                        sb.append(" and " + split[i3]);
                    }
                }
            }
            String sb2 = sb.toString();
            ArrayList arrayList = new ArrayList();
            for (int i4 = 0; i4 < strArr3.length; i4++) {
                if (!TextUtils.isEmpty(strArr3[i4])) {
                    arrayList.add(strArr3[i4]);
                }
            }
            String[] strArr4 = new String[arrayList.size()];
            for (int i5 = 0; i5 < arrayList.size(); i5++) {
                strArr4[i5] = (String) arrayList.get(i5);
            }
            str3 = sb2;
            strArr2 = strArr4;
        }
        return new String[][]{new String[]{str3}, strArr2};
    }

    private String[][] a(String str, String[] strArr) {
        String str2;
        String[] strArr2 = new String[strArr.length];
        String copyValueOf = String.copyValueOf(str.toCharArray());
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = strArr[i];
        }
        if (copyValueOf.contains(CommentDetailTabView.PARAMS_VERSION_CODE)) {
            String[] split = copyValueOf.split("and");
            for (int i2 = 0; i2 < split.length; i2++) {
                if (split[i2].contains(CommentDetailTabView.PARAMS_VERSION_CODE)) {
                    split[i2] = split[i2].replace("=", ">=");
                }
            }
            StringBuilder sb = new StringBuilder();
            for (int i3 = 0; i3 < split.length; i3++) {
                if (!TextUtils.isEmpty(split[i3])) {
                    if (i3 == 0) {
                        sb.append(split[i3]);
                    } else {
                        sb.append(" and " + split[i3]);
                    }
                }
            }
            str2 = sb.toString();
        } else {
            str2 = copyValueOf;
        }
        return new String[][]{new String[]{str2}, strArr2};
    }
}
