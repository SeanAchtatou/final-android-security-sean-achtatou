package com.tencent.assistant.adapter;

import com.tencent.assistantv2.component.o;

/* compiled from: ProGuard */
class ah implements o {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ x f694a;
    final /* synthetic */ long b;
    final /* synthetic */ ac c;

    ah(ac acVar, x xVar, long j) {
        this.c = acVar;
        this.f694a = xVar;
        this.b = j;
    }

    public void a(boolean z) {
        Long l = (Long) this.f694a.j.getTag();
        if (l != null && this.b == l.longValue()) {
            if (!z) {
                this.f694a.m.setVisibility(8);
                this.f694a.i.setOnClickListener(null);
                return;
            }
            this.f694a.m.setVisibility(0);
        }
    }
}
