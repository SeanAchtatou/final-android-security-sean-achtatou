package com.tencent.assistant.adapter;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class cx extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f756a;
    final /* synthetic */ int b;
    final /* synthetic */ int c;
    final /* synthetic */ cw d;

    cx(cw cwVar, SimpleAppModel simpleAppModel, int i, int i2) {
        this.d = cwVar;
        this.f756a = simpleAppModel;
        this.b = i;
        this.c = i2;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.d.c, AppDetailActivityV5.class);
        if (this.d.c instanceof BaseActivity) {
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.d.c).f());
        }
        intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, this.f756a);
        intent.putExtra("statInfo", this.d.k);
        this.d.c.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        return STInfoBuilder.buildSTInfo(this.d.c, this.f756a, this.d.b(this.b, this.c), 200, null);
    }
}
