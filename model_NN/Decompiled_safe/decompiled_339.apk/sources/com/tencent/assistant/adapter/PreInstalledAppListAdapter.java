package com.tencent.assistant.adapter;

import android.content.Context;
import android.os.Handler;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LocalPkgSizeTextView;
import com.tencent.assistant.component.PreInstallAppListView;
import com.tencent.assistant.component.appdetail.HorizonImageListView;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.cv;
import com.tencent.assistant.utils.df;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.component.MovingProgressBar;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class PreInstalledAppListAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f675a;
    private List<LocalApkInfo> b = new ArrayList();
    private LayoutInflater c;
    private IViewInvalidater d;
    private boolean e = true;
    private int f = 0;
    private Handler g;
    private boolean h;
    /* access modifiers changed from: private */
    public boolean i = true;

    public PreInstalledAppListAdapter(Context context) {
        this.f675a = context;
        this.c = LayoutInflater.from(context);
    }

    public void a(List<LocalApkInfo> list, int i2) {
        this.b.clear();
        this.b.addAll(list);
        this.f = i2;
        notifyDataSetChanged();
    }

    public int getCount() {
        if (this.b != null) {
            return this.b.size();
        }
        return 0;
    }

    public Object getItem(int i2) {
        if (this.b != null) {
            return this.b.get(i2);
        }
        return null;
    }

    public long getItemId(int i2) {
        return 0;
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        cm cmVar;
        if (view == null || view.getTag() == null) {
            view = this.c.inflate((int) R.layout.installed_app_list_item, (ViewGroup) null);
            cmVar = new cm(null);
            cmVar.f745a = view.findViewById(R.id.container_layout);
            cmVar.c = (TextView) view.findViewById(R.id.soft_name_txt);
            cmVar.b = (TXImageView) view.findViewById(R.id.soft_icon_img);
            cmVar.d = (LocalPkgSizeTextView) view.findViewById(R.id.soft_size_txt);
            cmVar.e = (TextView) view.findViewById(R.id.tv_check);
            cmVar.f = (TextView) view.findViewById(R.id.last_used_time_txt);
            cmVar.g = (TextView) view.findViewById(R.id.popbar_title);
            cmVar.h = (TextView) view.findViewById(R.id.tv_risk_tips);
            cmVar.k = view.findViewById(R.id.rl_appdetail);
            cmVar.i = view.findViewById(R.id.uninstall_progress);
            cmVar.j = (MovingProgressBar) view.findViewById(R.id.app_uninstall_progress_bar);
            cmVar.j.a(AstApp.i().getResources().getDimensionPixelSize(R.dimen.app_detail_pic_gap));
            cmVar.j.b(AstApp.i().getResources().getDimensionPixelSize(R.dimen.install_progress_bar_width));
            cmVar.l = view.findViewById(R.id.divide_line);
            cmVar.m = view.findViewById(R.id.top_margin);
            cmVar.n = view.findViewById(R.id.bottom_margin);
            cmVar.o = view.findViewById(R.id.last_line);
            view.setTag(cmVar);
        } else {
            cmVar = (cm) view.getTag();
        }
        a(this.b.get(i2), cmVar, i2, a(this.f, i2));
        return view;
    }

    private void a(LocalApkInfo localApkInfo, cm cmVar, int i2, STInfoV2 sTInfoV2) {
        cmVar.c.setText(localApkInfo.mAppName);
        cmVar.b.setInvalidater(this.d);
        cmVar.b.updateImageView(localApkInfo.mPackageName, R.drawable.pic_defaule, TXImageView.TXImageViewType.INSTALL_APK_ICON);
        if (this.h || !a()) {
            cmVar.e.setEnabled(false);
            cmVar.f745a.setEnabled(false);
            if (localApkInfo.mIsSelect) {
                cmVar.i.setVisibility(0);
                cmVar.k.setVisibility(8);
                cmVar.j.a();
            } else {
                cmVar.i.setVisibility(8);
                cmVar.k.setVisibility(0);
                cmVar.j.b();
            }
        } else {
            cmVar.e.setEnabled(true);
            cmVar.f745a.setEnabled(true);
            cmVar.i.setVisibility(8);
            cmVar.k.setVisibility(0);
            cmVar.j.b();
        }
        cmVar.e.setSelected(localApkInfo.mIsSelect);
        if (this.f == 1) {
            cmVar.d.setTextColor(this.f675a.getResources().getColor(R.color.appadmin_highlight_text));
        } else {
            cmVar.d.setTextColor(this.f675a.getResources().getColor(R.color.appadmin_card_size_text));
        }
        cmVar.d.updateTextView(localApkInfo.mPackageName, localApkInfo.occupySize);
        cmVar.d.setInvalidater(this.d);
        if (i2 == 0) {
            cmVar.g.setText(this.f675a.getString(R.string.popbar_title_installed_count, Integer.valueOf(getCount())));
            cmVar.g.setVisibility(0);
        } else {
            cmVar.g.setVisibility(8);
        }
        if (this.f == 3) {
            a(cmVar.f, localApkInfo.mInstallDate);
        } else {
            a(cmVar.f, localApkInfo.mLastLaunchTime, localApkInfo.mFakeLastLaunchTime);
        }
        if (this.e) {
            ((RelativeLayout.LayoutParams) cmVar.e.getLayoutParams()).rightMargin = df.a(this.f675a, 16.0f);
            ((LinearLayout.LayoutParams) cmVar.o.getLayoutParams()).rightMargin = df.a(this.f675a, 16.0f);
        } else {
            ((RelativeLayout.LayoutParams) cmVar.e.getLayoutParams()).rightMargin = df.a(this.f675a, 46.0f);
            ((LinearLayout.LayoutParams) cmVar.o.getLayoutParams()).rightMargin = df.a(this.f675a, 30.0f);
        }
        cmVar.e.setOnClickListener(new cj(this, cmVar, localApkInfo, sTInfoV2));
        cmVar.f745a.setOnClickListener(new ck(this, cmVar, localApkInfo, sTInfoV2));
        a(cmVar.f745a, cmVar.h, cmVar.l, cmVar.o, cmVar.m, cmVar.n, i2, this.b.size());
    }

    /* access modifiers changed from: private */
    public void a(cm cmVar, LocalApkInfo localApkInfo) {
        cl clVar = new cl(this, cmVar, localApkInfo);
        clVar.blockCaller = true;
        clVar.titleRes = this.f675a.getString(R.string.app_admin_uninstall_dialog_tips_title);
        clVar.lBtnTxtRes = this.f675a.getString(R.string.cancel);
        clVar.rBtnTxtRes = this.f675a.getString(R.string.app_admin_uninstall_dialog_tips_confirm);
        clVar.contentRes = this.f675a.getString(R.string.app_admin_uninstall_dialog_tips_content);
        v.a(clVar);
    }

    /* access modifiers changed from: private */
    public void b(cm cmVar, LocalApkInfo localApkInfo) {
        cmVar.e.setSelected(!cmVar.e.isSelected());
        localApkInfo.mIsSelect = cmVar.e.isSelected();
        if (this.g != null) {
            this.g.sendMessage(this.g.obtainMessage(10705, localApkInfo));
        }
    }

    private STInfoV2 a(int i2, int i3) {
        String b2 = b(i2, i3);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f675a, 100);
        buildSTInfo.slotId = b2;
        if (a()) {
            buildSTInfo.scene = STConst.ST_PAGE_PRE_APP_UNINSTALL_ROOT;
            b.getInstance().exposure(buildSTInfo);
        } else {
            buildSTInfo.scene = STConst.ST_PAGE_PRE_APP_UNINSTALL_NO_ROOT;
        }
        return buildSTInfo;
    }

    private String b(int i2, int i3) {
        if (i2 == 0) {
            return "03_" + ct.a(i3 + 1);
        }
        if (i2 == 1) {
            return "04_" + ct.a(i3 + 1);
        }
        if (i2 == 2) {
            return HorizonImageListView.TMA_ST_HORIZON_IMAGE_TAG + ct.a(i3 + 1);
        }
        return "06_" + ct.a(i3 + 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    private void a(TextView textView, long j, long j2) {
        int f2;
        long a2 = m.a().a("key_first_load_installed_app_time", 0L);
        boolean z = a2 != 0 && (System.currentTimeMillis() - a2) - 259200000 >= 0;
        if (j > 0 || z) {
            if (j > 0) {
                f2 = cv.f(j);
            } else {
                f2 = cv.f(j2);
            }
            if (f2 == 0) {
                textView.setVisibility(0);
                textView.setText(this.f675a.getResources().getString(R.string.today));
                return;
            }
            String format = String.format(this.f675a.getResources().getString(R.string.last_used_time), Integer.valueOf(f2));
            if (this.f == 0) {
                SpannableString spannableString = new SpannableString(format);
                spannableString.setSpan(new ForegroundColorSpan(this.f675a.getResources().getColor(R.color.appadmin_highlight_text)), 0, format.indexOf("天") + 1, 33);
                textView.setText(spannableString);
            } else {
                textView.setText(format);
            }
            textView.setVisibility(0);
            return;
        }
        textView.setVisibility(8);
    }

    private void a(TextView textView, long j) {
        if (j <= 0) {
            textView.setVisibility(8);
            return;
        }
        int f2 = cv.f(j);
        if (f2 == 0) {
            textView.setVisibility(0);
            textView.setText(this.f675a.getResources().getString(R.string.today_install));
            return;
        }
        textView.setText(String.format(this.f675a.getResources().getString(R.string.install_time), Integer.valueOf(f2)));
        textView.setVisibility(0);
    }

    public void a(View view, TextView textView, View view2, View view3, View view4, View view5, int i2, int i3) {
        if (i3 == 1) {
            view3.setVisibility(8);
            view5.setVisibility(0);
            view4.setVisibility(0);
            if (a()) {
                textView.setVisibility(0);
                view2.setVisibility(0);
            } else {
                textView.setVisibility(8);
                view2.setVisibility(8);
            }
        } else if (i2 == 0) {
            view3.setVisibility(0);
            view5.setVisibility(8);
            if (a()) {
                view4.setVisibility(8);
                textView.setVisibility(0);
                view2.setVisibility(0);
            } else {
                view4.setVisibility(0);
                textView.setVisibility(8);
                view2.setVisibility(8);
            }
        } else if (i2 == i3 - 1) {
            view3.setVisibility(8);
            view5.setVisibility(0);
            view4.setVisibility(8);
            textView.setVisibility(8);
            view2.setVisibility(8);
        } else {
            view3.setVisibility(0);
            view5.setVisibility(8);
            view4.setVisibility(8);
            textView.setVisibility(8);
            view2.setVisibility(8);
        }
        view.setBackgroundResource(R.drawable.helper_cardbg_all_selector_new);
    }

    public int a(int i2) {
        for (int i3 = 0; i3 < getCount(); i3++) {
            String str = this.b.get(i3).mSortKey;
            if (!TextUtils.isEmpty(str)) {
                char charAt = str.toUpperCase().charAt(0);
                if (charAt == i2) {
                    return i3;
                }
                if (i2 == 35 && charAt >= '0' && charAt <= '9') {
                    return i3;
                }
            }
        }
        return -1;
    }

    private boolean a() {
        return PreInstallAppListView.hasRoot;
    }

    public void a(IViewInvalidater iViewInvalidater) {
        this.d = iViewInvalidater;
    }

    public void a(boolean z) {
        this.e = z;
    }

    public void a(Handler handler) {
        this.g = handler;
    }

    public void a(boolean z, boolean z2) {
        this.h = z;
        if (z2) {
            notifyDataSetChanged();
        }
    }
}
