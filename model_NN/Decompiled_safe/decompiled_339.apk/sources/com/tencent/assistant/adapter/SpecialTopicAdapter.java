package com.tencent.assistant.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class SpecialTopicAdapter extends BaseAdapter {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f680a;
    private List<AppGroupInfo> b = new ArrayList();
    private LayoutInflater c;
    private b d = new b();

    public SpecialTopicAdapter(Context context, Map<AppGroupInfo, ArrayList<SimpleAppModel>> map) {
        this.f680a = context;
        a(map, true);
        this.c = LayoutInflater.from(context);
    }

    public void a(Map<AppGroupInfo, ArrayList<SimpleAppModel>> map, boolean z) {
        if (map != null && map.size() != 0) {
            if (z) {
                this.b.clear();
            }
            this.b.addAll(new ArrayList(map.keySet()));
            notifyDataSetChanged();
        }
    }

    public int getCount() {
        return this.b.size();
    }

    public int a() {
        if (this.b != null) {
            return this.b.size();
        }
        return 0;
    }

    public Object getItem(int i) {
        return this.b.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate;
        ea eaVar = null;
        AppGroupInfo appGroupInfo = this.b.get(i);
        if (appGroupInfo == null) {
            return null;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f680a, 100);
        if (buildSTInfo != null) {
            buildSTInfo.slotId = a(i);
            buildSTInfo.updateContentId(STCommonInfo.ContentIdType.SPECIAL, String.valueOf(appGroupInfo.f1628a));
        }
        this.d.exposureSpecialTopic(buildSTInfo);
        if (view != null) {
            eaVar = (ea) view.getTag();
            inflate = view;
        } else {
            inflate = this.c.inflate((int) R.layout.item_special_topic_layout, (ViewGroup) null);
        }
        if (eaVar == null) {
            ea eaVar2 = new ea();
            eaVar2.f786a = (TXImageView) inflate.findViewById(R.id.topic_pic);
            eaVar2.b = (TextView) inflate.findViewById(R.id.topic_title);
            eaVar2.c = (ImageView) inflate.findViewById(R.id.flag_img);
            eaVar2.d = appGroupInfo;
            inflate.setTag(eaVar2);
            eaVar = eaVar2;
        }
        if (appGroupInfo.e == 1) {
            eaVar.c.setVisibility(0);
        } else {
            eaVar.c.setVisibility(4);
        }
        eaVar.f786a.updateImageView(appGroupInfo.d, -1, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
        eaVar.b.setText(appGroupInfo.b);
        inflate.setTag(R.id.group_info, appGroupInfo);
        inflate.setOnClickListener(new dz(this, buildSTInfo));
        return inflate;
    }

    private String a(int i) {
        return a.a("03", i);
    }
}
