package com.tencent.assistant.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;

/* compiled from: ProGuard */
class cf {

    /* renamed from: a  reason: collision with root package name */
    TXAppIconView f739a;
    TextView b;
    DownloadButton c;
    View d;
    TextView e;
    TextView f;
    ImageView g;
    ListItemInfoView h;
    final /* synthetic */ OneMoreAdapter i;

    private cf(OneMoreAdapter oneMoreAdapter) {
        this.i = oneMoreAdapter;
    }

    /* synthetic */ cf(OneMoreAdapter oneMoreAdapter, cd cdVar) {
        this(oneMoreAdapter);
    }
}
