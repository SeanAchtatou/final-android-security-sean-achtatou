package com.tencent.assistant.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import java.util.List;

/* compiled from: ProGuard */
public class OneMoreAdapter extends BaseAdapter {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f674a;
    private List<SimpleAppModel> b;
    private int c = 2000;
    private String d = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public View e;
    private b f = new b();

    public OneMoreAdapter(Context context, View view, List<SimpleAppModel> list) {
        this.f674a = context;
        this.e = view;
        this.b = list;
        if (context instanceof BaseActivity) {
            this.c = ((BaseActivity) context).f();
        }
    }

    public void a(String str) {
        this.d = str;
    }

    public int getCount() {
        if (this.b != null) {
            return this.b.size();
        }
        return 0;
    }

    public Object getItem(int i) {
        return this.b.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        cf cfVar;
        SimpleAppModel simpleAppModel = this.b.get(i);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f674a, simpleAppModel, a.a(Constants.VIA_REPORT_TYPE_WPA_STATE, i), 100, null);
        if (buildSTInfo != null) {
            buildSTInfo.contentId = this.d;
        }
        if (view == null) {
            view = LayoutInflater.from(this.f674a).inflate((int) R.layout.app_one_more_item, (ViewGroup) null);
            cf cfVar2 = new cf(this, null);
            cfVar2.f739a = (TXAppIconView) view.findViewById(R.id.app_icon_img);
            cfVar2.b = (TextView) view.findViewById(R.id.app_name_txt);
            cfVar2.c = (DownloadButton) view.findViewById(R.id.state_app_btn);
            cfVar2.h = (ListItemInfoView) view.findViewById(R.id.download_info);
            cfVar2.d = view.findViewById(R.id.app_updatesizeinfo);
            cfVar2.e = (TextView) view.findViewById(R.id.app_size_sumsize);
            cfVar2.f = (TextView) view.findViewById(R.id.app_score_truesize);
            cfVar2.g = (ImageView) view.findViewById(R.id.app_one_more_item_line);
            view.setTag(cfVar2);
            cfVar = cfVar2;
        } else {
            cfVar = (cf) view.getTag();
        }
        view.setOnClickListener(new cd(this, simpleAppModel, buildSTInfo));
        if (simpleAppModel != null) {
            cfVar.b.setText(simpleAppModel.d);
            cfVar.f739a.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        }
        cfVar.c.a(simpleAppModel);
        cfVar.h.a(ListItemInfoView.InfoType.ONEMORE_DESC);
        cfVar.h.a(simpleAppModel);
        if (i == getCount() - 1) {
            cfVar.g.setVisibility(8);
        } else {
            cfVar.g.setVisibility(0);
        }
        if (s.a(simpleAppModel)) {
            cfVar.c.setClickable(false);
        } else {
            cfVar.c.setClickable(true);
            cfVar.c.a(buildSTInfo, new ce(this, simpleAppModel));
        }
        if (this.f != null) {
            this.f.exposure(buildSTInfo);
        }
        return view;
    }
}
