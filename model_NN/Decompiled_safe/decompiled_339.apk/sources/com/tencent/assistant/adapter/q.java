package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.AppCategoryListAdapter;
import com.tencent.assistant.link.b;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class q implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppCategoryListAdapter f806a;

    q(AppCategoryListAdapter appCategoryListAdapter) {
        this.f806a = appCategoryListAdapter;
    }

    public void onClick(View view) {
        String str;
        s sVar = (s) view.getTag(R.id.category_data);
        if (sVar != null) {
            if (sVar.d == 0 || sVar.d == 1) {
                str = ((("tpmast://" + (this.f806a.f == AppCategoryListAdapter.CategoryType.CATEGORYTYPESOFTWARE ? "appcategorydetail" : "gamecategorydetail") + "?") + "&" + "category_detail_category_id" + "=" + sVar.f808a) + "&" + "category_detail_tag_id" + "=" + sVar.b) + "&" + "category_detail_category_name" + "=" + sVar.c;
                XLog.d("AppCategoryListAdapter", "tmast url:" + str);
            } else {
                str = sVar.e;
            }
            b.b(this.f806a.g, str);
        }
    }
}
