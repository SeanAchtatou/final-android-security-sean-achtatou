package com.tencent.assistant.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.RoundRectDrawable;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.AppExCfg;
import com.tencent.assistant.utils.df;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class AppDetailAppCfgAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<AppExCfg> f667a = new ArrayList<>();
    private LayoutInflater b = null;
    private Context c;

    public AppDetailAppCfgAdapter(Context context, View view) {
        this.b = LayoutInflater.from(context);
        this.c = context;
    }

    public void a(ArrayList<AppExCfg> arrayList) {
        if (arrayList != null) {
            this.f667a.clear();
            this.f667a.addAll(arrayList);
            notifyDataSetChanged();
        }
    }

    public int getCount() {
        if (this.f667a == null) {
            return 0;
        }
        return this.f667a.size();
    }

    public Object getItem(int i) {
        if (this.f667a == null || this.f667a.size() == 0) {
            return null;
        }
        return this.f667a.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        v vVar;
        AppExCfg appExCfg;
        if (view == null || view.getTag() == null) {
            vVar = new v(this);
            view = this.b.inflate((int) R.layout.appdetail_cfg_item, (ViewGroup) null);
            vVar.f810a = (TXImageView) view.findViewById(R.id.cfg_icon_img);
            vVar.b = (TextView) view.findViewById(R.id.cfg_desc);
            view.setTag(vVar);
        } else {
            vVar = (v) view.getTag();
        }
        if (this.f667a == null || i < 0 || i >= this.f667a.size()) {
            appExCfg = null;
        } else {
            appExCfg = this.f667a.get(i);
        }
        a(vVar, appExCfg);
        return view;
    }

    private void a(v vVar, AppExCfg appExCfg) {
        if (vVar != null && appExCfg != null) {
            switch (appExCfg.f1994a) {
                case 1:
                    vVar.f810a.updateImageView(appExCfg.b, R.drawable.icon_libao, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                    a(vVar.f810a, appExCfg.e, R.color.app_detail_cfg_libao_icon_background);
                    break;
                case 2:
                    vVar.f810a.updateImageView(appExCfg.b, R.drawable.icon_gonglue, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                    a(vVar.f810a, appExCfg.e, R.color.app_detail_cfg_gonglue_icon_background);
                    break;
                case 3:
                    vVar.f810a.updateImageView(appExCfg.b, R.drawable.icon_hua, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                    a(vVar.f810a, appExCfg.e, R.color.app_detail_cfg_hua_icon_background);
                    break;
                case 4:
                    vVar.f810a.updateImageView(appExCfg.b, R.drawable.icon_peizhi, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                    a(vVar.f810a, appExCfg.e, R.color.app_detail_cfg_peizhi_icon_background);
                    break;
                default:
                    vVar.f810a.updateImageView(appExCfg.b, R.drawable.icon_libao, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                    a(vVar.f810a, appExCfg.e, R.color.app_detail_cfg_libao_icon_background);
                    break;
            }
            vVar.b.setText(appExCfg.c);
        }
    }

    private void a(ImageView imageView, String str, int i) {
        RoundRectDrawable roundRectDrawable = new RoundRectDrawable(this.c);
        roundRectDrawable.setRadius((float) df.a(this.c, 44.0f));
        if (TextUtils.isEmpty(str)) {
            roundRectDrawable.setColor(i);
        } else {
            try {
                roundRectDrawable.setColor(str);
            } catch (IllegalArgumentException e) {
                roundRectDrawable.setColor(i);
            }
        }
        imageView.setBackgroundDrawable(roundRectDrawable);
    }
}
