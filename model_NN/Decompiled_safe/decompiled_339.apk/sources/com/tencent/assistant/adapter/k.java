package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class k extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f801a;
    final /* synthetic */ n b;
    final /* synthetic */ d c;

    k(d dVar, int i, n nVar) {
        this.c = dVar;
        this.f801a = i;
        this.b = nVar;
    }

    public void onTMAClick(View view) {
        this.c.a(this.f801a, !this.b.b.isSelected());
        this.c.notifyDataSetChanged();
        if (this.c.f759a != null) {
            this.c.f759a.sendMessage(this.c.f759a.obtainMessage(110005, new LocalApkInfo()));
        }
    }

    public STInfoV2 getStInfo() {
        return STInfoBuilder.buildSTInfo(this.c.i, 200);
    }
}
