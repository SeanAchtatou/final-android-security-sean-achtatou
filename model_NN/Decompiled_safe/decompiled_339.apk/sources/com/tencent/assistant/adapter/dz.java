package com.tencent.assistant.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.SpecailTopicDetailActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class dz extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f784a;
    final /* synthetic */ SpecialTopicAdapter b;

    dz(SpecialTopicAdapter specialTopicAdapter, STInfoV2 sTInfoV2) {
        this.b = specialTopicAdapter;
        this.f784a = sTInfoV2;
    }

    public void onTMAClick(View view) {
        AppGroupInfo appGroupInfo = (AppGroupInfo) view.getTag(R.id.group_info);
        if (appGroupInfo.g == null || TextUtils.isEmpty(appGroupInfo.g.f1970a)) {
            Intent intent = new Intent(this.b.f680a, SpecailTopicDetailActivity.class);
            intent.putExtra("com.tencent.assistant.APP_GROUP_INFO", appGroupInfo);
            this.b.f680a.startActivity(intent);
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putSerializable("com.tencent.assistant.ACTION_URL", appGroupInfo.g);
        b.b(this.b.f680a, appGroupInfo.g.f1970a, bundle);
    }

    public STInfoV2 getStInfo() {
        if (this.f784a != null) {
            this.f784a.actionId = 200;
        }
        return this.f784a;
    }
}
