package com.tencent.assistant.adapter;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class cl extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ cm f744a;
    final /* synthetic */ LocalApkInfo b;
    final /* synthetic */ PreInstalledAppListAdapter c;

    cl(PreInstalledAppListAdapter preInstalledAppListAdapter, cm cmVar, LocalApkInfo localApkInfo) {
        this.c = preInstalledAppListAdapter;
        this.f744a = cmVar;
        this.b = localApkInfo;
    }

    public void onRightBtnClick() {
        this.c.b(this.f744a, this.b);
    }

    public void onLeftBtnClick() {
    }

    public void onCancell() {
    }
}
