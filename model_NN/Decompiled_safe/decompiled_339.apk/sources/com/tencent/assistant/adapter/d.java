package com.tencent.assistant.adapter;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.CommonViewInvalidater;
import com.tencent.assistant.component.listview.ManagerGeneralController;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.component.MovingProgressBar;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class d extends BaseExpandableListAdapter implements ManagerGeneralController.ManagerAdapterInterface {

    /* renamed from: a  reason: collision with root package name */
    public Handler f759a = null;
    public int b = 0;
    public long c = 0;
    public int d = 0;
    public long e = 0;
    private LinkedHashMap<Integer, ArrayList<LocalApkInfo>> f = new LinkedHashMap<>();
    private ArrayList<Integer> g = new ArrayList<>();
    private Map<String, Drawable> h = new HashMap();
    /* access modifiers changed from: private */
    public Context i;
    private LayoutInflater j;
    private CommonViewInvalidater k = new e(this);
    /* access modifiers changed from: private */
    public boolean l = true;
    private int m = 0;

    public d() {
    }

    public d(Context context, View view) {
        this.i = context;
        this.j = LayoutInflater.from(this.i);
    }

    private void e() {
        int i2 = 0;
        ArrayList arrayList = this.f.get(4);
        this.b = arrayList != null ? arrayList.size() : 0;
        this.c = a(arrayList);
        ArrayList arrayList2 = this.f.get(3);
        if (arrayList2 != null) {
            i2 = arrayList2.size();
        }
        this.d = i2;
        this.e = a(arrayList2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.adapter.d.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistant.adapter.d.a(int, int):com.tencent.assistant.localres.model.LocalApkInfo
      com.tencent.assistant.adapter.d.a(java.util.Map<java.lang.Integer, java.util.ArrayList<com.tencent.assistant.localres.model.LocalApkInfo>>, boolean):void
      com.tencent.assistant.adapter.d.a(int, boolean):void */
    public void a(Map<Integer, ArrayList<LocalApkInfo>> map, boolean z) {
        if (map != null && !map.isEmpty()) {
            this.f.clear();
            this.f.putAll(map);
            this.g.clear();
            for (Integer add : this.f.keySet()) {
                this.g.add(add);
            }
            e();
            if (z && this.g != null) {
                int size = this.g.size();
                for (int i2 = 0; i2 < size; i2++) {
                    if (this.g.get(i2).intValue() == 3) {
                        a(i2, true);
                        if (this.f759a != null) {
                            this.f759a.sendMessage(this.f759a.obtainMessage(110005, new LocalApkInfo()));
                        }
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    /* renamed from: a */
    public LocalApkInfo getChild(int i2, int i3) {
        ArrayList arrayList;
        if (this.f == null || this.g == null || i2 < 0 || this.g.size() <= i2 || (arrayList = this.f.get(this.g.get(i2))) == null || i3 < 0 || arrayList.size() <= i3) {
            return null;
        }
        return (LocalApkInfo) arrayList.get(i3);
    }

    public long getChildId(int i2, int i3) {
        return (long) i3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.adapter.d.a(com.tencent.assistant.adapter.l, com.tencent.assistant.localres.model.LocalApkInfo, com.tencent.assistantv2.st.page.STInfoV2):void
     arg types: [com.tencent.assistant.adapter.m, com.tencent.assistant.localres.model.LocalApkInfo, com.tencent.assistantv2.st.page.STInfoV2]
     candidates:
      com.tencent.assistant.adapter.d.a(com.tencent.assistant.adapter.m, com.tencent.assistant.localres.model.LocalApkInfo, com.tencent.assistantv2.st.page.STInfoV2):void
      com.tencent.assistant.adapter.d.a(com.tencent.assistant.adapter.l, com.tencent.assistant.localres.model.LocalApkInfo, com.tencent.assistantv2.st.page.STInfoV2):void */
    public View getChildView(int i2, int i3, boolean z, View view, ViewGroup viewGroup) {
        m mVar;
        LocalApkInfo a2 = getChild(i2, i3);
        int intValue = this.g.get(i2).intValue();
        XLog.d("ApkResultAdapter", "getChildView--apkInfo = " + a2);
        if (a2 == null) {
            return new View(this.i);
        }
        if (intValue == 4) {
            if (view == null || view.getTag() == null || !view.getTag().getClass().getSimpleName().equals(m.class.getSimpleName())) {
                view = this.j.inflate((int) R.layout.apkmgr_item_for_keep, (ViewGroup) null);
                m mVar2 = new m(null);
                view.setTag(mVar2);
                mVar2.f802a = (RelativeLayout) view.findViewById(R.id.content_layout);
                mVar2.c = (TXImageView) view.findViewById(R.id.app_icon_img);
                mVar2.e = (TextView) view.findViewById(R.id.app_version);
                mVar2.f = (TextView) view.findViewById(R.id.app_size);
                mVar2.b = (TextView) view.findViewById(R.id.check);
                mVar2.g = (ImageView) view.findViewById(R.id.last_line);
                mVar2.h = view.findViewById(R.id.bottom_margin);
                mVar2.d = (TextView) view.findViewById(R.id.app_name_txt);
                mVar2.j = (TextView) view.findViewById(R.id.tip_text);
                mVar2.i = view.findViewById(R.id.install_button);
                mVar2.k = (RelativeLayout) view.findViewById(R.id.app_info);
                mVar2.l = (MovingProgressBar) view.findViewById(R.id.app_install_progress_bar);
                mVar2.l.a(AstApp.i().getResources().getDimensionPixelSize(R.dimen.app_detail_pic_gap));
                mVar2.l.b(AstApp.i().getResources().getDimensionPixelSize(R.dimen.install_progress_bar_width));
                mVar2.m = (ImageView) view.findViewById(R.id.install_btn_img);
                mVar2.n = (TextView) view.findViewById(R.id.install_btn_txt);
                mVar2.o = (LinearLayout) view.findViewById(R.id.install_process);
                mVar = mVar2;
            } else {
                XLog.d("ApkResultAdapter", "convertView.getTag() = " + view.getTag());
                try {
                    mVar = (m) view.getTag();
                } catch (Exception e2) {
                    XLog.e("ApkResultAdapter", "CONVERTvIEW TYEP ERROR");
                    return view;
                }
            }
            if (a2.mApkState == 3) {
                mVar.k.setVisibility(8);
                mVar.m.setVisibility(8);
                mVar.n.setVisibility(0);
                mVar.o.setVisibility(0);
                mVar.l.a();
                mVar.i.setEnabled(false);
            } else {
                mVar.k.setVisibility(0);
                mVar.m.setVisibility(0);
                mVar.n.setVisibility(8);
                mVar.o.setVisibility(8);
                mVar.l.b();
                mVar.i.setEnabled(true);
            }
            mVar.c.setTag(a2.mLocalFilePath);
            STInfoV2 c2 = c(i2, i3);
            a((l) mVar, a2, c2);
            a(mVar, a2, c2);
            a(mVar.f802a, mVar.g, mVar.h, i2, i3, getChildrenCount(i2));
            return view;
        }
        if (view == null || view.getTag() == null || !view.getTag().getClass().getSimpleName().equals(l.class.getSimpleName())) {
            try {
                view = this.j.inflate((int) R.layout.apkmgr_item_for_delete, (ViewGroup) null);
                l lVar = new l(null);
                view.setTag(lVar);
                lVar.f802a = (RelativeLayout) view.findViewById(R.id.content_layout);
                lVar.c = (TXImageView) view.findViewById(R.id.app_icon_img);
                lVar.e = (TextView) view.findViewById(R.id.app_version);
                lVar.f = (TextView) view.findViewById(R.id.app_size);
                lVar.b = (TextView) view.findViewById(R.id.check);
                lVar.g = (ImageView) view.findViewById(R.id.last_line);
                lVar.h = view.findViewById(R.id.bottom_margin);
                lVar.d = (TextView) view.findViewById(R.id.app_name_txt);
            } catch (Exception e3) {
                return new View(this.i);
            }
        }
        try {
            l lVar2 = (l) view.getTag();
            lVar2.c.setTag(a2.mLocalFilePath);
            a(lVar2, a2, c(i2, i3));
            a(lVar2.f802a, lVar2.g, lVar2.h, i2, i3, getChildrenCount(i2));
            return view;
        } catch (Exception e4) {
            XLog.e("ApkResultAdapter", "CONVERTvIEW TYEP ERROR");
            return view;
        }
    }

    public void a() {
        XLog.d("ApkResultAdapter", "animationStart");
        synchronized (this) {
            this.l = false;
        }
    }

    public void b() {
        XLog.d("ApkResultAdapter", "animationEnd");
        synchronized (this) {
            this.l = true;
        }
        this.k.handleQueueMsg();
    }

    private void a(m mVar, LocalApkInfo localApkInfo, STInfoV2 sTInfoV2) {
        String string;
        if (localApkInfo != null) {
            XLog.d("ApkResultAdapter", "fillExtraValue--apkInfo.mIsUpdateApk = " + localApkInfo.mIsUpdateApk);
            if (localApkInfo.mIsUpdateApk) {
                string = this.i.getString(R.string.apkmgr_tip_update);
            } else {
                string = this.i.getString(R.string.apkmgr_tip_not_install);
            }
            mVar.j.setText(string);
            mVar.i.setOnClickListener(new f(this, localApkInfo, sTInfoV2));
        }
    }

    private void a(l lVar, LocalApkInfo localApkInfo, STInfoV2 sTInfoV2) {
        if (lVar != null && localApkInfo != null) {
            lVar.b.setSelected(localApkInfo.mIsSelect);
            lVar.b.setOnClickListener(new g(this, lVar, localApkInfo, sTInfoV2));
            lVar.f802a.setOnClickListener(new h(this, lVar, localApkInfo, sTInfoV2));
            lVar.c.setInvalidater(this.k);
            if (localApkInfo.mLocalFilePath == null || this.h == null || this.h.size() <= 0 || this.h.get(localApkInfo.mLocalFilePath) == null) {
                lVar.c.setImageDrawable(this.i.getResources().getDrawable(R.drawable.pic_defaule));
                TemporaryThreadManager.get().start(new i(this, localApkInfo, lVar));
            } else {
                lVar.c.setImageDrawable(this.h.get(localApkInfo.mLocalFilePath));
            }
            lVar.d.setText(localApkInfo.mAppName);
            if (TextUtils.isEmpty(localApkInfo.mVersionName)) {
                lVar.e.setText(Constants.STR_EMPTY);
            } else {
                lVar.e.setText("版本：" + localApkInfo.mVersionName);
            }
            lVar.f.setText(bt.a(localApkInfo.occupySize));
        }
    }

    private STInfoV2 c(int i2, int i3) {
        String d2 = d(i2, i3);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.i, 100);
        buildSTInfo.slotId = d2;
        return buildSTInfo;
    }

    private String d(int i2, int i3) {
        String str;
        if (this.g.get(i2).intValue() == 4) {
            str = "03";
        } else {
            str = "04";
        }
        return str + "_" + ct.a(i3 + 1);
    }

    public int getChildrenCount(int i2) {
        int i3;
        ArrayList arrayList;
        XLog.d("ApkResultAdapter", "getChildrenCount--");
        if (this.f == null || i2 >= this.g.size() || (arrayList = this.f.get(this.g.get(i2))) == null) {
            i3 = 0;
        } else {
            i3 = arrayList.size();
        }
        XLog.d("ApkResultAdapter", "getChildrenCount--COUNT = " + i3);
        return i3;
    }

    public Object getGroup(int i2) {
        if (this.g == null || this.g.size() <= 0 || i2 >= this.g.size() || i2 < 0) {
            return null;
        }
        return b(i2);
    }

    public int getGroupCount() {
        int i2 = 0;
        if (this.f != null) {
            i2 = this.f.size();
        }
        XLog.d("ApkResultAdapter", "getGroupCount count = " + i2);
        return i2;
    }

    public long getGroupId(int i2) {
        return (long) i2;
    }

    public View getGroupView(int i2, boolean z, View view, ViewGroup viewGroup) {
        n nVar;
        if (view == null) {
            view = this.j.inflate((int) R.layout.group_item, (ViewGroup) null);
            n nVar2 = new n(this);
            nVar2.f803a = (TextView) view.findViewById(R.id.group_title);
            nVar2.b = (TextView) view.findViewById(R.id.select_all);
            nVar2.b.setVisibility(0);
            view.setTag(nVar2);
            nVar = nVar2;
        } else {
            nVar = (n) view.getTag();
        }
        nVar.f803a.setText(b(i2));
        nVar.b.setSelected(a(i2));
        nVar.b.setOnClickListener(new k(this, i2, nVar));
        return view;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int i2, int i3) {
        return true;
    }

    public void a(int i2, boolean z) {
        try {
            ArrayList arrayList = this.f.get(Integer.valueOf(this.g.get(i2).intValue()));
            if (arrayList != null) {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    ((LocalApkInfo) it.next()).mIsSelect = z;
                }
            }
        } catch (Exception e2) {
        }
    }

    public boolean a(int i2) {
        try {
            ArrayList arrayList = this.f.get(Integer.valueOf(this.g.get(i2).intValue()));
            if (arrayList == null) {
                return false;
            }
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                if (!((LocalApkInfo) it.next()).mIsSelect) {
                    return false;
                }
            }
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    public ArrayList<Pair<Integer, Integer>> c() {
        int size = this.g.size();
        ArrayList<Pair<Integer, Integer>> arrayList = new ArrayList<>();
        for (int i2 = 0; i2 < size; i2++) {
            ArrayList arrayList2 = this.f.get(Integer.valueOf(this.g.get(i2).intValue()));
            if (arrayList2 != null) {
                Iterator it = arrayList2.iterator();
                int i3 = 0;
                while (it.hasNext()) {
                    if (((LocalApkInfo) it.next()).mIsSelect) {
                        arrayList.add(new Pair(Integer.valueOf(i2), Integer.valueOf(i3)));
                    }
                    i3++;
                }
            }
        }
        return arrayList;
    }

    private void b(LocalApkInfo localApkInfo) {
        int size = this.g.size();
        new ArrayList();
        for (int i2 = 0; i2 < size; i2++) {
            int intValue = this.g.get(i2).intValue();
            ArrayList arrayList = this.f.get(Integer.valueOf(intValue));
            if (arrayList != null) {
                Iterator it = arrayList.iterator();
                int i3 = 0;
                while (it.hasNext()) {
                    if (localApkInfo.equals((LocalApkInfo) it.next())) {
                        arrayList.remove(i3);
                        if (arrayList.isEmpty()) {
                            this.f.remove(Integer.valueOf(intValue));
                            this.g.remove(i2);
                            return;
                        }
                        return;
                    }
                    i3++;
                }
                continue;
            }
        }
    }

    private String b(int i2) {
        String str;
        if (this.g == null || i2 >= this.g.size()) {
            return Constants.STR_EMPTY;
        }
        if (this.g.get(i2).intValue() == 4) {
            if (this.f.get(4) != null) {
                str = String.format(this.i.getString(R.string.apkmgr_group_suggest_keep_txt), Integer.valueOf(this.b), bt.a(this.c));
            } else {
                str = Constants.STR_EMPTY;
            }
            return str;
        } else if (this.g.get(i2).intValue() != 3 || this.f.get(3) == null) {
            return Constants.STR_EMPTY;
        } else {
            return String.format(this.i.getString(R.string.apkmgr_group_suggest_delete_txt), Integer.valueOf(this.d), bt.a(this.e));
        }
    }

    public long a(List<LocalApkInfo> list) {
        long j2 = 0;
        if (list == null) {
            return 0;
        }
        Iterator<LocalApkInfo> it = list.iterator();
        while (true) {
            long j3 = j2;
            if (!it.hasNext()) {
                return j3;
            }
            LocalApkInfo next = it.next();
            if (next != null) {
                j2 = j3 + next.occupySize;
            } else {
                j2 = j3;
            }
        }
    }

    public String b(int i2, int i3) {
        String str;
        if (i2 == 0) {
            str = "03_";
        } else {
            str = "06_";
        }
        return str + ct.a(i3 + 1);
    }

    public Drawable a(LocalApkInfo localApkInfo) {
        Drawable drawable = null;
        if (localApkInfo != null) {
            String str = localApkInfo.mLocalFilePath;
            if (!TextUtils.isEmpty(str) && (drawable = this.h.get(str)) == null) {
                PackageManager packageManager = AstApp.i().getBaseContext().getPackageManager();
                try {
                    PackageInfo packageArchiveInfo = packageManager.getPackageArchiveInfo(localApkInfo.mLocalFilePath, 0);
                    if (packageArchiveInfo != null) {
                        ApplicationInfo applicationInfo = packageArchiveInfo.applicationInfo;
                        applicationInfo.sourceDir = localApkInfo.mLocalFilePath;
                        applicationInfo.publicSourceDir = localApkInfo.mLocalFilePath;
                        drawable = packageManager.getApplicationIcon(applicationInfo);
                        if (drawable != null) {
                            this.h.put(str, drawable);
                        }
                    }
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        }
        return drawable;
    }

    public void a(RelativeLayout relativeLayout, ImageView imageView, View view, int i2, int i3, int i4) {
        if (i4 == 1) {
            imageView.setVisibility(8);
            view.setVisibility(0);
        } else if (i3 == 0) {
            imageView.setVisibility(0);
            view.setVisibility(8);
        } else if (i3 == i4 - 1) {
            imageView.setVisibility(8);
            if (i2 == getGroupCount() - 1) {
                view.setVisibility(0);
            } else {
                view.setVisibility(8);
            }
        } else {
            imageView.setVisibility(0);
            view.setVisibility(8);
        }
    }

    public void d() {
        int i2;
        int i3;
        int size = this.f.size();
        int i4 = 0;
        while (i4 < size) {
            int intValue = this.g.get(i4).intValue();
            ArrayList arrayList = this.f.get(Integer.valueOf(intValue));
            if (arrayList != null) {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    if (((LocalApkInfo) it.next()).mIsSelect) {
                        it.remove();
                    }
                }
                if (arrayList.isEmpty()) {
                    this.f.remove(Integer.valueOf(intValue));
                    this.g.remove(i4);
                    i2 = i4 - 1;
                    i3 = size - 1;
                    i4 = i2 + 1;
                    size = i3;
                }
            }
            i2 = i4;
            i3 = size;
            i4 = i2 + 1;
            size = i3;
        }
        e();
        notifyDataSetChanged();
    }

    public Object getSelectItem() {
        ManagerGeneralController.PositionInfo selectPosition = getSelectPosition();
        if (selectPosition == null) {
            return null;
        }
        return getChild(selectPosition.groupPosition, selectPosition.childPosition);
    }

    public ManagerGeneralController.PositionInfo getSelectPosition() {
        ArrayList<Pair<Integer, Integer>> c2 = c();
        if (c2 == null || c2.isEmpty()) {
            return null;
        }
        Pair pair = c2.get(0);
        return new ManagerGeneralController.PositionInfo(((Integer) pair.first).intValue(), ((Integer) pair.second).intValue());
    }

    public void resetIndex() {
        this.m = 0;
    }

    public void addNextIndex() {
        this.m++;
    }

    public ManagerGeneralController.PositionInfo getNextPosition() {
        ArrayList<Pair<Integer, Integer>> c2 = c();
        if (c2 == null || c2.isEmpty() || this.m >= c2.size()) {
            return null;
        }
        Pair pair = c2.get(this.m);
        return new ManagerGeneralController.PositionInfo(((Integer) pair.first).intValue(), ((Integer) pair.second).intValue());
    }

    public Object getNextItem() {
        ManagerGeneralController.PositionInfo nextPosition = getNextPosition();
        if (nextPosition == null) {
            return null;
        }
        return getChild(nextPosition.groupPosition, nextPosition.childPosition);
    }

    public void notifyDataChange(Object obj) {
        if (obj != null) {
            b((LocalApkInfo) obj);
            e();
            notifyDataSetChanged();
        }
    }
}
