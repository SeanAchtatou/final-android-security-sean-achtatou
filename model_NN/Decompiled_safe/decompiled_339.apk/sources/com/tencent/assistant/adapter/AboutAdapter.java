package com.tencent.assistant.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistant.module.callback.ad;
import com.tencent.assistant.module.callback.o;
import com.tencent.assistant.module.cz;
import com.tencent.assistant.module.fa;
import com.tencent.assistant.protocol.jce.Feedback;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.model.ItemElement;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class AboutAdapter extends BaseAdapter implements ad, o {

    /* renamed from: a  reason: collision with root package name */
    private Context f662a;
    private LayoutInflater b;
    private List<ItemElement> c = new ArrayList();

    public AboutAdapter(Context context) {
        this.f662a = context;
        this.b = LayoutInflater.from(context);
        fa.a().register(this);
        cz.a().register(this);
    }

    public void a(List<ItemElement> list) {
        if (list != null && list.size() > 0) {
            this.c.addAll(list);
        }
    }

    public int getCount() {
        if (this.c == null || this.c.size() <= 0) {
            return 0;
        }
        return this.c.size();
    }

    public Object getItem(int i) {
        if (i < 0 || i > this.c.size() - 1) {
            return null;
        }
        return this.c.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.SelfUpdateManager.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistant.manager.SelfUpdateManager.a(com.tencent.assistant.manager.SelfUpdateManager, com.tencent.assistant.localres.model.LocalApkInfo):com.tencent.assistant.localres.model.LocalApkInfo
      com.tencent.assistant.manager.SelfUpdateManager.a(com.tencent.assistant.localres.model.LocalApkInfo, com.tencent.assistant.manager.SelfUpdateManager$SelfUpdateInfo):com.tencent.assistant.model.SimpleAppModel
      com.tencent.assistant.manager.SelfUpdateManager.a(boolean, boolean):void */
    public View getView(int i, View view, ViewGroup viewGroup) {
        b bVar;
        if (view == null) {
            view = this.b.inflate((int) R.layout.about_item_layout, (ViewGroup) null);
            bVar = new b(this);
            bVar.f713a = view.findViewById(R.id.item_layout);
            bVar.b = view.findViewById(R.id.top_margin);
            bVar.c = (TextView) view.findViewById(R.id.item_title);
            bVar.d = (TextView) view.findViewById(R.id.item_description);
            bVar.e = (TextView) view.findViewById(R.id.item_image);
            bVar.f = view.findViewById(R.id.item_line);
            bVar.g = view.findViewById(R.id.bottom_margin);
            view.setTag(bVar);
        } else {
            bVar = (b) view.getTag();
        }
        ItemElement itemElement = (ItemElement) getItem(i);
        if (itemElement == null) {
            return null;
        }
        bVar.c.setText(itemElement.f3309a);
        switch (itemElement.c) {
            case 1:
                bVar.d.setVisibility(0);
                bVar.d.setTextColor(this.f662a.getResources().getColor(R.color.about_description_txt_color));
                if (m.a().a("update_newest_versioncode", 0) > t.o()) {
                    if (!m.a().a("update_newest_versionname", (String) null).equals("null")) {
                        bVar.e.setVisibility(0);
                        bVar.d.setText(m.a().a("update_newest_versionname", (String) null));
                        bVar.d.setTextColor(this.f662a.getResources().getColor(R.color.about_last_version_color));
                        break;
                    } else {
                        bVar.e.setVisibility(8);
                        bVar.d.setText(this.f662a.getString(R.string.about_version_state_checking));
                        SelfUpdateManager.a().a(false, true);
                        break;
                    }
                } else {
                    bVar.e.setVisibility(8);
                    bVar.d.setText(this.f662a.getString(R.string.about_version_state_last));
                    break;
                }
            case 2:
                int b2 = cz.a().b();
                if (b2 <= 0) {
                    bVar.e.setVisibility(8);
                    bVar.d.setVisibility(8);
                    break;
                } else {
                    bVar.e.setVisibility(0);
                    bVar.d.setVisibility(0);
                    bVar.d.setText(String.valueOf(b2));
                    break;
                }
            case 4:
                bVar.e.setVisibility(8);
                bVar.d.setVisibility(8);
                break;
        }
        bVar.f713a.setBackgroundResource(R.drawable.common_download_item_selector);
        if (i == getCount() - 1) {
            bVar.f.setVisibility(8);
            bVar.b.setVisibility(8);
            bVar.g.setVisibility(0);
            return view;
        } else if (i == 0) {
            bVar.f.setVisibility(0);
            bVar.b.setVisibility(0);
            bVar.g.setVisibility(8);
            return view;
        } else {
            bVar.f.setVisibility(0);
            bVar.b.setVisibility(8);
            bVar.g.setVisibility(8);
            return view;
        }
    }

    public void a() {
        notifyDataSetChanged();
    }

    public void b() {
    }

    public void c() {
        fa.a().unregister(this);
        cz.a().unregister(this);
    }

    public void onCheckSelfUpdateFinish(int i, int i2, SelfUpdateManager.SelfUpdateInfo selfUpdateInfo) {
        m.a().b("update_newest_versionname", selfUpdateInfo.f);
        notifyDataSetChanged();
    }

    public void a(int i) {
        notifyDataSetChanged();
    }

    public void a(int i, boolean z, boolean z2, ArrayList<Feedback> arrayList) {
    }
}
