package com.tencent.assistant.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class TXTabViewPageAdapter extends PagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<ed> f682a = new ArrayList<>();

    public void addPageItem(String str, View view) {
        if (str != null && view != null) {
            this.f682a.add(new ed(this, str, view));
        }
    }

    public ArrayList<String> getTitleList() {
        if (this.f682a == null || this.f682a.size() <= 0) {
            return null;
        }
        ArrayList<String> arrayList = new ArrayList<>();
        Iterator<ed> it = this.f682a.iterator();
        while (it.hasNext()) {
            arrayList.add(it.next().f788a);
        }
        return arrayList;
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        ed edVar = this.f682a.get(i);
        if (edVar != null) {
            viewGroup.removeView(edVar.b);
        }
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        ed edVar = this.f682a.get(i);
        if (edVar == null) {
            return null;
        }
        viewGroup.addView(edVar.b);
        return edVar.b;
    }

    public int getCount() {
        if (this.f682a != null) {
            return this.f682a.size();
        }
        return 0;
    }

    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }
}
