package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.assistant.activity.SettingActivity;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistantv2.model.ItemElement;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class dp extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f775a;
    final /* synthetic */ int b;
    final /* synthetic */ Cdo c;

    dp(Cdo doVar, int i, int i2) {
        this.c = doVar;
        this.f775a = i;
        this.b = i2;
    }

    public void onTMAClick(View view) {
        ItemElement a2 = this.c.a(this.f775a, this.b);
        if (a2 != null) {
            ((SettingActivity) this.c.d).a(a2, view, this.b);
        }
    }

    public STInfoV2 getStInfo(View view) {
        if (!(view.getTag() instanceof dv)) {
            return null;
        }
        return this.c.a(this.c.d(this.f775a, this.b), this.c.b(((dv) view.getTag()).g.getSwitchState()), 200);
    }
}
