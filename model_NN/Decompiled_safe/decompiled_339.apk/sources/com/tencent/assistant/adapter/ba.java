package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;

/* compiled from: ProGuard */
class ba extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfoMultiAdapter f714a;

    ba(DownloadInfoMultiAdapter downloadInfoMultiAdapter) {
        this.f714a = downloadInfoMultiAdapter;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.DownloadInfoMultiAdapter, boolean):boolean
     arg types: [com.tencent.assistant.adapter.DownloadInfoMultiAdapter, int]
     candidates:
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(int, int):java.lang.String
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistantv2.mediadownload.m, com.tencent.assistantv2.model.d):java.lang.String
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.DownloadInfoMultiAdapter, java.util.List):java.util.ArrayList
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(java.util.List<com.tencent.assistant.download.DownloadInfoWrapper>, int):java.util.ArrayList<com.tencent.assistant.protocol.jce.InstalledAppItem>
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.DownloadInfoMultiAdapter, com.tencent.assistant.component.invalidater.ViewInvalidateMessage):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.DownloadInfoMultiAdapter, com.tencent.assistant.download.DownloadInfo):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.DownloadInfoMultiAdapter, com.tencent.assistant.download.DownloadInfoWrapper):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.bs, com.tencent.assistant.adapter.DownloadInfoMultiAdapter$CreatingTaskStatusEnum):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.bu, com.tencent.assistantv2.model.d):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.bx, com.tencent.assistantv2.model.h):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.by, boolean):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.download.DownloadInfoWrapper, android.view.View):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(java.util.List<com.tencent.assistant.download.DownloadInfoWrapper>, long):boolean
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(java.util.List<com.tencent.assistant.download.DownloadInfoWrapper>, com.tencent.assistant.download.DownloadInfoWrapper):boolean
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(java.lang.String, com.tencent.assistant.protocol.jce.InstalledAppItem):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.DownloadInfoMultiAdapter, boolean):boolean */
    public void onTMAClick(View view) {
        if (this.f714a.f.size() > 0) {
            ArrayList arrayList = new ArrayList(this.f714a.f);
            synchronized (this.f714a.f) {
                this.f714a.f.clear();
                this.f714a.g.clear();
                boolean unused = this.f714a.s = true;
                this.f714a.notifyDataSetChanged();
            }
            TemporaryThreadManager.get().start(new bb(this, arrayList));
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f714a.m, 200);
        buildSTInfo.status = Constants.VIA_REPORT_TYPE_MAKE_FRIEND;
        return buildSTInfo;
    }
}
