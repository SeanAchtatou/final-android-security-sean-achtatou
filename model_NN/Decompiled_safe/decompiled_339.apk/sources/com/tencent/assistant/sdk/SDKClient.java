package com.tencent.assistant.sdk;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import com.tencent.assistant.sdk.a.a;
import com.tencent.assistant.sdk.a.b;
import com.tencent.assistant.sdk.a.d;
import com.tencent.connect.common.Constants;
import com.tencent.d.a.e;
import java.io.UnsupportedEncodingException;
import java.util.UUID;
import java.util.concurrent.PriorityBlockingQueue;

/* compiled from: ProGuard */
public abstract class SDKClient implements ServiceConnection {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public a f2453a;
    private RequestQueue b = new RequestQueue();
    /* access modifiers changed from: private */
    public String c = null;
    private Context d;
    /* access modifiers changed from: private */
    public int e = 0;
    private d f = new j(this);

    /* access modifiers changed from: protected */
    public abstract void a(byte[] bArr);

    public synchronized void init(Context context) {
        this.d = context;
        d();
        new Thread(this.b, "SDKClient-send-thread").start();
    }

    /* access modifiers changed from: private */
    public synchronized void d() {
        this.e = 0;
        try {
            this.d.bindService(a(this.d), this, 1);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return;
    }

    public synchronized void close(Context context) {
        try {
            context.unbindService(this);
            context.stopService(a(context));
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return;
    }

    /* access modifiers changed from: protected */
    public Intent a(Context context) {
        Intent intent = new Intent("com.tencent.android.qqdownloader.SDKService");
        context.startService(intent);
        return intent;
    }

    /* access modifiers changed from: protected */
    public String a() {
        return UUID.randomUUID().toString();
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.f2453a = b.a(iBinder);
        try {
            this.c = a();
            this.f2453a.a(this.c, com.tencent.d.a.a.b(new e().b((System.currentTimeMillis() + Constants.STR_EMPTY).getBytes("utf-8"), this.c.getBytes()), 0), this.f);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        } catch (UnsupportedEncodingException e3) {
            e3.printStackTrace();
        } catch (Exception e4) {
            e4.printStackTrace();
        }
        b();
        this.b.a();
        this.e = 1;
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        if (this.f2453a != null) {
            this.f2453a = null;
        }
        c();
        this.e = -1;
    }

    /* compiled from: ProGuard */
    final class RequestQueue extends PriorityBlockingQueue<byte[]> implements Runnable {
        private Object b = new Object();

        public RequestQueue() {
            super(10, new k(SDKClient.this, null));
        }

        public void a() {
            synchronized (this.b) {
                this.b.notify();
            }
        }

        public void a(byte[] bArr) {
            synchronized (this.b) {
                super.put(bArr);
                this.b.notify();
            }
        }

        public void run() {
            while (true) {
                synchronized (this.b) {
                    while (true) {
                        if (size() != 0 && SDKClient.this.f2453a != null) {
                            break;
                        }
                        try {
                            if (SDKClient.this.e == -1) {
                                SDKClient.this.d();
                            }
                            this.b.wait(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (SDKClient.this.f2453a != null) {
                    try {
                        SDKClient.this.f2453a.b(SDKClient.this.c, (byte[]) take());
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    } catch (RemoteException e3) {
                        e3.printStackTrace();
                    } catch (Exception e4) {
                        e4.printStackTrace();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    /* access modifiers changed from: protected */
    public void c() {
    }

    public void send(byte[] bArr) {
        this.b.a(bArr);
    }
}
