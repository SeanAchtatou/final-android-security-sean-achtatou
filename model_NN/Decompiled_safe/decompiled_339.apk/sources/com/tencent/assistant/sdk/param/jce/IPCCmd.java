package com.tencent.assistant.sdk.param.jce;

import java.io.Serializable;

/* compiled from: ProGuard */
public final class IPCCmd implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final IPCCmd f2476a = new IPCCmd(0, 0, "Empty");
    public static final IPCCmd b = new IPCCmd(1, 1, "OperateDownloadTask");
    public static final IPCCmd c = new IPCCmd(2, 2, "GetDownloadState");
    public static final IPCCmd d = new IPCCmd(3, 3, "GetDownloadProgress");
    public static final IPCCmd e = new IPCCmd(4, 4, "QueryDownloadTask");
    public static final IPCCmd f = new IPCCmd(5, 5, "URIAction");
    public static final IPCCmd g = new IPCCmd(6, 6, "QueryLoginInfo");
    public static final IPCCmd h = new IPCCmd(7, 7, "QueryLoginState");
    public static final IPCCmd i = new IPCCmd(8, 8, "ServiceFreeAction");
    public static final IPCCmd j = new IPCCmd(9, 9, "BatchDownloadAction");
    static final /* synthetic */ boolean k = (!IPCCmd.class.desiredAssertionStatus());
    private static IPCCmd[] l = new IPCCmd[10];
    private int m;
    private String n = new String();

    public static IPCCmd a(int i2) {
        for (int i3 = 0; i3 < l.length; i3++) {
            if (l[i3].a() == i2) {
                return l[i3];
            }
        }
        if (k) {
            return null;
        }
        throw new AssertionError();
    }

    public static IPCCmd a(String str) {
        for (int i2 = 0; i2 < l.length; i2++) {
            if (l[i2].toString().equals(str)) {
                return l[i2];
            }
        }
        if (k) {
            return null;
        }
        throw new AssertionError();
    }

    public int a() {
        return this.m;
    }

    public String toString() {
        return this.n;
    }

    private IPCCmd(int i2, int i3, String str) {
        this.n = str;
        this.m = i3;
        l[i2] = this;
    }
}
