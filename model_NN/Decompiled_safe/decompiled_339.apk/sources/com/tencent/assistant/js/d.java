package com.tencent.assistant.js;

import android.app.Activity;
import android.text.TextUtils;
import com.tencent.assistant.activity.BrowserActivity;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1351a;
    final /* synthetic */ int b;
    final /* synthetic */ int c;
    final /* synthetic */ int d;
    final /* synthetic */ String e;
    final /* synthetic */ int f;
    final /* synthetic */ String g;
    final /* synthetic */ JsBridge h;

    d(JsBridge jsBridge, String str, int i, int i2, int i3, String str2, int i4, String str3) {
        this.h = jsBridge;
        this.f1351a = str;
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.e = str2;
        this.f = i4;
        this.g = str3;
    }

    public void run() {
        Activity activity = (Activity) this.h.mActivityRef.get();
        if (activity instanceof BrowserActivity) {
            if (!TextUtils.isEmpty(this.f1351a)) {
                activity.setTitle(this.f1351a);
            }
            if (this.b == 0) {
                ((BrowserActivity) activity).c(true);
            } else if (this.b == 1) {
                ((BrowserActivity) activity).c(false);
            }
            if (this.c == 0) {
                ((BrowserActivity) activity).d(false);
            } else if (this.c == 1) {
                ((BrowserActivity) activity).d(true);
            }
            if (this.d == 1) {
                ((BrowserActivity) activity).b(true);
            } else {
                ((BrowserActivity) activity).b(false);
            }
            this.h.response(this.e, this.f, this.g, Constants.STR_EMPTY);
            return;
        }
        this.h.responseFail(this.e, this.f, this.g, -1);
    }
}
