package com.tencent.assistant.js;

import android.net.Uri;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class e extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Uri f1352a;
    final /* synthetic */ String b;
    final /* synthetic */ int c;
    final /* synthetic */ String d;
    final /* synthetic */ JsBridge e;

    e(JsBridge jsBridge, Uri uri, String str, int i, String str2) {
        this.e = jsBridge;
        this.f1352a = uri;
        this.b = str;
        this.c = i;
        this.d = str2;
    }

    public void onLeftBtnClick() {
    }

    public void onRightBtnClick() {
        TemporaryThreadManager.get().start(new f(this));
    }

    public void onCancell() {
    }
}
