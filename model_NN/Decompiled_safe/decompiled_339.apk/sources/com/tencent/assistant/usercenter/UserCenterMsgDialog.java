package com.tencent.assistant.usercenter;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class UserCenterMsgDialog extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    private Context f2608a;
    private ListView b;
    private UserCenterMsgAdapter c;

    public UserCenterMsgDialog(Context context, int i) {
        super(context, i);
        this.f2608a = context;
    }

    public UserCenterMsgDialog(Context context) {
        super(context);
        this.f2608a = context;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialog_user_center_msg);
        a();
    }

    private void a() {
        this.b = (ListView) findViewById(R.id.listview);
        this.c = new UserCenterMsgAdapter(this.f2608a);
        this.b.setAdapter((ListAdapter) this.c);
        this.b.setDivider(null);
    }
}
