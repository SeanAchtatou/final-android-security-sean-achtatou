package com.tencent.assistant.c;

import android.os.Message;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class k implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f873a;

    k(h hVar) {
        this.f873a = hVar;
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_SHOW_TREASURE_BOX_ENTRY /*1118*/:
                boolean unused = this.f873a.g = true;
                this.f873a.e.setVisibility(0);
                this.f873a.f.setVisibility(0);
                return;
            case EventDispatcherEnum.UI_EVENT_DISMISS_TREASURE_BOX_ENTRY /*1119*/:
                boolean unused2 = this.f873a.g = false;
                this.f873a.e.setVisibility(8);
                this.f873a.f.setVisibility(8);
                TemporaryThreadManager.get().start(new l(this));
                return;
            default:
                return;
        }
    }
}
