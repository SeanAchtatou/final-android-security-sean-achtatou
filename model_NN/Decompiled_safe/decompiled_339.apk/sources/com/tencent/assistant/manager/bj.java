package com.tencent.assistant.manager;

import android.util.Log;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.s;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.model.StatInfo;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/* compiled from: ProGuard */
class bj implements s {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CountDownLatch f1504a;
    final /* synthetic */ be b;

    bj(be beVar, CountDownLatch countDownLatch) {
        this.b = beVar;
        this.f1504a = countDownLatch;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.be.a(com.tencent.assistant.manager.be, boolean):boolean
     arg types: [com.tencent.assistant.manager.be, int]
     candidates:
      com.tencent.assistant.manager.be.a(com.tencent.assistant.manager.be, com.tencent.assistant.download.DownloadInfo):void
      com.tencent.assistant.manager.be.a(com.tencent.assistant.manager.be, java.util.concurrent.CountDownLatch):void
      com.tencent.assistant.manager.be.a(int, com.tencent.assistant.download.DownloadInfo):void
      com.tencent.assistant.manager.be.a(com.tencent.assistant.manager.be, boolean):boolean */
    public void onGetAppInfoFail(int i, int i2) {
        if (this.f1504a != null) {
            boolean unused = this.b.e = false;
            this.f1504a.countDown();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, boolean):boolean
     arg types: [com.tencent.assistant.download.DownloadInfo, int]
     candidates:
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.a, java.util.List):java.util.List
      com.tencent.assistant.download.a.a(java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>, boolean):void
      com.tencent.assistant.download.a.a(long, long):boolean
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.a, boolean):boolean
      com.tencent.assistant.download.a.a(java.lang.String, int):boolean
      com.tencent.assistant.download.a.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, com.tencent.assistantv2.st.model.StatInfo):int
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.download.SimpleDownloadInfo$UIType):void
      com.tencent.assistant.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo):void
      com.tencent.assistant.download.a.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.download.a.a(java.lang.String, boolean):boolean
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.be.a(com.tencent.assistant.manager.be, boolean):boolean
     arg types: [com.tencent.assistant.manager.be, int]
     candidates:
      com.tencent.assistant.manager.be.a(com.tencent.assistant.manager.be, com.tencent.assistant.download.DownloadInfo):void
      com.tencent.assistant.manager.be.a(com.tencent.assistant.manager.be, java.util.concurrent.CountDownLatch):void
      com.tencent.assistant.manager.be.a(int, com.tencent.assistant.download.DownloadInfo):void
      com.tencent.assistant.manager.be.a(com.tencent.assistant.manager.be, boolean):boolean */
    public void onGetAppInfoSuccess(int i, int i2, AppSimpleDetail appSimpleDetail) {
        DownloadInfo downloadInfo;
        DownloadInfo downloadInfo2;
        SimpleAppModel a2 = u.a(appSimpleDetail);
        DownloadInfo a3 = DownloadProxy.a().a(a2);
        StatInfo statInfo = new StatInfo(a2.b, STConst.ST_PAGE_APP_DETAIL, 0, null, 0);
        if (a3 == null || !a3.needReCreateInfo(a2)) {
            downloadInfo = a3;
        } else {
            DownloadProxy.a().b(a3.downloadTicket);
            downloadInfo = null;
        }
        if (downloadInfo == null) {
            downloadInfo2 = DownloadInfo.createDownloadInfo(a2, statInfo);
            DownloadProxy.a().d(downloadInfo2);
        } else {
            downloadInfo2 = downloadInfo;
        }
        switch (this.b.k(downloadInfo2)) {
            case 0:
                List<DownloadInfo> e = DownloadProxy.a().e("com.tencent.qlauncher");
                if (e != null && e.size() > 0) {
                    be.a(true);
                    a.a().a(e.get(0), true);
                }
                if (this.f1504a != null) {
                    boolean unused = this.b.e = true;
                    break;
                }
                break;
            case 1:
                Log.d("QubeManager", "<Qube> download qlauncher form server");
                downloadInfo2.autoInstall = true;
                DownloadProxy.a().d(downloadInfo2);
                be.a(true);
                DownloadProxy.a().c(downloadInfo2);
                if (this.f1504a != null) {
                    boolean unused2 = this.b.e = true;
                    break;
                }
                break;
            case 2:
                Log.d("QubeManager", "<Qube> the qlauncher installed is the lastest !!!");
                be.d.sendEmptyMessage(4);
                if (this.f1504a != null) {
                    boolean unused3 = this.b.e = false;
                    break;
                }
                break;
        }
        if (this.f1504a != null) {
            this.f1504a.countDown();
        }
    }
}
