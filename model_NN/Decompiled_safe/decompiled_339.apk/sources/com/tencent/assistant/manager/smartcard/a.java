package com.tencent.assistant.manager.smartcard;

import com.tencent.assistant.model.a.c;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.q;
import com.tencent.assistant.model.a.r;
import com.tencent.assistant.model.a.s;
import com.tencent.assistant.utils.cv;
import java.util.List;

/* compiled from: ProGuard */
public class a extends y {
    public boolean a(i iVar, List<Long> list) {
        if (iVar == null || iVar.i != 16) {
            return false;
        }
        return a((c) iVar, (r) this.f1610a.get(Integer.valueOf(iVar.k())), (s) this.b.get(Integer.valueOf(iVar.k())));
    }

    private boolean a(c cVar, r rVar, s sVar) {
        if (sVar == null) {
            return false;
        }
        if (rVar == null) {
            rVar = new r();
            rVar.f = cVar.j;
            rVar.e = cVar.i;
            this.f1610a.put(Integer.valueOf(cVar.k()), rVar);
        }
        if (rVar.b >= sVar.c) {
            a(cVar.s, cVar.j + "||" + cVar.i + "|" + 1, cVar.i);
            return false;
        } else if (rVar.f1651a < sVar.f1652a) {
            return true;
        } else {
            a(cVar.s, cVar.j + "||" + cVar.i + "|" + 2, cVar.i);
            return false;
        }
    }

    public void a(q qVar) {
        r rVar;
        int i;
        if (qVar != null) {
            r rVar2 = (r) this.f1610a.get(Integer.valueOf(qVar.a()));
            if (rVar2 == null) {
                r rVar3 = new r();
                rVar3.e = qVar.f1650a;
                rVar3.f = qVar.b;
                rVar = rVar3;
            } else {
                rVar = rVar2;
            }
            if (qVar.d) {
                rVar.d = true;
            }
            if (qVar.c) {
                if (cv.b(qVar.e * 1000)) {
                    rVar.f1651a++;
                }
                s sVar = (s) this.b.get(Integer.valueOf(qVar.f1650a));
                if (sVar != null) {
                    i = sVar.i;
                } else {
                    i = 7;
                }
                if (cv.a(qVar.e * 1000, i)) {
                    rVar.b++;
                }
                rVar.c++;
            }
            this.f1610a.put(Integer.valueOf(rVar.a()), rVar);
        }
    }

    public void a(s sVar) {
        this.b.put(Integer.valueOf(sVar.a()), sVar);
    }

    public void a(i iVar) {
        if (iVar != null && iVar.i == 16) {
            c cVar = (c) iVar;
            s sVar = (s) this.b.get(Integer.valueOf(cVar.k()));
            if (sVar != null) {
                cVar.p = sVar.d;
                cVar.f = sVar.b;
                cVar.e = sVar.f1652a;
            }
        }
    }
}
