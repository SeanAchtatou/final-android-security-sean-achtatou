package com.tencent.assistant.manager.smartcard.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.manager.smartcard.b.c;
import com.tencent.assistant.utils.cn;

/* compiled from: ProGuard */
public class NormalSmartCardHeader extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private ImageView f1603a;
    private TextView b;
    private TextView c;
    private Context d;
    private c e;

    public NormalSmartCardHeader(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.d = context;
        a();
    }

    public NormalSmartCardHeader(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.d = context;
        a();
    }

    public NormalSmartCardHeader(Context context) {
        super(context);
        this.d = context;
        a();
    }

    private void a() {
        LayoutInflater.from(this.d).inflate((int) R.layout.normal_smartcard_header, this);
        this.f1603a = (ImageView) findViewById(R.id.title_icon);
        this.b = (TextView) findViewById(R.id.title);
        this.c = (TextView) findViewById(R.id.goto_text);
    }

    public void a(c cVar, View.OnClickListener onClickListener) {
        if (cVar != null) {
            this.e = cVar;
            int a2 = cn.a(this.e.f1591a);
            if (a2 != 0) {
                try {
                    this.f1603a.setImageResource(a2);
                } catch (Throwable th) {
                    cq.a().b();
                }
            }
            this.b.setText(this.e.b);
            if (!TextUtils.isEmpty(this.e.c)) {
                this.c.setText(this.e.c);
                this.c.setVisibility(0);
                this.c.setOnClickListener(onClickListener);
                return;
            }
            this.c.setVisibility(8);
        }
    }
}
