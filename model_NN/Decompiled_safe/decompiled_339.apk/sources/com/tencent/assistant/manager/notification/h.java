package com.tencent.assistant.manager.notification;

import android.os.Build;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class h {

    /* renamed from: a  reason: collision with root package name */
    private static h f1567a;
    /* access modifiers changed from: private */
    public m b;
    private m c;
    private ApkResCallback.Stub d = new i(this);

    public static synchronized h a() {
        h hVar;
        synchronized (h.class) {
            if (f1567a == null) {
                f1567a = new h();
            }
            hVar = f1567a;
        }
        return hVar;
    }

    public h() {
        b();
        if (this.b == null) {
            this.b = new m();
        }
        if (this.c == null) {
            this.c = new m();
        }
    }

    public void a(String str) {
        if (this.b != null) {
            this.b.a(str);
        }
        if (this.c != null) {
            this.c.a(str);
        }
    }

    public void a(String str, String str2, int i, String str3) {
        if (!Build.BRAND.contains("Meizu")) {
            this.c.a(str, str2, i, str3);
        }
    }

    public void b(String str) {
        DownloadInfo f;
        String string;
        if (!Build.BRAND.contains("Meizu")) {
            if (this.c == null && (f = DownloadProxy.a().f(str)) != null && !TextUtils.isEmpty(f.downloadTicket)) {
                if (!f.isSllUpdateApk() && !f.isUpdateApk()) {
                    if (!TextUtils.isEmpty(f.name)) {
                        string = f.name;
                    } else {
                        string = AstApp.i().getString(R.string.unknowappname_install_notification_title);
                    }
                    int i = (int) f.appId;
                    XLog.d("InstallPush", "id: " + i);
                    a().a(str, string, i, f.iconUrl);
                } else {
                    return;
                }
            }
            n b2 = this.c.b(str);
            if (b2 != null) {
                this.b.a(b2.c(), b2.b(), b2.a(), b2.d());
                j.a().a(this.b);
            }
        }
    }

    private void b() {
        ApkResourceManager.getInstance().registerApkResCallback(this.d);
    }
}
