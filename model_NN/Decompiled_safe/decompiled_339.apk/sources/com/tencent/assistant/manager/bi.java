package com.tencent.assistant.manager;

import android.util.Log;
import com.tencent.assistant.download.DownloadInfo;
import java.util.concurrent.CountDownLatch;

/* compiled from: ProGuard */
class bi implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f1503a;
    final /* synthetic */ be b;

    bi(be beVar, DownloadInfo downloadInfo) {
        this.b = beVar;
        this.f1503a = downloadInfo;
    }

    public void run() {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        try {
            this.b.a(countDownLatch);
            countDownLatch.await();
            if (this.b.e) {
                Log.i("QubeManager", "<Qube> downloadLastestQubeWithTheme download theme");
                this.f1503a.autoInstall = false;
                DownloadProxy.a().d(this.f1503a);
                DownloadProxy.a().c(this.f1503a);
                return;
            }
            this.b.c(this.f1503a);
        } catch (Exception e) {
            Log.e("QubeManager", "downloadLastestQubeWithTheme", e);
        }
    }
}
