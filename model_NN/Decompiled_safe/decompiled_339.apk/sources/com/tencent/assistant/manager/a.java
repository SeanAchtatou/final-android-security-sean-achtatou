package com.tencent.assistant.manager;

import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.ApkAutoOpenCfg;
import com.tencent.assistant.protocol.jce.ApkAutoOpenVec;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bh;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/* compiled from: ProGuard */
public class a implements v {

    /* renamed from: a  reason: collision with root package name */
    protected static a f1471a = null;
    protected ArrayList<ApkAutoOpenCfg> b = null;

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (f1471a == null) {
                f1471a = new a();
            }
            aVar = f1471a;
        }
        return aVar;
    }

    private a() {
        u.a().a(this);
        b();
    }

    public void a(HashMap<String, Object> hashMap) {
        b();
    }

    private void b() {
        ApkAutoOpenVec apkAutoOpenVec;
        byte[] e = m.a().e("key_get_apk_auto_open_action");
        if (e != null && e.length > 0 && (apkAutoOpenVec = (ApkAutoOpenVec) bh.b(e, ApkAutoOpenVec.class)) != null) {
            this.b = new ArrayList<>(apkAutoOpenVec.f1978a);
        }
    }

    public ApkAutoOpenCfg a(String str, String str2) {
        XLog.d("vivi", "getApkAutoOpenCfg:" + this.b);
        if (this.b == null) {
            return null;
        }
        Iterator<ApkAutoOpenCfg> it = this.b.iterator();
        while (it.hasNext()) {
            ApkAutoOpenCfg next = it.next();
            if (next != null && next.f1977a.equalsIgnoreCase(str) && next.d.equalsIgnoreCase(str2)) {
                return next;
            }
        }
        return null;
    }
}
