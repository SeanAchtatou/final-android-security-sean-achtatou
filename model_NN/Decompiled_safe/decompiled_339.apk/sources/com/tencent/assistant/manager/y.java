package com.tencent.assistant.manager;

import android.app.Dialog;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.manager.notification.v;

/* compiled from: ProGuard */
class y extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadProxy f1626a;

    y(DownloadProxy downloadProxy) {
        this.f1626a = downloadProxy;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.notification.v.a(boolean, int):void
     arg types: [int, int]
     candidates:
      com.tencent.assistant.manager.notification.v.a(int, long):void
      com.tencent.assistant.manager.notification.v.a(int, android.app.Notification):void
      com.tencent.assistant.manager.notification.v.a(int, java.lang.String):void
      com.tencent.assistant.manager.notification.v.a(com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void
      com.tencent.assistant.manager.notification.v.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.manager.notification.v.a(java.lang.String, boolean):void
      com.tencent.assistant.manager.notification.v.a(boolean, int):void */
    public void onLeftBtnClick() {
        v.a().a(false, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.notification.v.a(boolean, int):void
     arg types: [int, int]
     candidates:
      com.tencent.assistant.manager.notification.v.a(int, long):void
      com.tencent.assistant.manager.notification.v.a(int, android.app.Notification):void
      com.tencent.assistant.manager.notification.v.a(int, java.lang.String):void
      com.tencent.assistant.manager.notification.v.a(com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void
      com.tencent.assistant.manager.notification.v.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.manager.notification.v.a(java.lang.String, boolean):void
      com.tencent.assistant.manager.notification.v.a(boolean, int):void */
    public void onRightBtnClick() {
        v.a().a(false, 0);
        DownloadProxy.a().e();
    }

    public void onCancell() {
        Dialog unused = this.f1626a.h = (Dialog) null;
    }
}
