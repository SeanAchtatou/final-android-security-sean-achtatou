package com.tencent.assistant.manager.notification.a;

import android.graphics.Bitmap;
import android.widget.RemoteViews;
import com.tencent.assistant.manager.notification.a.a.e;

/* compiled from: ProGuard */
class q implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RemoteViews f1559a;
    final /* synthetic */ int[] b;
    final /* synthetic */ int c;
    final /* synthetic */ p d;

    q(p pVar, RemoteViews remoteViews, int[] iArr, int i) {
        this.d = pVar;
        this.f1559a = remoteViews;
        this.b = iArr;
        this.c = i;
    }

    public void a(Bitmap bitmap) {
        if (bitmap != null && !bitmap.isRecycled()) {
            this.f1559a.setViewVisibility(this.b[this.c], 0);
            this.f1559a.setImageViewBitmap(this.b[this.c], bitmap);
        }
    }
}
