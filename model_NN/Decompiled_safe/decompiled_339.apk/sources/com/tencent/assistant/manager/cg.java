package com.tencent.assistant.manager;

import android.content.Context;
import android.content.IntentFilter;
import com.tencent.assistant.receiver.SdCardEventReceiver;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: ProGuard */
public class cg {

    /* renamed from: a  reason: collision with root package name */
    protected ReferenceQueue<ch> f1527a = new ReferenceQueue<>();
    protected ConcurrentLinkedQueue<WeakReference<ch>> b = new ConcurrentLinkedQueue<>();
    private SdCardEventReceiver c = new SdCardEventReceiver();

    protected cg() {
    }

    /* access modifiers changed from: protected */
    public void a(ch chVar) {
        if (chVar != null) {
            while (true) {
                Reference<? extends ch> poll = this.f1527a.poll();
                if (poll == null) {
                    break;
                }
                this.b.remove(poll);
            }
            Iterator<WeakReference<ch>> it = this.b.iterator();
            while (it.hasNext()) {
                if (((ch) it.next().get()) == chVar) {
                    return;
                }
            }
            this.b.add(new WeakReference(chVar, this.f1527a));
        }
    }

    /* access modifiers changed from: protected */
    public void b(ch chVar) {
        if (chVar != null) {
            Iterator<WeakReference<ch>> it = this.b.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                if (((ch) next.get()) == chVar) {
                    this.b.remove(next);
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Context context) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.MEDIA_BAD_REMOVAL");
        intentFilter.addAction("android.intent.action.MEDIA_EJECT");
        intentFilter.addAction("android.intent.action.MEDIA_MOUNTED");
        intentFilter.addAction("android.intent.action.MEDIA_REMOVED");
        intentFilter.addAction("android.intent.action.MEDIA_UNMOUNTED");
        intentFilter.addDataScheme("file");
        context.getApplicationContext().registerReceiver(this.c, intentFilter);
    }

    /* access modifiers changed from: protected */
    public void a() {
        Iterator<WeakReference<ch>> it = this.b.iterator();
        while (it.hasNext()) {
            ch chVar = (ch) it.next().get();
            if (chVar != null) {
                chVar.a();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        Iterator<WeakReference<ch>> it = this.b.iterator();
        while (it.hasNext()) {
            ch chVar = (ch) it.next().get();
            if (chVar != null) {
                chVar.b();
            }
        }
    }
}
