package com.tencent.assistant.manager.notification;

import android.graphics.Bitmap;
import com.tencent.assistant.thumbnailCache.k;

/* compiled from: ProGuard */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f1565a;

    e(d dVar) {
        this.f1565a = dVar;
    }

    public void run() {
        Bitmap a2 = k.b().a(this.f1565a.f1564a, this.f1565a.b, this.f1565a.e);
        if (a2 != null && !a2.isRecycled()) {
            this.f1565a.a(0, a2);
        }
    }
}
