package com.tencent.assistant.manager;

import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.protocol.jce.AdviceApp;

/* compiled from: ProGuard */
class bs extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ cd f1513a;
    final /* synthetic */ bo b;

    bs(bo boVar, cd cdVar) {
        this.b = boVar;
        this.f1513a = cdVar;
    }

    public void onCancell() {
        int i = 2000;
        if (AstApp.m() != null) {
            i = AstApp.m().f();
        }
        AdviceApp b2 = this.f1513a.b();
        this.b.a(this.pageId, i, "03_002", 200, b2 != null ? b2.i() : null);
    }

    public void onLeftBtnClick() {
        int i = 2000;
        if (AstApp.m() != null) {
            i = AstApp.m().f();
        }
        AdviceApp b2 = this.f1513a.b();
        this.b.a(this.pageId, i, "03_002", 200, b2 != null ? b2.i() : null);
    }

    public void onRightBtnClick() {
        this.b.a(this.pageId);
        this.b.a(this.f1513a.e, this.f1513a.f);
        int i = 2000;
        if (AstApp.m() != null) {
            i = AstApp.m().f();
        }
        AdviceApp b2 = this.f1513a.b();
        this.b.a(this.pageId, i, "03_001", 200, b2 != null ? b2.i() : null);
    }
}
