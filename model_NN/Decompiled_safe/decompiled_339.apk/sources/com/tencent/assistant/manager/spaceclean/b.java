package com.tencent.assistant.manager.spaceclean;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.tencent.assistant.utils.XLog;
import com.tencent.tmsecurelite.commom.ServiceManager;
import com.tencent.tmsecurelite.optimize.ISystemOptimize;

/* compiled from: ProGuard */
class b implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceScanManager f1616a;

    b(SpaceScanManager spaceScanManager) {
        this.f1616a = spaceScanManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.spaceclean.SpaceScanManager.a(com.tencent.assistant.manager.spaceclean.SpaceScanManager, boolean):boolean
     arg types: [com.tencent.assistant.manager.spaceclean.SpaceScanManager, int]
     candidates:
      com.tencent.assistant.manager.spaceclean.SpaceScanManager.a(com.tencent.assistant.manager.spaceclean.SpaceScanManager, com.tencent.tmsecurelite.optimize.ISystemOptimize):com.tencent.tmsecurelite.optimize.ISystemOptimize
      com.tencent.assistant.manager.spaceclean.SpaceScanManager.a(long, com.tencent.assistant.model.spaceclean.SubRubbishInfo):void
      com.tencent.assistant.manager.spaceclean.SpaceScanManager.a(com.tencent.assistant.manager.spaceclean.SpaceScanManager, java.util.List):void
      com.tencent.assistant.manager.spaceclean.SpaceScanManager.a(com.tencent.assistant.module.callback.ak, java.util.ArrayList<java.lang.String>):void
      com.tencent.assistant.manager.spaceclean.SpaceScanManager.a(com.tencent.tmsecurelite.commom.b, java.util.ArrayList<java.lang.String>):void
      com.tencent.assistant.manager.spaceclean.SpaceScanManager.a(com.tencent.assistant.manager.spaceclean.SpaceScanManager, boolean):boolean */
    public void onServiceDisconnected(ComponentName componentName) {
        ISystemOptimize unused = this.f1616a.y = (ISystemOptimize) null;
        boolean unused2 = this.f1616a.l = false;
        this.f1616a.u();
        XLog.d("miles", "onServiceDisconnected.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.spaceclean.SpaceScanManager.a(com.tencent.assistant.manager.spaceclean.SpaceScanManager, boolean):boolean
     arg types: [com.tencent.assistant.manager.spaceclean.SpaceScanManager, int]
     candidates:
      com.tencent.assistant.manager.spaceclean.SpaceScanManager.a(com.tencent.assistant.manager.spaceclean.SpaceScanManager, com.tencent.tmsecurelite.optimize.ISystemOptimize):com.tencent.tmsecurelite.optimize.ISystemOptimize
      com.tencent.assistant.manager.spaceclean.SpaceScanManager.a(long, com.tencent.assistant.model.spaceclean.SubRubbishInfo):void
      com.tencent.assistant.manager.spaceclean.SpaceScanManager.a(com.tencent.assistant.manager.spaceclean.SpaceScanManager, java.util.List):void
      com.tencent.assistant.manager.spaceclean.SpaceScanManager.a(com.tencent.assistant.module.callback.ak, java.util.ArrayList<java.lang.String>):void
      com.tencent.assistant.manager.spaceclean.SpaceScanManager.a(com.tencent.tmsecurelite.commom.b, java.util.ArrayList<java.lang.String>):void
      com.tencent.assistant.manager.spaceclean.SpaceScanManager.a(com.tencent.assistant.manager.spaceclean.SpaceScanManager, boolean):boolean */
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        try {
            ISystemOptimize unused = this.f1616a.y = (ISystemOptimize) ServiceManager.getInterface(0, iBinder);
            if (!this.f1616a.y.checkVersion(2)) {
                XLog.d("miles", "SpaceScanManager >> onServiceConnected. But mobile manager version is low.");
                this.f1616a.d();
                this.f1616a.t();
                return;
            }
            boolean unused2 = this.f1616a.l = true;
            this.f1616a.s();
        } catch (Exception e) {
            e.printStackTrace();
            this.f1616a.d();
            this.f1616a.t();
        }
    }
}
