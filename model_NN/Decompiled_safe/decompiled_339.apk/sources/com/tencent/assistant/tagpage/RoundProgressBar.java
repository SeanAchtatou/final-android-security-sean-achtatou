package com.tencent.assistant.tagpage;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import com.tencent.android.qqdownloader.b;
import java.util.Random;

/* compiled from: ProGuard */
public class RoundProgressBar extends View {

    /* renamed from: a  reason: collision with root package name */
    private Paint f2541a;
    private int b;
    private int c;
    private int d;
    private float e;
    private float f;
    private int g;
    private int h;
    private boolean i;
    private int j;

    public RoundProgressBar(Context context) {
        this(context, null);
    }

    public RoundProgressBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public RoundProgressBar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f2541a = new Paint();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, b.l);
        this.b = obtainStyledAttributes.getColor(0, -65536);
        this.c = obtainStyledAttributes.getColor(1, -16711936);
        this.d = obtainStyledAttributes.getColor(3, -16711936);
        this.e = obtainStyledAttributes.getDimension(4, 15.0f);
        this.f = obtainStyledAttributes.getDimension(2, 5.0f);
        this.g = obtainStyledAttributes.getInteger(5, 100);
        this.i = obtainStyledAttributes.getBoolean(6, false);
        this.j = obtainStyledAttributes.getInt(7, 0);
        obtainStyledAttributes.recycle();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int width = getWidth() / 2;
        int i2 = (int) (((float) width) - (this.f / 2.0f));
        this.f2541a.setColor(this.b);
        this.f2541a.setStyle(Paint.Style.STROKE);
        this.f2541a.setStrokeWidth(this.f);
        this.f2541a.setAntiAlias(true);
        canvas.drawCircle((float) width, (float) width, (float) i2, this.f2541a);
        this.f2541a.setStrokeWidth(0.0f);
        this.f2541a.setColor(this.d);
        this.f2541a.setTextSize(this.e);
        this.f2541a.setTypeface(Typeface.DEFAULT_BOLD);
        int i3 = (int) ((((float) this.h) / ((float) this.g)) * 100.0f);
        float measureText = this.f2541a.measureText(i3 + "%");
        if (this.i && i3 != 0 && this.j == 0) {
            canvas.drawText(i3 + "%", ((float) width) - (measureText / 2.0f), ((float) width) + (this.e / 2.0f), this.f2541a);
        }
        this.f2541a.setStrokeWidth(this.f);
        this.f2541a.setColor(this.c);
        RectF rectF = new RectF((float) (width - i2), (float) (width - i2), (float) (width + i2), (float) (width + i2));
        switch (this.j) {
            case 0:
                this.f2541a.setStyle(Paint.Style.STROKE);
                canvas.drawArc(rectF, -90.0f, (float) ((this.h * 360) / this.g), false, this.f2541a);
                return;
            case 1:
                this.f2541a.setStyle(Paint.Style.FILL_AND_STROKE);
                if (this.h != 0) {
                    canvas.drawArc(rectF, -90.0f, (float) ((this.h * 360) / this.g), true, this.f2541a);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public synchronized void a(int i2) {
        if (i2 < 0) {
            throw new IllegalArgumentException("progress not less than 0");
        }
        if (i2 > this.g) {
            i2 = this.g;
        }
        if (i2 <= this.g) {
            this.h = i2;
            postInvalidate();
        }
    }

    public static int a(int i2, int i3) {
        return (i3 - i2) + 1 == 0 ? i2 : i2 + (new Random().nextInt(i3) % ((i3 - i2) + 1));
    }
}
