package com.tencent.assistant.st;

import com.tencent.assistant.model.a.u;
import com.tencent.assistantv2.component.RankNormalListView;

/* compiled from: ProGuard */
public class i {
    public static String a(u uVar, int i) {
        return a(uVar) + "_" + STConst.ST_STATUS_DEFAULT + String.valueOf(i);
    }

    public static String a(u uVar) {
        if (uVar != null) {
            if (uVar.i == 15) {
                return RankNormalListView.ST_HIDE_INSTALLED_APPS;
            }
            if (uVar.i == 14) {
                return "09";
            }
            if (uVar.i == 27) {
                return "04";
            }
        }
        return STConst.ST_STATUS_DEFAULT;
    }
}
