package com.tencent.assistant.utils;

import android.content.SharedPreferences;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.connect.common.Constants;
import org.json.JSONArray;
import org.json.JSONException;

/* compiled from: ProGuard */
public class ao {

    /* renamed from: a  reason: collision with root package name */
    private static ao f2636a;
    private static SharedPreferences b;
    private static SharedPreferences.Editor c;
    private static String d = "downloaded_pkg_name";
    private static String e = "pkg_name_key";

    private ao() {
    }

    private static void b() {
        b = AstApp.i().getSharedPreferences(d, 0);
        c = b.edit();
    }

    public static ao a() {
        if (f2636a == null) {
            synchronized (ao.class) {
                if (f2636a == null) {
                    f2636a = new ao();
                    b();
                }
            }
        }
        return f2636a;
    }

    public boolean a(String str) {
        JSONArray jSONArray;
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        if (d(str)) {
            return true;
        }
        synchronized (ao.class) {
            String string = b.getString(e, Constants.STR_EMPTY);
            if (TextUtils.isEmpty(string)) {
                jSONArray = new JSONArray();
            } else {
                try {
                    jSONArray = new JSONArray(string);
                } catch (JSONException e2) {
                    e2.printStackTrace();
                    return false;
                }
            }
            jSONArray.put(str);
            c.putString(e, jSONArray.toString());
            c.commit();
        }
        return true;
    }

    private boolean d(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        synchronized (ao.class) {
            String string = b.getString(e, Constants.STR_EMPTY);
            if (TextUtils.isEmpty(string)) {
                return false;
            }
            try {
                JSONArray jSONArray = new JSONArray(string);
                for (int i = 0; i < jSONArray.length(); i++) {
                    if (((String) jSONArray.get(i)).equals(str)) {
                        return true;
                    }
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
                return false;
            }
        }
    }

    public boolean b(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        if (!d(str)) {
            return true;
        }
        synchronized (ao.class) {
            String string = b.getString(e, Constants.STR_EMPTY);
            if (TextUtils.isEmpty(string)) {
                return true;
            }
            JSONArray jSONArray = new JSONArray();
            try {
                JSONArray jSONArray2 = new JSONArray(string);
                for (int i = 0; i < jSONArray2.length(); i++) {
                    String str2 = (String) jSONArray2.get(i);
                    if (!str2.equals(str)) {
                        jSONArray.put(str2);
                    }
                }
                c.putString(e, jSONArray.toString());
                c.commit();
                return true;
            } catch (JSONException e2) {
                e2.printStackTrace();
                return false;
            }
        }
    }

    public boolean c(String str) {
        return d(str);
    }
}
