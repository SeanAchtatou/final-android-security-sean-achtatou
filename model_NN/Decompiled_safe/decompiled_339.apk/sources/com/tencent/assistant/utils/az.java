package com.tencent.assistant.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
final class az extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Dialog f2643a;
    final /* synthetic */ Activity b;
    final /* synthetic */ SelfUpdateManager.SelfUpdateInfo c;

    az(Dialog dialog, Activity activity, SelfUpdateManager.SelfUpdateInfo selfUpdateInfo) {
        this.f2643a = dialog;
        this.b = activity;
        this.c = selfUpdateInfo;
    }

    public void onTMAClick(View view) {
        SelfUpdateManager.a().o().b(false);
        SelfUpdateManager.a().a(SelfUpdateManager.SelfUpdateType.NORMAL);
        this.f2643a.dismiss();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 b2 = FunctionUtils.b(this.b);
        b2.slotId += "_01_001";
        if (!SelfUpdateManager.a().a(this.c.e)) {
            b2.actionId = 900;
        } else {
            b2.actionId = STConstAction.ACTION_HIT_INSTALL;
        }
        b2.extraData = "02";
        return b2;
    }
}
