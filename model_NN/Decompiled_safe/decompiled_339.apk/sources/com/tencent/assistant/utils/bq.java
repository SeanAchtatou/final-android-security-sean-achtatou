package com.tencent.assistant.utils;

import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.securemodule.impl.AppInfo;
import com.tencent.securemodule.service.CloudScanListener;
import java.util.List;

/* compiled from: ProGuard */
class bq implements CloudScanListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bk f2657a;

    private bq(bk bkVar) {
        this.f2657a = bkVar;
    }

    /* synthetic */ bq(bk bkVar, bl blVar) {
        this(bkVar);
    }

    public void onFinish(int i) {
        XLog.i(this.f2657a.b, ">>Safe scan finish>>");
        Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_MGR_SAFE_SCAN_SUCCESS);
        obtainMessage.obj = 0;
        AstApp.i().j().dispatchMessage(obtainMessage);
    }

    public void onRiskFoud(List<AppInfo> list) {
        int i = 0;
        XLog.i(this.f2657a.b, "onRiskFoun:" + (list == null ? 0 : list.size()));
        Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_MGR_SAFE_SCAN_SUCCESS);
        if (list != null) {
            i = list.size();
        }
        obtainMessage.obj = Integer.valueOf(i);
        AstApp.i().j().dispatchMessage(obtainMessage);
        StringBuilder sb = new StringBuilder();
        for (AppInfo appInfo : list) {
            sb.append(appInfo.pkgName + "|");
        }
        br.a(sb.toString());
    }

    @Deprecated
    public void onRiskFound() {
    }
}
