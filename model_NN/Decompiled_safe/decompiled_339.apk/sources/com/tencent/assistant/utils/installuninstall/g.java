package com.tencent.assistant.utils.installuninstall;

import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.FunctionUtils;

/* compiled from: ProGuard */
class g extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstallUninstallDialogManager f2701a;

    g(InstallUninstallDialogManager installUninstallDialogManager) {
        this.f2701a = installUninstallDialogManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onCancell() {
        boolean unused = this.f2701a.c = false;
        this.f2701a.c();
        this.f2701a.a(this.pageId, AstApp.m() != null ? AstApp.m().f() : 2000, "00_002", STConstAction.ACTION_HIT_SEARCH_CANCEL);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onLeftBtnClick() {
        boolean unused = this.f2701a.c = false;
        this.f2701a.c();
        this.f2701a.a(this.pageId, AstApp.m() != null ? AstApp.m().f() : 2000, "00_002", STConstAction.ACTION_HIT_SEARCH_CANCEL);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onRightBtnClick() {
        boolean unused = this.f2701a.c = false;
        this.f2701a.c();
        FunctionUtils.a(this.pageId, this.f2701a.e.fileSize);
        this.f2701a.a(this.pageId, AstApp.m() != null ? AstApp.m().f() : 2000, "00_001", 200);
    }
}
