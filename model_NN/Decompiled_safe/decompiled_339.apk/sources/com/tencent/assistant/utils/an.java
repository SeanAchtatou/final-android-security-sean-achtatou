package com.tencent.assistant.utils;

import android.content.DialogInterface;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class an implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.TwoBtnDialogInfo f2635a;

    an(AppConst.TwoBtnDialogInfo twoBtnDialogInfo) {
        this.f2635a = twoBtnDialogInfo;
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.f2635a.onCancell();
    }
}
