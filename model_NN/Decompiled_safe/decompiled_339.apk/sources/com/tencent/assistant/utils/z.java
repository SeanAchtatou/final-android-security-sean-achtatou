package com.tencent.assistant.utils;

import android.app.Dialog;
import android.view.View;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class z implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.OneBtnDialogInfo f2729a;
    final /* synthetic */ Dialog b;

    z(AppConst.OneBtnDialogInfo oneBtnDialogInfo, Dialog dialog) {
        this.f2729a = oneBtnDialogInfo;
        this.b = dialog;
    }

    public void onClick(View view) {
        if (this.f2729a != null) {
            this.f2729a.onBtnClick();
            this.b.dismiss();
        }
    }
}
