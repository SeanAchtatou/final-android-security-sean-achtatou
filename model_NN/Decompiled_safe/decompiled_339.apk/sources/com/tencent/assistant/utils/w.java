package com.tencent.assistant.utils;

import android.app.Dialog;
import android.view.View;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class w implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.TwoBtnDialogInfo f2726a;
    final /* synthetic */ Dialog b;

    w(AppConst.TwoBtnDialogInfo twoBtnDialogInfo, Dialog dialog) {
        this.f2726a = twoBtnDialogInfo;
        this.b = dialog;
    }

    public void onClick(View view) {
        if (this.f2726a != null) {
            this.f2726a.onLeftBtnClick();
            this.b.dismiss();
        }
    }
}
