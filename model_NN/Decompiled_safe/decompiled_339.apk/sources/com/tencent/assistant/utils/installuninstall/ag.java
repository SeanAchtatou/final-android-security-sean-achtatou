package com.tencent.assistant.utils.installuninstall;

import android.text.TextUtils;
import android.util.Pair;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.module.wisedownload.e;
import com.tencent.assistantv2.st.business.o;
import com.tencent.assistantv2.st.k;

/* compiled from: ProGuard */
class ag extends Thread {

    /* renamed from: a  reason: collision with root package name */
    boolean f2693a = true;
    InstallUninstallTaskBean b;
    InstallUninstallTaskBean c;
    InstallUninstallDialogManager d;
    final /* synthetic */ ac e;

    public ag(ac acVar) {
        this.e = acVar;
        setName("Thread_SilentInstall");
        this.d = new InstallUninstallDialogManager();
        this.d.a(false);
    }

    public void run() {
        byte b2;
        byte b3;
        while (this.f2693a) {
            this.c = null;
            if (this.e.i()) {
                this.e.k();
            }
            this.b = this.e.j();
            this.c = this.b;
            if (this.b != null) {
                try {
                    if (this.b.action == 1) {
                        Pair a2 = this.e.a(this.d, this.b);
                        if (a2 == null || (!((Boolean) a2.first).booleanValue() && ((Boolean) a2.second).booleanValue())) {
                            this.e.b.sendMessage(this.e.b.obtainMessage(1027, this.b));
                        }
                        if (a2 != null && ((Boolean) a2.first).booleanValue()) {
                            ak a3 = InstallUninstallUtil.a(this.b.packageName, this.b.versionCode, this.b.style, this.b.filePath);
                            boolean z = a3.f2696a;
                            this.b.failDesc = a3.b;
                            k.a(this.b.packageName, this.b.versionCode, o.a().a(this.b.style), z ? (byte) 0 : 1, this.b.failDesc);
                            String str = this.b.packageName;
                            int i = this.b.versionCode;
                            byte a4 = o.a().a(this.b.style);
                            if (z) {
                                b2 = 0;
                            } else {
                                b2 = 1;
                            }
                            k.b(str, i, a4, b2, this.b.failDesc);
                            if (this.b.style == 2 && !z && a3.c && (m.a().i() || !m.a().h())) {
                                this.b.style = 1;
                                a3 = InstallUninstallUtil.a(this.b.packageName, this.b.versionCode, this.b.style, this.b.filePath);
                                z = a3.f2696a;
                                this.b.failDesc = a3.b;
                                k.a(this.b.packageName, this.b.versionCode, o.a().a(this.b.style), z ? (byte) 0 : 1, this.b.failDesc);
                                String str2 = this.b.packageName;
                                int i2 = this.b.versionCode;
                                byte a5 = o.a().a(this.b.style);
                                if (z) {
                                    b3 = 0;
                                } else {
                                    b3 = 1;
                                }
                                k.b(str2, i2, a5, b3, this.b.failDesc);
                            }
                            ak akVar = a3;
                            boolean z2 = z;
                            if (!this.b.trySystemAfterSilentFail && z2 && this.b.isImportantVersion) {
                                e eVar = new e();
                                eVar.f1912a = this.b.downloadTicket;
                                eVar.b = this.b.packageName;
                                eVar.c = this.b.versionCode;
                                m.a().a(eVar);
                            }
                            this.e.b.sendMessage(this.e.b.obtainMessage(z2 ? EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC : 1027, this.b));
                            if (!z2) {
                                if (akVar.b.toUpperCase().contains("INSTALL_FAILED_INSUFFICIENT_STORAGE")) {
                                    this.e.a(1, z2, AstApp.i().getResources().getString(R.string.toast_root_install_fail_space_no_enough, this.b.appName), this.b);
                                } else {
                                    this.e.a(1, z2, null, this.b);
                                }
                                if (this.b == null || TextUtils.isEmpty(this.b.filePath) || !this.b.trySystemAfterSilentFail) {
                                    k.a(this.b.packageName, this.b.versionCode, o.a().a(this.b.style), (byte) 1, this.b.failDesc, true);
                                } else {
                                    DownloadProxy.a().c(this.b.downloadTicket).installType = 0;
                                    InstallUninstallUtil.a(this.b.packageName, this.b.versionCode, this.b.filePath);
                                }
                            } else {
                                this.e.a(1, z2, null, this.b);
                                k.a(this.b.packageName, this.b.versionCode, o.a().a(this.b.style), (byte) 0, this.b.failDesc, true);
                            }
                        }
                    }
                } catch (Throwable th) {
                }
            }
        }
    }
}
