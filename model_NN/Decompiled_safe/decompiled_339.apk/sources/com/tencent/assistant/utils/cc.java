package com.tencent.assistant.utils;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.manager.bo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
final class cc extends AppConst.TwoBtnDialogInfo {
    cc() {
    }

    public void onLeftBtnClick() {
        k.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_DIALOG, "03_002", 0, STConst.ST_DEFAULT_SLOT, 200));
        bo.i = true;
    }

    public void onRightBtnClick() {
        bx.b();
        k.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_DIALOG, "03_001", 0, STConst.ST_DEFAULT_SLOT, 200));
        bo.i = true;
    }

    public void onCancell() {
        k.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_DIALOG, "03_002", 0, STConst.ST_DEFAULT_SLOT, 200));
        bo.i = true;
    }
}
