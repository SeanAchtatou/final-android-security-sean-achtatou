package com.tencent.assistant.utils;

import java.text.SimpleDateFormat;

/* compiled from: ProGuard */
final class cz extends ThreadLocal<SimpleDateFormat> {
    cz() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public SimpleDateFormat initialValue() {
        return new SimpleDateFormat("yyyy-MM-dd");
    }
}
