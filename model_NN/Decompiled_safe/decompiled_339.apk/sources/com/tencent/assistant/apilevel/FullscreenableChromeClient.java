package com.tencent.assistant.apilevel;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.ValueCallback;
import android.widget.FrameLayout;
import com.tencent.assistant.js.JsBridge;
import com.tencent.assistant.js.a;
import com.tencent.assistant.link.b;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.r;
import com.tencent.assistantv2.component.eb;
import com.tencent.assistantv2.component.ec;
import com.tencent.smtt.export.external.interfaces.ConsoleMessage;
import com.tencent.smtt.export.external.interfaces.GeolocationPermissionsCallback;
import com.tencent.smtt.export.external.interfaces.IX5WebChromeClient;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebView;
import java.lang.reflect.Method;

/* compiled from: ProGuard */
public class FullscreenableChromeClient extends WebChromeClient {
    private static final FrameLayout.LayoutParams COVER_SCREEN_PARAMS = new FrameLayout.LayoutParams(-1, -1);
    private static final String PARAMS_PRE_ACTIVITY_TAG_NAME = "preActivityTagName";
    protected Activity mActivity = null;
    private ec mClientListener = null;
    private FrameLayout mContentView;
    private Context mContext = null;
    private View mCustomView;
    private IX5WebChromeClient.CustomViewCallback mCustomViewCallback;
    private FrameLayout mFullscreenContainer;
    private JsBridge mJsBridge = null;
    protected eb mListener = null;
    private int mOriginalOrientation;
    private ValueCallback<Uri> mUploadMessage;
    private String url;

    public FullscreenableChromeClient(Context context, JsBridge jsBridge, ec ecVar, eb ebVar) {
        this.mContext = context;
        this.mJsBridge = jsBridge;
        this.mClientListener = ecVar;
        this.mListener = ebVar;
        this.mActivity = ebVar.a();
    }

    public void onProgressChanged(WebView webView, int i) {
        if (this.mListener != null) {
            this.mListener.a(webView, i);
        }
    }

    public void onGeolocationPermissionsShowPrompt(String str, GeolocationPermissionsCallback geolocationPermissionsCallback) {
        geolocationPermissionsCallback.invoke(str, true, false);
    }

    public void onReceivedTitle(WebView webView, String str) {
        if (webView != null) {
            this.url = webView.getUrl();
        }
        if (this.mListener != null) {
            this.mListener.a(webView, str);
        }
        if (webView != null) {
            try {
                Method method = webView.getClass().getMethod("removeJavascriptInterface", String.class);
                if (method != null) {
                    method.invoke(webView, "searchBoxJavaBridge_");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onShowCustomView(View view, IX5WebChromeClient.CustomViewCallback customViewCallback) {
        if (Build.VERSION.SDK_INT < 14) {
            return;
        }
        if (this.mCustomView != null) {
            customViewCallback.onCustomViewHidden();
            return;
        }
        this.mOriginalOrientation = this.mActivity.getRequestedOrientation();
        this.mFullscreenContainer = new a(this.mActivity);
        this.mFullscreenContainer.addView(view, COVER_SCREEN_PARAMS);
        ((FrameLayout) this.mActivity.getWindow().getDecorView()).addView(this.mFullscreenContainer, COVER_SCREEN_PARAMS);
        this.mCustomView = view;
        setFullscreen(true);
        this.mCustomViewCallback = customViewCallback;
    }

    public void onShowCustomView(View view, int i, IX5WebChromeClient.CustomViewCallback customViewCallback) {
        if (Build.VERSION.SDK_INT < 14) {
            return;
        }
        if (this.mCustomView != null) {
            customViewCallback.onCustomViewHidden();
            return;
        }
        this.mOriginalOrientation = this.mActivity.getRequestedOrientation();
        this.mFullscreenContainer = new a(this.mActivity);
        this.mFullscreenContainer.addView(view, COVER_SCREEN_PARAMS);
        ((FrameLayout) this.mActivity.getWindow().getDecorView()).addView(this.mFullscreenContainer, COVER_SCREEN_PARAMS);
        this.mCustomView = view;
        setFullscreen(true);
        this.mCustomViewCallback = customViewCallback;
        this.mActivity.setRequestedOrientation(i);
    }

    public void onHideCustomView() {
        if (this.mCustomView != null) {
            setFullscreen(false);
            ((FrameLayout) this.mActivity.getWindow().getDecorView()).removeView(this.mFullscreenContainer);
            this.mFullscreenContainer = null;
            this.mCustomView = null;
            this.mCustomViewCallback.onCustomViewHidden();
            this.mActivity.setRequestedOrientation(this.mOriginalOrientation);
        }
    }

    private void setFullscreen(boolean z) {
        Window window = this.mActivity.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        if (z) {
            attributes.flags |= 1024;
        } else {
            attributes.flags &= -1025;
            if (this.mCustomView != null) {
                setSystemUiVisibility(this.mCustomView);
            } else {
                setSystemUiVisibility(this.mContentView);
            }
        }
        window.setAttributes(attributes);
    }

    public void openFileChooser(ValueCallback<Uri> valueCallback, String str, String str2) {
        if (a.a().a(this.url)) {
            try {
                this.mUploadMessage = valueCallback;
                Intent intent = new Intent("android.intent.action.GET_CONTENT");
                intent.addCategory("android.intent.category.OPENABLE");
                intent.setType("*/*");
                this.mActivity.startActivityForResult(Intent.createChooser(intent, "完成操作需要使用"), 100);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public ValueCallback<Uri> getUploadMessage() {
        return this.mUploadMessage;
    }

    public void clearUploadMessage() {
        this.mUploadMessage = null;
    }

    public void openFileChooser(ValueCallback<Uri> valueCallback, String str) {
        if (a.a().a(this.url)) {
            try {
                this.mUploadMessage = valueCallback;
                Intent intent = new Intent("android.intent.action.GET_CONTENT");
                intent.addCategory("android.intent.category.OPENABLE");
                intent.setType("*/*");
                this.mActivity.startActivityForResult(Intent.createChooser(intent, "完成操作需要使用"), 100);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void openFileChooser(ValueCallback<Uri> valueCallback) {
        if (a.a().a(this.url)) {
            try {
                this.mUploadMessage = valueCallback;
                Intent intent = new Intent("android.intent.action.GET_CONTENT");
                intent.addCategory("android.intent.category.OPENABLE");
                intent.setType("*/*");
                this.mActivity.startActivityForResult(Intent.createChooser(intent, "完成操作需要使用"), 100);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setSystemUiVisibility(View view) {
        if (r.d() >= 14) {
            try {
                view.getClass().getMethod("setSystemUiVisibility", Integer.TYPE).invoke(view, 0);
            } catch (Exception e) {
            }
        }
    }

    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        XLog.i("Jie", "[onConsoleMessage] ---> 111");
        if (consoleMessage == null) {
            return false;
        }
        return handleConsoleMessage(consoleMessage.message());
    }

    @TargetApi(7)
    public void onConsoleMessage(String str, int i, String str2) {
        XLog.i("Jie", "[onConsoleMessage] ---> 222");
        handleConsoleMessage(str);
    }

    private boolean handleConsoleMessage(String str) {
        boolean z = true;
        XLog.i("Jie", "handleConsoleMessage --- url = " + str);
        if (TextUtils.isEmpty(str)) {
            XLog.i("Jie", "Interface url is empty");
            return false;
        } else if (str.startsWith("http") || str.startsWith("https")) {
            return false;
        } else {
            if (str.startsWith(JsBridge.JS_BRIDGE_SCHEME)) {
                XLog.i("Jie", "Interface request:" + str);
                if (this.mJsBridge != null) {
                    this.mJsBridge.invoke(str);
                }
                return true;
            } else if (str.equals("about:blank;") || str.equals("about:blank")) {
                if (Build.VERSION.SDK_INT >= 11) {
                    z = false;
                }
                return z;
            } else {
                Uri parse = Uri.parse(str);
                Intent intent = new Intent("android.intent.action.VIEW", parse);
                if (!b.a(this.mContext, intent)) {
                    return false;
                }
                String scheme = intent.getScheme();
                if (scheme == null || !scheme.equals("tmast")) {
                    intent.putExtra("preActivityTagName", this.mClientListener.a());
                    this.mContext.startActivity(intent);
                } else {
                    Bundle bundle = new Bundle();
                    int a2 = ct.a(parse.getQueryParameter("scene"), 0);
                    if (a2 != 0) {
                        bundle.putInt("preActivityTagName", a2);
                    } else {
                        bundle.putInt("preActivityTagName", this.mClientListener.a());
                    }
                    b.b(this.mContext, str, bundle);
                }
                return true;
            }
        }
    }
}
