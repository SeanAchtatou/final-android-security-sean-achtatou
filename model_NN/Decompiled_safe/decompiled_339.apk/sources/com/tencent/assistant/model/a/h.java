package com.tencent.assistant.model.a;

import com.tencent.assistant.protocol.jce.HotWordItem;
import com.tencent.assistant.protocol.jce.SmartCardHotWords;
import java.util.List;

/* compiled from: ProGuard */
public class h extends i {

    /* renamed from: a  reason: collision with root package name */
    public List<HotWordItem> f1642a;
    private boolean b = false;

    public boolean a(SmartCardHotWords smartCardHotWords, byte b2) {
        this.i = b2;
        if (smartCardHotWords == null) {
            return false;
        }
        this.k = smartCardHotWords.f2320a;
        this.n = smartCardHotWords.c;
        this.f1642a = smartCardHotWords.b;
        return true;
    }

    public List<HotWordItem> a() {
        return this.f1642a;
    }

    public int b() {
        if (this.f1642a != null) {
            return this.f1642a.size();
        }
        return 0;
    }
}
