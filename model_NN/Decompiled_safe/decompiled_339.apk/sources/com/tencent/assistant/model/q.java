package com.tencent.assistant.model;

import com.tencent.assistant.protocol.jce.FlashInfo;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class q {

    /* renamed from: a  reason: collision with root package name */
    private int f1670a;
    private String b;
    private String c;
    private long d;
    private long e;
    private int f;
    private int g;
    private int h;
    private String i;
    private String j;
    private int k;

    public q() {
    }

    public q(FlashInfo flashInfo) {
        this.f1670a = (int) flashInfo.a();
        this.b = flashInfo.b();
        this.c = flashInfo.c();
        this.d = flashInfo.d();
        this.e = flashInfo.e();
        this.f = flashInfo.f();
        this.g = flashInfo.g();
        this.k = flashInfo.i();
        this.i = flashInfo.h();
        this.j = Constants.STR_EMPTY;
        this.h = 0;
    }

    public int a() {
        return this.f1670a;
    }

    public void a(int i2) {
        this.f1670a = i2;
    }

    public String b() {
        return this.b;
    }

    public void a(String str) {
        this.b = str;
    }

    public String c() {
        return this.c;
    }

    public void b(String str) {
        this.c = str;
    }

    public long d() {
        return this.d;
    }

    public void a(long j2) {
        this.d = j2;
    }

    public long e() {
        return this.e;
    }

    public void b(long j2) {
        this.e = j2;
    }

    public int f() {
        return this.f;
    }

    public void b(int i2) {
        this.f = i2;
    }

    public int g() {
        return this.g;
    }

    public void c(int i2) {
        this.g = i2;
    }

    public int h() {
        return this.h;
    }

    public void d(int i2) {
        this.h = i2;
    }

    public String i() {
        return this.i;
    }

    public void c(String str) {
        this.i = str;
    }

    public String j() {
        return this.j;
    }

    public void d(String str) {
        this.j = str;
    }

    public int k() {
        return this.k;
    }

    public void e(int i2) {
        this.k = i2;
    }

    public int hashCode() {
        return this.f1670a + 31;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (this.f1670a != ((q) obj).f1670a) {
            return false;
        }
        return true;
    }
}
