package com.tencent.assistant.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.tencent.connect.common.Constants;
import com.tencent.wcs.jce.PCInfo;

/* compiled from: ProGuard */
public class OnlinePCListItemModel implements Parcelable {
    public static final Parcelable.Creator<OnlinePCListItemModel> CREATOR = new i();

    /* renamed from: a  reason: collision with root package name */
    public String f1629a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    public int c = 69904;
    public String d = Constants.STR_EMPTY;
    public String e = Constants.STR_EMPTY;

    public OnlinePCListItemModel() {
    }

    public OnlinePCListItemModel(PCInfo pCInfo) {
        a(pCInfo);
    }

    private void a(PCInfo pCInfo) {
        if (pCInfo != null) {
            this.f1629a = pCInfo.f3873a;
            this.b = pCInfo.b;
            this.c = pCInfo.c;
            this.d = pCInfo.d;
            this.e = pCInfo.e;
        }
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f1629a);
        parcel.writeString(this.b);
        parcel.writeInt(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
    }

    public int describeContents() {
        return 0;
    }
}
