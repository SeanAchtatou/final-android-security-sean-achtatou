package com.tencent.assistant.model.a;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.model.SimpleAppModel;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public abstract class a extends i {
    public abstract List<SimpleAppModel> a();

    public abstract boolean a(byte b, JceStruct jceStruct);

    public void c() {
    }

    public s b() {
        return null;
    }

    public List<Long> h() {
        List<SimpleAppModel> a2 = a();
        if (a2 == null || a2.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(a2.size());
        for (SimpleAppModel simpleAppModel : a2) {
            arrayList.add(Long.valueOf(simpleAppModel.f1634a));
        }
        return arrayList;
    }
}
