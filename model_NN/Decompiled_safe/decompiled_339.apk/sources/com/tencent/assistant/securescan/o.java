package com.tencent.assistant.securescan;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.tencent.assistant.manager.cq;

/* compiled from: ProGuard */
class o extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ScanningAnimView f2513a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    o(ScanningAnimView scanningAnimView, Looper looper) {
        super(looper);
        this.f2513a = scanningAnimView;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 11:
                break;
            default:
                return;
        }
        while (this.f2513a.r < 1) {
            if (this.f2513a.m = this.f2513a.a(this.f2513a.a(this.f2513a.s)) == null) {
                ScanningAnimView.c(this.f2513a);
            } else if (!this.f2513a.m.isRecycled()) {
                try {
                    Bitmap unused = this.f2513a.n = this.f2513a.m.copy(this.f2513a.m.getConfig(), true);
                } catch (Throwable th) {
                    cq.a().b();
                }
                int unused2 = this.f2513a.r = 1;
                ScanningAnimView.c(this.f2513a);
                if (this.f2513a.t.booleanValue()) {
                    Message message2 = new Message();
                    message2.what = 10;
                    this.f2513a.y.sendMessage(message2);
                }
            } else {
                return;
            }
        }
    }
}
