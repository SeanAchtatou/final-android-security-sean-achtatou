package com.tencent.d.a;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import com.tencent.connect.common.Constants;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

/* compiled from: ProGuard */
public class f {
    private static Boolean A = true;
    private static ThreadLocal<char[]> B = new g();

    /* renamed from: a  reason: collision with root package name */
    protected static boolean f3575a = true;
    protected static String b = Constants.STR_EMPTY;
    protected static int c = 2;
    protected static int d = 4;
    protected static boolean e = true;
    protected static boolean f = false;
    protected static boolean g = false;
    protected static Context h;
    protected static boolean i = false;
    static LinkedBlockingQueue<String> j;
    protected static BufferedWriter k;
    static long l = 0;
    static final ReentrantLock m = new ReentrantLock();
    protected static int n;
    protected static String o = Constants.STR_EMPTY;
    protected static String p = Constants.STR_EMPTY;
    protected static long q = 0;
    protected static SimpleDateFormat r = new SimpleDateFormat("yy-MM-dd HH:mm");
    protected static AtomicBoolean s = new AtomicBoolean(false);
    protected static AtomicBoolean t = new AtomicBoolean(false);
    protected static final int[] u = {1, 2, 4, 8, 16, 29};
    protected static AtomicInteger v = new AtomicInteger(0);
    protected static Handler w = new Handler(Looper.getMainLooper());
    protected static Runnable x = new h();
    static Thread y = new j();
    private static Field z;

    public static boolean a() {
        return true;
    }

    public static void a(String str, String str2) {
        a(4, str, str2, null);
    }

    public static void b(String str, String str2) {
        a(5, str, str2, null);
    }

    public static void c(String str, String str2) {
        a(6, str, str2, null);
    }

    private static void a(int i2, String str, String str2, Throwable th) {
        if (i2 >= c() || i2 >= b()) {
            if (e()) {
                String methodName = Thread.currentThread().getStackTrace()[4].getMethodName();
                StringBuilder sb = new StringBuilder();
                sb.append(methodName).append("|").append(str2);
                str2 = sb.toString();
            }
            if (i2 >= b()) {
                if (i2 == 2 && a()) {
                    if (th == null) {
                        Log.v(str, str2);
                    } else {
                        Log.v(str, str2, th);
                    }
                }
                if (i2 == 3 && a()) {
                    if (th == null) {
                        Log.d(str, str2);
                    } else {
                        Log.d(str, str2, th);
                    }
                }
                if (i2 == 4) {
                    if (th == null) {
                        Log.i(str, str2);
                    } else {
                        Log.i(str, str2, th);
                    }
                }
                if (i2 == 5) {
                    if (th == null) {
                        Log.w(str, str2);
                    } else {
                        Log.w(str, str2, th);
                    }
                }
                if (i2 == 6) {
                    if (th == null) {
                        Log.e(str, str2);
                    } else {
                        Log.e(str, str2, th);
                    }
                }
            }
            if (i2 >= c()) {
                b(i2, str, str2, th);
            }
        }
    }

    public static int b() {
        return c;
    }

    public static void a(int i2) {
        c = i2;
    }

    public static int c() {
        return d;
    }

    public static void b(int i2) {
        d = i2;
    }

    public static boolean d() {
        return e;
    }

    public static void a(boolean z2) {
        e = z2;
    }

    public static boolean e() {
        return f;
    }

    public static boolean f() {
        return g;
    }

    public static void b(boolean z2) {
        g = z2;
    }

    private static StringBuilder l() {
        StringBuilder sb = new StringBuilder();
        try {
            if (A.booleanValue()) {
                z = StringBuilder.class.getSuperclass().getDeclaredField("value");
                z.setAccessible(true);
                A = false;
            }
            if (z != null) {
                z.set(sb, B.get());
            }
        } catch (Exception e2) {
        }
        return sb;
    }

    public static boolean g() {
        return f3575a;
    }

    public static void c(boolean z2) {
        f3575a = z2;
    }

    public static String h() {
        if (TextUtils.isEmpty(b)) {
            b = String.valueOf(Environment.getExternalStorageDirectory().getPath()) + "/tencent/TMAssistantSDK/Logs/";
        }
        return b;
    }

    public static void a(String str) {
        if (!TextUtils.isEmpty(str) && str.startsWith("/") && str.endsWith("/")) {
            b = str;
        }
    }

    private static boolean c(String str) {
        try {
            j.add(str);
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    private static void b(int i2, String str, String str2, Throwable th) {
        if (g()) {
            long id = Thread.currentThread().getId();
            StringBuilder l2 = l();
            if (f()) {
                l2.append(i()).append("|");
            }
            l2.append(c(i2)).append("|pid=").append(n).append("|tid=").append(String.valueOf(id)).append("|").append(str).append("|").append(str2).append("\n");
            if (th != null) {
                l2 = l2.append("\n").append(Log.getStackTraceString(th)).append("\n");
            }
            if (!c(l2.toString())) {
                Log.d("TMLog", "addLogToCache failed!");
            }
        }
    }

    private static String c(int i2) {
        switch (i2) {
            case 2:
                return "V";
            case 3:
                return "D";
            case 4:
                return "I";
            case 5:
                return "W";
            case 6:
                return "E";
            default:
                return "D";
        }
    }

    public static String i() {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - q > 60000) {
            q = currentTimeMillis;
            p = r.format(Long.valueOf(q));
        }
        return p;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    /* access modifiers changed from: private */
    public static synchronized void m() {
        synchronized (f.class) {
            Log.d("TMLog", "start to init log file!");
            File file = new File(h());
            if (!file.exists()) {
                file.mkdirs();
            }
            File file2 = new File(String.valueOf(h()) + o + "_tmlog.txt");
            try {
                if (!file2.exists()) {
                    boolean createNewFile = file2.createNewFile();
                    n();
                    if (k != null && g()) {
                        k.write(String.valueOf(i()) + "|" + o + "|" + Build.MODEL + " " + Build.VERSION.RELEASE + " create newLogFile " + file2.getName() + " " + createNewFile + "\n");
                        k.flush();
                    }
                } else {
                    if (file2.length() >= 512000) {
                        file2.delete();
                        file2.createNewFile();
                        Log.d("TMLog", "old log file " + file2.getName() + " is deleted");
                    }
                    n();
                    if (k != null && g()) {
                        k.write(String.valueOf(i()) + "|" + o + "|" + Build.MODEL + " " + Build.VERSION.RELEASE + "|newLogFile " + file2.getName() + " is existed.\n");
                        k.flush();
                    }
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
            k = new BufferedWriter(new FileWriter(file2, true), 8192);
            n();
        }
        return;
    }

    /* access modifiers changed from: private */
    public static void d(String str) {
        if (g()) {
            if (k == null) {
                Log.d("TMLog", "can not write log.");
                long currentTimeMillis = System.currentTimeMillis();
                if (l == 0) {
                    l = currentTimeMillis;
                } else if (currentTimeMillis - l > 60000) {
                    try {
                        m();
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                    l = currentTimeMillis;
                }
            } else if (m.tryLock()) {
                try {
                    k.write(str);
                    if (!d()) {
                        k.flush();
                    }
                } finally {
                    m.unlock();
                }
            } else if (!c(str)) {
                Log.d("TMLog", "addLogToCache failed!");
            }
            s.compareAndSet(true, false);
        }
    }

    private static void n() {
    }

    /* access modifiers changed from: private */
    public static void o() {
        if ("mounted".equals(Environment.getExternalStorageState())) {
            Properties properties = new Properties();
            try {
                File file = new File(String.valueOf(Environment.getExternalStorageDirectory().getPath()) + "/tencent/TMAssistantSDK/Logs/logConfig.properties");
                if (file.exists()) {
                    properties.load(new FileInputStream(file));
                    String property = properties.getProperty("isWriteLogToFile", Constants.STR_EMPTY);
                    if (property.trim().equals("true")) {
                        c(true);
                    } else {
                        c(false);
                    }
                    String property2 = properties.getProperty("logfileOutputLevel", Constants.STR_EMPTY);
                    if (!TextUtils.isEmpty(property2)) {
                        try {
                            b(Integer.valueOf(property2).intValue());
                        } catch (NumberFormatException e2) {
                            e2.printStackTrace();
                        }
                    }
                    String property3 = properties.getProperty("logcatOutputLevel", Constants.STR_EMPTY);
                    if (!TextUtils.isEmpty(property3)) {
                        try {
                            a(Integer.valueOf(property3).intValue());
                        } catch (NumberFormatException e3) {
                            e3.printStackTrace();
                        }
                    }
                    String property4 = properties.getProperty("logDirPath", Constants.STR_EMPTY);
                    a(property4);
                    String property5 = properties.getProperty("isUseWriterCache", Constants.STR_EMPTY);
                    if (property5.trim().equals("false")) {
                        a(false);
                    } else {
                        a(true);
                    }
                    if (properties.getProperty("isAppendLogTime", Constants.STR_EMPTY).trim().equals("true")) {
                        b(true);
                    } else {
                        b(false);
                    }
                    Log.d("TMLog", "Properties Local File : isWriteLogToFile = " + property + ", fileLevel = " + property2 + ", logcatLevel = " + property3 + ", dirPath = " + property4 + ", isUseCache = " + property5 + ", isAppendMethodName = " + e() + ", isAppendLogTime = " + f());
                    Log.d("TMLog", "Log Configs : isWriteLogToFile = " + g() + ", fileLevel = " + c() + ", logcatLevel = " + b() + ", dirPath = " + h() + ", isUseCache = " + d() + ", isAppendMethodName = " + e() + ", isAppendLogTime = " + f());
                }
            } catch (IOException e4) {
                e4.printStackTrace();
            }
        } else {
            c(false);
        }
    }
}
