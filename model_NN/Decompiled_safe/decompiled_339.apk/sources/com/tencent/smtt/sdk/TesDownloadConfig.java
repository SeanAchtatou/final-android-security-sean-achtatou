package com.tencent.smtt.sdk;

import android.content.Context;
import android.util.Log;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

class TesDownloadConfig {
    private static final String LOGTAG = "TesDownloadConfig";
    private static final String TES_CFG_FILE = "tes_download_config.prop";
    private static final String TES_CFG_PATH = "tes_private";
    public static final long TES_CONFIG_CHECK_PERIOD = 86400000;
    private static TesDownloadConfig mInstance;
    public String mAppMetadata;
    public int mAppVersionCode;
    public String mAppVersionName;
    private File mConfigFile;
    public String mDownloadUrl = null;
    public long mLastCheckTime;
    public int mLastError = 0;
    public boolean mLastSecuritySwitch = false;
    public String mLastUrl;
    public boolean mNeedDownload = false;
    private int mResponseCode = 0;
    public Map<String, Object> mSyncMap = new HashMap();
    public int mTesVersion;
    private boolean mX5Disabled = false;

    public interface TesConfigKey {
        public static final String KEY_APP_METADATA = "app_metadata";
        public static final String KEY_APP_VERSIONCODE = "app_versioncode";
        public static final String KEY_APP_VERSIONNAME = "app_versionname";
        public static final String KEY_LASTERR = "tes_lasterror";
        public static final String KEY_LASTURL = "tes_lasturl";
        public static final String KEY_LAST_CHECK = "last_check";
        public static final String KEY_NEEDDOWNLOAD = "tes_needdownload";
        public static final String KEY_RESPONSECODE = "tes_responsecode";
        public static final String KEY_SECURITY_SWITCH = "security_switch";
        public static final String KEY_TESDOWNLOADURL = "tes_downloadurl";
        public static final String KEY_TESV = "tes_version";
        public static final String KEY_X5_DISABLED = "x5_disabled";
    }

    private TesDownloadConfig() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0148 A[SYNTHETIC, Splitter:B:20:0x0148] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x016f A[SYNTHETIC, Splitter:B:26:0x016f] */
    /* JADX WARNING: Removed duplicated region for block: B:38:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void loadConfig(android.content.Context r10) {
        /*
            r9 = this;
            java.lang.String r5 = "TesDownloadConfig"
            java.lang.String r6 = "loadConfig"
            android.util.Log.e(r5, r6)
            android.content.Context r5 = r10.getApplicationContext()
            java.lang.String r6 = "tes_private"
            r7 = 0
            java.io.File r0 = r5.getDir(r6, r7)
            java.io.File r5 = new java.io.File
            java.lang.String r6 = "tes_download_config.prop"
            r5.<init>(r0, r6)
            r9.mConfigFile = r5
            r2 = 0
            java.io.File r5 = r9.mConfigFile     // Catch:{ Exception -> 0x0129 }
            boolean r5 = r5.exists()     // Catch:{ Exception -> 0x0129 }
            if (r5 != 0) goto L_0x0036
            java.lang.String r5 = "TesDownloadConfig"
            java.lang.String r6 = "mConfigFile.createNewFile..."
            android.util.Log.e(r5, r6)     // Catch:{ Exception -> 0x0129 }
            java.io.File r5 = r9.mConfigFile     // Catch:{ Exception -> 0x0129 }
            r5.createNewFile()     // Catch:{ Exception -> 0x0129 }
        L_0x0030:
            if (r2 == 0) goto L_0x0035
            r2.close()     // Catch:{ IOException -> 0x010a }
        L_0x0035:
            return
        L_0x0036:
            java.util.Properties r4 = new java.util.Properties     // Catch:{ Exception -> 0x0129 }
            r4.<init>()     // Catch:{ Exception -> 0x0129 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0129 }
            java.io.File r5 = r9.mConfigFile     // Catch:{ Exception -> 0x0129 }
            r3.<init>(r5)     // Catch:{ Exception -> 0x0129 }
            r4.load(r3)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            java.lang.String r5 = "last_check"
            java.lang.String r6 = "0"
            java.lang.String r5 = r4.getProperty(r5, r6)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            long r5 = java.lang.Long.parseLong(r5)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            r9.mLastCheckTime = r5     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            java.lang.String r5 = "x5_disabled"
            java.lang.String r5 = r4.getProperty(r5)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            boolean r5 = java.lang.Boolean.parseBoolean(r5)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            r9.mX5Disabled = r5     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            java.lang.String r5 = "tes_version"
            java.lang.String r6 = "0"
            java.lang.String r5 = r4.getProperty(r5, r6)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            int r5 = java.lang.Integer.parseInt(r5)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            r9.mTesVersion = r5     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            java.lang.String r5 = "tes_needdownload"
            java.lang.String r5 = r4.getProperty(r5)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            boolean r5 = java.lang.Boolean.parseBoolean(r5)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            r9.mNeedDownload = r5     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            java.lang.String r5 = "tes_downloadurl"
            java.lang.String r6 = "null"
            java.lang.String r5 = r4.getProperty(r5, r6)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            r9.mDownloadUrl = r5     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            java.lang.String r5 = "tes_responsecode"
            java.lang.String r6 = "0"
            java.lang.String r5 = r4.getProperty(r5, r6)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            int r5 = java.lang.Integer.parseInt(r5)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            r9.mResponseCode = r5     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            java.lang.String r5 = "tes_lasturl"
            java.lang.String r6 = "null"
            java.lang.String r5 = r4.getProperty(r5, r6)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            r9.mLastUrl = r5     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            java.lang.String r5 = "tes_lasterror"
            java.lang.String r6 = "0"
            java.lang.String r5 = r4.getProperty(r5, r6)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            int r5 = java.lang.Integer.parseInt(r5)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            r9.mLastError = r5     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            java.lang.String r5 = "app_versionname"
            java.lang.String r6 = "null"
            java.lang.String r5 = r4.getProperty(r5, r6)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            r9.mAppVersionName = r5     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            java.lang.String r5 = "app_versioncode"
            java.lang.String r6 = "0"
            java.lang.String r5 = r4.getProperty(r5, r6)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            int r5 = java.lang.Integer.parseInt(r5)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            r9.mAppVersionCode = r5     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            java.lang.String r5 = "app_metadata"
            java.lang.String r6 = "null"
            java.lang.String r5 = r4.getProperty(r5, r6)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            r9.mAppMetadata = r5     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            java.lang.String r5 = "security_switch"
            java.lang.String r6 = "false"
            java.lang.String r5 = r4.getProperty(r5, r6)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            boolean r5 = java.lang.Boolean.parseBoolean(r5)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            r9.mLastSecuritySwitch = r5     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            java.lang.String r5 = "TesDownloadConfig"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            r6.<init>()     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            java.lang.String r7 = "mLastCheckTime:"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            java.lang.String r7 = "last_check"
            java.lang.String r7 = r4.getProperty(r7)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            java.lang.String r7 = "; mLastSecuritySwitch:"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            java.lang.String r7 = "security_switch"
            java.lang.String r7 = r4.getProperty(r7)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            android.util.Log.e(r5, r6)     // Catch:{ Exception -> 0x0194, all -> 0x0191 }
            r2 = r3
            goto L_0x0030
        L_0x010a:
            r1 = move-exception
            java.lang.String r5 = "TesDownloadConfig"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "exception2:"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = r1.getMessage()
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            android.util.Log.e(r5, r6)
            goto L_0x0035
        L_0x0129:
            r1 = move-exception
        L_0x012a:
            java.lang.String r5 = "TesDownloadConfig"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x016c }
            r6.<init>()     // Catch:{ all -> 0x016c }
            java.lang.String r7 = "exception1:"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x016c }
            java.lang.String r7 = r1.getMessage()     // Catch:{ all -> 0x016c }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x016c }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x016c }
            android.util.Log.e(r5, r6)     // Catch:{ all -> 0x016c }
            if (r2 == 0) goto L_0x0035
            r2.close()     // Catch:{ IOException -> 0x014d }
            goto L_0x0035
        L_0x014d:
            r1 = move-exception
            java.lang.String r5 = "TesDownloadConfig"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "exception2:"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = r1.getMessage()
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            android.util.Log.e(r5, r6)
            goto L_0x0035
        L_0x016c:
            r5 = move-exception
        L_0x016d:
            if (r2 == 0) goto L_0x0172
            r2.close()     // Catch:{ IOException -> 0x0173 }
        L_0x0172:
            throw r5
        L_0x0173:
            r1 = move-exception
            java.lang.String r6 = "TesDownloadConfig"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "exception2:"
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = r1.getMessage()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            android.util.Log.e(r6, r7)
            goto L_0x0172
        L_0x0191:
            r5 = move-exception
            r2 = r3
            goto L_0x016d
        L_0x0194:
            r1 = move-exception
            r2 = r3
            goto L_0x012a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.smtt.sdk.TesDownloadConfig.loadConfig(android.content.Context):void");
    }

    public static synchronized TesDownloadConfig getInstance(Context context) {
        TesDownloadConfig tesDownloadConfig;
        synchronized (TesDownloadConfig.class) {
            if (mInstance == null) {
                Log.e(LOGTAG, "TesDownloadConfig getInstance");
                mInstance = new TesDownloadConfig();
                mInstance.loadConfig(context);
            }
            tesDownloadConfig = mInstance;
        }
        return tesDownloadConfig;
    }

    public static synchronized TesDownloadConfig getInstance() {
        TesDownloadConfig tesDownloadConfig;
        synchronized (TesDownloadConfig.class) {
            tesDownloadConfig = mInstance;
        }
        return tesDownloadConfig;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:56:0x008e, code lost:
        r9 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0045 A[SYNTHETIC, Splitter:B:14:0x0045] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004a A[SYNTHETIC, Splitter:B:17:0x004a] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x006c A[SYNTHETIC, Splitter:B:34:0x006c] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0071 A[SYNTHETIC, Splitter:B:37:0x0071] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x008e A[ExcHandler: all (th java.lang.Throwable), Splitter:B:24:0x005a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void commit() {
        /*
            r11 = this;
            monitor-enter(r11)
            r2 = 0
            r0 = 0
            java.util.Properties r7 = new java.util.Properties     // Catch:{ Exception -> 0x0089, all -> 0x0069 }
            r7.<init>()     // Catch:{ Exception -> 0x0089, all -> 0x0069 }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0089, all -> 0x0069 }
            java.io.File r9 = r11.mConfigFile     // Catch:{ Exception -> 0x0089, all -> 0x0069 }
            r1.<init>(r9)     // Catch:{ Exception -> 0x0089, all -> 0x0069 }
            r7.load(r1)     // Catch:{ Exception -> 0x008b, all -> 0x0082 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x008b, all -> 0x0082 }
            java.io.File r9 = r11.mConfigFile     // Catch:{ Exception -> 0x008b, all -> 0x0082 }
            r3.<init>(r9)     // Catch:{ Exception -> 0x008b, all -> 0x0082 }
            java.util.Map<java.lang.String, java.lang.Object> r9 = r11.mSyncMap     // Catch:{ Exception -> 0x0040, all -> 0x0085 }
            java.util.Set r6 = r9.keySet()     // Catch:{ Exception -> 0x0040, all -> 0x0085 }
            java.util.Iterator r4 = r6.iterator()     // Catch:{ Exception -> 0x0040, all -> 0x0085 }
        L_0x0023:
            boolean r9 = r4.hasNext()     // Catch:{ Exception -> 0x0040, all -> 0x0085 }
            if (r9 == 0) goto L_0x004f
            java.lang.Object r5 = r4.next()     // Catch:{ Exception -> 0x0040, all -> 0x0085 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ Exception -> 0x0040, all -> 0x0085 }
            java.util.Map<java.lang.String, java.lang.Object> r9 = r11.mSyncMap     // Catch:{ Exception -> 0x0040, all -> 0x0085 }
            java.lang.Object r8 = r9.get(r5)     // Catch:{ Exception -> 0x0040, all -> 0x0085 }
            r11.syncMemory(r5, r8)     // Catch:{ Exception -> 0x0040, all -> 0x0085 }
            java.lang.String r9 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x0040, all -> 0x0085 }
            r7.setProperty(r5, r9)     // Catch:{ Exception -> 0x0040, all -> 0x0085 }
            goto L_0x0023
        L_0x0040:
            r9 = move-exception
            r0 = r1
            r2 = r3
        L_0x0043:
            if (r0 == 0) goto L_0x0048
            r0.close()     // Catch:{ IOException -> 0x007a }
        L_0x0048:
            if (r2 == 0) goto L_0x004d
            r2.close()     // Catch:{ IOException -> 0x007c }
        L_0x004d:
            monitor-exit(r11)
            return
        L_0x004f:
            java.util.Map<java.lang.String, java.lang.Object> r9 = r11.mSyncMap     // Catch:{ Exception -> 0x0040, all -> 0x0085 }
            r9.clear()     // Catch:{ Exception -> 0x0040, all -> 0x0085 }
            r9 = 0
            r7.store(r3, r9)     // Catch:{ Exception -> 0x0040, all -> 0x0085 }
            if (r1 == 0) goto L_0x005d
            r1.close()     // Catch:{ IOException -> 0x0078, all -> 0x008e }
        L_0x005d:
            if (r3 == 0) goto L_0x0092
            r3.close()     // Catch:{ IOException -> 0x0065, all -> 0x008e }
            r0 = r1
            r2 = r3
            goto L_0x004d
        L_0x0065:
            r9 = move-exception
            r0 = r1
            r2 = r3
            goto L_0x004d
        L_0x0069:
            r9 = move-exception
        L_0x006a:
            if (r0 == 0) goto L_0x006f
            r0.close()     // Catch:{ IOException -> 0x007e }
        L_0x006f:
            if (r2 == 0) goto L_0x0074
            r2.close()     // Catch:{ IOException -> 0x0080 }
        L_0x0074:
            throw r9     // Catch:{ all -> 0x0075 }
        L_0x0075:
            r9 = move-exception
        L_0x0076:
            monitor-exit(r11)
            throw r9
        L_0x0078:
            r9 = move-exception
            goto L_0x005d
        L_0x007a:
            r9 = move-exception
            goto L_0x0048
        L_0x007c:
            r9 = move-exception
            goto L_0x004d
        L_0x007e:
            r10 = move-exception
            goto L_0x006f
        L_0x0080:
            r10 = move-exception
            goto L_0x0074
        L_0x0082:
            r9 = move-exception
            r0 = r1
            goto L_0x006a
        L_0x0085:
            r9 = move-exception
            r0 = r1
            r2 = r3
            goto L_0x006a
        L_0x0089:
            r9 = move-exception
            goto L_0x0043
        L_0x008b:
            r9 = move-exception
            r0 = r1
            goto L_0x0043
        L_0x008e:
            r9 = move-exception
            r0 = r1
            r2 = r3
            goto L_0x0076
        L_0x0092:
            r0 = r1
            r2 = r3
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.smtt.sdk.TesDownloadConfig.commit():void");
    }

    private void syncMemory(String key, Object value) {
        if (TesConfigKey.KEY_LAST_CHECK.equals(key)) {
            this.mLastCheckTime = ((Long) value).longValue();
        } else if (TesConfigKey.KEY_X5_DISABLED.equals(key)) {
            this.mX5Disabled = ((Boolean) value).booleanValue();
        } else if (TesConfigKey.KEY_TESV.equals(key)) {
            this.mTesVersion = ((Integer) value).intValue();
        } else if (TesConfigKey.KEY_NEEDDOWNLOAD.equals(key)) {
            this.mNeedDownload = ((Boolean) value).booleanValue();
        } else if (TesConfigKey.KEY_TESDOWNLOADURL.equals(key)) {
            this.mDownloadUrl = (String) value;
        } else if (TesConfigKey.KEY_RESPONSECODE.equals(key)) {
            this.mResponseCode = ((Integer) value).intValue();
        } else if (TesConfigKey.KEY_LASTURL.equals(key)) {
            this.mLastUrl = (String) value;
        } else if (TesConfigKey.KEY_LASTERR.equals(key)) {
            this.mLastError = ((Integer) value).intValue();
        } else if (TesConfigKey.KEY_APP_VERSIONNAME.equals(key)) {
            this.mAppVersionName = (String) value;
        } else if (TesConfigKey.KEY_APP_VERSIONCODE.equals(key)) {
            this.mAppVersionCode = ((Integer) value).intValue();
        } else if (TesConfigKey.KEY_APP_METADATA.equals(key)) {
            this.mAppMetadata = (String) value;
        } else if (TesConfigKey.KEY_SECURITY_SWITCH.equals(key)) {
            this.mLastSecuritySwitch = ((Boolean) value).booleanValue();
        }
    }
}
