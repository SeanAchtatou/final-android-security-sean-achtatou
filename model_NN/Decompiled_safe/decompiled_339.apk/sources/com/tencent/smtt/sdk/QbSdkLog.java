package com.tencent.smtt.sdk;

public class QbSdkLog {
    private static QbSdkLogClient mQbSdkLogClient = null;

    public static void i(String tag, String msg) {
        mQbSdkLogClient.i(tag, "QB_SDK:" + msg);
    }

    public static void e(String tag, String msg) {
        mQbSdkLogClient.e(tag, "QB_SDK:" + msg);
    }

    public static void w(String tag, String msg) {
        mQbSdkLogClient.w(tag, "QB_SDK:" + msg);
    }

    public static void d(String tag, String msg) {
        mQbSdkLogClient.d(tag, "QB_SDK:" + msg);
    }

    public static void v(String tag, String msg) {
        mQbSdkLogClient.v(tag, "QB_SDK:" + msg);
    }

    static {
        if (mQbSdkLogClient == null) {
            setQbSdkLogClient(new QbSdkLogClient());
        }
    }

    public static boolean setQbSdkLogClient(QbSdkLogClient qbsdklogclient) {
        if (qbsdklogclient == null) {
            return false;
        }
        mQbSdkLogClient = qbsdklogclient;
        return true;
    }
}
