package com.tencent.smtt.sdk;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.ComponentCallbacks;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.Rect;
import android.net.http.SslCertificate;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.DownloadListener;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.tencent.connect.common.Constants;
import com.tencent.smtt.export.external.extension.interfaces.IX5WebChromeClientExtension;
import com.tencent.smtt.export.external.extension.interfaces.IX5WebSettingsExtension;
import com.tencent.smtt.export.external.extension.interfaces.IX5WebViewClientExtension;
import com.tencent.smtt.export.external.extension.interfaces.IX5WebViewExtension;
import com.tencent.smtt.export.external.extension.proxy.X5ProxyWebViewClientExtension;
import com.tencent.smtt.export.external.interfaces.IX5WebViewBase;
import com.tencent.smtt.sdk.stat.HttpUtils;
import com.tencent.smtt.sdk.stat.MttLoader;
import com.tencent.smtt.utils.ReflectionUtils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

public class WebView extends FrameLayout {
    private static final String LOGTAG = "WebView";
    public static int NIGHT_MODE_ALPHA = 153;
    public static final int NIGHT_MODE_COLOR = -16777216;
    public static final int NORMAL_MODE_ALPHA = 255;
    public static final String SCHEME_GEO = "geo:0,0?q=";
    public static final String SCHEME_MAILTO = "mailto:";
    public static final String SCHEME_TEL = "tel:";
    private static int X5Version = 0;
    /* access modifiers changed from: private */
    public static boolean mIsDayMode = true;
    /* access modifiers changed from: private */
    public static Paint mNightModePaint = null;
    /* access modifiers changed from: private */
    public static int mSecureLibraryStatus = 0;
    private static boolean mShutDownSplashLogo = true;
    private static FrameLayout.LayoutParams mSysSplashLogoLayoutParams = null;
    private static FrameLayout.LayoutParams mX5SplashLogoLayoutParams = null;
    private static Bitmap mbmpSysSplashLogo = null;
    private static Bitmap mbmpX5SplashLogo = null;
    private static Method sWebviewsetWebContentsDebuggingEnabled = null;
    private final String deleteNightMode;
    private boolean isX5Core;
    private Object longClickListener;
    /* access modifiers changed from: private */
    public Context mContext;
    private boolean mIsReported;
    int mPv;
    ImageView mSplashLogo;
    private SystemWebView mSysWebView;
    private WebSettings mWebSettings;
    private IX5WebViewBase mX5WebView;
    private final String nightMode;

    public interface PictureListener {
        void onNewPicture(WebView webView, Picture picture);
    }

    public class WebViewTransport {
        private WebView mWebview;

        public WebViewTransport() {
        }

        public synchronized void setWebView(WebView webview) {
            this.mWebview = webview;
        }

        public synchronized WebView getWebView() {
            return this.mWebview;
        }
    }

    public static class HitTestResult {
        @Deprecated
        public static final int ANCHOR_TYPE = 1;
        public static final int EDIT_TEXT_TYPE = 9;
        public static final int EMAIL_TYPE = 4;
        public static final int GEO_TYPE = 3;
        @Deprecated
        public static final int IMAGE_ANCHOR_TYPE = 6;
        public static final int IMAGE_TYPE = 5;
        public static final int PHONE_TYPE = 2;
        public static final int SRC_ANCHOR_TYPE = 7;
        public static final int SRC_IMAGE_ANCHOR_TYPE = 8;
        public static final int UNKNOWN_TYPE = 0;
        private IX5WebViewBase.HitTestResult mHitTestResultImpl;
        private WebView.HitTestResult mSysHitTestResult;

        public HitTestResult() {
            this.mSysHitTestResult = null;
            this.mHitTestResultImpl = null;
            this.mSysHitTestResult = null;
        }

        public HitTestResult(IX5WebViewBase.HitTestResult hitTestResult) {
            this.mSysHitTestResult = null;
            this.mHitTestResultImpl = hitTestResult;
            this.mSysHitTestResult = null;
        }

        public HitTestResult(WebView.HitTestResult hitTestResult) {
            this.mSysHitTestResult = null;
            this.mHitTestResultImpl = null;
            this.mSysHitTestResult = hitTestResult;
        }

        public void setType(int type) {
            if (this.mHitTestResultImpl != null) {
                this.mHitTestResultImpl.setType(type);
            }
        }

        public void setExtra(String extra) {
            if (this.mHitTestResultImpl != null) {
                this.mHitTestResultImpl.setExtra(extra);
            }
        }

        public int getType() {
            if (this.mHitTestResultImpl != null) {
                return this.mHitTestResultImpl.getType();
            }
            if (this.mSysHitTestResult != null) {
                return this.mSysHitTestResult.getType();
            }
            return 0;
        }

        public String getExtra() {
            if (this.mHitTestResultImpl != null) {
                return this.mHitTestResultImpl.getExtra();
            }
            if (this.mSysHitTestResult != null) {
                return this.mSysHitTestResult.getExtra();
            }
            return Constants.STR_EMPTY;
        }
    }

    public WebView(Context context) {
        this(context, null);
    }

    public WebView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WebView(Context context, AttributeSet attrs, int defStyle) {
        this(context, attrs, defStyle, false);
    }

    public WebView(Context context, AttributeSet attrs, int defStyle, boolean privateBrowsing) {
        this(context, attrs, defStyle, null, privateBrowsing);
        QbSdkLog.e(LOGTAG, "isSecurityApplyed:" + SecuritySwitch.isSecurityApplyed());
    }

    public WebView(Context context, AttributeSet attrs, int defStyle, Map<String, Object> map, boolean privateBrowsing) {
        super(context, attrs, defStyle);
        this.isX5Core = false;
        this.mWebSettings = null;
        this.mContext = null;
        this.mPv = 0;
        this.mIsReported = false;
        this.deleteNightMode = "javascript:document.getElementsByTagName('HEAD').item(0).removeChild(document.getElementById('QQBrowserSDKNightMode'));";
        this.nightMode = "javascript:var style = document.createElement('style');style.type='text/css';style.id='QQBrowserSDKNightMode';style.innerHTML='html,body{background:none !important;background-color: #1d1e2a !important;}html *{background-color: #1d1e2a !important; color:#888888 !important;border-color:#3e4f61 !important;text-shadow:none !important;box-shadow:none !important;}a,a *{border-color:#4c5b99 !important; color:#2d69b3 !important;text-decoration:none !important;}a:visited,a:visited *{color:#a600a6 !important;}a:active,a:active *{color:#5588AA !important;}input,select,textarea,option,button{background-image:none !important;color:#AAAAAA !important;border-color:#4c5b99 !important;}form,div,button,span{background-color:#1d1e2a !important; border-color:#4c5b99 !important;}img{opacity:0.5}';document.getElementsByTagName('HEAD').item(0).appendChild(style);";
        this.longClickListener = null;
        if (context == null) {
            throw new IllegalArgumentException("Invalid context argument");
        }
        init(context);
        this.mContext = context;
        if (this.isX5Core) {
            this.mX5WebView = SDKEngine.getInstance(true).wizard().createSDKWebview(context);
            if (this.mX5WebView == null || this.mX5WebView.getView() == null) {
                QbSdkLog.e("QbSdk", "sys WebView: failed to createSDKWebview");
                this.mX5WebView = null;
                this.isX5Core = false;
                QbSdk.forceSysWebViewInner();
                init(context);
                this.mSysWebView = new SystemWebView(context);
                this.mSysWebView.setFocusableInTouchMode(true);
                addView(this.mSysWebView, new FrameLayout.LayoutParams(-1, -1));
                try {
                    if (Build.VERSION.SDK_INT >= 11) {
                        removeJavascriptInterface("searchBoxJavaBridge_");
                        return;
                    }
                    return;
                } catch (Throwable thr) {
                    thr.printStackTrace();
                    return;
                }
            } else {
                this.mX5WebView.getView().setFocusableInTouchMode(true);
                addView(this.mX5WebView.getView(), new FrameLayout.LayoutParams(-1, -1));
                if (!mShutDownSplashLogo) {
                    showSplashLogo(this.isX5Core);
                }
                this.mX5WebView.setDownloadListener(new DownLoadListenerAdapter(this, null, this.isX5Core));
                this.mX5WebView.getX5WebViewExtension().setWebViewClientExtension(new X5ProxyWebViewClientExtension(SDKEngine.getInstance(false).wizard()) {
                    public void onScrollChanged(int oldx, int oldy, int newx, int newy) {
                        super.onScrollChanged(oldx, oldy, newx, newy);
                        WebView.this.onScrollChanged(newx, newy, oldx, oldy);
                    }
                });
                if (X5Version == 0) {
                    X5Version = this.mX5WebView.getX5WebViewExtension().getQQBrowserVersion();
                }
            }
        } else {
            this.mSysWebView = new SystemWebView(context);
            this.mSysWebView.setFocusableInTouchMode(true);
            addView(this.mSysWebView, new FrameLayout.LayoutParams(-1, -1));
            setDownloadListener(null);
            if (!mShutDownSplashLogo) {
                showSplashLogo(this.isX5Core);
            }
        }
        try {
            if (Build.VERSION.SDK_INT >= 11) {
                removeJavascriptInterface("searchBoxJavaBridge_");
            }
        } catch (Throwable thr2) {
            thr2.printStackTrace();
        }
    }

    private void showSplashLogo(boolean isX5) {
        FrameLayout.LayoutParams layoutParams;
        Bitmap bitmap = null;
        if (isX5) {
            try {
                if (mbmpX5SplashLogo == null) {
                    Context context = getContext().createPackageContext(SDKEngine.X5QQBROWSER_PKG_NAME, 2);
                    try {
                        bitmap = BitmapFactory.decodeResource(context.getResources(), context.getResources().getIdentifier("thrdcall_window_bg_normal", "drawable", SDKEngine.X5QQBROWSER_PKG_NAME), null);
                    } catch (OutOfMemoryError error) {
                        error.printStackTrace();
                    }
                } else {
                    bitmap = mbmpX5SplashLogo;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        } else if (mbmpSysSplashLogo != null) {
            bitmap = mbmpSysSplashLogo;
        }
        if (bitmap != null) {
            this.mSplashLogo = new ImageView(getContext());
            this.mSplashLogo.setImageBitmap(bitmap);
            if (isX5) {
                layoutParams = mX5SplashLogoLayoutParams;
            } else {
                layoutParams = mSysSplashLogoLayoutParams;
            }
            if (layoutParams == null) {
                layoutParams = new FrameLayout.LayoutParams(-2, -2);
                layoutParams.gravity = 17;
            }
            addView(this.mSplashLogo, layoutParams);
        }
    }

    public static void switchLogo(Context context, boolean isShutDown) {
        if (context != null) {
            try {
                String packageName = context.getPackageName();
                if (packageName.indexOf("com.tencent.mm") >= 0 || packageName.indexOf("com.tencent.mobileqq") >= 0) {
                    mShutDownSplashLogo = isShutDown;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void setSplashLogo(boolean isX5Logo, Bitmap bmp) {
        if (bmp != null) {
            if (isX5Logo) {
                mbmpX5SplashLogo = bmp;
            } else {
                mbmpSysSplashLogo = bmp;
            }
        }
    }

    public static void setSplashLogo(boolean isX5Logo, Bitmap bmp, FrameLayout.LayoutParams lp) {
        if (isX5Logo) {
            mX5SplashLogoLayoutParams = lp;
        } else {
            mSysSplashLogoLayoutParams = lp;
        }
        setSplashLogo(isX5Logo, bmp);
    }

    public void hideSplashLogo() {
        if (this.mSplashLogo != null && this.mSplashLogo.getVisibility() == 0) {
            this.mSplashLogo.setVisibility(8);
            this.mSplashLogo = null;
        }
    }

    private void init(Context context) {
        SDKEngine sdkEngine = SDKEngine.getInstance(true);
        sdkEngine.init(context);
        this.isX5Core = sdkEngine.isX5Core();
    }

    public void setScrollBarStyle(int style) {
        if (this.isX5Core) {
            this.mX5WebView.getView().setScrollBarStyle(style);
        } else {
            this.mSysWebView.setScrollBarStyle(style);
        }
    }

    public void setHorizontalScrollbarOverlay(boolean overlay) {
        if (!this.isX5Core) {
            this.mSysWebView.setHorizontalScrollbarOverlay(overlay);
        } else {
            this.mX5WebView.setHorizontalScrollbarOverlay(overlay);
        }
    }

    public void setVerticalScrollbarOverlay(boolean overlay) {
        if (!this.isX5Core) {
            this.mSysWebView.setVerticalScrollbarOverlay(overlay);
        } else {
            this.mX5WebView.setVerticalScrollbarOverlay(overlay);
        }
    }

    public boolean overlayHorizontalScrollbar() {
        if (!this.isX5Core) {
            return this.mSysWebView.overlayHorizontalScrollbar();
        }
        return this.mX5WebView.overlayHorizontalScrollbar();
    }

    public boolean overlayVerticalScrollbar() {
        if (this.isX5Core) {
            return this.mX5WebView.overlayVerticalScrollbar();
        }
        return this.mSysWebView.overlayVerticalScrollbar();
    }

    public boolean requestChildRectangleOnScreen(View child, Rect rectangle, boolean immediate) {
        if (this.isX5Core) {
            View v = this.mX5WebView.getView();
            if (!(v instanceof ViewGroup)) {
                return false;
            }
            ViewGroup viewGroup = (ViewGroup) v;
            if (child != this) {
                v = child;
            }
            return viewGroup.requestChildRectangleOnScreen(v, rectangle, immediate);
        }
        SystemWebView systemWebView = this.mSysWebView;
        if (child == this) {
            child = this.mSysWebView;
        }
        return systemWebView.requestChildRectangleOnScreen(child, rectangle, immediate);
    }

    public int getWebScrollX() {
        if (this.isX5Core) {
            return this.mX5WebView.getView().getScrollX();
        }
        return this.mSysWebView.getScrollX();
    }

    public int getWebScrollY() {
        if (this.isX5Core) {
            return this.mX5WebView.getView().getScrollY();
        }
        return this.mSysWebView.getScrollY();
    }

    public int getVisibleTitleHeight() {
        if (this.isX5Core) {
            return this.mX5WebView.getVisibleTitleHeight();
        }
        Object ret = ReflectionUtils.invokeInstance(this.mSysWebView, "getVisibleTitleHeight");
        if (ret == null) {
            return 0;
        }
        return ((Integer) ret).intValue();
    }

    public SslCertificate getCertificate() {
        if (!this.isX5Core) {
            return this.mSysWebView.getCertificate();
        }
        return this.mX5WebView.getCertificate();
    }

    @Deprecated
    public void setCertificate(SslCertificate obj) {
        if (!this.isX5Core) {
            this.mSysWebView.setCertificate(obj);
        } else {
            this.mX5WebView.setCertificate(obj);
        }
    }

    @Deprecated
    public void savePassword(String host, String username, String password) {
        if (!this.isX5Core) {
            ReflectionUtils.invokeInstance(this.mSysWebView, "savePassword", new Class[]{String.class, String.class, String.class}, host, username, password);
            return;
        }
        this.mX5WebView.savePassword(host, username, password);
    }

    public void setHttpAuthUsernamePassword(String host, String realm, String username, String password) {
        if (!this.isX5Core) {
            this.mSysWebView.setHttpAuthUsernamePassword(host, realm, username, password);
        } else {
            this.mX5WebView.setHttpAuthUsernamePassword(host, realm, username, password);
        }
    }

    public String[] getHttpAuthUsernamePassword(String host, String realm) {
        if (!this.isX5Core) {
            return this.mSysWebView.getHttpAuthUsernamePassword(host, realm);
        }
        return this.mX5WebView.getHttpAuthUsernamePassword(host, realm);
    }

    public void destroy() {
        Bundle bundle;
        if (!this.mIsReported) {
            this.mIsReported = true;
            String guid = Constants.STR_EMPTY;
            String qua = Constants.STR_EMPTY;
            String lc = Constants.STR_EMPTY;
            if (this.isX5Core && (bundle = this.mX5WebView.getX5WebViewExtension().getSdkQBStatisticsInfo()) != null) {
                guid = bundle.getString("guid");
                qua = bundle.getString("qua");
                lc = bundle.getString("lc");
            }
            HttpUtils.doReport(this.mContext, guid, qua, lc, this.mPv, this.isX5Core);
        }
        if (!this.isX5Core) {
            try {
                Class<?> classic = Class.forName("android.webkit.WebViewClassic");
                Method method = classic.getMethod("fromWebView", android.webkit.WebView.class);
                method.setAccessible(true);
                Object obj = method.invoke(null, this.mSysWebView);
                if (obj != null) {
                    Field f = classic.getDeclaredField("mListBoxDialog");
                    f.setAccessible(true);
                    Object obj2 = f.get(obj);
                    if (obj2 != null) {
                        Dialog dialog = (Dialog) obj2;
                        dialog.setOnCancelListener(null);
                        Class<?> dialogClass = Class.forName("android.app.Dialog");
                        Field cancelActionField = dialogClass.getDeclaredField("CANCEL");
                        cancelActionField.setAccessible(true);
                        int cancelAction = ((Integer) cancelActionField.get(dialog)).intValue();
                        Field listenerHandlerField = dialogClass.getDeclaredField("mListenersHandler");
                        listenerHandlerField.setAccessible(true);
                        ((Handler) listenerHandlerField.get(dialog)).removeMessages(cancelAction);
                    }
                }
            } catch (Exception e) {
            }
            this.mSysWebView.destroy();
            try {
                Field sConfigCallBackField = Class.forName("android.webkit.BrowserFrame").getDeclaredField("sConfigCallback");
                sConfigCallBackField.setAccessible(true);
                ComponentCallbacks sConfigCallBack = (ComponentCallbacks) sConfigCallBackField.get(null);
                if (sConfigCallBack != null) {
                    sConfigCallBackField.set(null, null);
                    Field sConfigCallBacksField = Class.forName("android.view.ViewRoot").getDeclaredField("sConfigCallbacks");
                    sConfigCallBacksField.setAccessible(true);
                    Object sConfigCallBacks = sConfigCallBacksField.get(null);
                    if (sConfigCallBacks != null) {
                        List<ComponentCallbacks> configCallbackList = (List) sConfigCallBacks;
                        synchronized (configCallbackList) {
                            configCallbackList.remove(sConfigCallBack);
                        }
                    }
                }
            } catch (Exception e2) {
            }
        } else {
            this.mX5WebView.destroy();
        }
    }

    @Deprecated
    public static void enablePlatformNotifications() {
        if (SDKEngine.getInstance(false) == null || !SDKEngine.getInstance(false).isX5Core()) {
            ReflectionUtils.invokeStatic("android.webkit.WebView", "enablePlatformNotifications");
        }
    }

    @Deprecated
    public static void disablePlatformNotifications() {
        if (SDKEngine.getInstance(false) == null || !SDKEngine.getInstance(false).isX5Core()) {
            ReflectionUtils.invokeStatic("android.webkit.WebView", "disablePlatformNotifications");
        }
    }

    @SuppressLint({"NewApi"})
    @TargetApi(3)
    public void setNetworkAvailable(boolean networkUp) {
        if (this.isX5Core) {
            this.mX5WebView.setNetworkAvailable(networkUp);
        } else if (Build.VERSION.SDK_INT >= 3) {
            this.mSysWebView.setNetworkAvailable(networkUp);
        }
    }

    public WebBackForwardList saveState(Bundle outState) {
        if (!this.isX5Core) {
            return WebBackForwardList.wrap(this.mSysWebView.saveState(outState));
        }
        return WebBackForwardList.wrap(this.mX5WebView.saveState(outState));
    }

    @Deprecated
    public boolean savePicture(Bundle b, File dest) {
        if (this.isX5Core) {
            return this.mX5WebView.savePicture(b, dest);
        }
        Object ret = ReflectionUtils.invokeInstance(this.mSysWebView, "savePicture", new Class[]{Bundle.class, File.class}, b, dest);
        if (ret == null) {
            return false;
        }
        return ((Boolean) ret).booleanValue();
    }

    @Deprecated
    public boolean restorePicture(Bundle b, File src) {
        if (this.isX5Core) {
            return this.mX5WebView.restorePicture(b, src);
        }
        Object ret = ReflectionUtils.invokeInstance(this.mSysWebView, "restorePicture", new Class[]{Bundle.class, File.class}, b, src);
        if (ret == null) {
            return false;
        }
        return ((Boolean) ret).booleanValue();
    }

    public WebBackForwardList restoreState(Bundle inState) {
        if (!this.isX5Core) {
            return WebBackForwardList.wrap(this.mSysWebView.restoreState(inState));
        }
        return WebBackForwardList.wrap(this.mX5WebView.restoreState(inState));
    }

    @TargetApi(8)
    public void loadUrl(String url, Map<String, String> extraHeaders) {
        if (this.isX5Core) {
            this.mX5WebView.loadUrl(url, extraHeaders);
        } else if (Build.VERSION.SDK_INT >= 8) {
            this.mSysWebView.loadUrl(url, extraHeaders);
        }
    }

    public void loadUrl(String url) {
        if (!this.isX5Core) {
            this.mSysWebView.loadUrl(url);
        } else {
            this.mX5WebView.loadUrl(url);
        }
    }

    @TargetApi(5)
    public void postUrl(String url, byte[] postData) {
        if (!this.isX5Core) {
            this.mSysWebView.postUrl(url, postData);
        } else {
            this.mX5WebView.postUrl(url, postData);
        }
    }

    public void loadData(String data, String mimeType, String encoding) {
        if (!this.isX5Core) {
            this.mSysWebView.loadData(data, mimeType, encoding);
        } else {
            this.mX5WebView.loadData(data, mimeType, encoding);
        }
    }

    public void loadDataWithBaseURL(String baseUrl, String data, String mimeType, String encoding, String failUrl) {
        if (!this.isX5Core) {
            this.mSysWebView.loadDataWithBaseURL(baseUrl, data, mimeType, encoding, failUrl);
        } else {
            this.mX5WebView.loadDataWithBaseURL(baseUrl, data, mimeType, encoding, failUrl);
        }
    }

    @TargetApi(11)
    public void saveWebArchive(String filename) {
        if (this.isX5Core) {
            this.mX5WebView.saveWebArchive(filename);
        } else if (Build.VERSION.SDK_INT >= 11) {
            ReflectionUtils.invokeInstance(this.mSysWebView, "saveWebArchive", new Class[]{String.class}, filename);
        }
    }

    @TargetApi(11)
    public void saveWebArchive(String basename, boolean autoname, ValueCallback<String> callback) {
        if (this.isX5Core) {
            this.mX5WebView.saveWebArchive(basename, autoname, callback);
        } else if (Build.VERSION.SDK_INT >= 11) {
            ReflectionUtils.invokeInstance(this.mSysWebView, "saveWebArchive", new Class[]{String.class, Boolean.TYPE, ValueCallback.class}, basename, Boolean.valueOf(autoname), callback);
        }
    }

    public void stopLoading() {
        if (!this.isX5Core) {
            this.mSysWebView.stopLoading();
        } else {
            this.mX5WebView.stopLoading();
        }
    }

    public static void setWebContentsDebuggingEnabled(boolean enabled) {
        if (Build.VERSION.SDK_INT >= 19) {
            try {
                sWebviewsetWebContentsDebuggingEnabled = Class.forName("android.webkit.WebView").getDeclaredMethod("setWebContentsDebuggingEnabled", Boolean.TYPE);
                if (sWebviewsetWebContentsDebuggingEnabled != null) {
                    sWebviewsetWebContentsDebuggingEnabled.setAccessible(true);
                    sWebviewsetWebContentsDebuggingEnabled.invoke(null, Boolean.valueOf(enabled));
                }
            } catch (Exception e) {
                QbSdkLog.e(LOGTAG, "Exception:" + e.getStackTrace());
                e.printStackTrace();
            }
        }
    }

    public void reload() {
        if (!this.isX5Core) {
            this.mSysWebView.reload();
        } else {
            this.mX5WebView.reload();
        }
    }

    public boolean canGoBack() {
        if (!this.isX5Core) {
            return this.mSysWebView.canGoBack();
        }
        return this.mX5WebView.canGoBack();
    }

    public void goBack() {
        if (!this.isX5Core) {
            this.mSysWebView.goBack();
        } else {
            this.mX5WebView.goBack();
        }
    }

    public boolean canGoForward() {
        if (!this.isX5Core) {
            return this.mSysWebView.canGoForward();
        }
        return this.mX5WebView.canGoForward();
    }

    public void goForward() {
        if (!this.isX5Core) {
            this.mSysWebView.goForward();
        } else {
            this.mX5WebView.goForward();
        }
    }

    public boolean canGoBackOrForward(int steps) {
        if (!this.isX5Core) {
            return this.mSysWebView.canGoBackOrForward(steps);
        }
        return this.mX5WebView.canGoBackOrForward(steps);
    }

    public void goBackOrForward(int steps) {
        if (!this.isX5Core) {
            this.mSysWebView.goBackOrForward(steps);
        } else {
            this.mX5WebView.goBackOrForward(steps);
        }
    }

    @TargetApi(11)
    public boolean isPrivateBrowsingEnable() {
        Object ret;
        if (this.isX5Core) {
            return this.mX5WebView.isPrivateBrowsingEnable();
        }
        if (Build.VERSION.SDK_INT < 11 || (ret = ReflectionUtils.invokeInstance(this.mSysWebView, "isPrivateBrowsingEnabled")) == null) {
            return false;
        }
        return ((Boolean) ret).booleanValue();
    }

    public boolean pageUp(boolean top) {
        if (!this.isX5Core) {
            return this.mSysWebView.pageUp(top);
        }
        return this.mX5WebView.pageUp(top, -1);
    }

    public boolean pageDown(boolean bottom) {
        if (!this.isX5Core) {
            return this.mSysWebView.pageDown(bottom);
        }
        return this.mX5WebView.pageDown(bottom, -1);
    }

    @Deprecated
    public void clearView() {
        if (!this.isX5Core) {
            ReflectionUtils.invokeInstance(this.mSysWebView, "clearView");
        } else {
            this.mX5WebView.clearView();
        }
    }

    @Deprecated
    public Picture capturePicture() {
        if (this.isX5Core) {
            return this.mX5WebView.capturePicture();
        }
        Object ret = ReflectionUtils.invokeInstance(this.mSysWebView, "capturePicture");
        if (ret == null) {
            return null;
        }
        return (Picture) ret;
    }

    @Deprecated
    public float getScale() {
        if (this.isX5Core) {
            return this.mX5WebView.getScale();
        }
        Object ret = ReflectionUtils.invokeInstance(this.mSysWebView, "getScale");
        return (ret == null ? null : (Float) ret).floatValue();
    }

    public void setInitialScale(int scaleInPercent) {
        if (!this.isX5Core) {
            this.mSysWebView.setInitialScale(scaleInPercent);
        } else {
            this.mX5WebView.setInitialScale(scaleInPercent);
        }
    }

    public void invokeZoomPicker() {
        if (!this.isX5Core) {
            this.mSysWebView.invokeZoomPicker();
        } else {
            this.mX5WebView.invokeZoomPicker();
        }
    }

    public HitTestResult getHitTestResult() {
        if (!this.isX5Core) {
            return new HitTestResult(this.mSysWebView.getHitTestResult());
        }
        return new HitTestResult(this.mX5WebView.getHitTestResult());
    }

    public void requestFocusNodeHref(Message hrefMsg) {
        if (!this.isX5Core) {
            this.mSysWebView.requestFocusNodeHref(hrefMsg);
        } else {
            this.mX5WebView.requestFocusNodeHref(hrefMsg);
        }
    }

    public void requestImageRef(Message msg) {
        if (!this.isX5Core) {
            this.mSysWebView.requestImageRef(msg);
        } else {
            this.mX5WebView.requestImageRef(msg);
        }
    }

    public String getUrl() {
        if (!this.isX5Core) {
            return this.mSysWebView.getUrl();
        }
        return this.mX5WebView.getUrl();
    }

    @TargetApi(3)
    public String getOriginalUrl() {
        if (!this.isX5Core) {
            return this.mSysWebView.getOriginalUrl();
        }
        return this.mX5WebView.getOriginalUrl();
    }

    public String getTitle() {
        if (!this.isX5Core) {
            return this.mSysWebView.getTitle();
        }
        return this.mX5WebView.getTitle();
    }

    public Bitmap getFavicon() {
        if (!this.isX5Core) {
            return this.mSysWebView.getFavicon();
        }
        return this.mX5WebView.getFavicon();
    }

    public int getProgress() {
        if (!this.isX5Core) {
            return this.mSysWebView.getProgress();
        }
        return this.mX5WebView.getProgress();
    }

    public int getContentHeight() {
        if (!this.isX5Core) {
            return this.mSysWebView.getContentHeight();
        }
        return this.mX5WebView.getContentHeight();
    }

    public int getContentWidth() {
        if (this.isX5Core) {
            return this.mX5WebView.getContentWidth();
        }
        Object ret = ReflectionUtils.invokeInstance(this.mSysWebView, "getContentWidth");
        if (ret == null) {
            return 0;
        }
        return ((Integer) ret).intValue();
    }

    public void pauseTimers() {
        if (!this.isX5Core) {
            this.mSysWebView.pauseTimers();
        } else {
            this.mX5WebView.pauseTimers();
        }
    }

    public void resumeTimers() {
        if (!this.isX5Core) {
            this.mSysWebView.resumeTimers();
        } else {
            this.mX5WebView.resumeTimers();
        }
    }

    public void onPause() {
        if (!this.isX5Core) {
            ReflectionUtils.invokeInstance(this.mSysWebView, "onPause");
        } else {
            this.mX5WebView.onPause();
        }
    }

    public void onResume() {
        if (!this.isX5Core) {
            ReflectionUtils.invokeInstance(this.mSysWebView, "onResume");
        } else {
            this.mX5WebView.onResume();
        }
    }

    @Deprecated
    public void freeMemory() {
        if (!this.isX5Core) {
            ReflectionUtils.invokeInstance(this.mSysWebView, "freeMemory");
        } else {
            this.mX5WebView.freeMemory();
        }
    }

    public void clearCache(boolean includeDiskFiles) {
        if (!this.isX5Core) {
            this.mSysWebView.clearCache(includeDiskFiles);
        } else {
            this.mX5WebView.clearCache(includeDiskFiles);
        }
    }

    public void clearFormData() {
        if (!this.isX5Core) {
            this.mSysWebView.clearFormData();
        } else {
            this.mX5WebView.clearFormData();
        }
    }

    public void clearHistory() {
        if (!this.isX5Core) {
            this.mSysWebView.clearHistory();
        } else {
            this.mX5WebView.clearHistory();
        }
    }

    public void clearSslPreferences() {
        if (!this.isX5Core) {
            this.mSysWebView.clearSslPreferences();
        } else {
            this.mX5WebView.clearSslPreferences();
        }
    }

    public WebBackForwardList copyBackForwardList() {
        if (this.isX5Core) {
            return WebBackForwardList.wrap(this.mX5WebView.copyBackForwardList());
        }
        return WebBackForwardList.wrap(this.mSysWebView.copyBackForwardList());
    }

    @TargetApi(16)
    public void setFindListener(final IX5WebViewBase.FindListener listener) {
        if (this.isX5Core) {
            this.mX5WebView.setFindListener(listener);
        } else if (Build.VERSION.SDK_INT < 16) {
            QbSdkLog.w(LOGTAG, "[setFindListener] -- Your sdk_version is:" + Build.VERSION.SDK_INT);
            QbSdkLog.e(LOGTAG, "Sorry, your sdk_version is too low to call this method!");
        } else {
            this.mSysWebView.setFindListener(new WebView.FindListener() {
                public void onFindResultReceived(int activeMatchOrdinal, int numberOfMatches, boolean isDoneCounting) {
                    listener.onFindResultReceived(activeMatchOrdinal, numberOfMatches, isDoneCounting);
                }
            });
        }
    }

    @TargetApi(3)
    public void findNext(boolean forward) {
        if (!this.isX5Core) {
            this.mSysWebView.findNext(forward);
        } else {
            this.mX5WebView.findNext(forward);
        }
    }

    @Deprecated
    public int findAll(String find) {
        if (this.isX5Core) {
            return this.mX5WebView.findAll(find);
        }
        Object ret = ReflectionUtils.invokeInstance(this.mSysWebView, "findAll", new Class[]{String.class}, find);
        if (ret == null) {
            return 0;
        }
        return ((Integer) ret).intValue();
    }

    public static String findAddress(String addr) {
        if (SDKEngine.getInstance(false) == null || SDKEngine.getInstance(false).isX5Core()) {
            return null;
        }
        return android.webkit.WebView.findAddress(addr);
    }

    @TargetApi(16)
    public void findAllAsync(String find) {
        if (this.isX5Core) {
            this.mX5WebView.findAllAsync(find);
        } else if (Build.VERSION.SDK_INT >= 16) {
            ReflectionUtils.invokeInstance(this.mSysWebView, "findAllAsync", new Class[]{String.class}, find);
        }
    }

    public boolean showFindDialog(String text, boolean showMe) {
        return false;
    }

    @TargetApi(3)
    public void clearMatches() {
        if (!this.isX5Core) {
            this.mSysWebView.clearMatches();
        } else {
            this.mX5WebView.clearMatches();
        }
    }

    public void documentHasImages(Message response) {
        if (!this.isX5Core) {
            this.mSysWebView.documentHasImages(response);
        } else {
            this.mX5WebView.documentHasImages(response);
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v2, types: [android.webkit.WebViewClient] */
    /* JADX WARN: Type inference failed for: r0v4, types: [com.tencent.smtt.export.external.interfaces.IX5WebViewClient] */
    /* JADX WARN: Type inference failed for: r0v5 */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setWebViewClient(com.tencent.smtt.sdk.WebViewClient r4) {
        /*
            r3 = this;
            r0 = 0
            boolean r1 = r3.isX5Core
            if (r1 == 0) goto L_0x001c
            com.tencent.smtt.export.external.interfaces.IX5WebViewBase r1 = r3.mX5WebView
            if (r4 != 0) goto L_0x000d
        L_0x0009:
            r1.setWebViewClient(r0)
        L_0x000c:
            return
        L_0x000d:
            com.tencent.smtt.sdk.SmttWebViewClient r0 = new com.tencent.smtt.sdk.SmttWebViewClient
            r2 = 1
            com.tencent.smtt.sdk.SDKEngine r2 = com.tencent.smtt.sdk.SDKEngine.getInstance(r2)
            com.tencent.smtt.export.external.WebViewWizardBase r2 = r2.wizard()
            r0.<init>(r2, r3, r4)
            goto L_0x0009
        L_0x001c:
            com.tencent.smtt.sdk.WebView$SystemWebView r1 = r3.mSysWebView
            if (r4 != 0) goto L_0x0024
        L_0x0020:
            r1.setWebViewClient(r0)
            goto L_0x000c
        L_0x0024:
            com.tencent.smtt.sdk.SystemWebViewClient r0 = new com.tencent.smtt.sdk.SystemWebViewClient
            r0.<init>(r3, r4)
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.smtt.sdk.WebView.setWebViewClient(com.tencent.smtt.sdk.WebViewClient):void");
    }

    public void setDownloadListener(final DownloadListener listener) {
        if (!this.isX5Core) {
            this.mSysWebView.setDownloadListener(new DownloadListener() {
                public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                    if (listener == null) {
                        MttLoader.loadUrl(WebView.this.mContext, url, null);
                    } else {
                        listener.onDownloadStart(url, userAgent, contentDisposition, mimetype, contentLength);
                    }
                }
            });
        } else {
            this.mX5WebView.setDownloadListener(new DownLoadListenerAdapter(this, listener, this.isX5Core));
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v2, types: [android.webkit.WebChromeClient] */
    /* JADX WARN: Type inference failed for: r0v4, types: [com.tencent.smtt.export.external.interfaces.IX5WebChromeClient] */
    /* JADX WARN: Type inference failed for: r0v5 */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setWebChromeClient(com.tencent.smtt.sdk.WebChromeClient r4) {
        /*
            r3 = this;
            r0 = 0
            boolean r1 = r3.isX5Core
            if (r1 == 0) goto L_0x001c
            com.tencent.smtt.export.external.interfaces.IX5WebViewBase r1 = r3.mX5WebView
            if (r4 != 0) goto L_0x000d
        L_0x0009:
            r1.setWebChromeClient(r0)
        L_0x000c:
            return
        L_0x000d:
            com.tencent.smtt.sdk.SmttWebChromeClient r0 = new com.tencent.smtt.sdk.SmttWebChromeClient
            r2 = 1
            com.tencent.smtt.sdk.SDKEngine r2 = com.tencent.smtt.sdk.SDKEngine.getInstance(r2)
            com.tencent.smtt.export.external.WebViewWizardBase r2 = r2.wizard()
            r0.<init>(r2, r3, r4)
            goto L_0x0009
        L_0x001c:
            com.tencent.smtt.sdk.WebView$SystemWebView r1 = r3.mSysWebView
            if (r4 != 0) goto L_0x0024
        L_0x0020:
            r1.setWebChromeClient(r0)
            goto L_0x000c
        L_0x0024:
            com.tencent.smtt.sdk.SystemWebChromeClient r0 = new com.tencent.smtt.sdk.SystemWebChromeClient
            r0.<init>(r3, r4)
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.smtt.sdk.WebView.setWebChromeClient(com.tencent.smtt.sdk.WebChromeClient):void");
    }

    public void setPictureListener(final PictureListener listner) {
        if (!this.isX5Core) {
            if (listner == null) {
                this.mSysWebView.setPictureListener(null);
            } else {
                this.mSysWebView.setPictureListener(new WebView.PictureListener() {
                    public void onNewPicture(android.webkit.WebView webview, Picture picture) {
                        WebView.this.setSysWebView(webview);
                        listner.onNewPicture(WebView.this, picture);
                    }
                });
            }
        } else if (listner == null) {
            this.mX5WebView.setPictureListener(null);
        } else {
            this.mX5WebView.setPictureListener(new IX5WebViewBase.PictureListener() {
                public void onNewPictureIfHaveContent(IX5WebViewBase webview, Picture picture) {
                }

                public void onNewPicture(IX5WebViewBase webview, Picture picture, boolean isFirst) {
                    WebView.this.setX5WebView(webview);
                    listner.onNewPicture(WebView.this, picture);
                }
            });
        }
    }

    public void addJavascriptInterface(Object obj, String interfaceName) {
        if (!this.isX5Core) {
            this.mSysWebView.addJavascriptInterface(obj, interfaceName);
        } else {
            this.mX5WebView.addJavascriptInterface(obj, interfaceName);
        }
    }

    @TargetApi(11)
    public void removeJavascriptInterface(String interfaceName) {
        if (this.isX5Core) {
            this.mX5WebView.removeJavascriptInterface(interfaceName);
        } else if (Build.VERSION.SDK_INT >= 11) {
            ReflectionUtils.invokeInstance(this.mSysWebView, "removeJavascriptInterface", new Class[]{String.class}, interfaceName);
        }
    }

    public WebSettings getSettings() {
        if (this.mWebSettings != null) {
            return this.mWebSettings;
        }
        if (this.isX5Core) {
            WebSettings webSettings = new WebSettings(this.mX5WebView.getSettings());
            this.mWebSettings = webSettings;
            return webSettings;
        }
        WebSettings webSettings2 = new WebSettings(this.mSysWebView.getSettings());
        this.mWebSettings = webSettings2;
        return webSettings2;
    }

    @Deprecated
    public static synchronized Object getPluginList() {
        Object obj;
        synchronized (WebView.class) {
            if (SDKEngine.getInstance(false) == null || SDKEngine.getInstance(false).isX5Core()) {
                obj = null;
            } else {
                obj = ReflectionUtils.invokeStatic("android.webkit.WebView", "getPluginList");
            }
        }
        return obj;
    }

    @Deprecated
    public void refreshPlugins(boolean reloadOpenPages) {
        if (!this.isX5Core) {
            ReflectionUtils.invokeInstance(this.mSysWebView, "refreshPlugins", new Class[]{Boolean.TYPE}, Boolean.valueOf(reloadOpenPages));
            return;
        }
        this.mX5WebView.refreshPlugins(reloadOpenPages);
    }

    @Deprecated
    public void setMapTrackballToArrowKeys(boolean setMap) {
        if (!this.isX5Core) {
            ReflectionUtils.invokeInstance(this.mSysWebView, "setMapTrackballToArrowKeys", new Class[]{Boolean.TYPE}, Boolean.valueOf(setMap));
            return;
        }
        this.mX5WebView.setMapTrackballToArrowKeys(setMap);
    }

    public void flingScroll(int vx, int vy) {
        if (!this.isX5Core) {
            this.mSysWebView.flingScroll(vx, vy);
        } else {
            this.mX5WebView.flingScroll(vx, vy);
        }
    }

    @Deprecated
    public View getZoomControls() {
        if (!this.isX5Core) {
            return (View) ReflectionUtils.invokeInstance(this.mSysWebView, "getZoomControls");
        }
        return this.mX5WebView.getZoomControls();
    }

    @Deprecated
    public boolean canZoomIn() {
        Object ret;
        if (this.isX5Core) {
            return this.mX5WebView.canZoomIn();
        }
        if (Build.VERSION.SDK_INT < 11 || (ret = ReflectionUtils.invokeInstance(this.mSysWebView, "canZoomIn")) == null) {
            return false;
        }
        return ((Boolean) ret).booleanValue();
    }

    public boolean isPrivateBrowsingEnabled() {
        Object ret;
        if (this.isX5Core) {
            return this.mX5WebView.isPrivateBrowsingEnable();
        }
        if (Build.VERSION.SDK_INT < 11 || (ret = ReflectionUtils.invokeInstance(this.mSysWebView, "isPrivateBrowsingEnabled")) == null) {
            return false;
        }
        return ((Boolean) ret).booleanValue();
    }

    @Deprecated
    public boolean canZoomOut() {
        Object ret;
        if (this.isX5Core) {
            return this.mX5WebView.canZoomOut();
        }
        if (Build.VERSION.SDK_INT < 11 || (ret = ReflectionUtils.invokeInstance(this.mSysWebView, "canZoomOut")) == null) {
            return false;
        }
        return ((Boolean) ret).booleanValue();
    }

    public boolean zoomIn() {
        if (!this.isX5Core) {
            return this.mSysWebView.zoomIn();
        }
        return this.mX5WebView.zoomIn();
    }

    public boolean zoomOut() {
        if (!this.isX5Core) {
            return this.mSysWebView.zoomOut();
        }
        return this.mX5WebView.zoomOut();
    }

    public void dumpViewHierarchyWithProperties(BufferedWriter out, int level) {
        if (!this.isX5Core) {
            ReflectionUtils.invokeInstance(this.mSysWebView, "dumpViewHierarchyWithProperties", new Class[]{BufferedWriter.class, Integer.TYPE}, out, Integer.valueOf(level));
            return;
        }
        this.mX5WebView.dumpViewHierarchyWithProperties(out, level);
    }

    public View findHierarchyView(String className, int hashCode) {
        if (this.isX5Core) {
            return this.mX5WebView.findHierarchyView(className, hashCode);
        }
        return (View) ReflectionUtils.invokeInstance(this.mSysWebView, "findHierarchyView", new Class[]{String.class, Integer.TYPE}, className, Integer.valueOf(hashCode));
    }

    public void computeScroll() {
        if (!this.isX5Core) {
            this.mSysWebView.computeScroll();
        } else {
            this.mX5WebView.computeScroll();
        }
    }

    public void setBackgroundColor(int color) {
        if (!this.isX5Core) {
            this.mSysWebView.setBackgroundColor(color);
        } else {
            this.mX5WebView.setBackgroundColor(color);
        }
        super.setBackgroundColor(color);
    }

    public View getView() {
        if (!this.isX5Core) {
            return this.mSysWebView;
        }
        return this.mX5WebView.getView();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        Bundle bundle;
        if (!this.mIsReported) {
            this.mIsReported = true;
            String guid = Constants.STR_EMPTY;
            String qua = Constants.STR_EMPTY;
            String lc = Constants.STR_EMPTY;
            if (this.isX5Core && (bundle = this.mX5WebView.getX5WebViewExtension().getSdkQBStatisticsInfo()) != null) {
                guid = bundle.getString("guid");
                qua = bundle.getString("qua");
                lc = bundle.getString("lc");
            }
            HttpUtils.doReport(this.mContext, guid, qua, lc, this.mPv, this.isX5Core);
        }
        super.onDetachedFromWindow();
    }

    public IX5WebViewExtension getX5WebViewExtension() {
        if (!this.isX5Core) {
            return null;
        }
        return this.mX5WebView.getX5WebViewExtension();
    }

    public IX5WebSettingsExtension getSettingsExtension() {
        if (!this.isX5Core) {
            return null;
        }
        return this.mX5WebView.getX5WebViewExtension().getSettingsExtension();
    }

    public void setWebViewClientExtension(IX5WebViewClientExtension client) {
        if (this.isX5Core) {
            this.mX5WebView.getX5WebViewExtension().setWebViewClientExtension(client);
        }
    }

    public void setWebChromeClientExtension(IX5WebChromeClientExtension client) {
        if (this.isX5Core) {
            this.mX5WebView.getX5WebViewExtension().setWebChromeClientExtension(client);
        }
    }

    public IX5WebChromeClientExtension getWebChromeClientExtension() {
        if (!this.isX5Core) {
            return null;
        }
        return this.mX5WebView.getX5WebViewExtension().getWebChromeClientExtension();
    }

    public IX5WebViewClientExtension getWebViewClientExtension() {
        if (!this.isX5Core) {
            return null;
        }
        return this.mX5WebView.getX5WebViewExtension().getWebViewClientExtension();
    }

    public void evaluateJavascript(String script, ValueCallback<String> resultCallback) {
        if (this.isX5Core) {
            loadUrl(script);
        } else if (Build.VERSION.SDK_INT >= 19) {
            try {
                Method m = Class.forName("android.webkit.WebView").getDeclaredMethod("evaluateJavascript", String.class, ValueCallback.class);
                m.setAccessible(true);
                m.invoke(this.mSysWebView, script, resultCallback);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static int getQQBrowserVersion() {
        return X5Version;
    }

    public static int getQQBrowserCoreVersion(Context context) {
        try {
            return SDKEngine.getQQBrowserCoreVersion(context);
        } catch (Throwable thr) {
            thr.printStackTrace();
            return 0;
        }
    }

    public boolean setVideoFullScreen(boolean bVideoFullScreen) {
        Log.e("1203", "setVideoFullScreen 01");
        if (this.mX5WebView != null) {
            Bundle data = new Bundle();
            if (bVideoFullScreen) {
                data.putInt("DefaultVideoScreen", 2);
            } else {
                data.putInt("DefaultVideoScreen", 1);
            }
            this.mX5WebView.getX5WebViewExtension().invokeMiscMethod("setVideoParams", data);
            Log.e("1203", "setVideoFullScreen 02");
            return true;
        }
        Log.e("1203", "setVideoFullScreen 03");
        return false;
    }

    /* access modifiers changed from: package-private */
    public void setSysWebView(android.webkit.WebView webview) {
        if (!this.isX5Core) {
        }
    }

    /* access modifiers changed from: package-private */
    public android.webkit.WebView getSysWebView() {
        if (!this.isX5Core) {
            return this.mSysWebView;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void setX5WebView(IX5WebViewBase view) {
        this.mX5WebView = view;
    }

    /* access modifiers changed from: package-private */
    public IX5WebViewBase getX5WebView() {
        return this.mX5WebView;
    }

    public void setOnTouchListener(View.OnTouchListener onTouchListener) {
        getView().setOnTouchListener(onTouchListener);
    }

    private class SystemWebView extends android.webkit.WebView {
        public SystemWebView(Context context) {
            super(context);
            if (WebView.mSecureLibraryStatus != 2) {
                SecuritySwitch.setSecureLibraryStatus(WebView.mSecureLibraryStatus);
                SecuritySwitch.setSecurityStatusIfNecessary(context, WebView.this);
            }
            CookieSyncManager.createInstance(WebView.this.mContext).startSync();
            try {
                Method m = Class.forName("android.webkit.WebViewWorker").getDeclaredMethod("getHandler", new Class[0]);
                m.setAccessible(true);
                ((Handler) m.invoke(null, new Object[0])).getLooper().getThread().setUncaughtExceptionHandler(new SQLiteUncaughtExceptionHandler());
            } catch (Exception e) {
            }
        }

        /* access modifiers changed from: protected */
        public void onScrollChanged(int l, int t, int oldl, int oldt) {
            try {
                super.onScrollChanged(l, t, oldl, oldt);
                WebView.this.onScrollChanged(l, t, oldl, oldt);
            } catch (Throwable e) {
                StringWriter errors = new StringWriter();
                e.printStackTrace(new PrintWriter(errors));
                Log.e(WebView.LOGTAG, "$SystemWebView.onScrollChanged - exceptions:" + errors.toString());
            }
        }

        public boolean onTouchEvent(MotionEvent event) {
            if (!hasFocus()) {
                requestFocus();
            }
            try {
                return super.onTouchEvent(event);
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        /* access modifiers changed from: protected */
        public void dispatchDraw(Canvas canvas) {
            try {
                super.dispatchDraw(canvas);
                if (!WebView.mIsDayMode && WebView.mNightModePaint != null) {
                    canvas.save();
                    canvas.drawPaint(WebView.mNightModePaint);
                    canvas.restore();
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public void switchNightMode(boolean isDayMode) {
        if (isDayMode != mIsDayMode) {
            mIsDayMode = isDayMode;
            if (mIsDayMode) {
                Log.e("QB_SDK", "deleteNightMode");
                loadUrl("javascript:document.getElementsByTagName('HEAD').item(0).removeChild(document.getElementById('QQBrowserSDKNightMode'));");
                return;
            }
            Log.e("QB_SDK", "nightMode");
            loadUrl("javascript:var style = document.createElement('style');style.type='text/css';style.id='QQBrowserSDKNightMode';style.innerHTML='html,body{background:none !important;background-color: #1d1e2a !important;}html *{background-color: #1d1e2a !important; color:#888888 !important;border-color:#3e4f61 !important;text-shadow:none !important;box-shadow:none !important;}a,a *{border-color:#4c5b99 !important; color:#2d69b3 !important;text-decoration:none !important;}a:visited,a:visited *{color:#a600a6 !important;}a:active,a:active *{color:#5588AA !important;}input,select,textarea,option,button{background-image:none !important;color:#AAAAAA !important;border-color:#4c5b99 !important;}form,div,button,span{background-color:#1d1e2a !important; border-color:#4c5b99 !important;}img{opacity:0.5}';document.getElementsByTagName('HEAD').item(0).appendChild(style);");
        }
    }

    public void switchToNightMode() {
        Log.e("QB_SDK", "switchToNightMode 01");
        if (!mIsDayMode) {
            Log.e("QB_SDK", "switchToNightMode");
            loadUrl("javascript:var style = document.createElement('style');style.type='text/css';style.id='QQBrowserSDKNightMode';style.innerHTML='html,body{background:none !important;background-color: #1d1e2a !important;}html *{background-color: #1d1e2a !important; color:#888888 !important;border-color:#3e4f61 !important;text-shadow:none !important;box-shadow:none !important;}a,a *{border-color:#4c5b99 !important; color:#2d69b3 !important;text-decoration:none !important;}a:visited,a:visited *{color:#a600a6 !important;}a:active,a:active *{color:#5588AA !important;}input,select,textarea,option,button{background-image:none !important;color:#AAAAAA !important;border-color:#4c5b99 !important;}form,div,button,span{background-color:#1d1e2a !important; border-color:#4c5b99 !important;}img{opacity:0.5}';document.getElementsByTagName('HEAD').item(0).appendChild(style);");
        }
    }

    public static synchronized void setLoadSecuritySOByApp(int SecureLibraryStatus) {
        synchronized (WebView.class) {
            mSecureLibraryStatus = SecureLibraryStatus;
            QbSdkLog.e("QbSdk", "in setLoadSecuritySOByApp, mSecureLibraryStatus is " + mSecureLibraryStatus);
        }
    }

    public static synchronized void setSysDayOrNight(boolean isDayMode) {
        synchronized (WebView.class) {
            if (isDayMode != mIsDayMode) {
                mIsDayMode = isDayMode;
                if (mNightModePaint == null) {
                    mNightModePaint = new Paint();
                    mNightModePaint.setColor((int) NIGHT_MODE_COLOR);
                }
                if (!isDayMode) {
                    if (mNightModePaint.getAlpha() != NIGHT_MODE_ALPHA) {
                        mNightModePaint.setAlpha(NIGHT_MODE_ALPHA);
                    }
                } else if (mNightModePaint.getAlpha() != 255) {
                    mNightModePaint.setAlpha(255);
                }
            }
        }
    }

    public void setDayOrNight(boolean isDayMode) {
        try {
            if (this.isX5Core) {
                getSettingsExtension().setDayOrNight(isDayMode);
            }
            setSysDayOrNight(isDayMode);
            getView().postInvalidate();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void setLongPressCopyViewDisplayShare(boolean isNeedShare) {
        if (this.isX5Core) {
            ReflectionUtils.invokeInstance(this.mX5WebView, "setLongPressCopyViewDisplayShare", new Class[]{Boolean.TYPE}, Boolean.valueOf(isNeedShare));
        }
    }

    public boolean isDayMode() {
        return mIsDayMode;
    }

    public int getSysNightModeAlpha() {
        return NIGHT_MODE_ALPHA;
    }

    public void setSysNightModeAlpha(int nightModeAlpha) {
        NIGHT_MODE_ALPHA = nightModeAlpha;
    }

    public void setOnLongClickListener(final View.OnLongClickListener l) {
        if (!this.isX5Core) {
            this.mSysWebView.setOnLongClickListener(l);
            return;
        }
        View view = this.mX5WebView.getView();
        try {
            Method method = ReflectionUtils.getParentDeclaredMethod(view, "getListenerInfo", new Class[0]);
            method.setAccessible(true);
            Object listenerInfo = method.invoke(view, null);
            Field longClickListenerField = listenerInfo.getClass().getDeclaredField("mOnLongClickListener");
            longClickListenerField.setAccessible(true);
            this.longClickListener = longClickListenerField.get(listenerInfo);
            getView().setOnLongClickListener(new View.OnLongClickListener() {
                public boolean onLongClick(View v) {
                    if (l == null) {
                        return WebView.this.callX5CoreOnLongClick(v);
                    }
                    if (!l.onLongClick(v)) {
                        return WebView.this.callX5CoreOnLongClick(v);
                    }
                    return true;
                }
            });
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: private */
    public boolean callX5CoreOnLongClick(View v) {
        Object clickResult = ReflectionUtils.invokeInstance(this.longClickListener, "onLongClick", new Class[]{View.class}, v);
        if (clickResult != null) {
            return ((Boolean) clickResult).booleanValue();
        }
        return false;
    }

    public void addView(View child) {
        if (!this.isX5Core) {
            this.mSysWebView.addView(child);
            return;
        }
        View view = this.mX5WebView.getView();
        try {
            Method method = ReflectionUtils.getParentDeclaredMethod(view, "addView", View.class);
            method.setAccessible(true);
            method.invoke(view, child);
        } catch (Throwable th) {
        }
    }

    public void removeView(View view) {
        if (!this.isX5Core) {
            this.mSysWebView.removeView(view);
            return;
        }
        View x5view = this.mX5WebView.getView();
        try {
            Method method = ReflectionUtils.getParentDeclaredMethod(x5view, "removeView", View.class);
            method.setAccessible(true);
            method.invoke(x5view, view);
        } catch (Throwable th) {
        }
    }

    public boolean getX5CoreNeedReboot() {
        return SDKEngine.getInstance(true).getX5CoreNeedReboot();
    }

    /* access modifiers changed from: package-private */
    public void compareQBTimestampForReboot() {
        SDKEngine.compareQBTimestampForReboot(this.mContext);
    }
}
