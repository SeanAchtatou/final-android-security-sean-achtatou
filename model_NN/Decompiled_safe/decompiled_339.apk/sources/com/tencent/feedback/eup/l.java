package com.tencent.feedback.eup;

import android.content.Context;
import com.tencent.connect.common.Constants;
import com.tencent.feedback.a.a;
import com.tencent.feedback.common.PlugInInfo;
import com.tencent.feedback.common.e;
import com.tencent.feedback.common.f;
import com.tencent.feedback.common.g;
import com.tencent.feedback.common.i;
import com.tencent.feedback.common.j;
import com.tencent.feedback.proguard.C;
import com.tencent.feedback.proguard.E;
import com.tencent.feedback.proguard.F;
import com.tencent.feedback.proguard.G;
import com.tencent.feedback.proguard.H;
import com.tencent.feedback.proguard.I;
import com.tencent.feedback.proguard.J;
import com.tencent.feedback.proguard.K;
import com.tencent.feedback.proguard.L;
import com.tencent.feedback.proguard.ac;
import com.tencent.feedback.proguard.aj;
import com.tencent.feedback.proguard.al;
import com.tencent.open.SocialConstants;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public final class l extends a {
    private static l d = null;
    private byte[] e = null;
    private C f = null;
    private List<e> g = null;

    public static synchronized l a(Context context) {
        l lVar;
        synchronized (l.class) {
            if (context != null) {
                if (d == null) {
                    d = new l(context);
                }
            }
            lVar = d;
        }
        return lVar;
    }

    private l(Context context) {
        super(context, 3, 206);
    }

    public final synchronized C a() {
        C c = null;
        synchronized (this) {
            if (this.f != null) {
                c = this.f;
            } else {
                k k = k.k();
                if (k != null && k.a()) {
                    try {
                        d q = k.k().q();
                        this.g = null;
                        try {
                            boolean e2 = q.e();
                            int b = i.a(this.c) ? q.b() : q.c();
                            if (!e2) {
                                g.f("rqdp{  not merge:}", new Object[0]);
                                this.g = b(this.c, b);
                            } else {
                                g.f("rqdp{  in merge:}", new Object[0]);
                                this.g = a(this.c, b);
                            }
                            if (this.g == null || this.g.size() <= 0) {
                                g.c("rqdp{  empty eup!}", new Object[0]);
                            } else {
                                g.f("rqdp{  pack n:}%d ,isM:%b", Integer.valueOf(this.g.size()), Boolean.valueOf(e2));
                                I a2 = a(this.c, this.g, (String) null);
                                if (a2 != null) {
                                    byte[] a3 = a2.a();
                                    if (a3 == null) {
                                        g.c("rqdp{  empty edatas!}", new Object[0]);
                                    } else {
                                        this.f = a(this.c, this.f3667a, a3);
                                        c = this.f;
                                    }
                                }
                            }
                        } catch (Throwable th) {
                            th.printStackTrace();
                            if (this.g != null && this.g.size() > 0) {
                                g.c("rqdp{ eup error remove} %d", Integer.valueOf(f.a(this.c, this.g)));
                                this.g = null;
                            }
                        }
                    } catch (Throwable th2) {
                        th2.printStackTrace();
                        g.d("rqdp{  imposiable} %s", th2.toString());
                    }
                }
            }
        }
        return c;
    }

    private List<e> b(Context context, int i) {
        List<e> a2;
        g.e("rqdp{  get MN:}%d", Integer.valueOf(i));
        if (context == null || i <= 0) {
            g.c("rqdp{  params!}", new Object[0]);
            return null;
        }
        try {
            List<e> a3 = f.a(context, i, SocialConstants.PARAM_APP_DESC, 1, null, -1, -1, -1, 3, -1, -1, null);
            if (a3 == null) {
                a3 = new ArrayList<>();
            }
            if (a3.size() < i && (a2 = f.a(context, i - a3.size(), SocialConstants.PARAM_APP_DESC, 2, null, -1, -1, -1, 3, -1, -1, null)) != null && a2.size() > 0) {
                a3.addAll(a2);
            }
            a(a3);
            f.b(context, a3);
            return a3;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    private I a(Context context, List<e> list, String str) {
        if (context == null || list == null || list.size() <= 0) {
            g.c("rqdp{  params!}", new Object[0]);
            return null;
        }
        try {
            ArrayList<H> arrayList = new ArrayList<>();
            ArrayList arrayList2 = new ArrayList();
            for (e next : list) {
                if (!e.a(context).d().equals(next.Q())) {
                    g.c("updated drop it", new Object[0]);
                    arrayList2.add(next);
                } else {
                    H a2 = a(context, next);
                    if (a2 != null) {
                        arrayList.add(a2);
                    } else {
                        arrayList2.add(next);
                    }
                }
            }
            I i = new I();
            i.f3709a = arrayList;
            if (arrayList2.size() <= 0) {
                return i;
            }
            g.c("rqdp{ delete error eup} %d", Integer.valueOf(f.a(context, arrayList2)));
            list.removeAll(arrayList2);
            return i;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    private H a(Context context, e eVar) {
        String str;
        e a2;
        J j;
        String str2;
        F a3;
        F a4;
        F a5;
        F a6;
        F a7;
        if (eVar == null) {
            g.c("rqdp{  params!}", new Object[0]);
            return null;
        }
        g.f("rqdp{  pack n:}%s , rqdp{  iM:}%b , rqdp{  iN:}%b", eVar.f(), Boolean.valueOf(eVar.c()), Boolean.valueOf(eVar.d()));
        if (eVar.d()) {
            str = eVar.c() ? "201" : "101";
        } else if (eVar.z()) {
            str = eVar.c() ? "203" : "103";
        } else if (eVar.b()) {
            str = eVar.c() ? "200" : "100";
        } else {
            str = eVar.c() ? "202" : "102";
        }
        H h = new H();
        h.s = eVar.x();
        h.q = str;
        h.d = eVar.i();
        h.f3708a = eVar.e();
        h.b = eVar.g();
        h.f = eVar.t();
        h.c = eVar.q();
        h.g = eVar.e() + ": " + eVar.f() + "\n" + eVar.h();
        h.h = Constants.STR_EMPTY;
        h.o = eVar.s();
        h.m = (long) eVar.o();
        f.a(context);
        h.n = f.d();
        ArrayList<F> arrayList = new ArrayList<>();
        try {
            F a8 = a(eVar.p(), "log.txt");
            if (a8 != null) {
                g.f("rqdp{  attach sys log}", new Object[0]);
                arrayList.add(a8);
            }
            if (eVar.c() && eVar.o() > 1 && (a7 = a(eVar.n().getBytes("utf8"), "alltimes.txt")) != null) {
                g.f("rqdp{  attach merge times}", new Object[0]);
                arrayList.add(a7);
            }
            if (!(!eVar.d() || eVar.r() == null || (a6 = a(context, eVar.r())) == null)) {
                g.f("rqdp{  attach tomb}", new Object[0]);
                arrayList.add(a6);
            }
            if (eVar.d()) {
                if (this.e == null) {
                    f.a(context);
                    this.e = f.e();
                }
                if (!(this.e == null || (a5 = a(this.e, "cpuinfo.txt")) == null)) {
                    g.f("rqdp{  attach cp info}", new Object[0]);
                    arrayList.add(a5);
                }
            }
            if (!(eVar.u() == null || (a4 = a(eVar.u().getBytes("utf8"), "extraMessage.txt")) == null)) {
                g.f("rqdp{  attach extra msg}", new Object[0]);
                arrayList.add(a4);
            }
            if (!(eVar.v() == null || (a3 = a(eVar.v(), "extraDatas.txt")) == null)) {
                g.f("rqdp{  attach extra datas}", new Object[0]);
                arrayList.add(a3);
            }
            if (eVar.F() != null && eVar.F().size() > 0) {
                StringBuilder sb = new StringBuilder();
                for (Map.Entry next : eVar.F().entrySet()) {
                    sb.append("#").append((String) next.getKey()).append(":\n");
                    sb.append((String) next.getValue());
                    sb.append("\n");
                }
                if (sb.length() > 0) {
                    F a9 = a(sb.toString().getBytes("utf8"), "allthread.txt");
                    g.f("rqdp{  attach all threads}", new Object[0]);
                    arrayList.add(a9);
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        try {
            if (arrayList.size() > 0) {
                h.e = arrayList;
            }
            L l = new L();
            l.f3712a = eVar.j();
            l.b = eVar.k();
            l.c = eVar.I();
            l.d = eVar.J();
            l.e = eVar.K();
            HashMap hashMap = new HashMap(5);
            hashMap.put("tStack", new StringBuilder().append(eVar.O()).toString());
            hashMap.put("tHeap", new StringBuilder().append(eVar.P()).toString());
            hashMap.put("pVer", eVar.Q());
            l.f = hashMap;
            g.b("type:%s fm:%d fs:%d fsd:%d tS:%d tH:%d pV:%s", h.q, Long.valueOf(l.c), Long.valueOf(l.d), Long.valueOf(l.e), Long.valueOf(eVar.O()), Long.valueOf(eVar.P()), eVar.Q());
            h.j = l;
            a2 = e.a(context);
            j = new J();
            j.f3710a = new StringBuilder().append(a2).toString() != null ? a2.p() : "null";
            j.d = a2.g();
            j.i = a2.x();
            j.h = a2.w();
            j.c = a2.j();
            j.f = a2.t();
            j.g = a2.u();
            j.e = a2.h();
            j.j = new HashMap(5);
            j.j.put("totalSD", new StringBuilder().append(eVar.N()).toString());
            j.j.put("imei", a2.p());
            j.j.put("imsi", a2.r());
            j.j.put("androidId", a2.s());
            j.j.put("mac", a2.q());
            j.j.put("country", eVar.R());
            str2 = URLEncoder.encode(eVar.A(), "utf-8");
        } catch (Throwable th2) {
            th2.printStackTrace();
            return null;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("isRooted=").append(j.a().b());
        sb2.append("&rom").append("=").append(str2);
        if (eVar.d()) {
            if (eVar.B() != null && !eVar.B().isEmpty()) {
                sb2.append("&errMsg").append("=").append(eVar.B());
            }
            if (eVar.D() != null && !eVar.D().isEmpty()) {
                sb2.append("&sendType").append("=").append(eVar.D());
            }
            if (eVar.C() != null && !eVar.C().isEmpty()) {
                sb2.append("&sendProcess").append("=").append(eVar.C());
            }
            if (eVar.H()) {
                sb2.append("&from").append("=NATIVE_RECORD_FILE");
            }
            sb2.append("&nativeRQDVersion").append("=").append(eVar.E());
        }
        j.b = sb2.toString();
        sb2.setLength(0);
        h.i = j;
        g.b("symbol %s", j.d);
        g.b("brand %s", j.d);
        g.b("cpuName %s", j.i);
        g.b("cpuType %s", j.h);
        g.b("deviceId %s", j.c);
        g.b("diskSize %s", Long.valueOf(j.f));
        g.b("memSize %s", Long.valueOf(j.g));
        g.b("osver %s", j.e);
        g.b("totalSD %s", j.j.get("totalSD"));
        g.b("country %s", j.j.get("country"));
        g.b("imei %s", j.j.get("imei"));
        g.b("imsi %s", j.j.get("imsi"));
        g.b("androidId %s", j.j.get("androidId"));
        g.b("mac %s", j.j.get("mac"));
        g.b("other %s", j.b);
        G g2 = new G();
        g2.f3707a = eVar.m();
        g2.b = eVar.m();
        h.k = g2;
        if (eVar.d()) {
            ArrayList<E> b = b(context);
            h.l = b;
            Object[] objArr = new Object[1];
            objArr[0] = Integer.valueOf(b == null ? 0 : b.size());
            g.f("rqdp{  attachlbinfo} %d", objArr);
        }
        h.p = a2 != null ? a2.m() : "null";
        Map<String, PlugInInfo> w = eVar.w();
        Object[] objArr2 = new Object[1];
        objArr2[0] = Integer.valueOf(w == null ? 0 : w.size());
        g.b("plugin size :%d", objArr2);
        if (w != null && w.size() > 0) {
            ArrayList<K> arrayList2 = new ArrayList<>();
            for (Map.Entry next2 : w.entrySet()) {
                K k = new K();
                k.f3711a = (String) next2.getKey();
                k.b = ((PlugInInfo) next2.getValue()).b;
                k.c = ((PlugInInfo) next2.getValue()).c;
                g.b("up %s %s %s", k.f3711a, k.b, k.c);
                arrayList2.add(k);
            }
            h.r = arrayList2;
        }
        return h;
    }

    private static F a(byte[] bArr, String str) {
        if (bArr == null || bArr.length <= 0 || str == null || str.trim().length() <= 0) {
            return null;
        }
        try {
            F f2 = new F();
            f2.f3706a = 1;
            f2.b = str;
            f2.c = bArr;
            return f2;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public final synchronized void a(boolean z) {
        boolean z2;
        Object[] objArr = new Object[1];
        objArr[0] = z ? "SUCC" : "FAIL";
        g.e("rqdp{  eupdone :} %s", objArr);
        if (this.g != null && z) {
            try {
                z2 = k.k().q().e();
            } catch (Throwable th) {
                th.printStackTrace();
                z2 = false;
            }
            if (z2) {
                long b = ac.b();
                Iterator<e> it = this.g.iterator();
                ArrayList arrayList = new ArrayList();
                while (it.hasNext()) {
                    e next = it.next();
                    if (next.i() > b) {
                        next.d(true);
                        next.a(0);
                        arrayList.add(next);
                        it.remove();
                    }
                }
                g.b("rqdp{  merge update today eup n:}%d , res:%b", Integer.valueOf(this.g.size()), Boolean.valueOf(f.b(this.c, arrayList)));
                g.b("rqdp{  me rm num:}%d", Integer.valueOf(f.a(this.c, this.g)));
            } else {
                g.b("rqdp{  rm n:}" + f.a(this.c, this.g), new Object[0]);
            }
        }
        this.f = null;
        this.g = null;
    }

    /* access modifiers changed from: protected */
    public final List<e> a(Context context, int i) {
        List<e> a2;
        List<e> a3;
        g.e("rqdp{  getEupInMe}", new Object[0]);
        if (context == null || i <= 0) {
            g.d("rqdp{  params!}", new Object[0]);
            return null;
        }
        try {
            long b = ac.b();
            ArrayList arrayList = new ArrayList();
            List<e> a4 = f.a(context, i, SocialConstants.PARAM_APP_DESC, -1, null, -1, -1, -1, 3, b, -1, false);
            if (a4 != null && a4.size() > 0) {
                g.b("rqdp{  tdeup ge c=1, n:}%d", Integer.valueOf(a4.size()));
                arrayList.addAll(a4);
                a4.clear();
            }
            if (arrayList.size() < i && (a3 = f.a(context, i, SocialConstants.PARAM_APP_DESC, -1, null, 2, -1, -1, 3, -1, b, null)) != null && a3.size() > 0) {
                g.b("rqdp{  yeseup c>=2,n:}%d" + a3.size(), new Object[0]);
                arrayList.addAll(a3);
                a3.clear();
            }
            if (arrayList.size() < i && (a2 = f.a(context, i, SocialConstants.PARAM_APP_DESC, -1, null, 0, 1, -1, -1, 3, b, false)) != null && a2.size() > 0) {
                g.b("rqdp{  yeseup c>=2,n:}%d" + a2.size(), new Object[0]);
                arrayList.addAll(a2);
                a2.clear();
            }
            a(arrayList);
            f.b(context, arrayList);
            return arrayList;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x005a A[SYNTHETIC, Splitter:B:20:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00b1 A[SYNTHETIC, Splitter:B:40:0x00b1] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:53:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.tencent.feedback.proguard.F a(android.content.Context r9, java.lang.String r10) {
        /*
            r2 = 1
            r0 = 0
            r8 = 0
            if (r10 == 0) goto L_0x0007
            if (r9 != 0) goto L_0x000f
        L_0x0007:
            java.lang.String r1 = "rqdp{  createZipAttachment sourcePath == null || context == null ,pls check}"
            java.lang.Object[] r2 = new java.lang.Object[r8]
            com.tencent.feedback.common.g.c(r1, r2)
        L_0x000e:
            return r0
        L_0x000f:
            java.lang.String r1 = "rqdp{  zp}%s"
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r2[r8] = r10
            com.tencent.feedback.common.g.f(r1, r2)
            java.io.File r1 = new java.io.File
            r1.<init>(r10)
            java.io.File r3 = new java.io.File
            java.io.File r2 = r9.getCacheDir()
            java.lang.String r4 = "tomb.zip"
            r3.<init>(r2, r4)
            r2 = 5000(0x1388, float:7.006E-42)
            boolean r1 = com.tencent.feedback.proguard.ac.a(r1, r3, r2)
            if (r1 != 0) goto L_0x0038
            java.lang.String r1 = "rqdp{  fail!}"
            java.lang.Object[] r2 = new java.lang.Object[r8]
            com.tencent.feedback.common.g.c(r1, r2)
            goto L_0x000e
        L_0x0038:
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r1.<init>()
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x00cc, all -> 0x00ac }
            r2.<init>(r3)     // Catch:{ Throwable -> 0x00cc, all -> 0x00ac }
            r4 = 1000(0x3e8, float:1.401E-42)
            byte[] r4 = new byte[r4]     // Catch:{ Throwable -> 0x0054 }
        L_0x0046:
            int r5 = r2.read(r4)     // Catch:{ Throwable -> 0x0054 }
            if (r5 <= 0) goto L_0x006e
            r6 = 0
            r1.write(r4, r6, r5)     // Catch:{ Throwable -> 0x0054 }
            r1.flush()     // Catch:{ Throwable -> 0x0054 }
            goto L_0x0046
        L_0x0054:
            r1 = move-exception
        L_0x0055:
            r1.printStackTrace()     // Catch:{ all -> 0x00ca }
            if (r2 == 0) goto L_0x005d
            r2.close()     // Catch:{ IOException -> 0x00a7 }
        L_0x005d:
            boolean r1 = r3.exists()
            if (r1 == 0) goto L_0x000e
            java.lang.String r1 = "rqdp{  del tmp}"
            java.lang.Object[] r2 = new java.lang.Object[r8]
            com.tencent.feedback.common.g.f(r1, r2)
            r3.delete()
            goto L_0x000e
        L_0x006e:
            byte[] r4 = r1.toByteArray()     // Catch:{ Throwable -> 0x0054 }
            java.lang.String r1 = "rqdp{  re sz:}%d"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x0054 }
            r6 = 0
            int r7 = r4.length     // Catch:{ Throwable -> 0x0054 }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Throwable -> 0x0054 }
            r5[r6] = r7     // Catch:{ Throwable -> 0x0054 }
            com.tencent.feedback.common.g.f(r1, r5)     // Catch:{ Throwable -> 0x0054 }
            com.tencent.feedback.proguard.F r1 = new com.tencent.feedback.proguard.F     // Catch:{ Throwable -> 0x0054 }
            r5 = 2
            java.lang.String r6 = r3.getName()     // Catch:{ Throwable -> 0x0054 }
            r1.<init>(r5, r6, r4)     // Catch:{ Throwable -> 0x0054 }
            r2.close()     // Catch:{ IOException -> 0x00a2 }
        L_0x008f:
            boolean r0 = r3.exists()
            if (r0 == 0) goto L_0x009f
            java.lang.String r0 = "rqdp{  del tmp}"
            java.lang.Object[] r2 = new java.lang.Object[r8]
            com.tencent.feedback.common.g.f(r0, r2)
            r3.delete()
        L_0x009f:
            r0 = r1
            goto L_0x000e
        L_0x00a2:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x008f
        L_0x00a7:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005d
        L_0x00ac:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x00af:
            if (r2 == 0) goto L_0x00b4
            r2.close()     // Catch:{ IOException -> 0x00c5 }
        L_0x00b4:
            boolean r1 = r3.exists()
            if (r1 == 0) goto L_0x00c4
            java.lang.String r1 = "rqdp{  del tmp}"
            java.lang.Object[] r2 = new java.lang.Object[r8]
            com.tencent.feedback.common.g.f(r1, r2)
            r3.delete()
        L_0x00c4:
            throw r0
        L_0x00c5:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00b4
        L_0x00ca:
            r0 = move-exception
            goto L_0x00af
        L_0x00cc:
            r1 = move-exception
            r2 = r0
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.eup.l.a(android.content.Context, java.lang.String):com.tencent.feedback.proguard.F");
    }

    private static ArrayList<E> b(Context context) {
        try {
            List<al> a2 = aj.a(context, null, 1, 50);
            if (a2 != null && a2.size() > 0) {
                ArrayList<E> arrayList = new ArrayList<>();
                for (al next : a2) {
                    E e2 = new E();
                    e2.b = next.f();
                    e2.f3705a = next.a();
                    e2.c = next.d();
                    arrayList.add(e2);
                }
                return arrayList;
            }
        } catch (Throwable th) {
            th.printStackTrace();
            g.d("rqdp{  Error: lb pack fail!}", new Object[0]);
        }
        return null;
    }

    private static void a(List<e> list) {
        if (list != null) {
            for (e next : list) {
                next.a(next.l() + 1);
            }
        }
    }
}
