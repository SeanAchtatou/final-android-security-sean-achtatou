package com.tencent.feedback.eup.jni;

import android.content.Context;
import com.tencent.assistant.st.STConst;
import com.tencent.connect.common.Constants;
import com.tencent.feedback.common.g;
import com.tencent.feedback.eup.e;
import com.tencent.feedback.eup.f;

/* compiled from: ProGuard */
public final class b implements NativeExceptionHandler {
    private static b b;

    /* renamed from: a  reason: collision with root package name */
    private Context f3698a;

    private b(Context context) {
        this.f3698a = context;
    }

    public final void handleNativeException(int i, int i2, long j, long j2, String str, String str2, String str3, String str4) {
        handleNativeException(i, i2, j, j2, str, str2, str3, str4, -1234567890, Constants.STR_EMPTY, -1, -1, -1, Constants.STR_EMPTY, STConst.ST_INSTALL_FAIL_STR_UNKNOWN);
    }

    public static synchronized NativeExceptionHandler a(Context context) {
        b bVar;
        synchronized (b.class) {
            if (b == null) {
                b = new b(context);
            }
            bVar = b;
        }
        return bVar;
    }

    protected static e a(Context context, long j, String str, String str2, String str3, String str4, int i, String str5, int i2, byte[] bArr, String str6, String str7) {
        String str8;
        if (str3 == null) {
            str8 = null;
        } else {
            int indexOf = str3.indexOf("java.lang.Thread.getStackTrace");
            if (indexOf < 0) {
                str8 = str3;
            } else {
                int indexOf2 = str3.indexOf("\n", indexOf);
                if (indexOf2 < 0) {
                    str8 = str3;
                } else {
                    str8 = str3.substring(0, indexOf) + str3.substring(indexOf2);
                }
            }
        }
        String h = com.tencent.feedback.common.b.h(context);
        String name = Thread.currentThread().getName();
        com.tencent.feedback.common.e a2 = com.tencent.feedback.common.e.a(context);
        e a3 = f.a(context, a2.i(), a2.p(), a2.l(), a2.y(), h, name, str2, str, str, str8, j, str6, bArr);
        if (a3 == null) {
            return null;
        }
        if (i > 0) {
            a3.a(a3.e() + "(" + str5 + ")");
            a3.o("kernel");
        } else {
            a3.o(str5);
            if (i2 > 0) {
                a3.n(com.tencent.feedback.common.b.a(context, i2));
            } else {
                a3.n(STConst.ST_INSTALL_FAIL_STR_UNKNOWN + i2);
            }
        }
        g.a("etype:%s,sType:%s,sPN:%s", a3.e(), a3.D(), a3.C());
        a3.a(true);
        a3.c(true);
        a3.h(str4);
        a3.p(str7);
        return a3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x014a  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x016d  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x01b9  */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void handleNativeException(int r20, int r21, long r22, long r24, java.lang.String r26, java.lang.String r27, java.lang.String r28, java.lang.String r29, int r30, java.lang.String r31, int r32, int r33, int r34, java.lang.String r35, java.lang.String r36) {
        /*
            r19 = this;
            java.lang.String r1 = "rqdp{  na eup p:} %d , t:%d , exT:%d ,exTMS: %d, exTP:%s ,exADD:%s ,parsed exSTA:%s, TMB:%s , si_code:%d , si_CodeType:%s , sPid:%d ,sUid:%d,siErr:%d,siErrMsg:%s,naVersion:%s"
            r2 = 15
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.Integer r4 = java.lang.Integer.valueOf(r20)
            r2[r3] = r4
            r3 = 1
            java.lang.Integer r4 = java.lang.Integer.valueOf(r21)
            r2[r3] = r4
            r3 = 2
            java.lang.Long r4 = java.lang.Long.valueOf(r22)
            r2[r3] = r4
            r3 = 3
            java.lang.Long r4 = java.lang.Long.valueOf(r24)
            r2[r3] = r4
            r3 = 4
            r2[r3] = r26
            r3 = 5
            r2[r3] = r27
            r3 = 6
            r2[r3] = r28
            r3 = 7
            r2[r3] = r29
            r3 = 8
            java.lang.Integer r4 = java.lang.Integer.valueOf(r30)
            r2[r3] = r4
            r3 = 9
            r2[r3] = r31
            r3 = 10
            java.lang.Integer r4 = java.lang.Integer.valueOf(r32)
            r2[r3] = r4
            r3 = 11
            java.lang.Integer r4 = java.lang.Integer.valueOf(r33)
            r2[r3] = r4
            r3 = 12
            java.lang.Integer r4 = java.lang.Integer.valueOf(r34)
            r2[r3] = r4
            r3 = 13
            r2[r3] = r35
            r3 = 14
            r2[r3] = r36
            com.tencent.feedback.common.g.e(r1, r2)
            java.lang.String r1 = "eup"
            java.lang.String r2 = "native crash happen"
            android.util.Log.e(r1, r2)
            java.lang.String r1 = "eup"
            r0 = r28
            android.util.Log.e(r1, r0)
            r16 = 0
            r17 = 0
            r1 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 * r22
            r3 = 1000(0x3e8, double:4.94E-321)
            long r3 = r24 / r3
            long r7 = r1 + r3
            com.tencent.feedback.eup.k r1 = com.tencent.feedback.eup.k.k()
            if (r1 != 0) goto L_0x00e3
            java.lang.String r1 = "rqdp{  instance == null}"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.c(r1, r2)
            r1 = 0
        L_0x0088:
            if (r1 == 0) goto L_0x00c0
            java.lang.String r2 = "your crhanlde start"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x00e8 }
            com.tencent.feedback.common.g.b(r2, r3)     // Catch:{ Throwable -> 0x00e8 }
            r2 = 1
            r1.a(r2)     // Catch:{ Throwable -> 0x00e8 }
        L_0x0096:
            java.lang.String r2 = "your crdata"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x00fc }
            com.tencent.feedback.common.g.b(r2, r3)     // Catch:{ Throwable -> 0x00fc }
            r2 = 1
            r3 = r26
            r4 = r27
            r5 = r28
            r6 = r30
            byte[] r16 = r1.a(r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x00fc }
        L_0x00ab:
            java.lang.String r2 = "your crmsg"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0110 }
            com.tencent.feedback.common.g.b(r2, r3)     // Catch:{ Throwable -> 0x0110 }
            r2 = 1
            r3 = r26
            r4 = r27
            r5 = r28
            r6 = r30
            java.lang.String r17 = r1.b(r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x0110 }
        L_0x00c0:
            r0 = r19
            android.content.Context r6 = r0.f3698a
            r9 = r26
            r10 = r27
            r11 = r28
            r12 = r29
            r13 = r30
            r14 = r31
            r15 = r32
            r18 = r36
            com.tencent.feedback.eup.e r13 = a(r6, r7, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18)
            if (r13 != 0) goto L_0x0124
            java.lang.String r1 = "rqdp{  cr eup msg fail!}"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.c(r1, r2)
        L_0x00e2:
            return
        L_0x00e3:
            com.tencent.feedback.eup.b r1 = r1.p()
            goto L_0x0088
        L_0x00e8:
            r2 = move-exception
            java.lang.String r3 = "on native hanlde start throw %s"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            java.lang.String r6 = r2.toString()
            r4[r5] = r6
            com.tencent.feedback.common.g.d(r3, r4)
            r2.printStackTrace()
            goto L_0x0096
        L_0x00fc:
            r2 = move-exception
            java.lang.String r3 = "get extra data error %s"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            java.lang.String r6 = r2.toString()
            r4[r5] = r6
            com.tencent.feedback.common.g.d(r3, r4)
            r2.printStackTrace()
            goto L_0x00ab
        L_0x0110:
            r2 = move-exception
            java.lang.String r3 = "get extra msg error %s"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            java.lang.String r6 = r2.toString()
            r4[r5] = r6
            com.tencent.feedback.common.g.d(r3, r4)
            r2.printStackTrace()
            goto L_0x00c0
        L_0x0124:
            r12 = 1
            if (r1 == 0) goto L_0x01a3
            java.lang.String r2 = "your ask2save"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]
            com.tencent.feedback.common.g.e(r2, r3)
            r2 = 1
            java.lang.String r9 = r13.m()     // Catch:{ Throwable -> 0x0190 }
            java.lang.String r10 = r13.G()     // Catch:{ Throwable -> 0x0190 }
            java.lang.String r11 = r13.x()     // Catch:{ Throwable -> 0x0190 }
            r3 = r26
            r4 = r27
            r5 = r28
            r6 = r30
            boolean r2 = r1.a(r2, r3, r4, r5, r6, r7, r9, r10, r11)     // Catch:{ Throwable -> 0x0190 }
        L_0x0148:
            if (r2 == 0) goto L_0x01b9
            r0 = r19
            android.content.Context r2 = r0.f3698a     // Catch:{ Throwable -> 0x01a5 }
            com.tencent.feedback.eup.i r2 = com.tencent.feedback.eup.i.a(r2)     // Catch:{ Throwable -> 0x01a5 }
            if (r2 == 0) goto L_0x016b
            com.tencent.feedback.eup.d r3 = com.tencent.feedback.eup.c.a()     // Catch:{ Throwable -> 0x01a5 }
            boolean r2 = r2.a(r13, r3)     // Catch:{ Throwable -> 0x01a5 }
            java.lang.String r3 = "rqdp{  eup save} %b"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x01a5 }
            r5 = 0
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ Throwable -> 0x01a5 }
            r4[r5] = r2     // Catch:{ Throwable -> 0x01a5 }
            com.tencent.feedback.common.g.e(r3, r4)     // Catch:{ Throwable -> 0x01a5 }
        L_0x016b:
            if (r1 == 0) goto L_0x00e2
            java.lang.String r2 = "your crhanlde end"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x017b }
            com.tencent.feedback.common.g.b(r2, r3)     // Catch:{ Throwable -> 0x017b }
            r2 = 1
            r1.b(r2)     // Catch:{ Throwable -> 0x017b }
            goto L_0x00e2
        L_0x017b:
            r1 = move-exception
            java.lang.String r2 = "on native hanlde end throw %s"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r4 = 0
            java.lang.String r5 = r1.toString()
            r3[r4] = r5
            com.tencent.feedback.common.g.d(r2, r3)
            r1.printStackTrace()
            goto L_0x00e2
        L_0x0190:
            r2 = move-exception
            java.lang.String r3 = "on Crash Saving error %s"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            java.lang.String r6 = r2.toString()
            r4[r5] = r6
            com.tencent.feedback.common.g.d(r3, r4)
            r2.printStackTrace()
        L_0x01a3:
            r2 = r12
            goto L_0x0148
        L_0x01a5:
            r2 = move-exception
            java.lang.String r3 = "your crash handle happen error %s"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            java.lang.String r6 = r2.toString()
            r4[r5] = r6
            com.tencent.feedback.common.g.d(r3, r4)
            r2.printStackTrace()
            goto L_0x016b
        L_0x01b9:
            java.lang.String r2 = "the eup no need to save!"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]
            com.tencent.feedback.common.g.c(r2, r3)
            goto L_0x016b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.eup.jni.b.handleNativeException(int, int, long, long, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, java.lang.String, int, int, int, java.lang.String, java.lang.String):void");
    }
}
