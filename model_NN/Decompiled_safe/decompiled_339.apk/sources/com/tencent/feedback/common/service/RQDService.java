package com.tencent.feedback.common.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;
import com.tencent.feedback.common.g;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: ProGuard */
public class RQDService extends IntentService {

    /* renamed from: a  reason: collision with root package name */
    protected static AtomicInteger f3688a = new AtomicInteger(0);

    public RQDService() {
        super("RQDSERVICE");
    }

    public void onCreate() {
        g.f3681a = true;
        super.onCreate();
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        a(intent);
    }

    /* access modifiers changed from: protected */
    public void a(Intent intent) {
        a a2;
        String str;
        if ("com.tencent.feedback.10".equals(intent.getAction()) && (a2 = b.a(intent)) != null) {
            g.b("handle task %d %s", Integer.valueOf(a2.b()), a2.a());
            try {
                f3688a.addAndGet(1);
                a2.a(this, intent);
            } catch (Throwable th) {
                th.printStackTrace();
            } finally {
                f3688a.addAndGet(-1);
                str = "current pending %d";
                g.b(str, Integer.valueOf(f3688a.get()));
            }
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onDestroy() {
        g.b("service destory", new Object[0]);
        super.onDestroy();
    }
}
