package com.tencent.feedback.common;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;

/* compiled from: ProGuard */
public class PlugInInfo implements Parcelable, Serializable {
    public static final Parcelable.Creator<PlugInInfo> CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    public final String f3676a;
    public final String b;
    public final String c;

    public String toString() {
        return "plid:" + this.f3676a + " plV:" + this.b + " plUUID:" + this.c;
    }

    public PlugInInfo(Parcel parcel) {
        this.f3676a = parcel.readString();
        this.b = parcel.readString();
        this.c = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f3676a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
    }
}
