package com.tencent.feedback.common;

import android.content.Context;
import com.tencent.assistant.st.STConst;
import com.tencent.connect.common.Constants;
import com.tencent.feedback.proguard.ac;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public final class e {
    private static e D = null;
    private Map<String, PlugInInfo> A = null;
    private boolean B = true;
    private String C = null;

    /* renamed from: a  reason: collision with root package name */
    private final Context f3679a;
    private final String b;
    private final byte c;
    private final String d;
    private final String e;
    private final String f;
    private final String g;
    private final String h;
    private final String i;
    private final String j;
    private String k = "10000";
    private String l = STConst.ST_INSTALL_FAIL_STR_UNKNOWN;
    private long m = 0;
    private String n = Constants.STR_EMPTY;
    private String o = null;
    private String p = null;
    private String q = null;
    private String r = null;
    private String s = null;
    private String t = null;
    private String u = null;
    private long v = -1;
    private long w = -1;
    private long x = -1;
    private String y = null;
    private String z = null;

    private e(Context context) {
        g.e("rqdp{  init cominfo}", new Object[0]);
        this.f3679a = context.getApplicationContext();
        f.a(this.f3679a);
        this.c = 1;
        this.d = b.b(context);
        this.e = b.c(context);
        this.f = "com.tencent.feedback";
        this.g = "1.8.6.2";
        this.h = f.m();
        this.i = f.a();
        this.j = "Android " + f.b() + ",level " + f.c();
        this.b = this.i + ";" + this.j;
    }

    public static synchronized e a(Context context) {
        e eVar;
        synchronized (e.class) {
            if (D == null) {
                D = new e(context);
            }
            eVar = D;
        }
        return eVar;
    }

    public final String a() {
        return this.b;
    }

    public final byte b() {
        return 1;
    }

    public final String c() {
        return this.d;
    }

    public final String d() {
        return this.e;
    }

    public final String e() {
        return this.f;
    }

    public final String f() {
        return this.g;
    }

    public final String g() {
        return this.h;
    }

    public final String h() {
        return this.j;
    }

    public final synchronized String i() {
        return this.k;
    }

    public final synchronized void a(String str) {
        if (str == null) {
            str = "10000";
        }
        this.k = str;
    }

    public final synchronized String j() {
        return this.n;
    }

    public final synchronized void b(String str) {
        this.n = str;
        if (str != null) {
            a(false);
        }
    }

    public final synchronized String k() {
        return this.l;
    }

    public final synchronized void c(String str) {
        this.l = str;
    }

    public final synchronized long l() {
        return this.m;
    }

    public final synchronized void a(long j2) {
        this.m = j2;
    }

    public final synchronized String m() {
        if (this.o == null) {
            this.o = b.d(this.f3679a);
        }
        return this.o;
    }

    public final synchronized String n() {
        if (this.q == null) {
            this.q = b.g(this.f3679a);
        }
        return this.q;
    }

    public final synchronized void d(String str) {
        this.q = str;
    }

    public final synchronized String o() {
        if (this.r == null) {
            this.r = b.a(this.f3679a);
            if (this.r == null || this.r.equals(Constants.STR_EMPTY)) {
                this.r = this.d;
            }
        }
        return this.r;
    }

    public final synchronized String p() {
        String str;
        if (!A()) {
            str = Constants.STR_EMPTY;
        } else {
            if (this.p == null) {
                f.a(this.f3679a);
                this.p = f.b(this.f3679a);
            }
            str = this.p;
        }
        return str;
    }

    public final synchronized String q() {
        String str;
        if (!A()) {
            str = Constants.STR_EMPTY;
        } else {
            if (this.s == null) {
                f.a(this.f3679a);
                this.s = f.e(this.f3679a);
            }
            str = this.s;
        }
        return str;
    }

    public final synchronized String r() {
        String str;
        if (!A()) {
            str = Constants.STR_EMPTY;
        } else {
            if (this.t == null) {
                f.a(this.f3679a);
                this.t = f.c(this.f3679a);
            }
            str = this.t;
        }
        return str;
    }

    public final synchronized String s() {
        String str;
        if (!A()) {
            str = Constants.STR_EMPTY;
        } else {
            if (this.u == null) {
                f.a(this.f3679a);
                this.u = f.d(this.f3679a);
            }
            str = this.u;
        }
        return str;
    }

    public final synchronized long t() {
        if (this.v <= 0) {
            f.a(this.f3679a);
            this.v = f.f();
        }
        return this.v;
    }

    public final synchronized long u() {
        if (this.w <= 0) {
            f.a(this.f3679a);
            this.w = f.h();
        }
        return this.w;
    }

    public final synchronized long v() {
        if (this.x <= 0) {
            this.x = f.a(this.f3679a).j();
        }
        return this.x;
    }

    public final synchronized String w() {
        if (this.y == null) {
            f.a(this.f3679a);
            this.y = f.d();
        }
        return this.y;
    }

    public final synchronized String x() {
        if (this.z == null) {
            f.a(this.f3679a);
            this.z = ac.b("ro.board.platform");
        }
        return this.z;
    }

    private synchronized boolean A() {
        return this.B;
    }

    private synchronized void a(boolean z2) {
        this.B = false;
    }

    public final synchronized Map<String, PlugInInfo> y() {
        HashMap hashMap;
        if (this.A == null || this.A.size() <= 0) {
            hashMap = null;
        } else {
            hashMap = new HashMap(this.A.size());
            hashMap.putAll(this.A);
        }
        return hashMap;
    }

    public final String z() {
        if (this.C == null) {
            f.a(this.f3679a);
            this.C = f.l();
        }
        return this.C;
    }
}
