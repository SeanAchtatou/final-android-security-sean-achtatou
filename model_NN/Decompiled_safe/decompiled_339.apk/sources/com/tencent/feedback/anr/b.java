package com.tencent.feedback.anr;

import java.io.BufferedReader;
import java.util.regex.Pattern;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private String f3673a;
    private long b;
    private String c;
    private String d;
    private int e;

    public final String a() {
        return this.f3673a;
    }

    public final void a(String str) {
        this.f3673a = str;
    }

    public final long b() {
        return this.b;
    }

    public final void a(long j) {
        this.b = j;
    }

    public final int c() {
        return this.e;
    }

    public final void a(int i) {
        this.e = i;
    }

    public final String d() {
        return this.c;
    }

    public final void b(String str) {
        this.c = str;
    }

    public final String e() {
        return this.d;
    }

    public final void c(String str) {
        this.d = str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x015a, code lost:
        if (r13.a(java.lang.Long.parseLong(r0[1].toString().split("\\s")[2])) != false) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0161, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0162, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:?, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0176 A[SYNTHETIC, Splitter:B:57:0x0176] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.lang.String r12, com.tencent.feedback.anr.e r13) {
        /*
            if (r12 == 0) goto L_0x0004
            if (r13 != 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            java.io.File r0 = new java.io.File
            r0.<init>(r12)
            boolean r1 = r0.exists()
            if (r1 == 0) goto L_0x0004
            r0.lastModified()
            r0.length()
            r1 = 0
            java.io.BufferedReader r6 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0184, all -> 0x0172 }
            java.io.FileReader r2 = new java.io.FileReader     // Catch:{ Exception -> 0x0184, all -> 0x0172 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0184, all -> 0x0172 }
            r6.<init>(r2)     // Catch:{ Exception -> 0x0184, all -> 0x0172 }
            java.lang.String r0 = "-{5}\\spid\\s\\d+\\sat\\s\\d+-\\d+-\\d+\\s\\d{2}:\\d{2}:\\d{2}\\s-{5}"
            java.util.regex.Pattern r7 = java.util.regex.Pattern.compile(r0)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.String r0 = "-{5}\\send\\s\\d+\\s-{5}"
            java.util.regex.Pattern r8 = java.util.regex.Pattern.compile(r0)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.String r0 = "Cmd\\sline:\\s(\\S+)"
            java.util.regex.Pattern r9 = java.util.regex.Pattern.compile(r0)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.String r0 = "\".+\"\\s(daemon\\s){0,1}prio=\\d+\\stid=\\d+\\s.*"
            java.util.regex.Pattern r10 = java.util.regex.Pattern.compile(r0)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.text.SimpleDateFormat r11 = new java.text.SimpleDateFormat     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.String r0 = "yyyy-MM-dd HH:mm:ss"
            java.util.Locale r1 = java.util.Locale.US     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            r11.<init>(r0, r1)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
        L_0x0042:
            r0 = 1
            java.util.regex.Pattern[] r0 = new java.util.regex.Pattern[r0]     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            r1 = 0
            r0[r1] = r7     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.Object[] r0 = a(r6, r0)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            if (r0 == 0) goto L_0x0167
            r1 = 1
            r0 = r0[r1]     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.String r1 = "\\s"
            java.lang.String[] r0 = r0.split(r1)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            r1 = 2
            r1 = r0[r1]     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            long r1 = java.lang.Long.parseLong(r1)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            r3.<init>()     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            r4 = 4
            r4 = r0[r4]     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.String r4 = " "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            r4 = 5
            r0 = r0[r4]     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.util.Date r0 = r11.parse(r0)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            long r3 = r0.getTime()     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            r0 = 1
            java.util.regex.Pattern[] r0 = new java.util.regex.Pattern[r0]     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            r5 = 0
            r0[r5] = r9     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.Object[] r0 = a(r6, r0)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            if (r0 != 0) goto L_0x009e
            r6.close()     // Catch:{ IOException -> 0x0098 }
            goto L_0x0004
        L_0x0098:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0004
        L_0x009e:
            r5 = 1
            r0 = r0[r5]     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.util.regex.Matcher r0 = r9.matcher(r0)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            r0.find()     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            r5 = 1
            r0.group(r5)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            r5 = 1
            java.lang.String r5 = r0.group(r5)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            r0 = r13
            boolean r0 = r0.a(r1, r3, r5)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            if (r0 != 0) goto L_0x00c7
            r6.close()     // Catch:{ IOException -> 0x00c1 }
            goto L_0x0004
        L_0x00c1:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0004
        L_0x00c7:
            r0 = 2
            java.util.regex.Pattern[] r0 = new java.util.regex.Pattern[r0]     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            r1 = 0
            r0[r1] = r10     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            r1 = 1
            r0[r1] = r8     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.Object[] r0 = a(r6, r0)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            if (r0 == 0) goto L_0x0042
            r1 = 0
            r1 = r0[r1]     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            if (r1 != r10) goto L_0x0142
            r1 = 1
            r0 = r0[r1]     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.String r1 = "\".+\""
            java.util.regex.Pattern r1 = java.util.regex.Pattern.compile(r1)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.util.regex.Matcher r1 = r1.matcher(r0)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            r1.find()     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.String r1 = r1.group()     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            r2 = 1
            int r3 = r1.length()     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            int r3 = r3 + -1
            java.lang.String r1 = r1.substring(r2, r3)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.String r2 = "NATIVE"
            r0.contains(r2)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.String r2 = "tid=\\d+"
            java.util.regex.Pattern r2 = java.util.regex.Pattern.compile(r2)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.util.regex.Matcher r0 = r2.matcher(r0)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            r0.find()     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.String r0 = r0.group()     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.String r2 = "="
            int r2 = r0.indexOf(r2)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            int r2 = r2 + 1
            java.lang.String r0 = r0.substring(r2)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.String r2 = a(r6)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.String r3 = b(r6)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            r13.a(r1, r0, r2, r3)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            goto L_0x00c7
        L_0x0130:
            r0 = move-exception
            r1 = r6
        L_0x0132:
            r0.printStackTrace()     // Catch:{ all -> 0x0181 }
            if (r1 == 0) goto L_0x0004
            r1.close()     // Catch:{ IOException -> 0x013c }
            goto L_0x0004
        L_0x013c:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0004
        L_0x0142:
            r1 = 1
            r0 = r0[r1]     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            java.lang.String r1 = "\\s"
            java.lang.String[] r0 = r0.split(r1)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            r1 = 2
            r0 = r0[r1]     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            long r0 = java.lang.Long.parseLong(r0)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            boolean r0 = r13.a(r0)     // Catch:{ Exception -> 0x0130, all -> 0x017f }
            if (r0 != 0) goto L_0x0042
            r6.close()     // Catch:{ IOException -> 0x0161 }
            goto L_0x0004
        L_0x0161:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0004
        L_0x0167:
            r6.close()     // Catch:{ IOException -> 0x016c }
            goto L_0x0004
        L_0x016c:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0004
        L_0x0172:
            r0 = move-exception
            r6 = r1
        L_0x0174:
            if (r6 == 0) goto L_0x0179
            r6.close()     // Catch:{ IOException -> 0x017a }
        L_0x0179:
            throw r0
        L_0x017a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0179
        L_0x017f:
            r0 = move-exception
            goto L_0x0174
        L_0x0181:
            r0 = move-exception
            r6 = r1
            goto L_0x0174
        L_0x0184:
            r0 = move-exception
            goto L_0x0132
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.anr.b.a(java.lang.String, com.tencent.feedback.anr.e):void");
    }

    protected static Object[] a(BufferedReader bufferedReader, Pattern... patternArr) {
        if (bufferedReader == null || patternArr == null) {
            return null;
        }
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                return null;
            }
            int length = patternArr.length;
            int i = 0;
            while (true) {
                if (i < length) {
                    Pattern pattern = patternArr[i];
                    if (pattern.matcher(readLine).matches()) {
                        return new Object[]{pattern, readLine};
                    }
                    i++;
                }
            }
        }
    }

    protected static String a(BufferedReader bufferedReader) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < 3; i++) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                return null;
            }
            stringBuffer.append(readLine + "\n");
        }
        return stringBuffer.toString();
    }

    protected static String b(BufferedReader bufferedReader) {
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null && readLine.trim().length() > 0) {
                stringBuffer.append(readLine + "\n");
            }
        }
        return stringBuffer.toString();
    }
}
