package com.tencent.feedback.proguard;

import java.io.Serializable;

/* renamed from: com.tencent.feedback.proguard.j  reason: case insensitive filesystem */
/* compiled from: ProGuard */
public abstract class C0008j implements Serializable {
    public abstract void a(ag agVar);

    public abstract void a(ah ahVar);

    public abstract void a(StringBuilder sb, int i);

    public final byte[] a() {
        ah ahVar = new ah();
        a(ahVar);
        return ahVar.b();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        a(sb, 0);
        return sb.toString();
    }
}
