package com.tencent.feedback.proguard;

import android.content.Context;
import android.util.SparseArray;
import com.tencent.feedback.a.h;
import com.tencent.feedback.a.i;
import com.tencent.feedback.common.c;
import com.tencent.feedback.common.g;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public final class ao {

    /* renamed from: a  reason: collision with root package name */
    private static ao f3728a = null;
    private at b;
    private boolean c;
    private int d;
    private h e;
    private Runnable f;
    private List<an> g;
    private SparseArray<i> h;
    private List<av> i;

    public static synchronized ao a(Context context) {
        ao aoVar;
        synchronized (ao.class) {
            if (f3728a == null && context != null) {
                f3728a = new ao(context);
            }
            aoVar = f3728a;
        }
        return aoVar;
    }

    public static synchronized i a() {
        i iVar;
        synchronized (ao.class) {
            if (f3728a != null) {
                iVar = f3728a.f();
            } else {
                iVar = null;
            }
        }
        return iVar;
    }

    private ao(Context context) {
        this.b = null;
        this.c = false;
        this.d = 0;
        this.e = null;
        this.f = null;
        this.g = new ArrayList(5);
        this.h = new SparseArray<>(5);
        this.i = new ArrayList(5);
        this.b = new at();
        this.e = new am(context);
        this.f = new ar(context);
        c.a().a(this.f);
    }

    public final synchronized at b() {
        return this.b;
    }

    private synchronized i f() {
        i iVar;
        if (this.h == null || this.h.size() <= 0) {
            iVar = null;
        } else {
            iVar = this.h.valueAt(0);
        }
        return iVar;
    }

    public final synchronized void a(int i2, i iVar) {
        if (this.h != null) {
            if (iVar == null) {
                this.h.remove(i2);
            } else {
                this.h.put(i2, iVar);
                iVar.a(300, c());
            }
        }
    }

    public final synchronized h c() {
        return this.e;
    }

    private synchronized boolean g() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public final synchronized void a(boolean z) {
        this.c = true;
        g.f("rqdp{  isFirst }%b", true);
    }

    public final synchronized an[] d() {
        an[] anVarArr;
        if (this.g == null || this.g.size() <= 0) {
            anVarArr = null;
        } else {
            anVarArr = (an[]) this.g.toArray(new an[0]);
        }
        return anVarArr;
    }

    private synchronized av[] h() {
        av[] avVarArr;
        if (this.i == null || this.i.size() <= 0) {
            avVarArr = null;
        } else {
            avVarArr = (av[]) this.i.toArray(new av[0]);
        }
        return avVarArr;
    }

    public final synchronized int e() {
        return this.d;
    }

    public final synchronized void a(int i2) {
        this.d = i2;
        g.f("rqdp{  step }%d", Integer.valueOf(i2));
    }

    public final synchronized void a(an anVar) {
        if (anVar != null) {
            if (this.g == null) {
                this.g = new ArrayList();
            }
            if (!this.g.contains(anVar)) {
                this.g.add(anVar);
                int e2 = e();
                if (g()) {
                    g.e("rqdp{  add listener should notify app first run! }%s", anVar.toString());
                    c.a().a(new ap(this, anVar));
                }
                if (e2 >= 2) {
                    g.e("rqdp{  add listener should notify app start query!} %s", anVar.toString());
                    c.a().a(new aq(this, anVar, e2));
                }
            }
        }
    }

    public final synchronized void a(av avVar) {
        if (avVar != null) {
            if (this.i != null && !this.i.contains(avVar)) {
                this.i.add(avVar);
            }
        }
    }

    public final void a(at atVar) {
        av[] h2 = h();
        if (h2 != null) {
            for (av a2 : h2) {
                try {
                    a2.a(atVar);
                } catch (Throwable th) {
                    th.printStackTrace();
                    g.d("rqdp{  com strategy changed error }%s", th.toString());
                }
            }
        }
    }
}
