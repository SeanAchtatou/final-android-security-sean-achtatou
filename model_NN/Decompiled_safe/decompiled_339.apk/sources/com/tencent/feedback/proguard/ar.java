package com.tencent.feedback.proguard;

import android.content.Context;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.st.STConstAction;
import com.tencent.feedback.a.h;
import com.tencent.feedback.a.i;
import com.tencent.feedback.common.b;
import com.tencent.feedback.common.c;
import com.tencent.feedback.common.e;
import com.tencent.feedback.common.g;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public final class ar implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private static long f3731a = 0;
    private Context b = null;

    public ar(Context context) {
        this.b = context;
    }

    private static synchronized long a() {
        long j;
        synchronized (ar.class) {
            j = f3731a;
        }
        return j;
    }

    private long b() {
        ba a2 = ac.a(this.b, 300);
        if (!(a2 == null || a2.b() != 300 || a2.c() == null)) {
            try {
                h c = ao.a(this.b).c();
                if (c != null) {
                    c.a(300, a2.c(), false);
                    g.e("rqdp{  common strategy setted by history}", new Object[0]);
                }
                return a2.d();
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return -1;
    }

    private void a(long j, long j2) {
        B b2;
        B b3 = null;
        int i = 0;
        i a2 = ao.a();
        aw a3 = aw.a(this.b, c.a(), a2);
        boolean b4 = a3.b();
        if (!b4) {
            a3.a(60000L);
        }
        if (new Date().getTime() < j + j2) {
            g.a("lastUpdate:%d ,return not query", Long.valueOf(j));
            return;
        }
        i iVar = a2;
        int i2 = 0;
        while (iVar == null) {
            int i3 = i2 + 1;
            if (i3 >= 5) {
                break;
            }
            g.e("rqdp{  wait uphandler:} %d", 200);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            iVar = ao.a();
            i2 = i3;
        }
        if (iVar == null || !com.tencent.feedback.common.i.b(this.b)) {
            g.h("rqdp{  no uphandler or offline ,not query!!}", new Object[0]);
            return;
        }
        if (!b4) {
            try {
                if (a3.b(b.h(this.b)) <= 0) {
                    g.e("rqdp{  wait lanch record:} %d", Integer.valueOf((int) STConstAction.ACTION_HIT_OPEN));
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        Y a4 = a.a(this.b, a3, (byte) 2);
        if (a4 != null) {
            HashMap hashMap = new HashMap();
            hashMap.put(Integer.valueOf((int) NormalErrorRecommendPage.ERROR_TYPE_SEARCH_RESULT_EMPTY), a4.a());
            b2 = new B();
            b2.f3702a = hashMap;
            Object[] objArr = new Object[1];
            if (a4.f != null) {
                i = a4.f.size();
            }
            objArr[0] = Integer.valueOf(i);
            g.b("rqdp{ common query add uin} %d", objArr);
        } else {
            b2 = null;
        }
        b3 = b2;
        iVar.a(new as(this.b, EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLIST_FAIL, 200, b3));
    }

    private void c() {
        an[] d;
        g.e("rqdp{  AppFirstRun } %s", e.a(this.b).m());
        g.e("rqdp{  clear ao count} %d", Integer.valueOf(aj.a(this.b, null, -1, Long.MAX_VALUE, -1, -1)));
        ao a2 = ao.a(this.b);
        synchronized (a2) {
            d = a2.d();
            ao.a(this.b).a(true);
        }
        if (d != null) {
            for (an f : d) {
                f.f();
            }
        }
    }

    public final void run() {
        an[] d;
        an[] d2;
        boolean z;
        long b2 = b();
        ao a2 = ao.a(this.b);
        at b3 = a2.b();
        long c = b3 == null ? -1 : (long) (b3.c() * 3600 * 1000);
        if (a2.e() == 0) {
            g.e("rqdp{  onlaunch}", new Object[0]);
            a2.a(1);
            String a3 = b.a(this.b, e.a(this.b).c());
            if (a3 == null || a3.trim().length() == 0) {
                g.d("not found apk %s", a3);
                z = false;
            } else {
                File file = new File(a3);
                if (!file.exists() || !file.canRead()) {
                    g.d("apk not exist or read %s", a3);
                    z = false;
                } else {
                    long lastModified = file.lastModified();
                    long length = file.length();
                    String d3 = e.a(this.b).d();
                    List<al> a4 = aj.a(this.b, a3, 0, 10);
                    al alVar = null;
                    if (a4 != null && a4.size() > 0) {
                        alVar = a4.get(0);
                    }
                    if (alVar == null || alVar.d() == null || !d3.equals(alVar.f()) || lastModified != alVar.b() || length != alVar.c()) {
                        String n = e.a(this.b).n();
                        if (n == null) {
                            g.d("rqdp{  apkid get error!return false}", new Object[0]);
                            z = false;
                        } else {
                            if (alVar != null) {
                                g.g("rqdp{  deleted old apkids} %d", Integer.valueOf(aj.d(this.b, a4)));
                            }
                            ArrayList arrayList = new ArrayList();
                            al alVar2 = new al();
                            alVar2.a(0);
                            alVar2.a(a3);
                            alVar2.b(n);
                            alVar2.c(d3);
                            alVar2.b(lastModified);
                            alVar2.c(length);
                            arrayList.add(alVar2);
                            aj.c(this.b, arrayList);
                            g.g("rqdp{  insert new app record:} %s ", alVar2.toString());
                            z = true;
                        }
                    } else {
                        e.a(this.b).d(alVar.d());
                        z = false;
                    }
                }
            }
            if (z) {
                c();
            } else {
                long a5 = a();
                if (a5 > 0) {
                    g.e("rqdp{  delay:} %d", Long.valueOf(a5));
                    try {
                        Thread.sleep(a5);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        g.e("rqdp{  on Query Start}", new Object[0]);
        synchronized (a2) {
            d = a2.d();
            a2.a(2);
        }
        if (d != null) {
            g.e("rqdp{  notify Query Start}", new Object[0]);
            for (an d4 : d) {
                d4.d();
            }
        }
        a(b2, c);
        g.e("rqdp{  on query end!}", new Object[0]);
        synchronized (a2) {
            d2 = a2.d();
            a2.a(3);
        }
        if (d2 != null) {
            g.e("rqdp{  notify Query end}", new Object[0]);
            for (an e2 : d2) {
                e2.e();
            }
        }
        if (c <= 0) {
            g.c("periodLimit %d return", Long.valueOf(c));
        } else if (c > 0) {
            c.a().a(this, c);
            g.h("rqdp{  next time} %d", Long.valueOf(c));
            ao.a(this.b).a(4);
        } else {
            g.h("rqdp{  stop loop query}", new Object[0]);
        }
    }
}
