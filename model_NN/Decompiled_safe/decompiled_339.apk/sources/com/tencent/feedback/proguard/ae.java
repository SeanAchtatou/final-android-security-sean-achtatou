package com.tencent.feedback.proguard;

import com.tencent.connect.common.Constants;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public final class ae extends ad {
    private static HashMap<String, byte[]> f = null;
    private static HashMap<String, HashMap<String, byte[]>> g = null;
    private C0006f e = new C0006f();

    public ae() {
        this.e.f3739a = 2;
    }

    public final <T> void a(String str, Object obj) {
        if (str.startsWith(".")) {
            throw new IllegalArgumentException("put name can not startwith . , now is " + str);
        }
        super.a(str, obj);
    }

    public final void d() {
        super.d();
        this.e.f3739a = 3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
     arg types: [java.util.HashMap, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void */
    public final byte[] a() {
        if (this.e.f3739a != 2) {
            if (this.e.c == null) {
                this.e.c = Constants.STR_EMPTY;
            }
            if (this.e.d == null) {
                this.e.d = Constants.STR_EMPTY;
            }
        } else if (this.e.c.equals(Constants.STR_EMPTY)) {
            throw new IllegalArgumentException("servantName can not is null");
        } else if (this.e.d.equals(Constants.STR_EMPTY)) {
            throw new IllegalArgumentException("funcName can not is null");
        }
        ah ahVar = new ah(0);
        ahVar.a(this.b);
        if (this.e.f3739a == 2) {
            ahVar.a((Map) this.f3720a, 0);
        } else {
            ahVar.a((Map) this.d, 0);
        }
        this.e.e = ai.a(ahVar.a());
        ah ahVar2 = new ah(0);
        ahVar2.a(this.b);
        this.e.a(ahVar2);
        byte[] a2 = ai.a(ahVar2.a());
        int length = a2.length;
        ByteBuffer allocate = ByteBuffer.allocate(length + 4);
        allocate.putInt(length + 4).put(a2).flip();
        return allocate.array();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
     arg types: [java.util.HashMap<java.lang.String, byte[]>, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
     arg types: [java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, byte[]>>, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V> */
    public final void a(byte[] bArr) {
        if (bArr.length < 4) {
            throw new IllegalArgumentException("decode package must include size head");
        }
        try {
            ag agVar = new ag(bArr, 4);
            agVar.a(this.b);
            this.e.a(agVar);
            if (this.e.f3739a == 3) {
                ag agVar2 = new ag(this.e.e);
                agVar2.a(this.b);
                if (f == null) {
                    HashMap<String, byte[]> hashMap = new HashMap<>();
                    f = hashMap;
                    hashMap.put(Constants.STR_EMPTY, new byte[0]);
                }
                this.d = agVar2.a((Map) f, 0, false);
                return;
            }
            ag agVar3 = new ag(this.e.e);
            agVar3.a(this.b);
            if (g == null) {
                g = new HashMap<>();
                HashMap hashMap2 = new HashMap();
                hashMap2.put(Constants.STR_EMPTY, new byte[0]);
                g.put(Constants.STR_EMPTY, hashMap2);
            }
            this.f3720a = agVar3.a((Map) g, 0, false);
            new HashMap();
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }

    public final void d(String str) {
        this.e.c = str;
    }

    public final void e(String str) {
        this.e.d = str;
    }

    public final void a(int i) {
        this.e.b = 1;
    }
}
