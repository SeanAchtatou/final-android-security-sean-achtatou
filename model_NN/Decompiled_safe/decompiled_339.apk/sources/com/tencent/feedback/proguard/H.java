package com.tencent.feedback.proguard;

import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Collection;

public final class H extends C0008j {
    private static G A;
    private static ArrayList<E> B;
    private static ArrayList<K> C;
    private static ArrayList<F> x;
    private static J y;
    private static L z;

    /* renamed from: a  reason: collision with root package name */
    public String f3708a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    public long d = 0;
    public ArrayList<F> e = null;
    public String f = Constants.STR_EMPTY;
    public String g = Constants.STR_EMPTY;
    public String h = Constants.STR_EMPTY;
    public J i = null;
    public L j = null;
    public G k = null;
    public ArrayList<E> l = null;
    public long m = 0;
    public String n = Constants.STR_EMPTY;
    public String o = Constants.STR_EMPTY;
    public String p = Constants.STR_EMPTY;
    public String q = Constants.STR_EMPTY;
    public ArrayList<K> r = null;
    public String s = Constants.STR_EMPTY;
    private String t = Constants.STR_EMPTY;
    private String u = Constants.STR_EMPTY;
    private String v = Constants.STR_EMPTY;
    private String w = Constants.STR_EMPTY;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.ArrayList<com.tencent.feedback.proguard.F>, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
     arg types: [com.tencent.feedback.proguard.J, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
     arg types: [com.tencent.feedback.proguard.L, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
     arg types: [com.tencent.feedback.proguard.G, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.ArrayList<com.tencent.feedback.proguard.E>, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.ArrayList<com.tencent.feedback.proguard.K>, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object */
    public final void a(ag agVar) {
        this.f3708a = agVar.b(0, true);
        this.b = agVar.b(1, false);
        this.c = agVar.b(2, false);
        this.d = agVar.a(this.d, 3, true);
        if (x == null) {
            x = new ArrayList<>();
            x.add(new F());
        }
        this.e = (ArrayList) agVar.a((Object) x, 4, true);
        this.f = agVar.b(5, false);
        this.g = agVar.b(6, false);
        this.h = agVar.b(7, false);
        if (y == null) {
            y = new J();
        }
        this.i = (J) agVar.a((C0008j) y, 8, false);
        if (z == null) {
            z = new L();
        }
        this.j = (L) agVar.a((C0008j) z, 9, false);
        if (A == null) {
            A = new G();
        }
        this.k = (G) agVar.a((C0008j) A, 10, false);
        this.t = agVar.b(11, false);
        if (B == null) {
            B = new ArrayList<>();
            B.add(new E());
        }
        this.l = (ArrayList) agVar.a((Object) B, 12, false);
        this.m = agVar.a(this.m, 13, false);
        this.u = agVar.b(14, false);
        this.n = agVar.b(15, false);
        this.o = agVar.b(16, false);
        this.p = agVar.b(17, false);
        this.q = agVar.b(18, false);
        this.v = agVar.b(19, false);
        this.w = agVar.b(20, false);
        if (C == null) {
            C = new ArrayList<>();
            C.add(new K());
        }
        this.r = (ArrayList) agVar.a((Object) C, 21, false);
        this.s = agVar.b(22, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
     arg types: [java.util.ArrayList<com.tencent.feedback.proguard.F>, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
     arg types: [com.tencent.feedback.proguard.J, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
     arg types: [com.tencent.feedback.proguard.L, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
     arg types: [com.tencent.feedback.proguard.G, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
     arg types: [java.util.ArrayList<com.tencent.feedback.proguard.E>, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
     arg types: [java.util.ArrayList<com.tencent.feedback.proguard.K>, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void */
    public final void a(ah ahVar) {
        ahVar.a(this.f3708a, 0);
        if (this.b != null) {
            ahVar.a(this.b, 1);
        }
        if (this.c != null) {
            ahVar.a(this.c, 2);
        }
        ahVar.a(this.d, 3);
        ahVar.a((Collection) this.e, 4);
        if (this.f != null) {
            ahVar.a(this.f, 5);
        }
        if (this.g != null) {
            ahVar.a(this.g, 6);
        }
        if (this.h != null) {
            ahVar.a(this.h, 7);
        }
        if (this.i != null) {
            ahVar.a((C0008j) this.i, 8);
        }
        if (this.j != null) {
            ahVar.a((C0008j) this.j, 9);
        }
        if (this.k != null) {
            ahVar.a((C0008j) this.k, 10);
        }
        if (this.t != null) {
            ahVar.a(this.t, 11);
        }
        if (this.l != null) {
            ahVar.a((Collection) this.l, 12);
        }
        ahVar.a(this.m, 13);
        if (this.u != null) {
            ahVar.a(this.u, 14);
        }
        if (this.n != null) {
            ahVar.a(this.n, 15);
        }
        if (this.o != null) {
            ahVar.a(this.o, 16);
        }
        if (this.p != null) {
            ahVar.a(this.p, 17);
        }
        if (this.q != null) {
            ahVar.a(this.q, 18);
        }
        if (this.v != null) {
            ahVar.a(this.v, 19);
        }
        if (this.w != null) {
            ahVar.a(this.w, 20);
        }
        if (this.r != null) {
            ahVar.a((Collection) this.r, 21);
        }
        if (this.s != null) {
            ahVar.a(this.s, 22);
        }
    }

    public final void a(StringBuilder sb, int i2) {
    }
}
