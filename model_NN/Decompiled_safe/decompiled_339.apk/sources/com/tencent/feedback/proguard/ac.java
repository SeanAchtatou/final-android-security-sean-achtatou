package com.tencent.feedback.proguard;

import android.content.Context;
import com.tencent.connect.common.Constants;
import com.tencent.feedback.common.e;
import com.tencent.feedback.common.g;
import com.tencent.feedback.eup.a;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

/* compiled from: ProGuard */
public class ac {

    /* renamed from: a  reason: collision with root package name */
    protected HashMap<String, HashMap<String, byte[]>> f3720a = new HashMap<>();
    protected String b;
    ag c;
    private HashMap<String, Object> d;

    public static int a(Context context, p[] pVarArr) {
        if (context == null || pVarArr == null || pVarArr.length <= 0) {
            return -1;
        }
        ArrayList arrayList = new ArrayList(pVarArr.length);
        for (p pVar : pVarArr) {
            byte[] a2 = a(pVar);
            if (a2 != null) {
                aj ajVar = new aj(7, 0, 0, a2);
                ajVar.a(pVar.a());
                arrayList.add(ajVar);
            }
        }
        if (arrayList.size() <= 0) {
            return 0;
        }
        if (aj.b(context, arrayList)) {
            return arrayList.size();
        }
        return -1;
    }

    ac() {
        new HashMap();
        this.d = new HashMap<>();
        this.b = "GBK";
        this.c = new ag();
    }

    /* JADX INFO: finally extract failed */
    public static byte[] a(String str, int i) {
        g.a("rqdp{  LogcatManager.getLogDatas() start + count:}" + i, new Object[0]);
        ArrayList arrayList = new ArrayList();
        arrayList.clear();
        arrayList.add("logcat");
        arrayList.add("-d");
        arrayList.add("-v");
        arrayList.add("time");
        if (str != null && str.length() > 0) {
            arrayList.add("-s");
            arrayList.add(str);
        }
        String[] strArr = new String[arrayList.size()];
        arrayList.toArray(strArr);
        a aVar = new a(i);
        a(strArr, aVar);
        if (aVar.size() <= 0) {
            return null;
        }
        g.a("rqdp{  get log success in list size:}" + aVar.size(), new Object[0]);
        try {
            StringBuffer stringBuffer = new StringBuffer();
            Iterator it = aVar.iterator();
            while (it.hasNext()) {
                stringBuffer.append(((String) it.next()) + "\n");
            }
            aVar.clear();
            byte[] bytes = stringBuffer.toString().getBytes("utf-8");
            stringBuffer.setLength(0);
            g.a("rqdp{  LogcatManager.getLogDatas() end}", new Object[0]);
            return bytes;
        } catch (Throwable th) {
            g.a("rqdp{  LogcatManager.getLogDatas() end}", new Object[0]);
            throw th;
        }
    }

    public static Long[] a(LinkedHashMap<Long, Long> linkedHashMap, long j) {
        long j2 = 0;
        if (linkedHashMap == null || linkedHashMap.size() <= 0 || j <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        Iterator<Long> it = linkedHashMap.keySet().iterator();
        while (true) {
            long j3 = j2;
            if (it.hasNext() && j3 < j) {
                long longValue = it.next().longValue();
                long longValue2 = linkedHashMap.get(Long.valueOf(longValue)).longValue();
                if (j3 + longValue2 <= j) {
                    arrayList.add(Long.valueOf(longValue));
                    j2 = j3 + longValue2;
                } else {
                    j2 = j3;
                }
            }
        }
        if (arrayList.size() > 0) {
            return (Long[]) arrayList.toArray(new Long[1]);
        }
        return null;
    }

    public static synchronized int a(Context context, q[] qVarArr) {
        int i;
        synchronized (ac.class) {
            if (!(context == null || qVarArr == null)) {
                if (qVarArr.length > 0) {
                    ArrayList arrayList = new ArrayList(qVarArr.length);
                    for (q qVar : qVarArr) {
                        byte[] a2 = a(qVar);
                        if (a2 == null) {
                            g.c("rqdp{ getSerData error }", new Object[0]);
                        } else {
                            aj ajVar = new aj(9, 0, qVar.a(), a2);
                            ajVar.a(qVar.c());
                            arrayList.add(ajVar);
                        }
                    }
                    i = (arrayList.size() <= 0 || !aj.a(context, arrayList)) ? 0 : arrayList.size();
                }
            }
            g.c("rqdp{  args error}", new Object[0]);
            i = 0;
        }
        return i;
    }

    public static List<p> a(Context context) {
        List<aj> a2;
        if (context == null || (a2 = aj.a(context, new int[]{7}, -1, -1, Long.MAX_VALUE, 5, null, -1, -1, -1, -1, -1, Long.MAX_VALUE)) == null || a2.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(a2.size());
        for (aj next : a2) {
            try {
                p cast = p.class.cast(b(next.b()));
                cast.a(next.a());
                arrayList.add(cast);
            } catch (Throwable th) {
                th.printStackTrace();
                g.d("rqdp{  netconsume error }%s", th.toString());
            }
        }
        return arrayList;
    }

    public static synchronized int b(Context context, q[] qVarArr) {
        int i = 0;
        synchronized (ac.class) {
            if (!(context == null || qVarArr == null)) {
                if (qVarArr.length > 0) {
                    ArrayList arrayList = new ArrayList(qVarArr.length);
                    for (q qVar : qVarArr) {
                        if (qVar.d() >= 0) {
                            arrayList.add(Long.valueOf(qVar.d()));
                        }
                    }
                    if (arrayList.size() > 0) {
                        i = aj.a(context, (Long[]) arrayList.toArray(new Long[0]));
                    }
                }
            }
            g.c("rqdp{  args error}", new Object[0]);
        }
        return i;
    }

    public static boolean a(String[] strArr, a<String> aVar) {
        if (strArr == null || strArr.length <= 0) {
            return false;
        }
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(strArr);
            if (aVar != null) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    aVar.add(readLine);
                }
                bufferedReader.close();
            } else {
                process.waitFor();
            }
            if (process != null) {
                try {
                    process.getOutputStream().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    process.getInputStream().close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                try {
                    process.getErrorStream().close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            }
            return true;
        } catch (Throwable th) {
            if (process != null) {
                try {
                    process.getOutputStream().close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                try {
                    process.getInputStream().close();
                } catch (IOException e5) {
                    e5.printStackTrace();
                }
                try {
                    process.getErrorStream().close();
                } catch (IOException e6) {
                    e6.printStackTrace();
                }
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0067 A[SYNTHETIC, Splitter:B:35:0x0067] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(java.lang.Object r5) {
        /*
            r0 = 0
            r3 = 0
            java.lang.String r1 = "rqdp{  en obj 2 bytes}"
            java.lang.Object[] r2 = new java.lang.Object[r3]
            com.tencent.feedback.common.g.b(r1, r2)
            if (r5 == 0) goto L_0x0013
            java.lang.Class<java.io.Serializable> r1 = java.io.Serializable.class
            boolean r1 = r1.isInstance(r5)
            if (r1 != 0) goto L_0x001b
        L_0x0013:
            java.lang.String r1 = "rqdp{  not serial obj}"
            java.lang.Object[] r2 = new java.lang.Object[r3]
            com.tencent.feedback.common.g.c(r1, r2)
        L_0x001a:
            return r0
        L_0x001b:
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream
            r3.<init>()
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ Throwable -> 0x0040, all -> 0x0062 }
            r2.<init>(r3)     // Catch:{ Throwable -> 0x0040, all -> 0x0062 }
            r2.writeObject(r5)     // Catch:{ Throwable -> 0x007a }
            r2.flush()     // Catch:{ Throwable -> 0x007a }
            byte[] r0 = r3.toByteArray()     // Catch:{ Throwable -> 0x007a }
            r2.close()     // Catch:{ IOException -> 0x003b }
        L_0x0032:
            r3.close()     // Catch:{ IOException -> 0x0036 }
            goto L_0x001a
        L_0x0036:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001a
        L_0x003b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0032
        L_0x0040:
            r1 = move-exception
            r2 = r0
        L_0x0042:
            r1.printStackTrace()     // Catch:{ all -> 0x0078 }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0078 }
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0078 }
            com.tencent.feedback.common.g.d(r1, r4)     // Catch:{ all -> 0x0078 }
            if (r2 == 0) goto L_0x0054
            r2.close()     // Catch:{ IOException -> 0x005d }
        L_0x0054:
            r3.close()     // Catch:{ IOException -> 0x0058 }
            goto L_0x001a
        L_0x0058:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001a
        L_0x005d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0054
        L_0x0062:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0065:
            if (r2 == 0) goto L_0x006a
            r2.close()     // Catch:{ IOException -> 0x006e }
        L_0x006a:
            r3.close()     // Catch:{ IOException -> 0x0073 }
        L_0x006d:
            throw r0
        L_0x006e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006a
        L_0x0073:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006d
        L_0x0078:
            r0 = move-exception
            goto L_0x0065
        L_0x007a:
            r1 = move-exception
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.ac.a(java.lang.Object):byte[]");
    }

    public static String a(ArrayList<String> arrayList) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < arrayList.size(); i++) {
            String str = arrayList.get(i);
            if (str.equals("java.lang.Integer") || str.equals("int")) {
                str = "int32";
            } else if (str.equals("java.lang.Boolean") || str.equals("boolean")) {
                str = "bool";
            } else if (str.equals("java.lang.Byte") || str.equals("byte")) {
                str = "char";
            } else if (str.equals("java.lang.Double") || str.equals("double")) {
                str = "double";
            } else if (str.equals("java.lang.Float") || str.equals("float")) {
                str = "float";
            } else if (str.equals("java.lang.Long") || str.equals("long")) {
                str = "int64";
            } else if (str.equals("java.lang.Short") || str.equals("short")) {
                str = "short";
            } else if (str.equals("java.lang.Character")) {
                throw new IllegalArgumentException("can not support java.lang.Character");
            } else if (str.equals("java.lang.String")) {
                str = "string";
            } else if (str.equals("java.util.List")) {
                str = "list";
            } else if (str.equals("java.util.Map")) {
                str = "map";
            }
            arrayList.set(i, str);
        }
        Collections.reverse(arrayList);
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            String str2 = arrayList.get(i2);
            if (str2.equals("list")) {
                arrayList.set(i2 - 1, "<" + arrayList.get(i2 - 1));
                arrayList.set(0, arrayList.get(0) + ">");
            } else if (str2.equals("map")) {
                arrayList.set(i2 - 1, "<" + arrayList.get(i2 - 1) + ",");
                arrayList.set(0, arrayList.get(0) + ">");
            } else if (str2.equals("Array")) {
                arrayList.set(i2 - 1, "<" + arrayList.get(i2 - 1));
                arrayList.set(0, arrayList.get(0) + ">");
            }
        }
        Collections.reverse(arrayList);
        Iterator<String> it = arrayList.iterator();
        while (it.hasNext()) {
            stringBuffer.append(it.next());
        }
        return stringBuffer.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
     arg types: [T, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void */
    public <T> void a(String str, T t) {
        if (str == null) {
            throw new IllegalArgumentException("put key can not is null");
        } else if (t == null) {
            throw new IllegalArgumentException("put value can not is null");
        } else if (t instanceof Set) {
            throw new IllegalArgumentException("can not support Set");
        } else {
            ah ahVar = new ah();
            ahVar.a(this.b);
            ahVar.a((Object) t, 0);
            byte[] a2 = ai.a(ahVar.a());
            HashMap hashMap = new HashMap(1);
            ArrayList arrayList = new ArrayList(1);
            a(arrayList, t);
            hashMap.put(a((ArrayList<String>) arrayList), a2);
            this.d.remove(str);
            this.f3720a.put(str, hashMap);
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: com.tencent.feedback.proguard.n} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v5, resolved type: android.content.ContentValues} */
    /* JADX WARN: Type inference failed for: r3v0 */
    /* JADX WARN: Type inference failed for: r3v2 */
    /* JADX WARN: Type inference failed for: r3v3 */
    /* JADX WARN: Type inference failed for: r3v12 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:78:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r10, com.tencent.feedback.proguard.ba r11) {
        /*
            r8 = 0
            r3 = 0
            r1 = 1
            r0 = 0
            if (r10 == 0) goto L_0x0009
            if (r11 != 0) goto L_0x0011
        L_0x0009:
            java.lang.String r1 = "rqdp{  context == null || bean == null}"
            java.lang.Object[] r2 = new java.lang.Object[r0]
            com.tencent.feedback.common.g.c(r1, r2)
        L_0x0010:
            return r0
        L_0x0011:
            com.tencent.feedback.proguard.n r4 = new com.tencent.feedback.proguard.n     // Catch:{ Throwable -> 0x0111, all -> 0x00f5 }
            r4.<init>(r10)     // Catch:{ Throwable -> 0x0111, all -> 0x00f5 }
            android.database.sqlite.SQLiteDatabase r2 = r4.getWritableDatabase()     // Catch:{ Throwable -> 0x0114, all -> 0x0109 }
            if (r2 != 0) goto L_0x0033
            java.lang.String r1 = "get db fail!,return "
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            com.tencent.feedback.common.g.d(r1, r3)     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            if (r2 == 0) goto L_0x002f
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x002f
            r2.close()
        L_0x002f:
            r4.close()
            goto L_0x0010
        L_0x0033:
            if (r11 != 0) goto L_0x005a
        L_0x0035:
            if (r3 == 0) goto L_0x00e5
            java.lang.String r5 = "strategy"
            java.lang.String r6 = "_id"
            long r5 = r2.replace(r5, r6, r3)     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            int r3 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r3 >= 0) goto L_0x00be
            java.lang.String r1 = "rqdp{  insert failure! return}"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            com.tencent.feedback.common.g.c(r1, r3)     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            if (r2 == 0) goto L_0x0056
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x0056
            r2.close()
        L_0x0056:
            r4.close()
            goto L_0x0010
        L_0x005a:
            android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            r3.<init>()     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            long r5 = r11.a()     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            int r5 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r5 < 0) goto L_0x0074
            java.lang.String r5 = "_id"
            long r6 = r11.a()     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
        L_0x0074:
            java.lang.String r5 = "_key"
            int r6 = r11.b()     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            java.lang.String r5 = "_datas"
            byte[] r6 = r11.c()     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            java.lang.String r5 = "_ut"
            long r6 = r11.d()     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            goto L_0x0035
        L_0x0098:
            r1 = move-exception
            r3 = r4
        L_0x009a:
            java.lang.String r4 = "rqdp{  Error strategy update!} %s"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x010e }
            r6 = 0
            java.lang.String r7 = r1.toString()     // Catch:{ all -> 0x010e }
            r5[r6] = r7     // Catch:{ all -> 0x010e }
            com.tencent.feedback.common.g.d(r4, r5)     // Catch:{ all -> 0x010e }
            r1.printStackTrace()     // Catch:{ all -> 0x010e }
            if (r2 == 0) goto L_0x00b7
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x00b7
            r2.close()
        L_0x00b7:
            if (r3 == 0) goto L_0x0010
            r3.close()
            goto L_0x0010
        L_0x00be:
            r11.a(r5)     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            java.lang.String r3 = "rqdp{  update strategy} %d rqdp{  true}"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            r6 = 0
            int r7 = r11.b()     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            r5[r6] = r7     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            com.tencent.feedback.common.g.e(r3, r5)     // Catch:{ Throwable -> 0x0098, all -> 0x010c }
            if (r2 == 0) goto L_0x00df
            boolean r0 = r2.isOpen()
            if (r0 == 0) goto L_0x00df
            r2.close()
        L_0x00df:
            r4.close()
            r0 = r1
            goto L_0x0010
        L_0x00e5:
            if (r2 == 0) goto L_0x00f0
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x00f0
            r2.close()
        L_0x00f0:
            r4.close()
            goto L_0x0010
        L_0x00f5:
            r0 = move-exception
            r2 = r3
            r4 = r3
        L_0x00f8:
            if (r2 == 0) goto L_0x0103
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x0103
            r2.close()
        L_0x0103:
            if (r4 == 0) goto L_0x0108
            r4.close()
        L_0x0108:
            throw r0
        L_0x0109:
            r0 = move-exception
            r2 = r3
            goto L_0x00f8
        L_0x010c:
            r0 = move-exception
            goto L_0x00f8
        L_0x010e:
            r0 = move-exception
            r4 = r3
            goto L_0x00f8
        L_0x0111:
            r1 = move-exception
            r2 = r3
            goto L_0x009a
        L_0x0114:
            r1 = move-exception
            r2 = r3
            r3 = r4
            goto L_0x009a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.ac.a(android.content.Context, com.tencent.feedback.proguard.ba):boolean");
    }

    public static synchronized q[] a(Context context, String str) {
        q[] qVarArr;
        synchronized (ac.class) {
            if (context == null || str == null) {
                g.c("rqdp{  args error}", new Object[0]);
                qVarArr = null;
            } else {
                List<aj> a2 = aj.a(context, new int[]{9}, -1, -1, 10000, -1, str, -1, -1, -1, -1, -1, Long.MAX_VALUE);
                if (a2 == null || a2.size() <= 0) {
                    qVarArr = null;
                } else {
                    ArrayList arrayList = new ArrayList(a2.size());
                    for (aj next : a2) {
                        Object b2 = b(next.b());
                        if (b2 != null && q.class.isInstance(b2)) {
                            q cast = q.class.cast(b2);
                            cast.b(next.a());
                            arrayList.add(cast);
                        }
                    }
                    if (arrayList.size() > 0) {
                        qVarArr = (q[]) arrayList.toArray(new q[0]);
                    } else {
                        qVarArr = null;
                    }
                }
            }
        }
        return qVarArr;
    }

    public static synchronized int b(Context context, String str) {
        int i = 0;
        synchronized (ac.class) {
            if (context == null || str == null) {
                g.c("rqdp{  args error}", new Object[0]);
            } else {
                i = aj.a(context, new int[]{9}, -1, Long.MAX_VALUE, str);
            }
        }
        return i;
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0055 A[SYNTHETIC, Splitter:B:34:0x0055] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object b(byte[] r5) {
        /*
            r2 = 0
            r0 = 0
            java.lang.String r1 = "rqdp{  de byte 2 obj}"
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.b(r1, r2)
            if (r5 == 0) goto L_0x000e
            int r1 = r5.length
            if (r1 >= 0) goto L_0x000f
        L_0x000e:
            return r0
        L_0x000f:
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream
            r3.<init>(r5)
            java.io.ObjectInputStream r2 = new java.io.ObjectInputStream     // Catch:{ Throwable -> 0x002e, all -> 0x0050 }
            r2.<init>(r3)     // Catch:{ Throwable -> 0x002e, all -> 0x0050 }
            java.lang.Object r0 = r2.readObject()     // Catch:{ Throwable -> 0x0068 }
            r2.close()     // Catch:{ IOException -> 0x0029 }
        L_0x0020:
            r3.close()     // Catch:{ IOException -> 0x0024 }
            goto L_0x000e
        L_0x0024:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000e
        L_0x0029:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0020
        L_0x002e:
            r1 = move-exception
            r2 = r0
        L_0x0030:
            r1.printStackTrace()     // Catch:{ all -> 0x0066 }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0066 }
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0066 }
            com.tencent.feedback.common.g.d(r1, r4)     // Catch:{ all -> 0x0066 }
            if (r2 == 0) goto L_0x0042
            r2.close()     // Catch:{ IOException -> 0x004b }
        L_0x0042:
            r3.close()     // Catch:{ IOException -> 0x0046 }
            goto L_0x000e
        L_0x0046:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000e
        L_0x004b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0042
        L_0x0050:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0053:
            if (r2 == 0) goto L_0x0058
            r2.close()     // Catch:{ IOException -> 0x005c }
        L_0x0058:
            r3.close()     // Catch:{ IOException -> 0x0061 }
        L_0x005b:
            throw r0
        L_0x005c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0058
        L_0x0061:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005b
        L_0x0066:
            r0 = move-exception
            goto L_0x0053
        L_0x0068:
            r1 = move-exception
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.ac.b(byte[]):java.lang.Object");
    }

    public static void a(Context context, int i, byte[] bArr) {
        if (bArr != null) {
            ba baVar = new ba();
            baVar.a(i);
            baVar.a(bArr);
            baVar.b(new Date().getTime());
            a(context, baVar);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:77:0x0147  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.tencent.feedback.proguard.ba a(android.content.Context r11, int r12) {
        /*
            r1 = 0
            r8 = 0
            if (r11 != 0) goto L_0x000d
            java.lang.String r0 = "rqdp{  context == null}"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.feedback.common.g.c(r0, r1)
            r0 = r8
        L_0x000c:
            return r0
        L_0x000d:
            com.tencent.feedback.proguard.n r9 = new com.tencent.feedback.proguard.n     // Catch:{ Throwable -> 0x015c, all -> 0x012c }
            r9.<init>(r11)     // Catch:{ Throwable -> 0x015c, all -> 0x012c }
            android.database.sqlite.SQLiteDatabase r0 = r9.getWritableDatabase()     // Catch:{ Throwable -> 0x0161, all -> 0x014b }
            if (r0 != 0) goto L_0x0030
            java.lang.String r1 = "rqdp{  getWritableDatabase fail!}"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x0167, all -> 0x014e }
            com.tencent.feedback.common.g.c(r1, r2)     // Catch:{ Throwable -> 0x0167, all -> 0x014e }
            if (r0 == 0) goto L_0x002b
            boolean r1 = r0.isOpen()
            if (r1 == 0) goto L_0x002b
            r0.close()
        L_0x002b:
            r9.close()
            r0 = r8
            goto L_0x000c
        L_0x0030:
            java.util.Locale r1 = java.util.Locale.US     // Catch:{ Throwable -> 0x0167, all -> 0x014e }
            java.lang.String r2 = " %s = %d "
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0167, all -> 0x014e }
            r4 = 0
            java.lang.String r5 = "_key"
            r3[r4] = r5     // Catch:{ Throwable -> 0x0167, all -> 0x014e }
            r4 = 1
            java.lang.Integer r5 = java.lang.Integer.valueOf(r12)     // Catch:{ Throwable -> 0x0167, all -> 0x014e }
            r3[r4] = r5     // Catch:{ Throwable -> 0x0167, all -> 0x014e }
            java.lang.String r3 = java.lang.String.format(r1, r2, r3)     // Catch:{ Throwable -> 0x0167, all -> 0x014e }
            java.lang.String r1 = "strategy"
            r2 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x0167, all -> 0x014e }
            if (r2 == 0) goto L_0x0112
            boolean r1 = r2.moveToNext()     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            if (r1 == 0) goto L_0x0112
            if (r2 == 0) goto L_0x0068
            boolean r1 = r2.isBeforeFirst()     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            if (r1 != 0) goto L_0x0068
            boolean r1 = r2.isAfterLast()     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            if (r1 == 0) goto L_0x009a
        L_0x0068:
            r1 = r8
        L_0x0069:
            if (r1 == 0) goto L_0x0112
            java.lang.String r3 = "rqdp{  read strategy key:}%d"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            r5 = 0
            int r6 = r1.b()     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            r4[r5] = r6     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            com.tencent.feedback.common.g.g(r3, r4)     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            if (r2 == 0) goto L_0x0089
            boolean r3 = r2.isClosed()
            if (r3 != 0) goto L_0x0089
            r2.close()
        L_0x0089:
            if (r0 == 0) goto L_0x0094
            boolean r2 = r0.isOpen()
            if (r2 == 0) goto L_0x0094
            r0.close()
        L_0x0094:
            r9.close()
            r0 = r1
            goto L_0x000c
        L_0x009a:
            java.lang.String r1 = "rqdp{  parse bean}"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            com.tencent.feedback.common.g.b(r1, r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            com.tencent.feedback.proguard.ba r1 = new com.tencent.feedback.proguard.ba     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            r1.<init>()     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            java.lang.String r3 = "_id"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            long r3 = r2.getLong(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            r1.a(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            java.lang.String r3 = "_key"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            int r3 = r2.getInt(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            r1.a(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            java.lang.String r3 = "_ut"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            long r3 = r2.getLong(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            r1.b(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            java.lang.String r3 = "_datas"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            byte[] r3 = r2.getBlob(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            r1.a(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0153 }
            goto L_0x0069
        L_0x00dc:
            r1 = move-exception
            r3 = r9
            r10 = r2
            r2 = r0
            r0 = r1
            r1 = r10
        L_0x00e2:
            r0.printStackTrace()     // Catch:{ all -> 0x0157 }
            java.lang.String r4 = "rqdp{  Error strategy query!} %s"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x0157 }
            r6 = 0
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0157 }
            r5[r6] = r0     // Catch:{ all -> 0x0157 }
            com.tencent.feedback.common.g.d(r4, r5)     // Catch:{ all -> 0x0157 }
            if (r1 == 0) goto L_0x00ff
            boolean r0 = r1.isClosed()
            if (r0 != 0) goto L_0x00ff
            r1.close()
        L_0x00ff:
            if (r2 == 0) goto L_0x010a
            boolean r0 = r2.isOpen()
            if (r0 == 0) goto L_0x010a
            r2.close()
        L_0x010a:
            if (r3 == 0) goto L_0x010f
            r3.close()
        L_0x010f:
            r0 = r8
            goto L_0x000c
        L_0x0112:
            if (r2 == 0) goto L_0x011d
            boolean r1 = r2.isClosed()
            if (r1 != 0) goto L_0x011d
            r2.close()
        L_0x011d:
            if (r0 == 0) goto L_0x0128
            boolean r1 = r0.isOpen()
            if (r1 == 0) goto L_0x0128
            r0.close()
        L_0x0128:
            r9.close()
            goto L_0x010f
        L_0x012c:
            r0 = move-exception
            r2 = r8
            r9 = r8
        L_0x012f:
            if (r2 == 0) goto L_0x013a
            boolean r1 = r2.isClosed()
            if (r1 != 0) goto L_0x013a
            r2.close()
        L_0x013a:
            if (r8 == 0) goto L_0x0145
            boolean r1 = r8.isOpen()
            if (r1 == 0) goto L_0x0145
            r8.close()
        L_0x0145:
            if (r9 == 0) goto L_0x014a
            r9.close()
        L_0x014a:
            throw r0
        L_0x014b:
            r0 = move-exception
            r2 = r8
            goto L_0x012f
        L_0x014e:
            r1 = move-exception
            r2 = r8
            r8 = r0
            r0 = r1
            goto L_0x012f
        L_0x0153:
            r1 = move-exception
            r8 = r0
            r0 = r1
            goto L_0x012f
        L_0x0157:
            r0 = move-exception
            r8 = r2
            r9 = r3
            r2 = r1
            goto L_0x012f
        L_0x015c:
            r0 = move-exception
            r1 = r8
            r2 = r8
            r3 = r8
            goto L_0x00e2
        L_0x0161:
            r0 = move-exception
            r1 = r8
            r2 = r8
            r3 = r9
            goto L_0x00e2
        L_0x0167:
            r1 = move-exception
            r2 = r0
            r3 = r9
            r0 = r1
            r1 = r8
            goto L_0x00e2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.ac.a(android.content.Context, int):com.tencent.feedback.proguard.ba");
    }

    public static byte[] a(byte[] bArr, int i, String str) {
        ab zVar;
        if (bArr == null || i == -1) {
            return bArr;
        }
        g.b("rqdp{  enD:} %d %d", Integer.valueOf(bArr.length), Integer.valueOf(i));
        if (i == 1) {
            try {
                zVar = new aa();
            } catch (Throwable th) {
                th.printStackTrace();
                g.d("rqdp{  err enD: }%s", th.toString());
                return null;
            }
        } else {
            zVar = i == 3 ? new z() : null;
        }
        if (zVar == null) {
            return null;
        }
        zVar.a(str);
        return zVar.b(bArr);
    }

    public static byte[] b(byte[] bArr, int i, String str) {
        ab zVar;
        if (bArr == null || i == -1) {
            return bArr;
        }
        if (i == 1) {
            try {
                zVar = new aa();
            } catch (Throwable th) {
                th.printStackTrace();
                g.d("rqdp{  err unD:} %s", th.toString());
                return null;
            }
        } else {
            zVar = i == 3 ? new z() : null;
        }
        if (zVar == null) {
            return null;
        }
        zVar.a(str);
        return zVar.a(bArr);
    }

    public static byte[] a(byte[] bArr, int i) {
        if (bArr == null || i == -1) {
            return bArr;
        }
        g.b("rqdp{  zp:} %s rqdp{  len:} %s", Integer.valueOf(i), Integer.valueOf(bArr.length));
        try {
            o a2 = m.a(i);
            if (a2 == null) {
                return null;
            }
            return a2.a(bArr);
        } catch (Throwable th) {
            th.printStackTrace();
            g.d("rqdp{  err zp :} %s", th.toString());
            return null;
        }
    }

    private void a(ArrayList<String> arrayList, Object obj) {
        if (obj.getClass().isArray()) {
            if (!obj.getClass().getComponentType().toString().equals("byte")) {
                throw new IllegalArgumentException("only byte[] is supported");
            } else if (Array.getLength(obj) > 0) {
                arrayList.add("java.util.List");
                a(arrayList, Array.get(obj, 0));
            } else {
                arrayList.add("Array");
                arrayList.add("?");
            }
        } else if (obj instanceof Array) {
            throw new IllegalArgumentException("can not support Array, please use List");
        } else if (obj instanceof List) {
            arrayList.add("java.util.List");
            List list = (List) obj;
            if (list.size() > 0) {
                a(arrayList, list.get(0));
            } else {
                arrayList.add("?");
            }
        } else if (obj instanceof Map) {
            arrayList.add("java.util.Map");
            Map map = (Map) obj;
            if (map.size() > 0) {
                Object next = map.keySet().iterator().next();
                Object obj2 = map.get(next);
                arrayList.add(next.getClass().getName());
                a(arrayList, obj2);
                return;
            }
            arrayList.add("?");
            arrayList.add("?");
        } else {
            arrayList.add(obj.getClass().getName());
        }
    }

    public static byte[] b(byte[] bArr, int i) {
        if (bArr == null || i == -1) {
            return bArr;
        }
        g.b("rqdp{  unzp:} %s rqdp{  len:} %s", Integer.valueOf(i), Integer.valueOf(bArr.length));
        try {
            o a2 = m.a(i);
            if (a2 == null) {
                return null;
            }
            return a2.b(bArr);
        } catch (Throwable th) {
            th.printStackTrace();
            g.d("rqdp{  err unzp}" + th.toString(), new Object[0]);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:46:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int b(android.content.Context r8, int r9) {
        /*
            r2 = 0
            r0 = 0
            if (r8 != 0) goto L_0x000c
            java.lang.String r1 = "rqdp{  context == null ||key < -1}"
            java.lang.Object[] r2 = new java.lang.Object[r0]
            com.tencent.feedback.common.g.c(r1, r2)
        L_0x000b:
            return r0
        L_0x000c:
            com.tencent.feedback.proguard.n r3 = new com.tencent.feedback.proguard.n     // Catch:{ Throwable -> 0x0074, all -> 0x009a }
            r3.<init>(r8)     // Catch:{ Throwable -> 0x0074, all -> 0x009a }
            android.database.sqlite.SQLiteDatabase r2 = r3.getWritableDatabase()     // Catch:{ Throwable -> 0x00af }
            if (r2 != 0) goto L_0x002e
            java.lang.String r1 = "get db fail!,return "
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x00af }
            com.tencent.feedback.common.g.d(r1, r4)     // Catch:{ Throwable -> 0x00af }
            if (r2 == 0) goto L_0x002a
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x002a
            r2.close()
        L_0x002a:
            r3.close()
            goto L_0x000b
        L_0x002e:
            java.lang.String r1 = "%s = %d"
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x00af }
            r5 = 0
            java.lang.String r6 = "_key"
            r4[r5] = r6     // Catch:{ Throwable -> 0x00af }
            r5 = 1
            r6 = 302(0x12e, float:4.23E-43)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Throwable -> 0x00af }
            r4[r5] = r6     // Catch:{ Throwable -> 0x00af }
            java.lang.String r1 = java.lang.String.format(r1, r4)     // Catch:{ Throwable -> 0x00af }
            java.lang.String r4 = "strategy"
            r5 = 0
            int r1 = r2.delete(r4, r1, r5)     // Catch:{ Throwable -> 0x00af }
            java.lang.String r4 = "rqdp{  delete Strategy key} %d rqdp{  , num} %d"
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x00af }
            r6 = 0
            r7 = 302(0x12e, float:4.23E-43)
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Throwable -> 0x00af }
            r5[r6] = r7     // Catch:{ Throwable -> 0x00af }
            r6 = 1
            java.lang.Integer r7 = java.lang.Integer.valueOf(r1)     // Catch:{ Throwable -> 0x00af }
            r5[r6] = r7     // Catch:{ Throwable -> 0x00af }
            com.tencent.feedback.common.g.g(r4, r5)     // Catch:{ Throwable -> 0x00af }
            if (r2 == 0) goto L_0x006f
            boolean r0 = r2.isOpen()
            if (r0 == 0) goto L_0x006f
            r2.close()
        L_0x006f:
            r3.close()
            r0 = r1
            goto L_0x000b
        L_0x0074:
            r1 = move-exception
            r3 = r2
        L_0x0076:
            r1.printStackTrace()     // Catch:{ all -> 0x00ad }
            java.lang.String r4 = "rqdp{  Error strategy delete!} %s"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x00ad }
            r6 = 0
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00ad }
            r5[r6] = r1     // Catch:{ all -> 0x00ad }
            com.tencent.feedback.common.g.d(r4, r5)     // Catch:{ all -> 0x00ad }
            if (r2 == 0) goto L_0x0093
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x0093
            r2.close()
        L_0x0093:
            if (r3 == 0) goto L_0x000b
            r3.close()
            goto L_0x000b
        L_0x009a:
            r0 = move-exception
            r3 = r2
        L_0x009c:
            if (r2 == 0) goto L_0x00a7
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x00a7
            r2.close()
        L_0x00a7:
            if (r3 == 0) goto L_0x00ac
            r3.close()
        L_0x00ac:
            throw r0
        L_0x00ad:
            r0 = move-exception
            goto L_0x009c
        L_0x00af:
            r1 = move-exception
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.ac.b(android.content.Context, int):int");
    }

    public static C a(int i, e eVar, byte[] bArr, int i2, int i3) {
        g.b("rqdp{  en2Req }", new Object[0]);
        if (eVar == null) {
            g.d("rqdp{  error no com info!}", new Object[0]);
            return null;
        }
        try {
            C c2 = new C();
            synchronized (eVar) {
                c2.h = eVar.a();
                c2.f3703a = eVar.b();
                c2.b = eVar.o();
                c2.c = eVar.d();
                c2.d = eVar.e();
                c2.e = eVar.f();
                c2.i = eVar.i();
                c2.l = eVar.m().trim().length() > 0 ? eVar.m() : eVar.n();
                c2.m = Constants.STR_EMPTY;
                c2.n = eVar.c();
                c2.o = Constants.STR_EMPTY;
            }
            c2.f = i;
            c2.j = (byte) i3;
            c2.k = (byte) i2;
            if (bArr == null) {
                bArr = Constants.STR_EMPTY.getBytes();
            }
            c2.g = bArr;
            return c2;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
     arg types: [java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, byte[]>>, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void */
    public byte[] a() {
        ah ahVar = new ah(0);
        ahVar.a(this.b);
        ahVar.a((Map) this.f3720a, 0);
        return ai.a(ahVar.a());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
     arg types: [java.util.HashMap, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V> */
    public void a(byte[] bArr) {
        this.c.a(bArr);
        this.c.a(this.b);
        HashMap hashMap = new HashMap(1);
        HashMap hashMap2 = new HashMap(1);
        hashMap2.put(Constants.STR_EMPTY, new byte[0]);
        hashMap.put(Constants.STR_EMPTY, hashMap2);
        this.f3720a = this.c.a((Map) hashMap, 0, false);
    }

    public static byte[] a(byte[] bArr, int i, int i2, String str) {
        if (bArr == null) {
            return null;
        }
        try {
            return a(a(bArr, i), i2, str);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public static byte[] b(byte[] bArr, int i, int i2, String str) {
        try {
            return b(b(bArr, i2, str), i);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static long b() {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            return simpleDateFormat.parse(simpleDateFormat.format(new Date())).getTime();
        } catch (Throwable th) {
            th.printStackTrace();
            return -1;
        }
    }

    private static String d(byte[] bArr) {
        if (bArr == null) {
            return Constants.STR_EMPTY;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b2 : bArr) {
            String hexString = Integer.toHexString(b2 & 255);
            if (hexString.length() == 1) {
                stringBuffer.append("0");
            }
            stringBuffer.append(hexString);
        }
        return stringBuffer.toString().toUpperCase();
    }

    public static String c(byte[] bArr) {
        if (bArr == null || bArr.length == 0) {
            return "NULL";
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.update(bArr);
            return d(instance.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            g.d(e.getMessage(), new Object[0]);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x0071 A[SYNTHETIC, Splitter:B:36:0x0071] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x007f A[SYNTHETIC, Splitter:B:43:0x007f] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:33:0x0062=Splitter:B:33:0x0062, B:18:0x0037=Splitter:B:18:0x0037} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r6) {
        /*
            r0 = 0
            if (r6 == 0) goto L_0x0009
            int r1 = r6.length()
            if (r1 != 0) goto L_0x000a
        L_0x0009:
            return r0
        L_0x000a:
            java.io.File r1 = new java.io.File
            r1.<init>(r6)
            boolean r2 = r1.exists()
            if (r2 == 0) goto L_0x0009
            boolean r2 = r1.canRead()
            if (r2 == 0) goto L_0x0009
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ IOException -> 0x008c, NoSuchAlgorithmException -> 0x0060, all -> 0x007a }
            r2.<init>(r1)     // Catch:{ IOException -> 0x008c, NoSuchAlgorithmException -> 0x0060, all -> 0x007a }
            java.lang.String r1 = "SHA-1"
            java.security.MessageDigest r1 = java.security.MessageDigest.getInstance(r1)     // Catch:{ IOException -> 0x0036, NoSuchAlgorithmException -> 0x008a }
            r3 = 4096(0x1000, float:5.74E-42)
            byte[] r3 = new byte[r3]     // Catch:{ IOException -> 0x0036, NoSuchAlgorithmException -> 0x008a }
        L_0x002a:
            int r4 = r2.read(r3)     // Catch:{ IOException -> 0x0036, NoSuchAlgorithmException -> 0x008a }
            r5 = -1
            if (r4 == r5) goto L_0x004f
            r5 = 0
            r1.update(r3, r5, r4)     // Catch:{ IOException -> 0x0036, NoSuchAlgorithmException -> 0x008a }
            goto L_0x002a
        L_0x0036:
            r1 = move-exception
        L_0x0037:
            r1.printStackTrace()     // Catch:{ all -> 0x0088 }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0088 }
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0088 }
            com.tencent.feedback.common.g.d(r1, r3)     // Catch:{ all -> 0x0088 }
            if (r2 == 0) goto L_0x0009
            r2.close()     // Catch:{ IOException -> 0x004a }
            goto L_0x0009
        L_0x004a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0009
        L_0x004f:
            byte[] r1 = r1.digest()     // Catch:{ IOException -> 0x0036, NoSuchAlgorithmException -> 0x008a }
            java.lang.String r0 = d(r1)     // Catch:{ IOException -> 0x0036, NoSuchAlgorithmException -> 0x008a }
            r2.close()     // Catch:{ IOException -> 0x005b }
            goto L_0x0009
        L_0x005b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0009
        L_0x0060:
            r1 = move-exception
            r2 = r0
        L_0x0062:
            r1.printStackTrace()     // Catch:{ all -> 0x0088 }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0088 }
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0088 }
            com.tencent.feedback.common.g.d(r1, r3)     // Catch:{ all -> 0x0088 }
            if (r2 == 0) goto L_0x0009
            r2.close()     // Catch:{ IOException -> 0x0075 }
            goto L_0x0009
        L_0x0075:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0009
        L_0x007a:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x007d:
            if (r2 == 0) goto L_0x0082
            r2.close()     // Catch:{ IOException -> 0x0083 }
        L_0x0082:
            throw r0
        L_0x0083:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0082
        L_0x0088:
            r0 = move-exception
            goto L_0x007d
        L_0x008a:
            r1 = move-exception
            goto L_0x0062
        L_0x008c:
            r1 = move-exception
            r2 = r0
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.ac.a(java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x0091 A[SYNTHETIC, Splitter:B:39:0x0091] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0096 A[SYNTHETIC, Splitter:B:42:0x0096] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00d7 A[SYNTHETIC, Splitter:B:65:0x00d7] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00dc A[SYNTHETIC, Splitter:B:68:0x00dc] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.io.File r6, java.io.File r7, int r8) {
        /*
            r3 = 0
            r0 = 0
            java.lang.String r1 = "rqdp{  ZF start}"
            java.lang.Object[] r2 = new java.lang.Object[r0]
            com.tencent.feedback.common.g.b(r1, r2)
            if (r6 == 0) goto L_0x0013
            if (r7 == 0) goto L_0x0013
            boolean r1 = r6.equals(r7)
            if (r1 == 0) goto L_0x001b
        L_0x0013:
            java.lang.String r1 = "rqdp{  err ZF 1R!}"
            java.lang.Object[] r2 = new java.lang.Object[r0]
            com.tencent.feedback.common.g.c(r1, r2)
        L_0x001a:
            return r0
        L_0x001b:
            boolean r1 = r6.exists()
            if (r1 == 0) goto L_0x0027
            boolean r1 = r6.canRead()
            if (r1 != 0) goto L_0x002f
        L_0x0027:
            java.lang.String r1 = "rqdp{  !sFile.exists() || !sFile.canRead(),pls check ,return!}"
            java.lang.Object[] r2 = new java.lang.Object[r0]
            com.tencent.feedback.common.g.c(r1, r2)
            goto L_0x001a
        L_0x002f:
            java.io.File r1 = r7.getParentFile()     // Catch:{ Throwable -> 0x00a2 }
            if (r1 == 0) goto L_0x0046
            java.io.File r1 = r7.getParentFile()     // Catch:{ Throwable -> 0x00a2 }
            boolean r1 = r1.exists()     // Catch:{ Throwable -> 0x00a2 }
            if (r1 != 0) goto L_0x0046
            java.io.File r1 = r7.getParentFile()     // Catch:{ Throwable -> 0x00a2 }
            r1.mkdirs()     // Catch:{ Throwable -> 0x00a2 }
        L_0x0046:
            boolean r1 = r7.exists()     // Catch:{ Throwable -> 0x00a2 }
            if (r1 != 0) goto L_0x004f
            r7.createNewFile()     // Catch:{ Throwable -> 0x00a2 }
        L_0x004f:
            boolean r1 = r7.exists()
            if (r1 == 0) goto L_0x001a
            boolean r1 = r7.canRead()
            if (r1 == 0) goto L_0x001a
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x00f9, all -> 0x00d2 }
            r4.<init>(r6)     // Catch:{ Throwable -> 0x00f9, all -> 0x00d2 }
            java.util.zip.ZipOutputStream r2 = new java.util.zip.ZipOutputStream     // Catch:{ Throwable -> 0x00fc, all -> 0x00f1 }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Throwable -> 0x00fc, all -> 0x00f1 }
            r1.<init>(r7)     // Catch:{ Throwable -> 0x00fc, all -> 0x00f1 }
            r2.<init>(r1)     // Catch:{ Throwable -> 0x00fc, all -> 0x00f1 }
            r1 = 8
            r2.setMethod(r1)     // Catch:{ Throwable -> 0x008a, all -> 0x00f4 }
            java.util.zip.ZipEntry r1 = new java.util.zip.ZipEntry     // Catch:{ Throwable -> 0x008a, all -> 0x00f4 }
            java.lang.String r3 = r6.getName()     // Catch:{ Throwable -> 0x008a, all -> 0x00f4 }
            r1.<init>(r3)     // Catch:{ Throwable -> 0x008a, all -> 0x00f4 }
            r2.putNextEntry(r1)     // Catch:{ Throwable -> 0x008a, all -> 0x00f4 }
            r1 = 5000(0x1388, float:7.006E-42)
            byte[] r1 = new byte[r1]     // Catch:{ Throwable -> 0x008a, all -> 0x00f4 }
        L_0x007f:
            int r3 = r4.read(r1)     // Catch:{ Throwable -> 0x008a, all -> 0x00f4 }
            if (r3 <= 0) goto L_0x00a8
            r5 = 0
            r2.write(r1, r5, r3)     // Catch:{ Throwable -> 0x008a, all -> 0x00f4 }
            goto L_0x007f
        L_0x008a:
            r1 = move-exception
            r3 = r4
        L_0x008c:
            r1.printStackTrace()     // Catch:{ all -> 0x00f6 }
            if (r3 == 0) goto L_0x0094
            r3.close()     // Catch:{ IOException -> 0x00c8 }
        L_0x0094:
            if (r2 == 0) goto L_0x0099
            r2.close()     // Catch:{ IOException -> 0x00cd }
        L_0x0099:
            java.lang.String r1 = "rqdp{  ZF end}"
            java.lang.Object[] r2 = new java.lang.Object[r0]
            com.tencent.feedback.common.g.b(r1, r2)
            goto L_0x001a
        L_0x00a2:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001a
        L_0x00a8:
            r2.flush()     // Catch:{ Throwable -> 0x008a, all -> 0x00f4 }
            r2.closeEntry()     // Catch:{ Throwable -> 0x008a, all -> 0x00f4 }
            r4.close()     // Catch:{ IOException -> 0x00be }
        L_0x00b1:
            r2.close()     // Catch:{ IOException -> 0x00c3 }
        L_0x00b4:
            java.lang.String r1 = "rqdp{  ZF end}"
            java.lang.Object[] r0 = new java.lang.Object[r0]
            com.tencent.feedback.common.g.b(r1, r0)
            r0 = 1
            goto L_0x001a
        L_0x00be:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00b1
        L_0x00c3:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00b4
        L_0x00c8:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0094
        L_0x00cd:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0099
        L_0x00d2:
            r1 = move-exception
            r2 = r3
            r4 = r3
        L_0x00d5:
            if (r4 == 0) goto L_0x00da
            r4.close()     // Catch:{ IOException -> 0x00e7 }
        L_0x00da:
            if (r2 == 0) goto L_0x00df
            r2.close()     // Catch:{ IOException -> 0x00ec }
        L_0x00df:
            java.lang.String r2 = "rqdp{  ZF end}"
            java.lang.Object[] r0 = new java.lang.Object[r0]
            com.tencent.feedback.common.g.b(r2, r0)
            throw r1
        L_0x00e7:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x00da
        L_0x00ec:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00df
        L_0x00f1:
            r1 = move-exception
            r2 = r3
            goto L_0x00d5
        L_0x00f4:
            r1 = move-exception
            goto L_0x00d5
        L_0x00f6:
            r1 = move-exception
            r4 = r3
            goto L_0x00d5
        L_0x00f9:
            r1 = move-exception
            r2 = r3
            goto L_0x008c
        L_0x00fc:
            r1 = move-exception
            r2 = r3
            r3 = r4
            goto L_0x008c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.ac.a(java.io.File, java.io.File, int):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x002d A[SYNTHETIC, Splitter:B:12:0x002d] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0032 A[SYNTHETIC, Splitter:B:15:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0070 A[SYNTHETIC, Splitter:B:40:0x0070] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0075 A[SYNTHETIC, Splitter:B:43:0x0075] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.ArrayList<java.lang.String> a(java.lang.String[] r6) {
        /*
            r1 = 0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.lang.Runtime r2 = java.lang.Runtime.getRuntime()     // Catch:{ Throwable -> 0x0088, all -> 0x006c }
            java.lang.Process r4 = r2.exec(r6)     // Catch:{ Throwable -> 0x0088, all -> 0x006c }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x0088, all -> 0x006c }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Throwable -> 0x0088, all -> 0x006c }
            java.io.InputStream r5 = r4.getInputStream()     // Catch:{ Throwable -> 0x0088, all -> 0x006c }
            r2.<init>(r5)     // Catch:{ Throwable -> 0x0088, all -> 0x006c }
            r3.<init>(r2)     // Catch:{ Throwable -> 0x0088, all -> 0x006c }
        L_0x001c:
            java.lang.String r2 = r3.readLine()     // Catch:{ Throwable -> 0x0026, all -> 0x0083 }
            if (r2 == 0) goto L_0x0037
            r0.add(r2)     // Catch:{ Throwable -> 0x0026, all -> 0x0083 }
            goto L_0x001c
        L_0x0026:
            r0 = move-exception
            r2 = r1
        L_0x0028:
            r0.printStackTrace()     // Catch:{ all -> 0x0085 }
            if (r3 == 0) goto L_0x0030
            r3.close()     // Catch:{ IOException -> 0x0062 }
        L_0x0030:
            if (r2 == 0) goto L_0x0035
            r2.close()     // Catch:{ IOException -> 0x0067 }
        L_0x0035:
            r0 = r1
        L_0x0036:
            return r0
        L_0x0037:
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x0026, all -> 0x0083 }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ Throwable -> 0x0026, all -> 0x0083 }
            java.io.InputStream r4 = r4.getErrorStream()     // Catch:{ Throwable -> 0x0026, all -> 0x0083 }
            r5.<init>(r4)     // Catch:{ Throwable -> 0x0026, all -> 0x0083 }
            r2.<init>(r5)     // Catch:{ Throwable -> 0x0026, all -> 0x0083 }
        L_0x0045:
            java.lang.String r4 = r2.readLine()     // Catch:{ Throwable -> 0x004f }
            if (r4 == 0) goto L_0x0051
            r0.add(r4)     // Catch:{ Throwable -> 0x004f }
            goto L_0x0045
        L_0x004f:
            r0 = move-exception
            goto L_0x0028
        L_0x0051:
            r3.close()     // Catch:{ IOException -> 0x005d }
        L_0x0054:
            r2.close()     // Catch:{ IOException -> 0x0058 }
            goto L_0x0036
        L_0x0058:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0036
        L_0x005d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0054
        L_0x0062:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0030
        L_0x0067:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0035
        L_0x006c:
            r0 = move-exception
            r3 = r1
        L_0x006e:
            if (r3 == 0) goto L_0x0073
            r3.close()     // Catch:{ IOException -> 0x0079 }
        L_0x0073:
            if (r1 == 0) goto L_0x0078
            r1.close()     // Catch:{ IOException -> 0x007e }
        L_0x0078:
            throw r0
        L_0x0079:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0073
        L_0x007e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0078
        L_0x0083:
            r0 = move-exception
            goto L_0x006e
        L_0x0085:
            r0 = move-exception
            r1 = r2
            goto L_0x006e
        L_0x0088:
            r0 = move-exception
            r2 = r1
            r3 = r1
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.ac.a(java.lang.String[]):java.util.ArrayList");
    }

    public static String b(String str) {
        if (str == null || str.trim().equals(Constants.STR_EMPTY)) {
            return Constants.STR_EMPTY;
        }
        ArrayList<String> a2 = a(new String[]{"/system/bin/sh", "-c", "getprop " + str});
        if (a2 == null || a2.size() <= 0) {
            return "fail";
        }
        return a2.get(0);
    }

    public static String c() {
        try {
            return UUID.randomUUID().toString();
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public static String c(String str) {
        if (str == null) {
            return null;
        }
        try {
            return Pattern.compile("\\d+").matcher(Pattern.compile("\\b[0-9a-fA-F]{8}\\b|\\b[0-9a-fA-F]{4}\\b").matcher(Pattern.compile("\\b[~!@#\\$%\\^&*uU][0-9a-fA-F_]{1,8}\\b").matcher(Pattern.compile("0[xX][0-9a-fA-F]{1,8}").matcher(str).replaceAll("[HEX]")).replaceAll("[SPV]")).replaceAll("[SHEX]")).replaceAll("[DIG]");
        } catch (Throwable th) {
            g.d("remove Str num fail", new Object[0]);
            return str;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public static void a(String str, String str2, int i) {
        FileOutputStream fileOutputStream;
        FileOutputStream fileOutputStream2;
        g.b("rqdp{  sv sd start} %s", str);
        if (str2 != null && str2.trim().length() > 0) {
            File file = new File(str);
            try {
                if (!file.exists()) {
                    if (file.getParentFile() != null) {
                        file.getParentFile().mkdirs();
                    }
                    file.createNewFile();
                }
                fileOutputStream = null;
                if (file.length() >= ((long) i)) {
                    fileOutputStream2 = new FileOutputStream(file, false);
                } else {
                    fileOutputStream2 = new FileOutputStream(file, true);
                }
                fileOutputStream2.write(str2.getBytes("UTF-8"));
                fileOutputStream2.flush();
                fileOutputStream2.close();
            } catch (Throwable th) {
                th.printStackTrace();
            }
            g.b("rqdp{  sv sd end}", new Object[0]);
        }
    }
}
