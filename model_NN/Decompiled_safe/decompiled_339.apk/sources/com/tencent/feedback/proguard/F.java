package com.tencent.feedback.proguard;

import com.tencent.connect.common.Constants;

public final class F extends C0008j {
    private static byte[] d;

    /* renamed from: a  reason: collision with root package name */
    public byte f3706a;
    public String b;
    public byte[] c;

    public F() {
        this.f3706a = 0;
        this.b = Constants.STR_EMPTY;
        this.c = null;
    }

    public F(byte b2, String str, byte[] bArr) {
        this.f3706a = 0;
        this.b = Constants.STR_EMPTY;
        this.c = null;
        this.f3706a = 2;
        this.b = str;
        this.c = bArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte */
    public final void a(ag agVar) {
        this.f3706a = agVar.a(this.f3706a, 0, true);
        this.b = agVar.b(1, false);
        if (d == null) {
            byte[] bArr = new byte[1];
            d = bArr;
            bArr[0] = 0;
        }
        byte[] bArr2 = d;
        this.c = agVar.c(2, true);
    }

    public final void a(ah ahVar) {
        ahVar.a(this.f3706a, 0);
        if (this.b != null) {
            ahVar.a(this.b, 1);
        }
        ahVar.a(this.c, 2);
    }

    public final void a(StringBuilder sb, int i) {
    }
}
