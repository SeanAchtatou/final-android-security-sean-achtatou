package com.qq.provider;

import android.app.ActivityManager;
import android.content.Context;
import com.qq.AppService.s;
import com.qq.g.c;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static b f333a = null;

    private b() {
    }

    public static b a() {
        if (f333a == null) {
            f333a = new b();
        }
        return f333a;
    }

    public void a(Context context, c cVar) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses == null) {
            cVar.a(8);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(runningAppProcesses.size()));
        cVar.a(arrayList);
    }

    public void b(Context context, c cVar) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses == null) {
            cVar.a(8);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(runningAppProcesses.size()));
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < runningAppProcesses.size()) {
                arrayList.add(s.a(runningAppProcesses.get(i2).pid));
                i = i2 + 1;
            } else {
                cVar.a(arrayList);
                return;
            }
        }
    }

    public void c(Context context, c cVar) {
        ActivityManager.RunningAppProcessInfo runningAppProcessInfo;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses == null) {
            cVar.a(8);
            return;
        }
        int i = 0;
        while (true) {
            if (i >= runningAppProcesses.size()) {
                runningAppProcessInfo = null;
                break;
            } else if (h == runningAppProcesses.get(i).pid) {
                runningAppProcessInfo = runningAppProcesses.get(i);
                break;
            } else {
                i++;
            }
        }
        if (runningAppProcessInfo == null) {
            cVar.a(7);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(h));
        arrayList.add(s.a(runningAppProcessInfo.uid));
        arrayList.add(s.a(runningAppProcessInfo.processName));
        arrayList.add(s.a(a(h)));
        arrayList.add(s.a(runningAppProcessInfo.importance));
        String[] strArr = runningAppProcessInfo.pkgList;
        if (strArr == null) {
            arrayList.add(s.a(0));
        } else {
            arrayList.add(s.a(strArr.length));
            for (String a2 : strArr) {
                arrayList.add(s.a(a2));
            }
        }
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void d(Context context, c cVar) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses == null) {
            cVar.a(8);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(runningAppProcesses.size()));
        for (int i = 0; i < runningAppProcesses.size(); i++) {
            ActivityManager.RunningAppProcessInfo runningAppProcessInfo = runningAppProcesses.get(i);
            arrayList.add(s.a(runningAppProcessInfo.pid));
            arrayList.add(s.a(runningAppProcessInfo.uid));
            arrayList.add(s.a(runningAppProcessInfo.processName));
            arrayList.add(s.a(a(runningAppProcessInfo.pid)));
            arrayList.add(s.a(runningAppProcessInfo.importance));
            String[] strArr = runningAppProcessInfo.pkgList;
            if (strArr == null) {
                arrayList.add(s.a(0));
            } else {
                arrayList.add(s.a(strArr.length));
                for (String a2 : strArr) {
                    arrayList.add(s.a(a2));
                }
            }
        }
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void e(Context context, c cVar) {
        String[] strArr;
        ActivityManager.RunningAppProcessInfo runningAppProcessInfo;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
        ActivityManager.RunningAppProcessInfo runningAppProcessInfo2 = null;
        if (runningAppProcesses != null) {
            int i = 0;
            while (i < runningAppProcesses.size()) {
                if (runningAppProcesses.get(i).pid == h) {
                    runningAppProcessInfo = runningAppProcesses.get(i);
                } else {
                    runningAppProcessInfo = runningAppProcessInfo2;
                }
                i++;
                runningAppProcessInfo2 = runningAppProcessInfo;
            }
        }
        if (runningAppProcessInfo2 != null && (strArr = runningAppProcessInfo2.pkgList) != null) {
            for (String restartPackage : strArr) {
                activityManager.restartPackage(restartPackage);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x006f A[SYNTHETIC, Splitter:B:30:0x006f] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0074 A[SYNTHETIC, Splitter:B:33:0x0074] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x007f A[SYNTHETIC, Splitter:B:39:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0084 A[SYNTHETIC, Splitter:B:42:0x0084] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int a(int r7) {
        /*
            r6 = this;
            r3 = 0
            r0 = -1
            java.io.File r1 = new java.io.File
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "/proc/"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r2 = r2.append(r7)
            java.lang.String r4 = "/statm"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            boolean r2 = r1.exists()
            if (r2 != 0) goto L_0x0027
        L_0x0026:
            return r0
        L_0x0027:
            java.io.FileReader r4 = new java.io.FileReader     // Catch:{ Exception -> 0x0068, all -> 0x007a }
            r4.<init>(r1)     // Catch:{ Exception -> 0x0068, all -> 0x007a }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00a4, all -> 0x009c }
            r1 = 4096(0x1000, float:5.74E-42)
            r2.<init>(r4, r1)     // Catch:{ Exception -> 0x00a4, all -> 0x009c }
            java.lang.String r1 = r2.readLine()     // Catch:{ Exception -> 0x00a8, all -> 0x009f }
            if (r1 != 0) goto L_0x0050
            java.lang.String r1 = "com.qq.connect"
            java.lang.String r3 = " get memory info failed!!!!!"
            android.util.Log.e(r1, r3)     // Catch:{ Exception -> 0x00a8, all -> 0x009f }
        L_0x0040:
            if (r4 == 0) goto L_0x0045
            r4.close()     // Catch:{ IOException -> 0x0097 }
        L_0x0045:
            if (r2 == 0) goto L_0x0026
            r2.close()     // Catch:{ IOException -> 0x004b }
            goto L_0x0026
        L_0x004b:
            r1 = move-exception
        L_0x004c:
            r1.printStackTrace()
            goto L_0x0026
        L_0x0050:
            java.lang.String r3 = "\\s+"
            java.lang.String[] r1 = r1.split(r3)     // Catch:{ Exception -> 0x00a8, all -> 0x009f }
            int r3 = r1.length     // Catch:{ Exception -> 0x00a8, all -> 0x009f }
            r5 = 6
            if (r3 < r5) goto L_0x0066
            r3 = 5
            r1 = r1[r3]     // Catch:{ Exception -> 0x00a8, all -> 0x009f }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x00a8, all -> 0x009f }
            int r0 = r1.intValue()     // Catch:{ Exception -> 0x00a8, all -> 0x009f }
            goto L_0x0040
        L_0x0066:
            r0 = 0
            goto L_0x0040
        L_0x0068:
            r1 = move-exception
            r2 = r3
        L_0x006a:
            r1.printStackTrace()     // Catch:{ all -> 0x00a1 }
            if (r3 == 0) goto L_0x0072
            r3.close()     // Catch:{ IOException -> 0x0092 }
        L_0x0072:
            if (r2 == 0) goto L_0x0026
            r2.close()     // Catch:{ IOException -> 0x0078 }
            goto L_0x0026
        L_0x0078:
            r1 = move-exception
            goto L_0x004c
        L_0x007a:
            r0 = move-exception
            r2 = r3
            r4 = r3
        L_0x007d:
            if (r4 == 0) goto L_0x0082
            r4.close()     // Catch:{ IOException -> 0x0088 }
        L_0x0082:
            if (r2 == 0) goto L_0x0087
            r2.close()     // Catch:{ IOException -> 0x008d }
        L_0x0087:
            throw r0
        L_0x0088:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0082
        L_0x008d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0087
        L_0x0092:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0072
        L_0x0097:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0045
        L_0x009c:
            r0 = move-exception
            r2 = r3
            goto L_0x007d
        L_0x009f:
            r0 = move-exception
            goto L_0x007d
        L_0x00a1:
            r0 = move-exception
            r4 = r3
            goto L_0x007d
        L_0x00a4:
            r1 = move-exception
            r2 = r3
            r3 = r4
            goto L_0x006a
        L_0x00a8:
            r1 = move-exception
            r3 = r4
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.b.a(int):int");
    }
}
