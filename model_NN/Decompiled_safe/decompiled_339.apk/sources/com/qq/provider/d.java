package com.qq.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Browser;
import com.qq.AppService.s;
import com.qq.g.c;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class d {
    private static d c = null;

    /* renamed from: a  reason: collision with root package name */
    private boolean f337a = false;
    private int b = -1;

    private d() {
    }

    public static d a() {
        if (c == null) {
            c = new d();
        }
        return c;
    }

    public void a(Context context, c cVar) {
        byte[] bArr;
        int i;
        boolean moveToNext;
        Cursor query = context.getContentResolver().query(Browser.BOOKMARKS_URI, Browser.HISTORY_PROJECTION, null, null, "_id");
        if (query == null) {
            cVar.a(8);
            return;
        }
        ArrayList arrayList = new ArrayList();
        int count = query.getCount();
        arrayList.add(s.a(count));
        int i2 = 0;
        boolean moveToFirst = query.moveToFirst();
        int i3 = 0;
        while (i2 < count && moveToFirst) {
            if (query.getInt(query.getColumnIndex("bookmark")) == 0) {
                int i4 = i3;
                moveToNext = query.moveToNext();
                i = i4;
            } else {
                arrayList.add(s.a(query.getInt(query.getColumnIndex("_id"))));
                byte[] blob = query.getBlob(query.getColumnIndex("title"));
                if (blob == null) {
                    blob = s.hr;
                }
                arrayList.add(blob);
                byte[] blob2 = query.getBlob(query.getColumnIndex(SocialConstants.PARAM_URL));
                if (blob == null) {
                    byte[] bArr2 = s.hr;
                }
                arrayList.add(blob2);
                arrayList.add(s.a(query.getInt(query.getColumnIndex("visits"))));
                byte[] blob3 = query.getBlob(query.getColumnIndex("favicon"));
                if (blob3 == null || blob3.length <= 16) {
                    blob3 = s.hr;
                }
                arrayList.add(blob3);
                int columnIndex = query.getColumnIndex("thumbnail");
                if (columnIndex >= 0) {
                    bArr = query.getBlob(columnIndex);
                } else {
                    bArr = null;
                }
                if (bArr == null || bArr.length <= 16) {
                    bArr = s.hr;
                }
                arrayList.add(bArr);
                i = i3 + 1;
                moveToNext = query.moveToNext();
            }
            i2++;
            int i5 = i;
            moveToFirst = moveToNext;
            i3 = i5;
        }
        query.close();
        arrayList.set(0, s.a(i3));
        cVar.a(arrayList);
    }

    public void b(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        String j = cVar.j();
        String j2 = cVar.j();
        int h2 = cVar.h();
        ContentValues contentValues = new ContentValues();
        if (j == null) {
            contentValues.putNull("title");
        } else {
            contentValues.put("title", j);
        }
        if (j2 == null) {
            contentValues.putNull(SocialConstants.PARAM_URL);
        } else {
            contentValues.put(SocialConstants.PARAM_URL, j2);
        }
        contentValues.put("visits", Integer.valueOf(h2));
        if (context.getContentResolver().update(Uri.withAppendedPath(Browser.BOOKMARKS_URI, Constants.STR_EMPTY + h), contentValues, null, null) < 1) {
            cVar.a(1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void c(Context context, c cVar) {
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        if (!this.f337a) {
            Cursor query = context.getContentResolver().query(Browser.BOOKMARKS_URI, null, "_id < 0", null, null);
            if (query == null) {
                cVar.a(8);
                return;
            }
            this.f337a = true;
            this.b = query.getColumnIndex("thumbnail");
            query.close();
        }
        String j = cVar.j();
        String j2 = cVar.j();
        byte[] k = cVar.k();
        byte[] k2 = cVar.k();
        if (s.b(j2) || s.b(j)) {
            cVar.a(1);
            return;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("bookmark", (Integer) 1);
        contentValues.put("title", j);
        contentValues.put(SocialConstants.PARAM_URL, j2);
        contentValues.put("visits", (Integer) 0);
        contentValues.put("created", Long.valueOf(System.currentTimeMillis()));
        if (k != null) {
            contentValues.put("favicon", k);
        }
        if (k2 != null && this.b >= 0) {
            contentValues.put("thumbnail", k2);
        }
        try {
            new ArrayList().add(s.a(Integer.parseInt(context.getContentResolver().insert(Browser.BOOKMARKS_URI, contentValues).getLastPathSegment())));
            cVar.a(0);
        } catch (Exception e) {
            cVar.a(8);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void d(Context context, c cVar) {
        int i;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        if (!this.f337a) {
            Cursor query = context.getContentResolver().query(Browser.BOOKMARKS_URI, null, "_id < 0", null, null);
            if (query == null) {
                cVar.a(8);
                return;
            }
            this.f337a = true;
            this.b = query.getColumnIndex("thumbnail");
            query.close();
        }
        int h = cVar.h();
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(h));
        ContentValues contentValues = new ContentValues();
        for (int i2 = 0; i2 < h; i2++) {
            String j = cVar.j();
            String j2 = cVar.j();
            byte[] k = cVar.k();
            byte[] k2 = cVar.k();
            if (s.b(j2) || s.b(j)) {
                arrayList.add(s.a(0));
            } else {
                contentValues.clear();
                contentValues.put("visits", (Integer) 0);
                contentValues.put("created", Long.valueOf(System.currentTimeMillis()));
                contentValues.put("bookmark", (Integer) 1);
                contentValues.put("title", j);
                contentValues.put(SocialConstants.PARAM_URL, j2);
                if (k != null) {
                    contentValues.put("favicon", k);
                }
                if (k2 != null && this.b >= 0) {
                    contentValues.put("thumbnail", k2);
                }
                try {
                    i = Integer.parseInt(context.getContentResolver().insert(Browser.BOOKMARKS_URI, contentValues).getLastPathSegment());
                } catch (Exception e) {
                    i = 0;
                }
                arrayList.add(s.a(i));
            }
        }
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void e(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        if (context.getContentResolver().delete(Uri.withAppendedPath(Browser.BOOKMARKS_URI, Constants.STR_EMPTY + cVar.h()), null, null) > 0) {
            cVar.a(1);
        } else {
            cVar.a(0);
        }
    }

    public void f(Context context, c cVar) {
        context.getContentResolver().delete(Browser.BOOKMARKS_URI, null, null);
    }
}
