package com.qq.provider;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import com.qq.AppService.s;
import com.qq.g.c;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;

/* compiled from: ProGuard */
public final class a {
    private a(Context context) {
        com.qq.a.a.a.a(context);
    }

    public static a a(Context context) {
        return new a(context);
    }

    public void a(Context context, c cVar) {
        ArrayList arrayList = new ArrayList();
        if (com.qq.a.a.a.f256a == null) {
            arrayList.add(s.a(0));
        } else {
            arrayList.add(s.a(1));
        }
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void b(Context context, c cVar) {
        int i;
        int i2;
        if (com.qq.a.a.a.f256a == null) {
            cVar.a(7);
            return;
        }
        Cursor query = context.getContentResolver().query(com.qq.a.a.a.f256a, null, null, null, null);
        if (query != null) {
            int count = query.getCount();
            ArrayList arrayList = new ArrayList();
            arrayList.add(s.a(count));
            for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
                arrayList.add(s.a((int) query.getLong(query.getColumnIndex("_id"))));
                arrayList.add(s.a(query.getInt(query.getColumnIndex("hour"))));
                arrayList.add(s.a(query.getInt(query.getColumnIndex("minutes"))));
                arrayList.add(s.a(query.getInt(query.getColumnIndex("daysofweek"))));
                arrayList.add(s.a(query.getInt(query.getColumnIndex("alarmtime"))));
                arrayList.add(s.a(query.getInt(query.getColumnIndex("enabled"))));
                arrayList.add(s.a(query.getInt(query.getColumnIndex("vibrate"))));
                byte[] blob = query.getBlob(query.getColumnIndex("message"));
                if (blob == null) {
                    blob = s.hr;
                }
                arrayList.add(blob);
                String string = query.getString(query.getColumnIndex("alert"));
                if (string == null) {
                    arrayList.add(s.a(0));
                    arrayList.add(s.a(0));
                } else {
                    try {
                        i = Integer.parseInt(Uri.parse(string).getLastPathSegment());
                    } catch (Exception e) {
                        i = 0;
                    }
                    arrayList.add(s.a(i));
                    if (string.contains("/external/")) {
                        i2 = 1;
                    } else {
                        i2 = 0;
                    }
                    arrayList.add(s.a(i2));
                }
            }
            query.close();
            cVar.a(arrayList);
            return;
        }
        cVar.a(7);
    }

    public void c(Context context, c cVar) {
        Uri withAppendedPath;
        if (com.qq.a.a.a.f256a == null) {
            cVar.a(7);
        } else if (cVar.e() < 9) {
            cVar.a(1);
        } else {
            int h = cVar.h();
            int h2 = cVar.h();
            int h3 = cVar.h();
            int h4 = cVar.h();
            cVar.g();
            int h5 = cVar.h();
            int h6 = cVar.h();
            String j = cVar.j();
            int h7 = cVar.h();
            int h8 = cVar.h();
            Uri withAppendedPath2 = Uri.withAppendedPath(com.qq.a.a.a.f256a, Constants.STR_EMPTY + h);
            ContentValues contentValues = new ContentValues();
            contentValues.put("hour", Integer.valueOf(h2));
            contentValues.put("minutes", Integer.valueOf(h3));
            contentValues.put("daysofweek", Integer.valueOf(h4));
            contentValues.put("enabled", Integer.valueOf(h5));
            contentValues.put("vibrate", Integer.valueOf(h6));
            if (!s.b(j)) {
                contentValues.put("message", j);
            } else {
                contentValues.putNull("message");
            }
            if (h7 > 0) {
                if (h8 > 0) {
                    withAppendedPath = Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
                } else {
                    withAppendedPath = Uri.withAppendedPath(MediaStore.Audio.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
                }
                contentValues.put("alert", withAppendedPath.toString());
            }
            int update = context.getContentResolver().update(withAppendedPath2, contentValues, null, null);
            context.getContentResolver().notifyChange(withAppendedPath2, null);
            context.sendBroadcast(new Intent("android.intent.action.TIME_SET"));
            if (update > 0) {
                cVar.a(0);
            } else {
                cVar.a(1);
            }
        }
    }

    public void d(Context context, c cVar) {
        boolean z;
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        int h3 = cVar.h();
        if (h2 < 1 || h2 > 3) {
            cVar.a(1);
        } else if (com.qq.a.a.a.f256a == null) {
            cVar.a(7);
        } else {
            Uri withAppendedPath = Uri.withAppendedPath(com.qq.a.a.a.f256a, Constants.STR_EMPTY + h);
            ContentValues contentValues = new ContentValues();
            if ((h2 == 1 || h2 == 2) && h3 > 0) {
                z = true;
            } else {
                z = false;
            }
            if (h2 == 1) {
                contentValues.put("enabled", Boolean.valueOf(z));
            } else if (h2 == 2) {
                contentValues.put("vibrate", Boolean.valueOf(z));
            } else {
                contentValues.put("daysofweek", Integer.valueOf(h3));
            }
            if (context.getContentResolver().update(withAppendedPath, contentValues, null, null) > 0) {
                context.sendBroadcast(new Intent("android.intent.action.TIME_SET"));
                cVar.a(0);
                return;
            }
            cVar.a(1);
        }
    }

    public void e(Context context, c cVar) {
        Uri withAppendedPath;
        if (cVar.e() < 8) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        int h3 = cVar.h();
        cVar.g();
        int h4 = cVar.h();
        int h5 = cVar.h();
        String j = cVar.j();
        int h6 = cVar.h();
        int h7 = cVar.h();
        if (com.qq.a.a.a.f256a == null) {
            cVar.a(7);
            return;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("hour", Integer.valueOf(h));
        contentValues.put("minutes", Integer.valueOf(h2));
        contentValues.put("daysofweek", Integer.valueOf(h3));
        contentValues.put("enabled", Integer.valueOf(h4));
        contentValues.put("vibrate", Integer.valueOf(h5));
        if (!s.b(j)) {
            contentValues.put("message", j);
        } else {
            contentValues.put("message", Constants.STR_EMPTY);
        }
        if (h6 > 0) {
            if (h7 > 0) {
                withAppendedPath = Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h6);
            } else {
                withAppendedPath = Uri.withAppendedPath(MediaStore.Audio.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + h6);
            }
            contentValues.put("alert", withAppendedPath.toString());
        }
        try {
            int parseInt = Integer.parseInt(context.getContentResolver().insert(com.qq.a.a.a.f256a, contentValues).getLastPathSegment());
            context.sendBroadcast(new Intent("android.intent.action.TIME_SET"));
            ArrayList arrayList = new ArrayList();
            arrayList.add(s.a(parseInt));
            cVar.a(arrayList);
        } catch (Exception e) {
            cVar.a(8);
        }
    }

    public void f(Context context, c cVar) {
        Uri withAppendedPath;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int e = cVar.e();
        if (cVar.e() < e * 8) {
            cVar.a(1);
        } else if (com.qq.a.a.a.f256a == null) {
            cVar.a(7);
        } else {
            context.getContentResolver().delete(com.qq.a.a.a.f256a, "enabled = 0", null);
            ArrayList arrayList = new ArrayList();
            arrayList.add(s.a(e));
            ContentValues contentValues = new ContentValues();
            for (int i = 0; i < e; i++) {
                int h = cVar.h();
                int h2 = cVar.h();
                int h3 = cVar.h();
                cVar.g();
                int h4 = cVar.h();
                int h5 = cVar.h();
                String j = cVar.j();
                int h6 = cVar.h();
                int h7 = cVar.h();
                contentValues.put("hour", Integer.valueOf(h));
                contentValues.put("minutes", Integer.valueOf(h2));
                contentValues.put("daysofweek", Integer.valueOf(h3));
                contentValues.put("enabled", Integer.valueOf(h4));
                contentValues.put("vibrate", Integer.valueOf(h5));
                if (!s.b(j)) {
                    contentValues.put("message", j);
                } else {
                    contentValues.putNull("message");
                }
                if (h6 > 0) {
                    if (h7 > 0) {
                        withAppendedPath = Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h6);
                    } else {
                        withAppendedPath = Uri.withAppendedPath(MediaStore.Audio.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + h6);
                    }
                    contentValues.put("alert", withAppendedPath.toString());
                }
                int i2 = 0;
                try {
                    i2 = Integer.parseInt(context.getContentResolver().insert(com.qq.a.a.a.f256a, contentValues).getLastPathSegment());
                } catch (Exception e2) {
                }
                arrayList.add(s.a(i2));
                contentValues.clear();
            }
            context.sendBroadcast(new Intent("android.intent.action.TIME_SET"));
            cVar.a(arrayList);
            cVar.a(0);
        }
    }

    public void g(Context context, c cVar) {
        Uri uri;
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        int h3 = cVar.h();
        if (com.qq.a.a.a.f256a == null) {
            cVar.a(7);
            return;
        }
        Uri withAppendedPath = Uri.withAppendedPath(com.qq.a.a.a.f256a, Constants.STR_EMPTY + h);
        ContentValues contentValues = new ContentValues();
        if (h2 <= 0) {
            contentValues.putNull("alert");
        } else {
            if (h3 > 0) {
                uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            } else {
                uri = MediaStore.Audio.Media.INTERNAL_CONTENT_URI;
            }
            contentValues.put("alert", Uri.withAppendedPath(uri, Constants.STR_EMPTY + h2).toString());
        }
        if (context.getContentResolver().update(withAppendedPath, contentValues, null, null) > 0) {
            context.sendBroadcast(new Intent("android.intent.action.TIME_SET"));
            cVar.a(0);
            return;
        }
        cVar.a(3);
    }

    public void h(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (com.qq.a.a.a.f256a == null) {
            cVar.a(7);
            return;
        }
        if (context.getContentResolver().delete(Uri.withAppendedPath(com.qq.a.a.a.f256a, Constants.STR_EMPTY + h), null, null) > 0) {
            context.sendBroadcast(new Intent("android.intent.action.TIME_SET"));
            cVar.a(0);
            return;
        }
        cVar.a(1);
    }

    public void i(Context context, c cVar) {
        if (com.qq.a.a.a.f256a == null) {
            cVar.a(7);
        } else if (context.getContentResolver().delete(com.qq.a.a.a.f256a, null, null) > 0) {
            context.sendBroadcast(new Intent("android.intent.action.TIME_SET"));
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }
}
