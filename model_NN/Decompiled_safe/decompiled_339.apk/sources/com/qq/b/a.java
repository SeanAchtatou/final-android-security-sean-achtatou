package com.qq.b;

import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import com.qq.AppService.AuthorReceiver;
import com.qq.ndk.NativeFileObject;
import com.qq.util.j;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketTimeoutException;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static volatile boolean f261a = false;
    public static boolean b = false;
    public static boolean c = false;
    private LocalSocket d;
    private InputStream e;
    private OutputStream f;
    private volatile int g;
    private int h;

    public static String a(String str, int i) {
        if (j.c >= 8 && i == 1) {
            return "/system/bin/pm install  -r -f " + str;
        }
        if (j.c < 8 || i != 2) {
            return "/system/bin/pm install  -r " + str;
        }
        return "/system/bin/pm install  -r -s " + str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0026  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:52:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r7) {
        /*
            r1 = 0
            r6 = 2000(0x7d0, float:2.803E-42)
            r5 = 12
            r4 = 0
            boolean r0 = com.qq.b.a.f261a
            if (r0 != 0) goto L_0x0083
            int r0 = com.qq.util.j.c
            r2 = 9
            if (r0 < r2) goto L_0x0081
            android.content.pm.ApplicationInfo r2 = r7.getApplicationInfo()
            java.lang.Class<android.content.pm.ApplicationInfo> r0 = android.content.pm.ApplicationInfo.class
            java.lang.String r3 = "nativeLibraryDir"
            java.lang.reflect.Field r0 = r0.getField(r3)     // Catch:{ SecurityException -> 0x006b, NoSuchFieldException -> 0x0071 }
        L_0x001c:
            if (r0 == 0) goto L_0x0081
            java.lang.Object r0 = r0.get(r2)     // Catch:{ IllegalArgumentException -> 0x0077, Throwable -> 0x007d }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IllegalArgumentException -> 0x0077, Throwable -> 0x007d }
        L_0x0024:
            if (r0 != 0) goto L_0x003f
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            android.content.pm.ApplicationInfo r1 = r7.getApplicationInfo()
            java.lang.String r1 = r1.dataDir
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "/lib"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
        L_0x003f:
            java.io.File r1 = new java.io.File
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = java.io.File.separator
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = "libaurora.so"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            boolean r0 = r1.exists()
            if (r0 == 0) goto L_0x0066
            r0 = 1
            com.qq.b.a.f261a = r0
        L_0x0066:
            boolean r0 = com.qq.b.a.f261a
            if (r0 != 0) goto L_0x0083
        L_0x006a:
            return
        L_0x006b:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r1
            goto L_0x001c
        L_0x0071:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r1
            goto L_0x001c
        L_0x0077:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r1
            goto L_0x0024
        L_0x007d:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0081:
            r0 = r1
            goto L_0x0024
        L_0x0083:
            boolean r0 = com.qq.b.a.c
            if (r0 == 0) goto L_0x008f
            java.lang.String r0 = com.qq.AppService.AuthorReceiver.b()
            if (r0 != 0) goto L_0x008f
            com.qq.b.a.c = r4
        L_0x008f:
            boolean r0 = com.qq.b.a.b
            if (r0 == 0) goto L_0x009b
            java.lang.String r0 = com.qq.AppService.AuthorReceiver.c()
            if (r0 != 0) goto L_0x009b
            com.qq.b.a.b = r4
        L_0x009b:
            boolean r0 = com.qq.b.a.c
            if (r0 == 0) goto L_0x00a3
            boolean r0 = com.qq.b.a.b
            if (r0 != 0) goto L_0x006a
        L_0x00a3:
            boolean r0 = com.qq.b.a.c
            if (r0 != 0) goto L_0x00c4
            com.qq.b.a r0 = new com.qq.b.a
            r0.<init>()
            java.lang.String r1 = "aurora_root"
            boolean r1 = r0.a(r1)
            if (r1 == 0) goto L_0x00c1
            r0.a(r6)
            com.qq.b.b r1 = com.qq.b.b.a(r5)
            r0.a(r1)
            r0.b()
        L_0x00c1:
            r0.a()
        L_0x00c4:
            boolean r0 = com.qq.b.a.b
            if (r0 != 0) goto L_0x006a
            com.qq.b.a r0 = new com.qq.b.a
            r0.<init>()
            java.lang.String r1 = "aurora_usb"
            boolean r1 = r0.a(r1)
            if (r1 == 0) goto L_0x00e2
            r0.a(r6)
            com.qq.b.b r1 = com.qq.b.b.a(r5)
            r0.a(r1)
            r0.b()
        L_0x00e2:
            r0.a()
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.b.a.a(android.content.Context):void");
    }

    public a() {
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = 0;
        this.h = 15000;
        this.d = new LocalSocket();
    }

    public boolean a(String str) {
        if (this.d != null) {
            try {
                this.d.connect(new LocalSocketAddress(str, LocalSocketAddress.Namespace.ABSTRACT));
                return true;
            } catch (IOException e2) {
                this.g = -2;
                return false;
            }
        } else {
            this.g = -3;
            return false;
        }
    }

    public c a(int i, String str, int i2, int i3) {
        c cVar = null;
        if (c) {
            if (a("aurora_root")) {
                a(i3);
                b a2 = b.a(i);
                if ("aurora_root".equals("aurora_root")) {
                    a2.b = AuthorReceiver.b();
                }
                a2.a(str);
                a2.c = i2;
                a(a2);
                cVar = b();
                if (cVar.f263a == 200) {
                }
                a();
            } else {
                a();
            }
        }
        return cVar;
    }

    public c b(String str, int i) {
        c cVar = null;
        if (c) {
            if (a("aurora_root")) {
                a(i);
                b a2 = b.a(3);
                if ("aurora_root".equals("aurora_root")) {
                    a2.b = AuthorReceiver.b();
                }
                a2.a(str);
                a(a2);
                cVar = b();
                if (cVar.f263a == 200) {
                    byte[] bArr = new byte[NativeFileObject.S_IFIFO];
                    int a3 = a(bArr, 0, bArr.length, i);
                    if (a3 > 0) {
                        cVar.c = new byte[a3];
                        System.arraycopy(bArr, 0, cVar.c, 0, a3);
                    }
                }
                a();
            } else {
                a();
            }
        }
        return cVar;
    }

    public c c(String str, int i) {
        String str2;
        if (c) {
            str2 = "aurora_root";
        } else if (!b) {
            return null;
        } else {
            str2 = "aurora_usb";
        }
        if (a(str2)) {
            a(i);
            b a2 = b.a(3);
            if (str2.equals("aurora_root")) {
                a2.b = AuthorReceiver.b();
            } else if (str2.equals("aurora_usb")) {
                a2.b = AuthorReceiver.c();
            }
            a2.a(str);
            a(a2);
            c b2 = b();
            if (b2.f263a == 200) {
                byte[] bArr = new byte[NativeFileObject.S_IFIFO];
                int b3 = b(bArr, 0, bArr.length, i);
                if (b3 > 0) {
                    b2.c = new byte[b3];
                    System.arraycopy(bArr, 0, b2.c, 0, b3);
                }
            }
            a();
            return b2;
        }
        a();
        return null;
    }

    public c d(String str, int i) {
        String str2;
        if (c) {
            str2 = "aurora_root";
        } else if (!b) {
            return null;
        } else {
            str2 = "aurora_usb";
        }
        if (a(str2)) {
            a(i);
            b a2 = b.a(3);
            if (str2.equals("aurora_root")) {
                a2.b = AuthorReceiver.b();
            } else if (str2.equals("aurora_usb")) {
                a2.b = AuthorReceiver.c();
            }
            a2.a(str);
            a(a2);
            c b2 = b();
            if (b2.f263a == 200) {
                byte[] bArr = new byte[NativeFileObject.S_IFIFO];
                int a3 = a(bArr, 0, bArr.length, i);
                if (a3 > 0) {
                    b2.c = new byte[a3];
                    System.arraycopy(bArr, 0, b2.c, 0, a3);
                }
            }
            a();
            return b2;
        }
        a();
        return null;
    }

    public void a() {
        if (this.e != null) {
            try {
                this.e.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            this.e = null;
        }
        if (this.f != null) {
            try {
                this.f.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            this.f = null;
        }
        if (this.d != null) {
            try {
                this.d.close();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
            this.d = null;
        }
    }

    private void c() {
        if (this.d != null && this.d.isConnected()) {
            if (this.e == null) {
                try {
                    this.e = this.d.getInputStream();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            if (this.f == null) {
                try {
                    this.f = this.d.getOutputStream();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            }
        }
    }

    private void a(byte[] bArr) {
        if (this.f != null) {
            try {
                this.f.write(bArr);
                this.f.flush();
            } catch (Throwable th) {
                th.printStackTrace();
                this.g = -4;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0052  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.qq.b.c r8) {
        /*
            r7 = this;
            r6 = -7
            r1 = 0
            r5 = 4
            int r0 = r7.g
            if (r0 >= 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            byte[] r2 = new byte[r5]
            java.io.InputStream r0 = r7.e
            if (r0 == 0) goto L_0x002d
            java.io.InputStream r0 = r7.e     // Catch:{ IOException -> 0x0024 }
            int r0 = r0.read(r2)     // Catch:{ IOException -> 0x0024 }
        L_0x0014:
            if (r0 != r5) goto L_0x002a
            int r0 = com.qq.AppService.s.a(r2)
            r2 = r0
        L_0x001b:
            if (r2 < r5) goto L_0x0021
            r0 = 1024(0x400, float:1.435E-42)
            if (r2 <= r0) goto L_0x002f
        L_0x0021:
            r7.g = r6
            goto L_0x0007
        L_0x0024:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r1
            goto L_0x0014
        L_0x002a:
            r0 = -5
            r7.g = r0
        L_0x002d:
            r2 = r1
            goto L_0x001b
        L_0x002f:
            byte[] r3 = new byte[r2]
            r0 = 1
            java.io.InputStream r4 = r7.e
            if (r4 == 0) goto L_0x0050
        L_0x0036:
            if (r0 <= 0) goto L_0x0046
            if (r1 >= r2) goto L_0x0046
            java.io.InputStream r0 = r7.e     // Catch:{ Throwable -> 0x004c }
            int r4 = r2 - r1
            int r0 = r0.read(r3, r1, r4)     // Catch:{ Throwable -> 0x004c }
            if (r0 <= 0) goto L_0x0036
            int r1 = r1 + r0
            goto L_0x0036
        L_0x0046:
            r0 = r1
        L_0x0047:
            if (r0 >= r2) goto L_0x0052
            r7.g = r6
            goto L_0x0007
        L_0x004c:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0050:
            r0 = r1
            goto L_0x0047
        L_0x0052:
            int r0 = com.qq.AppService.s.a(r3)
            r8.f263a = r0
            if (r2 <= r5) goto L_0x0007
            java.lang.String r0 = new java.lang.String     // Catch:{ UnsupportedEncodingException -> 0x0067 }
            r1 = 4
            int r2 = r2 + -4
            java.lang.String r4 = "UTF-8"
            r0.<init>(r3, r1, r2, r4)     // Catch:{ UnsupportedEncodingException -> 0x0067 }
            r8.b = r0     // Catch:{ UnsupportedEncodingException -> 0x0067 }
            goto L_0x0007
        L_0x0067:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.b.a.a(com.qq.b.c):void");
    }

    public void a(b bVar) {
        c();
        byte[] a2 = b.a(bVar);
        if (a2 == null) {
            this.g = -6;
        } else {
            a(a2);
        }
    }

    public void a(int i) {
        this.h = i;
        if (this.d != null) {
            try {
                this.d.setSoTimeout(i);
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    public c b() {
        c cVar = new c();
        a(cVar);
        return cVar;
    }

    public int a(byte[] bArr, int i, int i2, int i3) {
        int i4;
        if (this.e == null) {
            return -1;
        }
        int i5 = i3 / this.h;
        int i6 = 0;
        do {
            try {
                i4 = this.e.read(bArr, i + i6, i2 - i6);
                if (i4 > 0) {
                    i6 += i4;
                }
            } catch (SocketTimeoutException e2) {
                i5--;
                i4 = 0;
            } catch (IOException e3) {
                String iOException = e3.toString();
                if (iOException == null || !iOException.contains("Try again")) {
                    e3.printStackTrace();
                    i4 = -1;
                } else {
                    i5--;
                    i4 = 0;
                }
            }
            if (i4 < 0) {
                return i6;
            }
        } while (i5 >= 0);
        return i6;
    }

    public int b(byte[] bArr, int i, int i2, int i3) {
        int i4;
        if (this.e == null) {
            return -1;
        }
        int i5 = i3 / this.h;
        int i6 = 0;
        do {
            try {
                i4 = this.e.read(bArr, i + i6, i2 - i6);
                if (i4 > 0) {
                    i6 += i4;
                    String str = new String(bArr, 0, i6);
                    if (str != null && (str.contains("Success") || str.contains("Failure"))) {
                        return i6;
                    }
                }
            } catch (SocketTimeoutException e2) {
                i5--;
                i4 = 0;
            } catch (IOException e3) {
                String iOException = e3.toString();
                if (iOException == null || !iOException.contains("Try again")) {
                    e3.printStackTrace();
                    return i6;
                }
                i5--;
                i4 = 0;
            }
            if (i4 < 0) {
                return i6;
            }
        } while (i5 >= 0);
        return i6;
    }
}
