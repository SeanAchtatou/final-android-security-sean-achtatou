package com.qq.d.g;

import android.os.SystemProperties;
import android.text.TextUtils;
import java.util.Map;

/* compiled from: ProGuard */
public class g extends r {
    public boolean a(Map<String, String> map, boolean z, s sVar) {
        boolean z2;
        boolean z3;
        boolean z4 = false;
        String str = SystemProperties.get("ro.lewa.version");
        String str2 = SystemProperties.get("ro.lewa.version.type");
        if (TextUtils.isEmpty(str) || z) {
            z2 = false;
        } else {
            a(map, "rombrand", "lewa");
            a(map, "romversion", str);
            if ("1".equals(str2)) {
                a(map, "rombranch", "stable");
            } else {
                a(map, "rombranch", "develop");
            }
            z2 = true;
        }
        if (this.b == null) {
            return z2;
        }
        r rVar = this.b;
        if (z || z2) {
            z3 = true;
        } else {
            z3 = false;
        }
        boolean a2 = rVar.a(map, z3, sVar);
        if (z2 || a2) {
            z4 = true;
        }
        return z4;
    }
}
