package com.qq.d.g;

import android.os.SystemProperties;
import android.text.TextUtils;
import com.tencent.connect.common.Constants;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: ProGuard */
public class w extends r {
    public boolean a(Map<String, String> map, boolean z, s sVar) {
        boolean z2;
        boolean z3;
        boolean z4 = false;
        String str = SystemProperties.get("ro.build.version.nanji.display");
        boolean c = c("ro.build.nanji.releaseTime");
        boolean c2 = c("ro.tita.device");
        if (d(str) || c || c2) {
            if (!z) {
                a(map, "rombrand", "tita");
                a(map, "romversion", str);
                a(map, "rombranch", a(str));
            }
            z2 = true;
        } else {
            z2 = false;
        }
        if (this.b == null) {
            return z2;
        }
        r rVar = this.b;
        if (z || z2) {
            z3 = true;
        } else {
            z3 = false;
        }
        boolean a2 = rVar.a(map, z3, sVar);
        if (z2 || a2) {
            z4 = true;
        }
        return z4;
    }

    private String a(String str) {
        Matcher matcher = Pattern.compile("tita_([\\d\\.]+)_(\\w+)").matcher(str.toLowerCase());
        if (matcher.find()) {
            return matcher.group(2);
        }
        return Constants.STR_EMPTY;
    }

    private boolean d(String str) {
        return !TextUtils.isEmpty(str);
    }
}
