package com.qq.util;

import android.content.pm.Signature;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

/* compiled from: ProGuard */
public class aa {
    public static X509Certificate a(Signature signature) {
        X509Certificate x509Certificate;
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(signature.toByteArray());
        try {
            x509Certificate = (X509Certificate) CertificateFactory.getInstance("X509").generateCertificate(byteArrayInputStream);
        } catch (Exception e) {
            e.printStackTrace();
            x509Certificate = null;
        }
        try {
            byteArrayInputStream.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return x509Certificate;
    }
}
