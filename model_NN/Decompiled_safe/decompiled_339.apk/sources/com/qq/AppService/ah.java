package com.qq.AppService;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import com.qq.a.a.d;
import com.qq.d.f.a;
import com.qq.provider.h;
import com.tencent.open.SocialConstants;
import java.util.Vector;

/* compiled from: ProGuard */
public class ah {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public volatile boolean f216a = true;
    /* access modifiers changed from: private */
    public Vector<aj> b = new Vector<>();
    /* access modifiers changed from: private */
    public Object c = new Object();
    /* access modifiers changed from: private */
    public volatile boolean d = false;
    private Context e;
    private ak f = null;

    public boolean a() {
        return this.b.isEmpty();
    }

    public ah(Context context) {
        this.e = context.getApplicationContext();
    }

    public synchronized void b() {
        this.f = new ak(this);
        this.f.start();
    }

    public void c() {
        this.f216a = false;
        synchronized (this) {
            notifyAll();
        }
        synchronized (this.c) {
            this.c.notifyAll();
        }
        this.b.clear();
    }

    public void d() {
        synchronized (this.c) {
            this.d = true;
            this.c.notifyAll();
        }
    }

    public void a(aj ajVar) {
        Cursor cursor = null;
        if (ajVar != null) {
            Cursor cursor2 = null;
            try {
                this.e.getContentResolver().query(d.f259a, null, "address like '" + ajVar.b + '\'' + " AND " + "read" + " = 0 AND " + SocialConstants.PARAM_TYPE + " = " + 1 + " AND " + "date" + " > " + aq.f223a, null, "_id DESC");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            if (cursor == null) {
                Log.d("com.qq.connect", "cursor is null");
                return;
            }
            try {
                if (cursor2.moveToFirst()) {
                    String string = cursor2.getString(cursor2.getColumnIndex("body"));
                    String string2 = cursor2.getString(cursor2.getColumnIndex("address"));
                    int i = cursor2.getInt(cursor2.getColumnIndex(SocialConstants.PARAM_TYPE));
                    a aVar = new a();
                    aVar.j = string;
                    aVar.c = string2;
                    aVar.f281a = cursor2.getInt(cursor2.getColumnIndex("_id"));
                    aVar.b = cursor2.getInt(cursor2.getColumnIndex("thread_id"));
                    aVar.g = cursor2.getInt(cursor2.getColumnIndex("status"));
                    aVar.h = i;
                    aVar.f = cursor2.getInt(cursor2.getColumnIndex("read"));
                    aVar.e = s.b(cursor2.getLong(cursor2.getColumnIndex("date")));
                    aVar.d = cursor2.getString(cursor2.getColumnIndex("person"));
                    aVar.i = cursor2.getString(cursor2.getColumnIndex("subject"));
                    aq.f223a = System.currentTimeMillis();
                    h.a(aVar);
                } else {
                    b(ajVar);
                }
                if (cursor == null) {
                    return;
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
            cursor.close();
        }
    }

    public void b(aj ajVar) {
        this.b.add(ajVar);
        synchronized (this) {
            notifyAll();
        }
    }

    public void a(String str, String str2) {
        this.b.add(new aj(str, str2, System.currentTimeMillis()));
        synchronized (this) {
            notifyAll();
        }
    }

    public aj e() {
        if (this.b.size() <= 0) {
            return null;
        }
        aj ajVar = this.b.get(0);
        this.b.remove(0);
        return ajVar;
    }
}
