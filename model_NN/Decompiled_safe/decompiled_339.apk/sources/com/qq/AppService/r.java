package com.qq.AppService;

import android.content.Context;
import android.util.Log;
import com.qq.k.b;
import com.qq.provider.cache2.a;
import com.qq.provider.o;
import com.qq.util.j;

/* compiled from: ProGuard */
public class r extends Thread {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f250a = false;
    private Context b = null;
    private int c = 0;
    private String d = null;

    public r(Context context, int i) {
        this.b = context.getApplicationContext();
        this.c = i;
        f250a = b.c(context);
        o.a(context);
    }

    public void run() {
        super.run();
        if (this.b != null && this.c == 1 && this.d != null) {
            a.a(this.b, this.d);
        } else if (this.b != null && this.c >= 0) {
            try {
                long currentTimeMillis = System.currentTimeMillis();
                a.b(this.b);
                Log.d("com.qq.connect", "updateAllIconCache spent time: " + (System.currentTimeMillis() - currentTimeMillis));
            } catch (Throwable th) {
                th.printStackTrace();
                Log.d("com.qq.connect", th.toString() + th.getLocalizedMessage());
            }
            if (!f250a) {
                b.b(this.b);
                f250a = true;
            }
            j.f370a = 0;
        }
    }
}
