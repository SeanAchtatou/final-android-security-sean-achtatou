package com.google.zxing.d.a;

final class b {

    /* renamed from: a  reason: collision with root package name */
    private final int f175a;
    private final byte[] b;

    private b(int i, byte[] bArr) {
        this.f175a = i;
        this.b = bArr;
    }

    static b[] a(byte[] bArr, r rVar, o oVar) {
        if (bArr.length != rVar.c()) {
            throw new IllegalArgumentException();
        }
        t a2 = rVar.a(oVar);
        s[] b2 = a2.b();
        int i = 0;
        for (s a3 : b2) {
            i += a3.a();
        }
        b[] bVarArr = new b[i];
        int i2 = 0;
        for (s sVar : b2) {
            int i3 = 0;
            while (i3 < sVar.a()) {
                int b3 = sVar.b();
                bVarArr[i2] = new b(b3, new byte[(a2.a() + b3)]);
                i3++;
                i2++;
            }
        }
        int length = bVarArr[0].b.length;
        int length2 = bVarArr.length - 1;
        while (length2 >= 0 && bVarArr[length2].b.length != length) {
            length2--;
        }
        int i4 = length2 + 1;
        int a4 = length - a2.a();
        int i5 = 0;
        int i6 = 0;
        while (i5 < a4) {
            int i7 = i6;
            int i8 = 0;
            while (i8 < i2) {
                bVarArr[i8].b[i5] = bArr[i7];
                i8++;
                i7++;
            }
            i5++;
            i6 = i7;
        }
        int i9 = i4;
        while (i9 < i2) {
            bVarArr[i9].b[a4] = bArr[i6];
            i9++;
            i6++;
        }
        int length3 = bVarArr[0].b.length;
        while (a4 < length3) {
            int i10 = 0;
            int i11 = i6;
            while (i10 < i2) {
                bVarArr[i10].b[i10 < i4 ? a4 : a4 + 1] = bArr[i11];
                i10++;
                i11++;
            }
            a4++;
            i6 = i11;
        }
        return bVarArr;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.f175a;
    }

    /* access modifiers changed from: package-private */
    public byte[] b() {
        return this.b;
    }
}
