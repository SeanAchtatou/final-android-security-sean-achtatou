package com.google.zxing.a.a;

import com.google.zxing.ChecksumException;
import com.google.zxing.common.b;
import com.google.zxing.common.g;
import com.google.zxing.common.reedsolomon.ReedSolomonException;
import com.google.zxing.common.reedsolomon.a;
import com.google.zxing.common.reedsolomon.c;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private final c f119a = new c(a.b);

    private void a(byte[] bArr, int i) {
        int length = bArr.length;
        int[] iArr = new int[length];
        for (int i2 = 0; i2 < length; i2++) {
            iArr[i2] = bArr[i2] & 255;
        }
        try {
            this.f119a.a(iArr, bArr.length - i);
            for (int i3 = 0; i3 < i; i3++) {
                bArr[i3] = (byte) iArr[i3];
            }
        } catch (ReedSolomonException e) {
            throw ChecksumException.a();
        }
    }

    public g a(b bVar) {
        a aVar = new a(bVar);
        b[] a2 = b.a(aVar.a(), aVar.a(bVar));
        int i = 0;
        for (b a3 : a2) {
            i += a3.a();
        }
        byte[] bArr = new byte[i];
        int i2 = 0;
        for (b bVar2 : a2) {
            byte[] b = bVar2.b();
            int a4 = bVar2.a();
            a(b, a4);
            int i3 = 0;
            while (i3 < a4) {
                bArr[i2] = b[i3];
                i3++;
                i2++;
            }
        }
        return c.a(bArr);
    }
}
