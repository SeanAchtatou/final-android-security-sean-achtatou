package com.google.zxing.b.a;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;
import com.google.zxing.d;
import com.google.zxing.h;
import com.google.zxing.j;
import com.google.zxing.k;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.st.STConst;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public final class e extends a {
    private static final int[] g = {1, 10, 34, 70, 126};
    private static final int[] h = {4, 20, 48, 81};
    private static final int[] i = {0, 161, 961, STConst.ST_PAGE_SPLASH, 2715};
    private static final int[] j = {0, 336, EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FAILED, 1516};
    private static final int[] k = {8, 6, 4, 3, 1};
    private static final int[] l = {2, 4, 6, 8};
    private static final int[][] m = {new int[]{3, 8, 2, 1}, new int[]{3, 5, 5, 1}, new int[]{3, 3, 7, 1}, new int[]{3, 1, 9, 1}, new int[]{2, 7, 4, 1}, new int[]{2, 5, 6, 1}, new int[]{2, 3, 8, 1}, new int[]{1, 5, 7, 1}, new int[]{1, 3, 9, 1}};
    private final Vector n = new Vector();
    private final Vector o = new Vector();

    private b a(a aVar, c cVar, boolean z) {
        int[] iArr = this.b;
        iArr[0] = 0;
        iArr[1] = 0;
        iArr[2] = 0;
        iArr[3] = 0;
        iArr[4] = 0;
        iArr[5] = 0;
        iArr[6] = 0;
        iArr[7] = 0;
        if (z) {
            b(aVar, cVar.b()[0], iArr);
        } else {
            a(aVar, cVar.b()[1] + 1, iArr);
            int i2 = 0;
            for (int length = iArr.length - 1; i2 < length; length--) {
                int i3 = iArr[i2];
                iArr[i2] = iArr[length];
                iArr[length] = i3;
                i2++;
            }
        }
        int i4 = z ? 16 : 15;
        float a2 = ((float) a(iArr)) / ((float) i4);
        int[] iArr2 = this.e;
        int[] iArr3 = this.f;
        float[] fArr = this.c;
        float[] fArr2 = this.d;
        for (int i5 = 0; i5 < iArr.length; i5++) {
            float f = ((float) iArr[i5]) / a2;
            int i6 = (int) (0.5f + f);
            if (i6 < 1) {
                i6 = 1;
            } else if (i6 > 8) {
                i6 = 8;
            }
            int i7 = i5 >> 1;
            if ((i5 & 1) == 0) {
                iArr2[i7] = i6;
                fArr[i7] = f - ((float) i6);
            } else {
                iArr3[i7] = i6;
                fArr2[i7] = f - ((float) i6);
            }
        }
        a(z, i4);
        int length2 = iArr2.length - 1;
        int i8 = 0;
        int i9 = 0;
        while (length2 >= 0) {
            int i10 = (i8 * 9) + iArr2[length2];
            length2--;
            i8 = i10;
            i9 = iArr2[length2] + i9;
        }
        int i11 = 0;
        int i12 = 0;
        for (int length3 = iArr3.length - 1; length3 >= 0; length3--) {
            i11 = (i11 * 9) + iArr3[length3];
            i12 += iArr3[length3];
        }
        int i13 = i8 + (i11 * 3);
        if (z) {
            if ((i9 & 1) != 0 || i9 > 12 || i9 < 4) {
                throw NotFoundException.a();
            }
            int i14 = (12 - i9) / 2;
            int i15 = k[i14];
            return new b((f.a(iArr2, i15, false) * g[i14]) + f.a(iArr3, 9 - i15, true) + i[i14], i13);
        } else if ((i12 & 1) != 0 || i12 > 10 || i12 < 4) {
            throw NotFoundException.a();
        } else {
            int i16 = (10 - i12) / 2;
            int i17 = l[i16];
            return new b(f.a(iArr2, i17, true) + (f.a(iArr3, 9 - i17, false) * h[i16]) + j[i16], i13);
        }
    }

    private c a(a aVar, int i2, boolean z, int[] iArr) {
        int i3;
        boolean a2 = aVar.a(iArr[0]);
        int i4 = iArr[0] - 1;
        while (i4 >= 0 && (aVar.a(i4) ^ a2)) {
            i4--;
        }
        int i5 = i4 + 1;
        int i6 = iArr[0] - i5;
        int[] iArr2 = this.f127a;
        for (int length = iArr2.length - 1; length > 0; length--) {
            iArr2[length] = iArr2[length - 1];
        }
        iArr2[0] = i6;
        int a3 = a(iArr2, m);
        int i7 = iArr[1];
        if (z) {
            i3 = (aVar.a() - 1) - i5;
            i7 = (aVar.a() - 1) - i7;
        } else {
            i3 = i5;
        }
        return new c(a3, new int[]{i5, iArr[1]}, i3, i7, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.b.a.e.a(com.google.zxing.common.a, com.google.zxing.b.a.c, boolean):com.google.zxing.b.a.b
     arg types: [com.google.zxing.common.a, com.google.zxing.b.a.c, int]
     candidates:
      com.google.zxing.b.a.e.a(com.google.zxing.common.a, int, boolean):int[]
      com.google.zxing.b.a.e.a(int, com.google.zxing.common.a, java.util.Hashtable):com.google.zxing.h
      com.google.zxing.b.k.a(int[], int[], int):int
      com.google.zxing.b.k.a(com.google.zxing.common.a, int, int[]):void
      com.google.zxing.b.k.a(int, com.google.zxing.common.a, java.util.Hashtable):com.google.zxing.h
      com.google.zxing.b.a.e.a(com.google.zxing.common.a, com.google.zxing.b.a.c, boolean):com.google.zxing.b.a.b */
    private d a(a aVar, boolean z, int i2, Hashtable hashtable) {
        try {
            int[] a2 = a(aVar, 0, z);
            c a3 = a(aVar, i2, z, a2);
            k kVar = hashtable == null ? null : (k) hashtable.get(d.h);
            if (kVar != null) {
                float f = ((float) (a2[0] + a2[1])) / 2.0f;
                if (z) {
                    f = ((float) (aVar.a() - 1)) - f;
                }
                kVar.a(new j(f, (float) i2));
            }
            b a4 = a(aVar, a3, true);
            b a5 = a(aVar, a3, false);
            return new d((a4.a() * 1597) + a5.a(), a4.b() + (a5.b() * 4), a3);
        } catch (NotFoundException e) {
            return null;
        }
    }

    private static h a(d dVar, d dVar2) {
        String valueOf = String.valueOf((4537077 * ((long) dVar.a())) + ((long) dVar2.a()));
        StringBuffer stringBuffer = new StringBuffer(14);
        for (int length = 13 - valueOf.length(); length > 0; length--) {
            stringBuffer.append('0');
        }
        stringBuffer.append(valueOf);
        int i2 = 0;
        for (int i3 = 0; i3 < 13; i3++) {
            int charAt = stringBuffer.charAt(i3) - '0';
            if ((i3 & 1) == 0) {
                charAt *= 3;
            }
            i2 += charAt;
        }
        int i4 = 10 - (i2 % 10);
        if (i4 == 10) {
            i4 = 0;
        }
        stringBuffer.append(i4);
        j[] c = dVar.c().c();
        j[] c2 = dVar2.c().c();
        return new h(String.valueOf(stringBuffer.toString()), null, new j[]{c[0], c[1], c2[0], c2[1]}, com.google.zxing.a.m);
    }

    private static void a(Vector vector, d dVar) {
        boolean z;
        if (dVar != null) {
            Enumeration elements = vector.elements();
            while (true) {
                if (!elements.hasMoreElements()) {
                    z = false;
                    break;
                }
                d dVar2 = (d) elements.nextElement();
                if (dVar2.a() == dVar.a()) {
                    dVar2.e();
                    z = true;
                    break;
                }
            }
            if (!z) {
                vector.addElement(dVar);
            }
        }
    }

    private void a(boolean z, int i2) {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        boolean z8;
        boolean z9 = false;
        boolean z10 = true;
        int a2 = a(this.e);
        int a3 = a(this.f);
        int i3 = (a2 + a3) - i2;
        boolean z11 = (a2 & 1) == (z ? 1 : 0);
        boolean z12 = (a3 & 1) == 1;
        if (z) {
            if (a2 > 12) {
                z3 = true;
                z2 = false;
            } else if (a2 < 4) {
                z3 = false;
                z2 = true;
            } else {
                z3 = false;
                z2 = false;
            }
            if (a3 > 12) {
                z4 = false;
                z9 = true;
            } else {
                if (a3 < 4) {
                    z4 = true;
                }
                z4 = false;
            }
        } else {
            if (a2 > 11) {
                z5 = true;
                z6 = false;
            } else if (a2 < 5) {
                z5 = false;
                z6 = true;
            } else {
                z5 = false;
                z6 = false;
            }
            if (a3 > 10) {
                z4 = false;
                z9 = true;
            } else {
                if (a3 < 4) {
                    z4 = true;
                }
                z4 = false;
            }
        }
        if (i3 == 1) {
            if (z11) {
                if (z12) {
                    throw NotFoundException.a();
                }
                z8 = z2;
                z10 = z4;
                z7 = true;
            } else if (!z12) {
                throw NotFoundException.a();
            } else {
                z9 = true;
                z10 = z4;
                z7 = z3;
                z8 = z2;
            }
        } else if (i3 == -1) {
            if (z11) {
                if (z12) {
                    throw NotFoundException.a();
                }
                boolean z13 = z4;
                z7 = z3;
                z8 = true;
                z10 = z13;
            } else if (!z12) {
                throw NotFoundException.a();
            } else {
                z7 = z3;
                z8 = z2;
            }
        } else if (i3 != 0) {
            throw NotFoundException.a();
        } else if (z11) {
            if (!z12) {
                throw NotFoundException.a();
            } else if (a2 < a3) {
                z9 = true;
                boolean z14 = z4;
                z7 = z3;
                z8 = true;
                z10 = z14;
            } else {
                z7 = true;
                z8 = z2;
            }
        } else if (z12) {
            throw NotFoundException.a();
        } else {
            z10 = z4;
            z7 = z3;
            z8 = z2;
        }
        if (z8) {
            if (z7) {
                throw NotFoundException.a();
            }
            a(this.e, this.c);
        }
        if (z7) {
            b(this.e, this.c);
        }
        if (z10) {
            if (z9) {
                throw NotFoundException.a();
            }
            a(this.f, this.c);
        }
        if (z9) {
            b(this.f, this.d);
        }
    }

    private int[] a(a aVar, int i2, boolean z) {
        int[] iArr = this.f127a;
        iArr[0] = 0;
        iArr[1] = 0;
        iArr[2] = 0;
        iArr[3] = 0;
        int a2 = aVar.a();
        boolean z2 = false;
        int i3 = i2;
        while (i3 < a2) {
            z2 = !aVar.a(i3);
            if (z == z2) {
                break;
            }
            i3++;
        }
        boolean z3 = z2;
        int i4 = i3;
        int i5 = 0;
        for (int i6 = i3; i6 < a2; i6++) {
            if (aVar.a(i6) ^ z3) {
                iArr[i5] = iArr[i5] + 1;
            } else {
                if (i5 != 3) {
                    i5++;
                } else if (b(iArr)) {
                    return new int[]{i4, i6};
                } else {
                    i4 += iArr[0] + iArr[1];
                    iArr[0] = iArr[2];
                    iArr[1] = iArr[3];
                    iArr[2] = 0;
                    iArr[3] = 0;
                    i5--;
                }
                iArr[i5] = 1;
                z3 = !z3;
            }
        }
        throw NotFoundException.a();
    }

    private static boolean b(d dVar, d dVar2) {
        int a2 = dVar.c().a();
        int a3 = dVar2.c().a();
        if (!(a2 == 0 && a3 == 8) && a2 == 8 && a3 == 0) {
        }
        int b = (dVar.b() + (dVar2.b() * 16)) % 79;
        int a4 = (dVar.c().a() * 9) + dVar2.c().a();
        if (a4 > 72) {
            a4--;
        }
        if (a4 > 8) {
            a4--;
        }
        return b == a4;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.b.a.e.a(com.google.zxing.common.a, boolean, int, java.util.Hashtable):com.google.zxing.b.a.d
     arg types: [com.google.zxing.common.a, int, int, java.util.Hashtable]
     candidates:
      com.google.zxing.b.a.e.a(com.google.zxing.common.a, int, boolean, int[]):com.google.zxing.b.a.c
      com.google.zxing.b.a.e.a(com.google.zxing.common.a, boolean, int, java.util.Hashtable):com.google.zxing.b.a.d */
    public h a(int i2, a aVar, Hashtable hashtable) {
        a(this.n, a(aVar, false, i2, hashtable));
        aVar.c();
        a(this.o, a(aVar, true, i2, hashtable));
        aVar.c();
        int size = this.n.size();
        int size2 = this.o.size();
        for (int i3 = 0; i3 < size; i3++) {
            d dVar = (d) this.n.elementAt(i3);
            if (dVar.d() > 1) {
                for (int i4 = 0; i4 < size2; i4++) {
                    d dVar2 = (d) this.o.elementAt(i4);
                    if (dVar2.d() > 1 && b(dVar, dVar2)) {
                        return a(dVar, dVar2);
                    }
                }
                continue;
            }
        }
        throw NotFoundException.a();
    }

    public void a() {
        this.n.setSize(0);
        this.o.setSize(0);
    }
}
