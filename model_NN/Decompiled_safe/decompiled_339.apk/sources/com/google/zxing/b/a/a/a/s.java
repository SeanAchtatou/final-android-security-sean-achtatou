package com.google.zxing.b.a.a.a;

import com.google.zxing.common.a;

final class s {

    /* renamed from: a  reason: collision with root package name */
    private final a f133a;
    private final m b = new m();
    private final StringBuffer c = new StringBuffer();

    s(a aVar) {
        this.f133a = aVar;
    }

    static int a(a aVar, int i, int i2) {
        int i3 = 0;
        if (i2 > 32) {
            throw new IllegalArgumentException("extractNumberValueFromBitArray can't handle more than 32 bits");
        }
        for (int i4 = 0; i4 < i2; i4++) {
            if (aVar.a(i + i4)) {
                i3 |= 1 << ((i2 - i4) - 1);
            }
        }
        return i3;
    }

    private o a() {
        l b2;
        boolean b3;
        do {
            int i = this.b.f130a;
            if (this.b.a()) {
                b2 = d();
                b3 = b2.b();
            } else if (this.b.b()) {
                b2 = c();
                b3 = b2.b();
            } else {
                b2 = b();
                b3 = b2.b();
            }
            if (!(i != this.b.f130a) && !b3) {
                break;
            }
        } while (!b3);
        return b2.a();
    }

    private boolean a(int i) {
        if (i + 7 > this.f133a.b) {
            return i + 4 <= this.f133a.b;
        }
        for (int i2 = i; i2 < i + 3; i2++) {
            if (this.f133a.a(i2)) {
                return true;
            }
        }
        return this.f133a.a(i + 3);
    }

    private l b() {
        while (a(this.b.f130a)) {
            p b2 = b(this.b.f130a);
            this.b.f130a = b2.e();
            if (b2.c()) {
                return new l(b2.d() ? new o(this.b.f130a, this.c.toString()) : new o(this.b.f130a, this.c.toString(), b2.b()), true);
            }
            this.c.append(b2.a());
            if (b2.d()) {
                return new l(new o(this.b.f130a, this.c.toString()), true);
            }
            this.c.append(b2.b());
        }
        if (i(this.b.f130a)) {
            this.b.d();
            this.b.f130a += 4;
        }
        return new l(false);
    }

    private p b(int i) {
        if (i + 7 > this.f133a.b) {
            int a2 = a(i, 4);
            return a2 == 0 ? new p(this.f133a.b, 10, 10) : new p(this.f133a.b, a2 - 1, 10);
        }
        int a3 = a(i, 7);
        return new p(i + 7, (a3 - 8) / 11, (a3 - 8) % 11);
    }

    private l c() {
        while (c(this.b.f130a)) {
            n d = d(this.b.f130a);
            this.b.f130a = d.e();
            if (d.b()) {
                return new l(new o(this.b.f130a, this.c.toString()), true);
            }
            this.c.append(d.a());
        }
        if (h(this.b.f130a)) {
            this.b.f130a += 3;
            this.b.c();
        } else if (g(this.b.f130a)) {
            if (this.b.f130a + 5 < this.f133a.b) {
                this.b.f130a += 5;
            } else {
                this.b.f130a = this.f133a.b;
            }
            this.b.d();
        }
        return new l(false);
    }

    private boolean c(int i) {
        boolean z = true;
        if (i + 5 > this.f133a.b) {
            return false;
        }
        int a2 = a(i, 5);
        if (a2 >= 5 && a2 < 16) {
            return true;
        }
        if (i + 7 > this.f133a.b) {
            return false;
        }
        int a3 = a(i, 7);
        if (a3 >= 64 && a3 < 116) {
            return true;
        }
        if (i + 8 > this.f133a.b) {
            return false;
        }
        int a4 = a(i, 8);
        if (a4 < 232 || a4 >= 253) {
            z = false;
        }
        return z;
    }

    private l d() {
        while (e(this.b.f130a)) {
            n f = f(this.b.f130a);
            this.b.f130a = f.e();
            if (f.b()) {
                return new l(new o(this.b.f130a, this.c.toString()), true);
            }
            this.c.append(f.a());
        }
        if (h(this.b.f130a)) {
            this.b.f130a += 3;
            this.b.c();
        } else if (g(this.b.f130a)) {
            if (this.b.f130a + 5 < this.f133a.b) {
                this.b.f130a += 5;
            } else {
                this.b.f130a = this.f133a.b;
            }
            this.b.e();
        }
        return new l(false);
    }

    private n d(int i) {
        int a2 = a(i, 5);
        if (a2 == 15) {
            return new n(i + 5, '$');
        }
        if (a2 >= 5 && a2 < 15) {
            return new n(i + 5, (char) ((a2 + 48) - 5));
        }
        int a3 = a(i, 7);
        if (a3 >= 64 && a3 < 90) {
            return new n(i + 7, (char) (a3 + 1));
        }
        if (a3 >= 90 && a3 < 116) {
            return new n(i + 7, (char) (a3 + 7));
        }
        int a4 = a(i, 8);
        switch (a4) {
            case 232:
                return new n(i + 8, '!');
            case 233:
                return new n(i + 8, '\"');
            case 234:
                return new n(i + 8, '%');
            case 235:
                return new n(i + 8, '&');
            case 236:
                return new n(i + 8, '\'');
            case 237:
                return new n(i + 8, '(');
            case 238:
                return new n(i + 8, ')');
            case 239:
                return new n(i + 8, '*');
            case 240:
                return new n(i + 8, '+');
            case 241:
                return new n(i + 8, ',');
            case 242:
                return new n(i + 8, '-');
            case 243:
                return new n(i + 8, '.');
            case 244:
                return new n(i + 8, '/');
            case 245:
                return new n(i + 8, ':');
            case 246:
                return new n(i + 8, ';');
            case 247:
                return new n(i + 8, '<');
            case 248:
                return new n(i + 8, '=');
            case 249:
                return new n(i + 8, '>');
            case 250:
                return new n(i + 8, '?');
            case 251:
                return new n(i + 8, '_');
            case 252:
                return new n(i + 8, ' ');
            default:
                throw new RuntimeException(new StringBuffer().append("Decoding invalid ISO/IEC 646 value: ").append(a4).toString());
        }
    }

    private boolean e(int i) {
        boolean z = true;
        if (i + 5 > this.f133a.b) {
            return false;
        }
        int a2 = a(i, 5);
        if (a2 >= 5 && a2 < 16) {
            return true;
        }
        if (i + 6 > this.f133a.b) {
            return false;
        }
        int a3 = a(i, 6);
        if (a3 < 16 || a3 >= 63) {
            z = false;
        }
        return z;
    }

    private n f(int i) {
        int a2 = a(i, 5);
        if (a2 == 15) {
            return new n(i + 5, '$');
        }
        if (a2 >= 5 && a2 < 15) {
            return new n(i + 5, (char) ((a2 + 48) - 5));
        }
        int a3 = a(i, 6);
        if (a3 >= 32 && a3 < 58) {
            return new n(i + 6, (char) (a3 + 33));
        }
        switch (a3) {
            case 58:
                return new n(i + 6, '*');
            case 59:
                return new n(i + 6, ',');
            case 60:
                return new n(i + 6, '-');
            case 61:
                return new n(i + 6, '.');
            case 62:
                return new n(i + 6, '/');
            default:
                throw new RuntimeException(new StringBuffer().append("Decoding invalid alphanumeric value: ").append(a3).toString());
        }
    }

    private boolean g(int i) {
        if (i + 1 > this.f133a.b) {
            return false;
        }
        int i2 = 0;
        while (i2 < 5 && i2 + i < this.f133a.b) {
            if (i2 == 2) {
                if (!this.f133a.a(i + 2)) {
                    return false;
                }
            } else if (this.f133a.a(i + i2)) {
                return false;
            }
            i2++;
        }
        return true;
    }

    private boolean h(int i) {
        if (i + 3 > this.f133a.b) {
            return false;
        }
        for (int i2 = i; i2 < i + 3; i2++) {
            if (this.f133a.a(i2)) {
                return false;
            }
        }
        return true;
    }

    private boolean i(int i) {
        if (i + 1 > this.f133a.b) {
            return false;
        }
        int i2 = 0;
        while (i2 < 4 && i2 + i < this.f133a.b) {
            if (this.f133a.a(i + i2)) {
                return false;
            }
            i2++;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public int a(int i, int i2) {
        return a(this.f133a, i, i2);
    }

    /* access modifiers changed from: package-private */
    public o a(int i, String str) {
        this.c.setLength(0);
        if (str != null) {
            this.c.append(str);
        }
        this.b.f130a = i;
        o a2 = a();
        return (a2 == null || !a2.b()) ? new o(this.b.f130a, this.c.toString()) : new o(this.b.f130a, this.c.toString(), a2.c());
    }

    /* access modifiers changed from: package-private */
    public String a(StringBuffer stringBuffer, int i) {
        String str = null;
        while (true) {
            o a2 = a(i, str);
            stringBuffer.append(r.a(a2.a()));
            str = a2.b() ? String.valueOf(a2.c()) : null;
            if (i == a2.e()) {
                return stringBuffer.toString();
            }
            i = a2.e();
        }
    }
}
