package com.google.zxing.b.a.a.a;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;

final class d extends h {
    d(a aVar) {
        super(aVar);
    }

    public String a() {
        if (this.f128a.b < 48) {
            throw NotFoundException.a();
        }
        StringBuffer stringBuffer = new StringBuffer();
        b(stringBuffer, 8);
        int a2 = this.b.a(48, 2);
        stringBuffer.append("(393");
        stringBuffer.append(a2);
        stringBuffer.append(')');
        int a3 = this.b.a(50, 10);
        if (a3 / 100 == 0) {
            stringBuffer.append('0');
        }
        if (a3 / 10 == 0) {
            stringBuffer.append('0');
        }
        stringBuffer.append(a3);
        stringBuffer.append(this.b.a(60, (String) null).a());
        return stringBuffer.toString();
    }
}
