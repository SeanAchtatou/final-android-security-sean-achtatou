package defpackage;

import android.util.Log;
import android.view.ViewGroup;
import com.ideaworks3d.marmalade.LoaderAPI;
import com.ideaworks3d.marmalade.LoaderActivity;
import com.playhaven.src.publishersdk.metadata.PHNotificationView;

/* renamed from: s3ePlayhaven  reason: default package */
class s3ePlayhaven {
    private static final String TAG = "s3ePlayhaven";
    private s3ePlayhavenFacade PHFacade;
    private int currentHash;

    s3ePlayhaven() {
    }

    public int s3ePlayhavenInitWithKeys(String str, String str2) {
        LoaderActivity activity = LoaderAPI.getActivity();
        Log.d(TAG, "Setting PHConfig token and secret");
        this.PHFacade = new s3ePlayhavenFacade(activity, str, str2);
        return 0;
    }

    public void s3ePHRegisterCallback() {
    }

    public void s3ePHSendAppOpen(String str) {
        Log.d(TAG, "openRequest");
        this.currentHash = str == null ? 0 : str.hashCode();
        this.PHFacade.openRequest(this.currentHash);
    }

    public void s3ePHSendContentRequest(String str, boolean z) {
        Log.d(TAG, "contentRequest");
        this.PHFacade.contentRequest(this.currentHash, str);
    }

    public void s3ePHPreloadContentRequest(String str) {
        Log.d(TAG, "preloadRequest");
        this.PHFacade.preloadRequest(this.currentHash, str);
    }

    public void s3ePHCancelAllContentRequests() {
    }

    public void s3ePHReportResolution(int i) {
        Log.d(TAG, "reportResolution");
        this.PHFacade.reportResolution(i);
    }

    public void s3ePHSendPublisherIAPTrackingRequest(String str, int i, int i2) {
        Log.d(TAG, "iapTrackingRequest");
        this.PHFacade.iapTrackingRequest(str, i, i2);
    }

    public int s3ePHShowNotificationView(int i, int i2, boolean z, boolean z2) {
        LoaderActivity activity = LoaderAPI.getActivity();
        PHNotificationView pHNotificationView = new PHNotificationView(activity, "more_games");
        ((ViewGroup) activity.getWindow().getDecorView()).addView(pHNotificationView);
        pHNotificationView.refresh();
        return 0;
    }

    public void s3ePHClearNotificationView() {
    }

    public void s3ePHRefreshNotificationView(boolean z) {
    }

    public void s3ePHSetOptOutStatus(boolean z) {
    }
}
