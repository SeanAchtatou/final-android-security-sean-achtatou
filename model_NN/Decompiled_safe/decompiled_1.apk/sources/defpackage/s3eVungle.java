package defpackage;

import com.ideaworks3d.marmalade.LoaderActivity;
import com.vungle.sdk.VunglePub;

/* renamed from: s3eVungle  reason: default package */
class s3eVungle {
    public native void EndCallback();

    public native void StartCallback();

    public native void ViewCallback(double d, double d2);

    s3eVungle() {
    }

    public void onPause() {
        VunglePub.onPause();
    }

    public void onResume() {
        VunglePub.onResume();
    }

    public void s3eVungleStart(String str) {
        VunglePub.init(LoaderActivity.m_Activity, str);
        VunglePub.setEventListener(new VunglePubListener());
    }

    public String s3eVungleGetVersionString() {
        return VunglePub.getVersionString();
    }

    public boolean s3eVungleIsAdAvailable() {
        return VunglePub.isVideoAvailable();
    }

    public void s3eVunglePlayModalAd(boolean z, boolean z2) {
        VunglePub.displayAdvert();
    }

    public void s3eVunglePlayIncentivizedAd(boolean z, boolean z2, String str) {
        VunglePub.displayIncentivizedAdvert(str, z2);
    }

    public void s3eVungleSetSoundEnabled(boolean z) {
        VunglePub.setSoundEnabled(z);
    }

    public void s3eVungleSetAllowAutoRotate(boolean z) {
        VunglePub.setAutoRotation(z);
    }

    /* renamed from: s3eVungle$VunglePubListener */
    private final class VunglePubListener implements VunglePub.EventListener {
        private VunglePubListener() {
        }

        public void onVungleAdStart() {
            s3eVungle.this.StartCallback();
        }

        public void onVungleAdEnd() {
            s3eVungle.this.EndCallback();
        }

        public void onVungleView(double d, double d2) {
            s3eVungle.this.ViewCallback(d, d2);
        }
    }
}
