package defpackage;

import android.app.Activity;
import android.graphics.Bitmap;
import android.util.Log;
import com.playhaven.src.common.PHAPIRequest;
import com.playhaven.src.common.PHConfig;
import com.playhaven.src.common.PHSession;
import com.playhaven.src.publishersdk.content.PHContentView;
import com.playhaven.src.publishersdk.content.PHPublisherContentRequest;
import com.playhaven.src.publishersdk.content.PHPurchase;
import com.playhaven.src.publishersdk.content.PHReward;
import com.playhaven.src.publishersdk.metadata.PHPublisherMetadataRequest;
import com.playhaven.src.publishersdk.open.PHPublisherOpenRequest;
import com.playhaven.src.publishersdk.purchases.PHPublisherIAPTrackingRequest;
import org.json.JSONObject;
import v2.com.playhaven.model.PHContent;

/* renamed from: s3ePlayhavenFacade  reason: default package */
public class s3ePlayhavenFacade implements PHPublisherContentRequest.ContentDelegate, PHPublisherContentRequest.PurchaseDelegate, PHPublisherContentRequest.RewardDelegate, PHPublisherContentRequest.CustomizeDelegate, PHPublisherContentRequest.FailureDelegate {
    private Activity currentActivity;
    private PHPurchase currentPurchase;

    /* renamed from: s3ePlayhavenFacade$PublisherContent */
    public static class PublisherContent {
        public String closeButtonURLPath;
        public String dismissReason;
        public String placement;
        public int transition;
        public String url;
    }

    /* renamed from: s3ePlayhavenFacade$Purchase */
    public static class Purchase {
        public String productIdentifier;
        public int quantity;
        public String receipt;
    }

    /* renamed from: s3ePlayhavenFacade$Reward */
    public static class Reward {
        public String name;
        public int quantity;
        public String receipt;
    }

    public static native void native_contentDidDisplay(PublisherContent publisherContent);

    public static native void native_contentDismissing(PublisherContent publisherContent);

    public static native void native_contentFailed(String str);

    public static native void native_contentMakePurchase(Purchase purchase);

    public static native void native_contentReceived(String str);

    public static native void native_contentStart(String str);

    public static native void native_contentUnlockReward(Reward reward);

    public static native void native_contentWillDisplay(PublisherContent publisherContent);

    /* renamed from: s3ePlayhavenFacade$RequestRunner */
    private class RequestRunner implements Runnable {
        private PHAPIRequest request;

        private RequestRunner() {
        }

        public void run(Activity activity, PHAPIRequest pHAPIRequest) {
            this.request = pHAPIRequest;
            activity.runOnUiThread(this);
        }

        public void run() {
            this.request.send();
        }
    }

    /* renamed from: s3ePlayhavenFacade$PreloadRequestRunner */
    private class PreloadRequestRunner implements Runnable {
        private PHPublisherContentRequest contentRequest;

        private PreloadRequestRunner() {
        }

        public void run(Activity activity, PHPublisherContentRequest pHPublisherContentRequest) {
            this.contentRequest = pHPublisherContentRequest;
            activity.runOnUiThread(this);
        }

        public void run() {
            this.contentRequest.preload();
        }
    }

    public s3ePlayhavenFacade(Activity activity, String str, String str2) {
        setCurrentActivity(activity);
        setKeys(str, str2);
    }

    public void setCurrentActivity(Activity activity) {
        this.currentActivity = activity;
        PHConfig.cacheDeviceInfo(activity);
    }

    public void setKeys(String str, String str2) {
        Log.d("s3ePlayhavenFacade", "setKeys");
        PHConfig.token = str;
        PHConfig.secret = str2;
    }

    public void register() {
        if (this.currentActivity != null) {
            PHSession.register(this.currentActivity);
        }
    }

    public void unregister() {
        if (this.currentActivity != null) {
            PHSession.unregister(this.currentActivity);
        }
    }

    public void openRequest(int i) {
        Log.d("s3ePlayhavenFacade", "openRequest");
        PHPublisherOpenRequest pHPublisherOpenRequest = new PHPublisherOpenRequest(this.currentActivity, this);
        pHPublisherOpenRequest.setRequestTag(i);
        new RequestRunner().run(this.currentActivity, pHPublisherOpenRequest);
    }

    public void metaDataRequest(int i, String str) {
        Log.d("s3ePlayhavenFacade", "metaDataRequest");
        PHPublisherMetadataRequest pHPublisherMetadataRequest = new PHPublisherMetadataRequest(this.currentActivity, this, str);
        pHPublisherMetadataRequest.setRequestTag(i);
        new RequestRunner().run(this.currentActivity, pHPublisherMetadataRequest);
    }

    public void reportResolution(int i) {
        Log.d("s3ePlayhavenFacade", "reportResolution");
        if (this.currentPurchase != null) {
            this.currentPurchase.reportResolution(PHPurchase.Resolution.values()[i], this.currentActivity);
        }
        this.currentPurchase = null;
    }

    public void iapTrackingRequest(String str, int i, int i2) {
        Log.d("s3ePlayhavenFacade", "iapTrackingRequest");
        PHPublisherIAPTrackingRequest pHPublisherIAPTrackingRequest = new PHPublisherIAPTrackingRequest(this.currentActivity, this);
        pHPublisherIAPTrackingRequest.product = str;
        pHPublisherIAPTrackingRequest.quantity = i;
        pHPublisherIAPTrackingRequest.resolution = PHPurchase.Resolution.values()[i2];
        new RequestRunner().run(this.currentActivity, pHPublisherIAPTrackingRequest);
    }

    public void contentRequest(int i, String str) {
        Log.d("s3ePlayhavenFacade", "contentRequest");
        PHPublisherContentRequest pHPublisherContentRequest = new PHPublisherContentRequest(this.currentActivity, this, str);
        pHPublisherContentRequest.setRequestTag(i);
        new RequestRunner().run(this.currentActivity, pHPublisherContentRequest);
    }

    public void preloadRequest(int i, String str) {
        Log.d("s3ePlayhavenFacade", "preloadRequest");
        PHPublisherContentRequest pHPublisherContentRequest = new PHPublisherContentRequest(this.currentActivity, this, str);
        pHPublisherContentRequest.setRequestTag(i);
        new PreloadRequestRunner().run(this.currentActivity, pHPublisherContentRequest);
    }

    public void requestSucceeded(PHAPIRequest pHAPIRequest, JSONObject jSONObject) {
        Log.d("s3ePlayhavenFacade", "requestSucceeded");
    }

    public void requestFailed(PHAPIRequest pHAPIRequest, Exception exc) {
        Log.d("s3ePlayhavenFacade", "requestFailed");
        native_contentFailed(exc.getMessage());
    }

    public void didFail(PHPublisherContentRequest pHPublisherContentRequest, String str) {
        Log.d("s3ePlayhavenFacade", "requestFailed");
        native_contentFailed(str);
    }

    public void willGetContent(PHPublisherContentRequest pHPublisherContentRequest) {
    }

    private int TransitionToInt(PHContent.TransitionType transitionType) {
        switch (transitionType) {
            case Modal:
                return 1;
            case Dialog:
                return 2;
            default:
                return 0;
        }
    }

    public void willDisplayContent(PHPublisherContentRequest pHPublisherContentRequest, com.playhaven.src.publishersdk.content.PHContent pHContent) {
        Log.d("s3ePlayhavenFacade", "willDisplayContent");
        PublisherContent publisherContent = new PublisherContent();
        publisherContent.placement = "";
        publisherContent.url = pHContent.url.toString();
        publisherContent.transition = TransitionToInt(pHContent.transition);
        publisherContent.closeButtonURLPath = pHContent.closeURL;
        publisherContent.dismissReason = "";
        native_contentWillDisplay(publisherContent);
    }

    public void didDisplayContent(PHPublisherContentRequest pHPublisherContentRequest, com.playhaven.src.publishersdk.content.PHContent pHContent) {
        Log.d("s3ePlayhavenFacade", "didDisplayContent");
        PublisherContent publisherContent = new PublisherContent();
        publisherContent.placement = "";
        publisherContent.url = pHContent.url.toString();
        publisherContent.transition = TransitionToInt(pHContent.transition);
        publisherContent.closeButtonURLPath = pHContent.closeURL;
        publisherContent.dismissReason = "";
        native_contentDidDisplay(publisherContent);
    }

    public void didDismissContent(PHPublisherContentRequest pHPublisherContentRequest, PHPublisherContentRequest.PHDismissType pHDismissType) {
        Log.d("s3ePlayhavenFacade", "didDismissContent");
        PublisherContent publisherContent = new PublisherContent();
        publisherContent.placement = "";
        publisherContent.url = "";
        publisherContent.transition = 0;
        publisherContent.closeButtonURLPath = "";
        publisherContent.dismissReason = pHDismissType.toString();
        native_contentDismissing(publisherContent);
    }

    public void didFail(PHPublisherContentRequest pHPublisherContentRequest, Exception exc) {
        requestFailed(pHPublisherContentRequest, exc);
    }

    public void contentDidFail(PHPublisherContentRequest pHPublisherContentRequest, Exception exc) {
        requestFailed(pHPublisherContentRequest, exc);
    }

    public Bitmap closeButton(PHPublisherContentRequest pHPublisherContentRequest, PHContentView.ButtonState buttonState) {
        return null;
    }

    public int borderColor(PHPublisherContentRequest pHPublisherContentRequest, com.playhaven.src.publishersdk.content.PHContent pHContent) {
        return -1;
    }

    public void unlockedReward(PHPublisherContentRequest pHPublisherContentRequest, PHReward pHReward) {
        Log.d("s3ePlayhavenFacade", "unlockedReward");
        Reward reward = new Reward();
        reward.name = pHReward.name;
        reward.quantity = pHReward.quantity;
        reward.receipt = pHReward.receipt;
        native_contentUnlockReward(reward);
    }

    public void shouldMakePurchase(PHPublisherContentRequest pHPublisherContentRequest, PHPurchase pHPurchase) {
        Log.d("s3ePlayhavenFacade", "shouldMakePurchase");
        Purchase purchase = new Purchase();
        purchase.productIdentifier = pHPurchase.product;
        purchase.quantity = pHPurchase.quantity;
        purchase.receipt = pHPurchase.receipt;
        native_contentMakePurchase(purchase);
    }
}
