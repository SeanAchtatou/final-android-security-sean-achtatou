package defpackage;

import android.util.Log;
import com.flurry.android.FlurryAgent;
import com.ideaworks3d.marmalade.LoaderActivity;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/* renamed from: Flurry  reason: default package */
class Flurry {
    private static final String TAG = "Flurry";

    Flurry() {
    }

    private Map<String, String> ParamsToMap(String str) {
        HashMap hashMap = new HashMap();
        StringTokenizer stringTokenizer = new StringTokenizer(str, "|");
        int countTokens = stringTokenizer.countTokens() / 2;
        for (int i = 0; i < countTokens; i++) {
            hashMap.put(stringTokenizer.nextToken(), stringTokenizer.nextToken());
        }
        return hashMap;
    }

    public void s3eFlurryStartSession(String str) {
        Log.d(TAG, "s3eFlurryStartSession");
        Log.d(TAG, str);
        FlurryAgent.onStartSession(LoaderActivity.m_Activity, str);
    }

    public void s3eFlurryEndSession() {
        Log.d(TAG, "s3eFlurryEndSession");
        FlurryAgent.onEndSession(LoaderActivity.m_Activity);
    }

    public void s3eFlurrySetUserID(String str) {
        Log.d(TAG, "SUId " + str);
        FlurryAgent.setUserId(str);
    }

    public void s3eFlurrySetAge(int i) {
        FlurryAgent.setAge(i);
    }

    public void s3eFlurryCountPageView() {
        FlurryAgent.onPageView();
    }

    public void s3eFlurryLogEvent(String str) {
        Log.d(TAG, "LE " + str);
        FlurryAgent.logEvent(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.logEvent(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.flurry.android.FlurryAgent.logEvent(java.lang.String, java.util.Map<java.lang.String, java.lang.String>):void
      com.flurry.android.FlurryAgent.logEvent(java.lang.String, boolean):void */
    public void s3eFlurryLogEventTimed(String str) {
        Log.d(TAG, "LET " + str);
        FlurryAgent.logEvent(str, true);
    }

    public void s3eFlurryLogError(String str, String str2) {
        Log.w(TAG, "LEr " + str + " " + str2);
        FlurryAgent.onError(str, str2, "N/A");
    }

    public void s3eFlurryLogEventParams(String str, String str2) {
        Log.d(TAG, "LEP " + str + " - " + str2);
        FlurryAgent.logEvent(str, ParamsToMap(str2));
    }

    public void s3eFlurryLogEventParamsTimed(String str, String str2) {
        Log.d(TAG, "LEPT " + str + " - " + str2);
        FlurryAgent.logEvent(str, ParamsToMap(str2), true);
    }

    public void s3eFlurryEndTimedEvent(String str, String str2) {
        Log.d(TAG, "ETE " + str + " - " + str2);
        FlurryAgent.endTimedEvent(str);
    }

    public void s3eFlurrySetSessionReportsOnCloseEnabled(boolean z) {
    }

    public void s3eFlurrySetSessionReportsOnPauseEnabled(boolean z) {
    }
}
