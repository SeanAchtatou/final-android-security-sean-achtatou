package v2.com.playhaven.listeners;

import v2.com.playhaven.model.PHError;
import v2.com.playhaven.requests.purchases.PHIAPTrackingRequest;

public interface PHIAPRequestListener {
    void onIAPRequestFailed(PHIAPTrackingRequest pHIAPTrackingRequest, PHError pHError);

    void onIAPRequestSucceeded(PHIAPTrackingRequest pHIAPTrackingRequest);
}
