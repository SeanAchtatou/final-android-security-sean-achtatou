package v2.com.playhaven.listeners;

import java.nio.ByteBuffer;
import v2.com.playhaven.model.PHError;

public interface PHHttpRequestListener {
    void onHttpRequestFailed(PHError pHError);

    void onHttpRequestSucceeded(ByteBuffer byteBuffer, int i);
}
