package v2.com.playhaven.listeners;

import v2.com.playhaven.model.PHContent;
import v2.com.playhaven.model.PHError;
import v2.com.playhaven.requests.content.PHContentRequest;

public interface PHContentRequestListener {
    void onDismissedContent(PHContentRequest pHContentRequest, PHContentRequest.PHDismissType pHDismissType);

    void onDisplayedContent(PHContentRequest pHContentRequest, PHContent pHContent);

    void onFailedToDisplayContent(PHContentRequest pHContentRequest, PHError pHError);

    void onNoContent(PHContentRequest pHContentRequest);

    void onReceivedContent(PHContentRequest pHContentRequest, PHContent pHContent);

    void onSentContentRequest(PHContentRequest pHContentRequest);

    void onWillDisplayContent(PHContentRequest pHContentRequest, PHContent pHContent);
}
