package v2.com.playhaven.listeners;

import v2.com.playhaven.model.PHError;
import v2.com.playhaven.requests.open.PHOpenRequest;

public interface PHOpenRequestListener {
    void onOpenFailed(PHOpenRequest pHOpenRequest, PHError pHError);

    void onOpenSuccessful(PHOpenRequest pHOpenRequest);
}
