package v2.com.playhaven.listeners;

import org.json.JSONObject;
import v2.com.playhaven.model.PHError;
import v2.com.playhaven.requests.content.PHSubContentRequest;

public interface PHSubContentRequestListener {
    void onSubContentRequestFailed(PHSubContentRequest pHSubContentRequest, PHError pHError);

    void onSubContentRequestSucceeded(PHSubContentRequest pHSubContentRequest, JSONObject jSONObject);
}
