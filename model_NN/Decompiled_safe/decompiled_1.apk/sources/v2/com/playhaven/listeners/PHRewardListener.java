package v2.com.playhaven.listeners;

import v2.com.playhaven.model.PHReward;
import v2.com.playhaven.requests.content.PHContentRequest;

public interface PHRewardListener {
    void onUnlockedReward(PHContentRequest pHContentRequest, PHReward pHReward);
}
