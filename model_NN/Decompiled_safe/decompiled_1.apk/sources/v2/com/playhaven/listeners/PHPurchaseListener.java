package v2.com.playhaven.listeners;

import v2.com.playhaven.model.PHPurchase;
import v2.com.playhaven.requests.content.PHContentRequest;

public interface PHPurchaseListener {
    void onMadePurchase(PHContentRequest pHContentRequest, PHPurchase pHPurchase);
}
