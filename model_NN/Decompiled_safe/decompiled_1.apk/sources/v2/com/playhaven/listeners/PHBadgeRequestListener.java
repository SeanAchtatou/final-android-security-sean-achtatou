package v2.com.playhaven.listeners;

import org.json.JSONObject;
import v2.com.playhaven.model.PHError;
import v2.com.playhaven.requests.badge.PHBadgeRequest;

public interface PHBadgeRequestListener {
    void onBadgeRequestFailed(PHBadgeRequest pHBadgeRequest, PHError pHError);

    void onBadgeRequestSucceeded(PHBadgeRequest pHBadgeRequest, JSONObject jSONObject);
}
