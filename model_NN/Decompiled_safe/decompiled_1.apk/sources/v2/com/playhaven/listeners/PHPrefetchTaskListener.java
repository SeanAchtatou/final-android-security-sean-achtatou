package v2.com.playhaven.listeners;

public interface PHPrefetchTaskListener {
    void onPrefetchDone(int i);
}
