package v2.com.playhaven.listeners;

import v2.com.playhaven.requests.open.PHOpenRequest;

public interface PHPrefetchListener {
    void onPrefetchFinished(PHOpenRequest pHOpenRequest);
}
