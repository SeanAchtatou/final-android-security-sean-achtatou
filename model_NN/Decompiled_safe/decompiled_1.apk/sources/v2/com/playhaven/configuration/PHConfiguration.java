package v2.com.playhaven.configuration;

import android.content.Context;
import android.content.SharedPreferences;
import v2.com.playhaven.utils.PHStringUtil;

public class PHConfiguration {
    private static final int SHARED_PREF_MODE = 0;
    private static final String SHARED_PREF_NAME = PHConfiguration.class.getName();

    private enum Config {
        SdkVersion,
        Token,
        Secret,
        Username,
        Password,
        ShouldPrecache,
        APIUrl,
        RunningUiTests
    }

    public enum ConnectionType {
        NO_NETWORK,
        MOBILE,
        WIFI,
        NO_PERMISSION
    }

    private boolean validateCredential(String value) throws RuntimeException {
        if (value == null || value.length() == 0) {
            return false;
        }
        return true;
    }

    public void setCredentials(Context context, String token, String secret) {
        if (validateCredential(token)) {
            setString(context, Config.Token.toString(), token);
        }
        if (validateCredential(secret)) {
            setString(context, Config.Secret.toString(), secret);
        }
        setIsRunningUITests(context, false);
        setShouldPrecache(context, true);
        PHStringUtil.log("PlayHaven initialized: " + getPlayhavenBuildInfo());
    }

    public String getToken(Context context) {
        return getString(context, Config.Token.toString());
    }

    public String getSecret(Context context) {
        return getString(context, Config.Secret.toString());
    }

    public String getStagingUsername(Context context) {
        return getString(context, Config.Username.toString());
    }

    public String getStagingPassword(Context context) {
        return getString(context, Config.Password.toString());
    }

    public void setStagingUsername(Context context, String username) {
        setString(context, Config.Username.toString(), username);
    }

    public int getPrecacheSize() {
        return 8388608;
    }

    public void setStagingPassword(Context context, String password) {
        setString(context, Config.Password.toString(), password);
    }

    public String getSDKVersion() {
        return Version.PROJECT_VERSION;
    }

    public String getCleanSDKVersion() {
        return getSDKVersion().replace("-SNAPSHOT", "");
    }

    public String getPlayhavenBuildInfo() {
        return Version.BANNER;
    }

    public void setAPIUrl(Context context, String api) {
        setString(context, Config.APIUrl.toString(), api);
    }

    public String getAPIUrl(Context context) {
        String api_url = getString(context, Config.APIUrl.toString());
        if (api_url == null) {
            return "http://api2.playhaven.com";
        }
        return api_url;
    }

    public int getJSBridgeProtocolVersion() {
        return 4;
    }

    public boolean getShouldPrecache(Context context) {
        return Boolean.valueOf(getString(context, Config.ShouldPrecache.toString())).booleanValue();
    }

    public void setShouldPrecache(Context context, boolean shouldPrecache) {
        setString(context, Config.ShouldPrecache.toString(), Boolean.toString(shouldPrecache));
    }

    public boolean getIsRunningUITests(Context context) {
        return Boolean.valueOf(getString(context, Config.RunningUiTests.toString())).booleanValue();
    }

    public void setIsRunningUITests(Context context, boolean isRunning) {
        setString(context, Config.RunningUiTests.toString(), Boolean.toString(isRunning));
    }

    private void setString(Context context, String key, String value) {
        SharedPreferences.Editor editor = context.getApplicationContext().getSharedPreferences(SHARED_PREF_NAME, 0).edit();
        editor.putString(key, value);
        editor.commit();
    }

    private String getString(Context context, String key) {
        return context.getApplicationContext().getSharedPreferences(SHARED_PREF_NAME, 0).getString(key, null);
    }
}
