package v2.com.playhaven.resources.types;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.NinePatch;
import android.graphics.drawable.NinePatchDrawable;
import java.util.HashMap;

public class PHNinePatchResource extends PHImageResource {
    private HashMap<Integer, NinePatch> nine_patch_cache = new HashMap<>();

    public NinePatch loadNinePatch(int densityType) {
        this.densityType = densityType;
        return loadNinePatch();
    }

    private NinePatch loadNinePatch() throws ArrayIndexOutOfBoundsException {
        NinePatch cachedNinePatch = this.nine_patch_cache.get(Integer.valueOf(this.densityType));
        if (cachedNinePatch == null) {
            Bitmap image = super.loadImage(this.densityType);
            byte[] chunk = image.getNinePatchChunk();
            if (!NinePatch.isNinePatchChunk(chunk)) {
                return null;
            }
            cachedNinePatch = new NinePatch(image, chunk, null);
            this.nine_patch_cache.put(Integer.valueOf(this.densityType), cachedNinePatch);
        }
        return cachedNinePatch;
    }

    public NinePatchDrawable loadNinePatchDrawable(Resources res, int densityType) throws ArrayIndexOutOfBoundsException {
        return new NinePatchDrawable(res, loadNinePatch(densityType));
    }
}
