package v2.com.playhaven.resources.types;

import android.util.Base64;
import java.io.UnsupportedEncodingException;

public class PHResource {
    private String data;
    private String key;

    public void setDataByte(byte[] data2) throws UnsupportedEncodingException {
        this.data = new String(data2, "UTF-8");
    }

    public void setDataStr(String data2) {
        this.data = data2;
    }

    public byte[] getData() {
        return Base64.decode(this.data, 1);
    }

    public String getResourceKey() {
        return this.key;
    }

    public void setResourceKey(String key2) {
        this.key = key2;
    }
}
