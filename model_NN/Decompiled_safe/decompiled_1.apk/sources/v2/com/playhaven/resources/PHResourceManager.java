package v2.com.playhaven.resources;

import java.util.Hashtable;
import v2.com.playhaven.requests.crashreport.PHCrashReport;
import v2.com.playhaven.resources.data.PHBadgeImageResource;
import v2.com.playhaven.resources.data.PHCloseActiveImageResource;
import v2.com.playhaven.resources.data.PHCloseImageResource;
import v2.com.playhaven.resources.types.PHResource;

public class PHResourceManager {
    private static PHResourceManager res_manager = null;
    private boolean hasLoaded = false;
    private Hashtable<String, PHResource> resources = null;

    private void registerResources() {
        this.resources = new Hashtable<>();
        this.resources.put("close_inactive", new PHCloseImageResource());
        this.resources.put("close_active", new PHCloseActiveImageResource());
        this.resources.put("badge_image", new PHBadgeImageResource());
    }

    private PHResourceManager() {
    }

    public static PHResourceManager sharedResourceManager() {
        try {
            if (res_manager == null) {
                res_manager = new PHResourceManager();
                res_manager.loadResources();
            }
        } catch (Exception e) {
            PHCrashReport.reportCrash(e, "PHResourceManager - sharedResourceManager", PHCrashReport.Urgency.critical);
        }
        return res_manager;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public PHResource getResource(String key) {
        if (this.resources != null) {
            return this.resources.get(key);
        }
        return null;
    }

    public void registerResource(String key, PHResource resource) {
        if (this.resources != null && resource != null) {
            this.resources.put(key, resource);
        }
    }

    private void loadResources() {
        if (this.resources == null && !this.hasLoaded) {
            registerResources();
            this.hasLoaded = true;
        }
    }
}
