package v2.com.playhaven.resources.types;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import java.util.Hashtable;
import v2.com.playhaven.requests.base.PHAsyncRequest;

public class PHImageResource extends PHResource {
    private Hashtable<Integer, Bitmap> cached_images = new Hashtable<>();
    private Hashtable<Integer, String> data_map = new Hashtable<>();
    protected int densityType = 160;

    public byte[] getData(int density) {
        String dataStr = this.data_map.get(new Integer(density));
        if (dataStr == null) {
            return null;
        }
        return Base64.decode(dataStr.getBytes(), 1);
    }

    public void setDataStr(int density, String data) {
        if (data != null) {
            this.data_map.put(new Integer(density), data);
        }
    }

    public void setDataStr(int[] densities, String data) {
        for (int density : densities) {
            setDataStr(density, data);
        }
    }

    public void setDataStr(String data) {
        throw new UnsupportedOperationException("You must use setDataStr(density, data) when setting image data");
    }

    public byte[] getData() {
        throw new UnsupportedOperationException("You must use getData(density) when loading images");
    }

    private Bitmap getClosestImage(int requestedDensity) {
        if (this.data_map.size() == 0) {
            return null;
        }
        int minDiff = PHAsyncRequest.INFINITE_REDIRECTS;
        int closestDensity = 160;
        for (Integer density : this.data_map.keySet()) {
            int diff = Math.abs(density.intValue() - requestedDensity);
            if (diff < minDiff) {
                closestDensity = density.intValue();
                minDiff = diff;
            }
        }
        byte[] buffer = getData(closestDensity);
        if (buffer == null) {
            return null;
        }
        Bitmap closestImage = BitmapFactory.decodeByteArray(buffer, 0, buffer.length);
        if (closestImage != null) {
            closestImage.setDensity(closestDensity);
        }
        return closestImage;
    }

    private Bitmap loadImage() throws ArrayIndexOutOfBoundsException {
        Bitmap cached_image = this.cached_images.get(new Integer(this.densityType));
        if (cached_image == null) {
            cached_image = getClosestImage(this.densityType);
            if (cached_image == null) {
                throw new ArrayIndexOutOfBoundsException("You have not specified image data for the requested density or the image data is invalid");
            }
            this.cached_images.put(new Integer(this.densityType), cached_image);
        }
        return cached_image;
    }

    public Bitmap loadImage(int densityType2) throws ArrayIndexOutOfBoundsException {
        this.densityType = densityType2;
        return loadImage();
    }
}
