package v2.com.playhaven.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PHReward implements Parcelable {
    public static final Parcelable.Creator<PHReward> CREATOR = new Parcelable.Creator<PHReward>() {
        public PHReward[] newArray(int size) {
            return new PHReward[size];
        }

        public PHReward createFromParcel(Parcel source) {
            return new PHReward(source);
        }
    };
    public String name;
    public int quantity;
    public String receipt;

    public PHReward() {
    }

    public PHReward(Parcel in) {
        this.name = in.readString();
        if (this.name != null && this.name.equals(PHContent.PARCEL_NULL)) {
            this.name = null;
        }
        this.quantity = in.readInt();
        this.receipt = in.readString();
        if (this.receipt != null && this.receipt.equals(PHContent.PARCEL_NULL)) {
            this.receipt = null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.name == null ? PHContent.PARCEL_NULL : this.name);
        out.writeInt(this.quantity);
        out.writeString(this.receipt == null ? PHContent.PARCEL_NULL : this.receipt);
    }
}
