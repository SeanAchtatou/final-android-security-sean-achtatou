package v2.com.playhaven.model;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Currency;
import v2.com.playhaven.interstitial.requestbridge.BridgeManager;
import v2.com.playhaven.interstitial.requestbridge.bridges.ContentRequestToInterstitialBridge;

public class PHPurchase implements Parcelable {
    public static final Parcelable.Creator<PHPurchase> CREATOR = new Parcelable.Creator<PHPurchase>() {
        public PHPurchase[] newArray(int size) {
            return new PHPurchase[size];
        }

        public PHPurchase createFromParcel(Parcel source) {
            return new PHPurchase(source);
        }
    };
    public static final int DEFAULT_QUANTITY = 1;
    public static final String NO_CONTENTVIEW_INTENT = "v2.com.playhaven.null";
    public String callback;
    public String contentview_intent;
    public Currency currencyLocale;
    public PHError error;
    public PHMarketplaceOrigin marketplace = PHMarketplaceOrigin.Google;
    public String name;
    public double price = 0.0d;
    public String product;
    public int quantity = 1;
    public String receipt;
    public AndroidBillingResult resolution;
    private String tag;

    public enum AndroidBillingResult {
        Bought("buy"),
        Cancelled("cancel"),
        Failed("error");
        
        private String type;

        private AndroidBillingResult(String type2) {
            this.type = type2;
        }

        public String getType() {
            return this.type;
        }
    }

    public enum PHMarketplaceOrigin {
        Google("GoogleMarketplace"),
        Amazon("AmazonAppstore"),
        Paypal("Paypal"),
        Crossmo("Crossmo"),
        Motorola("MotorolaAppstore");
        
        private String originStr;

        private PHMarketplaceOrigin(String originStr2) {
            this.originStr = originStr2;
        }

        public String getOrigin() {
            return this.originStr;
        }
    }

    public PHPurchase(String tag2) {
        this.tag = tag2;
    }

    public PHPurchase() {
    }

    public boolean hasError() {
        return this.error != null;
    }

    public String getTag() {
        return this.tag;
    }

    public void reportAndroidBillingResult(AndroidBillingResult resolution2, Context context) {
        this.resolution = resolution2;
        Bundle message = new Bundle();
        message.putParcelable(ContentRequestToInterstitialBridge.InterstitialEventArgument.Purchase.getKey(), this);
        BridgeManager.sendMessageFromRequester(this.tag, ContentRequestToInterstitialBridge.InterstitialEvent.PurchaseResolved.toString(), message, context);
    }

    public PHPurchase(Parcel in) {
        this.product = in.readString();
        if (this.product != null && this.product.equals(PHContent.PARCEL_NULL)) {
            this.product = null;
        }
        this.name = in.readString();
        if (this.name != null && this.name.equals(PHContent.PARCEL_NULL)) {
            this.name = null;
        }
        this.receipt = in.readString();
        if (this.receipt != null && this.receipt.equals(PHContent.PARCEL_NULL)) {
            this.receipt = null;
        }
        this.callback = in.readString();
        if (this.callback != null && this.callback.equals(PHContent.PARCEL_NULL)) {
            this.callback = null;
        }
        this.tag = in.readString();
        if (this.tag != null && this.tag.equals(PHContent.PARCEL_NULL)) {
            this.tag = null;
        }
        String resCandidate = in.readString();
        this.resolution = resCandidate.equals(PHContent.PARCEL_NULL) ? null : AndroidBillingResult.valueOf(resCandidate);
        if (this.resolution != null && this.resolution.equals(PHContent.PARCEL_NULL)) {
            this.resolution = null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.product == null ? PHContent.PARCEL_NULL : this.product);
        out.writeString(this.name == null ? PHContent.PARCEL_NULL : this.name);
        out.writeString(this.receipt == null ? PHContent.PARCEL_NULL : this.receipt);
        out.writeString(this.callback == null ? PHContent.PARCEL_NULL : this.callback);
        out.writeString(this.tag == null ? PHContent.PARCEL_NULL : this.tag);
        out.writeString(this.resolution == null ? PHContent.PARCEL_NULL : this.resolution.toString());
    }
}
