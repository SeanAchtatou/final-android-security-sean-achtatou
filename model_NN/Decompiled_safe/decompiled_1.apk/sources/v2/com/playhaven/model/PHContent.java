package v2.com.playhaven.model;

import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.flurry.android.FlurryFullscreenTakeoverActivity;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;
import v2.com.playhaven.utils.PHStringUtil;

public class PHContent implements Parcelable {
    public static final Parcelable.Creator<PHContent> CREATOR = new Parcelable.Creator<PHContent>() {
        public PHContent createFromParcel(Parcel in) {
            return new PHContent(in);
        }

        public PHContent[] newArray(int size) {
            return new PHContent[size];
        }
    };
    public static final String PARCEL_NULL = "null";
    public double closeButtonDelay;
    public String closeURL;
    public JSONObject context;
    private HashMap<String, JSONObject> frameDict;
    public boolean preloaded;
    public TransitionType transition;
    public Uri url;

    public enum TransitionType {
        Unknown,
        Modal,
        Dialog
    }

    public PHContent() {
        this.transition = TransitionType.Modal;
        this.preloaded = false;
        this.frameDict = new HashMap<>();
        this.closeButtonDelay = 10.0d;
        this.transition = TransitionType.Modal;
    }

    public PHContent(JSONObject dict) {
        this.transition = TransitionType.Modal;
        this.preloaded = false;
        this.frameDict = new HashMap<>();
        fromJSON(dict);
    }

    public PHContent(Parcel in) {
        boolean z;
        this.transition = TransitionType.Modal;
        this.preloaded = false;
        this.frameDict = new HashMap<>();
        String transition_str = in.readString();
        if (transition_str != null && !transition_str.equals(PARCEL_NULL)) {
            this.transition = TransitionType.valueOf(transition_str);
        }
        this.closeURL = in.readString();
        if (this.closeURL != null && this.closeURL.equals(PARCEL_NULL)) {
            this.closeURL = null;
        }
        try {
            String context_str = in.readString();
            if (context_str != null && !context_str.equals(PARCEL_NULL)) {
                this.context = new JSONObject(context_str);
            }
        } catch (JSONException e) {
            PHStringUtil.log("Error hydrating PHContent JSON context from Parcel: " + e.getLocalizedMessage());
        }
        String url_str = in.readString();
        if (url_str != null && !url_str.equals(PARCEL_NULL)) {
            this.url = Uri.parse(url_str);
        }
        this.closeButtonDelay = in.readDouble();
        if (in.readByte() == 1) {
            z = true;
        } else {
            z = false;
        }
        this.preloaded = z;
        Bundle frameBundle = in.readBundle();
        if (frameBundle != null) {
            for (String key : frameBundle.keySet()) {
                try {
                    this.frameDict.put(key, new JSONObject(frameBundle.getString(key)));
                } catch (JSONException e2) {
                    PHStringUtil.log("Error deserializing frameDict from bundle in PHContent");
                }
            }
        }
    }

    public void fromJSON(JSONObject dict) {
        Uri uri;
        try {
            Object frame = dict.opt("frame");
            String url2 = dict.optString(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL);
            String transition2 = dict.optString("transition");
            this.closeButtonDelay = dict.optDouble("close_delay");
            this.closeURL = dict.optString("close_ping");
            this.frameDict.clear();
            if (frame instanceof String) {
                this.frameDict.put((String) frame, new JSONObject(String.format("{\"%s\" : \"%s\"}", frame, frame)));
            } else if (frame instanceof JSONObject) {
                setFrameDict((JSONObject) frame);
            }
            if (url2.compareTo("") != 0) {
                uri = Uri.parse(url2);
            } else {
                uri = null;
            }
            this.url = uri;
            JSONObject context2 = dict.optJSONObject("context");
            if (!JSONObject.NULL.equals(context2) && context2.length() > 0) {
                this.context = context2;
            }
            if (transition2.compareTo("") == 0) {
                return;
            }
            if (transition2.equals("PH_MODAL")) {
                this.transition = TransitionType.Modal;
            } else if (transition2.equals("PH_DIALOG")) {
                this.transition = TransitionType.Dialog;
            } else {
                this.transition = null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean isEmpty() {
        return this.context == null || this.context.length() == 0;
    }

    public HashMap<String, JSONObject> getFrames() {
        return this.frameDict;
    }

    public void setFrames(HashMap<String, JSONObject> frameDict2) {
        this.frameDict = frameDict2;
    }

    private void setFrameDict(JSONObject frame) {
        this.frameDict.clear();
        try {
            Iterator<String> keys = frame.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                this.frameDict.put(key, (JSONObject) frame.get(key));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public RectF getFrame(int orientation) {
        JSONObject dict;
        String orientKey = orientation == 2 ? "PH_LANDSCAPE" : "PH_PORTRAIT";
        if (this.frameDict.containsKey("PH_FULLSCREEN")) {
            return new RectF(0.0f, 0.0f, 2.14748365E9f, 2.14748365E9f);
        }
        if (!this.frameDict.containsKey(orientKey) || (dict = this.frameDict.get(orientKey)) == null) {
            return new RectF(0.0f, 0.0f, 0.0f, 0.0f);
        }
        float x = (float) dict.optDouble("x");
        float y = (float) dict.optDouble("y");
        return new RectF(x, y, x + ((float) dict.optDouble("w")), y + ((float) dict.optDouble("h")));
    }

    public String toString() {
        String formattedJson;
        try {
            formattedJson = this.context.toString(2);
        } catch (JSONException e) {
            e.printStackTrace();
            formattedJson = "(NULL)";
        }
        return String.format("Close URL: %s - Close Delay: %.1f - URL: %s\n\n---------------------------------\nJSON Context: %s", this.closeURL, Double.valueOf(this.closeButtonDelay), this.url, formattedJson);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.transition != null ? this.transition.name() : PARCEL_NULL);
        out.writeString(this.closeURL != null ? this.closeURL : PARCEL_NULL);
        out.writeString(this.context != null ? this.context.toString() : PARCEL_NULL);
        out.writeString(this.url != null ? this.url.toString() : PARCEL_NULL);
        out.writeDouble(this.closeButtonDelay);
        out.writeByte((byte) (this.preloaded ? 1 : 0));
        if (this.frameDict != null) {
            Bundle frameBundle = new Bundle();
            for (String key : this.frameDict.keySet()) {
                frameBundle.putString(key, this.frameDict.get(key).toString());
            }
            out.writeBundle(frameBundle);
        }
    }
}
