package v2.com.playhaven.model;

public class PHError {
    protected int errorCode;
    protected String message;

    public PHError(String message2, int errorCode2) {
        this.message = message2;
        this.errorCode = errorCode2;
    }

    public PHError(String message2) {
        this(message2, -1);
    }

    public PHError(Exception e) {
        this(e.getMessage(), 0);
    }

    public PHError(int errorCode2) {
        this("", errorCode2);
    }

    public String toString() {
        return String.format("PHError with message '%s' and error code %d", this.message, Integer.valueOf(this.errorCode));
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public String getMessage() {
        return this.message;
    }
}
