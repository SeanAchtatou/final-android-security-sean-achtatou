package v2.com.playhaven.views.badge;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.View;
import java.util.HashMap;
import org.json.JSONObject;
import v2.com.playhaven.listeners.PHBadgeRequestListener;
import v2.com.playhaven.model.PHError;
import v2.com.playhaven.requests.badge.PHBadgeRequest;
import v2.com.playhaven.requests.crashreport.PHCrashReport;

public class PHBadgeView extends View implements PHBadgeRequestListener {
    private static HashMap<String, Class> renderMap = new HashMap<>();
    private JSONObject notificationData;
    private AbstractBadgeRenderer notificationRenderer;
    private String placement;
    public PHBadgeRequest request;

    public PHBadgeView(Context context, String placement2) {
        super(context);
        this.placement = placement2;
    }

    public String getPlacement() {
        return this.placement;
    }

    public PHBadgeRequest getRequest() {
        return this.request;
    }

    public JSONObject getNotificationData() {
        return this.notificationData;
    }

    public AbstractBadgeRenderer getNotificationRenderer() {
        return this.notificationRenderer;
    }

    public static HashMap<String, Class> getRenderMap() {
        return renderMap;
    }

    public void refresh() {
        this.request = new PHBadgeRequest(this, this.placement);
        this.request.send(getContext());
    }

    public void clear() {
        this.request = null;
        this.notificationData = null;
    }

    public void updateBadgeData(JSONObject data) {
        if (data != null) {
            try {
                this.notificationData = data;
                this.notificationRenderer = createBadgeRenderer(data);
                requestLayout();
                invalidate();
            } catch (Exception e) {
                PHCrashReport.reportCrash(e, "PHBadgeView - updateBadgeData", PHCrashReport.Urgency.critical);
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v10, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: v2.com.playhaven.views.badge.AbstractBadgeRenderer} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public v2.com.playhaven.views.badge.AbstractBadgeRenderer createBadgeRenderer(org.json.JSONObject r8) {
        /*
            r7 = this;
            java.util.HashMap<java.lang.String, java.lang.Class> r5 = v2.com.playhaven.views.badge.PHBadgeView.renderMap
            int r5 = r5.size()
            if (r5 != 0) goto L_0x000b
            initRenderers()
        L_0x000b:
            java.lang.String r5 = "type"
            java.lang.String r6 = "badge"
            java.lang.String r4 = r8.optString(r5, r6)
            r3 = 0
            java.util.HashMap<java.lang.String, java.lang.Class> r5 = v2.com.playhaven.views.badge.PHBadgeView.renderMap     // Catch:{ Exception -> 0x004a }
            java.lang.Object r2 = r5.get(r4)     // Catch:{ Exception -> 0x004a }
            java.lang.Class r2 = (java.lang.Class) r2     // Catch:{ Exception -> 0x004a }
            java.lang.Object r5 = r2.newInstance()     // Catch:{ Exception -> 0x004a }
            r0 = r5
            v2.com.playhaven.views.badge.AbstractBadgeRenderer r0 = (v2.com.playhaven.views.badge.AbstractBadgeRenderer) r0     // Catch:{ Exception -> 0x004a }
            r3 = r0
            android.content.Context r5 = r7.getContext()     // Catch:{ Exception -> 0x004a }
            android.content.Context r6 = r7.getContext()     // Catch:{ Exception -> 0x004a }
            android.content.res.Resources r6 = r6.getResources()     // Catch:{ Exception -> 0x004a }
            r3.loadResources(r5, r6)     // Catch:{ Exception -> 0x004a }
        L_0x0033:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Created subclass of PHNotificationRenderer of type: "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r5 = r5.append(r4)
            java.lang.String r5 = r5.toString()
            v2.com.playhaven.utils.PHStringUtil.log(r5)
            return r3
        L_0x004a:
            r5 = move-exception
            r1 = r5
            java.lang.String r5 = "PHBadgeView - createBadgeRenderer"
            v2.com.playhaven.requests.crashreport.PHCrashReport$Urgency r6 = v2.com.playhaven.requests.crashreport.PHCrashReport.Urgency.critical
            v2.com.playhaven.requests.crashreport.PHCrashReport.reportCrash(r1, r5, r6)
            r3 = 0
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: v2.com.playhaven.views.badge.PHBadgeView.createBadgeRenderer(org.json.JSONObject):v2.com.playhaven.views.badge.AbstractBadgeRenderer");
    }

    public static void setBadgeRenderer(Class renderer, String type) {
        if (renderer.getSuperclass() != AbstractBadgeRenderer.class) {
            throw new IllegalArgumentException("Cannot create a new renderer of type " + type + " because it does not implement the PHNotificationRenderer interface");
        }
        renderMap.put(type, renderer);
    }

    public static void initRenderers() {
        renderMap.put("badge", PHBadgeRenderer.class);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        try {
            if (this.notificationRenderer != null) {
                this.notificationRenderer.draw(getContext(), canvas, this.notificationData);
            }
        } catch (Exception e) {
            PHCrashReport.reportCrash(e, "PHBadgeView - onDraw", PHCrashReport.Urgency.critical);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthSpec, int heightSpec) {
        try {
            Rect size = new Rect(0, 0, widthSpec, heightSpec);
            if (this.notificationRenderer != null) {
                size = this.notificationRenderer.size(getContext(), this.notificationData);
            }
            setMeasuredDimension(size.width(), size.height());
        } catch (Exception e) {
            PHCrashReport.reportCrash(e, "PHBadgeView - onDraw", PHCrashReport.Urgency.critical);
        }
    }

    public void onBadgeRequestSucceeded(PHBadgeRequest request2, JSONObject responseData) {
        this.request = null;
        JSONObject notification = responseData.optJSONObject("notification");
        if (!JSONObject.NULL.equals(notification) && notification.length() > 0) {
            updateBadgeData(notification);
        }
    }

    public void onBadgeRequestFailed(PHBadgeRequest request2, PHError error) {
        this.request = null;
        updateBadgeData(null);
    }
}
