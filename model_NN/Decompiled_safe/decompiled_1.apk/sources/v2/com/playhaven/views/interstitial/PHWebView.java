package v2.com.playhaven.views.interstitial;

import android.content.Context;
import android.webkit.WebView;
import v2.com.playhaven.cache.PHCache;
import v2.com.playhaven.configuration.PHConfiguration;
import v2.com.playhaven.interstitial.webview.PHWebViewChrome;
import v2.com.playhaven.interstitial.webview.PHWebViewClient;
import v2.com.playhaven.model.PHContent;
import v2.com.playhaven.utils.PHStringUtil;

public class PHWebView extends WebView {
    private PHConfiguration config = new PHConfiguration();
    private PHContent content;
    private boolean doCache;

    public PHWebView(Context context, boolean doCache2, PHWebViewClient client, PHWebViewChrome chrome, PHContent content2) {
        super(context);
        this.doCache = doCache2;
        this.content = content2;
        String cachePath = getContext().getApplicationContext().getCacheDir().getAbsolutePath();
        if (doCache2) {
            getSettings().setCacheMode(2);
        } else {
            getSettings().setCacheMode(-1);
            getSettings().setAppCacheMaxSize((long) this.config.getPrecacheSize());
            getSettings().setAppCachePath(cachePath);
            getSettings().setAllowFileAccess(true);
            getSettings().setAppCacheEnabled(true);
            getSettings().setDomStorageEnabled(true);
            getSettings().setDatabaseEnabled(true);
        }
        getSettings().setUseWideViewPort(true);
        getSettings().setSupportZoom(true);
        getSettings().setLoadWithOverviewMode(true);
        getSettings().setJavaScriptEnabled(true);
        setInitialScale(0);
        setScrollBarStyle(0);
        setBackgroundColor(0);
        setWebViewClient(client);
        setWebChromeClient(chrome);
    }

    public void cleanup() {
        setWebChromeClient(null);
        setWebViewClient(null);
        stopLoading();
    }

    public void loadContentTemplate() {
        stopLoading();
        if (this.content.url.toString().startsWith("http://")) {
            String cached_url = null;
            try {
                PHStringUtil.log("Should we access the cache: " + this.doCache + "....and has it been installed: " + PHCache.hasBeenInstalled());
                if (this.doCache && PHCache.hasBeenInstalled()) {
                    cached_url = PHCache.getSharedCache().getCachedFile(this.content.url.toString());
                }
                PHStringUtil.log("the cached template url returned: " + cached_url);
                PHStringUtil.log("the original template url: " + this.content.url.toString());
                if (cached_url != null) {
                    loadUrl(cached_url);
                } else {
                    loadUrl(this.content.url.toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
