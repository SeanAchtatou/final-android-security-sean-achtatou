package v2.com.playhaven.views.interstitial;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.StateListDrawable;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import v2.com.playhaven.resources.PHResourceManager;
import v2.com.playhaven.resources.data.PHCloseActiveImageResource;
import v2.com.playhaven.resources.data.PHCloseImageResource;

public class PHCloseButton extends ImageButton {
    private Listener listener;

    public interface Listener {
        void onClose(PHCloseButton pHCloseButton);
    }

    public enum CloseButtonState {
        Down(16842919),
        Up(16842910);
        
        private int android_state;

        private CloseButtonState(int android_state2) {
            this.android_state = android_state2;
        }

        public int getAndroidState() {
            return this.android_state;
        }
    }

    public PHCloseButton(Context context, Listener listener2) {
        super(context);
        this.listener = null;
        setContentDescription("closeButton");
        setScaleType(ImageView.ScaleType.FIT_XY);
        setBackgroundDrawable(null);
        loadDefaultStateImages();
        this.listener = listener2;
        final Listener finalListener = listener2;
        setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finalListener.onClose(PHCloseButton.this);
            }
        });
    }

    public PHCloseButton(Context context, Listener listener2, BitmapDrawable customActive, BitmapDrawable customInactive) {
        this(context, listener2);
        setActiveAndInactive(customActive, customInactive);
    }

    private void loadDefaultStateImages() {
        DisplayMetrics dm = getResources().getDisplayMetrics();
        PHResourceManager rm = PHResourceManager.sharedResourceManager();
        setActiveAndInactive(new BitmapDrawable(getResources(), ((PHCloseActiveImageResource) rm.getResource("close_active")).loadImage(dm.densityDpi)), new BitmapDrawable(getResources(), ((PHCloseImageResource) rm.getResource("close_inactive")).loadImage(dm.densityDpi)));
    }

    private void setActiveAndInactive(BitmapDrawable active, BitmapDrawable inactive) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{CloseButtonState.Down.getAndroidState()}, active);
        states.addState(new int[]{CloseButtonState.Up.getAndroidState()}, inactive);
        setImageDrawable(states);
    }
}
