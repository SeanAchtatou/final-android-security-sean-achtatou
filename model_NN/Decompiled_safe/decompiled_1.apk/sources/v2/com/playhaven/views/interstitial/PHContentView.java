package v2.com.playhaven.views.interstitial;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.widget.RelativeLayout;
import v2.com.playhaven.interstitial.jsbridge.ManipulatableContentDisplayer;
import v2.com.playhaven.interstitial.jsbridge.PHJSBridge;
import v2.com.playhaven.interstitial.webview.PHWebViewChrome;
import v2.com.playhaven.interstitial.webview.PHWebViewClient;
import v2.com.playhaven.model.PHContent;
import v2.com.playhaven.utils.PHConversionUtils;
import v2.com.playhaven.views.interstitial.PHCloseButton;

public class PHContentView extends RelativeLayout implements PHCloseButton.Listener {
    private static final int TIME_BEFORE_SHOW_CLOSE_BUTTON = 4000;
    private final float CLOSE_BUTTON_MARGIN = 2.0f;
    private PHCloseButton closeButton;
    private Handler closeButtonTimerHandler;
    private PHContent content;
    private Listener listener;
    private Runnable showCloseButtonRunnable;
    private PHWebView webview;

    public interface Listener {
        void onClose(PHContentView pHContentView);
    }

    public PHContentView(ManipulatableContentDisplayer contentDisplayer, Context context, PHContent content2, Listener listener2, PHJSBridge bridge, BitmapDrawable custom_active, BitmapDrawable custom_inactive) {
        super(context);
        this.listener = listener2;
        this.content = content2;
        setupCloseButton(custom_active, custom_inactive);
        setupWebview(context, bridge, contentDisplayer);
    }

    public void cleanup() {
        if (this.closeButtonTimerHandler != null) {
            this.closeButtonTimerHandler.removeCallbacks(this.showCloseButtonRunnable);
        }
        this.webview.setWebChromeClient(null);
        this.webview.setWebViewClient(null);
    }

    private void setupCloseButton(BitmapDrawable custom_active, BitmapDrawable custom_inactive) {
        float marginInPixels = PHConversionUtils.dipToPixels(getContext(), 2.0f);
        if (custom_active == null || custom_inactive == null) {
            this.closeButton = new PHCloseButton(getContext(), this);
        } else {
            this.closeButton = new PHCloseButton(getContext(), this, custom_active, custom_inactive);
        }
        RelativeLayout.LayoutParams closeLayoutParams = new RelativeLayout.LayoutParams(-2, -2);
        closeLayoutParams.addRule(11);
        closeLayoutParams.setMargins(0, (int) marginInPixels, (int) marginInPixels, 0);
        this.closeButton.setLayoutParams(closeLayoutParams);
        addView(this.closeButton);
        this.closeButton.setVisibility(4);
        startShowCloseButtonTimer();
    }

    private void setupWebview(Context context, PHJSBridge bridge, ManipulatableContentDisplayer contentDisplayer) {
        this.webview = new PHWebView(getContext(), true, new PHWebViewClient(contentDisplayer, bridge, this.content), new PHWebViewChrome(), this.content);
        this.webview.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        bridge.attachWebview(this.webview);
        addView(this.webview);
        this.webview.loadContentTemplate();
    }

    public void close() {
        this.webview.cleanup();
    }

    public void onClose(PHCloseButton button) {
        if (this.listener != null) {
            this.listener.onClose(this);
        }
    }

    public boolean closeButtonIsShowing() {
        return this.closeButton.getVisibility() == 0;
    }

    public void showCloseButton() {
        this.closeButton.bringToFront();
        this.closeButton.setVisibility(0);
    }

    public void hideCloseButton() {
        if (this.closeButtonTimerHandler != null) {
            this.closeButtonTimerHandler.removeCallbacks(this.showCloseButtonRunnable);
        }
        this.closeButton.setVisibility(8);
    }

    private void startShowCloseButtonTimer() {
        this.closeButtonTimerHandler = new Handler();
        this.showCloseButtonRunnable = new Runnable() {
            public void run() {
                PHContentView.this.showCloseButton();
            }
        };
        this.closeButtonTimerHandler.postDelayed(this.showCloseButtonRunnable, 4000);
    }
}
