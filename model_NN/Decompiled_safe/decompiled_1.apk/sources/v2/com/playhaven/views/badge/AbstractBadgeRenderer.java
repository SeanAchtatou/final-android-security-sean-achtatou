package v2.com.playhaven.views.badge;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import org.json.JSONObject;

public abstract class AbstractBadgeRenderer {
    public abstract void draw(Context context, Canvas canvas, JSONObject jSONObject);

    public abstract void loadResources(Context context, Resources resources);

    public abstract Rect size(Context context, JSONObject jSONObject);
}
