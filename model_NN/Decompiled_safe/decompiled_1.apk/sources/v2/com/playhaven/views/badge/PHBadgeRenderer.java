package v2.com.playhaven.views.badge;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.NinePatchDrawable;
import org.json.JSONObject;
import v2.com.playhaven.resources.PHResourceManager;
import v2.com.playhaven.resources.types.PHNinePatchResource;
import v2.com.playhaven.utils.PHConversionUtils;

public class PHBadgeRenderer extends AbstractBadgeRenderer {
    private final float TEXT_SIZE = 17.0f;
    private final float TEXT_SIZE_REDUCE = 8.0f;
    private NinePatchDrawable badgeImage;
    private Paint whitePaint;

    public void loadResources(Context context, Resources res) {
        this.badgeImage = ((PHNinePatchResource) PHResourceManager.sharedResourceManager().getResource("badge_image")).loadNinePatchDrawable(res, context.getResources().getDisplayMetrics().densityDpi);
        this.badgeImage.setFilterBitmap(true);
    }

    public void draw(Context context, Canvas canvas, JSONObject notificationData) {
        int value = requestedValue(notificationData);
        if (value != 0) {
            this.badgeImage.setBounds(size(context, notificationData));
            this.badgeImage.draw(canvas);
            canvas.drawText(Integer.toString(value), PHConversionUtils.dipToPixels(context, 10.0f), PHConversionUtils.dipToPixels(context, 17.0f), getTextPaint(context));
        }
    }

    private Paint getTextPaint(Context context) {
        if (this.whitePaint == null) {
            this.whitePaint = new Paint();
            this.whitePaint.setStyle(Paint.Style.FILL);
            this.whitePaint.setAntiAlias(true);
            this.whitePaint.setTextSize(PHConversionUtils.dipToPixels(context, 17.0f));
            this.whitePaint.setColor(-1);
        }
        return this.whitePaint;
    }

    private int requestedValue(JSONObject notificationData) {
        if (notificationData == null || notificationData.isNull("value")) {
            return 0;
        }
        return notificationData.optInt("value", -1);
    }

    public Rect size(Context context, JSONObject data) {
        float width = (float) this.badgeImage.getMinimumWidth();
        float height = (float) this.badgeImage.getMinimumHeight();
        if (requestedValue(data) == 0) {
            return new Rect(0, 0, 0, 0);
        }
        return new Rect(0, 0, (int) ((width + getTextPaint(context).measureText(String.valueOf(requestedValue(data)))) - PHConversionUtils.dipToPixels(context, 8.0f)), (int) height);
    }
}
