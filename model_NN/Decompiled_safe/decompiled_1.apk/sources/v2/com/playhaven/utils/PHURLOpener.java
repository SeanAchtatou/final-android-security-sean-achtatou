package v2.com.playhaven.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import org.json.JSONObject;
import v2.com.playhaven.configuration.PHConfiguration;
import v2.com.playhaven.listeners.PHHttpRequestListener;
import v2.com.playhaven.model.PHError;
import v2.com.playhaven.requests.base.PHAsyncRequest;

public class PHURLOpener extends ProgressDialog implements PHHttpRequestListener {
    private static final String LOADING_MESSAGE = "Loading...";
    private final String MARKET_URL_TEMPLATE;
    private final int MAXIMUM_REDIRECTS;
    private PHConfiguration config;
    private PHAsyncRequest conn;
    private String contentTemplateCallback;
    private boolean isLoading;
    private WeakReference<Listener> listener;
    private boolean shouldOpenUrl;
    private String targetURL;

    public interface Listener {
        void onURLOpenerFailed(PHURLOpener pHURLOpener);

        void onURLOpenerFinished(PHURLOpener pHURLOpener);
    }

    public PHURLOpener(Context context, Listener listener2) {
        this(context);
        this.listener = new WeakReference<>(listener2);
    }

    public PHURLOpener(Context context) {
        super(context);
        this.MARKET_URL_TEMPLATE = "http://play.google.com/marketplace/apps/details?id=%s";
        this.isLoading = false;
        this.MAXIMUM_REDIRECTS = 10;
        this.shouldOpenUrl = true;
        this.config = new PHConfiguration();
    }

    public void setShouldOpenFinalURL(boolean shouldOpen) {
        this.shouldOpenUrl = shouldOpen;
    }

    public boolean isLoading() {
        return this.isLoading;
    }

    public void setContentTemplateCallback(String callback) {
        this.contentTemplateCallback = callback;
    }

    public String getContentTemplateCallback() {
        return this.contentTemplateCallback;
    }

    public PHAsyncRequest getConnection() {
        return this.conn;
    }

    public void setConnection(PHAsyncRequest conn2) {
        this.conn = conn2;
    }

    public void setTargetURL(String url) {
        this.targetURL = url;
    }

    public String getTargetURL() {
        return this.targetURL;
    }

    public void open(String url) {
        this.targetURL = url;
        if (!JSONObject.NULL.equals(this.targetURL) && this.targetURL.length() > 0) {
            PHStringUtil.log(String.format("Opening url in PHURLOpener: %s", this.targetURL));
            this.isLoading = true;
            this.conn = new PHAsyncRequest(this);
            this.conn.setMaxRedirects(10);
            this.conn.request_type = PHAsyncRequest.RequestType.Get;
            this.conn.execute(Uri.parse(this.targetURL));
            setMessage(LOADING_MESSAGE);
            show();
        } else if (this.listener != null && this.listener.get() != null) {
            this.listener.get().onURLOpenerFinished(this);
        }
    }

    private void finish() {
        if (this.isLoading) {
            this.isLoading = false;
            this.targetURL = this.conn.getLastRedirectURL();
            PHStringUtil.log("PHURLOpener - final redirect location: " + this.targetURL);
            if (this.shouldOpenUrl && this.targetURL != null && !this.targetURL.equals("")) {
                if (this.targetURL.startsWith("market:")) {
                    openMarketURL(this.targetURL);
                } else {
                    launchActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.targetURL)));
                }
            }
            if (!(this.listener == null || this.listener.get() == null)) {
                this.listener.get().onURLOpenerFinished(this);
            }
            invalidate();
        }
    }

    private void launchActivity(Intent intent) {
        if (!this.config.getIsRunningUITests(getContext())) {
            PHStringUtil.log("PHURLOpener just launched intent: " + intent.getData());
            getContext().startActivity(intent);
        }
    }

    private void openMarketURL(String url) {
        PHStringUtil.log("Got a market:// URL, verifying market app is installed");
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(url));
        if (getContext().getPackageManager().queryIntentActivities(intent, 65536).size() == 0) {
            PHStringUtil.log("Market app is not installed and market:// not supported!");
            intent = new Intent("android.intent.action.VIEW", Uri.parse(String.format("http://play.google.com/marketplace/apps/details?id=%s", Uri.parse(url).getQueryParameter("id"))));
        }
        PHStringUtil.log("PHURLOpener is trying to launch: " + url);
        launchActivity(intent);
    }

    private void invalidate() {
        this.listener = null;
        if (this.conn != null) {
            synchronized (this) {
                this.conn.cancel(true);
            }
            dismiss();
        }
    }

    private void fail() {
        if (!(this.listener == null || this.listener.get() == null)) {
            this.listener.get().onURLOpenerFailed(this);
        }
        invalidate();
    }

    public void onHttpRequestSucceeded(ByteBuffer response, int responseCode) {
        if (responseCode < 300) {
            PHStringUtil.log("PHURLOpener finishing from initial url: " + this.targetURL);
            finish();
            return;
        }
        PHStringUtil.log("PHURLOpener failing from initial url: " + this.targetURL + " with error code: " + responseCode);
        fail();
    }

    public void onHttpRequestFailed(PHError e) {
        PHStringUtil.log("PHURLOpener failed with error: " + e);
        fail();
    }
}
