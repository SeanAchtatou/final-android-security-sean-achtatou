package v2.com.playhaven.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import v2.com.playhaven.configuration.PHConfiguration;

public class PHConnectionUtils {
    public static PHConfiguration.ConnectionType getConnectionType(Context context) {
        try {
            ConnectivityManager manager = (ConnectivityManager) context.getSystemService("connectivity");
            if (manager == null) {
                return PHConfiguration.ConnectionType.NO_NETWORK;
            }
            NetworkInfo mobileInfo = manager.getNetworkInfo(0);
            NetworkInfo wifiInfo = manager.getNetworkInfo(1);
            if (mobileInfo == null || wifiInfo == null) {
                return PHConfiguration.ConnectionType.NO_NETWORK;
            }
            NetworkInfo.State mobile = mobileInfo.getState();
            NetworkInfo.State wifi = wifiInfo.getState();
            if (mobile == NetworkInfo.State.CONNECTED || mobile == NetworkInfo.State.CONNECTING) {
                return PHConfiguration.ConnectionType.MOBILE;
            }
            if (wifi == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.CONNECTING) {
                return PHConfiguration.ConnectionType.WIFI;
            }
            return PHConfiguration.ConnectionType.NO_NETWORK;
        } catch (SecurityException e) {
            return PHConfiguration.ConnectionType.NO_PERMISSION;
        }
    }
}
