package v2.com.playhaven.utils;

import android.content.Context;

public class PHConversionUtils {
    public static float dipToPixels(Context context, float dips) {
        return context.getResources().getDisplayMetrics().density * dips;
    }
}
