package v2.com.playhaven.utils;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import v2.com.playhaven.configuration.PHConfiguration;

public class PHStringUtil {
    public static UUIDGenerator UUID_GENERATOR = new DefaultUUIDGenerator();

    public interface UUIDGenerator {
        String generateUUID();
    }

    public static String decodeURL(String url) {
        throw new UnsupportedOperationException("This method is not yet implemented");
    }

    public static String encodeURL(String url) {
        throw new UnsupportedOperationException("This method is not yet implemented");
    }

    private static class DefaultUUIDGenerator implements UUIDGenerator {
        private DefaultUUIDGenerator() {
        }

        public String generateUUID() {
            return UUID.randomUUID().toString();
        }
    }

    public static HashMap<String, String> createQueryDict(String query) {
        if (query == null) {
            return null;
        }
        HashMap<String, String> queryComps = new HashMap<>();
        for (String part : query.split("&|\\?")) {
            String[] kv = part.split("=");
            if (kv.length >= 2) {
                queryComps.put(urlDecode(kv[0]), urlDecode(kv[1]));
            }
        }
        return queryComps;
    }

    public static void log(String message) {
        Log.d(String.format("PlayHaven-%s", new PHConfiguration().getSDKVersion()), message);
    }

    public static String createQuery(HashMap<String, String> dict) {
        if (dict == null) {
            return null;
        }
        StringBuilder query = new StringBuilder();
        for (Map.Entry<String, String> entry : dict.entrySet()) {
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            if (!(key == null || value == null)) {
                query.append(String.format(query.length() == 0 ? "%s=%s" : "&%s=%s", urlEncode(key), weakUrlEncode(value)));
            }
        }
        return query.toString();
    }

    public static String queryComponent(String url) {
        int queryStart = url.indexOf("?");
        if (queryStart == -1) {
            return null;
        }
        return url.substring(queryStart + 1);
    }

    public static String urlEncode(String in) {
        return URLEncoder.encode(in);
    }

    public static String weakUrlEncode(String url) {
        if (url == null) {
            return null;
        }
        String[] reserved = {";", "?", " ", "&", "=", "$", ",", "[", "]", "#", "!", "'", "(", ")", "*"};
        String[] escaped = {"%3B", "%3F", "+", "%26", "%3D", "%24", "%2C", "%5B", "%5D", "%21", "%27", "%28", "%28", "%29", "%2A"};
        StringBuilder encUrl = new StringBuilder(url);
        for (int i = 0; i < escaped.length; i++) {
            String res = reserved[i];
            String esc = escaped[i];
            for (int index = encUrl.indexOf(res); index != -1; index = encUrl.indexOf(res)) {
                encUrl.replace(index, res.length() + index, esc);
            }
        }
        return encUrl.toString();
    }

    public static String urlDecode(String in) {
        return URLDecoder.decode(in);
    }

    private static String convertToHex(byte[] in) {
        Formatter formatter = new Formatter(new StringBuilder(in.length * 2));
        byte[] arr$ = in;
        int len$ = arr$.length;
        for (int i$ = 0; i$ < len$; i$++) {
            formatter.format("%02x", Byte.valueOf(arr$[i$]));
        }
        String hex = formatter.toString();
        formatter.close();
        return hex;
    }

    public static String hexDigest(String input) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        return convertToHex(dataDigest(input));
    }

    public static String base64Digest(String input) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String b64digest = convertToBase64(dataDigest(input));
        return b64digest.substring(0, b64digest.length() - 1);
    }

    private static String convertToBase64(byte[] in) throws UnsupportedEncodingException {
        if (in == null) {
            return null;
        }
        return new String(Base64.encode(in, 9), "UTF8");
    }

    private static byte[] dataDigest(String in) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        if (in == null) {
            return null;
        }
        return MessageDigest.getInstance("SHA-1").digest(in.getBytes("UTF8"));
    }

    public static String generateUUID() {
        return UUID_GENERATOR.generateUUID();
    }

    public static String encodeHtml(String to_encode) {
        return TextUtils.htmlEncode(to_encode);
    }
}
