package v2.com.playhaven.cache;

import com.flurry.android.FlurryFullscreenTakeoverActivity;
import com.playhaven.src.utils.PHStringUtil;
import java.util.Iterator;
import java.util.LinkedList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import v2.com.playhaven.model.PHContent;
import v2.com.playhaven.requests.open.PHPrefetchTask;

public class PHImageCache {
    private static String IMAGE_KEY = "image";
    private static String LANDSCAPE_KEY = "PH_LANDSCAPE";
    private static String NO_NAME = "<no name>";
    private static String PORTRAIT_KEY = "PH_PORTRAIT";
    private static String URL_KEY = FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL;
    private static PHImageCache sharedImageCache;

    private static class JSONNode {
        public JSONArray array;
        public String name;
        public JSONObject object;

        public JSONNode(JSONObject object2) {
            this.object = object2;
        }

        public JSONNode(JSONArray array2) {
            this.array = array2;
        }

        public JSONNode(JSONObject object2, String name2) {
            this.object = object2;
            this.name = name2;
        }

        public JSONNode(JSONArray array2, String name2) {
            this.array = array2;
            this.name = name2;
        }

        public boolean isArray() {
            return this.array != null;
        }

        public boolean isObject() {
            return this.object != null;
        }

        public boolean hasName() {
            return this.name != null;
        }

        public boolean nameIs(String name2) {
            return hasName() && name2.equals(name2);
        }

        public int children() {
            if (isObject()) {
                return this.object.length();
            }
            if (isArray()) {
                return this.array.length();
            }
            return -1;
        }
    }

    public static PHImageCache getSharedCache() {
        if (PHCache.hasBeenInstalled() && sharedImageCache == null) {
            sharedImageCache = new PHImageCache();
        }
        return sharedImageCache;
    }

    public static boolean hasBeenInstalled() {
        return PHCache.hasBeenInstalled();
    }

    public PHContent cacheImages(PHContent content) {
        PHStringUtil.log("Caching images");
        cacheAllImagesInContent(content.context);
        return content;
    }

    private String cacheResource(String url) {
        if (url == null || PHCache.hasNotBeenInstalled()) {
            return url;
        }
        String local_url = PHCache.getSharedCache().getCachedFile(url);
        PHStringUtil.log("Checking for image url in cache: " + url + " and finding local URL: " + local_url);
        if (local_url != null) {
            return local_url;
        }
        PHStringUtil.log("Starting new cache request for image: " + url);
        PHPrefetchTask task = new PHPrefetchTask();
        task.setURL(url);
        task.execute(new Integer[0]);
        return url;
    }

    private void cacheImageEntry(JSONObject imageEntry) {
        JSONObject portrait = imageEntry.optJSONObject(PORTRAIT_KEY);
        JSONObject landscape = imageEntry.optJSONObject(LANDSCAPE_KEY);
        if (portrait != null) {
            try {
                portrait.putOpt(URL_KEY, cacheResource(portrait.optString(URL_KEY, null)));
            } catch (JSONException e) {
                return;
            }
        }
        if (landscape != null) {
            landscape.putOpt(URL_KEY, cacheResource(landscape.optString(URL_KEY, null)));
        }
    }

    private void convertNodeToCached(JSONNode node) {
        if (node.isObject() && node.nameIs("image")) {
            cacheImageEntry(node.object);
        }
    }

    private void enqueueNodeChildren(JSONNode node, LinkedList<JSONNode> queue) {
        if (node != null && node.children() != 0) {
            if (node.isArray()) {
                JSONArray array = node.array;
                for (int i = 0; i < array.length(); i++) {
                    JSONObject childObject = array.optJSONObject(i);
                    if (childObject != null) {
                        queue.addFirst(new JSONNode(childObject, NO_NAME));
                    } else {
                        JSONArray childArray = array.optJSONArray(i);
                        if (childArray != null) {
                            queue.addFirst(new JSONNode(childArray, NO_NAME));
                        }
                    }
                }
            } else if (node.isObject()) {
                JSONObject object = node.object;
                Iterator<String> keys = object.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    JSONObject cur_obj = object.optJSONObject(key);
                    if (cur_obj != null) {
                        queue.addFirst(new JSONNode(cur_obj, key));
                    } else {
                        JSONArray cur_array = object.optJSONArray(key);
                        if (cur_array != null) {
                            queue.addFirst(new JSONNode(cur_array, key));
                        }
                    }
                }
            }
        }
    }

    private void cacheAllImagesInContent(JSONObject contentJSON) {
        if (contentJSON != null && contentJSON.length() != 0) {
            LinkedList<JSONNode> queue = new LinkedList<>();
            queue.addFirst(new JSONNode(contentJSON, NO_NAME));
            while (queue.size() > 0) {
                JSONNode cur_node = (JSONNode) queue.removeLast();
                convertNodeToCached(cur_node);
                enqueueNodeChildren(cur_node, queue);
            }
        }
    }
}
