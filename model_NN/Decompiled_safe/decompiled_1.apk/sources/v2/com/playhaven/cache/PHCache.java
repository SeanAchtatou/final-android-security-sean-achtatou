package v2.com.playhaven.cache;

import android.content.Context;
import com.playhaven.src.utils.PHStringUtil;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;

public class PHCache {
    private static final Integer BUFFER_SIZE = 1024;
    private static final String CACHE_DIR = "playhaven.cache";
    private static PHCache sharedCache;
    private final String FILE_PREFIX = "file://";
    private File cacheDirectory;
    private HashMap<URL, File> cachedMapping = new HashMap<>();

    public static PHCache getSharedCache() {
        return sharedCache;
    }

    public static void useTestingCache(PHCache testing_cache) {
        sharedCache = testing_cache;
    }

    public static void installCache(Context context) {
        File dir = new File(context.getCacheDir() + File.separator + CACHE_DIR);
        if (!dir.exists()) {
            dir.mkdir();
        }
        sharedCache = new PHCache(dir);
    }

    public PHCache(File cacheDirectory2) {
        this.cacheDirectory = cacheDirectory2;
    }

    public File getRootCacheDirectory() {
        return this.cacheDirectory;
    }

    public static boolean hasBeenInstalled() {
        return sharedCache != null;
    }

    public static boolean hasNotBeenInstalled() {
        return !hasBeenInstalled();
    }

    public File convertToFilename(URL url) {
        return new File(sharedCache.getRootCacheDirectory().getAbsolutePath() + File.separator + url.toString().replace(File.separator, "_"));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public void cacheFile(URL requestUrl, InputStream content, boolean isCompressed) {
        File outputFile = convertToFilename(requestUrl);
        PHStringUtil.log("Caching url: " + requestUrl + " to local file: " + outputFile);
        if (isCompressed) {
            try {
                content = new GZIPInputStream(content);
            } catch (IOException e) {
                return;
            }
        }
        BufferedOutputStream cachedFile = new BufferedOutputStream(new FileOutputStream(outputFile, false));
        byte[] buffer = new byte[BUFFER_SIZE.intValue()];
        while (true) {
            int bytesRead = content.read(buffer);
            if (bytesRead != -1) {
                cachedFile.write(buffer, 0, bytesRead);
            } else {
                cachedFile.flush();
                cachedFile.close();
                content.close();
                return;
            }
        }
    }

    public File getCachedFile(URL url) {
        File file = this.cachedMapping.get(url);
        PHStringUtil.log("Checking cache for URL: " + url);
        if (file != null) {
            return file;
        }
        File file2 = convertToFilename(url);
        PHStringUtil.log("Checking cache for file: " + file2);
        if (!file2.exists()) {
            return null;
        }
        this.cachedMapping.put(url, file2);
        return file2;
    }

    public String getCachedFile(String url) {
        try {
            File file = getCachedFile(new URL(url));
            if (file == null) {
                return null;
            }
            return "file://" + file.getAbsolutePath();
        } catch (MalformedURLException e) {
            return null;
        }
    }
}
