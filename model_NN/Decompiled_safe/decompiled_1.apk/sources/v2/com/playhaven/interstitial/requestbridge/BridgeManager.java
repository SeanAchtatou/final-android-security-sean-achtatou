package v2.com.playhaven.interstitial.requestbridge;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import java.util.HashMap;
import v2.com.playhaven.interstitial.requestbridge.base.ContentDisplayer;
import v2.com.playhaven.interstitial.requestbridge.base.ContentRequester;
import v2.com.playhaven.interstitial.requestbridge.base.RequestBridge;
import v2.com.playhaven.utils.PHStringUtil;

public class BridgeManager {
    public static HashMap<String, RequestBridge> bridges = new HashMap<>();

    public static void openBridge(String tag, RequestBridge bridge) {
        bridges.put(tag, bridge);
    }

    public static void closeBridge(String tag) {
        if (bridges.get(tag) != null) {
            bridges.remove(tag);
        }
    }

    public static void attachRequester(String tag, ContentRequester requester) {
        RequestBridge bridge = bridges.get(tag);
        if (bridge != null) {
            bridge.attachRequester(requester);
        }
    }

    public static void detachRequester(String tag) {
        RequestBridge bridge = bridges.get(tag);
        if (bridge != null) {
            bridge.detachRequester();
        }
    }

    public static ContentRequester getRequester(String tag) {
        RequestBridge bridge = bridges.get(tag);
        if (bridge == null) {
            return null;
        }
        return bridge.getRequester();
    }

    public static void attachDisplayer(String tag, ContentDisplayer displayer) {
        RequestBridge bridge = bridges.get(tag);
        if (bridge != null) {
            bridge.attachDisplayer(displayer);
        }
    }

    public static void transferBridge(String old_tag, String new_tag) {
        RequestBridge bridge = bridges.get(old_tag);
        if (bridge != null) {
            bridges.put(new_tag, bridge);
            bridges.remove(old_tag);
            bridge.onTagChanged(new_tag);
        }
    }

    public static void detachDisplayer(String tag) {
        RequestBridge bridge = bridges.get(tag);
        if (bridge != null) {
            bridge.detachDisplayer();
        }
    }

    public static ContentDisplayer getDisplayer(String tag) {
        RequestBridge bridge = bridges.get(tag);
        if (bridge == null) {
            return null;
        }
        return bridge.getDisplayer();
    }

    public static RequestBridge getBridge(String tag) {
        return bridges.get(tag);
    }

    private static void sendBroadcast(String intentFilter, String tag, String event, Bundle message, Context context) {
        Intent intent = new Intent(intentFilter);
        message.putString(RequestBridge.Message.Tag.getKey(), tag);
        message.putString(RequestBridge.Message.Event.getKey(), event);
        intent.putExtra(RequestBridge.BROADCAST_METADATA_KEY, message);
        context.getApplicationContext().sendBroadcast(intent);
    }

    public static void sendMessageFromRequester(String tag, String event, Bundle message, Context context) {
        RequestBridge bridge = bridges.get(tag);
        if (bridge != null) {
            sendBroadcast(bridge.getDisplayerIntentFilter(), tag, event, message, context);
        }
    }

    public static void sendMessageFromDisplayer(String tag, String event, Bundle message, Context context) {
        RequestBridge bridge = bridges.get(tag);
        if (bridge != null) {
            PHStringUtil.log("Sending message from displayer: " + event);
            sendBroadcast(bridge.getRequesterIntentFilter(), tag, event, message, context);
        }
    }
}
