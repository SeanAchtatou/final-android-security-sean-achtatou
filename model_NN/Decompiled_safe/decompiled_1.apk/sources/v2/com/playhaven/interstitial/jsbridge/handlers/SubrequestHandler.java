package v2.com.playhaven.interstitial.jsbridge.handlers;

import android.os.Bundle;
import com.flurry.android.FlurryFullscreenTakeoverActivity;
import org.json.JSONException;
import org.json.JSONObject;
import v2.com.playhaven.interstitial.PHContentEnums;
import v2.com.playhaven.interstitial.jsbridge.ManipulatableContentDisplayer;
import v2.com.playhaven.interstitial.requestbridge.bridges.ContentRequestToInterstitialBridge;
import v2.com.playhaven.listeners.PHSubContentRequestListener;
import v2.com.playhaven.model.PHContent;
import v2.com.playhaven.model.PHError;
import v2.com.playhaven.requests.content.PHSubContentRequest;
import v2.com.playhaven.requests.crashreport.PHCrashReport;

public class SubrequestHandler extends AbstractHandler implements PHSubContentRequestListener {
    public void handle(JSONObject jsonPayload) {
        if (jsonPayload != null) {
            PHSubContentRequest request = new PHSubContentRequest(this);
            request.setBaseURL(jsonPayload.optString(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL, ""));
            request.setWebviewCallback(this.bridge.getCurrentQueryVar("callback"));
            ((ManipulatableContentDisplayer) this.contentDisplayer.get()).launchSubRequest(request);
            notifyRequesterOfStarting();
        }
    }

    private void notifyRequesterOfStarting() {
        Bundle message = new Bundle();
        ((ManipulatableContentDisplayer) this.contentDisplayer.get()).sendEventToRequester(ContentRequestToInterstitialBridge.InterstitialEvent.SentSubrequest.toString(), message);
    }

    private void notifyRequesterOfError(PHContentEnums.Error error) {
        Bundle message = new Bundle();
        message.putString(ContentRequestToInterstitialBridge.InterstitialEventArgument.Error.toString(), error.toString());
        ((ManipulatableContentDisplayer) this.contentDisplayer.get()).sendEventToRequester(ContentRequestToInterstitialBridge.InterstitialEvent.Failed.toString(), message);
    }

    public void onSubContentRequestSucceeded(PHSubContentRequest request, JSONObject responseData) {
        if (responseData.length() != 0) {
            try {
                PHContent content = new PHContent(responseData);
                if (content.url != null) {
                    ((ManipulatableContentDisplayer) this.contentDisplayer.get()).launchNestedContentDisplayer(content);
                    sendResponseToWebview(request.getWebviewCallback(), responseData, null);
                    return;
                }
                try {
                    JSONObject error_dict = new JSONObject();
                    error_dict.put("error", "1");
                    sendResponseToWebview(request.getWebviewCallback(), responseData, error_dict);
                    notifyRequesterOfError(PHContentEnums.Error.FailedSubrequest);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (Exception e2) {
                PHCrashReport.reportCrash(e2, "PHInterstitialActivity - requestSucceeded(request, responseData)", PHCrashReport.Urgency.critical);
            }
        }
    }

    public void onSubContentRequestFailed(PHSubContentRequest request, PHError e) {
        if (request != null) {
            try {
                JSONObject error = new JSONObject();
                error.putOpt("error", "1");
                sendResponseToWebview(request.getWebviewCallback(), null, error);
                notifyRequesterOfError(PHContentEnums.Error.FailedSubrequest);
            } catch (JSONException e2) {
                PHCrashReport.reportCrash(e2, "PHInterstitialActivity - requestFailed(request, responseData)", PHCrashReport.Urgency.low);
            } catch (Exception e3) {
                PHCrashReport.reportCrash(e3, "PHInterstitialActivity - requestFailed(request, responseData)", PHCrashReport.Urgency.critical);
            }
        }
    }
}
