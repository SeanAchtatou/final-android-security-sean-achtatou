package v2.com.playhaven.interstitial.webview;

import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.lang.ref.WeakReference;
import v2.com.playhaven.configuration.PHConfiguration;
import v2.com.playhaven.interstitial.jsbridge.ManipulatableContentDisplayer;
import v2.com.playhaven.interstitial.jsbridge.PHJSBridge;
import v2.com.playhaven.interstitial.requestbridge.bridges.ContentRequestToInterstitialBridge;
import v2.com.playhaven.model.PHContent;
import v2.com.playhaven.requests.crashreport.PHCrashReport;
import v2.com.playhaven.utils.PHStringUtil;

public class PHWebViewClient extends WebViewClient {
    private PHJSBridge bridge;
    private PHConfiguration config = new PHConfiguration();
    private PHContent content;
    private WeakReference<ManipulatableContentDisplayer> contentDisplayer;

    public PHWebViewClient(ManipulatableContentDisplayer contentDisplayer2, PHJSBridge bridge2, PHContent content2) {
        this.bridge = bridge2;
        this.contentDisplayer = new WeakReference<>(contentDisplayer2);
        this.content = content2;
    }

    public boolean doesntHaveContentDisplayer() {
        return this.contentDisplayer == null || this.contentDisplayer.get() == null;
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public void onPageFinished(WebView webview, String url) {
        if (!doesntHaveContentDisplayer() && url.startsWith(this.content.url.toString())) {
            String event = ContentRequestToInterstitialBridge.InterstitialEvent.Loaded.toString();
            Bundle message = new Bundle();
            message.putParcelable(ContentRequestToInterstitialBridge.InterstitialEventArgument.Content.getKey(), this.content);
            this.contentDisplayer.get().sendEventToRequester(event, message);
        }
    }

    public void onLoadResource(WebView view, String url) {
        try {
            if (url.startsWith("ph://")) {
                routePlayhavenCallback(url);
            }
        } catch (Exception e) {
            PHCrashReport.reportCrash(e, "PHWebViewClient - onLoadResource", PHCrashReport.Urgency.critical);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        if (!doesntHaveContentDisplayer()) {
            try {
                view.loadUrl("");
                String description2 = String.format("Error loading template at url: %s Code: %d Description: %s", failingUrl, Integer.valueOf(errorCode), description);
                PHStringUtil.log(description2);
                Bundle message = new Bundle();
                String event = ContentRequestToInterstitialBridge.InterstitialEvent.Failed.toString();
                message.putString(event, description2);
                this.contentDisplayer.get().sendEventToRequester(event, message);
            } catch (Exception e) {
                PHCrashReport.reportCrash(e, "PHWebViewClient - onReceivedError", PHCrashReport.Urgency.low);
            }
        }
    }

    public boolean shouldOverrideUrlLoading(WebView webview, String url) {
        try {
            return routePlayhavenCallback(url);
        } catch (Exception e) {
            PHCrashReport.reportCrash(e, "PHWebViewClient - shouldOverrideUrlLoading", PHCrashReport.Urgency.critical);
            return false;
        }
    }

    private boolean routePlayhavenCallback(String url) {
        PHStringUtil.log("Received webview callback: " + url);
        try {
            if (this.bridge.hasRoute(url)) {
                this.bridge.route(url);
                return true;
            }
        } catch (Exception e) {
            PHCrashReport.reportCrash(e, "PHWebViewClient - url routing", PHCrashReport.Urgency.critical);
        }
        return false;
    }
}
