package v2.com.playhaven.interstitial.jsbridge.handlers;

import android.os.Bundle;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import org.json.JSONArray;
import org.json.JSONObject;
import v2.com.playhaven.configuration.PHConfiguration;
import v2.com.playhaven.interstitial.PHContentEnums;
import v2.com.playhaven.interstitial.jsbridge.ManipulatableContentDisplayer;
import v2.com.playhaven.interstitial.requestbridge.bridges.ContentRequestToInterstitialBridge;
import v2.com.playhaven.model.PHPurchase;
import v2.com.playhaven.requests.crashreport.PHCrashReport;
import v2.com.playhaven.requests.purchases.PHIAPTrackingRequest;
import v2.com.playhaven.utils.PHStringUtil;

public class PurchaseHandler extends AbstractHandler {
    public void handle(JSONObject jsonPayload) {
        JSONArray purchases;
        if (jsonPayload != null) {
            try {
                if (jsonPayload.isNull("purchases")) {
                    purchases = new JSONArray();
                } else {
                    purchases = jsonPayload.optJSONArray("purchases");
                }
                for (int i = 0; i < purchases.length(); i++) {
                    JSONObject purchase = purchases.optJSONObject(i);
                    if (validatePurchase(purchase)) {
                        PHPurchase p = new PHPurchase(((ManipulatableContentDisplayer) this.contentDisplayer.get()).getTag());
                        p.product = purchase.optString(PHContentEnums.Purchase.ProductIDKey.key(), "");
                        p.name = purchase.optString(PHContentEnums.Purchase.NameKey.key(), "");
                        p.receipt = purchase.optString(PHContentEnums.Purchase.ReceiptKey.key(), "");
                        p.callback = this.bridge.getCurrentQueryVar("callback");
                        PHIAPTrackingRequest.setConversionCookie(p.product, purchase.optString(PHContentEnums.Purchase.CookieKey.key()));
                        Bundle message = new Bundle();
                        message.putParcelable(ContentRequestToInterstitialBridge.InterstitialEventArgument.Purchase.getKey(), p);
                        ((ManipulatableContentDisplayer) this.contentDisplayer.get()).sendEventToRequester(ContentRequestToInterstitialBridge.InterstitialEvent.MadePurchase.toString(), message);
                    }
                }
                sendResponseToWebview(this.bridge.getCurrentQueryVar("callback"), null, null);
            } catch (Exception e) {
                PHCrashReport.reportCrash(e, "PHInterstitialActivity - handlePurchase", PHCrashReport.Urgency.critical);
            }
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 11 */
    private boolean validatePurchase(JSONObject data) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        if (JSONObject.NULL.equals(data) || data.length() == 0) {
            return false;
        }
        String productID = data.optString(PHContentEnums.Purchase.ProductIDKey.key(), "");
        String name = data.optString(PHContentEnums.Purchase.NameKey.key(), "");
        String receipt = data.optString(PHContentEnums.Purchase.ReceiptKey.key(), "");
        String signature = data.optString(PHContentEnums.Purchase.SignatureKey.key(), "");
        new PHConfiguration();
        String generatedSig = PHStringUtil.hexDigest(String.format("%s:%s:%s:%s:%s:%s", productID, name, 1, ((ManipulatableContentDisplayer) this.contentDisplayer.get()).getDeviceID(), receipt, ((ManipulatableContentDisplayer) this.contentDisplayer.get()).getSecret()));
        PHStringUtil.log("Checking purchase signature:  " + signature + " against: " + generatedSig);
        return generatedSig.equals(signature);
    }
}
