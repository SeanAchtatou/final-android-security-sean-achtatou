package v2.com.playhaven.interstitial.jsbridge.handlers;

import android.os.Bundle;
import com.flurry.android.FlurryFullscreenTakeoverActivity;
import org.json.JSONException;
import org.json.JSONObject;
import v2.com.playhaven.interstitial.PHContentEnums;
import v2.com.playhaven.interstitial.jsbridge.ManipulatableContentDisplayer;
import v2.com.playhaven.interstitial.requestbridge.bridges.ContentRequestToInterstitialBridge;
import v2.com.playhaven.requests.content.PHContentRequest;
import v2.com.playhaven.requests.crashreport.PHCrashReport;
import v2.com.playhaven.utils.PHURLOpener;

public class LaunchHandler extends AbstractHandler implements PHURLOpener.Listener {
    public void handle(JSONObject jsonPayload) {
        if (jsonPayload != null && !doesntHaveContentDisplayer()) {
            ((ManipulatableContentDisplayer) this.contentDisplayer.get()).launchURL(jsonPayload.optString(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL, ""), this);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 9 */
    public void onURLOpenerFinished(PHURLOpener loader) {
        if (loader.getContentTemplateCallback() != null) {
            try {
                JSONObject r = new JSONObject();
                r.put(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL, loader.getTargetURL());
                sendResponseToWebview(loader.getContentTemplateCallback(), r, null);
                Bundle message = new Bundle();
                message.putString(ContentRequestToInterstitialBridge.InterstitialEventArgument.LaunchURL.getKey(), loader.getTargetURL());
                ((ManipulatableContentDisplayer) this.contentDisplayer.get()).sendEventToRequester(ContentRequestToInterstitialBridge.InterstitialEvent.LaunchedURL.toString(), message);
            } catch (JSONException e) {
                PHCrashReport.reportCrash(e, "PHInterstitialActivity - onURLOpenerFinished", PHCrashReport.Urgency.critical);
            } catch (Exception e2) {
                PHCrashReport.reportCrash(e2, "PHInterstitialActivity - onURLOpenerFinished", PHCrashReport.Urgency.critical);
            }
        }
        Bundle message2 = new Bundle();
        message2.putString(ContentRequestToInterstitialBridge.InterstitialEventArgument.CloseType.getKey(), PHContentRequest.PHDismissType.AdSelfDismiss.toString());
        ((ManipulatableContentDisplayer) this.contentDisplayer.get()).sendEventToRequester(ContentRequestToInterstitialBridge.InterstitialEvent.Dismissed.toString(), message2);
        ((ManipulatableContentDisplayer) this.contentDisplayer.get()).dismiss();
    }

    /* Debug info: failed to restart local var, previous not found, register: 9 */
    public void onURLOpenerFailed(PHURLOpener loader) {
        if (loader.getContentTemplateCallback() != null) {
            try {
                JSONObject response = new JSONObject();
                JSONObject error = new JSONObject();
                error.put("error", "1");
                response.put(FlurryFullscreenTakeoverActivity.EXTRA_KEY_URL, loader.getTargetURL());
                sendResponseToWebview(loader.getContentTemplateCallback(), response, error);
            } catch (JSONException e) {
                PHCrashReport.reportCrash(e, "PHInterstitialActivity - onURLOpenerFailed", PHCrashReport.Urgency.critical);
            } catch (Exception e2) {
                PHCrashReport.reportCrash(e2, "PHInterstitialActivity - onURLOpenerFailed", PHCrashReport.Urgency.critical);
            }
        }
        Bundle message = new Bundle();
        message.putString(ContentRequestToInterstitialBridge.InterstitialEventArgument.Error.getKey(), PHContentEnums.Error.CouldNotLoadURL.toString());
        ((ManipulatableContentDisplayer) this.contentDisplayer.get()).sendEventToRequester(ContentRequestToInterstitialBridge.InterstitialEvent.Failed.toString(), message);
    }
}
