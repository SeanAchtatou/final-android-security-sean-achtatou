package v2.com.playhaven.interstitial;

public class PHContentEnums {

    public enum Error {
        NoBoundingBox("The interstitial you requested was not able to be shown because it is missing required orientation data."),
        CouldNotLoadURL("Ad was unable to load URL"),
        FailedSubrequest("Sub-request started from ad unit failed"),
        NoResponseField("No 'response' field in JSON resposne");
        
        private String error;

        public String getMessage() {
            return this.error;
        }

        private Error(String message) {
            this.error = message;
        }
    }

    public enum IntentArgument {
        CustomCloseBtn("custom_close"),
        Content("init_content_contentview"),
        Tag("content_tag");
        
        private String key;

        public String getKey() {
            return this.key;
        }

        private IntentArgument(String key2) {
            this.key = key2;
        }
    }

    public enum Reward {
        IDKey("reward"),
        QuantityKey("quantity"),
        ReceiptKey("receipt"),
        SignatureKey("signature");
        
        private final String keyName;

        private Reward(String key) {
            this.keyName = key;
        }

        public String key() {
            return this.keyName;
        }
    }

    public enum Purchase {
        ProductIDKey("product"),
        NameKey("name"),
        ReceiptKey("receipt"),
        SignatureKey("signature"),
        CookieKey("cookie"),
        QuantityKey("quantity");
        
        private final String keyName;

        private Purchase(String key) {
            this.keyName = key;
        }

        public String key() {
            return this.keyName;
        }
    }
}
