package v2.com.playhaven.interstitial.jsbridge.handlers;

import android.os.Bundle;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import org.json.JSONArray;
import org.json.JSONObject;
import v2.com.playhaven.configuration.PHConfiguration;
import v2.com.playhaven.interstitial.PHContentEnums;
import v2.com.playhaven.interstitial.jsbridge.ManipulatableContentDisplayer;
import v2.com.playhaven.interstitial.requestbridge.bridges.ContentRequestToInterstitialBridge;
import v2.com.playhaven.model.PHContent;
import v2.com.playhaven.model.PHReward;
import v2.com.playhaven.requests.crashreport.PHCrashReport;
import v2.com.playhaven.utils.PHStringUtil;

public class RewardHandler extends AbstractHandler {
    public void handle(JSONObject jsonPayload) {
        JSONArray rewards;
        if (jsonPayload != null) {
            try {
                if (jsonPayload.isNull("rewards")) {
                    rewards = new JSONArray();
                } else {
                    rewards = jsonPayload.optJSONArray("rewards");
                }
                for (int i = 0; i < rewards.length(); i++) {
                    JSONObject reward = rewards.optJSONObject(i);
                    if (validateReward(reward)) {
                        PHReward r = new PHReward();
                        r.name = reward.optString(PHContentEnums.Reward.IDKey.key(), "");
                        r.quantity = reward.optInt(PHContentEnums.Reward.QuantityKey.key(), -1);
                        r.receipt = reward.optString(PHContentEnums.Reward.ReceiptKey.key(), "");
                        Bundle args = new Bundle();
                        args.putParcelable(ContentRequestToInterstitialBridge.InterstitialEventArgument.Reward.getKey(), r);
                        ((ManipulatableContentDisplayer) this.contentDisplayer.get()).sendEventToRequester(ContentRequestToInterstitialBridge.InterstitialEvent.UnlockedReward.toString(), args);
                    }
                }
                sendResponseToWebview(this.bridge.getCurrentQueryVar("callback"), null, null);
            } catch (Exception e) {
                PHCrashReport.reportCrash(e, "PHInterstitialActivity - handleRewards", PHCrashReport.Urgency.low);
            }
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 11 */
    private boolean validateReward(JSONObject data) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        if (JSONObject.NULL.equals(data) || data.length() == 0) {
            return false;
        }
        String reward = data.optString(PHContentEnums.Reward.IDKey.key(), "");
        String quantity = data.optString(PHContentEnums.Reward.QuantityKey.key(), "");
        String receipt = data.optString(PHContentEnums.Reward.ReceiptKey.key(), "");
        String signature = data.optString(PHContentEnums.Reward.SignatureKey.key(), "");
        String device_id = ((ManipulatableContentDisplayer) this.contentDisplayer.get()).getDeviceID();
        if (device_id == null) {
            device_id = PHContent.PARCEL_NULL;
        }
        new PHConfiguration();
        String generatedSig = PHStringUtil.hexDigest(String.format("%s:%s:%s:%s:%s", reward, quantity, device_id, receipt, ((ManipulatableContentDisplayer) this.contentDisplayer.get()).getSecret()));
        PHStringUtil.log("Checking reward signature:  " + signature + " against: " + generatedSig);
        return generatedSig.equalsIgnoreCase(signature);
    }
}
