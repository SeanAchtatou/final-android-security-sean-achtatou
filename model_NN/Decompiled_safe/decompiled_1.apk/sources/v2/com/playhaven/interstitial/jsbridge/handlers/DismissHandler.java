package v2.com.playhaven.interstitial.jsbridge.handlers;

import org.json.JSONObject;
import v2.com.playhaven.interstitial.jsbridge.ManipulatableContentDisplayer;

public class DismissHandler extends LaunchHandler {
    public void handle(JSONObject jsonPayload) {
        ((ManipulatableContentDisplayer) this.contentDisplayer.get()).openURL(jsonPayload != null ? jsonPayload.optString("ping", "") : null, this);
    }
}
