package v2.com.playhaven.interstitial.requestbridge.base;

import android.content.Context;
import v2.com.playhaven.model.PHContent;

public interface ContentRequester {
    PHContent getContent();

    Context getContext();

    String getTag();

    void onTagChanged(String str);
}
