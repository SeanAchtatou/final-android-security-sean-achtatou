package v2.com.playhaven.interstitial.jsbridge.handlers;

import java.lang.ref.WeakReference;
import org.json.JSONException;
import org.json.JSONObject;
import v2.com.playhaven.interstitial.jsbridge.ManipulatableContentDisplayer;
import v2.com.playhaven.interstitial.jsbridge.PHJSBridge;
import v2.com.playhaven.model.PHContent;

public abstract class AbstractHandler {
    protected PHJSBridge bridge;
    protected WeakReference<ManipulatableContentDisplayer> contentDisplayer;

    /* access modifiers changed from: protected */
    public abstract void handle(JSONObject jSONObject);

    public final void handle() {
        if (!doesntHaveContentDisplayer()) {
            handle(getRequestContext());
        }
    }

    public void attachBridge(PHJSBridge bridge2) {
        this.bridge = bridge2;
    }

    public void attachContentDisplayer(ManipulatableContentDisplayer contentDisplayer2) {
        this.contentDisplayer = new WeakReference<>(contentDisplayer2);
    }

    /* access modifiers changed from: protected */
    public boolean doesntHaveContentDisplayer() {
        return this.contentDisplayer == null || this.contentDisplayer.get() == null;
    }

    private JSONObject getRequestContext() {
        JSONObject context;
        try {
            String contextStr = this.bridge.getCurrentQueryVar("context");
            if (contextStr == null || contextStr.equals("undefined") || contextStr.equals(PHContent.PARCEL_NULL)) {
                context = new JSONObject();
            } else {
                context = new JSONObject(contextStr);
            }
            if (!JSONObject.NULL.equals(context) && context.length() > 0) {
                return context;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void sendResponseToWebview(String callback, JSONObject payload, JSONObject error) {
        if (this.bridge != null) {
            this.bridge.sendMessageToWebview(callback, payload, error);
        }
    }
}
