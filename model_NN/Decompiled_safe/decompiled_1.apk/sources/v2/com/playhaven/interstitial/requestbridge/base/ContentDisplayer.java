package v2.com.playhaven.interstitial.requestbridge.base;

import android.content.Context;
import v2.com.playhaven.model.PHContent;

public interface ContentDisplayer {
    void dismiss();

    PHContent getContent();

    Context getContext();

    String getTag();

    void onTagChanged(String str);
}
