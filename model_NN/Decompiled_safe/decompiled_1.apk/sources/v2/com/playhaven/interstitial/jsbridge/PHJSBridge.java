package v2.com.playhaven.interstitial.jsbridge;

import android.net.Uri;
import android.webkit.WebView;
import java.lang.ref.WeakReference;
import java.util.Hashtable;
import org.json.JSONObject;
import v2.com.playhaven.interstitial.jsbridge.handlers.AbstractHandler;
import v2.com.playhaven.model.PHContent;
import v2.com.playhaven.utils.PHStringUtil;

public class PHJSBridge {
    private static final String JAVASCRIPT_CALLBACK_TEMPLATE = "javascript:PlayHaven.nativeAPI.callback(\"%s\", %s, %s)";
    protected WeakReference<ManipulatableContentDisplayer> contentDisplayer;
    private Uri mCurUrl;
    private Hashtable<String, AbstractHandler> routers = new Hashtable<>();
    private WebView webview;

    public PHJSBridge(ManipulatableContentDisplayer contentDisplayer2) {
        this.contentDisplayer = new WeakReference<>(contentDisplayer2);
    }

    public void attachWebview(WebView webview2) {
        this.webview = webview2;
    }

    public boolean hasWebviewAttached() {
        return this.webview != null;
    }

    public void loadUrlInWebView(String url) {
        if (this.webview != null) {
            this.webview.loadUrl(url);
        }
    }

    public void runJavascript(String js) {
        if (this.webview != null) {
            this.webview.loadUrl("javascript:" + js);
        }
    }

    public String getCurrentURL() {
        String uri;
        synchronized (PHJSBridge.class) {
            uri = this.mCurUrl != null ? this.mCurUrl.toString() : null;
        }
        return uri;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getCurrentQueryVar(java.lang.String r5) {
        /*
            r4 = this;
            r3 = 0
            java.lang.Class<v2.com.playhaven.interstitial.jsbridge.PHJSBridge> r1 = v2.com.playhaven.interstitial.jsbridge.PHJSBridge.class
            monitor-enter(r1)
            android.net.Uri r2 = r4.mCurUrl     // Catch:{ all -> 0x0029 }
            if (r2 != 0) goto L_0x000b
            monitor-exit(r1)     // Catch:{ all -> 0x0029 }
            r1 = r3
        L_0x000a:
            return r1
        L_0x000b:
            android.net.Uri r2 = r4.mCurUrl     // Catch:{ all -> 0x0029 }
            java.lang.String r0 = r2.getQueryParameter(r5)     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x0023
            java.lang.String r2 = ""
            boolean r2 = r0.equals(r2)     // Catch:{ all -> 0x0029 }
            if (r2 != 0) goto L_0x0023
            java.lang.String r2 = "null"
            boolean r2 = r0.equals(r2)     // Catch:{ all -> 0x0029 }
            if (r2 == 0) goto L_0x0026
        L_0x0023:
            monitor-exit(r1)     // Catch:{ all -> 0x0029 }
            r1 = r3
            goto L_0x000a
        L_0x0026:
            monitor-exit(r1)     // Catch:{ all -> 0x0029 }
            r1 = r0
            goto L_0x000a
        L_0x0029:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0029 }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: v2.com.playhaven.interstitial.jsbridge.PHJSBridge.getCurrentQueryVar(java.lang.String):java.lang.String");
    }

    public void addRoute(String route, AbstractHandler handler) {
        handler.attachBridge(this);
        handler.attachContentDisplayer(this.contentDisplayer.get());
        this.routers.put(route, handler);
    }

    private String stripQuery(String url) {
        return url.substring(0, url.indexOf("?") > 0 ? url.indexOf("?") : url.length());
    }

    public boolean hasRoute(String url) {
        PHStringUtil.log("Asking about route: " + url);
        return this.routers.containsKey(stripQuery(url));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void route(java.lang.String r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            android.net.Uri r2 = android.net.Uri.parse(r4)     // Catch:{ all -> 0x0020 }
            r3.mCurUrl = r2     // Catch:{ all -> 0x0020 }
            android.net.Uri r2 = r3.mCurUrl     // Catch:{ all -> 0x0020 }
            if (r2 != 0) goto L_0x000d
            monitor-exit(r3)     // Catch:{ all -> 0x0020 }
        L_0x000c:
            return
        L_0x000d:
            java.lang.String r1 = r3.stripQuery(r4)     // Catch:{ all -> 0x0020 }
            java.util.Hashtable<java.lang.String, v2.com.playhaven.interstitial.jsbridge.handlers.AbstractHandler> r2 = r3.routers     // Catch:{ all -> 0x0020 }
            java.lang.Object r0 = r2.get(r1)     // Catch:{ all -> 0x0020 }
            v2.com.playhaven.interstitial.jsbridge.handlers.AbstractHandler r0 = (v2.com.playhaven.interstitial.jsbridge.handlers.AbstractHandler) r0     // Catch:{ all -> 0x0020 }
            if (r0 == 0) goto L_0x001e
            r0.handle()     // Catch:{ all -> 0x0020 }
        L_0x001e:
            monitor-exit(r3)     // Catch:{ all -> 0x0020 }
            goto L_0x000c
        L_0x0020:
            r2 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0020 }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: v2.com.playhaven.interstitial.jsbridge.PHJSBridge.route(java.lang.String):void");
    }

    public void sendMessageToWebview(String callback, JSONObject payload, JSONObject error) {
        if (hasWebviewAttached()) {
            Object[] objArr = new Object[3];
            objArr[0] = callback != null ? callback : PHContent.PARCEL_NULL;
            objArr[1] = payload != null ? payload.toString() : PHContent.PARCEL_NULL;
            objArr[2] = error != null ? error.toString() : PHContent.PARCEL_NULL;
            String callbackCommand = String.format(JAVASCRIPT_CALLBACK_TEMPLATE, objArr);
            PHStringUtil.log("sending javascript callback to WebView: '" + callbackCommand);
            this.webview.loadUrl(callbackCommand);
        }
    }

    public void clearCurrentURL() {
        this.mCurUrl = null;
    }

    public void clearRoutes() {
        this.routers.clear();
    }
}
