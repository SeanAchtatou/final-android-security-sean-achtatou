package v2.com.playhaven.interstitial.jsbridge.handlers;

import org.json.JSONObject;
import v2.com.playhaven.configuration.PHConfiguration;
import v2.com.playhaven.interstitial.jsbridge.ManipulatableContentDisplayer;

public class LoadContextHandler extends AbstractHandler {
    private static final String SET_PROTOCOL_JAVASCRIPT = "window.PlayHavenDispatchProtocolVersion = %d";

    public void handle(JSONObject jsonObject) {
        this.bridge.runJavascript(String.format(SET_PROTOCOL_JAVASCRIPT, Integer.valueOf(new PHConfiguration().getJSBridgeProtocolVersion())));
        sendResponseToWebview(this.bridge.getCurrentQueryVar("callback"), ((ManipulatableContentDisplayer) this.contentDisplayer.get()).getContent().context, null);
    }
}
