package v2.com.playhaven.interstitial;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.view.MotionEvent;
import android.widget.RelativeLayout;
import java.util.HashMap;
import java.util.Random;
import org.json.JSONException;
import org.json.JSONObject;
import v2.com.playhaven.cache.PHCache;
import v2.com.playhaven.configuration.PHConfiguration;
import v2.com.playhaven.interstitial.PHContentEnums;
import v2.com.playhaven.interstitial.jsbridge.ManipulatableContentDisplayer;
import v2.com.playhaven.interstitial.jsbridge.PHJSBridge;
import v2.com.playhaven.interstitial.jsbridge.handlers.CloseButtonHandler;
import v2.com.playhaven.interstitial.jsbridge.handlers.DismissHandler;
import v2.com.playhaven.interstitial.jsbridge.handlers.LaunchHandler;
import v2.com.playhaven.interstitial.jsbridge.handlers.LoadContextHandler;
import v2.com.playhaven.interstitial.jsbridge.handlers.PurchaseHandler;
import v2.com.playhaven.interstitial.jsbridge.handlers.RewardHandler;
import v2.com.playhaven.interstitial.jsbridge.handlers.SubrequestHandler;
import v2.com.playhaven.interstitial.requestbridge.BridgeManager;
import v2.com.playhaven.interstitial.requestbridge.base.ContentDisplayer;
import v2.com.playhaven.interstitial.requestbridge.bridges.ContentRequestToInterstitialBridge;
import v2.com.playhaven.model.PHContent;
import v2.com.playhaven.model.PHPurchase;
import v2.com.playhaven.requests.content.PHContentRequest;
import v2.com.playhaven.requests.content.PHSubContentRequest;
import v2.com.playhaven.requests.crashreport.PHCrashReport;
import v2.com.playhaven.utils.PHStringUtil;
import v2.com.playhaven.utils.PHURLOpener;
import v2.com.playhaven.views.interstitial.PHCloseButton;
import v2.com.playhaven.views.interstitial.PHContentView;

public class PHInterstitialActivity extends Activity implements ContentDisplayer, ManipulatableContentDisplayer, PHContentView.Listener {
    public static final String BROADCAST_INTENT = "PHInterstitialActivityBroadcast";
    private static final String SUB_INTERSTITIAL_SUFFIX = "SubInterstitial";
    private static PHConfiguration config;
    public PHContent content;
    private PHContentView contentView;
    private HashMap<String, Bitmap> customCloseStates = new HashMap<>();
    private boolean isBackBtnCancelable;
    private boolean isTouchCancelable;
    private PHJSBridge jsBridge;
    private String tag;

    private void initCache() {
        if (config.getShouldPrecache(this)) {
            synchronized (PHCache.class) {
                PHCache.installCache(this);
            }
        }
    }

    public void onBackPressed() {
        if (getIsBackBtnCancelable()) {
            PHStringUtil.log("The interstitial unit was dismissed by the user using back button");
            notifyContentRequestOfClose(PHContentRequest.PHDismissType.CloseButton);
            super.onBackPressed();
        }
    }

    private void notifyContentRequestOfClose(PHContentRequest.PHDismissType type) {
        Bundle args = new Bundle();
        args.putString(ContentRequestToInterstitialBridge.InterstitialEventArgument.CloseType.getKey(), type.toString());
        BridgeManager.sendMessageFromDisplayer(this.tag, ContentRequestToInterstitialBridge.InterstitialEvent.Dismissed.toString(), args, this);
    }

    private void notifyContentRequestOfFailure(PHContentEnums.Error error) {
        Bundle args = new Bundle();
        args.putString(ContentRequestToInterstitialBridge.InterstitialEventArgument.Error.getKey(), error.toString());
        BridgeManager.sendMessageFromDisplayer(this.tag, ContentRequestToInterstitialBridge.InterstitialEvent.Failed.toString(), args, this);
    }

    public void onPause() {
        super.onPause();
        if (!isFinishing()) {
            PHStringUtil.log("The interstitial activity was backgrounded and dismissed itself");
            notifyContentRequestOfClose(PHContentRequest.PHDismissType.ApplicationBackgrounded);
            dismiss();
        }
    }

    public void onStart() {
        super.onStart();
        initCache();
        BitmapDrawable active = null;
        BitmapDrawable inactive = null;
        try {
            String active_key = PHCloseButton.CloseButtonState.Up.name();
            String inactive_key = PHCloseButton.CloseButtonState.Up.name();
            if (this.customCloseStates.get(active_key) != null) {
                active = new BitmapDrawable(getResources(), this.customCloseStates.get(active_key));
            }
            if (this.customCloseStates.get(inactive_key) != null) {
                inactive = new BitmapDrawable(getResources(), this.customCloseStates.get(inactive_key));
            }
            this.contentView = new PHContentView(this, this, this.content, this, this.jsBridge, active, inactive);
            setContentView(this.contentView, new RelativeLayout.LayoutParams(-1, -1));
            fitInterstitialWindowToContent();
        } catch (Exception e) {
            PHCrashReport.reportCrash(e, "PHInterstitialActivity - onStart()", PHCrashReport.Urgency.critical);
        }
    }

    public void onStop() {
        super.onStop();
    }

    public void onPurchaseResolved(PHPurchase resolvedPurchase) {
        try {
            JSONObject response = new JSONObject();
            response.put("resolution", resolvedPurchase.resolution.getType());
            this.jsBridge.sendMessageToWebview(resolvedPurchase.callback, response, null);
        } catch (JSONException e) {
            PHCrashReport.reportCrash(e, "PHInterstitialActivity - BroadcastReceiver - onReceive", PHCrashReport.Urgency.critical);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        BridgeManager.closeBridge(this.tag);
        this.contentView.cleanup();
    }

    private void setupWebviewJSBridge() {
        this.jsBridge = new PHJSBridge(this);
        this.jsBridge.addRoute("ph://dismiss", new DismissHandler());
        this.jsBridge.addRoute("ph://launch", new LaunchHandler());
        this.jsBridge.addRoute("ph://loadContext", new LoadContextHandler());
        this.jsBridge.addRoute("ph://reward", new RewardHandler());
        this.jsBridge.addRoute("ph://purchase", new PurchaseHandler());
        this.jsBridge.addRoute("ph://subcontent", new SubrequestHandler());
        this.jsBridge.addRoute("ph://closeButton", new CloseButtonHandler());
    }

    public void onAttachedToWindow() {
        try {
            super.onAttachedToWindow();
            if (!contentHasFrame()) {
                notifyContentRequestOfFailure(PHContentEnums.Error.NoBoundingBox);
            } else {
                getWindow().setBackgroundDrawable(new ColorDrawable(0));
            }
        } catch (Exception e) {
            PHCrashReport.reportCrash(e, "PHInterstitialActivity - onAttachedToWindow()", PHCrashReport.Urgency.critical);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            config = new PHConfiguration();
            this.content = (PHContent) getIntent().getParcelableExtra(PHContentEnums.IntentArgument.Content.getKey());
            if (this.content.isEmpty()) {
                dismiss();
            }
            this.tag = getIntent().getStringExtra(PHContentEnums.IntentArgument.Tag.getKey());
            BridgeManager.attachDisplayer(this.tag, this);
            if (getIntent().hasExtra(PHContentEnums.IntentArgument.CustomCloseBtn.getKey())) {
                this.customCloseStates = (HashMap) getIntent().getSerializableExtra(PHContentEnums.IntentArgument.CustomCloseBtn.getKey());
            }
            setCancelable(false, true);
            getWindow().requestFeature(1);
            setupWebviewJSBridge();
        } catch (Exception e) {
            PHCrashReport.reportCrash(e, PHCrashReport.Urgency.critical);
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        try {
            if (event.getAction() == 4) {
                if (getIsTouchCancelable()) {
                    notifyContentRequestOfClose(PHContentRequest.PHDismissType.CloseButton);
                    dismiss();
                }
                return true;
            }
        } catch (Exception e) {
            PHCrashReport.reportCrash(e, "PHInterstitialActivity - onTouchEvent()", PHCrashReport.Urgency.critical);
        }
        return false;
    }

    public String getTag() {
        return this.tag;
    }

    public PHContent getContent() {
        return this.content;
    }

    public void enableClosable() {
        this.contentView.showCloseButton();
    }

    public void disableClosable() {
        this.contentView.hideCloseButton();
    }

    public boolean isClosable() {
        return this.contentView.closeButtonIsShowing();
    }

    public void launchNestedContentDisplayer(PHContent content2) {
        String subcontent_tag = this.tag + SUB_INTERSTITIAL_SUFFIX + new Random(System.currentTimeMillis()).nextInt();
        BridgeManager.transferBridge(this.tag, subcontent_tag);
        PHContentRequest.displayInterstitialActivity(content2, this, null, subcontent_tag);
    }

    public String getSecret() {
        return config.getSecret(this);
    }

    public String getDeviceID() {
        return Settings.System.getString(getContentResolver(), "android_id");
    }

    public void openURL(String url, PHURLOpener.Listener listener) {
        PHURLOpener opener = new PHURLOpener(this, listener);
        opener.setShouldOpenFinalURL(false);
        opener.open(url);
    }

    public void launchURL(String url, PHURLOpener.Listener listener) {
        PHURLOpener opener = new PHURLOpener(this, listener);
        opener.setShouldOpenFinalURL(true);
        opener.open(url);
    }

    public void launchSubRequest(PHSubContentRequest request) {
        request.send(this);
    }

    public void sendEventToRequester(String event, Bundle message) {
        BridgeManager.sendMessageFromDisplayer(this.tag, event, message, this);
    }

    public Context getContext() {
        return getApplicationContext();
    }

    public void onTagChanged(String new_tag) {
        if (!new_tag.contains(SUB_INTERSTITIAL_SUFFIX)) {
            this.tag = new_tag;
        }
    }

    public void dismiss() {
        PHContentRequest.updateLastDismissedAdTime();
        this.contentView.close();
        super.finish();
    }

    public void setCancelable(boolean touchCancel, boolean backCancel) {
        this.isTouchCancelable = touchCancel;
        this.isBackBtnCancelable = backCancel;
    }

    public boolean setIsBackBtnCancelable(boolean backCancel) {
        this.isBackBtnCancelable = backCancel;
        return backCancel;
    }

    public boolean getIsTouchCancelable() {
        return this.isTouchCancelable;
    }

    public boolean getIsBackBtnCancelable() {
        return this.isBackBtnCancelable;
    }

    public PHContentView getRootView() {
        return this.contentView;
    }

    public void setContent(PHContent content2) {
        if (content2 != null) {
            this.content = content2;
        }
    }

    private boolean contentHasFrame() {
        if (this.content == null) {
            return false;
        }
        RectF contentFrame = this.content.getFrame(getResources().getConfiguration().orientation);
        return (((double) contentFrame.width()) == 0.0d || ((double) contentFrame.height()) == 0.0d) ? false : true;
    }

    private void fitInterstitialWindowToContent() {
        RectF contentFrame = this.content.getFrame(getResources().getConfiguration().orientation);
        if (contentFrame.right == 2.14748365E9f && contentFrame.bottom == 2.14748365E9f) {
            contentFrame.right = -1.0f;
            contentFrame.bottom = -1.0f;
            contentFrame.top = 0.0f;
            contentFrame.left = 0.0f;
            getWindow().setFlags(1024, 1024);
            getWindow().clearFlags(2048);
        } else {
            getWindow().clearFlags(1024);
            getWindow().addFlags(2048);
        }
        getWindow().setLayout((int) contentFrame.width(), (int) contentFrame.height());
    }

    public void onClose(PHContentView contentView2) {
        dismiss();
        notifyContentRequestOfClose(PHContentRequest.PHDismissType.CloseButton);
    }
}
