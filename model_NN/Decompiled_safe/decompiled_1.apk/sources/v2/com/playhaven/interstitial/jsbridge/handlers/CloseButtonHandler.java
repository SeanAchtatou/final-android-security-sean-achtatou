package v2.com.playhaven.interstitial.jsbridge.handlers;

import org.json.JSONObject;
import v2.com.playhaven.interstitial.jsbridge.ManipulatableContentDisplayer;
import v2.com.playhaven.requests.crashreport.PHCrashReport;
import v2.com.playhaven.utils.PHStringUtil;

public class CloseButtonHandler extends AbstractHandler {
    public void handle(JSONObject jsonPayload) {
        if (jsonPayload != null) {
            try {
                String shouldHide = jsonPayload.optString("hidden");
                PHStringUtil.log("WebView asks us to hide close button: " + shouldHide);
                if (!JSONObject.NULL.equals(shouldHide) && shouldHide.length() > 0) {
                    if (Boolean.parseBoolean(shouldHide)) {
                        ((ManipulatableContentDisplayer) this.contentDisplayer.get()).disableClosable();
                    } else {
                        ((ManipulatableContentDisplayer) this.contentDisplayer.get()).enableClosable();
                    }
                }
                JSONObject response = new JSONObject();
                response.put("hidden", ((ManipulatableContentDisplayer) this.contentDisplayer.get()).isClosable() ? "false" : "true");
                sendResponseToWebview(this.bridge.getCurrentQueryVar("callback"), response, null);
            } catch (Exception e) {
                PHCrashReport.reportCrash(e, "PHInterstitialActivity - handleCloseButton", PHCrashReport.Urgency.critical);
            }
        }
    }
}
