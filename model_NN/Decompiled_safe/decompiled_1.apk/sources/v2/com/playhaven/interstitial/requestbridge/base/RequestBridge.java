package v2.com.playhaven.interstitial.requestbridge.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import v2.com.playhaven.utils.PHStringUtil;

public abstract class RequestBridge {
    public static final String BROADCAST_METADATA_KEY = "v2.com.playhaven.notification";
    private ContentDisplayer displayer;
    private BroadcastReceiver displayerReceiver;
    private ContentRequester requester;
    private BroadcastReceiver requesterReceiver;
    /* access modifiers changed from: private */
    public String tag;

    public abstract void cleanup();

    public abstract String getDisplayerIntentFilter();

    public abstract String getRequesterIntentFilter();

    public abstract void onDisplayerSentMessage(String str, Bundle bundle);

    public abstract void onRequesterSentMessage(String str, Bundle bundle);

    public enum Message {
        Event("event_contentview"),
        Tag("content_tag");
        
        private String key;

        public String getKey() {
            return this.key;
        }

        private Message(String key2) {
            this.key = key2;
        }
    }

    public RequestBridge(String tag2) {
        this.tag = tag2;
    }

    public void attachRequester(ContentRequester requester2) {
        this.requester = requester2;
        registerRequesterReceiver();
        onRequesterAttached(requester2);
    }

    public void detachRequester() {
        if (this.requester != null) {
            this.requester.getContext().unregisterReceiver(this.requesterReceiver);
        }
        this.requester = null;
    }

    public ContentRequester getRequester() {
        return this.requester;
    }

    public void attachDisplayer(ContentDisplayer displayer2) {
        this.displayer = displayer2;
        registerDisplayerReceiver();
        onDisplayerAttached(displayer2);
    }

    public void detachDisplayer() {
        if (this.displayer != null) {
            this.displayer.getContext().unregisterReceiver(this.displayerReceiver);
        }
        this.displayer = null;
    }

    public ContentDisplayer getDisplayer() {
        return this.displayer;
    }

    public void close() {
        detachRequester();
        detachDisplayer();
        cleanup();
    }

    private void registerDisplayerReceiver() {
        if (this.displayer.getContext() != null) {
            this.displayerReceiver = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    Bundle md = intent.getBundleExtra(RequestBridge.BROADCAST_METADATA_KEY);
                    String event = md.getString(Message.Event.getKey());
                    String tag = md.getString(Message.Tag.getKey());
                    if (tag != null && tag.equals(RequestBridge.this.tag)) {
                        PHStringUtil.log("Receiving message from requester: " + event);
                        RequestBridge.this.onRequesterSentMessage(event, md);
                    }
                }
            };
            this.displayer.getContext().registerReceiver(this.displayerReceiver, new IntentFilter(getDisplayerIntentFilter()));
        }
    }

    private void registerRequesterReceiver() {
        if (this.requester.getContext() != null) {
            this.requesterReceiver = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    Bundle md = intent.getBundleExtra(RequestBridge.BROADCAST_METADATA_KEY);
                    String event = md.getString(Message.Event.getKey());
                    String tag = md.getString(Message.Tag.getKey());
                    if (tag != null && tag.equals(RequestBridge.this.tag)) {
                        PHStringUtil.log("Receiving message from displayer: " + event);
                        RequestBridge.this.onDisplayerSentMessage(event, md);
                    }
                }
            };
            this.requester.getContext().registerReceiver(this.requesterReceiver, new IntentFilter(getRequesterIntentFilter()));
        }
    }

    public void onTagChanged(String new_tag) {
        this.tag = new_tag;
        if (this.requester != null) {
            this.requester.onTagChanged(new_tag);
        }
        if (this.displayer != null) {
            this.displayer.onTagChanged(new_tag);
        }
    }

    public String getTag() {
        return this.tag;
    }

    public void onRequesterAttached(ContentRequester requester2) {
    }

    public void onDisplayerAttached(ContentDisplayer displayer2) {
    }
}
