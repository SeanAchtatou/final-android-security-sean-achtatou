package v2.com.playhaven.interstitial.jsbridge;

import android.os.Bundle;
import v2.com.playhaven.model.PHContent;
import v2.com.playhaven.requests.content.PHSubContentRequest;
import v2.com.playhaven.utils.PHURLOpener;

public interface ManipulatableContentDisplayer {
    void disableClosable();

    void dismiss();

    void enableClosable();

    PHContent getContent();

    String getDeviceID();

    String getSecret();

    String getTag();

    boolean isClosable();

    void launchNestedContentDisplayer(PHContent pHContent);

    void launchSubRequest(PHSubContentRequest pHSubContentRequest);

    void launchURL(String str, PHURLOpener.Listener listener);

    void openURL(String str, PHURLOpener.Listener listener);

    void sendEventToRequester(String str, Bundle bundle);
}
