package v2.com.playhaven.interstitial.requestbridge.bridges;

import android.os.Bundle;
import java.lang.ref.WeakReference;
import v2.com.playhaven.interstitial.PHInterstitialActivity;
import v2.com.playhaven.interstitial.requestbridge.base.ContentDisplayer;
import v2.com.playhaven.interstitial.requestbridge.base.ContentRequester;
import v2.com.playhaven.interstitial.requestbridge.base.RequestBridge;
import v2.com.playhaven.listeners.PHContentRequestListener;
import v2.com.playhaven.listeners.PHPurchaseListener;
import v2.com.playhaven.listeners.PHRewardListener;
import v2.com.playhaven.model.PHContent;
import v2.com.playhaven.model.PHError;
import v2.com.playhaven.model.PHPurchase;
import v2.com.playhaven.model.PHReward;
import v2.com.playhaven.requests.content.PHContentRequest;
import v2.com.playhaven.utils.PHStringUtil;

public class ContentRequestToInterstitialBridge extends RequestBridge {
    private WeakReference<PHContentRequest> contentRequest;
    private PHContentRequestListener content_listener;
    private WeakReference<PHInterstitialActivity> interstitialActivity;
    private PHPurchaseListener purchase_listener;
    private PHRewardListener reward_listener;

    public enum InterstitialEvent {
        Loaded,
        Dismissed,
        Failed,
        UnlockedReward,
        MadePurchase,
        PurchaseResolved,
        SentSubrequest,
        ReceivedWebviewDispatch,
        LaunchedURL
    }

    public enum InterstitialEventArgument {
        CloseType("closetype_contentview"),
        Content("content_contentview"),
        Error("error_contentview"),
        Reward("reward_contentview"),
        Purchase("purchase_contentview"),
        Dispatch("dispatch_contentview"),
        LaunchURL("launchurl_contentview");
        
        private String key;

        public String getKey() {
            return this.key;
        }

        private InterstitialEventArgument(String key2) {
            this.key = key2;
        }
    }

    public ContentRequestToInterstitialBridge(String tag) {
        super(tag);
    }

    public void onRequesterAttached(ContentRequester requester) {
        this.contentRequest = new WeakReference<>((PHContentRequest) requester);
    }

    public void onDisplayerAttached(ContentDisplayer displayer) {
        this.interstitialActivity = new WeakReference<>((PHInterstitialActivity) displayer);
    }

    public void attachRewardListener(PHRewardListener listener) {
        this.reward_listener = listener;
    }

    public void attachPurchaseListener(PHPurchaseListener listener) {
        this.purchase_listener = listener;
    }

    public void attachContentListener(PHContentRequestListener listener) {
        this.content_listener = listener;
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public void onRequesterSentMessage(String event, Bundle messageData) {
        if (event != null && this.interstitialActivity != null && this.interstitialActivity.get() != null && InterstitialEvent.valueOf(event) == InterstitialEvent.PurchaseResolved) {
            PHPurchase purchase = (PHPurchase) messageData.getParcelable(InterstitialEventArgument.Purchase.getKey());
            PHStringUtil.log("Displayer received purchase resolution: " + purchase.resolution);
            this.interstitialActivity.get().onPurchaseResolved(purchase);
        }
    }

    public String getRequesterIntentFilter() {
        return "v2.com.playhaven.interstitial.ContentRequesterEvent";
    }

    public String getDisplayerIntentFilter() {
        return "v2.com.playhaven.interstitial.ContentDisplayerEvent";
    }

    public void cleanup() {
        this.content_listener = null;
        this.reward_listener = null;
        this.purchase_listener = null;
    }

    /* Debug info: failed to restart local var, previous not found, register: 9 */
    public void onDisplayerSentMessage(String event, Bundle message) {
        if (this.contentRequest != null && this.contentRequest.get() != null) {
            InterstitialEvent parsedEvent = InterstitialEvent.valueOf(event);
            PHStringUtil.log("ContentRequestToInterstitial bridge handling: " + parsedEvent.name());
            PHStringUtil.log("ContentListener: " + this.content_listener);
            PHStringUtil.log("RewardListener: " + this.reward_listener);
            PHStringUtil.log("PurchaseListener: " + this.purchase_listener);
            switch (parsedEvent) {
                case Loaded:
                    PHContent content = (PHContent) message.getParcelable(InterstitialEventArgument.Content.getKey());
                    if (this.content_listener != null && content != null) {
                        this.content_listener.onDisplayedContent(this.contentRequest.get(), content);
                        return;
                    }
                    return;
                case Dismissed:
                    String dismissTypeStr = message.getString(InterstitialEventArgument.CloseType.getKey());
                    if (this.content_listener != null && dismissTypeStr != null) {
                        this.content_listener.onDismissedContent(this.contentRequest.get(), PHContentRequest.PHDismissType.valueOf(dismissTypeStr));
                        return;
                    }
                    return;
                case Failed:
                    String error = message.getString(InterstitialEventArgument.Error.getKey());
                    if (this.content_listener != null && error != null) {
                        this.content_listener.onFailedToDisplayContent(this.contentRequest.get(), new PHError(error));
                        return;
                    }
                    return;
                case UnlockedReward:
                    PHReward reward = (PHReward) message.getParcelable(InterstitialEventArgument.Reward.getKey());
                    if (this.reward_listener != null && reward != null) {
                        this.reward_listener.onUnlockedReward(this.contentRequest.get(), reward);
                        return;
                    }
                    return;
                case MadePurchase:
                    PHPurchase purchase = (PHPurchase) message.getParcelable(InterstitialEventArgument.Purchase.getKey());
                    if (this.purchase_listener != null && purchase != null) {
                        this.purchase_listener.onMadePurchase(this.contentRequest.get(), purchase);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }
}
