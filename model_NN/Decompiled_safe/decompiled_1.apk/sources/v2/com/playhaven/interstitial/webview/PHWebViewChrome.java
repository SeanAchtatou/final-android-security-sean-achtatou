package v2.com.playhaven.interstitial.webview;

import android.net.Uri;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import v2.com.playhaven.requests.crashreport.PHCrashReport;
import v2.com.playhaven.utils.PHStringUtil;

public class PHWebViewChrome extends WebChromeClient {
    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        String fname = "(no file)";
        try {
            if (consoleMessage.sourceId() != null) {
                fname = Uri.parse(consoleMessage.sourceId()).getLastPathSegment();
            }
            PHStringUtil.log("Javascript: " + consoleMessage.message() + " at line (" + fname + ") :" + consoleMessage.lineNumber());
            return true;
        } catch (Exception e) {
            PHCrashReport.reportCrash(e, "PHWebViewChrome - onConsoleMessage", PHCrashReport.Urgency.low);
            return true;
        }
    }
}
