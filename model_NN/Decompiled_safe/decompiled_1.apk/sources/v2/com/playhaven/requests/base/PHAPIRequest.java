package v2.com.playhaven.requests.base;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.DisplayMetrics;
import com.flurry.android.AdCreative;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;
import v2.com.playhaven.configuration.PHConfiguration;
import v2.com.playhaven.listeners.PHHttpRequestListener;
import v2.com.playhaven.model.PHContent;
import v2.com.playhaven.model.PHError;
import v2.com.playhaven.requests.base.PHAsyncRequest;
import v2.com.playhaven.requests.crashreport.PHCrashReport;
import v2.com.playhaven.utils.PHConnectionUtils;
import v2.com.playhaven.utils.PHStringUtil;

public class PHAPIRequest implements PHHttpRequestListener {
    public static final String API_CACHE_SUBDIR = "apicache";
    public static final Integer APP_CACHE_VERSION = 100;
    public static final Integer PRECACHE_FILE_KEY_INDEX = 0;
    private final String SESSION_PREFERENCES = "com_playhaven_sdk_session";
    private Hashtable<String, String> additionalParams;
    private PHConfiguration config = new PHConfiguration();
    private PHAsyncRequest conn;
    protected String fullUrl;
    private PHError lastError;
    private JSONObject lastResponse;
    private int requestTag;
    public boolean shouldComplainAboutNonOverridden = true;
    private HashMap<String, String> signedParams;
    private String urlPath;

    private void checkTokenAndSecret(String token, String secret) {
        if (token == null || secret == null || token.length() == 0 || secret.length() == 0) {
            throw new IllegalArgumentException("You must provide a token and secret from the Playhaven dashboard");
        }
    }

    public PHAsyncRequest getConnection() {
        return this.conn;
    }

    public void setRequestTag(int requestTag2) {
        this.requestTag = requestTag2;
    }

    public int getRequestTag() {
        return this.requestTag;
    }

    public HashMap<String, String> getSignedParams(Context context) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String str;
        String appId;
        String appVersion;
        String connection;
        if (this.signedParams == null) {
            String device = Settings.System.getString(context.getContentResolver(), "android_id");
            if (device == null) {
                device = PHContent.PARCEL_NULL;
            }
            String idiom = String.valueOf(context.getResources().getConfiguration().screenLayout & 15);
            String nonce = PHStringUtil.base64Digest(PHStringUtil.generateUUID());
            Object[] objArr = new Object[4];
            objArr[0] = this.config.getToken(context);
            objArr[1] = device != null ? device : "";
            if (nonce != null) {
                str = nonce;
            } else {
                str = "";
            }
            objArr[2] = str;
            objArr[3] = this.config.getSecret(context);
            String sigHash = PHStringUtil.hexDigest(String.format("%s:%s:%s:%s", objArr));
            try {
                PackageInfo pinfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                appId = pinfo.packageName;
                appVersion = pinfo.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                appId = "";
                appVersion = "";
            }
            String hardware = Build.MODEL;
            String os = String.format("%s %s", Build.VERSION.RELEASE, Integer.valueOf(Build.VERSION.SDK_INT));
            String sdk_version = this.config.getCleanSDKVersion();
            DisplayMetrics dm = context.getResources().getDisplayMetrics();
            String width = String.valueOf(dm.widthPixels);
            String height = String.valueOf(dm.heightPixels);
            String screen_density = String.valueOf(dm.densityDpi);
            PHConfiguration.ConnectionType type = PHConnectionUtils.getConnectionType(context);
            if (type == PHConfiguration.ConnectionType.NO_PERMISSION) {
                connection = null;
            } else {
                connection = String.valueOf(type.ordinal());
            }
            Hashtable<String, String> additionalParams2 = getAdditionalParams(context);
            HashMap<String, String> add_params = additionalParams2 != null ? new HashMap<>(additionalParams2) : new HashMap<>();
            this.signedParams = new HashMap<>();
            this.signedParams.put("device", device);
            this.signedParams.put("token", this.config.getToken(context));
            this.signedParams.put("signature", sigHash);
            this.signedParams.put("nonce", nonce);
            this.signedParams.put("app", appId);
            this.signedParams.put("app_version", appVersion);
            this.signedParams.put("hardware", hardware);
            this.signedParams.put("os", os);
            this.signedParams.put("idiom", idiom);
            this.signedParams.put(AdCreative.kFixWidth, width);
            this.signedParams.put(AdCreative.kFixHeight, height);
            this.signedParams.put("sdk_version", sdk_version);
            this.signedParams.put("sdk_platform", "android");
            this.signedParams.put("orientation", "0");
            this.signedParams.put("dpi", screen_density);
            this.signedParams.put("languages", Locale.getDefault().getLanguage());
            if (connection != null) {
                this.signedParams.put("connection", connection);
            }
            add_params.putAll(this.signedParams);
            this.signedParams = add_params;
        }
        return this.signedParams;
    }

    public void setAdditionalParameters(Hashtable<String, String> params) {
        this.additionalParams = params;
    }

    public Hashtable<String, String> getAdditionalParams(Context context) {
        return this.additionalParams;
    }

    public String signedParamsStr(Context context) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        return PHStringUtil.createQuery(getSignedParams(context));
    }

    private boolean hasValidTokenAndSecret(Context context) {
        String token = this.config.getToken(context);
        String secret = this.config.getSecret(context);
        if (token == null || token.length() <= 0 || secret == null || secret.length() <= 0) {
            return false;
        }
        return true;
    }

    public void send(Context context) {
        if (!hasValidTokenAndSecret(context)) {
            PHStringUtil.log("Either the token or secret has not been properly set");
            return;
        }
        this.conn = new PHAsyncRequest(this);
        if (!(this.config.getStagingUsername(context) == null || this.config.getStagingPassword(context) == null)) {
            this.conn.setUsername(this.config.getStagingUsername(context));
            this.conn.setPassword(this.config.getStagingPassword(context));
        }
        this.conn.request_type = getRequestType();
        checkTokenAndSecret(this.config.getToken(context), this.config.getSecret(context));
        send(context, this.conn);
    }

    private void send(Context context, PHAsyncRequest client) {
        try {
            this.conn = client;
            if (this.conn.request_type == PHAsyncRequest.RequestType.Post) {
                this.conn.addPostParams(getPostParams());
            }
            PHStringUtil.log("Sending PHAPIRequest of type: " + getRequestType().toString());
            PHStringUtil.log("PHAPIRequest URL: " + getURL(context));
            this.conn.execute(Uri.parse(getURL(context)));
        } catch (Exception e) {
            PHCrashReport.reportCrash(e, "PHAPIRequest - send()", PHCrashReport.Urgency.critical);
        }
    }

    public JSONObject getLastResponse() {
        return this.lastResponse;
    }

    public PHError getLastError() {
        return this.lastError;
    }

    public void resetLastError() {
        this.lastError = null;
    }

    public void resetLastResponse() {
        this.lastResponse = null;
    }

    public PHAsyncRequest.RequestType getRequestType() {
        return PHAsyncRequest.RequestType.Get;
    }

    public Hashtable<String, String> getPostParams() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void finish() {
        this.conn.cancel(true);
    }

    public void cancel() {
        PHStringUtil.log(toString() + " canceled!");
        finish();
    }

    public String getURL(Context context) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        if (this.fullUrl == null) {
            this.fullUrl = String.format("%s?%s", baseURL(context), signedParamsStr(context));
        }
        return this.fullUrl;
    }

    public String createAPIURL(Context context, String slug) {
        return this.config.getAPIUrl(context) + slug;
    }

    public String baseURL(Context context) {
        return this.urlPath;
    }

    public void setBaseURL(String url) {
        this.urlPath = url;
    }

    public void handleRequestSuccess(JSONObject res) {
        if (this.shouldComplainAboutNonOverridden) {
            throw new RuntimeException("Request succeeded and subclass has not override handleRequestSuccess");
        }
    }

    public void handleRequestFailure(PHError error) {
        if (this.shouldComplainAboutNonOverridden) {
            throw new RuntimeException("Request failed and subclass has not override handleRequestFailure");
        }
    }

    public void onHttpRequestSucceeded(ByteBuffer response, int responseCode) {
        PHStringUtil.log("Received response code: " + responseCode);
        if (responseCode != 200) {
            handleRequestFailure(new PHError("Request failed with code: " + responseCode));
            return;
        }
        if (response == null || response.array() == null) {
            processRequestResponse(new JSONObject());
        }
        try {
            String res_str = new String(response.array(), "UTF8");
            PHStringUtil.log("Unparsed JSON: " + res_str);
            processRequestResponse(new JSONObject(res_str));
        } catch (UnsupportedEncodingException e) {
            handleRequestFailure(new PHError("Unsupported encoding when parsing JSON"));
        } catch (JSONException e2) {
            handleRequestFailure(new PHError("Could not parse JSON because: " + e2.getMessage()));
        } catch (Exception e3) {
            Exception e4 = e3;
            e4.printStackTrace();
            handleRequestFailure(new PHError("Unknown error during API request: " + e4.getMessage()));
        }
    }

    public void processRequestResponse(JSONObject response) {
        String errmsg = response.optString("error");
        JSONObject errobj = response.optJSONObject("errobj");
        if ((JSONObject.NULL.equals(errobj) || errobj.length() <= 0) && (response.isNull("error") || errmsg.length() <= 0)) {
            this.lastResponse = response.optJSONObject("response");
            handleRequestSuccess(this.lastResponse);
            return;
        }
        this.lastError = new PHError("Server sent error message: " + errmsg);
        handleRequestFailure(this.lastError);
    }

    public void onHttpRequestFailed(PHError e) {
        this.lastError = e;
        handleRequestFailure(e);
    }

    public Hashtable<String, String> getAdditionalParams() {
        return this.additionalParams;
    }
}
