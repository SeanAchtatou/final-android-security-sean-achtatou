package v2.com.playhaven.requests.open;

import android.os.AsyncTask;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import v2.com.playhaven.cache.PHCache;
import v2.com.playhaven.listeners.PHPrefetchTaskListener;
import v2.com.playhaven.requests.crashreport.PHCrashReport;
import v2.com.playhaven.utils.PHStringUtil;

public class PHPrefetchTask extends AsyncTask<Integer, Integer, Integer> {
    private static final String GZIP_ENCODING = "gzip";
    public PHPrefetchTaskListener listener;
    public URL url;

    public void setPrefetchListener(PHPrefetchTaskListener listener2) {
        this.listener = listener2;
    }

    public PHPrefetchTaskListener getPrefetchListener() {
        return this.listener;
    }

    public URL getURL() {
        return this.url;
    }

    public void setURL(String url2) {
        try {
            this.url = new URL(url2);
        } catch (MalformedURLException e) {
            this.url = null;
            PHStringUtil.log("Malformed URL in PHPrefetchTask: " + url2);
        }
    }

    /* access modifiers changed from: protected */
    public Integer doInBackground(Integer... dummys) {
        int responseCode = 400;
        if (!PHCache.hasBeenInstalled()) {
            return 400;
        }
        try {
            synchronized (PHCache.getSharedCache()) {
                if (this.url == null) {
                    return 400;
                }
                DefaultHttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet(this.url.toString());
                request.addHeader("Accept-Encoding", GZIP_ENCODING);
                HttpResponse response = client.execute(request);
                responseCode = response.getStatusLine().getStatusCode();
                if (responseCode != 200) {
                    Integer valueOf = Integer.valueOf(responseCode);
                    return valueOf;
                }
                HttpEntity entity = response.getEntity();
                Header contentEncoding = null;
                try {
                    contentEncoding = entity.getContentEncoding();
                } catch (UnsupportedOperationException e) {
                }
                boolean isCompressed = contentEncoding == null ? false : contentEncoding.getValue().equals(GZIP_ENCODING);
                PHStringUtil.log("Prefetch done....caching file");
                PHCache.getSharedCache().cacheFile(this.url, entity.getContent(), isCompressed);
            }
        } catch (Exception e2) {
            PHCrashReport.reportCrash(e2, "PHPrefetchTask - doInBackground", PHCrashReport.Urgency.low);
        }
        return Integer.valueOf(responseCode);
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Integer... progress) {
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer result) {
        if (this.listener != null) {
            this.listener.onPrefetchDone(result.intValue());
        }
    }
}
