package v2.com.playhaven.requests.open;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.playhaven.src.utils.PHStringUtil;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.json.JSONArray;
import org.json.JSONObject;
import v2.com.playhaven.cache.PHCache;
import v2.com.playhaven.configuration.PHConfiguration;
import v2.com.playhaven.listeners.PHOpenRequestListener;
import v2.com.playhaven.listeners.PHPrefetchListener;
import v2.com.playhaven.listeners.PHPrefetchTaskListener;
import v2.com.playhaven.model.PHError;
import v2.com.playhaven.requests.base.PHAPIRequest;
import v2.com.playhaven.requests.crashreport.PHCrashReport;

public class PHOpenRequest extends PHAPIRequest implements PHPrefetchTaskListener {
    private PHConfiguration config;
    private PHOpenRequestListener open_listener;
    private ConcurrentLinkedQueue<PHPrefetchTask> prefetchTasks;
    private PHPrefetchListener prefetch_listener;
    private PHSession session;
    private boolean shouldPrecache;
    public boolean startPrecachingImmediately;

    public void setPrefetchListener(PHPrefetchListener listener) {
        this.prefetch_listener = listener;
    }

    public void setOpenRequestListener(PHOpenRequestListener listener) {
        this.open_listener = listener;
    }

    public PHOpenRequestListener getOpenRequestListener() {
        return this.open_listener;
    }

    public PHPrefetchListener getPrefetchListener() {
        return this.prefetch_listener;
    }

    public PHSession getSession() {
        return this.session;
    }

    public ConcurrentLinkedQueue<PHPrefetchTask> getPrefetchTasks() {
        return this.prefetchTasks;
    }

    public PHOpenRequest(PHOpenRequestListener listener) {
        this();
        this.open_listener = listener;
    }

    public PHOpenRequest() {
        this.prefetchTasks = new ConcurrentLinkedQueue<>();
        this.startPrecachingImmediately = true;
        this.config = new PHConfiguration();
    }

    public String baseURL(Context context) {
        return super.createAPIURL(context, "/v3/publisher/open/");
    }

    public void send(Context context) {
        this.shouldPrecache = this.config.getShouldPrecache(context);
        synchronized (PHOpenRequest.class) {
            if (this.shouldPrecache) {
                synchronized (PHCache.class) {
                    PHCache.installCache(context);
                }
            }
        }
        this.session = PHSession.getInstance(context);
        this.session.start();
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext()).edit();
        editor.putLong(PHSession.SSUM_PREF, this.session.getTotalTime());
        editor.putLong(PHSession.SCOUNT_PREF, this.session.getSessionCount());
        editor.commit();
        super.send(context);
    }

    public void handleRequestFailure(PHError e) {
        if (this.open_listener != null) {
            this.open_listener.onOpenFailed(this, e);
        }
    }

    public void handleRequestSuccess(JSONObject res) {
        PHStringUtil.log("Open request received a response...should we precache: " + this.shouldPrecache);
        if (res != null && this.shouldPrecache && res.has("precache")) {
            this.prefetchTasks.clear();
            JSONArray precached = res.optJSONArray("precache");
            if (precached != null) {
                for (int i = 0; i < precached.length(); i++) {
                    String url = precached.optString(i);
                    if (url != null) {
                        PHPrefetchTask task = new PHPrefetchTask();
                        task.setPrefetchListener(this);
                        task.setURL(url);
                        this.prefetchTasks.add(task);
                    }
                }
            }
            if (this.startPrecachingImmediately) {
                startNextPrefetch();
            }
        }
        this.session.startAndReset();
        if (this.open_listener != null) {
            this.open_listener.onOpenSuccessful(this);
        }
    }

    public void startNextPrefetch() {
        PHStringUtil.log("Starting precache task with a total of: " + this.prefetchTasks.size());
        if (this.prefetchTasks.size() > 0) {
            this.prefetchTasks.poll().execute(new Integer[0]);
        }
    }

    public void onPrefetchDone(int result) {
        try {
            PHStringUtil.log("Pre-cache task done. Starting next out of " + this.prefetchTasks.size());
            if (this.prefetchTasks.size() == 0 && this.prefetch_listener != null) {
                this.prefetch_listener.onPrefetchFinished(this);
            } else if (this.prefetchTasks.size() > 0 && this.startPrecachingImmediately) {
                startNextPrefetch();
            }
        } catch (Exception e) {
            PHCrashReport.reportCrash(e, "PHOpenRequest - prefetchDone", PHCrashReport.Urgency.low);
        }
    }

    public Hashtable<String, String> getAdditionalParams(Context context) {
        Hashtable<String, String> params = new Hashtable<>();
        params.put("ssum", String.valueOf(this.session.getTotalTime()));
        params.put("scount", String.valueOf(this.session.getSessionCount()));
        if (this.shouldPrecache) {
            params.put("precache", "1");
        }
        return params;
    }
}
