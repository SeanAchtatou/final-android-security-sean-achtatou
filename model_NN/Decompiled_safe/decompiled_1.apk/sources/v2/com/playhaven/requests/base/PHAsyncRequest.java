package v2.com.playhaven.requests.base;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Base64;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultRedirectHandler;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;
import v2.com.playhaven.listeners.PHHttpRequestListener;
import v2.com.playhaven.model.PHError;
import v2.com.playhaven.requests.crashreport.PHCrashReport;
import v2.com.playhaven.utils.PHStringUtil;

public class PHAsyncRequest extends AsyncTask<Uri, Integer, ByteBuffer> {
    public static final int INFINITE_REDIRECTS = Integer.MAX_VALUE;
    private PHHttpConn client;
    private boolean isDownloading;
    private PHError lastError;
    private PHHttpRequestListener listener;
    public HttpParams params;
    private String password;
    private ArrayList<NameValuePair> postParams = new ArrayList<>();
    private long requestStart;
    public RequestType request_type;
    private int responseCode;
    public Uri url;
    private String username;

    public enum RequestType {
        Post,
        Get,
        Put,
        Delete
    }

    public static class PHHttpConn {
        protected DefaultHttpClient client = new DefaultHttpClient(enableRedirecting(null));
        private HttpUriRequest cur_request;
        private PHSchemeRegistry mSchemeReg = new PHSchemeRegistry();
        private int max_redirects = PHAsyncRequest.INFINITE_REDIRECTS;
        private String password;
        private ArrayList<String> redirectUrls = new ArrayList<>();
        private int totalRedirects = 0;
        private String username;

        private class PHRedirectHandler extends DefaultRedirectHandler {
            private PHRedirectHandler() {
            }

            public boolean isRedirectRequested(HttpResponse response, HttpContext context) {
                return PHHttpConn.this.shouldRedirect(response);
            }
        }

        public static class PHSchemeRegistry {
            private SchemeRegistry schemeReg = new SchemeRegistry();

            public Scheme get(String name) {
                return this.schemeReg.get(name);
            }
        }

        public void setMaxRedirect(int max) {
            this.max_redirects = max;
        }

        public int getMaxRedirects() {
            return this.max_redirects;
        }

        public PHHttpConn() {
            this.client.setRedirectHandler(new PHRedirectHandler());
        }

        public void setSchemeRegistry(PHSchemeRegistry reg) {
            this.mSchemeReg = reg;
        }

        public DefaultHttpClient getHTTPClient() {
            return this.client;
        }

        /* Debug info: failed to restart local var, previous not found, register: 3 */
        public String getLastRedirect() {
            if (this.redirectUrls.size() == 0) {
                return null;
            }
            return this.redirectUrls.get(this.redirectUrls.size() - 1);
        }

        public void addRedirectUrl(String url) {
            this.redirectUrls.add(url);
        }

        public void clearRedirects() {
            this.redirectUrls.clear();
        }

        public boolean isRedirectResponse(int code) {
            return code >= 300 && code <= 307;
        }

        public boolean shouldRedirect(HttpResponse response) {
            if (!isRedirectResponse(response.getStatusLine().getStatusCode())) {
                return false;
            }
            if (response.getHeaders("Location").length == 0) {
                return false;
            }
            String redirectURL = response.getHeaders("Location")[0].getValue();
            if (redirectURL == null) {
                return false;
            }
            Uri uri = Uri.parse(redirectURL);
            if (uri == null || uri.getScheme() == null || uri.getPath() == null) {
                return false;
            }
            addRedirectUrl(uri.toString());
            if (this.mSchemeReg.get(uri.getScheme()) == null) {
                return false;
            }
            int i = this.totalRedirects + 1;
            this.totalRedirects = i;
            return i <= this.max_redirects;
        }

        private HttpParams enableRedirecting(HttpParams params) {
            if (params == null) {
                params = new BasicHttpParams();
            }
            params.setParameter("http.protocol.version", HttpVersion.HTTP_1_1);
            params.setBooleanParameter("http.protocol.allow-circular-redirects", true);
            HttpClientParams.setRedirecting(params, true);
            return params;
        }

        public void setUsername(String username2) {
            this.username = username2;
        }

        public String getUsername() {
            return this.username;
        }

        public String getPassword() {
            return this.password;
        }

        public void setPassword(String password2) {
            this.password = password2;
        }

        public HttpResponse start(HttpUriRequest request) throws IOException {
            this.cur_request = request;
            this.totalRedirects = 0;
            clearRedirects();
            if (!(this.username == null || this.password == null)) {
                request.setHeader("Authorization", String.format("Basic %s", Base64.encodeToString((this.username + ":" + this.password).getBytes(), 10)));
            }
            return this.client.execute(request);
        }

        public HttpUriRequest getCurrentRequest() {
            return this.cur_request;
        }
    }

    public PHAsyncRequest(PHHttpRequestListener delegate) {
        this.listener = delegate;
        this.client = new PHHttpConn();
        this.request_type = RequestType.Get;
    }

    public void setMaxRedirects(int max) {
        this.client.setMaxRedirect(max);
    }

    public int getMaxRedirects() {
        return this.client.getMaxRedirects();
    }

    public ArrayList<NameValuePair> getPostParams() {
        return this.postParams;
    }

    public void addPostParam(String key, String value) {
        this.postParams.add(new BasicNameValuePair(key, value));
    }

    public void addPostParams(Hashtable<String, String> params2) {
        if (params2 != null) {
            this.postParams.clear();
            for (Map.Entry<String, String> entry : params2.entrySet()) {
                this.postParams.add(new BasicNameValuePair((String) entry.getKey(), (String) entry.getValue()));
            }
        }
    }

    public String getLastRedirectURL() {
        return this.client.getLastRedirect();
    }

    public PHHttpConn getPHHttpClient() {
        return this.client;
    }

    /* access modifiers changed from: protected */
    public ByteBuffer doInBackground(Uri... urls) {
        return execRequest(urls);
    }

    private ByteBuffer execRequest(Uri... urls) {
        HttpResponse response;
        ByteBuffer buffer = null;
        this.responseCode = -1;
        this.lastError = null;
        synchronized (this) {
            try {
                this.isDownloading = true;
                this.requestStart = System.currentTimeMillis();
                this.client.clearRedirects();
                if (urls.length > 0) {
                    Uri url2 = urls[0];
                    if (!url2.equals(this.url) && this.url != null) {
                        url2 = this.url;
                    }
                    try {
                        if (isCancelled()) {
                            return null;
                        }
                        String net_uri = url2.toString();
                        if (this.request_type == RequestType.Post) {
                            HttpPost request = new HttpPost(net_uri);
                            request.setEntity(new UrlEncodedFormEntity(this.postParams));
                            response = this.client.start(request);
                        } else if (this.request_type == RequestType.Get) {
                            response = this.client.start(new HttpGet(net_uri));
                        } else {
                            response = this.client.start(new HttpGet(net_uri));
                        }
                        HttpEntity entity = response.getEntity();
                        this.responseCode = response.getStatusLine().getStatusCode();
                        if (this.responseCode == 302 && getLastRedirectURL() != null) {
                            this.responseCode = 200;
                        }
                        if (isCancelled()) {
                            return null;
                        }
                        if (entity != null) {
                            InputStream in_stream = entity.getContent();
                            buffer = readStream(in_stream);
                            in_stream.close();
                        }
                    } catch (IOException e) {
                        this.lastError = new PHError(e);
                    }
                }
            } catch (Exception e2) {
                PHCrashReport.reportCrash(e2, "PHAsyncRequest - doInBackground", PHCrashReport.Urgency.critical);
            }
            return buffer;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(ByteBuffer result) {
        super.onPostExecute((Object) result);
        try {
            this.isDownloading = false;
            PHStringUtil.log("PHAsyncRequest elapsed time (ms) = " + (System.currentTimeMillis() - this.requestStart));
            if (this.lastError != null && this.listener != null) {
                this.listener.onHttpRequestFailed(this.lastError);
            } else if (this.listener != null) {
                this.listener.onHttpRequestSucceeded(result, this.responseCode);
            }
        } catch (Exception e) {
            PHCrashReport.reportCrash(e, "PHAsyncRequest - onPostExecute", PHCrashReport.Urgency.critical);
        }
    }

    public void setUsername(String username2) {
        this.username = username2;
        this.client.setUsername(username2);
    }

    public String getUsername() {
        return this.username;
    }

    public void setPassword(String password2) {
        this.password = password2;
        this.client.setPassword(password2);
    }

    public RequestType getRequestType() {
        return this.request_type;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPHHttpClient(PHHttpConn client2) {
        this.client = client2;
    }

    public boolean isDownloading() {
        return this.isDownloading;
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.isDownloading = false;
    }

    private static ByteBuffer readStream(InputStream inputStream) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        while (true) {
            int len = inputStream.read(buffer);
            if (len != -1) {
                output.write(buffer, 0, len);
            } else {
                output.flush();
                return ByteBuffer.wrap(output.toByteArray());
            }
        }
    }

    public static String streamToString(InputStream inputStream) throws IOException, UnsupportedEncodingException {
        return new String(readStream(inputStream).array(), "UTF-8");
    }
}
