package v2.com.playhaven.requests.crashreport;

import android.content.Context;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import v2.com.playhaven.configuration.PHConfiguration;
import v2.com.playhaven.requests.base.PHAPIRequest;
import v2.com.playhaven.requests.base.PHAsyncRequest;

public class PHCrashReport extends PHAPIRequest {
    private final String CRASH_REPORT_TEMPLATE;
    private final String DATE_FORMAT;
    private final String POST_PAYLOAD_NAME;
    private Exception exception;
    private Urgency level;
    private Date reportTime;
    private String tag;

    public enum Urgency {
        critical,
        high,
        medium,
        low,
        none
    }

    public PHCrashReport() {
        this.level = Urgency.critical;
        this.reportTime = new Date();
        this.DATE_FORMAT = "MM/dd/yyyy HH:mm:ss";
        this.POST_PAYLOAD_NAME = "payload";
        this.CRASH_REPORT_TEMPLATE = "Crash Report [PHCrashReport]\nTag: %s\nPlatform: %s\nVersion: %s\nTime: %s\nSession: %s\nDevice: %s\nUrgency: %s\nMessage: %sStack Trace:\n\n%s";
        this.reportTime = new Date();
        this.exception = null;
    }

    public PHCrashReport(Exception e, Urgency level2) {
        this();
        this.exception = e;
        this.tag = null;
    }

    public PHCrashReport(Exception e, String tag2, Urgency level2) {
        this();
        this.exception = e;
        this.tag = null;
    }

    public static PHCrashReport reportCrash(Exception e, String tag2, Urgency level2) {
        e.printStackTrace();
        return null;
    }

    public static PHCrashReport reportCrash(Exception e, Urgency level2) {
        e.printStackTrace();
        return null;
    }

    public String baseURL(Context context) {
        return super.createAPIURL(context, "/v3/publisher/crash/");
    }

    public Hashtable<String, String> getAdditionalParams(Context context) {
        Hashtable<String, String> params = new Hashtable<>();
        params.put("ts", Long.toString(System.currentTimeMillis()));
        params.put("urgency", this.level.toString());
        if (this.tag != null) {
            params.put("tag", this.tag);
        }
        return params;
    }

    public PHAsyncRequest.RequestType getRequestType() {
        return PHAsyncRequest.RequestType.Post;
    }

    public Hashtable<String, String> getPostParams() {
        Hashtable<String, String> params = new Hashtable<>();
        params.put("payload", generateCrashReport());
        return params;
    }

    public void send(Context context) {
    }

    private String generateCrashReport() {
        if (this.exception == null) {
            return "(No Exception)";
        }
        this.exception.fillInStackTrace();
        StringWriter sWriter = new StringWriter();
        this.exception.printStackTrace(new PrintWriter(sWriter));
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        PHConfiguration config = new PHConfiguration();
        Object[] objArr = new Object[8];
        objArr[0] = this.tag != null ? this.tag : "(No Tag)";
        objArr[1] = "android";
        objArr[2] = config.getSDKVersion();
        objArr[3] = df.format(this.reportTime);
        objArr[4] = "(No Session)";
        objArr[5] = this.level.toString();
        objArr[6] = this.exception.getMessage();
        objArr[7] = sWriter.toString();
        return String.format("Crash Report [PHCrashReport]\nTag: %s\nPlatform: %s\nVersion: %s\nTime: %s\nSession: %s\nDevice: %s\nUrgency: %s\nMessage: %sStack Trace:\n\n%s", objArr);
    }
}
