package v2.com.playhaven.requests.content;

import android.content.Context;
import org.json.JSONObject;
import v2.com.playhaven.listeners.PHSubContentRequestListener;
import v2.com.playhaven.model.PHError;
import v2.com.playhaven.requests.base.PHAPIRequest;
import v2.com.playhaven.utils.PHStringUtil;

public class PHSubContentRequest extends PHAPIRequest {
    private PHSubContentRequestListener listener;
    private String webviewCallback;

    public PHSubContentRequest(PHSubContentRequestListener listener2) {
        this.listener = listener2;
    }

    public void setSubContentReuqestListener(PHSubContentRequestListener listener2) {
        this.listener = listener2;
    }

    public PHSubContentRequestListener getSubContentListener() {
        return this.listener;
    }

    public void setWebviewCallback(String callback) {
        this.webviewCallback = callback;
    }

    public String getWebviewCallback() {
        return this.webviewCallback;
    }

    public void handleRequestSuccess(JSONObject json) {
        if (this.listener != null) {
            this.listener.onSubContentRequestSucceeded(this, json);
        }
    }

    public void handleRequestFailure(PHError error) {
        if (this.listener != null) {
            this.listener.onSubContentRequestFailed(this, error);
        }
    }

    public String getURL(Context context) {
        if (this.fullUrl == null) {
            this.fullUrl = baseURL(context);
        }
        return this.fullUrl;
    }

    public void send(Context context) {
        if (JSONObject.NULL.equals(baseURL(context)) || baseURL(context).length() <= 0) {
            PHStringUtil.log("No URL set for PHSubContentRequest");
        } else {
            super.send(context);
        }
    }
}
