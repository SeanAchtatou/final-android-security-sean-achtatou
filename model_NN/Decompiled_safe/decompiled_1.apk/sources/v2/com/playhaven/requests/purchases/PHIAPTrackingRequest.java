package v2.com.playhaven.requests.purchases;

import android.content.Context;
import java.util.Currency;
import java.util.Hashtable;
import java.util.Locale;
import org.json.JSONObject;
import v2.com.playhaven.listeners.PHIAPRequestListener;
import v2.com.playhaven.model.PHError;
import v2.com.playhaven.model.PHPurchase;
import v2.com.playhaven.requests.base.PHAPIRequest;

public class PHIAPTrackingRequest extends PHAPIRequest {
    private static Hashtable<String, String> cookies = new Hashtable<>();
    private PHIAPRequestListener listener;
    private PHPurchase purchase;

    public PHIAPTrackingRequest() {
    }

    public PHIAPTrackingRequest(PHIAPRequestListener listener2) {
        setIAPListener(listener2);
    }

    public PHIAPTrackingRequest(PHIAPRequestListener listener2, PHPurchase purchase2) {
        this.purchase = purchase2;
        setIAPListener(listener2);
    }

    public PHIAPTrackingRequest(PHPurchase purchase2) {
        this.purchase = purchase2;
    }

    public void setPurchase(PHPurchase purchase2) {
        this.purchase = purchase2;
    }

    public void setIAPListener(PHIAPRequestListener listener2) {
        this.listener = listener2;
    }

    public PHIAPRequestListener getIAPListener() {
        return this.listener;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void setConversionCookie(java.lang.String r2, java.lang.String r3) {
        /*
            java.util.Hashtable<java.lang.String, java.lang.String> r0 = v2.com.playhaven.requests.purchases.PHIAPTrackingRequest.cookies
            monitor-enter(r0)
            java.lang.Object r1 = org.json.JSONObject.NULL     // Catch:{ all -> 0x001a }
            boolean r1 = r1.equals(r3)     // Catch:{ all -> 0x001a }
            if (r1 != 0) goto L_0x0011
            int r1 = r3.length()     // Catch:{ all -> 0x001a }
            if (r1 != 0) goto L_0x0013
        L_0x0011:
            monitor-exit(r0)     // Catch:{ all -> 0x001a }
        L_0x0012:
            return
        L_0x0013:
            java.util.Hashtable<java.lang.String, java.lang.String> r1 = v2.com.playhaven.requests.purchases.PHIAPTrackingRequest.cookies     // Catch:{ all -> 0x001a }
            r1.put(r2, r3)     // Catch:{ all -> 0x001a }
            monitor-exit(r0)     // Catch:{ all -> 0x001a }
            goto L_0x0012
        L_0x001a:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x001a }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: v2.com.playhaven.requests.purchases.PHIAPTrackingRequest.setConversionCookie(java.lang.String, java.lang.String):void");
    }

    public static String getAndExpireCookie(String product) {
        String cookie;
        synchronized (cookies) {
            cookie = cookies.get(product);
            cookies.remove(product);
        }
        return cookie;
    }

    public void handleRequestSuccess(JSONObject data) {
        if (this.listener != null) {
            this.listener.onIAPRequestSucceeded(this);
        }
    }

    public void handleRequestFailure(PHError error) {
        if (this.listener != null) {
            this.listener.onIAPRequestFailed(this, error);
        }
    }

    public String baseURL(Context context) {
        return super.createAPIURL(context, "/v3/publisher/iap/");
    }

    public Hashtable<String, String> getAdditionalParams(Context context) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        if (this.purchase == null) {
            return new Hashtable<>();
        }
        this.purchase.currencyLocale = Currency.getInstance(Locale.getDefault());
        Hashtable<String, String> purchaseData = new Hashtable<>();
        if (this.purchase.product != null) {
            str = this.purchase.product;
        } else {
            str = "";
        }
        purchaseData.put("product", str);
        if (this.purchase.resolution != null) {
            str2 = this.purchase.resolution.getType();
        } else {
            str2 = "";
        }
        purchaseData.put("resolution", str2);
        purchaseData.put("price", Double.toString(this.purchase.price));
        purchaseData.put("quantity", String.valueOf(1));
        if (this.purchase.hasError() && this.purchase.error.getErrorCode() != 0) {
            purchaseData.put("error", Integer.toString(this.purchase.error.getErrorCode()));
        }
        if (this.purchase.currencyLocale != null) {
            str3 = this.purchase.currencyLocale.getCurrencyCode();
        } else {
            str3 = "";
        }
        purchaseData.put("price_locale", str3);
        if (this.purchase.marketplace != null) {
            str4 = this.purchase.marketplace.getOrigin();
        } else {
            str4 = null;
        }
        purchaseData.put("store", str4);
        if (this.purchase.product != null) {
            str5 = this.purchase.product;
        } else {
            str5 = null;
        }
        String cookie = getAndExpireCookie(str5);
        if (cookie != null) {
            str6 = cookie;
        } else {
            str6 = "";
        }
        purchaseData.put("cookie", str6);
        return purchaseData;
    }
}
