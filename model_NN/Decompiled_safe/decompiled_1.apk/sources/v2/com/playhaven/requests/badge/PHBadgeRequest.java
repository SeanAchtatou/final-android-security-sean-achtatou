package v2.com.playhaven.requests.badge;

import android.content.Context;
import java.util.Hashtable;
import org.json.JSONObject;
import v2.com.playhaven.interstitial.PHContentEnums;
import v2.com.playhaven.listeners.PHBadgeRequestListener;
import v2.com.playhaven.model.PHError;
import v2.com.playhaven.requests.base.PHAPIRequest;

public class PHBadgeRequest extends PHAPIRequest {
    private PHBadgeRequestListener listener;
    public String placement;

    public PHBadgeRequest(String placement2) {
        this.placement = "";
        this.placement = placement2;
    }

    public PHBadgeRequest(PHBadgeRequestListener listener2, String placement2) {
        this(placement2);
        setMetadataListener(listener2);
    }

    public PHBadgeRequestListener getListener() {
        return this.listener;
    }

    public void setMetadataListener(PHBadgeRequestListener listener2) {
        this.listener = listener2;
    }

    public PHBadgeRequestListener getMetadataListener() {
        return this.listener;
    }

    public void handleRequestSuccess(JSONObject json) {
        if (json == null || JSONObject.NULL.equals(json)) {
            if (this.listener != null) {
                this.listener.onBadgeRequestFailed(this, new PHError(PHContentEnums.Error.NoResponseField.getMessage()));
            }
        } else if (this.listener != null) {
            this.listener.onBadgeRequestSucceeded(this, json);
        }
    }

    public void handleRequestFailure(PHError error) {
        if (this.listener != null) {
            this.listener.onBadgeRequestFailed(this, error);
        }
    }

    public String baseURL(Context context) {
        return super.createAPIURL(context, "/v3/publisher/content/");
    }

    public Hashtable<String, String> getAdditionalParams(Context context) {
        Hashtable<String, String> params = new Hashtable<>();
        if (this.placement != null) {
            params.put("placement_id", this.placement);
        }
        params.put("metadata", "1");
        return params;
    }
}
