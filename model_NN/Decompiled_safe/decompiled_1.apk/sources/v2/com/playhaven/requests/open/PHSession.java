package v2.com.playhaven.requests.open;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import v2.com.playhaven.utils.PHStringUtil;

public class PHSession {
    public static final String SCOUNT_PREF = "com_playhaven_time_in_game_scount";
    public static final String SSUM_PREF = "com_playhaven_time_in_game_ssum";
    private static PHSession mSession = null;
    private long mCurTime;
    private long mSessionCount;
    private boolean mSessionPaused = true;
    private boolean mSessionStarted = false;
    private long mSessionTime;
    private long mTotalTime;

    protected PHSession(Context context) {
        inflate(context);
    }

    public static PHSession getInstance(Context context) {
        if (mSession == null) {
            mSession = new PHSession(context);
        }
        return mSession;
    }

    public static PHSession regenerateInstance(Context context) {
        getInstance(context).clear(context);
        getInstance(context).reset();
        mSession = null;
        return getInstance(context);
    }

    public void start() {
        PHStringUtil.log("Starting a new session.");
        if (this.mSessionStarted) {
            this.mTotalTime += getSessionTime();
            this.mSessionCount++;
        }
        this.mSessionTime = 0;
        this.mCurTime = SystemClock.uptimeMillis();
        this.mSessionStarted = true;
    }

    public void startAndReset() {
        start();
        this.mTotalTime = 0;
        this.mSessionCount = 0;
    }

    public void reset() {
        this.mTotalTime = 0;
        this.mSessionCount = 0;
        this.mSessionTime = 0;
        this.mCurTime = SystemClock.uptimeMillis();
        this.mSessionStarted = false;
        this.mSessionPaused = true;
    }

    public long getSessionTime() {
        return this.mSessionTime + getLastElapsedTime();
    }

    public long getTotalTime() {
        return this.mTotalTime + getSessionTime();
    }

    public long getSessionCount() {
        return this.mSessionCount;
    }

    public static void register(Activity activity) {
        if (activity != null) {
            mSession = getInstance(activity);
            if (mSession.mSessionPaused) {
                mSession.resumeSession();
            }
        }
    }

    public static void unregister(Activity activity) {
        if (activity != null) {
            mSession = getInstance(activity);
            if (!mSession.mSessionPaused) {
                mSession.pauseSession();
                if (mSession.mSessionStarted && activity.isFinishing()) {
                    mSession.save(activity);
                }
            }
        }
    }

    private long getLastElapsedTime() {
        if (!this.mSessionStarted || this.mSessionPaused) {
            return 0;
        }
        return (SystemClock.uptimeMillis() - this.mCurTime) / 1000;
    }

    private void pauseSession() {
        this.mSessionTime = getSessionTime();
        this.mSessionPaused = true;
    }

    private void resumeSession() {
        this.mCurTime = SystemClock.uptimeMillis();
        this.mSessionPaused = false;
    }

    private void inflate(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        this.mTotalTime = prefs.getLong(SSUM_PREF, 0);
        this.mSessionCount = prefs.getLong(SCOUNT_PREF, 0);
    }

    public void clear(Context context) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext()).edit();
        editor.remove(SSUM_PREF);
        editor.remove(SCOUNT_PREF);
        editor.commit();
    }

    private void save(Context context) {
        if (this.mSessionStarted) {
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext()).edit();
            editor.putLong(SSUM_PREF, this.mTotalTime + getSessionTime());
            editor.putLong(SCOUNT_PREF, this.mSessionCount + 1);
            editor.commit();
        }
    }
}
