package v2.com.playhaven.requests.content;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import com.playhaven.src.publishersdk.content.PHContentView;
import com.playhaven.src.utils.PHStringUtil;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Random;
import org.json.JSONObject;
import v2.com.playhaven.cache.PHCache;
import v2.com.playhaven.configuration.PHConfiguration;
import v2.com.playhaven.interstitial.PHContentEnums;
import v2.com.playhaven.interstitial.requestbridge.BridgeManager;
import v2.com.playhaven.interstitial.requestbridge.base.ContentRequester;
import v2.com.playhaven.interstitial.requestbridge.bridges.ContentRequestToInterstitialBridge;
import v2.com.playhaven.listeners.PHContentRequestListener;
import v2.com.playhaven.listeners.PHPurchaseListener;
import v2.com.playhaven.listeners.PHRewardListener;
import v2.com.playhaven.model.PHContent;
import v2.com.playhaven.model.PHError;
import v2.com.playhaven.requests.base.PHAPIRequest;
import v2.com.playhaven.requests.crashreport.PHCrashReport;
import v2.com.playhaven.requests.open.PHSession;
import v2.com.playhaven.views.interstitial.PHCloseButton;

public class PHContentRequest extends PHAPIRequest implements ContentRequester {
    private static int PREVIOUS_AD_RANGE = 2000;
    private static Long mLastDismissed;
    private WeakReference<Activity> activityContext;
    private WeakReference<Context> applicationContext;
    private ContentRequestToInterstitialBridge bridge;
    private Bitmap close_button_down;
    private Bitmap close_button_up;
    private PHConfiguration config;
    private PHContent content;
    public String contentTag;
    private PHContentRequestListener content_listener;
    private PHRequestState currentContentState;
    public String placement;
    private PHPurchaseListener purchase_listener;
    private PHRewardListener reward_listener;
    private boolean shouldPrecache;
    private PHRequestState targetState;

    public enum PHDismissType {
        AdSelfDismiss,
        CloseButton,
        ApplicationBackgrounded
    }

    public enum PHRequestState {
        Initialized,
        Preloading,
        Preloaded,
        DisplayingContent,
        Done
    }

    public PHContentRequest(String placement2) {
        this.content_listener = null;
        this.reward_listener = null;
        this.purchase_listener = null;
        this.placement = placement2;
        setCurrentContentState(PHRequestState.Initialized);
        this.config = new PHConfiguration();
        createBridge();
    }

    public PHContentRequest(PHContentRequestListener listener, String placement2) {
        this(placement2);
        this.content_listener = listener;
    }

    public PHContentRequestListener getContentListener() {
        return this.content_listener;
    }

    public void setOnContentListener(PHContentRequestListener content_listener2) {
        this.content_listener = content_listener2;
        if (this.bridge != null) {
            this.bridge.attachContentListener(content_listener2);
        }
    }

    public PHRewardListener getRewardListener() {
        return this.reward_listener;
    }

    public void setOnRewardListener(PHRewardListener reward_listener2) {
        this.reward_listener = reward_listener2;
        if (this.bridge != null) {
            this.bridge.attachRewardListener(reward_listener2);
        }
    }

    public void setOnPurchaseListener(PHPurchaseListener purchase_listener2) {
        this.purchase_listener = purchase_listener2;
        if (this.bridge != null) {
            this.bridge.attachPurchaseListener(purchase_listener2);
        }
    }

    public PHPurchaseListener getPurchaseListener() {
        return this.purchase_listener;
    }

    public static boolean didDismissContentWithin(Long range) {
        if (mLastDismissed == null) {
            return false;
        }
        return mLastDismissed.longValue() > System.currentTimeMillis() - range.longValue();
    }

    public static boolean didJustShowAd() {
        if (mLastDismissed == null) {
            return false;
        }
        return mLastDismissed.longValue() > System.currentTimeMillis() - ((long) PREVIOUS_AD_RANGE);
    }

    public static void updateLastDismissedAdTime() {
        mLastDismissed = Long.valueOf(System.currentTimeMillis());
    }

    public void setCloseButton(Bitmap image, PHCloseButton.CloseButtonState state) {
        if (state == PHCloseButton.CloseButtonState.Up) {
            this.close_button_up = image;
        } else if (state == PHCloseButton.CloseButtonState.Down) {
            this.close_button_down = image;
        }
    }

    public String baseURL(Context context) {
        return super.createAPIURL(context, "/v3/publisher/content/");
    }

    public void setCurrentContentState(PHRequestState state) {
        if (state != null) {
            if (this.currentContentState == null) {
                this.currentContentState = state;
            }
            if (state.ordinal() > this.currentContentState.ordinal()) {
                this.currentContentState = state;
            }
        }
    }

    public PHRequestState getCurrentState() {
        return this.currentContentState;
    }

    public PHRequestState getTargetState() {
        return this.targetState;
    }

    public void setTargetState(PHRequestState state) {
        this.targetState = state;
    }

    public void setCurrentState(PHRequestState state) {
        this.currentContentState = state;
    }

    public String getTag() {
        return this.contentTag;
    }

    public PHContent getContent() {
        return this.content;
    }

    public Context getContext() {
        return this.applicationContext.get();
    }

    public void onTagChanged(String new_tag) {
        this.contentTag = new_tag;
    }

    public void preload(Activity activity) {
        if (activity != null) {
            this.shouldPrecache = this.config.getShouldPrecache(activity);
            this.applicationContext = new WeakReference<>(activity.getApplicationContext());
            this.activityContext = new WeakReference<>(activity);
            if (this.shouldPrecache) {
                synchronized (PHCache.class) {
                    PHCache.installCache(activity);
                }
            }
            this.targetState = PHRequestState.Preloaded;
            continueToNextContentState(activity);
        }
    }

    private void loadContent(Context context) {
        setCurrentContentState(PHRequestState.Preloading);
        super.send(context);
        if (this.content_listener != null) {
            this.content_listener.onSentContentRequest(this);
        }
    }

    public static void displayInterstitialActivity(PHContent content2, Activity context, HashMap<String, Bitmap> customCloseImages, String tag) {
        if (context != null) {
            Intent contentViewIntent = new Intent(context, PHContentView.class);
            contentViewIntent.putExtra(PHContentEnums.IntentArgument.Content.getKey(), content2);
            if (customCloseImages != null && customCloseImages.size() > 0) {
                contentViewIntent.putExtra(PHContentEnums.IntentArgument.CustomCloseBtn.getKey(), customCloseImages);
            }
            contentViewIntent.putExtra(PHContentEnums.IntentArgument.Tag.getKey(), tag);
            PHStringUtil.log("Added all relevant arguments now starting activity through context: " + context.getClass().getSimpleName());
            context.startActivity(contentViewIntent);
        }
    }

    private void createBridge() {
        this.contentTag = generateContentActivityTag();
        this.bridge = new ContentRequestToInterstitialBridge(this.contentTag);
        this.bridge.attachContentListener(this.content_listener);
        this.bridge.attachRewardListener(this.reward_listener);
        this.bridge.attachPurchaseListener(this.purchase_listener);
    }

    private void showContentActivityIfReady(Activity activity) {
        if (this.targetState == PHRequestState.DisplayingContent || this.targetState == PHRequestState.Done) {
            PHStringUtil.log("Attempting to show content interstitial");
            if (this.content_listener != null) {
                this.content_listener.onWillDisplayContent(this, this.content);
            }
            setCurrentContentState(PHRequestState.DisplayingContent);
            HashMap<String, Bitmap> customClose = new HashMap<>();
            if (!(this.close_button_down == null || this.close_button_up == null)) {
                customClose.put(PHCloseButton.CloseButtonState.Up.name(), this.close_button_up);
                customClose.put(PHCloseButton.CloseButtonState.Down.name(), this.close_button_down);
            }
            BridgeManager.openBridge(this.contentTag, this.bridge);
            BridgeManager.attachRequester(this.contentTag, this);
            displayInterstitialActivity(this.content, activity, customClose, this.contentTag);
        }
    }

    private String generateContentActivityTag() {
        return "PHInterstitialActivity: " + hashCode() + " ~ " + new Random(System.currentTimeMillis() / 1000).nextInt();
    }

    private void continueToNextContentState(Activity activity) {
        switch (this.currentContentState) {
            case Initialized:
                loadContent(activity);
                return;
            case Preloaded:
                showContentActivityIfReady(activity);
                return;
            default:
                return;
        }
    }

    public void send(Context context) {
        Activity activity = (Activity) context;
        this.shouldPrecache = this.config.getShouldPrecache(activity);
        this.applicationContext = new WeakReference<>(activity.getApplicationContext());
        this.activityContext = new WeakReference<>(activity);
        try {
            this.targetState = PHRequestState.DisplayingContent;
            continueToNextContentState(activity);
        } catch (Exception e) {
            PHCrashReport.reportCrash(e, "PHContentRequest - send", PHCrashReport.Urgency.critical);
        }
    }

    public void finish() {
        setCurrentContentState(PHRequestState.Done);
        super.finish();
    }

    public Hashtable<String, String> getAdditionalParams(Context context) {
        Hashtable<String, String> table = new Hashtable<>();
        table.put("placement_id", this.placement != null ? this.placement : "");
        table.put("preload", this.targetState == PHRequestState.Preloaded ? "1" : "0");
        table.put("stime", String.valueOf(PHSession.getInstance(context).getSessionTime()));
        return table;
    }

    public void handleRequestFailure(PHError error) {
        if (this.content_listener != null) {
            this.content_listener.onFailedToDisplayContent(this, new PHError("Could not get interstitial because: " + error.getMessage()));
        }
    }

    public void handleRequestSuccess(JSONObject response) {
        if (!JSONObject.NULL.equals(response) && response.length() != 0 && !response.equals("undefined")) {
            this.content = new PHContent(response);
            if (this.content.url == null) {
                setCurrentContentState(PHRequestState.Done);
                return;
            }
            setCurrentContentState(PHRequestState.Preloaded);
            if (this.content_listener != null) {
                this.content_listener.onReceivedContent(this, this.content);
            }
            if (this.activityContext != null && this.activityContext.get() != null) {
                continueToNextContentState(this.activityContext.get());
            }
        } else if (this.content_listener != null) {
            this.content_listener.onNoContent(this);
        }
    }
}
