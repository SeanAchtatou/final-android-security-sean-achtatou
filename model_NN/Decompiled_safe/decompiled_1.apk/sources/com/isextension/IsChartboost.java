package com.isextension;

import android.os.Bundle;
import android.util.Log;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.ChartboostDelegate;
import com.ideaworks3d.marmalade.LoaderActivity;

public class IsChartboost extends LoaderActivity {
    public static IsChartboost m_Activity = null;
    private String APP_ID = null;
    private String APP_SIGNATURE = null;
    private final String TAG = "CHARTBOOST";
    private Chartboost cb;
    private ChartboostDelegate chartBoostDelegate = new ChartboostDelegate() {
        public boolean shouldRequestInterstitial(String str) {
            Log.i("CHARTBOOST", "shouldRequestInterstitial");
            return true;
        }

        public boolean shouldDisplayInterstitial(String str) {
            Log.i("CHARTBOOST", "shouldDisplayInterstitial CB Set to " + IsChartboost.this.setCallback + " : " + str);
            if (!IsChartboost.this.setCallback.booleanValue()) {
                return true;
            }
            Boolean unused = IsChartboost.this.setCallback = false;
            Chartboost.sharedChartboost().cacheInterstitial();
            IsChartboost.this.IsChartboostRequestCallback(0);
            return false;
        }

        public void didCacheInterstitial(String str) {
        }

        public void didFailToLoadInterstitial(String str) {
            IsChartboost.this.IsChartboostRequestCallback(1);
            Log.i("CHARTBOOST", "INTERSTITIAL REQUEST FAILED");
        }

        public void didDismissInterstitial(String str) {
            IsChartboost.this.IsChartboostAdDismissedRequestCallback(1);
            Log.i("CHARTBOOST", "INTERSTITIAL DISMISSED");
        }

        public void didCloseInterstitial(String str) {
            IsChartboost.this.IsChartboostAdClosedCallback(1);
            Log.i("CHARTBOOST", "INSTERSTITIAL CLOSED");
        }

        public void didClickInterstitial(String str) {
            IsChartboost.this.IsChartboostAdClosedCallback(1);
            Log.i("CHARTBOOST", "DID CLICK INTERSTITIAL");
        }

        public void didShowInterstitial(String str) {
        }

        public boolean shouldDisplayLoadingViewForMoreApps() {
            return true;
        }

        public boolean shouldRequestMoreApps() {
            return true;
        }

        public void didCacheMoreApps() {
        }

        public boolean shouldDisplayMoreApps() {
            Log.i("CHARTBOOST", "SHOULD DISPLAY MORE APPS?");
            return true;
        }

        public void didFailToLoadMoreApps() {
            Log.i("CHARTBOOST", "MORE APPS REQUEST FAILED");
        }

        public void didDismissMoreApps() {
            Log.i("CHARTBOOST", "MORE APPS DISMISSED");
        }

        public void didCloseMoreApps() {
            Log.i("CHARTBOOST", "MORE APPS CLOSED");
        }

        public void didClickMoreApps() {
            Log.i("CHARTBOOST", "MORE APPS CLICKED");
        }

        public void didShowMoreApps() {
        }

        public boolean shouldRequestInterstitialsInFirstSession() {
            return true;
        }
    };
    /* access modifiers changed from: private */
    public Boolean setCallback = false;

    public native void IsChartboostAdClickedRequestCallback(int i);

    public native void IsChartboostAdClosedCallback(int i);

    public native void IsChartboostAdDismissedRequestCallback(int i);

    public native void IsChartboostRequestCallback(int i);

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        m_Activity = this;
        this.cb = Chartboost.sharedChartboost();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        Log.v("CHARTBOOST", "onPause");
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.v("CHARTBOOST", "onResume");
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.cb.onDestroy(this);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.cb.onStop(this);
    }

    public void onBackPressed() {
        if (!this.cb.onBackPressed()) {
            super.onBackPressed();
        }
    }

    public void IsChartboostCacheInterstitial(String str) {
        this.cb.cacheInterstitial(str);
    }

    public void IsChartboostSetAppID(String str) {
        if (str != null) {
            Log.i("CHARTBOOST", "AppID Called with " + str);
            this.APP_ID = str;
        }
    }

    public void IsChartboostSetAppSignature(String str) {
        if (str != null) {
            Log.i("CHARTBOOST", "AppSig Called with " + str);
            this.APP_SIGNATURE = str;
        }
    }

    public void IsChartboostStartSession() {
        this.cb.onCreate(this, this.APP_ID, this.APP_SIGNATURE, this.chartBoostDelegate);
        this.cb.startSession();
        this.cb.onStart(this);
    }

    public void IsChartboostRequestAd() {
        this.setCallback = true;
        this.cb.showInterstitial();
    }

    public void IsChartboostShowInterstitial(String str) {
        this.cb.showInterstitial();
    }

    public void IsChartboostCacheMoreApps() {
        this.cb.cacheMoreApps();
    }

    public void IsChartboostShowMoreApps() {
        this.cb.showMoreApps();
    }
}
