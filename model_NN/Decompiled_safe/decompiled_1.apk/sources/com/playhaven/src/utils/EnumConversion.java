package com.playhaven.src.utils;

import com.playhaven.src.publishersdk.content.PHContentView;
import com.playhaven.src.publishersdk.content.PHPublisherContentRequest;
import com.playhaven.src.publishersdk.content.PHPurchase;
import com.playhaven.src.publishersdk.purchases.PHPublisherIAPTrackingRequest;
import v2.com.playhaven.model.PHPurchase;
import v2.com.playhaven.requests.content.PHContentRequest;
import v2.com.playhaven.views.interstitial.PHCloseButton;

public class EnumConversion {
    public static PHPublisherContentRequest.PHDismissType convertToOldDismiss(PHContentRequest.PHDismissType type) {
        if (type == null) {
            return null;
        }
        switch (type) {
            case AdSelfDismiss:
                return PHPublisherContentRequest.PHDismissType.ContentUnitTriggered;
            case CloseButton:
                return PHPublisherContentRequest.PHDismissType.CloseButtonTriggered;
            case ApplicationBackgrounded:
                return PHPublisherContentRequest.PHDismissType.ApplicationTriggered;
            default:
                return null;
        }
    }

    public static PHPurchase.AndroidBillingResult convertToNewBillingResult(PHPurchase.Resolution resolution) {
        if (resolution == null) {
            return null;
        }
        switch (resolution) {
            case Buy:
                return PHPurchase.AndroidBillingResult.Bought;
            case Cancel:
                return PHPurchase.AndroidBillingResult.Cancelled;
            case Error:
                return PHPurchase.AndroidBillingResult.Failed;
            default:
                return null;
        }
    }

    public static PHPurchase.Resolution convertToOldBillingResult(PHPurchase.AndroidBillingResult resolution) {
        if (resolution == null) {
            return null;
        }
        switch (resolution) {
            case Bought:
                return PHPurchase.Resolution.Buy;
            case Cancelled:
                return PHPurchase.Resolution.Cancel;
            case Failed:
                return PHPurchase.Resolution.Error;
            default:
                return null;
        }
    }

    public static PHCloseButton.CloseButtonState convertToNewButtonState(PHContentView.ButtonState state) {
        if (state == null) {
            return null;
        }
        switch (state) {
            case Up:
                return PHCloseButton.CloseButtonState.Up;
            case Down:
                return PHCloseButton.CloseButtonState.Down;
            default:
                return null;
        }
    }

    public static PHPurchase.PHMarketplaceOrigin convertToNewOrigin(PHPublisherIAPTrackingRequest.PHPurchaseOrigin origin) {
        if (origin == null) {
            return null;
        }
        switch (origin) {
            case Google:
                return PHPurchase.PHMarketplaceOrigin.Google;
            case Amazon:
                return PHPurchase.PHMarketplaceOrigin.Amazon;
            case Motorola:
                return PHPurchase.PHMarketplaceOrigin.Motorola;
            case Paypal:
                return PHPurchase.PHMarketplaceOrigin.Paypal;
            case Crossmo:
                return PHPurchase.PHMarketplaceOrigin.Crossmo;
            default:
                return null;
        }
    }

    public static PHPublisherIAPTrackingRequest.PHPurchaseOrigin convertToOldOrigin(PHPurchase.PHMarketplaceOrigin origin) {
        if (origin == null) {
            return null;
        }
        switch (origin) {
            case Google:
                return PHPublisherIAPTrackingRequest.PHPurchaseOrigin.Google;
            case Amazon:
                return PHPublisherIAPTrackingRequest.PHPurchaseOrigin.Amazon;
            case Motorola:
                return PHPublisherIAPTrackingRequest.PHPurchaseOrigin.Motorola;
            case Paypal:
                return PHPublisherIAPTrackingRequest.PHPurchaseOrigin.Paypal;
            case Crossmo:
                return PHPublisherIAPTrackingRequest.PHPurchaseOrigin.Crossmo;
            default:
                return null;
        }
    }
}
