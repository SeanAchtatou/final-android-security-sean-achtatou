package com.playhaven.src.publishersdk.purchases;

import android.content.Context;
import com.playhaven.src.common.PHAPIRequest;
import com.playhaven.src.common.PHConfig;
import com.playhaven.src.publishersdk.content.PHPurchase;
import com.playhaven.src.utils.EnumConversion;
import java.lang.ref.WeakReference;
import java.util.Currency;
import java.util.Hashtable;
import v2.com.playhaven.configuration.PHConfiguration;
import v2.com.playhaven.model.PHError;
import v2.com.playhaven.requests.purchases.PHIAPTrackingRequest;

public class PHPublisherIAPTrackingRequest extends PHIAPTrackingRequest implements PHAPIRequest {
    private static Hashtable<String, String> cookies = new Hashtable<>();
    private WeakReference<Context> context;
    public Currency currencyLocale;
    public PHError error;
    public double price;
    public String product;
    public int quantity;
    public PHPurchase.Resolution resolution;
    public PHPurchaseOrigin store;

    public enum PHPurchaseOrigin {
        Google("GoogleMarketplace"),
        Amazon("AmazonAppstore"),
        Paypal("Paypal"),
        Crossmo("Crossmo"),
        Motorola("MotorolaAppstore");
        
        private String originStr;

        private PHPurchaseOrigin(String originStr2) {
            this.originStr = originStr2;
        }

        public String getOrigin() {
            return this.originStr;
        }
    }

    public PHPublisherIAPTrackingRequest(Context context2) {
        this.quantity = 0;
        this.store = PHPurchaseOrigin.Google;
        this.product = "";
        this.price = 0.0d;
        this.resolution = PHPurchase.Resolution.Cancel;
        this.context = new WeakReference<>(context2);
    }

    public PHPublisherIAPTrackingRequest(Context context2, PHAPIRequest.Delegate delegate) {
        super(new TrackingDelegateAdapter(delegate));
        this.quantity = 0;
        this.store = PHPurchaseOrigin.Google;
        this.product = "";
        this.price = 0.0d;
        this.resolution = PHPurchase.Resolution.Cancel;
    }

    public PHPublisherIAPTrackingRequest(Context context2, PHError error2) {
        this(context2);
        v2.com.playhaven.model.PHPurchase purchase = new v2.com.playhaven.model.PHPurchase();
        purchase.error = error2;
        super.setPurchase(purchase);
    }

    public PHPublisherIAPTrackingRequest(Context context2, PHPurchase purchase) {
        this(context2, purchase.product, purchase.quantity, purchase.resolution);
    }

    public PHPublisherIAPTrackingRequest(Context context2, String product_id, int quantity2, PHPurchase.Resolution resolution2) {
        this(context2);
        v2.com.playhaven.model.PHPurchase purchase = new v2.com.playhaven.model.PHPurchase();
        purchase.product = product_id;
        purchase.resolution = EnumConversion.convertToNewBillingResult(resolution2);
        purchase.quantity = quantity2;
        this.resolution = resolution2;
        this.product = product_id;
        this.quantity = quantity2;
        super.setPurchase(purchase);
    }

    public void setDelegate(PHAPIRequest.Delegate delegate) {
        super.setIAPListener(new TrackingDelegateAdapter(delegate));
    }

    public void send() {
        new PHConfiguration().setCredentials(this.context.get(), PHConfig.token, PHConfig.secret);
        super.send(this.context.get());
    }

    public Hashtable<String, String> getAdditionalParams(Context context2) {
        v2.com.playhaven.model.PHPurchase purchase = new v2.com.playhaven.model.PHPurchase();
        purchase.error = this.error;
        purchase.product = this.product;
        purchase.price = this.price;
        purchase.currencyLocale = this.currencyLocale;
        purchase.resolution = EnumConversion.convertToNewBillingResult(this.resolution);
        super.setPurchase(purchase);
        return super.getAdditionalParams(context2);
    }
}
