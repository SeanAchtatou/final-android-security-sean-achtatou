package com.playhaven.src.publishersdk.content.adapters;

import com.playhaven.src.publishersdk.content.PHPublisherContentRequest;
import v2.com.playhaven.listeners.PHRewardListener;
import v2.com.playhaven.model.PHReward;
import v2.com.playhaven.requests.content.PHContentRequest;

public class RewardDelegateAdapter implements PHRewardListener {
    private PHPublisherContentRequest.RewardDelegate delegate;

    public RewardDelegateAdapter(PHPublisherContentRequest.RewardDelegate delegate2) {
        this.delegate = delegate2;
    }

    public void onUnlockedReward(PHContentRequest request, PHReward reward) {
        this.delegate.unlockedReward((PHPublisherContentRequest) request, new com.playhaven.src.publishersdk.content.PHReward(reward));
    }
}
