package com.playhaven.src.publishersdk.metadata;

import com.playhaven.src.common.PHAPIRequest;
import org.json.JSONObject;
import v2.com.playhaven.listeners.PHBadgeRequestListener;
import v2.com.playhaven.model.PHError;
import v2.com.playhaven.requests.badge.PHBadgeRequest;

public class MetadataDelegateAdapter implements PHBadgeRequestListener {
    private PHAPIRequest.Delegate delegate;

    public MetadataDelegateAdapter(PHAPIRequest.Delegate delegate2) {
        this.delegate = delegate2;
    }

    public void onBadgeRequestSucceeded(PHBadgeRequest request, JSONObject responseData) {
        this.delegate.requestSucceeded((PHPublisherMetadataRequest) request, responseData);
    }

    public void onBadgeRequestFailed(PHBadgeRequest request, PHError error) {
        this.delegate.requestFailed((PHPublisherMetadataRequest) request, new Exception(error.getMessage()));
    }
}
