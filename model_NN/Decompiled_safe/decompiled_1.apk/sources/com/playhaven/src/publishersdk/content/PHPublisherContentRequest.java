package com.playhaven.src.publishersdk.content;

import android.app.Activity;
import android.graphics.Bitmap;
import com.playhaven.src.common.PHAPIRequest;
import com.playhaven.src.common.PHConfig;
import com.playhaven.src.publishersdk.content.PHContentView;
import com.playhaven.src.publishersdk.content.adapters.ContentDelegateAdapter;
import com.playhaven.src.publishersdk.content.adapters.PurchaseDelegateAdapter;
import com.playhaven.src.publishersdk.content.adapters.RewardDelegateAdapter;
import java.lang.ref.WeakReference;
import v2.com.playhaven.configuration.PHConfiguration;
import v2.com.playhaven.requests.content.PHContentRequest;
import v2.com.playhaven.utils.PHStringUtil;
import v2.com.playhaven.views.interstitial.PHCloseButton;

public class PHPublisherContentRequest extends PHContentRequest implements PHAPIRequest {
    private ContentDelegateAdapter content_adapter;
    public WeakReference<Activity> context;
    private CustomizeDelegate customize_delegate;

    public interface ContentDelegate extends PHAPIRequest.Delegate {
        void didDismissContent(PHPublisherContentRequest pHPublisherContentRequest, PHDismissType pHDismissType);

        void didDisplayContent(PHPublisherContentRequest pHPublisherContentRequest, PHContent pHContent);

        void willDisplayContent(PHPublisherContentRequest pHPublisherContentRequest, PHContent pHContent);

        void willGetContent(PHPublisherContentRequest pHPublisherContentRequest);
    }

    public interface CustomizeDelegate {
        int borderColor(PHPublisherContentRequest pHPublisherContentRequest, PHContent pHContent);

        Bitmap closeButton(PHPublisherContentRequest pHPublisherContentRequest, PHContentView.ButtonState buttonState);
    }

    public interface FailureDelegate {
        void contentDidFail(PHPublisherContentRequest pHPublisherContentRequest, Exception exc);

        void didFail(PHPublisherContentRequest pHPublisherContentRequest, String str);
    }

    public enum PHDismissType {
        ContentUnitTriggered,
        CloseButtonTriggered,
        ApplicationTriggered,
        NoContentTriggered
    }

    public interface PurchaseDelegate {
        void shouldMakePurchase(PHPublisherContentRequest pHPublisherContentRequest, PHPurchase pHPurchase);
    }

    public interface RewardDelegate {
        void unlockedReward(PHPublisherContentRequest pHPublisherContentRequest, PHReward pHReward);
    }

    public PHPublisherContentRequest(Activity activity, String placement) {
        super(placement);
        this.context = new WeakReference<>(activity);
    }

    public PHPublisherContentRequest(Activity activity, ContentDelegate delegate, String placement) {
        this(activity, placement);
        setDelegates(delegate);
    }

    public void setOnContentListener(ContentDelegate content_listener) {
        FailureDelegate existing_failure_delegate = null;
        if (this.content_adapter != null) {
            existing_failure_delegate = this.content_adapter.getFailureDelegate();
        }
        PHAPIRequest.Delegate existing_request_delegate = null;
        if (this.content_adapter != null) {
            existing_request_delegate = this.content_adapter.getRequestDelegate();
        }
        ContentDelegateAdapter contentDelegateAdapter = new ContentDelegateAdapter(content_listener, existing_failure_delegate, existing_request_delegate);
        this.content_adapter = contentDelegateAdapter;
        super.setOnContentListener(contentDelegateAdapter);
    }

    public void setOnRewardListener(RewardDelegate reward_listener) {
        super.setOnRewardListener(new RewardDelegateAdapter(reward_listener));
    }

    public void setOnPurchaseListener(PurchaseDelegate purchase_listener) {
        super.setOnPurchaseListener(new PurchaseDelegateAdapter(purchase_listener));
    }

    public void setOnCustomizeListener(CustomizeDelegate customize_listener) {
        this.customize_delegate = customize_listener;
    }

    public void setOnFailureListener(FailureDelegate failure_listener) {
        ContentDelegate existing_content_delegate = null;
        if (this.content_adapter != null) {
            existing_content_delegate = this.content_adapter.getContentDelegate();
        }
        PHAPIRequest.Delegate existing_request_delegate = null;
        if (this.content_adapter != null) {
            existing_request_delegate = this.content_adapter.getRequestDelegate();
        }
        ContentDelegateAdapter contentDelegateAdapter = new ContentDelegateAdapter(existing_content_delegate, failure_listener, existing_request_delegate);
        this.content_adapter = contentDelegateAdapter;
        super.setOnContentListener(contentDelegateAdapter);
    }

    public void setDelegates(Object delegate) {
        if (delegate instanceof RewardDelegate) {
            super.setOnRewardListener(new RewardDelegateAdapter((RewardDelegate) delegate));
        } else {
            PHStringUtil.log("*** RewardDelegate is not implemented. If you are using rewards this needs to be implemented.");
        }
        if (delegate instanceof PurchaseDelegate) {
            super.setOnPurchaseListener(new PurchaseDelegateAdapter((PurchaseDelegate) delegate));
        } else {
            PHStringUtil.log("*** PurchaseDelegate is not implemented. If you are using VGP this needs to be implemented.");
        }
        if (delegate instanceof CustomizeDelegate) {
            this.customize_delegate = (CustomizeDelegate) delegate;
        } else {
            this.customize_delegate = null;
            PHStringUtil.log("*** CustomizeDelegate is not implemented, using Play Haven close button bitmap. Implement to use own close button bitmap.");
        }
        if (delegate instanceof FailureDelegate) {
            super.setOnContentListener(new ContentDelegateAdapter(null, (FailureDelegate) delegate, null));
        } else {
            PHStringUtil.log("*** FailureDelegate is not implemented. Implement if want to be notified of failed content downloads.");
        }
        if (delegate instanceof ContentDelegate) {
            super.setOnContentListener(new ContentDelegateAdapter((ContentDelegate) delegate, null, null));
        } else {
            PHStringUtil.log("*** ContentDelegate is not implemented. Implement if want to be notified of content request states.");
        }
        if (delegate instanceof PHAPIRequest.Delegate) {
            super.setOnContentListener(new ContentDelegateAdapter(null, null, (PHAPIRequest.Delegate) delegate));
        }
        if ((delegate instanceof ContentDelegate) && (delegate instanceof FailureDelegate)) {
            super.setOnContentListener(new ContentDelegateAdapter((ContentDelegate) delegate, (FailureDelegate) delegate, null));
        }
    }

    public static boolean didDismissContentWithin(long range) {
        return PHContentRequest.didDismissContentWithin(Long.valueOf(range));
    }

    public void setOverlayImmediately(boolean doOverlay) {
    }

    public boolean getOverlayImmediately() {
        return false;
    }

    private void getCloseButtonImages() {
        if (this.customize_delegate != null) {
            Bitmap active = this.customize_delegate.closeButton(this, PHContentView.ButtonState.Down);
            Bitmap inactive = this.customize_delegate.closeButton(this, PHContentView.ButtonState.Up);
            if (active != null && inactive != null) {
                super.setCloseButton(active, PHCloseButton.CloseButtonState.Down);
                super.setCloseButton(inactive, PHCloseButton.CloseButtonState.Up);
            }
        }
    }

    public void setDelegate(PHAPIRequest.Delegate delegate) {
        FailureDelegate existing_failure_delegate = null;
        if (this.content_adapter != null) {
            existing_failure_delegate = this.content_adapter.getFailureDelegate();
        }
        if (ContentDelegate.class.isInstance(delegate)) {
            setOnContentListener(new ContentDelegateAdapter((ContentDelegate) delegate, existing_failure_delegate, null));
        } else {
            setOnContentListener(new ContentDelegateAdapter(null, existing_failure_delegate, delegate));
        }
    }

    public void preload() {
        getCloseButtonImages();
        PHConfiguration config = new PHConfiguration();
        config.setCredentials(this.context.get(), PHConfig.token, PHConfig.secret);
        config.setShouldPrecache(this.context.get(), PHConfig.precache);
        super.preload(this.context.get());
    }

    public void send() {
        getCloseButtonImages();
        PHConfiguration config = new PHConfiguration();
        config.setCredentials(this.context.get(), PHConfig.token, PHConfig.secret);
        config.setShouldPrecache(this.context.get(), PHConfig.precache);
        super.send(this.context.get());
    }
}
