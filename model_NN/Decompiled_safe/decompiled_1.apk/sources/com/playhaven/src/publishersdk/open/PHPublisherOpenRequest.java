package com.playhaven.src.publishersdk.open;

import android.content.Context;
import com.playhaven.src.common.PHAPIRequest;
import com.playhaven.src.common.PHConfig;
import java.lang.ref.WeakReference;
import v2.com.playhaven.configuration.PHConfiguration;
import v2.com.playhaven.requests.open.PHOpenRequest;

public class PHPublisherOpenRequest extends PHOpenRequest implements PHAPIRequest {
    private WeakReference<Context> context;

    public interface PrefetchListener {
        void prefetchFinished(PHPublisherOpenRequest pHPublisherOpenRequest);
    }

    public void setPrefetchListener(PrefetchListener delegate) {
        super.setPrefetchListener(new PrefetchDelegateAdapter(delegate));
    }

    public PHPublisherOpenRequest(Context context2, PHAPIRequest.Delegate delegate) {
        this(context2);
        setDelegate(delegate);
    }

    public PHPublisherOpenRequest(Context context2) {
        this.context = new WeakReference<>(context2);
    }

    public void send() {
        new PHConfiguration().setCredentials(this.context.get(), PHConfig.token, PHConfig.secret);
        super.send(this.context.get());
    }

    public void setDelegate(PHAPIRequest.Delegate delegate) {
        setOpenRequestListener(new APIRequestDelegateAdapter(delegate));
    }
}
