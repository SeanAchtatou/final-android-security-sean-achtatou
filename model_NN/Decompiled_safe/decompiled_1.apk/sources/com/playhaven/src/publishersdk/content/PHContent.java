package com.playhaven.src.publishersdk.content;

public class PHContent extends v2.com.playhaven.model.PHContent {
    public PHContent(v2.com.playhaven.model.PHContent content) {
        this.transition = content.transition;
        this.closeURL = content.closeURL;
        this.context = content.context;
        this.url = content.url;
        this.closeButtonDelay = content.closeButtonDelay;
        this.preloaded = content.preloaded;
        setFrames(content.getFrames());
    }
}
