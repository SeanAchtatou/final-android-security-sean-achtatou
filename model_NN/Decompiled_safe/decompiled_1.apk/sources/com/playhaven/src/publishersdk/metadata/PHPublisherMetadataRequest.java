package com.playhaven.src.publishersdk.metadata;

import android.content.Context;
import com.playhaven.src.common.PHAPIRequest;
import com.playhaven.src.common.PHConfig;
import java.lang.ref.WeakReference;
import v2.com.playhaven.configuration.PHConfiguration;
import v2.com.playhaven.requests.badge.PHBadgeRequest;

public class PHPublisherMetadataRequest extends PHBadgeRequest implements PHAPIRequest {
    private WeakReference<Context> context;

    public PHPublisherMetadataRequest(Context context2, String placement) {
        super(placement);
        this.context = new WeakReference<>(context2);
    }

    public PHPublisherMetadataRequest(Context context2, PHAPIRequest.Delegate delegate, String placement) {
        this(context2, placement);
        super.setMetadataListener(new MetadataDelegateAdapter(delegate));
    }

    public void send() {
        new PHConfiguration().setCredentials(this.context.get(), PHConfig.token, PHConfig.secret);
        super.send(this.context.get());
    }

    public void setDelegate(PHAPIRequest.Delegate delegate) {
        super.setMetadataListener(new MetadataDelegateAdapter(delegate));
    }
}
