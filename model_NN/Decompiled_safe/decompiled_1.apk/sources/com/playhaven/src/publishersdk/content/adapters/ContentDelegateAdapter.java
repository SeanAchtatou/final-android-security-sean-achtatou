package com.playhaven.src.publishersdk.content.adapters;

import com.playhaven.src.common.PHAPIRequest;
import com.playhaven.src.publishersdk.content.PHPublisherContentRequest;
import com.playhaven.src.utils.EnumConversion;
import v2.com.playhaven.listeners.PHContentRequestListener;
import v2.com.playhaven.model.PHContent;
import v2.com.playhaven.model.PHError;
import v2.com.playhaven.requests.content.PHContentRequest;

public class ContentDelegateAdapter implements PHContentRequestListener {
    private PHPublisherContentRequest.ContentDelegate content_delegate;
    private PHPublisherContentRequest.FailureDelegate failure_delegate;
    private PHAPIRequest.Delegate request_delegate;

    public ContentDelegateAdapter(PHPublisherContentRequest.ContentDelegate content_delegate2, PHPublisherContentRequest.FailureDelegate failure_delegate2, PHAPIRequest.Delegate delegate) {
        this.content_delegate = content_delegate2;
        this.failure_delegate = failure_delegate2;
        this.request_delegate = delegate;
    }

    public boolean hasFailureDelegate() {
        return this.failure_delegate != null;
    }

    public boolean hasContentDelegate() {
        return this.content_delegate != null;
    }

    public boolean hasRequestDelegate() {
        return this.request_delegate != null;
    }

    public PHPublisherContentRequest.ContentDelegate getContentDelegate() {
        return this.content_delegate;
    }

    public PHPublisherContentRequest.FailureDelegate getFailureDelegate() {
        return this.failure_delegate;
    }

    public PHAPIRequest.Delegate getRequestDelegate() {
        return this.request_delegate;
    }

    public void onSentContentRequest(PHContentRequest request) {
        if (this.content_delegate != null) {
            this.content_delegate.willGetContent((PHPublisherContentRequest) request);
        }
    }

    public void onReceivedContent(PHContentRequest request, PHContent content) {
        if (this.request_delegate != null) {
            this.request_delegate.requestSucceeded((PHAPIRequest) request, content.context);
        } else if (this.content_delegate != null) {
            this.content_delegate.requestSucceeded((PHAPIRequest) request, content.context);
        }
    }

    public void onWillDisplayContent(PHContentRequest request, PHContent content) {
        if (this.content_delegate != null) {
            this.content_delegate.willDisplayContent((PHPublisherContentRequest) request, new com.playhaven.src.publishersdk.content.PHContent(content));
        }
    }

    public void onDisplayedContent(PHContentRequest request, PHContent content) {
        if (this.content_delegate != null) {
            this.content_delegate.didDisplayContent((PHPublisherContentRequest) request, new com.playhaven.src.publishersdk.content.PHContent(content));
        }
    }

    public void onDismissedContent(PHContentRequest request, PHContentRequest.PHDismissType type) {
        if (this.content_delegate != null) {
            this.content_delegate.didDismissContent((PHPublisherContentRequest) request, EnumConversion.convertToOldDismiss(type));
        }
    }

    public void onFailedToDisplayContent(PHContentRequest request, PHError error) {
        if (this.failure_delegate != null) {
            this.failure_delegate.didFail((PHPublisherContentRequest) request, error.getMessage());
            this.failure_delegate.contentDidFail((PHPublisherContentRequest) request, new Exception(error.getMessage()));
        } else if (this.request_delegate != null) {
            this.request_delegate.requestFailed((PHPublisherContentRequest) request, new Exception(error.getMessage()));
        } else if (this.content_delegate != null) {
            this.content_delegate.requestFailed((PHPublisherContentRequest) request, new Exception(error.getMessage()));
        }
    }

    public void onNoContent(PHContentRequest request) {
        if (this.content_delegate != null) {
            this.content_delegate.didDismissContent((PHPublisherContentRequest) request, PHPublisherContentRequest.PHDismissType.NoContentTriggered);
        }
    }
}
