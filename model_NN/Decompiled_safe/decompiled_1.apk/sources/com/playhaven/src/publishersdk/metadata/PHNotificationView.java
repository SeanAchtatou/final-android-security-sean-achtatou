package com.playhaven.src.publishersdk.metadata;

import android.content.Context;
import v2.com.playhaven.views.badge.PHBadgeView;

public class PHNotificationView extends PHBadgeView {
    public PHNotificationView(Context context, String placement) {
        super(context, placement);
    }
}
