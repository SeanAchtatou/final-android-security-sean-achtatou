package com.playhaven.src.publishersdk.content.adapters;

import com.playhaven.src.publishersdk.content.PHPublisherContentRequest;
import v2.com.playhaven.listeners.PHPurchaseListener;
import v2.com.playhaven.model.PHPurchase;
import v2.com.playhaven.requests.content.PHContentRequest;

public class PurchaseDelegateAdapter implements PHPurchaseListener {
    private PHPublisherContentRequest.PurchaseDelegate delegate;

    public PurchaseDelegateAdapter(PHPublisherContentRequest.PurchaseDelegate delegate2) {
        this.delegate = delegate2;
    }

    public void onMadePurchase(PHContentRequest request, PHPurchase purchase) {
        this.delegate.shouldMakePurchase((PHPublisherContentRequest) request, new com.playhaven.src.publishersdk.content.PHPurchase(purchase));
    }
}
