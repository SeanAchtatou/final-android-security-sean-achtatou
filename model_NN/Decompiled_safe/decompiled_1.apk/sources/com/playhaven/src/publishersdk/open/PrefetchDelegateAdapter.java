package com.playhaven.src.publishersdk.open;

import com.playhaven.src.publishersdk.open.PHPublisherOpenRequest;
import v2.com.playhaven.listeners.PHPrefetchListener;
import v2.com.playhaven.requests.open.PHOpenRequest;

public class PrefetchDelegateAdapter implements PHPrefetchListener {
    private PHPublisherOpenRequest.PrefetchListener delegate;

    public PrefetchDelegateAdapter(PHPublisherOpenRequest.PrefetchListener delegate2) {
        this.delegate = delegate2;
    }

    public void onPrefetchFinished(PHOpenRequest request) {
        this.delegate.prefetchFinished((PHPublisherOpenRequest) request);
    }
}
