package com.playhaven.src.publishersdk.open;

import com.playhaven.src.common.PHAPIRequest;
import org.json.JSONObject;
import v2.com.playhaven.listeners.PHOpenRequestListener;
import v2.com.playhaven.model.PHError;
import v2.com.playhaven.requests.open.PHOpenRequest;

public class APIRequestDelegateAdapter implements PHOpenRequestListener {
    private PHAPIRequest.Delegate delegate;

    public APIRequestDelegateAdapter(PHAPIRequest.Delegate delegate2) {
        this.delegate = delegate2;
    }

    public void onOpenSuccessful(PHOpenRequest request) {
        this.delegate.requestSucceeded((PHPublisherOpenRequest) request, new JSONObject());
    }

    public void onOpenFailed(PHOpenRequest request, PHError error) {
        this.delegate.requestFailed((PHPublisherOpenRequest) request, new Exception(error.getMessage()));
    }
}
