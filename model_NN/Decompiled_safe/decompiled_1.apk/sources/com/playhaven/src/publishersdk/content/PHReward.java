package com.playhaven.src.publishersdk.content;

public class PHReward extends v2.com.playhaven.model.PHReward {
    public PHReward(v2.com.playhaven.model.PHReward reward) {
        this.name = reward.name;
        this.quantity = reward.quantity;
        this.receipt = reward.receipt;
    }
}
