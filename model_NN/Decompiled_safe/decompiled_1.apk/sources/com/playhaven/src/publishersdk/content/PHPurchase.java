package com.playhaven.src.publishersdk.content;

import android.app.Activity;
import com.playhaven.src.utils.EnumConversion;

public class PHPurchase extends v2.com.playhaven.model.PHPurchase {
    public int quantity;
    public Resolution resolution;

    public PHPurchase(String tag) {
        super(tag);
    }

    public PHPurchase(v2.com.playhaven.model.PHPurchase purchase) {
        super(purchase.getTag());
        this.quantity = 0;
        this.resolution = purchase.resolution;
        this.resolution = EnumConversion.convertToOldBillingResult(purchase.resolution);
        this.price = purchase.price;
        this.callback = purchase.callback;
        this.currencyLocale = purchase.currencyLocale;
        this.receipt = purchase.receipt;
        this.name = purchase.name;
        this.product = purchase.product;
        this.marketplace = purchase.marketplace;
        this.error = purchase.error;
    }

    public enum Resolution {
        Buy("buy"),
        Cancel("cancel"),
        Error("error");
        
        private String type;

        private Resolution(String type2) {
            this.type = type2;
        }

        public String getType() {
            return this.type;
        }
    }

    public void reportResolution(Resolution resolution2, Activity context) {
        this.resolution = resolution2;
        super.reportAndroidBillingResult(EnumConversion.convertToNewBillingResult(resolution2), context);
    }
}
