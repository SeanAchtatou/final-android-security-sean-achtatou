package com.playhaven.src.publishersdk.purchases;

import com.playhaven.src.common.PHAPIRequest;
import org.json.JSONObject;
import v2.com.playhaven.listeners.PHIAPRequestListener;
import v2.com.playhaven.model.PHError;
import v2.com.playhaven.requests.purchases.PHIAPTrackingRequest;

public class TrackingDelegateAdapter implements PHIAPRequestListener {
    private PHAPIRequest.Delegate delegate;

    public TrackingDelegateAdapter(PHAPIRequest.Delegate delegate2) {
        this.delegate = delegate2;
    }

    public void onIAPRequestSucceeded(PHIAPTrackingRequest request) {
        this.delegate.requestSucceeded((PHPublisherIAPTrackingRequest) request, new JSONObject());
    }

    public void onIAPRequestFailed(PHIAPTrackingRequest request, PHError error) {
        this.delegate.requestFailed((PHPublisherIAPTrackingRequest) request, new Exception(error.getMessage()));
    }
}
