package com.playhaven.src.common;

import android.content.Context;
import v2.com.playhaven.configuration.Version;

public class PHConfig {
    public static String api = "";
    public static boolean precache = true;
    public static boolean runningTests;
    public static String sdk_version = Version.PROJECT_VERSION;
    public static String secret;
    public static String token;

    public static void cacheDeviceInfo(Context context) {
    }
}
