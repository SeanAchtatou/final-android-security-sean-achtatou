package com.playhaven.src.common;

import org.json.JSONObject;

public interface PHAPIRequest {

    public interface Delegate {
        void requestFailed(PHAPIRequest pHAPIRequest, Exception exc);

        void requestSucceeded(PHAPIRequest pHAPIRequest, JSONObject jSONObject);
    }

    int getRequestTag();

    void send();

    void setDelegate(Delegate delegate);
}
