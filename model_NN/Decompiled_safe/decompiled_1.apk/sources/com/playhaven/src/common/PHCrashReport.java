package com.playhaven.src.common;

import android.content.Context;
import com.playhaven.src.common.PHAPIRequest;
import java.lang.ref.WeakReference;
import v2.com.playhaven.requests.crashreport.PHCrashReport;

public class PHCrashReport extends v2.com.playhaven.requests.crashreport.PHCrashReport implements PHAPIRequest {
    private WeakReference<Context> context;

    public PHCrashReport() {
    }

    public PHCrashReport(Exception e, PHCrashReport.Urgency level) {
        super(e, level);
    }

    public PHCrashReport(Exception e, String tag, PHCrashReport.Urgency level) {
        super(e, tag, level);
    }

    public static PHCrashReport reportCrash(Exception e, String tag, PHCrashReport.Urgency level) {
        return null;
    }

    public static PHCrashReport reportCrash(Exception e, PHCrashReport.Urgency level) {
        return null;
    }

    public void setDelegate(PHAPIRequest.Delegate delegate) {
    }

    public void send() {
    }
}
