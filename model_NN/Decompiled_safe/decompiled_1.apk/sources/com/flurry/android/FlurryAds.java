package com.flurry.android;

import android.content.Context;
import android.view.ViewGroup;
import com.flurry.android.impl.ads.FlurryAdModule;
import com.flurry.android.monolithic.sdk.impl.ja;
import java.util.Map;

public class FlurryAds {
    private static final String a = FlurryAds.class.getSimpleName();

    private FlurryAds() {
    }

    @Deprecated
    public static boolean getAd(Context context, String str, ViewGroup viewGroup, FlurryAdSize flurryAdSize, long j) {
        if (context == null) {
            ja.b(a, "Context passed to getAd was null.");
            return false;
        } else if (str == null) {
            ja.b(a, "Ad space name passed to getAd was null.");
            return false;
        } else if (str.length() == 0) {
            ja.b(a, "Ad space name passed to getAd was empty.");
            return false;
        } else if (viewGroup == null) {
            ja.b(a, "ViewGroup passed to getAd was null.");
            return false;
        } else if (flurryAdSize == null) {
            ja.b(a, "FlurryAdSize passed to getAd was null.");
            return false;
        } else {
            FlurryAdModule.getInstance().a(context);
            try {
                return FlurryAdModule.getInstance().b().a(context, str, flurryAdSize, viewGroup, j);
            } catch (Throwable th) {
                ja.b(a, "Exception while getting Ad : ", th);
                return false;
            }
        }
    }

    public static void initializeAds(Context context) {
        if (context == null) {
            ja.b(a, "Context passed to initializeAds was null.");
            return;
        }
        FlurryAdModule.getInstance().a(context);
        try {
            FlurryAdModule.getInstance().e(context);
        } catch (Throwable th) {
            ja.b(a, "Exception while initializing Ads: ", th);
        }
    }

    @Deprecated
    public static boolean isAdAvailable(Context context, String str, FlurryAdSize flurryAdSize, long j) {
        if (context == null) {
            ja.b(a, "Context passed to isAdAvailable was null.");
            return false;
        } else if (str == null) {
            ja.b(a, "Ad space name passed to isAdAvailable was null.");
            return false;
        } else if (str.length() == 0) {
            ja.b(a, "Ad space name passed to isAdAvailable was empty.");
            return false;
        } else if (flurryAdSize == null) {
            ja.b(a, "FlurryAdSize passed to isAdAvailable was null.");
            return false;
        } else {
            FlurryAdModule.getInstance().a(context);
            try {
                return FlurryAdModule.getInstance().b().a(str, flurryAdSize, j);
            } catch (Throwable th) {
                ja.b(a, "Exception while checking Ads if available: ", th);
                return false;
            }
        }
    }

    public static boolean isAdReady(String str) {
        if (str == null) {
            ja.b(a, "Ad space name passed to isAdReady was null.");
            return false;
        } else if (str.length() != 0) {
            return FlurryAdModule.getInstance().b().b(str);
        } else {
            ja.b(a, "Ad space name passed to isAdReady was empty.");
            return false;
        }
    }

    public static void fetchAd(Context context, String str, ViewGroup viewGroup, FlurryAdSize flurryAdSize) {
        if (context == null) {
            ja.b(a, "Context passed to fetchAd was null.");
        } else if (str == null) {
            ja.b(a, "Ad space name passed to fetchAd was null.");
        } else if (str.length() == 0) {
            ja.b(a, "Ad space name passed to fetchAd was empty.");
        } else if (viewGroup == null) {
            ja.b(a, "ViewGroup passed to fetchAd was null.");
        } else if (flurryAdSize == null) {
            ja.b(a, "FlurryAdSize passed to fetchAd was null.");
        } else {
            FlurryAdModule.getInstance().a(context);
            try {
                FlurryAdModule.getInstance().b().a(context, str, viewGroup, flurryAdSize);
            } catch (Throwable th) {
                ja.b(a, "Exception while fetching Ad: ", th);
            }
        }
    }

    public static void displayAd(Context context, String str, ViewGroup viewGroup) {
        if (context == null) {
            ja.b(a, "Context passed to displayAd was null.");
        } else if (str == null) {
            ja.b(a, "Ad space name passed to displayAd was null.");
        } else if (str.length() == 0) {
            ja.b(a, "Ad space name passed to displayAd was empty.");
        } else if (viewGroup == null) {
            ja.b(a, "ViewGroup  passed to displayAd was null.");
        } else {
            FlurryAdModule.getInstance().a(context);
            try {
                FlurryAdModule.getInstance().b().a(context, str, viewGroup);
            } catch (Throwable th) {
                ja.b(a, "Exception while displaying Ad: ", th);
            }
        }
    }

    public static void removeAd(Context context, String str, ViewGroup viewGroup) {
        if (context == null) {
            ja.b(a, "Context passed to removeAd was null.");
        } else if (str == null) {
            ja.b(a, "Ad space name passed to removeAd was null.");
        } else if (str.length() == 0) {
            ja.b(a, "Ad space name passed to removeAd was empty.");
        } else if (viewGroup == null) {
            ja.b(a, "ViewGroup passed to removeAd was null.");
        } else {
            FlurryAdModule.getInstance().a(context);
            try {
                FlurryAdModule.getInstance().b().a(context, str);
            } catch (Throwable th) {
                ja.b(a, "Exception while removing Ad: ", th);
            }
        }
    }

    public static void setAdListener(FlurryAdListener flurryAdListener) {
        FlurryAdModule.getInstance().a(flurryAdListener);
    }

    public static void setLocation(float f, float f2) {
        FlurryAdModule.getInstance().a(f, f2);
    }

    public static void clearLocation() {
        FlurryAdModule.getInstance().j();
    }

    public static void setCustomAdNetworkHandler(ICustomAdNetworkHandler iCustomAdNetworkHandler) {
        if (iCustomAdNetworkHandler == null) {
            ja.b(a, "ICustomAdNetworkHandler passed to setCustomAdNetworkHandler was null.");
        } else {
            FlurryAdModule.getInstance().a(iCustomAdNetworkHandler);
        }
    }

    public static void setUserCookies(Map<String, String> map) {
        if (map == null) {
            ja.b(a, "userCookies Map passed to setUserCookies was null.");
        } else {
            FlurryAdModule.getInstance().b(map);
        }
    }

    public static void clearUserCookies() {
        FlurryAdModule.getInstance().s();
    }

    public static void setTargetingKeywords(Map<String, String> map) {
        if (map == null) {
            ja.b(a, "targetingKeywords Map passed to setTargetingKeywords was null.");
        } else if (map != null) {
            FlurryAdModule.getInstance().c(map);
        }
    }

    public static void clearTargetingKeywords() {
        FlurryAdModule.getInstance().u();
    }

    public static void setAdServerUrl(String str) {
        FlurryAdModule.getInstance().a(str);
    }

    public static void setAdLogUrl(String str) {
        FlurryAdModule.getInstance().b(str);
    }

    public static void enableTestAds(boolean z) {
        FlurryAdModule.getInstance().a(z);
    }

    static void setFixedAdId(String str, CharSequence charSequence) {
        FlurryAdModule.getInstance().a(str, charSequence);
    }
}
