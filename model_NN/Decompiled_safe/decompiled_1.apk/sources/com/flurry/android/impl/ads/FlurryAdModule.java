package com.flurry.android.impl.ads;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.ViewGroup;
import com.flurry.android.AdCreative;
import com.flurry.android.AdNetworkView;
import com.flurry.android.FlurryAdListener;
import com.flurry.android.FlurryAdSize;
import com.flurry.android.FlurryAgent;
import com.flurry.android.FlurryFullscreenTakeoverActivity;
import com.flurry.android.ICustomAdNetworkHandler;
import com.flurry.android.impl.ads.avro.protocol.v6.AdFrame;
import com.flurry.android.impl.ads.avro.protocol.v6.AdReportedId;
import com.flurry.android.impl.ads.avro.protocol.v6.AdRequest;
import com.flurry.android.impl.ads.avro.protocol.v6.AdUnit;
import com.flurry.android.impl.ads.avro.protocol.v6.AdViewContainer;
import com.flurry.android.impl.ads.avro.protocol.v6.Callback;
import com.flurry.android.impl.ads.avro.protocol.v6.FrequencyCapInfo;
import com.flurry.android.impl.ads.avro.protocol.v6.Location;
import com.flurry.android.impl.ads.avro.protocol.v6.SdkAdLog;
import com.flurry.android.impl.ads.avro.protocol.v6.SdkLogRequest;
import com.flurry.android.impl.ads.avro.protocol.v6.SdkLogResponse;
import com.flurry.android.impl.ads.avro.protocol.v6.TestAds;
import com.flurry.android.monolithic.sdk.impl.ab;
import com.flurry.android.monolithic.sdk.impl.ac;
import com.flurry.android.monolithic.sdk.impl.ae;
import com.flurry.android.monolithic.sdk.impl.af;
import com.flurry.android.monolithic.sdk.impl.am;
import com.flurry.android.monolithic.sdk.impl.an;
import com.flurry.android.monolithic.sdk.impl.ay;
import com.flurry.android.monolithic.sdk.impl.bb;
import com.flurry.android.monolithic.sdk.impl.be;
import com.flurry.android.monolithic.sdk.impl.bh;
import com.flurry.android.monolithic.sdk.impl.bi;
import com.flurry.android.monolithic.sdk.impl.bn;
import com.flurry.android.monolithic.sdk.impl.bo;
import com.flurry.android.monolithic.sdk.impl.bp;
import com.flurry.android.monolithic.sdk.impl.br;
import com.flurry.android.monolithic.sdk.impl.bt;
import com.flurry.android.monolithic.sdk.impl.bv;
import com.flurry.android.monolithic.sdk.impl.bw;
import com.flurry.android.monolithic.sdk.impl.cd;
import com.flurry.android.monolithic.sdk.impl.ce;
import com.flurry.android.monolithic.sdk.impl.ch;
import com.flurry.android.monolithic.sdk.impl.ci;
import com.flurry.android.monolithic.sdk.impl.cj;
import com.flurry.android.monolithic.sdk.impl.cl;
import com.flurry.android.monolithic.sdk.impl.cm;
import com.flurry.android.monolithic.sdk.impl.cn;
import com.flurry.android.monolithic.sdk.impl.co;
import com.flurry.android.monolithic.sdk.impl.eg;
import com.flurry.android.monolithic.sdk.impl.i;
import com.flurry.android.monolithic.sdk.impl.ia;
import com.flurry.android.monolithic.sdk.impl.ie;
import com.flurry.android.monolithic.sdk.impl.ih;
import com.flurry.android.monolithic.sdk.impl.im;
import com.flurry.android.monolithic.sdk.impl.in;
import com.flurry.android.monolithic.sdk.impl.iw;
import com.flurry.android.monolithic.sdk.impl.ja;
import com.flurry.android.monolithic.sdk.impl.jb;
import com.flurry.android.monolithic.sdk.impl.jc;
import com.flurry.android.monolithic.sdk.impl.jd;
import com.flurry.android.monolithic.sdk.impl.je;
import com.flurry.android.monolithic.sdk.impl.jf;
import com.flurry.android.monolithic.sdk.impl.k;
import com.flurry.android.monolithic.sdk.impl.l;
import com.flurry.android.monolithic.sdk.impl.m;
import com.flurry.android.monolithic.sdk.impl.o;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FlurryAdModule implements cj, ih, jb {
    private static FlurryAdModule P;
    private static List<Integer> r;
    private static ae s;
    /* access modifiers changed from: private */
    public static final String t = FlurryAdModule.class.getSimpleName();
    private byte[] A = null;
    private boolean B = false;
    private volatile String C = null;
    private volatile String D = null;
    private m E;
    private AdUnit F;
    private final am G;
    private final co H;
    private WeakReference<FlurryAdListener> I = new WeakReference<>(null);
    private CharSequence J;
    private final List<Activity> K;
    private AdUnit L;
    private cl M;
    private final Map<String, String> N;
    private final Set<String> O;
    private boolean Q;
    private ch R;
    private Map<ie, ByteBuffer> S = new HashMap();
    private bv T;
    private bi U;
    /* access modifiers changed from: private */
    public bw V;
    public volatile Location a;
    volatile Map<String, String> b;
    long c;
    long d;
    long e;
    public String f;
    Handler g;
    ExecutorService h;
    ce i;
    public ICustomAdNetworkHandler j;
    int k;
    volatile List<m> l;
    volatile Map<String, m> m;
    volatile List<m> n;
    volatile List<String> o;
    volatile boolean p;
    Map<String, String> q;
    private boolean u = false;
    private boolean v = false;
    private File w = null;
    private File x = null;
    private String y;
    private String z;

    public FlurryAdModule() {
        jc.a().a(this);
        this.i = ce.a();
        this.T = new bv();
        this.V = new bw(this);
        this.l = new CopyOnWriteArrayList();
        this.m = new HashMap();
        this.n = new ArrayList();
        this.o = new ArrayList();
        this.p = false;
        HashMap hashMap = new HashMap();
        hashMap.put("open", "directOpen");
        hashMap.put("expand", "doExpand");
        hashMap.put("collapse", "doCollapse");
        this.N = Collections.unmodifiableMap(hashMap);
        HashSet hashSet = new HashSet();
        hashSet.addAll(Arrays.asList("notifyUser", "nextFrame", "closeAd", "expandAd", "collapseAd", "verifyURL"));
        this.O = Collections.unmodifiableSet(hashSet);
        HandlerThread handlerThread = new HandlerThread("FlurryAdThread");
        handlerThread.start();
        this.g = new Handler(handlerThread.getLooper());
        this.h = Executors.newSingleThreadExecutor();
        this.U = new bi(this);
        r = Arrays.asList(0, 1, 2, 3, 4, 5);
        s = new ae(this);
        this.y = Build.VERSION.RELEASE;
        this.z = Build.DEVICE;
        this.b = new HashMap();
        this.G = new bb();
        this.H = new be();
        this.K = new ArrayList();
    }

    public bi a() {
        return this.U;
    }

    public bw b() {
        return this.V;
    }

    public void a(jf jfVar) {
        this.g.post(jfVar);
    }

    public static synchronized FlurryAdModule getInstance() {
        FlurryAdModule flurryAdModule;
        synchronized (FlurryAdModule.class) {
            if (P == null) {
                P = new FlurryAdModule();
            }
            flurryAdModule = P;
        }
        return flurryAdModule;
    }

    public Future<?> b(jf jfVar) {
        return this.h.submit(jfVar);
    }

    public synchronized void a(Context context) {
        if (!this.Q) {
            this.w = context.getFileStreamPath(o());
            this.x = context.getFileStreamPath(p());
            this.U.a();
            this.A = im.a();
            b(new bn(this));
            this.Q = true;
        }
    }

    public void a(Context context, long j2, long j3) {
        this.R = new ch();
        this.c = j2;
        this.d = j3;
        this.e = 0;
        this.B = false;
    }

    public void b(Context context) {
        this.i.d();
    }

    public void c(Context context) {
        a(new bo(this));
        if (this.Q) {
            w();
        }
    }

    public void d(Context context) {
        this.V.a(context);
    }

    public void a(Context context, String str) {
        FlurryAdListener flurryAdListener;
        this.k++;
        if (1 == this.k && (flurryAdListener = this.I.get()) != null) {
            flurryAdListener.onAdOpened(str);
        }
    }

    public void b(Context context, String str) {
        FlurryAdListener flurryAdListener;
        this.k--;
        if (this.k == 0 && (flurryAdListener = this.I.get()) != null) {
            flurryAdListener.onAdClosed(str);
        }
    }

    public boolean c() {
        return this.k != 0;
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity) {
        AdUnit adUnit;
        int i2;
        if (1 == this.K.size()) {
            Activity activity2 = this.K.get(0);
            if (activity2 instanceof FlurryFullscreenTakeoverActivity) {
                FlurryFullscreenTakeoverActivity flurryFullscreenTakeoverActivity = (FlurryFullscreenTakeoverActivity) activity2;
                adUnit = flurryFullscreenTakeoverActivity.getAdUnit();
                o adUnityView = flurryFullscreenTakeoverActivity.getAdUnityView();
                i2 = adUnityView != null ? adUnityView.getAdFrameIndex() : 0;
            } else {
                adUnit = null;
                i2 = 0;
            }
            if (adUnit == null) {
                return;
            }
            if (adUnit.e().intValue() != 1 || i2 > 0) {
                if ((activity == null || !(activity instanceof FlurryFullscreenTakeoverActivity)) ? true : ((FlurryFullscreenTakeoverActivity) activity).getResult() != ay.WEB_RESULT_BACK) {
                    activity2.finish();
                }
            }
        }
    }

    public void a(Activity activity, Bundle bundle) {
        FlurryAdListener flurryAdListener;
        if (activity != null) {
            this.K.add(activity);
            Intent intent = activity.getIntent();
            String b2 = b(intent);
            a(activity, b2);
            if (a(intent) != null && (flurryAdListener = this.I.get()) != null) {
                flurryAdListener.onApplicationExit(b2);
            }
        }
    }

    public void b(Activity activity) {
        if (activity != null) {
            b(activity, b(activity.getIntent()));
            this.K.remove(activity);
            a(activity);
        }
    }

    public long d() {
        return this.c;
    }

    public static boolean e() {
        return ((KeyguardManager) ia.a().b().getSystemService("keyguard")).inKeyguardRestrictedInputMode();
    }

    public String f() {
        return eg.a().f();
    }

    /* access modifiers changed from: package-private */
    public String g() {
        return eg.a().g();
    }

    /* access modifiers changed from: package-private */
    public String h() {
        return eg.a().h();
    }

    private Location M() {
        float f2;
        float f3 = 0.0f;
        Location location = this.a;
        if (location != null) {
            return location;
        }
        android.location.Location e2 = in.a().e();
        if (e2 != null) {
            f3 = (float) e2.getLatitude();
            f2 = (float) e2.getLongitude();
        } else {
            f2 = 0.0f;
        }
        return Location.b().a(f3).b(f2).a();
    }

    public synchronized long i() {
        long elapsedRealtime = SystemClock.elapsedRealtime() - this.d;
        if (elapsedRealtime <= this.e) {
            elapsedRealtime = this.e + 1;
            this.e = elapsedRealtime;
        }
        this.e = elapsedRealtime;
        return this.e;
    }

    public void a(Map<ie, ByteBuffer> map) {
        this.S = map;
    }

    public void a(List<m> list) {
        this.n = list;
    }

    public void a(FlurryAdListener flurryAdListener) {
        this.I = new WeakReference<>(flurryAdListener);
    }

    public void a(float f2, float f3) {
        this.a = Location.b().a(f2).b(f3).a();
    }

    public void j() {
        this.a = null;
    }

    public void a(ICustomAdNetworkHandler iCustomAdNetworkHandler) {
        this.j = iCustomAdNetworkHandler;
    }

    public void a(String str, CharSequence charSequence) {
        this.J = charSequence;
        this.V.f(str);
    }

    public ICustomAdNetworkHandler k() {
        return this.j;
    }

    public String l() {
        return this.f;
    }

    public Map<ie, ByteBuffer> m() {
        return this.S;
    }

    /* access modifiers changed from: package-private */
    public Map<CharSequence, CharSequence> n() {
        String str;
        HashMap hashMap = new HashMap();
        for (Map.Entry next : this.q.entrySet()) {
            String str2 = (String) next.getKey();
            String str3 = (String) next.getValue();
            if (str2 == null) {
                str2 = "";
            }
            if (str3 == null) {
                str = "";
            } else {
                str = str3;
            }
            hashMap.put(str2, str);
        }
        return hashMap;
    }

    public synchronized void a(m mVar) {
        if (this.l.size() < 32767) {
            this.l.add(mVar);
            this.m.put(mVar.b(), mVar);
        }
    }

    public an a(Context context, ViewGroup viewGroup, String str) {
        return this.V.a(this, context, viewGroup, str);
    }

    /* access modifiers changed from: package-private */
    public Intent a(Intent intent) {
        if (intent == null) {
            return null;
        }
        return (Intent) intent.getParcelableExtra(FlurryFullscreenTakeoverActivity.EXTRA_KEY_TARGETINTENT);
    }

    /* access modifiers changed from: package-private */
    public String b(Intent intent) {
        if (intent == null) {
            return null;
        }
        return intent.getStringExtra(FlurryFullscreenTakeoverActivity.EXTRA_KEY_ADSPACENAME);
    }

    /* access modifiers changed from: package-private */
    public String o() {
        return ".flurryadlog." + Integer.toString(f().hashCode(), 16);
    }

    /* access modifiers changed from: package-private */
    public String p() {
        return ".flurryfreqcap." + Integer.toString(f().hashCode(), 16);
    }

    public void a(String str) {
        this.C = str;
    }

    public void b(String str) {
        this.D = str;
    }

    public void a(boolean z2) {
        this.p = z2;
    }

    /* access modifiers changed from: package-private */
    public String q() {
        if (this.C != null) {
            return this.C + "/v6/getAds.do";
        }
        if (FlurryAgent.getUseHttps()) {
            return "https://ads.flurry.com/v6/getAds.do";
        }
        return "http://ads.flurry.com/v6/getAds.do";
    }

    /* access modifiers changed from: package-private */
    public String r() {
        if (this.D != null) {
            return this.D;
        }
        if (FlurryAgent.getUseHttps()) {
            return "https://adlog.flurry.com";
        }
        return "http://adlog.flurry.com";
    }

    public void b(Map<String, String> map) {
        if (map != null && map.size() > 0) {
            for (Map.Entry next : map.entrySet()) {
                if (next.getKey() == null || next.getValue() == null) {
                    ja.e(t, "User cookie keys and values may not be null.");
                } else {
                    this.b.put(next.getKey(), next.getValue());
                }
            }
        }
    }

    public void s() {
        this.b.clear();
    }

    public Map<String, String> t() {
        return this.b;
    }

    public void c(Map<String, String> map) {
        this.q = map;
    }

    public void u() {
        this.q = null;
    }

    public Map<String, String> v() {
        Map<String, String> t2 = t();
        if (this.R != null) {
            String a2 = this.R.a();
            if (!TextUtils.isEmpty(a2)) {
                t2.put("appCloudUserId", a2);
            }
        }
        return t2;
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(SdkLogResponse sdkLogResponse) {
        if (sdkLogResponse.b().toString().equals("success")) {
            this.l.removeAll(this.n);
            ja.a(3, t, "removed reported AdLogs");
        }
    }

    public synchronized void e(Context context) {
        if (!this.B) {
            ja.c(t, "Initializing ads -- requesting ads with precaching enabled on the server");
            new br(this, "", null, true, FlurryAdSize.BANNER_BOTTOM, null).a();
            this.B = true;
        } else {
            ja.c(t, "Initializing ads -- not making a new ad request (for ads with precaching enabled on the server) since one was made already this session");
        }
    }

    public synchronized void w() {
        b(new bp(this));
    }

    public void x() {
        SdkLogResponse a2 = this.T.a(y(), r() + "/postAdLog.do");
        if (a2 != null) {
            try {
                b(a2);
            } catch (IOException e2) {
                ja.a(6, t, "Error in generateAndSendAdLogRequest: ", e2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public SdkLogRequest y() {
        List<AdReportedId> z2 = z();
        synchronized (this.l) {
            List<SdkAdLog> a2 = s.a(this.l);
            if (a2.size() == 0) {
                ja.a(3, t, "List of adLogs is empty");
                return null;
            }
            SdkLogRequest a3 = SdkLogRequest.b().a(f()).a(z2).b(a2).a(false).a(System.currentTimeMillis()).a();
            ja.a(3, t, "Got ad log request:" + a3.toString());
            return a3;
        }
    }

    /* access modifiers changed from: package-private */
    public List<AdReportedId> z() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(AdReportedId.b().a(ByteBuffer.wrap(l().getBytes())).a(eg.i()).a());
        byte[] bArr = this.A;
        if (bArr != null) {
            ja.a(3, t, "Fetched hashed IMEI");
            arrayList.add(AdReportedId.b().a(ByteBuffer.wrap(bArr)).a(ie.Sha1Imei.c).a());
        }
        for (Map.Entry next : m().entrySet()) {
            arrayList.add(AdReportedId.b().a((ByteBuffer) next.getValue()).a(((ie) next.getKey()).c).a());
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public List<FrequencyCapInfo> A() {
        ArrayList arrayList = new ArrayList();
        for (cd next : this.i.c()) {
            arrayList.add(FrequencyCapInfo.b().a(next.b()).b(next.h()).b(next.e()).c(next.f()).d(next.g()).a(next.i()).a(next.c()).a());
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public AdRequest a(String str, int i2, int i3, int i4, int i5, boolean z2, FlurryAdSize flurryAdSize) {
        List<AdReportedId> z3 = z();
        List<FrequencyCapInfo> A2 = A();
        ArrayList arrayList = new ArrayList();
        if (this.J != null && this.J.length() > 0) {
            arrayList.add("FLURRY_VIEWER");
            arrayList.add(this.J);
        }
        AdRequest a2 = AdRequest.b().a(f()).c("").b(r).a(z3).a(M()).a(this.p).b(Integer.toString(FlurryAgent.getAgentVersion())).a(this.c).a(AdViewContainer.b().d(i5).c(i4).b(i3).a(i2).a(je.a()).a()).d(g()).e(h()).f(this.y).g(this.z).b(false).a(N().a()).c(A2).h(Locale.getDefault().getLanguage()).d(arrayList).a();
        if (z2) {
            a2.a(Boolean.valueOf(z2));
        } else {
            a2.a(str);
        }
        if (flurryAdSize != null) {
            a2.a(TestAds.b().a(flurryAdSize.getValue()).a());
        }
        if (this.q != null) {
            a2.a(n());
        }
        ja.a(3, t, "Got ad request: " + a2);
        return a2;
    }

    /* access modifiers changed from: package-private */
    public void b(SdkLogResponse sdkLogResponse) throws IOException {
        ja.a(4, t, "Got ad log response: " + sdkLogResponse);
        if (sdkLogResponse.b().toString().equals("success")) {
            a(sdkLogResponse);
            return;
        }
        for (CharSequence obj : sdkLogResponse.c()) {
            ja.b(t, obj.toString());
        }
    }

    public synchronized void B() {
        DataOutputStream dataOutputStream;
        Throwable th;
        Throwable th2;
        try {
            if (!iw.a(this.w)) {
                je.a((Closeable) null);
            } else {
                dataOutputStream = new DataOutputStream(new FileOutputStream(this.w));
                try {
                    synchronized (this.l) {
                        l.a(this.l, dataOutputStream);
                    }
                    je.a(dataOutputStream);
                } catch (Throwable th3) {
                    th = th3;
                }
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            dataOutputStream = null;
            th2 = th5;
            je.a(dataOutputStream);
            throw th2;
        }
        try {
            ja.b(t, "Error when saving ad log data: ", th);
            je.a(dataOutputStream);
        } catch (Throwable th6) {
            th2 = th6;
            je.a(dataOutputStream);
            throw th2;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x002d A[Catch:{ Throwable -> 0x006c }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void C() {
        /*
            r5 = this;
            monitor-enter(r5)
            java.io.File r0 = r5.w     // Catch:{ Throwable -> 0x004e }
            boolean r0 = r0.exists()     // Catch:{ Throwable -> 0x004e }
            if (r0 == 0) goto L_0x0075
            r0 = 0
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x0080, all -> 0x005b }
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x0080, all -> 0x005b }
            java.io.File r3 = r5.w     // Catch:{ Throwable -> 0x0080, all -> 0x005b }
            r2.<init>(r3)     // Catch:{ Throwable -> 0x0080, all -> 0x005b }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x0080, all -> 0x005b }
            java.util.List r0 = com.flurry.android.monolithic.sdk.impl.l.a(r1)     // Catch:{ Throwable -> 0x0042 }
            java.util.List<com.flurry.android.monolithic.sdk.impl.m> r2 = r5.l     // Catch:{ Throwable -> 0x0042 }
            monitor-enter(r2)     // Catch:{ Throwable -> 0x0042 }
            java.util.List<com.flurry.android.monolithic.sdk.impl.m> r3 = r5.l     // Catch:{ all -> 0x003f }
            r3.addAll(r0)     // Catch:{ all -> 0x003f }
            monitor-exit(r2)     // Catch:{ all -> 0x003f }
            r0 = 1
            r5.u = r0     // Catch:{ Throwable -> 0x0042 }
            com.flurry.android.monolithic.sdk.impl.je.a(r1)     // Catch:{ Throwable -> 0x004e }
        L_0x0029:
            boolean r0 = r5.u     // Catch:{ Throwable -> 0x006c }
            if (r0 != 0) goto L_0x003d
            java.io.File r0 = r5.w     // Catch:{ Throwable -> 0x006c }
            boolean r0 = r0.delete()     // Catch:{ Throwable -> 0x006c }
            if (r0 == 0) goto L_0x0063
            r0 = 3
            java.lang.String r1 = com.flurry.android.impl.ads.FlurryAdModule.t     // Catch:{ Throwable -> 0x006c }
            java.lang.String r2 = "Deleted persistence adLogs file"
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r1, r2)     // Catch:{ Throwable -> 0x006c }
        L_0x003d:
            monitor-exit(r5)
            return
        L_0x003f:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x003f }
            throw r0     // Catch:{ Throwable -> 0x0042 }
        L_0x0042:
            r0 = move-exception
        L_0x0043:
            java.lang.String r2 = com.flurry.android.impl.ads.FlurryAdModule.t     // Catch:{ all -> 0x007e }
            java.lang.String r3 = "Error when loading ad log data: "
            com.flurry.android.monolithic.sdk.impl.ja.b(r2, r3, r0)     // Catch:{ all -> 0x007e }
            com.flurry.android.monolithic.sdk.impl.je.a(r1)     // Catch:{ Throwable -> 0x004e }
            goto L_0x0029
        L_0x004e:
            r0 = move-exception
            r1 = 6
            java.lang.String r2 = com.flurry.android.impl.ads.FlurryAdModule.t     // Catch:{ all -> 0x0058 }
            java.lang.String r3 = "Failed to load AdLogs cache file with exception:"
            com.flurry.android.monolithic.sdk.impl.ja.a(r1, r2, r3, r0)     // Catch:{ all -> 0x0058 }
            goto L_0x003d
        L_0x0058:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x005b:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x005f:
            com.flurry.android.monolithic.sdk.impl.je.a(r1)     // Catch:{ Throwable -> 0x004e }
            throw r0     // Catch:{ Throwable -> 0x004e }
        L_0x0063:
            r0 = 6
            java.lang.String r1 = com.flurry.android.impl.ads.FlurryAdModule.t     // Catch:{ Throwable -> 0x006c }
            java.lang.String r2 = "Cannot delete persistence adLogs file"
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r1, r2)     // Catch:{ Throwable -> 0x006c }
            goto L_0x003d
        L_0x006c:
            r0 = move-exception
            java.lang.String r1 = com.flurry.android.impl.ads.FlurryAdModule.t     // Catch:{ Throwable -> 0x004e }
            java.lang.String r2 = ""
            com.flurry.android.monolithic.sdk.impl.ja.b(r1, r2, r0)     // Catch:{ Throwable -> 0x004e }
            goto L_0x003d
        L_0x0075:
            r0 = 3
            java.lang.String r1 = com.flurry.android.impl.ads.FlurryAdModule.t     // Catch:{ Throwable -> 0x004e }
            java.lang.String r2 = "AdLogs cache file doesn't exist."
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r1, r2)     // Catch:{ Throwable -> 0x004e }
            goto L_0x003d
        L_0x007e:
            r0 = move-exception
            goto L_0x005f
        L_0x0080:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.impl.ads.FlurryAdModule.C():void");
    }

    public synchronized void D() {
        DataOutputStream dataOutputStream;
        Throwable th;
        Throwable th2;
        this.i.d();
        try {
            if (!iw.a(this.x)) {
                je.a((Closeable) null);
            } else {
                DataOutputStream dataOutputStream2 = new DataOutputStream(new FileOutputStream(this.x));
                try {
                    synchronized (this.i) {
                        a(this.i, dataOutputStream2);
                    }
                    dataOutputStream2.writeShort(0);
                    je.a(dataOutputStream2);
                } catch (Throwable th3) {
                    th2 = th3;
                    dataOutputStream = dataOutputStream2;
                    je.a(dataOutputStream);
                    throw th2;
                }
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            dataOutputStream = null;
            th2 = th5;
            je.a(dataOutputStream);
            throw th2;
        }
        try {
            ja.b(t, "", th);
            je.a(dataOutputStream);
        } catch (Throwable th6) {
            th2 = th6;
            je.a(dataOutputStream);
            throw th2;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0027 A[Catch:{ Throwable -> 0x0066 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x006f A[SYNTHETIC, Splitter:B:45:0x006f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void E() {
        /*
            r5 = this;
            monitor-enter(r5)
            java.lang.String r0 = com.flurry.android.impl.ads.FlurryAdModule.t     // Catch:{ all -> 0x0052 }
            java.lang.String r1 = "Attempting to load FreqCap data"
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r1)     // Catch:{ all -> 0x0052 }
            java.io.File r0 = r5.x     // Catch:{ Throwable -> 0x0048 }
            boolean r0 = r0.exists()     // Catch:{ Throwable -> 0x0048 }
            if (r0 == 0) goto L_0x0075
            r0 = 0
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x0039, all -> 0x0055 }
            java.io.File r2 = r5.x     // Catch:{ Throwable -> 0x0039, all -> 0x0055 }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x0039, all -> 0x0055 }
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x0039, all -> 0x0055 }
            r2.<init>(r1)     // Catch:{ Throwable -> 0x0039, all -> 0x0055 }
            r5.a(r2)     // Catch:{ Throwable -> 0x0083, all -> 0x007e }
            com.flurry.android.monolithic.sdk.impl.je.a(r2)     // Catch:{ Throwable -> 0x0048 }
        L_0x0023:
            boolean r0 = r5.v     // Catch:{ Throwable -> 0x0066 }
            if (r0 != 0) goto L_0x006f
            java.io.File r0 = r5.x     // Catch:{ Throwable -> 0x0066 }
            boolean r0 = r0.delete()     // Catch:{ Throwable -> 0x0066 }
            if (r0 == 0) goto L_0x005d
            r0 = 3
            java.lang.String r1 = com.flurry.android.impl.ads.FlurryAdModule.t     // Catch:{ Throwable -> 0x0066 }
            java.lang.String r2 = "Deleted persistence freqCap file"
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r1, r2)     // Catch:{ Throwable -> 0x0066 }
        L_0x0037:
            monitor-exit(r5)
            return
        L_0x0039:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x003d:
            java.lang.String r2 = com.flurry.android.impl.ads.FlurryAdModule.t     // Catch:{ all -> 0x0081 }
            java.lang.String r3 = "Error when loading persistent freqCap file"
            com.flurry.android.monolithic.sdk.impl.ja.b(r2, r3, r0)     // Catch:{ all -> 0x0081 }
            com.flurry.android.monolithic.sdk.impl.je.a(r1)     // Catch:{ Throwable -> 0x0048 }
            goto L_0x0023
        L_0x0048:
            r0 = move-exception
            r1 = 6
            java.lang.String r2 = com.flurry.android.impl.ads.FlurryAdModule.t     // Catch:{ all -> 0x0052 }
            java.lang.String r3 = "Failed to load freqCap cache file with exception:"
            com.flurry.android.monolithic.sdk.impl.ja.a(r1, r2, r3, r0)     // Catch:{ all -> 0x0052 }
            goto L_0x0037
        L_0x0052:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x0055:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0059:
            com.flurry.android.monolithic.sdk.impl.je.a(r1)     // Catch:{ Throwable -> 0x0048 }
            throw r0     // Catch:{ Throwable -> 0x0048 }
        L_0x005d:
            r0 = 6
            java.lang.String r1 = com.flurry.android.impl.ads.FlurryAdModule.t     // Catch:{ Throwable -> 0x0066 }
            java.lang.String r2 = "Cannot delete persistence freqCap file"
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r1, r2)     // Catch:{ Throwable -> 0x0066 }
            goto L_0x0037
        L_0x0066:
            r0 = move-exception
            java.lang.String r1 = com.flurry.android.impl.ads.FlurryAdModule.t     // Catch:{ Throwable -> 0x0048 }
            java.lang.String r2 = ""
            com.flurry.android.monolithic.sdk.impl.ja.b(r1, r2, r0)     // Catch:{ Throwable -> 0x0048 }
            goto L_0x0037
        L_0x006f:
            com.flurry.android.monolithic.sdk.impl.ce r0 = r5.i     // Catch:{ Throwable -> 0x0066 }
            r0.d()     // Catch:{ Throwable -> 0x0066 }
            goto L_0x0037
        L_0x0075:
            r0 = 3
            java.lang.String r1 = com.flurry.android.impl.ads.FlurryAdModule.t     // Catch:{ Throwable -> 0x0048 }
            java.lang.String r2 = "freqCap file doesn't exist."
            com.flurry.android.monolithic.sdk.impl.ja.a(r0, r1, r2)     // Catch:{ Throwable -> 0x0048 }
            goto L_0x0037
        L_0x007e:
            r0 = move-exception
            r1 = r2
            goto L_0x0059
        L_0x0081:
            r0 = move-exception
            goto L_0x0059
        L_0x0083:
            r0 = move-exception
            r1 = r2
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.impl.ads.FlurryAdModule.E():void");
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(DataInputStream dataInputStream) throws IOException {
        while (dataInputStream.readShort() != 0) {
            synchronized (this.i) {
                this.i.a(new cd(dataInputStream));
            }
        }
        this.v = true;
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(ce ceVar, DataOutputStream dataOutputStream) {
        for (cd next : ceVar.c()) {
            try {
                dataOutputStream.writeShort(1);
                next.a(dataOutputStream);
            } catch (IOException e2) {
                ja.a(t, "unable to convert freqCap to byte[]: " + next.b());
            }
        }
    }

    public synchronized m a(m mVar, String str, boolean z2, Map<String, String> map) {
        ja.a(3, t, "changeAdState(" + mVar + ", " + str + ", " + z2 + ", " + map + ")");
        synchronized (mVar) {
            if (!this.l.contains(mVar)) {
                this.l.add(mVar);
                ja.a(3, t, "changeAdState added adLog = " + mVar);
            }
            mVar.a(a(str, z2, map));
        }
        return mVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.impl.ads.FlurryAdModule.a(com.flurry.android.monolithic.sdk.impl.m, java.lang.String, boolean, java.util.Map<java.lang.String, java.lang.String>):com.flurry.android.monolithic.sdk.impl.m
     arg types: [com.flurry.android.monolithic.sdk.impl.m, java.lang.String, int, ?[OBJECT, ARRAY]]
     candidates:
      com.flurry.android.impl.ads.FlurryAdModule.a(java.lang.String, android.view.ViewGroup, boolean, com.flurry.android.FlurryAdSize):java.util.List<com.flurry.android.impl.ads.avro.protocol.v6.AdUnit>
      com.flurry.android.impl.ads.FlurryAdModule.a(com.flurry.android.monolithic.sdk.impl.m, java.lang.String, boolean, java.util.Map<java.lang.String, java.lang.String>):com.flurry.android.monolithic.sdk.impl.m */
    public m F() {
        return a(ab.a(this, null), "unfilled", true, (Map<String, String>) null);
    }

    /* access modifiers changed from: package-private */
    public synchronized k a(String str, boolean z2, Map<String, String> map) {
        return new k(str, z2, i(), map);
    }

    public void b(m mVar) {
        this.E = mVar;
    }

    public m G() {
        return this.E;
    }

    public void a(AdUnit adUnit) {
        this.F = adUnit;
    }

    public AdUnit H() {
        return this.F;
    }

    public List<i> a(AdFrame adFrame, bh bhVar) {
        ArrayList arrayList = new ArrayList();
        List<Callback> f2 = adFrame.f();
        String str = bhVar.a;
        for (Callback next : f2) {
            if (next.b().toString().equals(str)) {
                for (CharSequence obj : next.c()) {
                    HashMap hashMap = new HashMap();
                    String obj2 = obj.toString();
                    int indexOf = obj2.indexOf(63);
                    if (indexOf != -1) {
                        String substring = obj2.substring(0, indexOf);
                        String substring2 = obj2.substring(indexOf + 1);
                        if (substring2.contains("%{eventParams}")) {
                            substring2 = substring2.replace("%{eventParams}", "");
                            hashMap.putAll(bhVar.b);
                        }
                        hashMap.putAll(je.f(substring2));
                        obj2 = substring;
                    }
                    arrayList.add(new i(obj2, hashMap, bhVar));
                }
            }
        }
        return arrayList;
    }

    public void a(bh bhVar, ci ciVar, int i2) {
        i iVar;
        FlurryAdListener flurryAdListener;
        FlurryAdListener flurryAdListener2;
        FlurryAdListener flurryAdListener3;
        boolean z2;
        ja.a(3, t, "onEvent:event=" + bhVar.a + ",params=" + bhVar.b);
        List<i> a2 = a(bhVar.a(), bhVar);
        if (a2.isEmpty()) {
            for (Map.Entry next : this.N.entrySet()) {
                if (((String) next.getKey()).equals(bhVar.a)) {
                    a2.add(new i((String) next.getValue(), bhVar.b, bhVar));
                }
            }
        }
        if (bhVar.a.equals("adWillClose")) {
            Iterator<i> it = a2.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (this.O.contains(it.next().a)) {
                        z2 = true;
                        break;
                    }
                } else {
                    z2 = false;
                    break;
                }
            }
            if (!z2) {
                a2.add(0, new i("closeAd", Collections.emptyMap(), bhVar));
            }
        }
        if (bhVar.a.equals("renderFailed") && (flurryAdListener3 = this.I.get()) != null) {
            flurryAdListener3.onRenderFailed(bhVar.d.b().toString());
        }
        if (bhVar.a.equals("clicked") && (flurryAdListener2 = this.I.get()) != null) {
            flurryAdListener2.onAdClicked(bhVar.d.b().toString());
        }
        if (bhVar.a.equals("videoCompleted") && (flurryAdListener = this.I.get()) != null) {
            flurryAdListener.onVideoCompleted(bhVar.d.b().toString());
        }
        i iVar2 = null;
        for (i next2 : a2) {
            if (next2.a.equals("logEvent")) {
                next2.b.put("__sendToServer", "true");
                iVar = next2;
            } else {
                iVar = iVar2;
            }
            if (next2.a.equals("loadAdComponents")) {
                for (Map.Entry next3 : next2.c.b.entrySet()) {
                    next2.b.put(((String) next3.getKey()).toString(), ((String) next3.getValue()).toString());
                }
            }
            ja.d(t, next2.toString());
            ciVar.a(next2, this, i2 + 1);
            iVar2 = iVar;
        }
        if (iVar2 == null) {
            HashMap hashMap = new HashMap();
            hashMap.put("__sendToServer", "false");
            i iVar3 = new i("logEvent", hashMap, bhVar);
            ciVar.a(iVar3, this, i2 + 1);
            ja.d(t, iVar3.toString());
        }
    }

    public cl a(Context context, AdUnit adUnit) {
        List<AdFrame> d2;
        m mVar;
        this.L = adUnit;
        if (adUnit == null || (d2 = adUnit.d()) == null || d2.isEmpty()) {
            return null;
        }
        AdFrame adFrame = d2.get(0);
        int intValue = adFrame.b().intValue();
        String obj = adFrame.d().toString();
        String obj2 = adFrame.e().e().toString();
        String obj3 = adFrame.g().toString();
        m mVar2 = this.m.get(obj3);
        if (mVar2 == null) {
            mVar = ab.a(this, obj3);
        } else {
            mVar = mVar2;
        }
        a(new bh("filled", Collections.emptyMap(), context, adUnit, mVar, 0), this.U, 1);
        return a(context, adUnit, intValue, obj, obj2, mVar, adFrame);
    }

    /* access modifiers changed from: package-private */
    public cl a(Context context, AdUnit adUnit, int i2, String str, String str2, m mVar, AdFrame adFrame) {
        AdNetworkView adFromNetwork;
        AdCreative a2 = ab.a(adFrame.e());
        if (this.L == null || this.L != adUnit) {
            return this.M;
        }
        this.M = null;
        ICustomAdNetworkHandler k2 = k();
        if (i2 == 4 && k2 != null && (adFromNetwork = k2.getAdFromNetwork(context, a2, str)) != null) {
            adFromNetwork.setPlatformModule(this);
            adFromNetwork.setAdLog(mVar);
            adFromNetwork.setAdFrameIndex(0);
            adFromNetwork.setAdUnit(adUnit);
            this.M = new af(adFromNetwork, adUnit);
        } else if (str2.equals(AdCreative.kFormatTakeover)) {
            b(mVar);
            a(adUnit);
            cn a_ = this.H.a_(context, this, mVar, adUnit);
            if (a_ != null) {
                ja.a(3, t, "prepareAd:first frame of AdUnit is a takeover");
                this.M = new cm(a_, adUnit);
            }
        } else {
            ac a3 = this.G.a(context, this, mVar, adUnit);
            if (a3 != null) {
                ja.a(3, t, "prepareAd: first frame of AdUnit is a banner");
                this.M = new af(a3, adUnit);
            } else {
                ja.e(t, "Failed to create view for ad network: " + str + ", network is unsupported on Android, or no ICustomAdNetworkHandler was registered or it failed to return a view.");
                a(new bh("renderFailed", Collections.emptyMap(), context, adUnit, mVar, 0), this.U, 1);
            }
        }
        return this.M;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x007e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.flurry.android.impl.ads.avro.protocol.v6.AdUnit> a(java.lang.String r9, android.view.ViewGroup r10, boolean r11, com.flurry.android.FlurryAdSize r12) {
        /*
            r8 = this;
            com.flurry.android.monolithic.sdk.impl.ce r0 = r8.i
            r0.d()
            int r0 = android.os.Build.VERSION.SDK_INT
            r1 = 9
            if (r0 < r1) goto L_0x00f2
            android.util.Pair r1 = com.flurry.android.monolithic.sdk.impl.je.h()
            java.lang.Object r0 = r1.first
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r4 = r0.intValue()
            java.lang.Object r0 = r1.second
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r5 = r0.intValue()
            android.util.Pair r1 = com.flurry.android.monolithic.sdk.impl.je.g()
            java.lang.Object r0 = r1.first
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r2 = r0.intValue()
            java.lang.Object r0 = r1.second
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            com.flurry.android.FlurryAdSize r1 = com.flurry.android.FlurryAdSize.BANNER_BOTTOM
            boolean r1 = r12.equals(r1)
            if (r1 != 0) goto L_0x0043
            com.flurry.android.FlurryAdSize r1 = com.flurry.android.FlurryAdSize.BANNER_TOP
            boolean r1 = r12.equals(r1)
            if (r1 == 0) goto L_0x00f7
        L_0x0043:
            if (r10 == 0) goto L_0x0053
            int r1 = r10.getHeight()
            if (r1 <= 0) goto L_0x0053
            int r0 = r10.getHeight()
            int r0 = com.flurry.android.monolithic.sdk.impl.je.a(r0)
        L_0x0053:
            if (r10 == 0) goto L_0x00f7
            int r1 = r10.getWidth()
            if (r1 <= 0) goto L_0x00f7
            int r1 = r10.getWidth()
            int r1 = com.flurry.android.monolithic.sdk.impl.je.a(r1)
            r3 = r0
            r2 = r1
        L_0x0065:
            r0 = r8
            r1 = r9
            r6 = r11
            r7 = r12
            com.flurry.android.impl.ads.avro.protocol.v6.AdRequest r0 = r0.a(r1, r2, r3, r4, r5, r6, r7)
            com.flurry.android.monolithic.sdk.impl.bv r1 = r8.T
            java.lang.String r2 = r8.q()
            com.flurry.android.impl.ads.avro.protocol.v6.AdResponse r0 = r1.a(r0, r2)
            if (r0 != 0) goto L_0x007e
            java.util.List r0 = java.util.Collections.emptyList()
        L_0x007d:
            return r0
        L_0x007e:
            r1 = 3
            java.lang.String r2 = com.flurry.android.impl.ads.FlurryAdModule.t
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Got ad response: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            com.flurry.android.monolithic.sdk.impl.ja.a(r1, r2, r3)
            java.util.List r1 = r0.c()
            int r1 = r1.size()
            if (r1 <= 0) goto L_0x00cb
            java.lang.String r1 = com.flurry.android.impl.ads.FlurryAdModule.t
            java.lang.String r2 = "Ad server responded with the following error(s):"
            com.flurry.android.monolithic.sdk.impl.ja.b(r1, r2)
            java.util.List r0 = r0.c()
            java.util.Iterator r0 = r0.iterator()
        L_0x00b0:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x00c6
            java.lang.Object r8 = r0.next()
            java.lang.CharSequence r8 = (java.lang.CharSequence) r8
            java.lang.String r1 = com.flurry.android.impl.ads.FlurryAdModule.t
            java.lang.String r2 = r8.toString()
            com.flurry.android.monolithic.sdk.impl.ja.b(r1, r2)
            goto L_0x00b0
        L_0x00c6:
            java.util.List r0 = java.util.Collections.emptyList()
            goto L_0x007d
        L_0x00cb:
            boolean r1 = android.text.TextUtils.isEmpty(r9)
            if (r1 != 0) goto L_0x00ed
            java.util.List r1 = r0.b()
            if (r1 == 0) goto L_0x00e1
            java.util.List r1 = r0.b()
            int r1 = r1.size()
            if (r1 != 0) goto L_0x00ed
        L_0x00e1:
            java.lang.String r0 = com.flurry.android.impl.ads.FlurryAdModule.t
            java.lang.String r1 = "Ad server responded but sent no ad units."
            com.flurry.android.monolithic.sdk.impl.ja.b(r0, r1)
            java.util.List r0 = java.util.Collections.emptyList()
            goto L_0x007d
        L_0x00ed:
            java.util.List r0 = r0.b()
            goto L_0x007d
        L_0x00f2:
            java.util.List r0 = java.util.Collections.emptyList()
            goto L_0x007d
        L_0x00f7:
            r3 = r0
            goto L_0x0065
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.impl.ads.FlurryAdModule.a(java.lang.String, android.view.ViewGroup, boolean, com.flurry.android.FlurryAdSize):java.util.List");
    }

    public void b(boolean z2) {
    }

    public boolean I() {
        return jc.a().c();
    }

    private jd N() {
        return jc.a().e();
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        Thread.currentThread().setName(str + " , id = " + Thread.currentThread().getId());
    }

    public void a(Context context, AdUnit adUnit, cl clVar) {
        Context context2 = context;
        a(new bh("requested", Collections.emptyMap(), context2, clVar.b(), this.m.get(adUnit.d().get(0).g().toString()), 0), this.U, 0);
    }

    public void J() {
        this.M = null;
    }

    public FlurryAdListener K() {
        return this.I.get();
    }

    public void f(Context context) {
        bt.a().f(context);
    }

    public void g(Context context) {
        bt.a().g(context);
    }
}
