package com.flurry.android.impl.ads.avro.protocol.v6;

import com.flurry.android.monolithic.sdk.impl.jg;
import com.flurry.android.monolithic.sdk.impl.ji;
import com.flurry.android.monolithic.sdk.impl.ke;
import com.flurry.android.monolithic.sdk.impl.nt;
import com.flurry.android.monolithic.sdk.impl.nu;
import com.flurry.android.monolithic.sdk.impl.nv;
import java.nio.ByteBuffer;

public class AdReportedId extends nu implements nt {
    public static final ji SCHEMA$ = new ke().a("{\"type\":\"record\",\"name\":\"AdReportedId\",\"namespace\":\"com.flurry.android.impl.ads.avro.protocol.v6\",\"fields\":[{\"name\":\"type\",\"type\":\"int\"},{\"name\":\"id\",\"type\":\"bytes\"}]}");
    @Deprecated
    public int a;
    @Deprecated
    public ByteBuffer b;

    public ji a() {
        return SCHEMA$;
    }

    public Object a(int i) {
        switch (i) {
            case 0:
                return Integer.valueOf(this.a);
            case 1:
                return this.b;
            default:
                throw new jg("Bad index");
        }
    }

    public void a(int i, Object obj) {
        switch (i) {
            case 0:
                this.a = ((Integer) obj).intValue();
                return;
            case 1:
                this.b = (ByteBuffer) obj;
                return;
            default:
                throw new jg("Bad index");
        }
    }

    public static Builder b() {
        return new Builder();
    }

    public class Builder extends nv<AdReportedId> {
        private int a;
        private ByteBuffer b;

        private Builder() {
            super(AdReportedId.SCHEMA$);
        }

        public Builder a(int i) {
            a(b()[0], Integer.valueOf(i));
            this.a = i;
            c()[0] = true;
            return this;
        }

        public Builder a(ByteBuffer byteBuffer) {
            a(b()[1], byteBuffer);
            this.b = byteBuffer;
            c()[1] = true;
            return this;
        }

        public AdReportedId a() {
            try {
                AdReportedId adReportedId = new AdReportedId();
                adReportedId.a = c()[0] ? this.a : ((Integer) a(b()[0])).intValue();
                adReportedId.b = c()[1] ? this.b : (ByteBuffer) a(b()[1]);
                return adReportedId;
            } catch (Exception e) {
                throw new jg(e);
            }
        }
    }
}
