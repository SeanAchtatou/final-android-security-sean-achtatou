package com.flurry.android.impl.ads.avro.protocol.v6;

import com.flurry.android.monolithic.sdk.impl.jg;
import com.flurry.android.monolithic.sdk.impl.ji;
import com.flurry.android.monolithic.sdk.impl.ke;
import com.flurry.android.monolithic.sdk.impl.nt;
import com.flurry.android.monolithic.sdk.impl.nu;
import com.flurry.android.monolithic.sdk.impl.nv;
import java.util.List;

public class AdFrame extends nu implements nt {
    public static final ji SCHEMA$ = new ke().a("{\"type\":\"record\",\"name\":\"AdFrame\",\"namespace\":\"com.flurry.android.impl.ads.avro.protocol.v6\",\"fields\":[{\"name\":\"binding\",\"type\":\"int\"},{\"name\":\"display\",\"type\":\"string\"},{\"name\":\"content\",\"type\":\"string\"},{\"name\":\"adSpaceLayout\",\"type\":{\"type\":\"record\",\"name\":\"AdSpaceLayout\",\"fields\":[{\"name\":\"adWidth\",\"type\":\"int\"},{\"name\":\"adHeight\",\"type\":\"int\"},{\"name\":\"fix\",\"type\":\"string\"},{\"name\":\"format\",\"type\":\"string\"},{\"name\":\"alignment\",\"type\":\"string\"}]}},{\"name\":\"callbacks\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"Callback\",\"fields\":[{\"name\":\"event\",\"type\":\"string\"},{\"name\":\"actions\",\"type\":{\"type\":\"array\",\"items\":\"string\"}}]}}},{\"name\":\"adGuid\",\"type\":\"string\"}]}");
    @Deprecated
    public int a;
    @Deprecated
    public CharSequence b;
    @Deprecated
    public CharSequence c;
    @Deprecated
    public AdSpaceLayout d;
    @Deprecated
    public List<Callback> e;
    @Deprecated
    public CharSequence f;

    public ji a() {
        return SCHEMA$;
    }

    public Object a(int i) {
        switch (i) {
            case 0:
                return Integer.valueOf(this.a);
            case 1:
                return this.b;
            case 2:
                return this.c;
            case 3:
                return this.d;
            case 4:
                return this.e;
            case 5:
                return this.f;
            default:
                throw new jg("Bad index");
        }
    }

    public void a(int i, Object obj) {
        switch (i) {
            case 0:
                this.a = ((Integer) obj).intValue();
                return;
            case 1:
                this.b = (CharSequence) obj;
                return;
            case 2:
                this.c = (CharSequence) obj;
                return;
            case 3:
                this.d = (AdSpaceLayout) obj;
                return;
            case 4:
                this.e = (List) obj;
                return;
            case 5:
                this.f = (CharSequence) obj;
                return;
            default:
                throw new jg("Bad index");
        }
    }

    public Integer b() {
        return Integer.valueOf(this.a);
    }

    public CharSequence c() {
        return this.b;
    }

    public CharSequence d() {
        return this.c;
    }

    public AdSpaceLayout e() {
        return this.d;
    }

    public List<Callback> f() {
        return this.e;
    }

    public CharSequence g() {
        return this.f;
    }

    public class Builder extends nv<AdFrame> {
        private Builder() {
            super(AdFrame.SCHEMA$);
        }
    }
}
