package com.flurry.android.impl.ads.avro.protocol.v6;

import com.flurry.android.monolithic.sdk.impl.jg;
import com.flurry.android.monolithic.sdk.impl.ji;
import com.flurry.android.monolithic.sdk.impl.ke;
import com.flurry.android.monolithic.sdk.impl.nt;
import com.flurry.android.monolithic.sdk.impl.nu;
import com.flurry.android.monolithic.sdk.impl.nv;
import java.util.List;

public class Callback extends nu implements nt {
    public static final ji SCHEMA$ = new ke().a("{\"type\":\"record\",\"name\":\"Callback\",\"namespace\":\"com.flurry.android.impl.ads.avro.protocol.v6\",\"fields\":[{\"name\":\"event\",\"type\":\"string\"},{\"name\":\"actions\",\"type\":{\"type\":\"array\",\"items\":\"string\"}}]}");
    @Deprecated
    public CharSequence a;
    @Deprecated
    public List<CharSequence> b;

    public ji a() {
        return SCHEMA$;
    }

    public Object a(int i) {
        switch (i) {
            case 0:
                return this.a;
            case 1:
                return this.b;
            default:
                throw new jg("Bad index");
        }
    }

    public void a(int i, Object obj) {
        switch (i) {
            case 0:
                this.a = (CharSequence) obj;
                return;
            case 1:
                this.b = (List) obj;
                return;
            default:
                throw new jg("Bad index");
        }
    }

    public CharSequence b() {
        return this.a;
    }

    public List<CharSequence> c() {
        return this.b;
    }

    public class Builder extends nv<Callback> {
        private Builder() {
            super(Callback.SCHEMA$);
        }
    }
}
