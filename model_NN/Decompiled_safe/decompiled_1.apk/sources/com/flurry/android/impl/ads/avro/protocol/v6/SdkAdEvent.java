package com.flurry.android.impl.ads.avro.protocol.v6;

import com.flurry.android.monolithic.sdk.impl.jg;
import com.flurry.android.monolithic.sdk.impl.ji;
import com.flurry.android.monolithic.sdk.impl.ke;
import com.flurry.android.monolithic.sdk.impl.nt;
import com.flurry.android.monolithic.sdk.impl.nu;
import com.flurry.android.monolithic.sdk.impl.nv;
import java.util.Map;

public class SdkAdEvent extends nu implements nt {
    public static final ji SCHEMA$ = new ke().a("{\"type\":\"record\",\"name\":\"SdkAdEvent\",\"namespace\":\"com.flurry.android.impl.ads.avro.protocol.v6\",\"fields\":[{\"name\":\"type\",\"type\":\"string\"},{\"name\":\"params\",\"type\":{\"type\":\"map\",\"values\":\"string\"}},{\"name\":\"timeOffset\",\"type\":\"long\"}]}");
    @Deprecated
    public CharSequence a;
    @Deprecated
    public Map<CharSequence, CharSequence> b;
    @Deprecated
    public long c;

    public ji a() {
        return SCHEMA$;
    }

    public Object a(int i) {
        switch (i) {
            case 0:
                return this.a;
            case 1:
                return this.b;
            case 2:
                return Long.valueOf(this.c);
            default:
                throw new jg("Bad index");
        }
    }

    public void a(int i, Object obj) {
        switch (i) {
            case 0:
                this.a = (CharSequence) obj;
                return;
            case 1:
                this.b = (Map) obj;
                return;
            case 2:
                this.c = ((Long) obj).longValue();
                return;
            default:
                throw new jg("Bad index");
        }
    }

    public void a(CharSequence charSequence) {
        this.a = charSequence;
    }

    public void a(Map<CharSequence, CharSequence> map) {
        this.b = map;
    }

    public void a(Long l) {
        this.c = l.longValue();
    }

    public class Builder extends nv<SdkAdEvent> {
        private Builder() {
            super(SdkAdEvent.SCHEMA$);
        }
    }
}
