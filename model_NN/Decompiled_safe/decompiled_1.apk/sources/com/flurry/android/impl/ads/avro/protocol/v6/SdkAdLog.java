package com.flurry.android.impl.ads.avro.protocol.v6;

import com.flurry.android.monolithic.sdk.impl.jg;
import com.flurry.android.monolithic.sdk.impl.ji;
import com.flurry.android.monolithic.sdk.impl.ke;
import com.flurry.android.monolithic.sdk.impl.nt;
import com.flurry.android.monolithic.sdk.impl.nu;
import com.flurry.android.monolithic.sdk.impl.nv;
import java.util.List;

public class SdkAdLog extends nu implements nt {
    public static final ji SCHEMA$ = new ke().a("{\"type\":\"record\",\"name\":\"SdkAdLog\",\"namespace\":\"com.flurry.android.impl.ads.avro.protocol.v6\",\"fields\":[{\"name\":\"sessionId\",\"type\":\"long\"},{\"name\":\"adLogGUID\",\"type\":\"string\"},{\"name\":\"sdkAdEvents\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"SdkAdEvent\",\"fields\":[{\"name\":\"type\",\"type\":\"string\"},{\"name\":\"params\",\"type\":{\"type\":\"map\",\"values\":\"string\"}},{\"name\":\"timeOffset\",\"type\":\"long\"}]}}}]}");
    @Deprecated
    public long a;
    @Deprecated
    public CharSequence b;
    @Deprecated
    public List<SdkAdEvent> c;

    public ji a() {
        return SCHEMA$;
    }

    public Object a(int i) {
        switch (i) {
            case 0:
                return Long.valueOf(this.a);
            case 1:
                return this.b;
            case 2:
                return this.c;
            default:
                throw new jg("Bad index");
        }
    }

    public void a(int i, Object obj) {
        switch (i) {
            case 0:
                this.a = ((Long) obj).longValue();
                return;
            case 1:
                this.b = (CharSequence) obj;
                return;
            case 2:
                this.c = (List) obj;
                return;
            default:
                throw new jg("Bad index");
        }
    }

    public void a(Long l) {
        this.a = l.longValue();
    }

    public void a(CharSequence charSequence) {
        this.b = charSequence;
    }

    public void a(List<SdkAdEvent> list) {
        this.c = list;
    }

    public class Builder extends nv<SdkAdLog> {
        private Builder() {
            super(SdkAdLog.SCHEMA$);
        }
    }
}
