package com.flurry.android.impl.ads.avro.protocol.v6;

import com.flurry.android.monolithic.sdk.impl.jg;
import com.flurry.android.monolithic.sdk.impl.ji;
import com.flurry.android.monolithic.sdk.impl.ke;
import com.flurry.android.monolithic.sdk.impl.nt;
import com.flurry.android.monolithic.sdk.impl.nu;
import com.flurry.android.monolithic.sdk.impl.nv;

public class AdViewContainer extends nu implements nt {
    public static final ji SCHEMA$ = new ke().a("{\"type\":\"record\",\"name\":\"AdViewContainer\",\"namespace\":\"com.flurry.android.impl.ads.avro.protocol.v6\",\"fields\":[{\"name\":\"viewWidth\",\"type\":\"int\",\"default\":0},{\"name\":\"viewHeight\",\"type\":\"int\",\"default\":0},{\"name\":\"screenWidth\",\"type\":\"int\",\"default\":0},{\"name\":\"screenHeight\",\"type\":\"int\",\"default\":0},{\"name\":\"density\",\"type\":\"float\",\"default\":1.0}]}");
    @Deprecated
    public int a;
    @Deprecated
    public int b;
    @Deprecated
    public int c;
    @Deprecated
    public int d;
    @Deprecated
    public float e;

    public ji a() {
        return SCHEMA$;
    }

    public Object a(int i) {
        switch (i) {
            case 0:
                return Integer.valueOf(this.a);
            case 1:
                return Integer.valueOf(this.b);
            case 2:
                return Integer.valueOf(this.c);
            case 3:
                return Integer.valueOf(this.d);
            case 4:
                return Float.valueOf(this.e);
            default:
                throw new jg("Bad index");
        }
    }

    public void a(int i, Object obj) {
        switch (i) {
            case 0:
                this.a = ((Integer) obj).intValue();
                return;
            case 1:
                this.b = ((Integer) obj).intValue();
                return;
            case 2:
                this.c = ((Integer) obj).intValue();
                return;
            case 3:
                this.d = ((Integer) obj).intValue();
                return;
            case 4:
                this.e = ((Float) obj).floatValue();
                return;
            default:
                throw new jg("Bad index");
        }
    }

    public static Builder b() {
        return new Builder();
    }

    public class Builder extends nv<AdViewContainer> {
        private int a;
        private int b;
        private int c;
        private int d;
        private float e;

        private Builder() {
            super(AdViewContainer.SCHEMA$);
        }

        public Builder a(int i) {
            a(b()[0], Integer.valueOf(i));
            this.a = i;
            c()[0] = true;
            return this;
        }

        public Builder b(int i) {
            a(b()[1], Integer.valueOf(i));
            this.b = i;
            c()[1] = true;
            return this;
        }

        public Builder c(int i) {
            a(b()[2], Integer.valueOf(i));
            this.c = i;
            c()[2] = true;
            return this;
        }

        public Builder d(int i) {
            a(b()[3], Integer.valueOf(i));
            this.d = i;
            c()[3] = true;
            return this;
        }

        public Builder a(float f) {
            a(b()[4], Float.valueOf(f));
            this.e = f;
            c()[4] = true;
            return this;
        }

        public AdViewContainer a() {
            try {
                AdViewContainer adViewContainer = new AdViewContainer();
                adViewContainer.a = c()[0] ? this.a : ((Integer) a(b()[0])).intValue();
                adViewContainer.b = c()[1] ? this.b : ((Integer) a(b()[1])).intValue();
                adViewContainer.c = c()[2] ? this.c : ((Integer) a(b()[2])).intValue();
                adViewContainer.d = c()[3] ? this.d : ((Integer) a(b()[3])).intValue();
                adViewContainer.e = c()[4] ? this.e : ((Float) a(b()[4])).floatValue();
                return adViewContainer;
            } catch (Exception e2) {
                throw new jg(e2);
            }
        }
    }
}
