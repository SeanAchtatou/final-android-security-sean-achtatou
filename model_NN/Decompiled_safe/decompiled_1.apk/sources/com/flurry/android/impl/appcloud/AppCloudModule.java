package com.flurry.android.impl.appcloud;

import android.content.Context;
import com.flurry.android.monolithic.sdk.impl.fl;
import com.flurry.android.monolithic.sdk.impl.go;
import com.flurry.android.monolithic.sdk.impl.hd;
import com.flurry.android.monolithic.sdk.impl.ik;
import com.flurry.android.monolithic.sdk.impl.jb;
import com.flurry.android.monolithic.sdk.impl.jc;

public class AppCloudModule implements ik, jb {
    static int a = 0;
    static int b = 5;
    private static final String e = AppCloudModule.class.getSimpleName();
    private static AppCloudModule f;
    String c;
    volatile boolean d;
    private final hd g = new hd();

    public static synchronized AppCloudModule getInstance() {
        AppCloudModule appCloudModule;
        synchronized (AppCloudModule.class) {
            if (f == null) {
                f = new AppCloudModule();
            }
            appCloudModule = f;
        }
        return appCloudModule;
    }

    private AppCloudModule() {
        jc.a().a(this);
    }

    public void a(Context context, fl flVar) {
        jc.a().b();
        if (!this.d) {
            this.c = flVar.a;
            this.d = true;
        }
        go.a(this.c);
    }

    public boolean a() {
        return this.d;
    }

    public hd b() {
        return this.g;
    }

    public void a(Context context) {
        if (this.g != null) {
            this.g.b();
        }
    }

    public void b(Context context) {
        if (this.g != null) {
            this.g.c();
        }
    }

    public static boolean c() {
        return go.d().contains("localhost");
    }

    public boolean d() {
        if (c()) {
            return true;
        }
        return e();
    }

    public void b(boolean z) {
    }

    public boolean e() {
        return jc.a().c();
    }
}
